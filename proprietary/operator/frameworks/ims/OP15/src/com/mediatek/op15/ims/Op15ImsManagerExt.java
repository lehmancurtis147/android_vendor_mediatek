/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op15.ims;

import android.content.Context;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;

import com.mediatek.ims.internal.ext.ImsManagerExt;

/**
 * Plugin implementation for Imsmanager.
 */

public class Op15ImsManagerExt extends ImsManagerExt {

    private static final String TAG = "Entitlement-Op15ImsManagerExt";

    //private static final String VLT_SETTING_SYTEM_PROPERTY = "vendor.ril.vt.setting.support";
    //private static final String WFC_SETTING_SYTEM_PROPERTY = "vendor.ril.wfc.setting.support";
    private Context mContext;

    /**
     * Constructor of plugin.
     * @param context context
     */
    public Op15ImsManagerExt(Context context) {
        super();
        mContext = context;
    }

    @Override
    /**
      * An API to customize platform enabled status.
      * @param context The context for retrive plug-in.
      * @param feature The IMS feature defined in ImsConfig.FeatureConstants.
      * @param phoneId PhoneId for which feature to be enabled.
      * @return return enabled status.
      */
    public boolean isFeatureEnabledByPlatform(Context context, int feature, int phoneId) {
        if ("1".equals(SystemProperties.get("persist.vendor.mtk_bypass_ImsBySim"))) {
            Log.d(TAG, "Due to system property set, return directly");
            return true;
        }
        Log.d(TAG, "feature:" + feature);
        if ((feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE)
                || (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI)) {
                return isFeatureProvisionedOnDevice(feature);
        } else {
            return super.isFeatureEnabledByPlatform(context, feature, phoneId);
        }
    }

    public boolean isFeatureProvisionedOnDevice(int feature) {
        ImsConfig imsConfig = null;
        ImsManager imsManager = ImsManager.getInstance(mContext, SubscriptionManager
                .getDefaultVoicePhoneId());
        int value = 0;
        boolean result = false;
        if (!isEntitlementEnabled()) {
            Log.d(TAG, "Entitlement sys property not enabled return true");
            return true;
        }
        if (imsManager != null) {
            try {
                imsConfig = imsManager.getConfigInterface();
                if (imsConfig != null) {
                    if(feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE) {
                        value = imsConfig.getProvisionedValue(
                            ImsConfig.ConfigConstants.VLT_SETTING_ENABLED);
                        Log.d(TAG, "VoLTE provisioned value = " + value);
                        if (value == 1) {
                            result = true;
                        }
                    } else {
                        value = imsConfig.getProvisionedValue(
                            ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED);
                        Log.d(TAG, "VoWifi provisioned value = " + value);
                        if (value == 1) {
                            result = true;
                        }
                    }

                }
            } catch (ImsException e) {
                Log.e(TAG, "Volte not updated, ImsConfig null");
                e.printStackTrace();
            } catch (RuntimeException e) {
                Log.e(TAG, "ImsConfig not ready");
                e.printStackTrace();
            }
        } else {
            Log.e(TAG, "Volte not updated, ImsManager null");
        }
        /*if (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE) {
            result = SystemProperties.get(VLT_SETTING_SYTEM_PROPERTY).equals("1");
            Log.d(TAG, "volte system property value = " + result);
        } else if (feature == ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI) {
            result = SystemProperties.get(WFC_SETTING_SYTEM_PROPERTY).equals("1");
            Log.d(TAG, "wfc system property value = " + result);
        }*/
        Log.d(TAG, "isFeatureProvisionedOnDevice returns " + result);
        return result;
    }

    private boolean isEntitlementEnabled() {
        boolean isEntitlementEnabled = (1 == SystemProperties.getInt
                ("persist.vendor.entitlement_enabled", 1) ? true : false);
        Log.d(TAG, "in Op15fwkplugin, isEntitlementEnabled:" + isEntitlementEnabled);
        return isEntitlementEnabled;
    }
}