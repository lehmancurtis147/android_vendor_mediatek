package com.mediatek.op.wifi;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;
import com.android.server.wifi.WifiConfigManager;
import com.android.server.wifi.WifiInjector;
import com.android.server.wifi.WifiStateMachine;

import com.mediatek.provider.MtkSettingsExt;
import com.mediatek.server.wifi.MtkWifiServiceAdapter.IMtkWifiService;
import com.mediatek.server.wifi.WifiOperatorFactoryBase.DefaultMtkWifiServiceExt;
import com.mediatek.server.wifi.WifiOperatorFactoryBase.IMtkWifiServiceExt;

import java.util.List;

/**
 * OP01 plugin for wifi-service.
 */
public class Op01WifiService extends DefaultMtkWifiServiceExt {
    private static final String TAG = "Op01WifiService";

    private ConnectTypeObserver mConnectTypeObserver;
    private ReminderTypeObserver mReminderTypeObserver;
    private long mSwitchSuspendTime = 0;
    private long mReselectSuspendTime = 0;
    private int mCellToWiFiPolicy = MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO;
    private int mReminderType = IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS;
    private ConnectivityManager mCm;
    private ActivityManager mAm;

    public Op01WifiService(Context context, IMtkWifiService service) {
        super(context, service);
    }

    public void init() {
        super.init();
        mCm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        mAm = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        mCellToWiFiPolicy = Settings.System.getInt(
            mContext.getContentResolver(),
            MtkSettingsExt.System.WIFI_CONNECT_TYPE,
            MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO);
        mReminderType = Settings.System.getInt(
            mContext.getContentResolver(),
            MtkSettingsExt.System.WIFI_CONNECT_REMINDER,
            IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS);
        mConnectTypeObserver = new ConnectTypeObserver(new Handler(), mContext);
        mReminderTypeObserver = new ReminderTypeObserver(new Handler(), mContext);
        registerBroadcastForAp(mContext);
    }

    private void registerBroadcastForAp(Context context) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.WIFI_AP_STATE_CHANGED_ACTION);
        intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
        intentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        context.registerReceiver(new WifiSettingsReceiver(), intentFilter);
    }

    public boolean hasCustomizedAutoConnect() {
        return true;
    }

    public boolean shouldAutoConnect() {
        if (mCellToWiFiPolicy == MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO) {
            Log.d(TAG, "Should auto connect.");
            return true;
        } else {
            Log.d(TAG, "Shouldn't auto connect.");
            return false;
        }
    }

    private String makeNoise(String origin) {
        if (origin == null) {
            return null;
        }
        char [] org = origin.toCharArray();
        if (org == null) {
            return null;
        }

        String out = new String();
        for(int i=0; i<org.length; i++) {
            out += (org[i] - 'A') + ",";
        }

        return out;
    }

    public boolean isWifiConnecting(int connectingNetworkId, List<Integer> disconnectNetworks) {
        Log.d(TAG, "isWifiConnecting, mCellToWiFiPolicy:" + mCellToWiFiPolicy
            + ", connectingNetworkId:" + connectingNetworkId);
        boolean isConnecting = false;
        boolean autoConnect = false;
        NetworkInfo info = mCm.getActiveNetworkInfo();
        if (null == info) {
            Log.d(TAG, "No active network.");
        } else {
            Log.d(TAG, "Active network type:" + info.getTypeName());
        }
        String highestPriorityNetworkSSID = null;
        int highestPriority = -1;
        int highestPriorityNetworkId = -1;
        WifiInjector injector = WifiInjector.getInstance();
        List<WifiConfiguration> networks = injector.getWifiConfigManager().getSavedNetworks();
        Log.d(TAG, "getSavedNetworks() returned with " + networks.size() + " networks");
        List<ScanResult> scanResults = mService.getLatestScanResults();
        if (null != networks && null != scanResults) {
            for (WifiConfiguration network : networks) {
                for (ScanResult scanResult : scanResults) {
                    if ((network.SSID != null) && (scanResult.SSID != null)
                        && network.SSID.equals("\"" + scanResult.SSID + "\"")
                        && (getSecurity(network) == getSecurity(scanResult))) {
                        if (network.priority > highestPriority) {
                            highestPriority = network.priority;
                            highestPriorityNetworkId = network.networkId;
                            highestPriorityNetworkSSID = network.SSID;
                        }
                        if (network.networkId == connectingNetworkId) {
                            isConnecting = true;
                        }
                    }
                }
            }
        }
        Log.d(TAG, "highestPriorityNetworkId:" + highestPriorityNetworkId
            + ", highestPriorityNetworkSSID:" + makeNoise(highestPriorityNetworkSSID)
            + ", highestPriority:" + highestPriority
            + ", currentTimeMillis:" + System.currentTimeMillis()
            + ", mSwitchSuspendTime:" + mSwitchSuspendTime
            + ", mReminderType:" + mReminderType);
        if (!isConnecting) {
            if (null != info && info.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (mCellToWiFiPolicy == MtkSettingsExt.System.WIFI_CONNECT_TYPE_ASK) {
                    if (highestPriorityNetworkId != -1
                        && !TextUtils.isEmpty(highestPriorityNetworkSSID)
                        && (System.currentTimeMillis() - mSwitchSuspendTime >
                            IMtkWifiServiceExt.SUSPEND_NOTIFICATION_DURATION)
                        && mReminderType == IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS) {
                        // check WifiNotifyDialog
                        if (!mService.getShowReselectDialog()) {
                            Intent intent = new Intent(IMtkWifiServiceExt.WIFI_NOTIFICATION_ACTION);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            intent.putExtra(IMtkWifiServiceExt.EXTRA_NOTIFICATION_SSID,
                                            highestPriorityNetworkSSID);
                            intent.putExtra(IMtkWifiServiceExt.EXTRA_NOTIFICATION_NETWORKID,
                                            highestPriorityNetworkId);
                            mContext.startActivity(intent);
                        }
                    }
                } else if (mCellToWiFiPolicy == MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO) {
                    highestPriorityNetworkSSID = null;
                    highestPriority = -1;
                    highestPriorityNetworkId = -1;
                    if (null != networks && null != scanResults) {
                        for (WifiConfiguration network : networks) {
                            if (!disconnectNetworks.contains(network.networkId)) {
                                for (ScanResult scanresult : scanResults) {
                                    if ((network.SSID != null) && (scanresult.SSID != null)
                                        && network.SSID.equals("\"" + scanresult.SSID + "\"")
                                        && (getSecurity(network) == getSecurity(scanresult))) {
                                        if (network.priority > highestPriority) {
                                            highestPriority = network.priority;
                                            highestPriorityNetworkId = network.networkId;
                                            highestPriorityNetworkSSID = network.SSID;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Log.d(TAG,
                          "Mobile connected, highestPriorityNetworkId:" + highestPriorityNetworkId
                        + ", highestPriorityNetworkSSID:" + makeNoise(highestPriorityNetworkSSID)
                        + ", highestPriority:" + highestPriority);
                    if (highestPriorityNetworkId != -1
                        && !TextUtils.isEmpty(highestPriorityNetworkSSID)) {
                        Log.d(TAG, "Enable all networks for mobile is connected.");
                        autoConnect = true;
                    }
                } else if (mCellToWiFiPolicy == MtkSettingsExt.System.WIFI_CONNECT_TYPE_MANUL) {
                    ComponentName cn = null;
                    String classname = null;
                    List<RunningTaskInfo> runningTasks = mAm.getRunningTasks(1);
                    if (runningTasks != null
                        && runningTasks.size() > 0
                        && runningTasks.get(0) != null) {
                        cn = runningTasks.get(0).topActivity;
                    }
                    if (cn != null) {
                        classname = cn.getClassName();
                        Log.d(TAG, "Class Name:" + classname);
                    } else {
                        Log.e(TAG, "ComponentName is null!");
                    }
                    if (!IMtkWifiServiceExt.WIFISETTINGS_CLASSNAME.equals(classname)
                        && highestPriorityNetworkId != -1
                        && !TextUtils.isEmpty(highestPriorityNetworkSSID)
                        && (System.currentTimeMillis() - mSwitchSuspendTime >
                            IMtkWifiServiceExt.SUSPEND_NOTIFICATION_DURATION)
                        && mReminderType == IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS) {
                        Intent intent = new Intent(IMtkWifiServiceExt.WIFI_NOTIFICATION_ACTION);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra(IMtkWifiServiceExt.EXTRA_NOTIFICATION_SSID,
                                        highestPriorityNetworkSSID);
                        intent.putExtra(IMtkWifiServiceExt.EXTRA_NOTIFICATION_NETWORKID,
                                        highestPriorityNetworkId);
                        mContext.startActivity(intent);
                    }
                }
            } else {
                if (mCellToWiFiPolicy == MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO) {
                    highestPriorityNetworkSSID = null;
                    highestPriority = -1;
                    highestPriorityNetworkId = -1;
                    if (null != networks && null != scanResults) {
                        for (WifiConfiguration network : networks) {
                            if (!disconnectNetworks.contains(network.networkId)) {
                                for (ScanResult scanresult : scanResults) {
                                    if ((network.SSID != null) && (scanresult.SSID != null)
                                        && network.SSID.equals("\"" + scanresult.SSID + "\"")
                                        && (getSecurity(network) == getSecurity(scanresult))) {
                                        if (network.priority > highestPriority) {
                                            highestPriority = network.priority;
                                            highestPriorityNetworkId = network.networkId;
                                            highestPriorityNetworkSSID = network.SSID;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    Log.d(TAG,
                          "Mobile isn't connected, highestPriorityNetworkId:"
                          + highestPriorityNetworkId
                          + ", highestPriorityNetworkSSID:" + makeNoise(highestPriorityNetworkSSID)
                          + ", highestPriority:" + highestPriority);
                    if (highestPriorityNetworkId != -1
                        && !TextUtils.isEmpty(highestPriorityNetworkSSID)) {
                        Log.d(TAG, "Enable all networks for mobile is not connected.");
                        autoConnect = true;
                    }
                }
            }
        }
        Log.d(TAG,
              "isWifiConnecting, isConnecting:" + isConnecting + ", autoConnect:" + autoConnect);
        return (isConnecting || autoConnect);
    }

    public boolean hasConnectableAp() {
        WifiStateMachine wsm = WifiInjector.getInstance().getWifiStateMachine();
        if (wsm.syncGetWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            Log.d(TAG, "Scan for checking connectable AP.");
            WifiManager wm = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            wm.startScan();
            return true;
        } else {
            return false;
        }
    }

    public void suspendNotification(int type) {
        if (IMtkWifiServiceExt.NOTIFY_TYPE_SWITCH == type) {
            mSwitchSuspendTime = System.currentTimeMillis();
        } else if (IMtkWifiServiceExt.NOTIFY_TYPE_RESELECT == type) {
            mReselectSuspendTime = System.currentTimeMillis();
        }
        Log.d(TAG, "suspendNotification, mSwitchSuspendTime:" + mSwitchSuspendTime
            + ", mReselectSuspendTime:" + mReselectSuspendTime + ", type:" + type);
    }

    public int defaultFrameworkScanIntervalMs() {
        return IMtkWifiServiceExt.DEFAULT_FRAMEWORK_SCAN_INTERVAL_MS;
    }

    public String getApDefaultSsid() {
        return mContext.getString(
          com.mediatek.internal.R.string.wifi_tether_configure_ssid_default_for_cmcc);
    }

    public boolean handleNetworkReselection() {
        Log.d(TAG,
              "handleNetworkReselection, currentTimeMillis:" + System.currentTimeMillis()
              + ", mReselectSuspendTime:" + mReselectSuspendTime
              + ", mReminderType:" + mReminderType);
        if (System.currentTimeMillis() - mReselectSuspendTime >
                IMtkWifiServiceExt.SUSPEND_NOTIFICATION_DURATION
            && mReminderType == IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS) {
            ComponentName cn = null;
            String classname = null;
            List<RunningTaskInfo> runningTasks = mAm.getRunningTasks(1);
            if (runningTasks != null && runningTasks.size() > 0 && runningTasks.get(0) != null) {
                cn = runningTasks.get(0).topActivity;
            }
            if (cn != null) {
                classname = cn.getClassName();
                Log.d(TAG, "Class Name:" + classname);
            } else {
                Log.e(TAG, "ComponentName is null!");
            }
            if (!IMtkWifiServiceExt.RESELECT_DIALOG_CLASSNAME.equals(classname)) {
                Intent intent = new Intent(IMtkWifiServiceExt.ACTION_RESELECTION_AP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
                return true;
            }
        }
        return false;
    }

    private void sendUpdateSettingsBroadcast() {
        Intent intent = new Intent(IMtkWifiServiceExt.AUTOCONNECT_SETTINGS_CHANGE);
        intent.addFlags(Intent.FLAG_RECEIVER_REGISTERED_ONLY_BEFORE_BOOT);
        mContext.sendBroadcastAsUser(intent, UserHandle.ALL);
    }

    /**
     * Has network selection or not.
     * @return the operator value
     */
    public int hasNetworkSelection() {
        return IMtkWifiServiceExt.OP_01;
    }

    private class ConnectTypeObserver extends ContentObserver {
        private Context mMyContext;
        public ConnectTypeObserver(Handler handler, Context context) {
            super(handler);
            mMyContext = context;
            ContentResolver cr = mMyContext.getContentResolver();
            cr.registerContentObserver(Settings.System.getUriFor(
                MtkSettingsExt.System.WIFI_CONNECT_TYPE), false, this);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            mCellToWiFiPolicy = Settings.System.getInt(
                mMyContext.getContentResolver(),
                MtkSettingsExt.System.WIFI_CONNECT_TYPE,
                MtkSettingsExt.System.WIFI_CONNECT_TYPE_AUTO);
            mSwitchSuspendTime = 0;
            mReselectSuspendTime = 0;
            Log.d(TAG, "ConnectTypeObserver, mCellToWiFiPolicy:" + mCellToWiFiPolicy);
            sendUpdateSettingsBroadcast();
        }
    }

    /**
     * Reminder type observer.
     */
    private class ReminderTypeObserver extends ContentObserver {
        private Context mMyContext;
        public ReminderTypeObserver(Handler handler, Context context) {
            super(handler);
            mMyContext = context;
            ContentResolver cr = mMyContext.getContentResolver();
            cr.registerContentObserver(Settings.System.getUriFor(
                MtkSettingsExt.System.WIFI_CONNECT_REMINDER), false, this);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            mReminderType = Settings.System.getInt(mMyContext.getContentResolver(),
                MtkSettingsExt.System.WIFI_CONNECT_REMINDER,
                IMtkWifiServiceExt.WIFI_CONNECT_REMINDER_ALWAYS);
            Log.d(TAG, "ReminderTypeObserver, mReminderType:" + mReminderType);
        }
    }
}

