package com.mediatek.op02.telephony;

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import android.os.Build;
import android.os.SystemProperties;

import android.telephony.Rlog;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.SubscriptionController;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.datasub.IDataSubSelectorOPExt;
import com.mediatek.internal.telephony.datasub.DataSubSelector;
import com.mediatek.internal.telephony.datasub.DataSubSelectorUtil;
import com.mediatek.internal.telephony.datasub.ISimSwitchForDSSExt;
import com.mediatek.internal.telephony.datasub.CapabilitySwitch;
import com.mediatek.internal.telephony.datasub.DataSubConstants;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;

public class Op02DataSubSelectorOpExt implements IDataSubSelectorOPExt {
    private static final boolean USER_BUILD = TextUtils.equals(Build.TYPE, "user");
    private static boolean DBG = true;

    private static Context mContext = null;

    private static String LOG_TAG = "Op02DSSelector";

    private static DataSubSelector mDataSubSelector = null;

    private static ISimSwitchForDSSExt mSimSwitchForDSS = null;
    private static CapabilitySwitch mCapabilitySwitch = null;
    private int mPhoneNum = 0;

    public Op02DataSubSelectorOpExt(Context context) {
        mContext = context;
    }

    public void init(DataSubSelector dataSubSelector, ISimSwitchForDSSExt simSwitchForDSS) {
        mDataSubSelector = dataSubSelector;
        mSimSwitchForDSS = simSwitchForDSS;
        mCapabilitySwitch = CapabilitySwitch.getInstance(mContext, dataSubSelector);
        mPhoneNum = mDataSubSelector.getPhoneNum();
    }

    @Override
    public void handleSimStateChanged(Intent intent) {
        int simStatus = intent.getIntExtra(TelephonyManager.EXTRA_SIM_STATE,
                TelephonyManager.SIM_STATE_UNKNOWN);
        log("handleSimStateChanged, simStatus = " + simStatus);
        int slotId = intent.getIntExtra(PhoneConstants.SLOT_KEY, PhoneConstants.SIM_ID_1);
        if (simStatus == TelephonyManager.SIM_STATE_LOADED) {
            if (CapabilitySwitch.isNeedWaitImsi() || CapabilitySwitch.isNeedWaitImsiRoaming()) {
                handleNeedWaitImsi(intent);
            } else if (CapabilitySwitch.isNeedWaitUnlock()
                    || CapabilitySwitch.isNeedWaitUnlockRoaming()) {
                handleNeedWaitUnlock(intent);
            }
            mCapabilitySwitch.handleSimImsiStatus(intent);
        } else if (simStatus == TelephonyManager.SIM_STATE_NOT_READY) {
            mCapabilitySwitch.handleSimImsiStatus(intent);
        }
    }

    @Override
    public void handleSubinfoRecordUpdated(Intent intent) {
        int detectedType = intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE);
        log("handleSubinfoRecordUpdated, detectedType = " + detectedType);
        if (detectedType != MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE) {
            if (SystemProperties.getBoolean("ro.vendor.mtk_disable_cap_switch", false) == true) {
                subSelectorWithoutCapabilityChange();
            } else {
                subSelector(intent);
            }
        }
    }

    public void handlePlmnChanged(Intent intent) {

        String plmn = intent.getStringExtra(TelephonyIntents.EXTRA_PLMN);
        if ((plmn != null) && !("".equals(plmn)) && !("000000".equals(plmn))) {
            log("plmn = " + plmn);
            SharedPreferences preference = mContext.getSharedPreferences(
                    DataSubConstants.FIRST_TIME_ROAMING, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preference.edit();
            boolean firstTimeRoaming = preference.getBoolean(
                    DataSubConstants.NEED_TO_EXECUTE_ROAMING, true);
            if (!(plmn.startsWith(RadioCapabilitySwitchUtil.CN_MCC))) {
                if (firstTimeRoaming == true) {
                    if (CapabilitySwitch.isNeedWaitImsi() == false) {
                        checkCapSwitch(DataSubConstants.ROAMING_POLICY);
                    } else {
                        // If onSubInfoReady doesn't get IMSI, we assume
                        // checkOp02CapSwitch get the same result
                        CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(true));
                    }
                }
            } else {
                // Plmn is in home location.
                if (firstTimeRoaming == false) {
                    // Reset first_time_roaming flag
                    log("reset roaming flag");
                    editor.clear();
                    editor.commit();
                }
            }
        }
    }

    private void handleNeedWaitImsi(Intent intent) {
        if (CapabilitySwitch.isNeedWaitImsi()) {
            CapabilitySwitch.setNeedWaitImsi(Boolean.toString(false));
            log("get imsi and need to check op02 again");
            if (checkCapSwitch(DataSubConstants.HOME_POLICY) == false) {
                CapabilitySwitch.setNeedWaitImsi(Boolean.toString(true));
            }
        }
        if (CapabilitySwitch.isNeedWaitImsiRoaming() == true) {
            CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(false));
            log("get imsi and need to check op02Roaming again");
            if (checkCapSwitch(DataSubConstants.ROAMING_POLICY) == false) {
                CapabilitySwitch.setNeedWaitImsiRoaming(Boolean.toString(true));
            }
        }
    }

    private void handleNeedWaitUnlock(Intent intent) {
        if (CapabilitySwitch.isNeedWaitUnlock()) {
            CapabilitySwitch.setNeedWaitUnlock("false");
            if (SystemProperties.getBoolean("ro.vendor.mtk_disable_cap_switch", false) == true) {
                subSelectorWithoutCapabilityChange();
            } else {
                subSelector(intent);
            }
        }
        if (CapabilitySwitch.isNeedWaitUnlockRoaming()) {
            CapabilitySwitch.setNeedWaitUnlockRoaming("false");
            checkCapSwitch(DataSubConstants.ROAMING_POLICY);
        }
    }

    @Override
    public void subSelector(Intent intent) {
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int insertedSimCount = 0;
        int insertedStatus = 0;
        String[] currIccId = new String[mPhoneNum];

        log("DataSubSelector for op02 (subSelectorForOp02)");

        for (int i = 0; i < mPhoneNum; i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (!USER_BUILD) {
                log("currIccid[" + i + "] : " + SubscriptionInfo.givePrintableIccid(currIccId[i]));
            }
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                CapabilitySwitch.setNeedWaitImsi("true");
                log("error: iccid not found, wait for next sub ready");
                return;
            }
            if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                insertedSimCount++;
                insertedStatus = insertedStatus | (1 << i);
            }
        }
        // check pin lock
        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("DataSubSelector for OP02: do not switch because of sim locking");
            CapabilitySwitch.setNeedWaitUnlock("true");
            return;
        } else {
            log("DataSubSelector for OP02: no pin lock");
            CapabilitySwitch.setNeedWaitUnlock("false");
        }

        log("Inserted SIM count: " + insertedSimCount + ", insertedStatus: " + insertedStatus);

        if (insertedSimCount == 0) {
            // No SIM inserted
            // 1. Default Data: Unset
            // 2. Data Enable: off
            // 3. 34G: Slot1
            log("C0: No SIM inserted: set default data unset");
            setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        } else if (insertedSimCount == 1) {
            for (int i = 0; i < mPhoneNum; i++) {
                if ((insertedStatus & (1 << i)) != 0) {
                    phoneId = i;
                    break;
                }
            }
            // Case1: Single SIM
            // 1. Default Data: This SIM
            // 2. Data Enable: No change
            // 3. 34G: This SIM
            log("C1: Single SIM inserted: set default data to phone: " + phoneId);
            mCapabilitySwitch.setCapability(phoneId);
            setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        } else if (insertedSimCount >= 2) {
            if (checkCapSwitch(DataSubConstants.HOME_POLICY) == false) {
                CapabilitySwitch.setNeedWaitImsi(Boolean.toString(true));
            }
        }
    }

    private void subSelectorWithoutCapabilityChange() {
        log("subSelectorWithoutCapabilityChange");
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int insertedSimCount = 0;
        int insertedStatus = 0;
        String[] currIccId = new String[mPhoneNum];

        for (int i = 0; i < mPhoneNum; i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                CapabilitySwitch.setNeedWaitImsi("true");
                log("error: iccid not found, wait for next sub ready");
                return;
            }
            if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                ++insertedSimCount;
                insertedStatus = insertedStatus | (1 << i);
            }
        }
        // check pin lock
        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("subSelectorWithoutCapabilityChange: do not switch because of sim locking");
            CapabilitySwitch.setNeedWaitUnlock("true");
            return;
        } else {
            log("subSelectorWithoutCapabilityChange: no pin lock");
            CapabilitySwitch.setNeedWaitUnlock("false");
        }

        log("Inserted SIM count: " + insertedSimCount + ", insertedStatus: " + insertedStatus);

        if (insertedSimCount == 0) {
            // OP02 Case 1: No SIM inserted
            // 1. Default Data: unset
            // 2. Data Enable: No Change
            // 3. 34G: Always SIM1
            log("subSelectorWithoutCapabilityChange C1: No SIM inserted, set data unset");
            mDataSubSelector.setDefaultData(SubscriptionManager.INVALID_PHONE_INDEX);
        } else if (insertedSimCount == 1) {
            for (int i = 0; i < mPhoneNum; i++) {
                if ((insertedStatus & (1 << i)) != 0) {
                    phoneId = i;
                    break;
                }
            }
            //OP02 Case 2: Single SIM
            // 1. Default Data: This sub
            // 2. Data Enable: No Change
            // 3. 34G: Always SIM1
            log("subSelectorWithoutCapabilityChange C2: Single SIM: Set to phone:" + phoneId);
            setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        } else if (insertedSimCount >= 2) {
            //OP02 Case 3: Multi SIM
            // 1. Default Data: Always SIM1
            // 2. Data Enable: No Change
            // 3. 34G: Always SIM1
            log("subSelectorWithoutCapabilityChange C3: Multi SIM: Set Default data to phone1");
            mDataSubSelector.setDefaultData(PhoneConstants.SIM_ID_1);
            mDataSubSelector.syncDefaultDataToMd(PhoneConstants.SIM_ID_1);
        }
    }

    private boolean checkCapSwitch(int policy) {
        int[] simOpInfo = new int[mPhoneNum];
        int[] simType = new int[mPhoneNum];
        int insertedStatus = 0;
        int insertedSimCount = 0;
        String currIccId[] = new String[mPhoneNum];
        ArrayList<Integer> usimIndexList = new ArrayList<Integer>();
        ArrayList<Integer> simIndexList = new ArrayList<Integer>();
        ArrayList<Integer> op02IndexList = new ArrayList<Integer>();
        ArrayList<Integer> otherIndexList = new ArrayList<Integer>();

        for (int i = 0; i < mPhoneNum; i++) {
            currIccId[i] = DataSubSelectorUtil.getIccidFromProp(i);
            if (currIccId[i] == null || "".equals(currIccId[i])) {
                log("error: iccid not found, wait for next sub ready");
                return false;
            }
            if (!DataSubConstants.NO_SIM_VALUE.equals(currIccId[i])) {
                ++insertedSimCount;
                insertedStatus = insertedStatus | (1 << i);
            }
        }
        log("checkOp02CapSwitch : Inserted SIM count: " + insertedSimCount
                + ", insertedStatus: " + insertedStatus);

        // check pin lock
        if (mCapabilitySwitch.isSimUnLocked() == false) {
            log("checkOp02CapSwitch: sim locked");
            if (DataSubConstants.HOME_POLICY == policy) {
                CapabilitySwitch.setNeedWaitUnlock("true");
            } else {
                CapabilitySwitch.setNeedWaitImsiRoaming("true");
            }
        } else {
            log("checkOp02CapSwitch: no sim locked");
            if (DataSubConstants.HOME_POLICY == policy) {
                CapabilitySwitch.setNeedWaitUnlock("false");
            } else {
                CapabilitySwitch.setNeedWaitImsiRoaming("false");
            }
        }
        if (RadioCapabilitySwitchUtil.getSimInfo(simOpInfo, simType, insertedStatus) == false) {
            return false;
        }

        for (int i = 0; i < mPhoneNum; i++) {
            if (RadioCapabilitySwitchUtil.SIM_OP_INFO_OP02 == simOpInfo[i]) {
                op02IndexList.add(i);
            } else {
                otherIndexList.add(i);
            }
            if (RadioCapabilitySwitchUtil.SIM_TYPE_USIM == simType[i]) {
                usimIndexList.add(i);
            } else {
                simIndexList.add(i);
            }
        }
        log("usimIndexList size = " + usimIndexList.size());
        log("op02IndexList size = " + op02IndexList.size());
        log("policy = " + policy);

        switch(policy) {
            case DataSubConstants.HOME_POLICY:
                executeHomePolicy(usimIndexList, op02IndexList, simIndexList);
                break;
            case DataSubConstants.ROAMING_POLICY:
                executeRoamingPolocy(usimIndexList, op02IndexList, otherIndexList);
                break;
            default:
                loge("Should NOT be here");
                break;
        }
        return true;
    }

    private void executeHomePolicy(ArrayList<Integer> usimIndexList,
        ArrayList<Integer> op02IndexList, ArrayList<Integer> simIndexList) {
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int op02CardCount = 0;

        log("Enter op02HomePolicy");
        // Home policy: OP02 USIM > other USIM > OP02 SIM > other SIM
        if (usimIndexList.size() >= 2) {
            // check OP02USIM count
            for (int i = 0; i < usimIndexList.size(); i++) {
                if (op02IndexList.contains(usimIndexList.get(i))) {
                    op02CardCount++;
                    phoneId = i;
                }
            }

            if (op02CardCount == 1) {
                // Case2: OP02 USIM + other USIMs/ OP02 USIM + other SIMs
                // 1. Default Data: OP02
                // 2. Data Enable: No change
                // 3. 34G: OP02
                log("C2: Only one OP02 USIM inserted, set default data to phone: " +
                        phoneId);
                mCapabilitySwitch.setCapability(phoneId);
                setDefaultData(phoneId);
                mDataSubSelector.syncDefaultDataToMd(phoneId);
            } else {
                // Case3: More than two OP02 cards or other operator cards
                // Display dialog
                log("C3: More than two OP02 cards or other operator cards inserted," +
                        "Display dialog");
            }
        } else if (usimIndexList.size() == 1) {
            // Case4: USIM + SIMs
            // 1. Default Data: USIM
            // 2. Data Enable: No change
            // 3. 34G: USIM
            phoneId = usimIndexList.get(0);
            log("C4: Only one USIM inserted, set default data to phone: " +
                    phoneId);
            mCapabilitySwitch.setCapability(phoneId);
            setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        } else {
            // usimCount = 0 (Case: all SIMs)
            // check OP02SIM count
            for (int i = 0; i < simIndexList.size(); i++) {
                if (op02IndexList.contains(simIndexList.get(i))) {
                    op02CardCount++;
                    phoneId = i;
                }
            }

            if (op02CardCount == 1) {
                // Case5: OP02 card + otehr op cards
                // 1. Default Data: OP02 card
                // 2. Data Enable: No change
                // 3. 34G: OP02 card
                log("C5: OP02 card + otehr op cards inserted, set default data to phone: " +
                        phoneId);
                mCapabilitySwitch.setCapability(phoneId);
                setDefaultData(phoneId);
                mDataSubSelector.syncDefaultDataToMd(phoneId);
            } else {
                // case6: More than two OP02 cards or other operator cards
                // Display dialog
                log("C6: More than two OP02 cards or other operator cards inserted," +
                        "display dialog");
            }
        }
    }

    private void executeRoamingPolocy(ArrayList<Integer> usimIndexList,
            ArrayList<Integer> op02IndexList, ArrayList<Integer> otherIndexList) {
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        int usimCount = 0;

        log("Enter op02RoamingPolocy");

        if (mContext == null) {
            loge("mContext is null, return");
        }

        // Roaming policy: OP02 USIM > OP02 SIM > other UISM > other SIM
        if (op02IndexList.size() >= 2) {
            // check OP02USIM count
            for (int i = 0; i < op02IndexList.size(); i++) {
                if (usimIndexList.contains(op02IndexList.get(i))) {
                    usimCount++;
                    phoneId = i;
                }
            }

            if (usimCount == 1) {
                // Case2: OP02 USIM + other USIMs / OP02 USIM + other SIMs
                // 1.Deafult Data: USIM
                // 2.Data Enable: No change
                // 3.34G: USIM
                log("C2: Only one OP02 USIM inserted, set default data to phone: "
                        + phoneId);
                mCapabilitySwitch.setCapability(phoneId);
                setDefaultData(phoneId);
                mDataSubSelector.syncDefaultDataToMd(phoneId);
            } else {
                // Case3: More than two USIM cards or other SIM cards
                // Display dialog
                log("C3: More than two USIM cards or other SIM cards inserted, show dialog");
            }
        } else if (op02IndexList.size() == 1) {
            // Case4: OP02 card + other cards
            // Default Data: OP02
            // Data Enable: No change
            // 34G: OP02
            phoneId = op02IndexList.get(0);
            log("C4: OP02 card + other cards inserted, set default data to phone: "
                    + phoneId);
            mCapabilitySwitch.setCapability(phoneId);
            setDefaultData(phoneId);
            mDataSubSelector.syncDefaultDataToMd(phoneId);
        } else {
            // op02CardCount = 0 (Case: other operator cards)
            // Check otherUSIM count
            for (int i = 0; i < otherIndexList.size(); i++) {
                if (usimIndexList.contains(otherIndexList.get(i))) {
                    usimCount++;
                    phoneId = i;
                }
            }

            if (usimCount == 1) {
                // Case5: other USIM + other SIM cards
                // Default Data: USIM
                // Data Enable: No change
                // 34G: USIM
                log("C5: Other USIM + other SIM cards inserted, set default data to phone: " +
                        phoneId);
                mCapabilitySwitch.setCapability(phoneId);
                setDefaultData(phoneId);
                mDataSubSelector.syncDefaultDataToMd(phoneId);
            } else {
                // Case6: More than two USIM cards or all SIM cards
                // Display dialog
                log("C6: More than two USIM cards or all SIM cards inserted, diaplay dialog");
            }
        }

        SharedPreferences preferenceRoaming = mContext.getSharedPreferences(
                DataSubConstants.FIRST_TIME_ROAMING, Context.MODE_PRIVATE);
        SharedPreferences.Editor editorRoaming = preferenceRoaming.edit();
        editorRoaming.putBoolean(DataSubConstants.NEED_TO_EXECUTE_ROAMING, false);
        if (!editorRoaming.commit()) {
            loge("write sharedPreference ERROR");
        }
    }

    private void setDefaultData(int phoneId) {
        mDataSubSelector.setDefaultData(phoneId);
    }

    @Override
    public void handleAirPlaneModeOff(Intent intent) {}

    @Override
    public void handleDefaultDataChanged(Intent intent) {}

    public void handleSimMeLock(Intent intent) {}

    private void log(String txt) {
        if (DBG) {
            Rlog.d(LOG_TAG, txt);
        }
    }

    private void loge(String txt) {
        if (DBG) {
            Rlog.e(LOG_TAG, txt);
        }
    }
}
