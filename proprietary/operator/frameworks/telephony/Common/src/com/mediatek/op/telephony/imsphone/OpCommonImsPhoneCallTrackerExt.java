/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.telephony.imsphone;

import android.app.AlarmManager;
import android.app.PendingIntent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.telephony.ServiceState;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsStreamMediaProfile;

import android.os.Bundle;
import android.os.SystemClock;
import android.os.SystemProperties;

import android.util.Log;

import com.android.ims.ImsCall;
import com.android.internal.telephony.Call;
import com.android.internal.telephony.imsphone.ImsPhone;
import com.android.internal.telephony.imsphone.ImsPhoneCall;
import com.android.internal.telephony.imsphone.ImsPhoneConnection;

import com.mediatek.ims.MtkImsCall;
import com.mediatek.internal.telephony.imsphone.op.OpCommonImsPhoneCallTrackerBase;
import com.mediatek.internal.telephony.imsphone.MtkImsPhoneConnection;

import mediatek.telecom.MtkConnection;
import mediatek.telecom.MtkTelecomManager;
import com.mediatek.ims.internal.MtkImsManager;

import java.util.ArrayList;
import java.util.List;

public class OpCommonImsPhoneCallTrackerExt extends OpCommonImsPhoneCallTrackerBase {
    private static final String TAG = "OpImsPhoneCallTrackerExt";

    /// M: RTT  @{
    private  boolean mImsRttRegisteredStatus = false;
    private static final int IMS_RTT_CALL_TYPE_CS = 0;
    private static final int IMS_RTT_CALL_TYPE_RTT = 1;
    private static final int IMS_RTT_CALL_TYPE_PS = 2;
    private static final int IMS_RTT_CALL_TYPE_CS_NO_TTY = 3;
    private int mImsRttCallType = IMS_RTT_CALL_TYPE_PS;
    private static final String INTENT_RTT_EMC_GUARD_TIMER_180 =
          "com.mediatek.internal.telephony.imsphone.rtt_emc_guard_timer_180";
    private AlarmManager mAlarmManager;
    private PendingIntent mRttEmcIntent = null;
    private boolean mDuringRttGuardDuration = false;

    private static final boolean RTT_SUPPORT =
            SystemProperties.get("persist.vendor.mtk_rtt_support").equals("1");

    /// @}

    @Override
    public void onTextCapabilityChanged(ImsPhoneConnection conn,
            int localCapability, int remoteCapability,
            int localTextStatus, int realRemoteCapability) {

        boolean isTextCall = (remoteCapability == 1 && localTextStatus == 1)? true : false;
        boolean local = (localCapability == 1)? true : false;
        boolean remote = (realRemoteCapability == 1)? true : false;
        boolean remoteStatus = (remoteCapability == 1)? true : false;


        Log.d(TAG, "onTextCapabilityChanged op localCapability: " + localCapability +
            " remote status: " + remoteCapability + " localTextStatus"
            + localTextStatus + " RemoteCapability: " + realRemoteCapability);

        //VzW GTT text capability change
        Bundle bundle = new Bundle();
        bundle.putBoolean(MtkTelecomManager.EXTRA_GTT_MODE_LOCAL, local);
        bundle.putBoolean(MtkTelecomManager.EXTRA_GTT_MODE_REMOTE, remoteStatus);
        //TMO RTT text capability change
        bundle.putBoolean(MtkTelecomManager.EXTRA_RTT_SUPPORT_LOCAL, local);
        bundle.putBoolean(MtkTelecomManager.EXTRA_RTT_SUPPORT_REMOTE, remote);

        if (conn != null) {
            if (!RTT_SUPPORT) { // It means VzW GTT enabled only.
                //VzW GTT  updateTextCapability
                conn.onConnectionEvent(MtkTelecomManager.EVENT_GTT_MODE_CHANGED, bundle);
            }
            //TMO RTT
            conn.onConnectionEvent(MtkTelecomManager.EVENT_RTT_SUPPORT_CHANGED, bundle);
            Log.d(TAG, "onTextCapabilityChanged update to conn");
        }
    }


    @Override
    public void onRttEventReceived(ImsPhoneConnection conn, int event) {
        if (conn != null) {
            conn.onConnectionEvent(
                mediatek.telecom.MtkConnection.EVENT_RTT_EMERGENCY_REDIAL, null);
        }
    }

    @Override
    public void initRtt(ImsPhone imsPhone) {

        Log.d(TAG, "initRtt op ");
        if (imsPhone == null) {
            Log.d(TAG, "initRtt no imsPhone");
            return;
        }
        mAlarmManager = (AlarmManager) imsPhone.getContext().
            getSystemService(Context.ALARM_SERVICE);
        registerRttReceiver(imsPhone);
    }

    private BroadcastReceiver mRttReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(INTENT_RTT_EMC_GUARD_TIMER_180)) {

                Log.d(TAG, "onReceive : mRttReceiver rtt guard timer 180");
                stopRttEmcGuardTimer();
                mRttEmcIntent = null;
                mDuringRttGuardDuration = false;

            }
        }
    };

    @Override
    public void disposeRtt(ImsPhone imsPhone, ImsPhoneCall foregroundCall,
        Call.SrvccState srvccState) {

        Log.d(TAG, "disposeRtt op ");
        checkRttCallType(imsPhone, foregroundCall, srvccState);
        unregisterRttReceiver(imsPhone);
    }


    private void registerRttReceiver(ImsPhone imsPhone) {

        Log.d(TAG, "registerRttReceiver op");

        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction(INTENT_RTT_EMC_GUARD_TIMER_180);
        imsPhone.getContext().registerReceiver(mRttReceiver, intentfilter);

    }


    private void unregisterRttReceiver(ImsPhone imsPhone) {
        Log.d(TAG,"unregisterRttReceiver");
        imsPhone.getContext().unregisterReceiver(mRttReceiver);
    }

    @Override
    public void stopRttEmcGuardTimer() {

        Log.d(TAG, "stopRttEmcGuardTimer op ");

        if (mRttEmcIntent != null) {
            Log.d(TAG, "stopRttEmcGuardTimer");

            mAlarmManager.cancel(mRttEmcIntent);
            mRttEmcIntent = null;
            mDuringRttGuardDuration = false;
        }
    }

    @Override
    public void checkRttCallType(ImsPhone imsPhone, ImsPhoneCall foregroundCall,
        Call.SrvccState srvccState) {


        Log.d(TAG, "checkRttCallType op: " + imsPhone + "srvccState " + srvccState);

        if (imsPhone == null || foregroundCall == null) {
            return;
        }

        boolean imsRegistered = (imsPhone.getServiceState().getState() ==
            ServiceState.STATE_IN_SERVICE);

        ImsCall imscall = foregroundCall.getImsCall();
        boolean isRttCall = false;
        boolean isSrvcc = (srvccState == Call.SrvccState.STARTED ||
            srvccState == Call.SrvccState.COMPLETED)? true : false;
        if (imscall != null) {
            isRttCall = imscall.getCallProfile().mMediaProfile.isRttCall();
        }
        int preImsRttCallType = mImsRttCallType;
        if (imsRegistered == false) {
            mImsRttCallType = IMS_RTT_CALL_TYPE_CS;
        } else if (isRttCall== true && imsRegistered == true && isSrvcc == false) {
            mImsRttCallType = IMS_RTT_CALL_TYPE_RTT;
        } else if (preImsRttCallType == IMS_RTT_CALL_TYPE_RTT && isSrvcc == true) {
            mImsRttCallType = IMS_RTT_CALL_TYPE_CS_NO_TTY;
        } else {
            mImsRttCallType = IMS_RTT_CALL_TYPE_PS;

        }
        Log.d(TAG, "checkRttCallType : old" + preImsRttCallType + " new: " + mImsRttCallType);
        if (preImsRttCallType != mImsRttCallType) {
            Log.d(TAG, "set to audioManager " + mImsRttCallType);
            //set change to Andio manager
            AudioManager audioManager =
            (AudioManager) imsPhone.getContext().getSystemService(Context.AUDIO_SERVICE);
            audioManager.setParameters("rtt_call_type=" + mImsRttCallType);
        }
    }

    @Override
    public void setRttMode(Bundle intentExtras, ImsCallProfile profile) {

        Log.d(TAG, "setRttMode op");
        if (intentExtras == null) {
            Log.d(TAG, "setRttMode no extra");
            return;
        }
        if (intentExtras.containsKey(
            android.telecom.TelecomManager.EXTRA_START_CALL_WITH_RTT)) {
            profile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_FULL);
        }

    }

    @Override
    public void sendRttSrvccOrCsfbEvent(ImsPhoneCall call) {

        Log.d(TAG, "sendRttSrvccOrCsfbEvent op: " + call);
        if (call == null) {
            Log.d(TAG, "sendRttSrvccOrCsfbEvent no call");
            return;
        }
        if (call.hasConnections()) {
            ImsCall activeCall = call.getFirstConnection().getImsCall();
            ImsPhoneConnection conn = call.getFirstConnection();
            notifyRttStatusAfterSrvccOrCsfb(activeCall, conn);
            if (activeCall != null && conn != null) {
                if (call.getState() ==ImsPhoneCall.State.DIALING) {
                    //CSFB
                    conn.onConnectionEvent(
                        mediatek.telecom.MtkConnection.EVENT_CSFB, null);
                } else {
                    //Srvcc
                    conn.onConnectionEvent(
                        mediatek.telecom.MtkConnection.EVENT_SRVCC, null);
                }
            }
        }
    }

    private void notifyRttStatusAfterSrvccOrCsfb(ImsCall imsCall, ImsPhoneConnection conn) {
        Log.d(TAG, "notifyRttStatusAfterSrvccOrCsfb");
        // for UI to change status from RTT call to normal call.
        int failStatus = android.telecom.Connection.RttModifyStatus.SESSION_MODIFY_REQUEST_FAIL;
        processRttModifyFailCase(imsCall, failStatus, conn);
    }

    @Override
    public void checkIncomingRtt(Intent intent, ImsCall imsCall,
        ImsPhoneConnection conn) {

        Log.d(TAG, "checkIncomingRtt op: " + intent);

        if (intent == null || imsCall==null || conn == null) {
            Log.d(TAG, "checkIncomingRtt return");
            return;
        }

        int isIncomingCallRtt =
            intent.getIntExtra(MtkImsManager.EXTRA_RTT_INCOMING_CALL, 0);
        Log.d(TAG, "checkIncomingRtt RTT = " + isIncomingCallRtt);

        if (isIncomingCallRtt == 1) {

            ((MtkImsCall)imsCall).setRttMode(ImsStreamMediaProfile.RTT_MODE_FULL);
            ((MtkImsPhoneConnection) conn).setRttIncomingCall(true);
        }
        if (mDuringRttGuardDuration == true) {
            ((MtkImsPhoneConnection) conn).setIncomingRttDuringEmcGuard(true);
        } else {
            ((MtkImsPhoneConnection) conn).setIncomingRttDuringEmcGuard(false);
        }
    }

    @Override
    public void processRttModifyFailCase(ImsCall imsCall, int status,
        ImsPhoneConnection conn){

        Log.d(TAG, "processRttModifyFailCase op: " + status);

        if (status ==
            android.telecom.Connection.RttModifyStatus.SESSION_MODIFY_REQUEST_FAIL) {
            //0. if expect non RTT call then, ignore
            if (conn.mRttTextStream == null) {
                Log.d(TAG, "processRttModifyFailCase op: didn't expect rtt. Ignore");
                return;
            }
            //1. update status to upper layer
            conn.onRttModifyResponseReceived(status);
            //2. stop text stream handling
            if (conn != null && conn instanceof MtkImsPhoneConnection) {
                ((MtkImsPhoneConnection) conn).stopRttTextProcessing();
            }
            //3. clear mCallProfile.rtt status
            imsCall.getCallProfile().mMediaProfile.setRttMode(
                ImsStreamMediaProfile.RTT_MODE_DISABLED);
        } else if (status ==
            android.telecom.Connection.RttModifyStatus.SESSION_MODIFY_REQUEST_INVALID) {
            conn.onConnectionEvent(
                mediatek.telecom.MtkConnection.EVENT_RTT_UPDOWN_FAIL, null);
        }
    }

    @Override
    public void processRttModifySuccessCase(ImsCall imsCall, int status,
        ImsPhoneConnection conn){

        Log.d(TAG, "processRttModifySuccessCase op: " + status);
        Log.d(TAG, "processRttModifySuccessCase setCallProfile rtt true");
        imsCall.getCallProfile().mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_FULL);
   }

    @Override
    public void startRttEmcGuardTimer(ImsPhone imsPhone) {

        Log.d(TAG, "startRttEmcGuardTimer op: " + imsPhone);

        String optr = SystemProperties.get("persist.vendor.operator.optr");
        if (optr != null && !optr.equalsIgnoreCase("op08")) {
            Log.d(TAG, "startRttEmcGuardTimer: If not TMO, then return");
            return;
        }

        if (imsPhone == null) {
            Log.d(TAG, "startRttEmcGuardTimer return");
            return;
        }

        stopRttEmcGuardTimer();

        Intent intent = new Intent(INTENT_RTT_EMC_GUARD_TIMER_180);
        mRttEmcIntent = PendingIntent.getBroadcast(imsPhone.getContext(), 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        int delay = 180 * 1000; // 180s

        Log.d(TAG,"startRttEmcGuardTimer: delay=" + delay);

        mDuringRttGuardDuration = true;
        mAlarmManager.setExact(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + delay, mRttEmcIntent);

    }

    @Override
    public boolean isRttCallInvolved(ImsCall fgImsCall, ImsCall bgImsCall) {
        boolean ret = false;
        if (isRttCall(fgImsCall) || isRttCall(bgImsCall)) {
            ret = true;
        }
        Log.d(TAG, "isRttCallInvolved op: " + ret);
        return ret;
    }

    private boolean isRttCall(ImsCall call) {
        if (call != null) {
            return call.getCallProfile().mMediaProfile.isRttCall();
        }
        return false;
    }

    private static final List<String> RTT_NOT_ALLOW_MERGE_OPERATOR_LIST =
        new ArrayList<String>() {{
            add("OP08"); // TMO US
            add("OP12"); // VzW
        }};

    @Override
    public boolean isAllowMergeRttCallToVoiceOnly() {
        // Check current operator is TMO, then not allow.
        boolean isAllow = true;
        String optr = SystemProperties.get("persist.vendor.operator.optr");
        if (RTT_NOT_ALLOW_MERGE_OPERATOR_LIST.contains(optr)) {
            isAllow = false;
        }
        Log.d(TAG, "isAllowMergeRttCallToVoiceOnly optr: " + optr
                + ", isAllow: " + isAllow);
        return isAllow;
    }
}
