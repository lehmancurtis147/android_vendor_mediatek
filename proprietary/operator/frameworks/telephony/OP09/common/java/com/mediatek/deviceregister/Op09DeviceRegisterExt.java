/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.deviceregister;

import android.app.Activity;
import android.app.AlarmManager.OnAlarmListener;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.util.Log;

import com.mediatek.deviceregister.utils.AgentProxy;
import com.mediatek.deviceregister.utils.EsnWrapper;
import com.mediatek.deviceregister.utils.PlatformManager;
import com.mediatek.deviceregister.utils.Utils;
import com.mediatek.internal.telephony.devreg.DefaultDeviceRegisterExt;
import com.mediatek.internal.telephony.devreg.DeviceRegisterController;
import com.mediatek.selfregister.Op09SelfRegister;

public class Op09DeviceRegisterExt extends DefaultDeviceRegisterExt {

    private static final String TAG = Const.TAG_PREFIX + "RegisterService";

    private static final int MSG_PRE_BOOT_COMPLETED = 1;
    private static final int MSG_SIM_STATE_CHANGED = 2;
    private static final int MSG_SIM_INSERVICE = 3;
    private static final int MSG_MESSAGE_SEND = 4;

    private static final int TIMES_MAX_WAIT = 3;
    private static final int TIMES_MAX_RETRY = 3;

    private AgentProxy mAgentProxy;
    private PlatformManager mPlatformManager;
    private SmsSendReceiver mSmsSendReceiver;
    private StartupReceiver mStartupReceiver;

    private String mMeid = null;
    private String mPreviousEsnOrMeid = null;

    private boolean mHasInService = false;
    private boolean mHasSimLoadedIntent = false;

    private int mUimSlot = -1;
    private int mWaitTimes = 0;
    private int mRetryTimes = 0;

    private int[] mSlotList;
    private String[] mImsiOnSim;
    private PhoneStateListener[] mPhoneStateListener;

    private Handler mHandler;
    private HandlerThread mHandlerThread;

    public Op09DeviceRegisterExt(Context context, DeviceRegisterController constroller) {
        super(context, constroller);
        if (PlatformManager.isFeatureSupported()) {
            init();
        }
        Op09SelfRegister.newSelfRegister(context);
    }

    private void init() {
        Log.i(TAG, "init");
        mPlatformManager = new PlatformManager(mContext);
        mAgentProxy = new AgentProxy(mContext);

        if (mPlatformManager.isSingleLoad()) {
            mSlotList = Const.SINGLE_SIM_SLOT;
        } else {
            mSlotList = Const.DUAL_SIM_SLOTS;
        }

        mPhoneStateListener = new PhoneStateListener[mSlotList.length];
        for (int i = 0; i < mSlotList.length; ++i) {
            mPhoneStateListener[i] = null;
        }
        registerStartupReceiver();
        initHandler();
    }

    public void deinit() {
        Log.i(TAG, "deinit");

        unRegisterStartupReceiver();
        unRegisterSendReceiver();
        unRegisterPhoneListeners();
    }

    /*
     * rawValue: "oldEsn,newEsn"
     */
    @Override
    public void setCdmaCardEsnOrMeid(String rawValue){
        Log.i(TAG, "setCdmaCardEsnOrMeid " + PlatformManager.encryptMessage(rawValue));
        if (mPreviousEsnOrMeid == null) {
            String temp[] = rawValue.split(",");
            if (temp.length >= 1) {
                 mPreviousEsnOrMeid = temp[0].trim();

                 if (mHasSimLoadedIntent) {
                     mHandler.sendEmptyMessage(MSG_SIM_STATE_CHANGED);
                 }
            }
        }
    }

    @Override
    public void handleAutoRegMessage(byte[] pdu){
        if (mSmsSendReceiver != null) {
            boolean result = PlatformManager.SmsWrapper.checkRegisterResult(pdu);
            mAgentProxy.setRegisterFlag(result);
            if (result) {
                Log.i(TAG, "Register success!");
            } else {
                Log.i(TAG, "Register failed!");
            }
            deinit();
        }
    }

    private void initHandler() {
        mHandlerThread = new HandlerThread("DeviceRegister");
        mHandlerThread.start();
        mHandler = new Handler(mHandlerThread.getLooper()) {

            @Override
            public void handleMessage(Message msg) {

                switch (msg.what) {
                case MSG_PRE_BOOT_COMPLETED:
                    // system update or factory reset, reset register flag
                    mAgentProxy.resetRegisterFlag();
                    break;

                case MSG_SIM_STATE_CHANGED:
                    checkFingerPrint();
                    registerPhoneListeners();
                    break;

                case MSG_SIM_INSERVICE:
                    doAfterSimInService();
                    break;

                case MSG_MESSAGE_SEND:
                    doAfterMessageSend();
                    break;

                default:
                    break;
                }
            }
        };
    }

    /*
     * As DeviceRegister only trigger once after boot, so check fingerprint when both ESN/SIM ready
     * (could also check after boot completed like selfregister)
     */
    private void checkFingerPrint() {
        String savedFingerPrint = mAgentProxy.getSavedFingerPrint();
        String currentFingerPrint = mAgentProxy.getCurrentFingerPrint();
        Log.i(TAG, "savedFingerPrint " + savedFingerPrint);
        Log.i(TAG, "currentFingerPrint " + currentFingerPrint);

        // If finger print not the same (first time boot, factory reset or system update), reset
        // register flag
        if (!savedFingerPrint.equals(currentFingerPrint)) {
            Log.i(TAG, "System fingerprint changes, reset register flag");
            mAgentProxy.setSavedFingerPrint(currentFingerPrint);
            mAgentProxy.resetRegisterFlag();
        } else {
            Log.i(TAG, "Same fingerprint, do nothing");
        }
    }

    private void doAfterSimInService() {
        Log.i(TAG, "doAfterSimInService, wait times " + mWaitTimes);
        if (!mPlatformManager.areSlotsInfoReady(mSlotList) && mWaitTimes < TIMES_MAX_WAIT) {
            setInServiceDelayAlarm(30 * Const.ONE_SECOND);
            mWaitTimes++;
            return;
        }

        initIdentityInfo();

        // Scenario need to handle
        // 1. CT SIM's network change from CDMA to LTE
        // 2. CT SIM removed after IN_SERVICE & register CDMA network
        if (!isUimSlotValid()) {
            Log.i(TAG, "Can't find a valid UIM, retry");
            doAfterSendFailed();
            return;
        }

        if (needRegister()) {
            registerSendReceiver();
            sendRegisterMessage();
        } else {
            Log.d(TAG, "Phone no need to register.");
            deinit();
        }
    }

    private void doAfterMessageSend() {
        Log.i(TAG, "doAfterMessageSend");
        mAgentProxy.resetRegisterFlag();
        mAgentProxy.setSavedImsi(mImsiOnSim);
    }

    private void doAfterSendFailed() {
        Log.i(TAG, "doAfterSendFailed");
        if (mRetryTimes < TIMES_MAX_RETRY) {
            mRetryTimes++;
            // 2018 V1 requirement
            long second = 10 * Const.ONE_MINUTE;
            setInServiceDelayAlarm(second);
        } else {
            Log.i(TAG, "Send failed, reach limit " + mRetryTimes);
            deinit();
        }
    }

    private void setInServiceDelayAlarm() {
        setInServiceDelayAlarm(Const.ONE_MINUTE);
    }

    private void setInServiceDelayAlarm(long delay) {
        Log.i(TAG, "setInServiceDelayAlarm " + (delay / Const.ONE_SECOND) + "s");
        PlatformManager.setElapsedAlarm(mContext, mInServiceAlarmListener, delay);
        mHasInService = true;
    }

    private OnAlarmListener mInServiceAlarmListener = new OnAlarmListener() {

        @Override
        public void onAlarm() {
            mHandler.sendEmptyMessage(MSG_SIM_INSERVICE);
        }
    };

    private boolean needRegister() {

        if (mUimSlot == Const.SLOT_ID_INVALID) {
            Log.w(TAG, "[needRegister] no valid UIM");
            return false;
        }

        if (mPlatformManager.isNetworkRoaming(mUimSlot)) {
            Log.w(TAG, "[needRegister] uim " + mUimSlot + " is roaming");
            return false;
        }

        if (mAgentProxy.isRegistered()) {
            Log.d(TAG, "[needRegister] registered before, check imsi and meid.");

            if (isImsiSame() && isEsnOrMeidSame()) {
                Log.d(TAG, "[needRegister] same imsi and meid");
                return false;
            }
        }

        return true;
    }

    private void initIdentityInfo() {
        initUimSlot();

        mImsiOnSim = new String[mSlotList.length];
        for (int i = 0; i < mSlotList.length; ++i) {
            mImsiOnSim[i] = mPlatformManager.getImsiInfo(mSlotList[i]);
        }

        mMeid = mPlatformManager.getDeviceMeid(mSlotList);
    }

    private void initUimSlot() {
        mUimSlot = Const.SLOT_ID_INVALID;

        // 1. try mobile data slot
        int masterSlot = mPlatformManager.getDefaultSim();

        // 2. If master valid UIM, use this; else, first valid UIM
        if (masterSlot != Const.SLOT_ID_INVALID
                && mPlatformManager.isValidUim(masterSlot)
                && mPlatformManager.isValidCdmaNetwork(masterSlot)) {
            mUimSlot = masterSlot;

        } else {
            for (int slotId : mSlotList) {
                if (slotId != masterSlot && mPlatformManager.isValidUim(slotId)
                        && mPlatformManager.isValidCdmaNetwork(slotId)) {
                    mUimSlot = slotId;
                }
            }
        }

        Log.i(TAG, "[initUimSlot] Uim slot is " + mUimSlot);
    }

    private boolean isUimSlotValid() {
        return mUimSlot != Const.SLOT_ID_INVALID;
    }

    private boolean isImsiSame() {
        String[] imsiSaved = mAgentProxy.getSavedImsi();

        for (int i = 0; i < imsiSaved.length; ++i) {
            imsiSaved[i] = mPlatformManager.getProcessedImsi(imsiSaved[i]);
        }

        return Utils.compareUnsortArray(mImsiOnSim, imsiSaved);
    }

    private boolean isEsnOrMeidSame() {
        String rawEsnOrMeid = mPreviousEsnOrMeid;
        String esnOrMeid = EsnWrapper.processResponseFromFW(rawEsnOrMeid);

        Log.i(TAG, "device meid is " + PlatformManager.encryptMessage(mMeid));

        if (mMeid.length() == esnOrMeid.length()) {
            return mMeid.equals(esnOrMeid);
        }

        String deviceEsn = EsnWrapper.convertToEsn(mMeid);
        String savedEsn = EsnWrapper.convertToEsn(esnOrMeid);

        Log.i(TAG, "ESN device/saved are " + PlatformManager.encryptMessage(deviceEsn)
                + "/" + PlatformManager.encryptMessage(savedEsn));

        return deviceEsn.equals(savedEsn);
    }

    private void sendRegisterMessage() {
        byte[] registerMessage = new RegisterMessage(this).getRegisterMessage();

        Intent intent = new Intent(Const.ACTION_MESSAGE_SEND);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        try {
            PlatformManager.SmsWrapper
                    .sendRegisterMessage(
                            mDeviceRegisterController,
                            registerMessage,
                            pendingIntent,
                            mUimSlot);
        } catch (UnsupportedOperationException | SecurityException  e) {
            Log.i(TAG, "Exception " + e);
            e.printStackTrace();
            doAfterSendFailed();
        }
    }

    public String getImei(int slotId) {
        return mPlatformManager.getImei(slotId);
    }

    public String getDeviceMeid() {
        return mMeid;
    }

    public String getCurrentCDMAImsi() {
        return mPlatformManager.getCdmaImsiForCT(mUimSlot);
    }

    private void registerPhoneListeners() {
        for (int i = 0; i < mSlotList.length; ++i) {

            if (mPhoneStateListener[i] != null) {
                Log.i(TAG, "[registerPhoneListeners] slot " + i + " already registered.");
                continue;
            }

            int[] subId = PlatformManager.getSubId(mSlotList[i]);

            if (PlatformManager.isSubIdsValid(subId)) {
                Log.i(TAG, "[registerPhoneListeners] slot " + mSlotList[i]);

                mPhoneStateListener[i] = new CustomizedPhoneStateListener(mSlotList[i], subId[0]);
                mPlatformManager.registerPhoneListener(mPhoneStateListener[i]);
            } else {
                Log.i(TAG, "[registerPhoneListeners] no need for slot " + mSlotList[i]);
            }
        }
    }

    private void unRegisterPhoneListeners() {
        Log.i(TAG, "unRegisterPhoneListeners");

        for (int i = 0; i < mPhoneStateListener.length; ++i) {
            if (mPhoneStateListener[i] != null) {
                mPlatformManager.unRegisterPhoneListener(mPhoneStateListener[i]);
                mPhoneStateListener[i] = null;
            }
        }
    }

    private void registerStartupReceiver() {
        Log.i(TAG, "registerStartupReceiver");
        if (mStartupReceiver == null) {
            mStartupReceiver = new StartupReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Const.ACTION_PRE_BOOT_COMPLETED);
            filter.addAction(Const.ACTION_SIM_STATE_CHANGED);
            mContext.registerReceiver(mStartupReceiver, filter);
        }
    }

    private void unRegisterStartupReceiver() {
        Log.i(TAG, "unRegisterStartupReceiver");
        if (mStartupReceiver != null) {
            mContext.unregisterReceiver(mStartupReceiver);
            mStartupReceiver = null;
        }
    }

    private void registerSendReceiver() {
        if (mSmsSendReceiver == null) {
            mSmsSendReceiver = new SmsSendReceiver();
            IntentFilter filter = new IntentFilter();
            filter.addAction(Const.ACTION_MESSAGE_SEND);
            mContext.registerReceiver(mSmsSendReceiver, filter);
        }
    }

    private void unRegisterSendReceiver() {
        if (mSmsSendReceiver != null) {
            mContext.unregisterReceiver(mSmsSendReceiver);
            mSmsSendReceiver = null;
        }
    }

    private class CustomizedPhoneStateListener extends PhoneStateListener {

        private int mSlotId;

        public CustomizedPhoneStateListener(int slotId, int subId) {
            super(subId);
            mSlotId = slotId;
        }

        @Override
        public void onServiceStateChanged(ServiceState serviceState) {
            super.onServiceStateChanged(serviceState);

            if (mPlatformManager.isValidCdmaNetwork(serviceState)) {
                Log.i(TAG, "[onService " + mSlotId + "] voice/data in service");

                // As SIM info may not ready, can't check whether it's UIM here
                if (!mHasInService) {
                    setInServiceDelayAlarm();
                }

            } else {
                Log.i(TAG, "[onService " + mSlotId + "] not in service");
            }
        }
    }

    private class SmsSendReceiver extends BroadcastReceiver {
        private static final String TAG = Const.TAG_PREFIX + "SmsSendReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i(TAG, "onReceive " + intent);

            String action = intent.getAction();
            if (action.equalsIgnoreCase(Const.ACTION_MESSAGE_SEND)) {
                int resultCode = getResultCode();

                if (resultCode == Activity.RESULT_OK) {
                    mHandler.sendEmptyMessage(MSG_MESSAGE_SEND);

                } else {
                    Log.i(TAG, "ResultCode: " + resultCode + ", failed.");
                    doAfterSendFailed();
                }

            } else {
                Log.i(TAG, "action is not valid." + action);
            }
        }
    }

    private class StartupReceiver extends BroadcastReceiver {

        private static final String TAG = Const.TAG_PREFIX + "StartupReceiver";

        @Override
        public void onReceive(Context context, Intent intent) {

            boolean isSystem = android.os.Process.myUserHandle().isSystem();
            if (!isSystem) {
                Log.d(TAG, "This is not system user, return.");
                return;
            }

            if (!isSwitchOpen()) {
                Log.i(TAG, "Feature is not enabled, do nothing");
                return;
            }

            String action = intent.getAction();
            if (action.equalsIgnoreCase(Const.ACTION_PRE_BOOT_COMPLETED)) {
                Log.d(TAG, "onReceive " + intent);
                mHandler.sendEmptyMessage(MSG_PRE_BOOT_COMPLETED);

            } else if (action.equalsIgnoreCase(Const.ACTION_SIM_STATE_CHANGED)) {
                String state = intent.getStringExtra(Const.EXTRA_ICC_STATE);
                int slot = intent.getIntExtra(Const.EXTRA_ICC_SLOT, Const.SLOT_ID_INVALID);

                Log.i(TAG, "SIM_STATE_CHANGED slot " + slot + ", state is " + state);
                if (state.equals(Const.VALUE_ICC_LOADED)) {
                    simReady(context, intent);
                }
            } else {
                Log.i(TAG, "Invalid action " + action);
            }
        }

        private void simReady(Context context, Intent intent) {
            mHasSimLoadedIntent = true;

            if (mHasInService) {
                Log.i(TAG, "Already find a sim IN_SERVICE, do nothing");
            } else {

                // start service if MEID/ESN info also ready
                if (mPreviousEsnOrMeid != null) {
                    mHandler.sendEmptyMessage(MSG_SIM_STATE_CHANGED);
                }
            }
        }

        private boolean isSwitchOpen() {
            return PlatformManager.isFeatureEnabled();
        }
    }

}
