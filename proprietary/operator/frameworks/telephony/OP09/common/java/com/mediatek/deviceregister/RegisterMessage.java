/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.deviceregister;

import android.os.Build;
import android.util.Log;

import com.mediatek.deviceregister.utils.PlatformManager;

import java.util.zip.CRC32;

public class RegisterMessage {
    private static final String TAG = Const.TAG_PREFIX + "RegisterMessage";

    // Value specified by CT
    private static final byte PROTOCOL_VERSION_ESN = 0x01;
    private static final byte PROTOCOL_VERSION_MEID = 0x02;

    private static final byte COMMAND_TYPE_SEND = 0x03;
    public static final byte COMMAND_TYPE_RECEIVED = 0x04;

    private static final int LENGTH_MAX_MESSAGE = 127;
    private static final int LENGTH_MAX_MANUFACTURE = 3;
    private static final int LENGTH_MAX_MODEL = 20;
    private static final int LENGTH_MAX_VERSION = 60;
    private static final int LENGTH_CHECKSUM = 8;

    private byte mProtocolVersion = PROTOCOL_VERSION_MEID;
    private byte mCommandType = COMMAND_TYPE_SEND;
    private byte mDataLength = 0;
    private byte mFillByte = 0x0;

    private String mData = "";
    private String mChecksum = "";
    private Op09DeviceRegisterExt mService;

    public RegisterMessage(Op09DeviceRegisterExt service) {
        mService = service;
    }

    public byte[] getRegisterMessage() {
        mData = generateMessageData();
        mDataLength = (byte) mData.length();

        int byteArrayLenth = 4 + mDataLength;
        byte[] message = new byte[byteArrayLenth];
        message[0] = mProtocolVersion;
        message[1] = mCommandType;
        message[2] = mDataLength;
        message[3] = mFillByte;
        byte[] dataByte = mData.getBytes();
        int i = 4;
        for (int j = 0; j < dataByte.length; j++) {
            message[i] = dataByte[j];
            i++;
        }

        mChecksum = generateChecksum(message);
        Log.d(TAG, "checksum: " + mChecksum);
        byte[] crcByte = mChecksum.getBytes();

        byte[] messageFinal = new byte[message.length + LENGTH_CHECKSUM];

        int k = 0;
        for (int j = 0; j < message.length; j++) {
            messageFinal[k] = message[j];
            k++;
        }

        for (int j = 0; j < crcByte.length; j++) {
            messageFinal[k] = crcByte[j];
            k++;
        }
        return messageFinal;
    }

    private String generateMessageData() {
        String beginTag = "<a1>";
        String endTag = "</a1>";
        String modelBeginTag = "<b1>";
        String modelEndTag = "</b1>";
        String meidBeginTag = "<b2>";
        String meidEndTag = "</b2>";
        String imsiBeginTag = "<b3>";
        String imsiEndTag = "</b3>";
        String versionBeginTag = "<b4>";
        String versionEndTag = "</b4>";
        StringBuffer data = new StringBuffer();
        data.append(beginTag);
        data.append(modelBeginTag).append(getModel()).append(modelEndTag);
        data.append(meidBeginTag).append(getMeid()).append(meidEndTag);
        data.append(imsiBeginTag).append(getCMDAIMSI()).append(imsiEndTag);
        data.append(versionBeginTag).append(getSoftwareVersion()).append(versionEndTag);
        data.append(endTag);

        if (data.length() > LENGTH_MAX_MESSAGE) {
            Log.w(TAG, "Message length > " + LENGTH_MAX_MESSAGE + ", cut it!");
            int exceedLength = data.length() - LENGTH_MAX_MESSAGE;
            data = data.delete(data.length() - 10 - exceedLength, data.length() - 10);
        }

        Log.d(TAG, "message: " + PlatformManager.encryptMessage(data.toString()));
        return data.toString();
    }

    private String getModel() {
        String manufacturer = PlatformManager.getManufacturer();
        if (manufacturer.length() > LENGTH_MAX_MANUFACTURE) {
            Log.w(TAG, "Manufacturer length > " + LENGTH_MAX_MANUFACTURE + ", cut it!");
            manufacturer = manufacturer.substring(0, LENGTH_MAX_MANUFACTURE);
        }

        String model = Build.MODEL;
        model = model.replaceAll("-", " ");
        if (model.indexOf(manufacturer) != -1) {
            model = model.replaceFirst(manufacturer, "");
        }

        String result = manufacturer + "-" + model;
        if (result.length() > LENGTH_MAX_MODEL) {
            Log.w(TAG, "Model length > " + LENGTH_MAX_MODEL + ", cut it!");
            result = result.substring(0, LENGTH_MAX_MODEL);
        }

        return result;
    }

    private String getMeid() {
        String result = "";
        String meid = mService.getDeviceMeid();
        String imeiSlot0 = mService.getImei(0);
        Log.i(TAG,"[getMeid] meid/imei0: " + PlatformManager.encryptMessage(meid) + "/"
                + PlatformManager.encryptMessage(imeiSlot0));
        if (imeiSlot0.startsWith(meid)) {
            result = imeiSlot0;
        } else {
            result = meid;
        }
        return result;
    }

    private String getCMDAIMSI() {
        return mService.getCurrentCDMAImsi();
    }

    private String getSoftwareVersion() {
        String result = PlatformManager.getSoftwareVersion();
        if (result.length() > LENGTH_MAX_VERSION) {
            Log.w(TAG, "Software version length > " + LENGTH_MAX_VERSION + ", cut it!");
            result = result.substring(0, LENGTH_MAX_VERSION);
        }

        return result;
    }

    private String generateChecksum(byte[] data) {
        CRC32 checksum = new CRC32();
        checksum.update(data);
        long value = checksum.getValue();

        String crcString = Long.toHexString(value);
        int crcStringLength = crcString.length();
        if (crcStringLength < LENGTH_CHECKSUM) {
            String prefix = "";
            for (int i = crcStringLength; i < LENGTH_CHECKSUM; i++) {
                prefix += "0";
            }
            crcString = prefix + crcString;
        }
        return crcString;
    }

}
