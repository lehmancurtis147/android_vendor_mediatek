
package com.mediatek.op08.cellbroadcastreceiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mediatek.cmas.ext.DefaultCmasMainSettingsExt;


public class OP08CmasMainSettingsExt extends DefaultCmasMainSettingsExt {

    private static final int PRESIDENT_ALERT_ID = 4370;
    private static final String TAG = "CellBroadcastReceiver/OP08CmasMainSettingsExt";

    public static final String KEY_ALERT_SOUND_VOLUME = "enable_key_sound_volume";
    public static final String KEY_ENABLE_ALERT_VIBRATE = "enable_key_alert_vibrate";
    public static final String KEY_ENABLE_CELLBROADCAST = "enable_cell_broadcast";

    private float mAlertVolume = 1.0f;
    private Context mContext;

    public OP08CmasMainSettingsExt(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public boolean needToaddAlertSoundVolumeAndVibration() {
        Log.d(TAG, "OP08 NeedToaddAlertSoundVolumeAndVibration");
        return true;
    }

    @Override
    public float getAlertVolume(int msgId) {
        Log.d("@M_" + TAG, "[getAlertVolume]");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        float alertVolume = prefs.getFloat(KEY_ALERT_SOUND_VOLUME, 1.0f);

        Log.d("@M_" + TAG, "[getAlertVolume] AlertVolume: " + alertVolume);

        if (msgId == PRESIDENT_ALERT_ID && alertVolume == 0.0f) {
            Log.d("@M_" + TAG, "[getAlertVolume] PRESIDENT_ALERT");
            return 1.0f;
        }
        return alertVolume;
    }

    @Override
    public boolean getAlertVibration(int msgId) {
        Log.d("@M_" + TAG, "[getAlertVibration]");
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);

        if (msgId == PRESIDENT_ALERT_ID) {
            Log.d("@M_" + TAG, "[getAlertVibration] PRESIDENT_ALERT");
            return true;
        }
        return prefs.getBoolean(KEY_ENABLE_ALERT_VIBRATE, true);
    }

    @Override
    public boolean setAlertVolumeVibrate(int msgId, boolean currentValue) {
        Log.d("@M_" + TAG, "[setAlertVolumeVibrate]");
        if (msgId == PRESIDENT_ALERT_ID) {
            Log.d("@M_" + TAG, "[setAlertVolume] PRESIDENT_ALERT");
            return true;
        }
        Log.d("@M_" + TAG, "[setAlertVolumeVibrate] return currentValue:" + currentValue);
        return currentValue;
    }

    @Override
    public void updateVolumeValue(float alertVolume) {
        Log.d("@M_" + TAG, "[updateVolumeValue] " + alertVolume);
        mAlertVolume = alertVolume;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(KEY_ALERT_SOUND_VOLUME, mAlertVolume);
        editor.commit();
    }

    @Override
    public void updateVibrateValue(boolean value) {
        Log.d("@M_" + TAG, "[updateVibrateValue] " + value);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(KEY_ENABLE_ALERT_VIBRATE, value);
        editor.commit();
    }
}
