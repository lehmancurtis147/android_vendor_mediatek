
package com.mediatek.op12.cellbroadcastreceiver;

import android.content.Context;
import android.os.SystemProperties;
import android.util.Log;

import com.android.internal.telephony.TelephonyProperties;
import com.mediatek.cmas.ext.DefaultCmasMainSettingsExt;

public class OP12CmasMainSettingsExt extends DefaultCmasMainSettingsExt {

    private static final String TAG = "CellBroadcastReceiver/OP12CmasMainSettingsExt";

    public OP12CmasMainSettingsExt(Context context) {
        super(context);
        Log.i(TAG, "Load OP12 CmasMainSetting Plugin");
    }

    /**
     * Get ECBM value from Main Setting
     * @return boolean whether is Emergency Callback Mode
     */
    @Override
    public boolean needBlockMessageInEcbm() {
        Log.i(TAG, "Plugin isEmergencyCallbackMode");
        return Boolean.parseBoolean(
            SystemProperties.get(TelephonyProperties.PROPERTY_INECM_MODE, "false"));
    }

}
