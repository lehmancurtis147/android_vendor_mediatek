package com.mediatek.op18.mms;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.RingtonePreference;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;

import com.mediatek.mms.ext.DefaultOpNotificationPreferenceActivityExt;

import java.util.List;

public class Op18NotificationPreferenceActivityExt extends
                    DefaultOpNotificationPreferenceActivityExt {

    private static final String TAG = "Mms/Op18NotificationPreferenceActivityExt";

    public static final String DEFAULT_RINGTONE = "content://settings/system/notification_sound";
    private static int msimcount;
    private static final String PREFERENCE_NAME = "preference";
    private static final String PREFERENCE_NAME_NOTIFICATION_RINGTONE = "pref_key_ringtone";
    public static final String NOTIFICATION_RINGTONE_PREFERENCE_KEY = "pref_key_ringtone";
    private Op18MmsPreference mMmsPreferenceExt = null;
    private RingtonePreference mringtonePref;
    Context mContext;

    public  Op18NotificationPreferenceActivityExt(Context context) {
        mContext = context;
        mMmsPreferenceExt = Op18MmsPreference.getInstance(context);
    }

    @Override
    public String getKey(int simCount, String defaultKey) {
        Log.d(TAG, "changeSingleCardKeyToSimRelated simCount = " + simCount);
        return mMmsPreferenceExt.getRingtoneKey(simCount, defaultKey);
    }

    private void changeSingleCardKeyToSimRelated() {
        Log.d(TAG, "changeSingleCardKeyToSimRelated simCount = " + msimcount);
        String key = getKey(msimcount, null);
        mringtonePref.setKey(key);
    }

    @Override
    public void setMessagePreferences(final Activity hostActivity,  int simCount,
                                                PreferenceCategory pC) {
        Log.d(TAG, "setMessagePreferences simCount = " + simCount);
        mMmsPreferenceExt.configRingtonePreference(hostActivity,  simCount, pC);
    }

    @Override
    public boolean setRingtoneSummary() {
        return false;
    }
    public void restoreDefaultPreferences(Activity hostActivity, SharedPreferences.Editor editor) {
        List<SubscriptionInfo> simList = SubscriptionManager.from(hostActivity)
                                                    .getActiveSubscriptionInfoList();
        if (simList != null) {
            int simCount = simList.size();
            for (int index = 0; index < simCount; index ++) {
                int subId = simList.get(index).getSubscriptionId();
                String key = Integer.toString(subId) + "_" +
                                NOTIFICATION_RINGTONE_PREFERENCE_KEY;
                editor.remove(key);
            }
        }
    }

    @Override
    public void onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
    }

    @Override
    public boolean addSmsInputModePreference(Preference.OnPreferenceChangeListener listener) {
        return false;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object arg1) {
        return false;
    }

    @Override
    public boolean onSimStateChanged() {
        Log.d(TAG, "onSimStateChanged ");
        mMmsPreferenceExt.onSimStateChanged();
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                Log.d(TAG, "onActivityResult");
                mMmsPreferenceExt.onActivityResult(requestCode, resultCode, data);
    }
}

