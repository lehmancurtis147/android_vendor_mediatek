package com.mediatek.mms.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.mms.ext.IOpMessagePluginExt;
import com.mediatek.mms.ext.OpMmsCustomizationFactoryBase;

public class Op17MmsCustomizationFactory extends OpMmsCustomizationFactoryBase {

    private static String TAG = "Op17MmsCustomizationFactory";

    public Op17MmsCustomizationFactory(Context context) {
        super(context);
        Log.d(TAG, "[Op17MmsCustomizationFactory]context=" + context.getApplicationInfo());
    }

    @Override
    public IOpMessagePluginExt makeOpMessagePluginExt() {
        return new Op17MessagePluginExt(mContext);
    }
}
