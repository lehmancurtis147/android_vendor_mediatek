package com.mediatek.backuprestore.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Looper;
import android.os.RemoteException;
import android.os.storage.StorageManager;
import android.os.storage.StorageVolume;
import android.os.Environment;
import android.os.SystemProperties;
import android.support.v4.provider.DocumentFile;
import android.provider.DocumentsContract;
import android.util.Log;

import com.mediatek.backuprestore.R;
import com.mediatek.backuprestore.utils.Constants.LogTag;
import com.mediatek.backuprestore.utils.Constants.ModulePath;
import com.mediatek.storage.StorageManagerEx;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SDCardUtils {
    private static final String TAG = MyLogger.LOG_TAG + "/SDCardUtils";
    public static final int MINIMUM_SIZE = 512;
    //private static String usbPath = Environment.DIRECTORY_USBOTG;
    private static DocumentFile mBackupFolder = null;
    private static DocumentFile mDataFolder = null;
    private static DocumentFile mAppFolder = null;
    private static Uri userChoicePathUri = null;
    /*sdcard  exist, this file exist, or this file not exist*/
    private static String MMCBLK1 = "/dev/block/mmcblk1";

    /**
            this methord wil be called when user choice new folder to put backup data.
        */
    public static boolean setUserChoicePath(Context context, Uri uri) {
        if (uri.equals(userChoicePathUri) && mBackupFolder.exists()
                && mDataFolder.exists() && mAppFolder.exists()) {
            return true;
        }

        DocumentFile selectFolder = DocumentFile.fromTreeUri(context, uri);
        if (!checkStoragePath(selectFolder)) {
            Log.d(TAG,
                    "setUserChoicePath choice path again:"
                            + selectFolder.getName());
            return false;
        }

        DocumentFile backupFolder = null;
        if (selectFolder.getName().equals(ModulePath.FOLDER_BACKUP)) {
            Log.d(TAG, "setUserChoicePath :  folder name is backup");
            backupFolder = selectFolder;
        } else {
            Log.d(TAG,
                    "setUserChoicePath :  folder name: "
                            + selectFolder.getName());
            DocumentFile file = selectFolder.findFile(ModulePath.FOLDER_BACKUP);
            if (file != null && file.isDirectory()) {
                Log.d(TAG, "setUserChoicePath FOLDER_DATA EXITS cant create");
                backupFolder = file;
            }
        }
        if (backupFolder == null) {
            Log.d(TAG, "setUserChoicePath FOLDER_DATA not EXITS, create");
            backupFolder = selectFolder
                    .createDirectory(ModulePath.FOLDER_BACKUP);
        }
        if (backupFolder == null) {
            Log.d(TAG,
                    "setUserChoicePath backupFolder is null, choice path again:");
            return false;
        }

        userChoicePathUri = uri;
        Log.d(TAG, "setUserChoicePath :" + userChoicePathUri);
        mBackupFolder = backupFolder;
        Log.d(TAG, "setUserChoicePath mBackupFolder:" + mBackupFolder.getName());
        mDataFolder = null;
        mAppFolder = null;

        DocumentFile[] files = mBackupFolder.listFiles();
        for (DocumentFile file : files) {
            if (file.exists() && file.isDirectory()) {
                if (file.getName().equals(ModulePath.FOLDER_DATA)) {
                    Log.d(TAG, "FOLDER_DATA EXITS need not create");
                    mDataFolder = file;
                }
                if (file.getName().equals(ModulePath.FOLDER_APP)) {
                    Log.d(TAG, "FOLDER_DATA EXITS need not create");
                    mAppFolder = file;
                }
            }
        }
        if (mDataFolder == null) {
            Log.d(TAG, "setUserChoicePath create data folder");
            mDataFolder = mBackupFolder.createDirectory(ModulePath.FOLDER_DATA);
        }
        if (mAppFolder == null) {
            Log.d(TAG, "setUserChoicePath create app folder");
            mAppFolder = mBackupFolder.createDirectory(ModulePath.FOLDER_APP);
        }
        return true;
    }

    public static Uri getStoragePathUri(Context context) {
        if (checkStoragePath(mBackupFolder)) {
            return userChoicePathUri;
        } else {
            Log.d(TAG, "getStoragePathUri need user choice path again");
        }
        return null;
    }

   /* public static DocumentFile getStoragePath(Context context) {
        if (checkStoragePath(mBackupFolder)) {
            return mBackupFolder;
        } else {
            Log.d(TAG, "getStoragePath need user choice path again");
        }
        return null;
    }*/

    public static DocumentFile getDataBackupDF(Context context) {
        if (checkStoragePath(mDataFolder)) {
            return mDataFolder;
        } else if (checkStoragePath(mBackupFolder)) {
            Log.d(TAG, "getDataBackupDF app folder not exit,"
                    + " but backup folder exit, create data folder");
            mDataFolder = mBackupFolder.createDirectory(ModulePath.FOLDER_DATA);
            return mDataFolder;
        }
        Log.d(TAG, "getDataBackupDF need user choice path again");
        return null;
    }

    public static String getPhoneStoragePath(Context context) {
        StorageManager storageManager = null;
        String phonePath = null;
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                if (!volume.isRemovable()) {
                    phonePath = volume.getPath();
                    return phonePath;
                }
            }
        }
        return phonePath;
    }

    public static String getSdCardMountPath(Context context) {
        StorageManager storageManager = null;
        String sdcardPath = null;
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                if (volume.isRemovable()) {
                    sdcardPath = volume.getPath();
                    MyLogger.logD(LogTag.LOG_TAG, "getSdCardMountPath = " + sdcardPath);
                    return sdcardPath;
                }
            }
        }
        MyLogger.logD(LogTag.LOG_TAG, "getSdCardMountPath() = null");
        return sdcardPath;
    }

    public static DocumentFile getAppBackupDF(Context context) {
        if (checkStoragePath(mAppFolder)) {
            return mAppFolder;
        } else if (checkStoragePath(mBackupFolder)) {
            Log.d(TAG, "getAppBackupDF app folder not exit,"
                    + " but backup folder exit, create app folder");
            mAppFolder = mBackupFolder.createDirectory(ModulePath.FOLDER_APP);
            return mAppFolder;
        }
        Log.d(TAG, "getAppBackupDF need user choice path again");
        return null;
    }

    private static boolean checkStoragePath(DocumentFile storagePath) {
        boolean ret = false;
        if (storagePath != null) {
            if (storagePath.exists() && storagePath.isDirectory()) {
                try {
                    DocumentFile temp = storagePath.createDirectory(".BackupRestoretemp");
                    if (temp.exists()) {
                        ret = temp.delete();
                    } else {
                        MyLogger.logE(LogTag.LOG_TAG, "temp not exists() return false");
                        ret = false;
                    }
                } catch (Exception e) {
                    MyLogger.logE(LogTag.LOG_TAG, "createDirectory exception return false");
                    ret = false;
                }
            }
        }
        return ret;
    }

    public static boolean isSDCardExist() {
        File sdcard = new File(MMCBLK1);
        if (sdcard.exists()) {
            MyLogger.logD(LogTag.LOG_TAG, "isSDCardExist true");
            return true;
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "isSDCardExist false");
            return false;
        }
    }

    public static String getSDCardDataPath(Context context) {
        String storagePath = getSdCardMountPath(context);
        if (storagePath == null) {
            MyLogger.logD(LogTag.LOG_TAG, "storagePath == null return");
            return null;
        }
        storagePath = storagePath + File.separator + ModulePath.FOLDER_BACKUP;
        MyLogger.logD(LogTag.LOG_TAG, "getStoragePath: path is " + storagePath);
        if (isFileExist(storagePath)) {
            return storagePath;
        }
        return null;
    }

    public static String getPhoneDataPath(Context context) {
        String storagePath = getPhoneStoragePath(context);
        if (storagePath == null) {
            MyLogger.logD(LogTag.LOG_TAG, "storagePath == null return");
            return null;
        }
        storagePath = storagePath + File.separator + ModulePath.FOLDER_BACKUP;
        MyLogger.logD(LogTag.LOG_TAG, "getStoragePath: path is " + storagePath);
        if (isFileExist(storagePath)) {
            return storagePath;
        }
        return null;
    }

    private static boolean isFileExist(String path) {
        File file = new File(path);
        if (file != null && file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean getSupprotSDcardInfo() {
        File dir = new File("vendor/etc");
        File[] files = null;
        if (dir != null && dir.exists()) {
            files = dir.listFiles();
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "vendor/etc path is null, return false");
            return false;
        }

        File fstabFile = null;
        for (File file:files) {
            if (file.getName().startsWith("fstab")) {
                MyLogger.logD(LogTag.LOG_TAG, "dfstabFile path = " + file.getAbsolutePath());
                fstabFile = file;
                break;
            }
        }

        BufferedReader buffreader;
        if (fstabFile != null && fstabFile.exists()) {
            try {
                buffreader = new BufferedReader(new InputStreamReader(
                        new FileInputStream(fstabFile)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        } else {
            MyLogger.logD(LogTag.LOG_TAG, "fstab file is not exit, return false");
            return false;
        }

        String line =null;
        try {
            while ((line = buffreader.readLine()) != null) {
                if (line.contains("voldmanaged=sdcard1")) {
                    MyLogger.logD(LogTag.LOG_TAG,"support sdcard return true");
                    return true;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        MyLogger.logD(LogTag.LOG_TAG, " not find return false ");
        return false;
    }

    private static boolean isNeedGetSdCardStatus = true;
    private static boolean isSupportSdCard = true;
    public static boolean isSupprotSDcard() {
        if (isNeedGetSdCardStatus) {
            MyLogger.logD(LogTag.LOG_TAG,"get sdcard support state ");
            isSupportSdCard = getSupprotSDcardInfo();
            isNeedGetSdCardStatus = false;
        }
        MyLogger.logD(LogTag.LOG_TAG, "isSupportSdCard = " + isSupportSdCard);
        return isSupportSdCard;
    }

    public static long getAvailableSize(String file) {
        android.os.StatFs stat = new android.os.StatFs(file);
        long count = stat.getAvailableBlocks();
        long size = stat.getBlockSize();
        long totalSize = count * size;
        MyLogger.logD(LogTag.LOG_TAG, "file remain size = " + totalSize);
        return totalSize;
    }

    public static boolean checkedPath(String path) {
        File checkedFile = new File(path);
        if (checkedFile != null) {
            if (checkedFile.exists() && checkedFile.isDirectory()) {
                Log.e(LogTag.LOG_TAG, "checkedPath: the path is a folder and it is exists! ");
                return true;
            }
            if (checkedFile.exists() && checkedFile.isFile()) {
                Log.e(LogTag.LOG_TAG, "checkedPath: the path is a File and it is exists! ");
                return true;
            }
        }
        return false;
    }

    public static String getSDStatueMessage(Context context) {
        String message = context.getString(R.string.nosdcard_notice);
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_SHARED) || status.equals(Environment.MEDIA_UNMOUNTED)) {
            message = context.getString(R.string.sdcard_busy_message);
        }
        return message;
    }

    public static List<String> getPathList(Context context) {
        StorageManager storageManager = null;
        List<String> pathList = new ArrayList<String>();
        storageManager = (StorageManager) context.getSystemService(Context.STORAGE_SERVICE);
        StorageVolume[] volumes = storageManager.getVolumeList();
        if (volumes != null) {
            for (StorageVolume volume : volumes) {
                String path = volume.getPath();
              //  if (path != null && !StorageManagerEx.isUSBOTG(path)) {
                    if (path != null) {
                    pathList.add(path);
                }
            }
        }
        return pathList;
    }

    private static final String AUTHORITY_EXTERNAL_STORAGE =
            "com.android.externalstorage.documents";
    private static final String FILE_PATH_INDEX = "storage/";

    public static DocumentFile getDocumentFile(Context context, File file) {
        // /storage/3558-110E/mtklog/netlog/NTLog_2018_0620_142338
        String filePath = file.getAbsolutePath();
        int externalStroageIndex = filePath.indexOf(FILE_PATH_INDEX);
        if (externalStroageIndex == -1) {
            return null;
        }
        String docmentId = filePath.substring(externalStroageIndex
                + FILE_PATH_INDEX.length());
        if (!docmentId.contains("/")) {
            docmentId = docmentId + "/";
        }
        docmentId = docmentId.replaceFirst("/", ":");
        MyLogger.logD(LogTag.LOG_TAG, "docmentId: " + docmentId);
        Uri uri = DocumentsContract.buildTreeDocumentUri(AUTHORITY_EXTERNAL_STORAGE, docmentId);
        MyLogger.logD(LogTag.LOG_TAG, "buildTreeDocumentUri: " + uri);
        if (uri != null) {
            return DocumentFile.fromTreeUri(context, uri);
        } else {
            MyLogger.logE(LogTag.LOG_TAG, "buildTreeDocumentUri null, return");
            return null;
        }
    }

    public static boolean isPhonePath() {
        if (userChoicePathUri.toString().contains("primary")) {
            MyLogger.logD(LogTag.LOG_TAG, "isPhonePath is phone file: " + userChoicePathUri);
            return true;
        } else {
            return false;
        }
    }
}
