package com.mediatek.rcse.plugin.contacts;

import android.content.Context;

import com.mediatek.dialer.ext.IRCSeCallLogExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

public class RCSeDialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    private Context mContext;

    public RCSeDialerCustomizationFactory(Context context) {
        mContext = context;
    }

    public IRCSeCallLogExtension makeRCSeCallLogExt() {
        return new CallLogExtensionForRCS(mContext);
    }
}
