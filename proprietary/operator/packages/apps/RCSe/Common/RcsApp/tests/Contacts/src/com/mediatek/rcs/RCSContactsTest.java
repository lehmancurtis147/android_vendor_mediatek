package com.mediatek.rcs;

import java.util.ArrayList;

import junit.framework.Assert;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredName;
import android.provider.ContactsContract.Contacts.Data;
import android.provider.ContactsContract.PhoneLookup;
import android.provider.ContactsContract.RawContacts;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.gsma.services.rcs.capability.Capabilities;

import com.jayway.android.robotium.solo.Solo;
import com.android.contacts.activities.PeopleActivity;
import com.android.contacts.common.list.ContactListItemView;

/**
 * The Class RCSContactsTest.
 */
public class RCSContactsTest extends
    ActivityInstrumentationTestCase2<PeopleActivity> {

  private Solo solo;

  public static final String TAG = "ContactsTest";
  public static final String CONTACT_NONRCS_CAPABLE = "NonRcs Contact";
  public static final String CONTACT_RCS_CAPABLE = "Rcs Capable";
  public static final String CONTACT_LIVE_RCS_CAPABLE = "0593";
  
  public static final String CONTACT_NONRCS_CAPABLE_NUMBER = "13579";
  public static final String CONTACT_RCS_CAPABLE_NUMBER = "567";
  
  public static final int MAX_NUMBER_OF_SCROLLS = 100;
  
  private static final String CONTACT_CAPABILITIES =
      "com.orangelabs.rcs.capability.CONTACT_CAPABILITIES";
  /**
   * MIME type for RCS status
   */
  private static final String MIMETYPE_RCS_STATUS =
          "vnd.android.cursor.item/com.orangelabs.rcs.rcs-status";

  /**
   * Instantiates a new RCS contacts test.
   */
  public RCSContactsTest() {
    super(PeopleActivity.class);
  }

  public void setUp() throws Exception {
    solo = new Solo(getInstrumentation(), getActivity());
    Log.d(TAG, "setUp entry ");
    insertContactNonRcsCapable();
    insertContactRcsCapable();
  }

  
  @Override
  public void tearDown() throws Exception {
      Log.d(TAG, "tearDown entry ");
      solo.finishOpenedActivities();
  }
  
  /**
   * Test1 test non rcs capable contact, tap on contact
   * and verify Joyn entry should not be there.
   */
  public void test1NonRcsCapableContact(){

      Log.d(TAG, "testNonRcsCapableContact entry ");
      solo.assertCurrentActivity("wrong activity", PeopleActivity.class);

      //search for non rcs capable contact
      boolean found = false;
      for(int i=0 ; i <= MAX_NUMBER_OF_SCROLLS ; i++){
          found = solo.searchText("NonRcs");
          if(found == true){
              break;
          }
          solo.scrollDown();
      }
      //if still not found, search bottom
      if(found == false){
          solo.scrollToBottom();
      }
      //click non rcs capable contact
      solo.clickOnText("NonRcs");
      solo.sleep(5000);
      
      //check rcs joyn expanding entry view
      found = false;
      found = solo.searchText("Joyn");
      if(found == false){
          solo.scrollDown();
          found = solo.searchText("Joyn");
      }
      assertTrue(!found);
  }
  
  /**
   * Test2 rcs capable contact, tap on contact
   * and verify Joyn entry should be there.
   */
  public void test2RcsCapableContact(){

      Log.d(TAG, "testRcsCapableContact entry ");
      solo.assertCurrentActivity("wrong activity", PeopleActivity.class);
      
      //search for rcs capable contact
      boolean found = false;
      for(int i=0 ; i <= MAX_NUMBER_OF_SCROLLS ; i++){
          found = solo.searchText("Rcs Capable");
          if(found == true){
              break;
          }
          solo.scrollDown();
      }
      //if still not found, search bottom
      if(found == false){
          solo.scrollToBottom();
      }
      //click rcs capable contact
      solo.clickOnText("Rcs Capable");
      solo.sleep(5000);
      
      //check rcs expanding view
      found = false;
      found = solo.searchText("Joyn");
      if(found == false){
          solo.scrollDown();
          found = solo.searchText("Joyn");
      }
      assertTrue(found);
      
  }
  
  /**
   * Test3 live account.
   * Check live account which is rcs capable.
   */
  public void test3LiveAccount(){
      Log.d(TAG, "testLiveAccount entry ");
      solo.assertCurrentActivity("wrong activity", PeopleActivity.class);
      
      //search for rcs capable live contact
      boolean found = false;
      for(int i=0 ; i <= MAX_NUMBER_OF_SCROLLS ; i++){
          found = solo.searchText(CONTACT_LIVE_RCS_CAPABLE);
          if(found == true){
              break;
          }
          solo.scrollDown();
      }
      //if still not found, search bottom
      if(found == false){
          solo.scrollToBottom();
      }
      //click rcs capable contact
      solo.clickOnText(CONTACT_LIVE_RCS_CAPABLE);
      solo.sleep(5000);
      
      //check rcs expanding view
      found = false;
      found = solo.searchText("Joyn");
      if(found == false){
          solo.scrollDown();
          found = solo.searchText("Joyn");
      }
      assertTrue(found);
  }
  
  /**
   * Test4 rcs icon in contact list.
   */
  public void test4RcsIcon(){

      Log.d(TAG, "testRcsIcon entry ");
      solo.assertCurrentActivity("wrong activity", PeopleActivity.class);
      //search for rcs capable contact
      boolean found = false;
      for(int i=0 ; i <= MAX_NUMBER_OF_SCROLLS ; i++){
          found = solo.searchText("Rcs Capable");
          if(found == true){
              break;
          }
          solo.scrollDown();
      }
      ListView listView = (ListView)solo.getView("list");
      ArrayList<View> views = solo.getViews(listView);
      Log.d(TAG, "views: "+ views + ",size:"+views.size());
      for(View view : views){
          if(view instanceof TextView || view instanceof ImageView){
              
              Log.d(TAG, "view:"+view);
              if(view instanceof TextView){
                  Log.d(TAG, "text:"+((TextView) view).getText());
              } else if (view instanceof ImageView) {
                  Log.d(TAG, "image width"+view.getWidth());
              }
          }
      }
      
      /*if(found == true){
          TextView textView = solo.getText("Rcs Capable");
          View view = solo.getTopParent(textView);
          Log.d(TAG, "parent view:"+view);
      }*/
      assertTrue(true);
  }
  
  /**
   * Insert contact non rcs capable.
   */
  public void insertContactNonRcsCapable(){
      boolean found = contactExists(CONTACT_NONRCS_CAPABLE_NUMBER);
      Log.d(TAG, "found non rcs:"+found);
      if(found == false) {
          ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
          int rawContactInsertIndex = ops.size();
    
          ops.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
            .withValue(RawContacts.ACCOUNT_TYPE, null)
            .withValue(RawContacts.ACCOUNT_NAME, null).build());
    
          //Phone Number
          ops.add(ContentProviderOperation
            .newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
              rawContactInsertIndex)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.NUMBER, CONTACT_NONRCS_CAPABLE_NUMBER)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.TYPE, "1").build());
    
          //Display name/Contact name
          ops.add(ContentProviderOperation
            .newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID,
              rawContactInsertIndex)
            .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
            .withValue(StructuredName.DISPLAY_NAME, CONTACT_NONRCS_CAPABLE)
            .build());
          
          try {
              ContentProviderResult[] res = getActivity().getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);
             } catch (RemoteException e) {
              e.printStackTrace();
             } catch (Exception e) {
              e.printStackTrace();
             }
      }
  }
  
  /**
   * Insert contact rcs capable.
   */
  public void insertContactRcsCapable(){
      boolean found = contactExists(CONTACT_RCS_CAPABLE_NUMBER);
      Log.d(TAG, "found rcs:"+found);
      if(found == false) {
          ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();
          int rawContactInsertIndex = ops.size();
    
          ops.add(ContentProviderOperation.newInsert(RawContacts.CONTENT_URI)
            .withValue(RawContacts.ACCOUNT_TYPE, null)
            .withValue(RawContacts.ACCOUNT_NAME, null).build());
    
          //Phone Number
          ops.add(ContentProviderOperation
            .newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID,
              rawContactInsertIndex)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.NUMBER, CONTACT_RCS_CAPABLE_NUMBER)
            .withValue(Data.MIMETYPE, Phone.CONTENT_ITEM_TYPE)
            .withValue(Phone.TYPE, "1").build());
    
          //Display name/Contact name
          ops.add(ContentProviderOperation
            .newInsert(ContactsContract.Data.CONTENT_URI)
            .withValueBackReference(Data.RAW_CONTACT_ID,
              rawContactInsertIndex)
            .withValue(Data.MIMETYPE, StructuredName.CONTENT_ITEM_TYPE)
            .withValue(StructuredName.DISPLAY_NAME, CONTACT_RCS_CAPABLE)
            .build());
          
          ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                  .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                  .withValue(Data.MIMETYPE, MIMETYPE_RCS_STATUS)
                  .withValue(Data.DATA1, CONTACT_RCS_CAPABLE_NUMBER)
                  .withValue(Data.DATA2, 0)
                  .build());
          
          try {
              ContentProviderResult[] res = getActivity().getContentResolver().applyBatch(
                ContactsContract.AUTHORITY, ops);
             } catch (RemoteException e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
             } catch (Exception e) {
              // TODO Auto-generated catch block
              e.printStackTrace();
             }
      }
  }
  
  /**
   * Contact exists in phonebook or not.
   *
   * @param number the number
   * @return true, if successful
   */
  public boolean contactExists(String number) {
      if (number != null) {
          Uri lookupUri = Uri.withAppendedPath(PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
          String[] mPhoneNumberProjection = { PhoneLookup._ID, PhoneLookup.NUMBER, PhoneLookup.DISPLAY_NAME };
          Cursor cur = getActivity().getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
          try {
              if (cur.moveToFirst()) {
                  return true;
              }
          } finally {
              if (cur != null)
                  cur.close();
          }
          return false;
      } else {
          return false;
      }
  }
} 