package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter;
import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;
import com.orangelabs.rcs.core.ims.reg.RegCore;
import com.orangelabs.rcs.platform.AndroidFactory;

import com.orangelabs.rcs.utils.logger.Logger;


import java.util.ArrayList;

/**
 * The Class RcsUaEventDispatcher.
 */
public class RcsUaEventDispatcher extends Handler {

    private Context mContext;
    private ArrayList<RCSEventDispatcher> mRCSEventDispatcher = new ArrayList<RCSEventDispatcher>();
    private static final String TAG = "RcsUaEventDispatcher";
    private RcsProxySipHandler mRCSProxySiphandler;
    private RcsProxyRegistrationHandler mRCSRegistrationHandler;

    /**
     * The logger
     */
    private Logger logger = Logger.getLogger(this.getClass().getName());



    /**
     * Instantiates a new rcs ua event dispatcher.
     *
     * @param context the context
     */
    public RcsUaEventDispatcher(RcsUaAdapter adapter) {

        logger.debug( "RcsUaEventDispatcher");

        mContext = AndroidFactory.getApplicationContext();
        mRCSRegistrationHandler = new RcsProxyRegistrationHandler(adapter);
        mRCSEventDispatcher.add(mRCSRegistrationHandler);
        mRCSProxySiphandler = new RcsProxySipHandler(adapter);
        mRCSEventDispatcher.add(mRCSProxySiphandler);
    }

    /**
     * Dispatch callback.
     *
     * @param event the event
     */
    void dispatchCallback(RcsUaEvent event) {

        //logger.debug( "event from proxy: " + event.getRequestId());

        switch (event.getRequestId()) {

        /* Events for LTE/WFC/234G Registration */
        case RcsUaAdapter.RSP_REQ_REG_INFO:
        case RcsUaAdapter.RSP_IMS_REGISTER:
        case RcsUaAdapter.RSP_IMS_DEREGISTER:
        case RcsUaAdapter.RSP_IMS_REGISTERING:
        case RcsUaAdapter.RSP_IMS_DEREGISTERING:
            mRCSRegistrationHandler.EventCallback(event);
            break;

        /*  Events for SIP */
        case RcsUaAdapter.RSP_EVENT_SIP_MSG:
            mRCSProxySiphandler.EventCallback(event);
            break;

        /* Events for 234G Registration */
        case RcsUaAdapter.EVENT_IMS_AUTH_REQ:
        case RcsUaAdapter.EVENT_IMS_GEOLOCATION_REQ:
        case RcsUaAdapter.EVENT_IMS_QUERY_STATE:
        case RcsUaAdapter.EVENT_IMS_EMS_MODE_INFO:
        case RcsUaAdapter.EVENT_IMS_DIGITLING_REG_IND:
            RegCore.getInstance().handleEvent(event);
            break;

        default:
            logger.debug("un-supported event: " + event.getRequestId());
            break;
        }
    }

    /**
     * The Interface RCSEventDispatcher.
     */
    public interface RCSEventDispatcher {

        /**
         * Event callback.
         *
         * @param event the event
         */
        void EventCallback(RcsUaEvent event);

        /**
         * Enable request.
         */
        void enableRequest();

        /**
         * Disable request.
         */
        void disableRequest();
    }

    /**
     * Enable request.
     */
    void enableRequest() {
        for (RCSEventDispatcher dispatcher : mRCSEventDispatcher) {
            dispatcher.enableRequest();
        }
    }

    /**
     * Disable request.
     */
    void disableRequest() {
        for (RCSEventDispatcher dispatcher : mRCSEventDispatcher) {
            dispatcher.disableRequest();
        }
    }


    /**
     * HANDLE MESSAGES FROM RCS PROXY.
     *
     * @param msg the msg
     */
    @Override
    public void handleMessage(Message msg) {
        dispatchCallback((RcsUaEvent) msg.obj);
    }

    /**
     * Gets the sip event dispatcher.
     *
     * @return the sip event dispatcher
     */
    public RCSEventDispatcher getSipEventDispatcher() {
        return mRCSProxySiphandler;
    }

    /**
     * Gets the registration event handler.
     *
     * @return the registration event handler
     */
    public RCSEventDispatcher getRegistrationEventHandler() {
        return mRCSRegistrationHandler;
    }



}
