package com.orangelabs.rcs.core.ims.rcsua;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Looper;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsReasonInfo;
import android.text.TextUtils;

import com.android.ims.ImsConfig;
import com.android.ims.ImsConnectionStateListener;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsServiceClass;

import com.orangelabs.rcs.core.CoreException;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.network.sip.FeatureTags;
import com.orangelabs.rcs.core.ims.protocol.sip.SipException;
import com.orangelabs.rcs.core.ims.rcsua.RcsEventSettingTLV;
import com.orangelabs.rcs.core.ims.reg.RegCore;
import com.orangelabs.rcs.core.ims.userprofile.ImsVolteConfig;
import com.orangelabs.rcs.platform.AndroidFactory;
import com.orangelabs.rcs.platform.network.NetworkFactory;
import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.provider.settings.RcsSettingsData;
import com.orangelabs.rcs.utils.logger.Logger;

import com.mediatek.internal.telephony.MtkPhoneConstants;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.InterruptedIOException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.Thread.State;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;

import javax2.sip.ListeningPoint;import android.telephony.ServiceState;

import vendor.mediatek.hardware.radio_op.V1_1.IRadioOp;
import vendor.mediatek.hardware.rcs.V1_0.IRcs;
import vendor.mediatek.hardware.rcs.V1_0.IRcsIndication;

/**
 * The Class RcsUaAdapter.
 */
public class RcsUaAdapter {

    private boolean DEBUG_TRACE = false;

    private static final int BASE = 10000;

    /* request ims registartion info */
    public static final int CMD_REQ_REG_INFO                        = BASE + 1;
    /* request VoLTE stack for IMS Registration */
    public static final int CMD_IMS_REGISTER                        = BASE + 2;
    /* request VoLTE stack for IMS Deregistration */
    public static final int CMD_IMS_DEREGISTER                      = BASE + 3;
    /* send SIP request */
    public static final int CMD_SEND_SIP_MSG                        = BASE + 4;
    /* [ROI]update VoPS info to volte stack */
    public static final int CMD_IMS_NOTIFY_VOPS_INFO                = BASE + 5;
    /* [ROI]update IMS settings */
    public static final int CMD_IMS_UPDATE_SETTING                  = BASE + 6;
    /* [ROI]add capability for rcs service tag */
    public static final int CMD_IMS_ADD_CAPABILITY                  = BASE + 7;
    /* [ROI]notify RAT change info */
    public static final int CMD_IMS_NOTIFY_RAT_CHANGE               = BASE + 8;
    /* [ROI]notify network change info */
    public static final int CMD_IMS_NOTIFY_NETWORK_CHANGE           = BASE + 9;
    /* [ROI]send back authentication request result */
    public static final int CMD_IMS_AUTH_REQ_RESULT                 = BASE + 10;
    /* [ROI]send back geolocation request result */
    public static final int CMD_IMS_GEOLOCATION_REQ_RESULT          = BASE + 11;
    /* [ROI]send back query state result */
    public static final int CMD_IMS_QUERY_STATE_RESULT              = BASE + 12;
    /* [ROI]notify 3GPP RAT change info */
    public static final int CMD_IMS_NOTIFY_3GPP_RAT_CHANGE          = BASE + 13;
    /* notify rcs status to rds */
    public static final int CMD_RDS_NOTIFY_RCS_CONN_INIT            = BASE + 14;
    /* notify rcs connection status to rds */
    public static final int CMD_RDS_NOTIFY_RCS_CONN_ACTIVE          = BASE + 15;
    /* notify rcs connection status to rds */
    public static final int CMD_RDS_NOTIFY_RCS_CONN_INACTIVE        = BASE + 16;
    /* [ROI]update IMS RT settings */
    public static final int CMD_IMS_UPDATE_RT_SETTING               = BASE + 17;
    /* [ROI]update Digits Line Reg Info */
    public static final int CMD_IMS_REG_DIGITLINE                   = BASE + 18;
    /* RCS service activation */
    public static final int CMD_RCS_ACTIVATION                      = BASE + 19;
    /* RCS service deactivation */
    public static final int CMD_RCS_DEACTIVATION                    = BASE + 20;
    /* operator on SIM card */
    public static final int CMD_SIM_OPERATOR                        = BASE + 21;
    /*set Service Activation State, sync with rild*/
    public static final int CMD_SERVICE_ACTIVATION_STATE            = BASE + 22;



    /* response ims registartion info */
    public static final int RSP_REQ_REG_INFO                        = BASE + 31;
    /* intermediate response for IMS Registration */
    public static final int RSP_IMS_REGISTERING                     = BASE + 32;
    /* final response of IMS Registration */
    public static final int RSP_IMS_REGISTER                        = BASE + 33;
    /* intermediate response for IMS Deregistration */
    public static final int RSP_IMS_DEREGISTERING                   = BASE + 34;
    /* final response of IMS Deregistration */
    public static final int RSP_IMS_DEREGISTER                      = BASE + 35;
    /* a SIP request response or a SIP request from server */
    public static final int RSP_EVENT_SIP_MSG                       = BASE + 36;

    /* indicate that IMS is de-registering */
    public static final int EVENT_IMS_DEREGISTER_IND                = BASE + 51;
    /* [ROI]authentication request form volte stack */
    public static final int EVENT_IMS_AUTH_REQ                      = BASE + 52;
    /* [ROI]geolocation request form volte stack */
    public static final int EVENT_IMS_GEOLOCATION_REQ               = BASE + 53;
    /* [ROI]query state */
    public static final int EVENT_IMS_QUERY_STATE                   = BASE + 54;
    /* [ROI]EMS mode info indication */
    public static final int EVENT_IMS_EMS_MODE_INFO                 = BASE + 55;
    /* [ROI]AT command response */
    public static final int EVENT_IMS_DIGITLING_REG_IND             = BASE + 56;




    // RCS over 2/3G, follow VoLTE Stack Definition
    public static final int VOLTE_MAX_CELL_ID_LENGTH        = 64;
    public static final int VOLTE_MAX_SSID_LENGTH           = 32;
    public static final int VOLTE_MAX_TIME_STAMP_LENGTH     = 32;
    public static final int VOLTE_MAX_REG_CAPABILITY_LENGTH = 256;
    public static final int VOLTE_MAX_AUTH_NC               = 12;
    public static final int VOLTE_MAX_AUTH_RESPONSE         = 256;
    public static final int VOLTE_MAX_AUTH_AUTS             = 256;
    public static final int VOLTE_MAX_AUTH_CK               = 256;
    public static final int VOLTE_MAX_AUTH_IK               = 256;
    public static final int VOLTE_MAX_AT_CMDLINE_LEN        = 2028;

    /* align with VoLTE_Event_Reg_State_e */
    public final static int IMS_REG_STATE_REGISTERED        = 1;
    public final static int IMS_REG_STATE_UNREGISTERED      = 2;
    public final static int IMS_REG_STATE_REGISTERING       = 3;
    public final static int IMS_REG_STATE_DEREGISTERING     = 4;
    public final static int IMS_REG_STATE_DISCONNECTED      = 5;
    public final static int IMS_REG_STATE_AUTHENTICATING    = 6;
    public final static int IMS_REG_STATE_OOS               = 7;
    public final static int IMS_REG_STATE_CONNECTING        = 8;

    private static final String ACCESS_TYPE_PRELUDE = "IEEE";

    private static boolean hackyFlag = false;
    //IMS specific information
    private static String mPcscfAddress = "";
    private static int mLocalPort = 0;
    private static String mProtocolType = "UDP";
    private static String mLocalAddress = "";
    private static String mUserAgent = "";
    private static String mAssociatedUri = "";
    private static String mRoute = "";
    private static String mPANI = "";
    private static String[] mRoutes = {};
    private static String[] mAssociatedUris = {};
    private static String mInstanceId = "";
    private static int mRcsState = 0;
    private static int mRcsTag = 0;
    private static int mRegState = 0;
    private static int mLastRegstate = -1;

    //Digits related information
    private static int mVirtualLineCount = 0;

    /* Rcs HIDL */
    private volatile IRcs mRcsHal;
    private final IRcsDeathRecipient mIRcsDeathRecipient = new IRcsDeathRecipient();
    private final AtomicLong mRcsHalCookie = new AtomicLong(0);
    private RcsIndication mRcsIndication = new RcsIndication();

    private static final String TAG = "RcsUaAdapter";
    private static Context mContext;
    private RCSProxyEventQueue mEventQueue;
    private static RcsUaEventDispatcher mRCSUAEventDispatcher;

    private static RcsUaAdapter mInstance;
    private static boolean mIsRCSUAAdapterEnabled = false;
    private static boolean mIsRCSUAAdapterInit = false;
    private static boolean mIsSingleRegistrationSupported = false;
    private static boolean mRcsRegistered = false;
    private static boolean mWfcEnabled = false;
    private Messenger mMessanger;
    private static boolean isServiceStarted = false;
    private boolean mIsWfc;
    private static ImsManager mImsManager;


    //RCS SIP stack related details
    private static int mRcsSipStackPort = 0;
    public static final String VOLTE_SERVICE_NOTIFY_INTENT = "COM.MEDIATEK.RCS.IMS.VOLTE_SERVICE_NOTIFICATION";
    private Object mListenerLock = new Object();
    private Logger logger = Logger.getLogger(this.getClass().getName());

    /*
     * The Class RcsUaEvent for RCS UA Event Transmission
     */
    public static class RcsUaEvent {
        public static final int MAX_DATA_LENGTH = 70960;
        private int requestId;
        private int dataLen;
        private int readOffset;
        private byte data[];
        private int eventmaxdataLen = MAX_DATA_LENGTH;

        public RcsUaEvent(int id) {
            requestId = id;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        public RcsUaEvent(int id, int length) {
            requestId = id;
            eventmaxdataLen = length;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        public double putDouble(double value) {
            if (dataLen > eventmaxdataLen - 8) {
                return -1;
            }

            synchronized(this) {
                byte[] bytes = new byte[8];
                ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).putDouble(value);

                for (int i = 0; i < 8; ++i) {
                    data[dataLen] = bytes[i];
                    dataLen++;
                }
            }
            return 0;
        }

        public int putInt(int value) {
            if (dataLen > eventmaxdataLen - 4) {
                return -1;
            }
            synchronized(this) {
                for (int i = 0; i < 4; ++i) {
                    data[dataLen] = (byte)((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }
            return 0;
        }

        public int putShort(int value) {
            if (dataLen > eventmaxdataLen - 2) {
                return -1;
            }
            synchronized(this) {
                for (int i = 0; i < 2; ++i) {
                    data[dataLen] = (byte)((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }
            return 0;
        }

        public int putByte(int value) {
            if (dataLen > eventmaxdataLen - 1) {
                return -1;
            }

            synchronized(this) {
                data[dataLen] = (byte)(value & 0xFF);
                dataLen++;
            }
            return 0;
        }

        /**
         * Put string. Truncate last byte to 0 or fill remain with 0.
         * So reserve additional byte for your string.
         *
         */
        public int putString(String str, int len) {
            if (dataLen > eventmaxdataLen - len) {
                return -1;
            }
            synchronized(this) {
                byte s[] = str.getBytes();
                if (len < str.length()) {
                    System.arraycopy(s, 0, data, dataLen, len);
                    dataLen += len;
                    data[dataLen-1] = 0;
                } else {
                    System.arraycopy(s, 0, data, dataLen, s.length);
                    Arrays.fill(data, dataLen+s.length, dataLen+len, (byte)0);
                    dataLen += len;
                }
            }
            return 0;
        }

        /**
         * Put n bytes. Only fill remain with 0.
         * User need to set last byte to 0.
         * Int/Double need to be in LITTLE_ENDIAN(host)
         *
         */
        public int putNBytes(byte[] value, int len) {
            if (dataLen > eventmaxdataLen - len) {
                return -1;
            }
            synchronized(this) {
                if (len < value.length) {
                    System.arraycopy(value, 0, data, dataLen, len);
                    dataLen += len;
                } else {
                    System.arraycopy(value, 0, data, dataLen, value.length);
                    Arrays.fill(data, dataLen+value.length, dataLen+len, (byte)0);
                    dataLen += len;
                }
            }
            return 0;
        }

        public int putBytes(byte[] value) {
            if (value.length > eventmaxdataLen) {
                return -1;
            }

            synchronized(this) {
                System.arraycopy(value, 0, data, dataLen, value.length);
                dataLen += value.length;
            }
            return 0;
        }

        public byte[] getData() {
            return data;
        }

        public int getDataLen() {
            return dataLen;
        }

        public int getRequestId() {
            return requestId;
        }

        public int getInt() {
            int ret = 0;
            synchronized(this) {
                ret = ((data[readOffset + 3] & 0xff) << 24 |
                    (data[readOffset + 2] & 0xff) << 16 |
                    (data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 4;
            }
            return ret;
        }

        public int getShort() {
            int ret = 0;
            synchronized(this) {
                ret = ((data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 2;
            }
            return ret;
        }

        /**
         * Notice: getByte is to get int8 type from VA, not get one byte.
         */
        public int getByte() {
            int ret = 0;
            synchronized(this) {
                ret = (data[readOffset] & 0xff);
                readOffset += 1;
            }
            return ret;
        }

        public byte[] getBytes(int length) {
            if (length > dataLen - readOffset) {
                return null;
            }
            byte[] ret = new byte[length];
            synchronized(this) {
                for (int i = 0; i < length; i++) {
                    ret[i] = data[readOffset];
                    readOffset++;
                }
                return ret;
            }
        }

        public String getString(int len) {
            byte buf[] = new byte[len];

            synchronized(this) {
                System.arraycopy(data, readOffset, buf, 0, len);
                readOffset += len;
            }

            // to fix byte array including C string end '0x00'
            int index = 0;
            while (index < len) {
               if (buf[index] == 0x00 ||
                       buf[index] == '\0') {
                   break;
               }
               index++;
            }//while

            try {
               return (new String(buf, 0, index, "US-ASCII"));
            } catch (UnsupportedEncodingException e) {
               return (new String(buf)).trim();
            }
        }
    }

    private ImsConnectionStateListener mImsRegListener = new ImsConnectionStateListener() {
        @Override
        public void onFeatureCapabilityChanged(int serviceClass,
            int[] enabledFeatures, int[] disabledFeatures) {
            mIsWfc = (enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI] ==
                ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI);
            logger.debug("onFeatureCapabilityChanged mIsWfc = " + mIsWfc);
        }
        @Override
        public void onImsConnected(int imsRadioTech) {
            logger.debug("onImsConnected imsRadioTech= " + imsRadioTech);
            if (isServiceStarted()) {
                logger.debug("Rcs adapter already running");
                return;
            }
            connectRcsUaProxy();
        }
        @Override
        public void onImsDisconnected(ImsReasonInfo imsReasonInfo) {
            logger.debug("onImsDisconnected imsReasonInfo= " + imsReasonInfo);
        }
    };

    public static RcsUaAdapter getInstance() {
        if (mInstance == null) {
            mInstance = new RcsUaAdapter();
        }
        return mInstance;
    }

    private RcsUaAdapter() {

        mContext = AndroidFactory.getApplicationContext();
        mEventQueue = new RCSProxyEventQueue(this);
        mRCSUAEventDispatcher = new RcsUaEventDispatcher(this);
        mImsManager = ImsManager.getInstance(mContext, getMainCapabilityPhoneId());

        /* Initialize RIL HILD */
        //mRadioIndication = new RcsRadioIndication();
        //mRadioResponse = new RcsRadioResponse(this, instanceId);
    }

    public void initialize() {

        logger.debug("initialize");

        if (!isServiceStarted()) {
            connectRcsUaProxy();
            try {
                mImsManager.addRegistrationListener(ImsServiceClass.MMTEL, mImsRegListener);
            } catch (ImsException e) {
                logger.debug("addRegistrationListener: " + e);
            }
        } else {
            logger.debug("already initialized, skip it");
        }
    }

    public void terminate() {

        logger.debug("terminate");

        if (isServiceStarted()) {
            disableRcsUaAdapter();
        } else {
            logger.debug("already terminated, skip it!");
        }

    }

    public boolean isServiceStarted() {
        return isServiceStarted;
    }

    public void setServiceStatus(boolean status) {
        isServiceStarted = status;
    }

    public void disableRcsUaAdapter() {

        logger.debug("disableRcsUaAdapter");

        if (mIsRCSUAAdapterEnabled) {
            mIsRCSUAAdapterEnabled = false;
            mRCSUAEventDispatcher.disableRequest();

            /* We still need to process events from RCS UA Proxy
               event RCS service disabled. It's for DIGITS feature */
            //mEventQueue.stopEventQueuePolling();
        }

        cleanImsRegInfo();
        //set the rcs adapter service as false
        setServiceStatus(false);
        setRcsRegStatus(false);
    }

    public void connectRcsUaProxy() {

        logger.debug("connectRcsUaProxy");

        //set the service status to true
        setServiceStatus(true);

        for (int count = 0; count < 10; count++) {
            try {
                logger.debug("trying get rcs_hal_service...(" + count + ")");
                mRcsHal = IRcs.getService("rcs_hal_service");
                if (mRcsHal != null) {

                    /* linkToDeath */
                    mRcsHal.linkToDeath(mIRcsDeathRecipient,
                        mRcsHalCookie.incrementAndGet());

                    /* setResponseFunctions */
                    mRcsHal.setResponseFunctions(mRcsIndication);

                    /* start domain event dispatcher to recieve broadcast */
                    initSetup();
                    break;
                } else {
                    logger.debug("getService fail");
                    setServiceStatus(false);
                    return;
                }
            } catch (RemoteException | RuntimeException e) {
                mRcsHal = null;
                logger.debug("enable Rcs hal error: " + e);
            }
            SystemClock.sleep(1000 * (count + 1));
        }
        if (mRcsHal == null) {
            setServiceStatus(false);
        } else {
            mEventQueue.startEventQueuePolling();
        }
    }

    private void initSetup() {
        logger.debug("initSetup");
        mRCSUAEventDispatcher.enableRequest();
        mIsRCSUAAdapterEnabled = true;
        sendRegistrationInfoRequest();
        sendSimOperator();
    }

    /**
     * Gets the SIP event dispatcher.
     *
     * @return the SIP event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getSIPEventDispatcher() {
        return mRCSUAEventDispatcher.getSipEventDispatcher();
    }

    /**
     * Gets the registration event dispatcher.
     *
     * @return the registration event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getRegistrationEventDispatcher() {
        return mRCSUAEventDispatcher.getRegistrationEventHandler();
    }

    /**
     * Checks if is registered.
     *
     * @return true, if is registered
     */
    public boolean isRcsRegistered() {
        //logger.debug("isRegistered : " + mRcsRegistered);
        return mRcsRegistered;
    }

    public void setRcsRegStatus(boolean status) {
        mRcsRegistered = status;
        logger.debug("setRcsRegStatus : " + mRcsRegistered);
    }

    /**
     * Sets the registration status.
     *
     * @param status the new registration status
     */
    protected void setRegistrationStatus(boolean status) {
        //mRcsRegistered = status;
        logger.debug("[X] setRegistrationStatus : " + mRcsRegistered);
    }

    /**
     * Write event to socket.
     *
     * @param event the event
     */
    void writeEvent(RcsUaEvent event) {
        if (event != null) {
            mEventQueue.addEvent(RCSProxyEventQueue.OUTGOING_EVENT_MSG, event);
        }
    }

    void writeIncomingEvent(RcsUaEvent event) {
        if (event != null) {
            mEventQueue.addEvent(RCSProxyEventQueue.INCOMING_EVENT_MSG, event);
        }
    }

    /**
     * Send request to RCS_proxy to get the current info about the registration.
     */
    void sendRegistrationInfoRequest() {
        logger.debug("sendRegistrationInfoRequest");
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(CMD_REQ_REG_INFO);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * Send register request.
     */
    private void sendRegisterRequest() {
        logger.debug("sendRegisterRequest with Feature tag :  " +
            getRCSFeatureTag());
        String rcsCapabilityFeatureTags = getRCSFeatureTag();
        int length = rcsCapabilityFeatureTags.length();
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_REGISTER);
        event.putString(rcsCapabilityFeatureTags, length + 1);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * Update ims volte setting.
     */
    public void sendImsVolteConfig() {

        if (DEBUG_TRACE) {
            logger.debug("sendImsVolteConfig");
        }

        ImsVolteConfig mtkImsCfg = new ImsVolteConfig(mContext);
        mtkImsCfg.doAssignParam();
        //logger.debug(mtkImsCfg.toString01());
        //logger.debug(mtkImsCfg.toString02());

        // refer to ImsVolteConfig.java, remove runtime configuration
        int totalSettings = 110-12;
        byte[] byteParam = null;

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_UPDATE_SETTING);
        event.putInt(0); // account id
        event.putInt(0); // total data length, reserved field
        event.putInt(totalSettings); // total setting amounts

        /* TLV format, Tag refer to RcsEventSettingTLV.java */
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_System_Operator_ID);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_system_operator_id).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);
        //logger.debug("sendImsVolteConfig() Tag=" + RcsEventSettingTLV.VoLTE_Setting_System_Operator_ID);
        //logger.debug("sendImsVolteConfig() Len=" + byteParam.length);
        //logger.debug("sendImsVolteConfig() Value(decimal)=" + mtkImsCfg.mParam_system_operator_id);
        //logger.debug("sendImsVolteConfig() Value=" + bytesToHex(byteParam));

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Address);
        //byteParam = (mtkImsCfg.mParam_net_local_address+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);
        //logger.debug("sendImsVolteConfig() Tag=" + RcsEventSettingTLV.VoLTE_Setting_Net_Local_Address);
        //logger.debug("sendImsVolteConfig() Len=" + byteParam.length);
        //logger.debug("sendImsVolteConfig() Value(string)=" + mtkImsCfg.mParam_net_local_address);
        //logger.debug("sendImsVolteConfig() Value=" + bytesToHex(byteParam));

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Port);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_local_port).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Protocol_Type);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_local_protocol_type).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Protocol_Version);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_local_protocol_version).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_IPSec_Port_Start);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_local_ipsec_port_start).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_IPSec_Port_Range);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_local_ipsec_port_range).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_IPSec);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_ipsec).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_IF_Name);
        //byteParam = (mtkImsCfg.mParam_net_if_name+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Network_Id);
        //byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_network_id).array();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_SIP_Dscp);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_sip_dscp).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_SIP_Soc_Priority);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_sip_soc_priority).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_SIP_Soc_Tcp_Mss);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_sip_soc_tcp_mss).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_PCSCF_Port);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_pcscf_port).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_PCSCF_Number);
        //byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_net_pcscf_number).array();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_Private_UID);
        //byteParam = (mtkImsCfg.mParam_account_private_uid+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_Home_URI);
        //byteParam = (mtkImsCfg.mParam_account_home_uri+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_IMEI);
        //byteParam = (mtkImsCfg.mParam_account_imei+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Server_PCSCF_List);
        //byteParam = (mtkImsCfg.mParam_server_pcscf_list+'\0').getBytes();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_VoLTE_Call_UserAgent);
        byteParam = (mtkImsCfg.mParam_volte_call_useragent+'\0').getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Register_Expiry);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_register_expiry).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Contact_With_UserName);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_username).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_URI_With_Port);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_uri_with_port).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_IPSec_Algo_Set);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_ipsec_algo_set).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Enable_HTTP_Digest);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_enable_http_digest).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Auth_Name);
        //byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_auth_name).array();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Auth_Password);
        //byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_auth_password).array();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Specific_IPSec_Algo);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_specific_ipsec_algo).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_TRANSPORT);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_transport).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_REGID);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_regid).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_MOBILITY);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_mobility).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_EXPIRES);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_expires).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_AUTHORIZATION_WITH_ALGO);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_authorization_with_algo).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REREG_IN_RAT_CHANGE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_rereg_in_rat_change).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REREG_IN_OOS_END);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_rereg_in_oos_end).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_DE_SUBSCRIBE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_de_subscribe).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_USE_SPECIFIC_IPSEC_ALGO);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_use_specific_ipsec_algo).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TRY_NEXT_PCSCF);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_try_next_pcscf).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_DEREG_CLEAR_IPSEC);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_dereg_clear_ipsec).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_INITIAL_REG_WITHOUT_PANI);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_initial_reg_without_pani).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_DEREG_RESET_TCP_CLIENT);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_dereg_reset_tcp_client).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TREG);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_treg).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REREG_23G4);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_rereg_23g4).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_RESUB_23G4);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_resub_23g4).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_NOT_AUTO_REG_403);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_not_auto_reg_403).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CALL_ID_WITH_HOST_INREG);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_call_id_with_host_inreg).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_KEEP_ALIVE_MODE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_keep_alive_mode).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TCP_CONNECT_MAX_TIME_INVITE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_tcp_connect_max_time_invite).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_EMS_MODE_IND);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_ems_mode_ind).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_ACCESSTYPE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_accesstype).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_WFC_WITH_PLANI);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_wfc_with_plani).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_USE_UDP_ON_TCP_FAIL);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_use_udp_on_tcp_fail).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_IPSEC_FAIL_ALLOWED);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_ipsec_fail_allowed).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CONTACT_WITH_VIDEO_FEATURE_TAG_IN_SUBSCRIBE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_contact_with_video_feature_tag_in_subscribe).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_VIA_WITHOUT_RPORT);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_via_without_rport).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_ROUTE_HDR);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_reg_route_hdr).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_VIA_URI_WITH_DEFAULT_PORT);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_via_uri_with_default_port).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_NOTIFY_SMS_NOTIFY_DONE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_notify_sms_notify_done).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_EMERGENCY_USE_IMSI);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_emergency_use_imsi).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CHECK_MSISDN);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_check_msisdn).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_RETRY_INTERVAL_AFTER_403);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_retry_interval_after_403).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_SUPPORT_THROTTLING_ALGO);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_support_throttling_algo).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_AFTER_NW_DEREG_60S);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_reg_after_nw_dereg_60s).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_SUB_CONTACT_WITH_SIP_INSTANCE);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_sub_contact_with_sip_instance).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_STOP_REG_MD_LOWER_LAYER_ERR);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_stop_reg_md_lower_layer_err).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_GRUU_SUPPORT);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_reg_gruu_support).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_oos_end_reset_tcp_client);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_oos_end_reset_tcp_client).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_pidf_country);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_pidf_country).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_A_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_a_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_B_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_b_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_C_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_c_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_D_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_d_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_E_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_e_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_G_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_g_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_H_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_h_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_I_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_i_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_J_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_j_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_K_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_k_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_FAIL_NOT_NEED_REMOVE_BINDING);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_fail_not_need_remove_binding).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        // this should be configured dynamically
        /*
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_RCS_State);
        byteParam = (mtkImsCfg.mParam_reg_reg_rcs_state+'\0').getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);
        */

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_PEND_DEREG_IN_INITIAL_REG);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_pend_dereg_in_initial_reg).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TRY_NEXT_PCSCF_5626);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_try_next_pcscf_5626).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TRY_SAME_PCSCF_REREG);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_try_same_pcscf_rereg).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        //event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_PUBLIC_UIDS);
        //byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_public_uids).array();
        //event.putInt(byteParam.length);
        //event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_TRY_ALL_PCSCF_5626);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_try_all_pcscf_5626).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_Tdelay);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_reg_tdelay).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Try_N_Next_Pcscf_5626);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_try_n_next_pcscf_5626).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Dereg_Delay_Time);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_dereg_delay_time).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CHECK_NOTIFY_INSTANCE_ID);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_check_notify_instance_id).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_ADD_CS_VOLTE_FEATURE_TAG);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_add_cs_volte_feature_tag).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_CHECK_CALL_DOMAIN);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_check_call_domain).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_SUPPORT_SIP_BLOCK);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_support_sip_block).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_WHEN_STOP_B_TIMER);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_when_stop_b_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_support_resub);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_support_resub).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Expire_Time_By_User);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_expire_time_by_user).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Add_CNI_in_WIFI);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_add_cni_in_wifi).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Reg_Over_Tcp);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_reg_over_tcp).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Add_cell_info_age_to_cni);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_add_cell_info_age_to_cni).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_check_reg_contact);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_check_reg_contact).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_REG_Geolocation_Type);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_geolocation_type).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Init_Reg_Delay_Time);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_init_reg_delay_time).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_Update_IPSec_Port_494);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_reg_update_ipsec_port_494).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_T1_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_t1_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_T2_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_t2_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_T4_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_t4_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_Keep_Alive);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_ua_reg_keep_alive).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_Force_Use_UDP);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_force_use_udp).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_TCP_On_Demand);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_tcp_on_demand).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_TCP_MTU_Size);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_tcp_mtu_size).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_SIP_Transaction_Timer);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(mtkImsCfg.mParam_sip_transaction_timer).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        sendMsgToRCSUAProxy(event);
    }

    /**
     * handle the registration info send by rcs_proxy and save it based on state.
     *
     * @param event the event
     */
    public void handleRegistrationInfo(RcsUaEvent event) {

        int owner;
        int protocolVersion;
        String publicUid;
        int pcscfPort;

        // refer _RCS_PROXY_Event_Reg_State2_ structure

        owner = event.getInt();  // 0: VoLTE, 1: ROI
        mRegState = event.getInt();
        mRcsState = event.getInt();
        mRcsTag = event.getInt();
        mLocalAddress = event.getString(64);
        mLocalPort = event.getInt(); // 76
        mProtocolType = (event.getInt()==1)? "TCP" : "UDP"; // TCP=1,UDP=2,TCPUDP=3
        protocolVersion = event.getInt(); //IPv4=1,IPv6=2
        publicUid = event.getString(256);
        mPcscfAddress = event.getString(256);
        pcscfPort = event.getInt();
        mUserAgent = event.getString(128);
        mAssociatedUri = event.getString(512);
        mInstanceId = event.getString(128);
        mRoute = event.getString(128);
        mPANI = event.getString(256);
        mVirtualLineCount = event.getInt();

        logger.debug("handleRegistrationInfo - owner: " + owner + ", reg: "
                + mRegState + ", rcs: " + mRcsState + ", tag: " + mRcsTag
                + "mRcsRegistered: " + mRcsRegistered);

        /* Ignore ROI event when RegCore not get chance to be initialized */
        if (RegCore.getInstance() == null && owner == RegCore.REG_OWNER_RCS)
            return;

        if (mUserAgent == null || mUserAgent.length() < 2) {
            mUserAgent = "Default-RCS-UA";
        }

        /*
        logger.debug("local address: " + mLocalAddress);
        logger.debug("local port: " + mLocalPort);
        logger.debug("protocol type: " + protocolType);
        logger.debug("protocol version: " + protocolVersion);
        logger.debug("publicUid: " + publicUid);
        logger.debug("pcscfAddress: " + mPcscfAddress);
        logger.debug("pcscfPort: " + pcscfPort);
        logger.debug("userAgent: " + mUserAgent);
        logger.debug("associated Uri: " + mAssociatedUri);
        logger.debug("Instance id:" + mInstanceId);
        logger.debug("Route: " + mRoute);
        logger.debug("PANI: " + mPANI);
        */

        mAssociatedUris = mAssociatedUri.split(",");
        mRoutes = mRoute.split(",");

        setAccessNetworkInfo();

        if (mRegState == IMS_REG_STATE_DISCONNECTED) {
            cleanImsRegInfo();
        }

        if (RegCore.getInstance() != null) {
            boolean volteActive = RegCore.getInstance().isVolteActive();
            boolean rcsActive = RegCore.getInstance().isRcsActive();

            RegCore.getInstance().processRegistration(
                    new RegCore.InputEvent.Builder(RegCore.InputType.IMS_REGINFO_IND)
                            .setImsOwner(owner)
                            .setRegStatus(mRegState)
                            .build());
            if (owner == RegCore.REG_OWNER_RCS) {
                if (mRegState == IMS_REG_STATE_DISCONNECTED
                        && isRcsRegistered() && rcsActive) {
                    setRcsRegStatus(false);
                    Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
                    intent.putExtra("owner", owner);
                    intent.putExtra("reason", RegCore.REASON_DO_DEREG);
                    mContext.sendBroadcast(intent);
                } else
                 if (mRegState == IMS_REG_STATE_REGISTERED) {
                    Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
                    intent.putExtra("owner", owner);
                    intent.putExtra("reason", RegCore.REASON_REG_DONE);
                    mContext.sendBroadcast(intent);
                }
            } else
            if (owner == RegCore.REG_OWNER_VOLTE) {
                if (mRegState == IMS_REG_STATE_DISCONNECTED
                        && isRcsRegistered() && volteActive) {
                    Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
                    intent.putExtra("owner", owner);
                    intent.putExtra("rcs_state", isRcsRegistered());
                    intent.putExtra("reg_state", mRegState);
                    mContext.sendBroadcast(intent);
                } else
                if (mRegState == IMS_REG_STATE_REGISTERED) {
                    Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
                    intent.putExtra("owner", owner);
                    intent.putExtra("rcs_state", isRcsRegistered());
                    intent.putExtra("reg_state", mRegState);
                    mContext.sendBroadcast(intent);
                }
            }
        } else
        if (owner == RegCore.REG_OWNER_VOLTE) {
            if ((!isRcsRegistered() && mRegState == IMS_REG_STATE_REGISTERED) ||
                (isRcsRegistered() && mRegState == IMS_REG_STATE_DISCONNECTED)) {

                //hack to avoid multiple broadcast issue
                Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
                intent.putExtra("owner", owner);
                intent.putExtra("rcs_state", isRcsRegistered());
                intent.putExtra("reg_state", mRegState);
                mContext.sendBroadcast(intent);
            }
        }

        mLastRegstate = mRegState;
    }

    public void setAccessNetworkInfo() {

        try {
            String lastAccessNetworkInfo = ImsModule.IMS_USER_PROFILE.getAccessNetworkInfo();
            String currentAccessNetworkInfo = mPANI;

            // if the last access network information is null or Wifi MAC,we don't save it to database.
            if (!TextUtils.isEmpty(lastAccessNetworkInfo) &&
                !lastAccessNetworkInfo.startsWith(ACCESS_TYPE_PRELUDE)) {

                RcsSettings.getInstance().writeParameter(RcsSettingsData.LAST_ACCESS_NETWORKINFO,
                       lastAccessNetworkInfo);
            }

            ImsModule.IMS_USER_PROFILE.setAccessNetworkInfo(currentAccessNetworkInfo);
            RcsSettings.getInstance().writeParameter(RcsSettingsData.CURRENT_ACCESS_NETWORKINFO,
                    currentAccessNetworkInfo);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send msg to dispatcher.
     *
     * @param event the event
     */
    protected void sendMsgToDispatcher(RcsUaEvent event) {
        Message msg = new Message();
        msg.obj = event;
        mRCSUAEventDispatcher.sendMessage(msg);
    }

    /**
     * Send msg to rcsua proxy.
     *
     * @param event the event
     */
    protected void sendMsgToRCSUAProxy(RcsUaEvent event) {

        if (mRcsHal == null) {
            logger.debug("sendMsgToRCSUAProxy failed, mRcsHal is null!: " + event.getRequestId());
            return;
        }

        logger.debug( "event to proxy: " + getCmdName(event.getRequestId()));

        // send the event to UA
        int requestId = event.getRequestId();
        int length = event.getDataLen();
        try {
            mRcsHal.writeEvent(requestId, length, byteToArrayList(length, event.getData()));
        } catch (RemoteException e) {
            logger.debug("hal writeEvent e: " + e);
        }
    }

    // [TBD] need to refactor for Feature Tag implementation
    protected String getRCSFeatureTag() {
        String data = "";
        //data = FeatureTags.FEATURE_RCSE;
        data = FeatureTags.FEATURE_CPM_SESSION + "," + FeatureTags.FEATURE_CPM_FT + "," + FeatureTags.FEATURE_CPM_MSG;
        return data;
    }
    public String[] getImsProxyAddrForVoLTE() {
        String data[] = {mPcscfAddress};
        data[0] = mPcscfAddress;
        return data;
    }
    public int[] getImsProxyPortForVoLTE() {
        int data[] = {mLocalPort};
        return data;
    }
    public String getLocalAddress() {
        return mLocalAddress;
    }
    public String getUserAgent() {
        return mUserAgent;
    }
    public String getSIPDefaultProtocolForVoLTE() {
        String data = RcsSettings.getInstance().getSipDefaultProtocolForMobile();
        //[TBD] need to remove hardcode?
        data = "TCP"; //String data = mProtocolType;
        return data;
    }
    public void setRCSSipStackPort(int port) {
        mRcsSipStackPort = port;
    }
    public int getRCSSipStackPort() {
        return mRcsSipStackPort;
    }
    public String[] getmAssociatedUri() {
        return mAssociatedUris;
    }
    public String getmInstanceId() {
        return mInstanceId;
    }
    public String[] getmRoute() {
        return mRoutes;
    }

    public int getVirtualLineCount() {
        return mVirtualLineCount;
    }

    public int getRegState() {
        return mRegState;
    }

    /*
     *  RCS can do single registration only when:
     *  1) Platform has RCS UA Proxy
     *  2) Operators support single registration
     */
    public boolean isSingleRegistrationSupported() {
        boolean status = false;

        int type = RcsSettings.getInstance().getRcsVolteSingleRegistrationType();

        //logger.debug("getRcsVolteSingleRegistrationType:  " + type);

        if (type == RcsSettingsData.RCS_VOLTE_SINGLE_REGISTRATION_MODE) {
            status = true;
        } else if (type == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_ROAMING_MODE) {
            // to-do
        } else if (type == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_MODE) {
            // to-do
        }
        return status;
    }

    void cleanImsRegInfo() {

        logger.debug("cleanImsRegInfo");

        mPcscfAddress = "";
        mLocalPort = 0;
        mProtocolType = "UDP";
        mLocalAddress = "";
        mUserAgent = "";
        mRcsSipStackPort = 0;
    }

    public boolean isRegisteredbyVoLTEStack() {
        if ((mRcsState > 0) && isSingleRegistrationSupported() && (mRegState ==1)) {
            return true;
        } else {
            return false;
        }
    }

    public void setRegisteredbyVolte(int value) {
        mRcsState = value;
    }

    private ArrayList < Byte > byteToArrayList(int length, byte[] value) {
        ArrayList < Byte > al = new ArrayList < Byte > ();
        //logger.debug("byteToArrayList, value.length = " + value.length + ", length = " + length);
        for (int i = 0; i < length; i++) {
            al.add(value[i]);
        }
        return al;
    }

    final class IRcsDeathRecipient implements HwBinder.DeathRecipient {
        @Override
        public void serviceDied(long cookie) {
            // Deal with service going away
            logger.debug("Rcs hal serviceDied");
            // reconnect to hidl server directly
            // let the for loop take care UA reborn timing
            connectRcsUaProxy();
        }
    }

    final class RadioProxyDeathRecipient implements HwBinder.DeathRecipient {
        @Override
        public void serviceDied(long cookie) {}
    }

    public class RcsIndication extends IRcsIndication.Stub {
        private byte[] arrayListTobyte(ArrayList < Byte > data, int length) {
            byte[] byteList = new byte[length];
            for (int i = 0; i < length; i++) {
                byteList[i] = data.get(i);
            }
            //logger.debug("arrayListTobyte, byteList = " + byteList);
            return byteList;
        }

        public void readEvent(ArrayList < Byte > data, int request_id, int length) {
            if (logger.isActivated()) {
                logger.debug("event from proxy: " + getCmdName(request_id));
            }

            byte buf[];
            RcsUaEvent event;

            buf = arrayListTobyte(data, length);

            event = new RcsUaEvent(request_id);
            event.putBytes(buf);

            if (event != null) {
                mEventQueue.addEvent(RCSProxyEventQueue.INCOMING_EVENT_MSG, event);
            }
        }
    }

    private static int getMainCapabilityPhoneId() {
        int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
        return phoneId;
    }

    public void sendSimOperator() {

        logger.debug("sendSimOperator");

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_SIM_OPERATOR);

        sendMsgToRCSUAProxy(event);
    }

    public void sendServiceActivationState(int state) {

        logger.debug("sendServiceActivationState, state = " + state);

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_SERVICE_ACTIVATION_STATE);

        event.putInt(state);

        sendMsgToRCSUAProxy(event);
    }

    // RCS for 3g volte
    // VOLTE_REQ_SIPTX_REG_NOTIFY_IMSVOPS
    // Roi_Event_Notify_t
    public void sendRegNotifyImsvops(int acct_id, int data) {

        if (DEBUG_TRACE) {
            logger.debug("sendRegNotifyImsvops with id:" + acct_id + "data:" + data);
        }
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_NOTIFY_VOPS_INFO);
        event.putInt(acct_id);
        event.putInt(data);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_UPDATE_SETTING
    // Roi_Event_Setting_t
    public void sendRegUpdateSettings(int acct_id,
        int num_settings, ArrayList < RcsEventSettingTLV > settings_data) {

        if (DEBUG_TRACE) {
            logger.debug("sendRegUpdateSettings with id:" + acct_id + "num_settings:" + num_settings);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_UPDATE_SETTING);
        event.putInt(acct_id);
        int total_data_len = 0; //Will be re-calculated at lower layer. For sturcure complete.
        event.putInt(total_data_len);
        event.putInt(num_settings);
        for (int i = 0; i < num_settings; i++) {
            RcsEventSettingTLV item = settings_data.get(i);
            logger.debug("item with[" + i + "] type:" + item.mType + ",data_len:" +
                item.mDatalen + ",data:" + Arrays.toString(item.mData));
            event.putInt(item.mType);
            event.putInt(item.mDatalen);
            event.putNBytes(item.mData, item.mDatalen);
        }
        sendMsgToRCSUAProxy(event);
    }

    public void sendRegAddCapabilties() {
        updateRegAddCapability(true, true);
    }

    public void sendRegRemoveCapabilties() {
        updateRegAddCapability(false, true);
    }

    //VOLTE_REQ_SIPTX_SETTING
    //VoLTE_Event_Setting_t
    //volte_sip_stack_update_setting
    public void updateRegAddCapability(boolean isRcsSrvEnabled, boolean isATCmdNeed) {

        /******************************************************************
         *  [Important] Need to change according to "exact" feature tags  *
         *  Purpose: notify info to rcs_volte_stack                       *
         *  parameter: <rcs state>_<feature tag>                          *
         *  <rcs_state>: 0/1                                              *
         *  <feature tag>: hex value, use bit-map                         *
         *         0x01  session                                          *
         *         0x02  filetransfer                                     *
         *         0x04  msg                                              *
         *         0x08  largemsg                                         *
         *         0x10  geopush                                          *
         *         0x20  geopull                                          *
         *         0x40  geopullft                                        *
         *         0x80  imdn aggregation                                 *
         *        0x100  geosms                                           *
         *        0x200  fthttp                                           *
         *         0x00  RCS service tag was removed                      *
         ******************************************************************/
        String strRcsSrvTag = SystemProperties.get("persist.vendor.service.tag.rcs");
        int intRcsSrvTag = 0;
        try {
            if (strRcsSrvTag != null) {
                intRcsSrvTag = Integer.parseInt(strRcsSrvTag);
            }
        } catch (NumberFormatException e) {
            //logger.error("updateRegAddCapability(): bad strRcsSrvTag=" + strRcsSrvTag);
            intRcsSrvTag = 0;
        }
        String strRcsCustomized = SystemProperties.get("persist.vendor.customized.rcs");
        int intRcsCustomized = 0;
        try {
            if (strRcsCustomized != null) {
                intRcsCustomized = Integer.parseInt(strRcsCustomized);
            }
        } catch (NumberFormatException e) {
            //logger.error("updateRegAddCapability(): bad strRcsCustomized=" + strRcsCustomized);
            intRcsCustomized = 0;
        }

        // error handle
        if (intRcsSrvTag <= 0x00 || intRcsSrvTag > 0xFFFF) {
            if (intRcsCustomized == 0) {
                // MTK internal: session + filetransfer + msg + largemsg
                intRcsSrvTag = 0x0F;
            } else {
                // Customer default tags: session + filetransfer + geopush
                intRcsSrvTag = 0x13;
            }
        }

        // notify volte_stack
        String strATCmd = "AT+EIMSRCS=";
        String state_capabiliity = null;
        if (isRcsSrvEnabled == true) {
            strATCmd = strATCmd + "1," + intRcsSrvTag;
            state_capabiliity = "1_" + Integer.toHexString(intRcsSrvTag);
        } else {
            strATCmd = strATCmd + "0," + intRcsSrvTag;
            state_capabiliity = "0_" + Integer.toHexString(intRcsSrvTag);
        }
        if (isATCmdNeed == true) {
            RcsSettings.getInstance().sendAtCommand(strATCmd);
        }
        logger.debug("updateRegAddCapability()" +
            ": isATCmdNeed=" + isATCmdNeed +
            ", intRcsSrvTag=" + intRcsSrvTag +
            ", intRcsCustomized=" + intRcsCustomized +
            ", strATCmd:-->" + strATCmd + "<--" +
            ", state_capabiliity:-->" + state_capabiliity + "<--");

        // notify volte_rcs_stack: only OP08 needed
        if (!RcsSettings.getInstance().isOP08SupportedByPlatform()) {
            return;
        }

        byte[] byteParam = null;

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
           CMD_IMS_UPDATE_SETTING);
        event.putInt(0); // account id
        event.putInt(0); // total data length, reserved field
        event.putInt(1); // total setting amounts

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Reg_REG_RCS_State);
        byteParam = (state_capabiliity + '\0').getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_3GPP_RAT_CHANGE
    // Roi_Event_3gpp_Rat_Change_t
    public void sendReg3gppRatChange(int acct_id, int old_rat_type,
        String old_cell_id, int new_rat_type, String new_cell_id,
        double lat, double lng, int acc, String timestamp) {

        if (DEBUG_TRACE) {
            logger.debug("sendReg3gppRatChang with acct_id:" + acct_id +
                ",old_rat_type:" + old_rat_type +
                ",old_cell_id:" + old_cell_id +
                ",new_rat_type:" + new_rat_type +
                ",new_cell_id:" + new_cell_id +
                ",lat:" + lat +
                ",lng:" + lng +
                ",acc:" + acc +
                ",timestamp:" + timestamp);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_NOTIFY_3GPP_RAT_CHANGE);

        event.putInt(acct_id);
        event.putInt(old_rat_type);
        event.putString(old_cell_id, VOLTE_MAX_CELL_ID_LENGTH);
        event.putInt(new_rat_type);
        event.putString(new_cell_id, VOLTE_MAX_CELL_ID_LENGTH);
        //Roi_Event_LBS_Location_t
        event.putDouble(lat);
        event.putDouble(lng);
        event.putInt(acc);
        event.putString(timestamp, VOLTE_MAX_TIME_STAMP_LENGTH);
        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_RAT_CHANGE
    // Roi_Event_Rat_Change_t
    public void sendRegRatChange(int acct_id, int type, String cell_id,
        double lat, double lng, int acc, String timestamp, String ssid) {

        if (DEBUG_TRACE) {
            logger.debug("sendRegRatChange with acct_id:" + acct_id +
                "type:" + type +
                ",cell_id:" + cell_id +
                ",lat:" + lat +
                ",lng:" + lng +
                ",acc:" + acc +
                ",timestamp:" + timestamp +
                ",ssid:" + ssid);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_NOTIFY_RAT_CHANGE);
        event.putInt(acct_id);
        event.putInt(type);
        event.putString(cell_id, VOLTE_MAX_CELL_ID_LENGTH);
        //Roi_Event_LBS_Location_t
        event.putDouble(lat);
        event.putDouble(lng);
        event.putInt(acc);
        event.putString(timestamp, VOLTE_MAX_TIME_STAMP_LENGTH);
        event.putString(ssid, VOLTE_MAX_SSID_LENGTH);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_NETWORK_CHANGE
    // Roi_Event_Network_Change_t
    public void sendRegNetworkChange(int acct_id, int state,
        int type, String cell_id,
        double lat, double lng, int acc, String timestamp) {

        if (DEBUG_TRACE) {
            logger.debug("sendRegNetworkChange with acct_id:" + acct_id +
                ",state:" + state +
                ",type:" + type +
                ",cell_id:" + cell_id +
                ",lat:" + lat +
                ",lng:" + lng +
                ",acc:" + acc +
                ",timestamp:" + timestamp);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_NOTIFY_NETWORK_CHANGE);

        event.putInt(acct_id);
        event.putInt(state);
        event.putInt(type);
        event.putString(cell_id, VOLTE_MAX_CELL_ID_LENGTH);
        //Roi_Event_LBS_Location_t
        event.putDouble(lat);
        event.putDouble(lng);
        event.putInt(acc);
        event.putString(timestamp, VOLTE_MAX_TIME_STAMP_LENGTH);
        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_CNF_SIPTX_REG_AUTH_REQ
    // Roi_Event_Reg_Auth_Resp_t
    public void sendReqAuthReqResult(int acct_id, int result,
        String nc, byte[] response, byte[] auts,
        String ck_esp, String ik_esp, int user_data) {

        if (DEBUG_TRACE) {
            logger.debug("sendReqAuthReq with acct_id:" + acct_id +
                ",result:" + result +
                ",nc:" + nc +
                ",response:" + response +
                ",auts:" + auts +
                ",ck_esp:" + ck_esp +
                ",ik_esp:" + ik_esp +
                ",user_data:" + user_data);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_AUTH_REQ_RESULT);
        event.putInt(acct_id);
        event.putInt(result);
        event.putString(nc, VOLTE_MAX_AUTH_NC);
        event.putNBytes(response, VOLTE_MAX_AUTH_RESPONSE);
        event.putNBytes(auts, VOLTE_MAX_AUTH_AUTS);
        event.putString(ck_esp, VOLTE_MAX_AUTH_CK);
        event.putString(ik_esp, VOLTE_MAX_AUTH_IK);
        event.putInt(user_data);
        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_CNF_SIPTX_GEOLOCATION_REQ
    // Roi_Event_Geolocation_Info_t
    public void sendGeolocationReqResult(int ua_msg_hdr_id, int ua_msg_hdr_hsk,
        byte[] ua_msg_hdr_resv, int type_id,
        double lat, double lng, int accuracy,
        String method, String city, String geo_state, String zip, String country_code) {

        if (DEBUG_TRACE) {
            logger.debug("sendGeolocationReq with ua_msg_hdr id:"
                    + ua_msg_hdr_id + ",hsk:" + ua_msg_hdr_hsk);
            logger.debug("sendGeolocationReq with type_id:" + type_id +
                ",lat:" + lat +
                ",lng:" + lng +
                ",accuracy:" + accuracy +
                ",method:" + method +
                ",city:" + city +
                ",geo_state:" + geo_state +
                ",zip:" + zip +
                ",country_code:" + country_code);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_GEOLOCATION_REQ_RESULT);
        //Roi_Event_UA_Msg_Hdr_t
        event.putInt(ua_msg_hdr_id);
        event.putByte(ua_msg_hdr_hsk);
        event.putNBytes(ua_msg_hdr_resv, 3);

        event.putInt(type_id);
        event.putDouble(lat);
        event.putDouble(lng);
        event.putInt(accuracy);
        event.putString(method, 16);
        event.putString(city, 32);
        event.putString(geo_state, 32);
        event.putString(zip, 8);
        event.putString(country_code, 8);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_UPDATE_SETTING
    // VoLTE_Event_Setting_t
    public void sendNetworkRtSettings(int acct_id, String networkInterface,
        int networkid, String localAddress, String[] pCSCFList,
        int numberOfPcscf, int protocolVersion, String cellId) {

        if (DEBUG_TRACE) {
            logger.debug("sendNetworkRtSettings with acct_id:" + acct_id
                + ",networkInterface:" + networkInterface
                + ",networkid:" + networkid
                + ",localAddress:" + localAddress
                + ",pCSCFList:" + Arrays.toString(pCSCFList)
                + ",numberOfPcscf:" + numberOfPcscf
                + ",protocolVersion:" + protocolVersion
                + ",cellId:" + cellId);
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_UPDATE_RT_SETTING);

        int totalSettings = 6;
        if (cellId.length() > 0)
            totalSettings++;

        byte[] byteParam = null;

        event.putInt(0); // account id
        event.putInt(0); // total data length, reserved field
        event.putInt(totalSettings); // total setting amounts

        networkInterface = networkInterface + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_IF_Name);
        byteParam = networkInterface.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Network_Id);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
                .putInt(networkid).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        localAddress = localAddress + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Address);
        byteParam = localAddress.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Local_Protocol_Version);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
                .putInt(protocolVersion).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        String result_pcscf_list = new String();
        int result_length = 0;
        for (String s:pCSCFList) {
            result_pcscf_list = result_pcscf_list + s + ",";
            result_length = result_length + s.getBytes().length + 1;
        }
        result_pcscf_list = result_pcscf_list + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Server_PCSCF_List);
        byteParam = result_pcscf_list.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_PCSCF_Number);
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN)
                .putInt(numberOfPcscf).array();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        if (cellId.length() > 0) {
            cellId = cellId + '\0';
            event.putInt(RcsEventSettingTLV.VoLTE_Setting_Net_Cell_ID);
            byteParam = cellId.getBytes();
            event.putInt(byteParam.length);
            event.putNBytes(byteParam, byteParam.length);
        }

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_UPDATE_SETTING
    // VoLTE_Event_Setting_t
    public void sendSimRtSettings(int acct_id, String imei, String home_uri,
        String priviate_uid, String[] public_uid) {

        if (DEBUG_TRACE) {
            logger.debug("sendSimRtSettings with acct_id:" + acct_id
                + ",imei:" + imei
                + ",home_uri:" + home_uri
                + ",priviate_uid:" + priviate_uid
                + ",public_uid:" + Arrays.toString(public_uid));
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                CMD_IMS_UPDATE_RT_SETTING);
        int totalSettings = 4;
        byte[] byteParam = null;

        event.putInt(0); // account id
        event.putInt(0); // total data length, reserved field
        event.putInt(totalSettings); // total setting amounts

        priviate_uid = priviate_uid + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_Private_UID);
        byteParam = priviate_uid.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        home_uri = home_uri + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_Home_URI);
        byteParam = home_uri.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        imei = imei + '\0';
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_IMEI);
        byteParam = imei.getBytes();
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        String result_public_uid = new String();
        int result_length = 0;
        if (public_uid != null) {
            for (String s:public_uid) {
                result_public_uid = result_public_uid + s + ",";
                result_length = result_length + s.getBytes().length + 1;
            }
            //Replace end ',' with ';'
            result_public_uid = result_public_uid.substring(0,result_public_uid.lastIndexOf(',')) + ';';
            result_public_uid = result_public_uid + '\0';
        }
        event.putInt(RcsEventSettingTLV.VoLTE_Setting_Account_Public_UID);
        byteParam = result_public_uid.getBytes();
        logger.debug("byteParam.length:" + byteParam.length +" = result_length:" + result_length);
        event.putInt(byteParam.length);
        event.putNBytes(byteParam, byteParam.length);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_DIGITLINE
    // VoLTE_Event_ATCmd_Relay_t
    public void sendRegDigitLine(int acct_id, int atcmd_id, int pad,
        int pad2, String cmdlnie) {

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                   CMD_IMS_REG_DIGITLINE);

        event.putInt(acct_id);
        event.putInt(atcmd_id);
        event.putInt(pad);
        event.putInt(pad2);
        event.putString(cmdlnie, VOLTE_MAX_AT_CMDLINE_LEN);

        sendMsgToRCSUAProxy(event);
    }

    // VOLTE_REQ_SIPTX_REG_DEREG
    public void sendRegDeregister() {
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                   CMD_IMS_DEREGISTER);
        sendMsgToRCSUAProxy(event);
    }

    public void sendRcsActivation() {
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                   CMD_RCS_ACTIVATION);
        sendMsgToRCSUAProxy(event);
    }

    public void sendRcsDeactivation() {
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                   CMD_RCS_DEACTIVATION);
        sendMsgToRCSUAProxy(event);
    }

    public void UtRcsSendTest() {
        logger.debug("UtRcsSendTest 10:34");
        byte[] byteParam = null;
        //C.K. UT code
        int acct_id = 0;
        int data = 16;
        String imei = "358509014497232";
        String home_uri = "www.google.com"+'0';
        String priviate_uid = "1000";
        String[] public_uid = {"1000","2000"};
        String method = "method1";
        String city = "city1";
        String geo_state = "geo_state1";
        String zip = "zip1";
        String country_code = "country_code1";
        int ua_msg_hdr_id = 1;
        int ua_msg_hdr_hsk = 2;
        int type_id = 3;
        double lat = 4.44;
        double lng = 5.55;
        int accuracy = 6;
        byte[] ua_msg_hdr_resv ={0x30, 0x40, 0x50};
        int result = 1;
        String nc = "nc1";
        String response = "response1";
        String auts = "auts1";
        String ck_esp = "ck_esp1";
        String ik_esp = "ik_esp1";
        int user_data = 1;
        int type = 1;
        int state = 2;
        String cell_id = "cell_id1";
        String new_cell_id = "new_cell_id1";
        String old_cell_id = "old_cell_id1";
        String timestamp = "timestamp1";
        String ssid = "ssid1";
        int acc = 2;
        int old_rat_type = 0;
        int new_rat_type = 1;
        int rat_type = 1;
        int enable = 1;
        String capability = "capability1";
        int num_settings = 2;
        ArrayList < RcsEventSettingTLV > settings_data = new ArrayList<>();
        byteParam = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(data).array();
        RcsEventSettingTLV tlv1 = new RcsEventSettingTLV(100, byteParam.length, byteParam);
        settings_data.add(tlv1);

        byteParam = home_uri.getBytes();
        RcsEventSettingTLV tlv2 = new RcsEventSettingTLV(200, byteParam.length, byteParam);
        settings_data.add(tlv2);

        sendRegNotifyImsvops(acct_id, data);
        sendRegUpdateSettings(acct_id,
            num_settings, settings_data);
        sendReg3gppRatChange(acct_id, old_rat_type,
            old_cell_id, new_rat_type, new_cell_id,
            lat, lng, acc, timestamp);
        sendRegRatChange(acct_id, type, cell_id,
            lat, lng, acc, timestamp, ssid);
        sendRegNetworkChange(acct_id, state,
            type, cell_id,
            lat, lng, acc, timestamp);
        /*sendReqAuthReqResult(acct_id, result,
            nc, response, auts,
            ck_esp, ik_esp, user_data);*/
        sendGeolocationReqResult(ua_msg_hdr_id, ua_msg_hdr_hsk,
            ua_msg_hdr_resv, type_id,
            lat, lng, accuracy,
            method, city, geo_state, zip, country_code);
        sendSimRtSettings(acct_id, imei, home_uri,
            priviate_uid, public_uid);
        //C.K. UT code end
    }

    final protected static char[] hexArray = "0123456789abcdef".toCharArray();

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];

        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }

        return new String(hexChars);
    }

    public String getCmdName(int requestId) {
        switch(requestId) {
            case CMD_REQ_REG_INFO:                        return "CMD_REQ_REG_INFO";
            case CMD_IMS_REGISTER:                        return "CMD_IMS_REGISTER";
            case CMD_IMS_DEREGISTER:                      return "CMD_IMS_DEREGISTER";
            case CMD_SEND_SIP_MSG:                        return "CMD_SEND_SIP_MSG";
            case CMD_IMS_NOTIFY_VOPS_INFO:                return "CMD_IMS_NOTIFY_VOPS_INFO";
            case CMD_IMS_UPDATE_SETTING:                  return "CMD_IMS_UPDATE_SETTING";
            case CMD_IMS_ADD_CAPABILITY:                  return "CMD_IMS_ADD_CAPABILITY";
            case CMD_IMS_NOTIFY_RAT_CHANGE:               return "CMD_IMS_NOTIFY_RAT_CHANGE";
            case CMD_IMS_NOTIFY_NETWORK_CHANGE:           return "CMD_IMS_NOTIFY_NETWORK_CHANGE";
            case CMD_IMS_AUTH_REQ_RESULT:                 return "CMD_IMS_AUTH_REQ_RESULT";
            case CMD_IMS_GEOLOCATION_REQ_RESULT:          return "CMD_IMS_GEOLOCATION_REQ_RESULT";
            case CMD_IMS_QUERY_STATE_RESULT:              return "CMD_IMS_QUERY_STATE_RESULT";
            case CMD_IMS_NOTIFY_3GPP_RAT_CHANGE:          return "CMD_IMS_NOTIFY_3GPP_RAT_CHANGE";
            case CMD_RDS_NOTIFY_RCS_CONN_INIT:            return "CMD_RDS_NOTIFY_RCS_CONN_INIT";
            case CMD_RDS_NOTIFY_RCS_CONN_ACTIVE:          return "CMD_RDS_NOTIFY_RCS_CONN_ACTIVE";
            case CMD_RDS_NOTIFY_RCS_CONN_INACTIVE:        return "CMD_RDS_NOTIFY_RCS_CONN_INACTIVE";
            case CMD_IMS_UPDATE_RT_SETTING:               return "CMD_IMS_UPDATE_RT_SETTING";
            case CMD_IMS_REG_DIGITLINE:                   return "CMD_IMS_REG_DIGITLINE";
            case CMD_RCS_ACTIVATION:                      return "CMD_RCS_ACTIVATION";
            case CMD_RCS_DEACTIVATION:                    return "CMD_RCS_DEACTIVATION";
            case CMD_SIM_OPERATOR:                        return "CMD_SIM_OPERATOR";
            case CMD_SERVICE_ACTIVATION_STATE:            return "CMD_SERVICE_ACTIVATION_STATE";
            case RSP_REQ_REG_INFO:                        return "RSP_REQ_REG_INFO";
            case RSP_IMS_REGISTERING:                     return "RSP_IMS_REGISTERING";
            case RSP_IMS_REGISTER:                        return "RSP_IMS_REGISTER";
            case RSP_IMS_DEREGISTERING:                   return "RSP_IMS_DEREGISTERING";
            case RSP_IMS_DEREGISTER:                      return "RSP_IMS_DEREGISTER";
            case RSP_EVENT_SIP_MSG:                       return "RSP_EVENT_SIP_MSG";
            case EVENT_IMS_DEREGISTER_IND:                return "EVENT_IMS_DEREGISTER_IND";
            case EVENT_IMS_AUTH_REQ:                      return "EVENT_IMS_AUTH_REQ";
            case EVENT_IMS_GEOLOCATION_REQ:               return "EVENT_IMS_GEOLOCATION_REQ";
            case EVENT_IMS_QUERY_STATE:                   return "EVENT_IMS_QUERY_STATE";
            case EVENT_IMS_EMS_MODE_INFO:                 return "EVENT_IMS_EMS_MODE_INFO";
            case EVENT_IMS_DIGITLING_REG_IND:             return "EVENT_IMS_DIGITLING_REG_IND";
            default:                                      return "unknown command";
        }
    }

}
