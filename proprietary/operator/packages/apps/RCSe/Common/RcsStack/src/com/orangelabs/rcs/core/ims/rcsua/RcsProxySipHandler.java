package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;


import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;
import com.orangelabs.rcs.platform.AndroidFactory;
import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.utils.logger.Logger;


import java.net.*;
import java.util.Arrays;
import java.util.HashMap;

/**
 * The Class RcsProxySipHandler.
 */
public class RcsProxySipHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    /**
     * The logger
     */
    private Logger logger = Logger.getLogger(this.getClass().getName());

    private Logger SIPlogger = Logger.getLogger("System.out.RcsAdapter");
    private static final String TAG = "[RcsProxySipDispatcher]";
    private Context mContext = null;
    private RcsUaAdapter mRcsUaAdapter = null;

    //socket to send SIP data to RCS SIP stack
    private static DatagramSocket mUDPSIPSocket = null;
    private static Socket mSIPSocket = null;
    RcsSIPEventListener mSIPEvtListener = null;
    private static HashMap<String, sipTransaction> sipTransactTable = new HashMap<String, sipTransaction>();

    //sip logs
    private boolean sipTraceEnabled = RcsSettings.getInstance().isSipTraceActivated();
    private final static String TRACE_SEPARATOR = "-----------------------------------------------------------------------------";
    private final int INCOMING_MSG = 0;
    private final int OUTGOING_MSG = 1;

    private static final int LISTENER_TIMER = 1000;
    private static Object mWaitSIPListener = new Object();

    private static int mRequestId = 0x0FFF0000;

    /**
     * RcsSIPEventListener
     */
    public static abstract class RcsSIPEventListener {

        /**
         * Notify sip message.
         *
         * @param sipMsgResponse the sip msg response
         * @param address the address
         * @param port the port
         */
        public abstract void notifySIPMessage(byte[] sipMsgResponse,
                InetAddress address, int port);
    }

    /**
     * The Class sipTransaction.
     */
    private class sipTransaction {
        int requestId;
        // udp = 65536, tcp = 131073
        int connId;
        String callId;

        /**
         * Instantiates a new sip transaction.
         *
         * @param reqId the req id
         * @param connId the conn id
         * @param callId the call id
         */
        sipTransaction(int reqId, int connId, String callId) {
            this.requestId = reqId;
            if (requestId == 0) {
                this.requestId = mRequestId++;
            }
            this.connId = connId;
            this.callId = callId;
        }

        /**
         * Instantiates a new sip transaction.
         *
         * @param connId the conn id
         * @param callId the call id
         */
        sipTransaction(String callId, int connId) {
            this.requestId = mRequestId++;
            this.connId = connId;
            this.callId = callId;
        }

        /**
         * Instantiates a new sip transaction.
         *
         * @param callId the call id
         */
        sipTransaction(String callId) {
            this.requestId = mRequestId++;
            this.connId = 0;
            this.callId = callId;
        }

    };

    /**
     * Instantiates a new rcs proxy sip handler.
     *
     * @param context the context
     */
    public RcsProxySipHandler(RcsUaAdapter adapter) {
        mRcsUaAdapter = adapter;
        mContext = AndroidFactory.getApplicationContext();;
    }

    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
        logger.debug("disableRequest");

        //clean the haspmap for request mapping
        sipTransactTable.clear();
    }


    /**
     * Adds the rcs sip event listener.
     *
     * @param listener the listener
     */
    public void addRCSSipEventListener(RcsSIPEventListener listener) {
        mSIPEvtListener = listener;
        synchronized (mWaitSIPListener) {
            mWaitSIPListener.notify();
        }
    }

    /**
     * Event callback.
     *
     * @param event the event
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            if (mSIPEvtListener == null){
                synchronized (mWaitSIPListener) {
                    logger.error("ERROR : SIPEvtListener for SIP stack is null, Enqueue Event again: ");
                    mWaitSIPListener.wait(LISTENER_TIMER);
                    logger.debug("Again queue event");
                    mRcsUaAdapter.writeIncomingEvent(event);
               }
            } else {
            int requestId = event.getRequestId();

            switch (requestId) {
            case RcsUaAdapter.RSP_EVENT_SIP_MSG:
                int messageId = event.getInt();
                logger.debug("EventCallback : event RSP_EVENT_SIP_MSG:" + messageId);
                int connId = event.getInt();
                int sipMessageLength = event.getInt();
                int index=0;
                String etagKey = "SIP-ETag";

                byte[] sipMsgResponse = event.getBytes(sipMessageLength);
                byte[] sipMsgResponse3 = Arrays.copyOf(sipMsgResponse, sipMessageLength);
                String sipMsgResponse2 = new String(sipMsgResponse);

                // printSIPLog(Arrays.toString(sipMsgResponse),INCOMING_MSG);
                System.out.write(sipMsgResponse);
                // logger.debug("<<<Byte hex format data first");
                // StringBuilder builder = new StringBuilder();
                // for(index=0; index<sipMessageLength; index++) {
                //     builder.append(String.format("%02X", sipMsgResponse3[index]));
                //     if(((index+1))%8==0) {
                //         builder.append("  ");
                //     } else {
                //         builder.append(" ");
                //     }
                //     if(((index+1)%16)==0) {
                //        //builder.append("\n");
                //         logger.debug(builder.toString());
                //         builder = new StringBuilder();
                //     }
                // }
                // logger.debug(builder.toString());
                logger.debug(TRACE_SEPARATOR);


                String callID = getCallIDFromSIPMessage(new String(sipMsgResponse));
                // logger.debug( "INCOMING SIP MESSAGE, ID : " + messageId + " ; CONN_ID : " + connId + "; Call_ID: " + callID + " ;SIP_MESSAGE_LENGTH : " + sipMessageLength);


                //create new SIP transaction & update SIP transaction map
                sipTransaction sipTransact = new sipTransaction(messageId, connId, callID);
                // M: ALPS03934261
                // Need to fix requestId to the messageId from Volte Stack for MT transaction
                sipTransact.requestId = messageId;
                updateSIPTranscationTable(sipTransact);

                String ipaddrss = mRcsUaAdapter.getLocalAddress();
                InetAddress inetAddress = InetAddress.getByName(ipaddrss);
                int sipPort = mRcsUaAdapter.getRCSSipStackPort();


                //added to move etag header that comes after content-length header
                sipMsgResponse2 = sipEtagRetouching(sipMsgResponse2);

                if (mSIPEvtListener != null) {
                    logger.debug( "mSIPEvtListener.notifySIPMessage");
                    if(sipMsgResponse2.contains(etagKey)){
                        mSIPEvtListener.notifySIPMessage(sipMsgResponse2.getBytes(), inetAddress, sipPort);
                    } else {
                        mSIPEvtListener.notifySIPMessage(sipMsgResponse, inetAddress, sipPort);
                    }

                }else{
                     logger.error("ERROR : SIPEvtListener for SIP stack is null");
                }

                break;
            default:
                break;
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Send sip msg.
     *
     * @param sipMsgBuffer the sip msg buffer
     * @return true, if successful
     */
    public boolean sendSipMsg(byte[] sipMsgBuffer) {

        boolean temfalg = true;
        logger.debug( "sendSipMsg");
        String sipMessage = new String(sipMsgBuffer);

        printSIPLog(sipMessage.toString(),OUTGOING_MSG);

        //append string terminator for rcs_ua
        sipMessage += "\0";

        //get the callid for the sip message
        String callID = getCallIDFromSIPMessage(sipMessage);
        //get transport protocol from the sip message
        int transProto = getTransProtoFromSIPMessage(sipMessage);
        sipTransaction sipTxn = getTransactionDetails(callID, transProto);
        logger.debug( "sendSipMsg ; call_id: " + callID + " ; request_id : "
                + sipTxn.requestId + " conn_id :  " + sipTxn.connId
                + " length = " + sipMsgBuffer.length+1);

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
                RcsUaAdapter.CMD_SEND_SIP_MSG);

        // sip msg buffer
        // ALPS03773537 Based on IMS's comment, this should be a serial number
        // VoLTE DISP: send the sip message, request_id = 0
        event.putInt(sipTxn.requestId);
        event.putInt(sipTxn.connId);
        // "+1" is MUST for IMS stack!
        event.putInt(sipMsgBuffer.length+1);
        event.putBytes(sipMsgBuffer);

        //write event
        mRcsUaAdapter.writeEvent(event);
        return true;
    }

    /**
     * The Class SipDispatchThread.
     */
    class SipDispatchThread extends Thread {

        private byte[] mSipMessage;
        private InetAddress mInetAddress;
        private int mPort;

        /**
         * Instantiates a new sip dispatch thread.
         *
         * @param sipMessage the sip message
         * @param inetAddress the inet address
         * @param port the port
         */
        public SipDispatchThread(byte[] sipMessage, InetAddress inetAddress,
                int port) {
            mSipMessage = sipMessage;
            mInetAddress = inetAddress;
            mPort = port;
        }

        /**
         * Run.
         */
        @Override
        public void run() {
            mSIPEvtListener.notifySIPMessage(mSipMessage, mInetAddress, mPort);
        }
    }

    String CALL_ID_HEADER = "Call-ID";

    /**
     * Gets the call id from sip message.
     *
     * @param sipMessage the sip message
     * @return the call id from sip message
     */
    String getCallIDFromSIPMessage(String sipMessage) {

        //logger.debug( "getCallIDFromSIPMessage");
        String callID = "";
        String[] sipHeader = sipMessage.split(System
                .getProperty("line.separator"));
        for (int i = 0; i < sipHeader.length; i++) {
            String header = sipHeader[i];
            if (header.contains(CALL_ID_HEADER)) {
                //.substring(0, s.indexOf(")"));
                header = header.substring(header.indexOf(":") + 1);
                if(!callID.contains("@")){
                    callID = header;
                }else{
                    callID = header.substring(0, header.indexOf("@"));
                }
                callID = callID.trim();
                logger.debug( "CALl-ID found : " + callID);

                break;
            }

        }

        return callID;
    }

    String VIA_HEADER = "Via";
    String TCP_PROTOCOL = "tcp";
    String UDP_PROTOCOL = "udp";

    /**
     * Gets the tcp/udp from sip message.
     *
     * @param sipMessage the sip message
     * @return udp = 65536, tcp = 131073
     */
    int getTransProtoFromSIPMessage(String sipMessage) {

        logger.debug( "getTransProtoFromSIPMessage");
        int transProto = 0;
        String[] sipHeader = sipMessage.split(System
                .getProperty("line.separator"));
        String header = null;
        for (int i = 0; i < sipHeader.length; i++) {
            header = sipHeader[i];
            if (header.contains(VIA_HEADER)) {
                logger.debug( "1st Via found : " + header);

                if (header.toLowerCase().contains(TCP_PROTOCOL)) {
                    transProto = 131073;
                } else if (header.toLowerCase().contains(UDP_PROTOCOL)) {
                    transProto = 65536;
                }
                break;
            }

        }// for()

        return transProto;
    }

    /**
     * THIS FUNCTION UPDATES THE REQUEST ID AND CONNECT ID AND CALL-ID FOR
     * INCOMING SIP MESSAGES BASED ON CALL ID.
     * WHEN RESPONSE FOR THAT REQUEST ID IS SENT, THE ENTRY IS DELETED.
     * THE KEY IS MAINTED FOR ONE REQUEST-RESPONSE TRANSACTION.
     *
     * @param sipTxn the sip txn
     */
    void updateSIPTranscationTable(sipTransaction sipTxn) {

        String callId = "" + sipTxn.callId;
        if (sipTransactTable.containsKey(callId)) {
            //update the conn id
            sipTransactTable.put(callId, sipTxn);
        } else {
            sipTransactTable.put(callId, sipTxn);
        }
    }

    /**
     * get transaction details from map.
     *
     * @param callId the call id
     * @param transProto the transport protocol
     * @return the transaction details
     */
    sipTransaction getTransactionDetails(String callId, int transProto) {
        sipTransaction sipTxtn = null;
        if (sipTransactTable.containsKey(callId)) {
            sipTxtn = sipTransactTable.get(callId);
        } else {
            sipTxtn = new sipTransaction(callId, transProto);
            sipTransactTable.put(callId, sipTxtn);
        }
        return sipTxtn;
    }

    /*
     * print sip log by rcs_adapter
     */
    private void printSIPLog(String log , int mode)
    {
        if(sipTraceEnabled)
        {
            if(mode == INCOMING_MSG)
                log = "[<<<<]" + log;
            else if(mode == OUTGOING_MSG)
                log = "[>>>>]" + log;
            SIPlogger.debug(log);
            SIPlogger.debug(TRACE_SEPARATOR);

        }
    }

    private String sipEtagRetouching(String sipmessage){

    	String etagKey = "SIP-ETag";
    	String contentlengthKey = "Content-Length";
    	if(!sipmessage.contains(etagKey)){
    		return sipmessage;
    	}

    	logger.debug( "msg contains etag ");

    	String contentheader = "";
    	String etaGHeader = "";

    	String msg = "";
    	String[] headerList = sipmessage.split("\n");
        for (String header : headerList) {

        	if(header.contains(contentlengthKey)){
        		contentheader = header;
        	}else if(header.contains(etagKey)){
        		etaGHeader = header;
            }else{
            	msg += header+"\n";
            }
        }


        msg +=etaGHeader+"\n";
        msg +=contentheader;

        logger.debug( "msg after sipEtagRetouching is "+msg);
        return msg;
    }
}
