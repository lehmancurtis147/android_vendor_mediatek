package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;

import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.utils.logger.Logger;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxySessionHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * IMS network interface
     */
    private ImsNetworkInterface networkInterface;

    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 20000;

    private static final String TAG = "RcsProxySessionHandler";
    private static RcsUaAdapter mRcsUaAdapter = null;
    private static boolean mIsRegistrationSuccessful = false;
    private static boolean mIsDeRegistrationRequestSent = false;
    private static RcsProxySessionHandler mProxySessionHandler = null;

    private static Object mLock = new Object();
    private static int mInstanceCount  = -1;
    private static Object mWaitRegisterResponse = new Object();
    private static Object mWaitDeRegisterResponse = new Object();
    private static Object mWaitReRegisterResponse = new Object();

    /**
     * Instantiates a new rcs proxy session handler.
     *
     * @param context the context
     */
    private RcsProxySessionHandler(Context context) {
        logger.debug("constructor");
        if (mRcsUaAdapter == null) {
            mRcsUaAdapter = RcsUaAdapter.getInstance();
        }

    }

    public ImsNetworkInterface RcsProxyRegistrationNetworkInterface() {
        return networkInterface;
    }

    public static synchronized RcsProxySessionHandler getProxySessionHandler(Context context){
        if(mProxySessionHandler == null){
                mProxySessionHandler = new RcsProxySessionHandler(context);
            }
        return mProxySessionHandler;
    }

    public synchronized void initInstanceCount(){
        //for MD 93
        boolean result = RcsSettings.getInstance().sendAtCommand("AT+EIMSRCSCONN=254");
        logger.debug(TAG+" initInstanceCount(): send init AT command, result=" + result);

        //for MD 91/92
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsUaAdapter.CMD_RDS_NOTIFY_RCS_CONN_INIT);
        mRcsUaAdapter.writeEvent(event);

        mInstanceCount = 0;
    }

    public synchronized void incrementInstanceCount(){
        //for MD 93
        boolean result = RcsSettings.getInstance().sendAtCommand("AT+EIMSRCSCONN=1");
        logger.debug(TAG+" incrementInstanceCount(): send active AT command, result=" + result);

        //for MD 91/92
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsUaAdapter.CMD_RDS_NOTIFY_RCS_CONN_ACTIVE);
        mRcsUaAdapter.writeEvent(event);

        mInstanceCount++;
    }

    public synchronized void decrementInstanceCount(){
        //for MD 93
        boolean result = RcsSettings.getInstance().sendAtCommand("AT+EIMSRCSCONN=0");
        logger.debug(TAG+" decrementInstanceCount(): send inActive AT command, result=" + result);

        //for MD 91/92
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsUaAdapter.CMD_RDS_NOTIFY_RCS_CONN_INACTIVE);
        mRcsUaAdapter.writeEvent(event);

        mInstanceCount--;
    }

    public synchronized void deInitInstanceCount(){
        logger.debug("deInitInstanceCount(): send deInit");

        mInstanceCount = -1;
    }

    /**
     * Event callback.
     *
     * @param event the event
     * runs in RCSUA event dispatcher
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestId();;
            String state = event.getString(event.getDataLen());

            switch (requestId) {
            case RcsUaAdapter.RSP_IMS_REGISTERING:
                logger.debug("registering state : " +state);
                 if (state.equals("1")) {
                    logger.debug("RCS_UA registering currently. wait for REG Success update");
                 } else {
                    logger.debug("RCS_UA couldn't add capab successfully. stop registering process");
                    synchronized (mWaitRegisterResponse) {
                        //failure set registration as failed.
                        mIsRegistrationSuccessful = false;
                        logger.debug("setRegistrationStatus : "+ mIsRegistrationSuccessful);
                        mRcsUaAdapter.setRegistrationStatus(mIsRegistrationSuccessful);
                        mWaitRegisterResponse.notify();
                    }
                }
                break;

            case RcsUaAdapter.RSP_IMS_REGISTER:
                logger.debug(TAG+"RSP_IMS_REGISTER , state: "+state);
                //NOTIFY LISTENING MODULE read the content and based on that set the value
                synchronized (mWaitRegisterResponse) {
                    if (state.equals("1")) {
                        mIsRegistrationSuccessful = true;
                    } else {
                        mIsRegistrationSuccessful = false;
                    }
                    //set the registration status and notify
                    logger.debug("setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsUaAdapter.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitRegisterResponse.notify();
                }
                break;

            case RcsUaAdapter.RSP_IMS_DEREGISTER:
                logger.debug("Deregistered successfully");
                //read the content and based on that set the value
                synchronized (mWaitDeRegisterResponse) {
                    //set the registration status and notify
                    mIsRegistrationSuccessful = false;
                    logger.debug(TAG+"setRegistrationStatus : "+ mIsRegistrationSuccessful);
                    mRcsUaAdapter.setRegistrationStatus(mIsRegistrationSuccessful);
                    mWaitDeRegisterResponse.notify();
                }

                //set the registration status
                mRcsUaAdapter.setRegistrationStatus(mIsRegistrationSuccessful);
                break;
            case RcsUaAdapter.RSP_REQ_REG_INFO:
                if(mRcsUaAdapter.isRegisteredbyVoLTEStack()) {
                    synchronized (mWaitReRegisterResponse) {
                        mWaitReRegisterResponse.notify();
                    }
                }
                break;
            default:
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
    }

}
