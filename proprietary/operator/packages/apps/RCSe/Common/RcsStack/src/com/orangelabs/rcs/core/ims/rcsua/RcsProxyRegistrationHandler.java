package com.orangelabs.rcs.core.ims.rcsua;

import android.content.Context;

import com.orangelabs.rcs.platform.AndroidFactory;
import com.orangelabs.rcs.provider.settings.RcsSettings;
import com.orangelabs.rcs.utils.logger.Logger;
import com.orangelabs.rcs.core.ims.ImsModule;
import com.orangelabs.rcs.core.ims.network.ImsNetworkInterface;
import com.orangelabs.rcs.core.ims.network.MobileNetworkInterface;
import com.orangelabs.rcs.core.ims.network.VolteNetworkInterface;

import com.orangelabs.rcs.core.ims.rcsua.RcsProxySessionHandler;
import com.orangelabs.rcs.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxyRegistrationHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 20000;

    private static RcsUaAdapter mRcsUaAdapter = null;
    private static boolean mIsDeRegistrationRequestSent = false;

    /**
     * Wait  answer for register/deregister
     */
    private static Object mWaitRegisterResponse = new Object();
    private static Object mWaitDeRegisterResponse = new Object();
    private static Object mWaitReRegisterResponse = new Object();

    /**
     * Instantiates a new rcs proxy registration handler.
     *
     * @param context the context
     */
    public RcsProxyRegistrationHandler(RcsUaAdapter adapter) {
        mRcsUaAdapter = adapter;
    }


    /**
     * Register to volte stack
     * @param ni the used ImsNetworkInterface
     * @return true, if successful
     */
    public boolean register(ImsNetworkInterface ni) {

        logger.debug("Register");

        mRcsUaAdapter.setRcsRegStatus(true);

        if (ni instanceof VolteNetworkInterface) {

            /* [LTE/WFC] SIP Stack already initialized, so we can udpate related SIP headers */
            ni.getSipManager().getSipStack().setServiceRoutePath(mRcsUaAdapter.getmRoute());
            ni.getSipManager().getSipStack().setInstanceId(mRcsUaAdapter.getmInstanceId());
            ImsModule.IMS_USER_PROFILE.setAssociatedUri(mRcsUaAdapter.getmAssociatedUri());

            if(RcsSettings.getInstance().isSupportOP07()) {
                RcsProxySessionHandler.getProxySessionHandler(AndroidFactory.getApplicationContext()).initInstanceCount();
            }
        } else if (ni instanceof MobileNetworkInterface) {
            RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsUaAdapter.CMD_IMS_REGISTER);
            mRcsUaAdapter.writeEvent(event);
        }
        return true;
    }

    /**
     * Reregister to volte stack
     * @param ni the used ImsNetworkInterface
     * @return true, if successful
     */
    public boolean reRegister(ImsNetworkInterface ni) {

        logger.debug("reRegister");

        mRcsUaAdapter.setRegistrationStatus(true);
        boolean addStatus=RcsSettings.getInstance().sendAtCommand("AT+EIMSRCS=2,15");

        if(RcsSettings.getInstance().isSupportOP07()) {
            RcsProxySessionHandler.getProxySessionHandler(AndroidFactory.getApplicationContext()).initInstanceCount();
        }
        return true;
    }

    /**
     * deregister to volte stack
     * @return true, if successful
     */
    public boolean deRegister() {

        logger.debug("deRegister");

        mRcsUaAdapter.setRegistrationStatus(false);

        //problem because AT command disables when REGISTER is slow, TODO find better way
        if(RcsSettings.getInstance().isSupportOP07()){
            return true;
        }

        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(mRcsUaAdapter.CMD_IMS_DEREGISTER);
        mRcsUaAdapter.writeEvent(event);

        return true;
    }

    /**
     * Event callback.
     *
     * @param event the event
     * runs in RCSUA event dispatcher
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestId();;

            switch (requestId) {
            case RcsUaAdapter.RSP_IMS_REGISTERING:
                break;
            case RcsUaAdapter.RSP_IMS_REGISTER:
                break;
            case RcsUaAdapter.RSP_IMS_DEREGISTER:
                break;
            case RcsUaAdapter.RSP_REQ_REG_INFO:
                mRcsUaAdapter.handleRegistrationInfo(event);
                break;

            default:
                logger.debug("un-supported requestId: " + requestId);
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enableRequest() {
    }

    public void disableRequest() {
    }
}
