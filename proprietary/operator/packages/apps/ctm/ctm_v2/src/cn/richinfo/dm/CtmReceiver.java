package com.richinfo.dm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import com.dmyk.android.telephony.DmykAbsTelephonyManager;
import com.dmyk.android.telephony.DmykTelephonyManager;
import com.dmyk.android.telephony.DmykTelephonyManager.MLog;


import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import cn.richinfo.dm.CtmApplication;

public class CtmReceiver extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        MLog.d("CtmReceiver receives " + action);
        if ("android.intent.action.SIM_STATE_CHANGED".equals(action)) {
            String state = intent.getStringExtra("ss");
            int slotId = intent.getIntExtra("slot", -1);
            MLog.d("SIM_STATE_CHANGED: state=" + state + ", slotId=" + slotId);
            if (slotId != -1) {
                TelephonyManager tm =
                    (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                int stateCode = tm.getSimState(slotId);
                MLog.d("SIM_STATE_CHANGED: stateCode=" + stateCode);
                // CtmApplication instance should be created after Application is done
                CtmApplication ca = CtmApplication.getInstance(null);
                if (state.equals("LOADED")) {
                    MLog.d("sim state loaded");
                    int slotFlag = ca.getSlotFlag();
                    slotFlag |= 1 << slotId;
                    ca.setSlotFlag(slotFlag);
                    if (0x03 == slotFlag) {
                        // both slot are determined, send the last state
                        Intent newIntent =
                            new Intent(DmykAbsTelephonyManager.ACTION_SIM_STATE_CHANGED);
                        newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_PHONEID, slotId);
                        newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_STATE,
                                           DmykAbsTelephonyManager.SIM_STATE_READY);
                         newIntent.setFlags(Intent.FLAG_RECEIVER_INCLUDE_BACKGROUND);
                         context.sendBroadcast(newIntent);
                    }
                    ca.syncVolteStatus();
                    ca.updateAPNObservers();
                } else if (stateCode == TelephonyManager.SIM_STATE_ABSENT) {
                    MLog.d("sim state absent");
                    int slotFlag = ca.getSlotFlag();
                    slotFlag |= 1 << slotId;
                    ca.setSlotFlag(slotFlag);
                    if (0x03 == slotFlag) {
                        // both slot are determined, send the last state
                        Intent newIntent =
                            new Intent(DmykAbsTelephonyManager.ACTION_SIM_STATE_CHANGED);
                        newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_PHONEID, slotId);
                        newIntent.putExtra(DmykAbsTelephonyManager.EXTRA_SIM_STATE,
                                           DmykAbsTelephonyManager.SIM_STATE_ABSENT);
                        newIntent.setFlags(Intent.FLAG_RECEIVER_INCLUDE_BACKGROUND);
                        context.sendBroadcast(newIntent);
                    }
                    ca.updateAPNObservers();
                }
            }
        } else if (DmykAbsTelephonyManager.ACTION_VOLTE_STATE_SETTING.equals(action)) {
            int slotId = intent.getIntExtra(DmykAbsTelephonyManager.EXTRA_SIM_PHONEID, -1);
            Intent newIntent = new Intent(DmykAbsTelephonyManager.ACTION_VOLTE_STATE_SETTING);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            newIntent.addFlags(Intent.FLAG_RECEIVER_INCLUDE_BACKGROUND);
            context.startActivity(newIntent);
        }
    }
}

