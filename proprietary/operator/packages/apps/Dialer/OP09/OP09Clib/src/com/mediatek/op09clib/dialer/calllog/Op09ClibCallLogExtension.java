package com.mediatek.op09clib.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.CallLog.Calls;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.telecom.PhoneAccountHandle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.dialer.app.calllog.CallLogActivity;
import com.android.dialer.app.calllog.CallLogFragment;
import com.android.dialer.calllogutils.CallTypeIconsView;
import com.android.dialer.database.CallLogQueryHandler;
import com.mediatek.dialer.compat.CallLogCompat.CallsCompat;
import com.mediatek.dialer.ext.DefaultCallLogExtension;
import com.mediatek.dialer.ext.ICallLogAction;
import com.mediatek.dialer.ext.ICallLogExtension;

import com.mediatek.op09clib.dialer.R;
import java.lang.ref.WeakReference;

import java.util.List;

import com.mediatek.calllog.calllogfilter.PhoneAccountUtils;
import com.mediatek.calllog.calllogfilter.PhoneAccountPickerActivity;
import com.mediatek.calllog.calllogfilter.FilterOptions;
import com.mediatek.calllog.calllogfilter.PhoneAccountInfoHelper;

import java.util.HashMap;

public class Op09ClibCallLogExtension extends DefaultCallLogExtension
        implements PhoneAccountInfoHelper.AccountInfoListener {
    private static final String TAG = "Op09ClibCallLogExtension";

    private WeakReference<CallLogActivity> mCallLogActivity;
    private Context mPluginContext = null;

    public static final int PHONE_ACCOUNT_FILTER_MENU_ID = 0;
    public static final int CALL_LOG_MENU_INDEX = 0;

    private static final int NOTICE_ID = 0x10ffffff;
    private static final int ACCOUNT_MENU_ID = 0x2000;

    private static final int TAB_INDEX_INCOMING = 1;
    private static final int TAB_INDEX_OUTGOING = 2;
    private static final int TAB_INDEX_MISSED = 3;
    private static final int MAX_TAB_COUNT = 4;

    CallLogFragment mIncomingCallFragment;
    CallLogFragment mOutgoingCallFragment;
    CallLogFragment mMissedCallFragment;

    private static final String FRAGMENT_TAG_INCOMING = "fragment_tag_incoming";
    private static final String FRAGMENT_TAG_OUTGOING = "fragment_tag_outgoing";
    private static final String FRAGMENT_TAG_MISSED = "fragment_tag_missed";

    private String mIncomingCallFragmentTag = null;
    private String mOutgoingCallFragmentTag = null;
    private String mMissedCallFragmentTag = null;
    private int mCurrentIndex = -1;

    private HashMap<Context, Boolean> mRestoreMap = new HashMap<Context, Boolean>();

    /**
     * constructor
     * @param context the current context
     */
    public Op09ClibCallLogExtension(Context context) {
        mPluginContext = context;
        PhoneAccountInfoHelper.INSTANCE.onInit(mPluginContext);
    }

    /**
     * for op09
     * called when host create menu, to add plug-in own menu here
     * @param activity the current activity
     * @param menu menu
     * @param viewPagerTabs the ViewPagerTabs used in activity
     * @param callLogAction callback plug-in need if things need to be done by host
     */
    @Override
    public void createCallLogMenu(Activity activity, Menu menu,
            HorizontalScrollView viewPagerTabs, ICallLogAction callLogAction) {
        Log.d(TAG, "createCallLogMenu");
        mCallLogActivity = new WeakReference<CallLogActivity> ((CallLogActivity) activity);

        //add the AccountFilter MenuItem
        createAccountFilterMenuItem(activity, menu);
    }

    /// M: [Call Log Account Filter] @{
    private MenuItem createAccountFilterMenuItem(final Activity activity, Menu menu) {
        MenuItem accountFilterMenuItem = menu.add(Menu.NONE, PHONE_ACCOUNT_FILTER_MENU_ID,
            ACCOUNT_MENU_ID, mPluginContext.getText(R.string.select_account));
        //accountFilterMenuItem.setIcon(mPluginContext.getDrawable(R.drawable.ic_account_chooser));
        //accountFilterMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        accountFilterMenuItem.setOnMenuItemClickListener(
            new OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Log.d(TAG, "accountFilterMenu onMenuItemClick");
                Intent intent = new Intent(mPluginContext, PhoneAccountPickerActivity.class);
                intent.putExtra(FilterOptions.FILTER_ACCOUNT_PREFER,
                        PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext));
                activity.startActivityForResult(intent, FilterOptions.ACTIVITY_REQUEST_CODE);
                return true;
            }
        });
        return accountFilterMenuItem;
    }
    /// @}

    /**
     * for op09
     * called when host prepare menu, prepare plug-in own menu here
     * @param activity the current activity
     * @param menu the Menu Created
     * @param fragment the current fragment
     * @param itemDeleteAll the optionsmenu delete all item
     * @param adapterCount adapterCount
     */
    public void prepareCallLogMenu(Activity activity, Menu menu,
            Fragment fragment, MenuItem itemDeleteAll, int adapterCount) {

        /// M :[Call Log Account Filter] @{
        // hide choose account menu if only one or no account
        final MenuItem itemChooseAccount = menu.findItem(PHONE_ACCOUNT_FILTER_MENU_ID);
        if (itemChooseAccount != null) {
            if (fragment != null) {
                itemChooseAccount.setVisible(PhoneAccountUtils
                    .hasMultipleCallCapableAccounts(mPluginContext));
            } else {
                itemChooseAccount.setVisible(false);
            }
        } else {
            if (itemChooseAccount != null) {
                itemChooseAccount.setVisible(false);
            }
        }
        /// @}

        if (fragment != null && itemDeleteAll != null) {
            Log.d(TAG, "prepareCallLogMenu() adapter account: " + adapterCount);
            itemDeleteAll.setVisible(adapterCount > 0);
        }
    }

    /**
     * for op09
     * Get Tab Index Count
     * @return int
     */
    @Override
    public int getTabIndexCount() {
        Log.d(TAG, "getTabIndexCount");
        return MAX_TAB_COUNT;
    }

    /**
     * for op09
     * Init call Log tab
     * @param tabTitles tabTitles
     * @param viewPager viewPager
     */
    @Override
    public void initCallLogTab(CharSequence[] tabTitles, ViewPager viewPager) {
        Log.d(TAG, "initCallLogTab");
        if (mCallLogActivity != null && mCallLogActivity.get() != null) {
            Log.d(TAG, "initCallLogRes");
            CallTypeIconsView.Resources res =
                    new CallTypeIconsView.Resources(mCallLogActivity.get(), false);
            if (res != null) {
                tabTitles[1] = createSpannableString(res.incoming);
                tabTitles[2] = createSpannableString(res.outgoing);
                tabTitles[3] = createSpannableString(res.missed);
            }
        }
        viewPager.setOffscreenPageLimit(3);
    }

    /**
     * for op09
     * @param context the current context
     * @param pagerAdapter the view pager adapter used in activity
     * @param tabs the ViewPagerTabs used in activity
     */
    @Override
    public void restoreFragments(Bundle bundle, Context context,
            FragmentPagerAdapter pagerAdapter, HorizontalScrollView tabs) {
        Log.d(TAG, "restoreFragments");

        CallLogActivity callLogActivity = (CallLogActivity) context;
        if (bundle != null) {
            mIncomingCallFragmentTag = bundle.getString(FRAGMENT_TAG_INCOMING, null);
            mOutgoingCallFragmentTag = bundle.getString(FRAGMENT_TAG_OUTGOING, null);
            mMissedCallFragmentTag = bundle.getString(FRAGMENT_TAG_MISSED, null);
        }

        if (mIncomingCallFragment == null && mIncomingCallFragmentTag != null) {
            mIncomingCallFragment = (CallLogFragment) callLogActivity
                    .getFragmentManager().findFragmentByTag(mIncomingCallFragmentTag);
        }
        if (mOutgoingCallFragment == null && mOutgoingCallFragmentTag != null) {
            mOutgoingCallFragment = (CallLogFragment) callLogActivity
                    .getFragmentManager().findFragmentByTag(mOutgoingCallFragmentTag);
        }
        if (mMissedCallFragment == null && mMissedCallFragmentTag != null) {
            mMissedCallFragment = (CallLogFragment) callLogActivity
                    .getFragmentManager().findFragmentByTag(mMissedCallFragmentTag);
        }

        if (!mRestoreMap.containsKey(context)) {
            mRestoreMap.put(context, true);
        }
    }

    /**
     * for op09
     * @param activity the current activity
     * @param outState save state
     */
    @Override
    public void onSaveInstanceState(Activity activity, Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        if (mIncomingCallFragment != null) {
            outState.putString(FRAGMENT_TAG_INCOMING, mIncomingCallFragment.getTag());
        }

        if (mOutgoingCallFragment != null) {
            outState.putString(FRAGMENT_TAG_OUTGOING, mOutgoingCallFragment.getTag());
        }

        if (mMissedCallFragment != null) {
            outState.putString(FRAGMENT_TAG_MISSED, mMissedCallFragment.getTag());
        }
    }

    /**
      * for op09
      * Get the CallLog Item
      * @param position position
      * @return Fragment
      */
    @Override
    public Fragment getCallLogFragmentItem(int position) {
        Log.d(TAG, "getCallLogFragmentItem");
        if (position == TAB_INDEX_MISSED) {
            return new CallLogFragment(Calls.MISSED_TYPE , true);
        } else if (position == TAB_INDEX_INCOMING) {
            return new CallLogFragment(Calls.INCOMING_TYPE, true);
        } else if (position == TAB_INDEX_OUTGOING) {
            return new CallLogFragment(Calls.OUTGOING_TYPE, true);
        }
        return null;
    }

    /**
     * for op09
     * Instantiate CallLog Item
     * @param activity activity
     * @param position position
     * @param fragment fragment
     * @return Object if plugin init the item return the fragment, or else return null
     */
    @Override
    public Object instantiateCallLogFragmentItem(Activity activity, int position,
                               Fragment fragment) {
        Log.d(TAG, "instantiateCallLogFragmentItem");
        if (position == TAB_INDEX_MISSED) {
            mMissedCallFragment = (CallLogFragment) fragment;
        } else if (position == TAB_INDEX_INCOMING) {
            mIncomingCallFragment = (CallLogFragment) fragment;
        } else if (position == TAB_INDEX_OUTGOING) {
            mOutgoingCallFragment = (CallLogFragment) fragment;
        } else {
            return null;
        }
        return fragment;
    }

    /**
     * for op09
     * Get current calllog type by position
     * @param index int
     * @return int
     */
    public int getCurrentCallLogFilteType(int pos) {
        if (pos == TAB_INDEX_MISSED) {
            return Calls.MISSED_TYPE;
        } else if (pos == TAB_INDEX_INCOMING) {
            return Calls.INCOMING_TYPE;
        } else if (pos == TAB_INDEX_OUTGOING) {
            return Calls.OUTGOING_TYPE;
        }
        return -1;
    }

    /**
     * for op09
     * Get current fragment by position
     * @param index int
     * @return Fragment
     */
    @Override
    public Fragment getCurrentFragment(int index) {
        Log.d(TAG, "getCurrentFragment");
        if (index == TAB_INDEX_MISSED) {
            return mMissedCallFragment;
        } else if (index == TAB_INDEX_INCOMING) {
            return mIncomingCallFragment;
        } else if (index == TAB_INDEX_OUTGOING) {
            return mOutgoingCallFragment;
        }
        return null;
    }

    /**
     * for op09
     * @param typeFiler current query type
     * @param builder the query selection Stringbuilder
     * @param selectionArgs the query selection args, modify to change query selection
     */
    @Override
    public void appendQuerySelection(int typeFiler, StringBuilder builder,
            List<String> selectionArgs) {
        if (!shouldCheckAppendClause()) {
            Log.d(TAG, "appendQuerySelection, skip to set clause");
            return;
        }

        /// M: [Call Log Account Filter] @{
        String preferAccountId = PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext);
        if (!PhoneAccountInfoHelper.FILTER_ALL_ACCOUNT_ID.equals(preferAccountId)) {
            if (builder.length() > 0) {
                builder.append(" AND ");
            }
            // query the Call Log by account id
            builder.append(String.format("(%s = ?)", Calls.PHONE_ACCOUNT_ID));
            selectionArgs.add(preferAccountId);
        }
        /// @}
        if (typeFiler != CallLogQueryHandler.CALL_TYPE_ALL) {
            if (builder.length() > 0) {
                builder.append(" AND ");
            }
            // cant query conference call log when call log fragment filter is miss, outgong, incoming
            builder.append(String.format("(%s <= 0)", CallsCompat.CONFERENCE_CALL_ID));
        }
        Log.d(TAG, "builder: " + builder);
    }

    /**.
     * for op09
     * plug-in reset the reject mode in the host
     * @param activity the current activity
     * @param bundle bundle
     */
    @Override
    public void onCreate(Activity activity, Bundle bundle) {
        PhoneAccountInfoHelper.INSTANCE.registerForAccountChange(this);
        PhoneAccountInfoHelper.INSTANCE.setActivity(activity);

        mCallLogActivity = new WeakReference<CallLogActivity> ((CallLogActivity) activity);
    }

    /**.
     * for op09
     * plug-in reset the restore activty status
     * @param activity the current activity
     */
    @Override
    public void onResume(Activity activity) {
        if (!mRestoreMap.isEmpty() && mRestoreMap.containsKey(activity)) {
            mRestoreMap.put(activity, false);
            Log.d(TAG, "resetRejectMode() reset mRestoreMap");
        }
    }

    /**.
     * for op09
     * plug-in manage the state and unregister receiver
     * @param activity the current activity
     */
    @Override
    public void onDestroy(Activity activity) {
        PhoneAccountInfoHelper.INSTANCE.unRegisterForAccountChange(this);

        if (!mRestoreMap.isEmpty() && mRestoreMap.containsKey(activity)) {
            mRestoreMap.remove(activity);
            Log.d(TAG, "onDestroy() remove mRestoreMap");
        }
    }


    /**
     * for op09
     * plug-in handle Activity Result
     * @param requestCode requestCode
     * @param resultCode resultCode
     * @param data the intent return by setResult
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult requestCode,,resultCode,,data = " +
                requestCode + ",," + resultCode + ",," + data);
        if (requestCode == FilterOptions.ACTIVITY_REQUEST_CODE) {
            if (resultCode == FilterOptions.ACTIVITY_RESULT_CODE) {
                Bundle bundle = data.getExtras();
                String selectedId = bundle.getString(FilterOptions.SELECTED_ID);
                boolean result = PhoneAccountInfoHelper.INSTANCE.setPreferAccountId(mPluginContext,
                        String.valueOf(selectedId));
                if (result) {
                    PhoneAccountInfoHelper.INSTANCE.notifyPreferAccountChange(selectedId);
                }
            } else {
                Log.e(TAG, "onActivityResult failed");
            }
        }
    }

    /**
     * for op09
     * plug-in to create account info in calllog fragment
     * @param fragment the current fragment
     * @param view the current view
     */
    @Override
    public void onViewCreated(Fragment fragment, View view) {
        Activity activity = fragment.getActivity();
        Resources resource = activity.getResources();
        String packageName = activity.getPackageName();
        View recyclerView = view.findViewById(resource.getIdentifier("recycler_view",
                "id", packageName));

        if (recyclerView == null) {
            Log.d(TAG, "onViewCreated, recyclerView is null");
            return;
        }

        ViewGroup group = (ViewGroup) recyclerView.getParent();
        if (group == null) {
            Log.d(TAG, "onViewCreated, recyclerView parent is null");
            return;
        }

        addViewContent(group);
    }

    private void addViewContent(ViewGroup groupParent) {
        LayoutInflater inflater;
        inflater = LayoutInflater.from(mPluginContext);
        ViewGroup noticeLayout =
                (ViewGroup) inflater.inflate(R.layout.account_notice_indicator, groupParent, false);
        noticeLayout.setId(NOTICE_ID);
        groupParent.addView(noticeLayout, 1);
    }

    /**
     * for op09
     * plug-in refresh the CallLogFragment to show th notice
     * @param fragment the current fragment
     */
    @Override
    public void updateNotice(Fragment fragment) {
        View view = fragment.getView();

        Activity activity = fragment.getActivity();
        if (activity != null &&
            !(activity instanceof CallLogActivity)) {
          return;
        }

        ViewGroup viewChild = (ViewGroup) view.findViewById(NOTICE_ID);
        if (viewChild == null) {
            return;
        }

        TextView noticeText = (TextView) viewChild.getChildAt(0);
        View noticeTextDivider = (View) viewChild.getChildAt(1);
        String lable = null;
        int color = -1;
        String id = PhoneAccountInfoHelper.INSTANCE.getPreferAccountId(mPluginContext);
        if (mPluginContext != null && !PhoneAccountInfoHelper.FILTER_ALL_ACCOUNT_ID.equals(id)) {
            PhoneAccountHandle account = PhoneAccountUtils.getPhoneAccountById(mPluginContext, id);
            if (account != null) {
                lable = PhoneAccountUtils.getAccountLabel(mPluginContext, account);
                color = PhoneAccountUtils.getAccountColor(mPluginContext, account);
            }
        }
        Log.d(TAG, "updateNotice, lable = " + lable + ", id = " + id);
        if (!TextUtils.isEmpty(lable) && noticeText != null && noticeTextDivider != null) {
            String noticeString = mPluginContext.getResources().getString(
                    R.string.call_log_via_sim_name_notice, lable);
            SpannableStringBuilder style = applySpannableStyle(noticeString, lable, color);
            noticeText.setText(style);
            noticeText.setVisibility(View.VISIBLE);
            noticeTextDivider.setVisibility(View.VISIBLE);
        } else {
            noticeText.setVisibility(View.GONE);
            noticeTextDivider.setVisibility(View.GONE);
        }
    }

    private SpannableStringBuilder applySpannableStyle(String target, String source, int color) {
        SpannableStringBuilder style = new SpannableStringBuilder(target);

        int start = target.indexOf(source);
        int end = target.length() - 1;
        Log.d(TAG, "applySpannableStyle, start = " + start + ", end = " + end);

        if (end > start && start > 0 && color != -1) {
            style.setSpan(new ForegroundColorSpan(color),
                  start, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return style;
    }

    /**
     * Callback when preferrence account changed.
     * @param index String
     */
    @Override
    public void onPreferAccountChanged(String index) {
        Log.d(TAG, "onPreferAccountChanged");
        if (mCallLogActivity != null && mCallLogActivity.get() != null) {
            CallLogActivity activity = mCallLogActivity.get();
            activity.updateCallLogScreen();
        }

        forceToRefreshAllData();
    }

    /**
     * Callback when preferrence account updated.
     */
    @Override
    public void onAccountInfoUpdate() {
        Log.d(TAG, "onAccountInfoUpdate");
        if (mCallLogActivity != null && mCallLogActivity.get() != null) {
            CallLogActivity activity = mCallLogActivity.get();
            activity.updateCallLogScreen();
        }

        forceToRefreshAllData();
    }

    /**
     * for op02/op09
     * called when selec other page to clear missed call
     * @param index the index of fragment
     * @return boolean if plugin processed return true, or else return false to process in host
     */
    @Override
    public boolean updateMissedCalls(int index) {
        Log.d(TAG, "updateMissedCalls");
        CallLogFragment fragment = (CallLogFragment) getCurrentFragment(index);
        if (fragment != null) {
            fragment.markMissedCallsAsReadAndRemoveNotifications();
            return true;
        }
        return false;
    }

    /**
     * for op02/op09
     * called to update empty message
     * @param fragment the CallLogFragment
     * @param filter the type of call filter
     * @return boolean if plugin processed return true, or else return false to process in host
     */
    @Override
    public boolean updateEmptyMessage(Fragment fragment, int filter) {
        Log.d(TAG, "updateEmptyMessage, filter = " + filter);
        View view = fragment.getView();

        Activity activity = fragment.getActivity();
        Resources resource = activity.getResources();
        String packageName = activity.getPackageName();
        TextView emptyTextView = (TextView) view.findViewById(
                resource.getIdentifier("empty_list_view_message", "id", packageName));
        TextView actionTextView = (TextView) view.findViewById(
                resource.getIdentifier("empty_list_view_action", "id", packageName));

        final int messageId;
        switch(filter) {
            case Calls.INCOMING_TYPE:
                messageId = R.string.call_log_incoming_empty;
                break;
            case Calls.OUTGOING_TYPE:
                messageId = R.string.call_log_outgoing_empty;
                break;
            default:
                return false;
        }

        if (emptyTextView != null) {
            emptyTextView.setText(mPluginContext.getText(messageId));
        }

        if (actionTextView != null) {
            actionTextView.setText(null);
            actionTextView.setVisibility(View.GONE);
        }
        return true;
    }

    private void forceToRefreshAllData() {
        Log.d(TAG, "forceToRefreshAllData");
        if (mIncomingCallFragment != null) {
            mIncomingCallFragment.forceToRefreshData();
        }

        if (mOutgoingCallFragment != null) {
            mOutgoingCallFragment.forceToRefreshData();
        }

        if (mMissedCallFragment != null) {
            mMissedCallFragment.forceToRefreshData();
        }
    }

    private boolean shouldCheckAppendClause() {
        if (mCallLogActivity != null) {
            CallLogActivity activity = mCallLogActivity.get();
            if (activity != null && activity.isActivityResumed()) {
                return true;
            }
        }

        for (Context context : mRestoreMap.keySet()) {
            boolean calllogRestored = (Boolean) mRestoreMap.get(context);
            if (calllogRestored) {
                return true;
            }
        }
        return false;
    }

    private SpannableString createSpannableString(Drawable drawable) {
        drawable.setBounds(0, 0, (drawable.getIntrinsicWidth() * 3) / 2,
                (drawable.getIntrinsicHeight() * 3) / 2);
        SpannableString sp = new SpannableString("i");
        ImageSpan iconsp = new ImageSpan(drawable, ImageSpan.ALIGN_BOTTOM);
        sp.setSpan(iconsp, 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        return sp;
    }
}
