package com.mediatek.op08.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;
import android.view.View;
import android.os.Handler;
import android.os.Looper;
import com.android.dialer.calllogutils.CallbackActionHelper.CallbackAction;

import com.mediatek.op08.presence.ContactNumberUtils;
import com.mediatek.op08.presence.PresenceApiManager;
import com.mediatek.op08.presence.PresenceApiManager.CapabilitiesChangeListener;
import com.mediatek.op08.presence.PresenceApiManager.ContactInformation;

import java.util.HashMap;
import java.util.Map;

public class CallDetailItemsController implements CapabilitiesChangeListener {
    private static final String TAG = "CallDetailItemsController";
    private static final float ALPHA_TRANSPARENT_VALUE = 0.3f;
    private static final float ALPHA_OPAQUE_VALUE = 1.0f;
    private PresenceApiManager mTapi = null;
    private String mNumber;
    private View mVideoView;
    private String mFormatNumber;
    private Handler mHandler = null;

    public CallDetailItemsController(Activity activity) {
        Log.d(TAG, "CallDetailItemsController activity: " + activity);
        if (PresenceApiManager.initialize(activity)) {
            mTapi = PresenceApiManager.getInstance();
            mTapi.addCapabilitiesChangeListener(this);
            mHandler = new Handler(Looper.getMainLooper());
        }
    }

    public void clear() {
        Log.d(TAG, "clear");
        mFormatNumber = null;
        mVideoView = null;
        if (mTapi != null) {
            mTapi.removeCapabilitiesChangeListener(this);
        }
        mHandler = null;
    }

    public void customizeVideoItem(View view, int type, String number) {
        Log.d(TAG, "customizeVideoItem type: " + type + " number: " + number);
        if (type != CallbackAction.IMS_VIDEO) {
            Log.d(TAG, "customizeVideoItem is not video call detail, return");
            return;
        }

        mNumber = number;
        mVideoView = view;
        updateVideoAction();

        mFormatNumber = ContactNumberUtils.getDefault().getFormatNumber(number);
        Log.d(TAG, "customizeVideoItem: mFormatNumber = " + mFormatNumber);
        if (mTapi != null) {
            mTapi.requestContactPresence(mFormatNumber, false);
        }
    }

    private void updateVideoAction() {
        if (mVideoView == null) {
            Log.d(TAG, "updateVideoAction mVideoView is null return");
            return;
        }
        if (mTapi != null) {
            if (mTapi.isVideoCallCapable(mNumber)) {
                Log.d(TAG, "updateVideoAction has video capable,mNumber:" + mNumber);
                mVideoView.setAlpha(ALPHA_OPAQUE_VALUE);
            } else {
                Log.d(TAG, "updateVideoAction no video capable mNumber:" + mNumber);
                mVideoView.setAlpha(ALPHA_TRANSPARENT_VALUE);
            }
        }
    }

    public void onCapabilitiesChanged(String contact, ContactInformation info) {
        Log.d(TAG, "CallDetailItemsController: onCapabilitiesChanged: " + contact);
        if (mVideoView != null && contact.equals(mFormatNumber) && info != null) {
            Log.d(TAG, "onCapabilitiesChanged post to ui thread");
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onCapabilitiesChanged runnable:");
                    updateVideoAction();
                }
            });
        }
    }
}