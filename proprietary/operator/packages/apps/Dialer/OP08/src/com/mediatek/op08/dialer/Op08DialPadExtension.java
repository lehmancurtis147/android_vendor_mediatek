package com.mediatek.op08.dialer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;

import com.android.contacts.common.list.ContactListItemView;
import com.mediatek.dialer.ext.DefaultDialPadExtension;
import com.mediatek.op08.presence.ContactNumberUtils;
import com.mediatek.op08.presence.LogUtils;
import com.mediatek.op08.presence.PresenceApiManager;
import com.mediatek.op08.presence.PresenceApiManager.CapabilitiesChangeListener;
import com.mediatek.op08.presence.PresenceApiManager.ContactInformation;

import java.lang.ref.WeakReference;

public class Op08DialPadExtension extends DefaultDialPadExtension
        implements CapabilitiesChangeListener {
    private static final String TAG = "Op08DialPadExtension";
    private static final int SHORTCUT_MAKE_VIDEO_CALL = 4;
    private static final int ALPHA_TRANSPARENT_VALUE = 100;
    private static final int ALPHA_OPAQUE_VALUE = 255;

    private Context mContext;
    private PresenceApiManager mTapi = null;
    private String mLastDisplayNumber;
    private String mLastQueryNumber;
    private WeakReference<Activity> mActivity;
    private WeakReference<ContactListItemView> mItemView;

    public Op08DialPadExtension(Context context) {
        mContext = context;
        if (PresenceApiManager.initialize(context)) {
            mTapi = PresenceApiManager.getInstance();
            mTapi.addCapabilitiesChangeListener(this);
        }
    }

    /**
     * Request capability when input number in Dialer.
     * @param number Contact's number
     */
    @Override
    public void onSetQueryString(String number) {
        LogUtils.d(TAG, " onSetQueryString number: " + number);
        if (mLastQueryNumber != null && mLastQueryNumber.equals(number)) {
            LogUtils.d(TAG, " onSetQueryString same number: " + mLastQueryNumber);
            return;
        }

        // send force request
        if (mTapi != null) {
            mTapi.requestContactPresence(number, true);
            mLastQueryNumber = number;
        }
    }

    /**
     * Build Dialtacts Options Menu.
     * @param activity Dialtacts Activity
     * @param menu Dialtacts Menu
     */
    public void buildOptionsMenu(Activity activity, Menu menu) {
        Log.d(TAG, "buildOptionsMenu: " + activity);
        mActivity = new WeakReference<Activity>(activity);
    }

    private boolean isVisible(View view) {
        return view != null && view.getVisibility() == View.VISIBLE;
    }

    /**
    * Send content change notify to ContentResolver, so the UI will be refreshed by APP.
    * @param number Contact's number
    * @param info ContactInformation
    */
    @Override
    public void onCapabilitiesChanged(String number, ContactInformation info) {
        LogUtils.d(TAG, "onCapabilitiesChanged number: " + number);
        if (mActivity == null) {
            Log.i(TAG, "onCapabilitiesChanged: activity null");
            return;
        }
        Activity activity = mActivity.get();
        if (activity == null || activity.isFinishing()) {
            Log.i(TAG, "onCapabilitiesChanged: activity finish");
            return;
        }
        if (mLastDisplayNumber != null && mItemView != null &&
                mLastDisplayNumber.equals(number)) {
            ContactListItemView item = mItemView.get();
            boolean isVideoCapable = info.isVideoCapable;
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onCapabilitiesChanged: need update");
                    if (isVisible(item)) {
                        ImageView photo = item.getPhotoView();
                        int alpha = 0;
                        if (isVisible(photo)) {
                            alpha = photo.getImageAlpha();
                        }
                        Log.d(TAG, "onCapabilitiesChanged isVideoCapable: " + isVideoCapable
                                + " alpha: " + alpha);
                        if (isVideoCapable && alpha == ALPHA_TRANSPARENT_VALUE) {
                            photo.setImageAlpha(ALPHA_OPAQUE_VALUE);
                            item.getNameTextView().setTextColor(Color.BLACK);
                        } else if (!isVideoCapable && alpha == ALPHA_OPAQUE_VALUE) {
                            photo.setImageAlpha(ALPHA_TRANSPARENT_VALUE);
                            item.getNameTextView().setTextColor(Color.GRAY);
                        }
                    }
                }
            });
        }
    }

    /*
     *Customize List Item View.
     *@param view ContactListItemView
     *@param type Shortcut Type
     *@param number Query Number
     */
    @Override
    public void customizeDialerOptions(View view, int type, String number) {
        //Maybe extend other option later
        if (type != SHORTCUT_MAKE_VIDEO_CALL) {
            return;
        }
        ContactListItemView item = (ContactListItemView) view;
        if (type == SHORTCUT_MAKE_VIDEO_CALL) {
            if (mTapi != null && item != null) {
                mItemView = new WeakReference<ContactListItemView>(item);
                mLastDisplayNumber = ContactNumberUtils.getDefault().getFormatNumber(number);
                LogUtils.d(TAG, "customizeDialerOptions number: " + mLastDisplayNumber);
                if (mTapi.isVideoCallCapable(mLastDisplayNumber)) {
                    item.getPhotoView().setImageAlpha(ALPHA_OPAQUE_VALUE);
                    item.getNameTextView().setTextColor(Color.BLACK);
                } else {
                    item.getPhotoView().setImageAlpha(ALPHA_TRANSPARENT_VALUE);
                    item.getNameTextView().setTextColor(Color.GRAY);
                }
            }
        }
     }
}
