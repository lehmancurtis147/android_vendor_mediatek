package com.mediatek.op02.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;

import com.mediatek.op02.dialer.calllog.Op02CallLogExtension;

public class Op02DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    public Context mContext;
    public Op02DialerCustomizationFactory (Context context){
        mContext = context;
    }

    @Override
    public ICallLogExtension makeCallLogExt() {
        return new Op02CallLogExtension(mContext);
    }
}
