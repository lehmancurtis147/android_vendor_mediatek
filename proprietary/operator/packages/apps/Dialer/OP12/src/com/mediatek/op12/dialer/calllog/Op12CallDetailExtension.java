package com.mediatek.op12.dialer;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.dialer.compat.AppCompatConstants;
import com.android.dialer.calldetails.CallDetailsActivity;

import com.mediatek.dialer.ext.DefaultCallDetailExtension;
import com.mediatek.op12.dialer.VideoItemsManager;

public class Op12CallDetailExtension extends DefaultCallDetailExtension {

    private static final String TAG = "Op12CallDetailExtension";
    private static final int INCOMING_PULLED_AWAY_TYPE = 10;
    private static final int OUTGOING_PULLED_AWAY_TYPE = 11;
    private static final int DECLINED_EXTERNAL_TYPE = 12;
    private Context mContext;
    private VideoItemsManager mVideoItemsManager;

    public Op12CallDetailExtension(Context context) {
        mContext = context;
        mVideoItemsManager = VideoItemsManager.getInstance();
    }

    @Override
    public void onCreate(Object obj, Object tool) {
        if (obj instanceof CallDetailsActivity) {
            CallDetailsActivity activity = (CallDetailsActivity) obj;
            mVideoItemsManager.createCallDetailsController(activity);
        }
    }

    @Override
    public void onDestroy(Object obj) {
        if (obj instanceof CallDetailsActivity) {
            CallDetailsActivity activity = (CallDetailsActivity) obj;
            mVideoItemsManager.destroyCallDetailsController(activity);
        }
    }

    @Override
    public void setCallbackAction(ImageView callbackButton, int action, String number) {
        mVideoItemsManager.customizeVideoItem(callbackButton, action, number);
    }
}
