package com.mediatek.op12.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.app.calllog.IntentProvider;
import com.android.dialer.calllogutils.CallbackActionHelper.CallbackAction;
import com.android.dialer.util.CallUtil;

import com.mediatek.op12.presence.ContactNumberUtils;
import com.mediatek.op12.presence.PresenceApiManager;
import com.mediatek.op12.presence.PresenceApiManager.CapabilitiesChangeListener;
import com.mediatek.op12.presence.PresenceApiManager.ContactInformation;

import java.util.HashMap;
import java.util.Map;

public class CallLogItemsController {
    private static final String TAG = "CallLogItemsController";
    private static final float ALPHA_TRANSPARENT_VALUE = 0.3f;
    private static final float ALPHA_OPAQUE_VALUE = 1.0f;
    private PresenceApiManager mTapi = null;

    private Fragment mFragment;
    private Activity mActivity;
    private Map<CallLogListItemViewHolder, OPCallLogListItemViewHolder> mCallLogMap =
            new HashMap<CallLogListItemViewHolder, OPCallLogListItemViewHolder>();

    public CallLogItemsController(Fragment fragment) {
        mFragment = fragment;
        mActivity = mFragment.getActivity();
        if (PresenceApiManager.initialize(mActivity)) {
            mTapi = PresenceApiManager.getInstance();
        }
    }

    public void addCallLogViewHolder(CallLogListItemViewHolder hostHolder) {
        Log.d(TAG, "addCallLogViewHolder: " + hostHolder);
        if (mCallLogMap.containsKey(hostHolder)) {
            throw new RuntimeException("ViewHolder: " + hostHolder
                    + " can be initialized only one time");
        }
        OPCallLogListItemViewHolder opHolder = new OPCallLogListItemViewHolder(hostHolder);
        //opHolder.customizePrimaryActionButton(hostHolder.callbackAction);
        mCallLogMap.put(hostHolder, opHolder);
    }

    public void removeCallLogViewHolder(CallLogListItemViewHolder hostHolder) {
        Log.d(TAG, "onCallLogListItemViewHolderDetached: " + hostHolder);
        OPCallLogListItemViewHolder viewHolder = mCallLogMap.remove(hostHolder);
        if (viewHolder != null) {
            viewHolder.onDetached();
        }
    }

    public void showActions(CallLogListItemViewHolder holder, boolean show) {
        if (holder != null && holder.confCallNumbers != null) {
            Log.d(TAG, "showActions, conference calllog, do nothing.");
            return;
        }

        OPCallLogListItemViewHolder opHolder = mCallLogMap.get(holder);
        Log.d(TAG, "showActions, opHolder:" + opHolder);
        if (opHolder != null) {
            opHolder.showActions(show);
        }
    }

    public void clear() {
        Log.d(TAG, "clear");
        mFragment = null;
        for (CallLogListItemViewHolder viewHolder : mCallLogMap.keySet()) {
            mCallLogMap.get(viewHolder).onDetached();
        }
        mCallLogMap.clear();
        mCallLogMap = null;
    }

    class OPCallLogListItemViewHolder implements CapabilitiesChangeListener {
        private CallLogListItemViewHolder mHostHolder;
        private String mFormatNumber;
        private boolean mIsDetached = false;
        private Handler mHandler = null;
        private boolean isShowAction = false;

        public OPCallLogListItemViewHolder(CallLogListItemViewHolder host) {
            mHostHolder = host;
            Log.d(TAG, "OPCallLogListItemViewHolder: number = " + host.number);
            if (mTapi != null) {
                mTapi.addCapabilitiesChangeListener(this);
            }
            mHandler = new Handler(Looper.getMainLooper());
            mFormatNumber = ContactNumberUtils.getDefault()
                        .getFormatNumber(mHostHolder.number);
        }

        private boolean isVisible(View view) {
            return view != null && view.getVisibility() == View.VISIBLE;
        }

        private void showActions(boolean show) {
            Log.d(TAG, "OPCallLogListItemViewHolder showActions show: " + show);
            isShowAction = show;
            //update primary action button
            customizePrimaryActionButton(mHostHolder.callbackAction);

            //update action button
            if (isShowAction) {
                View videoButton = mHostHolder.videoCallButtonView;
                if (isVisible(videoButton) && mTapi != null) {
                    if (mTapi.isVideoCallCapable(mHostHolder.number)) {
                        Log.d(TAG, "OPCallLogListItemViewHolder showAction has video capable");
                        videoButton.setAlpha(ALPHA_OPAQUE_VALUE);
                    } else {
                        Log.d(TAG, "OPCallLogListItemViewHolder showAction no video capable");
                        videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                    }
                } else {
                    Log.d(TAG, "OPCallLogListItemViewHolder showAction  mTapi:"
                            + mTapi + " isVisible(videoButton): " + isVisible(videoButton));
                }

                Log.d(TAG, "showAction: mFormatNumber = " + mFormatNumber);
                if (mTapi != null) {
                    mTapi.requestContactPresence(mFormatNumber, false);
                }
            }

            // /for test
           /* mHandler.postDelayed(new Runnable() {
                    @Override public void run() { 
                    Log.d(TAG, "test begine runnable:");
                    onCapabilitiesChangedTest(); } }, 6000);*/
             
        }

        private void customizePrimaryActionButton(int callbackAction) {
            ImageView actionButtion = mHostHolder.primaryActionButtonView;
            if (callbackAction == CallbackAction.IMS_VIDEO) {
                if (!CallUtil.isVideoEnabled(mActivity)) {
                    Log.d(TAG, "customizePrimaryActionButton isVideoEnabled false, set gone");
                    actionButtion.setVisibility(View.GONE);
                    return;
                } else {
                    actionButtion.setVisibility(View.VISIBLE);
                }

                if (mTapi != null && mTapi.isVideoCallCapable(mHostHolder.number)) {
                    Log.d(TAG, "customizePrimaryActionButton has video capable");
                    actionButtion.setAlpha(ALPHA_OPAQUE_VALUE);
                } else {
                    actionButtion.setAlpha(ALPHA_TRANSPARENT_VALUE);
                    Log.d(TAG, "customizePrimaryActionButton no video capable");
                }
                Log.d(TAG, "OP12CallLogListItemViewHolder: mFormatNumber = " + mFormatNumber);
                if (mTapi != null) {
                    mTapi.requestContactPresence(mFormatNumber, false);
                }
            } else {
                actionButtion.setVisibility(View.VISIBLE);
                actionButtion.setAlpha(ALPHA_OPAQUE_VALUE);
            }
        }

        private void onDetached() {
            mIsDetached = true;
            if (mTapi != null) {
                mTapi.removeCapabilitiesChangeListener(this);
            }
            mHostHolder = null;
            mHandler = null;
        }

		@Override
        public void onCapabilitiesChanged(String contact,
                ContactInformation info) {
            if (mIsDetached) {
                Log.d(TAG, "OPCallLogListItemViewHolder: onCapabilitiesChanged: detach return");
                return;
            }
            if (mHostHolder == null) {
                Log.d(TAG, "OPCallLogListItemViewHolder: mHostHolder is null, return");
                return;
            }

            if (contact.equals(mFormatNumber) && info != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mHostHolder == null) {
                            return;
                        }
                        Log.d(TAG, "onCapabilitiesChanged runnable:");
                        ImageView videoAction = mHostHolder.primaryActionButtonView;
                        if (mHostHolder.callbackAction == CallbackAction.IMS_VIDEO) {
                            if (!CallUtil.isVideoEnabled(mActivity)) {
                                Log.d(TAG,
                                        "customizePrimaryActionButton isVideoEnabled false, set gone");
                                videoAction.setVisibility(View.GONE);
                            } else {
                                videoAction.setVisibility(View.VISIBLE);
                                if (info.isVideoCapable) {
                                    Log.d(TAG, "onCapabilitiesChanged has video capable");
                                    videoAction.setAlpha(ALPHA_OPAQUE_VALUE);
                                } else {
                                    Log.d(TAG, "onCapabilitiesChanged no video capable,SET GONE");
                                    videoAction.setAlpha(ALPHA_TRANSPARENT_VALUE);
                                }
                            }
                        }

                        View videoButton = mHostHolder.videoCallButtonView;
                        if (isShowAction && isVisible(videoButton)) {
                            if (info.isVideoCapable) {
                                Log.d(TAG, "onCapabilitiesChanged has video capable");
                                videoButton.setAlpha(ALPHA_OPAQUE_VALUE);
                            } else {
                                Log.d(TAG, "onCapabilitiesChanged no video capable");
                                videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public void onErrorReceived(String number, int type, int status, String reason) {
            Log.i(TAG, "onErrorReceived, number: " + number + " type: " + type
                    + " reason: " + reason);
            if (number.equals(mFormatNumber)) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Log.i(TAG, "onErrorReceived run");
                        ImageView actionButtion = mHostHolder.primaryActionButtonView;
                        if (mHostHolder.callbackAction == CallbackAction.IMS_VIDEO) {
                            Log.d(TAG, "onErrorReceived actionButtion set callback to voice");
                            if (!CallUtil.isVideoEnabled(mActivity)) {
                                Log.d(TAG,
                                        "customizePrimaryActionButton isVideoEnabled false, set gone");
                                actionButtion.setVisibility(View.GONE);
                            } else {
                                Log.d(TAG,
                                        "customizePrimaryActionButton isVideoEnabled true, set visible");
                                actionButtion.setVisibility(View.VISIBLE);
                                actionButtion.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            }
                        }

                        View videoButton = mHostHolder.videoCallButtonView;
                        if (isShowAction && isVisible(videoButton)) {
                            videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            Log.d(TAG, "onErrorReceived videoButton setAlpha 0.3f");
                        }
                        if (mFragment.getActivity().isResumed()) {
                            Log.i(TAG, "onErrorReceived,show toast");
                            Toast.makeText(mFragment.getActivity(), reason,
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }

        //for test
    /*private boolean testFlag = true;
    private boolean capability = true;

        public void onCapabilitiesChangedTest() {
            capability = !capability;
            Log.i(TAG, "onCapabilitiesChangedTest run capability:" + capability);
            if (mIsDetached) {
                Log.d(TAG, "OPCallLogListItemViewHolder: onCapabilitiesChanged: detach return");
                return;
            }
            if (mHostHolder == null) {
                Log.d(TAG, "OPCallLogListItemViewHolder: mHostHolder is null, return");
                return;
            }

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mHostHolder == null) {
                        return;
                    }
                    Log.d(TAG, "onCapabilitiesChanged runnable:");
                    ImageView videoAction = mHostHolder.primaryActionButtonView;
                    if (mHostHolder.callbackAction == CallbackAction.IMS_VIDEO) {
                        if (testFlag) {
                            Log.d(TAG,
                                    "customizePrimaryActionButton isVideoEnabled false, set gone");
                            videoAction.setVisibility(View.GONE);
                        } else {
                            videoAction.setVisibility(View.VISIBLE);
                            if (capability) {
                                Log.d(TAG,
                                        "onCapabilitiesChanged has video capable");
                                videoAction.setAlpha(ALPHA_OPAQUE_VALUE);
                            } else {
                                Log.d(TAG, "onCapabilitiesChanged no video capable,SET GONE");
                                videoAction.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            }
                        }
                    }

                    View videoButton = mHostHolder.videoCallButtonView;
                    if (isShowAction && isVisible(videoButton)) {
                        if (capability) {
                            Log.d(TAG, "onCapabilitiesChanged has video capable");
                            videoButton.setAlpha(ALPHA_OPAQUE_VALUE);
                        } else {
                            Log.d(TAG, "onCapabilitiesChanged no video capable");
                            videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                        }
                    }
                }
            });

            // /for test begine
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onErrorReceivedTest runnable:");
                    onErrorReceivedTest();
                }
            }, 8000);
        }

        public void onErrorReceivedTest() {
            if (mIsDetached) {
                Log.d(TAG, "OPCallLogListItemViewHolder: onErrorReceivedTest: detach return");
                return;
            }
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "onErrorReceivedTest run testFlag:" + testFlag);
                    ImageView actionButtion = mHostHolder.primaryActionButtonView;
                    if (mHostHolder.callbackAction == CallbackAction.IMS_VIDEO) {
                        Log.d(TAG, "onErrorReceived actionButtion set callback to voice");
                        if (testFlag) {
                            Log.d(TAG,
                                    "customizePrimaryActionButton isVideoEnabled false, set gone");
                            actionButtion.setVisibility(View.GONE);
                        } else {
                            Log.d(TAG,
                                    "customizePrimaryActionButton isVideoEnabled true, set visible");
                            actionButtion.setVisibility(View.VISIBLE);
                        }
                    }

                    View videoButton = mHostHolder.videoCallButtonView;
                    if (isShowAction && isVisible(videoButton)) {
                        videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                        Log.d(TAG, "onErrorReceived videoButton setAlpha 0.3f");
                    }
                    if (mFragment.getActivity().isResumed()) {
                        Log.i(TAG, "onErrorReceived,show toast");
                        Toast.makeText(mFragment.getActivity(), "test",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            });
            testFlag = !testFlag;
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "onCapabilitiesChangedTest runnable:");
                    onCapabilitiesChangedTest();
                }
            }, 8000);
        }*///for test end
    }
}
