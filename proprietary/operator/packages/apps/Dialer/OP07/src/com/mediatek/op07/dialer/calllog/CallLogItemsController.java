package com.mediatek.op07.dialer.calllog;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.android.dialer.app.calllog.IntentProvider;
import com.android.dialer.calllogutils.CallbackActionHelper.CallbackAction;

import com.mediatek.op07.presence.ContactNumberUtils;
import com.mediatek.op07.presence.PresenceApiManager;
import com.mediatek.op07.presence.PresenceApiManager.CapabilitiesChangeListener;
import com.mediatek.op07.presence.PresenceApiManager.ContactInformation;

import java.util.HashMap;
import java.util.Map;

public class CallLogItemsController {
    private static final String TAG = "CallLogItemsController";
    private static final float ALPHA_TRANSPARENT_VALUE = 0.3f;
    private static final float ALPHA_OPAQUE_VALUE = 1.0f;
    private PresenceApiManager mTapi = null;

    private Fragment mFragment;
    private Map<CallLogListItemViewHolder, OPCallLogListItemViewHolder> mCallLogMap =
            new HashMap<CallLogListItemViewHolder, OPCallLogListItemViewHolder>();

    public CallLogItemsController(Fragment fragment) {
        mFragment = fragment;
        if (PresenceApiManager.initialize(mFragment.getActivity())) {
            mTapi = PresenceApiManager.getInstance();
        }
    }

    public void addCallLogViewHolder(CallLogListItemViewHolder hostHolder) {
        Log.d(TAG, "addCallLogViewHolder: " + hostHolder);
        if (mCallLogMap.containsKey(hostHolder)) {
            throw new RuntimeException("ViewHolder: " + hostHolder
                    + " can be initialized only one time");
        }
        OPCallLogListItemViewHolder opHolder = new OPCallLogListItemViewHolder(hostHolder);
        opHolder.customizePrimaryActionButton(hostHolder.callbackAction);
        mCallLogMap.put(hostHolder, opHolder);
    }

    public void removeCallLogViewHolder(CallLogListItemViewHolder hostHolder) {
        Log.d(TAG, "onCallLogListItemViewHolderDetached: " + hostHolder);
        OPCallLogListItemViewHolder viewHolder = mCallLogMap.remove(hostHolder);
        if (viewHolder != null) {
            viewHolder.onDetached();
        }
    }

    public void showActions(CallLogListItemViewHolder holder, boolean show) {
        if (holder != null && holder.confCallNumbers != null) {
            Log.d(TAG, "showActions, conference calllog, do nothing.");
            return;
        }

        OPCallLogListItemViewHolder opHolder = mCallLogMap.get(holder);
        Log.d(TAG, "showActions, opHolder:" + opHolder);
        if (opHolder != null) {
            opHolder.showActions(show);
        }
    }

    public void clear() {
        Log.d(TAG, "clear");
        mFragment = null;
        for (CallLogListItemViewHolder viewHolder : mCallLogMap.keySet()) {
            mCallLogMap.get(viewHolder).onDetached();
        }
        mCallLogMap.clear();
        mCallLogMap = null;
    }

    class OPCallLogListItemViewHolder implements CapabilitiesChangeListener {
        private CallLogListItemViewHolder mHostHolder;
        private String mFormatNumber;
        private boolean mIsDetached = false;
        private Handler mHandler = null;
        private boolean isShowAction = false;

        public OPCallLogListItemViewHolder(CallLogListItemViewHolder host) {
            mHostHolder = host;
            Log.d(TAG, "OPCallLogListItemViewHolder: number = " + host.number);
            if (mTapi != null) {
                mTapi.addCapabilitiesChangeListener(this);
            }
            mHandler = new Handler(Looper.getMainLooper());
        }

        private boolean isVisible(View view) {
            return view != null && view.getVisibility() == View.VISIBLE;
        }

        private void showActions(boolean show) {
            Log.d(TAG, "OPCallLogListItemViewHolder showActions show: " + show);
            isShowAction = show;
            if (isShowAction) {
                View videoButton = mHostHolder.videoCallButtonView;
                if (isVisible(videoButton) && mTapi != null) {
                    if (mTapi.isVideoCallCapable(mHostHolder.number)) {
                        Log.d(TAG, "OPCallLogListItemViewHolder showAction has video capable");
                        videoButton.setAlpha(ALPHA_OPAQUE_VALUE);
                    } else {
                        Log.d(TAG, "OPCallLogListItemViewHolder showAction no video capable");
                        videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                    }
                } else {
                    Log.d(TAG, "OPCallLogListItemViewHolder showAction  mTapi:"
                            + mTapi + " isVisible(videoButton): " + isVisible(videoButton));
                }

                mFormatNumber = ContactNumberUtils.getDefault()
                        .getFormatNumber(mHostHolder.number);
                Log.d(TAG, "showAction: mFormatNumber = " + mFormatNumber);
                if (mTapi != null) {
                    mTapi.requestContactPresence(mFormatNumber, false);
                }
            }
        }

        private void customizePrimaryActionButton(int callbackAction) {
            ImageView actionButtion = mHostHolder.primaryActionButtonView;
            if (callbackAction == CallbackAction.IMS_VIDEO) {
                if (mTapi != null
                        && mTapi.isVideoCallCapable(mHostHolder.number)) {
                    Log.d(TAG, "customizePrimaryActionButton has video capable");
                    actionButtion.setAlpha(ALPHA_OPAQUE_VALUE);
                } else {
                    actionButtion.setAlpha(ALPHA_TRANSPARENT_VALUE);
                    Log.d(TAG, "customizePrimaryActionButton no video capable");
                }
                mFormatNumber = ContactNumberUtils.getDefault()
                        .getFormatNumber(mHostHolder.number);
                Log.d(TAG, "OP07CallLogListItemViewHolder: mFormatNumber = " + mFormatNumber);
                if (mTapi != null) {
                    mTapi.requestContactPresence(mFormatNumber, false);
                }
            } else if (isVisible(actionButtion)) {
                actionButtion.setAlpha(ALPHA_OPAQUE_VALUE);
            }

            // /for test
            /*
             * mHandler.postDelayed(new Runnable() {
             * 
             * @Override public void run() { Log.d(TAG,
             * "customizePrimaryActionButton runnable:");
             * onCapabilitiesChangedTest(); } }, 3000);
             */
        }

        private void onDetached() {
            mIsDetached = true;
            if (mTapi != null) {
                mTapi.removeCapabilitiesChangeListener(this);
            }
            mHostHolder = null;
            mHandler = null;
        }

        @Override
        public void onCapabilitiesChanged(String contact,
                ContactInformation info) {
            if (mIsDetached) {
                Log.d(TAG, "OPCallLogListItemViewHolder: onCapabilitiesChanged: detach return");
                return;
            }
            if (mHostHolder == null) {
                Log.d(TAG, "OPCallLogListItemViewHolder: mHostHolder is null, return");
                return;
            }

            if (contact.equals(mFormatNumber) && info != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mHostHolder == null) {
                            return;
                        }
                        Log.d(TAG, "onCapabilitiesChanged runnable:");
                        if (mHostHolder.callbackAction == CallbackAction.IMS_VIDEO) {
                            ImageView videoAction = mHostHolder.primaryActionButtonView;
                            if (info.isVideoCapable) {
                                Log.d(TAG, "onCapabilitiesChanged has video capable");
                                videoAction.setAlpha(ALPHA_OPAQUE_VALUE);
                            } else {
                                Log.d(TAG, "onCapabilitiesChanged no video capable");
                                videoAction.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            }
                        }

                        View videoButton = mHostHolder.videoCallButtonView;
                        if (isShowAction && isVisible(videoButton)) {
                            if (info.isVideoCapable) {
                                Log.d(TAG, "onCapabilitiesChanged has video capable");
                                videoButton.setAlpha(ALPHA_OPAQUE_VALUE);
                            } else {
                                Log.d(TAG, "onCapabilitiesChanged no video capable");
                                videoButton.setAlpha(ALPHA_TRANSPARENT_VALUE);
                            }
                        }
                    }
                });
            }
        }
    }
}
