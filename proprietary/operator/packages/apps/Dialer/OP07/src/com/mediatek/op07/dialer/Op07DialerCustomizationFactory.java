package com.mediatek.op07.dialer;

import android.content.Context;

import com.mediatek.dialer.ext.ICallDetailExtension;
import com.mediatek.dialer.ext.ICallLogExtension;
import com.mediatek.dialer.ext.IDialPadExtension;
import com.mediatek.dialer.ext.OpDialerCustomizationFactoryBase;
import com.mediatek.op07.dialer.calllog.Op07CalllogExtension;
import com.mediatek.op07.dialer.calllog.Op07CallDetailExtension;

public class Op07DialerCustomizationFactory extends OpDialerCustomizationFactoryBase {
    private Context mContext;

    public Op07DialerCustomizationFactory(Context context) {
        mContext = context;
    }

    public IDialPadExtension makeDialPadExt() {
        return new Op07DialPadExtension(mContext);
    }

    public ICallLogExtension makeCallLogExt() {
        return new Op07CalllogExtension(mContext);
    }

    @Override
    public ICallDetailExtension makeCallDetailExt() {
        return new Op07CallDetailExtension(mContext);
    }
}
