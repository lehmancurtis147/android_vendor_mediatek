package com.mediatek.op01.dialer;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;

import com.android.dialer.app.calllog.CallLogListItemViewHolder;
import com.mediatek.op01.dialer.PhoneStateUtils;

public class ItemsController
        implements PhoneStateUtils.OnChangedListener {
    private static final String TAG = "ItemsController";

    protected Activity mActivity;
    protected PhoneStateUtils mPhoneStateUtils;
    protected View            mVideoView;

    protected static final int CALLBACK_TYPE_NONE = 0;
    protected static final int CALLBACK_TYPE_IMS_VIDEO = 1;
    protected static final int CALLBACK_TYPE_DUO = 2;
    protected static final int CALLBACK_TYPE_VOICE = 3;
    protected static final int CALLBACK_TYPE_CONFERENCE = 4;

    protected final static int SHORTCUT_DIRECT_CALL = 0;
    protected final static int SHORTCUT_CREATE_NEW_CONTACT = 1;
    protected final static int SHORTCUT_ADD_TO_EXISTING_CONTACT = 2;
    protected final static int SHORTCUT_SEND_SMS_MESSAGE = 3;
    protected final static int SHORTCUT_MAKE_VIDEO_CALL = 4;
    protected final static int SHORTCUT_BLOCK_NUMBER = 5;

    protected static final boolean DEBUG = ("eng".equals(Build.TYPE) ||
                                        "userdebug".equals(Build.TYPE));

    public ItemsController(Fragment fragment) {
        Activity activity = fragment.getActivity();
        if (activity != null) {
            mActivity = activity;
            Context applicationContext = activity.getApplicationContext();
            mPhoneStateUtils = PhoneStateUtils.getInstance(applicationContext);
            mPhoneStateUtils.addPhoneStateListener(this);
        }
    }

    public ItemsController(Activity activity) {
        if (activity != null) {
            mActivity = activity;
            Context applicationContext = activity.getApplicationContext();
            mPhoneStateUtils = PhoneStateUtils.getInstance(applicationContext);
            mPhoneStateUtils.addPhoneStateListener(this);
        }
    }

    @Override
    public void onCallStatusChange(final int state) {
        Activity activity = getCurrentActivity();
        if (activity != null) {
            activity.runOnUiThread( new Runnable() {
                    public void run() {
                        updateVideoAction(state);
                        updateCallListViewHolders(state);
                    }});
        }
    }

    protected Activity getCurrentActivity() {
        return mActivity;
    }

    public void customizeVideoItem(View view, int type) {
        if (!isTypeVideo(type)) {
            return;
        }

        mVideoView = view;
        updateVideoAction(mPhoneStateUtils.getPhoneState());
    }

    public void reInitInfo(Fragment fragment) {
        Activity activity = fragment.getActivity();
        if (activity != null) {
            Log.d(TAG, "reInitInfo.");
            mActivity = activity;
            Context applicationContext = activity.getApplicationContext();
            mPhoneStateUtils = PhoneStateUtils.getInstance(applicationContext);
            mPhoneStateUtils.addPhoneStateListener(this);
        }
    }

    public void showAction(CallLogListItemViewHolder holder, boolean show) {
        /// Do nothing
    }

    public void addCallLogViewHolder(CallLogListItemViewHolder holder) {
        /// Do nothing
    }

    public void removeCallLogViewHolder(CallLogListItemViewHolder holder) {
        /// Do nothing
    }

    protected boolean isTypeVideo(int type) {
        return false;
    }

    protected void updateCallListViewHolders(int state) {
        /// Do nothing
    }

    protected void updateVideoAction(int state) {
        if (mVideoView == null) {
            Log.d(TAG, "updateVideoAction, view is null");
            return;
        }

        if (state != TelephonyManager.CALL_STATE_IDLE) {
            Log.d(TAG, "updateShortcutView set view as GONE.");
            mVideoView.setVisibility(View.GONE);
        } else {
            if (mVideoView.getVisibility() == View.GONE) {
                Log.d(TAG, "updateShortcutView set view as VISIBLE.");
                mVideoView.setVisibility(View.VISIBLE);
                mVideoView.invalidate();
            }
        }
    }

    public void clear() {
        mPhoneStateUtils.removePhoneStateListener(this);
        mPhoneStateUtils = null;
        mVideoView = null;
        mActivity = null;
    }
}