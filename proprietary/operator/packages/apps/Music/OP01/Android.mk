LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := $(call all-java-files-under, src) \
	src/com/android/music/IMediaPlaybackService.aidl \
	src/com/mediatek/bluetooth/avrcp/IBTAvrcpMusic.aidl \
	src/com/mediatek/bluetooth/avrcp/IBTAvrcpMusicCallback.aidl
LOCAL_STATIC_JAVA_LIBRARIES := \
    android-support-v4
LOCAL_PACKAGE_NAME := OP01Music
LOCAL_OVERRIDES_PACKAGES := Music
LOCAL_OVERRIDES_PACKAGES += MusicBspPlus

ifeq ($(strip $(MTK_CIP_SUPPORT)),yes)
LOCAL_MODULE_PATH := $(TARGET_CUSTOM_OUT)/app
else
LOCAL_MODULE_PATH := $(PRODUCT_OUT)/system/app
endif

LOCAL_CERTIFICATE := platform
LOCAL_PRIVATE_PLATFORM_APIS := true

LOCAL_PROGUARD_FLAG_FILES := proguard.flags

include $(BUILD_PACKAGE)
