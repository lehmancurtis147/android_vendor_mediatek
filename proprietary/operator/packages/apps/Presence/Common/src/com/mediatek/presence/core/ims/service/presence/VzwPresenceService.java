package com.mediatek.presence.core.ims.service.presence;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

import com.mediatek.presence.core.CoreException;
import com.mediatek.presence.core.ims.ImsModule;
import com.mediatek.presence.platform.AndroidFactory;
import com.mediatek.presence.core.ims.service.ImsService;
import com.mediatek.presence.utils.logger.Logger;

public class VzwPresenceService extends PresenceService{

    private Context mContext;
    public Logger logger = Logger.getLogger(this.getClass().getName());

    private HandlerThread mHandlerThread;
    private VzwPublishManager mVzwPublishManager = null;

    public VzwPresenceService(ImsModule imsModule) throws CoreException {
        super(imsModule);

        mContext = AndroidFactory.getApplicationContext();
        mHandlerThread = new HandlerThread("VzwPresenceService");
        mHandlerThread.start();
        mVzwPublishManager = new VzwPublishManager(
                    VzwPublishManager.class.getSimpleName(),
                    new Handler(mHandlerThread.getLooper()),
                    imsModule);
    }

    @Override
    public synchronized void start() {
        logger.debug("start()");
        mVzwPublishManager.startService();
    }

    /**
     * Stop the IMS service
     */
    @Override
    public synchronized void stop() {
        logger.debug("stop()");
        mVzwPublishManager.stopService();
    }

    /**
     * Check the IMS service
     */
    @Override
    public void check() {
    }

    @Override
    public void onForbiddenReceived(String reason) {
        mVzwPublishManager.onForbiddenReceived(reason);
    }

    @Override
    public void onNotProvisionedReceived() {
        mVzwPublishManager.onNotProvisionedReceived();
    }
}
