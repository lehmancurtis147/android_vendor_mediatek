package com.mediatek.presence.core.ims.service.presence.extension;

import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.presence.platform.AndroidFactory;
import com.mediatek.presence.provider.settings.RcsSettings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.telephony.SubscriptionManager;
import android.telephony.ServiceState;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;


public class ViLTEExtension extends PresenceExtension {

    public final String TAG = "ViLTEExtension";
    private Context mContext = AndroidFactory.getApplicationContext();
    private volatile boolean mVoiceSupported;
    private volatile boolean mVideoSupported;
    private volatile boolean mDuplexSupported;
    private int mCurrentNetworkType;
    private int mCurrentMobileNetworkClass;

    public ViLTEExtension() {
        super(PresenceExtension.EXTENSION_VILTE);
        extensionName = TAG;
        mVoiceSupported = false;
        mVideoSupported = false;
        mDuplexSupported = false;
    }


    @Override
    public void attachExtension(PresenceExtensionListener listener) {
        // TODO Auto-generated method stub
        logger.debug("attachExtension : " +extensionName);

       //intialize the value
        super.attachExtension(listener);

        mCurrentNetworkType = getNetworkType();
        logger.debug("currentNetworkType : " + mCurrentNetworkType);

        //init value.
        mVoiceSupported = RcsSettings.getInstance().isIR92VoiceCallSupported();
        mVideoSupported = RcsSettings.getInstance().isIR94VideoCallSupported();
        mDuplexSupported = mVoiceSupported && mVideoSupported;

        //workaround for 3G video tag.
        if (mCurrentNetworkType == TelephonyManager.NETWORK_TYPE_LTE ||
                mCurrentNetworkType == TelephonyManager.NETWORK_TYPE_IWLAN) {
            logger.debug("set mVideoSupported = true");
            mVideoSupported = true;
        } else {
            logger.debug("set mVideoSupported = false");
            mVideoSupported = false;
        }

        logger.debug("init volte: " + mVoiceSupported + " vilte: "
                + mVideoSupported + " duplex: " + mDuplexSupported);
    }

    @Override
    public void detachExtension() {
        logger.debug("detachExtension : " +extensionName);
        // TODO Auto-generated method stub
        super.detachExtension();
    }

    public void handleNetworkChanged(int networkType) {
        logger.debug("handleNetworkChanged:" + networkType);

        if (mCurrentNetworkType == networkType)
            return;
        if (networkType <= 2) {
            //2/3G
            mVoiceSupported = false;
            mVideoSupported = false;
            mDuplexSupported = false;
        } else if (networkType >= 3) {
            //LTE/Wifi
            mVoiceSupported = true;
            mVideoSupported = true;
            mDuplexSupported = true;
        }
        mCurrentNetworkType = networkType;
    }

    public void handleCapabilityChanged(boolean enabled) {
        logger.debug("handleCapabilityChanged capabilities: " + enabled);
        mVideoSupported = enabled;
        mDuplexSupported = enabled;
    }

    public boolean isVoiceCallSupported() {
        logger.debug("isVoiceCallSupported:" + mVoiceSupported);
        return mVoiceSupported;
    }

    public boolean isVideoCallSupported() {
        logger.debug("isVideoCallSupported:" + mVideoSupported);
        return mVideoSupported;
    }

    public boolean isDuplexSupported() {
        logger.debug("isDuplexSupported:" + mDuplexSupported);
        return mDuplexSupported;
    }

    @Override
    public void notifyListener() {
        // TODO Auto-generated method stub
        super.notifyListener();
    }

    private int getNetworkType() {
        TelephonyManager tm =
                (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        return tm.getNetworkType();
    }

    private static int getMainCapabilityPhoneId() {
    int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
    return phoneId;
    }
}
