package com.mediatek.presence.core.ims.rcsua;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Handler;
import android.os.HwBinder;
import android.os.Message;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsReasonInfo;

import com.android.ims.ImsConnectionStateListener;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.ims.ImsServiceClass;

import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.presence.core.ims.network.sip.FeatureTags;
import com.mediatek.presence.core.ims.protocol.sip.SipException;
import com.mediatek.presence.platform.AndroidFactory;
import com.mediatek.presence.platform.network.NetworkFactory;
import com.mediatek.presence.provider.settings.RcsSettings;
import com.mediatek.presence.provider.settings.RcsSettingsData;

import java.lang.Thread.State;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;
import java.util.List;
import java.util.Scanner;
import java.io.InterruptedIOException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.io.DataInputStream;
import java.io.BufferedOutputStream;
import javax2.sip.ListeningPoint;
import com.android.ims.ImsConfig;
import com.mediatek.presence.utils.logger.Logger;
import vendor.mediatek.hardware.presence.V1_0.IPresence;
import vendor.mediatek.hardware.presence.V1_0.IPresenceIndication;

//import com.mediatek.ims.WfcReasonInfo;

/**
 * The Class RcsUaAdapter.
 */
public class RcsUaAdapter {

    private static int MSG_ID_FOR_RCS_PROXY_TEST = 1;
    private static int MSG_ID_FOR_RCS_PROXY_TEST_RSP = 2;

    private static final int BASE = 20000;
    /* request ims registartion info */
    public static final int CMD_REQ_REG_INFO            = BASE + 1;
    /* response ims registartion info */
    public static final int RSP_REQ_REG_INFO            = BASE + 2;

    /* request VoLTE stack for IMS Registration */
    public static final int CMD_IMS_REGISTER            = BASE + 3;
    /* intermediate response for IMS Registration */
    public static final int RSP_IMS_REGISTERING         = BASE + 4;
    /* final response of IMS Registration */
    public static final int RSP_IMS_REGISTER            = BASE + 5;

    /* request VoLTE stack for IMS Deregistration */
    public static final int CMD_IMS_DEREGISTER          = BASE + 6;
    /* intermediate response for IMS Deregistration */
    public static final int RSP_IMS_DEREGISTERING       = BASE + 7;
    /* final response of IMS Deregistration */
    public static final int RSP_IMS_DEREGISTER          = BASE + 8;

    /* send SIP request */
    public static final int CMD_SEND_SIP_MSG            = BASE + 9;
    /* SIP message event, including a request response or a SIP event */
    public static final int RSP_EVENT_SIP_MSG           = BASE + 10;

    /* indicate that IMS is de-registering */
    public static final int EVENT_IMS_DEREGISTER_IND    = BASE + 11;
    /* confirm the unpublish is completed */
    public static final int CMD_UNPUBLISH_COMPLETED     = BASE + 12;

    /* request VoLTE stack for IMS recover registration */
    public static final int CMD_IMS_RECOVER_REGISTER_IND     = BASE + 13;

    public static final String ACTION_IMS_DEREG_START = "android.intent.presence.IMS_DEREG_START";
    public static final String ACTION_IMS_DEREG_UNPUBLISH_DONE = "android.intent.presence.IMS_DEREG_UNPUBLISH_DONE";
    public static final String ACTION_IMS_RECOVER_REGISTER = "android.intent.presence.IMS_RECOVER_REGISTER";
    public static final String EXTRA_DEREG_ID = "android:imsDeregId";

    /* align with VoLTE_Event_Reg_State_e */
    public final static int IMS_REG_STATE_REGISTERED        = 1;
    public final static int IMS_REG_STATE_UNREGISTERED      = 2;
    public final static int IMS_REG_STATE_REGISTERING       = 3;
    public final static int IMS_REG_STATE_DEREGISTERING     = 4;
    public final static int IMS_REG_STATE_DISCONNECTED      = 5;
    public final static int IMS_REG_STATE_AUTHENTICATING    = 6;
    public final static int IMS_REG_STATE_OOS               = 7;
    public final static int IMS_REG_STATE_CONNECTING        = 8;

    private final static int OWNER_INVALID      = -1;
    private final static int OWNER_VOLTE_STACK  = 0;
    private final static int OWNER_ROI_STACK    = 1;

    private static boolean  hackyFlag = false;

    //IMS specific information
    private static int mOwner = OWNER_INVALID;
    private static String IMSProxyAddr = "";
    private static int IMSProxyPort = 0;
    private static String SIPDefaultProtocolForVoLTE = "UDP";
    private static String mLocalIPAddr = "";
    private static String mPrivateID = "";
    private static String mUserAgent = "";
    private static String mAssociatedUri = "";
    private static String mRoute = "";
    private static String[] mRoutes = {};
    private static String[] mAssociatedUris = {};
    private static String mInstanceId = "";
    private static int mTagStatus = 0;
    private static int mTagState = 0;
    private static String mHomeDomain = "invalid";
    private static int mVirtualLineCount = 0;
    private static String mPidentifier;
    private static String mPpa;
    /* Presence HIDL */
    private volatile IPresence mPresenceHal;
    private final IPresenceDeathRecipient mIPresenceDeathRecipient = new IPresenceDeathRecipient();
    private final AtomicLong mPresenceHalCookie = new AtomicLong(0);
    private PresenceIndication mPresenceIndication = new PresenceIndication();

    public String[] getmAssociatedUri() {
        return mAssociatedUris;
    }

    public String getHomeDomain() {
        return mHomeDomain;
    }

    public static String getmInstanceId() {
        return mInstanceId;
    }

    public String[] getmRoute() {
        return mRoutes;
    }

    private static int RCSSIPStackPort = 0;
    public static final String VOLTE_SERVICE_NOTIFY_INTENT = "COM.MEDIATEK.PRESENCE.IMS.VOLTE_SERVICE_NOTIFICATION";
    private Object mListenerLock = new Object();
    private Logger logger = Logger.getLogger("PresenceUaAdapter");

    /**
     * The Class RcsUaEvent.
     */
    public static class RcsUaEvent {
        public static final int MAX_DATA_LENGTH = 70960;
        private int requestId;
        private int dataLen;
        private int readOffset;
        private byte data[];
        private int eventmaxdataLen = MAX_DATA_LENGTH;

        /**
         * Instantiates a new rcs ua event.
         *
         * @param rid the request id
         */
        public RcsUaEvent(int rid) {
            requestId = rid;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        /**
         * Instantiates a new rcs ua event.
         *
         * @param rid the rid
         * @param length the length
         */
        public RcsUaEvent(int rid, int length) {
            requestId = rid;
            eventmaxdataLen = length;
            data = new byte[eventmaxdataLen];
            dataLen = 0;
            readOffset = 0;
        }

        /**
         * Put int.
         *
         * @param value the value
         * @return the int
         */
        public int putInt(int value) {
            if (dataLen > eventmaxdataLen - 4) {
                return -1;
            }

            synchronized (this) {
                for (int i = 0; i < 4; ++i) {
                    data[dataLen] = (byte) ((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }
            return 0;
        }

        /**
         * Put short.
         *
         * @param value the value
         * @return the int
         */
        public int putShort(int value) {
            if (dataLen > eventmaxdataLen - 2) {
                return -1;
            }

            synchronized (this) {
                for (int i = 0; i < 2; ++i) {
                    data[dataLen] = (byte) ((value >> (8 * i)) & 0xFF);
                    dataLen++;
                }
            }

            return 0;
        }

        /**
         * Put byte.
         *
         * @param value the value
         * @return the int
         */
        public int putByte(int value) {
            if (dataLen > eventmaxdataLen - 1) {
                return -1;
            }

            synchronized (this) {
                data[dataLen] = (byte) (value & 0xFF);
                dataLen++;
            }

            return 0;
        }

        /**
         * Put string.
         *
         * @param str the str
         * @param len the len
         * @return the int
         */
        public int putString(String str, int len) {


            if (dataLen > eventmaxdataLen - len) {
                return -1;
            }

            synchronized (this) {
                byte s[] = str.getBytes();
                if (len < str.length()) {
                    System.arraycopy(s, 0, data, dataLen, len);
                    dataLen += len;
                } else {
                    int remain = len - str.length();
                    System.arraycopy(s, 0, data, dataLen, s.length);
                    dataLen += s.length;
                }
            }

            return 0;
        }

        /**
         * Put bytes.
         *
         * @param value the value
         * @return the int
         */
        public int putBytes(byte[] value) {
            int len = value.length;

            if (len > eventmaxdataLen) {
                return -1;
            }

            synchronized (this) {
                System.arraycopy(value, 0, data, dataLen, len);
                dataLen += len;
            }

            return 0;
        }

        /**
         * Gets the data.
         *
         * @return the data
         */
        public byte[] getData() {
            return data;
        }

        /**
         * Gets the data len.
         *
         * @return the data len
         */
        public int getDataLen() {
            return dataLen;
        }

        /**
         * Gets the request id.
         *
         * @return the request id
         */
        public int getRequestID() {
            return requestId;
        }

        /**
         * Gets the int.
         *
         * @return the int
         */
        public int getInt() {
            int ret = 0;
            synchronized (this) {
                ret = ((data[readOffset + 3] & 0xff) << 24
                       | (data[readOffset + 2] & 0xff) << 16
                       | (data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 4;
            }
            return ret;
        }

        /**
         * Gets the short.
         *
         * @return the short
         */
        public int getShort() {
            int ret = 0;
            synchronized (this) {
                ret = ((data[readOffset + 1] & 0xff) << 8 | (data[readOffset] & 0xff));
                readOffset += 2;
            }
            return ret;
        }

        // Notice: getByte is to get int8 type from VA, not get one byte.
        /**
         * Gets the byte.
         *
         * @return the byte
         */
        public int getByte() {
            int ret = 0;
            synchronized (this) {
                ret = (data[readOffset] & 0xff);
                readOffset += 1;
            }
            return ret;
        }

        /**
         * Gets the bytes.
         *
         * @param length the length
         * @return the bytes
         */
        public byte[] getBytes(int length) {
            if (length > dataLen - readOffset) {
                return null;
            }

            byte[] ret = new byte[length];

            synchronized (this) {
                for (int i = 0; i < length; i++) {
                    ret[i] = data[readOffset];
                    readOffset++;
                }
                return ret;
            }
        }

        /**
         * Gets the string.
         *
         * @param len the len
         * @return the string
         */
        public String getString(int len) {
            byte buf[] = new byte[len];

            synchronized (this) {
                System.arraycopy(data, readOffset, buf, 0, len);
                readOffset += len;
            }

            // to fix byte array including C string end '0x00'
            int index = 0;
            while (index < len) {
                if (buf[index] == 0x00 ||
                        buf[index] == '\0') {
                    break;
                }
                index++;
            }//while

            try {
                return (new String(buf, 0, index, "US-ASCII"));
            } catch (UnsupportedEncodingException e) {
                return (new String(buf)).trim();
            }
        }
    }

    private static final String TAG = "RcsUaAdapter";
    private static Context mContext;
    private RCSProxyEventQueue mEventQueue;
    private static RcsUaEventDispatcher mRCSUAEventDispatcher;

    private static RcsUaAdapter mInstance;
    private static boolean mIsRCSUAAdapterEnabled = false;

    private static boolean mIsImsRegistered = false;
    private static boolean mIsRegistered = false;
    private static boolean mIsRegistering = false;
    private static boolean misRATWFC = false;
    private Messenger mMessanger;

    private static boolean isServiceStarted = false;
    private boolean mIsWfc;
    private static ImsManager mImsManager;
    private ImsConnectionStateListener mImsRegListener = new ImsConnectionStateListener() {
        @Override
        public void onFeatureCapabilityChanged(int serviceClass,
                                               int[] enabledFeatures, int[] disabledFeatures) {
            mIsWfc = (enabledFeatures[ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI] ==
                      ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI);
            logger.debug("onFeatureCapabilityChanged mIsWfc = " + mIsWfc);
        }
        @Override
        public void onImsConnected(int imsRadioTech) {
            logger.debug("onImsConnected imsRadioTech=" + imsRadioTech);
            setWFCRATStatus(mIsWfc);
            if (isServiceStarted()) {
                logger.debug("Presence adapter already running");
                return;
            }
            connectUceUaProxy();
        }
        @Override
        public void onImsDisconnected(ImsReasonInfo imsReasonInfo) {
            logger.debug("onImsDisconnected imsReasonInfo=" + imsReasonInfo);
        }
    };

    /**
     * Gets the single instance of RcsUaAdapter.
     *
     * @return single instance of RcsUaAdapter
     */
    public static RcsUaAdapter getInstance() {
        return mInstance;
    }

    /**
     * Creates the instance.
     *
     * @param context the context
     */
    public static synchronized void createInstance(Context context) {
        if (mInstance == null) {
            mInstance = new RcsUaAdapter(context);
        }

        if (mImsManager == null) {
            mImsManager = ImsManager.getInstance(context, getMainCapabilityPhoneId());
        }
    }

    /**
     * Instantiates a new rcs ua adapter.
     *
     * @param context the context
     */
    private RcsUaAdapter(Context context) {
        mContext = context;
        if (mInstance == null) {
            mInstance = this;
            //TODO: remove this
            //RcsUaEventDispatcher -> RcsProxySipHandler -> RcsUaAdapter.getInstance()
            //crazy coding style
        }
        mEventQueue = new RCSProxyEventQueue(mInstance);
        mRCSUAEventDispatcher = new RcsUaEventDispatcher(mContext);
    }

    /**
     * Initialize.
     */
    public void initialize() {

        logger.debug("initialize");

        if (!isServiceStarted()) {
            connectUceUaProxy();
            try {
                mImsManager.addRegistrationListener(ImsServiceClass.MMTEL, mImsRegListener);
            } catch (ImsException e) {
                logger.debug("addRegistrationListener: " + e);
            }
        } else {
            logger.debug("already initialized, skip it");
        }
    }

    /**
     * terminate.
     */
    public void terminate() {

        if(!isServiceStarted()) {
            logger.debug("isServiceStarted = false, no need terminate");
            return;
        }

        try {
            //enable RCS UA
            Thread t = new Thread() {
                @Override
                public void run() {
                    disableUCEUAAdapter();
                }
            };
            t.start();

        } catch (Exception e) {

        }
    }

    public void unregisterIMSStateUpdates() {
        if(volteServiceUpListener != null) {
            synchronized (mListenerLock) {
                if(volteServiceUpListener !=null) {
                    AndroidFactory.getApplicationContext().unregisterReceiver(volteServiceUpListener);
                    volteServiceUpListener = null;
                }

            }
        }
    }


    /**
     * Battery level listener
     */
    private volatile BroadcastReceiver volteServiceUpListener = null;


    public boolean isServiceStarted() {
        return isServiceStarted;
    }

    public void setServiceStatus(boolean status) {
        logger.debug("setServiceStatus old = " + isServiceStarted + " new = " + status);
        isServiceStarted  = status;
    }

    /**
     * Disable rcsua adapter.
     *
     */
    public void disableUCEUAAdapter() {
        logger.debug("disableUCEUAAdapter");
        if (mIsRCSUAAdapterEnabled) {
            mIsRCSUAAdapterEnabled = false;
            mRCSUAEventDispatcher.disableRequest();
        }

        cleanIMSProfileDetails();
        //set the rcs adapter service as false
        setServiceStatus(false);
    }

    /**
     * Enable rcs proxy adapter.
     */
    public void connectUceUaProxy() {
        logger.debug("connectUceUaProxy");

        //set the service status to true
        setServiceStatus(true);

        for (int count = 0 ; count < 10 ; count++) {
            try {
                logger.debug("trying get presence_hal_service...(" + count + ")");
                mPresenceHal = IPresence.getService("presence_hal_service");
                if (mPresenceHal != null) {
                    /* linkToDeath */
                    mPresenceHal.linkToDeath(mIPresenceDeathRecipient,
                                             mPresenceHalCookie.incrementAndGet());

                    /* setResponseFunctions */
                    logger.debug("setResponseFunctions");
                    mPresenceHal.setResponseFunctions(mPresenceIndication);

                    /* start domain event dispatcher to recieve broadcast */
                    logger.debug("initSetup");
                    initSetup();
                    break;
                } else {
                    logger.debug("getService fail");
                    return;
                }
            } catch (RemoteException | RuntimeException e) {
                mPresenceHal = null;
                logger.debug("enable Presence hal error: " + e);
            }
            SystemClock.sleep(1000 * (count + 1));
        }
        mEventQueue.startEventQueuePolling();
    }

    private void initSetup() {
        mRCSUAEventDispatcher.enableRequest();
        mIsRCSUAAdapterEnabled = true;
        sendRegistrationInfoRequest();
    }

    /**
     * Gets the SIP event dispatcher.
     *
     * @return the SIP event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getSIPEventDispatcher() {
        return mRCSUAEventDispatcher.getSipEventDispatcher();
    }

    /**
     * Gets the registration event dispatcher.
     *
     * @return the registration event dispatcher
     */
    public RcsUaEventDispatcher.RCSEventDispatcher getRegistrationEventDispatcher() {
        return mRCSUAEventDispatcher.getRegistrationEventHandler();
    }

    /**
     * Checks if is single registration supported.
     *
     * @return true, if is single registration supported
     */
    public boolean isImsRegistered() {
        logger.debug("mIsImsRegistered : " + mIsImsRegistered);
        return mIsImsRegistered;
    }

    /**
     * Send test request.
     */
    public void sendTestRequest() {
        sendRegistrationInfoRequest();
    }

    /**
     * Write event to socket.
     *
     * @param event the event
     */
    public void writeEvent(RcsUaEvent event) {
        if (event != null) {
            mEventQueue.addEvent(RCSProxyEventQueue.OUTGOING_EVENT_MSG, event);
        }
    }

    /**
     * Send request to RCS_proxy to get the current info about the registration.
     */
    void sendRegistrationInfoRequest() {
        logger.debug("sendRegistrationInfoRequest");
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_REQ_REG_INFO);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * Send register request.
     */
    private void sendRegisterRequest() {
        logger.debug("sendRegisterRequest with Feature tag :  "
                     + getRCSFeatureTag());
        String rcsCapabilityFeatureTags = getRCSFeatureTag();
        int length = rcsCapabilityFeatureTags.length();
        RcsUaEvent event = new RcsUaAdapter.RcsUaEvent(
            CMD_IMS_REGISTER);
        event.putString(rcsCapabilityFeatureTags, length - 1);
        sendMsgToRCSUAProxy(event);
    }

    /**
     * handle the registration info send by rcs_proxy and save it based on state.
     *
     * @param event the event
     */
    public void handleRegistrationInfo(RcsUaEvent event) {
        //refer _UCE_PROXY_Event_Reg_State2_ structure

        mOwner = event.getInt();   // 0: VoLTE, 1: ROI
        int state = event.getInt();
        int rcs_state = event.getInt();
        int rcs_feature = event.getInt();
        mLocalIPAddr = event.getString(64);
        IMSProxyPort = event.getInt();
        SIPDefaultProtocolForVoLTE = (event.getInt()==1)? "TCP" : "UDP"; // TCP=1,UDP=2,TCPUDP=3
        int protocolVersion = event.getInt();
        String publicUid = event.getString(256);
        mPrivateID = "" ; //TODO check why not passing this from C layer of RCS
        IMSProxyAddr = event.getString(256);
        int pcscfPort = event.getInt();
        mUserAgent = event.getString(128);
        mAssociatedUri = event.getString(512);
        mInstanceId = event.getString(128);
        mRoute = event.getString(128);
        mHomeDomain = event.getString(256);

        logger.debug("handleRegistrationInfo - owner: " + mOwner + ", reg: " + state);

        if(mUserAgent == null || mUserAgent.length() < 2) {
            mUserAgent = "Default-RCS-UA";
        }

        mAssociatedUris = mAssociatedUri.split(",");
        mRoutes = mRoute.split(",");
        mVirtualLineCount = event.getInt();
        mPidentifier = event.getString(256);
        mPpa = event.getString(4096);

        logger.debug("rcs_state: " + rcs_state);
        logger.debug("rcs_feature: " + rcs_feature);
        logger.debug("localaddress:" + mLocalIPAddr);
        logger.debug("local port:" + IMSProxyPort);
//        logger.debug("protocolType:" + protocolType);
        logger.debug("protocolVersion:" + protocolVersion);
        logger.debug("publicUid:" + publicUid);
        logger.debug("privateUid:" + mPrivateID);
        logger.debug("pcscfAddress:" + IMSProxyAddr);
        logger.debug("pcscfPort:" + pcscfPort);
        logger.debug("userAgent:" + mUserAgent);
        logger.debug("associated Uri: " + mAssociatedUri);
        logger.debug("Instance id: " + mInstanceId);
        logger.debug("Route: "+ mRoute);
        logger.debug("Home domain: "+ mHomeDomain);
        logger.debug("Virtual Line Count: " + mVirtualLineCount);
        logger.debug("PIDENTIFIER: " + mPidentifier);
        logger.debug("PPA: " + mPpa);

        if (state == IMS_REG_STATE_REGISTERED) {
            if (!mIsImsRegistered) {
                mIsImsRegistered = true;
                logger.debug("broadcastVolteService for IMS registered");
                broadcastVolteService();
            }
        } else if (state == IMS_REG_STATE_DISCONNECTED ||
                state == IMS_REG_STATE_UNREGISTERED ||
                state == IMS_REG_STATE_OOS) {
            if (mIsImsRegistered) {
                mIsImsRegistered = false;
                logger.debug("broadcastVolteService for IMS unregistered");
                broadcastVolteService();
                cleanIMSProfileDetails();
            }
        } else {
            logger.debug("broadcastVolteService ignore");
        }
    }

    private void broadcastVolteService() {
        Intent intent = new Intent(RcsUaAdapter.VOLTE_SERVICE_NOTIFY_INTENT);
        intent.putExtra("owner", mOwner);
        mContext.sendBroadcast(intent);
    }
    /**
     * Send msg to dispatcher.
     *
     * @param event the event
     */
    protected void sendMsgToDispatcher(RcsUaEvent event) {
        Message msg = new Message();
        msg.obj = event;
        mRCSUAEventDispatcher.sendMessage(msg);
    }

    /**
     * Send msg to rcsua proxy.
     *
     * @param event the event
     */
    protected void sendMsgToRCSUAProxy(RcsUaEvent event) {
        if (event == null) {
            logger.debug("sendMsgToRCSUAProxy fail, event is null");
            return;
        }
        if (mPresenceHal == null) {
            logger.debug("sendMsgToRCSUAProxy fail, mPresenceHal is null");
            return;
        }
        // send the event to UA
        int requestId = event.getRequestID();
        int length = event.getDataLen();
        try {
            mPresenceHal.writeEvent(requestId, length, byteToArrayList(length, event.getData()));
        } catch (RemoteException e) {
            logger.debug("hal writeEvent e: " + e);
        }
    }

    /**
     * Gets the RCS feature tag.
     *
     * @return the RCS feature tag
     */
    protected String getRCSFeatureTag() {
        String data = "";
        //data = FeatureTags.FEATURE_RCSE;
        data = FeatureTags.FEATURE_CPM_SESSION +","+FeatureTags.FEATURE_CPM_FT + "," +FeatureTags.FEATURE_CPM_MSG;
        return data;
    }

    /**
     * Notify ims connection manager connect event.
     */
    public void notifyIMSConnectionManagerConnectEvent() {

    }

    /**
     * Gets the ims proxy addr for vo lte.
     *
     * @return the ims proxy addr for vo lte
     */
    public String[] getImsProxyAddrForVoLTE() {
        String data[] = { IMSProxyAddr };
        data[0] = IMSProxyAddr;
        return data;
    }

    /**
     * Gets the ims proxy port for vo lte.
     *
     * @return the ims proxy port for vo lte
     */
    public int[] getImsProxyPortForVoLTE() {
        int data[] = { IMSProxyPort };
        return data;
    }

    /**
     * Gets the vo lte stack ip address.
     *
     * @return the vo lte stack ip address
     */
    public String getVoLTEStackIPAddress() {
        return mLocalIPAddr;
    }


    /**
     * Gets the host address.
     *
     * @return the host address
     */
    public String getHostAddress() {
        return getVoLTEStackIPAddress();
    }

    public String getUserAgent() {
        return mUserAgent;
    }

    /**
     * Gets the SIP default protocol for vo lte.
     *
     * @return the SIP default protocol for vo lte
     */
    public String getSIPDefaultProtocolForVoLTE() {
        String data = RcsSettings.getInstance().getSipDefaultProtocolForMobile();
        //TODO FIX THIS
        data = "TCP";//String data = SIPDefaultProtocolForVoLTE;
        return data;
    }

    /**
     * Gets the IMS private id.
     *
     * @return the IMS private id
     */
    public String getIMSPrivateID() {
        String data = "";
        return data;
    }

    /**
     * Sets the RCS sip stack port.
     *
     * @param port the new RCS sip stack port
     */
    public void setRCSSipStackPort(int port) {
        RCSSIPStackPort = port;
    }

    /**
     * Gets the RCS sip stack port.
     *
     * @return the RCS sip stack port
     */
    public int getRCSSipStackPort() {
        return RCSSIPStackPort;
    }

    /**
     * Gets Digits virtual line count.
     *
     * @return Digits virtual line count
     */
    public int getVirtualLineCount() {
        return mVirtualLineCount;
    }

    /**
     * Gets Digits PIDENTIFIER header info.
     *
     * @return Digits PIDENTIFIER header info
     */
    public String getPidentifier() {
        return mPidentifier;
    }

    /**
     * Gets Digits P-Preferred-Association header info.
     *
     * @return Digits P-Preferred-Association header info
     */
    public String getPpa() {
        return mPpa;
    }

    /**
     * Checks if is IMS via wfc.
     *
     * @return true, if is IMS via wfc
     */
    public boolean isIMSViaWFC() {
        boolean isWfcRegistered = false;
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        isWfcRegistered = telephonyManager.isWifiCallingAvailable();

        return isWfcRegistered;
    }

    /*
     *  RCS can do single registration only whne
     *  1) Platform has rcs_ua support
     *  2) Operators allow single registration
     */
    public boolean isSingleRegistrationFeasible() {
        boolean status = false;
        int rcsVolteSingleRegistrationType = RcsSettings.getInstance()
                                             .getRcsVolteSingleRegistrationType();
        logger.debug("Operator single registration allowed state :  "
                     + rcsVolteSingleRegistrationType);

        if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_SINGLE_REGISTRATION_MODE) {
            logger.debug("Operator allows single registration in all states ");
            status = true;
        } else if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_ROAMING_MODE) {

            // if phone in roaming , then dont connect via single registration
            boolean isPhoneinRoaming = false;
            ConnectivityManager connectivityMgr = (ConnectivityManager) AndroidFactory.getApplicationContext().getSystemService( Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityMgr.getActiveNetworkInfo();

            if (networkInfo != null) {
                isPhoneinRoaming = networkInfo.isRoaming();
            } else {
                isPhoneinRoaming = false;
            }

            logger.debug("Phone roaming state : " + isPhoneinRoaming);
            // if in romaing , is not feasible
            if (isPhoneinRoaming) {
                logger.error("single registration not feasible in roaming state");
                status = false;
            } else {
                status = true;
            }
        } else if (rcsVolteSingleRegistrationType == RcsSettingsData.RCS_VOLTE_DUAL_REGISTRATION_MODE) {
            logger.debug("Operator doesnt allow single registration");
            status = false;
        }
        return status;
    }

    boolean isRATWFC() {
        return misRATWFC;
    }

    void setWFCRATStatus(boolean status) {
        misRATWFC = status;
    }

    /*
     * Clean IMS profile details
     */
    void cleanIMSProfileDetails() {
        logger.debug("cleanIMSProfileDetails");
        //IMS specific information
        mOwner = OWNER_INVALID;
        IMSProxyAddr = "";
        IMSProxyPort = 0;
        SIPDefaultProtocolForVoLTE = "UDP";
        mLocalIPAddr = "";
        mPrivateID = "";
        mUserAgent = "";
        RCSSIPStackPort = 0;
    }

    public boolean isRegisteredbyRoI() {
        if (mOwner == OWNER_ROI_STACK && isImsRegistered()) {
            return true;
        }
        return false;
    }

    public boolean isRegisteredbyVolte() {
        if((mTagState > 0) && isImsRegistered()) {
            return true;
        }
        return false;
    }

    public void setRegisteredbyVolte(int value) {
        mTagState= value;
    }

    private ArrayList<Byte> byteToArrayList(int length, byte [] value) {
        ArrayList<Byte> al = new ArrayList<Byte>();
        logger.debug("byteToArrayList, value.length = " + value.length + ", length = " + length);
        for(int i = 0; i < length; i++) {
            al.add(value[i]);
        }
        return al;
    }

    final class IPresenceDeathRecipient implements HwBinder.DeathRecipient {
        @Override
        public void serviceDied(long cookie) {
            // Deal with service going away
            logger.debug("Presence hal serviceDied");
            // reconnect to hidl server directly
            // let the for loop take care UA reborn timing
            connectUceUaProxy();
        }
    }

    public class PresenceIndication extends IPresenceIndication.Stub {
        private byte[] arrayListTobyte(ArrayList<Byte> data, int length) {
            byte[] byteList = new byte[length];
            for(int i = 0; i < length; i++) {
                byteList[i] = data.get(i);
            }
            logger.debug("arrayListTobyte, byteList = " + byteList);
            return byteList;
        }

        public void readEvent(ArrayList<Byte> data, int request_id, int length) {
            logger.debug("<< readEvent(" + length + "):" + " request_id = " + request_id);

            byte buf [];
            RcsUaEvent event;

            buf = arrayListTobyte(data, length);
            //drops leading bytes(request_id, length)
            //byte[] content = Arrays.copyOfRange(buf, 8, length);

            //logger.debug("--- dump data ---");
            //logger.debug(byte2Hex(buf));
            //logger.debug("--- end ---");

            event = new RcsUaEvent(request_id);
            event.putBytes(buf);

            if (event != null) {
                mEventQueue.addEvent(RCSProxyEventQueue.INCOMING_EVENT_MSG, event);
            }
        }
    }

    public String byte2Hex(byte[] b) {
        String result = "";
        for (int i=0 ; i<b.length ; i++)
            result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        return result;
    }

    private static int getMainCapabilityPhoneId() {
        int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
        return phoneId;
    }
}