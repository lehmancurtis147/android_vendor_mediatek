package com.mediatek.presence.core.ims.rcsua;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import com.mediatek.presence.utils.logger.Logger;
import com.mediatek.presence.core.ims.rcsua.RcsUaAdapter.RcsUaEvent;

/**
 * The Class RcsProxyRegistrationHandler.
 */
public class RcsProxyRegistrationHandler implements RcsUaEventDispatcher.RCSEventDispatcher {

    private Logger logger = Logger.getLogger(this.getClass().getName());

    private static final int REGISTRATION_TIMER = 1000;
    private static final int MAX_REGISTRATION_TIMER = 2000;

    private static Context mContext;
    private static final String TAG = "PresenceProxyRegistrationHandler";
    private static RcsUaAdapter mRcsuaAdapt = null;

    /**
     * Instantiates a new rcs proxy registration handler.
     *
     * @param context the context
     */
    public RcsProxyRegistrationHandler(Context context) {
        if (mRcsuaAdapt == null) {
            mContext = context;
            mRcsuaAdapt = RcsUaAdapter.getInstance();

            IntentFilter filter = new IntentFilter();
            filter.addAction(RcsUaAdapter.ACTION_IMS_DEREG_UNPUBLISH_DONE);
            filter.addAction(RcsUaAdapter.ACTION_IMS_RECOVER_REGISTER);
            mContext.registerReceiver(mBroadcastReceiver, filter);
        }
    }

    /**
     * Register.
     *
     * @return true, if successful
     */
    public boolean register() {
        logger.debug("register, Presence no need to do this");
        return true;
    }

    /**
     * Unregister.
     *
     * @return true, if successful
     */
    public boolean deRegister() {
        logger.debug( "deregister, Presence no need to do this");
        return true;
    }

    /**
     * Event callback.
     *
     * @param event the event
     * runs in RCSUA event dispatcher
     */
    public void EventCallback(RcsUaEvent event) {

        try {
            int requestId = event.getRequestID();
            switch (requestId) {
                case RcsUaAdapter.RSP_IMS_REGISTERING:
                    break;
                case RcsUaAdapter.RSP_IMS_REGISTER:
                    break;
                case RcsUaAdapter.RSP_IMS_DEREGISTER:
                    break;
                case RcsUaAdapter.RSP_REQ_REG_INFO:
                    mRcsuaAdapt.handleRegistrationInfo(event);
                    break;
                case RcsUaAdapter.EVENT_IMS_DEREGISTER_IND:
                    int deregId = event.getInt();
                    logger.debug("Handle IMS_DEREG_START event, deregId: " + deregId);
                    Intent intent = new Intent(RcsUaAdapter.ACTION_IMS_DEREG_START);
                    intent.putExtra(RcsUaAdapter.EXTRA_DEREG_ID, deregId);
                    mContext.sendBroadcast(intent);
                    break;

                default:
                    logger.debug("EventCallback unknown id:" + requestId);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Enable request.
     */
    public void enableRequest() {
    }

    /**
     * Disable request.
     */
    public void disableRequest() {
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            logger.debug(TAG + "Receive: " + action);

            if (action.equalsIgnoreCase(RcsUaAdapter.ACTION_IMS_DEREG_UNPUBLISH_DONE)) {
                int deregId = intent.getIntExtra(RcsUaAdapter.EXTRA_DEREG_ID, 1);
                logger.debug(TAG + "Receive IMS_DEREG_UNPUBLISH_DONE intent, deregId: " + deregId);

                RcsUaEvent event = new RcsUaEvent(RcsUaAdapter.CMD_UNPUBLISH_COMPLETED);
                event.putInt(deregId);
                mRcsuaAdapt.writeEvent(event);
            } else if (action.equalsIgnoreCase(RcsUaAdapter.ACTION_IMS_RECOVER_REGISTER)) {
                RcsUaEvent event = new RcsUaEvent(RcsUaAdapter.CMD_IMS_RECOVER_REGISTER_IND);
                mRcsuaAdapt.writeEvent(event);
            }
        }
    };
}
