/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.presence.utils;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PatternMatcher;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import com.android.ims.ImsConfig;
import com.android.ims.ImsConfigListener;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.presence.utils.logger.Logger;

public class ImsConfigMonitorService {
    private final static String TAG = "ImsConfigMonitorService";
    private final static String EXTRA_PHONE_ID = "phone_id";
    private final static Logger logger = Logger.getLogger(TAG);
    private Context mContext;
    private boolean mIsLvcEnabled = false;
    private int mPhoneId = 0;
    private static ImsConfigMonitorService instance = null;

    private static HashMap<FeatureValueListener, Integer> mFeatureListenerMap =
            new HashMap<FeatureValueListener, Integer>();
    private static HashMap<ProvisionedValueListener, Integer> mProvisionedListenerMap =
            new HashMap<ProvisionedValueListener, Integer>();

    public interface FeatureValueListener {
        void onFeatureValueChanged(int feature, int value);
    }

    public interface ProvisionedValueListener {
        void onProvisionedValueChanged(int provision);
    }

    public static ImsConfigMonitorService getInstance(Context context) {
        if (instance == null) {
            logger.info("ImsConfigMonitorService has been created");
            instance = new ImsConfigMonitorService(context);
        }
        return instance;
    }

    private BroadcastReceiver mFeatureValueReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }

            if (intent.getAction().equals(ImsConfig.ACTION_IMS_FEATURE_CHANGED)) {
                int feature = intent.getIntExtra(ImsConfig.EXTRA_CHANGED_ITEM, -1);
                int phoneId = intent.getIntExtra(EXTRA_PHONE_ID, -1);
                int value  = intent.getIntExtra(ImsConfig.EXTRA_NEW_VALUE, -1);
                logger.debug("onReceived IMS feature changed phoneId: "
                                + phoneId + ", feature: " + feature
                                + ", value: " + value);

                if (mFeatureListenerMap.containsValue(feature)) {
                    for(FeatureValueListener listener : mFeatureListenerMap.keySet()){
                        if(mFeatureListenerMap.get(listener).equals(feature)) {
                            listener.onFeatureValueChanged(feature, value);
                            logger.debug("feature(" + feature + ") callback triggered, value = " + value);
                        }
                    }
                }
            } else if (intent.getAction().equals(ImsConfig.ACTION_IMS_CONFIG_CHANGED)) {
                int config = intent.getIntExtra(ImsConfig.EXTRA_CHANGED_ITEM, -1);
                if (mProvisionedListenerMap.containsValue(config)) {
                    for(ProvisionedValueListener listener : mProvisionedListenerMap.keySet()){
                        if (mProvisionedListenerMap.get(listener).equals(config)) {
                            listener.onProvisionedValueChanged(config);
                            logger.debug("Provisioned value(" + config + ") callback triggered");
                        }
                    }
                }
            }
        }

        private List<FeatureValueListener> getKeysFromValue(HashMap<FeatureValueListener, Integer> map, Integer value){
            List<FeatureValueListener> list = new ArrayList<>();
            logger.debug("getKeysFromValue, value = " + value);
            for(FeatureValueListener o : map.keySet()){
                if(map.get(o).equals(value)) {
                    list.add(o);
                    logger.debug("getKeysFromValue, mapped " + value);
                }
            }
            return list;
        }
    };

    private ImsConfigMonitorService(Context context) {
        mContext = context;
        setupImsConfigMonitoring();
    }

    public boolean isLvcEnabled() {
        mIsLvcEnabled = SystemProperties.getInt("persist.mtk.vilte.enable", 0) == 1;
        logger.debug("isLvcEnabled, mIsLvcEnabled:" + mIsLvcEnabled);
        return mIsLvcEnabled;
    }

    public void registerFeatureValueListener(Integer featureKey, FeatureValueListener listener) {
        mFeatureListenerMap.put(listener, featureKey);
    }

    public void unRegisterFeatureValueListener(FeatureValueListener listener) {
        mFeatureListenerMap.remove(listener);
    }

    public void registerProvisionedValueListener(Integer provisionedKey, ProvisionedValueListener listener) {
        mProvisionedListenerMap.put(listener, provisionedKey);
    }

    public void unRegisterProvisionedValueListener(ProvisionedValueListener listener) {
        mProvisionedListenerMap.remove(listener);
    }

    private void setupImsConfigMonitoring() {
        IntentFilter filter = new IntentFilter();

        // Path: /tb_feature/phoneID/feature/networks
        String VoltePath = "/tb_feature/.*/" +
                ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_LTE + "/" +
                TelephonyManager.NETWORK_TYPE_LTE;
        String ViltePath = "/tb_feature/.*/" +
                ImsConfig.FeatureConstants.FEATURE_TYPE_VIDEO_OVER_LTE + "/" +
                TelephonyManager.NETWORK_TYPE_LTE;
        String VoWiFiPath = "/tb_feature/.*/" +
                ImsConfig.FeatureConstants.FEATURE_TYPE_VOICE_OVER_WIFI + "/" +
                TelephonyManager.NETWORK_TYPE_IWLAN;

        try {
            //For IMS provisioned value listening
            filter.addAction(ImsConfig.ACTION_IMS_CONFIG_CHANGED);
            //For IMS feature value listening
            filter.addDataPath(VoltePath, PatternMatcher.PATTERN_SIMPLE_GLOB);
            filter.addDataPath(ViltePath, PatternMatcher.PATTERN_SIMPLE_GLOB);
            filter.addDataPath(VoWiFiPath, PatternMatcher.PATTERN_SIMPLE_GLOB);
            filter.addAction(ImsConfig.ACTION_IMS_FEATURE_CHANGED);
            filter.addDataScheme("content");
            filter.addDataAuthority("com.mediatek.ims.config.provider", null);
            filter.addDataType("vnd.android.cursor.item/imsconfig");
        } catch (IntentFilter.MalformedMimeTypeException e) {
            logger.debug("setupImsConfigFeatureValueMonitoring: exception=" + e);
            return;
        }
        mContext.registerReceiver(mFeatureValueReceiver, filter);
    }
}
