/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op18.settings;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.util.Log;
import android.view.View;

import com.android.settingslib.core.AbstractPreferenceController;
import com.android.settings.wifi.tether.WifiTetherBasePreferenceController
        .OnTetherConfigUpdateListener;
import com.mediatek.settings.ext.DefaultWifiTetherSettingsExt;
import java.util.List;

/**
 * OP18 plugin implementation of WifiTetherSettingsExt feature.
 */
public class OP18WifiTetherSettingsExt extends DefaultWifiTetherSettingsExt {
    private static final String TAG = "OP18WifiTetherSettingsExt";
    private static OP18WifiTetherPreferenceController sWifiTetherPrefController = null;
    private static OP18WifiTetherSettingsUtil sWifiTetherSettingsUtil = null;
    private Context mContext;

    public OP18WifiTetherSettingsExt(Context context) {
        super(context);
        Log.i(TAG, "Constructor : context = " + context);
        mContext = context;
        sWifiTetherSettingsUtil = new OP18WifiTetherSettingsUtil(context);
    }

    @Override
    public void addPreferenceController(Context context, Object controllers, Object listener) {
        Log.i(TAG, "addPreferenceController");
        OnTetherConfigUpdateListener config_listener = (OnTetherConfigUpdateListener) listener;
        if (sWifiTetherPrefController == null) {
            sWifiTetherPrefController = new OP18WifiTetherPreferenceController(context,
                    mContext, config_listener);
        }

        List<AbstractPreferenceController> con =  (List<AbstractPreferenceController>) controllers;
        con.add(sWifiTetherPrefController);
    }

    /**
     * For RJIL WIFI TETHER feature.
     * Notify perference change (for Wifi AP Band preference).
     * @param pref_key preference key
     * @param value changed value onPreferenceChange
     */
    @Override
    public void onPrefChangeNotify(String pref_key, Object value) {
        sWifiTetherPrefController.onPrefChangeNotifyController(pref_key, value);
    }

    /**
     * Customize Wifi ap dialog view to add apChannel selection option.
     * @param context The parent context
     * @param view parent layout view
     * @param config wificonfiguration object
     */
     @Override
    public void customizeView(Context context, View view, WifiConfiguration config) {
        if (sWifiTetherSettingsUtil != null) {
            sWifiTetherSettingsUtil.customizeView(context, view, config);
        }
    }

    /**
     * Update wifiConfiguration with selected apChannel information.
     * @param config wificonfiguration object
     */
    @Override
    public void updateConfig(WifiConfiguration config) {
        if (sWifiTetherSettingsUtil != null) {
            sWifiTetherSettingsUtil.updateConfig(config);
        }
    }

    /**
     * Set update ApChannel spinner when band is changed.
     * @param apBand selected AP band
     * @param needToSet this is to check if different band is selected
     */
    public void setApChannel(int apBand, boolean needToSet) {
        if (sWifiTetherSettingsUtil != null) {
            sWifiTetherSettingsUtil.setApChannel(apBand, needToSet);
        }
    }
}
