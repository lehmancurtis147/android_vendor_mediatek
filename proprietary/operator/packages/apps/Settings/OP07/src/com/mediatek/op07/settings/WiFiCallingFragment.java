/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */


package com.mediatek.op07.settings;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsManager;

/**
 * Main WFC UI including switch and emergency address menu
 */
public class WiFiCallingFragment extends PreferenceFragment
        implements Preference.OnPreferenceChangeListener {

    private static final String TAG = "Op07WiFiCallingFragment";

    private static final String WIFICALLING_SWITCH_KEY = "toggle_wificalling";
    private static final String EMPTY_VIEW_KEY = "empty_view";
    private static final String EMERGENCY_ADDRESS_KEY = "emergency_address";

    private final boolean DISABLE_SWITCH = false;
    private final boolean ENABLE_SWITCH = true;

    public static int CALL_STATE_OVER_WFC = 0;
    public static int CALL_STATE_IDLE = 1;

    public static final String CALL_STATE = "call_state";
    public static final String NOTIFY_CALL_STATE = "OP07:call_state_Change";

    private SwitchPreference mWificallingSwitch;
    private Preference mEmergencyAddress;
    // private Preference mEmptyView;

    private WfcSettingsActivity mWfcSettingsActivity = null;

    private static int sCallState = -1;

    private final OnPreferenceClickListener mClickListener = new OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
            Log.d(TAG, "onPreferenceClick, pref:" + preference);
            if (preference == mEmergencyAddress) {
                mWfcSettingsActivity.updateLocationAndTc();
                return true;
            }
            return false;
        }
    };

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "action: " + intent.getAction());
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "isWfcEnabledByPlatform: false");
                return;
            }
            if (NOTIFY_CALL_STATE.equals(intent.getAction())) {
                int callState = intent.getIntExtra(CALL_STATE, CALL_STATE_IDLE);
                Log.v(TAG, "br call_satte: " + callState);
                if (callState == CALL_STATE_OVER_WFC) {
                    //check if wificalling is registered
                    updateState(DISABLE_SWITCH);
                } else {
                    updateState(ENABLE_SWITCH);
                }
            }
        }
    };

    /** Constructor.
     */
    public WiFiCallingFragment(WfcSettingsActivity parent) {
        mWfcSettingsActivity = parent;
    }

    public WiFiCallingFragment() {
        Log.d(TAG, "Empty constructor");
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(TAG, "onAttach activity = " + activity);
        if (activity instanceof WfcSettingsActivity) {
            if (mWfcSettingsActivity == null) {
                mWfcSettingsActivity = (WfcSettingsActivity) activity;
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.layout.wificalling_settings);

        mWificallingSwitch = (SwitchPreference) findPreference(WIFICALLING_SWITCH_KEY);
        mWificallingSwitch.setOnPreferenceChangeListener(this);
        mWificallingSwitch.setChecked(ImsManager.isWfcEnabledByUser(getActivity()));
        mWificallingSwitch.setEnabled((sCallState != CALL_STATE_OVER_WFC));
        Log.d(TAG, "onCreate sCallState =" + sCallState);
        //mEmptyView = findPreference(EMPTY_VIEW_KEY);

        mEmergencyAddress = findPreference(EMERGENCY_ADDRESS_KEY);
        mEmergencyAddress.setOnPreferenceClickListener(mClickListener);

        if (mWfcSettingsActivity != null) {
            IntentFilter filter = new IntentFilter();
            filter.addAction(WiFiCallingFragment.NOTIFY_CALL_STATE);
            Log.d(TAG, "Register receiver for call state");
            mWfcSettingsActivity.registerReceiver(mBroadcastReceiver, filter);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        /* Ask Service to turn on the switch, only if user has enabled it,
            * else keep user's choice */
        Log.d(TAG, "ON:" + mWificallingSwitch.isChecked());
        /*if (mWificallingSwitch.isChecked()) {
            mWificallingSwitch.setChecked(mWfcSettingsActivity.isWfcEntitled());
        }

        mWificallingSwitch.setTitle(mWificallingSwitch.isChecked() ? R.string.On : R.string.Off);
        mWificallingSwitch.setSummaryOff(mWfcSettingsActivity.getSummary());
        mWificallingSwitch.setSummaryOn(mWfcSettingsActivity.getSummary());


        if (mWificallingSwitch.isChecked()) {
            getPreferenceScreen().addPreference(mEmergencyAddress);
            getPreferenceScreen().removePreference(mEmptyView);
        } else {
            getPreferenceScreen().removePreference(mEmergencyAddress);
            //getPreferenceScreen().addPreference(mEmptyView);
            getPreferenceScreen().removePreference(mEmptyView);
        }*/
        updateUI(mWificallingSwitch.isChecked());
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onDestroy() {
        super.onPause();
        if (mWfcSettingsActivity != null) {
            mWfcSettingsActivity.unregisterReceiver(mBroadcastReceiver);
        }
    }

    /**
     * Implemented to support onPreferenceChangeListener to look for preference
     * changes.
     *
     * @param preference is the preference to be changed
     * @param objValue should be the value of the selection, NOT its localized
     * display value.
     * @return boolean
     */
    @Override
    public boolean onPreferenceChange(Preference preference, Object objValue) {
        Log.d(TAG, "onPreferenceChange:" + preference + ", changed to " + objValue);
        if (preference == mWificallingSwitch) {
            // TODO: do not change the switch on user click. check with service
            // if service returns true, enable else let it be off until response from
            // service is not received
            // if turn ON, enable wificallingMode & emergency preference

            boolean wfcOn = (boolean) objValue;
            Log.d(TAG, "Wfc ON:" + wfcOn);
            if (!wfcOn) {
                //Switch is turned OFF
                boolean result = mWfcSettingsActivity.updateWifiCalling(wfcOn);
                mWfcSettingsActivity.deactivateService();
                updateUI(result);
            } else {
               showLocationAlert();
               return false;
            }
        }
        return true;
    }

    /**
     * Method to be called on Ses Service events.
     * @param event like service_connected, disconnected, ses state change
     * @return
     */
    public void onEvent(int event, int state) {
        switch (event) {
            case WfcSettingsActivity.EVT_ID_SERVICE_CONNECTED:
                onServiceConnected();
                break;
            case WfcSettingsActivity.EVT_ID_SERVICE_DISCONNECTED:

                break;
            case WfcSettingsActivity.EVT_ID_SES_STATE_CHANGE:
                onSesStateChange(state);
                break;
            default:
                Log.d(TAG, "unknown event:" + event);
                break;
        }
    }

    private void onServiceConnected() {
        if (mWificallingSwitch.isChecked()) {
            mWificallingSwitch.setChecked(mWfcSettingsActivity.isWfcEntitled());
        }
        updateUI(mWificallingSwitch.isChecked());
    }

    private void onServiceDisconnected() {}

    private void onSesStateChange(int state) {
        /*int state = WfcUtils.getEntitlementStatus();*/
        Log.d(TAG, "entitlement state:" + state);
        switch (state) {
            case WfcSettingsActivity.STATE_PENDING:
                mWificallingSwitch.setSummaryOn(mWfcSettingsActivity.getSummary());
                break;
            case WfcSettingsActivity.STATE_ENTITlED:
                updateUI(true);
                break;
            case WfcSettingsActivity.STATE_NOT_ENTITLED:
                /* If service resets its state, turn WFC off. */
                updateUI(false);
                break;
            default:
                Log.d(TAG, "invalid state:");
            break;
        }
    }



    public void updateUI(final boolean enabled) {
        Log.d(TAG, "enabled:" + enabled);

        getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (enabled) {
                        getPreferenceScreen().addPreference(mEmergencyAddress);
                    }
                    else {
                        getPreferenceScreen().removePreference(mEmergencyAddress);
                    }
                    mWificallingSwitch.setChecked(enabled);
                    mWificallingSwitch.setTitle(enabled ? R.string.On : R.string.Off);
                    mWificallingSwitch.setSummaryOn(mWfcSettingsActivity.getSummary());
                    mWificallingSwitch.setSummaryOff(mWfcSettingsActivity.getSummary());
                    mWificallingSwitch.setEnabled((sCallState != CALL_STATE_OVER_WFC));
                   }
                });
    }

    private void showWifiAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mWfcSettingsActivity);
        builder.setMessage(R.string.wifi_off_message)
                .setTitle(R.string.wifi_off_title)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Toast.makeText(mWfcSettingsActivity,
                                        R.string.location_usage_info, Toast.LENGTH_SHORT).show();
                            }
                        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showLocationAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mWfcSettingsActivity);
        builder.setTitle(R.string.location_usage_info)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if (!mWfcSettingsActivity.isWifiConnected(mWfcSettingsActivity)) {
                                    Toast.makeText(mWfcSettingsActivity,
                                    R.string.wifi_off_message,
                                    Toast.LENGTH_SHORT).show();
                                }
                                boolean result = mWfcSettingsActivity.turnWfcOn();
                                updateUI(result);
                            }
                        })
                .setNegativeButton(android.R.string.cancel, null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private final void updateState(boolean switchState) {
        mWificallingSwitch.setEnabled(switchState);
    }

    public static void setCallState(int callState) {
        sCallState = callState;

    }
}
