package com.mediatek.op06.settings;

import android.content.BroadcastReceiver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.support.v14.preference.PreferenceFragment;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.ims.ImsManager;
import com.mediatek.settings.ext.DefaultWfcSettingsExt;

/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op06WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op06WfcSettingsExt";
    private static final String AOSP_BUTTON_WFC_MODE = "wifi_calling_mode";
    private Context mContext;
    private PreferenceFragment mPreferenceFragment;
    private ContentObserver mContentObserver;
    private Op06WfcSettings mWfcSettings;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "WFC is not enabled in platform");
                return;
            }
            if (CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED.equals(action)) {
                    if (mPreferenceFragment != null) {
                    handleWfcModeUpdation();
                }
            }
        }
    };

    /** Constructor.
     * @param context context
     */
    public Op06WfcSettingsExt(Context context) {
        super();
        mContext = context;
        mWfcSettings = Op06WfcSettings.getInstance(mContext);
        WifiCallingUtils.registerReceiver(mContext, mReceiver);
    }

   /**
    * Takes operator specific action on wfc list preference.
     * On Switch OFF, disable wfcModePref.
     * @param root
     * @param wfcModePref
     * @param wfcEnabled
     * @param wfcMode
        * @return
        */
    @Override
    public void updateWfcModePreference(PreferenceScreen root, ListPreference wfcModePref,
            boolean wfcEnabled, int wfcMode) {
        if (ImsManager.isWfcEnabledByPlatform(mContext)
            && WifiCallingUtils.isWifiCallingProvisioned(mContext,
                SubscriptionManager.getDefaultVoicePhoneId())) {
            Log.d(TAG, "wfc_enabled:" + wfcEnabled + " wfcMode:" + wfcMode +
                    "wfcModePref:" + wfcModePref);
            mWfcSettings.updateWfcModePreference(wfcModePref, wfcEnabled);
            super.updateWfcModePreference(root, wfcModePref, wfcEnabled, wfcMode);
        } else {
            Log.d(TAG, "isWfcEnabledByPlatform/isWifiCallingProvisioned not supported");
        }
    }

   /**
    * Called from onPause/onResume.
    * Used in WifiCallingSettings
     * @param event event happened
     * @return
     */
    @Override
    public void onWfcSettingsEvent(int event) {
        Log.d("@M_" + TAG, "Wificalling setting event:" + event);
        switch (event) {
            case DefaultWfcSettingsExt.CREATE:
                registerForWfcProvisioningChange();
                break;

            case DefaultWfcSettingsExt.RESUME:
                WifiCallingUtils.registerReceiver(mContext, mReceiver);
                break;

            case DefaultWfcSettingsExt.PAUSE:
                WifiCallingUtils.unRegisterReceiver(mContext, mReceiver);
                break;

            case DefaultWfcSettingsExt.DESTROY:
                unRegisterForWfcProvisioningChange();
                break;

            default:
                break;
        }
    }

    /*
    * Observes WFC provision settings changes .
    */
    private void registerForWfcProvisioningChange() {
        Uri contentUri = Uri.parse("content://com.mediatek.ims.config.provider/tb_master/");
        Uri result = ContentUris.withAppendedId(contentUri, SubscriptionManager
                .getDefaultVoicePhoneId());
        final Uri i = Uri.withAppendedPath(result, "VOICE_OVER_WIFI_SETTING_ENABLED");
        Log.d(TAG, "uri:" + i);

        mContentObserver = new ContentObserver(new Handler()) {
            @Override
            public void onChange(boolean selfChange) {
                this.onChange(selfChange, i);
            }

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                if (i != null && i.equals(uri)) {
                    if (!WifiCallingUtils.isWifiCallingProvisioned(mContext,
                            SubscriptionManager.getDefaultVoicePhoneId())) {
                        if (mPreferenceFragment != null
                                && (TextUtils.equals(mPreferenceFragment.getActivity().getClass()
                                    .getSimpleName(), "WifiCallingSettingsActivity")
                                    || TextUtils.equals(mPreferenceFragment.getActivity().getClass()
                                    .getSimpleName(), "SubSettings"))) {
                            Log.d(TAG, "finishing wfcsettings as its deProvisioned");
                            mPreferenceFragment.getActivity().finish();
                            mPreferenceFragment = null;
                        }
                    }
                }
            }
        };
        mContext.getContentResolver().registerContentObserver(i, false, mContentObserver);
    }

    private void unRegisterForWfcProvisioningChange() {
        mContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    private void handleWfcModeUpdation() {
        ListPreference wfcModeList = (ListPreference) mPreferenceFragment
                .findPreference(AOSP_BUTTON_WFC_MODE);
        updateWfcModePreference(mPreferenceFragment.getPreferenceScreen(), wfcModeList,
                ImsManager.isWfcEnabledByUser(mContext), ImsManager.getWfcMode(mContext));
    }
}


