package com.mediatek.op12.settings;

import android.content.Context;

import com.mediatek.settings.ext.IWfcSettingsExt;
import com.mediatek.settings.ext.OpSettingsCustomizationFactoryBase;

public class Op12SettingsCustomizationFactory extends OpSettingsCustomizationFactoryBase {
    private Context mContext;

    public Op12SettingsCustomizationFactory(Context context) {
        super(context);
        mContext = context;
    }

    public IWfcSettingsExt makeWfcSettingsExt() {
        return new Op12WfcSettingsExt(mContext);
    }
}
