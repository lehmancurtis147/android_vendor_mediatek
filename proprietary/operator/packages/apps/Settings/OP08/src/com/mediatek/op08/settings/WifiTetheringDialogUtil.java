package com.mediatek.op08.settings;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;

import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

class WifiTetheringDialogUtil extends AlertDialog {

    private static final String TAG = "WifiTetheringDialogUtil";
    private static final int BUTTON_SAVE = DialogInterface.BUTTON_POSITIVE;
    private static final int BUTTON_CANCEL = DialogInterface.BUTTON_NEGATIVE;
    private static final int MAC_ADDR_LEN = 17;
    private Context mContext;
    private View mView;
    private EditText mDeviceName;
    private EditText mMacEdit;
    private Button mSaveButton;
    private TextView mMacTutorial;
    private final DialogInterface.OnClickListener mListener;
    private String mPrevMac = null;
    private TextWatcher mMacListener;

    WifiTetheringDialogUtil(Context context, DialogInterface.OnClickListener listener) {
        super(context);
        mContext = context;
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        boolean mInit = true;
        mView = getLayoutInflater().inflate(R.layout.allowed_devices_add_device_dlg_layout, null);

        setView(mView);
        setInverseBackgroundForced(true);

        setTitle(R.string.add_device_title);
        mDeviceName = (EditText) mView.findViewById(R.id.device_name);
        mMacEdit = (EditText) mView.findViewById(R.id.mac_edit);
        mMacTutorial = (TextView) mView.findViewById(R.id.get_mac_tutorial);
        mSaveButton = (Button) getButton(BUTTON_SAVE);
        setButton(BUTTON_SAVE, mContext.getString(R.string.device_save), mListener);
        setButton(BUTTON_CANCEL, mContext.getString(R.string.device_cancel), mListener);
        //mSaveButton.setEnabled(false);
        registerMacTextChangedListener();
        registerDeviceNameTextChangedListener();
        mMacTutorial.setText(R.string.mac_tutorial);
        getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        super.onCreate(savedInstanceState);
    }

    private void registerMacTextChangedListener() {
        mMacListener = new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String enteredMacAddr = mMacEdit.getText().toString();
                Log.d(TAG, "afterTextChanged enteredMacAddr = " + enteredMacAddr);
                if (enteredMacAddr.length() ==  MAC_ADDR_LEN) {
                    Log.d(TAG, "Allow user to save item");
                    mSaveButton = (Button) getButton(BUTTON_SAVE);
                    mSaveButton.setEnabled(true);
                } else {
                    Log.d(TAG, "Don't allow user to save item");
                    mSaveButton = (Button) getButton(BUTTON_SAVE);
                    mSaveButton.setEnabled(false);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            //@SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d(TAG, "onTextChanged");
                String enteredMac = mMacEdit.getText().toString();
                String cleanedMac = removeNonMacCharacters(enteredMac);
                String formattedMac = formatMacAddress(cleanedMac);

                int selectStart = mMacEdit.getSelectionStart();
                formattedMac = handleUserColonDeletion(enteredMac, formattedMac, selectStart);
                int lengthDiff = formattedMac.length() - enteredMac.length();
                setMacAddrEdit(cleanedMac, formattedMac, selectStart, lengthDiff);
            }
        };
        mMacEdit.addTextChangedListener(mMacListener);
    }

    private void setMacAddrEdit(String cleanedMac, String formattedMac, int selectStart,
            int lengthDiff) {
        Log.d(TAG, "setMacAddrEdit");
        mMacEdit.removeTextChangedListener(mMacListener);
        if (cleanedMac.length() <= 12) {
            mMacEdit.setText(formattedMac);
            mMacEdit.setSelection(selectStart + lengthDiff);
            mPrevMac = formattedMac;
        } else {
            mMacEdit.setText(mPrevMac);
            mMacEdit.setSelection(mMacEdit.length());
        }
        mMacEdit.addTextChangedListener(mMacListener);
    }

    private String handleUserColonDeletion(String enteredMac, String formattedMac,
            int selectStart) {
        Log.d(TAG, "handleUserColonDeletion");
        if (mPrevMac != null && mPrevMac.length() > 1) {
            int prevColonCount = countColons(mPrevMac);
            int currColonCount = countColons(enteredMac);
            if (currColonCount < prevColonCount) {
                formattedMac = formattedMac.substring(0, selectStart - 1) +
                        formattedMac.substring(selectStart);
                String cleanMac = removeNonMacCharacters(formattedMac);
                formattedMac = formatMacAddress(cleanMac);
            }
        }
        Log.d(TAG, "handleUserColonDeletion: FormattedMac = " + formattedMac);
        return formattedMac;
    }

    private String removeNonMacCharacters(String macAddr) {
        Log.d(TAG, "removeNonMacCharacters + macAddr = " + macAddr);
        String formattedMac = macAddr.toString().replaceAll("[^A-Fa-f0-9]", "");
        Log.d(TAG, "removeNonMacCharacters + formattedMac = " + formattedMac);
        return formattedMac;
    }

    private String formatMacAddress(String macAddr) {
        Log.d(TAG, "unformattedMacAddress = " + macAddr);
        String formattedMac = "";
        int groupChars = 0;
        for (int i = 0; i < macAddr.length(); i++) {
            formattedMac += macAddr.charAt(i);
            ++groupChars;
            if (groupChars % 2 == 0) {
                formattedMac += ':';
                groupChars = 0;
            }
        }
        // remove trailing colons.
        if (formattedMac.length() == MAC_ADDR_LEN + 1) {
            formattedMac = formattedMac.substring(0, formattedMac.length() - 1);
        }
        Log.d(TAG, "formattedMacAddress = " + formattedMac);
        return formattedMac;
    }

    private int countColons(String macAddr) {
        return macAddr.replaceAll("[^:]", "").length();
    }

    private void registerDeviceNameTextChangedListener() {
        mDeviceName.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable arg0) {
                String enteredDeviceName = mDeviceName.getText().toString();
                Log.d(TAG, "afterTextChanged enteredDeviceName = " + enteredDeviceName);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            //@SuppressLint("DefaultLocale")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
    }

    protected String getDeviceName() {
        return mDeviceName.getText().toString();
    }

    protected String getMacAddr() {
        return mMacEdit.getText().toString();
    }
}