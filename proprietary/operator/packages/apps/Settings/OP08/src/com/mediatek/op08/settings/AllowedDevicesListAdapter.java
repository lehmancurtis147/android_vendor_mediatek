package com.mediatek.op08.settings;

import android.content.Context;
//import android.net.wifi.HotspotClient;
import android.net.wifi.WifiManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import mediatek.net.wifi.HotspotClient;

import java.util.List;

public class AllowedDevicesListAdapter extends ArrayAdapter<HotspotClient> {
    private static final String TAG = "OP08AllowedDevicesListAdapter";
    private final Context mContext;
    private final List<HotspotClient> mValues;
    private int mSelectedItemPosition = 0;

    public AllowedDevicesListAdapter(Context context, List<HotspotClient> values) {
        super(context, R.layout.allowed_devices_list_item, values);
        this.mContext = context;
        this.mValues = values;
        mSelectedItemPosition = -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.allowed_devices_list_item, parent, false);
        }
        TextView ssidView = (TextView) convertView.findViewById(R.id.ssid_name);
        TextView macAddressView = (TextView) convertView.findViewById(R.id.mac_address);
        // Display name priority: user-entered > device name > MAC Address
        WifiManager wifiManager  = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
        HotspotClient client = mValues.get(position);
        String macAddress = client.deviceAddress;
        String deviceName = wifiManager.getWifiHotspotManager().getClientDeviceName(macAddress);
        String nameText = (client.name != null) ? client.name : deviceName;
        Log.d(TAG, "macAddress:" + macAddress + ", deviceName:" + deviceName
            + ", client.name:" + client.name);

        ssidView.setText(((nameText != null) && (nameText.length() > 0)) ? nameText : macAddress);
        macAddressView.setText(macAddress);
        Log.d(TAG, "mSelectedItemPosition:" + mSelectedItemPosition + ", position:" + position);
        if (mSelectedItemPosition == position) {
            convertView.setBackgroundColor(mContext.getResources().getColor(R.color.grey_color));
        } else {
            convertView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        }
        return convertView;
    }

    public void setSelectedItemId(int id) {
        if (id < getCount()) {
            mSelectedItemPosition = id;
        } else {
            mSelectedItemPosition = -1;
        }
    }

    public int getSelectedItemId() {
        return mSelectedItemPosition;
    }
}