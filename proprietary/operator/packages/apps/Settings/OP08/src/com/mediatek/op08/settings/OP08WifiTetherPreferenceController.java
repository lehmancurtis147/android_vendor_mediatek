/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op08.settings;

import android.content.Context;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.support.v7.preference.Preference.OnPreferenceClickListener;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.android.settings.wifi.tether.WifiTetherBasePreferenceController;
import com.android.settings.wifi.tether.WifiTetherBasePreferenceController
        .OnTetherConfigUpdateListener;

public class OP08WifiTetherPreferenceController extends WifiTetherBasePreferenceController
            implements OnPreferenceClickListener {

    private String TAG = "OP08WifiTetherPreferenceController";

    private static final String KEY_ALLOWED_DEVICES_PREF = "allowed_devices_pref";
    private static final String PREF_KEY_NEW = "wifi_tether_network_connections";
    private static final String ACTION_ALLOWED_DEVICES_ACTIVITY =
            "mediatek.settings.TETHERING_ALLOWED_DEVICES";
    private Context mContext;
    private Context mOP08Context;
    private PreferenceScreen mPrefScreen;
    private Preference mAllowedDevicesPref = null;

    public OP08WifiTetherPreferenceController(Context context, Context op08Context,
            OnTetherConfigUpdateListener listener) {
        super(context, listener);
        mContext = context;
        mOP08Context = op08Context;
        Log.i(TAG, "mContext = " + mContext + ", mOP08Context = " + mOP08Context);
    }

    /**
     * Displays preference in this controller.
     */
    @Override
    public void displayPreference(PreferenceScreen screen) {
        Log.i(TAG, "displayPreference");
        if (screen.findPreference(KEY_ALLOWED_DEVICES_PREF) == null) {
            addAllowedDeviceListPreference(screen);
        }
    }

    private void addAllowedDeviceListPreference(PreferenceScreen prefScreen) {
        Log.d(TAG, "addAllowedDeviceListPreference");
        mAllowedDevicesPref = new Preference(prefScreen.getPreferenceManager()
                .getContext());
        mAllowedDevicesPref.setKey(KEY_ALLOWED_DEVICES_PREF);
        mAllowedDevicesPref.setTitle(mOP08Context.getText(R.string.allowed_devices_title));
        mAllowedDevicesPref.setSummary(mOP08Context.getText(R.string.allowed_devices_summary));

        mAllowedDevicesPref.setOnPreferenceClickListener(this);
        Preference orderPref = prefScreen.findPreference(PREF_KEY_NEW);
        if (orderPref != null) {
            int order = orderPref.getOrder();
            mAllowedDevicesPref.setOrder(order + 1);
        }
        prefScreen.addPreference(mAllowedDevicesPref);
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Log.i(TAG, "onPreferenceChange");
        return true;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String prefkey = preference.getKey();
        Log.d(TAG, "onPreferenceClick: prefkey : " + prefkey);
        if (prefkey.equals(KEY_ALLOWED_DEVICES_PREF)) {
            mOP08Context.startActivity(new Intent(ACTION_ALLOWED_DEVICES_ACTIVITY));
        }
        return true;
    }

    @Override
    public String getPreferenceKey() {
        return KEY_ALLOWED_DEVICES_PREF;
    }

    @Override
    public boolean isAvailable() {
        return false;
    }

    @Override
    public void updateDisplay() {
       Log.d(TAG, "updateDisplay");
    }
}
