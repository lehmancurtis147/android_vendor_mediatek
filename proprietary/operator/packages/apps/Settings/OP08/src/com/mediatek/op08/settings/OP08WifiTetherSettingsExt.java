package com.mediatek.op08.settings;

import android.content.Context;
import android.content.Intent;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.util.Log;

import com.android.settings.wifi.tether.WifiTetherBasePreferenceController
        .OnTetherConfigUpdateListener;
import com.android.settingslib.core.AbstractPreferenceController;
import com.mediatek.settings.ext.DefaultWifiTetherSettingsExt;

import java.util.List;

/**
 * Plugin implementation of Wifi Tethering plugin.
 */

public class OP08WifiTetherSettingsExt extends DefaultWifiTetherSettingsExt {

    private static final String TAG = "OP08WifiTetherSettingsExt";
    private Context mContext;
    private static final String KEY_ALLOWED_DEVICES_PREF = "allowed_devices_pref";
    private static final String PREF_KEY_OLD = "wps_connect";
    private static Preference mAllowedDevicesPref;
    private static final String ACTION_ALLOWED_DEVICES_ACTIVITY =
            "mediatek.settings.TETHERING_ALLOWED_DEVICES";

    private static OP08WifiTetherPreferenceController sAllowedListPrefController = null;

    /**
     * Constructor.
     * @param context context
     */
    public OP08WifiTetherSettingsExt(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public void addPreferenceController(Context context, Object controllers, Object listener) {
        Log.i(TAG, "addPreferenceController");
        OnTetherConfigUpdateListener config_listener = (OnTetherConfigUpdateListener) listener;
        if (sAllowedListPrefController == null) {
            sAllowedListPrefController = new OP08WifiTetherPreferenceController(context,
                    mContext, config_listener);
        }

        List<AbstractPreferenceController> con =  (List<AbstractPreferenceController>) controllers;
        con.add(sAllowedListPrefController);
    }

    /**
     * Add allowed-device-list preference.
     * @param ps parent preference screen
     */
    @Override
    public void addAllowedDeviceListPreference(Object ps) {
        PreferenceScreen prefScreen = (PreferenceScreen)ps;
        Log.d(TAG, "addAllowedDeviceListPreference");
        mAllowedDevicesPref = new Preference(prefScreen.getPreferenceManager()
                .getContext());
        mAllowedDevicesPref.setKey(KEY_ALLOWED_DEVICES_PREF);
        mAllowedDevicesPref.setTitle(mContext.getText(R.string.allowed_devices_title));
        mAllowedDevicesPref.setSummary(mContext.getText(R.string.allowed_devices_summary));

        Preference orderPref = prefScreen.findPreference(PREF_KEY_OLD);
        if (orderPref != null) {
            int order = orderPref.getOrder();
            mAllowedDevicesPref.setOrder(order + 1);
        }
        prefScreen.addPreference(mAllowedDevicesPref);
    }

    /**
     * Launch allowed device list activity.
     * @param preference allowedDevicePreference
     */
    @Override
    public void launchAllowedDeviceActivity(Object preference) {
        String prefkey = ((Preference) preference).getKey();
        Log.d(TAG, "launchAllowedDeviceScreen: prefkey : " + prefkey);
        if (prefkey.equals(KEY_ALLOWED_DEVICES_PREF)) {
            mContext.startActivity(new Intent(ACTION_ALLOWED_DEVICES_ACTIVITY));
        }
    }
}
