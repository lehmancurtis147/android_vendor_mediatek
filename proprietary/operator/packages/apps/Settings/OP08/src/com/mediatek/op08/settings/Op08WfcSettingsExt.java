package com.mediatek.op08.settings;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.support.v14.preference.PreferenceFragment;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceScreen;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.settings.SettingsActivity;
import com.android.settings.widget.SwitchBar;
import com.mediatek.ims.WfcReasonInfo;
import com.mediatek.settings.ext.DefaultWfcSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;


/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op08WfcSettingsExt extends DefaultWfcSettingsExt {

    private static final String TAG = "Op08WfcSettingsExt";
    private static final String TUTORIALS = "Tutorials";
    private static final String TOP_QUESTIONS = "Top_questions";
    private static final String WFC_MODE = "Wfc_mode";
    private static final String AOSP_BUTTON_WFC_MODE = "wifi_calling_mode";
    private static final String TUTORIAL_ACTION = "mediatek.settings.WFC_TUTORIALS";
    private static final String QA_ACTION = "mediatek.settings.WFC_QA";
    private static final String WFC_MODE_DIALOG_ACTION = "mediatek.settings.SHOW_WFC_MODE_DIALOG";
    private static final int FEATURE_CONFIGURED = 99;
    private static final int FEATURE_UNCONFIGURED = 100;

    private Context mContext;
    private PreferenceFragment mPreferenceFragment;
    private Preference mWfcModePreference;
    private SwitchBar mSwitchBar;

    private ContentObserver mWfcModeChangeObserver;

    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            // TODO Auto-generated method stub
            boolean isWfcEnabled = ImsManager.isWfcEnabledByPlatform(mContext);
            Log.d(TAG, "Phone state:" + state);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                   if (mSwitchBar == null) {
                        Log.d(TAG, "mWfcSwitchBar is null, so return");
                        return;
                    }
                    mSwitchBar.setEnabled(true);
                    /* Enable preference, only if wfc switch is ON */
                    //boolean modeEnabled = (mSwitchBar.isChecked()) ? true : false;
                    Log.d(TAG, "WFC mode Enabled :" + isWfcEnabled);
                    mWfcModePreference.setEnabled(isWfcEnabled);
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                case TelephonyManager.CALL_STATE_RINGING:
                    if (mSwitchBar != null) {
                        mSwitchBar.setEnabled(false);
                        Log.d(TAG, "mWfcSwitchBar set disabled");
                    }
                    mWfcModePreference.setEnabled(false);
                    Log.d(TAG, "mWfcModePreference set disabled");
                    break;
                default:
                    break;
            }
        }
    };


    /** Constructor.
     * @param context default summary res id
     */
    public Op08WfcSettingsExt(Context context) {
        super();
        mContext = context;
        Log.d(TAG, "Op08WfcSettingsExt");
    }

   /**
    * Initialize plugin with essential values.
     * @param
     * @return
     */
    @Override
    public void initPlugin(PreferenceFragment pf) {
        mPreferenceFragment = pf;
    }

    /*******************        ************************/

    /***********    Plugin for  WifiCallingSettings     *******/

    //@Override
    /** Called from onPause/onResume.
     * @param event event happened
     * @return
     */
    public void onWfcSettingsEvent(int event) {
        Log.d(TAG, "WfcSeting event:" + event);
        switch (event) {
            case DefaultWfcSettingsExt.RESUME:
                SettingsActivity settingsActivity = (SettingsActivity) mPreferenceFragment
                        .getActivity();
                mSwitchBar = settingsActivity.getSwitchBar();
                ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE))
                        .listen(mPhoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
                mContext.getContentResolver().registerContentObserver(android.provider
                        .Settings.Global.getUriFor(android.provider
                                .Settings.Global.WFC_IMS_MODE),
                        false, mWfcModeChangeObserver);
                break;
            case DefaultWfcSettingsExt.PAUSE:
                ((TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE))
                        .listen(mPhoneStateListener, PhoneStateListener.LISTEN_NONE);
                mContext.getContentResolver().unregisterContentObserver(mWfcModeChangeObserver);
                break;
            default:
                break;
        }
    }

    /** Add WFC tutorial prefernce, if any.
     * @param
     * @return
     */
    @Override
    public void addOtherCustomPreference() {
        PreferenceScreen ps = mPreferenceFragment.getPreferenceScreen();
        /* remove AOSP wfc mode preference */
        ps.removePreference(mPreferenceFragment.findPreference(AOSP_BUTTON_WFC_MODE));

        /* Add cutomized wfc mode preference */
        mWfcModePreference = new Preference(ps.getContext());
        mWfcModePreference.setKey(WFC_MODE);
        mWfcModePreference.setTitle(mContext.getText(R.string.wfc_mode_preference_title));
        mWfcModePreference.setSummary(WfcUtils.getWfcModeSummary(ImsManager.getWfcMode(mContext)));
        mWfcModePreference.setIntent(new Intent(WFC_MODE_DIALOG_ACTION));
        ps.addPreference(mWfcModePreference);
        /* Register content observer on Wfc Mode to change summary of this pref on mode change */
        registerForWfcModeChange(new Handler());

        Preference wfcTutorialPreference = new Preference(ps.getContext());
        wfcTutorialPreference.setKey(TUTORIALS);
        wfcTutorialPreference.setTitle(mContext.getText(R.string.Tutorials));
        wfcTutorialPreference.setIntent(new Intent(TUTORIAL_ACTION));
        ps.addPreference(wfcTutorialPreference);

        Preference wfcQAPreference = new Preference(ps.getContext());
        wfcQAPreference.setKey(TOP_QUESTIONS);
        wfcQAPreference.setTitle(mContext.getText(R.string.Top_questions));
        wfcQAPreference.setIntent(new Intent(QA_ACTION));
        ps.addPreference(wfcQAPreference);
    }

    @Override
    /** Takes operator specific action on wfc list preference on switch change:
     * On Switch OFF, disable wfcModePref.
     * @param root
     * @param wfcModePref
     * @return
     */
    public void updateWfcModePreference(PreferenceScreen root, ListPreference wfcModePref,
            boolean wfcEnabled, int wfcMode) {
        Log.d(TAG, "wfc_enabled:" + wfcEnabled + " wfcMode:" + wfcMode);
        mWfcModePreference.setSummary(WfcUtils.getWfcModeSummary(wfcMode));
        mWfcModePreference.setEnabled(wfcEnabled);
        // Remove Wfc mode preference because host app adds it if wfc is enabled
        if (wfcEnabled) {
            root.removePreference(wfcModePref);
        }
    }

    /*
    * Observes WFC mode changes to change summary of preference.
    */
    private void registerForWfcModeChange(Handler handler) {
        mWfcModeChangeObserver = new ContentObserver(handler) {

            @Override
            public void onChange(boolean selfChange) {
                this.onChange(selfChange,
                        android.provider.Settings.Global
                        .getUriFor(android.provider.Settings.Global.WFC_IMS_MODE));
            }

            @Override
            public void onChange(boolean selfChange, Uri uri) {
                Uri i = android.provider.Settings.Global
                        .getUriFor(android.provider.Settings.Global.WFC_IMS_MODE);
                Log.d("@M_" + TAG, "wfc mode:" + ImsManager.getWfcMode(mContext));
                if (i != null && i.equals(uri)) {
        //            if (ImsManager.isWfcEnabledByPlatform(mContext)) {
                        /* set summary */
                        mWfcModePreference.setSummary(WfcUtils
                                .getWfcModeSummary(ImsManager.getWfcMode(mContext)));
          //          }
                }
            }
        };
//        mContext.getContentResolver().registerContentObserver(
  //              android.provider.Settings.Global.getUriFor(android.provider
    //                    .Settings.Global.WFC_IMS_MODE),
      //          false, mWfcModeChangeObserver);
    }
    /***************************        *********************/
}


