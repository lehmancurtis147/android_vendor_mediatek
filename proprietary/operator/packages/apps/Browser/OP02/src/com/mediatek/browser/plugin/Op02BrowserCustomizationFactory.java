package com.mediatek.browser.plugin;

import android.content.Context;

import com.mediatek.browser.ext.IBrowserBookmarkExt;
import com.mediatek.browser.ext.IBrowserDownloadExt;
import com.mediatek.browser.ext.IBrowserSettingExt;
import com.mediatek.browser.ext.OpBrowserCustomizationFactoryBase;

public class Op02BrowserCustomizationFactory extends OpBrowserCustomizationFactoryBase {
    private Context mContext;

    public Op02BrowserCustomizationFactory(Context context) {
        mContext = context;
	}
	
    @Override
    public IBrowserBookmarkExt makeBrowserBookmarkExt() {
        android.util.Log.d("YifanTest", "new plugin framework op02!");
        return new Op02BrowserBookmarkExt(mContext);
    }
    @Override
    public IBrowserDownloadExt makeBrowserDownloadExt() {
        return new Op02BrowserDownloadExt(mContext);
    }
    @Override
    public IBrowserSettingExt makeBrowserSettingExt() {
        return new Op02BrowserSettingExt(mContext);
    }  
}