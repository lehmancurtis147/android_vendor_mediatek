package com.mediatek.browser.plugin;

import android.app.Activity;
import android.content.Context;
import android.text.format.Formatter;
import android.util.Log;
import android.widget.Toast;

import com.mediatek.browser.ext.DefaultBrowserDownloadExt;
import com.mediatek.browser.plugin.R;

import java.io.File;

public class Op02BrowserDownloadExt extends DefaultBrowserDownloadExt {

    private static final String TAG = "BrowserPluginEx";

    private Context mContext;

    public Op02BrowserDownloadExt(Context context) {
        super();
        mContext = context;
    }

    /**
     * Show the download toast with file size.
     * @param activity the activity
     * @param contentLength the download file content length
     * @param text toast information
     */
    public void showToastWithFileSize(Activity activity, long contentLength, String text) {
        Log.d("@M_" + TAG, "Enter: " + "shouldShowToastWithFileSize" + " --OP02 implement");
        if (contentLength > 0) {
            Toast.makeText(activity, mContext.getResources().getString(R.string.download_pending_with_file_size)
                + Formatter.formatFileSize(activity, contentLength), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(activity, mContext.getResources().getString(R.string.download_pending), Toast.LENGTH_SHORT).show();
        }
    }

}
