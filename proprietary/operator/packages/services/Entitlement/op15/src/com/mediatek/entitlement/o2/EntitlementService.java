package com.mediatek.entitlement.o2;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
import android.util.Log;
import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.internal.telephony.MtkSubscriptionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


interface Consumer<T> {
    void accept(T t);
}

public class EntitlementService extends Service {
    private static final String TAG = "EntitlementService";
    private static final boolean DEBUG = true;

    //private final Map<String, EntitlementHandling> mServices = new HashMap<>();
    private EntitlementHandling mHandling = null;
    private final Map<String, Provisioning> mProvisionings = new HashMap<>();

    static final String ACTION_PROVISIONING_PENDING = "provisioning-pending";
    private static boolean mWaitNetwork = false;
    private static Network mNetwork = null;
    private static int mRefCount = 0;
    private ConnectivityManager.NetworkCallback mNetworkCallback;
    private ConnectivityManager mConnectivityManager;
    private static String mProvisionedService = null;
    ArrayList<String> mServiceList = new ArrayList<String>();
    private static final String VOWIFI_SERVICE = "VoWiFi"; //"vowifi";
    private static final String VOLTE_SERVIVE = "VoLTE"; //"volte";
    static final String ACTION_SUBINFO_RECORD_UPDATED = "subinfo-record-updated";
    private static final int RETRY_TIMER = 30; //minutes.
    private static final int RETRY_COUNT = 5;
    public static final String SERVICE_STATUS_PREF = "EntitlementPref";
    private static final int NETWORK_REQUEST_TIMEOUT_MILLIS = 60 * 1000;
    private EntitlementCheck mEntitlementCheck = new EntitlementCheck();
    //private static final String VLT_SETTING_SYTEM_PROPERTY = "vendor.ril.vt.setting.support";
    //private static final String WFC_SETTING_SYTEM_PROPERTY = "vendor.ril.wfc.setting.support";

    // Periodically check whether a service is still pending.
    private class Provisioning {
        private final String mService;
        private final int mAlarmInterval;
        private int mRetryTimes;

        Provisioning(String service, int alarmInterval, int retryTimesMax) {
            mService = service;
            mAlarmInterval = alarmInterval;
            mRetryTimes = retryTimesMax;
            updateRetryStatus(service, mRetryTimes);
            setAlarm();
        }

        synchronized boolean shouldRetry() {
            log("In shouldRetry(), mRetryTimes = " + mRetryTimes);
            return mRetryTimes > 0;
        }

        synchronized boolean retry() {
            if (!shouldRetry()) {
                return false;
            }
            log("Start retry Mechanism for service = " + mService);
            mRetryTimes--;
            updateRetryStatus(mService, mRetryTimes);
            //getHandling(mService).startEntitlementCheck();
            //startEntitlement(mServiceList); // populate list.
            return true;
        }

        void setAlarm() {
            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_PROVISIONING_PENDING);
            i.putExtra("service", mService);
            log("Setting Retry alarm for Service = " + mService);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 1, i, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * mAlarmInterval, pi);
        }

        void cancelAlarm() {
            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_PROVISIONING_PENDING);
            i.putExtra("service", mProvisionedService);
            log("Cancel Retry alarm srv = " + mService + ", sent= " + mProvisionedService);
            mProvisionedService = null;
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 1, i, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    static final String ACTION_ENTITLEMENT_CHECK = "entitlement-check";

    // Once a service is entitled, check the entitlement daily
    private class EntitlementCheck {
        void setAlarm(String service) {
            int alarmInterval = mHandling.getPollInterval();
            log("Received alarm interval = " + alarmInterval);
            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_ENTITLEMENT_CHECK);
            i.putExtra("service", service);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP,
                System.currentTimeMillis() + 1000 * 60 * 60 * alarmInterval,
                1000 * 60 * 60 * alarmInterval/*AlarmManager.INTERVAL_DAY*/, pi);
        }

        void cancelAlarm(String service) {

            Intent i = new Intent(EntitlementService.this, EntitlementService.class);
            i.setAction(ACTION_ENTITLEMENT_CHECK);
            i.putExtra("service", service);
            PendingIntent pi = PendingIntent.getService(EntitlementService.this, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context cntx = getApplicationContext();
        mConnectivityManager
            = (ConnectivityManager) cntx.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    private EntitlementHandling getHandling(final ArrayList<String> serviceList) {
        /* synchronized (mServices) {
            EntitlementHandling handling = mServices.get(service); */
            if (mHandling == null) {
                log("Init Handling");
                mHandling = new EntitlementHandling(serviceList, getApplicationContext(), new EntitlementHandling.Listener() {
                    @Override
                    public void onStateChange(EntitlementHandling handling, int state, String service) {
                        //  Check state and set alarm
                        updateServiceStatus(service,state);

                        if (state == EntitlementHandling.STATE_ENTITLED) {
                            mEntitlementCheck.setAlarm(service);
                            mRefCount--;
                            synchronized (mProvisionings) {
                                Provisioning provisioning = mProvisionings.get(service);
                                log("In sucess, Stored provisioning = " + provisioning);
                                if (provisioning != null) {
                                    if (mProvisionings.size() == 1) {
                                        provisioning.cancelAlarm();
                                    }
                                    mProvisionings.remove(service);
                                    updateRetryStatus(service, 0);
                                }
                            }
                            provisionService(service);
                        } else {
                            mEntitlementCheck.cancelAlarm(service);
                            if (state == EntitlementHandling.STATE_ENTITLED_FAIL) {
                                ImsManager.setWfcSetting(getApplicationContext(), false);
                                synchronized (mProvisionings) {
                                    Provisioning provisioning = mProvisionings.get(service);
                                    log("Stored provisioning = " + provisioning);
                                    if (provisioning != null) {
                                        if (provisioning.shouldRetry()) {
                                            provisioning.setAlarm();
                                        } else {
                                            // provisioning failed after 30/60min retry.
                                            if (mProvisionings.size() == 1) {
                                                provisioning.cancelAlarm();
                                            }
                                            mProvisionings.remove(service);
                                        }
                                    } else {
                                        mProvisionings.put(service,
                                            new Provisioning(service, RETRY_TIMER, RETRY_COUNT));
                                    }
                                }
                                mRefCount--;
                            }
                        }
                        if (mRefCount == 0) {
                            if (mNetwork != null) {
                                releaseNetwork(mNetwork);
                            }
                        }
                    }

                    @Override
                    public void onWebsheetPost(String url, String serverData) {
                    }

                    @Override
                    public void onInfo(Bundle info) {
                        Log.d(TAG, "Info discovered: " + info);
                    }
                });
                //mServices.put(service, handling);
            }
            return mHandling;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        log("onStartCommand: " + intent + " " + startId);
        String action = null;
        if (!isEntitlementEnabled()) {
            log("Entitlement sys property not enabled return directly");
            return super.onStartCommand(intent, flags, startId);
        }
        if (intent != null) {
            action = intent.getAction();
            log("Received action = " + action);
            if (ACTION_ENTITLEMENT_CHECK.equals(action)) {
                //String service = intent.getStringExtra("service");
                //getHandling(service).startEntitlementCheck();
                ArrayList<String> list = new ArrayList<String>();
                list.add(VOLTE_SERVIVE);
                list.add(VOWIFI_SERVICE);
                startEntitlement(list);
            } else if (ACTION_PROVISIONING_PENDING.equals(action)) {
                String service = intent.getStringExtra("service");
                mProvisionedService = service;
                //mServiceList.clear();
                log("Received Retry alarm for service = " + service);
                ArrayList<String> list = new ArrayList<String>();
                synchronized (mProvisionings) {
                    for (String retryService : mProvisionings.keySet()) {
                        Provisioning provisioning = mProvisionings.get(retryService);
                        if (provisioning != null) {
                            if (!provisioning.retry()) {
                                continue;
                            }
                        }
                        list.add(retryService);
                    }
                }
                if (list != null) {
                    startEntitlement(list);
                }
            } else if (ACTION_SUBINFO_RECORD_UPDATED.equals(action)) {
                if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                    == MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM) {
                    Log.d(TAG, "Need handle, REMOVE_SIM, deprovision VoLTE and WFC");
                    resetEntitlementContext();
                } else if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                    MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                    == MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM) {
                    if (mHandling != null) {
                        if (mHandling.isinActiveState()) {
                            Log.d(TAG, "In New SIM, Currently Entitlement is ongoing, return");
                            return START_STICKY;
                        }
                    }
                    Log.d(TAG, "Need handle, New SIM, deprovision VoLTE and WFC");
                    resetEntitlementContext();
                    ArrayList<String> list = new ArrayList<String>();
                    list.add(VOLTE_SERVIVE);
                    list.add(VOWIFI_SERVICE);
                    startEntitlement(list);
                }
            }
        } else {
            ArrayList<String> list = new ArrayList<String>();
            if (needEntitlementCheck(list)) {
                log("Service Re-created by system. Start Entitlement.");
                startEntitlement(list);
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private static void log(String s) {
        if (DEBUG) Log.d(TAG, s);
    }

    private void startEntitlement(ArrayList<String> serviceList) {
        log("startEntitlement");
        if (serviceList != null ) {
            mRefCount = serviceList.size();
            log("startEntitlement, RefCount= " + mRefCount + ",Nwk= " + mNetwork);
            if (initEntitlementContext(serviceList)) {
                getHandling(mServiceList).startEntitlementCheck();
            }
        }
    }

    private void handleNetworkNotification(boolean success, Network network) {
        log("handleNetworkNotification, result = " + success + " ,for network " + network);
        mNetwork = network;
        if (!success && network != null ) {
            releaseNetwork(network);
        }
        if (mWaitNetwork) {
            if (mNetwork != null) {
                boolean result = mConnectivityManager.bindProcessToNetwork(mNetwork);
                log("Try to bind network " + mNetwork + ", result = " + result);
            }
            getHandling(mServiceList).startEntitlementCheck();
        }
        mWaitNetwork = false;
    }

    private void requestNetwork() {
        mNetworkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                super.onAvailable(network);
                mNetwork = network;
                log("NetworkCallbackListener.onAvailable: network=" + network);
                handleNetworkNotification(true, network);
            }

            @Override
            public void onLost(Network network) {
                super.onLost(network);
                log("NetworkCallbackListener.onLost: network=" + network);
                handleNetworkNotification(false, network);
            }

            @Override
            public void onUnavailable() {
                super.onUnavailable();
                log("NetworkCallbackListener.onUnavailable");
                handleNetworkNotification(false, null);
            }
        };
        NetworkRequest.Builder networkBuilder = new NetworkRequest.Builder();
        buildNetworkRequest(networkBuilder);
        NetworkRequest networkRequest = networkBuilder.build();
        log("Create Network Request to enable Data silently " + networkRequest);
        mConnectivityManager.requestNetwork(networkRequest, mNetworkCallback, NETWORK_REQUEST_TIMEOUT_MILLIS);
        mWaitNetwork = true;
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed (
            new Runnable() {
                @Override
                public void run() {
                    if (mWaitNetwork == true) {
                        log("No network response, timer expired");
                        handleNetworkNotification(false, null);
                    }
                }
            },
            NETWORK_REQUEST_TIMEOUT_MILLIS + 10 * 1000);
    }

    private void releaseNetwork(Network network) {
        log("releaseRequest: mNetwork=" + mNetwork + "network = " + network
                + ", mNetworkCallback=" + mNetworkCallback);
        if (mNetworkCallback != null) {
            mConnectivityManager.unregisterNetworkCallback(mNetworkCallback);
            mConnectivityManager.bindProcessToNetwork(null);
        }
        mNetworkCallback = null;
        mNetwork = null;
    }

    public boolean needRequestNetwork() {
        if (!isNetworkAvailable()) {
            return true;
        }
        log("Network already available, no need Request");
        return false;
    }

    /***Telefonica Request: Silently enable data to complete entitlement***/
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivity
              = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivity.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void buildNetworkRequest (NetworkRequest.Builder builder) {
        builder.addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
            .addCapability(NetworkCapabilities.NET_CAPABILITY_CBS);
    }

    private void deProvisionServices() {
        log("De-provision both VoLTE & VoWifi");
        //SystemProperties.set(VLT_SETTING_SYTEM_PROPERTY, "0");
        //SystemProperties.set(WFC_SETTING_SYTEM_PROPERTY, "0");
        ImsManager imsManager = ImsManager.getInstance(getApplicationContext(),
                    SubscriptionManager.getDefaultDataSubscriptionId());
        try {
            int result = imsManager.getConfigInterface().setProvisionedValue(
                    ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED, 0);
            //log("WFC deprovision result = " + result);
            imsManager.getConfigInterface().setProvisionedValue(
                    ImsConfig.ConfigConstants.VLT_SETTING_ENABLED, 0);
            //log("VoLTE deprovision result = " + result);
        } catch (ImsException e) {
            Log.e(TAG, "Exception happened! " + e.getMessage());
        }
    }

    private void provisionService(String service) {
        log("Restore provisioning for service = " + service);
        String sysProperty = null;
        ImsManager imsManager = ImsManager.getInstance(getApplicationContext(),
                    SubscriptionManager.getDefaultDataSubscriptionId());
        int setting = 0;
        if (service.equalsIgnoreCase(VOLTE_SERVIVE)) {
            setting = ImsConfig.ConfigConstants.VLT_SETTING_ENABLED;
            //SystemProperties.set(VLT_SETTING_SYTEM_PROPERTY, "1");
            //log("sys prop set = " + SystemProperties.get(VLT_SETTING_SYTEM_PROPERTY));
        } else if (service.equalsIgnoreCase(VOWIFI_SERVICE)) {
            setting = ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED;
            //SystemProperties.set(WFC_SETTING_SYTEM_PROPERTY, "1");
        } else {
            log("Invalid service!!!");
            return;
        }
        try {
            int result = imsManager.getConfigInterface().setProvisionedValue(
                    setting, 1);
            imsManager.updateImsServiceConfig(getApplicationContext(),
                SubscriptionManager.getDefaultDataSubscriptionId(), true);
            log("For service, " + service + "setting:" + setting + "Provision result = " + result);
        } catch (ImsException e) {
            Log.e(TAG, "Exception happened! " + e.getMessage());
        }
    }

    private void deProvisionService(String service) {
        log("Remove provisioning for service = " + service);
        ImsManager imsManager = ImsManager.getInstance(getApplicationContext(),
                    SubscriptionManager.getDefaultDataSubscriptionId());
        int setting = 0;
        if (service.equalsIgnoreCase(VOLTE_SERVIVE)) {
            setting = ImsConfig.ConfigConstants.VLT_SETTING_ENABLED;
            //SystemProperties.set(VLT_SETTING_SYTEM_PROPERTY, "0");
            //log("sys prop reset = " + SystemProperties.get(VLT_SETTING_SYTEM_PROPERTY));
        } else if (service.equalsIgnoreCase(VOWIFI_SERVICE)) {
            setting = ImsConfig.ConfigConstants.VOICE_OVER_WIFI_SETTING_ENABLED;
            //SystemProperties.set(WFC_SETTING_SYTEM_PROPERTY, "0");
        } else {
            log("Invalid service!!!");
            return;
        }
        try {
            int result = imsManager.getConfigInterface().setProvisionedValue(
                    setting, 0);
            log("For service, " + service + "De-Provisioning result = " + result);
        } catch (ImsException e) {
            Log.e(TAG, "Exception happened! " + e.getMessage());
        }
    }

    private boolean needEntitlementCheck(ArrayList<String> list) {
        SharedPreferences prefs = getSharedPreferences(SERVICE_STATUS_PREF, MODE_PRIVATE);
        int serviceStatus = prefs.getInt("volte", 0);
        log("from pref, volte status = " + serviceStatus);
        if (serviceStatus == EntitlementHandling.STATE_NOT_ENTITLED ||
                serviceStatus == EntitlementHandling.STATE_ACTIVATING) {
            list.add(VOLTE_SERVIVE);
        } else if (serviceStatus == EntitlementHandling.STATE_ENTITLED_FAIL) {
            int retriesleft = prefs.getInt("volteretry", 0);
            if (retriesleft > 0) {
                list.add(VOLTE_SERVIVE);
            }
        }
        serviceStatus = prefs.getInt("vowifi", 0);
        log("from pref, vowifi status = " + serviceStatus);
        if (serviceStatus == EntitlementHandling.STATE_NOT_ENTITLED ||
                serviceStatus == EntitlementHandling.STATE_ACTIVATING) {
            list.add(VOWIFI_SERVICE);
        } else if (serviceStatus == EntitlementHandling.STATE_ENTITLED_FAIL) {
            int retriesleft = prefs.getInt("vowifiretry", 0);
            if (retriesleft > 0) {
                list.add(VOWIFI_SERVICE);
            }
        }
        if (list != null) {
            return true;
        } else {
            return false;
        }
    }

    private void updateServiceStatus(String service, int status) {
        log("Update shared pref for service = " + service + " to status = " + status);
        SharedPreferences.Editor editor = getSharedPreferences(SERVICE_STATUS_PREF, MODE_PRIVATE).edit();
        if (service.equalsIgnoreCase(VOLTE_SERVIVE)) {
            editor.putInt("volte", status);
        } else if (service.equalsIgnoreCase(VOWIFI_SERVICE)) {
            editor.putInt("vowifi", status);
        }
        editor.commit();
    }

    private void updateRetryStatus(String service, int retriedTimes) {
        log("Update shared pref for service = " + service + " ,Retry times = " + retriedTimes);
        SharedPreferences.Editor editor = getSharedPreferences(SERVICE_STATUS_PREF, MODE_PRIVATE).edit();
        if (service.equalsIgnoreCase(VOLTE_SERVIVE)) {
            editor.putInt("volteretry", retriedTimes);
        } else if (service.equalsIgnoreCase(VOWIFI_SERVICE)) {
            editor.putInt("vowifiretry", retriedTimes);
        }
        editor.commit();
    }

    private void resetEntitlementContext() {
        deProvisionServices();
        if (mNetwork != null) {
            releaseNetwork(mNetwork);
        }
        mWaitNetwork = false;
        // in case of reset state, turn off vowifi
        ImsManager.setWfcSetting(getApplicationContext(), false);
        mServiceList.clear();
        mRefCount = 0;
        if (mHandling != null) {
            mHandling.stopEntitlementCheck();
        }
        mHandling = null;
        if (mProvisionedService != null) {
            if (mProvisionings != null) {
                Provisioning provisioning = mProvisionings.get(mProvisionedService);
                provisioning.cancelAlarm();
                mProvisionedService = null;
            }
        }
        mProvisionings.clear();
    }

    private boolean initEntitlementContext(ArrayList<String> serviceList) {
        log("initEntitlementContext");
        for(String service: serviceList) {
            deProvisionService(service);
        }
        mServiceList.clear();
        mHandling = null;
        mServiceList = serviceList;
        if (!(needRequestNetwork()) || (mNetwork != null)) {
            return true;
        } else {
            log("Waiting to connect network to start entitlement, " + mWaitNetwork);
            if (!mWaitNetwork) {
                requestNetwork();
            }
            return false;
        }
    }

    private boolean isEntitlementEnabled() {
        boolean isEntitlementEnabled = (1 == SystemProperties.getInt
                ("persist.vendor.entitlement_enabled", 1) ? true : false);
        Log.d(TAG, "In EntitlementService, isEntitlementEnabled:" + isEntitlementEnabled);
        return isEntitlementEnabled;
    }
}
