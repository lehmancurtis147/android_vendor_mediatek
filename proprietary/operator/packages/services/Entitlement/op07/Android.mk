LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under, src)
LOCAL_AIDL_INCLUDES := $(LOCAL_PATH)/aidl

# aidl
LOCAL_SRC_FILES += aidl/com/mediatek/entitlement/ISesServiceListener.aidl \
                   aidl/com/mediatek/entitlement/ISesService.aidl \

LOCAL_JAVA_LIBRARIES += telephony-common
LOCAL_JAVA_LIBRARIES += okhttp
LOCAL_JAVA_LIBRARIES += ims-common

# Add for Plug-in, include the plug-in framework
LOCAL_JAVA_LIBRARIES += mediatek-framework mediatek-telephony-base

#LOCAL_STATIC_JAVA_LIBRARIES += com.mediatek.entitlement.ext
LOCAL_STATIC_JAVA_LIBRARIES += android-support-v4 android-support-v7-appcompat

LOCAL_STATIC_JAVA_LIBRARIES += entitlement-utils

LOCAL_RESOURCE_DIR := $(LOCAL_PATH)/res frameworks/support/v7/appcompat/res

LOCAL_STATIC_JAVA_AAR_LIBRARIES := play-services-gcm play-services-iid play-services-basement play-services-base
LOCAL_AAPT_FLAGS += --extra-packages com.google.android.gms --auto-add-overlay
LOCAL_MODULE_TAGS := optional
LOCAL_PACKAGE_NAME := OP07Entitlement
LOCAL_PRIVATE_PLATFORM_APIS := true
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform
LOCAL_PRIVILEGED_MODULE := true

LOCAL_PROGUARD_ENABLED := disabled
LOCAL_PROGUARD_FLAGS := $(proguard.flags)

ifeq ($(strip $(MTK_CIP_SUPPORT)),yes)
LOCAL_MODULE_PATH := $(TARGET_CUSTOM_OUT)/app
else
LOCAL_MODULE_PATH := $(TARGET_OUT)/app
endif

include $(BUILD_PACKAGE)

# GCM
include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := play-services-gcm:../commonlibs/libs/play-services-gcm-9.4.0.aar play-services-iid:../commonlibs/libs/play-services-iid-9.4.0.aar play-services-basement:../commonlibs/libs/play-services-basement-9.4.0.aar play-services-base:../commonlibs/libs/play-services-base-9.4.0.aar


include $(BUILD_MULTI_PREBUILT)

# Other makefiles
include $(call all-makefiles-under, $(LOCAL_PATH))
