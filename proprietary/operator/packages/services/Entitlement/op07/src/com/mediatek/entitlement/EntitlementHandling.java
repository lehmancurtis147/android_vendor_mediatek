package com.mediatek.entitlement;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.mediatek.entitlement.gcm.GcmHandler;

class EntitlementHandling {
    private static final String TAG = "EntitlementHandling";
    private static final boolean DEBUG = true;

    public interface Listener {
        /**
         * Notify state change.
         */
        void onStateChange(int state);

        /**
         * Websheet callback.
         */
        void onWebsheetPost(String url, String serverData);

        /**
         * To pass information discovered during the entitlement process.
         */
        void onInfo(Bundle info);
    }

    private final List<Listener> mListeners = new ArrayList<>();

    private final EntitlementServerApi mApi;

    static final int STATE_NOT_ENTITLED = 0;
    static final int STATE_ACTIVATING = 10;
    static final int STATE_PENDING_WITH_NO_TC = 11;
    static final int STATE_PENDING_WITH_NO_ADDRESS = 12;
    static final int STATE_PENDING_WITH_ADDRESS = 13;
    static final int STATE_ENTITLED = 20;
    static final int STATE_DEACTIVATING = 30;
    static final int STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING = 31;

    private int mState = STATE_NOT_ENTITLED;

    private Context mContext;
    private String mService;

    private static final String DEVICE_GROUP_MEDIATEK = "Mediatek";
    private static final String WFC_AID_VALUE = "wfc_aid_value";

    public static final String ENTITLEMENT_CHECK_RETRY_TIMES = "entitlement-check-retry-times";
    private static final int ENTITLEMENT_CHECK_ERROR_NOT_PROVISIONED = 1048;
    private static final int ENTITLEMENT_CHECK_ERROR_SUSPENDED = 1063;
    private static final int ENTITLEMENT_CHECK_ERROR_SERVER_ERROR = 1111;

    private String mGcmToken; // Get from GcmService, used for managePushNotification.
    private String mMSISDN; // Get from device_activation.
    private String mServiceFingerprint; // Get from device_activation, used for location_registration.
    private String mAidExpiration; // Get from location_registration, used for AID expired checking.

    String mWebsheetToken;
    String mWebsheetUrl;
    boolean mWebsheetPostNeeded;
    int mLastStatus;

    EntitlementHandling(String service, Context context, Listener listener) {
        mService = service;
        mContext = context;
        mListeners.add(listener);

        mApi = new EntitlementServerApi(mContext, getEntitlementApiEndpoint(), getDeviceId(mContext));
    }

    // These 'public' methods will be called by any thread:

    void clearListeners() {
        synchronized (mListeners) {
            mListeners.clear();
        }
    }

    synchronized void startEntitlementCheck() {
        log("startEntitlementCheck: state = " + stateToString(mState));


        switch (mState) {
            case STATE_NOT_ENTITLED:
                updateState(STATE_ACTIVATING);
                if (mGcmToken == null) {
                    log("Query GCM token first");
                    doQueryGcmToken();
                } else {
                    log("Try activating device");
                    doDeviceActivation();
                }
                break;

            case STATE_ENTITLED:
            case STATE_PENDING_WITH_ADDRESS:
                log("Check entitlement status again");
                doEntitlementCheck();
                break;

            case STATE_PENDING_WITH_NO_TC:
            case STATE_PENDING_WITH_NO_ADDRESS:
                log("No TC or address. Should do address registration again.");
                doAddressRegistration(false);
                break;

            case STATE_ACTIVATING:
                log("Ignore request to do entitlement check.");
                break;

            case STATE_DEACTIVATING:
                log("Wait the deactivation to finish before starting entitlement check.");
                updateState(STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING);
                break;

            case STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                log("Already has a pending entitlement check.");
                break;

            default:
                Log.e(TAG, "Unhandled state!");
                break;
        }

        validateAid();
    }

    synchronized void stopEntitlementCheck() {
        log("stopEntitlementCheck: state = " + stateToString(mState));
        switch (mState) {
            case STATE_ACTIVATING:
            case STATE_PENDING_WITH_NO_TC:
            case STATE_PENDING_WITH_NO_ADDRESS:
            case STATE_PENDING_WITH_ADDRESS:
                log("Stop on-going processing");
                updateState(STATE_NOT_ENTITLED);
                break;

            case STATE_ENTITLED:
            case STATE_NOT_ENTITLED:
                log("Already in a stopped state.");
                break;

            case STATE_DEACTIVATING:
                log("Ignored in deactivation.");
                break;

            case STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                updateState(STATE_DEACTIVATING);
                break;

            default:
                Log.e(TAG, "Unhandled state!");
                break;
        }
    }

    synchronized void updateLocationAndTc() {
        log("updateLocationAndTc: state = " + stateToString(mState));
        switch (mState) {
            case STATE_ENTITLED:
                log("Perform address registration again.");
                doAddressRegistration(true);
                break;
            case STATE_ACTIVATING:
            case STATE_PENDING_WITH_NO_TC:
            case STATE_PENDING_WITH_NO_ADDRESS:
            case STATE_PENDING_WITH_ADDRESS:
            case STATE_NOT_ENTITLED:
                log("Cannot updateLocationAndTc at current state");
                break;

            case STATE_DEACTIVATING:
            case STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                log("Ignored in deactivation.");
                break;

            default:
                Log.e(TAG, "Unhandled state!");
                break;
        }
    }

    synchronized void deactivateService() {
        if (mGcmToken != null) {
            log("Has a valid GCM token. Send managePushToken REMOVE.");
            switch (mState) {
                case STATE_ENTITLED:
                case STATE_ACTIVATING:
                case STATE_PENDING_WITH_NO_TC:
                case STATE_PENDING_WITH_NO_ADDRESS:
                case STATE_PENDING_WITH_ADDRESS:
                case STATE_NOT_ENTITLED:
                    updateState(STATE_DEACTIVATING);
                    doDeactivateService();
                    break;

                case STATE_DEACTIVATING:
                case STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                    log("Already in deactivation.");
                    break;

                default:
                    Log.e(TAG, "Unhandled state!");
                    break;
            }
        } else {
            Log.e(TAG, "Push token is null. Do nothing.");
        }
    }

    synchronized void updateGcmToken(String token) {
        log("updateGcmToken: state = " + stateToString(mState));

        boolean isGcmTokenChanged = !token.equals(mGcmToken);
        log("receive GCM token: " + token + ", isChanged:" + isGcmTokenChanged);
        if (isGcmTokenChanged) {
            mGcmToken = token;
            doSendGcmToken(mGcmToken);
        }
    }

    synchronized void validateAid() {
        log("validateAid: state = " + stateToString(mState) + ", mAidExpiration:" + mAidExpiration);

        if (aidExpiresIn48Hours(mAidExpiration) && mGcmToken != null) {
            log("AID expired soon. Should do address registration again.");
            doAddressRegistration(false);
        }
    }

    void handleGcmNotification(Bundle data) {
        log("onHandleGcmNotification: " + data);

        String nsdsNotification = data.getString("nsds-notification");
        if (nsdsNotification == null) {
            Log.e(TAG, "Bad push notification!");
            return;
        }

        log("NSDS notification: " + nsdsNotification);
        try {
            JSONObject jsonObj = new JSONObject(nsdsNotification);
            String eventType = jsonObj.getString("event-type");
            log("Event type: " + eventType);
            switch (eventType) {
                case "entitlement-changed":
                    String eventData = jsonObj.getString("event-data");
                    if (eventData != null) {
                        JSONObject jsonObjEventData = new JSONObject(eventData);
                        JSONArray serviceNames = jsonObjEventData.getJSONArray("service-name");
                        log("Service names: " + serviceNames);
                        for (int i = 0; i < serviceNames.length(); i++) {
                            String service = serviceNames.getString(i);
                            if (service.equals(mService)) {
                                startEntitlementCheck();
                                break;
                            }
                        }
                    }
                    break;
                default:
                    Log.e(TAG, "Unknown event-type: " + eventType);
                    break;
            }
        } catch (JSONException e) {
            Log.e(TAG, "JSON object conversion error: ", e);
        }
    }

    synchronized int getState() {
        return mState;
    }

    private void doQueryGcmToken() {
        log("doQueryGcmToken");
        new Thread() {
            public void run() {
                String token = GcmHandler.getInstance().registerGcmToken(mContext);
                synchronized (EntitlementHandling.this) {
                    if (mState != STATE_ACTIVATING) {
                        log("Probably stopped");
                        return;
                    }

                    mGcmToken = token;
                    if (mGcmToken != null) {
                        doDeviceActivation();
                    } else {
                        Log.e(TAG, "doQueryGcmToken: failed to get GCM registration Token!");
                        updateState(STATE_NOT_ENTITLED);
                    }
                }
            }
        }.start();
    }

    private void doDeviceActivation() {
        mApi.createMethod()
                .addManageConnectivity(0 /* activate */, DEVICE_GROUP_MEDIATEK)
                .addGetMSISDN()
                .execute(new EntitlementServerApi.Callback() {
                    @Override
                    public void callback(EntitlementServerApi.Response rsp) {
                        onDeviceActivationResponse(rsp);
                    }
                });
    }

    private synchronized void onDeviceActivationResponse(EntitlementServerApi.Response rsp) {
        if (mState != STATE_ACTIVATING) {
            log("Probably stopped");
            return;
        }

        if (rsp == null || !rsp.isSuccessful(EntitlementServerApi.METHOD_GET_MSISDN)) {
            Log.e(TAG, "doDeviceActivation: receive failed callback, stop procedure now");
            updateState(STATE_NOT_ENTITLED);
            return;
        }

        String[] epdgAddress = rsp.getStrings(EntitlementServerApi.METHOD_MANAGE_CONN, "epdg-address");
        synchronized (mListeners) {
            for (Listener l : mListeners) {
                Bundle bundle = new Bundle();
                bundle.putStringArray("epdg-address", epdgAddress);
                l.onInfo(bundle);
            }
        }

        mMSISDN = rsp.getString(EntitlementServerApi.METHOD_GET_MSISDN, "msisdn");
        mServiceFingerprint = rsp.getString(EntitlementServerApi.METHOD_GET_MSISDN, "service-fingerprint");

        doAddressRegistration(false);
    }

    private void doAddressRegistration(boolean forceUpdateAddress) {
        mApi.createMethod()
                .addManageLocationAndTC(mServiceFingerprint)
                .addManagePushToken(mMSISDN, "conn-mgr+vowifi", 0, mGcmToken)
                .execute(new EntitlementServerApi.Callback() {
                    @Override
                    public void callback(EntitlementServerApi.Response rsp) {
                        onAddressRegistrationResponse(rsp, forceUpdateAddress);
                    }
                });
    }

    private synchronized void onAddressRegistrationResponse(EntitlementServerApi.Response rsp, boolean forceUpdateAddress) {
        if (mState != STATE_ACTIVATING && mState != STATE_ENTITLED
                && mState != STATE_PENDING_WITH_NO_TC && mState != STATE_PENDING_WITH_NO_ADDRESS) {
            log("Probably stopped");
            return;
        }
        if (rsp == null || !rsp.isSuccessful(EntitlementServerApi.METHOD_LOCATION_TC)) {
            log("doAddressRegistration: receive failed callback, stop procedure now");
            updateState(STATE_NOT_ENTITLED);
            return;
        }

        mWebsheetToken = rsp.getString(EntitlementServerApi.METHOD_LOCATION_TC, "server-data");
        mWebsheetUrl = rsp.getString(EntitlementServerApi.METHOD_LOCATION_TC, "server-url");
        String aid = rsp.getString(EntitlementServerApi.METHOD_LOCATION_TC, "address-id");
        mAidExpiration = rsp.getString(EntitlementServerApi.METHOD_LOCATION_TC, "aid-expiration");

        boolean hasProvisionInfo = rsp.checkValueExisted(EntitlementServerApi.METHOD_LOCATION_TC, "service-status");
        int provisionState = rsp.getInt(EntitlementServerApi.METHOD_LOCATION_TC, "service-status");
        boolean needCheckTc = rsp.checkValueExisted(EntitlementServerApi.METHOD_LOCATION_TC, "tc-status");
        boolean tcStatus = rsp.getBoolean(EntitlementServerApi.METHOD_LOCATION_TC, "tc-status");
        boolean needCheckLocation = rsp.checkValueExisted(EntitlementServerApi.METHOD_LOCATION_TC, "location-status");
        boolean locationStatus = rsp.getBoolean(EntitlementServerApi.METHOD_LOCATION_TC, "location-status");
        saveAidValue(mContext, aid);

        log("doAddressRegistration: response received: " +
                "hasProvisionInfo=" + hasProvisionInfo + ", provisionState=" + provisionState +
                ", needCheckTc=" + needCheckTc + ", tcStatus=" + tcStatus +
                ", needCheckLocation=" + needCheckLocation + ", locationStatus=" + locationStatus +
                ", aid=" + aid +
                ", isAidValid()=" + isAidValid(aid) +
                ", forceUpdateAddress:" + forceUpdateAddress);

        if (needCheckTc && !tcStatus) {
            updateState(STATE_PENDING_WITH_NO_TC);
        } else if (needCheckLocation && (!locationStatus || !isAidValid(aid))) {
            updateState(STATE_PENDING_WITH_NO_ADDRESS);
        } else {
            updateState(STATE_PENDING_WITH_ADDRESS);
        }

        if (mState == STATE_PENDING_WITH_NO_TC ||
             mState == STATE_PENDING_WITH_NO_ADDRESS ||
             forceUpdateAddress) {
            mWebsheetPostNeeded = true;
        } else {
            mWebsheetPostNeeded = false;
        }

        // LTE-BTR-5-7106, verify that after device restarts,
        // the device performs a Service Entitlement check automatically
        doEntitlementCheck();
    }

    private void onWebsheetPost() {
        log("onWebsheetPost(), url: " + mWebsheetUrl + ", token:" + mWebsheetToken);

        synchronized (mListeners) {
            for (Listener l : mListeners) {
                l.onWebsheetPost(mWebsheetUrl, mWebsheetToken);
            }
        }
    }

    private void doEntitlementCheck() {
        mApi.createMethod()
                .addServiceEntitlementStatus(new String[]{mService})
                .execute(new EntitlementServerApi.Callback() {
                    @Override
                    public void callback(EntitlementServerApi.Response rsp) {
                        onEntitlementCheckResponse(rsp);
                    }
                });
    }

    private synchronized void onEntitlementCheckResponse(EntitlementServerApi.Response rsp) {
        boolean isEntitled;
        if (rsp == null || !rsp.isSuccessful(EntitlementServerApi.METHOD_CHECK_ENTITLEMENT)) {
            Log.e(TAG, "Receive failed callback, rsp:" + rsp);
            isEntitled = false;
        } else {
            isEntitled = rsp.getEntitlementState(EntitlementServerApi.METHOD_CHECK_ENTITLEMENT, mService);
        }
        log("Entitlement result: " + isEntitled + ", mWebsheetPostNeeded:" + mWebsheetPostNeeded +
            ", mLastStatus:" + mLastStatus);


        if (isEntitled) {
            mLastStatus = ErrorCodes.REQUEST_SUCCESSFUL;

            switch (mState) {
                case STATE_ACTIVATING:
                case STATE_PENDING_WITH_NO_TC:
                case STATE_PENDING_WITH_NO_ADDRESS:
                    log("Keep in the same state to finish the remain flow");
                    break;
                default:
                    updateState(STATE_ENTITLED);
                    break;
            }
        } else {

            int status = -1;
            // LTE_BTR-5-7250: 1063 and 1111
            if (rsp != null) {
                status = rsp.getEntitlementStateValue(
                    EntitlementServerApi.METHOD_CHECK_ENTITLEMENT, mService);

                // step2: avoid websheet request after 1063 and no retry
                // step4: avoid websheet request after 1111 and retry 4 times

                if (status == ENTITLEMENT_CHECK_ERROR_SUSPENDED /* 1063 */) {
                    mWebsheetPostNeeded = false;  // avoid simulator thinks UE retry
                    onInfoEntitlementCheckRetryTimes(0);
                } else if (status == ENTITLEMENT_CHECK_ERROR_SERVER_ERROR /* 1111 */) {
                    mWebsheetPostNeeded = false;

                    // Retry 4 times when receives 1111 for the first time
                    if (mLastStatus != status) {
                        onInfoEntitlementCheckRetryTimes(4);
                }
                }
            }
            mLastStatus = status;

            /* No response or other error code */
            switch (mState) {
                case STATE_ACTIVATING:
                case STATE_NOT_ENTITLED:
                case STATE_PENDING_WITH_ADDRESS:
                case STATE_PENDING_WITH_NO_TC:
                case STATE_PENDING_WITH_NO_ADDRESS:
                    log("Keep in the same state even though entitlement check failed");
                    break;

                case STATE_ENTITLED:
                    updateState(STATE_NOT_ENTITLED);
                    break;

                default:
                    Log.e(TAG, "Unhandled state!");
                    break;
            }
        }

        if (mWebsheetPostNeeded) {
            onWebsheetPost();
            mWebsheetPostNeeded = false;
        }
    }

    private void doSendGcmToken(String token) {
        mApi.createMethod()
                .addManagePushToken(mMSISDN, "conn-mgr+vowifi", 0, token)
                .execute(new EntitlementServerApi.Callback() {
                    @Override
                    public void callback(EntitlementServerApi.Response rsp) {
                        if (rsp == null || !rsp.isSuccessful(EntitlementServerApi.METHOD_MANAGE_TOKEN)) {
                            log("doSendGcmToken: receive failed callback, stop procedure now");
                        }
                    }
                });
    }

    private void doDeactivateService() {
        mApi.createMethod()
                .addManagePushToken(mMSISDN, "conn-mgr+vowifi", 1, mGcmToken)
                .execute(new EntitlementServerApi.Callback() {
                    @Override
                    public void callback(EntitlementServerApi.Response rsp) { onDeactivateServiceResponse(rsp); }
                });
    }

    private synchronized void onDeactivateServiceResponse(EntitlementServerApi.Response rsp) {
        mGcmToken = null;
        if (rsp == null || !rsp.isSuccessful(EntitlementServerApi.METHOD_MANAGE_TOKEN)) {
            Log.e(TAG, "Received failed callback");
        }
        if (mState == STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING) {
            updateState(STATE_NOT_ENTITLED);
            startEntitlementCheck();
        } else {
            updateState(STATE_NOT_ENTITLED);
        }
    }

    private synchronized void updateState(int state) {
        log("updateState: from " + stateToString(mState) + " to " + stateToString(state));
        mState = state;

        synchronized (mListeners) {
            for (Listener l : mListeners) {
                l.onStateChange(mState);
            }
        }
    }

    private synchronized void onInfoEntitlementCheckRetryTimes(int times) {
        log("onInfoEntitlementCheckRetryTimes(), times:" + times);
        synchronized (mListeners) {
            for (Listener l : mListeners) {
                Bundle bundle = new Bundle();

                // only put key, ignore value
                bundle.putInt(ENTITLEMENT_CHECK_RETRY_TIMES, times);
                l.onInfo(bundle);
            }
        }

    }

    private boolean isAidValid(String aid) {
        return !(aid == null || aid.equals(""));
    }

    private String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    private void saveAidValue(Context context, String aid) {
        if (!android.provider.Settings.Global.putString(context.getContentResolver(),
            WFC_AID_VALUE, aid)) {
            Log.e(TAG, "Save AID failed");
        }
    }

    public static String stateToString(int state) {
        switch (state) {
            case STATE_ACTIVATING:
                return "STATE_ACTIVATING";
            case STATE_NOT_ENTITLED:
                return "STATE_NOT_ENTITLED";
            case STATE_PENDING_WITH_NO_TC:
                return "STATE_PENDING_WITH_NO_TC";
            case STATE_PENDING_WITH_NO_ADDRESS:
                return "STATE_PENDING_WITH_NO_ADDRESS";
            case STATE_PENDING_WITH_ADDRESS:
                return "STATE_PENDING_WITH_ADDRESS";
            case STATE_ENTITLED:
                return "STATE_ENTITLED";
            case STATE_DEACTIVATING:
                return "STATE_DEACTIVATING";
            case STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                return "STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING";
            default:
                return "UNKNOWN state";
        }
    }

    private boolean aidExpiresIn48Hours(String aidExpiration) {
        if (aidExpiration == null || aidExpiration.length() == 0) {
            return false;
        }

        boolean result = false;

        Date currentDate = new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR, 48);
        Date twoDaysLater = cal.getTime();

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.getDefault()); // Quoted "Z" to indicate UTC, no timezone offset
        df.setTimeZone(tz);

        try {
            result = twoDaysLater.after(df.parse(aidExpiration));
            log("aidExpiresIn48Hours: " + result);
        } catch (java.text.ParseException e) {
            Log.e(TAG, "UTC ISO8601 parse exception!", e);
        }
        return result;
    }

    private String getEntitlementApiEndpoint() {
        // Possible endpoints:
        // - AT&T Lab SES Server: https://sentitlement2.npc.mobilephone.net/WFC
        // - AT&T live Network SES Server: https://sentitlement2.mobile.att.net/WFC
        // - Ericson old SES Testing server: http://ses.ericsson-magic.net:10090/generic_devices
        // - Ericson new SES Testing server: http://ses.ericsson-magic.net:8080/generic_devices
        //
        // By default, connect to AT&T live network SES server.
        return SystemProperties.get("persist.vendor.entitlement.sesurl", "https://sentitlement2.mobile.att.net/WFC");
    }

    private void log(String s) {
        if (DEBUG) Log.d(TAG, s);
    }
}
