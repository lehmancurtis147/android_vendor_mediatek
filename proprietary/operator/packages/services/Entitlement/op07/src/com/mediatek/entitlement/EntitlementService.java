package com.mediatek.entitlement;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.SubscriptionManager;
import android.util.Log;
import com.android.ims.ImsManager;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.internal.telephony.MtkSubscriptionManager;

import java.util.HashMap;
import java.util.Map;

interface Consumer<T> {
    void accept(T t);
}

public class EntitlementService extends Service {
    private static final String TAG = "EntitlementService";
    private static final boolean DEBUG = true;

    private EntitlementHandling mEntitlementHandling;

    private final Map<String, Provisioning> mProvisionings = new HashMap<>();
    private final RemoteCallbackList<ISesServiceListener> mListeners = new RemoteCallbackList<>();

    private static final String VOWIFI_SERVIVE = "vowifi";

    private static final String BROADCAST_REGID_UPDATE = "com.mediatek.entitlement.gcm.RegistrationIdUpdate";
    private static final String BROADCAST_PUSH_NOTIFICATION = "com.mediatek.entitlement.gcm.PushNotification";

    static final String ACTION_PROVISIONING_PENDING = "com.mediatek.entitlement.ACTION_PROVISIONING_PENDING";
    static final String ACTION_ENTITLEMENT_CHECK = "com.mediatek.entitlement.ACTION_ENTITLEMENT_CHECK";

    static private final int AUTONOMOUS_STATUS_OFF = 0;
    static private final int AUTONOMOUS_STATUS_ON = 1;
    static private final int AUTONOMOUS_STATUS_CHECK = 2;
    private int mAutonomousCheck = AUTONOMOUS_STATUS_ON;

    private boolean mNetworkAvailable;
    private boolean mSimLoaded;

    // Periodically check whether a service is still pending.
    private class Provisioning {
        private final String mService;
        private final int mAlarmSec;
        private int mRetryTimes;

        Provisioning(String service, int alarmSec, int retryTimesMax) {
            Log.d(TAG, "Provisioning(), alarmSec:" + alarmSec + ", times:" + retryTimesMax);
            mService = service;
            mAlarmSec = alarmSec;
            mRetryTimes = retryTimesMax;

            setAlarm();
        }

        synchronized boolean shouldRetry() {
            return mRetryTimes > 0;
        }

        synchronized boolean retry() {
            if (!shouldRetry()) {
                return false;
            }

            mRetryTimes--;
            getHandling().startEntitlementCheck();
            return true;
        }

        void setAlarm() {
            Log.d(TAG, "Provisioning setAlarm(), alarmSec:" + mAlarmSec +
                ", times:" + mRetryTimes);

            PendingIntent pi = PendingIntent.getBroadcast(
                           getApplicationContext(),
                           1,
                           new Intent(ACTION_PROVISIONING_PENDING),
                           PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * mAlarmSec, pi);
        }

        void cancelAlarm() {
            Log.d(TAG, "Provisioning, cancelAlarm()");
            PendingIntent pi = PendingIntent.getBroadcast(
                           getApplicationContext(),
                           1,
                           new Intent(ACTION_PROVISIONING_PENDING),
                           PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    // Once a service is entitled, check the entitlement daily
    private class EntitlementCheck {
        void setAlarm(String service) {
            Log.d(TAG, "EntitlementCheck, setAlarm()");
            PendingIntent pi = PendingIntent.getBroadcast(
                           getApplicationContext(),
                           0,
                           new Intent(ACTION_ENTITLEMENT_CHECK),
                           PendingIntent.FLAG_UPDATE_CURRENT);

            AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + AlarmManager.INTERVAL_DAY, AlarmManager.INTERVAL_DAY, pi);
        }

        void cancelAlarm(String service) {
            Log.d(TAG, "EntitlementCheck, cancelAlarm()");
            PendingIntent pi = PendingIntent.getBroadcast(
                           getApplicationContext(),
                           0,
                           new Intent(ACTION_ENTITLEMENT_CHECK),
                           PendingIntent.FLAG_CANCEL_CURRENT);

            AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            alarmManager.cancel(pi);
        }
    }

    private EntitlementCheck mEntitlementCheck = new EntitlementCheck();

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {

            if (!isEntitlementEnabled()) {
                Log.d(TAG, "onReceive(), isEntitlementEnabled is false, return directly");
                return;
            }

            String action = intent.getAction();
            log("onReceive(), action:" + action);

            if (TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED.equals(action)) {
                if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                                       MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                        == MtkSubscriptionManager.EXTRA_VALUE_REMOVE_SIM) {
                    log("REMOVE_SIM, reset all state and set wfc disabled");


                    notifyListeners(new Consumer<ISesServiceListener>() {
                        @Override
                        public void accept(ISesServiceListener l) {
                            try {
                                l.onEntitlementEvent(VOWIFI_SERVIVE, "remove_sim", null);
                            } catch (RemoteException ignored) {
                            }
                        }
                    });

                    // Destroy handling and re-create again if need when sim switch
                    getHandling().clearListeners();
                    mEntitlementHandling = null;


                    // in case of reset state, turn off vowifi
                    ImsManager.setWfcSetting(getApplicationContext(), false);
                } else if (intent.getIntExtra(MtkSubscriptionManager.INTENT_KEY_DETECT_STATUS,
                                              MtkSubscriptionManager.EXTRA_VALUE_NOCHANGE)
                           == MtkSubscriptionManager.EXTRA_VALUE_NEW_SIM) {
                    log("NEW_SIM, notify UI");


                    notifyListeners(new Consumer<ISesServiceListener>() {
                        @Override
                        public void accept(ISesServiceListener l) {
                            try {
                                l.onEntitlementEvent(VOWIFI_SERVIVE, "new_sim", null);
                            } catch (RemoteException ignored) {
                            }
                        }
                    });

                }
            } else if (intent.getAction().equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {

                String state = intent.getStringExtra(IccCardConstants.INTENT_KEY_ICC_STATE);

                Log.d(TAG, "SIM_STATE_CHANGED, state:" + state);

                mSimLoaded = state.equals(IccCardConstants.INTENT_VALUE_ICC_LOADED) ? true : false;
                if (mSimLoaded) {
                    startAutonomousCheck();
                }

            } else if (BROADCAST_REGID_UPDATE.equals(action)) {
                String regId = intent.getStringExtra("RegId");

                getHandling().updateGcmToken(regId);

            } else if (BROADCAST_PUSH_NOTIFICATION.equals(action)) {

                getHandling().handleGcmNotification(intent.getExtras());

            } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
                boolean isAirplaneModeOn = intent.getBooleanExtra("state", false);

                // CDR-WiFi-1120: check AID expire time
                if (mNetworkAvailable && !isAirplaneModeOn) {
                    getHandling().validateAid();
                }
            } else if (ACTION_ENTITLEMENT_CHECK.equals(action)) {

                getHandling().startEntitlementCheck();

            } else if (ACTION_PROVISIONING_PENDING.equals(action)) {

                synchronized (mProvisionings) {
                    Provisioning provisioning = mProvisionings.get(VOWIFI_SERVIVE);
                    if (provisioning != null) {
                        provisioning.retry();
                    }
                }
            }

        }
    };

    @Override
    public void onCreate() {
        super.onCreate();

        Log.d(TAG, "onCreate()");

        if (!isEntitlementEnabled()) {
            Log.d(TAG, "onCreate(), isEntitlementEnabled is false, return directly");
            return;
        }

        final IntentFilter filter = new IntentFilter();
        /// M: Dynamic SIM Switch
        filter.addAction(TelephonyIntents.ACTION_SUBINFO_RECORD_UPDATED);
        filter.addAction(BROADCAST_REGID_UPDATE);
        filter.addAction(BROADCAST_PUSH_NOTIFICATION);
        filter.addAction(ACTION_ENTITLEMENT_CHECK);
        filter.addAction(ACTION_PROVISIONING_PENDING);
        filter.addAction(Intent.ACTION_AIRPLANE_MODE_CHANGED);
        filter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
        registerReceiver(mBroadcastReceiver, filter);

        registerDefaultNetwork();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "onDestroy(), check why ?");

        if (!isEntitlementEnabled()) {
            Log.d(TAG, "onDestroy(), isEntitlementEnabled is false, return directly");
            return;
        }
        unregisterReceiver(mBroadcastReceiver);
    }

    private EntitlementHandling getHandling() {
        if (mEntitlementHandling == null) {
            // Create entitlement handling
            mEntitlementHandling = new EntitlementHandling(VOWIFI_SERVIVE, getApplicationContext(),
                mEntitlementHandlingListener);
        }
        return mEntitlementHandling;
    }

    EntitlementHandling.Listener mEntitlementHandlingListener = new EntitlementHandling.Listener() {
        @Override
        public void onStateChange(int state) {
            Log.d(TAG, "onStateChange(), state:" + EntitlementHandling.stateToString(state) +
                ", mAutonomousCheck:" + mAutonomousCheck);

            boolean notify = (mAutonomousCheck != AUTONOMOUS_STATUS_OFF) ? false: true;
            boolean retryFail = false;

            if (state == EntitlementHandling.STATE_ENTITLED) {
                mEntitlementCheck.setAlarm(VOWIFI_SERVIVE);

                // no need to retry in 30/60mins or 2mins (autonomous check)
                removeProvisioning(VOWIFI_SERVIVE);

                //CDR-WiFi-1080: 1. toggle WFC on automatically
                if (mAutonomousCheck != AUTONOMOUS_STATUS_OFF) {
                    checkIfEnableWfc();
                }
            } else {
                mEntitlementCheck.cancelAlarm(VOWIFI_SERVIVE);

                if (state == EntitlementHandling.STATE_NOT_ENTITLED ||
                    state == EntitlementHandling.STATE_PENDING_WITH_NO_TC ||
                    state == EntitlementHandling.STATE_PENDING_WITH_NO_ADDRESS ||
                    state == EntitlementHandling.STATE_PENDING_WITH_ADDRESS) {
                    synchronized (mProvisionings) {
                        Provisioning provisioning = mProvisionings.get(VOWIFI_SERVIVE);
                        if (provisioning != null) {
                            if (provisioning.shouldRetry()) {
                                provisioning.setAlarm();
                            } else {
                                // provisioning failed
                                mProvisionings.remove(VOWIFI_SERVIVE);
                                log("Retry done. Notify entitlement failed");
                                // For LTE-BTR-5-7112, show "provisioning failed, please try again later"
                                // LTE-BTR-5-7112, retrying stops on STATE_PENDING_WITH_ADDRESS
                                retryFail = true;
                            }
                        }
                    }
                }

                //CDR-WiFi-1080: 2.b: toggle wfc off if no E911AID
                if (state == EntitlementHandling.STATE_NOT_ENTITLED ||
                    state == EntitlementHandling.STATE_PENDING_WITH_NO_TC ||
                    state == EntitlementHandling.STATE_PENDING_WITH_NO_ADDRESS) {
                        Log.d(TAG, "toggle wfc off automatically");
                        ImsManager.setWfcSetting(getApplicationContext(), false);
                }
            }

            if (notify) {
                final boolean failed = retryFail;
                notifyListeners(new Consumer<ISesServiceListener>() {
                    @Override
                    public void accept(ISesServiceListener l) {
                        try {
                            if (failed) {
                                l.onEntitlementEvent(VOWIFI_SERVIVE, "failed", null);
                            } else {
                                l.onEntitlementEvent(VOWIFI_SERVIVE, translateToExternalState(state), null);
                            }
                        } catch (RemoteException ignored) {
                        }
                    }
                });
            }
        }

        @Override
        public void onWebsheetPost(String url, String serverData) {
            Log.d(TAG, "onWebsheetPost(), url: " + url +
                ", mAutonomousCheck:" + mAutonomousCheck);

            // CDR-WiFi-1080: no open websheet URL when autonomous entitlement check
            if (mAutonomousCheck != AUTONOMOUS_STATUS_OFF) {
                return;
            }

            notifyListeners(new Consumer<ISesServiceListener>() {
                @Override
                public void accept(ISesServiceListener l) {
                    try {
                        l.onWebsheetPost(url, serverData);
                    } catch (RemoteException ignored) {
                    }
                }
            });
        }

        @Override
        public void onInfo(Bundle info) {
            Log.d(TAG, "Info discovered: " + info);

            // Handle internal info first
            boolean handled = handleInternalInfo(VOWIFI_SERVIVE, info);
            if (handled) {
                return;
            }

            notifyListeners(new Consumer<ISesServiceListener>() {
                @Override
                public void accept(ISesServiceListener l) {
                    try {
                        l.onEntitlementEvent(VOWIFI_SERVIVE, "info", info);
                    } catch (RemoteException ignored) {
                    }
                }
            });
        }
    };

    private boolean handleInternalInfo(String service, Bundle info) {

        if (info.containsKey(EntitlementHandling.ENTITLEMENT_CHECK_RETRY_TIMES)) {
            int times = info.getInt(EntitlementHandling.ENTITLEMENT_CHECK_RETRY_TIMES);
            Log.d(TAG, "ENTITLEMENT_CHECK_RETRY_TIMES times: " + times);

            // LTE_BTR-5-7250: step2, 1063 no retry
            if (times == 0) {
                removeProvisioning(service);
            }
            // LTE_BTR-5-7250: step4, retry 4 times with 30s interval
            else if (times == 4) {
            removeProvisioning(service);
                initProvisioning(service, 30, times);
            }
            return true;
        }
        return false;
    }

    private void notifyListeners(Consumer<ISesServiceListener> consumer) {
        synchronized (mListeners) {
            int i = mListeners.beginBroadcast();
            while (i > 0) {
                i--;
                consumer.accept(mListeners.getBroadcastItem(i));
            }
            mListeners.finishBroadcast();
        }
    }

    private void initProvisioning(String service, int retryTimer, int retryTimes) {
        synchronized (mProvisionings) {
            // For LTE-BTR-5-7112, avoid websheet reset retry counter
            Provisioning provisioning = mProvisionings.get(service);
            if (provisioning == null || !provisioning.shouldRetry()) {
                mProvisionings.put(service, new Provisioning(service, retryTimer, retryTimes));
            }
        }
    }

    private void removeProvisioning(String service) {
        synchronized (mProvisionings) {
            Provisioning provisioning = mProvisionings.get(service);
            if (provisioning != null) {
                log("removeProvisioning(), cancel alarm");
                provisioning.cancelAlarm();
                mProvisionings.remove(service);
            }
        }
    }


    @Override
    public IBinder onBind(Intent intent) {
        log("onBind: " + intent);
        return new ISesService.Stub() {
            @Override
            public void startEntitlementCheck(String service, int retryTimer, int retryTimes) {
                log("[EXT]startEntitlementCheck(), service:" + service +
                    ", retryTimer:" + retryTimer + ", retryTimes:" + retryTimes +
                    ", mAutonomousCheck:" + mAutonomousCheck);

                // Cancel autonomous task
                removeProvisioning(service);
                mAutonomousCheck = AUTONOMOUS_STATUS_OFF;


                if (retryTimer > 0) {
                    initProvisioning(service, retryTimer * 60, retryTimes);
                } else {
                    getHandling().startEntitlementCheck();
                }
            }

            @Override
            public void stopEntitlementCheck(String service) {
                log("[EXT]stopEntitlementCheck()");

                getHandling().stopEntitlementCheck();
            }

            @Override
            public void updateLocationAndTc(String service) {
                log("[EXT]updateLocationAndTc()");

                getHandling().updateLocationAndTc();
            }

            @Override
            public String getCurrentEntitlementState(String service) {
                log("[EXT]getCurrentEntitlementState()");

                return translateToExternalState(getHandling().getState());
            }

            @Override
            public void registerListener(ISesServiceListener listener) {
                log("[EXT]registerStateListener");
                if (listener != null) {
                    synchronized (mListeners) {
                        log("registerStateListener " + listener.getClass().toString());
                        mListeners.register(listener);
                    }
                }
            }

            @Override
            public void unregisterListener(ISesServiceListener listener) {
                if (listener != null) {
                    synchronized (mListeners) {
                        log("[EXT]unregisterStateListener " + listener.getClass().toString());
                        mListeners.unregister(listener);
                    }
                }
            }

            @Override
            public int getLastErrorCode() {
                int errCode = ErrorCodes.getErrorCode();
                log("[EXT]getErrorCode = " + errCode);
                return errCode;
            }

            @Override
            public void deactivateService(String service) {
                log("[EXT]deactivateService");

                getHandling().deactivateService();
            }
        };
    }

    private String translateToExternalState(int internalState) {
        switch (internalState) {
            case EntitlementHandling.STATE_NOT_ENTITLED:
                return "not-entitled";
            case EntitlementHandling.STATE_ACTIVATING:
            case EntitlementHandling.STATE_PENDING_WITH_NO_TC:
            case EntitlementHandling.STATE_PENDING_WITH_NO_ADDRESS:
            case EntitlementHandling.STATE_PENDING_WITH_ADDRESS:
            case EntitlementHandling.STATE_DEACTIVATING:
            case EntitlementHandling.STATE_DEACTIVATING_WITH_ENTITLEMENT_PENDING:
                return "pending";
            case EntitlementHandling.STATE_ENTITLED:
                return "entitled";
            default:
                Log.e(TAG, "Unhandled state: " + internalState);
                return "Unhandled";
        }
    }

    private void checkIfEnableWfc() {

        int setting = SubscriptionManager.getIntegerSubscriptionProperty(
                SubscriptionManager.getSubId(0)[0], SubscriptionManager.WFC_IMS_ENABLED,
                -1, getApplicationContext());
        Log.d(TAG, "checkIfEnableWfc(), setting:" + setting);
        // CDR-WiFi-1080: wfc toggle was not turned off manually by the user
        if (setting == -1) {
            ImsManager.setWfcSetting(getApplicationContext(),true);
        }
    }

    private void startAutonomousCheck() {
        Log.d(TAG, "startAutonomousCheck(), mAutonomousCheck:" + mAutonomousCheck +
            ", mSimLoaded:" + mSimLoaded + ", mNetworkAvailable:" + mNetworkAvailable);

        if (mAutonomousCheck == AUTONOMOUS_STATUS_ON && mSimLoaded && mNetworkAvailable) {

            mAutonomousCheck = AUTONOMOUS_STATUS_CHECK;
            getHandling().startEntitlementCheck();

            // CDR-WiFi-1080: device should retry once in 5 mins
            initProvisioning(VOWIFI_SERVIVE, 2 * 60, 1);
        }
    }

    private void registerDefaultNetwork() {

        ConnectivityManager cm = ConnectivityManager.from(getApplicationContext());
        cm.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Log.d(TAG, "NetworkCallback.onAvailable(), mAutonomousCheck:" + mAutonomousCheck);

                mNetworkAvailable = true;

                startAutonomousCheck();
            }

            @Override
            public void onLost(Network network) {
                Log.d(TAG, "NetworkCallback.onLost()");

                mNetworkAvailable = false;

            }
        });
    }

    private boolean isEntitlementEnabled() {
        boolean isEntitlementEnabled = (1 == SystemProperties.getInt
                ("persist.vendor.entitlement_enabled", 1) ? true : false);
        return isEntitlementEnabled;
    }


    private static void log(String s) {
        if (DEBUG) Log.d(TAG, s);
    }
}
