package com.mediatek.entitlement.gcm;

import android.content.Context;
import android.util.Log;
import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

public class GcmHandler {

    private static final String TAG = "Entitlement-gcm";
    private static final String[] TOPICS = {"global"};

    private static GcmHandler sInstance;

    public static GcmHandler getInstance() {
        if (sInstance == null) {
            sInstance = new GcmHandler();
        }
        return sInstance;
    }

    /*
     * trigger register for gcm, and get the token.
     *
     * @return GCM Token.
     */
    public String registerGcmToken(Context context) {
        try {
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            InstanceID instanceID = InstanceID.getInstance(context);
            String token = instanceID.getToken("268807506190",
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            Log.i(TAG, "GCM Registration Token: " + token);

            // Subscribe to topic channels
            subscribeTopics(token, context);

            return token;
        } catch (Exception e) {
            Log.e(TAG, "Failed to complete token refresh", e);
        }

        return null;
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */
    private void subscribeTopics(String token, Context context) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(context);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
}
