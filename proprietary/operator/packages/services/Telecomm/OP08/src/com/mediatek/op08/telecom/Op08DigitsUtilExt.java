/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op08.telecom;

import android.content.Context;
import android.os.Bundle;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import com.android.server.telecom.Call;
import com.mediatek.server.telecom.ext.DefaultDigitsUtilExt;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import mediatek.telecom.MtkPhoneAccount;
import mediatek.telecom.MtkTelecomManager;

public class Op08DigitsUtilExt extends DefaultDigitsUtilExt {

    private static final String VIRTUAL_ACCOUNT_ID_PREFIX = "virtual_";
    private static final String PARENT_ACCOUNT_ID_PREFIX = "_parentid_";

    @Override
    public void putExtrasForConnectionRequest(Bundle extras, Object call) {
        if ((extras != null) && (call instanceof Call)) {
            Call curCall = (Call) call;
            PhoneAccountHandle phoneAccountHandle = curCall.getTargetPhoneAccount();

            if (isPotentialVirtualPhoneAccount(phoneAccountHandle)) {
                extras.putParcelable(MtkTelecomManager.EXTRA_CALLING_VIA_PHONE_ACCOUNT_HANDLE,
                        phoneAccountHandle);
            }
        }
    }

    private boolean isPotentialVirtualPhoneAccount(PhoneAccountHandle phoneAccountHandle) {
        return phoneAccountHandle != null && phoneAccountHandle.getId() != null
                && phoneAccountHandle.getId().startsWith(VIRTUAL_ACCOUNT_ID_PREFIX)
                && phoneAccountHandle.getId().indexOf(PARENT_ACCOUNT_ID_PREFIX) > -1;
    }

    @Override
    public void updatePhoneAccounts(List<PhoneAccountHandle> targetAccounts,
            PhoneAccountHandle currentAccount) {
        if (targetAccounts == null || currentAccount == null) {
            return;
        }

        String parentId = idToParentId(currentAccount.getId());
        for (Iterator<PhoneAccountHandle> it = targetAccounts.iterator(); it.hasNext(); ) {
            PhoneAccountHandle account = it.next();
            if (idToParentId(account.getId()).equals(parentId)) {
                it.remove();
            }
        }
    }

    @Override
    public void sortPhoneAccounts(List<PhoneAccount> accounts,
            Comparator<PhoneAccount> bySimCapability,
            Comparator<PhoneAccount> bySortOrder,
            Comparator<PhoneAccount> byLabel) {
        if (accounts != null && accounts.size() > 1 && bySimCapability != null
                && bySortOrder != null && byLabel != null) {
            // Comparator to sort actual account before virtual account
            Comparator<PhoneAccount> byVirtualCapability = (p1, p2) -> {
                if (p1.hasCapabilities(MtkPhoneAccount.CAPABILITY_IS_VIRTUAL_LINE)
                        && !p2.hasCapabilities(MtkPhoneAccount.CAPABILITY_IS_VIRTUAL_LINE)) {
                    return 1;
                } else if (!p1.hasCapabilities(MtkPhoneAccount.CAPABILITY_IS_VIRTUAL_LINE)
                        && p2.hasCapabilities(MtkPhoneAccount.CAPABILITY_IS_VIRTUAL_LINE)) {
                    return -1;
                } else {
                    return 0;
                }
            };

            // Sort the phone accounts using new sort order:
            // 1) SIM accounts first, followed by non-sim accounts
            // 2) Sort order, with those specifying no sort order last.
            // 3) Actual account first, followed by virtual accounts
            // 4) Label
            // Example:
            // SIM1 actual account a1(label:CMCC), has virtual account v1(label:B), v2(label:A)
            // SIM2 actual account a2(label:CU) only
            // Other account o1(label:A)
            // Original order   : v2, v1, a1, a2, o1
            // Order after sort : a1, v2, v1, a2, o1
            accounts.sort(bySimCapability.thenComparing(bySortOrder.thenComparing(
                    byVirtualCapability.thenComparing(byLabel))));
        }
    }

    private String idToParentId(String id) {
        if (id != null && id.startsWith(VIRTUAL_ACCOUNT_ID_PREFIX)) {
            int index = id.indexOf(PARENT_ACCOUNT_ID_PREFIX);
            if (index > -1) {
                return id.substring(index + PARENT_ACCOUNT_ID_PREFIX.length());
            }
        }
        return id;
    }
}
