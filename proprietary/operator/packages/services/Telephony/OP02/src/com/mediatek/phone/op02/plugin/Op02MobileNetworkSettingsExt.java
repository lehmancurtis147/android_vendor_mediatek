/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.phone.op02.plugin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;

import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.TelephonyIntents;

import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;


public class Op02MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String LOG_TAG = "Op02MobileNetworkSettingsExt";
    private static final String[] MCCMNC_TABLE_TYPE_CU = {
        "46001", "46006", "46009", "45407", "46005"};
    private static final String MULTI_IMS_SUPPORT = "persist.vendor.mims_support";

    private Context mContext;
    public Op02MobileNetworkSettingsExt(Context context) {
        super();
        mContext = context;
        log("mContext =" + mContext);
    }

    public void log(String msg) {
        Log.d(LOG_TAG, msg);
    }

    /**
     * For CMCC VOLTE feature.
     * when is CMCC card, VOLTE show enable.
     * else VOLTE show disable.
     * SIM_STATE_CHANGED broadcast register.
     * @param intentFilter SIM_STATE_CHANGED.
     */
    @Override
    public void customizeDualVolteIntentFilter(IntentFilter intentFilter) {
        log("customizeDualVolteIntentFilter");
        intentFilter.addAction(TelephonyIntents.ACTION_SIM_STATE_CHANGED);
    }

    /**
     * For CU dual VOLTE feature.
     * @param subId sub id
     * @param enableForCtVolte enhance4glte state.
     * @return true if this is CU card AND has LTE network
     */
    @Override
    public boolean customizeDualVolteOpDisable(int subId, boolean enableForCtVolte) {
        if (isCUCard(subId)) {
            return isLTENetwork(subId);
        }
        return super.customizeDualVolteOpDisable(subId, enableForCtVolte);
    }

    private boolean isLTENetwork(int subId) {
        int type = TelephonyManager.getDefault().getNetworkType(subId);
        log("network type = " + type);
        return TelephonyManager.NETWORK_TYPE_LTE == type;
    }

    /**
     * For CU VOLTE feature.
     * when is CU card, VOLTE show enable.
     * else VOLTE show disable.
     * SIM_STATE_CHANGED broadcast dual with.
     * @param action SIM_STATE_CHANGED.
     * @return true if SIM_STATE_CHAGNED.
     */
    @Override
    public boolean customizeDualVolteReceiveIntent(String action) {
        log("customizeDualVolteReceiveIntent");
        if (null != action
                && action.equals(TelephonyIntents.ACTION_SIM_STATE_CHANGED)) {
            return true;
        }
        return super.customizeDualVolteReceiveIntent(action);
    }

    /**
     * For CU VOLTE feature.
     * when is CMCC card ,VOLTE show.
     * else VOLTE button hide.
     * @param preferenceScreen Mobile preferenceScreen
     * @param preference mEnhancedButton4glte
     * @param showPreference if true means CMCC card, need show item
     */
    @Override
    public void customizeDualVolteOpHide(PreferenceScreen preferenceScreen,
            Preference preference, boolean showPreference) {
        log("customizeDualVolteOpHide showPreference = " + showPreference);
        if (!showPreference) {
            preferenceScreen.removePreference(preference);
        }
    }
    
    /**
     * app use to judge the Card is CMCC
     * @param slotId
     * @return true is CMCC
     */
    private boolean isCUCard(int subId) {
        Log.d(LOG_TAG, "isCUCard, subId = " + subId);
        String simOperator = null;
        simOperator = getSimOperatorBySubId(subId);
        if (simOperator != null) {
            Log.d(LOG_TAG, "isCUCard, simOperator =" + simOperator);
            for (String mccmnc : MCCMNC_TABLE_TYPE_CU) {
                if (simOperator.equals(mccmnc)) {
                    return true;
                }
            }
        }
        Log.d(LOG_TAG, "isCUCard, false");
        return false;
    }

    private String getSimOperatorBySubId(int subId) {
        if (subId < 0) {
            return null;
        }
        String simOperator = null;
        int status = TelephonyManager.SIM_STATE_UNKNOWN;
        int slotId = SubscriptionManager.getSlotIndex(subId);
        if (slotId != SubscriptionManager.INVALID_SIM_SLOT_INDEX) {
             status = TelephonyManager.getDefault().getSimState(slotId);
        }
        if (status == TelephonyManager.SIM_STATE_READY) {
            simOperator = TelephonyManager.getDefault().getSimOperator(subId);
        }
        Log.d(LOG_TAG, "getSimOperatorBySubId, simOperator = "
                + simOperator + " subId = " + subId);
        return simOperator;
    }

    @Override
    public boolean customizeCUVolte() {
        return true;
    }
}
