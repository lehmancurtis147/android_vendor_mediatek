package com.mediatek.op15.phone;

import android.content.Context;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op15PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op15PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new Op15CallFeaturesSettingExt(mContext);
    }
}
