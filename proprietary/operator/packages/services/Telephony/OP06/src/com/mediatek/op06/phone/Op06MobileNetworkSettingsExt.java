/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op06.phone;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceFragment;;
import android.preference.PreferenceScreen;
import android.telephony.CarrierConfigManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.mediatek.ims.config.ImsConfigContract;
import com.mediatek.internal.telephony.MtkSubscriptionManager;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;


/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op06MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "Op06MobileNetworkSettingsExt";

    private Context mContext;
    private PreferenceFragment mPreferenceFragment;
    private OP06WfcSettings mWfcSettings;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "action:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(context)) {
                Log.d(TAG, "WFC is not enabled in platform");
                return;
            }
            if (action.equals(ImsConfigContract.ACTION_CONFIG_LOADED)) {
                int phoneId = intent.getIntExtra(ImsConfigContract.EXTRA_PHONE_ID, 0);
                if (WifiCallingUtils.isWifiCallingProvisioned(context, phoneId)) {
                    if (mPreferenceFragment != null) {
                        Log.d(TAG, "Config loaded. WFC is provisioned, so add pref.");
                        customizeWfcPreference(context,
                                    mPreferenceFragment.getPreferenceScreen(), phoneId);
                    }
                } else {
                    if (mPreferenceFragment != null) {
                        Log.d(TAG, "Config loaded. WFC is not provisioned, so remove pref.");
                        mWfcSettings.removeWfcPreference(mPreferenceFragment.getPreferenceScreen());
                    }
                }
            } else if (CarrierConfigManager.ACTION_CARRIER_CONFIG_CHANGED.equals(action)) {
                int phoneId = intent.getIntExtra(CarrierConfigManager.EXTRA_SLOT_INDEX , 0);
                if (mPreferenceFragment != null) {
                    customizeWfcPreference(context,
                                    mPreferenceFragment.getPreferenceScreen(), phoneId);
                }
            }
        }
    };

    /** Constructor.
     * @param context context
     */
    public Op06MobileNetworkSettingsExt(Context context) {
        mContext = context;
        mWfcSettings = OP06WfcSettings.getInstance(mContext);
    }

    /**
     * Customize WFC pref as per operator requirement, on basis of MCC+MNC.
     * Called from WirelessSettings
     * @param preferenceScreen preferenceScreen
     * @return
     */
    @Override
    public void customizeWfcPreference(Context context,
                            PreferenceScreen preferenceScreen, int phoneId) {
        Log.d(TAG, "customizeWfcPreference");
        if (ImsManager.isWfcEnabledByPlatform(context)
            && WifiCallingUtils.isWifiCallingProvisioned(context, phoneId)) {
            Log.d(TAG, "Do the customzation");
            mWfcSettings.customizedWfcPreference(context, preferenceScreen);
        } else {
            Log.d(TAG, "isWfcEnabledByPlatform/isWifiCallingProvisioned not supported");
            mWfcSettings.removeWfcPreference(preferenceScreen);
        }
    }

    /**
     * @param context Context
     * @param phoneId phoneId
     * @return
     * Customize boolean whether provisioned or not
     */
    @Override
    public boolean isWfcProvisioned(Context context, int phoneId) {
        if (!WifiCallingUtils.isImsProvSupported(context,
                        MtkSubscriptionManager.getSubIdUsingPhoneId(phoneId))) {
            Log.d(TAG, "Ims prov feature not supported, phoneId:" + phoneId);
            return super.isWfcProvisioned(context, phoneId);
        }
         boolean isWifiCallingProvisioned = WifiCallingUtils
                .isWifiCallingProvisioned(context, phoneId);
        if (!isWifiCallingProvisioned) {
            if (mPreferenceFragment != null) {
                Log.d(TAG, "WFC is not provisioned. remove WFC Pref");
                mWfcSettings.removeWfcPreference(mPreferenceFragment.getPreferenceScreen());
            }
        }
        return isWifiCallingProvisioned;
    }

   /**
    * Called from onResume.
    * @return
    */
    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        WifiCallingUtils.registerReceiver(mContext, mReceiver);
    }

   /**
    * Called from onPause.
    * @return
    */
    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        WifiCallingUtils.unRegisterReceiver(mContext, mReceiver);
    }
}
