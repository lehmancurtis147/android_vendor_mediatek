package com.mediatek.op06.phone;

import android.content.Context;
import android.os.PersistableBundle;
import android.telephony.CarrierConfigManager;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.widget.Toast;

import com.android.ims.ImsConfig;
import com.android.ims.ImsException;
import com.android.ims.ImsManager;
import com.mediatek.ims.internal.MtkImsManagerEx;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import mediatek.telephony.MtkCarrierConfigManager;

/**
 * Class to support operator customizations for WFC settings.
 */
public class WfcSwitchController {

    private static final String TAG = "Op06WfcSwitchController";
    private static final String AOSP_SETTING_WFC_PREFERENCE = "wifi_calling_settings";
    private static final String AOSP_CALL_SETTING_WFC_PREFERENCE
            = "button_wifi_calling_settings_key";
    private static final String OP06_WFC_PREFERENCE = "op06_wfc_preference_key";
    private static final int WFC_PREF_CHANGE_TYPE = 0;

    private static WfcSwitchController sController = null;

    private Context mPluginContext;
    private Context mAppContext;
    private Object mAospWfcPreference;
    private android.support.v14.preference.SwitchPreference mWfcSwitchSettings = null;
    private android.preference.SwitchPreference mWfcSwitchCallSettings = null;
    private android.support.v7.preference.PreferenceScreen mPreferenceScreenSettings = null;
    private android.preference.PreferenceScreen mPreferenceScreenCallSettings = null;

    private WfcSwitchController(Context context) {
       mPluginContext = context;
    }

    /** Returns instance of WfcSwitchController.
     * @param context context
     * @return WfcSettings
     */
    public static WfcSwitchController getInstance(Context context) {

        if (sController == null) {
            sController = new WfcSwitchController(context);
        }
        return sController;
    }

    /** Customize wfc preference.
     * @param context context
     * @param preferenceScreen preferenceScreen
     * @return
     */
    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mAppContext = context;
        int settingsUxType = -1;
        int defaultWfcMode = ImsConfig.WfcModeFeatureValueConstants.CELLULAR_PREFERRED;
        CarrierConfigManager configMgr = (CarrierConfigManager) mPluginContext
                .getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b =
                configMgr.getConfigForSubId(SubscriptionManager.getDefaultDataSubscriptionId());
        if (b != null) {
             settingsUxType = b.getInt(MtkCarrierConfigManager.MTK_KEY_WFC_SETTINGS_UX_TYPE_INT);
             defaultWfcMode = b.getInt(MtkCarrierConfigManager.MTK_KEY_DEFAULT_WFC_MODE_INT);
        }
        Log.d(TAG, "settingsUxType:" + settingsUxType);
        Log.d(TAG, "defaultWfcMode:" + defaultWfcMode);

        android.preference.Preference wfcPreferenceCallSettings = null;
        android.support.v7.preference.Preference wfcPreferenceSettings = null;
        if (!init(preferenceScreen)) {
            return;
        }
        if (mPreferenceScreenCallSettings != null) {
            wfcPreferenceCallSettings =
                    (android.preference.Preference) mPreferenceScreenCallSettings
                    .findPreference(AOSP_CALL_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "wfcPreferenceCallSettings: " + wfcPreferenceCallSettings);
        } else if (mPreferenceScreenSettings != null) {
            wfcPreferenceSettings =
                    (android.support.v7.preference.Preference) mPreferenceScreenSettings
                    .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            Log.d(TAG, "wfcSettingsPreference: " + wfcPreferenceSettings);
        }

        /* Store AOSP WFC preference for restoring AOSP UX, on receiving Carrier_config_change
            * intent. It may not be derived form Wireless/CallSettings preferencScreen/activity,
            * as it would have been removed earlier */
        if (wfcPreferenceCallSettings != null) {
            mAospWfcPreference = wfcPreferenceCallSettings;
        } else if (wfcPreferenceSettings != null) {
            mAospWfcPreference = wfcPreferenceSettings;
        }

        if (settingsUxType == WFC_PREF_CHANGE_TYPE) {
            CharSequence title = null;
            int order = 0;
            if (wfcPreferenceCallSettings != null) {
                mPreferenceScreenCallSettings.removePreference(wfcPreferenceCallSettings);
                title = wfcPreferenceCallSettings.getTitle();
                order = wfcPreferenceCallSettings.getOrder();
            } else if (wfcPreferenceSettings != null) {
                mPreferenceScreenSettings.removePreference(wfcPreferenceSettings);
                title = wfcPreferenceSettings.getTitle();
                order = wfcPreferenceSettings.getOrder();
            }
            checkAndAddWfcSwitch(title, order);
            updateWfcSwitchState(defaultWfcMode);
        }  else {    // Restore AOSP behavior
            if (mWfcSwitchCallSettings != null && mPreferenceScreenCallSettings != null) {
                mPreferenceScreenCallSettings.removePreference(mWfcSwitchCallSettings);
                mPreferenceScreenCallSettings.addPreference(wfcPreferenceCallSettings);
            } else if (mWfcSwitchSettings != null && mPreferenceScreenSettings != null) {
                mPreferenceScreenSettings.removePreference(mWfcSwitchSettings);
                mPreferenceScreenSettings.addPreference(wfcPreferenceSettings);
            }
            ImsManager.setWfcMode(mAppContext, defaultWfcMode);
            restoreAospUx(defaultWfcMode);
        }
    }

    private boolean init(Object preferenceScreen) {
        if (preferenceScreen instanceof android.preference.PreferenceScreen) {
            mPreferenceScreenCallSettings =
                    (android.preference.PreferenceScreen) preferenceScreen;
            mWfcSwitchCallSettings =
                    (android.preference.SwitchPreference) mPreferenceScreenCallSettings
                    .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "mPreferenceScreenCallSettings: " + mPreferenceScreenCallSettings);

        } else if (preferenceScreen instanceof android.support.v7.preference.PreferenceScreen) {
            mPreferenceScreenSettings =
                    (android.support.v7.preference.PreferenceScreen) preferenceScreen;
            mWfcSwitchSettings =
                    (android.support.v14.preference.SwitchPreference) mPreferenceScreenSettings
                    .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "mPreferenceScreenSettings: " + mPreferenceScreenSettings);
        } else {
            Log.d(TAG, "invalid class of preferenceScreen: " + preferenceScreen);
            return false;
        }
        return true;
    }

    private void checkAndAddWfcSwitch(CharSequence title, int order) {
        Log.d(TAG, "mWfcSwitchCallSettings: " + mWfcSwitchCallSettings);
        Log.d(TAG, "mWfcSwitchSettings: " + mWfcSwitchSettings);
        if (mWfcSwitchCallSettings == null && mWfcSwitchSettings == null) {
            if (mPreferenceScreenCallSettings != null) {
                mWfcSwitchCallSettings = new android.preference.SwitchPreference(mAppContext);
                mWfcSwitchCallSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForCallSettings());
                mWfcSwitchCallSettings.setKey(OP06_WFC_PREFERENCE);
                mWfcSwitchCallSettings.setTitle(title);
                mWfcSwitchCallSettings.setOrder(order);
                mPreferenceScreenCallSettings.addPreference(mWfcSwitchCallSettings);
            } else if (mPreferenceScreenSettings != null) {
                mWfcSwitchSettings =
                        new android.support.v14.preference.SwitchPreference(mAppContext);
                mWfcSwitchSettings
                        .setOnPreferenceChangeListener(new WfcSwitchListenerForSettings());
                mWfcSwitchSettings.setKey(OP06_WFC_PREFERENCE);
                mWfcSwitchSettings.setTitle(title);
                mWfcSwitchSettings.setOrder(order);
                mPreferenceScreenSettings.addPreference(mWfcSwitchSettings);
            }
        }
    }

    /** Updates wfc switch.
      * @param defaultWfcMode defaultWfcMode
      * @return
      */
    public void updateWfcSwitchState(int defaultWfcMode) {
        // Disable switch if PS call ongoing
        if (mWfcSwitchCallSettings != null) {
            mWfcSwitchCallSettings.setChecked(ImsManager.isWfcEnabledByUser(mPluginContext));
        } else if (mWfcSwitchSettings != null) {
            mWfcSwitchSettings.setChecked(ImsManager.isWfcEnabledByUser(mPluginContext));
        }
        ImsManager.setWfcMode(mPluginContext, defaultWfcMode);
    }

    private void restoreAospUx(int defaultWfcMode) {
        if (mWfcSwitchCallSettings != null && mPreferenceScreenCallSettings != null) {
            mPreferenceScreenCallSettings.removePreference(mWfcSwitchCallSettings);
            mPreferenceScreenCallSettings
                    .addPreference((android.preference.Preference) mAospWfcPreference);
        } else if (mWfcSwitchSettings != null && mPreferenceScreenSettings != null) {
            mPreferenceScreenSettings.removePreference(mWfcSwitchSettings);
            mPreferenceScreenSettings
                    .addPreference((android.support.v7.preference.Preference) mAospWfcPreference);
        }
        ImsManager.setWfcMode(mAppContext, defaultWfcMode);
    }

    private boolean isInSwitchProcess() {
        int imsState = MtkPhoneConstants.IMS_STATE_DISABLED;
        try {
         imsState = MtkImsManagerEx.getInstance().getImsState(RadioCapabilitySwitchUtil
                         .getMainCapabilityPhoneId());
        } catch (ImsException e) {
           return false;
        }
        Log.d(TAG, "isInSwitchProcess , imsState = " + imsState);
        return imsState == MtkPhoneConstants.IMS_STATE_DISABLING
                || imsState == MtkPhoneConstants.IMS_STATE_ENABLING;
    }


    /** Removes wfc preference.
     * @param preferenceScreen preferenceScreen
     * @return
     */
    public void removeWfcPreference(Object preferenceScreen) {
        if (preferenceScreen instanceof android.preference.PreferenceScreen) {
            android.preference.PreferenceScreen prefScreen
                = (android.preference.PreferenceScreen) preferenceScreen;
            android.preference.Preference wfcCallSettingsPreference
                = (android.preference.Preference) prefScreen
                .findPreference(AOSP_CALL_SETTING_WFC_PREFERENCE);
            android.preference.SwitchPreference wfcSwitch
                    = (android.preference.SwitchPreference) prefScreen
                    .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "wfcCallSettingsPreference: " + wfcCallSettingsPreference);
            Log.d(TAG, "wfcSwitch: " + wfcSwitch);
            Log.d(TAG, "prefScreen: " + prefScreen);
            if (wfcCallSettingsPreference != null) {
                ((android.preference.PreferenceScreen) preferenceScreen)
                        .removePreference(wfcCallSettingsPreference);
            } else if (wfcSwitch != null) {
                ((android.preference.PreferenceScreen) preferenceScreen)
                        .removePreference(wfcSwitch);
            }
        } else if (preferenceScreen instanceof android.support.v7.preference.PreferenceScreen) {
            android.support.v7.preference.PreferenceScreen prefScreen
                = (android.support.v7.preference.PreferenceScreen) preferenceScreen;
            android.support.v7.preference.Preference wfcSettingsPreference
                    = (android.support.v7.preference.Preference) prefScreen
                    .findPreference(AOSP_SETTING_WFC_PREFERENCE);
            android.support.v14.preference.SwitchPreference wfcSwitch
                    = (android.support.v14.preference.SwitchPreference) prefScreen
                    .findPreference(OP06_WFC_PREFERENCE);
            Log.d(TAG, "wfcSettingsPreference: " + wfcSettingsPreference);
            Log.d(TAG, "wfcSwitch: " + wfcSwitch);
            Log.d(TAG, "prefScreen: " + prefScreen);
            if (wfcSettingsPreference != null) {
                prefScreen.removePreference(wfcSettingsPreference);
            } else if (wfcSwitch != null) {
                prefScreen.removePreference(wfcSwitch);
            }
        } else {
            Log.e(TAG, "invalid class of preferenceScreen: " + preferenceScreen);
        }
    }

    /**
     * Class for WFC switch listener.
     */
    private class WfcSwitchListenerForSettings implements
            android.support.v7.preference.Preference.OnPreferenceChangeListener {

        public WfcSwitchListenerForSettings() {
        }

        @Override
        public boolean onPreferenceChange(android.support.v7.preference.Preference preference,
                Object newValue) {
            // TODO: check if newValue is right
            //final boolean isChecked = !mWfcSwitchSettings.isChecked();
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if (isInSwitchProcess()) {
                Toast.makeText(mAppContext, "Operation not allowed", Toast.LENGTH_SHORT)
                    .show();
                return false;
            }
            ImsManager.setWfcSetting(mAppContext, isChecked);
            return true;
        }
    }

    /**
     * Class for WFC switch listener.
     */
    private class WfcSwitchListenerForCallSettings implements
            android.preference.Preference.OnPreferenceChangeListener {

        public WfcSwitchListenerForCallSettings() {
        }

        @Override
        public boolean onPreferenceChange(android.preference.Preference preference,
                Object newValue) {
            //final boolean isChecked = !mWfcSwitchCallSettings.isChecked();
            boolean isChecked = ((Boolean) newValue).booleanValue();
            if (isInSwitchProcess()) {
                Toast.makeText(mAppContext, "Operation not allowed", Toast.LENGTH_SHORT)
                    .show();
                return false;
            }
            ImsManager.setWfcSetting(mAppContext, isChecked);
            return true;
        }
    }
}
