package com.mediatek.op06.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op06PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private static final String TAG = "Op06PhoneCustomizationFactory";
    private Context mContext;

    public Op06PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    @Override
    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        Log.d(TAG, "makeCallFeaturesSettingExt");
        return new Op06CallFeaturesSettingExt(mContext);
    }

    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.d(TAG, "makeMobileNetworkSettingsExt");
        return new Op06MobileNetworkSettingsExt(mContext);
    }
}
