/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.op07.phone;

import android.content.Context;
import android.os.SystemProperties;
import android.preference.Preference;
import android.preference.PreferenceCategory;
import android.util.Log;

import com.mediatek.phone.ext.DefaultNetworkSettingExt;

/**
 * Network Setting for OP07.
 */
public class OP07NetworkSettingExt extends DefaultNetworkSettingExt {
    private static final String TAG = "OP07NetworkSettingExt";

    private static final String PROP_FEMTOCELL_SUPPORT = "ro.vendor.mtk_femto_cell_support";

    private static final String BUTTTON_MANUAL_FEMTOCELL = "button_manual_femtocell";
    public static final String OP07_BUTTTON_MANUAL_FEMTOCELL = "op07_button_manual_femtocell";

    Context mContext;

    /**
     * Default cstructor for Network settings.
     * @param context host app context
     */
    public OP07NetworkSettingExt(Context context) {
        mContext = context;
    }

    public void initOtherNetworkSetting(PreferenceCategory prefSet) {
        String propValue = SystemProperties.get(PROP_FEMTOCELL_SUPPORT, "0");
        boolean femtocellSupport = propValue.equals("1");

        Log.d(TAG, "initOtherNetworkSetting, femtocellSupport=" + femtocellSupport);

        if (femtocellSupport) {
            Preference femtoPref = prefSet.findPreference(BUTTTON_MANUAL_FEMTOCELL);
            if (femtoPref != null) {
                prefSet.removePreference(femtoPref);
            }
            Preference femtocellProf = new ManualFemtoCellSelection(mContext, prefSet);
            femtocellProf.setKey(OP07_BUTTTON_MANUAL_FEMTOCELL);
            femtocellProf.setTitle(mContext.getString(R.string.search_femtocell_list));
            femtocellProf.setPersistent(false);
            prefSet.addPreference(femtocellProf);
        }
    }
}
