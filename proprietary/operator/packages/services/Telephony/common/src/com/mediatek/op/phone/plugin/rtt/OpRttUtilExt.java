/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.phone.plugin.rtt;

import android.telecom.PhoneAccount;
import android.telecom.TelecomManager;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telephony.Rlog;
import android.os.Bundle;
import android.os.SystemProperties;

import com.mediatek.op.phone.plugin.OpTelephonyUtils;
import com.mediatek.phone.ext.DefaultRttUtilExt;

import com.mediatek.internal.telephony.imsphone.MtkImsPhoneConnection;
import android.os.ParcelFileDescriptor;
import mediatek.telecom.MtkTelecomManager;

import java.util.ArrayList;
import java.util.List;


public class OpRttUtilExt extends DefaultRttUtilExt {
    @Override
    public void setIncomingRttCall(com.android.internal.telephony.Connection connection,
        Bundle extras) {
        Rlog.d(TAG , "setIncomingRttCall op");

        if (connection == null) {
            Rlog.d(TAG, "setIncomingRttCall return");
            return;
        }
        if (connection instanceof MtkImsPhoneConnection == true) {
            boolean shouldUpgradeRttForEMCGuardTimer =
                ((MtkImsPhoneConnection) connection).isIncomingRttDuringEmcGuard();
            if (shouldUpgradeRttForEMCGuardTimer && extras != null) {
                extras.putBoolean(MtkTelecomManager.EXTRA_IS_RTT_EMERGENCY_CALLBACK, true);
                Rlog.d(TAG, "setIncomingRttCall put extra shouldUpgradeRttForEMCGuardTimer");
            }
        }
    }

    private static final List<String> RTT_NOT_ALLOW_MERGE_OPERATOR_LIST =
        new ArrayList<String>() {{
            add("OP08"); // TMO US
            add("OP12"); // VzW
        }};

    @Override
    public boolean isRttCallAndNotAllowMerge(
            com.android.internal.telephony.Connection connection) {
        Rlog.d(TAG , "isRttCallAndNotAllowMerge op");
        boolean isRttCall = false;
        boolean isAllowMerge = true;

        if (connection == null) {
            Rlog.d(TAG, "isRttCallAndNotAllowMerge return");
            return false;
        }
        if (connection instanceof MtkImsPhoneConnection == true) {
            MtkImsPhoneConnection originalConnection = (MtkImsPhoneConnection) connection;
            if (originalConnection.mRttTextHandler != null) {
                isRttCall = true;
            }
        }

        String optr = SystemProperties.get("persist.vendor.operator.optr");
        if (RTT_NOT_ALLOW_MERGE_OPERATOR_LIST.contains(optr)) {
            isAllowMerge = false;
        }
        Rlog.d(TAG, "isRttCallAndNotAllowMerge isRttCall: " + isRttCall
                + ", isAllowMerge: " + isAllowMerge);
        return isRttCall && !isAllowMerge;
    }

    @Override
    public void onStopRtt(boolean isIms,
        com.android.internal.telephony.Connection connection) {

       Rlog.d(TAG, "onStopRtt op");
       //For RTT feature
       if (isIms) {
           if (connection instanceof MtkImsPhoneConnection == true) {
               MtkImsPhoneConnection originalConnection = (MtkImsPhoneConnection) connection;
               Rlog.d(TAG, "sendRttDowngradeRequest op");
               originalConnection.sendRttDowngradeRequest();
           }
       } else {
           Rlog.d(TAG, "onStartRtt - not in IMS, so RTT cannot be enabled.");
       }
    }

    @Override
    public boolean setTelephonyConnectionRttStatus(boolean status) {
        Rlog.d(TAG , "setTelephonyConnectionRttStatus op");
        return status;
    }

    @Override
    public int updatePropertyRtt(int newProperties, boolean isRttEnabled, int bitField) {
        Rlog.d(TAG , "updatePropertyRtt op" + isRttEnabled + " newProperties= "+ newProperties);
        int properties = newProperties;
        if (isRttEnabled) {
            properties =  newProperties | bitField;
        } else {
            properties =  newProperties & ~bitField;
        }
        Rlog.d(TAG , "updatePropertyRtt properties = " + properties);

        return properties;
    }

    @Override
    public boolean updateConnectionProperties() {
        Rlog.d(TAG , "updateConnectionProperties op ");
        return true;
    }
}
