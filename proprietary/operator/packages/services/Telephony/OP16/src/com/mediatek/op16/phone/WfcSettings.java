package com.mediatek.op16.phone;

import android.content.Context;



/**
 * Customized WFC Setting implementation.
 */
public class WfcSettings {

    private static final String TAG = "Op16WfcSettings";

    static WfcSettings sWfcSettings = null;

    Context mContext;
    private WfcSwitchController mController;

    private WfcSettings(Context context) {
       mContext = context;
       mController = WfcSwitchController.getInstance(context);
    }

    /** Provides instance of plugin.
     * @param context context
     * @return WfcSettings
     */
    public static WfcSettings getInstance(Context context) {

        if (sWfcSettings == null) {
            sWfcSettings = new WfcSettings(context);
        }
        return sWfcSettings;
    }

    /** .
     * Customize wfc preference
     * @param context Context
     * @param preferenceScreen preferenceScreen
     * @return
     */

    public void customizedWfcPreference(Context context, Object preferenceScreen) {
        mController.customizedWfcPreference(context, preferenceScreen);
    }


    /** Registers listener/receiver.
     * @param context context
     * @return
     */
    public void register(Context context) {
        mController.register();
    }

    /** Unregisters listener/receiver.
     * @param context context
     * @return
     */
    public void unRegister(Context context) {
        mController.unRegister();
    }

    /** Returns instance of OP18WfcSettings.
     * @return
     */
    public void removeWfcPreference() {
        mController.removeWfcPreference();
    }
}
