/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op16.phone;

import android.content.Context;
import android.preference.PreferenceFragment;;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;


/**
 * Plugin implementation for WFC Settings plugin
 */
public class Op16MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "Op16MobileNetworkSettingsExt";

    private Context mContext;
    private PreferenceFragment mPreferenceFragment;
    private WfcSettings  mWfcSettings = null;

    /** Constructor.
     * @param context context
     */
    public Op16MobileNetworkSettingsExt(Context context) {
        mContext = context;
    }

    /**
     * Customize WFC pref as per operator requirement, on basis of MCC+MNC.
     * Called from WirelessSettings
     * @param preferenceScreen preferenceScreen
     * @return
     */
    @Override
    public void customizeWfcPreference(Context context,
                            PreferenceScreen preferenceScreen, int phoneId) {
        Log.d(TAG, "customizeWfcPreference");
        mWfcSettings = WfcSettings.getInstance(context);
        mWfcSettings.register(mContext);
        mWfcSettings.customizedWfcPreference(context, preferenceScreen);
    }

   /**
    * Called from onResume.
    * @return
    */
    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        mWfcSettings.register(mContext);
    }

   /**
    * Called from onPause.
    * @return
    */
    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        mWfcSettings.unRegister(mContext);
    }
}
