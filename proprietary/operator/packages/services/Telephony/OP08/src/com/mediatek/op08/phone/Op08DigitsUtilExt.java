/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op08.phone;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemProperties;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.Connection;
import com.android.phone.PhoneUtils;
import com.android.services.telephony.ImsConference;
import com.android.services.telephony.PstnIncomingCallNotifier;
import com.android.services.telephony.TelephonyConnection;

import com.android.services.telephony.TelephonyConnection;
import com.mediatek.ims.MtkImsCallProfile;
import com.mediatek.phone.ext.DefaultDigitsUtilExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import mediatek.telecom.MtkPhoneAccount;
import mediatek.telecom.MtkTelecomManager;

import java.util.Objects;
import java.util.ArrayList;

public class Op08DigitsUtilExt extends DefaultDigitsUtilExt {

    private static final String VIRTUAL_ACCOUNT_ID_PREFIX = "virtual_";
    private static final String PARENT_ACCOUNT_ID_PREFIX = "_parentid_";
    private static final String TAG = "Op08DigitsUtilExt";

    private static boolean sIsDigitsSupported = MtkTelephonyManagerEx.getDefault()
            .isDigitsSupported();

    @Override
    public PhoneAccountHandle convertToPstnPhoneAccountHandle(
            PhoneAccountHandle phoneAccountHandle, Context context) {
        return getParentPhoneAccountHandle(phoneAccountHandle, context);
    }

    @Override
    public PhoneAccountHandle convertToPstnPhoneAccount(
            PhoneAccount phoneAccount) {
        return getParentPhoneAccountHandle(phoneAccount);
    }

    @Override
    public String getVirtualLineNumber(PhoneAccountHandle phoneAccountHandle) {
        return (phoneAccountHandle == null) ? null
               : (isPotentialVirtualPhoneAccount(phoneAccountHandle) ?
                idToNumber(phoneAccountHandle.getId()) : "");
    }

    @Override
    public PhoneAccountHandle makeVirtualPhoneAccountHandle(String virtualLineNumber,
            String parentId) {
        return PhoneUtils.makePstnPhoneAccountHandle(numberToId(virtualLineNumber, parentId));
    }

    @Override
    public boolean isVirtualPhoneAccount(PhoneAccountHandle phoneAccountHandle, Context context) {
        // Parent == self means not a virtual PhoneAccount.
        return (phoneAccountHandle != null) && !Objects.equals(
                getParentPhoneAccountHandle(phoneAccountHandle, context),
                phoneAccountHandle);
    }

    @Override
    public boolean isPotentialVirtualPhoneAccount(PhoneAccountHandle phoneAccountHandle) {
        return phoneAccountHandle != null && phoneAccountHandle.getId() != null
                && phoneAccountHandle.getId().startsWith(VIRTUAL_ACCOUNT_ID_PREFIX)
                && phoneAccountHandle.getId().indexOf(PARENT_ACCOUNT_ID_PREFIX) > -1;
    }

    @Override
    public Object replaceTelecomAccountRegistry(Object telecomAccountRegistry, Context context) {
        return new MtkTelecomAccountRegistry(context);
    }

    private PhoneAccountHandle getParentPhoneAccountHandle(
            PhoneAccountHandle virtualPhoneAccountHandle, Context context) {
        TelecomManager tm = (TelecomManager) context.getSystemService(Context.TELECOM_SERVICE);
        PhoneAccount phoneAccount = tm.getPhoneAccount(virtualPhoneAccountHandle);
        return getParentPhoneAccountHandle(phoneAccount);
    }

    private PhoneAccountHandle getParentPhoneAccountHandle(
            PhoneAccount virtualPhoneAccount) {
        if (virtualPhoneAccount == null) {
            return null;
        }

        PhoneAccountHandle parent = virtualPhoneAccount.getAccountHandle();
        if (isPotentialVirtualPhoneAccount(virtualPhoneAccount.getAccountHandle())) {
            if (virtualPhoneAccount != null &&
                    virtualPhoneAccount.hasCapabilities(MtkPhoneAccount
                            .CAPABILITY_IS_VIRTUAL_LINE)) {
                Bundle extras = virtualPhoneAccount.getExtras();
                if (extras != null &&
                        extras.containsKey(MtkPhoneAccount.EXTRA_PARENT_PHONE_ACCOUNT_HANDLE)) {
                    parent = extras.getParcelable(
                            MtkPhoneAccount.EXTRA_PARENT_PHONE_ACCOUNT_HANDLE);
                }
            }
        }
        return parent;
    }

    private String numberToId(String number, String parentIccid) {
        return VIRTUAL_ACCOUNT_ID_PREFIX + number + PARENT_ACCOUNT_ID_PREFIX + parentIccid;
    }

    private String idToNumber(String id) {
        if (id != null && id.startsWith(VIRTUAL_ACCOUNT_ID_PREFIX)) {
            int index = id.indexOf(PARENT_ACCOUNT_ID_PREFIX);
            if (index > -1) {
                return id.substring(VIRTUAL_ACCOUNT_ID_PREFIX.length(), index);
            }
        }
        return id;
    }

    private String idToParentId(String id) {
        if (id != null && id.startsWith(VIRTUAL_ACCOUNT_ID_PREFIX)) {
            int index = id.indexOf(PARENT_ACCOUNT_ID_PREFIX);
            if (index > -1) {
                return id.substring(index + PARENT_ACCOUNT_ID_PREFIX.length());
            }
        }
        return id;
    }

    @Override
    public Bundle putLineNumberToExtras(Bundle extras, Context context) {
        // get virtual line from phone account in extra
        PhoneAccountHandle phoneAccountHandle = (PhoneAccountHandle) extras.get(MtkTelecomManager
                .EXTRA_CALLING_VIA_PHONE_ACCOUNT_HANDLE);
        if (isVirtualPhoneAccount(phoneAccountHandle, context) == false) {
            return extras;
        }
        // put virtual line to extra with key MtkTelecomManager.EXTRA_VIRTUAL_LINE_NUMBER
        String virtualLineNumber = getVirtualLineNumber(phoneAccountHandle);
        extras.putString(MtkTelecomManager.EXTRA_VIRTUAL_LINE_NUMBER, virtualLineNumber);
        return extras;
    }

    @Override
    public boolean isConnectionMatched(Connection connection, PhoneAccountHandle
            phoneAccountHandle, Context context) {
        if (!sIsDigitsSupported || phoneAccountHandle == null) {
            return true;
        }
        // check virtual line info from connection
        String incomingNumber = getVirtualLineNumber(connection);
        // case for non-virtual incoming call
        if (incomingNumber == null || incomingNumber.equals("")) {
            if (isVirtualPhoneAccount(phoneAccountHandle, context) == false) {
                return true;
            } else {
                return false;
            }
        }
        // compare virtual line info with phoneAccountHandle
        String phoneAccountNumber = getVirtualLineNumber(phoneAccountHandle);
        return PhoneNumberUtils.compare(incomingNumber, phoneAccountNumber);
    }

    @Override
    public PhoneAccountHandle getCorrectPhoneAccountHandle(PhoneAccountHandle handle,
                                                           PhoneAccountHandle memberHandle,
                                                           Context context) {
        if (isVirtualPhoneAccount(memberHandle, context)) {
            Log.d(TAG, "getCorrectPhoneAccountHandle return member handle");
            return memberHandle;
        } else {
            Log.d(TAG, "getCorrectPhoneAccountHandle return original handle");
            return handle;
        }
    }

    @Override
    public void setPhoneAccountHandle(Object notifier, PhoneAccountHandle phoneAccountHandle) {
        if ((notifier instanceof PstnIncomingCallNotifier) == false) {
            return;
        }
        ((PstnIncomingCallNotifier) notifier).setPhoneAccountHandle(phoneAccountHandle);
    }

    private String getVirtualLineNumber(Connection connection) {
        if (connection == null) {
            return "";
        }
        Bundle extras = connection.getConnectionExtras();
        if (extras == null) {
            return "";
        }
        return extras.getString(MtkTelecomManager.EXTRA_VIRTUAL_LINE_NUMBER);
    }

    @Override
    public String getIccidFromPhoneAccountHandle(PhoneAccountHandle phoneAccountHandle) {
        return idToParentId(phoneAccountHandle.getId());
    }

    @Override
    public PhoneAccountHandle getHandleByConnectionIfRequired(PhoneAccountHandle
            defaultPhoneAccountHandle, Object connection) {
        if ((connection instanceof TelephonyConnection) == false
                || defaultPhoneAccountHandle == null) {
            return defaultPhoneAccountHandle;
        }

        String virtualLine = getVirtualLineNumber(((TelephonyConnection) connection)
                .getOriginalConnection());
        if (virtualLine == null || virtualLine.equals("")) {
            return defaultPhoneAccountHandle;
        }
        return makeVirtualPhoneAccountHandle(virtualLine, defaultPhoneAccountHandle.getId());
    }

    @Override
    public boolean areConnectionsInSameLine(Object connections, Object conference) {
        ArrayList teleConns = (ArrayList)connections;

        ArrayList<Connection> origConnections = new ArrayList<Connection>();

        if (conference != null && conference instanceof ImsConference) {
            if (teleConns.size() < 1) {
                return true;
            }
            // Loop through and collect all calls (within conference)
            ImsConference conf = (ImsConference) conference;
            TelephonyConnection conn = conf.getHostConnection();
            if (conn != null && conn.getOriginalConnection() != null) {
                origConnections.add(conn.getOriginalConnection());
            }
        } else {
            if (teleConns.size() < 2) {
                return true;
            }
        }

        // Loop through and collect all calls
        for (int index = 0; index < teleConns.size(); ++index) {
            TelephonyConnection conn = (TelephonyConnection)(teleConns.get(index));
            if (conn.getOriginalConnection() != null) {
                origConnections.add(conn.getOriginalConnection());
            }
        }

        String prevLineNumber = null;
        boolean result = true;

        int index = 0;
        for (Connection connection: origConnections) {
            ++index;
            if (connection == null) {
                //Log.d(TAG, "connection[" + index + "] null, skip. ");
                continue;
            }
            // get virtual line number from connection for MT
            String virtualLineNumber = getVirtualLineNumber(connection);
            if (!TextUtils.isEmpty(virtualLineNumber)) {
                Log.d(TAG, "connection[" + index + "] virtualLineNumber: " + virtualLineNumber);
            }

            // start compare when prevLineNumber be init.
            if (prevLineNumber != null) {
                if (TextUtils.isEmpty(prevLineNumber) && TextUtils.isEmpty(virtualLineNumber)) {
                    continue;
                }

                // different line appears
                if (TextUtils.equals(prevLineNumber, virtualLineNumber) == false) {
                    result = false;
                    break;
                }
            }
            prevLineNumber = virtualLineNumber;
        }
        return result;
    }
}
