/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op08.phone;

import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.os.SystemProperties;
import android.telecom.Log;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;

import com.android.internal.telephony.Phone;
import com.android.services.telephony.PstnIncomingCallNotifier;
import com.android.services.telephony.TelecomAccountRegistry;
import com.mediatek.digits.DigitsConst;
import com.mediatek.digits.DigitsLine;
import com.mediatek.digits.DigitsManager;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import mediatek.telecom.MtkPhoneAccount;

public class MtkTelecomAccountRegistry extends TelecomAccountRegistry {
    private DigitsManager mDigitsManager;
    private DigitsManager.EventListener mListener;
    private Handler mHandler;
    private final int MSG_UPDATE_ACCOUNTS = 1;
    private List<VirtualLine> mVirtualLines = new ArrayList<VirtualLine>();

    public MtkTelecomAccountRegistry(Context context) {
        super(context);
        if (MtkTelephonyManagerEx.getDefault().isDigitsSupported()) {
            mDigitsManager = new DigitsManager(context);
            mListener = new DigitsManager.EventListener() {
                @Override
                public void onEvent(int event, int result, Bundle extras) {
                    switch (event) {
                        case DigitsConst.EVENT_REGISTERED_MSISDN_CHANGED:
                            Log.i(this, "[onEvent] event: %d, result: %d, extras: %s",
                                    event, result, extras);
                            mHandler.removeMessages(MSG_UPDATE_ACCOUNTS);
                            mHandler.obtainMessage(MSG_UPDATE_ACCOUNTS, extras).sendToTarget();
                            break;
                        default:
                            break;
                    }
                }
            };

            mHandler = new Handler(Looper.getMainLooper()) {
                @Override
                public void handleMessage(Message msg) {
                    switch (msg.what) {
                        case MSG_UPDATE_ACCOUNTS: {
                            Bundle extras = (Bundle) (msg.obj);
                            DigitsLine[] lineList = null;

                            if (extras != null) {
                                extras.setClassLoader(getClass().getClassLoader());
                                Parcelable[] parcelables = extras.getParcelableArray(
                                        DigitsConst.EXTRA_OBJECT_ARRAY_DIGITS_LINE);
                                if (parcelables != null) {
                                    lineList = new DigitsLine[parcelables.length];
                                    for (int i = 0; i < parcelables.length; i++) {
                                        if (parcelables[i] instanceof DigitsLine) {
                                            lineList[i] = (DigitsLine) parcelables[i];
                                        }
                                    }
                                }
                            }

                            List<VirtualLine> virtualLines = getVirtualLines(lineList);
                            if (isVirtualLinesChanged(virtualLines)) {
                                Log.i(this, "[handleMessage] MSG_UPDATE_ACCOUNTS");
                                mVirtualLines = virtualLines;
                                tearDownAccounts();
                                setupAccounts();
                            }
                            break;
                        }
                        default:
                            break;
                    }
                }
            };
            mDigitsManager.registerEventListener(mListener);
        }
    }

    @Override
    protected void addVirtualPhoneAccountEntries(List<AccountEntry> accountEntries) {
        if (!MtkTelephonyManagerEx.getDefault().isDigitsSupported()) {
            return;
        }

        if (accountEntries != null) {
            List<AccountEntry> virtualAccounts = new LinkedList<AccountEntry>();
            for (AccountEntry entry : accountEntries) {
                // Only main protocol slot has virtual account.
                if (entry.mPhone != null
                        && entry.mPhone.getPhoneId() == getMainCapabilityPhoneId()) {
                    for (VirtualLine virtualLine : mVirtualLines) {
                        virtualAccounts.add(new VirtualAccoutnEntry(entry, virtualLine));
                    }
                }
            }
            accountEntries.addAll(virtualAccounts);
        }
    }

    /**
     * In AccountEntry constructor, the makePstnIncomingCallNotifier would be called to update
     * mIncomingCallNotifier. We have to fill it with null for VirtualAccoutnEntry, and replace it
     * in VirtualAccoutnEntry constructor.
     */
    @Override
    protected PstnIncomingCallNotifier makePstnIncomingCallNotifier(AccountEntry accountEntry,
            Phone phone, PhoneAccountHandle phoneAccountHandle) {
        if (MtkTelephonyManagerEx.getDefault().isDigitsSupported()) {
            if (accountEntry instanceof VirtualAccoutnEntry) {
                Log.i(this, "[makePstnIncomingCallNotifier] VirtualAccoutnEntry, " +
                        "not create PstnIncomingCallNotifier here");
                return null;
            } else {
                return new PstnIncomingCallNotifier(phone, phoneAccountHandle);
            }
        } else {
            return new PstnIncomingCallNotifier(phone);
        }
    }

    private List<VirtualLine> getVirtualLines(DigitsLine[] digitsLines) {
        if (digitsLines != null && digitsLines.length > 0) {
            List<VirtualLine> virtualLines = new ArrayList<>();
            for (int i = 0; i < digitsLines.length; i++) {
                if (digitsLines[i].getIsVirtual()
                        && digitsLines[i].getLineStatus() == DigitsLine.LINE_STATUS_REGISTERED) {
                    virtualLines.add(new VirtualLine(digitsLines[i].getMsisdn(),
                                                     digitsLines[i].getLineName(),
                                                     digitsLines[i].getLineColor()));
                }
            }
            return virtualLines;
        }
        return Collections.EMPTY_LIST;
    }

    private int getMainCapabilityPhoneId() {
       int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
       if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
           phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
       }
       return phoneId;
    }

    private boolean isVirtualLinesChanged(List<VirtualLine> newVirtualLines) {
        if (newVirtualLines.size() != mVirtualLines.size()) {
            return true;
        }

        for (VirtualLine newVirtualLine : newVirtualLines) {
            boolean lineFound = false;
            for (VirtualLine virtualLine : mVirtualLines) {
                if (newVirtualLine.equals(virtualLine)) {
                    lineFound = true;
                    break;
                }
            }
            if (!lineFound) {
                return true;
            }
        }

        return false;
    }

    private final class VirtualLine {
        private final String mLineNumber;
        private final String mLineName;
        private final int mLineColor;

        public VirtualLine(String lineNumber, String lineName, int lineColor) {
            mLineNumber = lineNumber == null ? "" : lineNumber;
            mLineName = lineName == null ? "" : lineName;
            mLineColor = lineColor;
        }

        public String getLineNumber() {
            return mLineNumber;
        }

        public String getLineName() {
            return mLineName;
        }

        public int getLineColor() {
            return mLineColor;
        }

        public String toString() {
            return "VirtualLine {mLineNumber=" + mLineNumber
                    + ", mLineName=" + mLineName
                    + ", mLineColor=" + mLineColor
                    + "}";
        }

        public boolean equals(Object other) {
            if (!(other instanceof VirtualLine)) {
                return false;
            }

            VirtualLine targetVirtualLine = (VirtualLine) other;
            return Objects.equals(targetVirtualLine.getLineNumber(), getLineNumber())
                    && Objects.equals(targetVirtualLine.getLineName(), getLineName())
                    && Objects.equals(targetVirtualLine.getLineColor(), getLineColor());
        }


    }

    private class VirtualAccoutnEntry extends AccountEntry {
        private AccountEntry mParentAccountEntry;

        public VirtualAccoutnEntry(AccountEntry parentAccountEntry, VirtualLine virtualLine) {
            super(parentAccountEntry.mPhone, false, false);
            mParentAccountEntry = parentAccountEntry;
            mAccount = registerVirtualPstnPhoneAccount(virtualLine);
            mIncomingCallNotifier = new PstnIncomingCallNotifier(mPhone,
                    mAccount.getAccountHandle());
        }

        /**
         * In super constructor, the registerPstnPhoneAccount would be called to assign a
         * PhoneAccount to mAccount. We have to fill it with a dummy one, and replace it later.
         */
        @Override
        protected PhoneAccount registerPstnPhoneAccount(
                boolean isEmergency, boolean isDummyAccount) {
            return makeDummyPhoneAccount();
        }

        @Override
        public void onVideoCapabilitiesChanged(boolean isVideoCapable) {
            mIsVideoCapable = isVideoCapable;
            synchronized (mAccountsLock) {
                if (!mAccounts.contains(this)) {
                    // Account has already been torn down, don't try to register it again.
                    // This handles the case where teardown has already happened, and we got a video
                    // update that lost the race for the mAccountsLock.  In such a scenario by the
                    // time we get here, the original phone account could have been torn down.
                    return;
                }
                mAccount = registerVirtualPstnPhoneAccount(new VirtualLine(
                        new Op08DigitsUtilExt().getVirtualLineNumber(mAccount.getAccountHandle()),
                        mAccount.getLabel().toString(),
                        mAccount.getHighlightColor()));
            }
        }

        private PhoneAccount registerVirtualPstnPhoneAccount(VirtualLine virtualLine) {
            PhoneAccount parentAccount = mParentAccountEntry.mAccount;
            int capabilities = setupAccountCapabilities(parentAccount);
            Bundle extras = setupAccountExtras(parentAccount);
            Icon icon = setupAccountIcon(virtualLine.getLineColor());

            updateAccountEntryCapability();

            PhoneAccount virtualPhoneAccount = PhoneAccount.builder(
                    new Op08DigitsUtilExt().makeVirtualPhoneAccountHandle(
                            virtualLine.getLineNumber(), parentAccount.getAccountHandle().getId()),
                            virtualLine.getLineName())
                    .setAddress(Uri.fromParts(PhoneAccount.SCHEME_TEL, virtualLine.getLineNumber(),
                            null))
                    .setSubscriptionAddress(
                            Uri.fromParts(PhoneAccount.SCHEME_TEL, virtualLine.getLineNumber(),
                            null))
                    .setCapabilities(capabilities)
                    .setIcon(icon)
                    .setHighlightColor(virtualLine.getLineColor())
                    .setShortDescription(virtualLine.getLineNumber())
                    .setSupportedUriSchemes(Arrays.asList(
                            PhoneAccount.SCHEME_TEL, PhoneAccount.SCHEME_VOICEMAIL))
                    .setExtras(extras)
                    .setGroupId(parentAccount.getGroupId())
                    .build();

            mTelecomManager.registerPhoneAccount(virtualPhoneAccount);
            Log.i(this, "[registerVirtualPstnPhoneAccount] %s with handle: %s",
                    virtualPhoneAccount, virtualPhoneAccount.getAccountHandle());
            return virtualPhoneAccount;
        }

        // Customize virtual phone account capability
        private int setupAccountCapabilities(PhoneAccount parentAccount) {
            int capabilities = (parentAccount.getCapabilities()
                            // Virtual phone account not support emergency call
                            & ~(PhoneAccount.CAPABILITY_PLACE_EMERGENCY_CALLS
                            | PhoneAccount.CAPABILITY_EMERGENCY_VIDEO_CALLING
                            | PhoneAccount.CAPABILITY_EMERGENCY_CALLS_ONLY))
                            | MtkPhoneAccount.CAPABILITY_IS_VIRTUAL_LINE;

            // Need update video capability of virtual account when onVideoCapabilitiesChanged
            mIsVideoCapable = mPhone.isVideoEnabled();

            if (!mIsPrimaryUser) {
                Log.i(this, "Disabling video calling for secondary user.");
                mIsVideoCapable = false;
            }

            if (mIsVideoCapable) {
                capabilities |= PhoneAccount.CAPABILITY_VIDEO_CALLING;
            }

            mIsVideoPresenceSupported = isCarrierVideoPresenceSupported();
            if (mIsVideoCapable && mIsVideoPresenceSupported) {
                capabilities |= PhoneAccount.CAPABILITY_VIDEO_CALLING_RELIES_ON_PRESENCE;
            }
            return capabilities;
        }

        // Customize virtual phone account extras
        private Bundle setupAccountExtras(PhoneAccount parentAccount) {
            Bundle extras = new Bundle(parentAccount.getExtras());

            extras.putParcelable(MtkPhoneAccount.EXTRA_PARENT_PHONE_ACCOUNT_HANDLE,
                    parentAccount.getAccountHandle());
            return extras;
        }

       /**
        * Customize some members of VirtualAccountEntry.
        * VirtualAccountEntry has already override registerPstnPhoneAccount and only
        * make a dummy PhoneAccount in it. So some parent class members which updated in
        * registerPstnPhoneAccount previously have no chance to update.
        * Need update these members specially for VirtualAccountEntry.
        */
        private void updateAccountEntryCapability() {
            mIsVideoPauseSupported = isCarrierVideoPauseSupported();
            mIsMergeCallSupported = isCarrierMergeCallSupported();
            mIsMergeImsCallSupported = isCarrierMergeImsCallSupported();
            mIsVideoConferencingSupported = isCarrierVideoConferencingSupported();
            mIsMergeOfWifiCallsAllowedWhenVoWifiOff =
                    isCarrierMergeOfWifiCallsAllowedWhenVoWifiOff();
            mIsManageImsConferenceCallSupported = isCarrierManageImsConferenceCallSupported();
            mIsShowPreciseFailedCause = isCarrierShowPreciseFailedCause();
        }

      /**
       * Customize virtual phone account icon.
       * Virtual account icon use parent account icon plus its own color now.
       * @param color The icon color.
       * @return A icon.
       */
        private Icon setupAccountIcon(int color) {
            SubscriptionInfo record =
                    mSubscriptionManager.getActiveSubscriptionInfo(mPhone.getSubId());
            Bitmap iconBitmap = null;

            if (mTelephonyManager.getPhoneCount() != 1 && record != null) {
                iconBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                        com.android.internal.R.drawable.ic_sim_card_multi_24px_clr);
            } else {
                iconBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                        com.android.phone.R.drawable.ic_multi_sim);
            }

            return createIconWithBitmap(mContext, iconBitmap, color);
        }

       /**
        * Creates and returns an icon.
        * @param context A valid context.
        * @param iconBitmap A valid iconBitmap.
        * @param color The icon color.
        * @return A icon.
        */
        private Icon createIconWithBitmap(Context context, Bitmap iconBitmap, int color) {
            if (iconBitmap == null) {
                return null;
            }

            int width = iconBitmap.getWidth();
            int height = iconBitmap.getHeight();
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();

            // Create a new bitmap of the same size because it will be modified.
            Bitmap workingBitmap = Bitmap.createBitmap(metrics, width, height,
                    iconBitmap.getConfig());

            Canvas canvas = new Canvas(workingBitmap);
            Paint paint = new Paint();

            // Tint the icon with the color.
            paint.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));
            canvas.drawBitmap(iconBitmap, 0, 0, paint);
            paint.setColorFilter(null);

            return Icon.createWithBitmap(workingBitmap);
        }

        private PhoneAccount makeDummyPhoneAccount() {
            return PhoneAccount.builder(
                    new PhoneAccountHandle(new ComponentName("dummy.package", "dummy.class"),
                            "dummy.id"), "dummy.label").build();
        }
    }
}
