package com.mediatek.op08.phone;

import android.content.Context;
import android.telephony.TelephonyManager;



//import com.mediatek.common.PluginImpl;
import com.mediatek.phone.ext.DefaultSsRoamingServiceExt;

/**
 * Plugin implementation for OP08 Roaming condition.
 */
public class Op08SsRoamingServiceExt extends DefaultSsRoamingServiceExt {
    private static final String LOG_TAG = "Op08SsRoamingServiceExt";
    private Context mContext;


    public Op08SsRoamingServiceExt(Context context) {
        mContext = context;
    }

    public boolean isNotificationForRoamingAllowed(Context context) {
        String networkOperator = TelephonyManager.getDefault().getNetworkOperatorName();
        if (networkOperator.equals("AT&T")) {
            return false;
        } else {
           return true;
        }

    }

}
