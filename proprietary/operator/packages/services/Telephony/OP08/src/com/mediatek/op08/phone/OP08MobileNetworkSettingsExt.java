/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.op08.phone;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.ims.ImsManager;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;

import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.phone.ext.DefaultMobileNetworkSettingsExt;
import com.mediatek.telephony.MtkTelephonyManagerEx;

/**
 * Plugin implementation for OP08.
 */
public class OP08MobileNetworkSettingsExt extends DefaultMobileNetworkSettingsExt {
    private static final String TAG = "OP08MobileNetworkSettingsExt";
    public static final String BUTTON_ENABLED_NETWORKS_KEY = "enabled_networks_key";
    private static final String KEY_WFC_SETTINGS = "wifi_calling_key";
    private static final String BUTTON_4G_LTE_KEY = "enhanced_4g_lte";
    private static final String BUTTON_VOLTE_TITLE = "VoLTE";
    private Context mContext = null;
    private ListPreference mButtonEnabledNetworks;
    public static final String PROP_SETTINGS_DISABLE_2G = "persist.radio.disable.2g";
    private Phone mPhone;
    MtkGsmCdmaPhone mGsmCdmaphone;
    private boolean mIsDisable2G = false ;
    private MyHandler mHandler;
    private int mSubId;
    private AlertDialog mDialog;
    private AlertDialog.Builder mDialogBuild;
    PreferenceActivity mActivity;
    private WfcSummary mWfcSummary;
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Log.d(TAG, "onReceive:" + action);
            if (!ImsManager.isWfcEnabledByPlatform(mContext)) {
                Log.d(TAG, "Wfc not present in platform");
            }
            if (action.equals(WfcSummary.ACTION_WFC_SUMMARY_CHANGE)) {
                if (mActivity == null) {
                   Log.d(TAG, "mActivity is null");
                   return;
                }
                Preference wfcPref = mActivity.findPreference(KEY_WFC_SETTINGS);
                Log.d(TAG, "wfcPreference:" + wfcPref);
                if (wfcPref != null) {
                    wfcPref.setSummary(intent
                            .getStringExtra(WfcSummary.EXTRA_SUMMARY));
                }
            }
        }
    };
    /**
     * Plugin implementation for Mobile network settings.
     * @param context context
     */
    public OP08MobileNetworkSettingsExt(Context context) {
        mContext = context;
        mHandler = new MyHandler();
        mWfcSummary = new WfcSummary(context);
    }


    @Override
    public void initOtherMobileNetworkSettings(PreferenceActivity activity, int subId) {
        Log.d("@M_" + TAG, "Initialize preference activity" + activity);
        int phoneId = SubscriptionManager.getPhoneId(subId);
        mActivity = activity;
        mSubId = subId;
        PreferenceScreen prefSet = activity.getPreferenceScreen();
        ListPreference mButtonEnabledNetworks = (ListPreference) prefSet.findPreference(
                BUTTON_ENABLED_NETWORKS_KEY);
        if (mButtonEnabledNetworks == null) {
            Log.d(TAG, "mButtonEnabledNetworks is null");
            return;
        }
        mPhone =
                PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
        mGsmCdmaphone =
                    (MtkGsmCdmaPhone) PhoneFactory.getPhone(mPhone.getPhoneId());
        mGsmCdmaphone.getDisable2G(mHandler.obtainMessage(MyHandler.MESSAGE_QUERY_2G_MODE));
        int currentNwMode =
                android.provider.Settings.Global.getInt(mPhone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId, 0);
        Log.d(TAG, "Current network mode for subId " + subId + " : " + currentNwMode);
        CharSequence entries[];
        CharSequence entryValues[];
            entries = mContext.getResources().getTextArray(R.array.enabled_networks_2g_on_choices);
            entryValues = mContext.getResources().getTextArray(
                    R.array.network_mode_options_2g_on_values);
            mButtonEnabledNetworks.setEntries(entries);
            mButtonEnabledNetworks.setEntryValues(entryValues);
        final Preference buttonDataLte = (Preference) prefSet
                .findPreference(BUTTON_4G_LTE_KEY);
        if (buttonDataLte != null) {
            prefSet.removePreference(buttonDataLte);
        }
        if (mActivity == null) {
           return;
        }
        Preference wfcPreference = mActivity.findPreference(KEY_WFC_SETTINGS);
        Log.d(TAG, "initOtherMobileNetworkSettings, wfcPreference:" + wfcPreference);
        if (wfcPreference == null) {
            return;
        }
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
        Log.d(TAG, "[getWfcSummary] isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
            wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        wfcPreference.setSummary(mWfcSummary.getWfcSummaryText(wfcState));
    }


    @Override
    public void customizePreferredNetworkMode(ListPreference buttonEnabledNetworks, int subId) {
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        Phone phone =
                PhoneFactory.getPhone(SubscriptionManager.getPhoneId(subId));
        int currentNwMode =
                android.provider.Settings.Global.getInt(phone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + subId, 0);
        Log.d(TAG, "Current network mode for subId " + subId + " : " + currentNwMode);
        CharSequence entries[];
        CharSequence entryValues[];
            entries = mContext.getResources().getTextArray(R.array.enabled_networks_2g_on_choices);
            entryValues = mContext.getResources().getTextArray(
                    R.array.network_mode_options_2g_on_values);
            buttonEnabledNetworks.setEntries(entries);
            buttonEnabledNetworks.setEntryValues(entryValues);
        switch (currentNwMode) {
            case Phone.NT_MODE_LTE_GSM_WCDMA:
            case Phone.NT_MODE_LTE_ONLY:
            case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                if (!isSettingOn()) {
                    buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_GSM_WCDMA));
                    buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_on));
                } else {
                    buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_WCDMA));
                    buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_3g));
                }
                break;
            case Phone.NT_MODE_LTE_WCDMA:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_WCDMA));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_3g));
                break;
            case Phone.NT_MODE_WCDMA_PREF:
            case Phone.NT_MODE_WCDMA_ONLY:
            case Phone.NT_MODE_GSM_UMTS:
            case Phone.NT_MODE_CDMA:
            case Phone.NT_MODE_EVDO_NO_CDMA:
            case Phone.NT_MODE_GLOBAL:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_WCDMA_PREF));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_off));
                break;
            case Phone.NT_MODE_GSM_ONLY:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_GSM_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.only_2g));
                break;
            default:
        }
    }

    /**
     * Updating network mode and summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     */
    @Override
    public void updatePreferredNetworkValueAndSummary(ListPreference buttonEnabledNetworks,
            int networkMode) {
        Log.d(TAG, "Updating network mode and summary. networkMode:" + networkMode);
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return;
        }
        switch (networkMode) {
            case Phone.NT_MODE_LTE_GSM_WCDMA:
            case Phone.NT_MODE_LTE_ONLY:
            case Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA:
                if (!isSettingOn()) {
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_GSM_WCDMA));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_on));
                } else {
                    buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_WCDMA));
                    buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_3g));
                }
                break;
            case Phone.NT_MODE_LTE_WCDMA:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_WCDMA));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_3g));
                break;
            case Phone.NT_MODE_WCDMA_PREF:
            case Phone.NT_MODE_WCDMA_ONLY:
            case Phone.NT_MODE_GSM_UMTS:
            case Phone.NT_MODE_CDMA:
            case Phone.NT_MODE_EVDO_NO_CDMA:
            case Phone.NT_MODE_GLOBAL:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_WCDMA_PREF));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_off));
                break;
            case Phone.NT_MODE_GSM_ONLY:
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_GSM_ONLY));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.only_2g));
                break;
            default:
        }
    }

    @Override
    public boolean isNetworkChanged(ListPreference buttonEnabledNetworks, int networkMode,
            int currentMode, Phone phone) {
        Log.d(TAG, "networkMode" + networkMode + "currentMode" + currentMode +
                "is stting on " + isSettingOn());
        if (networkMode == Phone.NT_MODE_LTE_GSM_WCDMA && isSettingOn()) {
            //network mode is changed lte_3g to lte_on
            return true;
        } else if (networkMode != currentMode) {
            return true;
        }
        return false;
    }

    /**
     * Updating network mode and summary.
     * @param buttonEnabledNetworks list preference
     * @param networkMode network mode
     * @param currentMode current network mode
     * @param phone Phone on which UE will handle
     * @param cr Content resolver of Settings
     * @param phoneSubId SubID on which change occurred
     * @param handler Handler to take care of NEtwork update
     * @return false if operator handle
     */
    @Override
    public boolean isNetworkUpdateNeeded(ListPreference buttonEnabledNetworks,
            int networkMode, int currentMode, Phone phone, ContentResolver cr,
            int phoneSubId, Handler handler) {
        Log.d(TAG, "isNetworkUpdateNeeded:" + networkMode);
        mButtonEnabledNetworks = buttonEnabledNetworks;
        mPhone = phone;
        if (buttonEnabledNetworks == null) {
            Log.d(TAG, "buttonEnabledNetworks is null");
            return true;
        }

        if (networkMode != Phone.NT_MODE_LTE_WCDMA) {
            if (networkMode == Phone.NT_MODE_LTE_GSM_WCDMA) {
                buttonEnabledNetworks.setValue(Integer.toString(Phone.NT_MODE_LTE_GSM_WCDMA));
                buttonEnabledNetworks.setSummary(mContext.getText(R.string.lte_on));
            }
            if (isSettingOn()) {
                mButtonEnabledNetworks.setEnabled(false);
                mIsDisable2G = false;
                handleSwitchAction(false);
            }

            return true;
        }
        switch (networkMode) {
            case Phone.NT_MODE_LTE_WCDMA:
                if (!isSettingOn()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    Log.d(TAG, "isNetworkUpdateNeeded alert");
                    builder.setTitle(mContext.getString(R.string.dialog_disable_2g_Alert_title));
                    builder.setMessage(mContext.getString(R.string.dialog_disable_2g_summary));
                    builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            Log.d(TAG, "onCancel");
                        }
                    });
                    builder.setPositiveButton(mContext.getString(
                            R.string.dialog_disable_2g_alert_ok),
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (mButtonEnabledNetworks != null) {
                                        buttonEnabledNetworks.setValue(
                                                Integer.toString(Phone.NT_MODE_LTE_WCDMA));
                                        buttonEnabledNetworks.setSummary(
                                                mContext.getText(R.string.lte_3g));
                                        updateNetworkMode(Phone.NT_MODE_LTE_GSM_WCDMA);
                                        mButtonEnabledNetworks.setEnabled(false);
                                    }

                                    mIsDisable2G = true;
                                    handleSwitchAction(true);
                                }
                            });
                    builder.setNegativeButton(mContext.getString(
                        R.string.dialog_disable_2g_alert_cancel),
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.d(TAG, "For no don't change mode");
                                updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks,
                                        currentMode);

                            }
                        });
                    mDialog = builder.show();
                }
                break;
                default:
                    return true;
        }
        return false;
    }

   /**
    * Get operator specific customized summary for WFC button.
    * Used in WirelessSettings
    * @param context context
    * @param defaultSummaryResId default summary res id
    * @return String
    */
    @Override
    public String customizeWfcSummary(Context context, int defaultSummaryResId, int phoneId) {
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
        Log.d(TAG, "[getWfcSummary]isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
            wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        return mWfcSummary.getWfcSummaryText(wfcState);
    }

   /**
    * Called from onResume.
    * @return
    */
    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        IntentFilter filter = new IntentFilter(WfcSummary.ACTION_WFC_SUMMARY_CHANGE);
        mContext.registerReceiver(mReceiver, filter);
        mWfcSummary.onResume();
        if (mActivity == null) {
           return;
        }
        Preference wfcPreference = mActivity.findPreference(KEY_WFC_SETTINGS);
        Log.d(TAG, "onResume, wfcPreference:" + wfcPreference);
        if (wfcPreference == null) {
            return;
        }
        boolean isWfcRegistered = MtkTelephonyManagerEx.getDefault()
                .isWifiCallingEnabled(SubscriptionManager.getDefaultVoiceSubscriptionId());
        Log.d(TAG, "[getWfcSummary] isWfcRegistered:" + isWfcRegistered);
        int wfcState = WfcReasonInfo.CODE_WFC_DEFAULT;
        if (isWfcRegistered) {
            wfcState = WfcReasonInfo.CODE_WFC_SUCCESS;
        }
        wfcPreference.setSummary(mWfcSummary.getWfcSummaryText(wfcState));
    }

   /**
    * Called from onPause.
    * @return
    */
    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        mContext.unregisterReceiver(mReceiver);
        mWfcSummary.onPause();
    }

    private void updateNetworkMode(int mode) {
        Log.d(TAG, "Updating network mode " + mode);

        updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks, mode);
        android.provider.Settings.Global.putInt(mPhone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + mSubId, mode);
        mPhone.setPreferredNetworkType(mode, null);
    }


    public boolean isEnhancedLTENeedToAdd(boolean defaultValue, int phoneId) {
        return false;
    }

    /**
     * Updating entry of list preference.
     * @param buttonEnabledNetworks preference activity
     */
    /*@Override
    public void changeEntries(ListPreference buttonEnabledNetworks) {
        Log.d(TAG, "update entry and entry values of list preference");
        CharSequence cs[] = mContext.getResources().getTextArray(
                R.array.enabled_networks_4g_choices);
        buttonEnabledNetworks.setEntries(cs);
    }*/


    /*@Override
    public void onPreferenceChange(Preference preference, Object objValue) {
        Log.i(TAG, "OP08 onPreferenceChange preference = " + preference);

        if (preference.getKey().equals(BUTTON_DISABLE_2G)) {
            Log.i(TAG, "OP08 BUTTON_DISABLE_2G preference = " + preference);
            if (((SwitchPreference) preference).isChecked() == (Boolean) objValue) {
                // State got changed
                Log.i(TAG, preference.getKey() + " : " + String.valueOf(objValue) +
                        " not changed");
                return;
            }
            Log.i(TAG, "OP08 onPreferenceChange disable preference ");
            boolean checked = ((SwitchPreference) preference).isChecked();
            mDisable2GPreference.setEnabled(false);
            handleSwitchAction(checked);
        }
    }*/

    private boolean isSettingOn() {
        return mIsDisable2G;
    }

    private void handleSwitchAction(boolean switchValue) {
        mGsmCdmaphone.setDisable2G(switchValue,
                mHandler.obtainMessage(MyHandler.MESSAGE_SET_2G_MODE));
    }

    /**
     * Handler class to handle Disable 2G button.
     */
    private class MyHandler extends Handler {
        private static final int MESSAGE_QUERY_2G_MODE = 0;
        private static final int MESSAGE_SET_2G_MODE = 1;
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_QUERY_2G_MODE:
                    handleQuery2gMode(msg);
                    break;
                case MESSAGE_SET_2G_MODE:
                    handleSet2gMode(msg);
                    break;
                default:
                    break;
            }
        }
    }
    private void handleQuery2gMode(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        if (ar.exception != null) {
            Log.d(TAG, "handleQuery2gMode with exception = " + ar.exception);
        } else {
            Log.d(TAG, "handleQuery2gMode no exception");
            int[] mode = (int[]) ar.result;
            Log.d(TAG, "handleQuery2gMode result: " + mode[0]);
            if (mode[0] == 0) {
                mIsDisable2G = false;
            } else if (mode[0] == 1) {
                mIsDisable2G = true;
            }
        }
    }
    private void handleSet2gMode(Message msg) {
        AsyncResult ar = (AsyncResult) msg.obj;
        Log.d(TAG, "handleSet2gMode");
        if (ar.exception != null) {
            Log.d(TAG, "handleSet2gMode with exception = " + ar.exception);
            mIsDisable2G = (mIsDisable2G ? false : true);
            customizePreferredNetworkMode(mButtonEnabledNetworks, mSubId);
        } else {
            customizePreferredNetworkMode(mButtonEnabledNetworks, mSubId);
            Log.d(TAG, "handleSet2gMode no exception");
        }
        initOtherMobileNetworkSettings(mActivity, mSubId);
        if (mButtonEnabledNetworks != null) {
            mButtonEnabledNetworks.setEnabled(true);
        }
        int currentNwMode =
                android.provider.Settings.Global.getInt(mPhone.getContext().getContentResolver(),
                android.provider.Settings.Global.PREFERRED_NETWORK_MODE + mSubId, 0);
        updatePreferredNetworkValueAndSummary(mButtonEnabledNetworks, currentNwMode);

    }
}