package com.mediatek.op08.phone;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import com.android.ims.ImsManager;


public class VideoSettingReceiverService extends Service {
    private static final String TAG = "VideoSettingReceiverService";
    private Context mContext;


    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        mContext = getApplicationContext();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals(WifiManager.WIFI_STATE_CHANGED_ACTION)) {
                boolean mIsWifiEnabled =
                        intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                        WifiManager.WIFI_STATE_UNKNOWN) == WifiManager.WIFI_STATE_ENABLED;
                if (ImsManager.isVtEnabledByPlatform(mContext)) {
                    if (mIsWifiEnabled) {
                        Log.d(TAG, "Wifi Enabled but no action taken as per TMO requirement");
                       // boolean value = (Settings.Global.getInt(mContext.getContentResolver(),
                         //       "KEY_VIDEO", 1)) == 1;
                        //ImsManager.setVtSetting(mContext, value);
                    } else {
                        Log.d(TAG, "Wifi disabled: Did not set VT false");
                       // ImsManager.setVtSetting(mContext, false);
                    }
                }
            } else {
                ContentObserver mContentObserver = new ContentObserver(new Handler()) {
                    @Override
                    public void onChange(boolean selfChange) {
                        if (ImsManager.isVtEnabledByPlatform(mContext)) {
                            int enabled = Settings.Global.getInt(mContext.getContentResolver(),
                                    Settings.Global.MOBILE_DATA,
                                    0);
                            if (enabled == 1) {
                                Log.d(TAG, "Mobile data Enabled");
                                boolean value = (Settings.Global.getInt(mContext
                                                  .getContentResolver(), "KEY_VIDEO", 1)) == 1;
                                ImsManager.setVtSetting(mContext, value);
                            } else {
                                Log.d(TAG, "Mobile data disabled; Set VT false");
                                ImsManager.setVtSetting(mContext, false);
                            }
                        }
                    }
                };
                mContext.getContentResolver().registerContentObserver(Settings.Global
                        .getUriFor(Settings.Global.MOBILE_DATA), false, mContentObserver);
            }
        }
        return Service.START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
}