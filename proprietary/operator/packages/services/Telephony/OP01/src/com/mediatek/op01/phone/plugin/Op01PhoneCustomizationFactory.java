package com.mediatek.op01.phone.plugin;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.IIncomingCallExt;
import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.INetworkSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op01PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {

    public Context mContext;
    public Op01PhoneCustomizationFactory (Context context){
        mContext = context;
    }

    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeMobileNetworkSettingsExt");
        return new Op01MobileNetworkSettingsExt(mContext);
    }

    public INetworkSettingExt makeNetworkSettingExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeNetworkSettingExt");
        return new OP01NetworkSettingExt(mContext);
    }

    public IIncomingCallExt makeIncomingCallExt() {
        Log.i("Op01PhoneCustomizationFactory", "makeIncomingCallExt");
        return new Op01IncomingCallExt(mContext);
    }
}
