package com.mediatek.op11.phone;

import android.content.Context;

import com.mediatek.phone.ext.ICallFeaturesSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;

public class Op11PhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {
    private Context mContext;

    public Op11PhoneCustomizationFactory(Context context) {
        mContext = context;
    }

    public ICallFeaturesSettingExt makeCallFeaturesSettingExt() {
        return new Op11CallFeaturesSettingExt(mContext);
    }
}
