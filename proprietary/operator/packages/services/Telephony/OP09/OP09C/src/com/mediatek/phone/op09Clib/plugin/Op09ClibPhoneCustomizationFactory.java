package com.mediatek.phone.op09Clib.plugin;

import android.content.Context;

import com.mediatek.phone.ext.IMobileNetworkSettingsExt;
import com.mediatek.phone.ext.INetworkSettingExt;
import com.mediatek.phone.ext.OpPhoneCustomizationFactoryBase;


public class Op09ClibPhoneCustomizationFactory extends OpPhoneCustomizationFactoryBase {

    public Context mContext;
    public Op09ClibPhoneCustomizationFactory (Context context){
        mContext = context;
    }
    @Override
    public IMobileNetworkSettingsExt makeMobileNetworkSettingsExt() {
        return new OP09MobileNetworkSettingsExt(mContext);
    }

    @Override
    public INetworkSettingExt makeNetworkSettingExt() {
        return new OP09ClibNetworkSettingExt(mContext);
    }
}
