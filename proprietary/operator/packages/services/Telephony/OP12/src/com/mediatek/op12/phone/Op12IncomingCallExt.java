package com.mediatek.op12.phone;

import android.content.Context;
import android.util.Log;

import com.mediatek.phone.ext.DefaultIncomingCallExt;

public class Op12IncomingCallExt extends DefaultIncomingCallExt {
    private static final String LOG_TAG = "Op12IncomingCallExt";
    protected Context mContext;

    public Op12IncomingCallExt (Context context) {
        mContext = context;
    }

    /**
     * change the disconnect cause when user reject a call.
     * @param disconnectCause disconnectCause
     * @return disconnectCause after modified
     */
    @Override
    public int changeDisconnectCause(int disconnectCause) {
        if (disconnectCause == android.telephony.DisconnectCause.INCOMING_REJECTED) {
            disconnectCause = android.telephony.DisconnectCause.INCOMING_MISSED;
            Log.i(LOG_TAG, "changeDisconnectCause() disconnectCause: " + disconnectCause);
        }
        return disconnectCause;
    }
}
