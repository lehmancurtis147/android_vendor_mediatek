/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.ims.ril;

import android.content.Context;
import android.os.Handler;
import android.os.Registrant;
import android.os.RegistrantList;
import com.mediatek.ims.ril.OpImsCommandsInterface;

/**
 * {@hide}
 */
public abstract class OpImsBaseCommands implements OpImsCommandsInterface {

    // ***** Instance Variables

    // Context
    protected Context mContext;

    // Current Phone Id
    protected int mPhoneId;

    // GTT Capability Indication RegistrantList
    protected RegistrantList mGttCapabilityIndicatorRegistrants = new RegistrantList();
    // RTT Modify Response RegistrantList
    protected RegistrantList mRttModifyResponseRegistrants = new RegistrantList();
    // RTT Text Receive RegistrantList
    protected RegistrantList mRttTextReceiveRegistrants = new RegistrantList();
    // RTT Modify Request Receive RegistrantList
    protected RegistrantList mRttModifyRequestReceiveRegistrants = new RegistrantList();

    public OpImsBaseCommands(Context context, int instanceId) {
        mContext = context;
        mPhoneId = instanceId;
    }

    /**
     * Registers the handler for GTT capability changed event.
     * @param h Handler for notification message.
     * @param what User-defined message code.
     * @param obj User object.
     *
     */
    @Override
    public void registerForGttCapabilityIndicator(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mGttCapabilityIndicatorRegistrants.add(r);
    }

    /**
     * Unregisters the handler for GTT capability changed event.
     *
     * @param h Handler for notification message.
     *
     */
    @Override
    public void unregisterForGttCapabilityIndicator(Handler h) {
        mGttCapabilityIndicatorRegistrants.remove(h);
    }


    /**
     * Registers the handler for Rtt Modify Response event.
     * @param h Handler for notification message.
     * @param what User-defined message code.
     * @param obj User object.
     *
     */
    @Override
    public void registerForRttModifyResponse(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mRttModifyResponseRegistrants.add(r);
    }

    /**
     * Unregisters the handler for Rtt Modify Response event.
     *
     * @param h Handler for notification message.
     *
     */
    @Override
    public void unregisterForRttModifyResponse(Handler h) {
        mRttModifyResponseRegistrants.remove(h);
    }

    /**
     * Registers the handler for Rtt Text Receive event.
     * @param h Handler for notification message.
     * @param what User-defined message code.
     * @param obj User object.
     *
     */
    @Override
    public void registerForRttTextReceive(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mRttTextReceiveRegistrants.add(r);
    }

    /**
     * Unregisters the handler for Rtt Text Receive event.
     *
     * @param h Handler for notification message.
     *
     */
    @Override
    public void unregisterForRttTextReceive(Handler h) {
        mRttTextReceiveRegistrants.remove(h);
    }

    /**
     * Registers the handler for Rtt Modify Request Receive event.
     * @param h Handler for notification message.
     * @param what User-defined message code.
     * @param obj User object.
     *
     */
    @Override
    public void registerForRttModifyRequestReceive(Handler h, int what, Object obj) {
        Registrant r = new Registrant(h, what, obj);
        mRttModifyRequestReceiveRegistrants.add(r);
    }

    /**
     * Unregisters the handler for Rtt Modify Request Receive event.
     *
     * @param h Handler for notification message.
     *
     */
    @Override
    public void unregisterForRttModifyRequestReceive(Handler h) {
        mRttModifyRequestReceiveRegistrants.remove(h);
    }
}
