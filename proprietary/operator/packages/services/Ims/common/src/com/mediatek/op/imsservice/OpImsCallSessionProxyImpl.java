/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2014. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.op.imsservice;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncResult;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.telephony.Rlog;

import android.telephony.ims.ImsCallProfile;
import android.telephony.ims.ImsStreamMediaProfile;
import android.telephony.ims.ImsCallSessionListener;

import com.android.ims.internal.IImsCallSession;

import com.mediatek.ims.internal.IMtkImsCallSessionListener;
import com.mediatek.ims.internal.IMtkImsCallSession;
import com.mediatek.ims.internal.op.OpImsCallSessionProxyBase;
import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.ims.MtkImsCallSessionProxy;
import com.mediatek.ims.ril.ImsCommandsInterface;
import com.mediatek.ims.ril.OpImsCommandsInterface;
import com.mediatek.ims.ImsCallSessionProxy;

import android.telecom.Connection;

import java.io.IOException;
import java.util.Arrays;

public class OpImsCallSessionProxyImpl extends OpImsCallSessionProxyBase {
    private static final String TAG = "OpImsCallSessionProxyImpl";

    private static final boolean RTT_SUPPORT = isRttSupport();

    private static boolean isRttSupport() {
        boolean isSupport = SystemProperties.get("persist.vendor.mtk_rtt_support").equals("1");
        Rlog.d(TAG, "isRttSupport: " + isSupport);
        return isSupport;
    }

    private int mIsIncomingCallRtt = 0;

    private boolean mIsRttEnabledForCallSession = false;

    public OpImsCallSessionProxyImpl() {
    }

    @Override
    public void handleGttCapabilityIndication(AsyncResult ar,
        IMtkImsCallSession proxy, String callId) {
        // +EIMSTCAP:<call id>,<local text capability>,<remote text capability>
        // <local text status>,<real remote text capability>
        // 0  Off
        // 1  On
        Rlog.d(TAG, "handleGttCapabilityIndication callId" + callId);
        if (ar == null) {
            Rlog.d(TAG, "handleGttCapabilityIndication ar is null");
            return;
        }

        int[] result = (int[]) ar.result;
        if (proxy == null || callId == null || result[0] != Integer.parseInt(callId)) {
            return;
        }
        int localCapability = result[1];
        int remoteCapability = result[2];
        int localTextStatus = result[3];
        int realRemoteTextCapability = result[4];
        Rlog.d(TAG, "handleGttCapabilityIndication local cap= " + localCapability +
                " remo status= " + remoteCapability + " local status= "+ localTextStatus +
                " remo cap= " + realRemoteTextCapability);


        mIsIncomingCallRtt = remoteCapability;
        mIsRttEnabledForCallSession = remoteCapability == 1 && localTextStatus == 1;
        Rlog.d(TAG, "handleGttCapabilityIndication mIsRttEnabledForCallSession: " + mIsRttEnabledForCallSession);

        try {
            proxy.notifyTextCapabilityChanged(localCapability, remoteCapability,
                localTextStatus, realRemoteTextCapability);
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException notifyTextCapabilityChanged()");
        }

    }

    @Override
    public void notifyTextCapabilityChanged(IMtkImsCallSessionListener mtkListener,
            IMtkImsCallSession mtkImsCallSessionProxy,
            int localCapability, int remoteCapability,
            int localTextStatus, int realRemoteTextCapability) {
        if (mtkListener == null) {
            Rlog.d(TAG, "notifyTextCapabilityChanged() listener is null");
            return;
        }
        try {
            mtkListener.callSessionTextCapabilityChanged(mtkImsCallSessionProxy,
                    localCapability, remoteCapability,
                    localTextStatus, realRemoteTextCapability);
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException callSessionTextCapabilityChanged()");
        }

    }

    @Override
    public void handleRttECCRedialEvent(IMtkImsCallSession proxy) {
        Rlog.d(TAG, "notifyRttECCRedialEvent");
        if (proxy == null) {
            return;
        }
        try {
            proxy.notifyRttECCRedialEvent();
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException notifyRttECCRedialEvent");
        }

    }
    @Override
    public void notifyRttECCRedialEvent(IMtkImsCallSessionListener mMtkListener,
        IMtkImsCallSession mtkImsCallSessionProxy) {
        if (mMtkListener == null) {
            Rlog.d(TAG, "notifyRttECCRedialEvent() listener is null");
            return;
        }
        try {
            mMtkListener.callSessionRttEventReceived(mtkImsCallSessionProxy, 137);
        } catch (RemoteException e) {
            Rlog.e(TAG, "RemoteException callSessionRttEventReceived");
        }
    }

    //M: for RTT utf8 convert to ucs2
    private final byte B_10000000 = 128 - 256;
    private final byte B_11000000 = 192 - 256;
    private final byte B_11100000 = 224 - 256;
    private final byte B_11110000 = 240 - 256;
    private final byte B_00011100 = 28;
    private final byte B_00000011 = 3;
    private final byte B_00000111 = 7;
    private final byte B_00111111 = 63;
    private final byte B_00001111 = 15;
    private final byte B_00111100 = 60;
    private final byte B_00110000 = 48;

    String printBytes(byte[] bytes) {
        String ret = "";
        for (byte b: bytes) {
            ret += Integer.toString((b & 0xff) , 16) + " ";
        }
        return ret;
    }

    byte[] mRemaining = new byte[]{};
    /** Convert from UTF8 bytes to UNICODE character */
    private char[] toUCS2(byte[] utf8Bytes) {
        CharList charList = new CharList();
        byte b2 = 0, b3 = 0, b4 = 0;
        int ub1 = 0, ub2 = 0, ub3 = 0;

        utf8Bytes = appendByteArray(mRemaining, utf8Bytes);
        clearRemaining();

        for (int i = 0; i < utf8Bytes.length; i++) {
            try{
                byte b = utf8Bytes[i];
                if (isNotHead(b)) {
                    // start with 10xxxxxx, skip it.
                    continue;
                } else if (b > 0) {
                    // 1 byte, ASCII
                    charList.add((char) b);
                } else if ((b & B_11110000) == B_11110000) {
                    // UCS-4 is used for emoji icons
                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b2 = utf8Bytes[i+1];
                    if (!isNotHead(b2)) continue;
                    i++;

                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b3 = utf8Bytes[i+1];
                    if (!isNotHead(b3)) continue;
                    i++;

                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b4 = utf8Bytes[i+1];
                    if (!isNotHead(b4)) continue;
                    i++;

                    ub1 = ((b & B_00000111) << 2) + ((b2 & B_00110000) >> 4);
                    ub2 = ((b2 & B_00001111) << 4) + ((b3 & B_00111100) >> 2);
                    ub3 = ((b3 & B_00000011) << 6) + ((b4 & B_00111111));
                    charList.add(makeChar(ub1, ub2, ub3));

                    clearRemaining();
                } else if ((b & B_11100000) == B_11100000) {
                    // 3 bytes
                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b2 = utf8Bytes[i+1];
                    if (!isNotHead(b2)) continue;
                    i++;

                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b3 = utf8Bytes[i+1];
                    if (!isNotHead(b3)) continue;
                    i++;

                    ub1 = ((b & B_00001111) << 4) + ((b2 & B_00111100) >> 2);
                    ub2 = ((b2 & B_00000011) << 6) + ((b3 & B_00111111));
                    charList.add(makeChar(ub1, ub2));

                    clearRemaining();
                } else {
                    // 2 bytes
                    if (checkIsRemaining(i+1, utf8Bytes)) break;
                    b2 = utf8Bytes[i+1];
                    if (!isNotHead(b2)) continue;
                    i++;

                    ub1 = (b & B_00011100) >> 2;
                    ub2 = ((b & B_00000011) << 6) + (b2 & B_00111111);
                    charList.add(makeChar(ub1, ub2));
                    clearRemaining();
                }
            } catch (IndexOutOfBoundsException e) {
                Rlog.e(TAG, "toUCS2: " + e);
                break;
            }
        }
        return charList.toArray();
    }
    private boolean isNotHead(byte b) {
        return (b & B_11000000) == B_10000000;
    }
    private int makeChar(int b1, int b2) {
        return ((b1 << 8) + b2);
    }
    private int makeChar(int b1, int b2, int b3) {
        return ((b1 << 16) + (b2 << 8) + b3);
    }

    private boolean checkIsRemaining(int index, byte[] utf8Bytes) {
        addRemaining(utf8Bytes[index-1]);
        if (index >= utf8Bytes.length) {
            return true;
        } else return false;
    }

    private void addRemaining(byte b) {
        mRemaining = appendByteArray(mRemaining, new byte[] {b});
    }

    private void clearRemaining() {
        mRemaining = new byte[]{};
    }

    private byte[] appendByteArray(byte[] a, byte[] b) {
        if (a.length == 0) return b;
        if (b.length == 0) return a;
        byte[] byteArray = new byte[a.length + b.length];
        System.arraycopy(a, 0, byteArray, 0, a.length);
        System.arraycopy(b, 0, byteArray, a.length, b.length);
        return byteArray;
    }

    private class CharList {
        private char[] data = null;
        private int used = 0;
        public void add(int c) {
            if (data == null) {
                data = new char[16];
            } else if (used >= data.length) {
                char[] temp = new char[data.length * 2];
                System.arraycopy(data, 0, temp, 0, used);
                data = temp;
            }
            char[] tmp = Character.toChars(c);
            for (int i = 0 ; i < tmp.length ; i++) {
                data[used++] = tmp[i];
            }
        }
        public char[] toArray() {
            char[] chars = new char[used];
            System.arraycopy(data, 0, chars, 0, used);
            return chars;
        }
    }

    @Override
    public void handleRttTextReceive(AsyncResult ar, String callId,
        ImsCallSessionListener listener) {

        if (!RTT_SUPPORT) {
            return;
        }

        Rlog.d(TAG, "handleRttTextReceive callId: " + callId);
        if (ar == null) {
            Rlog.d(TAG, "handleRttTextReceive ar is null");
            return;
        }
        String[] textReceived = (String[]) ar.result;
        if ( textReceived[0] == null || textReceived[1] == null || textReceived[2] == null) {
            Rlog.d(TAG, "textReceived is null");
            return;
        }
        int targetCallid =  Integer.parseInt(textReceived[0]);
        if (listener == null || callId == null || targetCallid != Integer.parseInt(callId)) {
            return;
        }
        Rlog.d(TAG, "Received call id = " + textReceived[0] + " len = " + textReceived[1] +
                        " textMessage = " + textReceived[2] +
                        " actual len = " + textReceived[2].length());
        if (textReceived[2].length() == 0 || Integer.parseInt(textReceived[1]) == 0) {
            Rlog.e(TAG, "handleRttTextReceivedIndication: length is 0");
            return;
        }

        String decodeText = null;
        try {

            String text = new String(textReceived[2]);
            int length = text.length();
            if (length <= 0) return;
            byte[] data = new byte[length / 2];
            for (int i = 0; i < length; i += 2) {
                data[i / 2] = (byte) ((Character.digit(text.charAt(i), 16) << 4)
                             + Character.digit(text.charAt(i+1), 16));
            }
            char[] c = toUCS2(data);
            decodeText = new String(c);
            Rlog.d(TAG, "Decode len = " + decodeText.length()
                    + ", textMessage = " + printBytes(decodeText.getBytes())
                    + ", remain len: " + mRemaining.length
                    + ", " + printBytes(mRemaining));
        } catch (Exception e) {
            Rlog.e(TAG, "handleRttTextReceivedIndication:exception " + e);
            return;
        }

        if (decodeText.length() == 0) {
            Rlog.e(TAG, "handleRttTextReceivedIndication: decodeText length is 0");
            return;
        }

        byte[] bom = new byte[]{(byte)0xEF, (byte)0xBB, (byte)0xBF};
        String BOM = null;
        try {
            BOM = new String(bom, "utf-8");
        } catch (IOException e) {
            Rlog.d(TAG, "Exception when transcode bom to string, " + e);
        }
        if (decodeText.length() == 1 && decodeText.equals(BOM) && mRemaining.length == 0) {
            Rlog.d(TAG, "found BOM, ignore it");
            return;
        }

        listener.callSessionRttMessageReceived(decodeText);
    }

    @Override
    public void handleRttModifyResponse(AsyncResult ar, String callId,
        ImsCallSessionListener listener) {

        if (!RTT_SUPPORT) {
            return;
        }

        Rlog.d(TAG, "handleRttModifyResponse callId: " + callId);
        if (ar == null || listener == null) {
            Rlog.d(TAG, "handleRttModifyResponse ar or  mListener is null");
            return;
        }
        int[] result = (int[]) ar.result;
        if (callId == null || result[0] != Integer.parseInt(callId)) {
            return;
        }
        int response = result[1];
        int status = 0;
        /*
            URC RTTCALL
            Result:
            0: command success
            1: command fail
        */
        if (response == 0) { //RTTCALL command success : do nothing
            Rlog.d(TAG, "handleRttModifyResponse ignore modify success");
            return;
        } else { //RTTCALL Fail
            Rlog.d(TAG, "handleRttModifyResponse fail status = " + response);
            status = android.telecom.Connection.RttModifyStatus.SESSION_MODIFY_REQUEST_INVALID;
        }

        listener.callSessionRttModifyResponseReceived(status);
    }

    @Override
    public void handleRttModifyRequestReceive(AsyncResult ar,
        IImsCallSession proxy, String callId, ImsCallSessionListener listener,
        ImsCommandsInterface imsRILAdapter) {

        if (!RTT_SUPPORT) {
            return;
        }

        Rlog.d(TAG, "handleRttModifyRequestReceive callId: " + callId);
        if (ar == null || proxy == null|| listener == null) {
            Rlog.d(TAG, "handleRttModifyRequestReceive ar or proxy or  mListener is null");
            return;
        }
        int[] result = (int[]) ar.result;
        if (callId == null || result[0] != Integer.parseInt(callId)) {
            return;
        }
        int status = result[1];
        ImsCallProfile imsCallProfile = new ImsCallProfile();
        if (status == 1) {
            imsCallProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_FULL);
        } else {
            imsCallProfile.mMediaProfile.setRttMode(ImsStreamMediaProfile.RTT_MODE_DISABLED);
            //auto accept PRTTCALL to MD
            sendRttModifyResponse(callId, imsRILAdapter, true);
        }

        listener.callSessionRttModifyRequestReceived(imsCallProfile);
    }


    private static String HEX = "0123456789ABCDEF";
    private static String toHex(int n) {
        StringBuilder b = new StringBuilder();
        if (n < 0) n += 256;
        b.append(HEX.charAt(n >> 4));
        b.append(HEX.charAt(n & 0x0F));
        return b.toString();
    }

    @Override
    public void sendRttMessage(String callIdString, ImsCommandsInterface imsRILAdapter,
        String rttMessage) {

        if (!RTT_SUPPORT) {
            return;
        }
        int callId = Integer.parseInt(callIdString);

        Rlog.d(TAG, "sendRttMessage rttMessage= " + rttMessage);
        if (rttMessage == null) return;

        //trascode utf8 string to utf8 byte array
        int length = rttMessage.length();
        String encodeText = null;
        Rlog.d(TAG, "sendRttMessage rttMessage= " + rttMessage + " len= " + length);
        if (rttMessage == null || length <= 0) return;
        int utf8_len = 0;
        try {
           byte[] bytes_utf8 = rttMessage.getBytes("utf-8");
           if (bytes_utf8 != null ) utf8_len = bytes_utf8.length;
           StringBuilder sbuild = new StringBuilder();
           for (int i = 0; i < bytes_utf8.length; i++) {
               Byte b = new Byte(bytes_utf8[i]);
               int ch = b.intValue();
               String bb = toHex(ch);
               sbuild.append(bb);
           }
           encodeText = sbuild.toString();
           Rlog.d(TAG, " len ="+ rttMessage.length() + " = " +
               encodeText + " encodeText.length= "+ bytes_utf8.length);
        } catch (java.io.UnsupportedEncodingException e) {
           e.printStackTrace();
           return;
        }
        if (encodeText != null && utf8_len > 0) {
            OpImsCommandsInterface opUtil = imsRILAdapter.getOpCommandsInterface();
            opUtil.sendRttText(callId, encodeText, utf8_len, null);
        }
    }

    @Override
    public void sendRttModifyRequest(String callIdString, ImsCommandsInterface imsRILAdapter,
        ImsCallProfile to) {

        if (!RTT_SUPPORT) {
            return;
        }

        int callId = Integer.parseInt(callIdString);
        if (to == null) {
            Rlog.d(TAG, "sendRttModifyRequest invalid ImsCallProfile");
            return;
        }
        OpImsCommandsInterface opUtil = imsRILAdapter.getOpCommandsInterface();
        if (to.mMediaProfile.isRttCall() == true) {
            Rlog.d(TAG, "sendRttModifyRequest upgrade mCallId= " + callId);
            opUtil.sendRttModifyRequest(callId, 1, null);

        } else {
            Rlog.d(TAG, "sendRttModifyRequest downgrade mCallId= " + callId);
            opUtil.sendRttModifyRequest(callId, 0, null);
        }
    }

    @Override
    public void sendRttModifyResponse(String callIdString, ImsCommandsInterface imsRILAdapter,
        boolean response) {

        if (!RTT_SUPPORT) {
            return;
        }

        int callId = Integer.parseInt(callIdString);
        Rlog.d(TAG, "sendRttModifyResponse = " + response);
        OpImsCommandsInterface opUtil = imsRILAdapter.getOpCommandsInterface();
        ///MD didn't support result 1. Alaways send 0.
        opUtil.setRttModifyRequestResponse(callId, 0, null);
    }

    @Override
    public void setRttModeForDial(String callIdString, ImsCommandsInterface imsRILAdapter,
        boolean isRtt) {
        Rlog.d(TAG, "setRttModeForDial + isRtt: " + isRtt + " callIdString = " + callIdString);

        if (!RTT_SUPPORT) {
            return;
        }

        OpImsCommandsInterface opUtil = imsRILAdapter.getOpCommandsInterface();
        Rlog.d(TAG, "opUtil: " +  opUtil);

        /// AT+EIMSRTT = <op>
        ///0 : not a RTT call (RTT off)
        ///1 : RTT call  (RTT auto)
        ////2: enable IMS RTT capability with Upon Request RTT Operation Mode (upon request)
        ///Description: indicate RTT call type of the following ATD command
        if (isRtt) {
            Rlog.d(TAG, "setRttModeForDial setRttMode 1");
            opUtil.setRttMode(1, null);
        } else {
            Rlog.d(TAG, "setRttModeForDial setRttMode 2");
            opUtil.setRttMode(2, null);
        }
    }

    @Override
    public void checkIncomingRttCallType(Intent intent) {

        if (!RTT_SUPPORT) {
            return;
        }

        Rlog.d(TAG, "checkIncomingRttCallType incoming a RTT call = " + mIsIncomingCallRtt);

        if (mIsIncomingCallRtt == 1) {
            intent.putExtra(MtkImsManager.EXTRA_RTT_INCOMING_CALL, mIsIncomingCallRtt);
        }
    }

    @Override
    public boolean isRttEnabledForCallSession() {
        Rlog.d(TAG, "isRttEnabledForCallSession: " + mIsRttEnabledForCallSession);
        return mIsRttEnabledForCallSession;
    }
}
