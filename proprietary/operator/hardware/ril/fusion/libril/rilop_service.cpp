/*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein
* is confidential and proprietary to MediaTek Inc. and/or its licensors.
* Without the prior written permission of MediaTek inc. and/or its licensors,
* any reproduction, modification, use or disclosure of MediaTek Software,
* and information contained herein, in whole or in part, shall be strictly prohibited.
*
* MediaTek Inc. (C) 2017. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
* AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
* NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
* SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
* SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
* THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
* THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
* CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
* SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
* CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
* AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
* OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
* MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek Software")
* have been modified by MediaTek Inc. All revisions are subject to any receiver's
* applicable license agreements with MediaTek Inc.
*/

#define LOG_TAG "RILS-OP"

#include <android/hardware/radio/1.0/IRadio.h>
//#include <android/hardware/radio/deprecated/1.0/IOemHook.h>
#include <vendor/mediatek/hardware/radio_op/1.1/IRadioOp.h>

#include <hwbinder/IPCThreadState.h>
#include <hwbinder/ProcessState.h>
#include <rilop_service.h>
#include <ril_service.h>
#include <hidl/HidlTransportSupport.h>
#include <utils/SystemClock.h>
#include <inttypes.h>
#include "libmtkrilutils.h"
#include <telephony/mtk_rilop.h>
#include <telephony/mtk_ril.h>
#include "mtk_log.h"

#define INVALID_HEX_CHAR 16

using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;
using ::android::hardware::Return;
using ::android::hardware::Status;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_array;
using ::android::hardware::Void;
using android::CommandInfo;
using android::RequestInfo;
using android::requestToString;
using android::sp;

using ::android::hardware::radio::V1_0::RadioResponseType;
using ::android::hardware::radio::V1_0::RadioIndicationType;
using ::android::hardware::radio::V1_0::RadioResponseInfo;
using ::android::hardware::radio::V1_0::CardStatus;
using ::android::hardware::radio::V1_0::RadioError;
using ::android::hardware::radio::V1_0::RadioState;

using namespace vendor::mediatek::hardware::radio_op::V1_1;

#define BOOL_TO_INT(x) (x ? 1 : 0)
#define ATOI_NULL_HANDLED(x) (x ? atoi(x) : -1)
#define ATOI_NULL_HANDLED_DEF(x, defaultVal) (x ? atoi(x) : defaultVal)

RIL_RadioFunctions *s_vendorFunctions = NULL;
static CommandInfo *s_commands;

struct RadioImpl;
struct OemHookImpl;

#define MAX_SIM_COUNT 4
//#if (SIM_COUNT >= 2)
sp<RadioImpl> radioService[MAX_SIM_COUNT * DIVISION_COUNT];
// counter used for synchronization. It is incremented every time response callbacks are updated.
volatile int32_t mCounterRadio[MAX_SIM_COUNT * DIVISION_COUNT];

extern bool dispatchVoid(int serial, int slotId, int request);
extern bool dispatchInts(int serial, int slotId, int request, int countInts, ...);
extern RadioIndicationType convertIntToRadioIndicationType(int indicationType);
extern bool dispatchStrings(int serial, int slotId, int request, bool allowEmpty,
        int countStrings, ...);
extern bool dispatchString(int serial, int slotId, int request, const char * str);
extern void populateResponseInfo(RadioResponseInfo& responseInfo, int serial,
                                 int responseType, RIL_Errno e);
extern hidl_string convertCharPtrToHidlString(const char *ptr);
extern void memsetAndFreeStrings(int numPointers, ...);
extern bool copyHidlStringToRil(char **dest, const hidl_string &src, RequestInfo *pRI);


// To Compute IMS Slot Id
extern "C" int toRealSlot(int slotId);
extern "C" int isImsSlot(int slotId);
extern "C" int toImsSlot(int slotId);

struct RadioImpl : public IRadioOp {
    int32_t mSlotId;
    sp<IRadioResponseOp> mRadioResponseMtk;
    sp<IRadioIndicationOp> mRadioIndicationMtk;
    sp<IImsRadioResponseOp> mRadioResponseIms;
    sp<IImsRadioIndicationOp> mRadioIndicationIms;
    sp<IDigitsRadioResponse> mRadioResponseDigits;
    sp<IDigitsRadioIndication> mRadioIndicationDigits;
    sp<IRcsRadioResponse> mRadioResponseRcs;
    sp<IRcsRadioIndication> mRadioIndicationRcs;

    Return<void> responseAcknowledgement();

    Return<void> setResponseFunctions(
            const ::android::sp<IRadioResponseOp>& radioResponse,
            const ::android::sp<IRadioIndicationOp>& radioIndication);

    Return<void> setResponseFunctionsIms(
           const ::android::sp<IImsRadioResponseOp>& radioResponse,
           const ::android::sp<IImsRadioIndicationOp>& radioIndication);

    Return<void> setResponseFunctionsDigits(
           const ::android::sp<IDigitsRadioResponse>& radioResponse,
           const ::android::sp<IDigitsRadioIndication>& radioIndication);

    Return<void> setRttMode(int serial, int mode);

    Return<void> sendRttModifyRequest(int serial, int callId, int newMode);

    Return<void> sendRttText(int serial, int callId, int lenOfString,
                             const hidl_string& text);

    Return<void> rttModifyRequestResponse(int serial, int callId, int result);

    Return<void> setDigitsLine(int serial, int accountId,
                               int digitsSerial, bool isLogout,
                               bool hasNext, bool isNative,
                               const ::android::hardware::hidl_string& msisdn,
                               const ::android::hardware::hidl_string& sit);

    Return<void> setTrn(int serial,
                        const ::android::hardware::hidl_string& fromMsisdn,
                        const ::android::hardware::hidl_string& toMsisdn,
                        const ::android::hardware::hidl_string& trn);

    Return<void> setIncomingVirtualLine(int serial,
                        const ::android::hardware::hidl_string& fromMsisdn,
                        const ::android::hardware::hidl_string& toMsisdn);

    Return<void> dialFrom(int serial, const DialFrom& dialInfo);

    Return<void> sendUssiFrom(int serial, const hidl_string& from, int action,
                             const hidl_string& ussi);

    Return<void> cancelUssiFrom(int serial, const hidl_string& from);

    Return<void> setEmergencyCallConfig(int serial, int category, bool isForceEcc);

    Return<void> deviceSwitch(int serial,
                              const ::android::hardware::hidl_string& number,
                              const ::android::hardware::hidl_string& deviceId);

    Return<void> cancelDeviceSwitch(int serial);

    void checkReturnStatus(Return<void>& ret);

    Return<void> setRxTestConfig(int32_t serial, int32_t antType);
    Return<void> getRxTestResult(int32_t serial, int32_t mode);

    Return<void> setDisable2G(int32_t serial, bool mode);
    Return<void> getDisable2G(int32_t serial);

    Return<void> setResponseFunctionsRcs(
        const ::android::sp<IRcsRadioResponse>& radioResponse,
        const ::android::sp<IRcsRadioIndication>& radioIndication);

    Return<void> setDigitsRegStatus(int serial,
                                    const ::android::hardware::hidl_string& digitsinfo);
};

void checkReturnStatus(int32_t slotId, Return<void>& ret, bool isRadioService) {
    if (ret.isOk() == false) {
        mtkLogE(LOG_TAG, "checkReturnStatus: unable to call response/indication callback");
        // Remote process hosting the callbacks must be dead. Reset the callback objects;
        // there's no other recovery to be done here. When the client process is back up, it will
        // call setResponseFunctions()

        // Caller should already hold rdlock, release that first
        // note the current counter to avoid overwriting updates made by another thread before
        // write lock is acquired.
        int counter = mCounterRadio[slotId];
        pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(slotId);
        int ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // acquire wrlock
        ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // make sure the counter value has not changed
        if (counter == mCounterRadio[slotId]) {
            radioService[slotId]->mRadioResponseMtk = NULL;
            radioService[slotId]->mRadioIndicationMtk = NULL;
            mCounterRadio[slotId]++;
        } else {
            mtkLogE(LOG_TAG, "checkReturnStatus: not resetting responseFunctions as they likely "
                    "got updated on another thread");
        }

        // release wrlock
        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // Reacquire rdlock
        ret = pthread_rwlock_rdlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }
}

void RadioImpl::checkReturnStatus(Return<void>& ret) {
    ::checkReturnStatus(mSlotId, ret, true);
}

Return<void> RadioImpl::responseAcknowledgement() {
    android::releaseWakeLock();
    return Void();
}

void populateResponseInfo(RadioResponseInfo& responseInfo, int serial, int responseType,
                         RIL_Errno e) {
    responseInfo.serial = serial;
    switch (responseType) {
        case RESPONSE_SOLICITED:
            responseInfo.type = RadioResponseType::SOLICITED;
            break;
        case RESPONSE_SOLICITED_ACK_EXP:
            responseInfo.type = RadioResponseType::SOLICITED_ACK_EXP;
            break;
    }
    responseInfo.error = (RadioError) e;
}

Return<void> RadioImpl::setResponseFunctions(
        const ::android::sp<IRadioResponseOp>& radioResponseParam,
        const ::android::sp<IRadioIndicationOp>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctions");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseMtk = radioResponseParam;
    mRadioIndicationMtk = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsIms(
        const ::android::sp<IImsRadioResponseOp>& radioResponseParam,
        const ::android::sp<IImsRadioIndicationOp>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctionsIms");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseIms = radioResponseParam;
    mRadioIndicationIms = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsDigits(
           const ::android::sp<IDigitsRadioResponse>& radioResponseParam,
           const ::android::sp<IDigitsRadioIndication>& radioIndicationParam) {

    mtkLogD(LOG_TAG, "setResponseFunctionsDigits");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseDigits = radioResponseParam;
    mRadioIndicationDigits = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

int responseInt(RadioResponseInfo& responseInfo, int serial, int responseType, RIL_Errno e,
               void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    int ret = -1;

    if (response == NULL || responseLen != sizeof(int)) {
        mtkLogE(LOG_TAG, "responseInt: Invalid response");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
    } else {
        int *p_int = (int *) response;
        ret = p_int[0];
    }
    return ret;
}

Return<void> RadioImpl::setRxTestConfig(int32_t serial, int32_t antType) {
    mtkLogD(LOG_TAG, "setRxTestConfig: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_VSS_ANTENNA_CONF, 1, antType);
    return Void();
}

Return<void> RadioImpl::getRxTestResult(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "getRxTestResult: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_VSS_ANTENNA_INFO, 1, mode);
    return Void();
}

/* RCS over Internet PDN & DIGITS */
Return<void> RadioImpl::setResponseFunctionsRcs(
        const ::android::sp<IRcsRadioResponse>& radioResponseParam,
        const ::android::sp<IRcsRadioIndication>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctionsRcs");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseRcs = radioResponseParam;
    mRadioIndicationRcs = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}
/* RCS over Internet PDN & DIGITS */
Return<void> RadioImpl::setDigitsRegStatus(int serial,
                            const ::android::hardware::hidl_string& digitsinfo) {

    mtkLogD(LOG_TAG, "setDigitsRegStatus - %s", digitsinfo.c_str());

    dispatchString(serial, mSlotId, RIL_REQUEST_SET_DIGITS_REG_STATUS, digitsinfo.c_str());
    return Void();
}

/*********************************************************************************/
/*  Vendor Command                                                               */
/*********************************************************************************/
Return<void> RadioImpl::setRttMode(int serial, int mode) {
    mtkLogD(LOG_TAG, "setRttMode: serial %d", serial);

    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_RTT_MODE, 1, mode);
    return Void();
}

Return<void> RadioImpl::sendRttModifyRequest(int serial, int callId, int newMode) {
    mtkLogD(LOG_TAG, "sendRttModifyRequest: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SEND_RTT_MODIFY_REQUEST, 2,
                 callId, newMode);

    return Void();
}

Return<void> RadioImpl::sendRttText(int serial, int callId, int lenOfString,
                         const hidl_string& text) {
    mtkLogD(LOG_TAG, "sendRttText: serial %d", serial);
    hidl_string strCallId = std::to_string(callId);
    hidl_string strlenOfString = std::to_string(lenOfString);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_RTT_TEXT, false, 3,
                    strCallId.c_str(), strlenOfString.c_str(), text.c_str());

    return Void();
}

Return<void> RadioImpl::rttModifyRequestResponse(int serial, int callId, int result) {
    mtkLogD(LOG_TAG, "rttModifyRequestResponse: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RTT_MODIFY_REQUST_RESPONSE, 2,
                 callId, result);
    return Void();
}

Return<void> RadioImpl::setDigitsLine(int serial, int accountId,
                                      int digitsSerial, bool isLogout,
                                      bool hasNext, bool isNative,
                                      const ::android::hardware::hidl_string& msisdn,
                                      const ::android::hardware::hidl_string& sit) {
    mtkLogD(LOG_TAG, "setDigitsLine: serial %d", serial);

    hidl_string strAccountId = std::to_string(accountId);
    hidl_string strDigitsSerial = std::to_string(digitsSerial);
    hidl_string strIsLogout = std::to_string(isLogout);
    hidl_string strHasNext = std::to_string(hasNext);
    hidl_string strIsNative = std::to_string(isNative);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_DIGITS_LINE, true, 7,
                    strAccountId.c_str(), strDigitsSerial.c_str(), strIsLogout.c_str(),
                    strHasNext.c_str(), strIsNative.c_str(), msisdn.c_str(), sit.c_str());

    return Void();
}

Return<void> RadioImpl::setTrn(int serial,
                               const ::android::hardware::hidl_string& fromMsisdn,
                               const ::android::hardware::hidl_string& toMsisdn,
                               const ::android::hardware::hidl_string& trn) {

    mtkLogD(LOG_TAG, "setTrn: serial %d", serial);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_TRN, true, 3,
                    fromMsisdn.c_str(), toMsisdn.c_str(), trn.c_str());

    return Void();
}

Return<void> RadioImpl::setIncomingVirtualLine(int serial,
                               const ::android::hardware::hidl_string& fromMsisdn,
                               const ::android::hardware::hidl_string& toMsisdn) {
    mtkLogD(LOG_TAG, "setIncomingVirtualLine: serial %d", serial);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_INCOMING_VIRTUAL_LINE, true, 2,
            fromMsisdn.c_str(), toMsisdn.c_str());

    return Void();
}

Return<void> RadioImpl::dialFrom(int32_t serial, const DialFrom& dialInfo) {
    mtkLogD(LOG_TAG, "dialFrom: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_DIAL_FROM);
    if (pRI == NULL) {
        return Void();
    }
    RIL_DialFrom dial = {};
    int32_t sizeOfDial = sizeof(dial);
    if (!copyHidlStringToRil(&dial.address, dialInfo.address, pRI)) {
        return Void();
    }
    if (!copyHidlStringToRil(&dial.fromAddress, dialInfo.fromAddress, pRI)) {
        return Void();
    }
    dial.clir = (int) dialInfo.clir;
    dial.isVideoCall= (bool) dialInfo.isVideoCall;
    s_vendorFunctions->onRequest(RIL_REQUEST_DIAL_FROM, &dial, sizeOfDial, pRI, pRI->socket_id);
    memsetAndFreeStrings(2, dial.address, dial.fromAddress);
    return Void();
}

Return<void> RadioImpl::sendUssiFrom(int serial, const hidl_string& from, int action,
                         const hidl_string& ussi) {
    mtkLogD(LOG_TAG, "sendUssiFrom: serial %d", serial);
    hidl_string strAction = std::to_string(action);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_USSI_FROM, false, 3,
                    from.c_str(), strAction.c_str(), ussi.c_str());

    return Void();
}

Return<void> RadioImpl::cancelUssiFrom(int serial, const hidl_string& from) {
    mtkLogD(LOG_TAG, "cancelUssiFrom: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_CANCEL_USSI_FROM, from.c_str());

    return Void();
}

Return<void> RadioImpl::setEmergencyCallConfig(int serial, int category, bool isForceEcc) {
    mtkLogD(LOG_TAG, "setEmergencyCallConfig: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_EMERGENCY_CALL_CONFIG, 2,
                 category, (int)isForceEcc);
    return Void();
}

Return<void> RadioImpl::getDisable2G(int serial) {
    mtkLogD(LOG_TAG, "getDisable2G: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_DISABLE_2G);
    return Void();
}

Return<void> RadioImpl::setDisable2G(int serial, bool mode) {
    mtkLogD(LOG_TAG, "setDisable2G: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_DISABLE_2G, 1, BOOL_TO_INT(mode));
    return Void();
}

Return<void> RadioImpl::deviceSwitch(int serial,
                                     const ::android::hardware::hidl_string& number,
                                     const ::android::hardware::hidl_string& deviceId) {
    mtkLogD(LOG_TAG, "deviceSwitch: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_DEVICE_SWITCH, false, 2,
                    number.c_str(), deviceId.c_str());
    return Void();
}

Return<void> RadioImpl::cancelDeviceSwitch(int serial) {
    mtkLogD(LOG_TAG, "cancelDeviceSwitch: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CANCEL_DEVICE_SWITCH);
    return Void();
}

/*********************************************************************************/
/*  Response of Vendor Solicated Command                                         */
/*********************************************************************************/
int radio::setRttModeResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "setRttModeResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setRttModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRttModeResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::sendRttModifyRequestResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "sendRttModifyRequestResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 sendRttModifyRequestResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendRttModifyRequestResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                  slotId);
    }

    return 0;
}

int radio::sendRttTextResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "sendRttTextResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 sendRttTextResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendRttTextResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::rttModifyRequestResponseResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "rttModifyRequestResponseResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setRttModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "rttModifyRequestResponseResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                      slotId);
    }

    return 0;
}

int radio::setDigitsLineResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "setDigitsLineResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseDigits != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseDigits->
                                 setDigitsLineResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setDigitsLineResponse: radioService[%d]->mRadioResponseDigits == NULL",
                                                                              slotId);
    }

    return 0;
}

int radio::setRxTestConfigResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setRxTestConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> respAntConf;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "setRxTestConfigResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            respAntConf.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                respAntConf[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setRxTestConfigResponse(responseInfo,
                respAntConf);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRxTestConfigResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::getRxTestResultResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getRxTestResultResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> respAntInfo;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "getRxTestResultResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            respAntInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                respAntInfo[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getRxTestResultResponse(responseInfo,
                respAntInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getRxTestResultResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::setTrnResponse(int slotId,
                          int responseType, int serial, RIL_Errno e,
                          void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "setTrnResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseDigits != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseDigits->
                                 setTrnResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setTrnResponse: radioService[%d]->mRadioResponseDigits == NULL",
                                                                       slotId);
    }

    return 0;
}

int radio::setIncomingVirtualLineResponse(int slotId,
                          int responseType, int serial, RIL_Errno e,
                          void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setIncomingVirtualLineResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseMtk->
                                 setIncomingVirtualLineResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setIncomingVirtualLineResponse: radioService[%d]->mRadioResponseMtk == NULL",
                                                                       slotId);
    }

    return 0;
}

int radio::dialFromResponse(int slotId,
                int responseType, int serial, RIL_Errno e, void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "dialFromResponse: serial %d", serial);
    return 0;
}

int radio::sendUssiFromResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "sendUssiFromResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 sendUssiFromResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendUssiFromResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::cancelUssiFromResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "cancelUssiFromResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 cancelUssiFromResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cancelUssiFromResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::setEmergencyCallConfigResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "setEmergencyCallConfigResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setEmergencyCallConfigResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setEmergencyCallConfigResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::setDisable2GResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setDisable2GResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setDisable2GResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setDisable2GResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }
    return 0;
}

int radio::getDisable2GResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getDisable2GResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getDisable2GResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getDisable2GResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }
    return 0;
}

int radio::deviceSwitchResponse(int slotId,
                        int responseType, int serial, RIL_Errno e,
                        void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "deviceSwtichResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 deviceSwitchResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "deviceSwtichResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::cancelDeviceSwitchResponse(int slotId,
                        int responseType, int serial, RIL_Errno e,
                        void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "cancelDeviceSwtichResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 cancelDeviceSwitchResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cancelDeviceSwtichResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::setDigitsRegStatuseResponse(int slotId,
                          int responseType, int serial, RIL_Errno e,
                          void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "setDigitsRegStatuseResponse: serial %d", serial);
    return 0;
}


/*********************************************************************************/
/*  Vendor Unsolicated Command                                                   */
/*********************************************************************************/
int radio::rttModifyResponseInd(int slotId,
                               int indicationType, int token, RIL_Errno e,
                               void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 2) {
            mtkLogE(LOG_TAG, "rttModifyResponseInd: items length invalid, slotId = %d",
                                                                 imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int result = ((int32_t *) resp)[1];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttModifyResponse(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, result);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "rttModifyResponseInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                         imsSlotId);
    }

    return 0;
}

int radio::rttTextReceiveInd(int slotId,
                             int indicationType, int token, RIL_Errno e,
                             void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        if(numStrings < 3) {
            mtkLogE(LOG_TAG, "rttTextReceiveInd: items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        int callId = atoi(resp[0]);
        int length = atoi(resp[1]);
        hidl_string text = convertCharPtrToHidlString(resp[2]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttTextReceive(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, length, text);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "rttTextReceiveInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                      imsSlotId);
    }

    return 0;
}

int radio::gttCapabilityIndicationInd(int slotId,
                                      int indicationType, int token, RIL_Errno e,
                                      void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 5) {
            mtkLogE(LOG_TAG, "gttCapabilityIndicationInd: items length invalid, slotId = %d",
                                                                       imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int localCap = ((int32_t *) resp)[1];
        int remoteCap = ((int32_t *) resp)[2];
        int localStatus = ((int32_t *) resp)[3];
        int remoteStatus = ((int32_t *) resp)[4];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->gttCapabilityIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, localCap, remoteCap, localStatus, remoteStatus);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "gttCapabilityIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                               imsSlotId);
    }

    return 0;
}

int radio::rttModifyRequestReceiveInd(int slotId,
                                      int indicationType, int token, RIL_Errno e,
                                      void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 2) {
            mtkLogE(LOG_TAG, "rttModifyRequestReceiveInd: items length invalid, slotId = %d",
                                                                       imsSlotId);
            return 0;
        }

        int callId = ((int32_t *) resp)[0];
        int type = ((int32_t *) resp)[1];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->rttModifyRequestReceive(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, type);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "rttModifyRequestReceiveInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                               imsSlotId);
    }

    return 0;
}

int radio::digitsLineIndicationInd(int slotId,
                                   int indicationType, int token, RIL_Errno e,
                                   void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationDigits != NULL) {

        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        if(numStrings < 23) {
            mtkLogE(LOG_TAG, "digitsLineIndicationInd: items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        int accountId = atoi(resp[0]);
        int serial = atoi(resp[1]);
        int msisdnNum = atoi(resp[2]);

        hidl_vec<hidl_string> msisdns;
        msisdns.resize(10);

        hidl_vec<bool> isActive;
        isActive.resize(10);

        int index, respIdx = 3;
        for (index = 0; index < 10; index ++) {
            msisdns[index] = convertCharPtrToHidlString(resp[respIdx ++]);
            isActive[index] = atoi(resp[respIdx ++]);
        }

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationDigits->digitsLineIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 accountId, serial, msisdnNum, msisdns, isActive);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "digitsLineIndicationInd: radioService[%d]->mRadioIndicationDigits == NULL",
                                                                               imsSlotId);
    }

    return 0;
}

int radio::getTrnIndicationInd(int slotId,
                               int indicationType, int token, RIL_Errno e,
                               void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationDigits != NULL) {

        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        if(numStrings < 2) {
            mtkLogE(LOG_TAG, "getTrnIndicationInd: items length invalid, slotId = %d",
                                                              imsSlotId);
            return 0;
        }

        hidl_string fromMsisdn = convertCharPtrToHidlString(resp[0]);
        hidl_string toMsisdn = convertCharPtrToHidlString(resp[1]);


        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationDigits->getTrnIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 fromMsisdn, toMsisdn);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "getTrnIndicationInd: radioService[%d]->mRadioIndicationDigits == NULL",
                                                                          imsSlotId);
    }

    return 0;
}

int radio::responseModulationInfoInd(int slotId,
                                           int indicationType, int token, RIL_Errno e,
                                           void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "responseModulationInfoInd Invalid response: NULL");
            return 0;
        }
        mtkLogD(LOG_TAG, "responseModulationInfoInd");
        hidl_vec<int32_t> data;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseModulationInfoInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseModulationInfoInd: radioService[%d]->responseModulationInfoInd == NULL", slotId);
    }
    return 0;
}

extern "C" void radio::registerOpService(RIL_RadioFunctions *callbacks,
                                         android::CommandInfo *commands) {

    using namespace android::hardware;
    int simCount = 1;
    const char *serviceNames[] = {
            android::RIL_getServiceName()
            , RIL2_SERVICE_NAME
            , RIL3_SERVICE_NAME
            , RIL4_SERVICE_NAME
            };
    const char *imsServiceNames[] = {
            IMS_WWOP_RIL1_SERVICE_NAME
            , IMS_WWOP_RIL2_SERVICE_NAME
            , IMS_WWOP_RIL3_SERVICE_NAME
            , IMS_WWOP_RIL4_SERVICE_NAME
            };

    simCount = getSimCount();
    configureRpcThreadpool(1, true /* callerWillJoin */);
    for (int i = 0; i < simCount; i++) {
        pthread_rwlock_t *radioServiceRwlockPtr = getRadioServiceRwlock(i);
        int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        radioService[i] = new RadioImpl;
        radioService[i]->mSlotId = i;
        mtkLogI(LOG_TAG, "registerOpService: starting IRadioOp %s", serviceNames[i]);
        android::status_t status = radioService[i]->registerAsService(serviceNames[i]);

        int imsSlot = toImsSlot(i);
        radioService[imsSlot] = new RadioImpl;
        radioService[imsSlot]->mSlotId = imsSlot;
        mtkLogD(LOG_TAG, "radio::registerService: starting IMS IRadioOp %s, slot = %d, realSlot = %d",
              imsServiceNames[i], radioService[imsSlot]->mSlotId, imsSlot);

        // Register IMS Radio Stub
        status = radioService[imsSlot]->registerAsService(imsServiceNames[i]);
        mtkLogD(LOG_TAG, "radio::registerService IRadio for IMS status:%d", status);
        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }

    s_vendorFunctions = callbacks;
    s_commands = commands;
}

// MTK-START: SIM TMO RSU
int radio::onSimMeLockEvent(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        int32_t eventId = ((int32_t *) response)[0];

        mtkLogD(LOG_TAG, "onSimMeLockEvent: eventId %d", eventId);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->
                onSimMeLockEvent(convertIntToRadioIndicationType(indicationType), eventId);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimMeLockEvent: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// MTK-END

int radio::imsVoPSIndication(int slotId,
                                int indicationType, int token, RIL_Errno e,
                                void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationRcs != NULL) {

        int *resp = (int *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 1) {
            mtkLogE(LOG_TAG, "imsVoPSIndication: items length invalid, slotId = %d",imsSlotId);
            return 0;
        }

        int vops = ((int32_t *) resp)[0];

        mtkLogD(LOG_TAG, "imsVoPSIndication - vops = %d", vops);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationRcs->imsVoPSIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 vops);

        radioService[imsSlotId]->checkReturnStatus(retStatus);

    } else {
        mtkLogE(LOG_TAG, "imsVoPSIndication: radioService[%d]->mRadioIndicationIms == NULL", imsSlotId);
    }

    return 0;
}


int radio::digitsLineIndication(int slotId,
                               int indicationType, int token, RIL_Errno e,
                               void *response, size_t responselen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationRcs != NULL) {

        char *resp = (char *) response;
        int numStrings = responselen / sizeof(int);
        if(numStrings < 1) {
            mtkLogE(LOG_TAG, "digitsLineIndication: items length invalid, slotId = %d",imsSlotId);
            return 0;
        }

        mtkLogD(LOG_TAG, "digitsLineIndication: %s", resp);
        hidl_string cmdline = convertCharPtrToHidlString(resp);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationRcs->digitsLineIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 cmdline);

        radioService[imsSlotId]->checkReturnStatus(retStatus);

    } else {
        mtkLogE(LOG_TAG, "digitsLineIndication: radioService[%d]->mRadioIndicationIms == NULL", imsSlotId);
    }

    return 0;
}

