/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
#ifndef _CAMERA_CUSTOM_MFLL_H_
#define _CAMERA_CUSTOM_MFLL_H_

/**************************************************************************
 *                      D E F I N E S / M A C R O S                       *
 **************************************************************************/
// For MFLL Customer Parameters
//
// [SW Workaround Solution]
//     - When CUST_MFLL_ENABLE_SWWKN_FIX_MIX==0,
//          Use software Workaround for hardware MIX3.
//          Some hardware "MIXER3" may have hardware limitation, we have to perform
//          this SW Workaround solution but performance may be slow down.
//          (12M Pixel costs 70~80 ms)
//     - When CUST_MFLL_ENABLE_SWWKN_FIX_MIX==1,
//          Do not use software Workaround for hardware MIX3.
//          If the software Workaround is disabled, the image quality may have
//          a little problem. However, MIX3 problem is not in all chips.
#define CUST_MFLL_ENABLE_SWWKN_FIX_MIX  0

//
// [Content-Aware AIS]
//     CUST_MFLL_ENABLE_CONTENT_AWARE_AIS is a bitwise flag for enable/disable
//     content-aware AIS by sensor index. (still experimental, not suggest to enable)
//     - When the i-th bits of CUST_MFLL_ENABLE_CONTENT_AWARE_AIS==0,
//          Disable content-aware AIS at the i-th sensor,
//
//     - When the i-th bits of CUST_MFLL_ENABLE_CONTENT_AWARE_AIS==1,
//          Enable content-aware AIS at the i-th sensor.
#define CUST_MFLL_ENABLE_CONTENT_AWARE_AIS 0

// [Best Shot Selection (BSS)]
//     - When CUST_MFLL_ENABLE_BSS_FOR_MFLL==0,
//          Use the first image as base image for blending.
//          The advantage is shutter lag has been minimized.
//          The drawback is final image has higher motion blur (cause by handshack)possibility .
//     - When CUST_MFLL_ENABLE_BSS_FOR_MFLL==1 (recommended),
//          Use the image with the highest sharpness as base image.
//          The advantage is the motion blur (cause by handshack) has been minimized.
//          The drawback is a longer shutter lag (average ~+100ms)
#define CUST_MFLL_ENABLE_BSS_FOR_MFLL   1

//     - When CUST_MFLL_ENABLE_BSS_FOR_AIS==0,
//          Use the first image as base image for blending.
//          The advantage is shutter lag has been minimized.
//          The drawback is final image has higher motion blur (cause by handshack)possibility .
//     - When CUST_MFLL_ENABLE_BSS_FOR_AIS==1 (recommended),
//          Use the image with the highest sharpness as base image.
//          The advantage is the motion blur (cause by handshack) has been minimized.
//          The drawback is a longer shutter lag (average ~+100ms)
#define CUST_MFLL_ENABLE_BSS_FOR_AIS    1

//     - how many rows are skipped during processing
//          recommand range: >=8
//          recommand value: 8
//          larger scale factor cause less accurate but faster execution time.
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_SCALE_FACTOR      8
#define CUST_MFLL_BSS_SCALE_FACTOR_00   8
#define CUST_MFLL_BSS_SCALE_FACTOR_01   8
#define CUST_MFLL_BSS_SCALE_FACTOR_02   8
#define CUST_MFLL_BSS_SCALE_FACTOR_03   8

//     - minimum edge response
//          recommand range: 10~40
//          recommand value: 20
//          larger th0 cause better noise resistence but may miss real edges.
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_CLIP_TH0          10
#define CUST_MFLL_BSS_CLIP_TH0_00       10
#define CUST_MFLL_BSS_CLIP_TH0_01       10
#define CUST_MFLL_BSS_CLIP_TH0_02       10
#define CUST_MFLL_BSS_CLIP_TH0_03       10

//     - maximum edge response
//          recommand range: 50~120
//          recommand value: 100
//          larger th1 will suppress less high contrast edges
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_CLIP_TH1          20
#define CUST_MFLL_BSS_CLIP_TH1_00       20
#define CUST_MFLL_BSS_CLIP_TH1_01       20
#define CUST_MFLL_BSS_CLIP_TH1_02       20
#define CUST_MFLL_BSS_CLIP_TH1_03       20

//     - minimum edge response
//          recommand range: 10~40
//          recommand value: 20
//          larger th0 cause better noise resistence but may miss real edges.
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_CLIP_TH2          10
#define CUST_MFLL_BSS_CLIP_TH2_00       10
#define CUST_MFLL_BSS_CLIP_TH2_01       10
#define CUST_MFLL_BSS_CLIP_TH2_02       10
#define CUST_MFLL_BSS_CLIP_TH2_03       10

//     - maximum edge response
//          recommand range: 50~120
//          recommand value: 100
//          larger th1 will suppress less high contrast edges
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_CLIP_TH3          20
#define CUST_MFLL_BSS_CLIP_TH3_00       20
#define CUST_MFLL_BSS_CLIP_TH3_01       20
#define CUST_MFLL_BSS_CLIP_TH3_02       20
#define CUST_MFLL_BSS_CLIP_TH3_03       20

//     - tri-pod/static scene detection
//          recommand range: 0~10
//          recommand value: 10
//          larger zero cause more scene will be considered as static
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_ZERO              12
#define CUST_MFLL_BSS_ZERO_00           12
#define CUST_MFLL_BSS_ZERO_01           12
#define CUST_MFLL_BSS_ZERO_02           12
#define CUST_MFLL_BSS_ZERO_03           12

//     - brightness difference threshold
//          recommand range: 0~31
//          recommand value: 12
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_ADF_TH            18
#define CUST_MFLL_BSS_ADF_TH_00         18
#define CUST_MFLL_BSS_ADF_TH_01         18
#define CUST_MFLL_BSS_ADF_TH_02         18
#define CUST_MFLL_BSS_ADF_TH_03         18

//     - sharpness difference threshold
//          recommand range: 0~90
//          recommand value: 70
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_SDF_TH            80
#define CUST_MFLL_BSS_SDF_TH_00         80
#define CUST_MFLL_BSS_SDF_TH_01         80
#define CUST_MFLL_BSS_SDF_TH_02         80
#define CUST_MFLL_BSS_SDF_TH_03         80

//     - minimum gain difference threshold
//          recommand range:
//          recommand value: 853
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_GAIN_TH0          853
#define CUST_MFLL_BSS_GAIN_TH0_00       853
#define CUST_MFLL_BSS_GAIN_TH0_01       853
#define CUST_MFLL_BSS_GAIN_TH0_02       853
#define CUST_MFLL_BSS_GAIN_TH0_03       853

//     - maximum gain difference threshold
//          recommand range:
//          recommand value: 1229
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_GAIN_TH1          1229
#define CUST_MFLL_BSS_GAIN_TH1_00       1229
#define CUST_MFLL_BSS_GAIN_TH1_01       1229
#define CUST_MFLL_BSS_GAIN_TH1_02       1229
#define CUST_MFLL_BSS_GAIN_TH1_03       1229

//     - minimum isp gain
//          recommand range:
//          recommand value: 1024
//     - CUST_MFLL_BSS_xxx + (_00, _01, _02, _03) for each sensor id (0, 1, 2, 3).
#define CUST_MFLL_BSS_MIN_ISP_GAIN_     546
#define CUST_MFLL_BSS_MIN_ISP_GAIN_00   546
#define CUST_MFLL_BSS_MIN_ISP_GAIN_01   546
#define CUST_MFLL_BSS_MIN_ISP_GAIN_02   546
#define CUST_MFLL_BSS_MIN_ISP_GAIN_03   546

#define CUST_MFLL_BSS_YPF_EN            1
#define CUST_MFLL_BSS_YPF_EN_00         1
#define CUST_MFLL_BSS_YPF_EN_01         1
#define CUST_MFLL_BSS_YPF_EN_02         1
#define CUST_MFLL_BSS_YPF_EN_03         1

#define CUST_MFLL_BSS_YPF_FAC           50
#define CUST_MFLL_BSS_YPF_FAC_00        50
#define CUST_MFLL_BSS_YPF_FAC_01        50
#define CUST_MFLL_BSS_YPF_FAC_02        50
#define CUST_MFLL_BSS_YPF_FAC_03        50

#define CUST_MFLL_BSS_YPF_ADJTH         12
#define CUST_MFLL_BSS_YPF_ADJTH_00      12
#define CUST_MFLL_BSS_YPF_ADJTH_01      12
#define CUST_MFLL_BSS_YPF_ADJTH_02      12
#define CUST_MFLL_BSS_YPF_ADJTH_03      12

#define CUST_MFLL_BSS_YPF_DFMED0        20
#define CUST_MFLL_BSS_YPF_DFMED0_00     20
#define CUST_MFLL_BSS_YPF_DFMED0_01     20
#define CUST_MFLL_BSS_YPF_DFMED0_02     20
#define CUST_MFLL_BSS_YPF_DFMED0_03     20

#define CUST_MFLL_BSS_YPF_DFMED1        32
#define CUST_MFLL_BSS_YPF_DFMED1_00     32
#define CUST_MFLL_BSS_YPF_DFMED1_01     32
#define CUST_MFLL_BSS_YPF_DFMED1_02     32
#define CUST_MFLL_BSS_YPF_DFMED1_03     32

#define CUST_MFLL_BSS_YPF_TH0           102
#define CUST_MFLL_BSS_YPF_TH0_00        102
#define CUST_MFLL_BSS_YPF_TH0_01        102
#define CUST_MFLL_BSS_YPF_TH0_02        102
#define CUST_MFLL_BSS_YPF_TH0_03        102

#define CUST_MFLL_BSS_YPF_TH1           104
#define CUST_MFLL_BSS_YPF_TH1_00        104
#define CUST_MFLL_BSS_YPF_TH1_01        104
#define CUST_MFLL_BSS_YPF_TH1_02        104
#define CUST_MFLL_BSS_YPF_TH1_03        104

#define CUST_MFLL_BSS_YPF_TH2           98
#define CUST_MFLL_BSS_YPF_TH2_00        98
#define CUST_MFLL_BSS_YPF_TH2_01        98
#define CUST_MFLL_BSS_YPF_TH2_02        98
#define CUST_MFLL_BSS_YPF_TH2_03        98

#define CUST_MFLL_BSS_YPF_TH3           96
#define CUST_MFLL_BSS_YPF_TH3_00        96
#define CUST_MFLL_BSS_YPF_TH3_01        96
#define CUST_MFLL_BSS_YPF_TH3_02        96
#define CUST_MFLL_BSS_YPF_TH3_03        96

#define CUST_MFLL_BSS_YPF_TH4           96
#define CUST_MFLL_BSS_YPF_TH4_00        96
#define CUST_MFLL_BSS_YPF_TH4_01        96
#define CUST_MFLL_BSS_YPF_TH4_02        96
#define CUST_MFLL_BSS_YPF_TH4_03        96

#define CUST_MFLL_BSS_YPF_TH5           96
#define CUST_MFLL_BSS_YPF_TH5_00        96
#define CUST_MFLL_BSS_YPF_TH5_01        96
#define CUST_MFLL_BSS_YPF_TH5_02        96
#define CUST_MFLL_BSS_YPF_TH5_03        96

#define CUST_MFLL_BSS_YPF_TH6           96
#define CUST_MFLL_BSS_YPF_TH6_00        96
#define CUST_MFLL_BSS_YPF_TH6_01        96
#define CUST_MFLL_BSS_YPF_TH6_02        96
#define CUST_MFLL_BSS_YPF_TH6_03        96

#define CUST_MFLL_BSS_YPF_TH7           96
#define CUST_MFLL_BSS_YPF_TH7_00        96
#define CUST_MFLL_BSS_YPF_TH7_01        96
#define CUST_MFLL_BSS_YPF_TH7_02        96
#define CUST_MFLL_BSS_YPF_TH7_03        96

#define CUST_MFLL_BSS_FD_EN             1
#define CUST_MFLL_BSS_FD_EN_00          1
#define CUST_MFLL_BSS_FD_EN_01          1
#define CUST_MFLL_BSS_FD_EN_02          1
#define CUST_MFLL_BSS_FD_EN_03          1

#define CUST_MFLL_BSS_FD_FAC            2
#define CUST_MFLL_BSS_FD_FAC_00         2
#define CUST_MFLL_BSS_FD_FAC_01         2
#define CUST_MFLL_BSS_FD_FAC_02         2
#define CUST_MFLL_BSS_FD_FAC_03         2

#define CUST_MFLL_BSS_FD_FNUM           1
#define CUST_MFLL_BSS_FD_FNUM_00        1
#define CUST_MFLL_BSS_FD_FNUM_01        1
#define CUST_MFLL_BSS_FD_FNUM_02        1
#define CUST_MFLL_BSS_FD_FNUM_03        1

#define CUST_MFLL_BSS_EYE_EN            1
#define CUST_MFLL_BSS_EYE_EN_00         1
#define CUST_MFLL_BSS_EYE_EN_01         1
#define CUST_MFLL_BSS_EYE_EN_02         1
#define CUST_MFLL_BSS_EYE_EN_03         1

#define CUST_MFLL_BSS_EYE_CFTH          60
#define CUST_MFLL_BSS_EYE_CFTH_00       60
#define CUST_MFLL_BSS_EYE_CFTH_01       60
#define CUST_MFLL_BSS_EYE_CFTH_02       60
#define CUST_MFLL_BSS_EYE_CFTH_03       60

#define CUST_MFLL_BSS_EYE_RATIO0        60
#define CUST_MFLL_BSS_EYE_RATIO0_00     60
#define CUST_MFLL_BSS_EYE_RATIO0_01     60
#define CUST_MFLL_BSS_EYE_RATIO0_02     60
#define CUST_MFLL_BSS_EYE_RATIO0_03     60

#define CUST_MFLL_BSS_EYE_RATIO1        40
#define CUST_MFLL_BSS_EYE_RATIO1_00     40
#define CUST_MFLL_BSS_EYE_RATIO1_01     40
#define CUST_MFLL_BSS_EYE_RATIO1_02     40
#define CUST_MFLL_BSS_EYE_RATIO1_03     40

#define CUST_MFLL_BSS_EYE_FAC           50
#define CUST_MFLL_BSS_EYE_FAC_00        50
#define CUST_MFLL_BSS_EYE_FAC_01        50
#define CUST_MFLL_BSS_EYE_FAC_02        50
#define CUST_MFLL_BSS_EYE_FAC_03        50

//     - config MFLL mode when App trigger auto mode
//          recommand range: 0~2 (0:disable, 1:MFNR, 2:AIS)
//          recommand value: 1 or 2
#define CUST_MFLL_AUTO_MODE             1

#endif  // _CAMERA_CUSTOM_MFLL_H_
