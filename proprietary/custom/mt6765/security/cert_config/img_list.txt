[single_bin]
logo.bin=logo
lk.img=lk
boot.img=boot
recovery.img=recovery
spmfw.img=spmfw
sspm.img=tinysys-sspm
odmdtbo.img=dtbo
dtbo.img=dtbo
recovery-ramdisk.img=recovery-ramdisk
recovery-vendor.img=recovery-vendor

[multi_bin]
tee.img=atf,atf_dram,tee
scp.img=tinysys-loader-CM4_A,tinysys-scp-CM4_A,tinysys-scp-CM4_A_dram
md1img.img=md1rom,md1dsp,md1drdi
