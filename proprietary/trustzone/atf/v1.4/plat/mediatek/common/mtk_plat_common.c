/*
 * Copyright (c) 2016, ARM Limited and Contributors. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <arch_helpers.h>
#include <arm_gic.h>
#include <bl_common.h>
#include <bl31.h>
#include <cci.h>
#include <console.h>
#include <debug.h>
#include <mmio.h>
#include <mtk_aee_debug.h>
#include <mtk_plat_common.h>
#include <mtk_sip_svc.h>
#include <mtspmc.h>
#include <platform.h>
#include <plat_private.h>
#include <xlat_tables_v2.h>

#if defined(MTK_DEVMPU_SUPPORT)
#include <mpu_v1.h>
#include <devmpu.h>
#endif

unsigned long __RW_START__;
unsigned long __RW_END__;


#define BL31_RO_BASE (unsigned long)(&__RO_START__)
#define BL31_RO_LIMIT (unsigned long)(&__RO_END__)
#define BL31_COHERENT_RAM_BASE (unsigned long)(&__COHERENT_RAM_START__)
#define BL31_COHERENT_RAM_LIMIT (unsigned long)(&__COHERENT_RAM_END__)

#define BL31_RW_BASE (unsigned long)(&__RW_START__)
#define BL31_RW_LIMIT (unsigned long)(&__RW_END__)

#pragma weak check_wdt_sgi_routing

#ifdef DRAM_EXTENSION_SUPPORT

#define DRAM_RO_BASE (unsigned long)(&__DRAM_RO_START__)
#define DRAM_RO_END (unsigned long)(&__DRAM_RO_END__)
#define DRAM_RW_BASE (unsigned long)(&__DRAM_RW_START__)
#define DRAM_RW_END (unsigned long)(&__DRAM_RW_END__)
#endif

struct atf_arg_t gteearg;
uint8_t percpu_plat_suspend_state[PLATFORM_CORE_COUNT]
#if USE_COHERENT_MEM
__section("tzfw_coherent_mem")
#endif
;
void clean_top_32b_of_param(uint32_t smc_fid,
				uint64_t *px1,
				uint64_t *px2,
				uint64_t *px3,
				uint64_t *px4)
{
	/* if parameters from SMC32. Clean top 32 bits */
	if (0 == (smc_fid & SMC_AARCH64_BIT)) {
		*px1 = *px1 & SMC32_PARAM_MASK;
		*px2 = *px2 & SMC32_PARAM_MASK;
		*px3 = *px3 & SMC32_PARAM_MASK;
		*px4 = *px4 & SMC32_PARAM_MASK;
	}
}

static struct kernel_info k_info;
static void save_kernel_info(uint64_t pc,
			uint64_t r0,
			uint64_t r1,
			uint64_t k32_64)
{
	k_info.k32_64 = k32_64;
	k_info.pc = pc;

	if (k32_64 == LINUX_KERNEL_32) {
		/* for 32 bits kernel */
		k_info.r0 = 0;
		/* machtype */
		k_info.r1 = r0;
		/* tags */
		k_info.r2 = r1;
	} else {
		/* for 64 bits kernel */
		k_info.r0 = r0;
		k_info.r1 = r1;
	}
}

uint64_t get_kernel_info_pc(void)
{
	return k_info.pc;
}

uint64_t get_kernel_info_r0(void)
{
	return k_info.r0;
}

uint64_t get_kernel_info_r1(void)
{
	return k_info.r1;
}

uint64_t get_kernel_info_r2(void)
{
	return k_info.r2;
}

void set_kernel_k32_64(uint64_t k32_64)
{
	k_info.k32_64 = k32_64;
}

uint64_t get_kernel_k32_64(void)
{
	return k_info.k32_64;
}

void boot_to_kernel(uint64_t x1, uint64_t x2, uint64_t x3, uint64_t x4)
{
	static uint8_t kernel_boot_once_flag;
	/* only support in booting flow */
	if (kernel_boot_once_flag == 0) {
		kernel_boot_once_flag = 1;
		console_init(gteearg.atf_log_port, UART_CLOCK, UART_BAUDRATE);

		INFO("save kernel info\n");
		save_kernel_info(x1, x2, x3, x4);
		bl31_prepare_kernel_entry(x4);

		INFO("el3_exit\n");
		console_uninit();
	}
}

#if defined(MTK_ENABLE_GENIEZONE)
#define GZ_KERNEL_LOAD_OFFSET (0x38000000)
#define EL2_BOOT_DISABLE (1 << 0)
static uint32_t gz_configs = EL2_BOOT_DISABLE;
static uint64_t gz_exec_start_offset;
static uint32_t gz_reserved_mem_size;
uint32_t is_el2_enabled(void)
{
	if (gz_configs & EL2_BOOT_DISABLE)
		return 0; /* el2 is disabled */

	return 1; /* el2 is enabled */
}
uint64_t get_el2_exec_start_offset(void)
{
	return gz_exec_start_offset;
}
uint32_t get_el2_reserved_mem_size(void)
{
	return gz_reserved_mem_size;
}
void configure_el2_info(struct boot_tag_gz_info *gz_info)
{
	gz_configs = gz_info->gz_configs;
	INFO("GZ CONFIGS = 0x%x\n", gz_configs);
}
#include <string.h>
void configure_el2_plat(struct boot_tag_gz_platform *gz_plat)
{
	gz_reserved_mem_size = gz_plat->reserve_mem_size;
	INFO("GZ RESERVE MEM SIZE = 0x%x\n", gz_plat->reserve_mem_size);

	/* do copy here to prevent un-aligned access for 64-bit memory */
	memcpy(&gz_exec_start_offset, &gz_plat->exec_start_offset,
		sizeof(uint64_t));
	INFO("GZ EXEC START OFFSET = 0x%lx\n", gz_exec_start_offset);

#if defined(MTK_DEVMPU_SUPPORT)
	int rc;
	uint64_t gz_base;

	enum DEVMPU_PERM vmd_rw_perm[DEVMPU_VMD_NUM];

	gz_base = gz_plat->exec_start_offset + GZ_KERNEL_LOAD_OFFSET;
	gz_base -= gz_plat->ddr_remap_offset;

	vmd_rw_perm[0] = DEVMPU_PERM_BLOCK;
	vmd_rw_perm[1] = DEVMPU_PERM_NS;
	vmd_rw_perm[2] = DEVMPU_PERM_BLOCK;
	vmd_rw_perm[3] = DEVMPU_PERM_BLOCK;

	rc = devmpu_domain_remap_set(gz_plat->remap_domain, 1);
	if (rc)
		WARN("failed to remap GZ EMI domain %d to VM domain 1, rc=%d\n",
				gz_plat->remap_domain, rc);

	rc = devmpu_rw_perm_set(gz_base, gz_reserved_mem_size, vmd_rw_perm, false);
	if (rc)
		WARN("failed to set Device MPU read permission for GZ DRAM, rc=%d\n", rc);

	rc = devmpu_rw_perm_set(gz_base, gz_reserved_mem_size, vmd_rw_perm, true);
	if (rc)
		WARN("failed to set Device MPU write permission for GZ DRAM, rc=%d\n", rc);
#endif
}
#endif

#if defined(MTK_DEVMPU_SUPPORT)
void configure_devmpu(void)
{
	int i;
	struct emi_region_info_t region_info;

	/* disable anyway */
	devmpu_disable();

	/*
	 * Device MPU protection relies on at least one region
	 * covered by EMI MPU to become fully functional. Thus,
	 * we need setup EMI MPU AP region (31) at early stage.
	 */
	region_info.start  = DEVMPU_DRAM_BASE;
	region_info.end    = DEVMPU_DRAM_BASE + DEVMPU_DRAM_SIZE - 1;
	region_info.region = EMI_MPU_REGION_ID_AP;
	SET_ACCESS_PERMISSION(region_info.apc, UNLOCK,
			NO_PROTECTION, NO_PROTECTION, NO_PROTECTION, NO_PROTECTION,
			NO_PROTECTION, NO_PROTECTION, NO_PROTECTION, NO_PROTECTION,
			NO_PROTECTION, NO_PROTECTION, NO_PROTECTION, NO_PROTECTION,
			NO_PROTECTION, NO_PROTECTION, NO_PROTECTION, NO_PROTECTION);
	if (emi_mpu_set_protection(&region_info)) {
		WARN("%s: failed to set EMI MPU AP region for Device MPU\n", __func__);
		return;
	}

	/* reset device mpu for clean permission control state */
	if (devmpu_reset()) {
		WARN("%s: failed to reset Device MPU\n", __func__);
		return;
	}

	/* set all EMI domain to the last VM domain */
	for (i = 0; i < EMI_MPU_DOMAIN_NUM; ++i) {
		if (devmpu_domain_remap_set(i, DEVMPU_VMD_NUM - 1)) {
			WARN("%s: failed to set Device MPU domain remap, EMI domain=%d\n", __func__, i);
			return;
		}
	}

	/* initiate DeviceMPU protection */
	devmpu_enable();
}

void configure_devmpu_atf_protection(void)
{
	int ret;
	enum DEVMPU_PERM vmd_rw_perm[DEVMPU_VMD_NUM];

	vmd_rw_perm[0] = DEVMPU_PERM_S;
	vmd_rw_perm[1] = DEVMPU_PERM_BLOCK;
	vmd_rw_perm[2] = DEVMPU_PERM_BLOCK;
	vmd_rw_perm[3] = DEVMPU_PERM_BLOCK;

	ret = devmpu_domain_remap_set(0, 0);
	if (ret)
		WARN("Failed to remap EMI domain 0 to VM domain 0, ret=%d\n", ret);

	ret = devmpu_rw_perm_set(TZRAM2_BASE, TZRAM2_SIZE, vmd_rw_perm, false);
	if (ret)
		WARN("Failed to set Device MPU read ermission for ATF DRAM, ret=%d\n", ret);

	ret = devmpu_rw_perm_set(TZRAM2_BASE, TZRAM2_SIZE, vmd_rw_perm, true);
	if (ret)
		WARN("Failed to set Device MPU write permission for ATF DRAM, ret=%d\n", ret);
}
#endif

uint32_t plat_get_spsr_for_bl33_entry(void)
{
	unsigned int mode;
	uint32_t spsr;
	unsigned int ee;
	unsigned long daif;

#if defined(MTK_ENABLE_GENIEZONE)
	if (is_el2_enabled()) {
		INFO("Booting from EL2!\n");
		spsr = SPSR_64(MODE_EL2, MODE_SP_ELX, DISABLE_ALL_EXCEPTIONS);
		return spsr;
	}
#endif

	INFO("Secondary bootloader is AArch32\n");
	mode = MODE32_svc;
	ee = 0;
	/*
	 * TODO: Choose async. exception bits if HYP mode is not
	 * implemented according to the values of SCR.{AW, FW} bits
	 */
	daif = DAIF_ABT_BIT | DAIF_IRQ_BIT | DAIF_FIQ_BIT;

	spsr = SPSR_MODE32(mode, 0, ee, daif);
	return spsr;
}
/*******************************************************************************
 * common function setting up the pagetables of the platform memory
 * map & initialize the mmu, for the given exception level.
 * Use plat_mmp_tbl as platform customized page table mapping.
 ******************************************************************************/
void configure_mmu_el3(const mmap_region_t *plat_mmap_tbl)
{
	unsigned long total_base = TZRAM_BASE;
#ifdef DRAM_EXTENSION_SUPPORT
	unsigned long total_size = (TZRAM_SIZE) & ~(PAGE_SIZE_MASK);
#else
	unsigned long total_size = (TZRAM_SIZE + TZRAM2_SIZE) & ~(PAGE_SIZE_MASK);
#endif
	unsigned long ro_start = BL31_RO_BASE & ~(PAGE_SIZE_MASK);
	unsigned long ro_size = BL31_RO_LIMIT - ro_start;
#if USE_COHERENT_MEM
	unsigned long coh_start, coh_size;
#endif

	/* add memory regions */
	mmap_add_region(total_base, total_base,
			total_size,
			MT_MEMORY | MT_RW | MT_SECURE);
	mmap_add_region(ro_start, ro_start,
			ro_size,
			MT_MEMORY | MT_RO | MT_SECURE);

#if USE_COHERENT_MEM
	coh_start = BL31_COHERENT_RAM_BASE;
	coh_size = BL31_COHERENT_RAM_LIMIT - BL31_COHERENT_RAM_BASE;

	mmap_add_region(coh_start, coh_start,
			coh_size,
			MT_DEVICE | MT_RW | MT_SECURE);
#endif
#ifdef DRAM_EXTENSION_SUPPORT
	mmap_add_region(DRAM_RO_BASE, DRAM_RO_BASE,
			DRAM_RO_END - DRAM_RO_BASE,
			MT_MEMORY | MT_RO | MT_SECURE);
	mmap_add_region(DRAM_RW_BASE, DRAM_RW_BASE,
			DRAM_RW_END - DRAM_RW_BASE,
			MT_MEMORY | MT_RW | MT_SECURE);
#endif
	/* add mmap table */
	if (plat_mmap_tbl)
		mmap_add(plat_mmap_tbl);
	else
		WARN("platform mmap table is not available\n");

	/* set up translation tables */
	init_xlat_tables();

	/* enable the MMU */
	enable_mmu_el3(0);
}

void check_wdt_sgi_routing(void)
{
}

void bl31_plat_runtime_setup(void)
{
	/* check WDT/SGI routing after S-OS init */
	check_wdt_sgi_routing();
#ifdef MTK_FPGA_LDVT
	boot_to_kernel(KERNEL_ENTRYPOINT, KERNEL_DTB_ADDR, 0, LINUX_KERNEL_64);
	bl31_prepare_next_image_entry();
#endif
	/*
	 * Finish the use of console driver in BL31 so that any runtime logs
	 * from BL31 will be suppressed.
	 */
	console_uninit();
}
