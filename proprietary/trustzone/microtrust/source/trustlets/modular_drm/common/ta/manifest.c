/* TA Configuration file */
#include <manifest.h>

// clang-format off

TA_CONFIG_BEGIN

uuid : { 0xe97c270e, 0xa5c4, 0x4c58, { 0xbc, 0xd3, 0x38, 0x4a, 0x2f, 0xa2, 0x53, 0x9e } },
log_tag : "wvl1_ta",
ipc_buf_size: 0x100000,

TA_CONFIG_END
