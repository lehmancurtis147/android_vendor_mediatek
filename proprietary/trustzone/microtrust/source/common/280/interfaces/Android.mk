# update interfaces before compile, this should exec before include interfaces/*/Android.bp
$(info $(shell vendor/mediatek/proprietary/trustzone/microtrust/source/common/$(MICROTRUST_TEE_VERSION)/interfaces/update.sh))

LOCAL_PATH := $(call my-dir)
include $(shell find -L $(LOCAL_PATH)/*/ -type f -name Android.mk)
