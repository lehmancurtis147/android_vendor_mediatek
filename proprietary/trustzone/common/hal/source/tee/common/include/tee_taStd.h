/*
 * Copyright (C) 2016 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#ifndef __MTK_TEE_TASTD_H__
#define __MTK_TEE_TASTD_H__


#if defined(__TRUSTONIC_TEE__)
//==============================================================================
// __TRUSTONIC_TEE__
//==============================================================================

#pragma message ( "TRUSTONIC_TEE" )
#include "taStd.h"

#elif defined(__MICROTRUST_TEE__)
//==============================================================================
// __MICROTRUST_TEE__
//==============================================================================

#pragma message ( "MICROTRUST_TEE" )
#include "taStd.h"

#elif defined(__TRUSTY_TEE__)
//==============================================================================
// __TRUSTY_TEE__
//==============================================================================

#pragma message ( "TRUSTY_TEE" )

#elif defined(__BLOWFISH_TEE__)
//==============================================================================
// __BLOWFISH_TEE__
//==============================================================================

#pragma message ( "BLOWFISH_TEE" )

#include <stdint.h>
#include <stdio.h>

//==============================================================================
// C/C++ compatibility
//==============================================================================

#if defined(__cplusplus)

    #define _EXTERN_C                extern "C"
    #define _BEGIN_EXTERN_C          extern "C" {
    #define _END_EXTERN_C            }

#else

    #define _EXTERN_C
    #define _BEGIN_EXTERN_C
    #define _END_EXTERN_C

#endif // defined(__cplusplus)

#define _TLAPI_EXTERN_C     _EXTERN_C
#define _TLAPI_NORETURN     _NORETURN

#if !defined(TRUE)
    #define TRUE    (1==1)
#endif // !defined(TRUE)

#if !defined(FALSE)
    #define FALSE   (1!=1)
#endif // !defined(TRUE)

#ifndef NULL
#  ifdef __cplusplus
#     define NULL  0
#  else
#     define NULL  ((void *)0)
#  endif
#endif

#define DECLARE_TRUSTLET_MAIN_STACK(_size_)
#define DECLARE_TRUSTED_APPLICATION_MAIN_STACK  DECLARE_TRUSTLET_MAIN_STACK

#ifndef EOL
    #define EOL "\n"
#endif

#define IN
#define OUT

#define TEE_ALLOCATION_HINT_ZEROED           0x00000000
//#define TEE_MALLOC_FILL_ZERO                 TEE_ALLOCATION_HINT_ZEROED

//#define TEE_LogPrintf msee_ta_printf

#ifdef DEBUG
#define TEE_DbgPrintf TEE_LogPrintf
#else
#define TEE_DbgPrintf
#endif

#define TEE_DbgPrintLnf(...)      do{TEE_DbgPrintf(__VA_ARGS__);TEE_DbgPrintf(EOL);}while(FALSE)

#else
#error "NO TEE SUPPORT defined!!!"

#endif


#endif /* __MTK_TEE_TASTD_H__ */
