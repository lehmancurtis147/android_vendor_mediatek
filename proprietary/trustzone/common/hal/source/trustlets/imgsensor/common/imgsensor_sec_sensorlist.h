/*
 * Copyright (C) 2018 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */
#ifndef __IMGSENSOR_SEC_SENSORLIST_H__
#define __IMGSENSOR_SEC_SENSORLIST_H__

#include "kd_imgsensor_define.h"
#include "kd_camera_typedef.h"

/*Add sensor ID here*/
#define S5K4E6_SENSOR_ID                        0x4e60

/*Add sensor Init function here*/
UINT32 S5K4E6_MIPI_RAW_SensorInit(PSENSOR_FUNCTION_STRUCT *pfFunc);

/*------------------Common structure----------------------*/
struct IMGSENSOR_SEC_SENSOR_LIST {
	MUINT32 id;
	MUINT32 (*init)(PSENSOR_FUNCTION_STRUCT *pfFunc);
};

extern struct IMGSENSOR_SEC_SENSOR_LIST imgsensor_sec_sensor_list[];

#endif
