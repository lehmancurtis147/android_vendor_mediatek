/*
 * Copyright (C) 2018 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */
#ifndef __IMGSENSOR_SEC_TYPEDEF_H__
#define __IMGSENSOR_SEC_TYPEDEF_H__

#include<drv_call.h>

/*typedef for struct*/
#ifndef u32
typedef unsigned short u32;
#endif

#ifndef u8
typedef unsigned short u8;
#endif

#ifndef u16
typedef unsigned short u16;
#endif

#ifndef uintptr_t
typedef unsigned int uintptr_t;
#endif

#ifndef BOOL
typedef unsigned char BOOL;
#endif

#ifndef bool
typedef unsigned char bool;
#endif

#undef pr_debug
#define pr_debug msee_ta_loge

#undef pr_info
#define pr_info msee_ta_loge

#ifndef PFX
#define PFX "SECURE_SENSOR"
#endif

#undef LOG_INF/*define msee_ta_loge in secure world*/
#define LOG_INF(fmt, args...)\
	msee_ta_loge(PFX"_TEE" "[%s] " fmt, __FUNCTION__, ##args)

/*Due to sensor develop in tee environment, lock is useless*/

#undef spin_lock(arg) /*now tee is single thread*/
#define spin_lock(arg)

#undef spin_unlock(arg) /*now tee is single thread*/
#define spin_unlock(arg)

#undef spin_lock_irqsave(arg1,arg2) /*now tee is single thread*/
#define spin_lock_irqsave(arg1,arg2)

#undef spin_unlock_irqrestore(arg1,arg2) /*now tee is single thread*/
#define spin_unlock_irqrestore(arg1,arg2)

#undef mDELAY
//#define mDELAY(arg) sleep(arg)// microtrust
#define mDELAY(arg) TEE_Wait(arg)

#undef mdelay
#define mdelay(arg) TEE_Wait(arg)

#undef imgsensor_drv_lock /*now tee is single thread*/
#define imgsensor_drv_lock

#endif
