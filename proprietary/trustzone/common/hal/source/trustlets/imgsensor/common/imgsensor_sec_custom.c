/*
 * Copyright (C) 2017 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#include "imgsensor_sec_custom.h"

int imgsensor_query_SEC_I2C_BUS() {
	/*int i2cBUS = MTK_SECURE_I2C_BUS;*/
	return SENSOR_MTK_SECURE_I2C_BUS;
}

int imgsensor_query_SEC_CSI() {
	/*int csi = MTK_SECURE_CSI;*/
	return SENSOR_MTK_SECURE_CSI;
}
