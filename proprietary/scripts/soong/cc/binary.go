package mtkCc

import (
	"android/soong/android"
	"android/soong/cc"
	"android/soong/android/mediatek"
)

func init() {
	android.RegisterModuleType("mtk_cc_binary", binaryFactory)
}

func binaryFactory() android.Module {
	m, _ := cc.NewBinary(android.HostAndDeviceSupported)
	module := m.Init()
	c := mediatek.InitVariableProperties().Interface()
	module.AddProperties(c)
	android.AddLoadHook(module, func(ctx android.LoadHookContext) { mtkCcLoadHook(ctx, c) })
	return module
}
