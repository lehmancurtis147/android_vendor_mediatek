LOCAL_PATH := $(call my-dir)

##### build libgz_uree static library #####

include $(CLEAR_VARS)

LOCAL_MODULE := libgz_uree
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := uree.c uree_mem.c

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include/

LOCAL_CFLAGS += -Wall -Wno-unused-parameter -Wno-unused-function -Werror
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_STATIC_LIBRARY)

##### build libgz_uree shared library #####

include $(CLEAR_VARS)

LOCAL_MODULE := libgz_uree
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_TAGS := optional
LOCAL_SRC_FILES := uree.c uree_mem.c

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/../include/

LOCAL_CFLAGS += -Wall -Wno-unused-parameter -Wno-unused-function -Werror
LOCAL_SHARED_LIBRARIES += liblog

include $(BUILD_SHARED_LIBRARY)
