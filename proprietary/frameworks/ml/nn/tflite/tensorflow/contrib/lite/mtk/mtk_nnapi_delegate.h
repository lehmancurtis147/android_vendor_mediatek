/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef MTK_TENSORFLOW_CONTRIB_LITE_NNAPI_DELEGATE_H_
#define MTK_TENSORFLOW_CONTRIB_LITE_NNAPI_DELEGATE_H_
#include <map>

#include "tensorflow/contrib/lite/interpreter.h"
#include "tensorflow/contrib/lite/nnapi_delegate.h"
#include "tensorflow/contrib/lite/nnapi/NeuralNetworksShim.h"

class ANeuralNetworsModel;

namespace tflite {

class MtkNNAPIDelegate : public NNAPIDelegate {
 public:
  MtkNNAPIDelegate();
  virtual ~MtkNNAPIDelegate() {
    for (auto it = oem_scalar_map_.begin(); it != oem_scalar_map_.end(); ++it) {
      free(it->second);
    }
  }

  void BindToDevice(uint32_t device) { device_ = device; }

  TfLiteStatus AddOpsAndParams(tflite::Interpreter* interpreter,
                       ANeuralNetworksModel* nn_model, uint32_t next_id,
                       std::vector<int>* model_state_inputs,
                       std::vector<int>* model_state_outputs,
                       const std::vector<int64_t>& tensor_id_to_nnapi_id) override;

 private:

  uint32_t device_ = 0;  // Device execute the model
  bool use_oem_scalar_ = false;
  std::map<int32_t, uint8_t*> oem_scalar_map_;
  std::map<int32_t, uint32_t> oem_scalar_size_map_;
};

}  // namespace tflite

#endif  // MTK_TENSORFLOW_CONTRIB_LITE_NNAPI_DELEGATE_H_
