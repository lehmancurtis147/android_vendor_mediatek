/* Copyright 2017 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#ifndef TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_
#define TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_

#include "tensorflow/contrib/lite/kernels/internal/reference/reference_ops.h"

namespace tflite {
namespace reference_ops {
namespace mtk {

inline void LeakyRelu(const uint8_t* input_data, const Dims<4>& input_dims,
               uint8_t* output_data, const Dims<4>& output_dims,
               int32_t input_zero_point, int32_t output_zero_point,
               int32_t pos_output_multiplier, int32_t pos_output_shift,
               int32_t neg_output_multiplier, int32_t neg_output_shift
               ) {
  const int batches = MatchingArraySize(input_dims, 3, output_dims, 3);
  const int height = MatchingArraySize(input_dims, 2, output_dims, 2);
  const int width = MatchingArraySize(input_dims, 1, output_dims, 1);
  const int depth = MatchingArraySize(input_dims, 0, output_dims, 0);
  for (int b = 0; b < batches; ++b) {
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        for (int c = 0; c < depth; ++c) {
            int32_t input_val = input_data[Offset(input_dims, c, x, y, b)] - input_zero_point;
            int32_t unclamped_output;
            if (input_val >= 0) {  // Positive Case
              if (pos_output_multiplier == 0 && pos_output_shift == 0) {
                  // Special Case, pos_multiplier == 1
                  unclamped_output = (input_val + output_zero_point);
              } else if (pos_output_shift <= 0) {    // SmallerThanOne
                  int32_t pos_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(input_val,
                                          pos_output_multiplier, -1 * pos_output_shift);
                  unclamped_output = pos_scaled + output_zero_point;
              } else {    // GreaterThanOne
                  int32_t pos_scaled = MultiplyByQuantizedMultiplierGreaterThanOne(input_val,
                                          pos_output_multiplier, pos_output_shift);
                  unclamped_output = pos_scaled + output_zero_point;
              }
            } else {  // Negative Case
              int32_t neg = 255 * input_val;    // map float-alpha to 255
              int32_t neg_scaled = MultiplyByQuantizedMultiplierSmallerThanOneExp(
                  neg, neg_output_multiplier, neg_output_shift);
              unclamped_output = neg_scaled + output_zero_point;
            }
          int32_t clamped_output = std::min(255, std::max(0, unclamped_output));
          output_data[Offset(output_dims, c, x, y, b)] =
              static_cast<uint8>(clamped_output);
        }
      }
    }
  }
}

inline void TransposeConv(const float* input_data, const Dims<4>& input_dims,
                          const float* filter_data, const Dims<4>& filter_dims,
                          const float* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, float* output_data,
                          const Dims<4>& output_dims, float* /*im2col_data*/,
                          const Dims<4>& /*im2col_dims*/) {
  const int batches = MatchingArraySize(input_dims, 3, output_dims, 3);
  const int input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);
  const int output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);
  const int input_height = ArraySize(input_dims, 2);
  const int input_width = ArraySize(input_dims, 1);
  const int filter_height = ArraySize(filter_dims, 2);
  const int filter_width = ArraySize(filter_dims, 1);
  const int output_height = ArraySize(output_dims, 2);
  const int output_width = ArraySize(output_dims, 1);

  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 3), ArraySize(bias_dims, 0));
  }

  // Although transpose convolution simplifies to convolution with transposed
  // weights for strides of 1, non-unitary striding complicates matters. To
  // keep this reference implementation as clear as possible, we use a
  // "scatter" access pattern, where we loop through all the input elements,
  // computing their influence on the output, rather than looping through the
  // output elements in the typical "gather" access pattern of a conv. We
  // therefore must initialize the output array to zero.
  for (int batch = 0; batch < batches; ++batch) {
    for (int in_y = 0; in_y < output_height; ++in_y) {
      for (int in_x = 0; in_x < output_width; ++in_x) {
        for (int out_channel = 0; out_channel < output_depth; ++out_channel) {
          const int output_index = Offset(output_dims, out_channel, in_x, in_y, batch);
          if (bias_data) {
            output_data[output_index] = bias_data[out_channel];
          } else {
            output_data[output_index] = 0.0f;
          }
        }
      }
    }
  }

  // Loop through input elements one at a time.
  for (int batch = 0; batch < batches; ++batch) {
    for (int in_y = 0; in_y < input_height; ++in_y) {
      for (int in_x = 0; in_x < input_width; ++in_x) {
        for (int in_channel = 0; in_channel < input_depth; ++in_channel) {
          // Loop through the output elements it will influence
          const int out_x_origin = (in_x * stride_width) - pad_width;
          const int out_y_origin = (in_y * stride_height) - pad_height;
          for (int filter_y = 0; filter_y < filter_height; ++filter_y) {
            for (int filter_x = 0; filter_x < filter_width; ++filter_x) {
              for (int out_channel = 0; out_channel < output_depth;
                   ++out_channel) {
                // Compute output element location
                const int out_x = out_x_origin + filter_x;
                const int out_y = out_y_origin + filter_y;
                // We cannot accumulate out of bounds
                if ((out_x >= 0) && (out_x < output_width) && (out_y >= 0) &&
                    (out_y < output_height)) {
                  float input_value = input_data[Offset(input_dims, in_channel,
                                                        in_x, in_y, batch)];
                  float filter_value =
                      filter_data[Offset(filter_dims, in_channel, filter_x,
                                         filter_y, out_channel)];
                  output_data[Offset(output_dims, out_channel, out_x, out_y,
                                     batch)] += input_value * filter_value;
                }
              }
            }
          }
        }
      }
    }
  }
}

inline void TransposeConv(const float* input_data, const Dims<4>& input_dims,
                          const float* filter_data, const Dims<4>& filter_dims,
                          const float* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, float output_activation_min,
                          float output_activation_max, float* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);
  //printf("input shape=(%d,%d,%d,%d)\n", input_depth, input_width, input_height, input_batches);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);
  //printf("filter shape=(%d,%d,%d,%d)\n", filter_dims.sizes[0], filter_dims.sizes[1], filter_dims.sizes[2], filter_dims.sizes[3]);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);
  //printf("output shape=(%d,%d,%d,%d)\n", output_dims.sizes[0], output_dims.sizes[1], output_dims.sizes[2], output_dims.sizes[3]);

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);
  // printf("filter_left_offset=%d, filter_top_offset=%d\n", filter_left_offset, filter_top_offset);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
          if (bias_data) {
            // Assign bias directly.
            output_data[output_index] = bias_data[c];
          } else {
            output_data[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          //printf("input(n,h,w)=(%d,%d,%d),  output_central(x,y,c) = (%d,%d,%d)\n", batch,h,w, x,y,c);
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                //printf("k(h,w)=(%d,%d) o(h,w)=(%d,%d)\n",kx,ky, ox,oy);
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  auto total = output_data[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      input_data[Offset(input_dims, f, w, h, batch)];

                    const auto filter_source_value =
                      filter_data[Offset(filter_dims, f, ky, kx, c)];
                    //printf("acc=%f by in=(%d,%d,%d,%d)=%f, f=(%d,%d,%d)=%f\n",input_source_value * filter_source_value,
                    //        batch,h,w,f, input_source_value, c,kx,ky, filter_source_value);
                    total += input_source_value * filter_source_value;
                  }
                  output_data[output_index] = total;
                  //printf("output_data(%d)=%f\n", output_index, total);
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    output_data[o_i] =
      ActivationFunctionWithMinMax(output_data[o_i],
                                   output_activation_min, output_activation_max);
  }
  return;
}

inline void TransposeConv(const uint8* input_data, const Dims<4>& input_dims,
                          int32 input_offset, const uint8* filter_data,
                          const Dims<4>& filter_dims, int32 filter_offset,
                          const int32* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, int32 output_offset, int32 output_multiplier,
                          int output_shift, int32 output_activation_min,
                          int32 output_activation_max, uint8* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);

  //int32 output_data_meta[input_batches*output_height*output_width*output_depth];
  int32* output_data_meta = new int32[input_batches*output_height*output_width*output_depth];

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
                                   //(batch * output_height * output_width * output_depth) +
                                   //(x * output_width * output_depth) + (y * output_depth) + (c);
          if (bias_data) {
            // Assign bias directly.
            output_data_meta[output_index] = bias_data[c];
          } else {
            output_data_meta[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  int32 total = output_data_meta[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      input_data[Offset(input_dims, f, w, h ,batch)];

                    const auto filter_source_value =
                      filter_data[Offset(filter_dims, f, ky, kx, c)];

                    const int32 input_value =
                      static_cast<int32>(input_source_value) - input_offset;
                    const int32 filter_value =
                      static_cast<int32>(filter_source_value) - filter_offset;

                    total += input_value * filter_value;
                  }
                  output_data_meta[output_index] = total;
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation and requant.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    auto acc = output_data_meta[o_i];
    acc = MultiplyByQuantizedMultiplierSmallerThanOneExp(acc, output_multiplier,
                                            ::tflite::reference_ops::kReverseShift * output_shift);
    acc += output_offset;
    acc = std::max(acc, output_activation_min);
    acc = std::min(acc, output_activation_max);
    output_data[o_i] = acc;
  }
  delete [] output_data_meta;
  return;
}

namespace nbits {
template <typename input_type, typename filter_type, typename output_type>
inline void TransposeConv(const void* input_data, const Dims<4>& input_dims,
                          int32 input_offset, const void* filter_data,
                          const Dims<4>& filter_dims, int32 filter_offset,
                          const int32* bias_data, const Dims<4>& bias_dims,
                          int stride_width, int stride_height, int pad_width,
                          int pad_height, int32 output_offset, int32 output_multiplier,
                          int output_shift, int32 output_activation_min,
                          int32 output_activation_max, void* output_data,
                          const Dims<4>& output_dims) {
  if (bias_data) {
    TFLITE_DCHECK_EQ(ArraySize(filter_dims, 0), ArraySize(bias_dims, 0));
  }

  const auto* typed_input_data = reinterpret_cast<const input_type*>(input_data);
  const auto* typed_filter_data = reinterpret_cast<const filter_type*>(filter_data);
  auto* typed_output_data = reinterpret_cast<output_type*>(output_data);

  int32 input_batches = ArraySize(input_dims, 3);
  int32 input_height = ArraySize(input_dims, 2);
  int32 input_width = ArraySize(input_dims, 1);
  int32 input_depth = MatchingArraySize(input_dims, 0, filter_dims, 0);

  int32 filter_height = ArraySize(filter_dims, 2);
  int32 filter_width = ArraySize(filter_dims, 1);

  int32 output_height = ArraySize(output_dims, 2);
  int32 output_width = ArraySize(output_dims, 1);
  int32 output_depth = MatchingArraySize(filter_dims, 3, output_dims, 0);

  //int32 output_data_meta[input_batches*output_height*output_width*output_depth];
  int32* output_data_meta = new int32[input_batches*output_height*output_width*output_depth];

  int filter_left_offset =
    // std::max(((input_width - 1) * stride_width + filter_width - output_width + 1) / 2, 0);
    std::max(((input_width - 1) * stride_width + filter_width - output_width) / 2, 0);
  int filter_top_offset =
    // std::max(((input_height - 1) * stride_height + filter_height - output_height + 1) / 2, 0);
    std::max(((input_height - 1) * stride_height + filter_height - output_height) / 2, 0);

  for (int batch = 0; batch < input_batches; batch++) {
    // For each channel in the output (which is the input of the conv2d).
    for (int c = 0; c < output_depth; c++) {
      // NOTE: output_data should be initialized to 0 or there will
      // be some issues (weird number) during accumulation.
      for (int x = 0; x < output_height; x++) {
        for (int y = 0; y < output_width; y++) {
          const int output_index = Offset(output_dims, c, y, x, batch);
                                   //(batch * output_height * output_width * output_depth) +
                                   //(x * output_width * output_depth) + (y * output_depth) + (c);
          if (bias_data) {
            // Assign bias directly.
            output_data_meta[output_index] = bias_data[c];
          } else {
            output_data_meta[output_index] = 0.0f;
          }
        }
      }
      // We know that output_data is initialized as an array with zeros
      // h and w are the coordinate for an element in the gradient of output
      // (input_data)
      for (int h = 0; h < input_height; h++) {
        for (int w = 0; w < input_width; w++) {
          // x and y are the coordinate of the center of the kernel that
          // outputs the element at (h, w).
          int x = filter_height / 2 + h * stride_height- filter_top_offset;
          int y = filter_width / 2 + w * stride_width- filter_left_offset;
          if ((h >= 0) && (h < input_height) && (w >= 0) && (w < input_width)){
            for (int kx = 0; kx < filter_height; kx++) {
              for (int ky = 0; ky < filter_width; ky++) {
                int ox = x + kx - filter_height / 2;
                int oy = y + ky - filter_width / 2;
                if ((ox >= 0) && (ox < output_height) && (oy >= 0) && (oy < output_width)){
                  const int output_index = Offset(output_dims, c, oy, ox, batch);
                  // initialize
                  int32 total = output_data_meta[output_index];
                  for (int f = 0; f < input_depth; f++) {
                    const auto input_source_value =
                      typed_input_data[Offset(input_dims, f, w, h ,batch)];

                    const auto filter_source_value =
                      typed_filter_data[Offset(filter_dims, f, ky, kx, c)];

                    const int32 input_value =
                      static_cast<int32>(input_source_value) - input_offset;
                    const int32 filter_value =
                      static_cast<int32>(filter_source_value) - filter_offset;

                    total += input_value * filter_value;
                  }
                  output_data_meta[output_index] = total;
                }
              }
            }
          }
        }
      }
    }
  }
  // Apply activation and requant.
  for (int o_i = 0; o_i < input_batches*output_width*output_height*output_depth; o_i++) {
    auto acc = output_data_meta[o_i];
    acc = MultiplyByQuantizedMultiplierSmallerThanOneExp(acc, output_multiplier,
                                            ::tflite::reference_ops::kReverseShift * output_shift);
    acc += output_offset;
    acc = std::max(acc, output_activation_min);
    acc = std::min(acc, output_activation_max);
    typed_output_data[o_i] = static_cast<output_type>(acc);
  }
  delete [] output_data_meta;
  return;
}

template <typename input_type, typename output_type>
inline void Requantize(int left_shift, const void* input_data, const Dims<4>& input_dims,
                       int input_offset, int output_offset,
                       int output_multiplier, int output_shift,
                       void* output_data, const Dims<4>& output_dims) {

  const auto* typed_input_data = reinterpret_cast<const input_type*>(input_data);
  auto* typed_output_data = reinterpret_cast<output_type*>(output_data);

  const int flat_size = MatchingFlatSize(output_dims, input_dims);

  for (int i = 0; i < flat_size; i++) {
    const int32 val = typed_input_data[i] + input_offset;
    const int32 shifted_val = val * (1 << left_shift);
    int32 output_val = MultiplyByQuantizedMultiplierSmallerThanOneExp(
        shifted_val, output_multiplier, output_shift) + output_offset;

    output_val = std::max(output_val, int32(std::numeric_limits<output_type>::min()));
    output_val = std::min(output_val, int32(std::numeric_limits<output_type>::max()));

    typed_output_data[i] = static_cast<output_type>(output_val);
  }
}

}  // namespace nbits
}  // namespace mtk
}  // namespace reference_ops
}  // namespace tflite

#endif  // TENSORFLOW_CONTRIB_LITE_KERNELS_INTERNAL_REFERENCE_MTK_REFERENCE_OPS_H_
