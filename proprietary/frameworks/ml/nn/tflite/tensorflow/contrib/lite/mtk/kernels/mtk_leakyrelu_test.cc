/* Copyright 2018 The TensorFlow Authors. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <functional>
#include <memory>
#include <vector>

#include <gtest/gtest.h>
#include "flatbuffers/flexbuffers.h"
#include "tensorflow/contrib/lite/interpreter.h"
#include "tensorflow/contrib/lite/kernels/register.h"
#include "tensorflow/contrib/lite/kernels/test_util.h"
#include "tensorflow/contrib/lite/model.h"

namespace tflite {
namespace ops {
namespace mtk {

TfLiteRegistration* Register_MTK_LEAKYRELU();

namespace {

using ::testing::ElementsAre;
using ::testing::ElementsAreArray;

class BaseMtkLeakyReluOpModel : public SingleOpModel {
 public:
  BaseMtkLeakyReluOpModel(const TensorData& input, const TensorData& output,
                          int version, float alpha) {
    input_ = AddInput(input);
    output_ = AddOutput(output);

    flexbuffers::Builder fbb;
    fbb.Map([&]() {
      fbb.Int("version", version);
      fbb.Float("alpha", alpha);
    });
    fbb.Finish();
    SetCustomOp("MTK_LEAKYRELU", fbb.GetBuffer(), Register_MTK_LEAKYRELU);

    BuildInterpreter({GetShape(input_)});
  }

  int input() { return input_; }
  std::vector<int> GetOutputShape() { return GetTensorShape(output_); }

 protected:
  int input_;
  int output_;
};

class FloatMtkLeakyReluOpModel : public BaseMtkLeakyReluOpModel {
 public:
  using BaseMtkLeakyReluOpModel::BaseMtkLeakyReluOpModel;

  std::vector<float> GetOutput() { return ExtractVector<float>(output_); }
};

class QuantizedMtkLeakyReluOpModel : public BaseMtkLeakyReluOpModel {
 public:
  using BaseMtkLeakyReluOpModel::BaseMtkLeakyReluOpModel;

  std::vector<float> GetDequantizedOutput() {
    return Dequantize<uint8_t>(ExtractVector<uint8_t>(output_),
                               GetScale(output_), GetZeroPoint(output_));
  }
};

// for quantized MtkLeakyRelu, the error shouldn't exceed 1 step
float GetTolerance(float min, float max) {
  return (max - min) / 255.0;
}

TEST(FloatMtkLeakyReluOpModel, SimpleTest) {
  FloatMtkLeakyReluOpModel m({TensorType_FLOAT32, {1, 2, 3, 1}},
                             {TensorType_FLOAT32, {}},
                             /*version=*/1, /*alpha=*/0.4);
  m.PopulateTensor<float>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetOutput(), ElementsAreArray(ArrayFloatNear({-1.44, -0.4, 0.0, 1.0, 1.6, 12.3}, 1e-5)));
}

TEST(FloatMtkLeakyReluOpModel, PassthroughTest) {
  FloatMtkLeakyReluOpModel m({TensorType_FLOAT32, {1, 2, 3, 1}},
                             {TensorType_FLOAT32, {}},
                             /*version=*/1, /*alpha=*/1);
  m.PopulateTensor<float>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetOutput(), ElementsAreArray(ArrayFloatNear({-3.6, -1, 0.0, 1.0, 1.6, 12.3}, 1e-5)));
}

TEST(FloatMtkLeakyReluOpModel, AsReluTest) {
  FloatMtkLeakyReluOpModel m({TensorType_FLOAT32, {1, 2, 3, 1}},
                             {TensorType_FLOAT32, {}},
                             /*version=*/1, /*alpha=*/0);
  m.PopulateTensor<float>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetOutput(), ElementsAreArray(ArrayFloatNear({0.0, 0.0, 0.0, 1.0, 1.6, 12.3}, 1e-5)));
}

TEST(QuantizedMtkLeakyReluOpModel, SimpleTest) {
  float kQuantizedTolerance = GetTolerance(-3.6, 12.3);
  QuantizedMtkLeakyReluOpModel m({TensorType_UINT8, {1, 2, 3, 1}, -3.6, 12.3},
                                 {TensorType_UINT8, {}, -3.6, 12.3},
                                 /*version=*/1, /*alpha=*/0.4);
  m.QuantizeAndPopulate<uint8_t>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetDequantizedOutput(), ElementsAreArray(ArrayFloatNear(
      {-1.44, -0.4, 0.0, 1.0, 1.6, 12.3}, kQuantizedTolerance)));
}

TEST(QuantizedMtkLeakyReluOpModel, PassthroughTest) {
  float kQuantizedTolerance = GetTolerance(-3.6, 12.3);
  QuantizedMtkLeakyReluOpModel m({TensorType_UINT8, {1, 2, 3, 1}, -3.6, 12.3},
                                 {TensorType_UINT8, {}, -3.6, 12.3},
                                 /*version=*/1, /*alpha=*/1);
  m.QuantizeAndPopulate<uint8_t>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetDequantizedOutput(), ElementsAreArray(ArrayFloatNear(
      {-3.6, -1, 0.0, 1.0, 1.6, 12.3}, kQuantizedTolerance)));
}

TEST(QuantizedMtkLeakyReluOpModel, SmallAlphaTest) {
  float kQuantizedTolerance = GetTolerance(-3.6, 12.3);
  QuantizedMtkLeakyReluOpModel m({TensorType_UINT8, {1, 2, 3, 1}, -3.6, 12.3},
                                 {TensorType_UINT8, {}, -3.6, 12.3},
                                 /*version=*/1, /*alpha=*/1e-4);
  m.QuantizeAndPopulate<uint8_t>(m.input(), {-3.6, -1, 0, 1, 1.6, 12.3});
  m.Invoke();

  std::vector<int> output_shape = m.GetOutputShape();
  EXPECT_THAT(output_shape, ElementsAre(1, 2, 3, 1));
  EXPECT_THAT(m.GetDequantizedOutput(), ElementsAreArray(ArrayFloatNear(
      {0.0, 0.0, 0.0, 1.0, 1.6, 12.3}, kQuantizedTolerance)));
}

}  // namespace
}  // namespace mtk
}  // namespace ops
}  // namespace tflite

int main(int argc, char** argv) {
  ::tflite::LogToStderr();
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
