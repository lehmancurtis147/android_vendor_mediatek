/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkUtil"
#include "mtk_log.h"
#include "mtk_util.h"

namespace tflite {
namespace mtk {
int32_t Hash(const char* str) {
  int32_t hash = 5381;
  int c;
  size_t count = 0;
  size_t len = strlen(str);
  LOG_D("Hash string: %s", str);

  while (count < len) {
    c = *(str + count);
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    count++;
  }

  return hash;
}

int EncodeOperandValue(OemOperandValue* operand, uint8_t* output) {
  size_t currPos = 0;

  // 1 byte for typeLen, 4 bytes for bufferLen
  if (output == nullptr) {
    return -1;
  }

  // Set length of type
  *output = operand->typeLen;
  currPos += sizeof(size_t);

  // Copy type to output
  memcpy(output + currPos, operand->type, operand->typeLen);
  currPos += operand->typeLen;

  output[currPos] = operand->dataLen;
  currPos += sizeof(size_t);

  // Copy  operand value to output
  memcpy(&output[currPos], operand->data, operand->dataLen);

  return 0;
}

int DecodeOperandValue(uint8_t *input, OemOperandValue *operand) {
  size_t currPos = 0;

  // Get Type length
  operand->typeLen = (size_t)*input;
  currPos += sizeof(size_t);

  // Get Types
  operand->type = (uint8_t*) malloc(operand->typeLen);
  memcpy(operand->type, &input[currPos], operand->typeLen);
  currPos += operand->typeLen;

    // Get Buffer length
  operand->dataLen = (size_t)input[currPos];
  currPos += sizeof(size_t);

  // Get Buffer
  operand->data = (uint8_t*)malloc(operand->dataLen);
  memcpy(operand->data, &input[currPos], operand->dataLen);

  return 0;
}

size_t PackOemScalarString(const char* str, uint8_t** out_buffer) {
  size_t out_len = 0;
  uint8_t type[] = {'s', 't', 'r', 'i', 'n', 'g'};
  OemOperandValue operand_value;

  operand_value.typeLen = sizeof(type);
  operand_value.type = type;
  operand_value.dataLen = strlen(str);
  operand_value.data = (uint8_t*)malloc(operand_value.dataLen);
  memcpy(operand_value.data, str, operand_value.dataLen);

  out_len = operand_value.typeLen + operand_value.dataLen + (sizeof(size_t) * 2);
  *out_buffer = (uint8_t*)malloc(out_len);
  EncodeOperandValue(&operand_value, *out_buffer);
  free(operand_value.data);

  LOG_D("PackOemScalarString: %s, buffer size:%zu", str, out_len);
  return out_len;
}

}
}  // namespace tflite
