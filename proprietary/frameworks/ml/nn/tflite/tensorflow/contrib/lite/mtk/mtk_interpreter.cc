/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkInterpreter"
#include "mtk_log.h"

#include "tensorflow/contrib/lite/mtk/mtk_interpreter.h"
#include <cassert>
#include <cstdarg>
#include <cstdint>
#include <cstring>
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/error_reporter.h"
#include "tensorflow/contrib/lite/mtk/mtk_nnapi_delegate.h"
#include "tensorflow/contrib/lite/mtk/mtk_util.h"

namespace tflite {

int32_t add_leaky_relu_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data);
int32_t add_transpose_conv_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data);
int32_t add_requantize_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data);

MtkInterpreter::MtkInterpreter(ErrorReporter* error_reporter) : Interpreter(error_reporter) {
  LOG_D("MtkInterpreter()");
  SetMtkExtOpParameterFunc("MTK_LEAKYRELU", add_leaky_relu_params);
  SetMtkExtOpParameterFunc("MTK_TRANSPOSE_CONV", add_transpose_conv_params);
  SetMtkExtOpParameterFunc("MTK_REQUANTIZE", add_requantize_params);
}

MtkInterpreter::~MtkInterpreter() {
}

TfLiteStatus MtkInterpreter::Invoke() {
  LOG_D("Invoke()");
  TfLiteStatus status = Interpreter::Invoke();

  // Dump OP result after the invoke is complete
  if (status == kTfLiteOk && nnapi_delegate_.get() == nullptr) {
    for (int execution_plan_index = 0;
         execution_plan_index < execution_plan_.size(); execution_plan_index++) {
      int node_index = execution_plan_[execution_plan_index];
      TfLiteNode& node = nodes_and_registration_[node_index].first;
      const TfLiteRegistration& registration =
          nodes_and_registration_[node_index].second;
        DumpOpOutput((BuiltinOperator)registration.builtin_code,
                     &(context_.tensors[node.outputs->data[0]]));
    }
  }

  return status;
}

void MtkInterpreter::UseNNAPI(bool enable) {
  LOG_D("UseNNAPI(%d)", enable);
  // TODO(aselle): This is a workaround for finding if NNAPI exists.
  // We also need to make sure getLibraryHandle() is renamed to be NNAPI
  // prefixed.
  if (!NNAPIExists()) enable = false;
  if (!enable) {
    nnapi_delegate_.reset();
  } else if (!nnapi_delegate_) {
    nnapi_delegate_.reset(new MtkNNAPIDelegate);
  }
}

void MtkInterpreter::BindToDevice(uint32_t device) {
  LOG_D("BindToDevice(%d)", device);
  ((MtkNNAPIDelegate *)nnapi_delegate_.get())->BindToDevice(device);
}

void MtkInterpreter::SetParamsFunc(const char* op_name,
                                   const char* vendor_name, ParameterFunc add_params) {
  LOG_D("SetParamsFunc, OP: %s, Vendor: %s", op_name, vendor_name);
  std::string full_name = std::string(op_name) + std::string(vendor_name);

  int32_t op_hash = mtk::Hash(full_name.c_str());
  LOG_D("Hash value in 32bit: %d", op_hash);
  index_addparams_.insert(std::make_pair(op_name, add_params));
  index_hash_.insert(std::make_pair(op_name, op_hash));
}
}  // namespace tflite
