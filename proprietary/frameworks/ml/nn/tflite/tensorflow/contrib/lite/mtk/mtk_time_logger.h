/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef MTK_TENSORFLOW_CONTRIB_LITE_TIME_LOGGER_H_
#define MTK_TENSORFLOW_CONTRIB_LITE_TIME_LOGGER_H_

#include <chrono>
#include <mutex>
#include <string.h>
#include <vector>

namespace tflite {

enum TimeLoggerStep {
  STEP_INVALID = -1,
  STEP_BUILD_INTERPRETER_BEGIN = 0,
  STEP_BUILD_INTERPRETER_END,
  STEP_CALL_NNAPI_BEGIN,
  STEP_CALL_NNAPI_END,
  STEP_TRIGGER_INFERENCE_BEGIN,
  STEP_TRIGGER_INFERENCE_END,
  STEP_NN_RUNTIME_INFERENCE_BEGIN,
  STEP_NN_RUNTIME_INFERENCE_END,
};

class TimeLogger {
 public:
  void Log(TimeLoggerStep step, std::chrono::high_resolution_clock::time_point point) {
    std::pair<TimeLoggerStep, std::chrono::high_resolution_clock::time_point> p =
        std::make_pair(step, point);

    time_point_list_.push_back(p);
  }

  double GetDuration(TimeLoggerStep begin, TimeLoggerStep end) {
    int begin_index = -1, end_index = -1;
    std::chrono::duration<double> elapsed;
    double ret = -1;

    if (end == STEP_INVALID) {
      end = static_cast<TimeLoggerStep>(begin + 1);
    }

    for (size_t i = 0; i < time_point_list_.size(); i++) {
      if (time_point_list_.at(i).first == begin) {
        begin_index = i;
      }

      if (time_point_list_.at(i).first == end) {
        end_index = i;
      }
    }

    if ((begin_index != -1 && end_index != -1) && (begin_index < end_index)) {
      elapsed = time_point_list_.at(end_index).second - time_point_list_.at(begin_index).second;
      ret = elapsed.count() * 1000;
    }

    return ret;
  }

  void Clear() { time_point_list_.clear(); }

 private:
  std::vector<std::pair<TimeLoggerStep ,
    std::chrono::high_resolution_clock::time_point>> time_point_list_;
};

}  // namespace tflite

#endif  // MTK_TENSORFLOW_CONTRIB_LITE_TIME_LOGGER_H_
