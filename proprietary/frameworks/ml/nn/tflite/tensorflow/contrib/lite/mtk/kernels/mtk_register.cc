/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkExtOpResolver"
#include "tensorflow/contrib/lite/mtk/mtk_log.h"
#include "tensorflow/contrib/lite/mtk/kernels/mtk_register.h"

namespace tflite {
namespace ops {

namespace mtk {

TfLiteRegistration* Register_MTK_LEAKYRELU();
TfLiteRegistration* Register_MTK_TRANSPOSE_CONV();
TfLiteRegistration* Register_MTK_REQUANTIZE();

}  // namespace mtk

namespace builtin {

MtkExtOpResolver::MtkExtOpResolver() {
    BuiltinOpResolver::AddCustom("MTK_LEAKYRELU", mtk::Register_MTK_LEAKYRELU());
    BuiltinOpResolver::AddCustom("MTK_TRANSPOSE_CONV", mtk::Register_MTK_TRANSPOSE_CONV());
    BuiltinOpResolver::AddCustom("MTK_REQUANTIZE", mtk::Register_MTK_REQUANTIZE());
}

void MtkExtOpResolver::AddCustom(const char* op_name,
                                 const char* vendor_name,
                                 TfLiteRegistration* registration) {
  TfLiteRegistration *reg = new TfLiteRegistration();
  reg->init = registration->init;
  reg->free = registration->free;
  reg->prepare = registration->prepare;
  reg->invoke = nullptr;
  reg->custom_name = op_name;
  custom_op_registrations_.insert(std::make_pair(std::string(op_name), reg));
  BuiltinOpResolver::AddCustom(op_name, reg);
}

}  // namespace builtin
}  // namespace ops
}  // namespace tflite

