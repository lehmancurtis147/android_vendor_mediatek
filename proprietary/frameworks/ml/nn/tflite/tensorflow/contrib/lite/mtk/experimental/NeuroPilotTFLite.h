/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef __MTK_NEURO_PILOT_TFLITE_H__
#define __MTK_NEURO_PILOT_TFLITE_H__

#if __ANDROID_API__ >= __ANDROID_API_O_MR1__

#include "NeuroPilotTFLiteShim.h"

__BEGIN_DECLS


/**
 * Create an {@link ANeuroPilotTFLite} with the TFlite model stored in a file.
 *
 * <p>This only creates the object. Computation is performed once
 * {@link ANeuroPilotTFLite_invoke} is invoked.
 *
 * <p>{@link ANeuroPilotTFLite_free} should be called once the instance
 * is no longer needed.</p>
 *
 * @param tflite The {@link ANeuroPilotTFLite} to be created.
 *               Set to NULL if unsuccessful.
 * @param modelPath The full path of the tflite model file.
 *
 * @return ANEURALNETWORKS_NO_ERROR if successful.
 */
int ANeuroPilotTFLite_create(ANeuralNetworksTFLite** tflite, const char* modelPath);

/**
 * Create an {@link ANeuroPilotTFLite} with the TFLite model stored in a data buffer pointer.
 * The data buffer will be duplicated in ANeuralNetworksTFLite instance.
 * Caller could free the input data buffer after calling this API.
 *
 * <p>This only creates the object. Computation is performed once
 * {@link ANeuroPilotTFLite_invoke} is invoked.
 *
 * <p>{@link ANeuroPilotTFLite_free} should be called once the instance
 * is no longer needed.</p>
 *
 * @param tflite The {@link ANeuroPilotTFLite} to be created.
 *              Set to NULL if unsuccessful.
 * @param buffer The pointer to the tflite model buffer.
 * @param bufferSize The number of bytes of the tflite model buffer.
 *
 * @return ANEURALNETWORKS_NO_ERROR if successful.
 */
int ANeuroPilotTFLite_createWithBuffer(ANeuralNetworksTFLite** tflite, const char* buffer,
                                           size_t bufferSize);

int ANeuroPilotTFLite_createCustom(ANeuralNetworksTFLite** tflite, const char* modelPath,
                                       const std::vector<TFLiteCustomOpExt>& customOperations);

int ANeuroPilotTFLite_createCustomWithBuffer(ANeuralNetworksTFLite** tflite, const char* buffer,
        size_t bufferSize, const std::vector<TFLiteCustomOpExt>& customOperations);

/**
 * Get a tensor data structure. This function returns the first input or output tensor.
 *
 * @param tflite The instance to get input/out tensor.
 * @param btype Input or output tensor.
 * @param tfliteTensor A pointer to store the tensor data structure.
 */
int ANeuroPilotTFLite_getTensor(ANeuralNetworksTFLite* tflite,
                                    TFLiteBufferType btype, TFLiteTensorExt* tfliteTensor);

/**
 * Get a tensor data structure. This function returns the input or output tensor by the given index.
 *
 * @param tflite The instance to get input/out tensor.
 * @param btype Input or output tensor.
 * @param tfliteTensor A pointer to store the tensor data structure.
 * @param tensorIndex Zero-based index of tensor.
 */
int ANeuroPilotTFLite_getTensorByIndex(ANeuralNetworksTFLite* tflite, TFLiteBufferType btype,
                                           TFLiteTensorExt *tfliteTensor, int tensorIndex);

/**
 * Invoke inference. (run the whole graph in dependency order).
 *
 * @param tflite The instance to invoke inference.
 *
 * @return ANEURALNETWORKS_NO_ERROR if successful.
 */
int ANeuroPilotTFLite_invoke(ANeuralNetworksTFLite* tflite);

/**
 * Delete a memory object.
 *
 * Destroys the object used by the run time to keep track of the memory.
 * This will free the underlying actual memory if no other code has open
 * handles to this memory.
 *
 * @param memory The memory object to be freed.
 */
void ANeuroPilotTFLite_free(ANeuralNetworksTFLite* tflite);

/**
 * Bind a {@link ANeuroPilotTFLite} instance to the specified device.(CPU/GPU/APU)
 *
 * @param tflite The instance.
 * @param device Device ID.(ANEURALNETWORKS_CPU/ANEURALNETWORKS_GPU/ANEURALNETWORKS_APU)
 *
 * @return ANEURALNETWORKS_NO_ERROR if successful.
*/
int ANeuroPilotTFLite_bindToDeivce(ANeuralNetworksTFLite* tflite, uint32_t device);

int ANeuroPilotTFLiteCustomOp_getIntAttribute(const char* buffer, size_t length,
                                              const char* attr, int32_t* outValue);

int ANeuroPilotTFLiteCustomOp_getFloatAttribute(const char* buffer, size_t length,
                                                    const char* attr, float* outValue);

void* ANeuroPilotTFLiteCustomOp_getUserData(TfLiteNode* node);

int ANeuroPilotTFLiteCustomOp_resizeOutput(TfLiteContext* context,
                                       TfLiteNode* node,
                                       int index,
                                       TfLiteIntArray* new_size);

int ANeuroPilotTFLiteCustomOp_getInput(TfLiteContext* context, TfLiteNode* node,
                              int index, TFLiteTensorExt *tfliteTensor);

int ANeuroPilotTFLiteCustomOp_getOutput(TfLiteContext* context, TfLiteNode* node,
                               int index, TFLiteTensorExt *tfliteTensor);

TfLiteIntArray* ANeuroPilotTFLite_createIntArray(int size);

int ANeuroPilotTFLite_freeIntArray(TfLiteIntArray* v);

int ANeuroPilotTFLiteLegacy_createCustomWithBuffer(ANeuralNetworksTFLite**
        tflite, const char* buffer,
        size_t bufferSize, const std::vector<TFLiteCustomOp>& customOperations);

int ANeuroPilot_getInferencePreference(void);

__END_DECLS

#endif  //  __ANDROID_API__ >= 27

#endif  // __MTK_NEURO_PILOT_TFLITE_H__
