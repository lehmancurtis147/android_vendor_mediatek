/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include "mtk_nnapi_delegate.h"
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "tensorflow/contrib/lite/builtin_op_data.h"
#include "tensorflow/contrib/lite/error_reporter.h"
#include "tensorflow/contrib/lite/model.h"
#include "tensorflow/contrib/lite/nnapi/NeuralNetworksShim.h"
#include "NeuralNetworksOEM.h"

#define LOG_TAG "MtkNNAPIDelegate"
#include "tensorflow/contrib/lite/mtk/mtk_log.h"
#include "tensorflow/contrib/lite/mtk/mtk_interpreter.h"
#include "tensorflow/contrib/lite/mtk/mtk_util.h"
#include <iostream>
#include <cutils/properties.h>

namespace tflite {
namespace mtk {
void logError(const char* format, ...) {
  // TODO(mikie): use android logging, stderr is not captured for Java
  // applications
  va_list args;
  va_start(args, format);
  LOG_E(format, args);
  vfprintf(stderr, format, args);
  va_end(args);
  fprintf(stderr, "\n");
  fflush(stderr);
}
}  // namespace mtk

#define FATAL(...)       \
  mtk::logError(__VA_ARGS__); \
  exit(1);

// TODO(aselle): Change the error model to use status codes.
#define CHECK_TFLITE_SUCCESS(x)                             \
  if (x != kTfLiteOk) {                                     \
    LOG_E("Aborting since tflite returned failure.");       \
    std::cout << "Aborting since tflite returned failure."  \
              << __FILE__ << ":" << __LINE__ << std::endl;  \
  }

#define CHECK_NN(x)                                         \
  if (x != ANEURALNETWORKS_NO_ERROR) {                      \
    LOG_E("Aborting since NN returned failure.");           \
    std::cout << "Aborting since NN returned failure. "     \
              << __FILE__ << ":" << __LINE__ << std::endl;  \
  }

#define RETURN_ERROR_IF_NN_FAILED(x)                                               \
  if (x != ANEURALNETWORKS_NO_ERROR) {                                             \
    mtk::logError(                                                                 \
        "Returning error since NNAPI returned failure mtk_nnapi_delegate.cc:%d.",  \
        __LINE__);                                                                 \
    return kTfLiteError;                                                           \
  }

namespace {

int32_t GetAndroidSdkVersion() {
#ifdef __ANDROID__
  const char* sdkProp = "ro.build.version.sdk";
  char sdkVersion[PROP_VALUE_MAX];
  int length = __system_property_get(sdkProp, sdkVersion);
  if (length != 0) {
    for (int i = 0; i < length; ++i) {
      int digit = sdkVersion[i] - '0';
      if (digit < 0 || digit > 9) {
        // Non-numeric SDK version, assume it's higher then expected;
        return 0xFFFF;
      }
    }
    return atoi(sdkVersion);
  }
  FATAL("No %s prop", sdkProp);
#endif  // __ANDROID__
  return 0;
}

static const int32_t kAndroidSdkVersion = GetAndroidSdkVersion();

}

// Tracking of NNAPI operand ids
static const int64_t kOperandIdNotSet = -1;
static const int64_t kOperandNotNeeded = -2;

MtkNNAPIDelegate::MtkNNAPIDelegate() {
  use_oem_scalar_ = property_get_bool("vendor.nn.oem.scalar", false);
  LOG_D ("MtkNNAPIDelegate(), use_oem_scalar_=%d", use_oem_scalar_);
}

// Adds the operations and their parameters to the NN API model.
// 'next-id' is the operand ID of the next operand of the model.
TfLiteStatus MtkNNAPIDelegate::AddOpsAndParams(
    tflite::Interpreter* interpreter, ANeuralNetworksModel* nn_model,
    uint32_t next_id, std::vector<int>* model_state_inputs,
    std::vector<int>* model_state_outputs,
    const std::vector<int64_t>& tensor_id_to_nnapi_id) {
  ANeuralNetworksModel_relaxComputationFloat32toFloat16(nn_model,
    ((MtkInterpreter*)interpreter)->GetRelaxComputationFloat32toFloat16());
  /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
  bool fuse_pad = false;
  int explicit_pad[4] = {0};
  std::vector<TfLiteNode> fusion_pad_list;  // List of the PAD OPs to be fused with other OPs
  /// M: NeuroPilot FUSE_PAD_WITH_CONV @}

  for (size_t i = 0; i < interpreter->nodes_size(); i++) {
    const auto* node_and_registration = interpreter->node_and_registration(i);
    const TfLiteNode& node = node_and_registration->first;
    const TfLiteRegistration& registration = node_and_registration->second;
    tflite::BuiltinOperator builtin =
        static_cast<tflite::BuiltinOperator>(registration.builtin_code);

    // Add the parameters.
    std::vector<uint32_t> augmented_inputs, augmented_outputs;
    MapAndAddTensorIds(node.inputs->data, node.inputs->size, &augmented_inputs,
                       tensor_id_to_nnapi_id);
    MapAndAddTensorIds(node.outputs->data, node.outputs->size,
                       &augmented_outputs, tensor_id_to_nnapi_id);

    auto add_scalar_int32 = [&nn_model, &augmented_inputs,
                             &next_id](int value) {
      ANeuralNetworksOperandType operand_type{.type = ANEURALNETWORKS_INT32};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
      CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, &value,
                                                    sizeof(int32_t)))
      augmented_inputs.push_back(next_id++);
    };

    auto add_scalar_float32 = [&nn_model, &augmented_inputs,
                               &next_id](float value) {
      ANeuralNetworksOperandType operand_type{.type = ANEURALNETWORKS_FLOAT32};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
      CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, &value,
                                                    sizeof(float)))
      augmented_inputs.push_back(next_id++);
    };

    auto add_vector_int32 = [&](const int* values, uint32_t num_values) {
      ANeuralNetworksOperandType operand_type{
          .type = ANEURALNETWORKS_TENSOR_INT32,
          .dimensionCount = 1,
          .dimensions = &num_values};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
      CHECK_NN(ANeuralNetworksModel_setOperandValue(
          nn_model, next_id, values, sizeof(int32_t) * num_values));
      augmented_inputs.push_back(next_id++);
    };

    auto add_oem_scalar = [&](void* oem_scalar, uint32_t size) {
      ANeuralNetworksOperandType operand_type{
          .type = ANEURALNETWORKS_OEM_SCALAR,
          .dimensionCount = 0,
          .dimensions = nullptr};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
      CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, oem_scalar, size))
      augmented_inputs.push_back(next_id++);
    };

    auto add_oem_scalar_string = [&](int32_t op_hash, const char* op_string) {
      ANeuralNetworksOperandType operand_type{
          .type = ANEURALNETWORKS_OEM_SCALAR,
          .dimensionCount = 0,
          .dimensions = nullptr};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
      auto scalar_it = oem_scalar_map_.find(op_hash);
      auto size_it = oem_scalar_size_map_.find(op_hash);
      if (scalar_it != oem_scalar_map_.end()) {
        CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id,
                                                      scalar_it->second,
                                                      size_it->second))
      } else {
        size_t oem_scalar_size = 0;
        uint8_t* oem_scalar = nullptr;
        oem_scalar_size = mtk::PackOemScalarString(op_string, &oem_scalar);
        oem_scalar_map_.insert(std::pair<int32_t, uint8_t*>(op_hash, oem_scalar));
        oem_scalar_size_map_.insert(std::pair<int32_t, uint32_t>(op_hash, oem_scalar_size));

        CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id,
                                                      oem_scalar, oem_scalar_size))
      }
      augmented_inputs.push_back(next_id++);
    };

    // Handle state tensors of RNN, LSTM, SVDF.
    // For each state_out tensor, a corresponding state_in operand needs to be
    // created for NNAPI.
    auto duplicate_state_tensor_float32 =
        [interpreter, &nn_model, &next_id, &augmented_inputs,
         &model_state_inputs, &model_state_outputs](int tensor_id) {
          const TfLiteTensor* tensor = interpreter->tensor(tensor_id);
          ANeuralNetworksOperandType operand_type{
              ANEURALNETWORKS_TENSOR_FLOAT32,
              static_cast<uint32_t>(tensor->dims->size),
              reinterpret_cast<uint32_t*>(tensor->dims->data),
              tensor->params.scale, tensor->params.zero_point};
          CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type));
          augmented_inputs.push_back(next_id);
          model_state_inputs->push_back(next_id);
          model_state_outputs->push_back(tensor_id);
          next_id++;
        };
    auto check_and_add_activation = [&add_scalar_int32](int activation) {
      if (activation > kTfLiteActRelu6) {
        FATAL("NNAPI only supports RELU, RELU1 and RELU6 activations");
      }
      add_scalar_int32(activation);
    };

    auto add_add_params = [&add_scalar_int32](void* data) {
      auto* builtin = reinterpret_cast<TfLiteAddParams*>(data);
      if (builtin->activation > kTfLiteActRelu6) {
        FATAL("NNAPI only supports RELU, RELU1 and RELU6 activations");
      }
      add_scalar_int32(builtin->activation);
    };

    auto add_pooling_params = [&add_scalar_int32,
                               &check_and_add_activation](void* data) {
      auto builtin = reinterpret_cast<TfLitePoolParams*>(data);
      add_scalar_int32(builtin->padding);
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->filter_width);
      add_scalar_int32(builtin->filter_height);
      check_and_add_activation(builtin->activation);
    };

    /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
    auto add_convolution_explicit_pad_params = [&add_scalar_int32,
                                   &fuse_pad,
                                   &explicit_pad](void* data) {
      auto builtin = reinterpret_cast<TfLiteConvParams*>(data);
      if (fuse_pad && explicit_pad[0] != 0 && explicit_pad[1] != 0 &&
          explicit_pad[2] != 0 && explicit_pad[3] != 0) {
        add_scalar_int32(explicit_pad[2]);
        add_scalar_int32(explicit_pad[3]);
        add_scalar_int32(explicit_pad[0]);
        add_scalar_int32(explicit_pad[1]);
      } else {
        add_scalar_int32(builtin->padding);
      }
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->activation);
    };

    auto add_depthwise_conv_explicit_pad_params = [&add_scalar_int32,
                                      &fuse_pad,
                                      &explicit_pad](void* data) {
       auto builtin = reinterpret_cast<TfLiteDepthwiseConvParams*>(data);
       if (fuse_pad && explicit_pad[0] != 0 && explicit_pad[1] != 0 &&
          explicit_pad[2] != 0 && explicit_pad[3] != 0) {
        add_scalar_int32(explicit_pad[2]);
        add_scalar_int32(explicit_pad[3]);
        add_scalar_int32(explicit_pad[0]);
        add_scalar_int32(explicit_pad[1]);
      } else {
        add_scalar_int32(builtin->padding);
      }
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->depth_multiplier);
      add_scalar_int32(builtin->activation);
    };
    /// M: NeuroPilot FUSE_PAD_WITH_CONV @}
    auto add_convolution_params = [&add_scalar_int32,
                                   &check_and_add_activation](void* data) {
      auto builtin = reinterpret_cast<TfLiteConvParams*>(data);
      add_scalar_int32(builtin->padding);
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      check_and_add_activation(builtin->activation);
    };

    auto add_depthwise_conv_params = [&add_scalar_int32,
                                      &check_and_add_activation](void* data) {
      auto builtin = reinterpret_cast<TfLiteDepthwiseConvParams*>(data);
      add_scalar_int32(builtin->padding);
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->depth_multiplier);
      check_and_add_activation(builtin->activation);
    };

    auto add_fully_connected_params = [&check_and_add_activation](void* data) {
      auto builtin = reinterpret_cast<TfLiteFullyConnectedParams*>(data);
      check_and_add_activation(builtin->activation);
    };

    auto add_concatenation_params = [&add_scalar_int32, &node, &interpreter](void* data) {
      auto builtin = reinterpret_cast<TfLiteConcatenationParams*>(data);
      if (builtin->axis < 0) {
        // Support negative axis
        const TfLiteTensor* tensor = interpreter->tensor(node.inputs->data[0]);
        builtin->axis += tensor->dims->size;
      }
      add_scalar_int32(builtin->axis);
      if (builtin->activation != kTfLiteActNone) {
        FATAL("Concatenation does not support fused activation in NNAPI");
      }
    };

    auto add_softmax_params = [&add_scalar_float32](void* data) {
      auto builtin = reinterpret_cast<TfLiteSoftmaxParams*>(data);
      add_scalar_float32(builtin->beta);
    };

    auto add_space_to_depth_params = [&add_scalar_int32](void* data) {
      auto builtin = reinterpret_cast<TfLiteSpaceToDepthParams*>(data);
      add_scalar_int32(builtin->block_size);
    };

    auto add_lstm_params = [&add_scalar_int32,
                            &add_scalar_float32](void* data) {
      auto builtin = reinterpret_cast<TfLiteLSTMParams*>(data);
      add_scalar_int32(builtin->activation);
      add_scalar_float32(builtin->cell_clip);
      add_scalar_float32(builtin->proj_clip);
    };

    // LSTM in NNAPI requires scratch tensor as an output operand.
    auto add_lstm_scratch_tensor_float32 = [interpreter, &node, &nn_model,
                                            &next_id, &augmented_outputs]() {
      if (node.temporaries->size == 0) return;
      int scratch_buffer_index = node.temporaries->data[0];
      const TfLiteTensor* tensor = interpreter->tensor(scratch_buffer_index);
      ANeuralNetworksOperandType operand_type{
          ANEURALNETWORKS_TENSOR_FLOAT32,
          static_cast<uint32_t>(tensor->dims->size),
          reinterpret_cast<uint32_t*>(tensor->dims->data), tensor->params.scale,
          tensor->params.zero_point};
      CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type));
      augmented_outputs.insert(augmented_outputs.begin(), next_id++);
    };

    auto add_mean_params = [&add_scalar_int32](void* data) {
      auto builtin = reinterpret_cast<TfLiteReducerParams*>(data);
      add_scalar_int32(builtin->keep_dims);
    };

    auto add_svdf_params = [&add_scalar_int32](void* data) {
      auto builtin = reinterpret_cast<TfLiteSVDFParams*>(data);
      add_scalar_int32(builtin->rank);
      add_scalar_int32(builtin->activation);
    };

    auto add_rnn_params = [&add_scalar_int32](void* data) {
      auto builtin = reinterpret_cast<TfLiteRNNParams*>(data);
      add_scalar_int32(builtin->activation);
    };

    auto add_squeeze_params = [&](void* data) {
      const auto* builtin = reinterpret_cast<TfLiteSqueezeParams*>(data);
      // Note that we add the squeeze dimensions even if the dimensions were
      // unspecified (empty), as NNAPI requires the operand.
      add_vector_int32(builtin->squeeze_dims,
                       static_cast<uint32_t>(builtin->num_squeeze_dims));
    };

    // Handle optional input tensors.
    auto add_optional_tensors = [&nn_model, &augmented_inputs,
                                 &next_id](int nn_type) {
      for (size_t idx = 0; idx < augmented_inputs.size(); idx++) {
        if (augmented_inputs[idx] == kOptionalTensor) {
          const std::vector<uint32_t> dim = {0, 0};
          ANeuralNetworksOperandType operand_type{nn_type, 2, dim.data(), 0, 0};
          CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
          CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id,
                                                        nullptr, 0))
          augmented_inputs[idx] = next_id++;
        }
      }
    };
    /// M: NeuroPilot @{
    auto add_mul_params = [&add_scalar_int32]() { add_scalar_int32(0); };

    auto add_resize_bilinear_params = [&add_scalar_int32, &node, &interpreter](void* data) {
      auto builtin = reinterpret_cast<TfLiteResizeBilinearParams*>(data);
      // Get the height and width of output tensor
      const TfLiteTensor* tensor = interpreter->tensor(node.inputs->data[1]);
      if (tensor->dims->size == 1) {
        add_scalar_int32(tensor->dims->data[0]);
        add_scalar_int32(tensor->dims->data[1]);
        LOG_D ("Resize bilinear height=%d, width=%d", tensor->dims->data[0], tensor->dims->data[1]);
      }
      // Todo
      // add_scalar_int32(builtin->align_corners);
    };

    /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
    auto add_dilated_convolution_explicit_pad_params = [&add_scalar_int32,
                                   &fuse_pad,
                                   &explicit_pad](void* data) {
      auto builtin = reinterpret_cast<TfLiteConvParams*>(data);
      if (fuse_pad && explicit_pad[0] != 0 && explicit_pad[1] != 0 &&
          explicit_pad[2] != 0 && explicit_pad[3] != 0) {
        add_scalar_int32(explicit_pad[2]);
        add_scalar_int32(explicit_pad[3]);
        add_scalar_int32(explicit_pad[0]);
        add_scalar_int32(explicit_pad[1]);
      } else {
        add_scalar_int32(builtin->padding);
      }
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->dilation_width_factor);
      add_scalar_int32(builtin->dilation_height_factor);
      add_scalar_int32(builtin->activation);
    };
    /// M: NeuroPilot FUSE_PAD_WITH_CONV @}
    auto add_dilated_convolution_params = [&add_scalar_int32,
                                   &check_and_add_activation](void* data) {
      auto builtin = reinterpret_cast<TfLiteConvParams*>(data);
      add_scalar_int32(builtin->padding);
      add_scalar_int32(builtin->stride_width);
      add_scalar_int32(builtin->stride_height);
      add_scalar_int32(builtin->dilation_width_factor);
      add_scalar_int32(builtin->dilation_height_factor);
      check_and_add_activation(builtin->activation);
    };

/*
    auto add_depth_to_space_params = [&add_scalar_int32](void* data) {
      auto builtin = reinterpret_cast<TfLiteDepthToSpaceParams*>(data);
      add_scalar_int32(builtin->block_size);
    };
*/
    auto add_local_response_normalization_params = [&add_scalar_int32, &add_scalar_float32](void* data) {
      auto builtin = reinterpret_cast<TfLiteLocalResponseNormParams*>(data);
      add_scalar_int32(builtin->radius);
      add_scalar_float32(builtin->bias);
      add_scalar_float32(builtin->alpha);
      add_scalar_float32(builtin->beta);
    };

    auto add_strided_slice_params = [&](void* data) {
      auto builtin = reinterpret_cast<TfLiteStridedSliceParams*>(data);
      add_scalar_int32(builtin->begin_mask);
      add_scalar_int32(builtin->end_mask);
      add_scalar_int32(builtin->shrink_axis_mask);
    };
    /// M: NeuroPilot @}

    auto add_reshape_params = [&](void* data) {
      auto builtin = reinterpret_cast<TfLiteReshapeParams*>(data);
      add_vector_int32(builtin->shape,
                       static_cast<uint32_t>(builtin->num_dimensions));
    };

    LOG_D("Process builtin OP(%d):%s", builtin, EnumNameBuiltinOperator(builtin));
    LOG_D("Num of input:%d", node.inputs->size);
    for (int l = 0; l <node.inputs->size; l++) {
      LOG_D("Input(%d)'s index: %d", l, node.inputs->data[l]);
    }
    LOG_D("Num of output:%d", node.outputs->size);
    for (int m = 0; m <node.outputs->size; m++) {
      LOG_D("Output(%d)'s index: %d", m, node.outputs->data[m]);
    }

    /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
    if (fuse_pad_with_conv_) {
      if ((builtin == tflite::BuiltinOperator_CONV_2D ||
          builtin == tflite::BuiltinOperator_DEPTHWISE_CONV_2D) && !fusion_pad_list.empty()) {
        int i, j, val_index;
        int32_t* data = nullptr;
        for (i = 0; i < fusion_pad_list.size(); i++) {
          if (fusion_pad_list.at(i).outputs->data[0] == node.inputs->data[0]) {
            fuse_pad = true;
            // Get padding value
            val_index = fusion_pad_list.at(i).inputs->data[1];
            data = interpreter->typed_tensor<int32_t>(val_index);
            for(j = 0; j < 4; j++) {
              explicit_pad[j] = data[j + 2];
            }
            break;
          }
        }

        if (fuse_pad) {
          // Set the input tensor of pad as the input tensor of conv
          LOG_D("Change builtin OP(%d):%s's input index:%d to index:%d", builtin,
              EnumNameBuiltinOperator(builtin),
              node.inputs->data[0],
              fusion_pad_list.at(i).inputs->data[0]);
          std::vector<uint32_t> pad_augmented_inputs;
          // Map PAD's tensor indices to NNAPI operand ids
          MapAndAddTensorIds(fusion_pad_list.at(i).inputs->data,
                             fusion_pad_list.at(i).inputs->size,
                             &pad_augmented_inputs,
                             tensor_id_to_nnapi_id);
          augmented_inputs[0] = pad_augmented_inputs[0];
          LOG_D("Set input to NNAPI operand id:%d", augmented_inputs[0]);
          // Remove the fused pad form list
          fusion_pad_list.erase(fusion_pad_list.begin() + i);
        }
      }
    }
    /// M: NeuroPilot FUSE_PAD_WITH_CONV @}

    int nnapi_version = 10;
    ANeuralNetworksOperationType nn_op_type;

    switch (builtin) {
      case tflite::BuiltinOperator_ADD:
        nn_op_type = ANEURALNETWORKS_ADD;
        add_add_params(node.builtin_data);
        break;
      case tflite::BuiltinOperator_MUL:
        nn_op_type = ANEURALNETWORKS_MUL;
        add_add_params(node.builtin_data);
        break;
      case tflite::BuiltinOperator_AVERAGE_POOL_2D:
        add_pooling_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_AVERAGE_POOL_2D;
        break;
      case tflite::BuiltinOperator_MAX_POOL_2D:
        add_pooling_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_MAX_POOL_2D;
        break;
      case tflite::BuiltinOperator_L2_POOL_2D:
        add_pooling_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_L2_POOL_2D;
        break;
      case tflite::BuiltinOperator_CONV_2D: {
        auto builtin = reinterpret_cast<TfLiteConvParams*>(node.builtin_data);
/*
        if (builtin->dilation_width_factor != 1 ||
            builtin->dilation_height_factor != 1 || node.inputs->size != 3) {
          mtk::logError("NNAPI does not support dilated Conv2D.");
          return kTfLiteError;
        }
*/

        if ((builtin->dilation_width_factor == 1 && builtin->dilation_height_factor > 1) ||
            (builtin->dilation_width_factor > 1 && builtin->dilation_height_factor == 1) ||
            (builtin->dilation_width_factor > 1 && builtin->dilation_height_factor > 1)) {
          // Dilated Conv2D
          if (fuse_pad) {
            add_dilated_convolution_explicit_pad_params(node.builtin_data);
          } else {
            add_dilated_convolution_params(node.builtin_data);
          }
          int32_t op_hash = mtk::Hash("dilatedconv2dmtk");
          if (use_oem_scalar_) {
            add_oem_scalar_string(op_hash, "dilatedconv2dmtk");

          } else {
            // Put OP hash as the last operand
            add_scalar_int32(op_hash);
          }
          nn_op_type = ANEURALNETWORKS_OEM_OPERATION;
          break;
        }
      }
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
        if (fuse_pad) {
          add_convolution_explicit_pad_params(node.builtin_data);
        } else {
          add_convolution_params(node.builtin_data);
        }
        fuse_pad = false;
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @}
        nn_op_type = ANEURALNETWORKS_CONV_2D;
        break;
      case tflite::BuiltinOperator_RELU:
        nn_op_type = ANEURALNETWORKS_RELU;
        break;
      case tflite::BuiltinOperator_RELU6:
        nn_op_type = ANEURALNETWORKS_RELU6;
        break;
      case tflite::BuiltinOperator_TANH:
        nn_op_type = ANEURALNETWORKS_TANH;
        break;
      case tflite::BuiltinOperator_FLOOR:
        nn_op_type = ANEURALNETWORKS_FLOOR;
        break;
      case tflite::BuiltinOperator_LOGISTIC:
        nn_op_type = ANEURALNETWORKS_LOGISTIC;
        break;
      case tflite::BuiltinOperator_DEPTHWISE_CONV_2D:
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
        if (fuse_pad) {
          add_depthwise_conv_explicit_pad_params(node.builtin_data);
        } else {
          add_depthwise_conv_params(node.builtin_data);
        }
        fuse_pad = false;
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @}
        nn_op_type = ANEURALNETWORKS_DEPTHWISE_CONV_2D;
        break;
      case tflite::BuiltinOperator_CONCATENATION:
        add_concatenation_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_CONCATENATION;
        break;
      case tflite::BuiltinOperator_SOFTMAX:
        add_softmax_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_SOFTMAX;
        break;
      case tflite::BuiltinOperator_FULLY_CONNECTED:
        add_fully_connected_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_FULLY_CONNECTED;
        break;
      case tflite::BuiltinOperator_RESHAPE:
        if (node.inputs->size < 2) {
          add_reshape_params(node.builtin_data);
        }
        nn_op_type = ANEURALNETWORKS_RESHAPE;
        break;
      case tflite::BuiltinOperator_SPACE_TO_DEPTH:
        add_space_to_depth_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_SPACE_TO_DEPTH;
        break;
      case tflite::BuiltinOperator_LSTM: {
#if 0
        if (node.inputs->size + /* no of params */ 3 != 21) {
          mtk::logError("NNAPI only supports 21-input LSTMs");
          return kTfLiteError;
        }
#endif
        duplicate_state_tensor_float32(
            node.outputs->data[/*kOutputStateTensor*/ 0]);
        duplicate_state_tensor_float32(
            node.outputs->data[/*kCellStateTensor*/ 1]);
        add_lstm_params(node.builtin_data);
        add_lstm_scratch_tensor_float32();
        add_optional_tensors(ANEURALNETWORKS_TENSOR_FLOAT32);
        nn_op_type = ANEURALNETWORKS_LSTM;
        break;
      }
      case tflite::BuiltinOperator_SVDF: {
        duplicate_state_tensor_float32(node.outputs->data[/*kStateTensor*/ 0]);
        add_svdf_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_SVDF;
        break;
      }
      case tflite::BuiltinOperator_RNN: {
        duplicate_state_tensor_float32(
            node.outputs->data[/*kHiddenStateTensor*/ 0]);
        add_rnn_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_RNN;
        break;
      }
      case tflite::BuiltinOperator_EMBEDDING_LOOKUP:
        nn_op_type = ANEURALNETWORKS_EMBEDDING_LOOKUP;
        break;
      case tflite::BuiltinOperator_PAD:
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @{
        if (fuse_pad_with_conv_) {
          bool skip = false;
          // Check if there is a CONV connected by this PAD
          for (size_t k = i + 1; k < interpreter->nodes_size(); k++) {
            const auto* target_node_and_registration = interpreter->node_and_registration(k);
            const TfLiteNode& target_node = target_node_and_registration->first;
            const TfLiteRegistration& target_registration = target_node_and_registration->second;
            tflite::BuiltinOperator target_builtin =
              static_cast<tflite::BuiltinOperator>(target_registration.builtin_code);

            if ((target_builtin == tflite::BuiltinOperator_CONV_2D ||
                target_builtin == tflite::BuiltinOperator_DEPTHWISE_CONV_2D) &&
                (target_node.inputs->data[0] == node.outputs->data[0])) {
              LOG_D("Store to fusion pad list");
              fusion_pad_list.push_back(node);
              LOG_D("PAD's input NNAPI operand id:%d", augmented_inputs[0]);
              skip = true;
              break;
            }
          }

          if (skip) {
            continue;
          } else {
            nn_op_type = ANEURALNETWORKS_PAD;
          }
        } else {
          nnapi_version = 11;  // require NNAPI 1.1
          nn_op_type = ANEURALNETWORKS_PAD;
        }
        /// M: NeuroPilot FUSE_PAD_WITH_CONV @}
        break;
      case tflite::BuiltinOperator_MEAN:
        nnapi_version = 11;  // require NNAPI 1.1
        add_mean_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_MEAN;
        break;
      case tflite::BuiltinOperator_DIV:
        nnapi_version = 11;  // require NNAPI 1.1
        nn_op_type = ANEURALNETWORKS_DIV;
        check_and_add_activation(
            reinterpret_cast<TfLiteDivParams*>(node.builtin_data)->activation);
        break;
      case tflite::BuiltinOperator_SUB:
        nnapi_version = 11;  // require NNAPI 1.1
        nn_op_type = ANEURALNETWORKS_SUB;
        check_and_add_activation(
            reinterpret_cast<TfLiteSubParams*>(node.builtin_data)->activation);
        break;
      case tflite::BuiltinOperator_SQUEEZE:
        nnapi_version = 11;  // requires NNAPI 1.1
        add_squeeze_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_SQUEEZE;
        break;
      case tflite::BuiltinOperator_TRANSPOSE:
        // The permutation input tensor value dictates the output dimensions.
        // TODO(b/110888333): Support dynamically-sized tensors in delegates.
        if ((node.inputs->size > 1) &&
            (interpreter->tensor(node.inputs->data[1])->allocation_type !=
             kTfLiteMmapRo)) {
          mtk::logError("NNAPI does not yet support dynamic tensors.");
          return kTfLiteError;
        }
        nnapi_version = 11;  // require NNAPI 1.1
        nn_op_type = ANEURALNETWORKS_TRANSPOSE;
        break;
      case tflite::BuiltinOperator_L2_NORMALIZATION:
        nn_op_type = ANEURALNETWORKS_L2_NORMALIZATION;
        if (reinterpret_cast<TfLiteL2NormParams*>(node.builtin_data)
                ->activation != kTfLiteActNone) {
          FATAL(
              "NNAPI does not support L2Normalization with fused activations");
        }
        break;
      case tflite::BuiltinOperator_CUSTOM: {
        nn_op_type = -1;
        MtkInterpreter::ParameterFunc customOpParamFunc =
          ((MtkInterpreter*)interpreter)->GetParamFunc(registration.custom_name);
        MtkInterpreter::MtkExtOpParameterFunc extOpParamFunc =
          ((MtkInterpreter*)interpreter)->GetMtkExtOpParamFunc(registration.custom_name);

        if (extOpParamFunc == nullptr) {
          LOG_E("No MtkExtOpParamFunc for %s.", registration.custom_name);
          if (customOpParamFunc == nullptr) {
            LOG_E("No customOpParamFunc for %s.", registration.custom_name);
          } else {
            int32_t op_hash = ((MtkInterpreter*)interpreter)->GetHash(registration.custom_name);
            // Set nn_op_type as the 1st operand of ANEURALNETWORKS_OEM_OPERATION
            customOpParamFunc(node.user_data, nn_model, augmented_inputs, next_id);
            nn_op_type = ANEURALNETWORKS_OEM_OPERATION;
            // Put OP hash as the last operand
            add_scalar_int32(op_hash);
            LOG_D("Use customOpParamFunc for %s, hash:%d", registration.custom_name, op_hash);
          }
        } else {
          int32_t op_hash = extOpParamFunc(nn_model, augmented_inputs, next_id, node.user_data);
          nn_op_type = ANEURALNETWORKS_OEM_OPERATION;
          if (use_oem_scalar_) {
            add_oem_scalar_string(op_hash, registration.custom_name);
          } else {
            // Put OP hash as the last operand
            add_scalar_int32(op_hash);
          }
          LOG_D("Use MtkExtOpParamFunc for %s, hash:%d", registration.custom_name, op_hash);
        }
        LOG_D("OP type: %d", nn_op_type);
        break;
      }
      /// M: NeuroPilot @{
      // Redirect TFLite only OPs to NN runtime
      case tflite::BuiltinOperator_LOCAL_RESPONSE_NORMALIZATION:
        add_local_response_normalization_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_LOCAL_RESPONSE_NORMALIZATION;
        break;
      case tflite::BuiltinOperator_RESIZE_BILINEAR:
        augmented_inputs.pop_back();
        add_resize_bilinear_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_RESIZE_BILINEAR;
        break;
      case tflite::BuiltinOperator_STRIDED_SLICE:
        add_strided_slice_params(node.builtin_data);
        nn_op_type = ANEURALNETWORKS_STRIDED_SLICE;
        break;
      case tflite::BuiltinOperator_BATCH_TO_SPACE_ND:
        // O1 NeuroPilot support BATCH_TO_SPACE_ND with 3 inputs.
        // Only change the input for P.
        if (kAndroidSdkVersion >= 28) {
          // NNAPI does not support crops. Remove crops from input.
          // Since crops is the last input, just pop it out.
          augmented_inputs.pop_back();
        }
        nn_op_type = ANEURALNETWORKS_BATCH_TO_SPACE_ND;
        break;
      case tflite::BuiltinOperator_SPACE_TO_BATCH_ND:
        nn_op_type = ANEURALNETWORKS_SPACE_TO_BATCH_ND;
        break;
      case tflite::BuiltinOperator_DEQUANTIZE:
        nn_op_type = ANEURALNETWORKS_DEQUANTIZE;
        break;
      case tflite::BuiltinOperator_RELU_N1_TO_1:
        nn_op_type = ANEURALNETWORKS_RELU1;
        break;
      default:
        mtk::logError("Operation is not supported when using NNAPI.");
        return kTfLiteError;
        break;
      /// M: NeuroPilot @}
    }

    // Add the operation.
    RETURN_ERROR_IF_NN_FAILED(ANeuralNetworksModel_addOperation(
        nn_model, nn_op_type, static_cast<uint32_t>(augmented_inputs.size()),
        augmented_inputs.data(),
        static_cast<uint32_t>(augmented_outputs.size()),
        reinterpret_cast<uint32_t*>(augmented_outputs.data())));
  }
  return kTfLiteOk;
}

}  // namespace tflite
