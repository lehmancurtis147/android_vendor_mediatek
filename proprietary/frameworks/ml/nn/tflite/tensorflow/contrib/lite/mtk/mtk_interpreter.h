/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef MTK_TENSORFLOW_CONTRIB_LITE_INTERPRETER_H_
#define MTK_TENSORFLOW_CONTRIB_LITE_INTERPRETER_H_

#include <stdlib.h>
#include <unordered_map>
#include "tensorflow/contrib/lite/error_reporter.h"
#include "tensorflow/contrib/lite/interpreter.h"
#include "tensorflow/contrib/lite/mtk/mtk_nnapi_delegate.h"
#include "tensorflow/contrib/lite/mtk/mtk_time_logger.h"

// Forward declare since MtkNNAPIDelegate uses MtkInterpreter.
class MtkNNAPIDelegate;

namespace tflite {
class MtkInterpreter : public Interpreter {
 public:
  // Instantiate an interpreter. All errors associated with reading and
  // processing this model will be forwarded to the error_reporter object.
  //
  // Note, if error_reporter is nullptr, then a default StderrReporter is
  // used.
  explicit MtkInterpreter(ErrorReporter* error_reporter = DefaultErrorReporter());

  virtual ~MtkInterpreter();

  // Invoke the interpreter (run the whole graph in dependency order).
  //
  // NOTE: It is possible that the interpreter is not in a ready state
  // to evaluate (i.e. if a ResizeTensor() has been performed without an
  // AllocateTensors().
  // Returns status of success or failure.
  TfLiteStatus Invoke() override;

  // Enable or disable the NN API (true to enable)
  void UseNNAPI(bool enable) override;

  typedef TfLiteStatus (*ParameterFunc)(void*, ANeuralNetworksModel*, std::vector<uint32_t>&, uint32_t&);

  void SetParamsFunc(const char* op_name, const char* vendor_name, ParameterFunc add_params);

  ParameterFunc GetParamFunc(std::string op_name) {
    std::unordered_map<std::string, ParameterFunc>::const_iterator got =
                                                        index_addparams_.find(op_name);
    if (got != index_addparams_.end()) {
      return got->second;
    } else {
      return nullptr;
    }
  }

  typedef int32_t (*MtkExtOpParameterFunc)(ANeuralNetworksModel*,
        std::vector<uint32_t>&, uint32_t&, void*);

  void SetMtkExtOpParameterFunc(std::string op_name, MtkExtOpParameterFunc add_params) {
    mtk_ext_op_addparams_.insert(make_pair(op_name, add_params));
  }

  MtkExtOpParameterFunc GetMtkExtOpParamFunc(std::string op_name) {
    std::unordered_map<std::string, MtkExtOpParameterFunc>::const_iterator got =
        mtk_ext_op_addparams_.find(op_name);
    if (got != mtk_ext_op_addparams_.end()) {
      return got->second;
    } else {
      return nullptr;
    }
  }

  void UseLogTime(bool enable) {
    log_time_ = enable;
  }

  void ClearLogTime() { time_logger_.Clear(); }

  void LogTime(TimeLoggerStep step) {
    if (log_time_) {
      time_logger_.Log(step, std::chrono::high_resolution_clock::now());
    }
  }

  void LogTime(TimeLoggerStep step, std::chrono::high_resolution_clock::time_point point) {
    if (log_time_) {
      time_logger_.Log(step, point);
    }
  }

  double GetDuration(TimeLoggerStep step) {
    return time_logger_.GetDuration(step, STEP_INVALID);
  }

  double GetDuration(TimeLoggerStep begin, TimeLoggerStep end) {
    return time_logger_.GetDuration(begin, end);
  }

  void BindToDevice(uint32_t device);

  int32_t GetHash(std::string op_name) {
    std::unordered_map<std::string, int32_t>::const_iterator got = index_hash_.find(op_name);
    if (got != index_hash_.end()) {
      return got->second;
    } else {
      return -1;
    }
  }

  void RelaxComputationFloat32toFloat16(bool allow) {
    relax_computation_float32_to_float16_ = allow;
  }

  bool GetRelaxComputationFloat32toFloat16(void) {
    return relax_computation_float32_to_float16_;
  }

 private:
  std::unordered_map<std::string, ParameterFunc> index_addparams_;
  std::unordered_map<std::string, MtkExtOpParameterFunc> mtk_ext_op_addparams_;
  std::unordered_map<std::string, int32_t> index_hash_;
  TimeLogger time_logger_;
  bool log_time_ = false;
  bool relax_computation_float32_to_float16_ = false;
};

}  // namespace tflite
#endif  // MTK_TENSORFLOW_CONTRIB_LITE_INTERPRETER_H_
