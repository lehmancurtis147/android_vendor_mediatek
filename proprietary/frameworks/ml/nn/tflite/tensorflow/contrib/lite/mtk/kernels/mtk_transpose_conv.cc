/* Copyright 2018 The TensorFlow Authors. All Rights Reserved.
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
    http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/
#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <limits>

#include "tensorflow/contrib/lite/builtin_op_data.h"
#include "tensorflow/contrib/lite/context.h"
#include "tensorflow/contrib/lite/model.h"
#include "tensorflow/contrib/lite/mtk/kernels/internal/reference/mtk_reference_ops.h"
#include "tensorflow/contrib/lite/kernels/internal/quantization_util.h"
#include "tensorflow/contrib/lite/kernels/internal/tensor.h"
#include "tensorflow/contrib/lite/kernels/kernel_util.h"
#include "tensorflow/contrib/lite/kernels/op_macros.h"
#include "tensorflow/contrib/lite/kernels/padding.h"
#include "tensorflow/contrib/lite/mtk/kernels/mtk_ops.h"
#include "tensorflow/contrib/lite/nnapi/NeuralNetworksShim.h"

#include "mtk_log.h"
#define LOG_TAG "MtkTransposeConv"
#include "tensorflow/contrib/lite/mtk/mtk_util.h"

#include "flatbuffers/flexbuffers.h"

namespace tflite {
namespace ops {
namespace mtk {
namespace transpose_conv {

#define CHECK_NN(x)                                         \
  if (x != ANEURALNETWORKS_NO_ERROR) {                      \
    LOG_E("Aborting since NN returned failure.");           \
    std::cout << "Aborting since NN returned failure."      \
              << __FILE__ << ":" << __LINE__ << std::endl;  \
    exit(1);                                                \
  }

constexpr int kOutputShapeTensor = 0;
constexpr int kWeightsTensor = 1;
constexpr int kDataInputTensor = 2;
constexpr int kBiasTensor = 3;
constexpr int kOutputTensor = 0;

enum KernelType {
  kReference,
};


TfLiteStatus ResizeOutputShape(TfLiteContext* context,
                               const TfLiteTensor* output_shape,
                               TfLiteTensor* output) {
  // Currently only support int32 for output shape.
  if (output_shape->type != kTfLiteInt32) {
    context->ReportError(context, "Output shape is %d, not int32.",
                         output_shape->type);
    return kTfLiteError;
  }
  const int output_dimensions = NumElements(output_shape);
  TfLiteIntArray* output_shape_array = TfLiteIntArrayCreate(output_dimensions);
  for (int i = 0; i < output_dimensions; ++i) {
    output_shape_array->data[i] = GetTensorData<int32_t>(output_shape)[i];
  }

  return context->ResizeTensor(context, output, output_shape_array);
}

void* Init(TfLiteContext* context, const char* buffer, size_t length) {
  //convert paddding and activation enum, copy from lite/model.cc
  auto parse_padding = [](Padding padding) {
    switch (padding) {
      case Padding_SAME:
        return kTfLitePaddingSame;
      case Padding_VALID:
        return kTfLitePaddingValid;
    }
    return kTfLitePaddingUnknown;
  };
  auto parse_activation = [](ActivationFunctionType activation) {
    switch (activation) {
      case ActivationFunctionType_NONE:
        return kTfLiteActNone;
      case ActivationFunctionType_RELU:
        return kTfLiteActRelu;
      case ActivationFunctionType_RELU_N1_TO_1:
        return kTfLiteActRelu1;
      case ActivationFunctionType_RELU6:
        return kTfLiteActRelu6;
      case ActivationFunctionType_TANH:
        return kTfLiteActTanh;
      case ActivationFunctionType_SIGN_BIT:
        return kTfLiteActSignBit;
    }
    return kTfLiteActNone;
  };

  OpData* data = new OpData;

  const uint8_t* buffer_t = reinterpret_cast<const uint8_t*>(buffer);
  const flexbuffers::Map& m = flexbuffers::GetRoot(buffer_t, length).AsMap();
  data->padding = parse_padding(static_cast<Padding>(m["PaddingType"].AsInt64()));
  data->stride_width = m["stride_width"].AsInt64();
  data->stride_height = m["stride_height"].AsInt64();
  data->activation = parse_activation(static_cast<ActivationFunctionType>(m["activation"].AsInt64()));
  data->depth_multiplier =  m["depth_multiplier"].AsInt64();
  data->dilation_width_factor = m["dilation_width_factor"].AsInt64();
  data->dilation_height_factor = m["dilation_height_factor"].AsInt64();

  return data;
}

void Free(TfLiteContext* context, void* buffer) {
  delete reinterpret_cast<OpData*>(buffer);
}

TfLiteStatus Prepare(TfLiteContext* context, TfLiteNode* node) {
  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  //currently transpose convolution don't support dilation and depthwise computation
  TF_LITE_ENSURE_EQ(context, data->depth_multiplier, 1);
  TF_LITE_ENSURE_EQ(context, data->dilation_width_factor, 1);
  TF_LITE_ENSURE_EQ(context, data->dilation_height_factor, 1);

  bool hasBias = NumInputs(node) == 4;
  TF_LITE_ENSURE(context, hasBias || NumInputs(node) == 3);
  TF_LITE_ENSURE_EQ(context, NumOutputs(node), 1);

  const TfLiteTensor* output_shape =
      GetInput(context, node, kOutputShapeTensor);
  const TfLiteTensor* weights = GetInput(context, node, kWeightsTensor);
  const TfLiteTensor* input = GetInput(context, node, kDataInputTensor);
  TfLiteTensor* output = GetOutput(context, node, kOutputTensor);

  TF_LITE_ENSURE_EQ(context, NumDimensions(output_shape), 1);
  TF_LITE_ENSURE_EQ(context, NumDimensions(input), 4);
  TF_LITE_ENSURE_EQ(context, NumDimensions(weights), 4);

  // // Check types. (We assume that UINT8 refers to quantized tensors)
  // const TfLiteType data_type = input->type;
  // TF_LITE_ENSURE(context,
  //                data_type == kTfLiteFloat32 || data_type == kTfLiteUInt8);
  // TF_LITE_ENSURE_EQ(context, output->type, data_type);
  // TF_LITE_ENSURE_EQ(context, weights->type, data_type);

  // Chia-Lin Yu @ Mediatek
  // Check if it uses nbits implementation
  const TfLiteType data_type = input->type;
  const TfLiteType weights_type = weights->type;
  const TfLiteType output_type = output->type;

  if (data_type == kTfLiteFloat32) {
    TF_LITE_ENSURE(context, weights_type == kTfLiteFloat32);
    TF_LITE_ENSURE(context, output_type == kTfLiteFloat32);
    data->need_nbits_impl = false;
  }
  else {
    TF_LITE_ENSURE(context, data_type == kTfLiteUInt8 || data_type == kTfLiteInt16);
    TF_LITE_ENSURE(context, weights_type == kTfLiteUInt8 || weights_type == kTfLiteInt16);
    TF_LITE_ENSURE(context, output_type == kTfLiteUInt8 || output_type == kTfLiteInt16);

    if (data_type == kTfLiteUInt8 &&
        weights_type == kTfLiteUInt8 &&
        output_type == kTfLiteUInt8) {
      data->need_nbits_impl = false;
    }
    else {
      data->need_nbits_impl = true;
    }
  }

  // Ensure that weights and inputs have the same channel dimension.
  // Note: TOCO will reorder weights in the following format: OHWI.
  TF_LITE_ENSURE_EQ(context, SizeOfDimension(input, 3),
                    SizeOfDimension(weights, 3));

  // Current implementation only supports equal strides in the row and column dimensions
  TF_LITE_ENSURE_EQ(context, data->stride_width, data->stride_height);

  const TfLiteTensor* bias = (hasBias) ? GetInput(context, node, kBiasTensor) : nullptr;
  if (hasBias) {
    if (data_type == kTfLiteUInt8 || data_type == kTfLiteInt16) {
      TF_LITE_ENSURE_EQ(context, bias->type, kTfLiteInt32);
      TF_LITE_ENSURE_EQ(context, bias->params.zero_point, 0);
    } else {
      TF_LITE_ENSURE_EQ(context, bias->type, data_type);
    }
    TF_LITE_ENSURE_EQ(context, NumDimensions(bias), 1);
    TF_LITE_ENSURE_EQ(context, SizeOfDimension(bias, 0), SizeOfDimension(weights, 0));
  }

  // Note that quantized inference requires that all tensors have their
  // parameters set. This is usually done during quantized training.
  if (output_type != kTfLiteFloat32) {
    double real_multiplier = 0.0;
    TF_LITE_ENSURE_STATUS(GetQuantizedConvolutionMultipler(
      context, input, weights, bias, output, &real_multiplier));
    QuantizeMultiplierSmallerThanOneExp(real_multiplier, &data->output_multiplier,
                                        &data->output_shift);
    data->output_shift *= -1;

    if (output_type == kTfLiteUInt8) {
      CalculateActivationRangeUint8(data->activation, output,
                                    &data->output_activation_min,
                                    &data->output_activation_max);
    }
    else {
      TF_LITE_ENSURE(context, output_type == kTfLiteInt16);
      CalculateActivationRangeQuantized(context, data->activation, output,
                                        &data->output_activation_min,
                                        &data->output_activation_max);
    }
  }

  if (!IsConstantTensor(output_shape)) {
    SetTensorToDynamic(output);
    return kTfLiteOk;
  }
  auto ret = ResizeOutputShape(context, output_shape, output);

  // Check the expected input shape from the output shape
  // with the VALID padding condition.
  auto output_width = SizeOfDimension(output, 2);
  auto output_height = SizeOfDimension(output, 1);
  auto filter_width = SizeOfDimension(weights, 2);
  auto filter_height = SizeOfDimension(weights, 1);

  // Matching GetWindowedOutputSize in TensorFlow.
  auto padding = data->padding;
  auto computeOutSize = [padding](int imageSize, int filterSize,
                                  int stride) -> int {
    return padding == kTfLitePaddingSame
               ? (imageSize + stride - 1) / stride
               : padding == kTfLitePaddingValid
                     ? (imageSize - filterSize + stride) / stride
                     : 0;
  };
  int expected_width = computeOutSize(output_width, filter_width, data->stride_width);
  int expected_height = computeOutSize(output_height, filter_height, data->stride_height);

  TF_LITE_ENSURE_EQ(context, SizeOfDimension(input, 2), expected_width);
  TF_LITE_ENSURE_EQ(context, SizeOfDimension(input, 1), expected_height);

  return ret;
}

void EvalQuantizedNbits(TfLiteContext* context, TfLiteNode* node,
                 OpData* data, const TfLiteTensor* input, const TfLiteTensor* filter,
                 const TfLiteTensor* bias, TfLiteTensor* output) {
  auto input_offset = input->params.zero_point;
  auto filter_offset = filter->params.zero_point;
  auto output_offset = output->params.zero_point;

#define TFLITE_NBIT_TRANSPOSE_CONV(input_type, filter_type, output_type)                \
  do {                                                                                  \
    reference_ops::mtk::nbits::TransposeConv<input_type, filter_type, output_type>(       \
        GetTensorData<uint8_t>(input), GetTensorDims(input), input_offset,              \
        GetTensorData<uint8_t>(filter), GetTensorDims(filter), filter_offset,           \
        GetTensorData<int32_t>(bias), GetTensorDims(bias), data->stride_width,          \
        data->stride_height, data->paddingValues.width, data->paddingValues.height,     \
        output_offset, data->output_multiplier, data->output_shift,                     \
        data->output_activation_min, data->output_activation_max,                       \
        GetTensorData<uint8_t>(output), GetTensorDims(output));                         \
  } while(0)

  const auto itype = input->type;
  const auto ftype = filter->type;
  const auto otype = output->type;

  if (itype == kTfLiteUInt8 && ftype == kTfLiteUInt8 && otype == kTfLiteUInt8)
    TFLITE_NBIT_TRANSPOSE_CONV(uint8_t, uint8_t, uint8_t);
  else if (itype == kTfLiteUInt8 && ftype == kTfLiteUInt8 && otype == kTfLiteInt16)
    TFLITE_NBIT_TRANSPOSE_CONV(uint8_t, uint8_t, int16_t);
  else if (itype == kTfLiteUInt8 && ftype == kTfLiteInt16 && otype == kTfLiteUInt8)
    TFLITE_NBIT_TRANSPOSE_CONV(uint8_t, int16_t, uint8_t);
  else if (itype == kTfLiteUInt8 && ftype == kTfLiteInt16 && otype == kTfLiteInt16)
    TFLITE_NBIT_TRANSPOSE_CONV(uint8_t, int16_t, int16_t);
  else if (itype == kTfLiteInt16 && ftype == kTfLiteUInt8 && otype == kTfLiteUInt8)
    TFLITE_NBIT_TRANSPOSE_CONV(int16_t, uint8_t, uint8_t);
  else if (itype == kTfLiteInt16 && ftype == kTfLiteUInt8 && otype == kTfLiteInt16)
    TFLITE_NBIT_TRANSPOSE_CONV(int16_t, uint8_t, int16_t);
  else if (itype == kTfLiteInt16 && ftype == kTfLiteInt16 && otype == kTfLiteUInt8)
    TFLITE_NBIT_TRANSPOSE_CONV(int16_t, int16_t, uint8_t);
  else if (itype == kTfLiteInt16 && ftype == kTfLiteInt16 && otype == kTfLiteInt16)
    TFLITE_NBIT_TRANSPOSE_CONV(int16_t, int16_t, int16_t);
}

template <KernelType kernel_type>
void EvalQuantized(TfLiteContext* context, TfLiteNode* node,
                 OpData* data, const TfLiteTensor* input, const TfLiteTensor* filter,
                 const TfLiteTensor* bias, TfLiteTensor* output) {
  auto input_offset = input->params.zero_point;
  auto filter_offset = filter->params.zero_point;
  auto output_offset = output->params.zero_point;

  if (kernel_type == kReference) {
    reference_ops::mtk::TransposeConv(GetTensorData<uint8_t>(input), GetTensorDims(input), input_offset,
    GetTensorData<uint8_t>(filter), GetTensorDims(filter), filter_offset,
    GetTensorData<int32_t>(bias), GetTensorDims(bias), data->stride_width,
    data->stride_height, data->paddingValues.width, data->paddingValues.height,
    output_offset, data->output_multiplier, data->output_shift,
    data->output_activation_min, data->output_activation_max,
    GetTensorData<uint8_t>(output), GetTensorDims(output));
  } else {
    // TDDO: Optimized version.
    context->ReportError(context, "Optimized quantized implementation is not currently supported.");
  }
}

template <KernelType kernel_type>
void EvalFloat(TfLiteContext* context, TfLiteNode* node,
               OpData* data, const TfLiteTensor* input, const TfLiteTensor* filter,
               const TfLiteTensor* bias, TfLiteTensor* output) {
  float output_activation_min, output_activation_max;
  CalculateActivationRange(data->activation, &output_activation_min,
                           &output_activation_max);
  const float* filter_data = GetTensorData<float>(filter);

  if (kernel_type == kReference) {
    reference_ops::mtk::TransposeConv(
      GetTensorData<float>(input), GetTensorDims(input), filter_data,
      GetTensorDims(filter), GetTensorData<float>(bias), GetTensorDims(bias),
      data->stride_width, data->stride_height, data->paddingValues.width,
      data->paddingValues.height, output_activation_min, output_activation_max,
      GetTensorData<float>(output), GetTensorDims(output));
  } else {
    // TDDO: Optimized version.
    context->ReportError(context, "Optimized quantized implementation is not currently supported.");
  }
}

template <KernelType kernel_type>
TfLiteStatus Eval(TfLiteContext* context, TfLiteNode* node) {
  const TfLiteTensor* output_shape =
      GetInput(context, node, kOutputShapeTensor);
  const TfLiteTensor* weights = GetInput(context, node, kWeightsTensor);
  const TfLiteTensor* input = GetInput(context, node, kDataInputTensor);
  TfLiteTensor* output = GetOutput(context, node, kOutputTensor);
  bool hasBias = NumInputs(node) == 4;
  const TfLiteTensor* bias =
    hasBias ? GetInput(context, node, kBiasTensor) : nullptr;

  OpData* data = reinterpret_cast<OpData*>(node->user_data);

  if (IsDynamicTensor(output)) {
    TF_LITE_ENSURE_OK(context,
                      ResizeOutputShape(context, output_shape, output));
  }

  // Get height and width of the output image.
  const int width = SizeOfDimension(output, 2);
  const int height = SizeOfDimension(output, 1);
  const int filter_width = SizeOfDimension(weights, 2);
  const int filter_height = SizeOfDimension(weights, 1);

  const int stride_width = data->stride_width;
  const int stride_height = data->stride_height;

  data->paddingValues =
      ComputePaddingHeightWidth(stride_height, stride_width, 1, height, width,
                                filter_height, filter_width, data->padding);

  switch (input->type) {  // Already know in/out types are same.
    case kTfLiteFloat32:
      EvalFloat<kernel_type>(context, node, data, input, weights, bias, output);
      break;
    case kTfLiteInt16:
    case kTfLiteUInt8:
      if (data->need_nbits_impl) {
        EvalQuantizedNbits(context, node, data, input, weights, bias, output);
        break;
      }
      EvalQuantized<kernel_type>(context, node, data, input, weights, bias, output);
      break;
    default:
      context->ReportError(context, "Type %d, not currently supported.",
                           input->type);
      return kTfLiteError;
  }
  return kTfLiteOk;
}

}  // namespace transpose_conv

TfLiteRegistration* Register_MTK_TRANSPOSE_CONV_REF() {
  static TfLiteRegistration r = {transpose_conv::Init,
                                 transpose_conv::Free,
                                 transpose_conv::Prepare,
                                 transpose_conv::Eval<transpose_conv::kReference>};
  r.custom_name = strdup("MTK_TRANSPOSE_CONV");
  return &r;
}

TfLiteRegistration* Register_MTK_TRANSPOSE_CONV() {
#if 0
  static TfLiteRegistration r = {transpose_conv::Init,
                                 transpose_conv::Free,
                                 transpose_conv::Prepare,
                                 transpose_conv::Eval<transpose_conv::kReference>};
  r.custom_name = strdup("MTK_TRANSPOSE_CONV");
  return &r;
#endif
  return Register_MTK_TRANSPOSE_CONV_REF();
}

}  // namespace mtk
}  // namespace ops

int32_t add_transpose_conv_params(ANeuralNetworksModel* nn_model,
        std::vector<uint32_t>& augmented_inputs, uint32_t& next_id, void* data) {
  auto add_scalar_int32 = [&nn_model, &augmented_inputs,
                           &next_id](int value) {
    ANeuralNetworksOperandType operand_type{.type = ANEURALNETWORKS_INT32};
    CHECK_NN(ANeuralNetworksModel_addOperand(nn_model, &operand_type))
    CHECK_NN(ANeuralNetworksModel_setOperandValue(nn_model, next_id, &value,
                                                  sizeof(int32_t)))
    augmented_inputs.push_back(next_id++);
  };

  auto builtin = reinterpret_cast<ops::mtk::transpose_conv::OpData*>(data);
  add_scalar_int32(builtin->padding);
  add_scalar_int32(builtin->stride_width);
  add_scalar_int32(builtin->stride_height);
  add_scalar_int32(builtin->activation);
  add_scalar_int32(builtin->depth_multiplier);
  add_scalar_int32(builtin->dilation_width_factor);
  add_scalar_int32(builtin->dilation_height_factor);
  return mtk::Hash("transposeconvmtk");
}

}  // namespace tflite
