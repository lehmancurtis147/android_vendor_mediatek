/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkExecutionBuilder"

#include "MtkExecutionBuilder.h"
#include "MtkCompilationBuilder.h"

// Profiler
#include <iostream>
#include <fstream>
#include <sstream>
#include "MtkUtils.h"
#include "Manager.h"
#include "GraphDump.h"
#include "Options.h"

namespace android {
namespace nn {

MtkExecutionBuilder::MtkExecutionBuilder(const CompilationBuilder* compilation) :
        ExecutionBuilder(compilation), mCurrentStep(0),
        mPartitionExt(reinterpret_cast<const MtkCompilationBuilder*>
        (compilation)->getPartitionExtType()) {
    VLOG(EXECUTION) << "MtkExecutionBuilder::MtkExecutionBuilder";
}

MtkExecutionBuilder::~MtkExecutionBuilder() {
    clearProfilerInfo();
}

// Profiler Start
int MtkExecutionBuilder::startProfile(const char* device) {
    VLOG(EXECUTION) << "Start Profile: this = " << this;

    size_t count = mProfilerData.size();
    mProfilerData.resize(count+1);

    auto& data = mProfilerData[count];
    data.step = mCurrentStep;
    if (device != NULL) {
        data.deviceTime.devName = device;
    } else {
        data.deviceTime.devName = "";
    }
    data.deviceTime.delta = getCurrentTimeUs();

    VLOG(EXECUTION) << "startProfile end";
    return ANEURALNETWORKS_NO_ERROR;
}

int MtkExecutionBuilder::stopProfile(const char* request, int err) {
    const ModelBuilder* model = nullptr;
    std::string name = "";
    MemoryTracker memories;
    std::vector<ModelArgumentInfo> outputs;
    double currentTime = getCurrentTimeUs();

    VLOG(EXECUTION) << "Stop Profile: this = " << this;
    const ExecutionPlan *plan = getPlan();
    if (plan->forTest_getKind() == ExecutionPlan::Kind::SIMPLE) {
        // Got Device Name
        name = plan->forTest_simpleGetDevice() == nullptr ?
                "CPU" : plan->forTest_simpleGetDevice()->getName();

        // Got Model
        model = getModel();

        // For Execution Result
        outputs = getOutputs();
        memories = getMemories();
    } else if (plan->forTest_getKind() == ExecutionPlan::Kind::COMPOUND) {
        const auto& steps = plan->forTest_compoundGetSteps();
        // Got Device Name
        name = steps[mCurrentStep]->getDevice() == nullptr ?
                "CPU" : steps[mCurrentStep]->getDevice()->getName();

        // Got Model
        model = steps[mCurrentStep]->getSubModel();

        // For Execution Result
        outputs = mTempOutputs;
        memories = mTempMemoryTracker;
    } else {
        // TODO: Find a way to get device name
        /* In this case, the device name is not passed to execution builder
        * we also can't get the information from plan(no plan).
        *
        * Actually, developer can got the executed device from previous log,
        * "ExecutionBuilder::startCompute (without plan) on xxx"
        *  We think maybe it's enough for debug.
        */
        name = "unknown";

        // Got Model
        model = getModel();

        // For Execution Result
        outputs = getOutputs();
        memories = getMemories();
    }

    size_t count = mProfilerData.size();
    auto& data = mProfilerData[count-1];
    if (err == ANEURALNETWORKS_NO_ERROR) {
        data.success = true;
    } else {
        data.success = false;
    }

    // Model
    Model hidlModel;
    model->setHidlModel(&hidlModel);

    // Execution Time
    prepareExecutionTime(model, name, currentTime);

    // Execution Result
    prepareExecutionResult(model, memories, outputs);

    // Print to log and file
    char strPath[PROPERTY_VALUE_MAX];
    bool toFile = false;
    std::ofstream kFile;
    std::string kString;
    if (getDumpPath(strPath)) {
        kFile.open(strPath, std::ofstream::out | std::ofstream::app);
        if (kFile.is_open()) {
            toFile = true;
        }
    }

    // Print to console
    printf("****************** Profiler Start ******************\n");
    printf("Execution Step   : %d\n", data.step);
    printf("Execution Result : %d\n", data.success);

    // Print to log
    VLOG(EXECUTION) << "****************** Profiler Start ******************";
    VLOG(EXECUTION) << "Execution Step   : " << data.step;
    VLOG(EXECUTION) << "Execution Result : " << data.success;

    // Dump to file
    if (toFile) {
        kString += std::string("\n****************** Profiler Start ******************\n")
                + std::string("Execution Step   : ") + std::to_string(data.step)
                + std::string("\nExecution Result : ") + std::to_string(data.success)
                + std::string("\n");
    }

    dumpModel(kString, hidlModel, toFile);
    dumpRequest(kString, request, toFile);
    dumpExecutionTime(kString, toFile);
    dumpExecutionResult(model, kString, toFile);

    if (toFile) {
        if (isDumpModelEnable()) {
            kString += "\n";
            kFile << kString;
            graphDump("Graph", hidlModel, kFile);
            kString = "";
        }
        kString += std::string("\n****************** Profiler End   ******************\n");
        kFile << kString;
        kFile.close();
    }
    VLOG(EXECUTION) << "****************** Profiler End   ******************";
    printf("****************** Profiler End   ******************\n");
    return ANEURALNETWORKS_NO_ERROR;
}

void MtkExecutionBuilder::setTemporaryData(std::shared_ptr<StepExecutor> executor) {
    mTempOutputs = executor->getOutputs();
    mTempMemoryTracker = executor->getMemories();
}

int MtkExecutionBuilder::clearProfilerInfo() {
    mCurrentStep = 0;

    for (auto& data : mProfilerData) {
        for (auto& result : data.opResults) {
            if (result.buffer) {
                delete result.buffer;
            }
        }
        data.opResults.clear();
    }
    mProfilerData.clear();
    return ANEURALNETWORKS_NO_ERROR;
}

int MtkExecutionBuilder::getProfilerResult(std::vector<ProfilerResult> *result) {
    *result = mProfilerData;
    return ANEURALNETWORKS_NO_ERROR;
}

int MtkExecutionBuilder::dumpModel(std::string &kString, Model model, bool toFile) {
    if (!toFile) {
        return ANEURALNETWORKS_BAD_STATE;
    }

    kString += std::string("\n-------------------- Model -------------------------\n")
            + toString(model) + std::string("\n");

    return ANEURALNETWORKS_NO_ERROR;
}

int MtkExecutionBuilder::dumpRequest(std::string &kString, const char* request, bool toFile) {
    if (!toFile) {
        return ANEURALNETWORKS_BAD_STATE;
    }

    kString += std::string("\n-------------------- Request -----------------------\n")
            + request + std::string("\n");

    return ANEURALNETWORKS_NO_ERROR;
}

void MtkExecutionBuilder::dumpExecutionTime(std::string &kString, bool toFile) {
    size_t count = mProfilerData.size();
    auto& data = mProfilerData[count-1];

    if (toFile) {
        kString += std::string("\n-------------------- Execution Time ----------------\n")
                + std::string("Device Name      : ") + data.deviceTime.devName
                + std::string("\nOperations       : ") + data.deviceTime.opName
                + std::string("\nSpent Time       : ") + std::to_string(data.deviceTime.delta)
                + std::string(" us\n");
    }
    VLOG(EXECUTION) << "Device Name      : " << data.deviceTime.devName;
    VLOG(EXECUTION) << "Operations       : " << data.deviceTime.opName;
    VLOG(EXECUTION) << "Spent Time       : " << data.deviceTime.delta << " us";
    printf("Device Name      : %s\n", data.deviceTime.devName.c_str());
    printf("Operations       : %s\n", data.deviceTime.opName.c_str());
    printf("Spent Time       : %f us\n", data.deviceTime.delta);
}

int MtkExecutionBuilder::dumpExecutionResult(
        const ModelBuilder* model, std::string &kString, bool toFile) {
    if (!toFile) {
        return ANEURALNETWORKS_BAD_STATE;
    }
    size_t count = mProfilerData.size();
    auto& data = mProfilerData[count-1];
    std::vector<ExecResult> opResults = data.opResults;

    uint32_t size;
    auto dumpResult = [&](auto out) {
        for (uint32_t j = 0; j < (size - 1); j++) {
            kString += std::to_string(out[j]) + ",";
        }
        // Last element
        kString += std::to_string(out[size-1]) + std::string("\n");
    };

    for (uint32_t i = 0; i < opResults.size(); i++) {
        auto &result = opResults[i];
        size = result.size;

        kString += std::string("\n-------------------- Output [") + std::to_string(i)
                + std::string("] --------------------\n")
                + std::string("\nOperand detail   : ") + toString(model->getOutputOperand(i))
                + std::string("\nOperand Type     : ") + std::to_string(result.operandType)
                + std::string("\nSize             : ") + std::to_string(result.size)
                + std::string("\nResult           : ");

        OperandType operandType = static_cast<OperandType>(result.operandType);
        switch (operandType) {
            case OperandType::TENSOR_FLOAT32:
            case OperandType::FLOAT32: {
                float* output = (float*) result.buffer;
                dumpResult(output);
            } break;
            case OperandType::TENSOR_INT32:
            case OperandType::INT32: {
                int* output = (int*) result.buffer;
                dumpResult(output);
            } break;
            case OperandType::UINT32: {
                uint32_t* output = (uint32_t*) result.buffer;
                dumpResult(output);
            } break;
            case OperandType::TENSOR_QUANT8_ASYMM: {
                uint8_t* output = (uint8_t*) result.buffer;
                dumpResult(output);
            } break;
            default:
                uint8_t* output = (uint8_t*) result.buffer;
                dumpResult(output);
                break;
        }
     }
    return ANEURALNETWORKS_NO_ERROR;
}

void MtkExecutionBuilder::prepareExecutionTime(
        const ModelBuilder* model, std::string deviceName, double currentTime) {
    size_t count = mProfilerData.size();
    auto& data = mProfilerData[count-1];

    VLOG(EXECUTION) << "saveExecutionTime";

    // Execution time
    auto& operations = model->getOperations();
    std::string ops = "";
    uint32_t opCount = operations.size();
    for (uint32_t i = 0; i < opCount; i++) {
        ops += getOperationName(operations[i].type);
        if (i < opCount - 1) {
            ops += ":";
        }
    }
    if (data.deviceTime.devName == "") {
        data.deviceTime.devName = deviceName;
    }
    data.deviceTime.opName = ops;

    if (data.success) {
        data.deviceTime.delta = currentTime - data.deviceTime.delta;
    } else {
        data.deviceTime.delta = 0;
    }
}

void MtkExecutionBuilder::prepareExecutionResult(const ModelBuilder* model,
                                                 const MemoryTracker memories,
                                                 std::vector<ModelArgumentInfo> outputs) {
    size_t count = mProfilerData.size();
    auto& data = mProfilerData[count-1];

    if (!data.success) {
        VLOG(EXECUTION) << "Previous operation is failed, don't store";
        return;
    }

    VLOG(EXECUTION) << "prepareExecutionResult";

    for (uint32_t i = 0; i < model->outputCount(); i++) {
        Operand operand = model->getOutputOperand(i);
        ModelArgumentInfo arg = outputs[i];

        ExecResult result;
        result.operandType = static_cast<int32_t>(operand.type);

        if (copyArgumentBuffer(&result.buffer, memories, arg) != ANEURALNETWORKS_NO_ERROR) {
            result.size = 0;
            continue;
        }

        switch (operand.type) {
            case OperandType::TENSOR_FLOAT32:
            case OperandType::FLOAT32: {
                result.size = arg.locationAndLength.length / sizeof(float);
            } break;
            case OperandType::TENSOR_INT32:
            case OperandType::INT32: {
                result.size = arg.locationAndLength.length / sizeof(int);
            } break;
            case OperandType::UINT32: {
                result.size = arg.locationAndLength.length / sizeof(uint32_t);
            } break;
            case OperandType::TENSOR_QUANT8_ASYMM: {
                result.size = arg.locationAndLength.length / sizeof(uint8_t);
            } break;
            default:
                break;
        }
        data.opResults.push_back(result);
    }
}

int MtkExecutionBuilder::copyArgumentBuffer(uint8_t** buffers,
        const MemoryTracker memories, ModelArgumentInfo arg) {
    if (arg.state == ModelArgumentInfo::POINTER) {
        LOG(DEBUG) << "POINTER";
        *buffers = new uint8_t[arg.locationAndLength.length];
        if (!*buffers) {
            LOG(ERROR) << "out of memory";
            return ANEURALNETWORKS_UNEXPECTED_NULL;
        } else {
            memcpy(*buffers, reinterpret_cast<uint8_t*>(arg.buffer),
                    arg.locationAndLength.length);
        }
    } else if (arg.state == ModelArgumentInfo::MEMORY) {
        LOG(DEBUG) << "MEMORY";
        if (arg.locationAndLength.poolIndex >= memories.size()) {
            LOG(ERROR) << "illegal arg.locationAndLength.poolIndex("
                    << arg.locationAndLength.poolIndex
                    << ") >= memories.size()(" << memories.size() << ")";
            return ANEURALNETWORKS_BAD_DATA;
        }
        uint8_t* buffer = nullptr;
        const Memory *mem = memories[arg.locationAndLength.poolIndex];
        int ret = mem->getPointer(&buffer);
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            LOG(ERROR) << "failed to get pointer from memory";
            return ret;
        }
        *buffers = new uint8_t[arg.locationAndLength.length];
        if (!*buffers) {
            LOG(ERROR) << "out of memory";
            return ANEURALNETWORKS_UNEXPECTED_NULL;
        } else {
            memcpy(*buffers, &buffer[arg.locationAndLength.offset],
                    arg.locationAndLength.length);
        }
    }
    return ANEURALNETWORKS_NO_ERROR;
}
// Profiler end

}  // namespace nn
}  // namespace android
