/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "NeuroPilotLegacy"

#include "MtkCompilationBuilder.h"
#include "MtkExecutionBuilder.h"
#include "NeuralNetworks.h"
#include "NeuroPilotLegacy.h"

#include "Options.h"

static std::string gDevName;
static std::string gOpName;

using namespace android::nn;

int ANeuroPilotCompilationLegacy_enableProfiler(
        ANeuralNetworksCompilation* compilation, uint32_t type) {
    MtkCompilationBuilder* c = reinterpret_cast<MtkCompilationBuilder*>(compilation);
    if (type == ANEURALNETWORKS_PROFILER_OPERATION) {
        return c->setPartitionExtType(ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION);
    } else {
        return c->setPartitionExtType(ANEUROPILOT_PARTITIONING_EXTENSION_NONE);
    }
}

int ANeuroPilotExecutionLegacy_getProfilerInfoCount(
        ANeuralNetworksExecution *execution, uint32_t *count) {
    if (!isProfilerSupported()) {
        LOG(ERROR) << "Profiler is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }
    MtkExecutionBuilder* c = reinterpret_cast<MtkExecutionBuilder*>(execution);
    std::vector<ProfilerResult> results;
    c->getProfilerResult(&results);
    uint32_t successCnt = 0;
    for (auto &result : results) {
        if (result.success) {
            successCnt++;
        }
    }
    *count = successCnt;
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotExecutionLegacy_getProfilerInfo(
        ANeuralNetworksExecution *execution, uint32_t index, ProfilerInfo* info) {
    if (!isProfilerSupported()) {
        LOG(ERROR) << "Profiler is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    MtkExecutionBuilder* c = reinterpret_cast<MtkExecutionBuilder*>(execution);
    std::vector<ProfilerResult> results;
    c->getProfilerResult(&results);
    uint32_t count = 0;

    for (auto &result : results) {
        if (result.success) {
            if (count == index) {
                gDevName = result.deviceTime.devName;
                gOpName = result.deviceTime.opName;
                info->devName = gDevName.c_str();
                info->opName = gOpName.c_str();
                info->delta = result.deviceTime.delta;
            }
            count++;
        }
    }
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotExecutionLegacy_keepOperations(
        ANeuralNetworksExecution *execution, uint32_t* list, uint32_t listSize) {
    if (!isProfilerSupported()) {
        LOG(ERROR) << "Profiler is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    if (!execution) {
        LOG(ERROR) << "ANeuralNetworksExecution_keepOperation passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    if (!list) {
        LOG(ERROR) << "list is a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkExecutionBuilder *e = reinterpret_cast<MtkExecutionBuilder*>(execution);

    const ModelBuilder* model = e->getModel();
    if (listSize > model->operationCount()) {
        LOG(ERROR) << "the number of keepped operations is bigger model's";
        return ANEURALNETWORKS_BAD_DATA;
    }

    if (e->getPartitionExtType() != ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION) {
        LOG(ERROR) << "the debugger only allow to enable "
                << "ANEURALNETWORKS_PROFILER_OPERATION mode";
        return ANEURALNETWORKS_BAD_DATA;
    }
    e->mKeeppingIndex.clear();
    e->mKeeppingIndex.resize(listSize);
    for (uint32_t i = 0; i < listSize; i++) {
        if (list[i] >= model->operationCount()) {
            return ANEURALNETWORKS_BAD_DATA;
        }
        e->mKeeppingIndex[i] = list[i];
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotExecutionLegacy_getOperationOutput(
        ANeuralNetworksExecution *execution, uint32_t keeppingIndex,
        uint32_t outputIndex, uint8_t** buffer) {
    if (!isProfilerSupported()) {
        LOG(ERROR) << "Profiler is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    if (!execution) {
        LOG(ERROR) << "ANeuralNetworksExecution_printOperation passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    if (buffer == nullptr) {
        LOG(ERROR) << "illegal buffer pointer";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkExecutionBuilder *e = reinterpret_cast<MtkExecutionBuilder*>(execution);


    if (e->getPartitionExtType() != ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION) {
        LOG(ERROR) << "the debugger only allow to enable "
                << "ANEURALNETWORKS_PROFILER_OPERATION mode";
        return ANEURALNETWORKS_BAD_STATE;
    }

    *buffer = nullptr;
    if (keeppingIndex >= e->mKeeppingIndex.size()) {
        LOG(ERROR) << "illegal keeppingIndex : " << keeppingIndex;
        return ANEURALNETWORKS_BAD_DATA;
    }

    std::vector<ProfilerResult> results;
    e->getProfilerResult(&results);
    uint32_t count = 0;
    uint32_t opIndex = e->mKeeppingIndex[keeppingIndex];
    for (auto &result : results) {
        if (result.success) {
            if (count == opIndex) {
                if (outputIndex >= result.opResults.size()) {
                    LOG(ERROR) << "illegal outputIndex : " << outputIndex;
                    return ANEURALNETWORKS_BAD_DATA;
                }

                // LOG(DEBUG) << "getOperationOutput : " << toString(opInfo.op);
                *buffer = result.opResults[outputIndex].buffer;
                return ANEURALNETWORKS_NO_ERROR;
            }
            count++;
        }
    }
    return ANEURALNETWORKS_BAD_DATA;
}

