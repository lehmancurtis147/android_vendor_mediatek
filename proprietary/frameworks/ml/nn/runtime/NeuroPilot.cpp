/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "NeuroPilot"

#include "Manager.h"
#include "MtkCompilationBuilder.h"
#include "MtkExecutionBuilder.h"
#include "MtkModelBuilder.h"
#include "MtkUtils.h"
#include "MtkMemoryExt.h"
#include "NeuroPilot.h"
#include "Options.h"

using namespace android::nn;

int ANeuroPilotModel_create(ANeuralNetworksModel** model) {
    if (!model) {
        LOG(ERROR) << "ANeuralNetworksModel_create passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    ModelBuilder* m = new (std::nothrow) MtkModelBuilder();
    if (m == nullptr) {
        *model = nullptr;
        return ANEURALNETWORKS_OUT_OF_MEMORY;
    }
    *model = reinterpret_cast<ANeuralNetworksModel*>(m);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotCompilation_create(ANeuralNetworksModel* model,
                                      ANeuralNetworksCompilation** compilation) {
    if (!model || !compilation) {
        LOG(ERROR) << "ANeuralNetworksCompilation_create passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkModelBuilder* m = reinterpret_cast<MtkModelBuilder*>(model);
    CompilationBuilder* c = nullptr;
    int result = m->createCompilation(&c);
    *compilation = reinterpret_cast<ANeuralNetworksCompilation*>(c);
    return result;
}

int ANeuroPilotExecution_create(ANeuralNetworksCompilation* compilation,
                                    ANeuralNetworksExecution** execution) {
    if (!compilation || !execution) {
        LOG(ERROR) << "ANeuralNetworksExecution_create passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkCompilationBuilder* c = reinterpret_cast<MtkCompilationBuilder*>(compilation);
    ExecutionBuilder* r = nullptr;
    int result = c->createExecution(&r);
    *execution = reinterpret_cast<ANeuralNetworksExecution*>(r);
    return result;
}

/// M: Bind operation @{
int ANeuroPilotModel_getAvailableDeviceName(std::string &device) {
    if (!isBindOperationSupported()) {
        NP_VLOG << "Bind operation function is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    device = "cpunn:";

    for (auto dev : DeviceManager::get()->getDrivers()) {
        device += dev->getName() + ":";
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotModel_getAvailableDevice(uint32_t* deviceId) {
    if (!isBindOperationSupported()) {
        NP_VLOG << "Bind operation function is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    // CPU
    *deviceId = ANEUROPILOT_CPU;
    for (auto dev : DeviceManager::get()->getDrivers()) {
        *deviceId = *deviceId | getDeviceId(dev->getName().c_str());
    }
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotModel_bindOperationToDeviceByName(ANeuralNetworksModel* model, uint32_t operation,
        const char* deviceName) {
    uint32_t deviceId = getDeviceId(deviceName);
    return ANeuroPilotModel_bindOperationToDevice(model, operation, deviceId);
}

int ANeuroPilotModel_bindOperationToDevice(
        ANeuralNetworksModel* model, uint32_t operation, uint32_t deviceId) {
    if (!isBindOperationSupported()) {
        NP_VLOG << "Bind operation function is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    if (!model) {
        LOG(ERROR) << "ANeuroPilotModel_bindOperationToDevice passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkModelBuilder *m = reinterpret_cast<MtkModelBuilder*>(model);
    int n = m->bindOperationToDevice(operation, deviceId);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        return n;
    }

    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotModel_bindAllOperationsToDeviceByName(ANeuralNetworksModel* model,
        const char* deviceName) {
    uint32_t deviceId = getDeviceId(deviceName);
    return ANeuroPilotModel_bindAllOperationsToDevice(model, deviceId);
}


int ANeuroPilotModel_bindAllOperationsToDevice(ANeuralNetworksModel* model, uint32_t device) {
    if (!isBindOperationSupported()) {
        NP_VLOG << "Bind operation function is off, can't use the function";
        return ANEURALNETWORKS_BAD_STATE;
    }

    if (!model) {
        LOG(ERROR) << "ANeuralNetworksModel_bindOperationToDevice passed a nullptr";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    MtkModelBuilder *m = reinterpret_cast<MtkModelBuilder*>(model);
    int n = m->bindAllOperationsToDevice(device);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        return n;
    }

    return ANEURALNETWORKS_NO_ERROR;
}
/// M: Bind operation @}

/// Partition Extension @{
int ANeuroPilotCompilation_setPartitionExtType(ANeuralNetworksCompilation* compilation, uint32_t type) {
    MtkCompilationBuilder* c = reinterpret_cast<MtkCompilationBuilder*>(compilation);
    return c->setPartitionExtType(type);
}
/// Partition Extension @}

/// Profiler @{
int ANeuroPilotExecution_setCurrentExecutionStep(
        ANeuralNetworksExecution *execution, uint32_t step) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }
    MtkExecutionBuilder* e = reinterpret_cast<MtkExecutionBuilder*>(execution);
    e->setCurrentExecutionStep(step);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotExecution_setExecution(std::thread::id tid, ANeuralNetworksExecution *execution) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    MtkExecutionBuilder* e = reinterpret_cast<MtkExecutionBuilder*>(execution);
    return setExecution(tid, e);
}

int ANeuroPilotExecution_eraseExecution(std::thread::id tid) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    return eraseExecution(tid);
}

int ANeuroPilotExecution_startProfile(std::thread::id tid, const char* device) {
    // Dump Runtime hash
    VLOG(EXECUTION) << "NeuroPilot Runtime Hash: " << get_nn_hash();

    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    return startProfile(tid, device);
}

int ANeuroPilotExecution_stopProfile(std::thread::id tid, const char* request, int err) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    return stopProfile(tid, request, err);
}

int ANeuroPilotExecution_clearProfilerInfo(ANeuralNetworksExecution *execution) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    MtkExecutionBuilder* e = reinterpret_cast<MtkExecutionBuilder*>(execution);
    return e->clearProfilerInfo();
}

int ANeuroPilotExecution_getProfilerInfo(
        ANeuralNetworksExecution *execution, std::vector<ProfilerResult> *result) {
    if (!isProfilerSupported()) {
        NP_VLOG << "Profiler not support";
        return ANEURALNETWORKS_BAD_STATE;
    }

    return reinterpret_cast<MtkExecutionBuilder*>(execution)->getProfilerResult(result);
}
/// @}

/// M: Sys Trace @{
int ANeuroPilotUtils_sysTraceStart(const char* name) {
    sysTraceStart(name);
    return ANEURALNETWORKS_NO_ERROR;
}

int ANeuroPilotUtils_sysTraceStop() {
    sysTraceStop();
    return ANEURALNETWORKS_NO_ERROR;
}
/// M: Sys Trace @}

/// M: Utils @{
int ANeuroPilotUtils_setCpuOnly(bool onlyCpu) {
    LOG(INFO) << "ANeuroPilotUtils_setCpuOnly, onlyCPU : "<< onlyCpu;
    DeviceManager::get()->setUseCpuOnly(onlyCpu);
    return ANEURALNETWORKS_NO_ERROR;
}

bool ANeuroPilotUtils_forbidCpuExecution() {
    bool ret = false;
    if (!isFallbackCpuSupported()) {
        LOG(INFO) << "Disable CPU fallback";
        printf("Disable CPU fallback\n");
        ret = true;
    }
    return ret;
}
/// @}

/// M: Shared Memory Extension @{
int ANeuroPilotMemory_create(size_t size, ANeuralNetworksMemory **memory) {
    int ret = ANEURALNETWORKS_INCOMPLETE;
    if (isIonMemorySupported()) {
        VLOG(EXECUTION) << "MemoryExt::create ion";
        ret = createIonMemory(size, memory);
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            LOG(ERROR) << "MemoryExt::create ion error";
        }
    } else {
        VLOG(EXECUTION) << "MemoryExt::No extended type supported, use AOSP type";
    }
    return ret;
}


int ANeuroPilotMemory_getPointer(uint8_t** buffer, ANeuralNetworksMemory **memory) {
    MemoryExt *mem = reinterpret_cast<MemoryExt *>(*memory);
    int ret = ANEURALNETWORKS_INCOMPLETE;
    if (mem->getMemoryType() == ANEUROPILOT_MEMORY_ION) {
        ret = getIonPointer(buffer, memory);
    } else {
        VLOG(EXECUTION) << "ANeuroPilotMemory_getPointer: not extended type.";
    }
    return ret;
}

int ANeuroPilotMemory_freeMemory(ANeuralNetworksMemory **memory) {
    MemoryExt *mem = reinterpret_cast<MemoryExt *>(*memory);
    int ret = ANEURALNETWORKS_INCOMPLETE;
    if (mem->getMemoryType() == ANEUROPILOT_MEMORY_ION) {
        VLOG(EXECUTION) << "freeIonMemory";
        ret = freeIonMemory(memory);
    } else {
        VLOG(EXECUTION) << "ANeuroPilotMemory_freeMemory: not extended type.";
    }
    return ret;
}
/// M: Shared Memory Extension @}

