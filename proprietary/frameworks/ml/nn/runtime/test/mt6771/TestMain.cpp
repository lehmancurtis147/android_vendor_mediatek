/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "NeuralNetworksWrapper.h"

#ifndef NNTEST_ONLY_PUBLIC_API
#include "Utils.h"
#endif

#include <gtest/gtest.h>
#include <cutils/properties.h>

using namespace android::nn::wrapper;

void setCpuOnly(bool cpuOnly) {
    const char* value = cpuOnly ? "1" : "0";
    property_set("debug.nn.cpuonly", value);
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);

#ifndef NNTEST_ONLY_PUBLIC_API
    android::nn::initVLogMask();
#endif

#if defined(CPU_CTS)
    // Test with the CPU driver only.
    setCpuOnly(true);
#endif

    int n1 = RUN_ALL_TESTS();

#if defined(CPU_CTS)
    // Restore settings.
    setCpuOnly(false);
#endif

    return n1;
}
