// Generated file (from: mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed.mod.py). Do not edit
void CreateModel(Model *model) {
  OperandType type4(Type::INT32, {});
  OperandType type3(Type::TENSOR_FLOAT32, {2, 3});
  OperandType type1(Type::TENSOR_FLOAT32, {3, 10});
  OperandType type2(Type::TENSOR_FLOAT32, {3});
  OperandType type0(Type::TENSOR_FLOAT32, {4, 1, 5, 1});
  // Phase 1, operands
  auto op1 = model->addOperand(&type0);
  auto op2 = model->addOperand(&type1);
  auto b0 = model->addOperand(&type2);
  auto op3 = model->addOperand(&type3);
  auto act = model->addOperand(&type4);
  // Phase 2, operations
  static float op2_init[] = {1.6131302902f, 0.0590406138f, 1.7770792244f, 0.4824579224f, 0.9468421372f, 1.9934023285f, 1.1796773092f, 1.1781081512f, 1.2027892791f, 0.1060474049f, 1.1655493292f, 1.7728376638f, 0.5711190424f, 1.298725547f, 0.5116630966f, 0.9323873446f, 0.6197670253f, 1.6332614771f, 1.8247361622f, 0.3581210411f, 1.5552388189f, 1.1624979001f, 1.0145868245f, 1.1640163874f, 1.8952196161f, 0.6656816524f, 1.5643875863f, 1.4265459722f, 0.3812877888f, 0.3690465995f};
  model->setOperandValue(op2, op2_init, sizeof(float) * 30);
  static float b0_init[] = {0.0639008796f, 1.2992238741f, 0.0458298588f};
  model->setOperandValue(b0, b0_init, sizeof(float) * 3);
  static int32_t act_init[] = {0};
  model->setOperandValue(act, act_init, sizeof(int32_t) * 1);
  model->addOperation(ANEURALNETWORKS_FULLY_CONNECTED, {op1, op2, b0, act}, {op3});
  // Phase 3, inputs and outputs
  model->identifyInputsAndOutputs(
    {op1},
    {op3});
  // Phase 4: set relaxed execution
  model->relaxComputationFloat32toFloat16(true);
  assert(model->isValid());
}

bool is_ignored(int i) {
  static std::set<int> ignore = {};
  return ignore.find(i) != ignore.end();
}
