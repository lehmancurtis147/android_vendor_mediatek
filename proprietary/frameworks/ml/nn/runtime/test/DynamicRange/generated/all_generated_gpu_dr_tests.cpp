// DO NOT EDIT;
// Generated by ml/nn/runtime/test_mtk/DynamicRange/specs/generate_gpu_test.sh

namespace mtk_add_broadcast_float_random_gen_act_0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_broadcast_float_random_gen_act_0_relaxed test
#include "generated/examples/mtk_add_broadcast_float_random_gen_act_0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_broadcast_float_random_gen_act_0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_broadcast_float_random_gen_act_0_relaxed ranges
#include "ranges/mtk_add_broadcast_float_random_gen_act_0_relaxed.diff.cpp"
};
} // namespace mtk_add_broadcast_float_random_gen_act_0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_broadcast_float_random_gen_act_0_relaxed) {
    execute(mtk_add_broadcast_float_random_gen_act_0_relaxed::CreateModel,
            mtk_add_broadcast_float_random_gen_act_0_relaxed::is_ignored,
            mtk_add_broadcast_float_random_gen_act_0_relaxed::examples,
            mtk_add_broadcast_float_random_gen_act_0_relaxed::ranges);
}

namespace mtk_add_broadcast_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_broadcast_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_add_broadcast_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_broadcast_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_broadcast_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_add_broadcast_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_add_broadcast_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_broadcast_float_random_gen_act_1_relaxed) {
    execute(mtk_add_broadcast_float_random_gen_act_1_relaxed::CreateModel,
            mtk_add_broadcast_float_random_gen_act_1_relaxed::is_ignored,
            mtk_add_broadcast_float_random_gen_act_1_relaxed::examples,
            mtk_add_broadcast_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_add_broadcast_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_broadcast_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_add_broadcast_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_broadcast_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_broadcast_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_add_broadcast_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_add_broadcast_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_broadcast_float_random_gen_act_2_relaxed) {
    execute(mtk_add_broadcast_float_random_gen_act_2_relaxed::CreateModel,
            mtk_add_broadcast_float_random_gen_act_2_relaxed::is_ignored,
            mtk_add_broadcast_float_random_gen_act_2_relaxed::examples,
            mtk_add_broadcast_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_add_broadcast_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_broadcast_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_add_broadcast_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_broadcast_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_broadcast_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_add_broadcast_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_add_broadcast_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_broadcast_float_random_gen_act_3_relaxed) {
    execute(mtk_add_broadcast_float_random_gen_act_3_relaxed::CreateModel,
            mtk_add_broadcast_float_random_gen_act_3_relaxed::is_ignored,
            mtk_add_broadcast_float_random_gen_act_3_relaxed::examples,
            mtk_add_broadcast_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_add_float_random_gen_act_0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_float_random_gen_act_0_relaxed test
#include "generated/examples/mtk_add_float_random_gen_act_0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_float_random_gen_act_0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_float_random_gen_act_0_relaxed ranges
#include "ranges/mtk_add_float_random_gen_act_0_relaxed.diff.cpp"
};
} // namespace mtk_add_float_random_gen_act_0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_float_random_gen_act_0_relaxed) {
    execute(mtk_add_float_random_gen_act_0_relaxed::CreateModel,
            mtk_add_float_random_gen_act_0_relaxed::is_ignored,
            mtk_add_float_random_gen_act_0_relaxed::examples,
            mtk_add_float_random_gen_act_0_relaxed::ranges);
}

namespace mtk_add_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_add_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_add_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_add_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_float_random_gen_act_1_relaxed) {
    execute(mtk_add_float_random_gen_act_1_relaxed::CreateModel,
            mtk_add_float_random_gen_act_1_relaxed::is_ignored,
            mtk_add_float_random_gen_act_1_relaxed::examples,
            mtk_add_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_add_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_add_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_add_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_add_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_float_random_gen_act_2_relaxed) {
    execute(mtk_add_float_random_gen_act_2_relaxed::CreateModel,
            mtk_add_float_random_gen_act_2_relaxed::is_ignored,
            mtk_add_float_random_gen_act_2_relaxed::examples,
            mtk_add_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_add_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_add_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_add_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_add_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_add_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_add_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_add_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_add_float_random_gen_act_3_relaxed) {
    execute(mtk_add_float_random_gen_act_3_relaxed::CreateModel,
            mtk_add_float_random_gen_act_3_relaxed::is_ignored,
            mtk_add_float_random_gen_act_3_relaxed::examples,
            mtk_add_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_avg_pool_float_random_gen_filter_4_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_avg_pool_float_random_gen_filter_4_relaxed test
#include "generated/examples/mtk_avg_pool_float_random_gen_filter_4_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_avg_pool_float_random_gen_filter_4_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_avg_pool_float_random_gen_filter_4_relaxed ranges
#include "ranges/mtk_avg_pool_float_random_gen_filter_4_relaxed.diff.cpp"
};
} // namespace mtk_avg_pool_float_random_gen_filter_4_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_avg_pool_float_random_gen_filter_4_relaxed) {
    execute(mtk_avg_pool_float_random_gen_filter_4_relaxed::CreateModel,
            mtk_avg_pool_float_random_gen_filter_4_relaxed::is_ignored,
            mtk_avg_pool_float_random_gen_filter_4_relaxed::examples,
            mtk_avg_pool_float_random_gen_filter_4_relaxed::ranges);
}

namespace mtk_avg_pool_float_random_gen_filter_5_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_avg_pool_float_random_gen_filter_5_relaxed test
#include "generated/examples/mtk_avg_pool_float_random_gen_filter_5_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_avg_pool_float_random_gen_filter_5_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_avg_pool_float_random_gen_filter_5_relaxed ranges
#include "ranges/mtk_avg_pool_float_random_gen_filter_5_relaxed.diff.cpp"
};
} // namespace mtk_avg_pool_float_random_gen_filter_5_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_avg_pool_float_random_gen_filter_5_relaxed) {
    execute(mtk_avg_pool_float_random_gen_filter_5_relaxed::CreateModel,
            mtk_avg_pool_float_random_gen_filter_5_relaxed::is_ignored,
            mtk_avg_pool_float_random_gen_filter_5_relaxed::examples,
            mtk_avg_pool_float_random_gen_filter_5_relaxed::ranges);
}

namespace mtk_conv_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_conv_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_conv_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_conv_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_conv_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_conv_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_conv_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_conv_float_random_gen_act_1_relaxed) {
    execute(mtk_conv_float_random_gen_act_1_relaxed::CreateModel,
            mtk_conv_float_random_gen_act_1_relaxed::is_ignored,
            mtk_conv_float_random_gen_act_1_relaxed::examples,
            mtk_conv_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_conv_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_conv_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_conv_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_conv_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_conv_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_conv_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_conv_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_conv_float_random_gen_act_2_relaxed) {
    execute(mtk_conv_float_random_gen_act_2_relaxed::CreateModel,
            mtk_conv_float_random_gen_act_2_relaxed::is_ignored,
            mtk_conv_float_random_gen_act_2_relaxed::examples,
            mtk_conv_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_conv_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_conv_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_conv_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_conv_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_conv_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_conv_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_conv_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_conv_float_random_gen_act_3_relaxed) {
    execute(mtk_conv_float_random_gen_act_3_relaxed::CreateModel,
            mtk_conv_float_random_gen_act_3_relaxed::is_ignored,
            mtk_conv_float_random_gen_act_3_relaxed::examples,
            mtk_conv_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_conv_float_random_gen_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_conv_float_random_gen_relaxed test
#include "generated/examples/mtk_conv_float_random_gen_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_conv_float_random_gen_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_conv_float_random_gen_relaxed ranges
#include "ranges/mtk_conv_float_random_gen_relaxed.diff.cpp"
};
} // namespace mtk_conv_float_random_gen_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_conv_float_random_gen_relaxed) {
    execute(mtk_conv_float_random_gen_relaxed::CreateModel,
            mtk_conv_float_random_gen_relaxed::is_ignored,
            mtk_conv_float_random_gen_relaxed::examples,
            mtk_conv_float_random_gen_relaxed::ranges);
}

namespace mtk_depthwise_conv2d_float_random_gen_act_0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_0_relaxed test
#include "generated/examples/mtk_depthwise_conv2d_float_random_gen_act_0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_depthwise_conv2d_float_random_gen_act_0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_0_relaxed ranges
#include "ranges/mtk_depthwise_conv2d_float_random_gen_act_0_relaxed.diff.cpp"
};
} // namespace mtk_depthwise_conv2d_float_random_gen_act_0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_depthwise_conv2d_float_random_gen_act_0_relaxed) {
    execute(mtk_depthwise_conv2d_float_random_gen_act_0_relaxed::CreateModel,
            mtk_depthwise_conv2d_float_random_gen_act_0_relaxed::is_ignored,
            mtk_depthwise_conv2d_float_random_gen_act_0_relaxed::examples,
            mtk_depthwise_conv2d_float_random_gen_act_0_relaxed::ranges);
}

namespace mtk_depthwise_conv2d_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_depthwise_conv2d_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_depthwise_conv2d_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_depthwise_conv2d_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_depthwise_conv2d_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_depthwise_conv2d_float_random_gen_act_1_relaxed) {
    execute(mtk_depthwise_conv2d_float_random_gen_act_1_relaxed::CreateModel,
            mtk_depthwise_conv2d_float_random_gen_act_1_relaxed::is_ignored,
            mtk_depthwise_conv2d_float_random_gen_act_1_relaxed::examples,
            mtk_depthwise_conv2d_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_depthwise_conv2d_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_depthwise_conv2d_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_depthwise_conv2d_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_depthwise_conv2d_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_depthwise_conv2d_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_depthwise_conv2d_float_random_gen_act_2_relaxed) {
    execute(mtk_depthwise_conv2d_float_random_gen_act_2_relaxed::CreateModel,
            mtk_depthwise_conv2d_float_random_gen_act_2_relaxed::is_ignored,
            mtk_depthwise_conv2d_float_random_gen_act_2_relaxed::examples,
            mtk_depthwise_conv2d_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_depthwise_conv2d_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_depthwise_conv2d_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_depthwise_conv2d_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_depthwise_conv2d_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_depthwise_conv2d_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_depthwise_conv2d_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_depthwise_conv2d_float_random_gen_act_3_relaxed) {
    execute(mtk_depthwise_conv2d_float_random_gen_act_3_relaxed::CreateModel,
            mtk_depthwise_conv2d_float_random_gen_act_3_relaxed::is_ignored,
            mtk_depthwise_conv2d_float_random_gen_act_3_relaxed::examples,
            mtk_depthwise_conv2d_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_depthwise_conv2d_float_random_gen_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_depthwise_conv2d_float_random_gen_relaxed test
#include "generated/examples/mtk_depthwise_conv2d_float_random_gen_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_depthwise_conv2d_float_random_gen_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_depthwise_conv2d_float_random_gen_relaxed ranges
#include "ranges/mtk_depthwise_conv2d_float_random_gen_relaxed.diff.cpp"
};
} // namespace mtk_depthwise_conv2d_float_random_gen_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_depthwise_conv2d_float_random_gen_relaxed) {
    execute(mtk_depthwise_conv2d_float_random_gen_relaxed::CreateModel,
            mtk_depthwise_conv2d_float_random_gen_relaxed::is_ignored,
            mtk_depthwise_conv2d_float_random_gen_relaxed::examples,
            mtk_depthwise_conv2d_float_random_gen_relaxed::ranges);
}

namespace mtk_fully_connected_float_2_act1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_fully_connected_float_2_act1_relaxed test
#include "generated/examples/mtk_fully_connected_float_2_act1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_fully_connected_float_2_act1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_fully_connected_float_2_act1_relaxed ranges
#include "ranges/mtk_fully_connected_float_2_act1_relaxed.diff.cpp"
};
} // namespace mtk_fully_connected_float_2_act1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_fully_connected_float_2_act1_relaxed) {
    execute(mtk_fully_connected_float_2_act1_relaxed::CreateModel,
            mtk_fully_connected_float_2_act1_relaxed::is_ignored,
            mtk_fully_connected_float_2_act1_relaxed::examples,
            mtk_fully_connected_float_2_act1_relaxed::ranges);
}

namespace mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed test
#include "generated/examples/mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed ranges
#include "ranges/mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed.diff.cpp"
};
} // namespace mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed) {
    execute(mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed::CreateModel,
            mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed::is_ignored,
            mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed::examples,
            mtk_fully_connected_float_random_gen_4d_simple_act0_relaxed::ranges);
}

namespace mtk_lstm3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_lstm3_relaxed test
#include "generated/examples/mtk_lstm3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_lstm3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_lstm3_relaxed ranges
#include "ranges/mtk_lstm3_relaxed.diff.cpp"
};
} // namespace mtk_lstm3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_lstm3_relaxed) {
    execute(mtk_lstm3_relaxed::CreateModel,
            mtk_lstm3_relaxed::is_ignored,
            mtk_lstm3_relaxed::examples,
            mtk_lstm3_relaxed::ranges);
}

namespace mtk_lstm3_state_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_lstm3_state_relaxed test
#include "generated/examples/mtk_lstm3_state_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_lstm3_state_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_lstm3_state_relaxed ranges
#include "ranges/mtk_lstm3_state_relaxed.diff.cpp"
};
} // namespace mtk_lstm3_state_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_lstm3_state_relaxed) {
    execute(mtk_lstm3_state_relaxed::CreateModel,
            mtk_lstm3_state_relaxed::is_ignored,
            mtk_lstm3_state_relaxed::examples,
            mtk_lstm3_state_relaxed::ranges);
}

namespace mtk_mobilenet_224_gender_basic_fixed_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mobilenet_224_gender_basic_fixed_relaxed test
#include "generated/examples/mtk_mobilenet_224_gender_basic_fixed_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mobilenet_224_gender_basic_fixed_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mobilenet_224_gender_basic_fixed_relaxed ranges
#include "ranges/mtk_mobilenet_224_gender_basic_fixed_relaxed.diff.cpp"
};
} // namespace mtk_mobilenet_224_gender_basic_fixed_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mobilenet_224_gender_basic_fixed_relaxed) {
    execute(mtk_mobilenet_224_gender_basic_fixed_relaxed::CreateModel,
            mtk_mobilenet_224_gender_basic_fixed_relaxed::is_ignored,
            mtk_mobilenet_224_gender_basic_fixed_relaxed::examples,
            mtk_mobilenet_224_gender_basic_fixed_relaxed::ranges);
}

namespace mtk_mul_broadcast_float_random_gen_act_0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_broadcast_float_random_gen_act_0_relaxed test
#include "generated/examples/mtk_mul_broadcast_float_random_gen_act_0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_broadcast_float_random_gen_act_0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_broadcast_float_random_gen_act_0_relaxed ranges
#include "ranges/mtk_mul_broadcast_float_random_gen_act_0_relaxed.diff.cpp"
};
} // namespace mtk_mul_broadcast_float_random_gen_act_0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_broadcast_float_random_gen_act_0_relaxed) {
    execute(mtk_mul_broadcast_float_random_gen_act_0_relaxed::CreateModel,
            mtk_mul_broadcast_float_random_gen_act_0_relaxed::is_ignored,
            mtk_mul_broadcast_float_random_gen_act_0_relaxed::examples,
            mtk_mul_broadcast_float_random_gen_act_0_relaxed::ranges);
}

namespace mtk_mul_broadcast_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_broadcast_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_mul_broadcast_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_broadcast_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_broadcast_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_mul_broadcast_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_mul_broadcast_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_broadcast_float_random_gen_act_1_relaxed) {
    execute(mtk_mul_broadcast_float_random_gen_act_1_relaxed::CreateModel,
            mtk_mul_broadcast_float_random_gen_act_1_relaxed::is_ignored,
            mtk_mul_broadcast_float_random_gen_act_1_relaxed::examples,
            mtk_mul_broadcast_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_mul_broadcast_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_broadcast_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_mul_broadcast_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_broadcast_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_broadcast_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_mul_broadcast_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_mul_broadcast_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_broadcast_float_random_gen_act_2_relaxed) {
    execute(mtk_mul_broadcast_float_random_gen_act_2_relaxed::CreateModel,
            mtk_mul_broadcast_float_random_gen_act_2_relaxed::is_ignored,
            mtk_mul_broadcast_float_random_gen_act_2_relaxed::examples,
            mtk_mul_broadcast_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_mul_broadcast_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_broadcast_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_mul_broadcast_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_broadcast_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_broadcast_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_mul_broadcast_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_mul_broadcast_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_broadcast_float_random_gen_act_3_relaxed) {
    execute(mtk_mul_broadcast_float_random_gen_act_3_relaxed::CreateModel,
            mtk_mul_broadcast_float_random_gen_act_3_relaxed::is_ignored,
            mtk_mul_broadcast_float_random_gen_act_3_relaxed::examples,
            mtk_mul_broadcast_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_mul_float_random_gen_act_0_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_float_random_gen_act_0_relaxed test
#include "generated/examples/mtk_mul_float_random_gen_act_0_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_float_random_gen_act_0_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_float_random_gen_act_0_relaxed ranges
#include "ranges/mtk_mul_float_random_gen_act_0_relaxed.diff.cpp"
};
} // namespace mtk_mul_float_random_gen_act_0_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_float_random_gen_act_0_relaxed) {
    execute(mtk_mul_float_random_gen_act_0_relaxed::CreateModel,
            mtk_mul_float_random_gen_act_0_relaxed::is_ignored,
            mtk_mul_float_random_gen_act_0_relaxed::examples,
            mtk_mul_float_random_gen_act_0_relaxed::ranges);
}

namespace mtk_mul_float_random_gen_act_1_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_float_random_gen_act_1_relaxed test
#include "generated/examples/mtk_mul_float_random_gen_act_1_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_float_random_gen_act_1_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_float_random_gen_act_1_relaxed ranges
#include "ranges/mtk_mul_float_random_gen_act_1_relaxed.diff.cpp"
};
} // namespace mtk_mul_float_random_gen_act_1_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_float_random_gen_act_1_relaxed) {
    execute(mtk_mul_float_random_gen_act_1_relaxed::CreateModel,
            mtk_mul_float_random_gen_act_1_relaxed::is_ignored,
            mtk_mul_float_random_gen_act_1_relaxed::examples,
            mtk_mul_float_random_gen_act_1_relaxed::ranges);
}

namespace mtk_mul_float_random_gen_act_2_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_float_random_gen_act_2_relaxed test
#include "generated/examples/mtk_mul_float_random_gen_act_2_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_float_random_gen_act_2_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_float_random_gen_act_2_relaxed ranges
#include "ranges/mtk_mul_float_random_gen_act_2_relaxed.diff.cpp"
};
} // namespace mtk_mul_float_random_gen_act_2_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_float_random_gen_act_2_relaxed) {
    execute(mtk_mul_float_random_gen_act_2_relaxed::CreateModel,
            mtk_mul_float_random_gen_act_2_relaxed::is_ignored,
            mtk_mul_float_random_gen_act_2_relaxed::examples,
            mtk_mul_float_random_gen_act_2_relaxed::ranges);
}

namespace mtk_mul_float_random_gen_act_3_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_mul_float_random_gen_act_3_relaxed test
#include "generated/examples/mtk_mul_float_random_gen_act_3_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_mul_float_random_gen_act_3_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_mul_float_random_gen_act_3_relaxed ranges
#include "ranges/mtk_mul_float_random_gen_act_3_relaxed.diff.cpp"
};
} // namespace mtk_mul_float_random_gen_act_3_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_mul_float_random_gen_act_3_relaxed) {
    execute(mtk_mul_float_random_gen_act_3_relaxed::CreateModel,
            mtk_mul_float_random_gen_act_3_relaxed::is_ignored,
            mtk_mul_float_random_gen_act_3_relaxed::examples,
            mtk_mul_float_random_gen_act_3_relaxed::ranges);
}

namespace mtk_softmax_2D_float_random_gen_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_softmax_2D_float_random_gen_relaxed test
#include "generated/examples/mtk_softmax_2D_float_random_gen_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_softmax_2D_float_random_gen_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_softmax_2D_float_random_gen_relaxed ranges
#include "ranges/mtk_softmax_2D_float_random_gen_relaxed.diff.cpp"
};
} // namespace mtk_softmax_2D_float_random_gen_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_softmax_2D_float_random_gen_relaxed) {
    execute(mtk_softmax_2D_float_random_gen_relaxed::CreateModel,
            mtk_softmax_2D_float_random_gen_relaxed::is_ignored,
            mtk_softmax_2D_float_random_gen_relaxed::examples,
            mtk_softmax_2D_float_random_gen_relaxed::ranges);
}

namespace mtk_softmax_4D_float_random_gen_relaxed {
std::vector<MixedTypedExample> examples = {
// Generated mtk_softmax_4D_float_random_gen_relaxed test
#include "generated/examples/mtk_softmax_4D_float_random_gen_relaxed.example.cpp"
};
// Generated model constructor
#include "generated/models/mtk_softmax_4D_float_random_gen_relaxed.model.cpp"
std::vector<MixedTypedExample> ranges = {
// Generated mtk_softmax_4D_float_random_gen_relaxed ranges
#include "ranges/mtk_softmax_4D_float_random_gen_relaxed.diff.cpp"
};
} // namespace mtk_softmax_4D_float_random_gen_relaxed
TEST_F(GeneratedGpuDynamicRangeTests, mtk_softmax_4D_float_random_gen_relaxed) {
    execute(mtk_softmax_4D_float_random_gen_relaxed::CreateModel,
            mtk_softmax_4D_float_random_gen_relaxed::is_ignored,
            mtk_softmax_4D_float_random_gen_relaxed::examples,
            mtk_softmax_4D_float_random_gen_relaxed::ranges);
}
