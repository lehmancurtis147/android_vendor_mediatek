#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

model = Model()
i1 = Input("op1", "TENSOR_FLOAT32", "{1, 3, 3, 2}")
f1 = Parameter("op2", "TENSOR_FLOAT32", "{1, 2, 2, 4}", [1.8997076055,0.2746282284,1.7518149676,1.8707548223,1.806924003,0.0691976217,0.7399854053,1.9717478927,0.0009707382,0.9918566862,0.459853818,0.9850721822,1.322887103,0.3043321916,0.2622606215,0.1805015218])
b1 = Parameter("op3", "TENSOR_FLOAT32", "{4}", [0.2527918018,0.256753258,1.6168371771,1.582075712])
pad0 = Int32Scalar("pad0", 0)
act = Int32Scalar("act", 1)
stride = Int32Scalar("stride", 1)
cm = Int32Scalar("channelMultiplier", 1)
output = Output("op4", "TENSOR_FLOAT32", "{1, 2, 2, 4}")

model = model.Operation("DEPTHWISE_CONV_2D",
                        i1, f1, b1,
                        pad0, pad0, pad0, pad0,
                        stride, stride,
                        cm, act).To(output)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [0.9006898447,1.7894135905,1.9819008765,0.1872001949,1.3492162686,1.9966081308,0.6421406604,1.0679345703,0.997021895,0.5232610875,1.3549987615,0.9348634965,0.2159897651,1.8590187391,1.4806685743,0.1193190469,1.1447031068,0.546296992]}
# (i1 (conv) f1) + b1
# filter usage:
#   in_ch1 * f_1  --> output_d1
#   in_ch1 * f_2  --> output_d2
#   in_ch2 * f_3  --> output_d3
#   in_ch3 * f_4  --> output_d4
output0 = {output: # output 0
           [6.86455392837524414, 1.97961378097534180, 9.61327838897705078, 2.57515573501586914, 0.25279179215431213, 0.25675326585769653, 1.61683714389801025, 1.58207571506500244, 5.23318099975585938, 2.46643948554992676, 7.47501659393310547, 2.07507157325744629, 0.25279179215431213, 0.25675326585769653, 1.61683714389801025, 1.58207571506500244]}

# Instantiate an example
Example((input0, output0))
