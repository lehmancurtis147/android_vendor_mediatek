#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# model
model = Model()
i1 = Input("op1", "TENSOR_FLOAT32", "{1, 4, 4, 1}") # input 0
cons1 = Int32Scalar("cons1", 1)
cons2 = Int32Scalar("cons2", 2)
pad0 = Int32Scalar("pad0", 0)
act = Int32Scalar("act", 0)
i3 = Output("op3", "TENSOR_FLOAT32", "{1, 3, 3, 1}") # output 0
model = model.Operation("AVERAGE_POOL_2D", i1, pad0, pad0, pad0, pad0, cons1, cons1, cons2, cons2, act).To(i3)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [1.2426573112,0.3566121164,1.992444648,1.2822226393,0.7021563718,1.2416358389,0.5919583863,0.0791935622,0.2236869457,0.0264277952,1.1530372777,0.867914587,1.6948078747,1.2759742292,1.1991557008,1.2946061368]}
output0 = {i3: # output 0
          [0.88576537370681763, 1.04566276073455811, 0.98645484447479248, 0.54847669601440430, 0.75326478481292725, 0.67302596569061279, 0.80522423982620239, 0.91364872455596924, 1.12867844104766846]}
# Instantiate an example
Example((input0, output0))
