#
# Copyright (C) 2018 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# model
model = Model()
i1 = Input("op1", "TENSOR_FLOAT32", "{2, 4, 3, 1}") # a vector of 8 float32s
i2 = Input("op2", "TENSOR_FLOAT32", "{2, 4, 3, 1}") # another vector of 24 float32s
act = Int32Scalar("act", 1) # an int32_t scalar activation
i3 = Output("op3", "TENSOR_FLOAT32", "{2, 4, 3, 1}")
model = model.Operation("ADD", i1, i2, act).To(i3)
model = model.RelaxedExecution(True)

# Example 1. Input in operand 0,
input0 = {i1: # input 0
          [0.8444939351,1.0716826623,1.2350995873,1.5965641122,1.1944434763,0.1212651993,1.1958277659,0.104434857,0.0424125336,1.1301054499,1.4550725366,0.5414490444,0.3706982953,1.1894290365,0.3689160172,1.6503016363,0.0952362868,0.1244964743,1.8203393502,0.2916153206,0.6344079835,0.4039772798,0.625222566,0.2245784764],
          i2: # input 1
          [1.4374168385,0.5843404578,1.736938147,1.0864739752,0.1298754319,0.6973406143,0.6561101426,0.9463441975,1.7244153707,1.0208614058,0.4727088493,1.2969531052,1.4933821346,0.9534290718,0.8915458446,0.2888897457,0.9452383974,1.5533001746,0.7139904598,1.4771544494,0.9038666809,1.7957362509,1.8064562763,0.8285854864]}

output0 = {i3: # output 0
           [2.28191065788269043, 1.65602314472198486, 2.97203779220581055, 2.68303799629211426, 1.32431888580322266, 0.81860578060150146, 1.85193789005279541, 1.05077910423278809, 1.76682794094085693, 2.15096688270568848, 1.92778134346008301, 1.83840215206146240, 1.86408042907714844, 2.14285802841186523, 1.26046180725097656, 1.93919146060943604, 1.04047465324401855, 1.67779660224914551, 2.53432989120483398, 1.76876974105834961, 1.53827476501464844, 2.19971346855163574, 2.43167877197265625, 1.05316400527954102]}

# Instantiate an example
Example((input0, output0))
