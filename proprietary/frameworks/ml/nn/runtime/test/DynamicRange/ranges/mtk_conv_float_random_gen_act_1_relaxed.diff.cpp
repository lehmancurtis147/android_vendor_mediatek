// Generated file (from: vendor_conv_float_random_gen_act_1_relaxed.mod.py). Do not edit
// Begin of an example
{
//Input(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {1.2182915615f, 1.9517464966f, 1.3041792875f, 0.3723773628f, 1.0133912663f, 0.9130921618f, 0.615666132f, 1.6842522403f, 0.0373733978f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
},
//Output(s)
{ // See tools/test_generator/include/TestHarness.h:MixedTyped
  // int -> FLOAT32 map
  {{0, {0.000964164733886718750f, 0.000641822814941406250f, 0.000443935394287109375f, 0.000839233398437500000f}}},
  // int -> INT32 map
  {},
  // int -> QUANT8_ASYMM map
  {}
}
}, // End of an example
