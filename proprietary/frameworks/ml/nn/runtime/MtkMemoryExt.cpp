/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkMemoryExt"

#include "Memory.h"
#include "MtkMemoryExt.h"

#include <ion/ion.h>
#include <cutils/native_handle.h>

#define ION_HEAP_TYPE 1024   // 1 << HEAP_TYPE_MULTI_MEDIA;

namespace android {
namespace nn {

int createIonMemory(size_t size, ANeuralNetworksMemory **memory) {
    int ionFd;
    int shareFd;
    ion_user_handle_t handle;
    MemoryExt *ion = reinterpret_cast<MemoryExt *>(*memory);
    unsigned int flag = 0;
    int heapType = ION_HEAP_TYPE;
    int prot = PROT_READ | PROT_WRITE;

    if (ion->mMapping) {
        if (munmap(ion->mMapping, ion->getHidlMemory().size()) != 0) {
            LOG(ERROR) << "Failed to remove the existing mapping";
            // This is not actually fatal.
        }
        ion->mMapping = nullptr;
    }

    if (ion->mHandle) {
        native_handle_delete(ion->mHandle);
    }
    ion->mHandle = native_handle_create(1, 4);
    if (ion->mHandle == nullptr) {
        LOG(ERROR) << "Failed to create native_handle";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    // open a ion fd, register as a ion client
    ionFd = ion_open();
    if (ionFd < 0) {
        LOG(ERROR) << "Cannot open ion device.";
        return ANEURALNETWORKS_INCOMPLETE;
    }
    // Debug Infos
    // TODO: Change to log debug
    LOG(INFO) << "Debug info: ionFd: " << ionFd;
    LOG(INFO) << "Debug info: heap type: " << heapType;
    LOG(INFO) << "Debug info: size: " << size;
    LOG(INFO) << "Debug info: flag: " << flag;

    // request memory from ion core
    int ret = ion_alloc(ionFd, size, 0, heapType, flag, &handle);
    if (ret < 0) {
        LOG(ERROR) << "IOCTL[ION_IOC_ALLOC] failed!";
        return ANEURALNETWORKS_INCOMPLETE;
    }
    // get share memory fd
    ret = ion_share(ionFd, handle, &shareFd);
    if (ret < 0) {
        LOG(ERROR) << "IOCTL[ION_IOC_SHARE] failed!";
        return ANEURALNETWORKS_INCOMPLETE;
    }

    // TODO: Change to log debug
    LOG(INFO) << "Debug info: shareFd: " << shareFd;

    ion->mHandle->data[0] = shareFd;
    ion->mHandle->data[1] = prot;
    ion->mHandle->data[2] = ionFd;
    ion->mHandle->data[3] = handle;
    ion->setHidlMemory(hidl_memory("ion", ion->mHandle, size));
    ion->setMemoryType(ANEUROPILOT_MEMORY_ION);

    return ANEURALNETWORKS_NO_ERROR;
}

int getIonPointer(uint8_t** buffer, ANeuralNetworksMemory **memory) {
    MemoryExt *ion = reinterpret_cast<MemoryExt *>(*memory);

    if (ion->getMemoryType() != ANEUROPILOT_MEMORY_ION) {
        return ANEURALNETWORKS_BAD_DATA;
    }

    if (ion->mMapping) {
        *buffer = ion->mMapping;
        return ANEURALNETWORKS_NO_ERROR;
    }

    if (ion->mHandle == nullptr) {
        LOG(ERROR) << "MemoryExt not initialized";
        return ANEURALNETWORKS_UNEXPECTED_NULL;
    }

    int fd = ion->mHandle->data[0];
    int prot = ion->mHandle->data[1];

    void* data = mmap(nullptr, ion->getHidlMemory().size(), prot, MAP_SHARED, fd, 0);
    if (data == MAP_FAILED) {
        LOG(ERROR) << "getIonPointer(): Can't mmap the file descriptor.";
        return ANEURALNETWORKS_UNMAPPABLE;
    } else {
        ion->mMapping = *buffer = static_cast<uint8_t*>(data);
        return ANEURALNETWORKS_NO_ERROR;
    }
}

int freeIonMemory(ANeuralNetworksMemory **memory) {
    MemoryExt *memoryExt = reinterpret_cast<MemoryExt *>(*memory);
    int ret = ANEURALNETWORKS_NO_ERROR;

    // Unmap the memory.
    if (memoryExt->mMapping) {
        munmap(memoryExt->mMapping, memoryExt->getHidlMemory().size());
    }
    if (memoryExt->getMemoryType() == ANEUROPILOT_MEMORY_ION) {
        // Delete the native_handle.
        if (memoryExt->mHandle) {
            int shareFd = memoryExt->mHandle->data[0];
            int ionFd = memoryExt->mHandle->data[2];
            ion_user_handle_t handle = memoryExt->mHandle->data[3];

            // TODO: Change to log debug
            LOG(INFO) << "Free Ion memory: shareFd " << shareFd;
            LOG(INFO) << "Free Ion memory: ionFd " << ionFd;
            LOG(INFO) << "Free Ion memory: handle " << handle;


            ion_close(shareFd);
            if (ion_free(ionFd, handle)) {
                LOG(ERROR) << "IOCTL[ION_IOC_FREE] failed!\n";
                ret = ANEURALNETWORKS_INCOMPLETE;
            }
            ion_close(ionFd);
            native_handle_delete(memoryExt->mHandle);
        }
    }
    return ret;
}

}  // namespace nn
}  // namespace android