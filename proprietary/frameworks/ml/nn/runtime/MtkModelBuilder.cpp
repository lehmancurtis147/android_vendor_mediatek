/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkModelBuilder"

#include "Manager.h"
#include "MtkModelBuilder.h"
#include "MtkUtils.h"

// Debug
#include "Options.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "GraphDump.h"

#include "NeuroPilotDef.h"

namespace android {
namespace nn {

int MtkModelBuilder::createCompilation(CompilationBuilder** compilation) {
    if (!mCompletedModel || mInvalidModel) {
        LOG(ERROR) << "ANeuralNetworksCompilation_create passed an unfinished or invalid model";
        *compilation = nullptr;
        return ANEURALNETWORKS_BAD_STATE;
    }
    *compilation = new (std::nothrow) MtkCompilationBuilder(this);
    return (*compilation ? ANEURALNETWORKS_NO_ERROR : ANEURALNETWORKS_OUT_OF_MEMORY);
}

// Bind Device Start
int MtkModelBuilder::bindOperationToDevice(uint32_t operationType, uint32_t device) {
    if (!isDeviceValid(device)) {
        return ANEURALNETWORKS_BAD_DATA;
    }
    OperationType op = (OperationType) operationType;
    auto search = mRuntimeMap.find(op);
    if (search != mRuntimeMap.end()) {
        search->second = device;
        LOG(DEBUG) << "overwrite runtime HW : " << toString(op) << ", device : " << device;
    } else {
        mRuntimeMap[op] = device;
    }
    for (auto item : mRuntimeMap) {
        LOG(DEBUG) << "operation : " << toString(item.first) << ", device : " << item.second;
    }
    return ANEURALNETWORKS_NO_ERROR;
}

int MtkModelBuilder::bindAllOperationsToDevice(uint32_t device) {
    for (Operation op : mOperations) {
        uint32_t operationType = (uint32_t) op.type;
        int ret = bindOperationToDevice(operationType, device);
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            return ret;
        }
    }
    return ANEURALNETWORKS_NO_ERROR;
}

void MtkModelBuilder::overrideSupportsOperation(
        hidl_vec<bool>* outSupportedOperations, std::shared_ptr<Device> device) const {
    if (isBindOperationSupported()) {
        std::map<OperationType, uint32_t> runtimeMap = getRuntimeMap();
        for (uint32_t i = 0; i < operationCount(); i++) {
            const Operation& op = getOperation(i);
            //  LOG(INFO) << "before , outSupportedOperations[" << i << "] : "
            //        << (*outSupportedOperations)[i];
            auto search = runtimeMap.find(op.type);
            if (search != runtimeMap.end()) {
                LOG(DEBUG) << "found runtimeMap , " << toString(op.type)
                        << " -> " << search->second;

                if (getDeviceId(device->getName().c_str()) == search->second) {
                    (*outSupportedOperations)[i] = true;
                } else {
                    (*outSupportedOperations)[i] = false;
                }
            }
            //  LOG(INFO) << "after , outSupportedOperations[" << i << "] : "
            //        << (*outSupportedOperations)[i];
        }
    } else {
        NP_VLOG << "Not support bind operation to device";
    }
}
// Bind Device End

// Partition Extension Start
int MtkModelBuilder::partitionTheWorkExt(const std::vector<std::shared_ptr<Device>>& devices,
        uint32_t preference, ExecutionPlan* plan, uint32_t partitionExtType) const {
    int ret = ANEURALNETWORKS_NO_ERROR;

    // No Preference, fallback to AOSP
    if (partitionExtType == ANEUROPILOT_PARTITIONING_EXTENSION_NONE) {
        return ModelBuilder::partitionTheWork(devices, preference, plan);
    }

    // MTK policy
    if (partitionExtType == ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION) {
        const size_t nonCpuDeviceCount = devices.size();
        // The device count is the number of HAL devices + 1. The +1 is for the CPU.
        // Note that deviceCount includes CPU, which has no entry in devices[].
        const size_t deviceCount = nonCpuDeviceCount + 1;
        const size_t operationCount = mOperations.size();

        VLOG(COMPILATION) << "MtkModelBuilder::partitionTheWorkExt: deviceCount = " << deviceCount
                          << ", operationCount = " << operationCount;

        // If the graph has no operations, no need to try to partition.
        if (operationCount == 0) {
            plan->becomeSingleStep(nullptr /* CPU */, this);
            return plan->finish(this, preference);
        }

        // Figure out where each operation will best execute.
        // The value of the vector is the index in the devices vector, with devices.size()
        // representing the CPU.
        std::vector<int> bestDeviceForOperation(operationCount);
        int status = findBestDeviceForEachOperation(preference, devices, deviceCount,
                                                    &bestDeviceForOperation);
        if (status != ANEURALNETWORKS_NO_ERROR) {
            return status;
        }

        // If one device will run all the operations, we don't need to split the work.
        if (operationCount == 1) {
            const int bestDeviceIndex = bestDeviceForOperation[0];
            const bool cpu = (size_t(bestDeviceIndex) == deviceCount - 1);
            VLOG(COMPILATION) << "MtkModelBuilder::partitionTheWork: only one best device: "
                              << bestDeviceIndex << " = "
                              << (cpu ? "CPU" : devices[bestDeviceIndex]->getName());

            plan->becomeSingleStep(cpu ? nullptr : devices[bestDeviceIndex], this);
            return plan->finish(this, preference);
        }

        // For each iteration of this loop, we'll create an execution step.
        for (size_t operationIndex = 0; operationIndex < operationCount; operationIndex++) {
            int deviceIndex = bestDeviceForOperation[operationIndex];

            // nullptr represents the CPU.
            std::shared_ptr<Device> device =
                    static_cast<size_t>(deviceIndex) < nonCpuDeviceCount
                    ? devices[deviceIndex] : nullptr;

            // Assign as much as possible to this device.
            std::shared_ptr<ExecutionStep> step = plan->createNewStep(device);
            step->addOperation(operationIndex, *this);
        }

        ret = plan->finish(this, preference);
    }

    if (VLOG_IS_ON(COMPILATION)) {
        Model model;
        setHidlModel(&model);
        VLOG(COMPILATION) << "MtkModelBuilder::partitionTheWorkExt: original model: ";
        logModelToInfo(model);
        plan->dump();
    }

    return ret;
}
// Partition Extension End

// Debug Start
int MtkModelBuilder::finish() {
     // Print to log and file
    char strPath[PROPERTY_VALUE_MAX];
    std::ofstream kFile;
    if (getDumpPath(strPath) && isDumpModelEnable()) {
        kFile.open(strPath, std::ofstream::out | std::ofstream::app);
        if (kFile.is_open()) {
            Model modelForDumpGraph;
            setHidlModel(&modelForDumpGraph);
            graphDump("Origial Model", modelForDumpGraph, kFile);
            kFile.close();
        }
    }
    return ModelBuilder::finish();
}

void MtkModelBuilder::sortIntoRunOrder() {
    if (ignoreSortRunOrder()) {
        VLOG(MODEL) << "Ignore sortIntoRunOrder";
        return;
    }
    ModelBuilder::sortIntoRunOrder();
}
// Debug End

}  // namespace nn
}  // namespace android
