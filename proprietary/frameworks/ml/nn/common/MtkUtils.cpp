/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include "NeuralNetworks.h"
#include "MtkUtils.h"
#include "NeuroPilot.h"
#include "Options.h"
#include <android-base/logging.h>

// Sys Trace
#define ATRACE_TAG  ATRACE_TAG_APP
#include <utils/Trace.h>
#include <cutils/trace.h>
#include "Options.h"

// Profiler
#define START_PROFILE_MAX_RETRY_COUNT 10
#include "Manager.h"
#include "ExecutionPlan.h"
#include "MtkExecutionBuilder.h"

// NeuroPilot  Operation
#include "NeuroPilot.h"

// NN Version
static const char* nn_hash_str =
#include "__nn_hash.inc"
;

static const char* nn_ver_str =
#include "__nn_ver.inc"
;

#ifdef LOG_TAG
#undef LOG_TAG
#define LOG_TAG "MtkUtils"
#endif

namespace android {
namespace nn {

// Profiler
static std::mutex executionMutex;
static std::map<std::thread::id, ExecutionBuilder*> sExecution;

// Bind operation to device
uint32_t getDeviceId(const char* deviceName) {
    uint32_t ret = 0;
    if (strcmp(deviceName, "cpunn") == 0) {
        ret = ANEUROPILOT_CPU;
    } else if (strcmp(deviceName, "gpunn") == 0) {
        ret = ANEUROPILOT_GPU;
    } else if (strcmp(deviceName, "apunn") == 0) {
        ret = ANEUROPILOT_APU;
    } else {
        LOG(ERROR) << "unknown device name:" << deviceName;
    }
    return ret;
}

bool isDeviceValid(uint32_t device) {
    if (device <= 0) {
        LOG(ERROR) << "invalid device:" << device;
        return false;
    }

    if ((device & device - 1) != 0) {
        LOG(ERROR) << "Only allow to input one device at one time";
        return false;
    }

    uint32_t activeDevice = 0;
    if (ANeuroPilotModel_getAvailableDevice(&activeDevice) != ANEURALNETWORKS_NO_ERROR) {
        LOG(ERROR) << "Get available device error";
        return false;
    }
    if ((device & activeDevice) == 0) {
        LOG(ERROR) << "Input device is inactive.";
        LOG(ERROR) << "Device: " << device << ", active devices: " << activeDevice;
        return false;
    }

    return true;
}

// Sys Trace Start
void sysTraceStart(const char* name) {
    if (isSysTraceSupported()) {
        NP_VLOG << "sysTraceStart: " << name;
        ATRACE_BEGIN(name);
    }
}

void sysTraceStop() {
    if (isSysTraceSupported()) {
        NP_VLOG << "sysTraceStop";
        ATRACE_END();
    }
}
// Sys Trace End

// Profiler
int setExecution(std::thread::id tid, ExecutionBuilder* execution) {
    if (execution == nullptr) {
        LOG(ERROR) << "execution is null";
        return ANEURALNETWORKS_BAD_DATA;
    }

    // Prevent concurrent executions that may access the sExecution.
    std::unique_lock<std::mutex> lock(executionMutex);
    auto search = sExecution.find(tid);
    if (search != sExecution.end()) {
        search->second = execution;
        LOG(DEBUG) << "Update execution with key:" << tid;
    } else {
        LOG(DEBUG) << "Set execution with key:" << tid;
        sExecution[tid] = execution;
        LOG(DEBUG) << "After set execution:" << sExecution[tid];
    }
    return ANEURALNETWORKS_NO_ERROR;
}

int eraseExecution(std::thread::id tid) {
    // Prevent concurrent executions that may access the sExecution.
    std::unique_lock<std::mutex> lock(executionMutex);
    int n = sExecution.erase(tid);
    LOG(INFO) << "eraseExecution with key " << tid << ", success? " << n;

    return ANEURALNETWORKS_NO_ERROR;
}

int startProfile(std::thread::id tid, const char* device) {
    int ret = ANEURALNETWORKS_NO_ERROR;
    int retry = 0;
    while (retry < START_PROFILE_MAX_RETRY_COUNT) {
        // Prevent concurrent executions that may access the sExecution.
        std::unique_lock<std::mutex> lock(executionMutex);
        auto search = sExecution.find(tid);
        if (search != sExecution.end()) {
            LOG(DEBUG) << "Start Profile found with key " << tid << " execution:" << search->second;
            MtkExecutionBuilder* execution =
                    reinterpret_cast<MtkExecutionBuilder*>(search->second);

            ret = execution->startProfile(device);
            break;
        } else {
            LOG(ERROR) << "Not Found execution with key " << tid << ", retry";
            ret = ANEURALNETWORKS_BAD_DATA;
            retry++;
        }
    }

    return ret;
}

int stopProfile(std::thread::id tid, const char* request, int err) {
    int ret = ANEURALNETWORKS_NO_ERROR;

    // Prevent concurrent executions that may access the sExecution.
    std::unique_lock<std::mutex> lock(executionMutex);
    auto search = sExecution.find(tid);
    if (search != sExecution.end()) {
        MtkExecutionBuilder* execution =
                reinterpret_cast<MtkExecutionBuilder*>(search->second);
        ret = execution->stopProfile(request, err);
        LOG(DEBUG) << "Stop Profile with key " << tid;
    } else {
        LOG(ERROR) << "Not Found executor with key " << tid;
        ret = ANEURALNETWORKS_BAD_DATA;
    }
    return ret;
}
// Profiler End

// NN Version
const char* get_nn_hash(void) {
    return nn_hash_str;
}

const char* get_nn_ver(void) {
    return nn_ver_str;
}

__attribute__((constructor))
static void nn_ver_init(void) {
    LOG(INFO) << "NeuroPilot hash:"  << get_nn_hash();
}


}  // namespace nn
}  // namespace android
