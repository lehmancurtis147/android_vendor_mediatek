/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <iostream>
#include <vector>
#include <string.h>
#include "NeuralNetworks.h"
#include "NeuroPilotShimLegacy.h"
#include "NeuroPilotNN_Legacy.h"

#define ROWS        4
#define COLS        4

typedef float Matrix4x4[4][4];


static const Matrix4x4 matrix1 = {{1.f, 2.f, 3.f, 4.f},
                                  {5.f, 6.f, 7.f, 8.f},
                                  {9.f, 10.f, 11.f, 12.f},
                                  {13.f, 14.f, 15.f, 16.f}};

static const Matrix4x4 matrix2 = {{100.f, 200.f, 300.f, 400.f},
                                  {500.f, 600.f, 700.f, 800.f},
                                  {900.f, 1000.f, 1100.f, 1200.f},
                                  {1300.f, 1400.f, 1500.f, 1600.f}};

static const Matrix4x4 matrix3 = {{20.f, 30.f, 40.f, 50.f},
                                  {21.f, 22.f, 23.f, 24.f},
                                  {31.f, 32.f, 33.f, 34.f},
                                  {41.f, 42.f, 43.f, 44.f}};

static const Matrix4x4 expected3 = {{121.f, 232.f, 343.f, 454.f},
                                    {526.f, 628.f, 730.f, 832.f},
                                    {940.f, 1042.f, 1144.f, 1246.f},
                                    {1354.f, 1456.f, 1558.f, 1660.f}};

static const Matrix4x4 expected3_2 = {{101.f, 202.f, 303.f, 404.f},
                                    {505.f, 606.f, 707.f, 808.f},
                                    {909.f, 1010.f, 1111.f, 1212.f},
                                    {1313.f, 1414.f, 1515.f, 1616.f}};

static uint32_t sDims[2] = {4, 4};
static ANeuralNetworksOperandType sOp = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = 2,
        .dimensions        = sDims,
        .scale             = 0.0f,
        .zeroPoint         = 0
};

ANeuralNetworksOperandType sScalar = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = 0,
        .dimensions        = nullptr,
        .scale             = 0.0f,
        .zeroPoint         = 0
};

static int CompareMatrices(const Matrix4x4 expected, const Matrix4x4 actual) {
    int errors = 0;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            if (expected[i][j] != actual[i][j]) {
                printf("expected[%d][%d] != actual[%d][%d], %f != %f\n", i, j, i, j,
                       static_cast<double>(expected[i][j]), static_cast<double>(actual[i][j]));
                errors++;
            }
        }
    }
    return errors;
}

static uint32_t getDeviceType(const char* deviceName) {
    uint32_t ret = 0;
    if (strcmp(deviceName, "CPU") == 0) {
        ret = ANEURALNETWORKS_CPU;
    } else if (strcmp(deviceName, "gpunn") == 0) {
        ret = ANEURALNETWORKS_GPU;
    } else if (strcmp(deviceName, "apunn") == 0) {
        ret = ANEURALNETWORKS_APU;
    } else {
        printf("unknown device name: %s\n", deviceName);
    }
    return ret;
}

int createModelForTest(ANeuralNetworksModel *model) {
    //
    // create operand
    //
    std::vector<uint32_t> dim{4, 4};
    ANeuralNetworksOperandType op = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(dim.size()),
        .dimensions        = dim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
        };

    std::vector<uint32_t> scalarDim;
    ANeuralNetworksOperandType scalar = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = static_cast<uint32_t>(scalarDim.size()),
        .dimensions        = scalarDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
        };

    //
    // add operands
    // index : 0 -> a
    // index : 1 -> b
    // index : 2 -> c
    // index : 3 -> d
    // index : 4 -> e
    // index : 5 -> f (activation)
    //
    if (ANeuralNetworksModel_addOperand(model, &op) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_addOperand(model, &op) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_addOperand(model, &op) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_addOperand(model, &op) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_addOperand(model, &op) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_addOperand(model, &scalar) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperand\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    //
    // set operand value
    //
    if (ANeuralNetworksModel_setOperandValue(
            model, 4, matrix3, sizeof(Matrix4x4)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_setOperandValue\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    int32_t activation(0);
    if (ANeuralNetworksModel_setOperandValue(
            model, 5, &activation, sizeof(activation)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_setOperandValue\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    //
    // add operation
    //
    // a(0) + c(2) = b(1)
    std::vector<uint32_t> in1{0, 2, 5};
    std::vector<uint32_t> out1{1};
    if (ANeuralNetworksModel_addOperation(model,
                                          ANEURALNETWORKS_ADD,
                                          static_cast<uint32_t>(in1.size()),
                                          in1.data(),
                                          static_cast<uint32_t>(out1.size()),
                                          out1.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // b(1) + e(4) = d(3)
    std::vector<uint32_t> in2{1, 4, 5};
    std::vector<uint32_t> out2{3};
    if (ANeuralNetworksModel_addOperation(model,
                                          ANEURALNETWORKS_ADD,
                                          static_cast<uint32_t>(in2.size()),
                                          in2.data(),
                                          static_cast<uint32_t>(out2.size()),
                                          out2.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    //
    // set inputs and outputs
    //
    std::vector<uint32_t> in3{0, 2};
    std::vector<uint32_t> out3{3};
    if (ANeuralNetworksModel_identifyInputsAndOutputs(model,
                                          static_cast<uint32_t>(in3.size()),
                                          in3.data(),
                                          static_cast<uint32_t>(out3.size()),
                                          out3.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_identifyInputsAndOutputs\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    return 1;
}

int createRequestForTest(ANeuralNetworksExecution *execution, Matrix4x4 *outputMat) {
    //
    // set buffer
    //
    if (ANeuralNetworksExecution_setInput(execution,
                                        0,
                                        nullptr,
                                        matrix1,
                                        sizeof(Matrix4x4)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setInput(execution,
                                        1,
                                        nullptr,
                                        matrix2,
                                        sizeof(Matrix4x4)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setOutput(execution,
                                         0,
                                         nullptr,
                                         outputMat,
                                         sizeof(Matrix4x4)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setOutput\n");
        return 0;
    }
    return 1;
}

static int startCompute(ANeuralNetworksExecution* execution) {
    //
    // start to compute
    //
    ANeuralNetworksEvent* event = nullptr;
    if (ANeuralNetworksExecution_startCompute(execution, &event) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_startCompute\n");
        return 0;
    }

    ANeuralNetworksEvent_wait(event);
    ANeuralNetworksEvent_free(event);

    return 1;
}

// An util function, we can choice bind one operation or bind all operations.
static int doBindOperation(ANeuralNetworksModel *model, uint32_t device, bool bindAll) {
    if (bindAll) {
        if (ANeuralNetworksModel_bindAllOperationsToDevice(
                model, device) != ANEURALNETWORKS_NO_ERROR) {
            printf("Error: bind all operations to %d fail\n", device);
            return 0;
        }
    } else {
        if (ANeuralNetworksModel_bindOperationToDevice(
                model, ANEURALNETWORKS_CONV_2D, device) != ANEURALNETWORKS_NO_ERROR) {
            printf("Error: bind conv2d to %d fail\n", device);
            return 0;
        }
        if (ANeuralNetworksModel_bindOperationToDevice(
                model, ANEURALNETWORKS_ADD, device) != ANEURALNETWORKS_NO_ERROR) {
            printf("Error: bind add to %d fail\n", device);
            return 0;
        }
    }
    return 1;
}

// An util function, we can use it to assign which device we want to bind
// and bina all operations or not.
static int runBindOperationToDevice(uint32_t device, bool bindAll) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (doBindOperation(model, device, bindAll) == 0) {
        printf("doBindOperation fail\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End


    //
    // create compilation
    //
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // enable profiler
    if (ANeuralNetworksCompilation_enableProfiler(
            compilation, ANEURALNETWORKS_PROFILER_GRAPH) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_compilation\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //////////////////////////////////////////////////////////////////////////////////
    // Verify result
    //////////////////////////////////////////////////////////////////////////////////

    // Get device name and make sure it really ran on input device
    uint32_t count = 0;
    if (ANeuralNetworksExecution_getProfilerInfoCount(execution, &count)
            != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_getProfilerInfo\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    for (uint32_t i=0 ; i < count ; i++) {
        ProfilerInfo info;
        if (ANeuralNetworksExecution_getProfilerInfo(execution, i, &info)
                != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuralNetworksExecution_getProfilerInfo\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }

        if (getDeviceType(info.devName) != device) {
            printf("failed to bind operation to CPU\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }
    return 1;
}

/*************************************************************************************************/

int utProfilerGraphLegacy(void) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // enable profiler
    if (ANeuralNetworksCompilation_enableProfiler(
            compilation, ANEURALNETWORKS_PROFILER_GRAPH) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_compilation\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);

    //
    // Test it a second time to make sure the model is reusable.
    //
    // Create Execution and Request Start
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);
    return 1;
}

int utProfilerOperationLegacy(void) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // enable profiler
    if (ANeuralNetworksCompilation_enableProfiler(
            compilation, ANEURALNETWORKS_PROFILER_OPERATION) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_compilation\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);

    //
    // Test it a second time to make sure the model is reusable.
    //
    // Create Execution and Request Start
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

int utDebuggerLegacy(uint32_t mode) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // enable profiler
    if (ANeuralNetworksCompilation_enableProfiler(
            compilation, ANEURALNETWORKS_PROFILER_OPERATION) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_compilation\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End


    //
    // set debugging operations
    //
    if (mode == 0) {
        uint32_t list[2] = {0, 1};
        if (ANeuralNetworksExecution_keepOperations(
                execution, list, 2) != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuralNetworksExecution_keepOperations\n");
            ANeuralNetworksExecution_free(execution);
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            return 0;
        }
    } else if (mode == 1) {
        uint32_t list[1] = {1};
        if (ANeuralNetworksExecution_keepOperations(
                execution, list, 1) != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuralNetworksExecution_keepOperations\n");
            ANeuralNetworksExecution_free(execution);
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            return 0;
        }
    }

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    uint8_t *outBuf = nullptr;
    if (ANeuralNetworksExecution_getOperationOutput(
            execution, 0 , 0, &outBuf) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_getOperationOutput\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    if (!outBuf) {
        printf("failed to ANeuralNetworksExecution_getOperationOutput\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 tempMat;
    float *buf = reinterpret_cast<float*>(outBuf);
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            tempMat[i][j] = buf[i * 4 + j];
        }
    }

    //
    // compare results
    //
    if (mode == 0) {
        if (CompareMatrices(expected3_2, tempMat) != 0) {
            printf("failed to compare matrices\n");
            ANeuralNetworksExecution_free(execution);
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            return 0;
        }
    } else if (mode == 1) {
        if (CompareMatrices(expected3, tempMat) != 0) {
            printf("failed to compare matrices\n");
            ANeuralNetworksExecution_free(execution);
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            return 0;
        }
    }

    auto printOperationResult = [&](auto out) {
        for (uint32_t k = 0; k < 16; k++) {
            printf("%s,", std::to_string(out[k]).c_str());
        }
        printf("\n\n");
    };
    printf("Operation Result:\n");
    printOperationResult((float *) outBuf);

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

// Test ANeuralNetworksModel_bindOperationToDevice()
// Pre-Condition: Start gpunn and apunn
int utBindOperationToDeviceLegacy() {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    // Test 1 bind to vaild device
    if (doBindOperation(model, ANEURALNETWORKS_CPU, false) == 0) {
        printf("doBindOperation to cpu fail\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // if (doBindOperation(model, ANEURALNETWORKS_GPU, false) == 0) {
    //    printf("doBindOperation to gpu fail\n");
    //    ANeuralNetworksModel_free(model);
    //    return 0;
    // }

    // if (doBindOperation(model, ANEURALNETWORKS_APU, false) == 0) {
    //    printf("doBindOperation to apu fail\n");
    //    ANeuralNetworksModel_free(model);
    //    return 0;
    // }

    if (doBindOperation(model, ANEURALNETWORKS_CPU, true) == 0) {
        printf("doBindOperation all op to cpu fail\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    // if (doBindOperation(model, ANEURALNETWORKS_GPU, true) == 0) {
    //    printf("doBindOperation all op to gpu fail\n");
    //    ANeuralNetworksModel_free(model);
    //    return 0;
    // }

    // if (doBindOperation(model, ANEURALNETWORKS_APU, true) == 0) {
    //    printf("doBindOperation all op to apu fail\n");
    //    ANeuralNetworksModel_free(model);
    //    return 0;
    // }

    // Test 2 bind to unknown device
    if (ANeuralNetworksModel_bindOperationToDevice(
            model, ANEURALNETWORKS_CONV_2D, 3) == ANEURALNETWORKS_NO_ERROR) {
        printf("Error: should not success since we input an invalid device\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_bindAllOperationsToDevice(
            model, 3) == ANEURALNETWORKS_NO_ERROR) {
        printf("Error: should not success since we input an invalid device\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_bindOperationToDevice(
            model, ANEURALNETWORKS_CONV_2D, 8) == ANEURALNETWORKS_NO_ERROR) {
        printf("Error: should not success since we input an invalid device\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_bindAllOperationsToDevice(
            model, 8) == ANEURALNETWORKS_NO_ERROR) {
        printf("Error: should not success since we input an invalid device\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    ANeuralNetworksModel_free(model);
    return 1;
}

int utRunBindOperationToDeviceLegacy() {
    if (runBindOperationToDevice(ANEURALNETWORKS_CPU, false) == 0) {
        printf("bind cpu test fail");
        return 0;
    }

    // if (runBindOperationToDevice(ANEURALNETWORKS_GPU, false) == 0) {
    //    printf("bind gpu test fail");
    //    return 0;
    // }

    // if (runBindOperationToDevice(ANEURALNETWORKS_APU, false) == 0) {
    //    printf("bind apu test fail");
    //    return 0;
    // }

    if (runBindOperationToDevice(ANEURALNETWORKS_CPU, true) == 0) {
        printf("bind all operations to cpu test fail");
        return 0;
    }

    return 1;
}

int utSelRuntimeLegacy(void) {
    const char* devs = ANeuralNetworks_listRegDevice();
    printf("registered devs : %s\n", devs);

    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    Matrix4x4 outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    //
    // compare results
    //
    if (CompareMatrices(expected3, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);
    ANeuralNetworksExecution_free(execution);

    return 1;
}

int utSetCpuOnlyLegacy() {
    ANeuralNetworks_setCpuOnly(true);
    ANeuralNetworks_setCpuOnly(false);
    return 1;
}

