/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <stdio.h>
#include <gtest/gtest.h>
#include <cutils/properties.h>

#include "NeuralNetworks.h"

#include "NeuroPilotNN.h"
#include "NeuroPilotNN_Legacy.h"
#include "NeuroPilotShim.h"

#define TEST_ADD_DATA_LENGTH    2048 * 2048
float *ADDInput1 = nullptr;
float *ADDInput2 = nullptr;
float *ADDExpected = nullptr;

void prepareData() {
    srand(time(nullptr));
    ADDInput1 = new float[TEST_ADD_DATA_LENGTH];
    ADDInput2 = new float[TEST_ADD_DATA_LENGTH];
    ADDExpected = new float[TEST_ADD_DATA_LENGTH];
    for (int i = 0 ; i < TEST_ADD_DATA_LENGTH ; i++) {
        ADDInput1[i] = (float)(rand() % 1000) / 4;
        ADDInput2[i] = (float)(rand() % 1000) / 8;
        ADDExpected[i] = ADDInput1[i] + ADDInput2[i];
    }
}

void releaseData() {
    if (ADDInput1) delete []ADDInput1;
    if (ADDInput2) delete []ADDInput2;
    if (ADDExpected) delete []ADDExpected;
}

bool getProfilerSupport() {
    return property_get_bool("debug.nn.profiler.supported", false);
}

void enableProfiler(bool enable) {
    const char* value = enable ? "1" : "0";
    property_set("debug.nn.profiler.supported", value);
}

bool getBindDeviceSupport() {
    return property_get_bool("debug.nn.bindoperation.supported", false);
}

void enableBindDevice(bool enable) {
    const char* value = enable ? "1" : "0";
    property_set("debug.nn.bindoperation.supported", value);
}

bool getIonSupport() {
    return property_get_bool("debug.nn.ion.supported", false);
}

void enableIon(bool enable) {
    const char* value = enable ? "1" : "0";
    property_set("debug.nn.ion.supported", value);
}

#ifdef DEVICE_CAP_TEST
TEST(NeuroPilotUnittest, utDeviceCapWorseThanCpu) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }

    bool profilerEnable = getProfilerSupport();
    if (!profilerEnable) {
        enableProfiler(true);
    }
    int ret = utDeviceCapWorseThanCpu();
    if (!enable) {
        enableBindDevice(false);
    }
    if (!profilerEnable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

#else  // DEVICE_CAP_TEST
TEST(NeuroPilotUnittest, utBindDeviceApiSupport) {
    bool enable = getBindDeviceSupport();
    if (enable) {
        enableBindDevice(false);
    }
    int ret = utBindDeviceApi(false);
    if (enable) {
        enableBindDevice(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindDeviceApiNotSupport) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindDeviceApi(true);
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindOpToDeviceByName) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindDevice(true, false);
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindOpToDeviceById) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindDevice(false, false);
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindAllOpsToDeviceByName) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindDevice(true, true);
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindAllOpsToDeviceById) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindDevice(false, true);
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utSetPartitionExtType) {
    int ret = utSetPartitionExtType();
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utPartitionExtension) {
    int ret = utPartitionExtension(0) | utPartitionExtension(1);
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerApiSupport) {
    bool enable = getProfilerSupport();
    if (enable) {
        enableProfiler(false);
    }
    int ret = utProfilerApi(false);
    if (enable) {
        enableProfiler(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerApiNotSupport) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utProfilerApi(true);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerDefaultPartition) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utProfiler(ANEUROPILOT_PARTITIONING_EXTENSION_NONE);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerPerOperation) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utProfiler(ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utOperationResultDefaultPartition) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utOperationResult(ANEUROPILOT_PARTITIONING_EXTENSION_NONE);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utOperationResultPerOperation) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utOperationResult(ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utSetCpuOnly) {
    int ret = utSetCpuOnly();
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utCustomOperation) {
    int ret = utCustomOperation();
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utNeuroPilotOperationName) {
    int ret = utNeuroPilotOperationName();
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utCreateAshmemMemory) {
    bool enable = getIonSupport();
    if (enable) {
        enableIon(false);
    }
    int ret = utCreateHidlMemory(0);
    if (enable) {
        enableIon(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utCreateIonMemory) {
    bool enable = getIonSupport();
    if (!enable) {
        enableIon(true);
    }
    int ret = utCreateHidlMemory(1);
    if (!enable) {
        enableIon(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utGetAshmemMemoryPointer) {
    bool enable = getIonSupport();
    if (enable) {
        enableIon(false);
    }
    int ret = utGetMemoryPointer();
    if (enable) {
        enableIon(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utGetIonMemoryPointer) {
    bool enable = getIonSupport();
    if (!enable) {
        enableIon(true);
    }
    int ret = utGetMemoryPointer();
    if (!enable) {
        enableIon(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utMapAshmemMemory) {
    bool enable = getIonSupport();
    if (enable) {
        enableIon(false);
    }
    int ret = utGetMemoryPointer();
    if (enable) {
        enableIon(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utMapIonMemory) {
    bool enable = getIonSupport();
    if (!enable) {
        enableIon(true);
    }
    int ret = utGetMemoryPointer();
    if (!enable) {
        enableIon(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utMtkRuntimePoolInfoAshmem) {
    bool enable = getIonSupport();
    if (enable) {
        enableIon(false);
    }
    int ret = utMtkRuntimePoolInfo(0);
    if (enable) {
        enableIon(true);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utMtkRuntimePoolInfoIon) {
    bool enable = getIonSupport();
    if (!enable) {
        enableIon(true);
    }
    int ret = utMtkRuntimePoolInfo(1);
    if (!enable) {
        enableIon(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utEnableIonRunGpu) {
    bool enable = getIonSupport();
    if (!enable) {
        enableIon(true);
    }
    bool enableP = getProfilerSupport();
    if (!enableP) {
        enableProfiler(true);
    }
    int ret = utRunExecution();
    if (!enable) {
        enableIon(false);
    }
    if (!enableP) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utDisableIonRunGpu) {
    bool enable = getIonSupport();
    if (enable) {
        enableIon(false);
    }
    bool enableP = getProfilerSupport();
    if (!enableP) {
        enableProfiler(true);
    }
    int ret = utRunExecution();
    if (enable) {
        enableIon(true);
    }
    if (!enableP) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utSetOemOperandValue) {
    int ret = utSetOemOperandValue();
    ASSERT_EQ(1, ret);
}

// TODO: Remove these legacy cases when phase out the APIs.
// Test Legacy APIs Start
 TEST(NeuroPilotUnittest, utSelRuntimeLegacy) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utSelRuntimeLegacy();
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utBindOperationToDeviceLegacy) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    int ret = utBindOperationToDeviceLegacy();
    if (!enable) {
        enableBindDevice(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utRunBindOperationToDeviceLegacy) {
    bool enable = getBindDeviceSupport();
    if (!enable) {
        enableBindDevice(true);
    }
    bool profilerEnable = getProfilerSupport();
    if (!profilerEnable) {
        enableProfiler(true);
    }

    int ret = utRunBindOperationToDeviceLegacy();
    if (!enable) {
        enableBindDevice(false);
    }
    if (!profilerEnable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerGraphLegacy) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utProfilerGraphLegacy();
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utProfilerOperationLegacy) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utProfilerOperationLegacy();
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utDebuggerKeepOpLegacy0) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utDebuggerLegacy(0);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utDebuggerKeepOpLegacy1) {
    bool enable = getProfilerSupport();
    if (!enable) {
        enableProfiler(true);
    }
    int ret = utDebuggerLegacy(1);
    if (!enable) {
        enableProfiler(false);
    }
    ASSERT_EQ(1, ret);
}

TEST(NeuroPilotUnittest, utSetCpuOnlyLegacy) {
    int ret = utSetCpuOnlyLegacy();
    ASSERT_EQ(1, ret);
}
// Test Legacy APIs End

#endif  // DEVICE_CAP_TEST

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    int ret;

    prepareData();

    ret = RUN_ALL_TESTS();

    releaseData();
    return 0;
}

