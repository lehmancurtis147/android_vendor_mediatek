/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include <stdio.h>
#include <gtest/gtest.h>

// NeuroPilot Supported Operation
#include "NeuralNetworksWrapper.h"
#include "NeuralNetworksOEM.h"

#include "NeuroPilotNN.h"
#include "NeuroPilotShim.h"

#ifndef NNTEST_ONLY_PUBLIC_API
// Set PartitionExt
#include "CompilationBuilder.h"
#include "ExecutionPlan.h"

// Get operation name
#include "Utils.h"

// Memory Extension
#include "Memory.h"
#include "CpuExecutor.h"

#else

// Memory Extension
#include "MtkRunTimePoolInfo.h"

#endif

// Profiler
#include <thread>
#include "NeuroPilotPrivate.h"

// Ion
#include <ion/ion.h>
#include <cutils/native_handle.h>
#include <hidlmemory/mapping.h>
#include <sys/mman.h>

#define ION_HEAP_TYPE 1024   // 1 << HEAP_TYPE_MULTI_MEDIA;

#define ROWS        4
#define COLS        4
#define ROWS_OUT    2
#define COLS_OUT    2

typedef float Matrix4x4[4][4];
typedef float MatrixInput[ROWS][COLS];
typedef float MatrixOutput[ROWS_OUT][COLS_OUT];
typedef float MatrixFilter[2][2];
typedef float MatrixBias[1];

static const MatrixInput inputMat = {
    {1.f, 2.f, 3.f, 4.f},
    {5.f, 6.f, 7.f, 8.f},
    {9.f, 10.f, 11.f, 12.f},
    {13.f, 14.f, 15.f, 16.f},
};

static const MatrixFilter filterMat = {
    {1.f, 1.f},
    {1.f, 1.f}
};

static const MatrixOutput matrix = {
    {2.f, 4.f},
    {6.f, 8.f}
};

static const MatrixBias biasMat = {1};

static const MatrixOutput expectedMat = {
    {17.f, 27.f},
    {53.f, 63.f},
};

static int CompareMatrices(const MatrixOutput expected, const MatrixOutput actual) {
    int errors = 0;
    for (int i = 0; i < ROWS_OUT; i++) {
        for (int j = 0; j < COLS_OUT; j++) {
            if (expected[i][j] != actual[i][j]) {
                printf("expected[%d][%d] != actual[%d][%d], %f != %f\n", i, j, i, j,
                       static_cast<double>(expected[i][j]), static_cast<double>(actual[i][j]));
                errors++;
            }
        }
    }
    return errors;
}

int createModelForTest(ANeuralNetworksModel *model) {
    //
    // create operand
    //
    std::vector<uint32_t> dim{1, ROWS, COLS, 1};
    ANeuralNetworksOperandType op = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(dim.size()),
        .dimensions        = dim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> dimOut{1, ROWS_OUT, COLS_OUT, 1};
    ANeuralNetworksOperandType opOut = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(dimOut.size()),
        .dimensions        = dimOut.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };


    std::vector<uint32_t> filterDim{1, 2, 2, 1};
    ANeuralNetworksOperandType opFilter = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(filterDim.size()),
        .dimensions        = filterDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> biasDim{1};
    ANeuralNetworksOperandType opBias = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(biasDim.size()),
        .dimensions        = biasDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> pad0Data;
    ANeuralNetworksOperandType pad0 = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = static_cast<uint32_t>(pad0Data.size()),
        .dimensions        = pad0Data.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> addDim{1, ROWS_OUT, COLS_OUT, 1};
    ANeuralNetworksOperandType addOp = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_TENSOR_FLOAT32),
        .dimensionCount    = static_cast<uint32_t>(addDim.size()),
        .dimensions        = addDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    std::vector<uint32_t> addScalarDim;
    ANeuralNetworksOperandType addScalar = {
        .type              = static_cast<int32_t>(ANEURALNETWORKS_INT32),
        .dimensionCount    = static_cast<uint32_t>(addScalarDim.size()),
        .dimensions        = addScalarDim.data(),
        .scale             = 0.0f,
        .zeroPoint         = 0
    };

    //
    // max_pooling_2d operands
    // index : 0 -> input matrix
    // index : 1 -> filter matrix
    // index : 2 -> bias matrix
    // index : 3 -> padding_left
    // index : 4 -> padding_right
    // index : 5 -> padding_top
    // index : 6 -> padding_bottom
    // index : 7 -> stride_width
    // index : 8 -> stride_height
    // index : 9 -> activation
    // index : 10 -> conv2D output (add input)
    // index : 11 -> add input
    // index : 12 -> add activation
    // index : 13 -> output
    //
#define ADD_OPERAND(m, t) \
    if (ANeuralNetworksModel_addOperand(m, &t) != ANEURALNETWORKS_NO_ERROR) { \
        printf("failed to ANeuralNetworksModel_addOperand\n"); \
        ANeuralNetworksModel_free(m); \
        return 0; \
    }

    ADD_OPERAND(model, op)
    ADD_OPERAND(model, opFilter)
    ADD_OPERAND(model, opBias)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, pad0)
    ADD_OPERAND(model, opOut)
    ADD_OPERAND(model, addOp)
    ADD_OPERAND(model, addScalar)
    ADD_OPERAND(model, addOp)

    //
    // set operand value
    //
#define SET_INT32_OPERAND_VALUE(v, i, m) \
    { \
        int32_t val(v); \
        if (ANeuralNetworksModel_setOperandValue(m, i, &val, sizeof(val)) != ANEURALNETWORKS_NO_ERROR) { \
            printf("failed to ANeuralNetworksModel_setOperandValue\n"); \
            ANeuralNetworksModel_free(m); \
            return 0; \
        } \
    }

    SET_INT32_OPERAND_VALUE(0, 3, model)
    SET_INT32_OPERAND_VALUE(0, 4, model)
    SET_INT32_OPERAND_VALUE(0, 5, model)
    SET_INT32_OPERAND_VALUE(0, 6, model)
    SET_INT32_OPERAND_VALUE(2, 7, model)
    SET_INT32_OPERAND_VALUE(2, 8, model)
    SET_INT32_OPERAND_VALUE(0, 9, model)
    SET_INT32_OPERAND_VALUE(0, 12, model)

#define SET_TF_OPERAND_VALUE(v, l, i, m) \
    { \
        if (ANeuralNetworksModel_setOperandValue(m, i, &v, sizeof(float) * l) != ANEURALNETWORKS_NO_ERROR) { \
            printf("failed to ANeuralNetworksModel_setOperandValue\n"); \
            ANeuralNetworksModel_free(m); \
            return 0; \
        } \
    }

    SET_TF_OPERAND_VALUE(filterMat, filterDim.size(), 1, model)
    SET_TF_OPERAND_VALUE(biasMat,   biasDim.size(), 2, model)


    //
    // add operation
    //
    std::vector<uint32_t> in{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<uint32_t> out{10};
    if (ANeuralNetworksModel_addOperation(
            model, ANEURALNETWORKS_CONV_2D, static_cast<uint32_t>(in.size()), in.data(),
            static_cast<uint32_t>(out.size()), out.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    std::vector<uint32_t> in2{10, 11, 12};
    std::vector<uint32_t> out2{13};
    if (ANeuralNetworksModel_addOperation(
            model, ANEURALNETWORKS_ADD, static_cast<uint32_t>(in2.size()), in2.data(),
            static_cast<uint32_t>(out2.size()), out2.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_addOperation\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    //
    // set inputs and outputs
    //
    std::vector<uint32_t> in3{0, 11};
    std::vector<uint32_t> out3{13};
    if (ANeuralNetworksModel_identifyInputsAndOutputs(
            model, static_cast<uint32_t>(in3.size()), in3.data(),
            static_cast<uint32_t>(out3.size()), out3.data()) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_identifyInputsAndOutputs\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    return 1;
}

int createRequestForTest(ANeuralNetworksExecution *execution, MatrixOutput *outputMat) {
    //
    // set buffer
    //
    if (ANeuralNetworksExecution_setInput(
            execution,
            0,
            nullptr,
            inputMat,
            sizeof(MatrixInput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - input\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setInput(
            execution,
            1,
            nullptr,
            matrix,
            sizeof(MatrixOutput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setInput - bias\n");
        return 0;
    }

    if (ANeuralNetworksExecution_setOutput(
            execution, 0, nullptr, outputMat, sizeof(MatrixOutput)) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_setOutput\n");
        return 0;
    }


    return 1;
}


static int startCompute(ANeuralNetworksExecution* execution) {
    //
    // start to compute
    //
    ANeuralNetworksEvent* event = nullptr;
    if (ANeuralNetworksExecution_startCompute(execution, &event) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_startCompute\n");
        return 0;
    }

    ANeuralNetworksEvent_wait(event);
    ANeuralNetworksEvent_free(event);

    return 1;
}

#ifndef NNTEST_ONLY_PUBLIC_API
const char* kOperationNames[android::nn::kNumberOfOperationTypes] = {
        "ADD",
        "AVERAGE_POOL",
        "CONCATENATION",
        "CONV",
        "DEPTHWISE_CONV",
        "DEPTH_TO_SPACE",
        "DEQUANTIZE",
        "EMBEDDING_LOOKUP",
        "FLOOR",
        "FULLY_CONNECTED",
        "HASHTABLE_LOOKUP",
        "L2_NORMALIZATION",
        "L2_POOL",
        "LOCAL_RESPONSE_NORMALIZATION",
        "LOGISTIC",
        "LSH_PROJECTION",
        "LSTM",
        "MAX_POOL",
        "MUL",
        "RELU",
        "RELU1",
        "RELU6",
        "RESHAPE",
        "RESIZE_BILINEAR",
        "RNN",
        "SOFTMAX",
        "SPACE_TO_DEPTH",
        "SVDF",
        "TANH",
        "BATCH_TO_SPACE_ND",
        "DIV",
        "MEAN",
        "PAD",
        "SPACE_TO_BATCH_ND",
        "SQUEEZE",
        "STRIDED_SLICE",
        "SUB",
        "TRANSPOSE",
};
#endif

bool testNpOperationType(android::nn::wrapper::Model *model, uint32_t type) {
    // Prepare a model
    android::nn::wrapper::OperandType type0(android::nn::wrapper::Type::TENSOR_FLOAT32, {2});
    auto op1 = model->addOperand(&type0);
    auto op2 = model->addOperand(&type0);
    auto op3 = model->addOperand(&type0);

    // Test add operation
    model->addOperation(type, {op1, op2}, {op3});
    if (!model->isValid()) {
        return false;
    }
    return true;
}

/*************************************************************************************************/
int utRunExecution() {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;

    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

// Profiler
int utProfilerApi(bool featureOn) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }
    if (createModelForTest(model) == 0) {
        return 0;
    }
    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    int ret = ANeuroPilotExecutionPrivate_setCurrentExecutionStep(execution, 0);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotExecutionPrivate_setCurrentExecutionStep\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotExecutionPrivate_setCurrentExecutionStep\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }

    ret = ANeuroPilotExecutionPrivate_setExecution(std::this_thread::get_id(), execution);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotExecutionPrivate_setExecution\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotExecutionPrivate_setExecution\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }

    std::vector<ProfilerResult> infos;
    ret = ANeuroPilotExecutionWrapper_getProfilerInfo(execution, &infos);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotExecutionWrapper_getProfilerInfo\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotExecutionWrapper_getProfilerInfo\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }

    ret = ANeuroPilotExecutionPrivate_clearProfilerInfo(execution);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotExecutionPrivate_clearProfilerInfo\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotExecutionPrivate_clearProfilerInfo\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }

    ret = ANeuroPilotExecutionPrivate_eraseExecution(std::this_thread::get_id());
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotExecutionPrivate_eraseExecution\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotExecutionPrivate_eraseExecution\n");
            ANeuralNetworksModel_free(model);
            ANeuralNetworksCompilation_free(compilation);
            ANeuralNetworksExecution_free(execution);
            return 0;
        }
    }

    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);
    ANeuralNetworksExecution_free(execution);
    return 1;
}

int utProfiler(int partitionType) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, partitionType) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuroPilotCompilationWrapper_setPartitionExtType\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }


    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    std::vector<ProfilerResult> infos;
    if (ANeuroPilotExecutionWrapper_getProfilerInfo(execution, &infos)
            != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_getProfilerInfo\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    int stepCount;
    for (auto info : infos) {
        printf("========== Profiler Start ==========\n");
        printf("Execution Step   : %d\n", info.step);
        printf("Execution Result : %d\n", info.success);
        printf("Device Name      : %s\n", info.deviceTime.devName.c_str());
        printf("Operations       : %s\n", info.deviceTime.opName.c_str());
        printf("Spent Time       : %f us\n", info.deviceTime.delta);
        printf("========== Profiler End ============\n");
        stepCount = info.step;
    }

    if (partitionType == ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION && stepCount != 1) {
        printf("failed to partition, step: %d\n", stepCount);
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

// Operation Result
int utOperationResult(int partitionType) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, partitionType) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuroPilotCompilationWrapper_setPartitionExtType\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }


    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    std::vector<ProfilerResult> infos;
    if (ANeuroPilotExecutionWrapper_getProfilerInfo(execution, &infos)
            != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_getProfilerInfo\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    uint32_t size;
    auto printOperationResult = [&](auto out) {
        for (uint32_t k = 0; k < size; k++) {
            printf("%s,", std::to_string(out[k]).c_str());
        }
        printf("\n");
    };

    for (auto info : infos) {
        printf("========== Profiler Start ==========\n");
        printf("Execution Step   : %d\n", info.step);
        printf("Execution Result : %d\n", info.success);
        for (auto& result : info.opResults) {
            size = result.size;
            printf("------ Output [0] ----------\n");
            printf("Operand Type     : %d\n", result.operandType);
            printf("Size             : %d\n", size);
            printf("Result           : ");
            printOperationResult((float *) result.buffer);
        }
        printf("========== Profiler End ============\n\n");
     }

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

// Bind Device
int utBindDeviceApi(bool featureOn) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }
    if (createModelForTest(model) == 0) {
        return 0;
    }

    std::string device;
    int ret = ANeuroPilotModelWrapper_getAvailableDeviceName(device);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_getAvailableDeviceName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_getAvailableDeviceName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }

    uint32_t deviceId = 0;
    ret = ANeuroPilotModelWrapper_getAvailableDevice(&deviceId);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_getAvailableDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_getAvailableDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }

    ret = ANeuroPilotModelWrapper_bindOperationToDeviceByName(
            model, ANEURALNETWORKS_ADD, "cpunn");
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_bindOperationToDeviceByName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_bindOperationToDeviceByName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }

    ret = ANeuroPilotModelWrapper_bindOperationToDevice(
            model, ANEURALNETWORKS_ADD, ANEUROPILOT_CPU);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_bindOperationToDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_bindOperationToDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }

    ret = ANeuroPilotModelWrapper_bindAllOperationsToDeviceByName(model, "cpunn");
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_bindAllOperationsToDeviceByName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_bindAllOperationsToDeviceByName\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }

    ret = ANeuroPilotModelWrapper_bindAllOperationsToDevice(model, ANEUROPILOT_CPU);
    if (featureOn) {
        if (ret != ANEURALNETWORKS_NO_ERROR) {
            printf("failed to ANeuroPilotModelWrapper_bindAllOperationsToDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    } else {
        if (ret == ANEURALNETWORKS_NO_ERROR) {
            printf("Should not pass ANeuroPilotModelWrapper_bindAllOperationsToDevice\n");
            ANeuralNetworksModel_free(model);
            return 0;
        }
    }
    ANeuralNetworksModel_free(model);
    return 1;
}

int utBindDevice(bool byName, bool bindAll) {
    int ret = 1;
    if (byName) {
        std::string devs;
        if (ANeuroPilotModelWrapper_getAvailableDeviceName(devs) != ANEURALNETWORKS_NO_ERROR) {
            printf("fail to get available device");
        }
        printf("registered devs : %s\n", devs.c_str());
        if (devs.empty()) {
            printf("Get ANeuroPilotModelWrapper_getAvailableDeviceName error\n");
            return 0;
        }
    } else {
        uint32_t device = 0;
        ANeuroPilotModelWrapper_getAvailableDevice(&device);
        printf("registered devs : %d\n", device);
        if (device == 0) {
            printf("Get ANeuroPilotModelWrapper_getAvailableDevice error\n");
            return 0;
        }
    }

    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (byName) {
        if (bindAll) {
            if (ANeuroPilotModelWrapper_bindAllOperationsToDeviceByName(model, "cpunn")
                    != ANEURALNETWORKS_NO_ERROR) {
                printf("failed to ANeuroPilotModelWrapper_bindOperationToDeviceByName\n");
                ANeuralNetworksModel_free(model);
                return 0;
            }

        } else {
            if (ANeuroPilotModelWrapper_bindOperationToDeviceByName(
                    model, ANEURALNETWORKS_ADD, "cpunn") != ANEURALNETWORKS_NO_ERROR) {
                printf("failed to ANeuroPilotModelWrapper_bindOperationToDeviceByName\n");
                ANeuralNetworksModel_free(model);
                return 0;
            }
        }
    } else {
        if (bindAll) {
            if (ANeuroPilotModelWrapper_bindAllOperationsToDevice(
                    model, ANEUROPILOT_CPU) != ANEURALNETWORKS_NO_ERROR) {
                printf("failed to ANeuroPilotModelWrapper_bindOperationToDevice\n");
                ANeuralNetworksModel_free(model);
                return 0;
            }


        } else {
            if (ANeuroPilotModelWrapper_bindOperationToDevice(
                    model, ANEURALNETWORKS_ADD, ANEUROPILOT_CPU) != ANEURALNETWORKS_NO_ERROR) {
                printf("failed to ANeuroPilotModelWrapper_bindOperationToDevice\n");
                ANeuralNetworksModel_free(model);
                return 0;
            }
        }
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    //
    // start to compute
    //
    ANeuralNetworksEvent* event = nullptr;
    if (ANeuralNetworksExecution_startCompute(execution, &event) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_startCompute\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksEvent_wait(event);

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ret = 0;
    }

    ANeuralNetworksEvent_free(event);
    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return ret;
}

int utSetPartitionExtType() {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuroPilotCompilationWrapper_setPartitionExtType\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, 999) == ANEURALNETWORKS_NO_ERROR) {
        printf("ANeuroPilotCompilationWrapper_setPartitionExtType should not pass invaild type\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, ANEUROPILOT_PARTITIONING_EXTENSION_NONE)
            == ANEURALNETWORKS_NO_ERROR) {
        printf("Should not success when set partitionExtType after compilation finished\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Compilation End

    ANeuralNetworksModel_free(model);
    return 1;
}

int utPartitionExtension(int partition) {
    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, partition) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuroPilotCompilationWrapper_setPartitionExtType\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    if (ANeuroPilotCompilationWrapper_setPartitionExtType(
            compilation, ANEUROPILOT_PARTITIONING_EXTENSION_NONE)
            == ANEURALNETWORKS_NO_ERROR) {
        printf("Should not success when set partitionExtType after compilation finished\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Compilation End

#ifndef NNTEST_ONLY_PUBLIC_API
    // Make sure we really devided graph into two sub-graphs.
    android::nn::CompilationBuilder* c
            = reinterpret_cast<android::nn::CompilationBuilder*>(compilation);

    if (c->forTest_getExecutionPlan().forTest_getKind()
            != android::nn::ExecutionPlan::Kind::COMPOUND) {
        printf("Should be compound body\n");
        return 0;
    }

    const auto& steps = c->forTest_getExecutionPlan().forTest_compoundGetSteps();

    int step = 1;
    if (partition == ANEUROPILOT_PARTITIONING_EXTENSION_PER_OPERATION) {
        step = 2;
    }

    if (steps.size() != step) {
        printf("fail to divied into %d graphs\n", step);
        return 0;
    }
#endif

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    //
    // start to compute
    //
    ANeuralNetworksEvent* event = nullptr;
    if (ANeuralNetworksExecution_startCompute(execution, &event) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_startCompute\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksEvent_wait(event);
    //
    // compare results
    //
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksEvent_free(event);
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksEvent_free(event);
    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

// NeuroPilot Supported Operation
int utCustomOperation() {
    {
        printf("Add OEM operation\n");
        android::nn::wrapper::Model model;
        if (!testNpOperationType(&model, ANEURALNETWORKS_OEM_OPERATION)) {
            printf("Add OEM operation fail\n");
            return 0;
        }
    }

    {
        printf("Add invalid operation\n");
        android::nn::wrapper::Model model;
        if (testNpOperationType(&model, 99999)) {
            printf("Add NeuroPilot invaild operation type 99999 should not be successed \n");
            return 0;
        }
    }
    return 1;
}

int utNeuroPilotOperationName() {
#ifndef NNTEST_ONLY_PUBLIC_API
    printf("Get AOSP operation name\n");
    for (uint32_t i = 0; i < android::nn::kNumberOfOperationTypes; i++) {
        const char* name = android::nn::getOperationName(static_cast<OperationType>(i));
        if (strcmp(name, kOperationNames[i]) != 0) {
            printf("GetOperationName[%d] error: %s\n", i, name);
            return 0;
        }
    }

    printf("Get OEM operation name\n");
    const char* name = android::nn::getOperationName(
            static_cast<OperationType>(ANEURALNETWORKS_OEM_OPERATION));
    if (strcmp(name, "OEM_OPERATION") != 0) {
        printf("Get OEM OperationName error: %s\n", name);
        return 0;
    }
#else
    printf("Only support for static test\n");
#endif
    return 1;
}


// Set Cpu only
int utSetCpuOnly() {
    if (ANeuroPilotUtilsWrapper_setCpuOnly(true) != ANEURALNETWORKS_NO_ERROR) {
        printf("set cpu only fail");
        return 0;
    }

    if (ANeuroPilotUtilsWrapper_setCpuOnly(false) != ANEURALNETWORKS_NO_ERROR) {
        printf("set cpu only fail");
        return 0;
    }

    return 1;
}

int utDeviceCapWorseThanCpu() {
    uint32_t devices = 0;
    if (ANeuroPilotModelWrapper_getAvailableDevice(&devices) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to get available device\n");
        return 0;
    }
    if ((devices & ANEUROPILOT_GPU) != ANEUROPILOT_GPU) {
        printf("GPU unavailable %d\n", devices);
        return 0;
    }

    // Create Model Start
    ANeuralNetworksModel *model = nullptr;
    if (ANeuralNetworksModel_create(&model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_create\n");
        return 0;
    }

    if (createModelForTest(model) == 0) {
        return 0;
    }

    if (ANeuralNetworksModel_finish(model) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksModel_finish\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }
    // Create Model End

    // Create Compilation Start
    ANeuralNetworksCompilation* compilation = nullptr;
    if (ANeuralNetworksCompilation_create(model, &compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_create\n");
        ANeuralNetworksModel_free(model);
        return 0;
    }

    if (ANeuralNetworksCompilation_finish(compilation) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksCompilation_finish\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }
    // Create Compilation End

    // Create Execution and Request Start
    ANeuralNetworksExecution* execution = nullptr;
    if (ANeuralNetworksExecution_create(compilation, &execution) != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_create\n");
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    MatrixOutput outputMat;
    memset(&outputMat, 0, sizeof(outputMat));
    if (createRequestForTest(execution, &outputMat) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }
    // Create Execution and Request End

    if (startCompute(execution) == 0) {
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        ANeuralNetworksExecution_free(execution);
        return 0;
    }

    std::vector<ProfilerResult> infos;
    if (ANeuroPilotExecutionWrapper_getProfilerInfo(execution, &infos)
            != ANEURALNETWORKS_NO_ERROR) {
        printf("failed to ANeuralNetworksExecution_getProfilerInfo\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    int stepCount;
    for (auto info : infos) {
        printf("========== Profiler Start ==========\n");
        printf("Execution Step   : %d\n", info.step);
        printf("Execution Result : %d\n", info.success);
        printf("Device Name      : %s\n", info.deviceTime.devName.c_str());
        printf("Operations       : %s\n", info.deviceTime.opName.c_str());
        printf("Spent Time       : %f us\n", info.deviceTime.delta);
        printf("========== Profiler End ============\n");
        stepCount = info.step;

        if (info.deviceTime.devName != "CPU") {
            printf("failed to Use CPU\n");
            return 0;
        }
    }

    // Compare Results
    if (CompareMatrices(expectedMat, outputMat) != 0) {
        printf("failed to compare matrices\n");
        ANeuralNetworksExecution_free(execution);
        ANeuralNetworksModel_free(model);
        ANeuralNetworksCompilation_free(compilation);
        return 0;
    }

    ANeuralNetworksExecution_free(execution);
    ANeuralNetworksModel_free(model);
    ANeuralNetworksCompilation_free(compilation);

    return 1;
}

int utCreateHidlMemory(int type) {
#ifndef NNTEST_ONLY_PUBLIC_API
    android::nn::MemoryExt memory;
    int n = memory.create(10);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        printf("Create memory error\n");
        return 0;
    }

    if (type == ANEUROPILOT_MEMORY_ASHMEM) {
        if (memory.getHidlMemory().name() != "ashmem") {
            printf("Hidl memory name is not ashmem.\n");
            return 0;
        }
        if (memory.getMemoryType() != ANEUROPILOT_MEMORY_ASHMEM) {
            printf("Memory type is not ANEUROPILOT_MEMORY_ASHMEM.\n");
            return 0;
        }
    } else if (type == ANEUROPILOT_MEMORY_ION) {
        if (memory.getHidlMemory().name() != "ion") {
            printf("Hidl memory name is not ion.\n");
            return 0;
        }
        if (memory.getMemoryType() != ANEUROPILOT_MEMORY_ION) {
            printf("Memory type is not ANEUROPILOT_MEMORY_ION.\n");
            return 0;
        }
    } else {
        printf("input Type error\n");
    }

#else
    printf("Only support for static test\n");
#endif
    return 1;
}

int utGetMemoryPointer() {
#ifndef NNTEST_ONLY_PUBLIC_API
    android::nn::MemoryExt memory;
    int n = memory.create(10);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        printf("Create memory error\n");
        return 0;
    }

    uint8_t *data = nullptr;
    n = memory.getPointer(&data);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        printf("Get Pointer error\n");
        return 0;
    }
    uint8_t test[4] = {'t', 'e', 's', 't'};
    memcpy(data, test, 4);

    printf("%s\n", data);
    std::string str(data, data+4);
    if (str != "test") {
        printf("memory copy fail\n");
        return 0;
    }
#else
    printf("Only support for static test\n");
#endif
    return 1;
}

int utMapMemory() {
#ifndef NNTEST_ONLY_PUBLIC_API
    android::nn::MemoryExt memory;
    int n = memory.create(10);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        printf("Create memory error\n");
        return 0;
    }

    uint8_t *data = nullptr;
    n = memory.getPointer(&data);
    if (n != ANEURALNETWORKS_NO_ERROR) {
        printf("Get Pointer error\n");
        return 0;
    }
    uint8_t test[4] = {'t', 'e', 's', 't'};
    memcpy(data, test, 4);

    int type = memory.getMemoryType();
    const auto& hidlMemory = memory.getHidlMemory();

    uint8_t* buffer = nullptr;
    if (type == ANEUROPILOT_MEMORY_ASHMEM) {
        size_t size = hidlMemory.size();
        int fd = hidlMemory.handle()->data[0];
        int prot = hidlMemory.handle()->data[1];
        size_t offset = android::nn::getSizeFromInts(hidlMemory.handle()->data[2],
                                        hidlMemory.handle()->data[3]);
        buffer = static_cast<uint8_t*>(mmap(nullptr, size, prot, MAP_SHARED, fd, offset));
        if (buffer == MAP_FAILED) {
            printf("Can't mmap the ashmem file descriptor.\n");
            return 0;
        }
    } else if (type == ANEUROPILOT_MEMORY_ION) {
        size_t size = hidlMemory.size();
        int fd = hidlMemory.handle()->data[0];
        int prot = hidlMemory.handle()->data[1];
        buffer = static_cast<uint8_t*>(mmap(nullptr, size, prot, MAP_SHARED, fd, 0));
        if (buffer == MAP_FAILED) {
            printf("Can't mmap the ion file descriptor.\n");
            return 0;
        }
    } else {
        printf("input Type error\n");
        return 0;
    }

    std::string str(buffer, buffer + 4);
    printf("%s\n", buffer);

    if (str != "test") {
        printf("memory map fail\n");
        return 0;
    }
#else
    printf("Only support for static test\n");
#endif
    return 1;
}

android::hardware::hidl_memory createAshmemHidlMemory() {
    android::hardware::hidl_memory hidlMemory = android::nn::allocateSharedMemory(4);
    android::sp<IMemory> memory;
    uint8_t* buffer = nullptr;

    memory = mapMemory(hidlMemory);
    if (memory == nullptr) {
        printf("Can't map shared memory.\n");
        return android::hardware::hidl_memory();
    }
    memory->update();
    buffer = reinterpret_cast<uint8_t*>(static_cast<void*>(memory->getPointer()));
    if (buffer == nullptr) {
        printf("Can't access shared memory.\n");
        return android::hardware::hidl_memory();
    }

    uint8_t test[4] = {'t', 'e', 's', 't'};
    memcpy(buffer, test, 4);

    return hidlMemory;
}

android::hardware::hidl_memory createIonHidlMemory() {
    int ionFd;
    int shareFd;
    ion_user_handle_t handle;
    native_handle_t* nativeHandle = nullptr;
    int size = 4;
    unsigned int flag = 0;
    int heapType = ION_HEAP_TYPE;
    int prot = PROT_READ | PROT_WRITE;

    nativeHandle = native_handle_create(1, 4);
    if (nativeHandle == nullptr) {
        printf("Failed to create native_handle\n");
        return android::hardware::hidl_memory();
    }

    // open a ion fd, register as a ion client
    ionFd = ion_open();
    if (ionFd < 0) {
        printf("Cannot open ion device.\n");
        native_handle_delete(nativeHandle);
        return android::hardware::hidl_memory();
    }
    // Debug Infos
    printf("Debug info: ionFd: %d\n", ionFd);
    printf("Debug info: heap type: %d\n", heapType);
    printf("Debug info: size: %d\n", size);
    printf("Debug info: flag: %d\n", flag);

    // request memory from ion core
    int ret = ion_alloc(ionFd, size, 0, heapType, flag, &handle);
    if (ret < 0) {
        printf("IOCTL[ION_IOC_ALLOC] failed!\n");
        native_handle_delete(nativeHandle);
        return android::hardware::hidl_memory();
    }

    // get share memory fd
    ret = ion_share(ionFd, handle, &shareFd);
    if (ret < 0) {
        printf("IOCTL[ION_IOC_SHARE] failed!\n");
        native_handle_delete(nativeHandle);
        return android::hardware::hidl_memory();
    }

    printf("Debug info: shareFd: %d\n", shareFd);

    uint8_t* buffer = nullptr;
    buffer = static_cast<uint8_t*>(mmap(nullptr, size, prot, MAP_SHARED, shareFd, 0));
    uint8_t test[4] = {'t', 'e', 's', 't'};
    memcpy(buffer, test, 4);

    nativeHandle->data[0] = shareFd;
    nativeHandle->data[1] = prot;
    nativeHandle->data[2] = ionFd;
    nativeHandle->data[3] = handle;
    return android::hardware::hidl_memory("ion", nativeHandle, size);
}

int freeIonMemory(const android::hardware::hidl_memory &memory) {
    int ret = 1;
    if (memory.handle()) {
        int shareFd = memory.handle()->data[0];
        int ionFd = memory.handle()->data[2];
        ion_user_handle_t handle = memory.handle()->data[3];

        printf("Free Ion memory: shareFd %d\n", shareFd);
        printf("Free Ion memory: ionFd %d\n", ionFd);
        printf("Free Ion memory: handle %d\n", handle);

        ion_close(shareFd);
        if (ion_free(ionFd, handle)) {
            printf("IOCTL[ION_IOC_FREE] failed!\n");
            ret = 0;
        }
        ion_close(ionFd);
        native_handle_delete(const_cast<native_handle_t*>(memory.handle()));
    }
    return ret;
}

int utMtkRuntimePoolInfo(int type) {
#ifndef NNTEST_ONLY_PUBLIC_API
    printf("Only support for non static test\n");
#else
    bool fail = false;
    if (type == ANEUROPILOT_MEMORY_ASHMEM) {
        const android::hardware::hidl_memory memory = createAshmemHidlMemory();
        android::nn::MtkRunTimePoolInfo poolInfo = android::nn::MtkRunTimePoolInfo(memory, &fail);
        if (fail) {
            printf("Create MtkRunTimePoolInfo fail\n");
            return 0;
        }

        uint8_t *buffer = poolInfo.getBuffer();
        std::string str(buffer, buffer + 4);
        printf("%s\n", buffer);
        if (str != "test") {
            printf("getBuffer fail\n");
            return 0;
        }

    } else if (type == ANEUROPILOT_MEMORY_ION) {
        const android::hardware::hidl_memory memory = createIonHidlMemory();
        android::nn::MtkRunTimePoolInfo poolInfo = android::nn::MtkRunTimePoolInfo(memory, &fail);
        if (fail) {
            printf("Create MtkRunTimePoolInfo fail\n");
            return 0;
        }

        uint8_t *buffer = poolInfo.getBuffer();
        std::string str(buffer, buffer + 4);
        printf("%s\n", buffer);
        if (str != "test") {
            printf("getBuffer fail\n");
            return 0;
        }
        freeIonMemory(memory);
    } else {
        printf("input Type error\n");
        return 0;
    }
#endif
    return 1;
}

int utSetOemOperandValue() {
#ifndef NNTEST_ONLY_PUBLIC_API
    printf("Only support non static test\n");
#else
    uint8_t type[5] = {'i', 'n', 't', '1', '6'};
    uint8_t buffer[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    uint8_t *raw = (uint8_t *) malloc(20);

    android::nn::OemOperandValue operand;
    operand.typeLen = 5;
    operand.type = type;
    operand.dataLen = 10;
    operand.data = buffer;

    android::nn::OemOperandValue decodeOperand;
    encodeOperandValue(&operand, raw);

    // Printf result
    for (size_t i = 0; i < 20; i++) {
        printf("%d ", raw[i]);
    }
    printf("\n");

    decodeOperandValue(raw, &decodeOperand);

    // Printf Result
    printf("typeLen %zu \n", decodeOperand.typeLen);
    for (size_t i = 0; i < decodeOperand.typeLen; i++) {
        printf("%d ", decodeOperand.type[i]);
    }

    printf("\nbufferLen %zu \n", decodeOperand.dataLen);
    for (size_t i = 0; i < decodeOperand.dataLen; i++) {
        printf("%d ", decodeOperand.data[i]);
    }
    printf("\n");

    if (decodeOperand.typeLen != 5) {
        printf("decode type length is not 5 \n");
        free(raw);
        free(decodeOperand.data);
        free(decodeOperand.type);
        return 0;
    }

    std::string str(decodeOperand.type, decodeOperand.type + 5);
    if (str != "int16") {
        printf("decode type fail\n");
        free(raw);
        free(decodeOperand.data);
        free(decodeOperand.type);
        return 0;
    }

    if (decodeOperand.dataLen != 10) {
        printf("decode dataLen length is not 10 \n");
        free(raw);
        free(decodeOperand.data);
        free(decodeOperand.type);
        return 0;
    }

    for (uint32_t i = 0; i < decodeOperand.dataLen; i++) {
        if (decodeOperand.data[i] != buffer[i]) {
            printf("raw data changes after encode/decode: i = %d, encode: %d, decode: %d\n",
                    i, buffer[i], decodeOperand.data[i]);
            free(raw);
            free(decodeOperand.data);
            free(decodeOperand.type);
            return 0;
        }
    }

    free(raw);
    free(decodeOperand.data);
    free(decodeOperand.type);
#endif
    return 1;
}
