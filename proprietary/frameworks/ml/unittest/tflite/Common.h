#ifndef __COMMON_H__
#define __COMMON_H__
#include <string>

using namespace std;

const char* getDataPath(std::string file_path);

#endif  // __COMMON_H__