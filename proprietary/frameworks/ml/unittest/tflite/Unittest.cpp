/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include <stdio.h>
#include "InterpreterWrapper.h"
#include "gtest/gtest.h"
#include <cutils/properties.h>

int getLoopCount(void) {
    char buf[PROPERTY_VALUE_MAX] = {'\0',};

    if (property_get("debug.tflite.test.loop", buf, "") > 0) {
        return atoi(buf);
    }
    return 1;
}

int loopModelRecreation(const TestItem* item) {
    int ret = 0;
    for (int i = 0; i < getLoopCount(); i++) {
        ret = runInterpreter(item);
        if (ret != 0) {
            break;
        }
    }
    return ret;
}

int loopInvoke(const TestItem* item) {
    InterpreterWrapperConfig c;
    c.loop_count = getLoopCount();
    c.show_output = false;
    c.save_output = false;
    c.use_nnapi = true;

    return runInterpreter(item, &c);
}

TEST(TFLiteStressTestFp32, mobilenet_v1_224) {
    const TestItem* item = getTestItem("mobilenet_v1_224_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, mobilenet_v2_224) {
    const TestItem* item = getTestItem("mobilenet_v2_224_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, inception_v1) {
    const TestItem* item = getTestItem("inception_v1_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, inception_v3) {
    const TestItem* item = getTestItem("inception_v3_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, resnet_v1_50) {
    const TestItem* item = getTestItem("resnet_v1_50_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, resnet_v2_50) {
    const TestItem* item = getTestItem("resnet_v2_50_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, vgg_16_fc) {
    const TestItem* item = getTestItem("vgg_16_fc_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestFp32, vgg_16) {
    const TestItem* item = getTestItem("vgg_16_model", FP32, FP32);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, mobilenet_v1_224) {
    const TestItem* item = getTestItem("mobilenet_v1_224_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, mobilenet_v2_224) {
    const TestItem* item = getTestItem("mobilenet_v2_224_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, inception_v1) {
    const TestItem* item = getTestItem("inception_v1_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, inception_v3) {
    const TestItem* item = getTestItem("inception_v3_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, resnet_v1_50) {
    const TestItem* item = getTestItem("resnet_v1_50_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, resnet_v2_50) {
    const TestItem* item = getTestItem("resnet_v2_50_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, vgg_16_fc) {
    const TestItem* item = getTestItem("vgg_16_fc_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

TEST(TFLiteStressTestUint8, vgg_16) {
    const TestItem* item = getTestItem("vgg_16_model", UINT8, UINT8);

    ASSERT_NE(nullptr, item);
    ASSERT_EQ(0, loopModelRecreation(item));
    ASSERT_EQ(0, loopInvoke(item));
}

int main(int argc, char** argv) {
    testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    return ret;
}
