#include <getopt.h>
#include <iostream>
#include <fstream>
#include <vector>
#include "gtest/gtest.h"
#include "TestItems.h"
#include "InterpreterWrapper.h"

using namespace std;

enum OutputType {
    OUTPUT_NONE = 0,
    SHOW_OUTPUT = 1,
    SAVE_OUTPUT = 2,
};

struct Settings {
    bool accel = true;
    int mode = false; // 0: default mode, 1 specified model with input/output, 2. specified model with random input
    int output_type = 0;
    int loop_count = 1;
    int data_type = FP32;
    string model_path;
    string input_path;
    string golden_path;
    bool relax_computation_float32_to_float16;
};

static std::string model_path = "/data/local/tmp/data/tfliterunner/model.lite";
static std::string input_path = "/data/local/tmp/data/tfliterunner/batch_xs.npy";
static std::string golden_path = "/data/local/tmp/data/tfliterunner/ys.npy";

Settings processOptions(int argc, char** argv) {
    static struct option long_options[] = {
        {"accelerated", required_argument, 0, 'a'},
        {"specified model", required_argument, 0, 's'},
        {"loop count", required_argument, 0, 'c'},
        {"show output", required_argument, 0, 'o'},
        {"tflite_model", required_argument, 0, 'm'},
        {"batch_xs", required_argument, 0, 'x'},
        {"ys", required_argument, 0, 'y'},
        {"relax computation float32 to float16", required_argument, 0, 'r'},
        {0, 0, 0, 0},
    };

    Settings s = {0};
    int c = -1;
    int option_index = 0;
    s.model_path = model_path;
    s.input_path = input_path;
    s.golden_path = golden_path;

    while (1) {
        /* getopt_long stores the option index here. */
        c = getopt_long(argc, argv, "a:s:c:o:m:x:y:r:", long_options,
                        &option_index);

        /* Detect the end of the options. */
        if (c == -1) break;

        switch (c) {
            case 'a':
                s.accel = strtol(optarg, (char**)NULL, 10);
                break;

            case 'c':
                s.loop_count = strtol(optarg, (char**)NULL, 10);
                break;

            case 's':
                s.mode = strtol(optarg, (char**)NULL, 10);
                break;

            case 'o':
                s.output_type = strtol(optarg, (char**)NULL, 10);
                break;

            case 'm':
                s.model_path = optarg;
                break;

            case 'x':
                s.input_path = optarg;
                break;

            case 'y':
                s.golden_path = optarg;
                break;

            case 'r':
                s.relax_computation_float32_to_float16 = strtol(optarg, (char**)NULL, 10);
                break;

            default:
                exit(-1);
        }
    }

    cout << "Loop count: " << s.loop_count << "\n";
    cout << "Use NNAPI: " << s.accel << "\n";
    cout << "Mode: " << s.mode << "\n";
    cout << "Output type: " << s.output_type << "\n";

    return s;
}

int main(int argc, char** argv) {
    Settings s = processOptions(argc, argv);
    InterpreterWrapperConfig c;
    int ret = 0;

    c.use_nnapi = s.accel;
    c.show_output = (s.output_type == SHOW_OUTPUT ? true : false);
    c.save_output = (s.output_type == SAVE_OUTPUT ? true : false);
    c.loop_count = s.loop_count;
    c.relax_computation_float32_to_float16 = s.relax_computation_float32_to_float16;

    switch (s.mode) {
    case 0: {
            const TestItem* item = nullptr;

            for (int i = 0; i < getTestItemSize(); i++) {
                item = getTestItemByIndex(i);

                if (item != nullptr) {
                    ret = runInterpreter(item, &c);
                }
            }
        }
        break;
    case 1: {
            TestItem item = {
                "",
                s.model_path.c_str(),
                s.input_path.c_str(),
                s.golden_path.c_str(),
                s.data_type,
                s.data_type,
            };

            ret = runInterpreter(&item, &c);
        }
        break;
    case 2: {
            TestItem item = {
                "",
                s.model_path.c_str(),
                nullptr,
                nullptr,
                s.data_type,
                s.data_type,
            };

            ret = runInterpreter(&item, &c);
        }
        break;
    default:
        break;
    }

    return ret;
}
