/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <chrono>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <math.h>
#include <string>
#include <sstream>
#include "cnpy.h"
#include "Common.h"
#include "InterpreterWrapper.h"
#include "tensorflow/contrib/lite/mtk/mtk_model.h"
#include "tensorflow/contrib/lite/mtk/kernels/mtk_register.h"
#include "tensorflow/contrib/lite/mtk/mtk_time_logger.h"
#include "tensorflow/contrib/lite/mtk/mtk_interpreter.h"
#include "tensorflow/contrib/lite/mtk/mtk_error_reporter.h"
#include "tensorflow/contrib/lite/optional_debug_tools.h"

using namespace tflite;
using namespace std;

typedef enum {
    kNone = -1,
    kNpy = 0,
    kNpz,
    kBin,
} DataOption;

DataOption getDataOptionByFilePath(const char* path) {
    DataOption ret = kNone;
    std::string file_path(path);

    string extension_name = file_path.substr(file_path.find_last_of("."));

    if (extension_name == ".npy") {
        ret = kNpy;
    } else if (extension_name == ".npz") {
        ret = kNpz;
    } else if (extension_name == ".bin") {
        ret = kBin;
    }

    return ret;
}

template <typename T>
class TFLiteRunner {
  public:
    TFLiteRunner(Interpreter* interpreter,
                 const bool use_nnapi,
                 const bool show_output)
        : m_interpreter(interpreter),
          m_use_nnapi(use_nnapi),
          m_show_output(show_output),
          m_save_output(false) {}
    TFLiteRunner(Interpreter* interpreter,
                 InterpreterWrapperConfig* c)
        : m_interpreter(interpreter),
          m_use_nnapi(c->use_nnapi),
          m_show_output(c->show_output),
          m_save_output(c->save_output) {}
    TfLiteStatus Run(const char* batch_xs,
                     const char* batch_ys,
                     const int output_tensor_idx,
                     const int loop);

  private:
    Interpreter* m_interpreter;
    const bool m_use_nnapi;
    const bool m_show_output;
    const bool m_save_output;
    std::vector<double> m_inference_time_list;

    TfLiteStatus ReshapeInputs(const char* batch_xs);
    TfLiteStatus ReshapeInput(const int tensor_id, cnpy::NpyArray array);
    TfLiteStatus ClearOutputs();
    TfLiteStatus PrepareInputs(const char* batch_xs);
    TfLiteStatus PrepareInput(const int tensor_id, cnpy::NpyArray array);
    TfLiteStatus PrepareInput(const int tensor_id, const char* batch_xs);
    TfLiteStatus PrepareInput(const int tensor_id, void* input_data);
    TfLiteStatus SaveOutputs(const char* batch_ys);
    TfLiteStatus CompareOutputs(const char* batch_ys);
    TfLiteStatus CompareOutput(const int tensor_id, cnpy::NpyArray array);
    TfLiteStatus CompareOutput(const int tensor_id, const char* batch_ys);
    TfLiteStatus CompareOutput(const int tensor_id, void* golden_data);
    TfLiteStatus RunWithRandomInput(const int output_tensor_idx,
                                    const int loop);
};

template <typename T>
TfLiteStatus TFLiteRunner<T>::Run(const char* batch_xs,
                                  const char* batch_ys,
                                  const int output_tensor_idx,
                                  const int loop) {
    if (batch_xs == nullptr && batch_ys == nullptr) {
        return RunWithRandomInput(output_tensor_idx, loop);
    }

    std::chrono::high_resolution_clock::time_point start_time_point;
    std::chrono::duration<double> elapsed;

    m_interpreter->UseNNAPI(m_use_nnapi);

    if (output_tensor_idx != -1) {
        m_interpreter->SetOutputs({output_tensor_idx});
    }

    // Reshape with batch
    TF_LITE_ENSURE_STATUS(ReshapeInputs(batch_xs));

    // Allocate Tensors
    TF_LITE_ENSURE_STATUS(m_interpreter->AllocateTensors());

    // Clear outputs[0]
    TF_LITE_ENSURE_STATUS(ClearOutputs());

    // Prepare inputs[0]
    TF_LITE_ENSURE_STATUS(PrepareInputs(batch_xs));

    // Invoke = Run
    if (!m_use_nnapi) {
        m_interpreter->SetNumThreads(4);
    }

    for (int i = 0; i < loop; i++) {
        start_time_point = std::chrono::high_resolution_clock::now();
        TF_LITE_ENSURE_STATUS(m_interpreter->Invoke());
        elapsed = std::chrono::high_resolution_clock::now() - start_time_point;
        m_inference_time_list.push_back(elapsed.count() * 1000);

        // Compare result with golden answer
        if (!m_save_output) {
            CompareOutputs(batch_ys);
        }

        if (i == 0) {
            cout << "Inference time(with model preparation and OP support query) : " <<
                 elapsed.count() * 1000 << " ms" << endl;
        } else {
            cout << "Inference time : " << elapsed.count() * 1000 << " ms" << endl;
        }
    }

    if (loop > 1) {
        double total_inference_time = 0;
        double avg_inference_time = 0;

        // Calculate the avgerage inference time except the first run.
        for (size_t i = 1; i < m_inference_time_list.size(); i++) {
            total_inference_time += m_inference_time_list.at(i);
        }

        avg_inference_time =
                        total_inference_time / (float)(m_inference_time_list.size() - 1);
        cout << "Avg Inference time: " \
             << avg_inference_time << " ms" << endl;
    }


    if (m_save_output) {
        // Save outputs
        TF_LITE_ENSURE_STATUS(SaveOutputs(batch_ys));
    }

    return kTfLiteOk;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::ReshapeInput(const int tensor_id,
        cnpy::NpyArray array) {
    // TODO(yumaokao): assert doesn't work
    assert(array.word_size == sizeof(T));

    if (array.word_size != sizeof(T)) {
        cout << "Input array work_size " << array.word_size << " != "
             << " sizeof(T) " << sizeof(T) << endl;
        return kTfLiteError;
    }

    T* src_data = array.data<T>();

    if (!src_data) {
        return kTfLiteError;
    }

    std::vector<int> shape;

    for (size_t s : array.shape) {
        shape.push_back(static_cast<int>(s));
    }

    m_interpreter->ResizeInputTensor(tensor_id, shape);
    return kTfLiteOk;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::ReshapeInputs(const char* batch_xs) {
    std::ifstream fp(batch_xs);

    if (!fp.good()) {
        cout << "Could not read " << batch_xs << endl;
        return kTfLiteError;
    }

    TfLiteStatus result = kTfLiteOk;
    DataOption data_option = getDataOptionByFilePath(batch_xs);

    if (data_option == kNpz) {
        // check inputs size
        const std::vector<int> inputs = m_interpreter->inputs();
        cnpy::npz_t arrs = cnpy::npz_load(batch_xs);

        if (arrs.size() != inputs.size()) {
            cout << "Input npz arrays size " << arrs.size() << " != "
                 << " network inputs size " << inputs.size() << endl;
            return kTfLiteError;
        }

        for (size_t i = 0; i < inputs.size(); i++) {
            int tensor_id = inputs[i];
            const char* tensor_name = m_interpreter->GetInputName(i);

            if (arrs.find(tensor_name) == arrs.end()) {
                cout << "Could not find input array name " << tensor_name
                     << " in npz arrays " << endl;
                return kTfLiteError;
            }

            result = ReshapeInput(tensor_id, arrs[tensor_name]);

            if (result != kTfLiteOk) {
                return result;
            }
        }

        return result;
    } else if (data_option == kNpy) {  // use npy, so default inputs.size() == 1
        cnpy::NpyArray arr = cnpy::npy_load(batch_xs);
        int tensor_id = m_interpreter->inputs()[0];
        return ReshapeInput(tensor_id, arr);
    }

    return result;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::ClearOutputs() {
    const std::vector<int> outputs = m_interpreter->outputs();

    for (size_t i = 0; i < outputs.size(); i++) {
        TfLiteTensor* tensor =
                        m_interpreter->tensor(m_interpreter->outputs()[i]);
        T* data = m_interpreter->typed_tensor<T>(m_interpreter->outputs()[i]);

        if (!data) {
            cout << "Fail to clear output tensor" << endl;
            return kTfLiteError;
        }

        if (data) {
            size_t num = tensor->bytes / sizeof(T);

            for (T* p = data; p < data + num; p++) {
                *p = 0;
            }
        }
    }

    return kTfLiteOk;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::PrepareInput(const int tensor_id,
        cnpy::NpyArray array) {
    return PrepareInput(tensor_id, (void*)array.data<T>());
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::PrepareInput(const int tensor_id,
        const char* batch_xs) {
    std::ifstream fp(batch_xs);

    if (!fp.good()) {
        cout << "Could not read " << batch_xs << endl;
        return kTfLiteError;
    }

    TfLiteTensor* tensor = m_interpreter->tensor(tensor_id);
    size_t num = tensor->bytes / sizeof(T);

    T loaded_data[num];
    fp.read((char*) loaded_data, tensor->bytes);

    return PrepareInput(tensor_id, (void*)loaded_data);
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::PrepareInput(const int tensor_id,
        void* input_data) {
    TfLiteTensor* tensor = m_interpreter->tensor(tensor_id);
    T* dst_data = m_interpreter->typed_tensor<T>(tensor_id);
    T* src_data = (T*)input_data;

    if (!dst_data || !src_data) {
        cout << "Fail to prepare input" << endl;
        return kTfLiteError;
    }

    size_t num = tensor->bytes / sizeof(T);
    cout << "Input tensor" << endl;
    cout << "    Dimension[ ";

    for (int i = 0; i < tensor->dims->size ; i++) {
        cout << tensor->dims->data[i] << " ";
    }

    cout << "]" << endl;
    cout << "    Size in bytes: " << tensor->bytes << endl;
    cout << "    Num of elemnts: " << num << endl;

    for (size_t i = 0; i < num; i++) {
        dst_data[i] = src_data[i];
    }

    return kTfLiteOk;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::PrepareInputs(const char* batch_xs) {

    std::ifstream fp(batch_xs);

    if (!fp.good()) {
        cout << "Could not read " << batch_xs << endl;
        return kTfLiteError;
    }

    TfLiteStatus result = kTfLiteError;
    DataOption data_option = getDataOptionByFilePath(batch_xs);

    if (data_option == kNpz) {
        // check inputs size
        const std::vector<int> inputs = m_interpreter->inputs();
        cnpy::npz_t arrs = cnpy::npz_load(batch_xs);

        if (arrs.size() != inputs.size()) {
            cout << "Input npz arrays size " << arrs.size() << "!= "
                 << " network inputs size " << inputs.size() << endl;
            return kTfLiteError;
        }

        for (size_t i = 0; i < inputs.size(); i++) {
            int tensor_id = inputs[i];
            const char* tensor_name = m_interpreter->GetInputName(i);

            if (arrs.find(tensor_name) == arrs.end()) {
                cout << "Could not find input array name " << tensor_name
                     << " in npz arrays " << endl;
                return kTfLiteError;
            }

            result = PrepareInput(tensor_id, arrs[tensor_name]);

            if (result != kTfLiteOk) {
                return result;
            }
        }

        return result;
    } else if (data_option == kNpy) {
        cnpy::NpyArray arr = cnpy::npy_load(batch_xs);
        int tensor_id = m_interpreter->inputs()[0];
        return PrepareInput(tensor_id, arr);
    } else if (data_option == kBin) {
        int tensor_id = m_interpreter->inputs()[0];
        return PrepareInput(tensor_id, batch_xs);
    }

    return result;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::CompareOutput(const int tensor_id,
        void* golden_data) {
    const double kRelativeThreshold = 1e-2f;
    const double kAbsoluteThreshold = 1e-4f;
    constexpr int kUint8AbsoluteThreshold = 2;
    TfLiteTensor* tensor = m_interpreter->tensor(tensor_id);
    T* src_data = m_interpreter->typed_tensor<T>(tensor_id);
    T* dst_data = (T*)golden_data;

    if (!dst_data || !src_data) {
        return kTfLiteError;
    }

    size_t num = tensor->bytes / sizeof(T);
    cout << "Output tensor" << endl;
    cout << "    Dimension[ ";

    for (int i = 0; i < tensor->dims->size ; i++) {
        cout << tensor->dims->data[i] << " ";
    }

    cout << "]" << endl;
    cout << "    Size in bytes: " << tensor->bytes << endl;
    cout << "    Num of elemnts: " << num << endl;

    if (m_show_output) {
        cout << "Result output data" << endl;

        if (std::is_same<T, float>::value) {
            for (int h = 0 ; h < 10 ; h++) {
                cout << "    " << src_data[h * 10 + 0] \
                     << " " << src_data[h * 10 + 1] \
                     << " " << src_data[h * 10 + 2] \
                     << " " << src_data[h * 10 + 3] \
                     << " " << src_data[h * 10 + 4] \
                     << " " << src_data[h * 10 + 5] \
                     << " " << src_data[h * 10 + 6] \
                     << " " << src_data[h * 10 + 7] \
                     << " " << src_data[h * 10 + 8] \
                     << " " << src_data[h * 10 + 9] << endl;
            }

            cout << "Golden output data" << endl;

            for (int h = 0 ; h < 10 ; h++) {
                cout << "    " << dst_data[h * 10 + 0] \
                     << " " << dst_data[h * 10 + 1] \
                     << " " << dst_data[h * 10 + 2] \
                     << " " << dst_data[h * 10 + 3] \
                     << " " << dst_data[h * 10 + 4] \
                     << " " << dst_data[h * 10 + 5] \
                     << " " << dst_data[h * 10 + 6] \
                     << " " << dst_data[h * 10 + 7] \
                     << " " << dst_data[h * 10 + 8] \
                     << " " << dst_data[h * 10 + 9] << endl;
            }
        } else if (std::is_same<T, uint8_t>::value) {
            for (int h = 0 ; h < 10 ; h++) {
                cout << "    " << (int)src_data[h * 10 + 0] \
                     << " " << (int)src_data[h * 10 + 1] \
                     << " " << (int)src_data[h * 10 + 2] \
                     << " " << (int)src_data[h * 10 + 3] \
                     << " " << (int)src_data[h * 10 + 4] \
                     << " " << (int)src_data[h * 10 + 5] \
                     << " " << (int)src_data[h * 10 + 6] \
                     << " " << (int)src_data[h * 10 + 7] \
                     << " " << (int)src_data[h * 10 + 8] \
                     << " " << (int)src_data[h * 10 + 9] << endl;
            }

            cout << "Golden output data" << endl;

            for (int h = 0 ; h < 10 ; h++) {
                cout << "    " << (int)dst_data[h * 10 + 0] \
                     << " " << (int)dst_data[h * 10 + 1] \
                     << " " << (int)dst_data[h * 10 + 2] \
                     << " " << (int)dst_data[h * 10 + 3] \
                     << " " << (int)dst_data[h * 10 + 4] \
                     << " " << (int)dst_data[h * 10 + 5] \
                     << " " << (int)dst_data[h * 10 + 6] \
                     << " " << (int)dst_data[h * 10 + 7] \
                     << " " << (int)dst_data[h * 10 + 8] \
                     << " " << (int)dst_data[h * 10 + 9] << endl;
            }
        }
    }

    if (std::is_same<T, float>::value) {
        for (size_t i = 0; i < num; i++) {
            float diff = std::abs(src_data[i] - dst_data[i]);
            bool error_is_large = false;

            // For very small numbers, try absolute error, otherwise go with
            // relative.
            if (std::abs((float)dst_data[i]) < kRelativeThreshold) {
                error_is_large = (diff > kAbsoluteThreshold);
            } else {
                error_is_large =
                                (diff > kRelativeThreshold * std::abs((float)dst_data[i]));
            }

            if (error_is_large) {
                cout << "Result " << i << "= " << src_data[i] << endl;
                cout << "Golden " << i << "= " << dst_data[i] << endl;
                cout << "Result is not matched to golden value" << endl;
                return kTfLiteError;
            }
        }
    } else if (std::is_same<T, uint8_t>::value) {
        for (size_t i = 0; i < num; i++) {
            int diff = std::abs((int)src_data[i] - (int)dst_data[i]);

            if (diff >= kUint8AbsoluteThreshold) {
                cout << "Result " << i << "= " << (int)src_data[i] << endl;
                cout << "Golden " << i << "= " << (int)dst_data[i] << endl;
                cout << "Result is not matched to golden value" \
                     << endl;
                return kTfLiteError;
            }
        }
    }

    cout << "Result is matched!!" << endl;
    return kTfLiteOk;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::CompareOutput(const int tensor_id,
        cnpy::NpyArray array) {
    return CompareOutput(tensor_id, (void*)array.data<T>());
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::CompareOutput(const int tensor_id,
        const char* batch_ys) {
    std::ifstream fp(batch_ys);

    if (!fp.good()) {
        cout << "Could not read " << batch_ys << endl;
        return kTfLiteError;
    }

    TfLiteTensor* tensor = m_interpreter->tensor(tensor_id);
    size_t num = tensor->bytes / sizeof(T);

    fp.seekg(0, ios_base::end);
    size_t ys_len = fp.tellg();

    if (ys_len != tensor->bytes) {
        cout << "The size in bytes of " << batch_ys << "is not matched to output tensor"
             << endl;
        return kTfLiteError;
    }

    fp.seekg(0, ios_base::beg);

    T loaded_data[num];
    fp.read((char*) loaded_data, sizeof(loaded_data));

    return CompareOutput(tensor_id, (void*)loaded_data);
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::CompareOutputs(const char* batch_ys) {
    TfLiteStatus result = kTfLiteError;
    DataOption data_option = getDataOptionByFilePath(batch_ys);

    if (data_option == kNpz) {
        // check output size
        const std::vector<int> outputs = m_interpreter->outputs();
        cnpy::npz_t arrs = cnpy::npz_load(batch_ys);

        if (arrs.size() != outputs.size()) {
            cout << "Output npz arrays size " << arrs.size() << "!= "
                 << " network outputs size " << outputs.size() << endl;
            return kTfLiteError;
        }

        for (size_t i = 0; i < outputs.size(); i++) {
            int tensor_id = outputs[i];
            const char* tensor_name = m_interpreter->GetOutputName(i);

            if (arrs.find(tensor_name) == arrs.end()) {
                cout << "Could not find output array name " << tensor_name
                     << " in npz arrays " << endl;
                return kTfLiteError;
            }

            cout << "Compare tensor: " << tensor_name << endl;
            result = CompareOutput(tensor_id, arrs[tensor_name]);

            if (result != kTfLiteOk) {
                return result;
            }
        }

        return result;
    } else if (data_option == kNpy) {
        cnpy::NpyArray arr = cnpy::npy_load(batch_ys);
        int tensor_id = m_interpreter->outputs()[0];
        return CompareOutput(tensor_id, arr);
    } else if (data_option == kBin) {
        int tensor_id = m_interpreter->outputs()[0];
        return CompareOutput(tensor_id, batch_ys);
    }

    return result;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::SaveOutputs(const char* batch_ys) {
    TfLiteStatus result = kTfLiteError;
    DataOption data_option = getDataOptionByFilePath(batch_ys);

    if (data_option == kNpz) {
        const std::vector<int> outputs = m_interpreter->outputs();
        bool append = false;

        for (size_t o = 0; o < outputs.size(); o++) {
            int tensor_id = outputs[o];
            const char* tensor_name = m_interpreter->GetOutputName(o);
            cout << "Save output tensor" << tensor_id << ": " \
                 << tensor_name << endl;

            TfLiteTensor* tensor = m_interpreter->tensor(tensor_id);
            T* out_data = m_interpreter->typed_tensor<T>(tensor_id);

            if (!out_data) {
                return kTfLiteError;
            }

            // get shape
            std::vector<size_t> npyshape;

            for (int i = 0; i < tensor->dims->size; i++) {
                npyshape.push_back(tensor->dims->data[i]);
            }

            // get data
            std::vector<T> npydata;
            size_t num = tensor->bytes / sizeof(T);

            for (size_t idx = 0; idx < num; idx++) {
                npydata.push_back(out_data[idx]);
            }

            // save npz
            cnpy::npz_save(batch_ys, tensor_name, &npydata[0], npyshape,
                           (append) ? "a" : "w");
            append = true;
            result = kTfLiteOk;
        }

        return result;
    } else if (data_option == kNpy) {
        TfLiteTensor* tensor =
                        m_interpreter->tensor(m_interpreter->outputs()[0]);
        T* out_data =
                        m_interpreter->typed_tensor<T>(m_interpreter->outputs()[0]);

        if (!out_data) {
            return kTfLiteError;
        }

        result = kTfLiteOk;
        // get output shape
        std::vector<size_t> npyshape;

        for (int i = 0; i < tensor->dims->size; i++) {
            npyshape.push_back(tensor->dims->data[i]);
        }

        std::vector<T> npydata;
        size_t num = tensor->bytes / sizeof(T);

        for (size_t idx = 0; idx < num; idx++) {
            npydata.push_back(out_data[idx]);
        }

        cnpy::npy_save(batch_ys, &npydata[0], npyshape, "w");
        return result;
    }

    return result;
}

template <typename T>
TfLiteStatus TFLiteRunner<T>::RunWithRandomInput(const int output_tensor_idx,
        const int loop) {
    std::chrono::high_resolution_clock::time_point start_time_point;
    std::chrono::duration<double> elapsed;

    m_interpreter->UseNNAPI(m_use_nnapi);

    if (output_tensor_idx != -1) {
        m_interpreter->SetOutputs({output_tensor_idx});
    }

    // Allocate Tensors
    TF_LITE_ENSURE_STATUS(m_interpreter->AllocateTensors());

    // Clear outputs[0]
    TF_LITE_ENSURE_STATUS(ClearOutputs());

    // Invoke = Run
    if (!m_use_nnapi) {
        m_interpreter->SetNumThreads(4);
    }

    for (int i = 0; i < loop; i++) {
        start_time_point = std::chrono::high_resolution_clock::now();
        TF_LITE_ENSURE_STATUS(m_interpreter->Invoke());
        elapsed = std::chrono::high_resolution_clock::now() - start_time_point;
        m_inference_time_list.push_back(elapsed.count() * 1000);
        cout << "Inference time : " << elapsed.count() * 1000 << " ms" << endl;
    }

    if (loop > 1) {
        double total_inference_time = 0;
        double avg_inference_time = 0;

        // Calculate the avgerage inference time except the first run.
        for (size_t i = 1; i < m_inference_time_list.size(); i++) {
            total_inference_time += m_inference_time_list.at(i);
        }

        avg_inference_time =
                        total_inference_time / (float)(m_inference_time_list.size() - 1);
        cout << "Avg Inference time: " \
             << avg_inference_time << " ms" << endl;
    }

    return kTfLiteOk;

}

int runInterpreter(const TestItem* item) {
    InterpreterWrapperConfig c = InterpreterWrapperConfig();
    c.loop_count = 1;
    c.show_output = false;
    c.save_output = false;
    c.use_nnapi = true;

    return runInterpreter(item, &c);
}

int runInterpreter(const TestItem* item, InterpreterWrapperConfig* config) {
    std::vector<double> runtime_inference_log;

    cout << "Model: " << item->tflite << "\n";

    if (item->input != nullptr) {
        cout << "Golden: " << item->input << "\n";
    }

    if (item->golden != nullptr) {
        cout << "Golden: " << item->golden << "\n";
    }

    auto model = FlatBufferModel::BuildFromFile(item->tflite,
                 MakeMtkErrorReporter());

    ops::builtin::MtkExtOpResolver resolver;
    std::unique_ptr<Interpreter> interpreter;
    MtkInterpreterBuilder(*model, resolver)(&interpreter);
    MtkInterpreter* mtkInterpreter = (MtkInterpreter*)interpreter.get();

    interpreter->UseNNAPI(config->use_nnapi);
    mtkInterpreter->RelaxComputationFloat32toFloat16(
                    config->relax_computation_float32_to_float16);

    if (kTfLiteOk != interpreter->AllocateTensors()) {
        cout << "Fail to allocate tensors" << endl;
        return 0;
    }

    int input = interpreter->inputs()[0];
    TfLiteType input_data_type = interpreter->tensor(input)->type;
    cout << "Input data type: " << input_data_type << endl;

    int output = interpreter->outputs()[0];
    TfLiteType output_data_type = interpreter->tensor(output)->type;
    cout << "Output data type: " << output_data_type << endl;

    TfLiteStatus result = kTfLiteError;

    if (input_data_type == kTfLiteFloat32) {
        TFLiteRunner<float> runner(mtkInterpreter, config);
        result = runner.Run(item->input,
                            item->golden,
                            -1,
                            config->loop_count);
    } else if (input_data_type == kTfLiteUInt8) {
        TFLiteRunner<uint8_t> runner(mtkInterpreter, config);
        result = runner.Run(item->input,
                            item->golden,
                            -1,
                            config->loop_count);
    } else {
        cout << "The input data type is not float or uint8" << endl;
    }

    return (result == kTfLiteOk ? 0 : 1);
}

