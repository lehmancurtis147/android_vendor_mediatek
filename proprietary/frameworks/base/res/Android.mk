LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

include $(LOCAL_PATH)/base_rules.mk
# include apicheck.mk later, we need the build pass to prepare the first version
# include $(LOCAL_PATH)/apicheck.mk

LOCAL_PACKAGE_NAME := mediatek-res
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform

LOCAL_AAPT_FLAGS := -x8
LOCAL_AAPT_FLAGS += --private-symbols com.mediatek.internal

# Tell aapt to build resource in utf16(the ROM will be enlarged),
# in order to save RAM size for string cache table
ifeq (yes,$(strip $(MTK_GMO_RAM_OPTIMIZE)))
LOCAL_AAPT_FLAGS += --utf16
endif


LOCAL_MODULE_TAGS := optional

LOCAL_PRIVATE_PLATFORM_APIS := true

# Install this alongside the libraries.
LOCAL_MODULE_PATH := $(TARGET_OUT_JAVA_LIBRARIES)

# Create package-export.apk, which other packages can use to get
# PRODUCT-agnostic resource data like IDs and type definitions.
LOCAL_EXPORT_PACKAGE_RESOURCES := true

include $(BUILD_PACKAGE)

# workaround the build error
# ninja: error: 'out/target/common/obj/APPS/mediatek-res_intermediates/classes-header.jar', needed by 'out/target/common/obj/APPS/MtkDownloadProvider_intermediates/classes-turbine.jar', missing and no known rule to make it
# caused by https://android-review.googlesource.com/#/c/platform/build/+/404743/
$(call app-lib-header-files,mediatek-res): $(call intermediates-dir-for,APPS,mediatek-res,,COMMON)/src/R.stamp

# define a global intermediate target that other module may depend on.
.PHONY: mediatek-res-package-target
mediatek-res-package-target: $(LOCAL_BUILT_MODULE)
resource_export_package :=
