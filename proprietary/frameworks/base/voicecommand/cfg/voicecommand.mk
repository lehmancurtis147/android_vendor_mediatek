#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_MTK_PATH:= vendor/mediatek/proprietary/frameworks/base/voicecommand/cfg/

ifeq ($(strip $(MTK_VOW_SUPPORT)), yes)
    PRODUCT_COPY_FILES += \
       $(LOCAL_MTK_PATH)/64.dat:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/64.dat:mtk \
       $(LOCAL_MTK_PATH)/128.dat:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/128.dat:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/c_e_dic.bin.f:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/c_e_dic.bin.f:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/c_e_dic.bin.l:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/c_e_dic.bin.l:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/c_e_dic.bin.p:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/c_e_dic.bin.p:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/c_e_dic.bin.t:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/c_e_dic.bin.t:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/c_e_dic.bin.gen:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/c_e_dic.bin.gen:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/commandfilr3.dic:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/commandfilr3.dic:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/Model1.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/Model1.bin:mtk \
       $(LOCAL_MTK_PATH)/antiCmds/Model4.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/antiCmds/Model4.bin:mtk \
       $(LOCAL_MTK_PATH)/1.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/1.pcm:mtk \
       $(LOCAL_MTK_PATH)/2.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/2.pcm:mtk \
       $(LOCAL_MTK_PATH)/3.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/3.pcm:mtk \
       $(LOCAL_MTK_PATH)/4.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/4.pcm:mtk \
       $(LOCAL_MTK_PATH)/5.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/5.pcm:mtk \
       $(LOCAL_MTK_PATH)/6.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/6.pcm:mtk \
       $(LOCAL_MTK_PATH)/7.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/7.pcm:mtk \
       $(LOCAL_MTK_PATH)/nl:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/nl:mtk \
       $(LOCAL_MTK_PATH)/p0:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/p0:mtk \
       $(LOCAL_MTK_PATH)/ubmModel0.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel0.bin:mtk \
       $(LOCAL_MTK_PATH)/ubmModel1.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel1.bin:mtk \
       $(LOCAL_MTK_PATH)/ubmModel2.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel2.bin:mtk \
       $(LOCAL_MTK_PATH)/ubmModel3.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel3.bin:mtk \
       $(LOCAL_MTK_PATH)/ubmModel4.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel4.bin:mtk \
       $(LOCAL_MTK_PATH)/ubmModel10.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2/ubmModel10.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/64.dat:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/64.dat:mtk \
       $(LOCAL_MTK_PATH)/vowp23/128.dat:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/128.dat:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/c_e_dic.bin.f:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/c_e_dic.bin.f:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/c_e_dic.bin.l:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/c_e_dic.bin.l:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/c_e_dic.bin.p:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/c_e_dic.bin.p:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/c_e_dic.bin.t:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/c_e_dic.bin.t:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/c_e_dic.bin.gen:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/c_e_dic.bin.gen:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/commandfilr3.dic:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/commandfilr3.dic:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/Model1.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/Model1.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/antiCmds/Model4.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/antiCmds/Model4.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/1.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/1.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/2.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/2.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/3.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/3.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/4.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/4.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/5.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/5.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/6.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/6.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/7.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/7.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/8.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/8.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/9.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/9.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/10.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/10.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/11.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/11.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/12.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/12.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/13.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/13.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/14.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/14.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/15.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/15.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/16.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/16.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/17.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/17.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/18.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/18.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/19.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/19.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/20.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/20.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/21.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/21.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/22.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/22.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/23.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/23.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/24.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/24.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/25.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/25.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/26.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/26.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/27.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/27.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/28.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/28.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/29.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/29.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/30.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/30.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/31.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/31.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/32.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/32.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/33.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/33.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/34.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/34.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp23/nl:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/nl:mtk \
       $(LOCAL_MTK_PATH)/vowp23/p0:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/p0:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel0.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel0.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel1.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel1.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel2.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel2.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel3.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel3.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel4.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel4.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp23/ubmModel10.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.3/ubmModel10.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp25/1.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/1.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/2.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/2.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/3.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/3.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/4.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/4.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/5.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/5.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/6.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/6.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/7.pcm:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/7.pcm:mtk \
       $(LOCAL_MTK_PATH)/vowp25/n0:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/n0:mtk \
       $(LOCAL_MTK_PATH)/vowp25/n1:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/n1:mtk \
       $(LOCAL_MTK_PATH)/vowp25/nl:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/nl:mtk \
       $(LOCAL_MTK_PATH)/vowp25/p0:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/p0:mtk \
       $(LOCAL_MTK_PATH)/vowp25/p1:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/p1:mtk \
       $(LOCAL_MTK_PATH)/vowp25/ubmModel0.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/ubmModel0.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp25/ubmModel3.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/ubmModel3.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp25/ubmModel4.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/ubmModel4.bin:mtk \
       $(LOCAL_MTK_PATH)/vowp25/ubmModel10.bin:$(TARGET_COPY_OUT_VENDOR)/etc/voicecommand/training/ubmfile/p2.5/ubmModel10.bin:mtk
endif
