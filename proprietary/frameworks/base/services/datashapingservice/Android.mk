LOCAL_PATH:= $(call my-dir)

# merge all mtk added services into one jar
# ============================================================
include $(CLEAR_VARS)

LOCAL_SRC_FILES := $(call all-java-files-under,java)

LOCAL_MODULE := services.datashapingservice

LOCAL_JAVA_LIBRARIES += services.core \
                        mediatek-telephony-base \
                        mediatek-framework

include $(BUILD_STATIC_JAVA_LIBRARY)

# ============================================================
include $(CLEAR_VARS)
include $(call all-makefiles-under,$(LOCAL_PATH))
