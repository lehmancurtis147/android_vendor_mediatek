package com.mediatek.fmradioservice;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Slog;
import com.android.internal.os.SomeArgs;
import android.os.RemoteCallbackList;
import java.lang.reflect.Field;
import java.util.ArrayList;
import android.Manifest;
import android.os.RemoteException;
import com.android.server.FgThread;
import com.huawei.android.hardware.fmradio.FmConfig;
import com.huawei.android.hardware.fmradio.IFmReceiver;
import com.huawei.android.hardware.fmradio.IFmRxEvCallbacksAdaptor;
import com.huawei.android.hardware.fmradio.IFmHidlClientCallbacks;
import com.huawei.android.hardware.fmradio.FmHidlClient;
import com.huawei.android.hardware.fmradio.FmState;
import com.huawei.android.hardware.fmradio.FmReceiver;
import static com.huawei.android.hardware.fmradio.FmReceiver.Rx_Turned_On;
import static com.huawei.android.hardware.fmradio.FmReceiver.Srch_InProg;
import static com.huawei.android.hardware.fmradio.FmReceiver.Turned_Off;
import static com.huawei.android.hardware.fmradio.FmReceiver.FMRx_Starting;
import static com.huawei.android.hardware.fmradio.FmReceiver.FMTx_Starting;
import static com.huawei.android.hardware.fmradio.FmReceiver.FMTurning_Off;

import static com.huawei.android.hardware.fmradio.FmReceiver.NoSearch;
import static com.huawei.android.hardware.fmradio.FmReceiver.SeekInPrg;
import static com.huawei.android.hardware.fmradio.FmReceiver.ScanInProg;
import static com.huawei.android.hardware.fmradio.FmReceiver.SrchListInProg;
import static com.huawei.android.hardware.fmradio.FmReceiver.SrchComplete;
import static com.huawei.android.hardware.fmradio.FmReceiver.SrchAbort;

public class FmRadioServiceImpl extends IFmReceiver.Stub implements IFmHidlClientCallbacks{
    private static final String TAG = "FmRadioServiceImpl";
    private final Callbacks mCallbacks;
    private FmHidlClient mHidlClient;
    private final Object mLock = new Object();
    private FmState mFmState;
    private int mGrpMask;


    public FmRadioServiceImpl(Context context) {
        mCallbacks = new Callbacks(FgThread.get().getLooper());
        mFmState  = new FmState();
        mHidlClient =  new FmHidlClient(this,mFmState);

    }

   public boolean enable(FmConfig configSettings) {
        boolean status = false;
        int state;
        Slog.d(TAG, "enable");
        /*
         * Check for FM State.
         * If FMRx already on, then return.
         */
        synchronized (mLock) {
            state = mFmState.getSyncFmPowerState();
            Slog.d(TAG, "getSyncFmPowerState:" + state);
            mFmState.setFmPowerState(FMRx_Starting);
            Slog.v(TAG, "enable: CURRENT-STATE : FMOff ---> NEW-STATE : FMRxStarting");
            status = mHidlClient.enable(configSettings);
            if (status == true) {
                mFmState.setFmPowerState(Rx_Turned_On);
            } else {
                Slog.e(TAG, "FM enable fail");
                mFmState.setFmPowerState(Turned_Off);
            }
            return status;
        }
    }

    public boolean disable() {
        Slog.d(TAG, "disable");
        boolean status = false;
        int state = mFmState.getSyncFmPowerState();
        synchronized (mLock) {
            Slog.d(TAG, "getSyncFmPowerState:" + state);
            switch (state) {
                case Turned_Off:
                    Slog.d(TAG, "FM is alreayd turned off");
                    break;
                case Srch_InProg:
                    Slog.d(TAG, "Cancel the search operation to disable the FM");
                    mFmState.setSearchState(SrchAbort);
                    cancelSearch();
                    break;
                case FMRx_Starting:
                    state = mFmState.getSyncFmPowerState();
                    if (state == FMRx_Starting) {
                        Slog.e(TAG, "FM is in bad state");
                        return status;
                    }
                    break;

                case FMTurning_Off:
                    Slog.e(TAG, "FM is in process to tuned off");
                    return status;


            }
            mFmState.setFmPowerState(Turned_Off);
            status = mHidlClient.disable();
            Slog.d(TAG, "getSyncFmPowerState after disable:" + state);
            return status;
        }
    }

    public boolean cancelSearch() {
        Slog.d(TAG, "cancelSearch");
        boolean status = false;
        int state;
        synchronized (mLock) {
            state = mFmState.getSyncFmPowerState();
            if (state == Srch_InProg) {
                mFmState.setSearchState(SrchAbort);
                status = mHidlClient.cancelSearch();
                Slog.d(TAG, "status :" + status);

            } else {
                Slog.d(TAG, "No search opeartion");

            }
            return status;
        }

    }

    public boolean searchStations(int mode,
                                  int dwellPeriod,
                                  int direction,
                                  int pty,
                                  int pi) {

        Slog.d(TAG, "searchStations");
        boolean status = true;
        int state;
        synchronized (mLock) {
            state = mFmState.getSyncFmPowerState();
            Slog.d(TAG, "searchStations :state:" + state);
            if (state == Srch_InProg || state == Turned_Off) {
                Slog.e(TAG, "FM is busy in another command");
                return false;
            }
            if (mode != FmReceiver.FM_RX_SRCH_MODE_SCAN &&
                    mode != FmReceiver.FM_RX_SRCH_MODE_SEEK) {
                Slog.d(TAG, "invalid serach mode" + mode);
                status = false;
            }

            if (dwellPeriod < FmReceiver.FM_RX_DWELL_PERIOD_1S ||
                    dwellPeriod > FmReceiver.FM_RX_DWELL_PERIOD_7S) {
                Slog.d(TAG, "invalid dwelling time" + dwellPeriod);
                status = false;
            }
            if (direction != FmReceiver.FM_RX_SEARCHDIR_UP &&
                    direction != FmReceiver.FM_RX_SEARCHDIR_DOWN) {

                Slog.d(TAG, "invalid direction" + direction);
                status = false;
            }
            if (status) {
                if (mode == FmReceiver.FM_RX_SRCH_MODE_SCAN)
                    mFmState.setSearchState(ScanInProg);
                else if (mode == FmReceiver.FM_RX_SRCH_MODE_SEEK)
                    mFmState.setSearchState(SeekInPrg);
                status = mHidlClient.searchStations(mode, dwellPeriod, direction, pty, pi);
                if (!status) {
                    if (mFmState.getSyncFmPowerState() == Srch_InProg) {
                        mFmState.setSearchState(SrchComplete);
                    }
                    status = false;
                }

            }
            return status;
        }
    }

    public boolean setStation(int frequencyKHz) {
        Slog.d(TAG, "setStation :" + frequencyKHz);
        int ret = 0;
        synchronized (mLock) {
            ret = mHidlClient.setFreq(frequencyKHz);
            if (ret != 0) {
                return false;
            } else {
                return true;
            }
        }
    }

    public boolean setRdsOnOff(int onOff) {
        Slog.d(TAG, "setRdsOnOff onOff :" + onOff);
        boolean status = false;
        synchronized (mLock) {
            status = mHidlClient.setRdsOnOff(onOff);
            return status;
        }
   }

    public boolean setLowPwrMode(boolean lpmode) {
        Slog.d(TAG, "setLowPwrMode lpmode :" + lpmode);
        boolean status = false;
        synchronized (mLock) {
          status = mHidlClient.setLowPwrMode(lpmode);
          return status;
        }
    }

    public String getPrgmServices() {
        Slog.d(TAG, "getPrgmServices");
        String psString = "";
        synchronized (mLock) {
            psString = mHidlClient.getPrgmServices();
            return  psString;
        }
    }

    public String getRadioText()  {
        Slog.d(TAG, "getRadioText");
        String rtString = "";
        synchronized (mLock) {
            rtString = mHidlClient.getRadioText();
            return rtString;
        }
    }

    public int getPrgmType() {
        Slog.d(TAG, "getPrgmType");
        int  pTy = 0;
        synchronized (mLock) {
            pTy = mHidlClient.getPrgmType();
            return pTy;
        }
    }


    public int getPrgmId() {
        Slog.d(TAG, "getPrgmId");
        int  pI = 0;
        synchronized (mLock) {
            pI = mHidlClient.getPrgmId();
            return pI;
        }
    }

    public int getRdsStatus() {
        Slog.d(TAG, "getRdsStatus");
        int  rdsEvent = 0;
        synchronized (mLock) {
            rdsEvent = mHidlClient.getRdsStatus();
            return rdsEvent;
        }
    }

    public int getRssi() {
        Slog.d(TAG, "getRssi");
        int  rssi = 0;
        synchronized (mLock) {
            rssi = mHidlClient.getRssi();
            return rssi;
        }
    }

    public int[] getAfInfo() {
        Slog.d(TAG, "getAfInfo");
        synchronized (mLock) {
            return mHidlClient.getAfInfo();
        }
    }


    public boolean registerRdsGroupProcessing(int fmGrpsToProc) {
        Slog.d(TAG, "registerRdsGroupProcessing fmGrpsToProc :" + fmGrpsToProc);
        boolean status = false;
        int state;
        synchronized (mLock) {
            state = mFmState.getSyncFmPowerState();
            /* Check current state of FM device */
            if (state == Turned_Off || state == Srch_InProg) {
                Slog.d(TAG, "registerRdsGroupProcessing: Device currently busy");
                return false;
            }
            // Enable RDS
            status = mHidlClient.setRdsOnOff(1);
            if(status == true)
                mGrpMask = fmGrpsToProc ;
            return status;
        }
    }


    private static class Callbacks extends Handler {
        private static final int MSG_TUNE_STATE_CHANGED = 1;
        private static final int MSG_RDS_STATE_CHANGED = 2;
        private static final int MSG_SEARCH_IN_PROGRESS = 3;
        private static final int MSG_SEARCH_CANCELLED = 4;
        private static final int MSG_SEARCH_COMPLETED = 5;
        private static final int MSG_RDS_AVAL_CHANGED = 6;
        private static final int MSG_RDS_PS_CHANGED = 7;
        private static final int MSG_RDS_RT_CHANGED = 8;
        private static final int MSG_RDS_AF_LIST_CHANGED = 9;

        private final RemoteCallbackList<IFmRxEvCallbacksAdaptor>
                mCallbacks = new RemoteCallbackList<>();

        public Callbacks(Looper looper) {
            super(looper);
        }

        public void register(IFmRxEvCallbacksAdaptor callback) {
            mCallbacks.register(callback);
        }

        public void unregister(IFmRxEvCallbacksAdaptor callback) {
            mCallbacks.unregister(callback);
        }

        @Override
        public void handleMessage(Message msg) {
            final SomeArgs args = (SomeArgs) msg.obj;
            final int n = mCallbacks.beginBroadcast();
            for (int i = 0; i < n; i++) {
                final IFmRxEvCallbacksAdaptor callback = mCallbacks.getBroadcastItem(i);
                try {
                    invokeCallback(callback, msg.what, args);
                } catch (RemoteException ignored) {
                }
            }
            mCallbacks.finishBroadcast();
            args.recycle();
        }

        private void invokeCallback(IFmRxEvCallbacksAdaptor callback, int what, SomeArgs args)
                throws RemoteException {
            switch (what) {
                case MSG_TUNE_STATE_CHANGED:
                    callback.fmRxEvRadioTuneStatus((int) args.arg1);
                    break;
                case MSG_RDS_STATE_CHANGED:
                    callback.fmRxEvRdsLockStatus((boolean) args.arg1);
                    break;
                case MSG_SEARCH_IN_PROGRESS:
                    callback.fmRxEvSearchInProgress();
                    break;
                case MSG_SEARCH_CANCELLED:
                    callback.fmRxEvSearchCancelled();
                    break;
                case MSG_SEARCH_COMPLETED:
                    callback.fmRxEvSearchComplete((int) args.arg1);
                    break;
                case MSG_RDS_AVAL_CHANGED:
                    callback.fmRxEvRdsInfo((int) args.arg1);
                    break;
                case MSG_RDS_PS_CHANGED:
                    callback.fmRxEvRdsPsInfo();
                    break;
                case MSG_RDS_RT_CHANGED:
                    callback.fmRxEvRdsRtInfo();
                    break;
                case MSG_RDS_AF_LIST_CHANGED:
                    callback.fmRxEvRdsAfInfo();
                    break;
                default:
                    break;
                }
            }

        public void fmRxEvRadioTuneStatus(int freq) {
            final SomeArgs args = SomeArgs.obtain();
            args.arg1 = freq;
            obtainMessage(MSG_TUNE_STATE_CHANGED, args).sendToTarget();
        }


        public void fmRxEvRdsLockStatus(boolean avail) {
            final SomeArgs args = SomeArgs.obtain();
            args.arg1 = avail;
            obtainMessage(MSG_RDS_STATE_CHANGED, args).sendToTarget();
        }

        public void fmRxEvSearchInProgress() {
            final SomeArgs args = SomeArgs.obtain();
            obtainMessage(MSG_SEARCH_IN_PROGRESS, args).sendToTarget();
        }


        public void fmRxEvSearchCancelled() {
            final SomeArgs args = SomeArgs.obtain();
            obtainMessage(MSG_SEARCH_CANCELLED, args).sendToTarget();
        }

        public void fmRxEvSearchComplete(int freq) {
            final SomeArgs args = SomeArgs.obtain();
            args.arg1 = freq;
            obtainMessage(MSG_SEARCH_COMPLETED, args).sendToTarget();
        }

        public void fmRxEvRdsInfo(int event) {
            final SomeArgs args = SomeArgs.obtain();
            args.arg1 = event;
            obtainMessage(MSG_RDS_AVAL_CHANGED, args).sendToTarget();
        }

        public void fmRxEvRdsPsInfo() {
            final SomeArgs args = SomeArgs.obtain();
            obtainMessage(MSG_RDS_PS_CHANGED, args).sendToTarget();
        }
        public void fmRxEvRdsRtInfo() {
            final SomeArgs args = SomeArgs.obtain();
            obtainMessage(MSG_RDS_RT_CHANGED, args).sendToTarget();
        }
        public void fmRxEvRdsAfInfo() {
            final SomeArgs args = SomeArgs.obtain();
            obtainMessage(MSG_RDS_AF_LIST_CHANGED, args).sendToTarget();
        }

    }

    @Override
    public void registerListener(IFmRxEvCallbacksAdaptor listener) {
        mCallbacks.register(listener);
    }

    @Override
    public void unregisterListener(IFmRxEvCallbacksAdaptor listener) {
        mCallbacks.unregister(listener);
    }

    @Override
    public void onRadioTunedStatus(int freq) {
        mCallbacks.fmRxEvRadioTuneStatus(freq);
    }

    @Override
    public void onRdsLockedStatus(boolean rdsAvail) {
        mCallbacks.fmRxEvRdsLockStatus(rdsAvail);
    }

    @Override
    public void onSearchInProgress() {
       String str = "SearchInProgress";
       mCallbacks.fmRxEvSearchInProgress();
    }

    @Override
    public void onSearchCancelled() {
       String str = "SearchCancelled";
       mCallbacks.fmRxEvSearchCancelled();
    }

    @Override
    public void onSearchComplete(int freq) {
        mCallbacks.fmRxEvSearchComplete(freq);
    }

   @Override
    public void onRdsInfo(int iRdsEvents){
       Slog.d(TAG, "onRdsInfo iRdsEvents:" + iRdsEvents);
       if (FmReceiver.RDS_EVENT_PROGRAMNAME == (FmReceiver.RDS_EVENT_PROGRAMNAME & iRdsEvents)) {
           if(((mGrpMask & FmReceiver.FM_RX_RDS_GRP_PS_EBL) == FmReceiver.FM_RX_RDS_GRP_PS_EBL)
              ||
              ((mGrpMask & FmReceiver.FM_RX_RDS_GRP_PS_SIMPLE_EBL) ==
                   FmReceiver.FM_RX_RDS_GRP_PS_SIMPLE_EBL)) {
               mCallbacks.fmRxEvRdsPsInfo();
           }
       }
       if (FmReceiver.RDS_EVENT_LAST_RADIOTEXT ==
           (FmReceiver.RDS_EVENT_LAST_RADIOTEXT & iRdsEvents)) {
           if((mGrpMask & FmReceiver.FM_RX_RDS_GRP_RT_EBL) == FmReceiver.FM_RX_RDS_GRP_RT_EBL) {
               mCallbacks.fmRxEvRdsRtInfo();
           }
       }
       if (FmReceiver.RDS_EVENT_AF == (FmReceiver.RDS_EVENT_AF & iRdsEvents)) {
           if((mGrpMask & FmReceiver.FM_RX_RDS_GRP_AF_EBL) == FmReceiver.FM_RX_RDS_GRP_AF_EBL) {
               mCallbacks.fmRxEvRdsAfInfo();
           }
       }
       /* TO DO As of now used fmRxEvRdsInfo callback api to get PI and PTY*/
       if ((FmReceiver.RDS_EVENT_PI_CODE == (FmReceiver.RDS_EVENT_PI_CODE & iRdsEvents))
           ||
           (FmReceiver.RDS_EVENT_PTY_CODE == (FmReceiver.RDS_EVENT_PTY_CODE & iRdsEvents))) {
               mCallbacks.fmRxEvRdsInfo(iRdsEvents);
       }
    }

}
