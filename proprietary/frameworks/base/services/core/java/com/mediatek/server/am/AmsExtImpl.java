/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.server.am;

import android.os.Process;
import com.android.server.am.ContentProviderRecord;
import com.android.server.am.ProcessList;

import static com.android.server.am.ActivityManagerDebugConfig.APPEND_CATEGORY_NAME;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ALL;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ALL_ACTIVITIES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ADD_REMOVE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_ANR;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_APP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKGROUND_CHECK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BACKUP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_BACKGROUND;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_BROADCAST_LIGHT;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CLEANUP;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CONFIGURATION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_CONTAINERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_FOCUS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_IDLE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_IMMERSIVE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_LOCKTASK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_LRU;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_MU;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_NETWORK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_OOM_ADJ_REASON;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PAUSE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_POWER_QUICK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESS_OBSERVERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROCESSES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PROVIDER;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PSS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RECENTS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RELEASE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RESULTS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SAVED_STATE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_FOREGROUND_SERVICE;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SERVICE_EXECUTING;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_STACK;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_STATES;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_SWITCH;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_TASKS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_RECENTS_TRIM_TASKS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_METRICS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_TRANSITION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_UID_OBSERVERS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_URI_PERMISSION;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_USER_LEAVING;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_VISIBILITY;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_USAGE_STATS;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_PERMISSIONS_REVIEW;
import static com.android.server.am.ActivityManagerDebugConfig.DEBUG_WHITELISTS;

/*
import static com.android.server.am.ProcessList.medLoosen;
import static android.os.Process.getTotalMemory;
*/
import android.app.AppGlobals;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.util.ArrayMap;
import android.util.Slog;
import android.util.SparseArray;
import com.android.internal.app.procstats.ProcessStats;
import com.android.internal.os.ProcessCpuTracker;
import com.android.server.am.ActivityManagerService;
import com.android.server.am.ActivityRecord;
import com.android.server.am.ProcessRecord;

/// M: App-based AAL @{
import com.mediatek.amsAal.AalUtils;
/// @}

import com.mediatek.server.powerhal.PowerHalManager;
import com.mediatek.server.powerhal.PowerHalManagerImpl;

/// M: DuraSpeed @{
import com.mediatek.duraspeed.manager.DuraSpeedService;
import com.mediatek.duraspeed.suppress.SuppressAction;
/// @}

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.DataOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AmsExtImpl extends AmsExt {
    private static final String TAG = "AmsExtImpl";
    private boolean isDebug = false;

    private boolean isDuraSpeedSupport = "1".equals
            (SystemProperties.get("persist.vendor.duraspeed.support"));
    private boolean isLowMemoryDevice = "true".equals(SystemProperties.get("ro.config.low_ram"));

    public PowerHalManagerImpl mPowerHalManagerImpl = null;

    /// M: App-based AAL @{
    private AalUtils mAalUtils = null;
    /// @}

    /// M: DuraSpeed @{
    private DuraSpeedService mDuraSpeedService;
    private SuppressAction mSuppressAction;
    private Context mContext;
    /// @}

    private boolean mIsLaunchingProvider;

    /*
    /// M: Add for higher LMKD levels @{
    private int highOomAdj = 800;
    private int mediumOomAdj = 700;
    private boolean is512Project = getTotalMemory() / 1024.0 / 1024.0  < 512.0;
    /// @}
    */

    private final String amsLogProp = "persist.vendor.sys.activitylog";

    public AmsExtImpl() {

        mPowerHalManagerImpl = new PowerHalManagerImpl();
        /// M: DuraSpeed @{
        if (isDuraSpeedSupport) {
            mDuraSpeedService = DuraSpeedService.getInstance();
            mSuppressAction = SuppressAction.getInstance();
        }
        /// @}

        /// M: App-based AAL @{
        if (mAalUtils == null && AalUtils.isSupported()) {
            mAalUtils = AalUtils.getInstance();
        }
        ///@}
    }

    @Override
    public void onAddErrorToDropBox(String dropboxTag, String info, int pid) {
        if (isDebug) {
            Slog.d(TAG, "onAddErrorToDropBox, dropboxTag=" + dropboxTag
                    + ", info=" + info
                    + ", pid=" + pid);
        }
    }

    @Override
    public void onSystemReady(Context context) {
        Slog.d(TAG, "onSystemReady");


        /// M: DuraSpeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onSystemReady();
        }
        mContext = context;
        /// @}
    }

    @Override
    public void onBeforeActivitySwitch(ActivityRecord lastResumedActivity,
                                       ActivityRecord nextResumedActivity,
                                       boolean pausing,
                                       int nextResumedActivityType) {
        if (nextResumedActivity == null || nextResumedActivity.info == null
                || lastResumedActivity == null) {
            return;
        } else if (nextResumedActivity.packageName == lastResumedActivity.packageName
                && nextResumedActivity.info.name == lastResumedActivity.info.name) {
            // same activity
            return;
        }

        String lastResumedPackageName = lastResumedActivity.packageName;
        String nextResumedPackageName = nextResumedActivity.packageName;
        if (isDebug) {
            Slog.d(TAG, "onBeforeActivitySwitch, lastResumedPackageName=" + lastResumedPackageName
                    + ", nextResumedPackageName=" + nextResumedPackageName);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostResume(lastResumedPackageName, nextResumedPackageName);
        }
        /// M: DuraSpeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onBeforeActivitySwitch(lastResumedPackageName,
                    nextResumedPackageName, pausing, nextResumedActivityType);
        }
        /// @}
    }

    @Override
    public void onAfterActivityResumed(ActivityRecord resumedActivity) {
        if (resumedActivity.app == null) {
            return;
        }

        int pid = resumedActivity.app.pid;
        String activityName = resumedActivity.info.name;
        String packageName = resumedActivity.info.packageName;
        if (isDebug) {
            Slog.d(TAG, "onAfterActivityResumed, pid=" + pid
                    + ", activityName=" + activityName
                    + ", packageName=" + packageName);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostNotify(pid, activityName, packageName);
        }

        /// M: App-based AAL @{
        if (mAalUtils != null) {
            mAalUtils.onAfterActivityResumed(packageName, activityName);
        }
        /// @}
    }

    @Override
    public void onUpdateSleep(boolean wasSleeping, boolean isSleepingAfterUpdate) {
        /// M: App-based AAL @{
        if (mAalUtils != null) {
            mAalUtils.onUpdateSleep(wasSleeping, isSleepingAfterUpdate);
        }
        /// @}
    }

    /// M: App-based AAL @{
    @Override
    public void setAalMode(int mode) {
        if (mAalUtils != null) {
            mAalUtils.setAalMode(mode);
        }
    }

    @Override
    public void setAalEnabled(boolean enabled) {
        if (mAalUtils != null) {
            mAalUtils.setEnabled(enabled);
        }
    }

    @Override
    public int amsAalDump(PrintWriter pw, String[] args, int opti) {
        if (mAalUtils != null) {
            return mAalUtils.dump(pw, args, opti);
        } else {
            return opti;
        }
    }
    /// @}


    @Override
    public void onStartProcess(String hostingType, String packageName) {
        if (isDebug) {
            Slog.d(TAG, "onStartProcess, hostingType=" + hostingType
                    + ", packageName=" + packageName);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostProcessCreate(hostingType, packageName);
        }
    }

    @Override
    public void onEndOfActivityIdle(Context context, Intent idleIntent) {
        if (isDebug) {
            Slog.d(TAG, "onEndOfActivityIdle, idleIntent=" + idleIntent);
        }

        if (mPowerHalManagerImpl != null) {
            mPowerHalManagerImpl.amsBoostStop();
        }
        /// M: DuraSpeed @{
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onActivityIdle(context, idleIntent);
        }
        /// @}
    }

    @Override
    public void enableAmsLog(ArrayList<ProcessRecord> lruProcesses) {
        String activitylog = SystemProperties.get(amsLogProp, null);
        if (activitylog != null && !activitylog.equals("")) {
            if (activitylog.indexOf(" ") != -1
                    && activitylog.indexOf(" ") + 1 <= activitylog.length()) {
                String[] args = new String[2];
                args[0] = activitylog.substring(0, activitylog.indexOf(" "));
                args[1] = activitylog.substring(activitylog.indexOf(" ") + 1, activitylog.length());
                enableAmsLog(null, args, 0, lruProcesses);
            } else {
                SystemProperties.set(amsLogProp, "");
            }
        }
    }

    @Override
    public void enableAmsLog(PrintWriter pw, String[] args,
            int opti, ArrayList<ProcessRecord> lruProcesses) {
        String option = null;
        boolean isEnable = false;
        int indexLast = opti + 1;

        if (indexLast >= args.length) {
            if (pw != null) {
                pw.println("  Invalid argument!");
            }
            SystemProperties.set(amsLogProp, "");
        } else {
            option = args[opti];
            isEnable = "on".equals(args[indexLast]) ? true : false;
            SystemProperties.set(amsLogProp, args[opti] + " " + args[indexLast]);

            if (option.equals("x")) {
                enableAmsLog(isEnable, lruProcesses);
            } else {
                if (pw != null) {
                    pw.println("  Invalid argument!");
                }
                SystemProperties.set(amsLogProp, "");
            }
        }
    }

    private void enableAmsLog(boolean isEnable, ArrayList<ProcessRecord> lruProcesses) {
        isDebug = isEnable;

        APPEND_CATEGORY_NAME = isEnable;
        DEBUG_ALL = isEnable;
        DEBUG_ALL_ACTIVITIES = isEnable;
        DEBUG_ADD_REMOVE = isEnable;
        DEBUG_ANR = isEnable;
        DEBUG_APP = isEnable;
        DEBUG_BACKGROUND_CHECK = isEnable;
        DEBUG_BACKUP = isEnable;
        DEBUG_BROADCAST = isEnable;
        DEBUG_BROADCAST_BACKGROUND = isEnable;
        DEBUG_BROADCAST_LIGHT = isEnable;
        DEBUG_CLEANUP = isEnable;
        DEBUG_CONFIGURATION = isEnable;
        DEBUG_CONTAINERS = isEnable;
        DEBUG_FOCUS = isEnable;
        DEBUG_IDLE = isEnable;
        DEBUG_IMMERSIVE = isEnable;
        DEBUG_LOCKTASK = isEnable;
        DEBUG_LRU = isEnable;
        DEBUG_MU = isEnable;
        DEBUG_NETWORK = isEnable;
        //DEBUG_OOM_ADJ = isEnable;
        //DEBUG_OOM_ADJ_REASON = isEnable;
        DEBUG_PAUSE = isEnable;
        DEBUG_POWER = isEnable;
        DEBUG_POWER_QUICK = isEnable;
        DEBUG_PROCESS_OBSERVERS = isEnable;
        DEBUG_PROCESSES = isEnable;
        DEBUG_PROVIDER = isEnable;
        DEBUG_PSS = isEnable;
        DEBUG_RECENTS = isEnable;
        DEBUG_RELEASE = isEnable;
        DEBUG_RESULTS = isEnable;
        DEBUG_SAVED_STATE = isEnable;
        DEBUG_RECENTS_TRIM_TASKS = isEnable;
        DEBUG_METRICS = isEnable;
        DEBUG_SERVICE = isEnable;
        DEBUG_FOREGROUND_SERVICE = isEnable;
        DEBUG_SERVICE_EXECUTING = isEnable;
        DEBUG_STACK = isEnable;
        DEBUG_STATES = isEnable;
        DEBUG_SWITCH = isEnable;
        DEBUG_TASKS = isEnable;
        DEBUG_TRANSITION = isEnable;
        DEBUG_UID_OBSERVERS = isEnable;
        DEBUG_URI_PERMISSION = isEnable;
        DEBUG_USER_LEAVING = isEnable;
        DEBUG_VISIBILITY = isEnable;
        DEBUG_USAGE_STATS = isEnable;
        DEBUG_PERMISSIONS_REVIEW = isEnable;
        DEBUG_WHITELISTS = isEnable;

        for (int i = 0; i < lruProcesses.size(); i++) {
            ProcessRecord app = lruProcesses.get(i);
            if (app != null && app.thread != null) {
                try {
                    app.thread.enableActivityThreadLog(isEnable);
                } catch (Exception e) {
                     Slog.e(TAG, "Error happens when enableActivityThreadLog", e);
                }
            }
        }
    }

    /// M: DuraSpeed @{
    @Override
    public void onWakefulnessChanged(int wakefulness) {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.onWakefulnessChanged(wakefulness);
        }
    }

    @Override
    public void addDuraSpeedService() {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            ServiceManager.addService("duraspeed", (IBinder) mDuraSpeedService, true);
        }
    }

    @Override
    public void startDuraSpeedService(Context context) {
        if (isDuraSpeedSupport && mDuraSpeedService != null) {
            mDuraSpeedService.startDuraSpeedService(context);
            // This code just co-work with lmkd, and which only work in low memory device,
            // so we need take care about lmkd feature, this logic may change.
            if (isLowMemoryDevice) {
                MemoryServerThread memoryServer = new MemoryServerThread();
                memoryServer.start();
            }
        }
    }

    @Override
    public String onReadyToStartComponent(String packageName, int uid, String suppressReason) {
        if (mDuraSpeedService != null && mDuraSpeedService.isDuraSpeedEnabled()) {
            return mSuppressAction.onReadyToStartComponent(packageName, uid, suppressReason);
        }
        return null;
    }

    @Override
    public boolean onBeforeStartProcessForStaticReceiver(String packageName) {
        if (mDuraSpeedService != null && mDuraSpeedService.isDuraSpeedEnabled()) {
            return mSuppressAction.onBeforeStartProcessForStaticReceiver(packageName);
        }
        return false;
    }

    @Override
    public void addToSuppressRestartList(String packageName) {
        if (mDuraSpeedService != null && mDuraSpeedService.isDuraSpeedEnabled() &&
                mContext != null) {
            mSuppressAction.addToSuppressRestartList(mContext, packageName);
        }
    }
    /// @}

    /*
    // update LMKD OomAdj leves
    public void updateLMKDForBrowser(boolean isHigher) {
        if (!is512Project) {
            return;
        }
        int OomAdjLeves = isHigher ? highOomAdj : mediumOomAdj;
        if (isDebug) {
            Slog.w(TAG, "updateLMKDForBrowser OomAdjLeves=" + OomAdjLeves);
        }
        medLoosen(OomAdjLeves);
    }

    // resetLMKD levels to medium levels
    public void resetLMKDForBrowserIfNeed(ProcessRecord proc) {
        if (!is512Project) {
            return;
        }
        if (proc.processName != null && proc.processName.equals("com.android.browser")) {
            if (isDebug) {
                Slog.w(TAG, "resetLMKDForBrowserIfNeed OomAdjLeves=" + mediumOomAdj);
            }
            medLoosen(mediumOomAdj);
        }
    }
    */

    /// Enhance wtf excetpion
    public boolean IsBuildInApp() {
        IPackageManager pm = AppGlobals.getPackageManager();
        try {
            String pkgName = pm.getNameForUid(Binder.getCallingUid());
            ApplicationInfo appInfo = pm.getApplicationInfo(pkgName, 0, UserHandle.getCallingUserId());
            //Build in app
            if (appInfo != null && ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0
                || (appInfo.flags & ApplicationInfo.FLAG_UPDATED_SYSTEM_APP) != 0)) {
                    return true;
            }
        } catch (RemoteException e) {
                Slog.e(TAG, "getCallerProcessName exception :" + e);
        }
        return false;
    }

    public boolean shouldKilledByAm(String processName, String reason) {
        // Just for media process should not be killed by am because of empty process
        if (processName != null && processName.equals("android.process.media") &&
            reason != null && reason.startsWith("empty")) {
            if (isDebug) {
                Slog.w(TAG, "skip kill " + processName + " by am for " + reason);
            }
            return false;
        }
        return true;
    }

    /**
     *  Check whether is launching a provider
     */
    @Override
    public void checkAppInLaunchingProvider(ProcessRecord app, int adj,
             int schedGroup, ArrayList<ContentProviderRecord> providers) {
        if (app.pubProviders.size() != 0 && (adj > ProcessList.FOREGROUND_APP_ADJ)) {
            final int N = providers.size();
            int i;
            ContentProviderRecord cpr;
            for (i = 0; i < N; i++) {
                cpr = providers.get(i);
                if (cpr.launchingApp == app) {
                    mIsLaunchingProvider = true;
                    break;
                }
            }
        }
    }

    /**
     *  Set APP's adj foreground during launching a provider
     */
    @Override
    public void setAppInLaunchingProviderAdj(ProcessRecord app, int adj, int clientAdj) {
        if (mIsLaunchingProvider &&
                  (adj > ProcessList.PERCEPTIBLE_APP_ADJ) && (adj >= clientAdj)) {
            mIsLaunchingProvider = false;
            adj = clientAdj > ProcessList.PERCEPTIBLE_APP_ADJ ?
                  clientAdj - 1 : ProcessList.PERCEPTIBLE_APP_ADJ;
            app.adjType = "launching-provider";
        }
    }

    /**
     * Maintain a task which waiting for LMKD client connected, LMKD will trigger DuraSpeed.
     */
    private class MemoryServerThread extends Thread {
        public static final String HOST_NAME = "duraspeed_memory";

        @Override
        public void run() {
            LocalServerSocket serverSocket = null;
            ExecutorService threadExecutor = Executors.newCachedThreadPool();

            try {
                Slog.d(TAG, "Crate local socket: duraspeed_memory");
                // Create server socket
                serverSocket = new LocalServerSocket(HOST_NAME);

                while (true) {
                    Slog.d(TAG, "Waiting Client connected...");
                    // Allow multiple connection connect to server.
                    LocalSocket socket = serverSocket.accept();
                    socket.setReceiveBufferSize(256);
                    socket.setSendBufferSize(256);
                    Slog.i(TAG, "There is a client is accpted: " + socket.toString());
                    threadExecutor.execute(new ConnectionHandler(socket));
                }
            } catch (Exception e) {
                Slog.w(TAG, "listenConnection catch Exception");
                e.printStackTrace();
            } finally {
                Slog.d(TAG, "listenConnection finally shutdown!!");
                if (threadExecutor != null )
                    threadExecutor.shutdown();
                if (serverSocket != null) {
                    try {
                        serverSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            Slog.d(TAG, "listenConnection() - end");
        }
    }

    /*
     * level: 0\1\2 means low\medium\critical
     * pressure: value range 0~100, for low memory project:
     *     >60 means low, <40 means critical. 40~60 means medium.
     */
    public class ConnectionHandler implements Runnable {
        private LocalSocket mSocket;
        private boolean mIsContinue = true;
        private InputStreamReader mInput = null;
        private DataOutputStream mOutput = null;

        public ConnectionHandler(LocalSocket clientSocket) {
            mSocket = clientSocket;
        }

        public void terminate() {
            Slog.d(TAG, "DuraSpeed memory trigger process terminate.");
            mIsContinue = false;
        }

        @Override
        public void run() {
            Slog.i(TAG, "DuraSpeed new connection: " + mSocket.toString());

            try {
                mInput = new InputStreamReader(mSocket.getInputStream());
                mOutput = new DataOutputStream(mSocket.getOutputStream());
                try {
                    final BufferedReader bufferedReader = new BufferedReader(mInput);
                    while (mIsContinue) {
                        String lmkdData = bufferedReader.readLine();
                        String[] result = lmkdData.split(":");
                        if (result[0] == null || result[1] == null) {
                            Slog.e(TAG, "Received lmkdData error");
                            continue;
                        }
                        String level = result[0].trim();
                        String memPressrue = result[1].trim();
                        int levelResult = Integer.parseInt(level);
                        int memPressrueResult = Integer.parseInt(memPressrue);
                        if (levelResult >= 0 && memPressrueResult > 0 &&
                                mDuraSpeedService.isDuraSpeedEnabled()) {
                            mDuraSpeedService.triggerMemory(memPressrueResult);
                        }
                    }
                } catch (Exception e) {
                    Slog.w(TAG, "duraSpeed: memory Exception.");
                    e.printStackTrace();
                    terminate();
               }
                Slog.w(TAG, "duraSpeed: New connection running ending ");

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
