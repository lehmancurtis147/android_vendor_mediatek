package com.mediatek.view.impl;

import android.content.pm.IPackageManager;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;

import com.mediatek.view.SurfaceExt;


public class SurfaceExtimpl extends SurfaceExt {
    private static final String TAG = "SurfaceExt";
    private static final boolean ENABLE_WHITE_LIST = SystemProperties.getBoolean(
            "debug.enable.whitelist", false);
    private static final String WHITE_LIST[] = {"com.tencent.qqlive"};
    // "com.qiyi.video","tv.pps.mobile"

    @Override
    public boolean isInWhiteList() {
        if (ENABLE_WHITE_LIST) {
            return true;
            }
        boolean isContained = false;
        String packageName = getCallerProcessName();
        if (WHITE_LIST != null && packageName != null) {
            for (String item : WHITE_LIST) {
                if (item.equals(packageName)) {
                    isContained = true;
                    break;
                }
            }
        }
        return isContained;
    }

    private String getCallerProcessName() {
        int binderuid = Binder.getCallingUid();
        IPackageManager pm = IPackageManager.Stub.asInterface(ServiceManager.getService("package"));
        if (pm != null) {
            try {
                String callingApp = pm.getNameForUid(binderuid);
                // Log.d(TAG, "callingApp: " + callingApp);
                return callingApp;
            } catch (RemoteException e) {
                Log.e(TAG, "getCallerProcessName exception :" + e);
            }
        }
        return null;
    }
}
