/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "FBC"
#include <log/log.h>
#include <dlfcn.h>

#include <stdio.h>

#include "jni.h"

#undef NELEM
#define NELEM(x) (sizeof(x)/sizeof(*(x)))

namespace android
{
#if defined(MTK_AFPSGO_FBT_GAME)
static int inited = false;

static int (*fbcNotifyIntendedVsync)(int, int64_t) = NULL;
static int (*fbcNotifyNoRender)(int64_t) = NULL;
static int (*fbcNotifyDrawStart)(int64_t) = NULL;
static int (*fbcNotifyFrameComplete)(int, int, int, int64_t) = NULL;
#endif

typedef int (*notify_intended_vsync)(int, int64_t);
typedef int (*notify_no_render)(int64_t);
typedef int (*notify_draw_start)(int64_t);
typedef int (*notify_frame_complete)(int, int, int, int64_t);

#define LIB_FULL_NAME "libperfctl.so"

#if defined(MTK_AFPSGO_FBT_GAME)
static void init()
{
    void *handle, *func;

    // only enter once
    inited = true;

    handle = dlopen(LIB_FULL_NAME, RTLD_NOW);
    if (handle == NULL) {
        ALOGE("Can't load library: %s", dlerror());
        return;
    }

    func = dlsym(handle, "fbcNotifyIntendedVsync");
    fbcNotifyIntendedVsync = reinterpret_cast<notify_intended_vsync>(func);

    if (fbcNotifyIntendedVsync == NULL) {
        ALOGE("fbcNotifyIntendedVsync error: %s", dlerror());
        fbcNotifyIntendedVsync = NULL;
        dlclose(handle);
        return;
    }

    func = dlsym(handle, "fbcNotifyNoRender");
    fbcNotifyNoRender = reinterpret_cast<notify_no_render>(func);

    if (fbcNotifyNoRender == NULL) {
        ALOGE("fbcNotifyNoRender error: %s", dlerror());
        fbcNotifyNoRender = NULL;
        dlclose(handle);
        return;
    }

    func = dlsym(handle, "fbcNotifyDrawStart");
    fbcNotifyDrawStart = reinterpret_cast<notify_draw_start>(func);

    if (fbcNotifyDrawStart == NULL) {
        ALOGE("fbcNotifyDrawStart error: %s", dlerror());
        fbcNotifyDrawStart = NULL;
        dlclose(handle);
        return;
    }

    func = dlsym(handle, "fbcNotifyFrameComplete");
    fbcNotifyFrameComplete = reinterpret_cast<notify_frame_complete>(func);

    if (fbcNotifyFrameComplete == NULL) {
        ALOGE("fbcNotifyFrameComplete error: %s", dlerror());
        fbcNotifyFrameComplete = NULL;
        dlclose(handle);
        return;
    }
}
#endif

static int
com_mediatek_perfframe_PerfFrameInfoManagerImpl_markIntendedVsync(JNIEnv* /* env */,
                                        jobject /* thiz */, jint tid, jlong frame_id)
{
#if defined(MTK_AFPSGO_FBT_GAME)
    if (!inited)
        init();

    if (fbcNotifyIntendedVsync)
        return fbcNotifyIntendedVsync(tid, frame_id);

#endif
    return -1;
}

static int
com_mediatek_perfframe_PerfFrameInfoManagerImpl_markNoRender(JNIEnv* /* env */,
                                             jobject /* thiz */, jlong frame_id)
{
#if defined(MTK_AFPSGO_FBT_GAME)
    if (!inited)
        init();

    if (fbcNotifyNoRender)
        return fbcNotifyNoRender(frame_id);

#endif
    return -1;
}

static int
com_mediatek_perfframe_PerfFrameInfoManagerImpl_markDrawStart(JNIEnv* /* env */,
                                              jobject /* thiz */, jlong frame_id)
{
#if defined(MTK_AFPSGO_FBT_GAME)
    if (!inited)
        init();

    if (fbcNotifyDrawStart)
        return fbcNotifyDrawStart(frame_id);

#endif
    return -1;
}

static int
com_mediatek_perfframe_PerfFrameInfoManagerImpl_markMarkFrameComplete(JNIEnv* /* env */,
          jobject /* thiz */, jint tid, jint frame_time, jint frame_type, jlong frame_id)
{
#if defined(MTK_AFPSGO_FBT_GAME)
    if (!inited)
        init();

    if (fbcNotifyFrameComplete)
        return fbcNotifyFrameComplete(tid, frame_time, frame_type, frame_id);

#endif
    return -1;
}

static JNINativeMethod sMethods[] = {
    {"nativeMarkIntendedVsync",   "(IJ)I",
            (int *)com_mediatek_perfframe_PerfFrameInfoManagerImpl_markIntendedVsync},
    {"nativeMarkNoRender",   "(J)I",
            (int *)com_mediatek_perfframe_PerfFrameInfoManagerImpl_markNoRender},
    {"nativeMarkDrawStart",   "(J)I",
            (int *)com_mediatek_perfframe_PerfFrameInfoManagerImpl_markDrawStart},
    {"nativeMarkFrameComplete",   "(IIIJ)I",
            (int *)com_mediatek_perfframe_PerfFrameInfoManagerImpl_markMarkFrameComplete},
};

int register_com_mediatek_perfframe_PerfFrameInfoManagerImpl(JNIEnv* env)
{
    jclass clazz = env->FindClass("com/mediatek/perfframe/PerfFrameInfoManagerImpl");

    if (env->RegisterNatives(clazz, sMethods, NELEM(sMethods)) < 0) {
        ALOGE("RegisterNatives error");
        return JNI_ERR;
    }

    return JNI_OK;
}

}

