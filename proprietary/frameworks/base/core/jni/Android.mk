#
# Copyright (C) 2008 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)
ifeq ($(strip $(MTK_AFPSGO_FBT_GAME)), yes)
    LOCAL_CFLAGS += -DMTK_AFPSGO_FBT_GAME
endif

# App-based AAL @{
ifeq ($(strip $(MTK_AAL_SUPPORT)), yes)
  LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/aal/include \
                      $(TOP)/system/core/base/include
  LOCAL_SHARED_LIBRARIES += libaalservice
  LOCAL_CFLAGS += -DMTK_AAL_SUPPORT
endif
# @}

LOCAL_SHARED_LIBRARIES += libcutils
LOCAL_LDLIBS := -llog
LOCAL_SRC_FILES := \
  com_mediatek_perfframe_PerfFrameInfoManagerImpl.cpp \
  com_mediatek_powerhalwrapper_PowerHalWrapper.cpp \
  com_mediatek_amsAal_AalUtils.cpp
LOCAL_C_INCLUDES += \
    $(JNI_H_INCLUDE) \
    frameworks/base/core/jni \
    $(LOCAL_PATH)/inc
LOCAL_MODULE := libperfframeinfo_jni
LOCAL_MODULE_TAGS := optional
LOCAL_PRELINK_MODULE := false
include $(MTK_SHARED_LIBRARY)
