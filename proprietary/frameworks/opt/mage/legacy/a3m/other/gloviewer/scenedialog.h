/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 * 
 * MediaTek Inc. (C) 2010. All rights reserved.
 * 
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/**
 * Scene Dialog
 *
 */
#pragma once
#ifndef A3M_SCENEDIALOG_H
#define A3M_SCENEDIALOG_H

#include "viewerdialog.h"  /* Used as base class */
#include "lightdialog.h"   /* Container for light-specific controls */
#include "cameradialog.h"  /* Container for camera-specific controls */
#include "soliddialog.h"   /* Container for solid-specific controls */

#include <a3m/scenenodevisitor.h> /* Visit node to determine type   */

/*
 * Modeless dialog to allow user to examine the scene graph and click on
 * individual nodes to examine data specific to each node type.
 */
class SceneDialog : public ViewerDialog, a3m::SceneNodeVisitor
{
public:
  SceneDialog( HWND parentWindowHandle );

  // Called by client when the scene has been loaded.
  void init( a3m::SceneNode *node );

  // Overriden methods from ViewerDialog.
  virtual void onCommand( A3M_INT32 control, A3M_INT32 event, A3M_INT32 lparam );
  virtual void onNotify( A3M_INT32 control, A3M_INT32 lparam );

  // Overriden methods from SceneNodeVisitor.
  virtual void visit( a3m::SceneNode *node );
  virtual void visit( a3m::Solid *solid );
  virtual void visit( a3m::Light *light );
  virtual void visit( a3m::Camera *camera );

  // Update control text to reflect any changes in data.
  void update();

  // Hide all child dialogs when a new node has been selected
  void hideChildDialogs();
private:
  a3m::SceneNode *m_selected;  // Currently selected node
  LightDialog m_lightDlg;      // Light-specific controls
  CameraDialog m_cameraDlg;    // Camera-specific controls
  SolidDialog m_solidDlg;      // Solid-specific controls
};

#endif // A3M_SCENEDIALOG_H
