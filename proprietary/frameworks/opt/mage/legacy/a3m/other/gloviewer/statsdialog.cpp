/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include "statsdialog.h"            /* Class header */
#include <a3m/glofile.h>            /* For Glo */
#include <a3m/indexbuffer.h>        /* For IndexBuffer */
#include <a3m/mesh.h>               /* For Mesh */
#include <a3m/scenenode.h>          /* For SceneNode */
#include <a3m/sceneutility.h>       /* For visitScene() */
#include <a3m/scenenodevisitor.h>   /* For SceneNodeVisitor */

#include "resource.h"           /* Dialog and control resource identifiers */

class StatsDialog::StatsCollector : public a3m::SceneNodeVisitor
{
public:
  StatsCollector() :
    vertexCount( 0 ),
    triangleCount( 0 ),
    solidCount( 0 ),
    lightCount( 0 ),
    cameraCount( 0 )
  {
  }

  void visit( a3m::Solid* solid )
  {
    ++solidCount;

    a3m::Mesh::Ptr const& mesh = solid->getMesh();

    if( mesh )
    {
      vertexCount += mesh->getVertexBuffer().getVertexCount();
      triangleCount += a3m::getTriangleCount( mesh->getIndexBuffer() );
    }
  }

  void visit( a3m::Light* light )
  {
    ++lightCount;
  }

  void visit( a3m::Camera* camera )
  {
    ++cameraCount;
  }

  A3M_INT32 vertexCount;
  A3M_INT32 triangleCount;
  A3M_INT32 solidCount;
  A3M_INT32 lightCount;
  A3M_INT32 cameraCount;
};

StatsDialog::StatsDialog( HWND parentWindowHandle ) :
  ViewerDialog( parentWindowHandle, IDD_STATISTICS ),
  m_collector( new StatsCollector() )
{
}

StatsDialog::~StatsDialog()
{
  // Empty destructor required to prevent deletion of StatsCollector in header
}

void StatsDialog::add( a3m::Glo& glo )
{
  a3m::visitScene( *m_collector, *glo.getSceneNode() );

  setEditText( IDC_VERTEX_COUNT, m_collector->vertexCount );
  setEditText( IDC_TRIANGLE_COUNT, m_collector->triangleCount );
  setEditText( IDC_SOLID_COUNT, m_collector->solidCount );
  setEditText( IDC_LIGHT_COUNT, m_collector->lightCount );
  setEditText( IDC_CAMERA_COUNT, m_collector->cameraCount );
}

void StatsDialog::clear()
{
  m_collector.reset( new StatsCollector() );
}
