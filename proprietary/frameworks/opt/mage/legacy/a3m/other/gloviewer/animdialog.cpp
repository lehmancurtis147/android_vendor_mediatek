/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include "animdialog.h"  /* Class header */

#include "resource.h"    /* Dialog resource identifier */

AnimationDialog::AnimationDialog( HWND parentWindowHandle )
  : ViewerDialog( parentWindowHandle, IDD_ANIMATION ), m_scrubbing( A3M_FALSE )
{
  setScrollRange( IDC_PROGRESS_SLIDER, 0, 1000 );
  setScrollPageSize( IDC_PROGRESS_SLIDER, 50 );
}

void AnimationDialog::update()
{
  if( !m_controller )
  {
    return;
  }

  // Update slider position and text to reflect animation progress
  A3M_FLOAT position =  m_controller->getProgress();
  setEditText( IDC_PROGRESS, position );
  setScrollPos( IDC_PROGRESS_SLIDER, static_cast< A3M_INT32 >(
    position * 1000 / getLength( *m_controller ) ) );

  // Stop animation when it reaches the end
  if( !( m_scrubbing || m_controller->getLooping() ) &&
    m_controller->getProgress() == m_controller->getEnd() )
  {
    m_controller->setPaused( A3M_TRUE );
  }

  setEditText( IDC_PLAY_PAUSE, m_controller->getPaused() ? "Play" : "Pause" );
}

void AnimationDialog::setAnimation( a3m::AnimationController::Ptr const& controller )
{
  m_controller = controller;

  if ( !m_controller )
  {
    return;
  }

  A3M_FLOAT start = m_controller->getStart();
  A3M_FLOAT end = m_controller->getEnd();
  A3M_FLOAT progress = m_controller->getProgress();
  A3M_BOOL looping = hasLoop( *m_controller );
  m_controller->setLooping( looping );
  setChecked( IDC_LOOP, hasLoop( *m_controller ) );

  if( !looping )
  {
    setLoopRange( *m_controller, m_controller->getStart(), m_controller->getEnd() );
  }

  setEditText( IDC_PROGRESS, progress );
  setEditText( IDC_DURATION, end - start );
  setEditText( IDC_LOOP_START, m_controller->getLoopStart() );
  setEditText( IDC_LOOP_END, m_controller->getLoopEnd() );

  update();
}

void AnimationDialog::onHScroll( A3M_UINT32 pos )
{
  if( !m_controller )
  {
    return;
  }

  if( !m_scrubbing )
  {
    m_scrubbing = A3M_TRUE;
    m_wasPaused = m_controller->getPaused();
    m_controller->setPaused( A3M_TRUE );
  }

  seek( *m_controller, pos / 1000.0f * getLength( *m_controller ) );
}

void AnimationDialog::onHScrollEnd()
{
  if( !m_controller )
  {
    return;
  }

  m_controller->setPaused( m_wasPaused );
  m_controller->setEnabled( A3M_TRUE );
  m_scrubbing = A3M_FALSE;
}

void AnimationDialog::onCommand( A3M_INT32 control, A3M_INT32 event, A3M_INT32 lParam )
{
  if( !m_controller )
  {
    return;
  }

  switch( control )
  {
    case IDC_PLAY_PAUSE:
      if( m_controller->getPaused() &&
          m_controller->getProgress() == m_controller->getEnd() )
      {
        rewind( *m_controller );
      }

      m_controller->setPaused( !m_controller->getPaused() );
      break;

    case IDC_STOP:
      m_controller->setPaused( A3M_TRUE );
      rewind( *m_controller );
      break;

    case IDC_LOOP:
      m_controller->setLooping( isChecked( control ) );
      break;
  }
}
