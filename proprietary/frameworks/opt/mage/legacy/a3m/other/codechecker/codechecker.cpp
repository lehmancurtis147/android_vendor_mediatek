/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**
 * This utility checks for any tab, line lengths bigger than allowed length.
 * It also checks if it is using forbidden C++ datatypes directly.
 *
 * Compile on Linux using makefile:  make codechecker
 */

#include <stdio.h>    /* for strncpy */
#include <string.h>   /* for strncpy */
#include <string>     /* for std::string */
#include <iostream>   /* for std::cout */
#include <fstream>    /* for std::ifstream */

#ifdef STRICT_SDG

// Limit set for strict maximum length of a line in characters
const int longLineLength = 80;
// How much leeway is allowed before a Warning is raised
const int longLineLeeway = 0;
// Limit set for absolute maximum length of a line in characters
const int veryLongLineLength = 80;
// Limit for max file length
const int longFileLength = 2000;
// Limit for max file length before an Error is raised
const int veryLongFileLength = 2000;

#else

// Allow +20% on everything before raising a red flag
const int longLineLength = 85; // >Length is noted / Info
const int longLineLeeway = 5;  // >Length+Leewway is Warning
const int veryLongLineLength = 100; // >VeryLong is an Error
const int longFileLength = 2000;
const int veryLongFileLength = 2400;

#endif


const int copyrightLineNumber = 3;
const std::string copyrightNotice = " * Copyright (c)";

const int idKeywordLineNumber = 11;

// Returns TRUE if character is alphanumeric or '_'
bool symbolCharacter( int c )
{
  return isalnum( c ) || c == '_';
}

// Checks for existance of the 'word' in a line where it isn't part of a symbol
bool containsWord( std::string const &line, std::string const &word )
{
  size_t wordPosition = line.find( word );
  while( wordPosition != std::string::npos )
  {
    if( wordPosition == 0 || !symbolCharacter( line[wordPosition - 1] ) )
    {
      if( wordPosition + word.length() >= line.length() ||
          !symbolCharacter( line[ wordPosition + word.length() ] ) )
      {
        return true;
      }
    }
    wordPosition = line.find( word, wordPosition + 1 );
  }
  return false;
}

// It strips line read from the file to make sure that it part of the line to
// be checked is not within '" "' and '/* */' and '//'
std::string stripLine( std::string const &line,
                       bool &inString,
                       bool &inComment )
{
  std::string result;
  size_t index = 0;
  while( index < line.length() )
  {
    while( inString && ( index < line.length() ) )
    {
      // if there is escaped character advance index an extra time to ignore it
      if( line[ index ] == '\\' )
      {
        ++index;
      }
      else if( line[ index ] == '"' )
      {
        inString = false;
      }
      ++index;
    }

    while( inComment && ( index < line.length() ) )
    {
      if( line.compare( index, 2, "*/" ) == 0 )
      {
        inComment = false;
        ++index; // eat up the extra '/'
      }
      ++index;
    }

    if( line[ index ] == '\'' )
    {
      if( line.compare( index, 2, "'\\" ) == 0 ) ++index;
      index += 2;
    }
    else if( line[ index ] == '"' )
    {
      inString = true;
    }
    else if( line.compare( index, 2, "//" ) == 0 )
    {
      return result;
    }
    else if( line.compare( index, 2, "/*" ) == 0 )
    {
      inComment = true;
    }
    else
    {
      result += line[index];
    }
    ++index;
  }
  return result;
}


std::string removeWhiteSpace( std::string const &line )
{
  std::string result;
  for( unsigned int i = 0; i < line.length(); ++i )
  {
    if( !isspace( static_cast<unsigned char>( line[i] ) ) ) result += line[i];
  }
  return result;
}



/* For consistency, note the following:
 *
 * Error - means a mandatory coding standard rule has been violated.
 * Warning - means that an agreed rule has been violated.
 * Info - means that a coding standard recommendation has been exceeded or
 * that a mandatory or agreed rule is near to being broken.
 *
 * Errors must be fixed.
 * Warnings must be fixed if that's what the project has agreed on.
 * Info messages should ideally be fixed.
 *
 * Automated build/reporting system parses lines for Error and Warning; thus
 * Info messages are not reported by it; nor any additional lines of text that
 * don't contain "Error" or "Warning".
 */
int process( const char* fname, const bool verbose )
{
  char const *forbiddenWords[] = { "char", "bool", "int", "float", "long",
                                   "unsigned", "short", "double", "signed",
                                   "near", "far"};

  const int forbiddenCount = sizeof( forbiddenWords ) / sizeof( char *);

  int errorCount = 0;
  int warningCount = 0;
  int infoCount = 0;
  int numLongLines = 0;

  std::string filename( fname );

  std::ifstream file( filename.c_str() );

  if( !file )
  {
    std::cout << "Error: file " << filename << " not found\n";
    return -1;
  }

  std::string line;
  int lineNumber = 0;

  bool inString = false;
  bool inComment = false;
  while( std::getline( file, line ) )
  {
    ++lineNumber;
    // Check that all characters are in range 0-127
    for( unsigned int i = 0; i < line.length(); ++i )
    {
      if( ( line[i] < 0 ) || ( line[i] > 127 ) )
      {
        std::cout << filename << "(" << lineNumber << ") : Error:" <<
          " contains non-ascii character.\n";
        return 1;
      }
    }

    std::string noWhite = removeWhiteSpace( line );
    if( noWhite.length() >= 4 && noWhite.substr( 0, 4 ) == "#if0" )
    {
      std::cout << filename << "(" << lineNumber << ") : Warning: " <<
        " #if 0 encountered.\n";
      ++warningCount;
    }


    if( noWhite.length() >= 8 && noWhite.substr( 0, 8 ) == "#include" )
    {
      if( ( noWhite.find( "//" ) == std::string::npos ) &&
          ( noWhite.find( "/*" ) == std::string::npos ) )
      {
        std::cout << filename << "(" << lineNumber << ") : Warning: " <<
          "#include without justifying comment.\n";
        ++warningCount;
      }
    }

    // Check for long lines.

    // Omit the automated P4 'Id' keyword line which we have no control over.
    if( lineNumber != idKeywordLineNumber )
    {
      // Excessively long - beyond mandatory limit
      if( line.size() > veryLongLineLength )
      {
        std::cout << filename << "(" << lineNumber << ") : Error:" <<
          " exceeds " << veryLongLineLength <<
          "-character limit. (" << line.size() << ").\n";

        ++errorCount;
        ++numLongLines;
      }
      // Moderately long - noticeably over the Code Std. limit
      else if( line.size() > (longLineLength + longLineLeeway) )
      {
        std::cout << filename << "(" << lineNumber << ") : Warning:" <<
          " exceeds " << longLineLength + longLineLeeway <<
          "-character limit. (" << line.size() << ").\n";
        ++warningCount;
        ++numLongLines;
      }
      // Just over - mention but don't get upset
      else if( line.size() > (longLineLength) )
      {
        if( verbose )
        {
          std::cout << filename << "(" << lineNumber << ") : Info:" <<
              " line length (" << line.size() << ").\n";
        }
        ++infoCount;
        ++numLongLines;
      }
    }

    // Check for tabs
    size_t tabPosition = line.find( '\t' );
    if( tabPosition != std::string::npos )
    {
      std::cout << filename << "(" << lineNumber << ") : Error:" <<
        " contains TAB characters.\n";
      ++errorCount;
    }

    // Remove comments and strings from line before looking for forbidden words
    std::string strippedLine = stripLine( line, inString, inComment );

    // We shouldn't be in the middle of a string between lines
    if( inString )
    {
      std::cout << filename << "(" << lineNumber << ") : Warning:" <<
        " string literal not closed in file\n";
      inString = false;
      ++warningCount;
    }

    for( int i = 0; i < forbiddenCount; ++i )
    {
      if( containsWord( strippedLine, forbiddenWords[i] ) )
      {
        std::cout << filename << "(" << lineNumber << ") : Error:" <<
          " contains " << forbiddenWords[i] << "\n";
        ++errorCount;
      }
    }
  }

  // End of file checks
  if( lineNumber > veryLongFileLength )
  {
    std::cout << filename << "(" << lineNumber << ") : Error:" <<
    " Exceeds absolute limit of " << veryLongFileLength << " lines.\n";
    ++errorCount;
  }
  else if( lineNumber > longFileLength )
  {
    std::cout << filename << "(" << lineNumber << ") : Warning:" <<
    " Exceeds limit of " << longFileLength << " lines.\n";
    ++infoCount;
  }

  if( lineNumber > 0 ) // avoid divide-by-zero
  {
    const int maxLongLinePercent = 10;
    const int percent = (100 * numLongLines) / lineNumber;

    // A few % is not worth bothering with, but
    // much more than that is 'taking the mick'.
    if( percent > maxLongLinePercent  )
    {
      std::cout << filename << "(" << lineNumber << ") : Error: >" <<
        percent << "% of lines exceed limit.\n";
      ++warningCount;
    }
  }

  if (verbose && (errorCount != 0 || warningCount != 0))
  {
    std::cout << filename << ": " << errorCount+warningCount+infoCount
      << " issue(s); " << errorCount <<
      " error(s), " << warningCount << " warning(s).\n\n" ;
  }

  return errorCount ? 1 : 0;
}


int main( int argc, char* argv[] )
{
  if( argc <= 1 )
  {
    std::cout << "Usage: code_checker [V] <filename>\n"
              << "or (on Linux): <command> | codechecker [V] -\n"
              << "  e.g. find engine/facility -name \"*.cpp\" -print "
              << "| other/codechecker/codechecker -\n\n"
              << "V option is for Verbose output.\n" ;
    return -1;
  }

  int firstArg = 1;
  bool verbose = false;

  if( argv[firstArg][0] == 'V' )
  {
    // Verbose
    verbose = true;
    firstArg++;
  }

  int countErrors = 0;

  if( argv[firstArg][0] == '-' )
  {
    // Options are provided.  For now the only option is 'read pipe not filenames'
    std::string line;

    while ( getline(std::cin, line) )
    {
      countErrors += process( line.data(), verbose );
    }
  }
  else
  {
    for( int i=firstArg ; i<argc; i++ )
    {
      countErrors += process( argv[i], verbose );
    }
  }

  std::cout.flush();
  return 0;
}


