/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Model interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>GLO3 File Scene Import</h2>

The contents of a Glo3 file (.glo) can be imported into a scene in the form of
a Model object.  A Glo3 file may contain just a single item, or an entire
animated scene with multiple cameras. Note that Glo3 files may contain either a
scene graph, or an animation, or both.

To load a Glo3 file you simply need to specify the filename, and optionally the
root of the portion of the scene graph to which to apply the animation.

The resulting SceneNode (the one containing the Glo object) may be queried using
getSceneNode() function of the Model objectm and any animation contained within
the node obtained by getAnimation().

*/

/*!
 * \ingroup a3mJusrScenenodes
 * @{
 */
/**
 * A Model object is the view in Java of the entity described by a GLO3 file.
 * A Glo3 file is the output of the Digital Content Creation (DCC) pipeline
 * that starts with a 3D artist's tool such as 3dsMax and then post-processed
 * into an efficient form [into a glo3 file] for loading into the A3M system.
 * The Model object is merely a container for the scene graph and/or animation
 * that was loaded from the Glo3 file.
 */
public interface Model {
    /**
     * Returns the root scene node of the scene graph for the Model.
     *
     * @return The root SceneNode, or null if no scene graph exists.
     */
    SceneNode getSceneNode();

    /**
     * Returns the animation for the Model.
     * The animation is contained inside a controller, which provides
     * automatic playback and looping capabilities.
     *
     * @return The AnimationController, or null if no animation exists.
     */
    AnimationController getAnimation();
};

/*! @} */
