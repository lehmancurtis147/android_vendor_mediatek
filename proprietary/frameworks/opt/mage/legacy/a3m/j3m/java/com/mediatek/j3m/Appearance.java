/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * J3M Appearance interface.
 */

package com.mediatek.j3m;

/* Extend JNI Overview */ /*! \addtogroup a3mJusr

<h2>Appearances</h2>

The Appearance class defines how the geometry of the Solid object is rendered on
the screen. This is a combination of many factors:

 - How the object is blended with what is already on the display (BlendFactor).
   This creates partially transparent objects.
 - Whether triangles facing away from the camera can be ignored (CullingMode),
   to enhance performance.
 - Whether the object is affected by a clipping (scissor) rectangle, limiting
   the render to a certain area of the screen.
 - The application of any 2D images (Texture2D) to the surface of the object.
 - The stencil parameters, which affect how the object renders to the stencil
   buffer.
 - The Shader Program chosen to render the object.
 - The appearance properties, which directly correspond to uniform values
   referenced in the shader program code.

*/

/*!
 * \ingroup a3mJusrAppear
 * @{
 */
/**
 * The Appearance includes blending factors, choice of shader and setting of
 * properties, and clipping rectangle, etc.
 */
public interface Appearance {
    /**
     * Culling modes as defined in a3m::PolygonMode::Culling enum
     *
     */
    public static class CullingMode {
        /** Back-facing polygons will not be drawn (default)*/
        public static final int BACK = 0;
        /** Front-facing polygons will not be drawn */
        public static final int FRONT = 1;
        /** Front and back facing polygons will not be drawn.
         * Note: Using this mode no polygons will be drawn. Points and lines will
         * still be drawn. */
        public static final int FRONT_AND_BACK = 2;
        /** All polygons will be drawn. */
        public static final int NONE = 3;
    }

    /**
     * Winding orders as defined in a3m::PolygonMode::Winding enum
     *
     */
    public static class WindingOrder {
        /** Faces whose vertices appear in a counter-clockwise
         * (anti-clockwise) order on-screen will be rendered (default). */
        public static final int CCW = 0;
        /** Faces whose vertices appear in a clockwise order on-screen will be
         * rendered. */
        public static final int CW = 1;
    }

    /**
     * Comparison functions for depth tests and stencil tests as defined in
     * a3m::CompositingMode::Function and a3m::Stencil::Function enums
     *
     */
    public static class Function {
        /** Always fails */
        public static final int NEVER = 0;
        /** Passes if ( ref &amp; mask ) &lt; ( value &amp; mask ) */
        public static final int LESS = 1;
        /** Passes if ( ref &amp; mask ) = ( value &amp; mask ) */
        public static final int EQUAL = 2;
        /** Passes if ( ref &amp; mask ) &lt;= ( value &amp; mask ) */
        public static final int LEQUAL = 3;
        /** Passes if ( ref &amp; mask ) &gt; ( value &amp; mask ) */
        public static final int GREATER = 4;
        /** Passes if ( ref &amp; mask ) != ( value &amp; mask ) */
        public static final int NOTEQUAL = 5;
        /** Passes if ( ref &amp; mask ) &gt;= ( value &amp; mask ) */
        public static final int GEQUAL = 6;
        /** Always passes */
        public static final int ALWAYS = 7;
    }

    /**
     * Blend functions as defined in a3m::Blender::BlendFunc enum
     *
     */
    public static class BlendFunction {
        /** The default additive blending mode */
        public static final int ADD = 0;
        /** Reversed subtractive blending */
        public static final int REVERSE_SUBTRACT = 1;
        /** Subtractive blending */
        public static final int SUBTRACT = 2;
    }

    /**
     * Blend factors as defined in a3m::Blender::BlendFactor enum
     *
     */
    public static class BlendFactor {
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by the alpha component of the constant blend colour.
         */
        public static final int CONSTANT_ALPHA = 0;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by the constant blend colour (or alpha).
         */
        public static final int CONSTANT_COLOUR = 1;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by the alpha component of the destination colour.
         */
        public static final int DST_ALPHA = 2;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by the destination colour and alpha.
         */
        public static final int DST_COLOUR = 3;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by one (that is = ; taken as such).
         */
        public static final int ONE = 4;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by one minus the alpha component of the constant blend
         * colour.
         */
        public static final int ONE_MINUS_CONSTANT_ALPHA = 5;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by one minus the constant blend colour (or
         * alpha).
         */
        public static final int ONE_MINUS_CONSTANT_COLOUR = 6;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by one minus the alpha component of the destination blend
         * colour.
         */
        public static final int ONE_MINUS_DST_ALPHA = 7;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by one minus the destination colour and
         * alpha.
         */
        public static final int ONE_MINUS_DST_COLOUR = 8;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by one minus the alpha component of the source colour.
         */
        public static final int ONE_MINUS_SRC_ALPHA = 9;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by one minus the source colour and alpha.
         */
        public static final int ONE_MINUS_SRC_COLOUR = 10;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by the alpha component of the source colour.
         */
        public static final int SRC_ALPHA = 11;
        /**
         * The source colour is to be multiplied by the source alpha, or one
         * minus the destination alpha = ; whichever is the smallest.
         */
        public static final int SRC_ALPHA_SATURATE = 12;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied component-wise by the source colour and alpha.
         */
        public static final int SRC_COLOUR = 13;
        /**
         * The source or destination colour (or equivalently alpha) is to be
         * multiplied by zero (that is, ignored).
         */
        public static final int ZERO = 14;
    }

    /**
     * Side of a face as defined in a3m::Stencil::Face enum
     *
     */
    public static class Face {
        /** Back-facing primitive */
        public static final int BACK = 0;
        /** Front-facing primitive */
        public static final int FRONT = 1;
    }

    /**
     * Stencil operations as defined in a3m::Stencil::Operation enum
     *
     */
    public static class Operation {
        /** Sets the stencil buffer value to zero */
        public static final int ZERO = 0;
        /** Leaves the existing stencil buffer contents unmodified */
        public static final int KEEP = 1;
        /** Copies the stencil reference value to the buffer */
        public static final int REPLACE = 2;
        /** Increments the stencil buffer value by one */
        public static final int INCREMENT = 3;
        /** Decrements the stencil buffer value by one */
        public static final int DECREMENT = 4;
        /** Performs a bitwise inversion to the stencil buffer */
        public static final int INVERT = 5;
        /** Increments the stencil buffer value by one, but wrap the value if
         * the stencil value overflows. */
        public static final int INCREMENT_WRAP = 6;
        /** Decrements the stencil buffer value by one, but wrap the value if
         * the stencil value underflows. */
        public static final int DECREMENT_WRAP = 7;
    }

    /**
     * Sets the name of the appearance.
     *
     * @param name appearance name
     */
    void setName(String name);

    /**
     * Returns the name of the appearance.
     *
     * @return appearance name
     */
    String getName();

    /**
     * Sets the shader program used by this appearance.
     *
     * @param program shader program
     */
    void setShaderProgram(ShaderProgram program);

    /**
     * Returns the shader program used by this appearance.
     *
     * @return shader program
     */
    ShaderProgram getShaderProgram();

    /**
     * Sets face culling mode.
     *
     * @param mode one of the CullingMode constants
     */
    void setCullingMode(int mode);

    /**
     * Returns face culling mode.
     *
     * @return one of the CullingMode constants
     */
    int getCullingMode();

    /**
     * Sets the width of rendered lines.
     *
     * @param width width of lines
     */
    void setLineWidth(float width);

    /**
     * Returns the width of rendered lines.
     *
     * @return width of lines
     */
    float getLineWidth();

    /**
     * Sets the face winding order.
     *
     * @param order one of the WindingOrder constants
     */
    void setWindingOrder(int order);

    /**
     * Returns the face winding order.
     *
     * @return one of the WindingOrder constants
     */
    int getWindingOrder();

    /**
     * Sets the constant blend colour.
     *
     * @param r red component
     * @param g green component
     * @param b blue component
     * @param a alpha component
     */
    void setBlendColour(float r, float g, float b, float a);

    /**
     * Returns the red component of the constant blend colour.
     *
     * @return red component
     */
    float getBlendColourR();

    /**
     * Returns the green component of the constant blend colour.
     *
     * @return green component
     */
    float getBlendColourG();

    /**
     * Returns the blue component of the constant blend colour.
     *
     * @return blue component
     */
    float getBlendColourB();

    /**
     * Returns the alpha component of the constant blend colour.
     *
     * @return alpha component
     */
    float getBlendColourA();

    /**
     * Sets the RGB and alpha blend factors.
     *
     * @param src Source RGB and alpha factor
     * @param dst Destination RGB and alpha factor
     */
    void setBlendFactors(int src, int dst);

    /**
     * Sets the RGB and alpha blend factors separately.
     *
     * @param srcRgb Source RGB factor
     * @param srcAlpha Source alpha factor
     * @param dstRgb Destination RGB factor
     * @param dstAlpha Destination alpha factor
     */
    void setBlendFactors(int srcRgb, int srcAlpha, int dstRgb, int dstAlpha);

    /**
     * Returns the source RGB factor.
     *
     * @return Blend factor
     */
    int getBlendFactorSrcRgb();

    /**
     * Returns the source alpha factor.
     *
     * @return Blend factor
     */
    int getBlendFactorSrcAlpha();

    /**
     * Returns the destination RGB factor.
     *
     * @return Blend factor
     */
    int getBlendFactorDstRgb();

    /**
     * Returns the destination alpha factor.
     *
     * @return Blend factor
     */
    int getBlendFactorDstAlpha();

    /**
     * Sets RGB and alpha blend functions.
     *
     * @param rgb RGB blend function
     * @param alpha Alpha blend function
     */
    void setBlendFunctions(int rgb, int alpha);

    /**
     * Returns the RGB blend function.
     *
     * @return RGB blend function
     */
    int getBlendFunctionRgb();

    /**
     * Returns the alpha blend function.
     *
     * @return Alpha blend function
     */
    int getBlendFunctionAlpha();

    /**
     * Sets whether to force transparency to be turned off.
     *
     * @param flag True to force transparency off
     */
    void setForceOpaque(boolean flag);

    /**
     * Returns whether transparency is forced off.
     *
     * @return True if transparency is forced off
     */
    boolean getForceOpaque();

    /**
     * Returns whether the appearance is opaque.
     * This takes the force opaque setting into account.
     *
     * @return True if appearance is opaque
     */
    boolean isOpaque();

    /**
     * Sets the colour write mask for each colour channel.
     *
     * @param r Red channel mask flag
     * @param g Green channel mask flag
     * @param b Blue channel mask flag
     * @param a Alpha channel mask flag
     */
    void setColourMask(boolean r, boolean g, boolean b, boolean a);

    /**
     * Returns the write mask flag for the red channel.
     *
     * @return Write mask flag
     */
    boolean getColourMaskR();

    /**
     * Returns the write mask flag for the green channel.
     *
     * @return Write mask flag
     */
    boolean getColourMaskG();

    /**
     * Returns the write mask flag for the blue channel.
     *
     * @return Write mask flag
     */
    boolean getColourMaskB();

    /**
     * Returns the write mask flag for the alpha channel.
     *
     * @return Write mask flag
     */
    boolean getColourMaskA();

    /**
     * Sets the slope-dependent depth offset factor.
     *
     * @param factor Depth offset factor
     */
    void setDepthOffsetFactor(float factor);

    /**
     * Returns the slope-dependent depth offset factor.
     *
     * @return Depth offset factor
     */
    float getDepthOffsetFactor();

    /**
     * Sets the constant depth offset value.
     *
     * @param units Depth offset value
     */
    void setDepthOffsetUnits(float units);

    /**
     * Returns the constant depth offset value.
     *
     * @return Depth offset value
     */
    float getDepthOffsetUnits();

    /**
     * Sets the depth test function.
     *
     * @param function Depth test function
     */
    void setDepthTestFunction(int function);

    /**
     * Returns the depth test function.
     *
     * @return Depth test function
     */
    int getDepthTestFunction();

    /**
     * Sets whether a depth test is performed.
     *
     * @param flag Depth test enable flag
     */
    void setDepthTestEnabled(boolean flag);

    /**
     * Returns whether a depth test is performed.
     *
     * @return Depth test enable flag
     */
    boolean isDepthTestEnabled();

    /**
     * Sets whether to write to the depth buffer.
     *
     * @param flag Depth write enable flag
     */
    void setDepthWriteEnabled(boolean flag);

    /**
     * Returns whether the depth buffer is written to.
     *
     * @return Depth write enable flag
     */
    boolean isDepthWriteEnabled();

    /**
     * Enables or disables a scissor (clipping area) test.
     * If enabled, a scissor test is performs to determine whether each pixel
     * lies within a scissor (clipping) rectangle.  The buffer is written to
     * only if this test is true.
     * @see #setScissorBox(int,int,int,int)
     *
     * @param flag Boolean flag
     */
    void setScissorTestEnabled(boolean flag);

    /**
     * Returns whether a scissor test is enabled.
     *
     * @return Boolean flag
     */
    boolean isScissorTestEnabled();

    /**
     * Sets the scissor (clipping) rectangle.
     * The clipping rectangle is the area used for the scissor test if it is
     * enabled.
     * @see #setScissorTestEnabled(boolean)
     *
     * @param left The left edge of the rectangle
     * @param bottom The bottom edge of the rectangle
     * @param width The width of the rectangle
     * @param height The height of the rectangle
     */
    void setScissorBox(int left, int bottom, int width, int height);

    /**
     * Returns the left edge of the clipping rectangle.
     *
     * @return Edge of rectangle in pixels
     */
    int getScissorBoxLeft();

    /**
     * Returns the bottom edge of the clipping rectangle.
     *
     * @return Edge of rectangle in pixels
     */
    int getScissorBoxBottom();

    /**
     * Returns the width of the clipping rectangle.
     *
     * @return Width of rectangle in pixels
     */
    int getScissorBoxWidth();

    /**
     * Returns the height of the clipping rectangle.
     *
     * @return Height of rectangle in pixels
     */
    int getScissorBoxHeight();

    /**
     * Sets the stencil function, reference value, and mask.
     * These can be separately for front-facing and back-facing primitives, or
     * for both at the same time.
     * @param face The face for which to set the parameters
     * @param function The stencil function
     * @param reference The stencil test reference value
     * @param mask The stencil test bitmask
     */
    void setStencilFunction(int face, int function, int reference, int mask);

    /**
     * Returns the stencil function for the given face.
     *
     * @param face Queried face
     * @return Function
     */
    int getStencilFunction(int face);

    /**
     * Returns the stencil reference for the given face.
     *
     * @param face Queried face
     * @return Reference value
     */
    int getStencilReference(int face);

    /**
     * Returns the stencil function for the given face.
     *
     * @param face Queried face
     * @return Mask
     */
    int getStencilReferenceMask(int face);

    /**
     * Sets the stencil operators to execute depending on the outcome of the
     * depth and stencil tests.
     * The set of operators can be set for font-facing, back-facing, or both
     * kinds of primitives at the same time.
     * @param face The face for which to set the parameters
     * @param stencilFail The operator to execute if both the stencil test
     *                    fails
     * @param stencilPassDepthFail The operator to execute if the stencil test
     *                             pass but the depth test fails
     * @param stencilPassDepthPass The operator to execute if both the stencil
     *                             test and the depth test pass
     */
    void setStencilOperations(
        int face,
        int stencilFail,
        int stencilPassDepthFail,
        int stencilPassDepthPass);

    /**
     * Returns the stencil fail operation for the specified face.
     *
     * @param face Queried face
     * @return Operation
     */
    int getStencilFail(int face);

    /**
     * Returns the stencil pass depth fail operation for the specified face.
     *
     * @param face Queried face
     * @return Operation
     */
    int getStencilPassDepthFail(int face);

    /**
     * Returns the stencil pass depth pass operation for the specified face.
     *
     * @param face Queried face
     * @return Operation
     */
    int getStencilPassDepthPass(int face);

    /**
     * Sets the stencil buffer write enable mask for either or both of
     * back-facing and front-facing primitives.
     * If a particular bit in the write mask is set, the corresponding
     * bitplane in the stencil buffer is enabled for writing. A zero bit in
     * the mask indicates that the corresponding bitplane will not be written
     * to. For a stencil buffer with s bitplanes, only the s low-order bits of
     * the given mask are effective; the higher-order bits are ignored and can
     * have any value.
     *
     * @param face Queried face
     * @param mask Stencil mask
     */
    void setStencilMask(int face, int mask);

    /**
     * Returns the stencil write mask for a specific face.
     *
     * @param face Queried face
     * @return Mask
     */
    int getStencilMask(int face);

    /**
     * Checks whether a named property exists.
     *
     * @param name Property name
     * @return True if the property does exist
     */
    boolean propertyExists(String name);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param value Value to set
     */
    void setBoolean(String name, boolean value);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param value Value to set
     * @param i Index into the property array
     */
    void setBoolean(String name, boolean value, int i);

    /**
     * Returns a named appearance property.
     *
     * @param name Property name
     * @return Property value
     */
    boolean getBoolean(String name);

    /**
     * Returns a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Property value
     */
    boolean getBoolean(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param value Value to set
     */
    void setInt(String name, int value);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param value Value to set
     * @param i Index into the property array
     */
    void setInt(String name, int value, int i);

    /**
     * Returns a named appearance property.
     *
     * @param name Property name
     * @return Property value
     */
    int getInt(String name);

    /**
     * Returns a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Property value
     */
    int getInt(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param value Value to set
     */
    void setFloat(String name, float value);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param value Value to set
     * @param i Index into the property array
     */
    void setFloat(String name, float value, int i);

    /**
     * Returns a named appearance property.
     *
     * @param name Property name
     * @return Property value
     */
    float getFloat(String name);

    /**
     * Returns a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Property value
     */
    float getFloat(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     */
    void setVector2b(String name, boolean x, boolean y);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param i Index into the property array
     */
    void setVector2b(String name, boolean x, boolean y, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    boolean getVector2bX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    boolean getVector2bX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    boolean getVector2bY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    boolean getVector2bY(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     */
    void setVector3b(String name, boolean x, boolean y, boolean z);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param i Index into the property array
     */
    void setVector3b(String name, boolean x, boolean y, boolean z, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    boolean getVector3bX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    boolean getVector3bX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    boolean getVector3bY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    boolean getVector3bY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    boolean getVector3bZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    boolean getVector3bZ(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     */
    void setVector4b(String name, boolean x, boolean y, boolean z, boolean w);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     * @param i Index into the property array
     */
    void setVector4b(
        String name, boolean x, boolean y, boolean z, boolean w, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    boolean getVector4bX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    boolean getVector4bX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    boolean getVector4bY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    boolean getVector4bY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    boolean getVector4bZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    boolean getVector4bZ(String name, int i);

    /**
     * Returns the W component of a named appearance property.
     *
     * @param name Property name
     * @return W component
     */
    boolean getVector4bW(String name);

    /**
     * Returns the W component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return W component
     */
    boolean getVector4bW(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     */
    void setVector2i(String name, int x, int y);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param i Index into the property array
     */
    void setVector2i(String name, int x, int y, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    int getVector2iX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    int getVector2iX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    int getVector2iY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    int getVector2iY(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     */
    void setVector3i(String name, int x, int y, int z);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param i Index into the property array
     */
    void setVector3i(String name, int x, int y, int z, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    int getVector3iX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    int getVector3iX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    int getVector3iY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    int getVector3iY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    int getVector3iZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    int getVector3iZ(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     */
    void setVector4i(String name, int x, int y, int z, int w);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     * @param i Index into the property array
     */
    void setVector4i(String name, int x, int y, int z, int w, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    int getVector4iX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    int getVector4iX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    int getVector4iY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    int getVector4iY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    int getVector4iZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    int getVector4iZ(String name, int i);

    /**
     * Returns the W component of a named appearance property.
     *
     * @param name Property name
     * @return W component
     */
    int getVector4iW(String name);

    /**
     * Returns the W component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return W component
     */
    int getVector4iW(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     */
    void setVector2f(String name, float x, float y);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param i Index into the property array
     */
    void setVector2f(String name, float x, float y, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    float getVector2fX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    float getVector2fX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    float getVector2fY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    float getVector2fY(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     */
    void setVector3f(String name, float x, float y, float z);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param i Index into the property array
     */
    void setVector3f(String name, float x, float y, float z, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    float getVector3fX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    float getVector3fX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    float getVector3fY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    float getVector3fY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    float getVector3fZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    float getVector3fZ(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     */
    void setVector4f(String name, float x, float y, float z, float w);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param x X component of value to set
     * @param y Y component of value to set
     * @param z Z component of value to set
     * @param w W component of value to set
     * @param i Index into the property array
     */
    void setVector4f(String name, float x, float y, float z, float w, int i);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param mtx The Matrix to set
     */
    void setMatrix4f(String name, float[] mtx);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param mtx The Matrix to set
     * @param i Index into the property array
     */
    void setMatrix4f(String name, float[] mtx, int i);

    /**
     * Returns the X component of a named appearance property.
     *
     * @param name Property name
     * @return X component
     */
    float getVector4fX(String name);

    /**
     * Returns the X component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return X component
     */
    float getVector4fX(String name, int i);

    /**
     * Returns the Y component of a named appearance property.
     *
     * @param name Property name
     * @return Y component
     */
    float getVector4fY(String name);

    /**
     * Returns the Y component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Y component
     */
    float getVector4fY(String name, int i);

    /**
     * Returns the Z component of a named appearance property.
     *
     * @param name Property name
     * @return Z component
     */
    float getVector4fZ(String name);

    /**
     * Returns the Z component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Z component
     */
    float getVector4fZ(String name, int i);

    /**
     * Returns the W component of a named appearance property.
     *
     * @param name Property name
     * @return W component
     */
    float getVector4fW(String name);

    /**
     * Returns the W component of a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return W component
     */
    float getVector4fW(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param value Value to set
     */
    void setTexture2D(String name, Texture2D value);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param value Value to set
     * @param i Index into the property array
     */
    void setTexture2D(String name, Texture2D value, int i);

    /**
     * Returns a named appearance property.
     *
     * @param name Property name
     * @return Property value
     */
    Texture2D getTexture2D(String name);

    /**
     * Returns a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Property value
     */
    Texture2D getTexture2D(String name, int i);

    /**
     * Sets a named appearance property.
     *
     * @param name Property name
     * @param value Value to set
     */
    void setTextureCube(String name, TextureCube value);

    /**
     * Sets a named appearance array property.
     *
     * @param name Property name
     * @param value Value to set
     * @param i Index into the property array
     */
    void setTextureCube(String name, TextureCube value, int i);

    /**
     * Returns a named appearance property.
     *
     * @param name Property name
     * @return Property value
     */
    TextureCube getTextureCube(String name);

    /**
     * Returns a named appearance array property.
     *
     * @param name Property name
     * @param i Index into the property array
     * @return Property value
     */
    TextureCube getTextureCube(String name, int i);
}

/*! @} */
