/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Version class
 *
 */
#pragma once
#ifndef A3M_VERSION_H
#define A3M_VERSION_H

#include <string>
#include <a3m/base_types.h>

namespace a3m
{

  /**
   * A version number with major and minor numeric parts, and an additional
   * information field.
   */
  class Version
  {
  public:
    /**
     * Constructor.
     */
    Version(
      A3M_INT32 major = 0,
      /**< Major version number */
      A3M_INT32 minor = 0,
      /**< Minor version number */
      A3M_INT32 patch = 0,
      /**< Patch/bugfix version number */
      A3M_CHAR8 const* extra = ""
                               /**< Extra information string */);

    /**
     * Returns the major version number.
     * \return Major version number
     */
    A3M_INT32 getMajor() const;

    /**
     * Returns the minor version number.
     * \return Minor version number
     */
    A3M_INT32 getMinor() const;

    /**
     * Returns the patch version number.
     * \return Patch version number
     */
    A3M_INT32 getPatch() const;

    /**
     * Returns the extra information field.
     * \return Extra information string
     */
    A3M_CHAR8 const* getExtra() const;

  private:
    A3M_INT32 m_major;
    A3M_INT32 m_minor;
    A3M_INT32 m_patch;
    std::string m_extra;
  };

  /**
   * Version equality comparison operator.
   * Two versions will test equal each of their respective version numbers are
   * equal.
   * \return A3M_TRUE if the versions are equal
   */
  A3M_BOOL operator==(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

  /**
   * Version inequality comparison operator.
   * Two versions will test not equal each of their respective version numbers
   * are not equal.
   * \return A3M_TRUE if the versions are not equal
   */
  A3M_BOOL operator!=(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

  /**
   * Version less-than comparison operator.
   * Version number comparison is done by comparing first the major, then the
   * minor, then the patch number, each time continuing the check only if the
   * numbers are equal.
   * \return A3M_TRUE if left-hand operand is less than the right
   */
  A3M_BOOL operator<(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

  /**
   * Version greater-than comparison operator.
   * \copydetail operator<(Version const&, Version const&)
   * \return A3M_TRUE if left-hand operand is greater than the right
   */
  A3M_BOOL operator>(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

  /**
   * Version less-than or equal to comparison operator.
   * \copydetail operator<(Version const&, Version const&)
   * \return A3M_TRUE if left-hand operand is less than or equal to the right
   */
  A3M_BOOL operator<=(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

  /**
   * Version greater-than or equal to comparison operator.
   * \copydetail operator<(Version const&, Version const&)
   * \return A3M_TRUE if left-hand operand is greater than or equal to the right
   */
  A3M_BOOL operator>=(
    Version const& lhs,
    /**< Left-hand operand */
    Version const& rhs
    /**< Right-hand operand */);

} // namespace a3m

#endif // A3M_VERSION_H
