/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A vertex shader for fog effect
 *
 */

precision mediump float;
precision mediump int;

/* Transformation uniforms */
uniform float u_time;

attribute vec4 a_position;
attribute vec2 a_uv0;


// Varyings
varying vec2 v_screen;

varying vec2 v_texCoord0;
varying vec2 v_texCoord1;
varying vec2 v_texCoord2;
varying vec2 v_texCoord3;

// The animation must loop every minute (because u_time is time mod 60)
// so each multipier must be an integer multiple of 2 * pi / 60
const float sinMult1 = 0.10472 * 10.0;
const float sinMult2 = 0.10472 * 7.0;
const float sinMult3 = 0.10472 * 3.0;

// These multipiers must be an integer multiple of 1 / 60
const float horizMult1 = 0.016666 * 10.0;
const float horizMult2 = 0.016666 * 14.0;
const float horizMult3 = 0.016666 * 17.0;

//------------------------------------------------------------------------------
void main(void)
{
    gl_Position = vec4( a_uv0.x * 2.0 - 1.0, a_uv0.y * -1.0 + 0.6, 0.0, 1.0 );
    v_texCoord0 = a_uv0.xy;
    v_texCoord1 = a_uv0.xy + sin( u_time * sinMult1 + 0.1 ) * vec2( 0.1, 0.1 ) +
                  vec2( u_time * horizMult1 + 0.4, 0.0 );
    v_texCoord2 = a_uv0.xy + sin( u_time * sinMult2 + 0.2 ) * vec2( -0.2, 0.2 ) +
                  vec2( u_time * horizMult2 + 0.5, 0.0 );
    v_texCoord3 = a_uv0.xy + sin( u_time * sinMult3 + 0.3 ) * vec2( 0.2, -0.2 ) +
                  vec2( u_time * horizMult3 + 0.6, 0.0 );
    v_screen = v_texCoord0 * 2.0 - 1.0;
    v_texCoord1 *= 2.0;
}


