/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A fragment shader for text fade quads
 *
 */

precision mediump float;

/* Material uniforms */
uniform sampler2D u_m_diffuseTexture;
uniform vec4 u_m_diffuseColour;
uniform float u_m_opacity;
uniform vec2 u_m_fadeoutCoord;

varying vec2 v_texcoord;      // Adjusted texture coordinate

void main()
{
  gl_FragColor = u_m_diffuseColour *
    texture2D( u_m_diffuseTexture, v_texcoord );
  gl_FragColor.a *= u_m_opacity;

  /**
   * Handle long string ellipsize by fade out effect.
   * u_m_fadeoutCoord.x: the fade out start point.
   * u_m_fadeoutCoord.y: the fade out end point.
   * The alpha is linear interpolation(1 to 0) between
   * fade out start/ end point.
   * For better performance, we make u_m_fadeoutCoord.y =
   * 1 / ( fadeoutEnd - fadeoutStart ) in ngin3D.
   */
  if (v_texcoord.x > u_m_fadeoutCoord.x) {
    gl_FragColor.a *=
    (1.0 - (v_texcoord.x - u_m_fadeoutCoord.x) * u_m_fadeoutCoord.y);
  }
}
