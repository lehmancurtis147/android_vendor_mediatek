/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A simple vertex shader example
 *
 */

precision mediump float;
precision mediump int;

uniform sampler2D u_m_diffuseTexture;

// Varyings

varying vec4 v_colour;
varying vec2 v_texCoord;


//------------------------------------------------------------------------------
void main(void)
{
    vec4 texture = texture2D( u_m_diffuseTexture, v_texCoord );
    gl_FragColor = texture * v_colour;
}
