/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * A vertex shader for text fade quads.
 *
 */
/* Transformation uniforms */
uniform mat4 u_t_modelViewProjection;
uniform vec4 u_uvCoordOffsetScale;

attribute vec4 a_position;    // Vertex position (model space)
attribute vec2 a_uv0;         // Texture coordinate

varying vec2 v_texcoord;      // Adjusted texture coordinate

void main()
{
  gl_Position = u_t_modelViewProjection * a_position;
  v_texcoord = a_uv0 * u_uvCoordOffsetScale.zw + u_uvCoordOffsetScale.xy;
}
