/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * GLO file class
 *
 */
#pragma once
#ifndef A3M_GLO_FILE_H
#define A3M_GLO_FILE_H

#include <a3m/animation.h>          /* for AnimationController */
#include <a3m/pointer.h>            /* for SharedPtr */
#include <a3m/scenenode.h>          /* for SceneNode */

namespace a3m
{
  class AssetCachePool;

  /** \todo Integrate into documentation (this is not currently in any groups) */

  /** Glo class.
   * Contains the scene graph and animation loaded from a Glo file.
   */
  class Glo : public Shared
  {
  public:
    A3M_NAME_SHARED_CLASS(Glo)

    /** Smart pointer type for this class */
    typedef SharedPtr<Glo> Ptr;

    /** Sets the root scene graph node.
     */
    void setSceneNode(SceneNode::Ptr const& scene /**< Scene graph node */)
    {
      m_sceneNode = scene;
    }

    /** Returns the root scene graph node.
     * \return Root node
     */
    SceneNode::Ptr const& getSceneNode() const
    {
      return m_sceneNode;
    }

    /** Returns the Glo animation.
     * \return Animation
     */
    void setAnimation(
      AnimationController::Ptr const& animation /**< Animation */)
    {
      m_animation = animation;
    }

    /** Returns the Glo animation.
     * \return Animation
     */
    AnimationController::Ptr const& getAnimation() const
    {
      return m_animation;
    }

  private:
    SceneNode::Ptr m_sceneNode; /**< root of scene created from file */
    AnimationController::Ptr m_animation; /**< animation created from file */
  };

  /** Load GLO File.
   * The file specified by fileName is loaded. The contents of the file are
   * returned in the GLO node and animation members of a GLO struct. Either
   * of these may be null if the file contains only geometry or only animation.
   * The SceneNode contained in the GLO structure is NOT added to the scene.
   * The scene parameter exists to link any animation in the file to existing
   * scene nodes.
   *
   * \return GLO struct representing the contents of the file.
   */
  Glo::Ptr loadGloFile(
    /** Pool of asset caches to use to create the assets (textures, shader
     * programs, etc.) */
    AssetCachePool& pool,
    /** Root of scene graph to use when linking animations to scene nodes */
    SceneNode::Ptr const& scene,
    /** Name of file to load */
    A3M_CHAR8 const* fileName );
}


#endif // A3M_GLO_FILE_H
