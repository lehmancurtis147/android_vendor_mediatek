/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Solid SceneNode class
 *
 */
#pragma once
#ifndef A3M_SOLID_H
#define A3M_SOLID_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/scenenode.h>       /* for SceneNode (base class)               */
#include <a3m/mesh.h>            /* for Mesh class                           */
#include <a3m/appearance.h>      /* for Appearance class                     */

namespace a3m
{
  class RenderContext;

  /** \ingroup a3mScenenodes
   * A Solid object represents a geometrical entity within a scene
   */
  class Solid : public SceneNode
  {
  public:
    A3M_NAME_SHARED_CLASS( Solid )

    /** Smart pointer type for this class */
    typedef SharedPtr< Solid > Ptr;

    /** Type ID for this class */
    static A3M_UINT32 const NODE_TYPE;

    /** Constructor.
     * Constructs an empty solid node with an identity local transformation.
     */
    Solid();

    /** Solid constructor
     * Constructs a Solid from a Mesh and an Appearance
     */
    Solid( Mesh::Ptr const& mesh, /**< mesh object */
           Appearance::Ptr const& appearance /**< appearance object */ );

    // Override
    virtual A3M_UINT32 getType() const { return NODE_TYPE; }

    /** Get Mesh
     * \return The mesh used by this object
     */
    Mesh::Ptr const& getMesh() const { return m_mesh; }

    /** Set Mesh.
     * Changes the Mesh used by this MeshInstance.
     */
    void setMesh( Mesh::Ptr const& mesh /**< new mesh object */ )
    {
      m_mesh = mesh;
    }

    /** Get Appearance
     * \return The appearance used by this object
     */
    Appearance::Ptr const& getAppearance() const { return m_appearance; }

    /** Set Appearance.
     * Changes the Appearance used by this MeshInstance.
     */
    void setAppearance( Appearance::Ptr const& appearance /**< new Appearance */ )
    {
      m_appearance = appearance;
    }

    /** Sets the number of joints which can influence any on vertex at a time.
     */
    void setJointsPerVertex(A3M_INT32 count /**< Number of joints */);

    /** Returns the number of joints per vertex. */
    A3M_INT32 getJointsPerVertex() const
    {
      return m_jointsPerVertex;
    }

    /** Sets the transformation of the Solid at binding time.
     */
    void setBindShapeTransform(
      Matrix4f const& transform /**< Bind shape transform */);

    /** Returns the bind shape transform.
     * \return Bind shape transform
     */
    Matrix4f const& getBindShapeTransform() const
    {
      return m_bindShapeTransform;
    }

    /** Sets a SceneNode which represents a joint within the Solid.
     * A solid whose mesh contains rigging data (joint indices and weights)
     * requires scene nodes to represent each of the joints.  Use this function
     * to associate scene nodes with joints.
     *
     * The joints rigging indices refer to the Nth joint attached to the solid.
     */
    void attachJoint(
      /**< Index of joint to attach */
      SceneNode::Ptr const& sceneNode,
      /**< Scene node representing the joint */
      Matrix4f const& inverseBindPoseTransform
      /**< Transformation of the joint at binding time */);

    /**
     * Renders the Solid to the screen.
     */
    void draw(RenderContext& context /**< Render context */);

    /**
     * Renders the Solid to the screen.
     */
    void draw(RenderContext& context /**< Render context */,
              Appearance& appearance /**< Appearance to use */);

    /** Accept a scene node visitor.
     */
    virtual void accept( SceneNodeVisitor& visitor /**< visitor */ );

  private:
    /*
     * Contains data associated with a single joint-solid attachment.
     */
    struct JointAttachment
    {
      // Binding information
      SceneNode::Ptr sceneNode;
      Matrix4f inverseBindPoseTransform;

      // Cached matrices (to improve performance)
      Matrix4f jointTransform;
      Matrix4f poseTransform;
      Matrix4f worldTransform;
    };

    typedef std::vector<JointAttachment> JointAttachmentVector;

    /* Calculates the model- to joint-space transform for a given attachment.
     */
    void calculateJointTransform(JointAttachment& attachment /* Attachment */);

    Mesh::Ptr m_mesh;
    Appearance::Ptr m_appearance;
    JointAttachmentVector m_jointAttachments;
    Matrix4f m_bindShapeTransform;
    A3M_INT32 m_jointsPerVertex;
  };
} /* namespace a3m */

#endif /* A3M_SOLID_H */
