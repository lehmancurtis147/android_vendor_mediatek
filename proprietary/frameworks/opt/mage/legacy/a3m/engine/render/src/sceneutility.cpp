/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Scene Node Utility Function Implementations
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/assetcachepool.h>   /* for AssetCachePool */
#include <a3m/appearance.h>       /* for Appearance */
#include <a3m/meshutility.h>      /* for mesh primitive creation functions */
#include <a3m/detail/resourcecache.h>    /* for ResourceCache */
#include <a3m/sceneutility.h>     /* Declerations of functions defined here */
#include <a3m/shaderprogram.h>    /* ShaderProgram class */
#include <a3m/scenenodevisitor.h> /* For SceneNodeVisitor interface */

using namespace a3m;

namespace
{
  class ShaderProgramSetter : public SceneNodeVisitor
  {
  public:
    ShaderProgramSetter( ShaderProgram::Ptr program ) : m_program( program ) {}

    void visit( Solid* solid )
    {
      solid->getAppearance()->setShaderProgram( m_program );
    }
  private:
    ShaderProgram::Ptr m_program;
  };

  class AppearanceSetter : public SceneNodeVisitor
  {
  public:
    AppearanceSetter( Appearance::Ptr appearance ) : m_appearance( appearance )
    {
    }

    void visit( Solid* solid )
    {
      solid->setAppearance( m_appearance );
    }
  private:
    Appearance::Ptr m_appearance;
  };

  // Creates a solid out of the given mesh and a default appearance
  Solid::Ptr createSolid(AssetCachePool& pool, Mesh::Ptr mesh)
  {
    Appearance::Ptr appearance = loadAppearance(pool, "a3m#default_pvl.ap");
    Solid::Ptr solid(new Solid(mesh, appearance));

    return solid;
  }

  // Structure used to store the derived flag state when recursing up or down
  // a scene node hierarchy.
  struct FlagState
  {
    FlagState(FlagMask const& mask) :
      state(A3M_FALSE),
      locked(A3M_FALSE),
      flags(),
      allDefaultFalse(!(mask.getFlags() & mask.getDefaultState()).any())
    {
    }

    A3M_BOOL state;
    A3M_BOOL locked;
    FlagSet flags;
    A3M_BOOL const allDefaultFalse;
  };

  // In order to get the derived flag of a node, the state of any flags must
  // filter down from parent nodes, but only if the parent flags are in their
  // non-default state (having the default state filter down would be
  // pointless).  To return TRUE, all flags defined by the mask must match the
  // derived flag values.  Since flags have a recursive effect when they are set
  // to their non-default value, irrespective of whether their default value is
  // TRUE or FALSE, we can only stop iterating when a single flag is non-default
  // and FALSE (all flags must be set to return TRUE) in which case FALSE will
  // be returned, or when all the flags have become non-default (this will only
  // happen if all flags are non-default and TRUE) in which case TRUE will be
  // returned.  If the root of the scene graph is reached (the node with no
  // parent) then the derived flags must be evaluated to determine the return
  // value.  We can
  A3M_BOOL combineFlags(SceneNode const& node, FlagMask const& filterMask,
                        FlagMask const& recursiveMask, FlagState& state)
  {
    // The inherited flags are only combined where recursion is set
    state.flags |= (node.getFlags() & recursiveMask.getFlags());
    A3M_BOOL newState = state.flags.get(recursiveMask);

    // If state has become FALSE, there is no chance of it turning back to
    // TRUE so we can return.
    A3M_BOOL stateLockedFalse = (!newState && state.state);

    // If all flags are FALSE by default, and the state is TRUE, then all flags
    // are non-default, and there is no chance of turning back to TRUE, set we
    // can return.
    A3M_BOOL stateLockedTrue = (newState && state.allDefaultFalse);

    // Pass state and locked flags back out.
    state.state = newState;
    state.locked = stateLockedFalse || stateLockedTrue;

    // Combine the current node's flags with the inherited flags.
    FlagSet thisFlags = state.flags | node.getFlags();
    // Will this node be filtered?
    A3M_BOOL thisState = thisFlags.get(filterMask);

    return thisState;
  }

  // Collects all child scene nodes using the visitor pattern.
  void visitScene(SceneNodeVisitor& visitor, SceneNode& node,
                  FlagMask const& filterMask, FlagMask const& recursiveMask,
                  FlagState state)
  {
    A3M_BOOL nodeState = combineFlags(node, filterMask, recursiveMask, state);

    if (state.locked)
    {
      // If state is locked to TRUE, we collect all child scene nodes
      // unconditionally.  If locked to FALSE, we stop collecting scene nodes
      // completely.
      if (state.state)
      {
        visitScene(visitor, node);
      }
      else if (nodeState)
      {
        node.accept(visitor);
      }
    }
    else
    {
      if (nodeState)
      {
        node.accept(visitor);
      }

      // If state is not locked, we must continue collecting nodes conditionally
      for (A3M_UINT32 i = 0; i < node.getChildCount(); ++i)
      {
        SceneNode::Ptr childNode = node.getChild(i);
        visitScene(visitor, *childNode, filterMask, recursiveMask, state);
      }
    }
  }

  A3M_BOOL getDerivedFlags(SceneNode const& node, FlagMask const& mask,
                           FlagState state)
  {
    // Recurse up to the top of the scene graph before checking the state of
    // this node, because we must accumulate state in a top-down manner.
    a3m::SceneNode* parentNode = node.getParent().get();
    if (parentNode)
    {
      getDerivedFlags(*parentNode, mask, state);
    }

    // If the state has been locked higher up the scene graph, then this node
    // must also be of that state.  Otherwise, we calculate the state of this
    // node.
    A3M_BOOL nodeState = state.state;

    if (!state.locked)
    {
      nodeState = combineFlags(node, mask, mask, state);
    }

    return nodeState;
  }
} // namespace

namespace a3m
{
  /*
   * Set Shader Program for Node.
   */
  void setShaderProgram( SceneNode& node, /**< Scene node to change */
                         SharedPtr< ShaderProgram > const& program
                         /**< ShaderProgram to use for all descendent Solids */
                       )
  {
    ShaderProgramSetter setter( program );
    node.accept( setter );
  }

  /*
   * Set Appearance for Node.
   */
  void setAppearance( SceneNode& node, /**< Scene node to change */
                      SharedPtr< Appearance > const& appearance
                      /**< Appearance to use for all descendent Solids */
                    )
  {
    AppearanceSetter setter( appearance );
    node.accept( setter );
  }

  void point(SceneNode& node,
             Vector3f const& worldForward, Vector3f const& worldUp,
             Vector3f const& localForward, Vector3f const& localUp)
  {
    static A3M_FLOAT const DOT_THRESHOLD = 0.9995f;

    // Get the node's inverse world transform without the previous rotation
    node.setRotation(Quaternionf::IDENTITY);
    Matrix4f iwt = inverse(node.getWorldTransform());

    // Transform the forward-vector into node-space
    Vector3f targetForward =
      normalize(Vector3f(iwt * Vector4f(worldForward, 0.0f)));

    // Get the rotation required to point along the target vector
    Quaternionf rotation = toQuaternion(localForward, targetForward);

    // Transform the up-vectors into node-space
    Vector3f targetUp =
      normalize(Vector3f(iwt * Vector4f(worldUp, 0.0f)));
    Vector3f currentUp =
      normalize(Vector3f(toMatrix4(rotation) * Vector4f(localUp, 0.0)));

    // Project the target up-vector onto the plane perpendicular to the target
    // forward-vector, and align the local up-vector with the projected
    // up-vector by rolling around the target forward-vector
    A3M_FLOAT upDotUp = dot(targetForward, targetUp);

    if (upDotUp < DOT_THRESHOLD)
    {
      Vector3f projectedUp = normalize(targetUp - upDotUp * targetForward);
      rotation = toQuaternion(currentUp, projectedUp, targetForward) * rotation;
    }

    node.setRotation(rotation);
  }

  Solid::Ptr createSquare(AssetCachePool& assetCachePool,
                          Vector2f const& uvScale)
  {
    Mesh::Ptr mesh = createSquareMesh(*assetCachePool.meshCache(), uvScale);
    Solid::Ptr solid = createSolid(assetCachePool, mesh);
    return solid;
  }

  Solid::Ptr createCube(AssetCachePool& assetCachePool,
                        Vector2f const& uvScale)
  {
    Mesh::Ptr mesh = createCubeMesh(*assetCachePool.meshCache(), uvScale);
    Solid::Ptr solid = createSolid(assetCachePool, mesh);
    return solid;
  }

  Solid::Ptr createSphere(AssetCachePool& assetCachePool,
                          A3M_UINT32 segmentCount,
                          A3M_UINT32 wedgeCount,
                          Vector2f const& uvScale)
  {
    Mesh::Ptr mesh = createSphereMesh(*assetCachePool.meshCache(),
                                      segmentCount, wedgeCount, uvScale);
    Solid::Ptr solid = createSolid(assetCachePool, mesh);
    return solid;
  }

  void visitScene(SceneNodeVisitor& visitor, SceneNode& node)
  {
    node.accept(visitor);

    for (A3M_UINT32 i = 0; i < node.getChildCount(); ++i)
    {
      SceneNode::Ptr childNode = node.getChild(i);
      visitScene(visitor, *childNode);
    }
  }

  void visitScene(SceneNodeVisitor& visitor, SceneNode& node,
                  FlagMask const& filterFlags, FlagMask const& recursiveFlags)
  {
    // Check whether any flags were passed
    if (filterFlags.getFlags().any())
    {
      ::visitScene(visitor, node, filterFlags, recursiveFlags,
                   FlagState(recursiveFlags));
    }
    else
    {
      visitScene(visitor, node);
    }
  }

  void setFlags( SceneNode& node, FlagMask const& mask, A3M_BOOL state )
  {
    FlagSet flags = node.getFlags();
    flags.set( mask, state );
    node.setFlags( flags );
  }

  A3M_BOOL getDerivedFlags(SceneNode const& node, FlagMask const& mask)
  {
    return ::getDerivedFlags(node, mask, FlagState(mask));
  }

} /* namespace a3m */
