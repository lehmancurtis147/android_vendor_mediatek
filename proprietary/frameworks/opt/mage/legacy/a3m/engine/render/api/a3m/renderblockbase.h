/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 *  RenderBlock abstract base class
 */
#pragma once
#ifndef A3M_RENDERBLOCKBASE_H
#define A3M_RENDERBLOCKBASE_H

#include <a3m/pointer.h>       /* for SharedPtr                     */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \defgroup a3mRenderBlock A3M Render Block
   * \ingroup  a3mRefRender
   *
   * Abstract base class for render blocks.
   *
   * @{
   */

  /**
   * RenderBlockBase class
   *
   * Classes RenderBlockBase, RenderBlock and RenderBlockGroup enable
   * multiple views (of the same or different scenes) to be set up and then
   * rendered at once.
   */
  class RenderBlockBase : public Shared
  {
  public:
    /** Smart pointer type for this class */
    typedef SharedPtr< RenderBlockBase > Ptr;

    /** Virtual destructor so that subclasses are destroyed properly.
     */
    virtual ~RenderBlockBase() {}

    /** Render this block.
     */
    virtual void render() = 0;

    /**
     * Update the time for shader based effects.
     */
    virtual void update( A3M_FLOAT timeInSeconds
                         /**< Time (mod 60) to set in uniform u_time */ ) = 0;

    /** Set stereoscopic parameters for camera
     */
    virtual void setStereo(
      A3M_FLOAT zFocal /**< distance camera to focal plane in world units */,
      A3M_FLOAT eyeSep /**< distance between camera positions in world units */
    ) = 0;
  };
  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERBLOCKBASE_H */
