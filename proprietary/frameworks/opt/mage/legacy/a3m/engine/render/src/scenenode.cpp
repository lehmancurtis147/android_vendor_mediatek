/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * SceneNode Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/scenenode.h>   /* This class's API */
#include <a3m/scenenodevisitor.h> /* For SceneNodeVisitor interface */

namespace a3m
{
  // Instantiate the type ID for this class (please ensure this is unique!)
  A3M_UINT32 const SceneNode::NODE_TYPE = 'N';

  /*
   * Constructor
   */
  SceneNode::SceneNode() :
    m_localMirrored( A3M_FALSE ),
    m_worldMirrored( A3M_FALSE ),
    m_scale( 1.f, 1.f, 1.f ),
    m_rotation( 1.f, 0.f, 0.f, 0.f ),
    m_position( 0.f, 0.f, 0.f ),
    m_localTransformChanged( A3M_FALSE ),
    m_worldTransformChanged( A3M_FALSE ),
    m_parent( 0 )
  {
  }

  SceneNode::~SceneNode()
  {
    // Ensure children do not reference their about-to-be-deleted parent.
    for (A3M_INT32 i = 0; i < static_cast<A3M_INT32>(m_children.size()); ++i)
    {
      m_children[i]->onParentDestroyed();
    }
  }

  /*
   * Set node's parent.
   */
  void SceneNode::setParent( Ptr const& newParent,
                             A3M_BOOL preserveWorldTransform )
  {
    /* Avoid cycles by testing if this node is either equal to or above the new
     * parent in the scene graph */
    if( newParent && newParent->hasAncestor( this ) ) { return; }

    /* Remove child from its current parent (if any). */
    if( m_parent )
    {
      m_parent->removeChild( Ptr( this ) );
      m_parent = 0;
    }

    /* If the new parent is not null, add this node as its child.
     * The effect of passing a null parent is to simply remove this node from
     * its previous parent, if it had one. */
    if( newParent )
    {
      newParent->m_children.push_back( Ptr( this ) );
      m_parent = newParent.get();
    }

    if (preserveWorldTransform)
    {
      if (newParent)
      {
        m_localTransform = inverse(newParent->getWorldTransform()) * m_worldTransform;
      }
      else
      {
        m_localTransform = m_worldTransform;
      }
    }
    else
    {
      setTransformChanged();
    }
  }

  /*
   * Remove child
   */
  void SceneNode::removeChild( SceneNode::Ptr const& child )
  {
    m_children.erase( std::remove( m_children.begin(),
                                   m_children.end(), child ), m_children.end());
  }

  /*
   * Called when a node's parent is destroyed.
   */
  void SceneNode::onParentDestroyed()
  {
    m_parent = 0;
  }

  /*
   * Set rotation relative to parent.
   */
  void SceneNode::setRotation( Quaternionf const& rotation )
  {
    if( ( rotation != m_rotation ) )
    {
      setTransformChanged();
      m_rotation = rotation;
    }
  }

  /*
   * Set scale relative to parent.
   */
  void SceneNode::setScale( Vector3f const& scale )
  {
    if( m_scale != scale )
    {
      setTransformChanged();
      m_scale = scale;
    }
  }

  /*
   * Set position relative to parent.
   */
  void SceneNode::setPosition( Vector3f const& position )
  {
    if( m_position != position )
    {
      setTransformChanged();
      m_position = position;
    }
  }

  /*
   * Get local transformation
   */
  Matrix4f const& SceneNode::getLocalTransform() const
  {
    updateLocal();
    return m_localTransform;
  }

  /*
   * Set transform relative to parent.
   */
  void SceneNode::setLocalTransform( Matrix4f const& tran )
  {
    m_localTransformChanged = A3M_FALSE;
    m_localTransform = tran;
    m_localMirrored = determinant3( m_localTransform ) < 0.0f;

    setWorldTransformChanged();
  }

  /*
   * Get world transformation
   */
  Matrix4f const& SceneNode::getWorldTransform() const
  {
    update();
    return m_worldTransform;
  }

  /*
   * Get local mirrored flag.
   */
  A3M_BOOL SceneNode::getLocalMirrored() const
  {
    updateLocal();
    return m_localMirrored;
  }

  /*
   * Get world mirrored flag.
   */
  A3M_BOOL SceneNode::getWorldMirrored() const
  {
    update();
    return m_worldMirrored;
  }

  void SceneNode::setTransformChanged()
  {
    m_localTransformChanged = A3M_TRUE;
    setWorldTransformChanged();
  }

  void SceneNode::setWorldTransformChanged()
  {
    // Stop recursing when we reach a dirty part of the scene graph
    if( !m_worldTransformChanged )
    {
      m_worldTransformChanged = A3M_TRUE;

      for ( ChildList::iterator child = m_children.begin();
            child != m_children.end(); ++child )
      {
        (*child)->setWorldTransformChanged();
      }
    }
  }

  /*
   * Update scene graph.
   * This function first iterates up to the top of the dirty part of the scene
   * graph, and then triggers an update down through the scene graph.
   */
  A3M_BOOL SceneNode::update() const
  {
    // If this node is not dirty, return false.
    if( !m_worldTransformChanged )
    {
      return A3M_FALSE;
    }

    // Iterate upwards through the scene graph until there is no parent node, or
    // the parent node is not dirty.
    if( !m_parent || !m_parent->update() )
    {
      // Update downwards through the dirty part of the scene graph
      if( m_parent )
      {
        // Parent is up-to-date
        updateAll( m_parent->m_worldTransform, m_parent->m_worldMirrored );
      }
      else
      {
        // Parent doesn't exist
        updateAll( Matrix4f::IDENTITY, A3M_FALSE );
      }
    }

    return A3M_TRUE;
  }

  /*
   * Update local transform
   */
  void SceneNode::updateLocal() const
  {
    if( m_localTransformChanged )
    {
      m_localTransform =
        toMatrix4( m_rotation ) *
        scale( m_scale.x, m_scale.y, m_scale.z, 1.0f );

      m_localTransform.t.x = m_position.x;
      m_localTransform.t.y = m_position.y;
      m_localTransform.t.z = m_position.z;

      A3M_BOOL mirroredX = m_scale.x < 0.0f;
      A3M_BOOL mirroredY = m_scale.y < 0.0f;
      A3M_BOOL mirroredZ = m_scale.z < 0.0f;

      // XOR all mirrored flags.
      m_localMirrored = (mirroredX != mirroredY) != mirroredZ;

      m_localTransformChanged = A3M_FALSE;
    }
  }

  /*
   * Update all of the scene graph under this node
   */
  void SceneNode::updateAll(
    Matrix4f const& parentTransform, A3M_BOOL parentMirrored ) const
  {
    updateLocal();

    m_worldTransform = parentTransform * m_localTransform;
    m_worldMirrored = parentMirrored != m_localMirrored; // XOR

    for( ChildList::const_iterator child = m_children.begin(),
         endChild = m_children.end(); child != endChild; ++child  )
    {
      (*child)->updateAll( m_worldTransform, m_worldMirrored );
    }
    m_worldTransformChanged = A3M_FALSE;
  }

  SceneNode::Ptr SceneNode::find( A3M_CHAR8 const* name )
  {
    if( name == m_name ) { return Ptr( this ); }

    for( ChildList::iterator child = m_children.begin(),
         endChild = m_children.end(); child != endChild; ++child  )
    {
      Ptr node = (*child)->find( name );
      if( node ) { return node; }
    }
    return Ptr();
  }

  A3M_BOOL SceneNode::hasAncestor( SceneNode* node ) const
  {
    // This node also counts as an "ancestor"
    if( this == node ) { return A3M_TRUE; }
    if( m_parent ) { return m_parent->hasAncestor( node ); }
    return A3M_FALSE;
  }

  /*
   * Accept a visitor
   */
  void SceneNode::accept( SceneNodeVisitor& visitor )
  {
    visitor.visit( this );
  }

  /* Update the last world transform.
   * Record the current world position of this node. This function will
   * be called after the MotionBlurRenderer has drawn a frame so that it can
   * be used to calculate velocity vectors for the next frame.
   */
  void SceneNode::updateMotionData()
  {
    m_lastWorldTransform = getWorldTransform();
  }

  /* Get the last world transform.
   * This will return the world transform as it was when updateMotionData() was
   * last called.
   */
  Matrix4f const& SceneNode::getLastWorldTransform() const
  {
    return m_lastWorldTransform;
  }

} /* namespace a3m */
