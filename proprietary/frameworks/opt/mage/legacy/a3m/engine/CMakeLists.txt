add_library(a3m
    assets/src/memfilesource_a3m.cpp
    assets_ngin3d/src/memfilesource_ngin3d.cpp
    collision/src/plane.cpp
    collision/src/ray.cpp
    collision/src/shape.cpp
    collision/src/sphere.cpp
    collision/src/square.cpp
    common/src/info.cpp
    common/src/version.cpp
    external/src/md5/md5.c
    external/src/pvr/PVRTDecompress.cpp
    external/src/pvr/PVRTTextureAPI.cpp
    external/src/pvr/PVRTgles2Ext.cpp
    external/src/stb/stb_image.c
    external/src/stb/stb_truetype.c
    facility/src/appearance.cpp
    facility/src/assetcachepool.cpp
    facility/src/assetpath.cpp
    facility/src/background.cpp
    facility/src/colour.cpp
    facility/src/digestmaker.cpp
    facility/src/error.cpp
    facility/src/extensions.cpp
    facility/src/fileutility.cpp
    facility/src/font.cpp
    facility/src/fontloader.cpp
    facility/src/image.cpp
    facility/src/indexbuffer.cpp
    facility/src/memorystream.cpp
    facility/src/mesh.cpp
    facility/src/meshutility.cpp
    facility/src/rendercontext.cpp
    facility/src/renderdevice.cpp
    facility/src/rendertarget.cpp
    facility/src/resource.cpp
    facility/src/resourcecache.cpp
    facility/src/shaderprogram.cpp
    facility/src/shaderprogramloader.cpp
    facility/src/shaderuniform.cpp
    facility/src/stdstringutility.cpp
    facility/src/texture.cpp
    facility/src/texture2dloader.cpp
    facility/src/texturecubeloader.cpp
    facility/src/vertexbuffer.cpp
    maths/src/fpmaths.cpp
    maths/src/mathsstring.cpp
    pss/src/eglcontext.cpp
    pss/src/linux/filestream_linux.cpp
    pss/src/linux/log_linux.cpp
    pss/src/linux/stream_linux.cpp
    pss/src/linux/window_linux.cpp
    render/src/animation.cpp
    render/src/camera.cpp
    render/src/glofile.cpp
    render/src/light.cpp
    render/src/renderblock.cpp
    render/src/renderblockgroup.cpp
    render/src/scenenode.cpp
    render/src/sceneutility.cpp
    render/src/simplerenderer.cpp
    render/src/solid.cpp
)

include_directories(
    ${A3M_SOURCE_DIR}/engine/assets/src
    ${A3M_SOURCE_DIR}/engine/assets_ngin3d/src
    ${A3M_SOURCE_DIR}/engine/collision/api
    ${A3M_SOURCE_DIR}/engine/common/api
    ${A3M_SOURCE_DIR}/engine/common/src
    ${A3M_SOURCE_DIR}/engine/external/api
    ${A3M_SOURCE_DIR}/engine/facility/api
    ${A3M_SOURCE_DIR}/engine/facility/src
    ${A3M_SOURCE_DIR}/engine/maths/api
    ${A3M_SOURCE_DIR}/engine/pss/api
    ${A3M_SOURCE_DIR}/engine/pss/src
    ${A3M_SOURCE_DIR}/engine/pss/src/linux
    ${A3M_SOURCE_DIR}/engine/render/api
    ${GLES2_DIR}/Include
)
