/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/** \file
 * Ray class
 */
#pragma once
#ifndef A3M_RAY_H
#define A3M_RAY_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/matrix4.h>           /* for Matrix4f                           */
#include <a3m/vector3.h>           /* for Vector3f                           */

/*****************************************************************************
 * A3M Namespace
 *****************************************************************************/
namespace a3m
{
  /** \ingroup a3mCollision
   *
   * An ray extending infinitely in either direction.
   *
   * @{
   */

  /**
   * Ray class.
   * A ray is described by an arbitrary point on the ray, plus a direction,
   * which may have an arbitrary non-zero length.  While the position and
   * magnitude of the ray may be unimportant in describing the ray, they may be
   * used as reference values when performing calculations such as raycast
   * collisions.
   */
  class Ray
  {
  public:
    /**
     * Default constructor.
     * Constructs a ray at the origin pointing along the positive z-axis.
     */
    Ray() :
      m_direction(Vector3f::Z_AXIS)
    {
    }

    /**
     * Constructor.
     * Constructs a plane pointing along the positive z-axis.
     */
    Ray(
      Vector3f const& position,
      /**< Position of ray. */
      Vector3f const& direction
      /**< Direction of ray. */) :
      m_position(position),
      m_direction(direction)
    {
    }

    /**
     * Sets the position of the ray.
     */
    void setPosition(Vector3f const& position /**< Position vector */)
    {
      m_position = position;
    }

    /**
     * Returns the position of the ray.
     * \return Position vector
     */
    Vector3f const& getPosition() const
    {
      return m_position;
    }

    /**
     * Sets the direction of the ray.
     */
    void setDirection(Vector3f const& direction /**< Direction vector */)
    {
      m_direction = direction;
    }

    /**
     * Returns the direction of the ray.
     * \return Direction vector
     */
    Vector3f const& getDirection() const
    {
      return m_direction;
    }

  private:
    Vector3f m_position; /**< Position of the ray */
    Vector3f m_direction; /**< Direction of the ray */
  };

  /**
   * Transforms a ray using a transformation matrix.
   * \return Transformed ray
   */
  Ray transform(
    Ray const& ray,
    /**< Ray to transform */
    Matrix4f const& matrix
    /**< Tranformation matrix */);

  /**
   * Returns a ray with a normalized direction.
   * \return Normalized ray
   */
  Ray normalize(Ray const& ray /**< Ray to normalize */);

  /** @} */

} // namespace a3m

#endif // A3M_RAY_H
