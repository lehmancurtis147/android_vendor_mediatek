/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <GLES2/gl2.h>          /* OpenGL ES 2.0 API */

#include <a3m/rendercontext.h>  /* This module's header */

#include <error.h>              /* for CHECK_GL_ERROR */

namespace
{

  using namespace a3m;

  /*
   * Convert a blender factor field to an OpenGL type enum
   */
  GLenum glBlendFactorConvert( BlendFactor factor )
  {
    switch( factor )
    {
    case BLEND_CONSTANT_ALPHA:            return GL_CONSTANT_ALPHA;
    case BLEND_CONSTANT_COLOUR:           return GL_CONSTANT_COLOR;
    case BLEND_DST_ALPHA:                 return GL_DST_ALPHA;
    case BLEND_DST_COLOUR:                return GL_DST_COLOR;
    case BLEND_ONE:                       return GL_ONE;
    case BLEND_ONE_MINUS_CONSTANT_ALPHA:  return GL_ONE_MINUS_CONSTANT_ALPHA;
    case BLEND_ONE_MINUS_CONSTANT_COLOUR: return GL_ONE_MINUS_CONSTANT_COLOR;
    case BLEND_ONE_MINUS_DST_ALPHA:       return GL_ONE_MINUS_DST_ALPHA;
    case BLEND_ONE_MINUS_DST_COLOUR:      return GL_ONE_MINUS_DST_COLOR;
    case BLEND_ONE_MINUS_SRC_ALPHA:       return GL_ONE_MINUS_SRC_ALPHA;
    case BLEND_ONE_MINUS_SRC_COLOUR:      return GL_ONE_MINUS_SRC_COLOR;
    case BLEND_SRC_ALPHA:                 return GL_SRC_ALPHA;
    case BLEND_SRC_ALPHA_SATURATE:        return GL_SRC_ALPHA_SATURATE;
    case BLEND_SRC_COLOUR:                return GL_SRC_COLOR;
    case BLEND_ZERO:                      return GL_ZERO;
    default:
      A3M_LOG_ERROR( "Invalid blend factor %d", factor );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a blender function field to an OpenGL type enum
   */
  GLenum glBlendFunctionConvert( BlendFunction function )
  {
    switch( function )
    {
    case BLEND_ADD:              return GL_FUNC_ADD;
    case BLEND_REVERSE_SUBTRACT: return GL_FUNC_REVERSE_SUBTRACT;
    case BLEND_SUBTRACT:         return GL_FUNC_SUBTRACT;
    default:
      A3M_LOG_ERROR( "Invalid blend function %d", function );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a depth test function field to an OpenGL type enum
   */
  GLenum glDepthFunctionConvert( DepthFunction function )
  {
    switch( function )
    {
    case DEPTH_NEVER:    return GL_NEVER;
    case DEPTH_LESS:     return GL_LESS;
    case DEPTH_EQUAL:    return GL_EQUAL;
    case DEPTH_LEQUAL:   return GL_LEQUAL;
    case DEPTH_GREATER:  return GL_GREATER;
    case DEPTH_NOTEQUAL: return GL_NOTEQUAL;
    case DEPTH_GEQUAL:   return GL_GEQUAL;
    case DEPTH_ALWAYS:   return GL_ALWAYS;
    default:
      A3M_LOG_ERROR( "Invalid depth function %d", function );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a Cull mode to an OpenGL type
   */
  GLenum glCullingModeConvert( CullingMode mode )
  {
    switch( mode )
    {
    case CULL_BACK:           return GL_BACK;
    case CULL_FRONT:          return GL_FRONT;
    case CULL_FRONT_AND_BACK: return GL_FRONT_AND_BACK;
    case CULL_NONE:           return GL_NONE;
    default:
      A3M_LOG_ERROR( "Invalid culling mode %d", mode );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a Winding type to an OpenGL type
   */
  GLenum glWindingOrderConvert( WindingOrder order )
  {
    switch( order )
    {
    case WIND_CCW: return GL_CCW;
    case WIND_CW:  return GL_CW;
    default:
      A3M_LOG_ERROR( "Invalid winding order %d", order );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a stencil face field to an OpenGL type enum
   */
  GLenum glStencilFaceConvert( StencilFace format )
  {
    switch( format )
    {
    case STENCIL_BACK:  return GL_BACK;
    case STENCIL_FRONT: return GL_FRONT;

    default:
      A3M_LOG_ERROR("Invalid stencil face %d", format );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a stencil function field to an OpenGL type enum
   */
  GLenum glStencilFunctionConvert( StencilFunction format )
  {
    switch( format )
    {
    case STENCIL_NEVER:    return GL_NEVER;
    case STENCIL_LESS:     return GL_LESS;
    case STENCIL_EQUAL:    return GL_EQUAL;
    case STENCIL_LEQUAL:   return GL_LEQUAL;
    case STENCIL_GREATER:  return GL_GREATER;
    case STENCIL_NOTEQUAL: return GL_NOTEQUAL;
    case STENCIL_GEQUAL:   return GL_GEQUAL;
    case STENCIL_ALWAYS:   return GL_ALWAYS;
    default:
      A3M_LOG_ERROR("Invalid stencil function %d", format );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Convert a stencil operation field to an OpenGL type enum
   */
  GLenum glStencilOperationConvert( StencilOperation format )
  {
    switch( format )
    {
    case STENCIL_ZERO:      return GL_ZERO;
    case STENCIL_KEEP:      return GL_KEEP;
    case STENCIL_REPLACE:   return GL_REPLACE;
    case STENCIL_INCR:      return GL_INCR;
    case STENCIL_DECR:      return GL_DECR;
    case STENCIL_INVERT:    return GL_INVERT;
    case STENCIL_INCR_WRAP: return GL_INCR_WRAP;
    case STENCIL_DECR_WRAP: return GL_DECR_WRAP;
    default:
      A3M_LOG_ERROR("Invalid stencil operation %d", format );
      return GL_INVALID_ENUM;
    }
  }

  /*
   * Enables or disables a given OpenGL capability.
   */
  void setGlCapabilityEnabled(GLenum capability, A3M_BOOL enabled)
  {
    if (enabled)
    {
      glEnable(capability);
      CHECK_GL_ERROR;
    }
    else
    {
      glDisable(capability);
      CHECK_GL_ERROR;
    }
  }

} // namespace

namespace a3m
{
  RenderContext::RenderContext() :
    m_blendEnabled(A3M_FALSE),
    m_blendColour(0.0f, 0.0f, 0.0f, 0.0f),
    m_srcRgbBlendFactor(BLEND_ONE),
    m_srcAlphaBlendFactor(BLEND_ONE),
    m_dstRgbBlendFactor(BLEND_ZERO),
    m_dstAlphaBlendFactor(BLEND_ZERO),
    m_rgbBlendFunction(BLEND_ADD),
    m_alphaBlendFunction(BLEND_ADD),
    m_cullingMode(CULL_NONE),
    m_windingOrder(WIND_CCW),
    m_lineWidth(1.0f),

    m_colourMaskR(A3M_TRUE),
    m_colourMaskG(A3M_TRUE),
    m_colourMaskB(A3M_TRUE),
    m_colourMaskA(A3M_TRUE),

    m_depthWriteEnabled(A3M_TRUE),
    m_depthRangeNear(0.0f),
    m_depthRangeFar(1.0f),
    m_depthOffsetFactor(0),
    m_depthOffsetUnits(0),
    m_depthTestEnabled(A3M_FALSE),
    m_depthTestFunction(DEPTH_LESS),

    m_scissorTestEnabled(A3M_FALSE),
    m_scissorBoxLeft(0),
    m_scissorBoxBottom(0),
    m_scissorBoxWidth(0),
    m_scissorBoxHeight(0),

    m_stencilTestEnabled(A3M_FALSE),
    m_viewportLeft(0),
    m_viewportBottom(0),
    m_viewportWidth(0),
    m_viewportHeight(0),
    m_clearColour(0.0f, 0.0f, 0.0f, 0.0f),
    m_clearDepth(1.0f),
    m_clearStencil(0)
  {
    // Scissor box's initial value will depend on the render surface size.
    A3M_INT32 scissorBox[4];
    glGetIntegerv(GL_SCISSOR_BOX, scissorBox);
    CHECK_GL_ERROR;

    m_scissorBoxLeft = scissorBox[0];
    m_scissorBoxBottom = scissorBox[1];
    m_scissorBoxWidth = scissorBox[2];
    m_scissorBoxHeight = scissorBox[3];

    // Cannot initialize arrays in the initializer list
    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      m_stencilFunction[face] = STENCIL_ALWAYS;
      m_stencilReference[face] = 0;
      m_stencilReferenceMask[face] = ~0;
      m_stencilFail[face] = STENCIL_KEEP;
      m_stencilPassDepthFail[face] = STENCIL_KEEP;
      m_stencilPassDepthPass[face] = STENCIL_KEEP;
      m_stencilMask[face] = ~0;
    }

    // Viewport's initial value will depend on the render surface size.
    A3M_INT32 viewport[4];
    glGetIntegerv(GL_VIEWPORT, viewport);
    CHECK_GL_ERROR;

    m_viewportLeft = viewport[0];
    m_viewportBottom = viewport[1];
    m_viewportWidth = viewport[2];
    m_viewportHeight = viewport[3];
  }

  void RenderContext::addProperty(
    ShaderUniformBase::Ptr const& uniform, A3M_CHAR8 const* name)
  {
    if (m_uniforms.find(name) != m_uniforms.end())
    {
      A3M_LOG_ERROR("Property \"%s\" already exists in context.", name);
      return;
    }

    m_uniforms[name] = uniform;
  }

  ShaderUniformBase::Ptr RenderContext::getProperty(A3M_CHAR8 const* name) const
  {
    ShaderUniformMap::const_iterator it = m_uniforms.find(name);

    if (it == m_uniforms.end())
    {
      return ShaderUniformBase::Ptr();
    }

    return it->second;
  }

  void RenderContext::linkUniform(
    ShaderUniformBase::Ptr& uniform, A3M_CHAR8 const* name)
  {
    ShaderUniformMap::iterator it = m_uniforms.find(name);

    if (it != m_uniforms.end())
    {
      uniform = it->second;
    }
  }

  void RenderContext::setBlendEnabled(A3M_BOOL enabled)
  {
    if (m_blendEnabled != enabled)
    {
      m_blendEnabled = enabled;
      setGlCapabilityEnabled(GL_BLEND, m_blendEnabled);
    }
  }

  void RenderContext::setBlendColour(Colour4f const& colour)
  {
    if (m_blendColour != colour)
    {
      m_blendColour = colour;
      glBlendColor(m_blendColour.r, m_blendColour.g, m_blendColour.b, m_blendColour.a);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setBlendFactors(
    BlendFactor srcRgb, BlendFactor srcAlpha,
    BlendFactor dstRgb, BlendFactor dstAlpha)
  {
    if (m_srcRgbBlendFactor   != srcRgb   ||
        m_srcAlphaBlendFactor != srcAlpha ||
        m_dstRgbBlendFactor   != dstRgb   ||
        m_dstAlphaBlendFactor != dstAlpha)
    {
      m_srcRgbBlendFactor   = srcRgb;
      m_srcAlphaBlendFactor = srcAlpha;
      m_dstRgbBlendFactor   = dstRgb;
      m_dstAlphaBlendFactor = dstAlpha;

      glBlendFuncSeparate(
        glBlendFactorConvert(m_srcRgbBlendFactor),
        glBlendFactorConvert(m_dstRgbBlendFactor),
        glBlendFactorConvert(m_srcAlphaBlendFactor),
        glBlendFactorConvert(m_dstAlphaBlendFactor));
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setBlendFunctions(BlendFunction rgb, BlendFunction alpha)
  {
    if (m_rgbBlendFunction != rgb || m_alphaBlendFunction != alpha)
    {
      m_rgbBlendFunction = rgb;
      m_alphaBlendFunction = alpha;

      glBlendEquationSeparate(
        glBlendFunctionConvert(m_rgbBlendFunction),
        glBlendFunctionConvert(m_alphaBlendFunction));
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setCullingMode(CullingMode mode)
  {
    if (m_cullingMode != mode)
    {
      if (mode == CULL_NONE)
      {
        glDisable(GL_CULL_FACE);
        CHECK_GL_ERROR;
      }
      else
      {
        if (m_cullingMode == CULL_NONE)
        {
          glEnable(GL_CULL_FACE);
          CHECK_GL_ERROR;
        }

        glCullFace(glCullingModeConvert(mode));
        CHECK_GL_ERROR;
      }

      m_cullingMode = mode;
    }
  }

  void RenderContext::setWindingOrder(WindingOrder order)
  {
    if (m_windingOrder != order)
    {
      m_windingOrder = order;

      glFrontFace(glWindingOrderConvert(m_windingOrder));
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setLineWidth(A3M_FLOAT width)
  {
    if (m_lineWidth != width)
    {
      m_lineWidth = width;

      glLineWidth(m_lineWidth);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setColourMask(A3M_BOOL r, A3M_BOOL g, A3M_BOOL b, A3M_BOOL a)
  {
    if (m_colourMaskR != r || m_colourMaskG != g ||
        m_colourMaskB != b || m_colourMaskA != a)
    {
      m_colourMaskR = r;
      m_colourMaskG = g;
      m_colourMaskB = b;
      m_colourMaskA = a;

      glColorMask(m_colourMaskR, m_colourMaskG, m_colourMaskB, m_colourMaskA);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setDepthWriteEnabled(A3M_BOOL mask)
  {
    if (m_depthWriteEnabled != mask)
    {
      m_depthWriteEnabled = mask;

      glDepthMask(m_depthWriteEnabled);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setDepthOffset(A3M_FLOAT factor, A3M_FLOAT units)
  {
    if (m_depthOffsetFactor != factor || m_depthOffsetUnits != units)
    {
      // Automatically disable depth offset if both values are zero
      if (factor == 0.0f && units == 0.0f)
      {
        glDisable(GL_POLYGON_OFFSET_FILL);
        CHECK_GL_ERROR;
      }
      else
      {
        if (m_depthOffsetFactor == 0.0f && m_depthOffsetUnits == 0.0f)
        {
          glEnable(GL_POLYGON_OFFSET_FILL);
          CHECK_GL_ERROR;
        }

        glPolygonOffset(m_depthOffsetFactor, m_depthOffsetUnits);
        CHECK_GL_ERROR;
      }

      m_depthOffsetFactor = factor;
      m_depthOffsetUnits = units;
    }
  }

  void RenderContext::setDepthTestEnabled(A3M_BOOL flag)
  {
    if (m_depthTestEnabled != flag)
    {
      m_depthTestEnabled = flag;
      setGlCapabilityEnabled(GL_DEPTH_TEST, m_depthTestEnabled);
    }
  }

  void RenderContext::setDepthTestFunction(DepthFunction function)
  {
    if (m_depthTestFunction != function)
    {
      m_depthTestFunction = function;

      glDepthFunc(glDepthFunctionConvert(m_depthTestFunction));
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setScissorTestEnabled(A3M_BOOL flag)
  {
    if (m_scissorTestEnabled != flag)
    {
      m_scissorTestEnabled = flag;
      setGlCapabilityEnabled(GL_SCISSOR_TEST, m_scissorTestEnabled);
    }
  }

  void RenderContext::setScissorBox(
    A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height)
  {
    if (m_scissorBoxLeft   != left   ||
        m_scissorBoxBottom != bottom ||
        m_scissorBoxWidth  != width  ||
        m_scissorBoxHeight != height)
    {
      m_scissorBoxLeft   = left;
      m_scissorBoxBottom = bottom;
      m_scissorBoxWidth  = width;
      m_scissorBoxHeight = height;

      glScissor(
        m_scissorBoxLeft, m_scissorBoxBottom, m_scissorBoxWidth, m_scissorBoxHeight);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setStencilTestEnabled(A3M_BOOL flag)
  {
    if (m_stencilTestEnabled != flag)
    {
      m_stencilTestEnabled = flag;
      setGlCapabilityEnabled(GL_STENCIL_TEST, m_stencilTestEnabled);
    }
  }

  void RenderContext::setStencilFunction(
    StencilFace face, StencilFunction function, A3M_INT32 reference, A3M_UINT32 mask)
  {
    if (m_stencilFunction[face]      != function  ||
        m_stencilReference[face]     != reference ||
        m_stencilReferenceMask[face] != mask)
    {
      m_stencilFunction[face]      = function;
      m_stencilReference[face]     = reference;
      m_stencilReferenceMask[face] = mask;

      glStencilFuncSeparate(
        glStencilFaceConvert(face),
        glStencilFunctionConvert(m_stencilFunction[face]),
        m_stencilReference[face],
        m_stencilReferenceMask[face]);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setStencilOperations(
    StencilFace face,
    StencilOperation fail,
    StencilOperation passDepthFail,
    StencilOperation passDepthPass)
  {
    if (m_stencilFail[face]          != fail          ||
        m_stencilPassDepthFail[face] != passDepthFail ||
        m_stencilPassDepthPass[face] != passDepthPass)
    {
      m_stencilFail[face]          = fail;
      m_stencilPassDepthFail[face] = passDepthFail;
      m_stencilPassDepthPass[face] = passDepthPass;

      glStencilOpSeparate(
        glStencilFaceConvert(face),
        glStencilOperationConvert(m_stencilFail[face]),
        glStencilOperationConvert(m_stencilPassDepthFail[face]),
        glStencilOperationConvert(m_stencilPassDepthPass[face]));
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setStencilMask(StencilFace face, A3M_UINT32 mask)
  {
    if (m_stencilMask[face] != mask)
    {
      m_stencilMask[face] = mask;

      glStencilMaskSeparate(glStencilFaceConvert(face), m_stencilMask[face]);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setViewport(
    A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height)
  {
    if (m_viewportLeft   != left   ||
        m_viewportBottom != bottom ||
        m_viewportWidth  != width  ||
        m_viewportHeight != height)
    {
      m_viewportLeft   = left;
      m_viewportBottom = bottom;
      m_viewportWidth  = width;
      m_viewportHeight = height;

      glViewport(m_viewportLeft, m_viewportBottom, m_viewportWidth, m_viewportHeight);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setDepthRange(A3M_FLOAT near_, A3M_FLOAT far_)
  {
    if (m_depthRangeNear != near_ ||
        m_depthRangeFar  != far_)
    {
      m_depthRangeNear = near_;
      m_depthRangeFar  = far_;

      glDepthRangef(m_depthRangeNear, m_depthRangeFar);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setClearColour(Colour4f const& colour)
  {
    if (m_clearColour != colour)
    {
      m_clearColour = colour;

      glClearColor(m_clearColour.r, m_clearColour.g, m_clearColour.b, m_clearColour.a);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setClearDepth(A3M_FLOAT depth)
  {
    if (m_clearDepth != depth)
    {
      m_clearDepth = depth;

      glClearDepthf(m_clearDepth);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::setClearStencil(A3M_INT32 stencil)
  {
    if (m_clearStencil != stencil)
    {
      m_clearStencil = stencil;

      glClearStencil(m_clearStencil);
      CHECK_GL_ERROR;
    }
  }

  void RenderContext::clear()
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    CHECK_GL_ERROR;
  }

  void setStencilFunction(
    RenderContext& context,
    StencilFunction function,
    A3M_INT32 reference,
    A3M_UINT32 mask)
  {
    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      context.setStencilFunction(
        static_cast<StencilFace>(face), function, reference, mask);
    }
  }

  void setStencilOperations(
    RenderContext& context,
    StencilOperation fail,
    StencilOperation passDepthFail,
    StencilOperation passDepthPass)
  {
    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      context.setStencilOperations(
        static_cast<StencilFace>(face), fail, passDepthFail, passDepthPass);
    }
  }

  void setStencilMask(RenderContext& context, A3M_UINT32 mask)
  {
    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      context.setStencilMask(static_cast<StencilFace>(face), mask);
    }
  }

} // namespace a3m
