/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Mesh Implementation
 *
 */

#include <a3m/mesh.h>        // for this class's declaration
#include <algorithm>          /* for std::max */

namespace a3m
{
  const A3M_FLOAT MeshHeader::INVALID_BOUNDING_RADIUS = -1.f;

  /// Default constructor.
  MeshHeader::MeshHeader() :
    boundingRadius(INVALID_BOUNDING_RADIUS),
    boundingBox(0.f, 0.f, 0.f),
    offsetOrigin()
  {}

  /*************
   * MeshCache *
   *************/

  MeshCache::MeshCache(
    IndexBufferCache::Ptr const& indexBufferCache,
    VertexBufferCache::Ptr const& vertexBufferCache) :
    m_indexBufferCache(indexBufferCache),
    m_vertexBufferCache(vertexBufferCache)
  {
  }

  Mesh::Ptr MeshCache::create(
    MeshHeader const& header,
    IndexBuffer::Ptr const& indexBuffer,
    VertexBuffer::Ptr const& vertexBuffer,
    A3M_CHAR8 const* name)
  {
    // Create new asset and add to cache
    Mesh::Ptr mesh(new Mesh(header, indexBuffer, vertexBuffer));
    add(mesh, name);

    return mesh;
  }

  /********
   * Mesh *
   ********/

  /*
   * Constructor
   */
  Mesh::Mesh(MeshHeader const& meshHeader,
             IndexBuffer::Ptr const& indexBuffer,
             VertexBuffer::Ptr const& vertexBuffer) :
    m_meshHeader(meshHeader),
    m_indexBuffer(indexBuffer),
    m_vertexBuffer(vertexBuffer)
  {
  }

  A3M_FLOAT computeMaximumLength(VertexArray::Ptr vertexArray)
  {
    A3M_FLOAT maxLengthSquared = 0.f;

    if (vertexArray)
    {
      Vector3f const* positions = reinterpret_cast<Vector3f const*>(
                                    vertexArray->data< A3M_FLOAT >());

      A3M_INT32 vertexCount = vertexArray->vertexCount();
      while (vertexCount--)
      {
        const A3M_FLOAT lengthSquared = dot(*positions, *positions);
        maxLengthSquared = std::max(maxLengthSquared, lengthSquared);
        ++positions;
      }
    }

    return sqrt(maxLengthSquared);
  }

} /* namespace a3m */
