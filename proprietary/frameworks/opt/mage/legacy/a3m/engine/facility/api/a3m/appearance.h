/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Appearance class
 *
 */
#pragma once
#ifndef A3M_APPEARANCE_H
#define A3M_APPEARANCE_H

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/assetcachepool.h>  /* AssetCachePool             */
#include <a3m/base_types.h>      /* A3M base type defines      */
#include <a3m/colour.h>          /* for Colour4f               */
#include <a3m/pointer.h>         /* for SharedPtr              */
#include <a3m/noncopyable.h>     /* for NonCopyable            */
#include <a3m/shaderprogram.h>   /* for ShaderProgram::Ptr     */
#include <a3m/shaderuniform.h>   /* for ShaderUniformBase::Ptr */
#include <a3m/stream.h>          /* for Stream::Ptr            */
#include <a3m/texture2d.h>       /* for Texture2DCache         */
#include <a3m/renderdevice.h>    /* for render()               */
#include <a3m/renderstate.h>     /* for render state enums     */

#include <map>                   /* for std::map               */
#include <string>                /* for std::string            */
#include <vector>                /* for std::vector            */

namespace a3m
{
  /** \defgroup a3mAppearance Appearance
   * \ingroup  a3mRefScene
   *
   * The appearance class specifies all aspects of how a primitive will be
   * rendered. It contains a ShaderProgram and a list of properties which
   * correspond to ShaderUniforms in the ShaderProgram. It also specifies the
   * polygon mode (culling operations), alpha blending mode and depth
   * testing/writing that will be used.
   *
   * Uniforms in the ShaderProgram are associated with defined property names.
   * (this association is usually made in the shader program file).  The
   * property values can either be defined in a RenderContext object, which is
   * required to use an Appearance, or can be defined in the Appearance itself.
   * Appearance values take precedence over values linked from the
   * RenderContext, so it is possible to override "global" values locally in an
   * Appearance.
   *
   * The most convenient way of defining properties is in appearance (.ap)
   * files, although properties may be set programmatically.  Setting the
   * property will not automatically add it to the Appearance, so all
   * properties must be added manually beforehand.
   *
   * \code
   * // Set standard "A3M-defined" diffuse colour
   * appearance.setProperty( properties::M_DIFFUSE_COLOUR, diffuseColour ) );
   *
   * // Add and set custom user-defined property array of size 2
   * appearance.addProperty( "CUSTOM_PARAMS", 2 ) );
   * appearance.setProperty( "CUSTOM_PARAMS", myFirstParam, 0 ) );
   * appearance.setProperty( "CUSTOM_PARAMS", myOtherParam, 1 ) );
   * \endcode
   *
   * While you are free to define your own properties, A3M has a defined set of
   * property names which are recognised by the built-in Glo loader and
   * renderer, and can be found in properties.h.
   *
   *  @{
   */

  /** Interface for property collector objects.
   * Classes should implement this interface to be able to collect property data
   * via the Appearance::collectProperties() function.
   */
  class PropertyCollector
  {
  public:
    virtual ~PropertyCollector() {}

    /** Called by the appearance once per property to pass property information.
     * \return A3M_TRUE to continue collecting the properties, or A3M_FALSE to
     * terminate collection early.
     */
    virtual A3M_BOOL collect(
      ShaderUniformBase::Ptr const& uniform,
      /**< Property uniform object */
      A3M_CHAR8 const* name,
      /**< Name of the property */
      A3M_INT32 index
      /**< Index of the property uniform in its associated shader
       * program */) = 0;
  };

  /** Appearance class.
   * The Appearance class specifies all aspects of how a primitive will be
   * rendered. It contains a ShaderProgram and a list of ShaderUniforms that
   * will be written to the ShaderProgram. It also specifies the polygon mode
   * (culling operations), alpha blending mode and depth testing/writing that
   * will be used.
   */
  class Appearance : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( Appearance )

    /** Smart pointer type for this class */
    typedef SharedPtr< Appearance > Ptr;

    /** Default constructor.
     */
    Appearance();

    /**
     * Loads and applies settings from an Appearance file.
     * Any attributes of the Appearance which are defined in the Appearance
     * file will be overwritten; any attributes which are not defined in the
     * file will be retained.
     */
    void apply(
      AssetCachePool& pool,
      /**< Pool from which to load the required assets */
      A3M_CHAR8 const* appearanceName
      /**< Name of appearance file to read */ );

    /** Set ShaderProgram.
     * Sets the ShaderProgram object used by this Appearance.
     */
    void setShaderProgram( ShaderProgram::Ptr const& shaderProgram
                           /**< ShaderProgram */ );
    /** Get ShaderProgram.
     * \return the ShaderProgram object used by this Appearance.
     */
    ShaderProgram::Ptr const& getShaderProgram() const;

    /** Add a property to the appearance.
     * If a property has not been set globally, it is important that it be set
     * in the appearance, otherwise objects using the appearance may not render
     * correctly.
     */
    template< typename T >
    void addProperty(
      A3M_CHAR8 const* propertyName, /**< Name of the property */
      A3M_INT32 size = 1 /**< Size of the property (should be >1 for arrays) */)
    {
      if (size < 1)
      {
        // Logical error in client's code.
        A3M_LOG_ERROR("Size of property cannot be less than 1.");

        return;
      }

      ShaderUniformBase::Ptr uniform(new ShaderUniform<T>(size));
      m_properties[propertyName] = Property(uniform);
      m_linked = A3M_FALSE;
    }

    /** Sets the values of a named property.
     * If the property in question does not yet exist, it will be created.  A
     * warning will be logged if an index greater than zero is given, and the
     * property has not been previously defined as an array.
     */
    template< typename T >
    void setProperty(
      A3M_CHAR8 const* propertyName,  /**< Name of the property */
      T const& value, /**< Value to set */
      A3M_INT32 i = 0 /**< Index into property array (should = 0 for non-arrays) */ )
    {
      if (i < 0)
      {
        // Logical error in client's code.
        A3M_LOG_ERROR("Property index cannot be less than 0.");
        return;
      }

      PropertyMap::iterator it = m_properties.find( propertyName );

      if ( it == m_properties.end() )
      {
        if (i > 0)
        {
          A3M_LOG_WARN("Property array \"%s[%d]\" value is being set without "
                       "prior knowledge of the size of the array; please define size of "
                       "array explicitly using addProperty() before setting values.",
                       propertyName, i);
        }

        addProperty< T >( propertyName, i + 1 );
        it = m_properties.find( propertyName );
      }

      it->second.uniform->setValue( value, i );
    }

    /** Gets the uniform associated with a property by name.
     * \return Property uniform, or null if the property doesn't exist
     */
    ShaderUniformBase::Ptr const& getPropertyUniform(
      A3M_CHAR8 const* propertyName
      /**< Name of the property */) const;

    /** Returns whether a property exists.
     * \return True if the property exists
     */
    A3M_BOOL propertyExists(
      /** Name of the property */
      A3M_CHAR8 const* propertyName) const;

    /** Passes data for each property in turn to a collector object.
     * This function can be used to acquire information about each of the
     * properties in the appearance via callbacks.
     */
    void collectProperties(
      PropertyCollector* collector
      /**< Collector to which the appearance passes the data */ ) const;

    /** Returns the Appearance's name.
     * \return name for this Appearance
     */
    A3M_CHAR8 const* getName() const { return m_name.c_str(); }

    /** Sets the Appearance's name.
     */
    void setName( A3M_CHAR8 const* name /**< New name for this Appearance */ )
    {
      m_name = name;
    }

    /** \copydoc RenderContext::setBlendColour() */
    void setBlendColour(Colour4f const& colour);
    /** \see setBlendColour() */
    Colour4f const& getBlendColour() const { return m_blendColour; }

    /** \copydoc RenderContext::setBlendFactors() */
    void setBlendFactors(
      BlendFactor srcRgb, BlendFactor srcAlpha,
      BlendFactor dstRgb, BlendFactor dstAlpha);
    /** \see setBlendFactors() */
    BlendFactor const& getSrcRgbBlendFactor() const { return m_srcRgbBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getSrcAlphaBlendFactor() const { return m_srcAlphaBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getDstRgbBlendFactor() const { return m_dstRgbBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getDstAlphaBlendFactor() const { return m_dstAlphaBlendFactor; }

    /** \copydoc RenderContext::setBlendFunctions() */
    void setBlendFunctions(BlendFunction rgb, BlendFunction alpha);
    /** \see setBlendFunctions() */
    BlendFunction const& getRgbBlendFunction() const { return m_rgbBlendFunction; }
    /** \see setBlendFunctions() */
    BlendFunction const& getAlphaBlendFunction() const { return m_alphaBlendFunction; }

    /** By forcing opacity, an appearance will never use blending, regardless
     * of the blend factors and functions.
     * \see setBlendFactors()
     * \see setBlendFunctions()
     */
    void setForceOpaque(A3M_BOOL enabled) { m_forceOpaque = enabled; }
    /** \see setForceOpaque() */
    A3M_BOOL getForceOpaque() const { return m_forceOpaque; }

    /** Returns TRUE if the appearance does not enable blending.
     * An appearance will be opaque either if the blend factors are set to
     * (BLEND_ONE, BLEND_ONE, BLEND_ZERO, BLEND_ZERO) and the blend functions
     * are both set to BLEND_ADD, or if opacity has been forced.
     * \see setBlendFactors()
     * \see setBlendFunctions()
     * \see setForceOpaque()
     */
    A3M_BOOL isOpaque() const;

    /** \copydoc RenderContext::setCullingMode() */
    void setCullingMode(CullingMode mode);
    /** \see setCullingMode() */
    CullingMode getCullingMode() const { return m_cullingMode; }

    /** \copydoc RenderContext::setWindingOrder() */
    void setWindingOrder(WindingOrder order);
    /** \see setWindingOrder() */
    WindingOrder getWindingOrder() const { return m_windingOrder; }

    /** \copydoc RenderContext::setLineWidth() */
    void setLineWidth(A3M_FLOAT width);
    /** \see setLineWidth() */
    A3M_FLOAT getLineWidth() const { return m_lineWidth; }

    /** \copydoc RenderContext::setColourMask() */
    void setColourMask(A3M_BOOL r, A3M_BOOL g, A3M_BOOL b, A3M_BOOL a);
    /** \see setColourMask() */
    A3M_BOOL getColourMaskR() const { return m_colourMaskR; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskG() const { return m_colourMaskG; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskB() const { return m_colourMaskB; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskA() const { return m_colourMaskA; }

    /** \copydoc RenderContext::setDepthWriteEnabled() */
    void setDepthWriteEnabled(A3M_BOOL enabled);
    /** \see setDepthWriteEnabled() */
    A3M_BOOL isDepthWriteEnabled() const { return m_depthWriteEnabled; }

    /** \copydoc RenderContext::setDepthOffset() */
    void setDepthOffset(A3M_FLOAT factor, A3M_FLOAT units);
    /** \see setDepthOffset() */
    A3M_FLOAT getDepthOffsetFactor() const { return m_depthOffsetFactor; }
    /** \see setDepthOffset() */
    A3M_FLOAT getDepthOffsetUnits() const { return m_depthOffsetUnits; }

    /** \copydoc RenderContext::setDepthTestEnabled() */
    void setDepthTestEnabled(A3M_BOOL flag);
    /** \see setDepthTestEnabled() */
    A3M_BOOL isDepthTestEnabled() const { return m_depthTestEnabled; }

    /** \copydoc RenderContext::setDepthTestFunction() */
    void setDepthTestFunction(DepthFunction function);
    /** \see setDepthTestFunction() */
    DepthFunction getDepthTestFunction() const { return m_depthTestFunction; }

    /** \copydoc RenderContext::setScissorTestEnabled() */
    void setScissorTestEnabled(A3M_BOOL flag);
    /** \see setScissorTestEnabled() */
    A3M_BOOL isScissorTestEnabled() const { return m_scissorTestEnabled; }

    /** \copydoc RenderContext::setScissorBox() */
    void setScissorBox(
      A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height);
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxLeft() const { return m_scissorBoxLeft; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxBottom() const { return m_scissorBoxBottom; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxWidth() const { return m_scissorBoxWidth; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxHeight() const { return m_scissorBoxHeight; }

    /** \copydoc RenderContext::setStencilTestEnabled() */
    void setStencilTestEnabled(A3M_BOOL enabled);
    /** \see setStencilTestEnabled() */
    A3M_BOOL isStencilTestEnabled(A3M_BOOL) const { return m_stencilTestEnabled; }

    /** \copydoc RenderContext::setStencilFunction() */
    void setStencilFunction(
      StencilFace face, StencilFunction function, A3M_INT32 reference, A3M_UINT32 mask);
    /** \see setStencilFunction() */
    StencilFunction getStencilFunction(StencilFace face) const
    { return m_stencilFunction[face]; }
    /** \see setStencilFunction() */
    A3M_INT32 getStencilReference(StencilFace face) const
    { return m_stencilReference[face]; }
    /** \see setStencilFunction() */
    A3M_UINT32 getStencilReferenceMask(StencilFace face) const
    { return m_stencilReferenceMask[face]; }

    /** \copydoc RenderContext::setStencilOperations() */
    void setStencilOperations(
      /** Which side of the polygon to apply the stencil operations to */
      StencilFace face,
      /** What happens when the stencil test fails */
      StencilOperation fail,
      /** What happens when the stencil test passes, but the depth test fails */
      StencilOperation passDepthFail,
      /** What happens when the stencil test passes, and the depth test passes */
      StencilOperation passDepthPass);

    /** \see setStencilOperations() */
    StencilOperation getStencilFail(StencilFace face) const
    { return m_stencilFail[face]; }
    /** \see setStencilOperations() */
    StencilOperation getStencilPassDepthFail(StencilFace face) const
    { return m_stencilPassDepthFail[face]; }
    /** \see setStencilOperations() */
    StencilOperation getStencilPassDepthPass(StencilFace face) const
    { return m_stencilPassDepthPass[face]; }

    /** \copydoc RenderContext::setStencilMask() */
    void setStencilMask(StencilFace face, A3M_UINT32 mask);
    /** \see setStencilMask() */
    A3M_UINT32 getStencilMask(StencilFace face) const { return m_stencilMask[face]; }

    /** Makes this Appearance current for future drawing operations.
     */
    void enable( RenderContext& context );

  private:
    /*
     * Properties associate a uniform value with an index into the shader
     * program, and are mapped by name.
     */
    struct Property
    {
      // Required to store in a std::map
      Property() :
        index(-1)
      {
      }

      explicit Property(
        ShaderUniformBase::Ptr uniform_, A3M_INT32 index_ = -1) :
        uniform(uniform_),
        index(index_)
      {
      }

      ShaderUniformBase::Ptr uniform;
      A3M_INT32 index;
    };

    /* Enables the shader program and the appearance properties.
     */
    void enableShaderProgram( RenderContext& context );

    /* Set the uniforms in the shader program which correspond to properties in
     * this appearance.  Properties are only set if they are null in the
     * shader program.
     */
    void applyProperties();

    /* Reset the properties defined in this appearance, in the shader program.
     */
    void resetProperties();

    /* Links properties defined in the appearance with uniforms in the shader
     * program.
     */
    void linkShaderProgram();

    typedef std::map<std::string, Property> PropertyMap;

    std::string            m_name;            /* Name of the appearance      */
    ShaderProgram::Ptr     m_shaderProgram;   /* Shared ShaderProgram object */
    PropertyMap            m_properties;      /* Properties mapped by name   */
    A3M_BOOL               m_linked;          /* Is program linked?          */

    Colour4f m_blendColour;
    BlendFactor m_srcRgbBlendFactor;
    BlendFactor m_srcAlphaBlendFactor;
    BlendFactor m_dstRgbBlendFactor;
    BlendFactor m_dstAlphaBlendFactor;
    BlendFunction m_rgbBlendFunction;
    BlendFunction m_alphaBlendFunction;
    A3M_BOOL m_forceOpaque;
    CullingMode m_cullingMode;
    WindingOrder m_windingOrder;
    A3M_FLOAT m_lineWidth;

    A3M_BOOL m_colourMaskR;
    A3M_BOOL m_colourMaskG;
    A3M_BOOL m_colourMaskB;
    A3M_BOOL m_colourMaskA;

    A3M_BOOL m_depthWriteEnabled;
    A3M_FLOAT m_depthOffsetFactor;
    A3M_FLOAT m_depthOffsetUnits;
    A3M_BOOL m_depthTestEnabled;
    DepthFunction m_depthTestFunction;

    A3M_BOOL m_scissorTestEnabled;
    A3M_INT32 m_scissorBoxLeft;
    A3M_INT32 m_scissorBoxBottom;
    A3M_INT32 m_scissorBoxWidth;
    A3M_INT32 m_scissorBoxHeight;

    A3M_BOOL m_stencilTestEnabled;
    StencilFunction m_stencilFunction[STENCIL_NUM_FACES];
    A3M_INT32  m_stencilReference[STENCIL_NUM_FACES];
    A3M_UINT32 m_stencilReferenceMask[STENCIL_NUM_FACES];
    StencilOperation  m_stencilFail[STENCIL_NUM_FACES];
    StencilOperation  m_stencilPassDepthFail[STENCIL_NUM_FACES];
    StencilOperation  m_stencilPassDepthPass[STENCIL_NUM_FACES];
    A3M_UINT32 m_stencilMask[STENCIL_NUM_FACES];
  };

  /**
   * Convenience function which loads a new appearance from an Appearance file.
   * \sa Appearance::apply()
   * \return The appearance, or null if it failed to load.
   */
  Appearance::Ptr loadAppearance(
    AssetCachePool& pool,
    /**< Pool from which to load the required assets */
    A3M_CHAR8 const* appearanceName
    /**< Name of appearance file to read */ );

  /** @} */

} /* end of namespace */

#endif /* A3M_APPEARANCE_H */
