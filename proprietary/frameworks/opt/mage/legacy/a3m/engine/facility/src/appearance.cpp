/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Appearance Implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/appearance.h>   /* This class's API           */
#include <a3m/rendercontext.h>   /* for RenderContext       */
#include <fileutility.h>      /* FileToString and CharRange */
#include <error.h>            /* for CHECK_GL_ERROR         */
#include <a3m/log.h>          /* A3M log API                */
#include <string>             /* for std::string::substr()  */

//lint -esym(960, 33) Disable Lint warning about side effects on right hand of
// logical operators ||and &&. Many functions in this file rely on
// left-to-right evaluation and short-circuiting.

namespace
{
  using namespace a3m;

  /* Appearance file version number */
  const A3M_INT32 CURRENT_MAJOR_VERSION = 1;
  const A3M_INT32 CURRENT_MINOR_VERSION = 0;

  /* Index value that indicates a property not linked to a program. */
  A3M_INT32 const UNLINKED_INDEX = -1;

  /*
   * Read a CullingMode enum
   */
  CullingMode readCullingMode( CharRange& range,
                               A3M_CHAR8 const* appearanceName )
  {
    // \todo Remove old versions in MAGE2
    CharRange token = readToken( range );
    if(      token == "BACK" || token == "CULL_BACK" )
    { return CULL_BACK; }
    else if( token == "FRONT" || token == "CULL_FRONT" )
    { return CULL_FRONT; }
    else if( token == "FRONT_AND_BACK" || token == "CULL_FRONT_AND_BACK" )
    { return CULL_FRONT_AND_BACK; }
    else if( token == "NONE" || token == "CULL_NONE" )
    { return CULL_NONE; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected culling mode %s",
                     appearanceName, token.begin );
      return CULL_NONE;
    }
  }

  /*
   * Read a WindingOrder enum
   */
  WindingOrder readWindingOrder( CharRange& range,
                                 A3M_CHAR8 const* appearanceName )
  {
    // \todo Remove old versions in MAGE2
    CharRange token = readToken( range );
    if(      token == "CCW" || token == "WINDING_CCW" ) { return WIND_CCW; }
    else if( token == "CW" || token == "WINDING_CW" )  { return WIND_CW; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected winding mode %s",
                     appearanceName, token.begin );
      return WIND_CCW;
    }
  }

  /*
   * Read a DepthFunction enum
   */
  DepthFunction readDepthFunction( CharRange& range,
                                   A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if(      token == "NEVER" )    { return DEPTH_NEVER; }
    else if( token == "LESS" )     { return DEPTH_LESS; }
    else if( token == "EQUAL" )    { return DEPTH_EQUAL; }
    else if( token == "LEQUAL" )   { return DEPTH_LEQUAL; }
    else if( token == "GREATER" )  { return DEPTH_GREATER; }
    else if( token == "NOTEQUAL" ) { return DEPTH_NOTEQUAL; }
    else if( token == "GEQUAL" )   { return DEPTH_GEQUAL; }
    else if( token == "ALWAYS" )   { return DEPTH_ALWAYS; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected depth function %s",
                     appearanceName, token.begin );
      return DEPTH_ALWAYS;
    }
  }

  /*
   * Read a BlendFactor enum
   */
  BlendFactor readBlendFactor( CharRange& range,
                               A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if( token == "CONSTANT_ALPHA" )
    { return BLEND_CONSTANT_ALPHA; }
    else if( token == "CONSTANT_COLOUR" )
    { return BLEND_CONSTANT_COLOUR; }
    else if( token == "DST_ALPHA" )
    { return BLEND_DST_ALPHA; }
    else if( token == "DST_COLOUR" )
    { return BLEND_DST_COLOUR; }
    else if( token == "ONE" )
    { return BLEND_ONE; }
    else if( token == "ONE_MINUS_CONSTANT_ALPHA" )
    { return BLEND_ONE_MINUS_CONSTANT_ALPHA; }
    else if( token == "ONE_MINUS_CONSTANT_COLOUR" )
    { return BLEND_ONE_MINUS_CONSTANT_COLOUR; }
    else if( token == "ONE_MINUS_DST_ALPHA" )
    { return BLEND_ONE_MINUS_DST_ALPHA; }
    else if( token == "ONE_MINUS_DST_COLOUR" )
    { return BLEND_ONE_MINUS_DST_COLOUR; }
    else if( token == "ONE_MINUS_SRC_ALPHA" )
    { return BLEND_ONE_MINUS_SRC_ALPHA; }
    else if( token == "ONE_MINUS_SRC_COLOUR" )
    { return BLEND_ONE_MINUS_SRC_COLOUR; }
    else if( token == "SRC_ALPHA" )
    { return BLEND_SRC_ALPHA; }
    else if( token == "SRC_ALPHA_SATURATE" )
    { return BLEND_SRC_ALPHA_SATURATE; }
    else if( token == "SRC_COLOUR" )
    { return BLEND_SRC_COLOUR; }
    else if( token == "ZERO" )
    { return BLEND_ZERO; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected blend factor %s",
                     appearanceName, token.begin );
      return BLEND_ONE;
    }
  }

  /*
   * Read a BlendFunction enum
   */
  BlendFunction readBlendFunction( CharRange& range,
                                   A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if(      token == "ADD" )              { return BLEND_ADD; }
    else if( token == "REVERSE_SUBTRACT" ) { return BLEND_REVERSE_SUBTRACT; }
    else if( token == "SUBTRACT" )         { return BLEND_SUBTRACT; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected blend function %s",
                     appearanceName, token.begin );
      return BLEND_ADD;
    }
  }

  /*
   * Read a StencilFace enum
   */
  StencilFace readStencilFace( CharRange& range,
                               A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if(      token == "BACK" )  { return STENCIL_BACK; }
    else if( token == "FRONT" ) { return STENCIL_FRONT; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected stencil face %s",
                     appearanceName, token.begin );
      return STENCIL_FRONT;
    }
  }

  /*
   * Read a StencilFunction enum
   */
  StencilFunction readStencilFunction( CharRange& range,
                                       A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if(      token == "NEVER" )    { return STENCIL_NEVER; }
    else if( token == "LESS" )     { return STENCIL_LESS; }
    else if( token == "EQUAL" )    { return STENCIL_EQUAL; }
    else if( token == "LEQUAL" )   { return STENCIL_LEQUAL; }
    else if( token == "GREATER" )  { return STENCIL_GREATER; }
    else if( token == "NOTEQUAL" ) { return STENCIL_NOTEQUAL; }
    else if( token == "GEQUAL" )   { return STENCIL_GEQUAL; }
    else if( token == "ALWAYS" )   { return STENCIL_ALWAYS; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected stencil function %s",
                     appearanceName, token.begin );
      return STENCIL_ALWAYS;
    }
  }

  /*
   * Read a StencilOperation enum
   */
  StencilOperation readStencilOperation( CharRange& range,
                                         A3M_CHAR8 const* appearanceName )
  {
    CharRange token = readToken( range );
    if(      token == "ZERO" )      { return STENCIL_ZERO; }
    else if( token == "KEEP" )      { return STENCIL_KEEP; }
    else if( token == "REPLACE" )   { return STENCIL_REPLACE; }
    else if( token == "INCR" )      { return STENCIL_INCR; }
    else if( token == "DECR" )      { return STENCIL_DECR; }
    else if( token == "INVERT" )    { return STENCIL_INVERT; }
    else if( token == "INCR_WRAP" ) { return STENCIL_INCR_WRAP; }
    else if( token == "DECR_WRAP" ) { return STENCIL_DECR_WRAP; }
    else
    {
      A3M_LOG_ERROR( "Appearance file: %s unexpected stencil operation %s",
                     appearanceName, token.begin );
      return STENCIL_REPLACE;
    }
  }

  ShaderUniformBase::Ptr const& getNullUniform()
  {
    static ShaderUniformBase::Ptr uniform;
    return uniform;
  }

} /* anonymous namespace */


namespace a3m
{
  Appearance::Appearance() :
    m_linked(A3M_FALSE),
    m_blendColour(0.0f, 0.0f, 0.0f, 0.0f),
    m_srcRgbBlendFactor(BLEND_ONE),
    m_srcAlphaBlendFactor(BLEND_ONE),
    m_dstRgbBlendFactor(BLEND_ZERO),
    m_dstAlphaBlendFactor(BLEND_ZERO),
    m_rgbBlendFunction(BLEND_ADD),
    m_alphaBlendFunction(BLEND_ADD),
    m_forceOpaque(A3M_FALSE),
    m_cullingMode(CULL_BACK),
    m_windingOrder(WIND_CCW),
    m_lineWidth(1.0f),
    m_colourMaskR(A3M_TRUE),
    m_colourMaskG(A3M_TRUE),
    m_colourMaskB(A3M_TRUE),
    m_colourMaskA(A3M_TRUE),
    m_depthWriteEnabled(A3M_TRUE),
    m_depthOffsetFactor(0),
    m_depthOffsetUnits(0),
    m_depthTestEnabled(A3M_TRUE),
    m_depthTestFunction(DEPTH_LESS),
    m_scissorTestEnabled(A3M_FALSE),
    m_scissorBoxLeft(0),
    m_scissorBoxBottom(0),
    m_scissorBoxWidth(0),
    m_scissorBoxHeight(0),
    m_stencilTestEnabled(A3M_FALSE)
  {
    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      m_stencilFunction[face] = STENCIL_ALWAYS;
      m_stencilReference[face] = 0;
      m_stencilReferenceMask[face] = ~0;
      m_stencilFail[face] = STENCIL_KEEP;
      m_stencilPassDepthFail[face] = STENCIL_KEEP;
      m_stencilPassDepthPass[face] = STENCIL_KEEP;
      m_stencilMask[face] = ~0;
    }
  }

  /*
   * Apply settings from an Appearance file.
   */
  void Appearance::apply(
    AssetCachePool& pool,
    A3M_CHAR8 const* appearanceName )
  {
    std::string normName = detail::normalizeAssetName(appearanceName);

    Stream::Ptr stream = pool.getStream(normName.c_str());

    if (!stream)
    {
      A3M_LOG_ERROR( "Appearance file: %s not found", normName.c_str() );
      return;
    }

    FileToString file( *stream );
    CharRange range( file );

    if( range.empty() )
    {
      A3M_LOG_ERROR( "Appearance file: %s empty", normName.c_str() );
      return;
    }

    /* Check version number (major and minor) */
    A3M_INT32 major = readInt( range, -1 );

    if( major != CURRENT_MAJOR_VERSION )
    {
      A3M_LOG_ERROR( "Appearance file: %s wrong version", normName.c_str() );
      return;
    }

    if( !requireChar( range, '.' ) )
    {
      A3M_LOG_ERROR( "Appearance file: %s expected '.'", normName.c_str() );
      return;
    }
    A3M_INT32 minor = readInt( range, -1 );

    if( minor != CURRENT_MINOR_VERSION )
    {
      A3M_LOG_ERROR( "Appearance file: %s wrong version", normName.c_str() );
      return;
    }

    while( !range.empty() )
    {
      CharRange tag = readToken( range );
      if( tag == "property" )
      {
        CharRange name = readToken( range );
        CharRange type = readToken( range );
        if( type == "Texture2D" )
        {
          CharRange textureFile = readToken( range );
          setProperty( name.begin,
                       pool.texture2DCache()->get( textureFile.begin ) );
        }
        else if( type == "TextureCube" )
        {
          CharRange textureFile = readToken( range );
          setProperty( name.begin,
                       pool.textureCubeCache()->get( textureFile.begin ) );
        }
        else if( ( type == "Colour4f" ) || ( type == "Vector4f" ) )
        {
          A3M_FLOAT r = readFloat( range );
          A3M_FLOAT g = readFloat( range );
          A3M_FLOAT b = readFloat( range );
          A3M_FLOAT a = readFloat( range );
          setProperty( name.begin, Vector4f( r, g, b, a  ) );
        }
        else if( type == "Vector3f" )
        {
          A3M_FLOAT x = readFloat( range );
          A3M_FLOAT y = readFloat( range );
          A3M_FLOAT z = readFloat( range );
          setProperty( name.begin, Vector3f( x, y, z  ) );
        }
        else if( type == "Vector2f" )
        {
          A3M_FLOAT x = readFloat( range );
          A3M_FLOAT y = readFloat( range );
          setProperty( name.begin, Vector2f( x, y ) );
        }
        else if( type == "float" )
        {
          A3M_FLOAT x = readFloat( range );
          setProperty( name.begin, x );
        }
        else if( type == "int" )
        {
          A3M_INT32 x = readInt( range );
          setProperty( name.begin, x );
        }
        else if( type == "bool" )
        {
          A3M_BOOL x = readBool( range );
          setProperty( name.begin, x );
        }
        else
        {
          A3M_LOG_ERROR( "Appearance file: %s unexpected type %s",
                         normName.c_str(), type.begin );
          return;
        }
      }
      else if( std::string( tag.begin, tag.end ).substr(0, 2) == "//" )
      {
        // This is a comment, so skip the entire line
        readTo( range, '\n' );
      }
      else if( tag == "shader_program" )
      {
        CharRange shaderProgramFile = readToken( range );
        this->setShaderProgram(
          pool.shaderProgramCache()->get( shaderProgramFile.begin ) );
      }
      // \todo Remove in MAGE2 (replaced by separate commands)
      else if( tag == "polygon_mode" )
      {
        setCullingMode( readCullingMode( range, normName.c_str() ) );
        setWindingOrder( readWindingOrder( range, normName.c_str() ) );
        setLineWidth( readFloat( range, 1.0f ) );
      }
      else if( tag == "culling_mode" )
      {
        setCullingMode( readCullingMode( range, normName.c_str() ) );
      }
      else if( tag == "winding_order" )
      {
        setWindingOrder( readWindingOrder( range, normName.c_str() ) );
      }
      else if( tag == "line_width" )
      {
        setLineWidth( readFloat( range, 1.0f ) );
      }
      else if( tag == "depth_offset" )
      {
        A3M_FLOAT factor = readFloat( range, 0.0f );
        A3M_FLOAT units  = readFloat( range, 0.0f );
        setDepthOffset( factor, units );
      }
      else if( tag == "depth_test" )
      {
        setDepthTestEnabled( readBool( range ) );
      }
      else if( tag == "depth_function" )
      {
        setDepthTestFunction(
          readDepthFunction( range, normName.c_str() ) );
      }
      else if( tag == "depth_write" )
      {
        setDepthWriteEnabled( readBool( range ) );
      }
      else if( tag == "colour_mask" )
      {
        A3M_BOOL r = readBool( range );
        A3M_BOOL g = readBool( range );
        A3M_BOOL b = readBool( range );
        A3M_BOOL a = readBool( range );
        setColourMask( r, g, b, a );
      }
      else if( tag == "blend_colour" )
      {
        A3M_FLOAT r = readFloat( range );
        A3M_FLOAT g = readFloat( range );
        A3M_FLOAT b = readFloat( range );
        A3M_FLOAT a = readFloat( range );
        setBlendColour( Colour4f( r, g, b, a ) );
      }
      else if( tag == "blend_factors" )
      {
        BlendFactor srcColour = readBlendFactor( range, normName.c_str() );
        BlendFactor srcAlpha = readBlendFactor( range, normName.c_str() );
        BlendFactor dstColour = readBlendFactor( range, normName.c_str() );
        BlendFactor dstAlpha = readBlendFactor( range, normName.c_str() );
        setBlendFactors( srcColour, srcAlpha, dstColour, dstAlpha );
      }
      else if( tag == "blend_functions" )
      {
        BlendFunction colourFunction = readBlendFunction( range, normName.c_str() );
        BlendFunction alphaFunction = readBlendFunction( range, normName.c_str() );
        setBlendFunctions( colourFunction, alphaFunction );
      }
      else if( tag == "stencil_function" )
      {
        StencilFace face = readStencilFace( range, normName.c_str() );
        StencilFunction function = readStencilFunction( range, normName.c_str() );
        A3M_INT32  ref  = readInt( range, 0 );
        A3M_UINT32 mask = readUInt( range, 0 );
        setStencilFunction( face, function, ref, mask );
      }
      else if( tag == "stencil_operations" )
      {
        StencilFace face = readStencilFace( range, normName.c_str() );
        StencilOperation fail = readStencilOperation( range, normName.c_str() );
        StencilOperation passDepthFail = readStencilOperation( range, normName.c_str() );
        StencilOperation passDepthPass = readStencilOperation( range, normName.c_str() );
        setStencilOperations( face, fail, passDepthFail, passDepthPass );
      }
      // \todo Remove old version in MAGE2
      else if( tag == "stencil_mask" || tag == "stencil_write_mask" )
      {
        StencilFace face = readStencilFace( range, normName.c_str() );
        A3M_UINT32    mask = static_cast< A3M_UINT32 >( readInt( range, 0 ) );
        setStencilMask( face, mask );
      }
      else
      {
        A3M_LOG_ERROR( "Appearance file: %s unexpected tag %s",
                       normName.c_str(), tag.begin );
        return;
      }
      /* Make sure range will be empty if we don't have another token */
      eatWhite( range );
    }
  }

  /*
   * Set ShaderProgram
   */
  void Appearance::setShaderProgram( ShaderProgram::Ptr const& shaderProgram )
  {
    if (m_shaderProgram != shaderProgram)
    {
      m_shaderProgram = shaderProgram;
      m_linked = A3M_FALSE;
    }
  }

  /*
   * Get ShaderProgram
   */
  ShaderProgram::Ptr const& Appearance::getShaderProgram() const
  {
    return m_shaderProgram;
  }

  ShaderUniformBase::Ptr const& Appearance::getPropertyUniform(
    A3M_CHAR8 const* propertyName) const
  {
    PropertyMap::const_iterator it = m_properties.find(propertyName);

    if (it == m_properties.end())
    {
      return getNullUniform();
    }

    return it->second.uniform;
  }

  A3M_BOOL Appearance::propertyExists(A3M_CHAR8 const* propertyName) const
  {
    return m_properties.find(propertyName) != m_properties.end();
  }

  void Appearance::collectProperties(PropertyCollector* collector) const
  {
    for (PropertyMap::const_iterator it = m_properties.begin();
         it != m_properties.end(); ++ it)
    {
      A3M_CHAR8 const* name = it->first.c_str();
      A3M_INT32 index = it->second.index;
      ShaderUniformBase::Ptr const& uniform = it->second.uniform;

      // The collector will return A3M_FALSE if it wants to stop collecting.
      if (!collector->collect(uniform, name, index))
      {
        break;
      }
    }
  }

  void Appearance::setBlendColour(Colour4f const& colour)
  {
    m_blendColour = colour;
  }

  void Appearance::setBlendFactors(
    BlendFactor srcRgb, BlendFactor srcAlpha,
    BlendFactor dstRgb, BlendFactor dstAlpha)
  {
    m_srcRgbBlendFactor   = srcRgb;
    m_srcAlphaBlendFactor = srcAlpha;
    m_dstRgbBlendFactor   = dstRgb;
    m_dstAlphaBlendFactor = dstAlpha;
  }

  void Appearance::setBlendFunctions(BlendFunction rgb, BlendFunction alpha)
  {
    m_rgbBlendFunction = rgb;
    m_alphaBlendFunction = alpha;
  }

  A3M_BOOL Appearance::isOpaque() const
  {
    return
      m_forceOpaque || (
        m_srcRgbBlendFactor == BLEND_ONE &&
        m_srcAlphaBlendFactor == BLEND_ONE &&
        m_dstRgbBlendFactor == BLEND_ZERO &&
        m_dstAlphaBlendFactor == BLEND_ZERO &&
        m_rgbBlendFunction == BLEND_ADD &&
        m_alphaBlendFunction == BLEND_ADD);
  }

  void Appearance::setCullingMode(CullingMode mode)
  {
    m_cullingMode = mode;
  }

  void Appearance::setWindingOrder(WindingOrder order)
  {
    m_windingOrder = order;
  }

  void Appearance::setLineWidth(A3M_FLOAT width)
  {
    m_lineWidth = width;
  }

  void Appearance::setColourMask(A3M_BOOL r, A3M_BOOL g, A3M_BOOL b, A3M_BOOL a)
  {
    m_colourMaskR = r;
    m_colourMaskG = g;
    m_colourMaskB = b;
    m_colourMaskA = a;
  }

  void Appearance::setDepthWriteEnabled(A3M_BOOL enabled)
  {
    m_depthWriteEnabled = enabled;
  }

  void Appearance::setDepthOffset(A3M_FLOAT factor, A3M_FLOAT units)
  {
    m_depthOffsetFactor = factor;
    m_depthOffsetUnits = units;
  }

  void Appearance::setDepthTestEnabled(A3M_BOOL flag)
  {
    m_depthTestEnabled = flag;
  }

  void Appearance::setDepthTestFunction(DepthFunction function)
  {
    m_depthTestFunction = function;
  }

  void Appearance::setScissorTestEnabled(A3M_BOOL flag)
  {
    m_scissorTestEnabled = flag;
  }

  void Appearance::setScissorBox(
    A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height)
  {
    m_scissorBoxLeft   = left;
    m_scissorBoxBottom = bottom;
    m_scissorBoxWidth  = width;
    m_scissorBoxHeight = height;
  }

  void Appearance::setStencilTestEnabled(A3M_BOOL enabled)
  {
    m_stencilTestEnabled = enabled;
  }

  void Appearance::setStencilFunction(
    StencilFace face, StencilFunction function, A3M_INT32 reference, A3M_UINT32 mask)
  {
    m_stencilFunction[face]      = function;
    m_stencilReference[face]     = reference;
    m_stencilReferenceMask[face] = mask;
  }

  void Appearance::setStencilOperations(
    StencilFace face,
    StencilOperation fail,
    StencilOperation passDepthFail,
    StencilOperation passDepthPass)
  {
    m_stencilFail[face]          = fail;
    m_stencilPassDepthFail[face] = passDepthFail;
    m_stencilPassDepthPass[face] = passDepthPass;
  }

  void Appearance::setStencilMask(StencilFace face, A3M_UINT32 mask)
  {
    m_stencilMask[face] = mask;
  }

  /*
   * Enable this Appearance
   */
  void Appearance::enable( RenderContext& context )
  {
    context.setBlendEnabled(!isOpaque());
    context.setBlendColour(m_blendColour);
    context.setBlendFactors(
      m_srcRgbBlendFactor, m_srcAlphaBlendFactor,
      m_dstRgbBlendFactor, m_dstAlphaBlendFactor);
    context.setBlendFunctions(m_rgbBlendFunction, m_alphaBlendFunction);
    context.setCullingMode(m_cullingMode);
    context.setWindingOrder(m_windingOrder);
    context.setLineWidth(m_lineWidth);
    context.setColourMask(m_colourMaskR, m_colourMaskG, m_colourMaskB, m_colourMaskA);
    context.setDepthWriteEnabled(m_depthWriteEnabled);
    context.setDepthOffset(m_depthOffsetFactor, m_depthOffsetUnits);
    context.setDepthTestEnabled(m_depthTestEnabled);
    context.setDepthTestFunction(m_depthTestFunction);
    context.setScissorTestEnabled(m_scissorTestEnabled);
    context.setScissorBox(
      m_scissorBoxLeft, m_scissorBoxBottom,
      m_scissorBoxWidth, m_scissorBoxHeight);
    context.setStencilTestEnabled(m_stencilTestEnabled);

    for (A3M_INT32 face = 0; face < STENCIL_NUM_FACES; ++face)
    {
      context.setStencilFunction(
        static_cast<StencilFace>(face),
        m_stencilFunction[face],
        m_stencilReference[face],
        m_stencilMask[face]);

      context.setStencilOperations(
        static_cast<StencilFace>(face),
        m_stencilFail[face],
        m_stencilPassDepthFail[face],
        m_stencilPassDepthPass[face]);

      context.setStencilMask(static_cast<StencilFace>(face), m_stencilMask[face]);
    }

    enableShaderProgram( context );
  }

  /*
   * Performs the apply, enable and reset of appearance properties (and enables
   * the shader program of course).
   * This function avoid code duplication in the enable() functions.
   */
  void Appearance::enableShaderProgram( RenderContext& context )
  {
    if (m_shaderProgram)
    {
      linkShaderProgram();
      applyProperties();
      m_shaderProgram->enable( context );
      resetProperties();
    }
  }

  /*
   * Uniforms are stored along with an index into the shader program.  An
   * UNLINKED_INDEX indicates that the uniform doesn't exist in the shader
   * program, or exists, but already has its value set.  This function iterates
   * over all properties in the appearance, and sets what it can in the shader
   * program.
   */
  void Appearance::applyProperties()
  {
    for (PropertyMap::iterator it = m_properties.begin();
         it != m_properties.end(); ++it)
    {
      Property& propertyInfo = it->second;

      if (propertyInfo.index != UNLINKED_INDEX)
      {
        m_shaderProgram->setUniformPropertyValue(
          propertyInfo.index, propertyInfo.uniform);
      }
    }
  }

  /*
   * In order to allow appearances using the same shader program as this one to
   * apply their properties, and to avoid this appearance's property values from
   * being perminantly applied to the shader program, we must reset the uniform
   * values that we previously set in applyProperties().
   */
  void Appearance::resetProperties()
  {
    for (PropertyMap::iterator it = m_properties.begin();
         it != m_properties.end(); ++it)
    {
      Property& propertyInfo = it->second;

      if (propertyInfo.index != UNLINKED_INDEX)
      {
        m_shaderProgram->setUniformPropertyValue(
          propertyInfo.index, ShaderUniformBase::Ptr());
      }
    }
  }

  /*
   * When a new shader program is applied, we must scan it to find out which
   * uniforms match with the appearance's properties.  If a uniform matches, its
   * index from the shader program is stored alongside its corresponding
   * property in the appearance.
   */
  void Appearance::linkShaderProgram()
  {
    if (m_linked)
    {
      return;
    }

    A3M_ASSERT(m_shaderProgram);

    // Clear all previous uniform indices.
    for (PropertyMap::iterator it = m_properties.begin();
         it != m_properties.end(); ++it)
    {
      it->second.index = UNLINKED_INDEX;
    }

    // Get some new indices from the shader program.
    A3M_INT32 uniformCount = m_shaderProgram->getUniformCount();

    for (A3M_INT32 i = 0; i < uniformCount; ++i)
    {
      // Uniforms are linked by matching their property names.
      A3M_CHAR8 const* propertyName =
        m_shaderProgram->getUniformPropertyName(i);
      PropertyMap::iterator it = m_properties.find(propertyName);

      if (it != m_properties.end())
      {
        it->second.index = i;
      }
    }

    m_linked = A3M_TRUE;
  }

  /*
   * Convenience appearance loading function.
   */
  Appearance::Ptr loadAppearance(
    AssetCachePool& pool, A3M_CHAR8 const* appearanceName)
  {
    Appearance::Ptr appearance(new Appearance());
    appearance->apply(pool, appearanceName);
    return appearance;
  }

} /* namespace a3m */
