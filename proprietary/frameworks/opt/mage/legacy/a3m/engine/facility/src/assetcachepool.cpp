/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * AssetCachePool Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/assetcachepool.h> /* This class's API */
#include <fontloader.h> /* for FontLoader */
#include <memfilesource_a3m.h> /* for create_memfilesource_a3m() */
#include <memfilesource_ngin3d.h> /* for create_memfilesource_ngin3d() */
#include <shaderprogramloader.h> /* for ShaderProgramLoader */
#include <texture2dloader.h> /* for Texture2DLoader */
#include <texturecubeloader.h> /* for TextureCubeLoader */

namespace a3m
{
  AssetCachePool::AssetCachePool() :
    m_assetPath(new detail::AssetPath()),

    m_indexBufferCache(new IndexBufferCache()),
    m_shaderProgramCache(new ShaderProgramCache()),
    m_texture2DCache(new Texture2DCache()),
    m_textureCubeCache(new TextureCubeCache()),
    m_vertexBufferCache(new VertexBufferCache()),

    m_fontCache(new FontCache(m_texture2DCache)),
    m_meshCache(new MeshCache(m_indexBufferCache, m_vertexBufferCache))
  {
    // Register default loaders
    m_fontCache->registerLoader(
      FontLoader::Ptr(new FontLoader()));
    m_shaderProgramCache->registerLoader(
      ShaderProgramLoader::Ptr(new ShaderProgramLoader()));
    m_texture2DCache->registerLoader(
      Texture2DLoader::Ptr(new Texture2DLoader()));
    m_textureCubeCache->registerLoader(
      TextureCubeLoader::Ptr(new TextureCubeLoader()));

    // Register A3M built-in memory source
    registerSource(create_memfilesource_a3m());
    // Register ngin3d built-in memory source
    // \todo Move this into ngin3d
    registerSource(create_memfilesource_ngin3d());
  }

  void AssetCachePool::registerSource(StreamSource::Ptr const& source)
  {
    m_assetPath->add(source);

    m_fontCache->registerSource(source);
    m_indexBufferCache->registerSource(source);
    m_meshCache->registerSource(source);
    m_shaderProgramCache->registerSource(source);
    m_texture2DCache->registerSource(source);
    m_textureCubeCache->registerSource(source);
    m_vertexBufferCache->registerSource(source);
  }

  void AssetCachePool::setCacheSource(StreamSource::Ptr const& source)
  {
    m_cacheSource = source;

    m_fontCache->setCacheSource(source);
    m_indexBufferCache->setCacheSource(source);
    m_meshCache->setCacheSource(source);
    m_shaderProgramCache->setCacheSource(source);
    m_texture2DCache->setCacheSource(source);
    m_textureCubeCache->setCacheSource(source);
    m_vertexBufferCache->setCacheSource(source);
  }

  Stream::Ptr AssetCachePool::getStream(A3M_CHAR8 const* name)
  {
    return m_assetPath->find(name);
  }

  Stream::Ptr AssetCachePool::getCacheStream(
    A3M_CHAR8 const* name, A3M_BOOL writable)
  {
    Stream::Ptr stream;

    if (m_cacheSource)
    {
      stream = m_cacheSource->open(name, writable);
    }

    return stream;
  }

  void AssetCachePool::flush()
  {
    m_fontCache->flush();
    m_indexBufferCache->flush();
    m_meshCache->flush();
    m_shaderProgramCache->flush();
    m_texture2DCache->flush();
    m_textureCubeCache->flush();
    m_vertexBufferCache->flush();
  }

  void AssetCachePool::release()
  {
    m_fontCache->release();
    m_indexBufferCache->release();
    m_meshCache->release();
    m_shaderProgramCache->release();
    m_texture2DCache->release();
    m_textureCubeCache->release();
    m_vertexBufferCache->release();
  }

  void registerSource(
    AssetCachePool& pool, A3M_CHAR8 const* path, A3M_BOOL isArchive)
  {
    pool.registerSource(StreamSource::get(path, isArchive));
  }

  void setCacheSource(
    AssetCachePool& pool, A3M_CHAR8 const* path)
  {
    pool.setCacheSource(StreamSource::get(path));
  }

} /* namespace a3m */

