/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * IndexBuffer Implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/indexbuffer.h> /* Declaration of IndexBuffer Class            */
#include <cstring>            /* for strncpy etc.                            */
#include <GLES2/gl2.h>        /* for glGenBuffers etc.                       */
#include <error.h>       /* for CHECK_GL_ERROR                          */
#include <a3m/log.h>          /* A3M log API                                 */

namespace
{
  /*****************************************************************************
   * Local functions
   *****************************************************************************/
  /*
   * Convert a Primitive format to an OpenGL primitive type
   */
  GLenum gLtypeFromPrimitive( a3m::IndexBuffer::Primitive primitive )
  {
    switch( primitive )
    {
    case a3m::IndexBuffer::PRIMITIVE_POINTS:
      return GL_POINTS;
    case a3m::IndexBuffer::PRIMITIVE_LINE_STRIP:
      return GL_LINE_STRIP;
    case a3m::IndexBuffer::PRIMITIVE_LINE_LOOP:
      return GL_LINE_LOOP;
    case a3m::IndexBuffer::PRIMITIVE_LINES:
      return GL_LINES;
    case a3m::IndexBuffer::PRIMITIVE_TRIANGLE_STRIP:
      return GL_TRIANGLE_STRIP;
    case a3m::IndexBuffer::PRIMITIVE_TRIANGLE_FAN:
      return GL_TRIANGLE_FAN;
    case a3m::IndexBuffer::PRIMITIVE_TRIANGLES:
      return GL_TRIANGLES;
    default:
      A3M_ASSERT(A3M_FALSE);
      return 0;
    }
    /* All switch cases return */
  }
}

/*****************************************************************************
 * a3m Namespace
 *****************************************************************************/
namespace a3m
{
  /********************
   * IndexBufferCache *
   ********************/

  IndexBuffer::Ptr IndexBufferCache::create(IndexBuffer::Primitive primitive,
      A3M_UINT32 count, A3M_UINT16 const* indices, A3M_CHAR8 const* name)
  {
    // Create underlying OpenGL buffer and track with resource cache
    detail::BufferResource::Ptr resource(new detail::BufferResource());
    getResourceCache()->add(resource);

    // Create new asset
    IndexBuffer::Ptr indexBuffer(new IndexBuffer(
                                   primitive, count, A3M_TRUE, resource));

    if (!indexBuffer->data())
    {
      A3M_LOG_ERROR( "Failed to allocate index buffer array", 0 );
      return IndexBuffer::Ptr();
    }

    // Add asset to the cache
    add(indexBuffer, name);

    // Copy indices if they are given
    if( indices )
    {
      memcpy( indexBuffer->data(), indices,
              sizeof(A3M_UINT16) * static_cast< size_t >( count ) );
    }

    return indexBuffer;
  }

  IndexBuffer::Ptr IndexBufferCache::create(IndexBufferCache& cache,
      IndexBuffer::Primitive primitive, A3M_UINT32 count, A3M_CHAR8 const* name)
  {
    IndexBuffer::Ptr indexBuffer;
	(void)cache;

    // Create underlying OpenGL buffer and track with resource cache
    detail::BufferResource::Ptr resource(new detail::BufferResource());
    getResourceCache()->add(resource);

    // Create new asset and add to cache.
    indexBuffer.reset(new IndexBuffer(primitive, count, A3M_FALSE, resource));
    add(indexBuffer, name);

    return indexBuffer;
  }

  /***************
   * IndexBuffer *
   ***************/

  IndexBuffer::IndexBuffer(Primitive primitive, A3M_INT32 count,
                           A3M_BOOL allocate,
                           detail::BufferResource::Ptr const& resource) :
    m_primitive(primitive),
    m_indexArray(0),
    m_count(count),
    m_resource(resource),
    m_valid(A3M_TRUE)
  {
    if (allocate)
    {
      m_indexArray = new A3M_UINT16[ static_cast< size_t > ( count )];
    }
  }

  /*
   * Destructor
   */
  IndexBuffer::~IndexBuffer()
  {
    delete[] m_indexArray;
  }

  /*
   * Commits the IndexBuffer for rendering.
   *
   * Committing offloads index buffer data to GPU.
   * After a buffer is committed, the index data is no longer available to
   * change.
   *
   */
  void IndexBuffer::commit()
  {
    if( m_indexArray == NULL )
    {
      return;
    }

    /* Create buffer */
    if (!m_resource->allocate()) { return; }

    /* Make it current */
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_resource->getId() );
    CHECK_GL_ERROR;

    glBufferData( GL_ELEMENT_ARRAY_BUFFER,
                  sizeof(A3M_UINT16) * static_cast < size_t >( m_count ),
                  m_indexArray, GL_STATIC_DRAW );
    CHECK_GL_ERROR;

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0);
    CHECK_GL_ERROR;

    delete [] m_indexArray;
    m_indexArray = NULL;
  }

  /*
   * Renders primitive.
   * This is a private function called only by a3m::RenderDevice::render().
   * It renders primitives as specified in index array or vertex array.
   */
  void IndexBuffer::draw()
  {
    if( !m_valid )
    {
      return;
    }
    if ( m_resource->getId() )
    {
      /* Make it current */
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_resource->getId() );
      CHECK_GL_ERROR;

      glDrawElements( gLtypeFromPrimitive( m_primitive ), m_count,
                      GL_UNSIGNED_SHORT,
                      0 /* We're drawing from a buffer, so this is an offset */
                    );
      CHECK_GL_ERROR;

      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
      CHECK_GL_ERROR;
    }
    else if ( m_indexArray )
    {
      glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
      CHECK_GL_ERROR;

      glDrawElements( gLtypeFromPrimitive(m_primitive), m_count,
                      GL_UNSIGNED_SHORT, m_indexArray);
      CHECK_GL_ERROR;
    }
    else
    {
      glDrawArrays( gLtypeFromPrimitive(m_primitive), 0 /* first always 0 */,
                    m_count );
      CHECK_GL_ERROR;
    }
  }

  A3M_INT32 getTriangleCount(IndexBuffer const& indexBuffer)
  {
    A3M_INT32 indexCount = indexBuffer.getIndexCount();

    switch (indexBuffer.getPrimitiveType())
    {
    case IndexBuffer::PRIMITIVE_TRIANGLES:
      // Triangle lists use three separate indices for each triangle.
      return indexCount / 3;

    case IndexBuffer::PRIMITIVE_TRIANGLE_STRIP:
    case IndexBuffer::PRIMITIVE_TRIANGLE_FAN:
      // Strips and fans add one triangle per index after the initial three.
      return indexCount - 2;

    default:
      // Other primitive types do not render triangles.
      return 0;
    }
  }

} /* namespace a3m */
