/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#pragma once
#ifndef A3M_RENDERCONTEXT_H
#define A3M_RENDERCONTEXT_H

#include <a3m/colour.h>                 /* for Colour4f */
#include <a3m/renderstate.h>            /* for render state enums */
#include <a3m/shaderuniform.h>          /* for ShaderUniformBase */

namespace a3m
{
  /** \ingroup a3mRefRender
   * @{
   */

  /**
   * Stores global rendering state, as well as globally shared uniforms.
   * A RenderContext can be created at any point after the actual
   * OpenGL context has been created.  A context is generally required when
   * rendering objects or when enabling object to be used for rendering.
   * A single context object should be used per OpenGL context, otherwise
   * the state will not be tracked correctly, and rendering errors will occur
   */
  class RenderContext : public Shared
  {
  public:
    A3M_NAME_SHARED_CLASS(RenderContext)

    /** Smart pointer type for this class */
    typedef SharedPtr<RenderContext> Ptr;

    /** Make sure to create the context object after creating the OpenGL context.
     */
    RenderContext();

    /** Adds a new property of a given name and type to the context.
     * Must be called before calling getProperty() for the property to exist.
     */
    void addProperty(ShaderUniformBase::Ptr const& uniform, A3M_CHAR8 const* name);

    /** Returns the property of a given name, or null if no such property exists.
     */
    ShaderUniformBase::Ptr getProperty(A3M_CHAR8 const* name) const;

    /** Sets a uniform equal to the uniform of the property of a given name.
     * If no property exists by that name, the uniform will be unmodified.
     */
    void linkUniform(
      /** [inout] Uniform to link */
      ShaderUniformBase::Ptr& uniform,
      /** Property name */
      A3M_CHAR8 const* name);

    /** Blending mixes primitives with the contents of render target according
     * to equations defined by the blend factors, functions and colour.
     * Blending can be used to simulate effects like transparency, but is more
     * expensive, mainly because it causes the same pixels to be rendered several
     * times.
     * \see setBlendColour()
     * \see setBlendFactors()
     * \see setBlendFunctions()
     */
    void setBlendEnabled(A3M_BOOL enabled);
    /** \see setBlendEnabled() */
    A3M_BOOL isBlendEnabled() const { return m_blendEnabled; }

    /** Colour used by the BlendFactor::BLEND_CONSTANT_COLOUR and
     * BlendFactor::BLEND_ONE_MINUS_CONSTANT_COLOUR blend factors.
     * \see setBlendFactors()
     */
    void setBlendColour(Colour4f const& colour);
    /** \see setBlendColour() */
    Colour4f const& getBlendColour() const { return m_blendColour; }

    /** Blend factors are multiplied by the source and destination fragment
     * colours before they are combined according to the blend functions
     * to produce the final fragment colour.
     * The source fragment colour is that of the primitive being drawn.  The
     * destination fragment colour is that of the render target (e.g. what is
     * already on-screen).
     */
    void setBlendFactors(
      BlendFactor srcRgb, BlendFactor srcAlpha,
      BlendFactor dstRgb, BlendFactor dstAlpha);
    /** \see setBlendFactors() */
    BlendFactor const& getSrcRgbBlendFactor() const { return m_srcRgbBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getSrcAlphaBlendFactor() const { return m_srcAlphaBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getDstRgbBlendFactor() const { return m_dstRgbBlendFactor; }
    /** \see setBlendFactors() */
    BlendFactor const& getDstAlphaBlendFactor() const { return m_dstAlphaBlendFactor; }

    /** Functions used to mix the source and destination fragment colours after
     * they are multiplied by the blend factors.
     */
    void setBlendFunctions(BlendFunction rgb, BlendFunction alpha);
    /** \see setBlendFunctions() */
    BlendFunction const& getRgbBlendFunction() const { return m_rgbBlendFunction; }
    /** \see setBlendFunctions() */
    BlendFunction const& getAlphaBlendFunction() const { return m_alphaBlendFunction; }

    /** Determines which side of a polygon is drawn.
     * The front of a polygon is defined by the winding order.
     * \see setWindingOrder()
     */
    void setCullingMode(CullingMode mode);
    /** \see setCullingMode() */
    CullingMode getCullingMode() const { return m_cullingMode; }

    /** The front of a polygon is drawn when the vertices are arranged in the
     * direction described by the winding order.
     * \see setCullingMode()
     */
    void setWindingOrder(WindingOrder order);
    /** \see setWindingOrder() */
    WindingOrder getWindingOrder() const { return m_windingOrder; }

    /** The width of rasterized lines in pixels when when drawing line
     * primitives.
     * \see IndexBuffer::Primitive
     */
    void setLineWidth(A3M_FLOAT width);
    /** \see setLineWidth() */
    A3M_FLOAT getLineWidth() const { return m_lineWidth; }

    /** Determines whether each colour channel is writable.
     * Set any of the flags to FALSE to make that channel read-only.
     */
    void setColourMask(A3M_BOOL r, A3M_BOOL g, A3M_BOOL b, A3M_BOOL a);
    A3M_BOOL getColourMaskR() const { return m_colourMaskR; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskG() const { return m_colourMaskG; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskB() const { return m_colourMaskB; }
    /** \see setColourMask() */
    A3M_BOOL getColourMaskA() const { return m_colourMaskA; }

    /** Enables or disables writing to the depth buffer.
     */
    void setDepthWriteEnabled(A3M_BOOL mask);
    /** \see setDepthWriteEnabled() */
    A3M_BOOL isDepthWriteEnabled() const { return m_depthWriteEnabled; }

    /** Specifies the range of depth values written to the render target.
     * Normalized depth values range from -1 to 1, representing the near and
     * far clipping planes.  These values are mapped onto the near and far
     * depth range values.
     */
    void setDepthRange(A3M_FLOAT near_, A3M_FLOAT far_);
    /** \see setDepthRange() */
    A3M_FLOAT getDepthRangeNear() const { return m_depthRangeNear; }
    /** \see setDepthRange() */
    A3M_FLOAT getDepthRangeFar() const { return m_depthRangeFar; }

    /** Controls the amount by which depth values are adjusted before being
     * written to the depth buffer.
     * The offset amount is equal to factor * dz + r * units, where dz measures
     * the depth gradient of a polygon, and r is an implementation-dependent
     * value equal to the smallest resolvable difference in depth buffer value.
     */
    void setDepthOffset(A3M_FLOAT factor, A3M_FLOAT units);
    /** \see setDepthOffset() */
    A3M_FLOAT getDepthOffsetFactor() const { return m_depthOffsetFactor; }
    /** \see setDepthOffset() */
    A3M_FLOAT getDepthOffsetUnits() const { return m_depthOffsetUnits; }

    /** Depth testing discards pixels based on the value of the depth buffer.
     * The depth value of the pixel to be drawn is compared against the depth
     * value of the render target; if the comparison tests true, the pixel will
     * be drawn.  Depth testing is commonly used to avoid drawing pixels which
     * are meant to be hidden behind other pixels.
     * \see setDepthTestFunction()
     */
    void setDepthTestEnabled(A3M_BOOL flag);
    /** \see setDepthTestEnabled() */
    A3M_BOOL isDepthTestEnabled() const { return m_depthTestEnabled; }

    /** The depth function is used to compare depth values when depth testing
     * is enabled.
     * \see setDepthTestEnabled()
     */
    void setDepthTestFunction(DepthFunction function);
    /** \see setDepthTestFunction() */
    DepthFunction getDepthTestFunction() const { return m_depthTestFunction; }

    /** Pixels outside of the scissor box will not be drawn if scissor testing
     * is enabled.
     * \see setScissorBox()
     */
    void setScissorTestEnabled(A3M_BOOL flag);
    /** \see setScissorTestEnabled() */
    A3M_BOOL isScissorTestEnabled() const { return m_scissorTestEnabled; }

    /** The scissor box defines a rectangle in pixel, outside of which pixels
     * will not be drawn, if scissor testing is enabled.
     * \see setScissorTestEnabled()
     */
    void setScissorBox(
      A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height);
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxLeft() const { return m_scissorBoxLeft; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxBottom() const { return m_scissorBoxBottom; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxWidth() const { return m_scissorBoxWidth; }
    /** \see setScissorBox() */
    A3M_INT32 getScissorBoxHeight() const { return m_scissorBoxHeight; }

    /** Stencil testing discards pixels based on the contents of the stencil
     * buffer.  The stencil buffer value is compared against a reference value,
     * and if the comparison tests true, the pixel is drawn.  Stencil testing
     * can be used to perform advanced multi-pass rendering techniques.
     */
    void setStencilTestEnabled(A3M_BOOL enabled);
    /** \see setStencilTestEnabled() */
    A3M_BOOL isStencilTestEnabled() const { return m_stencilTestEnabled; }

    /** Specifies the conditions under which a pixel passes the stencil test.
     * Different conditions can be provided for the both sides (faces) of a
     * polygon.  The condition is defined as:
     *
     *   function(reference & mask, stencil * & mask)
     *
     * For example, when using StencilFunction::STENCIL_LESS, the condition is:
     *
     *   (reference & mask) < (stencil * & mask)
     */
    void setStencilFunction(
      StencilFace face,
      StencilFunction function,
      A3M_INT32 reference,
      A3M_UINT32 mask);
    /** \see setStencilFunction() */
    StencilFunction getStencilFunction(StencilFace face) const
    { return m_stencilFunction[face]; }
    /** \see setStencilFunction() */
    A3M_INT32 getStencilReference(StencilFace face) const
    { return m_stencilReference[face]; }
    /** \see setStencilFunction() */
    A3M_UINT32 getStencilReferenceMask(StencilFace face) const
    { return m_stencilReferenceMask[face]; }

    /** Specifies what to do with a pixel given different outcomes of the
     * stencil and depth tests.
     * Different operations can be provided for the both sides (faces) of a
     * polygon.
     */
    void setStencilOperations(
      /** Which side of the polygon to apply the stencil operations to */
      StencilFace face,
      /** What happens when the stencil test fails */
      StencilOperation fail,
      /** What happens when the stencil test passes, but the depth test fails */
      StencilOperation passDepthFail,
      /** What happens when the stencil test passes, and the depth test passes */
      StencilOperation passDepthPass);

    /** \see setStencilOperations() */
    StencilOperation getStencilFail(StencilFace face) const
    { return m_stencilFail[face]; }
    /** \see setStencilOperations() */
    StencilOperation getStencilPassDepthFail(StencilFace face) const
    { return m_stencilPassDepthFail[face]; }
    /** \see setStencilOperations() */
    StencilOperation getStencilPassDepthPass(StencilFace face) const
    { return m_stencilPassDepthPass[face]; }

    /** Controls which bits of the stencil buffer can be written to.
     * Different masks can be specified for both sides (faces) of a polygon.
     */
    void setStencilMask(StencilFace face, A3M_UINT32 mask);
    /** \see setStencilMask() */
    A3M_UINT32 getStencilMask(StencilFace face) const { return m_stencilMask[face]; }

    /** The viewport defines a rectangle in pixels into which the scene is
     * drawn.
     */
    void setViewport(A3M_INT32 left, A3M_INT32 bottom, A3M_INT32 width, A3M_INT32 height);
    /** \see setViewport() */
    A3M_INT32 getViewportLeft() const { return m_viewportLeft; }
    /** \see setViewport() */
    A3M_INT32 getViewportBottom() const { return m_viewportBottom; }
    /** \see setViewport() */
    A3M_INT32 getViewportWidth() const { return m_viewportWidth; }
    /** \see setViewport() */
    A3M_INT32 getViewportHeight() const { return m_viewportHeight; }

    /** The value written to the colour buffer when calling clear().
     */
    void setClearColour(Colour4f const& colour);
    /** \see setClearColour() */
    Colour4f getClearColour() const { return m_clearColour; }

    /** The value written to the depth buffer when calling clear().
     */
    void setClearDepth(A3M_FLOAT depth);
    /** \see setClearDepth() */
    A3M_FLOAT getClearDepth() const { return m_clearDepth; }

    /** The value written to the stencil buffer when calling clear().
     */
    void setClearStencil(A3M_INT32 stencil);
    /** \see setClearStencil() */
    A3M_INT32 getClearStencil() const { return m_clearStencil; }

    /**
     * Clears the frame buffer.
     * \see setClearColour()
     * \see setClearDepth()
     * \see setClearStencil()
     */
    void clear();

  private:
    typedef std::map<std::string, ShaderUniformBase::Ptr> ShaderUniformMap;

    ShaderUniformMap m_uniforms; /* Uniforms mapped by name */

    A3M_BOOL m_blendEnabled;
    Colour4f m_blendColour;
    BlendFactor m_srcRgbBlendFactor;
    BlendFactor m_srcAlphaBlendFactor;
    BlendFactor m_dstRgbBlendFactor;
    BlendFactor m_dstAlphaBlendFactor;
    BlendFunction m_rgbBlendFunction;
    BlendFunction m_alphaBlendFunction;
    CullingMode m_cullingMode;
    WindingOrder m_windingOrder;
    A3M_FLOAT m_lineWidth;

    A3M_BOOL m_colourMaskR;
    A3M_BOOL m_colourMaskG;
    A3M_BOOL m_colourMaskB;
    A3M_BOOL m_colourMaskA;

    A3M_BOOL m_depthWriteEnabled;
    A3M_FLOAT m_depthRangeNear;
    A3M_FLOAT m_depthRangeFar;
    A3M_FLOAT m_depthOffsetFactor;
    A3M_FLOAT m_depthOffsetUnits;
    A3M_BOOL m_depthTestEnabled;
    DepthFunction m_depthTestFunction;

    A3M_BOOL m_scissorTestEnabled;
    A3M_INT32 m_scissorBoxLeft;
    A3M_INT32 m_scissorBoxBottom;
    A3M_INT32 m_scissorBoxWidth;
    A3M_INT32 m_scissorBoxHeight;

    A3M_BOOL m_stencilTestEnabled;
    StencilFunction m_stencilFunction[STENCIL_NUM_FACES];
    A3M_INT32  m_stencilReference[STENCIL_NUM_FACES];
    A3M_UINT32 m_stencilReferenceMask[STENCIL_NUM_FACES];
    StencilOperation  m_stencilFail[STENCIL_NUM_FACES];
    StencilOperation  m_stencilPassDepthFail[STENCIL_NUM_FACES];
    StencilOperation  m_stencilPassDepthPass[STENCIL_NUM_FACES];
    A3M_UINT32 m_stencilMask[STENCIL_NUM_FACES];

    A3M_INT32 m_viewportLeft;
    A3M_INT32 m_viewportBottom;
    A3M_INT32 m_viewportWidth;
    A3M_INT32 m_viewportHeight;

    Colour4f m_clearColour;
    A3M_FLOAT m_clearDepth;
    A3M_INT32 m_clearStencil;
  };

  /** Adds a named property to a render context by automatically detecting the
   * initial value's type.
   */
  template<typename T>
  void addProperty(
    RenderContext& context,
    /** Value to initialize the property to */
    T const& value,
    /** Name of the property to add */
    A3M_CHAR8 const* name)
  {
    context.addProperty(createUniform(value), name);
  }

  /** Adds a named property array of a given size to a render context.
   */
  template<typename T>
  void addPropertyArray(
    RenderContext& context,
    /** Size of the array */
    A3M_INT32 size,
    /** Name of the property to add */
    A3M_CHAR8 const* name)
  {
    context.addProperty(createUniformArray<T>(size), name);
  }

  /** Sets the conditions under which a pixel passes the stencil test, for both
   * faces at once.
   * \see RenderContext::setStencilFunction()
   */
  void setStencilFunction(
    RenderContext& context,
    StencilFunction function,
    A3M_INT32 reference,
    A3M_UINT32 mask);

  /** Specifies what to do with a pixel given different outcomes of the stencil
   * and depth tests, for both faces at once.
   * \see RenderContext::setStencilOperations()
   */
  void setStencilOperations(
    RenderContext& context,
    StencilOperation fail,
    StencilOperation passDepthFail,
    StencilOperation passDepthPass);

  /** Controls which bits of the stencil buffer can be written to for both, for
   * both faces at once.
   */
  void setStencilMask(RenderContext& context, A3M_UINT32 mask);

  /** @} */

} /* namespace a3m */

#endif /* A3M_RENDERCONTEXT_H */
