/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * TextureCubeLoader Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <string>                 /* for std::string                         */
#include <a3m/image.h>            /* for Image                               */
#include <a3m/log.h>              /* for logging                             */
#include <stdstringutility.h>     /* for endsWithI()                         */
#include <texturecubeloader.h>    /* This class's API                        */

namespace
{

  A3M_INT32 const FACE_COUNT = 6;
  A3M_INT32 const FACE_COUNT_X = 3;
  A3M_INT32 const FACE_COUNT_Y = 2;

  /*
   * Local function to calculate the pixel format based on the image channel
   * count returned by STB.
   */
  a3m::Texture::Format formatFromChannelCount( A3M_INT32 n )
  {
    static const a3m::Texture::Format format[] =
    {
      a3m::Texture::ALPHA,
      a3m::Texture::ALPHA,
      a3m::Texture::LUMINANCE_ALPHA,
      a3m::Texture::RGB,
      a3m::Texture::RGBA
    };

    return format[n];
  }

  /**
   * Splits a name into tokens using the semicolon as the separator.
   */
  A3M_BOOL getTextureCubeFaceNames(std::string name,
                                   std::string& positiveX, std::string& negativeX,
                                   std::string& positiveY, std::string& negativeY,
                                   std::string& positiveZ, std::string& negativeZ)
  {
    std::string* faces[] =
    {
      &positiveX, &negativeX,
      &positiveY, &negativeY,
      &positiveZ, &negativeZ
    };

    A3M_UINT32 start = 0;
    for (A3M_INT32 i = 0; i < FACE_COUNT; ++i)
    {
      A3M_UINT32 end = name.find(';', start);

      if (end == std::string::npos)
      {
        if (i == FACE_COUNT - 1)
        {
          end = name.length();
        }
        else
        {
          return A3M_FALSE;
        }
      }

      *(faces[i]) = name.substr(start, end - start);
      start = end + 1;
    }

    return A3M_TRUE;
  }

} /* anonymous namespace */

namespace a3m
{
  /*
   * Loads a new cube texture from the file system.
   */
  TextureCube::Ptr TextureCubeLoader::load(
    TextureCubeCache& cache,
    A3M_CHAR8 const* name)
  {
    TextureCube::Ptr texture;

    Image::Ptr faceImages[FACE_COUNT];

    // We have to check that these match for all face images.
    A3M_INT32 width = 0;
    A3M_INT32 height = 0;
    A3M_INT32 channelCount = 0;

    TextureCube::Face faceEnum[FACE_COUNT] =
    {
      TextureCube::POSITIVE_X,
      TextureCube::NEGATIVE_X,
      TextureCube::POSITIVE_Y,
      TextureCube::NEGATIVE_Y,
      TextureCube::POSITIVE_Z,
      TextureCube::NEGATIVE_Z
    };

    // If no semicolon is in name, assume this is a single image file.
    if (std::string(name).find(";") == std::string::npos)
    {
      // This is the order in which the faces appear on the image:
      //
      //  -------------
      //  | 0 | 1 | 2 |
      //  -------------
      //  | 3 | 4 | 5 |
      //  -------------
      //
      // This is how Blender does it.
      faceEnum[0] = TextureCube::NEGATIVE_X;
      faceEnum[1] = TextureCube::NEGATIVE_Z;
      faceEnum[2] = TextureCube::POSITIVE_X;
      faceEnum[3] = TextureCube::NEGATIVE_Y;
      faceEnum[4] = TextureCube::POSITIVE_Y;
      faceEnum[5] = TextureCube::POSITIVE_Z;

      Stream::Ptr stream = cache.getStream(name);

      if (!stream)
      {
        return texture;
      }

      Image::Ptr image(new Image(*stream));

      if (image->width() % FACE_COUNT_X != 0)
      {
        A3M_LOG_ERROR("Cube map \"%s\" width must be divisible by %d.",
                      name, FACE_COUNT_X);
        return texture;
      }

      if (image->height() % FACE_COUNT_Y != 0)
      {
        A3M_LOG_ERROR("Cube map \"%s\" height must be divisible by %d.",
                      name, FACE_COUNT_Y);
        return texture;
      }

      width = image->width() / FACE_COUNT_X;
      height = image->height() / FACE_COUNT_Y;
      channelCount = image->channelCount();

      for (A3M_INT32 x = 0; x < FACE_COUNT_X; ++x)
      {
        for (A3M_INT32 y = 0; y < FACE_COUNT_Y; ++y)
        {
          faceImages[FACE_COUNT_X * y + x] = crop(*image,
                                                  width * x,
                                                  height * y,
                                                  width,
                                                  height);
        }
      }
    }
    else // Otherwise, split the name into six names - one for each face.
    {
      std::string faceNames[FACE_COUNT];

      if (!getTextureCubeFaceNames(name,
                                   faceNames[0], faceNames[1],
                                   faceNames[2], faceNames[3],
                                   faceNames[4], faceNames[5]))
      {
        // Filenames were not successfully obtained, so return NULL pointer
        A3M_LOG_ERROR( "TextureCube name is invalid: %s", name );
        return texture;
      }

      // Load all the face images.
      for (A3M_INT32 i = 0; i < FACE_COUNT; ++i)
      {
        Stream::Ptr faceStream = cache.getStream(faceNames[i].c_str());

        // Make sure stream was found.
        if (!faceStream)
        {
          A3M_LOG_ERROR("TextureCube image \"%s\" not found.",
                        faceNames[i].c_str());
          return texture;
        }

        Image::Ptr image(new Image(*faceStream));

        // Make sure image loaded correctly.
        if (!image->data())
        {
          A3M_LOG_ERROR("TextureCube image \"%s\" failed to load.",
                        faceNames[i].c_str());
          return texture;
        }

        // Make sure all face images are the same size.
        if (i == 0)
        {
          width = image->width();
          height = image->height();
          channelCount = image->channelCount();
        }
        else if (
          image->width() != width ||
          image->height() != height ||
          image->channelCount() != channelCount)
        {
          A3M_LOG_ERROR("TextureCube images must have matching dimensions.");
          return texture;
        }

        faceImages[i] = image;
      }
    }

    // Create the cube texture.
    texture = cache.create(
                static_cast<A3M_UINT32>(width),
                static_cast<A3M_UINT32>(height),
                formatFromChannelCount(channelCount),
                Texture::UNSIGNED_BYTE,
                name);

    for (A3M_INT32 i = 0; i < FACE_COUNT; ++i)
    {
      texture->setFace(faceEnum[i], faceImages[i]->data());
    }

    return texture;
  }

  A3M_BOOL TextureCubeLoader::isKnown(A3M_CHAR8 const* name)
  {
    std::string str = name;
    return
      endsWithI(str, ".jpg") || endsWithI(str, ".jpeg") ||
      endsWithI(str, ".bmp") || endsWithI(str, ".png") ||
      endsWithI(str, ".tga");
  }

} /* namespace a3m */
