/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * AssetPath implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <cstring> /* strcmp */
#include <a3m/detail/assetpath.h> /* This class's API */

namespace a3m
{
  namespace detail
  {
    /*
     * Destructor
     */
    AssetPath::~AssetPath()
    {
      m_paths.clear();
    }

    /*
     * Adds a new asset location/path to the list of search paths.
     */
    A3M_BOOL AssetPath::add(A3M_CHAR8 const* path, A3M_BOOL archive)
    {
      StreamSource::Ptr source = StreamSource::get(path, archive);
      if (source)
      {
        m_paths.push_back(source);
        return A3M_TRUE;
      }
      // Given path is not valid, so return failure
      return A3M_FALSE;
    }

    /*
     * Adds a new asset stream to the list of search paths based on stream source.
     */
    A3M_BOOL AssetPath::add(StreamSource::Ptr const& streamSource)
    {
      m_paths.push_back(streamSource);
      return A3M_TRUE;
    }

    /*
     * Removes an asset location/path from the list of search paths.
     */
    A3M_BOOL AssetPath::remove(A3M_CHAR8 const* path)
    {
      for (PathList::iterator itr = m_paths.begin(); itr != m_paths.end(); ++itr)
      {
        if (strcmp((*itr)->getName(), path) == 0)
        {
          m_paths.erase(itr);
          return A3M_TRUE;
        }
      }
      // Could not find the path, so return failure
      return A3M_FALSE;
    }

    /*
     * Searches through the list of paths to locate the asset
     * with the specified name.  If successful, a valid Stream::Ptr
     * is returned from which the binary asset data can be read.
     */
    Stream::Ptr AssetPath::find(A3M_CHAR8 const* assetName) const
    {
      for (A3M_UINT32 i = 0; i < m_paths.size(); ++i)
      {
        if (m_paths[i]->exists(assetName))
        {
          return m_paths[i]->open(assetName);
        }
      }
      return Stream::Ptr();
    }
    /*
     * Set cache directory
     */
    void AssetPath::setCacheDirectory( A3M_CHAR8 const* dirName
                                       /** location of cached files */ )
    {
      m_cacheDirectory = dirName;
    }

    /*
     * Get cache directory
     */
    A3M_CHAR8 const* AssetPath::getCacheDirectory() const
    {
      return m_cacheDirectory.c_str();
    }

    std::string normalizeAssetName(A3M_CHAR8 const* name)
    {
      std::string normalizedName = name;

      // Windows does not like filenames in which there are colons, but colons
      // used to be used to indicate "namespaces" with memfiles.  Replace all
      // colons with hash character, which is used by genmemfiles script.
      if (normalizedName.compare(0, 4, "a3m:") == 0)
      {
        normalizedName[3] = '#';
        A3M_LOG_WARN("Use of \"a3m:\" to indicate asset namespace is "
                     "deprecated; use \"a3m#\" instead.");
      }
      else if (normalizedName.compare(0, 7, "ngin3d:") == 0)
      {
        normalizedName[6] = '#';
        A3M_LOG_WARN("Use of \"ngin3d:\" to indicate asset namespace is "
                     "deprecated; use \"ngin3d#\" instead.");
      }

      return normalizedName;
    }

  } /* namespace detail */

} /* namespace a3m */
