/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Image class
 *
 */
#ifndef A3M_IMAGE_H
#define A3M_IMAGE_H

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/pointer.h>          /* for SharedPtr                           */
#include <a3m/noncopyable.h>      /* for NonCopyable                         */
#include <a3m/stream.h>           /* for Stream                              */

namespace a3m
{
  /** \defgroup a3mImage Images
   * \ingroup  a3mRefAssets
   *
   * An Image stores uncompressed image data as a shared resource.
   * Unlike textures, which represent an OpenGL resource, images are simply
   * blocks of pixel data in a convenient form.
   *
   * @{
   */

  /**
   * Image data with a specific width, height and number of channels.
   * Images can either be constructed directly from pixel data, or can
   * be loaded from a stream.
   */
  class Image : public Shared, NonCopyable
  {
  public:
    A3M_NAME_SHARED_CLASS( Image )

    /** Smart pointer type for this class */
    typedef SharedPtr<Image> Ptr;

    /**
     * Contructs an image from an array of image data.
     * The data will be copied.
     */
    Image(
      A3M_INT32 width,
      /**< Image width in pixels */
      A3M_INT32 height,
      /**< Image height in pixels */
      A3M_INT32 channelCount,
      /**< Number of channels (bytes per pixel) */
      A3M_UINT8 const* data
      /**< Image data (if null, image is uninitialized) */ = 0);

    /**
     * Loads an image from a stream.
     */
    Image(Stream& stream /**< Data stream */);

    /**
     * Destructor.
     */
    ~Image();

    /**
     * Returns the width of the image.
     * \return Width in pixels
     */
    A3M_INT32 width() const;

    /**
     * Returns the height of the image.
     * \return Height in pixels
     */
    A3M_INT32 height() const;

    /**
     * Returns the number of channels in the image.
     * \return Number of channels
     */
    A3M_INT32 channelCount() const;

    /**
     * Returns mutable image data, or null if image is not valid.
     * \return Image data array or null
     */
    A3M_UINT8* data();

    /**
     * Returns immutable image data, or null if image is not valid.
     * \return Image data array or null
     */
    A3M_UINT8 const* data() const;

  private:
    A3M_BOOL m_usingStb;
    A3M_INT32 m_width;
    A3M_INT32 m_height;
    A3M_INT32 m_channelCount;
    A3M_UINT8* m_data;
  };

  /** @} */

  /**
   * Creates a cropped version of an image.
   * \return Cropped image
   */
  Image::Ptr crop(
    Image const& source,
    /**< Image to use as input */
    A3M_INT32 left,
    /**< Number of pixels from the left of the image to crop */
    A3M_INT32 top,
    /**< Number of pixels from the top of the image to crop */
    A3M_INT32 width,
    /**< Width of the cropped image */
    A3M_INT32 height
    /**< Height of the cropped image */);

} /* namespace a3m */

#endif /* A3M_IMAGE_H */
