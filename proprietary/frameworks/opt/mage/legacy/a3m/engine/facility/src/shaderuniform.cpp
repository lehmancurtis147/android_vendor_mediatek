/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * ShaderUniform Implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/shaderuniform.h>               /* This class's API            */
#include <GLES2/gl2.h>                        /* for glGenBuffers etc.       */
#include <error.h>                       /* for CHECK_GL_ERROR          */
#include <a3m/log.h>                          /* A3M log API                 */

namespace
{

  /*
   * Generates an array containing sequential integers from 0 to
   * GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS - 1.
   */
  std::vector<A3M_INT32> generateTextureUnits()
  {
    A3M_INT32 size = 0;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &size);
    CHECK_GL_ERROR;

    std::vector<A3M_INT32> units(size);
    for (A3M_INT32 i = 0; i < size; ++i)
    {
      units[i] = i;
    }

    return units;
  }

  /*
   * Returns an array containing all valid texture unit indices.
   */
  A3M_INT32 const* getTextureUnits()
  {
    static std::vector<A3M_INT32> units = generateTextureUnits();
    return &units[0];
  }

  template< typename T >
  void castToGLint( GLint* dest, const T* src, A3M_INT32 size )
  {
    for (A3M_INT32 i = 0; i < size; ++i)
    {
      dest[i] = static_cast< GLint >( src[i] );
    }
  }

} // namespace

namespace a3m
{
  namespace ShaderUniformPrivate
  {
    /*
     * setUniform overloads for each supported type.
     */

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const A3M_BOOL* value )
    {
      (void)texUnit;
      GLint* valueGl = new GLint[size];
      castToGLint( valueGl, value, size );
      glUniform1iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector2<A3M_BOOL>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 2;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform2iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector3<A3M_BOOL>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 3;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform3iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector4<A3M_BOOL>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 4;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform4iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const A3M_UINT8* value )
    {
      (void)texUnit;
      GLint* array = new GLint[size];
      castToGLint( array, value, size );
      glUniform1iv( location, size, array );
      delete[] array;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector2<A3M_UINT8>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 2;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform2iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector3<A3M_UINT8>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 3;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform3iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector4<A3M_UINT8>* value )
    {
      (void)texUnit;
      A3M_INT32 const arraySize = size * 4;
      GLint* valueGl = new GLint[arraySize];
      castToGLint( valueGl, &value[0][0], arraySize );
      glUniform4iv( location, size, valueGl );
      delete[] valueGl;
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const A3M_INT32* value )
    {
      (void)texUnit;
      glUniform1iv( location, size, value );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector2<A3M_INT32>* value )
    {
      (void)texUnit;
      glUniform2iv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector3<A3M_INT32>* value )
    {
      (void)texUnit;
      glUniform3iv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector4<A3M_INT32>* value )
    {
      (void)texUnit;
      glUniform4iv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const A3M_FLOAT* value )
    {
      (void)texUnit;
      glUniform1fv( location, size, value );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector2<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniform2fv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector3<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniform3fv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Vector4<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniform4fv( location, size, &value[0][0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Matrix2<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniformMatrix2fv( location, size, GL_FALSE, &value[0].i[0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Matrix3<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniformMatrix3fv( location, size, GL_FALSE, &value[0].i[0] );
      CHECK_GL_ERROR;
    }

    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     const a3m::Matrix4<A3M_FLOAT>* value )
    {
      (void)texUnit;
      glUniformMatrix4fv( location, size, GL_FALSE, &value[0].i[0] );
      CHECK_GL_ERROR;
    }

    /*
     * Overloaded version for Texture2D.
     * Associates all textures in the uniform array with sequential texture
     * units, and then sets the sampler uniform array accordingly.
     */
    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     Texture2D::Ptr const* texture2D )
    {
      for ( A3M_INT32 i = 0; i < size; ++i )
      {
        glActiveTexture( static_cast<A3M_UINT32>(GL_TEXTURE0 + texUnit + i) );
        CHECK_GL_ERROR;
        if( *texture2D ) { (*texture2D)->enable(); }
      }

      setUniform( location, -1, size, &getTextureUnits()[texUnit] );
    }

    /*
     * Overloaded version for TextureCube.
     * Associates all textures in the uniform array with sequential texture
     * units, and then sets the sampler uniform array accordingly.
     */
    void setUniform( A3M_INT32 location, A3M_INT32 texUnit, A3M_INT32 size,
                     TextureCube::Ptr const* textureCube )
    {
      for ( A3M_INT32 i = 0; i < size; ++i )
      {
        glActiveTexture( static_cast<A3M_UINT32>(GL_TEXTURE0 + texUnit + i) );
        CHECK_GL_ERROR;
        if( *textureCube ) { (*textureCube)->enable(); }
      }

      setUniform( location, -1, size, &getTextureUnits()[texUnit] );
    }

  } /* namespace ShaderUniformPrivate */

} /* namespace a3m */
