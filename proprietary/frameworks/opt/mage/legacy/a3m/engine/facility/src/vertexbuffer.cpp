/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * VertexBuffer Implementaion
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <GLES2/gl2.h>          /* for glGenBuffers etc. */
#include <a3m/log.h>            /* A3M log API */
#include <error.h>              /* for CHECK_GL_ERROR */
#include <a3m/vertexbuffer.h>   /* VertexBuffer */

//lint -e429 custodial pointer (tempData) not freed or returned (see createTempData)

namespace
{
  /*
   * Convert a a3m attribute format to an OpenGL type
   */
  GLenum gLtypeFromFormat( a3m::VertexBuffer::AttribFormat format )
  {
    switch( format )
    {
    case a3m::VertexBuffer::ATTRIB_FORMAT_INT8:
      return GL_BYTE;
    case a3m::VertexBuffer::ATTRIB_FORMAT_UINT8:
      return GL_UNSIGNED_BYTE;
    case a3m::VertexBuffer::ATTRIB_FORMAT_INT16:
      return GL_SHORT;
    case a3m::VertexBuffer::ATTRIB_FORMAT_UINT16:
      return GL_UNSIGNED_SHORT;
    case a3m::VertexBuffer::ATTRIB_FORMAT_FLOAT:
      return GL_FLOAT;
    case a3m::VertexBuffer::ATTRIB_FORMAT_FIXED:
      return GL_FIXED;
      /* This is only available as an extension - not yet supported
      case a3m::VertexBuffer::ATTRIB_FORMAT_HALF_FLOAT:
        return GL_HALF_FLOAT_OES;
      */
    }
    return 0;
  }
}

namespace a3m
{
  /* VertexBuffer::Attrib holds all the data for a particular attribute */
  struct VertexBuffer::Attrib
  {
    /* This name should match a shader attribute name */
    std::string name;
    /* OpenGL type of the data held in corresponding array */
    GLenum type;
    /* Array containing data for attribute */
    VertexArray::Ptr array;
    /* True if the attribute data should be normalized */
    A3M_BOOL normalize;
    /* Data either points to VertexArray data or is the offset into VBO */
    void const* data;
    /* Number of components per vertex */
    A3M_INT32 componentCount;
    /* Offset from begining of one vertex to begining of the next or 0 for
     * contiguous data */
    A3M_INT32 stride;
    /* Flag indicates whether attribute array should be copied to GPU memory */
    VertexBuffer::AttribUsage usage;

    /* Next attribute in the linked list */
    VertexBuffer::Attrib* next;
  };

  /*********************
   * VertexBufferCache *
   *********************/

  VertexBuffer::Ptr VertexBufferCache::create(A3M_CHAR8 const* name)
  {
    // Create underlying OpenGL buffer and track with resource cache
    detail::BufferResource::Ptr resource(new detail::BufferResource());
    getResourceCache()->add(resource);

    // Create new asset and add to cache
    VertexBuffer::Ptr vertexBuffer(new VertexBuffer(resource));
    add(vertexBuffer, name);

    return vertexBuffer;
  }

  /****************
   * VertexBuffer *
   ****************/

  /*
   * Constructor
   */
  VertexBuffer::VertexBuffer(detail::BufferResource::Ptr const& resource) :
    m_firstAttrib( 0 ),
    m_vertexCount( 0 ),
    m_rawData( 0 ),
    m_resource( resource )
  {
  }

  /*
   * Destructor
   */
  VertexBuffer::~VertexBuffer()
  {
    while( m_firstAttrib )
    {
      Attrib* next = m_firstAttrib->next;
      delete m_firstAttrib;
      m_firstAttrib = next;
    }

    if (m_rawData) { delete [] m_rawData; }
  }

  /*
   * Add an attribute to the buffer
   */
  void VertexBuffer::addAttrib( VertexArray::Ptr const& va,
                                const A3M_CHAR8* name,
                                AttribFormat format,
                                AttribUsage usage,
                                A3M_BOOL normalize )
  {
    Attrib* att = new VertexBuffer::Attrib;

    /* link new attribute into our list (at begining) */
    att->next = m_firstAttrib;
    m_firstAttrib = att;

    att->name = name;
    att->type = gLtypeFromFormat( format );
    att->array = va;
    att->normalize = normalize;
    att->data = va->voidData();
    att->componentCount = va->componentCount();
    att->stride = 0;
    att->usage = usage;
  }

  /*
   * Enable an attribute in the buffer
   */
  A3M_BOOL VertexBuffer::enableAttrib( A3M_UINT32 index,
                                       A3M_CHAR8 const* name )
  {
    for( Attrib* att = m_firstAttrib; att; att = att->next )
    {
      if( att->name == name )
      {
        if( m_resource->getId() && ( att->usage == ATTRIB_USAGE_WRITE_ONCE ) )
        {
          glBindBuffer( GL_ARRAY_BUFFER, m_resource->getId() );
          CHECK_GL_ERROR;
        }
        else
        {
          glBindBuffer( GL_ARRAY_BUFFER, 0 );
          CHECK_GL_ERROR;
        }

        glEnableVertexAttribArray( index );
        glVertexAttribPointer( index, att->componentCount, att->type,
                               static_cast< GLboolean >( att->normalize ),
                               att->stride, att->data );
        CHECK_GL_ERROR;
        return A3M_TRUE;
      }
    }
    glDisableVertexAttribArray( index );
    CHECK_GL_ERROR;
    /* Set some default value here ? */
    return A3M_FALSE;
  }

  /*
   * Commit the buffer
   */
  void VertexBuffer::commit()
  {
    A3M_UINT32 size = 0;
    A3M_INT32 stride = 0;
    getSizeAndStride(size, stride);

    A3M_ASSERT(stride != 0);
    m_vertexCount = size / stride;

    if( size == 0 ) { return; } /* no immutable attributes to store */

    /* Create buffer */
    if (!m_resource->allocate()) { return; }

    /* Make it current */
    glBindBuffer( GL_ARRAY_BUFFER, m_resource->getId() );
    CHECK_GL_ERROR;

    /*
     * Interleave the vertex data using a temporary buffer. If this turns
     * out to be too costly we could use the version commented out below
     */
    A3M_BYTE* tempData = new A3M_BYTE[ static_cast< size_t >( size ) ];
    /* if allocating the temporary buffer fails, delete the VBO and
     * fall back to using array data */
    if( !tempData )
    {
      A3M_LOG_ERROR( "Unable to create temporary buffer", 0 );
      m_resource->deallocate();
      return;
    }

    createTempData(size, stride, tempData);

    glBufferData( GL_ARRAY_BUFFER,
                  static_cast<GLsizei>(size),
                  tempData, GL_STATIC_DRAW );
    CHECK_GL_ERROR;

    delete [] tempData;

    return;

    /* Non-interleaved version */
    /* It may turn out that interleaving the vertex data is not worth the
     * extra cost in time or memory. In future we might have usage hints
     * that help this function decide which strategy to use.

    glBufferData( GL_ARRAY_BUFFER, size, 0, GL_STATIC_DRAW );
    CHECK_GL_ERROR;

    A3M_INT32 offset = 0;
    for( Attrib *att = m_firstAttrib; att; att = att->next )
    {
      // only copy attributes that are not going to change
      if( att->usage == ATTRIB_USAGE_WRITE_ONCE )
      {
        glBufferSubData( GL_ARRAY_BUFFER, offset,
                         att->array->arraySizeInBytes(), att->data );
        CHECK_GL_ERROR;
        att->data = reinterpret_cast< void * >( offset );
        offset += att->array->arraySizeInBytes();
        att->array = VertexArray::Ptr();
      }
    }
    */
  }

  /*
   *
   */
  void VertexBuffer::getSizeAndStride(A3M_UINT32& size, A3M_INT32& stride)
  {
    size = 0;
    stride = 0;

    for( Attrib* att = m_firstAttrib; att; att = att->next )
    {
      /* Only count attributes that are not going to change */
      if( att->usage == ATTRIB_USAGE_WRITE_ONCE )
      {
        size += att->array->arraySizeInBytes();
        stride += att->array->componentCount() * att->array->typeSize();
      }
    }
  }

  /*
   *
   */
  void VertexBuffer::createTempData(A3M_UINT32 size,
                                    A3M_INT32 stride,
                                    A3M_BYTE* tempData)
  {
    A3M_PARAM_NOT_USED(size);

    A3M_INT32 offset = 0;
    for( Attrib* att = m_firstAttrib; att; att = att->next )
    {
      /* Only copy attributes that are not going to change */
      if( att->usage == ATTRIB_USAGE_WRITE_ONCE )
      {
        att->stride = stride;
        /* att->data currently points to data in the VertexArray */
        A3M_BYTE const* source = static_cast< A3M_BYTE const* >( att->data );
        /* now we set att->data as an offset into the VBO */
        att->data = reinterpret_cast< void* >( offset );
        A3M_BYTE* dest = tempData + offset;
        A3M_INT32 componentSize = att->array->componentCount() *
                                  att->array->typeSize();

        for( A3M_INT32 i = 0; i != att->array->vertexCount(); ++i )
        {
          memcpy( dest, source, static_cast< size_t >( componentSize ) );
          source += componentSize;
          dest += stride;
        }
        offset += att->array->componentCount() * att->array->typeSize();
        att->array = VertexArray::Ptr();
      }
    }
  }

  /*
   *
   */
  A3M_BOOL VertexBuffer::save(Stream::Ptr const& outStream)
  {
    if (m_resource->getId())
    {
      A3M_LOG_ERROR( "Can't write buffer to file (already committed).", 0 );
      return A3M_FALSE;
    }

    // duplicate buffer data
    A3M_UINT32 size = 0;
    A3M_INT32 stride = 0;
    getSizeAndStride(size, stride);

    A3M_INT32 attribCount = 0;
    for (Attrib* att = m_firstAttrib; att; att = att->next)
    {
      attribCount++;
    }

    outStream->write(&size,  sizeof(size  ));
    outStream->write(&stride, sizeof(stride));
    outStream->write(&attribCount, sizeof(attribCount));

    A3M_BYTE* tempData = new A3M_BYTE[ static_cast<size_t>( size ) ];
    A3M_ASSERT( tempData );
    createTempData(size, stride, tempData);

    for( Attrib* att = m_firstAttrib; att; att = att->next )
    {
      /* the call to createTempData sets the attrib values
       */
      outStream->write(att, sizeof(Attrib));
    }

    outStream->write(tempData, size);

    delete [] tempData;

    return A3M_TRUE;
  }

  /*
   *
   */
  A3M_BOOL VertexBuffer::load(Stream::Ptr const& inStream)
  {
    A3M_UINT32 size;
    A3M_INT32 stride;
    A3M_INT32 attribCount;

    inStream->read(&size,       sizeof(size));
    inStream->read(&stride,     sizeof(stride));
    inStream->read(&attribCount, sizeof(attribCount));

    A3M_ASSERT(stride != 0);
    m_vertexCount = size / stride;

    for (A3M_INT32 i = 0; i < attribCount; ++i)
    {
      Attrib* attrib = new VertexBuffer::Attrib;
      inStream->read(attrib, sizeof(VertexBuffer::Attrib));

      // link in to the list
      attrib->next = m_firstAttrib;
      m_firstAttrib = attrib;
    }

    // reverse the attribute list
    Attrib* tmp;
    Attrib* prev = 0;
    Attrib* ptr = m_firstAttrib;
    while (ptr)
    {
      tmp = ptr->next;
      ptr->next = prev;
      prev = ptr;
      ptr = tmp;
    }
    m_firstAttrib = prev;

    //===========================================
    //
    // perform commit
    //

    if( size == 0 ) { return A3M_FALSE; } /* no immutable attributes to store */

    /* Create buffer */
    if (!m_resource->allocate()) { return A3M_FALSE; }

    /* Make it current */
    glBindBuffer( GL_ARRAY_BUFFER, m_resource->getId() );
    CHECK_GL_ERROR;

    A3M_BYTE* tempData = new A3M_BYTE[ static_cast<size_t>( size ) ];
    A3M_ASSERT( tempData );
    inStream->read( tempData, size );

    glBufferData( GL_ARRAY_BUFFER,
                  static_cast<GLsizei>(size),
                  tempData, GL_STATIC_DRAW );
    CHECK_GL_ERROR;

    delete [] tempData;

    return A3M_TRUE;
  }

  /*
   * Set vertex buffer data for all attributes
   */
  void VertexBuffer::setAllAttributes( AttribDescription const* attributes,
                                       A3M_UINT32 attributeCount,
                                       A3M_BYTE const* data,
                                       A3M_UINT32 dataSize )
  {
    /* Create buffer */
    if (!m_resource->allocate()) { return; }

    /* Make it current */
    glBindBuffer( GL_ARRAY_BUFFER, m_resource->getId() );
    CHECK_GL_ERROR;

    m_vertexCount = 0;

    // copy attribute information
    Attrib** attrib = &m_firstAttrib;
    while( attributeCount-- )
    {
      *attrib = new Attrib;
      (*attrib)->name = attributes->name;
      (*attrib)->type = gLtypeFromFormat( attributes->type );
      (*attrib)->data = static_cast< A3M_BYTE* >( 0 ) + attributes->offset;
      (*attrib)->componentCount = attributes->componentCount;
      (*attrib)->stride = attributes->stride;
      (*attrib)->usage = ATTRIB_USAGE_WRITE_ONCE;
      (*attrib)->normalize = attributes->normalize;

      A3M_ASSERT((*attrib)->stride != 0);
      m_vertexCount = std::max<A3M_INT32>(m_vertexCount, dataSize / (*attrib)->stride);

      attrib = &( (*attrib)->next );
      ++attributes;
    }
    *attrib = 0;

    glBufferData( GL_ARRAY_BUFFER,
                  static_cast<GLsizei>( dataSize ),
                  data, GL_STATIC_DRAW );
    CHECK_GL_ERROR;
  }

} /* namespace a3m */
