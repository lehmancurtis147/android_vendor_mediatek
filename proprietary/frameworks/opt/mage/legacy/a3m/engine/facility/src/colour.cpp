/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * Colour Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/colour.h>   // a3m::Colour

namespace a3m
{
  const Colour4f Colour4f::RED(1.f, 0.f, 0.f, 1.f);
  const Colour4f Colour4f::GREEN(0.f, 1.f, 0.f, 1.f);
  const Colour4f Colour4f::BLUE(0.f, 0.f, 1.f, 1.f);
  const Colour4f Colour4f::WHITE(1.f, 1.f, 1.f, 1.f);
  const Colour4f Colour4f::BLACK(0.f, 0.f, 0.f, 1.f);
  const Colour4f Colour4f::LIGHT_GREY(0.67f, 0.67f, 0.67f, 1.f);
  const Colour4f Colour4f::DARK_GREY(0.33f, 0.33f, 0.33f, 1.f);

  /*
   * Constructor: initialises local member variables
   * with the supplied values
   */
  Colour4f::Colour4f(A3M_FLOAT r, A3M_FLOAT g, A3M_FLOAT b, A3M_FLOAT a) :
    r(r),
    g(g),
    b(b),
    a(a)
  {
  }

  /*
   * Sets colour with byte vales
   */
  void Colour4f::setColour( A3M_UINT8 rbyte,
                            A3M_UINT8 gbyte,
                            A3M_UINT8 bbyte,
                            A3M_UINT8 abyte )
  {
    r = static_cast<A3M_FLOAT>(rbyte) / 255.0f;
    g = static_cast<A3M_FLOAT>(gbyte) / 255.0f;
    b = static_cast<A3M_FLOAT>(bbyte) / 255.0f;
    a = static_cast<A3M_FLOAT>(abyte) / 255.0f;
  }

  /*
   * Scales the float values of the colour components
   */
  void Colour4f::scale(A3M_FLOAT scalar /**< amount to scale colour */)
  {
    r *= scalar;
    g *= scalar;
    b *= scalar;
    a *= scalar;
  }

  /*
   * Component-wise addition for Colour4f.
   */
  Colour4f operator+( Colour4f const& lhs,
                      Colour4f const& rhs)
  {
    return Colour4f( lhs.r + rhs.r,
                     lhs.g + rhs.g,
                     lhs.b + rhs.b,
                     lhs.a + rhs.a );
  }

  /** Component-wise subtract for Colour4f.
   */
  Colour4f operator-( Colour4f const& lhs,
                      Colour4f const& rhs )
  {
    return Colour4f( lhs.r - rhs.r,
                     lhs.g - rhs.g,
                     lhs.b - rhs.b,
                     lhs.a - rhs.a );
  }

  /** Component-wise multiply for Colour4f.
   */
  Colour4f operator*( Colour4f const& lhs,
                      Colour4f const& rhs )
  {
    return Colour4f( lhs.r * rhs.r,
                     lhs.g * rhs.g,
                     lhs.b * rhs.b,
                     lhs.a * rhs.a );
  }

  /** Component-wise divide for Colour4f.
   */
  Colour4f operator/( Colour4f const& lhs,
                      Colour4f const& rhs )
  {
    return Colour4f( lhs.r / rhs.r,
                     lhs.g / rhs.g,
                     lhs.b / rhs.b,
                     lhs.a / rhs.a );
  }

} /* namespace a3m */
