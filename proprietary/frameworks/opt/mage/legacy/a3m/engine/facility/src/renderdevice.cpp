/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 *
 * Copyright (c) 2011 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 *****************************************************************************/
/** \file
 * RenderDevice Implementation
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/renderdevice.h>     /* Render device API declaration          */
#include <GLES2/gl2.h>             /* OpenGL ES 2.0 functions and constants  */
#include <a3m/indexbuffer.h>      /* a3m::IndexBuffer::draw()               */
#include <a3m/background.h>       /* a3m::Background::enable()              */
#include <a3m/appearance.h>       /* a3m::Appearance interface              */
#include <error.h>            /* for CHECK_GL_ERROR macro               */
#include <a3m/log.h>               /* A3M log API                            */

/*****************************************************************************
 * Namespace a3m
 *****************************************************************************/
namespace a3m
{
  /***************************************************************************
   * Namespace RenderDevice defined within a3m.
   * It defines rendering functions declared in renderdevice.h.
   ***************************************************************************/
  namespace RenderDevice
  {
    /************************************************************************
     * Functions
     ************************************************************************/

    /*
     * Retrieves the last error reported by the device as an enumerated error
     * code.
     */
    ErrorCode getError(A3M_BOOL cached)
    {
      ErrorCode error;

      switch (glGetError())
      {
      case GL_NO_ERROR:
        error = NO_ERROR;
        break;

      case GL_INVALID_ENUM:
        error = INVALID_ENUM;
        break;

      case GL_INVALID_VALUE:
        error = INVALID_VALUE;
        break;

      case GL_INVALID_OPERATION:
        error = INVALID_OPERATION;
        break;

      case GL_INVALID_FRAMEBUFFER_OPERATION:
        error = INVALID_FRAMEBUFFER_OPERATION;
        break;

      case GL_OUT_OF_MEMORY:
        error = OUT_OF_MEMORY;
        break;

      default:
        error = UNDEFINED_ERROR;
        break;
      }

      // Last OpenGL error may be cached instead of cleared.
      static RenderDevice::ErrorCode s_lastError = RenderDevice::NO_ERROR;

      if (cached)
      {
        // If no error has occurred since the last time this function was
        // called, return the cached error.
        if (error == NO_ERROR)
        {
          error = s_lastError;
        }

        // Clear the cached error.
        s_lastError = NO_ERROR;
      }
      else
      {
        // If an error has occurred since the last time this function was
        // called, cache the error.
        if (error != NO_ERROR)
        {
          s_lastError = error;
        }
      }

      return error;
    }

    /*
     * Returns a human-readable string corresponding to the given error code.
     */
    A3M_CHAR8 const* getErrorString(ErrorCode error)
    {
      switch (error)
      {
      case NO_ERROR:
        return "No error";

      case INVALID_ENUM:
        return "Invalid enum";

      case INVALID_VALUE:
        return "Invalid value";

      case INVALID_OPERATION:
        return "Invalid operation";

      case INVALID_FRAMEBUFFER_OPERATION:
        return "Invalid framebuffer operation";

      case OUT_OF_MEMORY:
        return "Out of memory";

      default:
        return "Undefined error";
      }
    }

    /*
     * Retrieves the vendor name for this device
     * as a string.
     *
     * Returns pointer to NULL-terminated Vendor name string.
     */
    const A3M_CHAR8* getVendor()
    {
      return (reinterpret_cast<const A3M_CHAR8*>(glGetString(GL_VENDOR)));
    }

    /*
     * Retrieves the version for this device
     * as a string.
     *
     * Returns pointer to NULL-terminated version string.
     */
    const A3M_CHAR8* getVersion()
    {
      return (reinterpret_cast<const A3M_CHAR8*>(glGetString(GL_VERSION)));
    }

    A3M_INT32 getMaxVertexUniformVectors()
    {
      A3M_INT32 value;
      glGetIntegerv(GL_MAX_VERTEX_UNIFORM_VECTORS, &value);
      return value;
    }

    A3M_INT32 getMaxFragmentUniformVectors()
    {
      A3M_INT32 value;
      glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_VECTORS, &value);
      return value;
    }

    A3M_INT32 getMaxVertexAttributeVectors()
    {
      A3M_INT32 value;
      glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &value);
      return value;
    }

    A3M_INT32 getMaxVaryingVectors()
    {
      A3M_INT32 value;
      glGetIntegerv(GL_MAX_VARYING_VECTORS, &value);
      return value;
    }

    /*
     * Retrieves the extensions for this device
     * as a string. All extension entries are space
     * separated.
     *
     * Returns pointer to NULL-terminated extension string.
     */
    const A3M_CHAR8* getExtensions()
    {
      return (reinterpret_cast<const A3M_CHAR8*>(glGetString(GL_EXTENSIONS)));
    }

    /*
     * Looks up whether an OpenGL extension is supported.
     */
    A3M_BOOL isExtensionSupported(A3M_CHAR8 const* name)
    {
      // Extensions is a space-delimited list of extension names.
      std::string extensions = getExtensions();

      for (A3M_UINT32 start = extensions.find(name);
           start != std::string::npos;
           start = extensions.find(name, start + 1))
      {
        A3M_UINT32 end = start + strlen(name);

        // Check that the string is not part of a sub-string.
        A3M_CHAR8 leading = ' ';

        if (start > 0)
        {
          leading = extensions[start - 1];
        }

        A3M_CHAR8 trailing = ' ';

        if (end < extensions.length())
        {
          trailing = extensions[end];
        }

        if (leading == ' ' && trailing == ' ')
        {
          return A3M_TRUE;
        }
      }

      return A3M_FALSE;
    }

    /*
     * Retrieves the shader language version supported by
     * this device as a string.
     *
     * Returns pointer to NULL-terminated shader (language) version string.
     */
    const A3M_CHAR8* getShaderVersion()
    {
      return (reinterpret_cast<const A3M_CHAR8*>(
                glGetString(GL_SHADING_LANGUAGE_VERSION)));
    }

    /*
     * get pixels from frame buffer
     */
    void getPixels(
      A3M_INT32 left, A3M_INT32 bottom,
      A3M_UINT32 width, A3M_UINT32 height,
      A3M_UINT8* pixels)
    {
      /* Note that the OpenGL ES 2.0 Programming Guide specifies GL_RGB
       * (not GL_RGBA) as the minimally supported format. This is incorrect.
       * ref: http://www.khronos.org/opengles/documentation/opengles1_0/html/...
       *      ...glReadPixels.html
       */
      glReadPixels( left, bottom,
                    static_cast< GLsizei >( width ),
                    static_cast< GLsizei >( height ),
                    GL_RGBA,
                    GL_UNSIGNED_BYTE,
                    pixels );
      CHECK_GL_ERROR;
    }

  } /* namespace RenderDevice */

} /* namespace a3m */
