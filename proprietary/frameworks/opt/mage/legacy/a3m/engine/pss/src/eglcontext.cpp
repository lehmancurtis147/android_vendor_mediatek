/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2013. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/**
 * EGL context wrapper.
 *
 */

/*****************************************************************************
 * Include Files
 *****************************************************************************/
#include <a3m/log.h>                  /* A3M log functions                   */
#include <eglcontext.h>               /* this module's header                */

namespace
{
  EGLint const CONFIG_ATTRIBS[] =
  {
    EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
    EGL_RED_SIZE,       8,
    EGL_GREEN_SIZE,     8,
    EGL_BLUE_SIZE,      8,
    EGL_ALPHA_SIZE,     8,
    EGL_DEPTH_SIZE,     16,
    EGL_STENCIL_SIZE,   8,
    EGL_SURFACE_TYPE,   EGL_WINDOW_BIT,
    EGL_SAMPLE_BUFFERS, 1,
    EGL_SAMPLES, 4,
    EGL_NONE
  };

  EGLint const CONTEXT_ATTRIBS[] =
  {
    EGL_CONTEXT_CLIENT_VERSION,
    2,
    EGL_NONE,
    EGL_NONE
  };

  /*
   * Check for EGL errors
   */
  void checkEglErrors()
  {
    EGLint error = eglGetError();

    if (error != EGL_SUCCESS)
    {
      A3M_LOG_ERROR("EGL Error: 0x%04x", error);
    }
  }

} // namespace

namespace a3m
{

  /*
   * Initialise EGL and create rendering context
   */
  EglContext::EglContext(
    NativeDisplayType nativeDisplay, NativeWindowType nativeWindow) :
    m_display(EGL_NO_DISPLAY),
    m_context(EGL_NO_CONTEXT),
    m_surface(EGL_NO_SURFACE)
  {
    m_display = eglGetDisplay(nativeDisplay);

    if (m_display == EGL_NO_DISPLAY)
    {
      A3M_LOG_ERROR("No EGL display connection available.");
      return;
    }

    EGLint majorVersion;
    EGLint minorVersion;

    if (!eglInitialize(m_display, &majorVersion, &minorVersion))
    {
      A3M_LOG_ERROR("Failed to initialize EGL display connection.");
      checkEglErrors();
      return;
    }

    A3M_LOG_INFO("Initialized EGL version %d.%d.", majorVersion, minorVersion);

    EGLint numConfigs;

    if (!eglGetConfigs(m_display, NULL, 0, &numConfigs))
    {
      A3M_LOG_ERROR("Failed to acquire EGL configuration.");
      checkEglErrors();
      return;
    }

    if (!eglChooseConfig(m_display, CONFIG_ATTRIBS, &m_config, 1, &numConfigs))
    {
      A3M_LOG_ERROR("Failed to set EGL configuration.");
      checkEglErrors();
      return;
    }

    m_surface = eglCreateWindowSurface(m_display, m_config, nativeWindow, 0);

    if (m_surface == EGL_NO_SURFACE)
    {
      A3M_LOG_ERROR("Failed to create EGL window surface.");
      checkEglErrors();
      return;
    }

    m_context = eglCreateContext(m_display, m_config, 0, CONTEXT_ATTRIBS);

    if (m_context == EGL_NO_CONTEXT)
    {
      A3M_LOG_ERROR("Failed to create EGL context.");
      checkEglErrors();
      return;
    }

    if (!eglMakeCurrent(m_display, m_surface, m_surface, m_context))
    {
      A3M_LOG_ERROR("Failed to attach EGL context.");
      checkEglErrors();
      return;
    }

    if (!eglSwapInterval(m_display, 1))
    {
      A3M_LOG_ERROR("Failed to swap interval buffer.");
      checkEglErrors();
      return;
    }
  }

  /*
   * Shut down EGL
   */
  EglContext::~EglContext(void)
  {
    if (m_display != EGL_NO_DISPLAY)
    {
      if (!eglMakeCurrent(m_display, 0, 0, 0))
      {
        A3M_LOG_ERROR("Failed to detach EGL context.");
      }

      if (m_surface != EGL_NO_SURFACE)
      {
        if (m_context)
        {
          if (!eglDestroyContext(m_display, m_context))
          {
            A3M_LOG_ERROR("Failed to destroy EGL context.");
          }
        }

        if (!eglDestroySurface(m_display, m_surface))
        {
          A3M_LOG_ERROR("Failed to destroy EGL surface.");
        }
      }

      if (!eglTerminate(m_display))
      {
        A3M_LOG_ERROR("Failed to terminate EGL display connection.");
      }
    }
  }

  void EglContext::swapBuffers()
  {
    if (!eglSwapBuffers(m_display, m_surface))
    {
      A3M_LOG_ERROR("Failed to swap frame buffers.");
    }
  }

  /*
   * Whether the context was successfully created.
   */
  A3M_BOOL EglContext::isValid() const
  {
    return m_context != EGL_NO_CONTEXT;
  }

} // namespace a3m
