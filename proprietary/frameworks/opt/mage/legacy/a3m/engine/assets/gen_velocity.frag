/**************************************************************************
 *
 * Copyright (c) 2013 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Fragment shader used for generation of velocity map
 */

precision mediump float;

varying vec2 v_velocity;


void main()
{
  gl_FragColor.xy = v_velocity;
  gl_FragColor.zw = vec2( 0.0, 0.0 );
}
