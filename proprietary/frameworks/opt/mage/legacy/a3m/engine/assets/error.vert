/**************************************************************************
 *
 * Copyright (c) 2012 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Shader error vertex shader.
 *
 */

uniform mat4 u_t_modelViewProjection;

attribute vec4 a_position;

void main()
{
  gl_Position = u_t_modelViewProjection * a_position;
}
