/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

/**************************************************************************
 *
 * Copyright (c) 2010 MediaTek Inc. All Rights Reserved.
 * --------------------
 * This software is protected by copyright and the information contained
 * herein is confidential. The software may not be copied and the information
 * contained herein may not be used or disclosed except with the written
 * permission of MediaTek Inc.
 *
 ***************************************************************************/
/** \file
 * Fast floating point maths functions
 *
 */
#pragma once
#ifndef A3M_FPMATHS_H
#define A3M_FPMATHS_H

/******************************************************************************
* Include Files
******************************************************************************/
#include <cfloat>                       /* for _copysign() (MSVC only)       */
#include <a3m/base_types.h>             /* for A3M_FLOAT                     */
#include <a3m/compiler.h>               /* for A3M_COMPILER                  */

namespace a3m
{

  /** \defgroup  a3mMathsConst Constants
   *  \ingroup   a3mRefMaths
   *  pi, 2.pi, pi/2, float-epsilon, etc.
   *  @{
   */

  /** Pi */
  const A3M_FLOAT FLOAT_PI = 3.1415926535897932f;

  /** Two times Pi */
  const A3M_FLOAT FLOAT_TWO_PI = (2.f* FLOAT_PI);

  /** Pi over two */
  const A3M_FLOAT FLOAT_HALF_PI = (FLOAT_PI / 2.f);

  /** Smallest such that 1.0+FLT_EPSILON != 1.0 */
  const A3M_FLOAT A3M_FLOAT_EPSILON = 1.192092896e-07f;

  /** @} */




  /** \defgroup  a3mFastFloat Fast floating point maths functions
   *  \ingroup   a3mRefMaths
   * These are designed to be a faster alternative to those in math.h.
   * They may be less accurate, but should be accurate
   * enough for 3D graphics applications.
   *  @{
   */

  /******************************************************************************
   * Square Root Functions
   ******************************************************************************/

  /**
  * Calculate square root
  * \return square root of given number
  */
  A3M_FLOAT sqrt( A3M_FLOAT x  /**< number */ );

  /**
  * Calculate inverse square root (1/sqrt(x))
  * \return inverse square root of given number
  */
  A3M_FLOAT invSqrt( A3M_FLOAT x  /**< number */ );

  /******************************************************************************
   * Absolute function
   ******************************************************************************/

  /**
   * Returns absolute value of the number of type < T >.
   *
   * \tparam T A3M_INT32, A3M_INT64, A3M_FLOAT etc.
   * \return Absolute value of given number
   */
  template < typename T >
  T abs( T x /**< number of type T */ )
  {
    return ( ( x < T(0) ) ? -x : x );
  }

  /******************************************************************************
   * Clamp
   ******************************************************************************/

  /** Clamp the value to the given range.
      \return a value between min and max.
      */
  template < typename T >
  T clamp(
    T value /**< the input value */,
    T min   /**< the minimum value */,
    T max   /**< the maximum value */)
  {
    if (value <= min)
    {
      return min;
    }

    if (max <= value)
    {
      return max;
    }

    return value;
  }

  /******************************************************************************
   * Miscellaneous
   ******************************************************************************/
  /** Compute the floating-point remainder of numerator/denominator.
      \return the result of subtracting the integral quotient
              multiplied by the denominator from the numerator. */
  A3M_FLOAT fmod(
    A3M_FLOAT numerator   /**< the division numerator */,
    A3M_FLOAT denominator /**< the division denominator */);

  // \todo Group this with the rest of the platform-specific stuff, like
  // atomic.h, when a tidy-up is done.
#if A3M_COMPILER == A3M_MSVC
  /**
   * Composes a floating point value with the magnitude of x and the sign of y.
   * std::copysign() exists in C++11, but VC++ 2010 only has a _copysign()
   * "IEEE Recommended" function (see float.h).
   * \return Composed floating point value
   */
  inline A3M_DOUBLE copysign(
    A3M_DOUBLE x,
    /**< Number whose magnitude is used to construct the result */
    A3M_DOUBLE y
    /**< Number whose sign is used to construct the result */)
  {
    return _copysign(x, y);
  }
#endif

  /** @} */
} /* namespace a3m */

#endif /* A3M_FPMATHS_H */
