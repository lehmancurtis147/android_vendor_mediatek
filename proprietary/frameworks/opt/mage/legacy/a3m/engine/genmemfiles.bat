REM Generates "memory files" for both a3m and ngin3d
python ../buildtools/genmemfiles.py -n a3m -i assets -o assets
python ../buildtools/genmemfiles.py -n ngin3d -i assets_ngin3d -o assets_ngin3d