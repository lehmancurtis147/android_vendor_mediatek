/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Square unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/square.h>                 /* for Square                         */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mSquareTest : public AutotestTest
  {
    Square::Ptr square;
    Ray ray;
    RaycastResult result;

    A3mSquareTest() :
      square(new Square())
    {
    }
  };

} // namespace

/*
 * Verify state after construction.
 */
TEST_F(A3mSquareTest, InitialState)
{
  EXPECT_EQ(Matrix4f(), square->getTransform());
}

/*
 * Test intersections with default square.
 */
TEST_F(A3mSquareTest, Bounds)
{
  // Ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_TRUE(result.getIntersected());

  // Ray inside right edge of square
  ray = Ray(Vector3f(0.49f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_TRUE(result.getIntersected());

  // Ray outside right edge of square
  ray = Ray(Vector3f(0.51f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Ray inside left edge of square
  ray = Ray(Vector3f(-0.49f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_TRUE(result.getIntersected());

  // Ray outside left edge of square
  ray = Ray(Vector3f(-0.51f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Ray inside top edge of square
  ray = Ray(Vector3f(0.0f, 0.49f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_TRUE(result.getIntersected());

  // Ray outside top edge of square
  ray = Ray(Vector3f(0.0f, 0.51f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Ray inside bottom edge of square
  ray = Ray(Vector3f(0.0f, -0.49f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_TRUE(result.getIntersected());

  // Ray outside bottom edge of square
  ray = Ray(Vector3f(0.0f, -0.51f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = square->raycast(ray);

  EXPECT_FALSE(result.getIntersected());
}
