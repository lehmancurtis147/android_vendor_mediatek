/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Mesh unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/mesh.h>                   /* for Mesh                           */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mMeshHeaderTest : public AutotestTest
  {
    MeshHeader header;
  };

  /*
   * Test fixture.
   */
  struct A3mMeshTest : public A3mMeshHeaderTest
  {
    Mesh::Ptr mesh;
    MeshCache::Ptr cache;

    A3mMeshTest()
    {
      cache.reset(new MeshCache(
                    IndexBufferCache::Ptr(new IndexBufferCache()),
                    VertexBufferCache::Ptr(new VertexBufferCache())));
    }
  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mMeshHeaderTest, InitialState)
{
  MeshHeader header;
  EXPECT_FLOAT_EQ(-1.0f, header.boundingRadius);
  EXPECT_EQ(Vector3f::ZERO, header.boundingBox);
  EXPECT_EQ(Matrix4f::IDENTITY, header.offsetOrigin);
}

/*
 * Validate state after construction.
 */
TEST_F(A3mMeshTest, InitialState)
{
  A3M_FLOAT const BOUNDING_RADIUS = 1.0f;
  Vector3f const BOUNDING_BOX(200.0f, -5.0f, 50.0f);
  Matrix4f const OFFSET_ORIGIN(
    Vector4f(1.0f, 20.0f, 0.0f, 0.0f),
    Vector4f(0.0f, 1.0f, 50.0f, 1.0f),
    Vector4f(0.0f, 20.0f, 6.0f, 0.0f),
    Vector4f(20.0f, 10.0f, 1.0f, 3.3f));

  header.boundingRadius = BOUNDING_RADIUS;
  header.boundingBox = BOUNDING_BOX;
  header.offsetOrigin = OFFSET_ORIGIN;

  mesh = cache->create(header, IndexBuffer::Ptr(), VertexBuffer::Ptr());

  EXPECT_FLOAT_EQ(BOUNDING_RADIUS, header.boundingRadius);
  EXPECT_EQ(BOUNDING_BOX, header.boundingBox);
  EXPECT_EQ(OFFSET_ORIGIN, header.offsetOrigin);
}
