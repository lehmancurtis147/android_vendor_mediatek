/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Plane unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/plane.h>                  /* for Plane                          */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mPlaneTest : public AutotestTest
  {
    Plane::Ptr plane;
    Ray ray;
    RaycastResult result;

    A3mPlaneTest() :
      plane(new Plane())
    {
    }
  };

} // namespace

/*
 * Verify state after construction.
 */
TEST_F(A3mPlaneTest, InitialState)
{
  EXPECT_EQ(Matrix4f(), plane->getTransform());
}

/*
 * Test intersections with default plane.
 */
TEST_F(A3mPlaneTest, DefaultIntersections)
{
  // Ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Negative ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Ray on plane
  ray = Ray(Vector3f(2.0f, 3.5f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Ray off of plane
  ray = Ray(Vector3f(0.0f, 0.0f, 2.2f), Vector3f(0.0f, 0.0f, 1.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(-2.2f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Negative ray off of plane
  ray = Ray(Vector3f(0.0f, 0.0f, 2.2f), Vector3f(0.0f, 0.0f, -1.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(2.2f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Scaled ray off of plane
  ray = Ray(Vector3f(0.0f, 0.0f, 2.2f), Vector3f(0.0f, 0.0f, 2.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(-2.2f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Negative scaled ray off of plane
  ray = Ray(Vector3f(0.0f, 0.0f, 2.2f), Vector3f(0.0f, 0.0f, -0.5f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(2.2f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Angled ray at origin.
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(1.0f, 2.0f, 2.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Angled ray off of plane.
  ray = Ray(Vector3f(0.0f, 0.0f, -2.0f), Vector3f(1.0f, 2.0f, 2.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(3.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Angled normalized ray off of plane.
  ray = Ray(Vector3f(0.0f, 0.0f, -2.0f), normalize(Vector3f(1.0f, 2.0f, 2.0f)));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(3.0f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Parallel ray in plane
  ray = Ray(Vector3f(-20.0f, 5.56f, 0.0f), Vector3f(-2.0f, 2.0f, 0.0f));
  result = plane->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Parallel ray off of plane
  ray = Ray(Vector3f(-20.0f, 5.56f, 0.1f), Vector3f(-2.0f, 2.0f, 0.0f));
  result = plane->raycast(ray);

  EXPECT_FALSE(result.getIntersected());
}

/*
 * Test intersections with a transformed plane.
 */
TEST_F(A3mPlaneTest, TransformedIntersections)
{
  // Scale, then rotate by 90 degrees and move by (1, 2, 3).
  Matrix4f transform =
    translation(Vector4f(1.0f, 2.0f, 3.0f, 0.0f)) *
    rotation(Vector4f::Y_AXIS, degrees(90.0f)) *
    scale(2.0f, 3.0f, 0.5f, 1.0f);

  Vector3f normal(normalize(transform * Vector4f::Z_AXIS));
  plane->setTransform(transform);

  // Parallel ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = plane->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Negative parallel ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = plane->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(1.0f, result.getDistance());
  EXPECT_EQ(normal, result.getNormal());

  // Negative ray at origin
  ray = Ray(Vector3f(0.0f, 0.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(-1.0f, result.getDistance());
  EXPECT_EQ(normal, result.getNormal());

  // Ray on plane origin
  ray = Ray(Vector3f(1.0f, 2.0f, 3.0f), Vector3f(1.0f, 0.0f, 0.0f));
  result = plane->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_NEAR(0.0f, result.getDistance(), 1e-6);
  EXPECT_EQ(normal, result.getNormal());
}
