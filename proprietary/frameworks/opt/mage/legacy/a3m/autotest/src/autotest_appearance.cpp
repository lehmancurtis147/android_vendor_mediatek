/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Appearance unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <GLES2/gl2.h>                  /* for glGetFloatv() etc.             */
#include <a3m/appearance.h>             /* for Appearance                     */
#include <a3m/rendercontext.h>          /* for RenderContext                  */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  A3M_CHAR8 const* TEST_FILE = "autotest#test.ap";

  /*
   * Test fixture.
   */
  struct A3mAppearanceTest : public AutotestTest
  {
    RenderContext::Ptr context;
    Appearance::Ptr appearance;

    A3mAppearanceTest() :
      context(new RenderContext()),
      appearance(new Appearance())
    {
    }
  };

} // namespace

/*
 * Validate loading of appearance files.
 */
TEST_F(A3mAppearanceTest, Load)
{
  AssetCachePool::Ptr pool(new AssetCachePool());
  pool->registerSource(create_memfilesource_autotest());

  appearance->apply(*pool, TEST_FILE);

  ShaderUniformBase::Ptr uniform =
    appearance->getPropertyUniform("M_DIFFUSE_COLOUR");
  EXPECT_TRUE(uniform);

  if (uniform)
  {
    EXPECT_TRUE(uniform->setValue(Vector4f(1.0f, 0.0f, 0.0f, 0.0f)));
  }

  EXPECT_EQ(CULL_BACK, appearance->getCullingMode());
  EXPECT_EQ(WIND_CW, appearance->getWindingOrder());

  EXPECT_FLOAT_EQ(1.0f, appearance->getDepthOffsetFactor());
  EXPECT_FLOAT_EQ(2.0f, appearance->getDepthOffsetUnits());
  EXPECT_TRUE(appearance->isDepthTestEnabled());

  EXPECT_TRUE(appearance->getColourMaskR());
  EXPECT_TRUE(appearance->getColourMaskG());
  EXPECT_FALSE(appearance->getColourMaskB());
  EXPECT_FALSE(appearance->getColourMaskA());

  Colour4f const& blendColour = appearance->getBlendColour();
  EXPECT_EQ(Colour4f(0.1f, 0.2f, 0.3f, 0.4f), blendColour);
  EXPECT_EQ(BLEND_CONSTANT_ALPHA, appearance->getSrcRgbBlendFactor());
  EXPECT_EQ(BLEND_DST_ALPHA, appearance->getSrcAlphaBlendFactor());
  EXPECT_EQ(BLEND_ONE_MINUS_CONSTANT_ALPHA, appearance->getDstRgbBlendFactor());
  EXPECT_EQ(BLEND_ONE, appearance->getDstAlphaBlendFactor());
  EXPECT_EQ(BLEND_ADD, appearance->getRgbBlendFunction());
  EXPECT_EQ(BLEND_REVERSE_SUBTRACT, appearance->getAlphaBlendFunction());

  EXPECT_EQ(STENCIL_GREATER, appearance->getStencilFunction(STENCIL_FRONT));
  EXPECT_EQ(1, appearance->getStencilReference(STENCIL_FRONT));
  EXPECT_EQ(3u, appearance->getStencilReferenceMask(STENCIL_FRONT));
  EXPECT_EQ(STENCIL_REPLACE, appearance->getStencilFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilPassDepthFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_INCR, appearance->getStencilPassDepthPass(STENCIL_BACK));
  EXPECT_EQ(15u, appearance->getStencilMask(STENCIL_FRONT));

  pool->release();
}

/*
 * Validate state after construction.
 */
TEST_F(A3mAppearanceTest, InitialState)
{
  EXPECT_EQ(CULL_BACK, appearance->getCullingMode());
  EXPECT_EQ(WIND_CCW, appearance->getWindingOrder());
  EXPECT_EQ(1.0f, appearance->getLineWidth());

  EXPECT_EQ(0.0f, appearance->getBlendColour().r);
  EXPECT_EQ(0.0f, appearance->getBlendColour().g);
  EXPECT_EQ(0.0f, appearance->getBlendColour().b);
  EXPECT_EQ(0.0f, appearance->getBlendColour().a);
  EXPECT_EQ(BLEND_ONE, appearance->getSrcRgbBlendFactor());
  EXPECT_EQ(BLEND_ONE, appearance->getSrcAlphaBlendFactor());
  EXPECT_EQ(BLEND_ZERO, appearance->getDstAlphaBlendFactor());
  EXPECT_EQ(BLEND_ZERO, appearance->getDstRgbBlendFactor());
  EXPECT_EQ(BLEND_ADD, appearance->getRgbBlendFunction());
  EXPECT_EQ(BLEND_ADD, appearance->getAlphaBlendFunction());

  EXPECT_EQ(DEPTH_LESS, appearance->getDepthTestFunction());
  EXPECT_EQ(A3M_TRUE, appearance->isDepthTestEnabled());
  EXPECT_EQ(A3M_TRUE, appearance->isDepthWriteEnabled());
  EXPECT_FLOAT_EQ(0.0f, appearance->getDepthOffsetFactor());
  EXPECT_FLOAT_EQ(0.0f, appearance->getDepthOffsetUnits());
  EXPECT_TRUE(appearance->getColourMaskR());
  EXPECT_TRUE(appearance->getColourMaskG());
  EXPECT_TRUE(appearance->getColourMaskB());
  EXPECT_TRUE(appearance->getColourMaskA());
  EXPECT_EQ(STENCIL_ALWAYS, appearance->getStencilFunction(STENCIL_BACK));
  EXPECT_EQ(0, appearance->getStencilReference(STENCIL_BACK));
  EXPECT_EQ(~0u, appearance->getStencilReferenceMask(STENCIL_BACK));

  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilPassDepthFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilPassDepthPass(STENCIL_BACK));

  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilFail(STENCIL_FRONT));
  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilPassDepthFail(STENCIL_FRONT));
  EXPECT_EQ(STENCIL_KEEP, appearance->getStencilPassDepthPass(STENCIL_FRONT));

  EXPECT_EQ(~0u, appearance->getStencilMask(STENCIL_BACK));
  EXPECT_EQ(~0u, appearance->getStencilMask(STENCIL_FRONT));
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mAppearanceTest, Culling)
{
  appearance->setCullingMode(CULL_FRONT);
  appearance->enable(*context);

  GLint glMode;
  glGetIntegerv(GL_CULL_FACE_MODE, &glMode);
  EXPECT_EQ(GL_FRONT, glMode);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mAppearanceTest, Winding)
{
  appearance->setWindingOrder(WIND_CW);
  appearance->enable(*context);

  GLint glMode;
  glGetIntegerv(GL_FRONT_FACE, &glMode);
  EXPECT_EQ(GL_CW, glMode);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mAppearanceTest, BlendColour)
{
  appearance->setBlendColour(Colour4f(0.2f, 0.3f, 0.4f, 1.0f));
  appearance->enable(*context);

  A3M_FLOAT glColour[4];
  glGetFloatv(GL_BLEND_COLOR, glColour);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(glColour[0], appearance->getBlendColour().r);
  EXPECT_EQ(glColour[1], appearance->getBlendColour().g);
  EXPECT_EQ(glColour[2], appearance->getBlendColour().b);
  EXPECT_EQ(glColour[3], appearance->getBlendColour().a);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mAppearanceTest, BlendFunctions)
{
  appearance->setBlendFunctions(BLEND_SUBTRACT, BLEND_REVERSE_SUBTRACT);
  appearance->enable(*context);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(appearance->getRgbBlendFunction(), BLEND_SUBTRACT);
  EXPECT_EQ(appearance->getAlphaBlendFunction(), BLEND_REVERSE_SUBTRACT);

  GLint glFunc;

  glGetIntegerv(GL_BLEND_EQUATION_RGB, &glFunc);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_FUNC_SUBTRACT, glFunc);

  glGetIntegerv(GL_BLEND_EQUATION_ALPHA, &glFunc);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_FUNC_REVERSE_SUBTRACT, glFunc);
}

/*
 * Validate OpenGL state.
 */
TEST_F(A3mAppearanceTest, BlendFactors)
{
  appearance->setBlendFactors(
    BLEND_SRC_COLOUR,
    BLEND_SRC_ALPHA_SATURATE,
    BLEND_ONE_MINUS_SRC_COLOUR,
    BLEND_ONE_MINUS_SRC_ALPHA);
  appearance->enable(*context);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());

  GLint glFactor;
  glGetIntegerv(GL_BLEND_SRC_RGB, &glFactor);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_SRC_COLOR, glFactor);

  glGetIntegerv(GL_BLEND_SRC_ALPHA, &glFactor);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_SRC_ALPHA_SATURATE, glFactor);

  glGetIntegerv(GL_BLEND_DST_RGB, &glFactor);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_ONE_MINUS_SRC_COLOR, glFactor);

  glGetIntegerv(GL_BLEND_DST_ALPHA, &glFactor);
  ASSERT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
  EXPECT_EQ(GL_ONE_MINUS_SRC_ALPHA, glFactor);
}

/*
 * Test set/getColourWriteMask().
 */
TEST_F(A3mAppearanceTest, ColourWriteMask)
{
  appearance->setColourMask(A3M_FALSE, A3M_FALSE, A3M_FALSE, A3M_FALSE);
  appearance->enable(*context);

  EXPECT_EQ(A3M_FALSE, appearance->getColourMaskR());
  EXPECT_EQ(A3M_FALSE, appearance->getColourMaskG());
  EXPECT_EQ(A3M_FALSE, appearance->getColourMaskB());
  EXPECT_EQ(A3M_FALSE, appearance->getColourMaskA());
}

/*
 * Test set/getDepthOffset().
 */
TEST_F(A3mAppearanceTest, DepthOffset)
{
  appearance->setDepthOffset(0.5f, 0.8f);
  appearance->enable(*context);

  EXPECT_FLOAT_EQ(0.5f, appearance->getDepthOffsetFactor());
  EXPECT_FLOAT_EQ(0.8f, appearance->getDepthOffsetUnits());
}

/*
 * Test set/getDepthTestFunc().
 */
TEST_F(A3mAppearanceTest, DepthTestFunc)
{
  appearance->setDepthTestFunction(DEPTH_GEQUAL);
  appearance->enable(*context);

  EXPECT_EQ(DEPTH_GEQUAL, appearance->getDepthTestFunction());
}

/*
 * Test set/isDepthTestEnabled().
 */
TEST_F(A3mAppearanceTest, DepthTestEnabled)
{
  appearance->setDepthTestEnabled(A3M_FALSE);
  appearance->enable(*context);

  EXPECT_EQ(A3M_FALSE, appearance->isDepthTestEnabled());
}

/*
 * Test set/isDepthWriteEnabled().
 */
TEST_F(A3mAppearanceTest, DepthWriteEnabled)
{
  appearance->setDepthWriteEnabled(A3M_FALSE);
  appearance->enable(*context);

  EXPECT_EQ(A3M_FALSE, appearance->isDepthWriteEnabled());
}

/*
 * Test stencil function functions.
 */
TEST_F(A3mAppearanceTest, StencilFunctions)
{
  // Verify appearance parameters for BACK face.
  appearance->setStencilFunction(STENCIL_BACK, STENCIL_GREATER, 0x11111111, 0x22222222);

  EXPECT_EQ(STENCIL_GREATER, appearance->getStencilFunction(STENCIL_BACK));
  EXPECT_EQ(0x11111111, appearance->getStencilReference(STENCIL_BACK));
  EXPECT_EQ(0x22222222u, appearance->getStencilReferenceMask(STENCIL_BACK));

  // Verify appearance parameters for FRONT face.
  appearance->setStencilFunction(STENCIL_FRONT, STENCIL_GEQUAL, 0xFFFFFFFF, 0xAAAAAAAA);

  EXPECT_EQ(STENCIL_GEQUAL, appearance->getStencilFunction(STENCIL_FRONT));
  EXPECT_EQ(A3M_INT32(0xFFFFFFFF), appearance->getStencilReference(STENCIL_FRONT));
  EXPECT_EQ(0xAAAAAAAAu, appearance->getStencilReferenceMask(STENCIL_FRONT));
}

/*
 * Test stencil operations functions.
 */
TEST_F(A3mAppearanceTest, StencilOperations)
{
  // Verify appearance parameters for BACK face.
  appearance->setStencilOperations(STENCIL_BACK, STENCIL_REPLACE, STENCIL_INCR, STENCIL_DECR_WRAP);

  EXPECT_EQ(STENCIL_REPLACE, appearance->getStencilFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_INCR, appearance->getStencilPassDepthFail(STENCIL_BACK));
  EXPECT_EQ(STENCIL_DECR_WRAP, appearance->getStencilPassDepthPass(STENCIL_BACK));

  // Verify appearance parameters for FRONT face.
  appearance->setStencilOperations(STENCIL_FRONT, STENCIL_REPLACE, STENCIL_INCR, STENCIL_DECR_WRAP);

  EXPECT_EQ(STENCIL_REPLACE, appearance->getStencilFail(STENCIL_FRONT));
  EXPECT_EQ(STENCIL_INCR, appearance->getStencilPassDepthFail(STENCIL_FRONT));
  EXPECT_EQ(STENCIL_DECR_WRAP, appearance->getStencilPassDepthPass(STENCIL_FRONT));
}

/*
 * Test stencil set/getWriteMask().
 */
TEST_F(A3mAppearanceTest, StencilWriteMask)
{
  // Verify appearance parameters for BACK face.
  appearance->setStencilMask(STENCIL_BACK, 0x22222222);
  EXPECT_EQ(0x22222222u, appearance->getStencilMask(STENCIL_BACK));

  // Verify appearance parameters for FRONT face.
  appearance->setStencilMask(STENCIL_FRONT, 0x33333333);
  EXPECT_EQ(0x33333333u, appearance->getStencilMask(STENCIL_FRONT));
}
