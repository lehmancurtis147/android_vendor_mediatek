/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M Sphere unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/sphere.h>                 /* for Sphere                         */
#include <autotest_common.h>            /* for AutotestTest                   */

using namespace a3m;

namespace
{

  /*
   * Test fixture.
   */
  struct A3mSphereTest : public AutotestTest
  {
    Sphere::Ptr sphere;
    Ray ray;
    RaycastResult result;

    A3mSphereTest() :
      sphere(new Sphere())
    {
    }
  };

} // namespace

/*
 * Verify state after construction.
 */
TEST_F(A3mSphereTest, InitialState)
{
  EXPECT_EQ(Matrix4f(), sphere->getTransform());
}

/*
 * Test intersections with sphere.
 */
TEST_F(A3mSphereTest, Intersections)
{
  // Ray at right
  ray = Ray(Vector3f(1.0f, 0.0f, 0.0f), Vector3f(-1.0f, 0.0f, 0.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(Vector3f::X_AXIS, result.getNormal());

  // Ray at left
  ray = Ray(Vector3f(-1.0f, 0.0f, 0.0f), Vector3f(1.0f, 0.0f, 0.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(-Vector3f::X_AXIS, result.getNormal());

  // Ray at top
  ray = Ray(Vector3f(0.0f, 1.0f, 0.0f), Vector3f(0.0f, -1.0f, 0.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(Vector3f::Y_AXIS, result.getNormal());

  // Ray at bottom
  ray = Ray(Vector3f(0.0f, -1.0f, 0.0f), Vector3f(0.0f, 1.0f, 0.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(-Vector3f::Y_AXIS, result.getNormal());

  // Ray at front
  ray = Ray(Vector3f(0.0f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(Vector3f::Z_AXIS, result.getNormal());

  // Ray at back
  ray = Ray(Vector3f(0.0f, 0.0f, -1.0f), Vector3f(0.0f, 0.0f, 1.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
  EXPECT_EQ(-Vector3f::Z_AXIS, result.getNormal());

  // Ray at angle
  ray = Ray(
          normalize(Vector3f(1.0f, 0.0f, 1.0f)),
          normalize(Vector3f(-1.0f, 0.0f, -1.0f)));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_FLOAT_EQ(0.5f, result.getDistance());
}

/*
 * Test bounds of sphere intersection.
 */
TEST_F(A3mSphereTest, Bounds)
{
  // Test bounds at right-hand side.
  ray = Ray(Vector3f(0.499999f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_NEAR(1.0f, result.getDistance(), 0.01f);

  ray = Ray(Vector3f(0.500001f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = sphere->raycast(ray);

  EXPECT_FALSE(result.getIntersected());

  // Test bounds at left-hand side.
  ray = Ray(Vector3f(-0.499999f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = sphere->raycast(ray);

  EXPECT_TRUE(result.getIntersected());
  EXPECT_NEAR(1.0f, result.getDistance(), 0.01f);

  ray = Ray(Vector3f(-0.500001f, 0.0f, 1.0f), Vector3f(0.0f, 0.0f, -1.0f));
  result = sphere->raycast(ray);

  EXPECT_FALSE(result.getIntersected());
}
