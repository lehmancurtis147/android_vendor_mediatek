/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2012. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/*
 * A3M ShaderProgram unit test.
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <a3m/renderdevice.h>           /* for RenderDevice                   */
#include <a3m/shaderprogram.h>          /* for ShaderProgram                  */
#include <a3m/vertexbuffer.h>           /* for VertexBuffer                   */
#include <shaderprogramloader.h>        /* for ShaderProgramLoader            */
#include <autotest_common.h>            /* for AutotestTest                   */
#include <memfilesource_autotest.h>     /* for create_memfilesource_autotest  */

using namespace a3m;

namespace
{

  A3M_FLOAT const POSITION[] =
  {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.5f, 0.5f, 0.0f,
    -0.5f, 0.5f, 0.0f
  };

  A3M_UINT8 const COLOUR[] =
  {
    255, 0, 0,
    0, 255, 0,
    0, 0, 255,
    255, 255, 255
  };

  /*
   * Test fixture.
   */
  struct A3mShaderProgramTest : public AutotestTest
  {
    VertexBufferCache::Ptr vertexBufferCache;
    ShaderProgramCache::Ptr cache;
    ShaderProgram::Ptr program;

    A3mShaderProgramTest()
    {
      vertexBufferCache.reset(new VertexBufferCache());
      cache.reset(new ShaderProgramCache());
      cache->registerLoader(
        ShaderProgramLoader::Ptr(new ShaderProgramLoader()));
      cache->registerSource(create_memfilesource_autotest());

      program = cache->get("autotest#test.sp");
    }

    ~A3mShaderProgramTest()
    {
      vertexBufferCache->release();
      cache->release();
    }

    VertexBuffer::Ptr createVertexBuffer()
    {
      VertexBuffer::Ptr buffer(vertexBufferCache->create());

      VertexArray::Ptr position(new VertexArray(4, 3, POSITION));
      VertexArray::Ptr colour(new VertexArray(4, 3, COLOUR));

      buffer->addAttrib(position, "a_position");
      buffer->addAttrib(
        colour,
        "a_colour",
        VertexBuffer::ATTRIB_FORMAT_UINT8,
        VertexBuffer::ATTRIB_USAGE_WRITE_MANY,
        A3M_TRUE);

      buffer->commit();

      return buffer;
    };

  };

} // namespace

/*
 * Validate state after construction.
 */
TEST_F(A3mShaderProgramTest, InitialState)
{
  EXPECT_TRUE(program);
}

/*
 * Test binding of a vertex buffer.
 */
TEST_F(A3mShaderProgramTest, Bind)
{
  VertexBuffer::Ptr buffer = createVertexBuffer();
  program->bind(*buffer);

  EXPECT_EQ(RenderDevice::NO_ERROR, RenderDevice::getError());
}
