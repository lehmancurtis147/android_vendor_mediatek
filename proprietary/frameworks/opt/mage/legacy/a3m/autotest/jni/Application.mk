$(info A3M-AUTOTEST-JNI JA3M APPLICATION.MK)
#
# Used by NDK builds.  *NOT* used by makeMtk builds.
# Both build system use the related Android.mk file
#
# Invoke (from local folder) with ndk-build NDK_APPLICATION_MK=Application.mk
#
# Note. This is a pragmatic build solution.  The NDK build is useful to help
# work around problems in the makeMtk build system.  It is also useful for
# building with eclipse.  This use of Application.mk is NOT as intended but
# it is the only way to reliably build code in this environment at present.
# Ideally this should be fixed (globally, not just in A3M).  See NDK/docs.

# Necessary for NDK build to locate STL headers for A3M STL usage
APP_STL:= stlport_shared
