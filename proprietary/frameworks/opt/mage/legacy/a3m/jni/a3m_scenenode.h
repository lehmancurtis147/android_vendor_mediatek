/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M SceneNode "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_SCENENODE_H
#define JNI_A3M_SCENENODE_H

#include <a3m/flags.h>
#include <a3m/scenenode.h>
#include <a3m/sceneutility.h>

class A3mSceneNode
{
private:
  a3m::SceneNode::Ptr m_native;

protected:
  A3mSceneNode(a3m::SceneNode::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mSceneNode* toWrapper(a3m::SceneNode::Ptr const& native)
  {
    return native ? new A3mSceneNode(native) : 0;
  }

  static a3m::SceneNode::Ptr toNative(A3mSceneNode* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::SceneNode::Ptr();
  }

  a3m::SceneNode::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  A3mSceneNode() :
    m_native(a3m::SceneNode::Ptr(new a3m::SceneNode()))
  {
  }

  virtual ~A3mSceneNode()
  {
  }

  char getNodeType() const
  {
    return m_native->getType();
  }

  void setName(char const* name)
  {
    m_native->setName(name);
  }

  char const* getName() const
  {
    return m_native->getName();
  }

  void setParent(
      A3mSceneNode* node,
      bool preserveWorldTransform = A3M_FALSE)
  {
    m_native->setParent(toNative(node), preserveWorldTransform);
  }

  int getChildCount()
  {
    return m_native->getChildCount();
  }

  A3mSceneNode* getChild(int i)
  {
    return toWrapper(m_native->getChild(i));
  }

  A3mSceneNode* find(char const* name)
  {
    return toWrapper(m_native->find(name));
  }

  void point(float worldForwardX, float worldForwardY, float worldForwardZ)
  {
    a3m::point(*m_native, a3m::Vector3f(
          worldForwardX, worldForwardY, worldForwardZ));
  }

  void point(
      float worldForwardX, float worldForwardY, float worldForwardZ,
      float worldUpX, float worldUpY, float worldUpZ)
  {
    a3m::point(*m_native,
        a3m::Vector3f(worldForwardX, worldForwardY, worldForwardZ),
        a3m::Vector3f(worldUpX, worldUpY, worldUpZ));
  }

  void point(
      float worldForwardX, float worldForwardY, float worldForwardZ,
      float worldUpX, float worldUpY, float worldUpZ,
      float localForwardX, float localForwardY, float localForwardZ)
  {
    a3m::point(*m_native,
          a3m::Vector3f(worldForwardX, worldForwardY, worldForwardZ),
          a3m::Vector3f(worldUpX, worldUpY, worldUpZ),
          a3m::Vector3f(localForwardX, localForwardY, localForwardZ));
  }

  void point(
      float worldForwardX, float worldForwardY, float worldForwardZ,
      float worldUpX, float worldUpY, float worldUpZ,
      float localForwardX, float localForwardY, float localForwardZ,
      float localUpX, float localUpY, float localUpZ)
  {
    a3m::point(*m_native,
          a3m::Vector3f(worldForwardX, worldForwardY, worldForwardZ),
          a3m::Vector3f(worldUpX, worldUpY, worldUpZ),
          a3m::Vector3f(localForwardX, localForwardY, localForwardZ),
          a3m::Vector3f(localUpX, localUpY, localUpZ));
  }

  A3mSceneNode* getParent() const
  {
    return toWrapper(m_native->getParent());
  }

  void setPosition(float x, float y, float z)
  {
    m_native->setPosition(a3m::Vector3f(x, y, z));
  }

  float getPositionX() const
  {
    return m_native->getPosition().x;
  }

  float getPositionY() const
  {
    return m_native->getPosition().y;

  }

  float getPositionZ() const
  {
    return m_native->getPosition().z;
  }

  void setRotation(float a, float b, float c, float d)
  {
    m_native->setRotation(a3m::Quaternionf(a, b, c, d));
  }

  float getRotationA() const
  {
    return m_native->getRotation().a;
  }

  float getRotationB() const
  {
    return m_native->getRotation().b;
  }

  float getRotationC() const
  {
    return m_native->getRotation().c;
  }

  float getRotationD() const
  {
    return m_native->getRotation().d;
  }

  void setScale(float x, float y, float z)
  {
    m_native->setScale(a3m::Vector3f(x, y, z));
  }

  float getScaleX() const
  {
    return m_native->getScale().x;
  }

  float getScaleY() const
  {
    return m_native->getScale().y;
  }

  float getScaleZ() const
  {
    return m_native->getScale().z;
  }

  bool getLocalMirrored() const
  {
    return m_native->getLocalMirrored();
  }

  bool getWorldMirrored() const
  {
    return m_native->getWorldMirrored();
  }

  void setFlags(A3mFlagMask const& mask, bool state)
  {
    a3m::setFlags(*m_native, mask.getNative(), state);
  }

  bool getFlags(A3mFlagMask const& mask) const
  {
    return a3m::getFlags(*m_native, mask.getNative());
  }

  bool getDerivedFlags(A3mFlagMask const& mask) const
  {
    return a3m::getDerivedFlags(*m_native, mask.getNative());
  }
};

#endif // JNI_A3M_SCENENODE_H
