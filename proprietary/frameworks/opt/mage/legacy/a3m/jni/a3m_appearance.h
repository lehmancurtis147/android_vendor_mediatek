/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * A3M Appearance "Java" implementation.
 */
#pragma once
#ifndef JNI_A3M_APPEARANCE_H
#define JNI_A3M_APPEARANCE_H

#include <a3m/appearance.h>
#include <a3m/noncopyable.h>
#include <a3m/vector2.h>
#include <a3m/vector3.h>
#include <a3m/vector4.h>
#include <a3m_shaderprogram.h>
#include <a3m_texture2d.h>
#include <a3m_texturecube.h>

class A3mAppearance
{
private:
  a3m::Appearance::Ptr m_native;

  A3mAppearance(a3m::Appearance::Ptr const& native) :
    m_native(native)
  {
  }

public:
  // The following functions are ignored by SWIG (see a3m.i)
  static A3mAppearance* toWrapper(a3m::Appearance::Ptr const& native)
  {
    return native ? new A3mAppearance(native) : 0;
  }

  static a3m::Appearance::Ptr toNative(A3mAppearance* wrapper)
  {
    return wrapper ? wrapper->getNative() : a3m::Appearance::Ptr();
  }

  a3m::Appearance::Ptr const& getNative() const
  {
    return m_native;
  }

public:
  A3mAppearance() :
    m_native(a3m::Appearance::Ptr(new a3m::Appearance()))
  {
  }

  void setName(char const* name)
  {
    m_native->setName(name);
  }

  char const* getName() const
  {
    return m_native->getName();
  }

  void setShaderProgram(A3mShaderProgram program)
  {
    m_native->setShaderProgram(program.getNative());
  }

  A3mShaderProgram* getShaderProgram() const
  {
    return A3mShaderProgram::toWrapper(m_native->getShaderProgram());
  }

  void setCullingMode(int cullingMode)
  {
    m_native->setCullingMode(static_cast<a3m::CullingMode>(cullingMode));
  }

  int getCullingMode() const
  {
    return static_cast<int>(m_native->getCullingMode());
  }

  void setLineWidth(float width)
  {
    m_native->setLineWidth(width);
  }

  float getLineWidth() const
  {
    return m_native->getLineWidth();
  }

  void setWindingOrder(int windingOrder)
  {
    m_native->setWindingOrder(static_cast<a3m::WindingOrder>(windingOrder));
  }

  int getWindingOrder() const
  {
    return static_cast<int>(m_native->getWindingOrder());
  }

  void setBlendColour(float r, float g, float b, float a)
  {
    m_native->setBlendColour(a3m::Colour4f(r, g, b, a));
  }

  float getBlendColourR() const
  {
    return m_native->getBlendColour().r;
  }

  float getBlendColourG() const
  {
    return m_native->getBlendColour().g;
  }

  float getBlendColourB() const
  {
    return m_native->getBlendColour().b;
  }

  float getBlendColourA() const
  {
    return m_native->getBlendColour().a;
  }

  void setBlendFactors(int src, int dst)
  {
    m_native->setBlendFactors(
        static_cast<a3m::BlendFactor>(src),
        static_cast<a3m::BlendFactor>(src),
        static_cast<a3m::BlendFactor>(dst),
        static_cast<a3m::BlendFactor>(dst));
  }

  void setBlendFactors(int srcRgb, int srcAlpha, int dstRgb, int dstAlpha)
  {
    m_native->setBlendFactors(
        static_cast<a3m::BlendFactor>(srcRgb),
        static_cast<a3m::BlendFactor>(srcAlpha),
        static_cast<a3m::BlendFactor>(dstRgb),
        static_cast<a3m::BlendFactor>(dstAlpha));
  }

  int getBlendFactorSrcRgb() const
  {
    return static_cast<int>(m_native->getSrcRgbBlendFactor());
  }

  int getBlendFactorSrcAlpha() const
  {
    return static_cast<int>(m_native->getSrcAlphaBlendFactor());
  }

  int getBlendFactorDstRgb() const
  {
    return static_cast<int>(m_native->getDstRgbBlendFactor());
  }

  int getBlendFactorDstAlpha() const
  {
    return static_cast<int>(m_native->getDstAlphaBlendFactor());
  }

  void setBlendFunctions(int rgb, int alpha)
  {
    m_native->setBlendFunctions(
        static_cast<a3m::BlendFunction>(rgb),
        static_cast<a3m::BlendFunction>(alpha));
  }

  int getBlendFunctionRgb() const
  {
    return static_cast<int>(m_native->getRgbBlendFunction());
  }

  int getBlendFunctionAlpha() const
  {
    return static_cast<int>(m_native->getAlphaBlendFunction());
  }

  void setForceOpaque(bool flag)
  {
    m_native->setForceOpaque(flag);
  }

  bool getForceOpaque() const
  {
    return m_native->getForceOpaque();
  }

  bool isOpaque() const
  {
    return m_native->isOpaque();
  }

  void setColourMask(bool r, bool g, bool b, bool a)
  {
    m_native->setColourMask(r, g, b, a);
  }

  bool getColourMaskR() const
  {
    return m_native->getColourMaskR();
  }

  bool getColourMaskG() const
  {
    return m_native->getColourMaskG();
  }

  bool getColourMaskB() const
  {
    return m_native->getColourMaskB();
  }

  bool getColourMaskA() const
  {
    return m_native->getColourMaskA();
  }

  void setDepthOffsetFactor(float factor)
  {
    m_native->setDepthOffset(factor, getDepthOffsetUnits());
  }

  float getDepthOffsetFactor() const
  {
    return m_native->getDepthOffsetFactor();
  }

  void setDepthOffsetUnits(float units)
  {
    m_native->setDepthOffset(getDepthOffsetFactor(), units);
  }

  float getDepthOffsetUnits() const
  {
    return m_native->getDepthOffsetUnits();
  }

  void setDepthTestFunction(int function)
  {
    m_native->setDepthTestFunction(static_cast<a3m::DepthFunction>(function));
  }

  int getDepthTestFunction() const
  {
    return static_cast<int>(m_native->getDepthTestFunction());
  }

  void setDepthTestEnabled(bool flag)
  {
    m_native->setDepthTestEnabled(flag);
  }

  bool isDepthTestEnabled() const
  {
    return m_native->isDepthTestEnabled();
  }

  void setDepthWriteEnabled(bool flag)
  {
    m_native->setDepthWriteEnabled(flag);
  }

  bool isDepthWriteEnabled() const
  {
    return m_native->isDepthWriteEnabled();
  }

  void setScissorTestEnabled(bool flag)
  {
    m_native->setScissorTestEnabled(flag);
  }

  bool isScissorTestEnabled() const
  {
    return m_native->isScissorTestEnabled();
  }

  void setScissorBox(int left, int bottom, int width, int height)
  {
    m_native->setScissorBox(left, bottom, width, height);
  }

  int getScissorBoxLeft() const
  {
    return m_native->getScissorBoxLeft();
  }

  int getScissorBoxBottom() const
  {
    return m_native->getScissorBoxBottom();
  }

  int getScissorBoxWidth() const
  {
    return m_native->getScissorBoxWidth();
  }

  int getScissorBoxHeight() const
  {
    return m_native->getScissorBoxHeight();
  }

  void setStencilFunction(int face, int function, int reference, int mask)
  {
    m_native->setStencilFunction(
        static_cast<a3m::StencilFace>(face),
        static_cast<a3m::StencilFunction>(function),
        reference,
        mask);
  }

  int getStencilFunction(int face) const
  {
    return static_cast<int>(m_native->getStencilFunction(
        static_cast<a3m::StencilFace>(face)));
  }

  int getStencilReference(int face) const
  {
    return m_native->getStencilReference(
        static_cast<a3m::StencilFace>(face));
  }

  int getStencilReferenceMask(int face) const
  {
    return m_native->getStencilReferenceMask(
        static_cast<a3m::StencilFace>(face));
  }

  void setStencilOperations(
      int face,
      int stencilFail,
      int stencilPassDepthFail,
      int stencilPassDepthPass)
  {
    m_native->setStencilOperations(
        static_cast<a3m::StencilFace>(face),
        static_cast<a3m::StencilOperation>(stencilFail),
        static_cast<a3m::StencilOperation>(stencilPassDepthFail),
        static_cast<a3m::StencilOperation>(stencilPassDepthPass));
  }

  int getStencilFail(int face) const
  {
    return static_cast<int>(m_native->getStencilFail(
        static_cast<a3m::StencilFace>(face)));
  }

  int getStencilPassDepthFail(int face) const
  {
    return static_cast<int>(m_native->getStencilPassDepthFail(
        static_cast<a3m::StencilFace>(face)));
  }

  int getStencilPassDepthPass(int face) const
  {
    return static_cast<int>(m_native->getStencilPassDepthPass(
        static_cast<a3m::StencilFace>(face)));
  }

  void setStencilMask(int face, int mask) const
  {
    m_native->setStencilMask(static_cast<a3m::StencilFace>(face), mask);
  }

  int getStencilMask(int face) const
  {
    return m_native->getStencilMask(static_cast<a3m::StencilFace>(face));
  }

  bool propertyExists(char const* name) const
  {
    return m_native->propertyExists(name);
  }

  void setBoolean(char const* name, bool value, int i = 0)
  {
    m_native->setProperty(name, value, i);
  }

  bool getBoolean(char const* name, int i = 0) const
  {
    bool value = false;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value;
  }

  void setInt(char const* name, int value, int i = 0)
  {
    m_native->setProperty(name, value, i);
  }

  int getInt(char const* name, int i = 0) const
  {
    int value = 0;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value;
  }

  void setFloat(char const* name, float value, int i = 0)
  {
    m_native->setProperty(name, value, i);
  }

  float getFloat(char const* name, int i = 0) const
  {
    float value = 0.0f;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value;
  }

  void setVector2b(char const* name, bool x, bool y, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector2b(x, y), i);
  }

  bool getVector2bX(char const* name, int i = 0) const
  {
    a3m::Vector2b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  bool getVector2bY(char const* name, int i = 0) const
  {
    a3m::Vector2b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  void setVector3b(char const* name, bool x, bool y, bool z, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector3b(x, y, z), i);
  }

  bool getVector3bX(char const* name, int i = 0) const
  {
    a3m::Vector3b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  bool getVector3bY(char const* name, int i = 0) const
  {
    a3m::Vector3b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  bool getVector3bZ(char const* name, int i = 0) const
  {
    a3m::Vector3b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  void setVector4b(char const* name, bool x, bool y, bool z, bool w, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector4b(x, y, z, w), i);
  }

  bool getVector4bX(char const* name, int i = 0) const
  {
    a3m::Vector4b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  bool getVector4bY(char const* name, int i = 0) const
  {
    a3m::Vector4b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  bool getVector4bZ(char const* name, int i = 0) const
  {
    a3m::Vector4b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  bool getVector4bW(char const* name, int i = 0) const
  {
    a3m::Vector4b value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.w;
  }

  void setVector2i(char const* name, int x, int y, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector2i(x, y), i);
  }

  int getVector2iX(char const* name, int i = 0) const
  {
    a3m::Vector2i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  int getVector2iY(char const* name, int i = 0) const
  {
    a3m::Vector2i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  void setVector3i(char const* name, int x, int y, int z, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector3i(x, y, z), i);
  }

  int getVector3iX(char const* name, int i = 0) const
  {
    a3m::Vector3i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  int getVector3iY(char const* name, int i = 0) const
  {
    a3m::Vector3i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  int getVector3iZ(char const* name, int i = 0) const
  {
    a3m::Vector3i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  void setVector4i(char const* name, int x, int y, int z, int w, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector4i(x, y, z, w), i);
  }

  int getVector4iX(char const* name, int i = 0) const
  {
    a3m::Vector4i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  int getVector4iY(char const* name, int i = 0) const
  {
    a3m::Vector4i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  int getVector4iZ(char const* name, int i = 0) const
  {
    a3m::Vector4i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  int getVector4iW(char const* name, int i = 0) const
  {
    a3m::Vector4i value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.w;
  }

  void setVector2f(char const* name, float x, float y, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector2f(x, y), i);
  }

  float getVector2fX(char const* name, int i = 0) const
  {
    a3m::Vector2f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  float getVector2fY(char const* name, int i = 0) const
  {
    a3m::Vector2f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  void setVector3f(char const* name, float x, float y, float z, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector3f(x, y, z), i);
  }

  float getVector3fX(char const* name, int i = 0) const
  {
    a3m::Vector3f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  float getVector3fY(char const* name, int i = 0) const
  {
    a3m::Vector3f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  float getVector3fZ(char const* name, int i = 0) const
  {
    a3m::Vector3f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  void setVector4f(char const* name, float x, float y, float z, float w, int i = 0)
  {
    m_native->setProperty(name, a3m::Vector4f(x, y, z, w), i);
  }

  float getVector4fX(char const* name, int i = 0) const
  {
    a3m::Vector4f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.x;
  }

  float getVector4fY(char const* name, int i = 0) const
  {
    a3m::Vector4f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.y;
  }

  float getVector4fZ(char const* name, int i = 0) const
  {
    a3m::Vector4f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.z;
  }

  float getVector4fW(char const* name, int i = 0) const
  {
    a3m::Vector4f value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return value.w;
  }

  void setMatrix4f(char const* name, float const mtx[], int i = 0)
  {
    m_native->setProperty(name, a3m::Matrix4f(
    a3m::Vector4f(mtx[0], mtx[1], mtx[2], mtx[3]),
    a3m::Vector4f(mtx[4], mtx[5], mtx[6], mtx[7]),
    a3m::Vector4f(mtx[8], mtx[9], mtx[10], mtx[11]),
    a3m::Vector4f(mtx[12], mtx[13], mtx[14], mtx[15])
    ), i);
  }

  void setTexture2D(char const* name, A3mTexture2D const& value, int i = 0)
  {
    m_native->setProperty(name, value.getNative(), i);
  }

  A3mTexture2D* getTexture2D(char const* name, int i = 0) const
  {
    a3m::Texture2D::Ptr value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return A3mTexture2D::toWrapper(value);
  }

  void setTextureCube(char const* name, A3mTextureCube const& value, int i = 0)
  {
    m_native->setProperty(name, value.getNative(), i);
  }

  A3mTextureCube* getTextureCube(char const* name, int i = 0) const
  {
    a3m::TextureCube::Ptr value;
    a3m::ShaderUniformBase::Ptr uniform = m_native->getPropertyUniform(name);

    if (uniform)
    {
      uniform->getValue(value, i);
    }

    return A3mTextureCube::toWrapper(value);
  }
};

#endif // JNI_A3M_APPEARANCE_H
