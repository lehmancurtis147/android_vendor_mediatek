REM Runs SWIG using the a3m.i interface file, generating A3M Java wrapper files
REM and a C++ wrapper file.

..\other\swig\swig -v -c++ -java -o jni/a3m_wrap.cpp -outdir java/com/mediatek/ja3m -package com.mediatek.ja3m a3m.i
