/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
/** \file
 * Android Resource Stream Implementation
 */

/******************************************************************************
 * Include Files
 ******************************************************************************/
#include <cstring> /* for memcpy */

#include <resourcestream.h> /* this file's header */
#include <a3m/log.h> /* pssLogError */

/******************
 * ResourceStream *
 ******************/

ResourceStream::ResourceStream(JNIEnv* env, jbyteArray jByteArray) :
  m_byteArray(env, jByteArray),
  m_index(0)
{
}

A3M_BOOL ResourceStream::valid()
{
  return A3M_TRUE;
}

A3M_BOOL ResourceStream::eof()
{
  return (m_index >= m_byteArray.getLength());
}

A3M_INT32 ResourceStream::size()
{
  return m_byteArray.getLength();
}

A3M_INT32 ResourceStream::seek(A3M_UINT32 offset)
{
  m_index = std::min(static_cast<A3M_INT32>(offset), size());
  return m_index;
}

A3M_INT32 ResourceStream::tell()
{
  return m_index;
}

A3M_INT32 ResourceStream::read(void* dest, A3M_UINT32 byteLength)
{
  A3M_INT32 prevIndex = m_index;

  A3M_INT32 actualByteLength = std::min(
                                 static_cast<A3M_INT32>(byteLength),
                                 size() - m_index);
  memcpy(dest, m_byteArray.getByteArray() + m_index, actualByteLength);
  m_index += actualByteLength;

  pssLogInfo(__FILE__, __FUNCTION__, __LINE__,
             "ResourceStream::read [size=%d, index=(%d, %d), requested=%d, read=%d]",
             size(), prevIndex, m_index, byteLength, actualByteLength);
  return actualByteLength;
}

A3M_INT32 ResourceStream::write(const void*, A3M_UINT32)
{
  // Stream is not writable
  return 0;
}

/************************
 * ResourceStreamSource *
 ************************/

ResourceStreamSource::ResourceStreamSource(
  JNIEnv* env, jobject jResourceDataSource) :
  m_name("ResourceStreamSource"),
  m_jResourceDataSource(env, jResourceDataSource),
  m_jGet(),
  m_jExists()
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    pssLogError(__FILE__, __FUNCTION__, __LINE__,
                "Failed to acquire JavaVM");
    return;
  }

  // Get ResourceDataSource class reference (local)
  jclass jResourceDataSourceClass =
    env->FindClass("com/mediatek/ja3m/ResourceDataSource");

  // Cache the get() method for later use
  m_jGet = env->GetMethodID(jResourceDataSourceClass,
                            "get",
                            "(Ljava/lang/String;)[B");

  // Cache the exists() method for later use
  m_jExists = env->GetMethodID(jResourceDataSourceClass,
                               "exists",
                               "(Ljava/lang/String;)Z");
}

A3M_BOOL ResourceStreamSource::exists(const A3M_CHAR8* stream)
{
  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    pssLogError(__FILE__, __FUNCTION__, __LINE__,
                "Failed to acquire JNIEnv");
    return A3M_FALSE;
  }

  CString name(env, stream);

  jboolean jExists = JNI_FALSE;

  if (name.isValid())
  {
    // Call the Java-side object to determine whether the asset exists
    jExists = env->CallBooleanMethod(
                m_jResourceDataSource.get(), m_jExists,
                name.getJString());
  }

  return jExists;
}

a3m::Stream::Ptr ResourceStreamSource::open(
  const A3M_CHAR8* stream, A3M_BOOL)
{
  a3m::Stream::Ptr streamPtr;

  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    pssLogError(__FILE__, __FUNCTION__, __LINE__,
                "Failed to acquire JNIEnv");
    return streamPtr;
  }

  CString name(env, stream);

  if (name.isValid())
  {
    // Call the Java-side object to aquire the asset
    jbyteArray jByteArray = static_cast<jbyteArray>(env->CallObjectMethod(
                              m_jResourceDataSource.get(), m_jGet,
                              name.getJString()));

    if (jByteArray)
    {
      streamPtr.reset(new ResourceStream(env, jByteArray));
      pssLogInfo(__FILE__, __FUNCTION__, __LINE__,
                 "%s %s opened successfully", m_name.c_str(), stream);
    }
    else
    {
      pssLogError(__FILE__, __FUNCTION__, __LINE__,
                  "%s %s not found", m_name.c_str(), stream);
    }
  }

  return streamPtr;
}

A3M_CHAR8 const* ResourceStreamSource::getName() const
{
  return m_name.c_str();
}

/** @} */
