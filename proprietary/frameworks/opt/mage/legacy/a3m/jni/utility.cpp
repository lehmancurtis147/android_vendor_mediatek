/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <a3m/log.h>                /* for A3M_LOG_ERROR()                   */
#include <utility.h>                /* this module's header                  */

/*****************************************************************************
 *                                CString                                    *
 *****************************************************************************/

CString::CString(JNIEnv* env, jstring string) :
  m_jString(env, string),
  m_cString(0)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }

  // Try to acquire the Java string as a C-string.
  if (m_jString.get())
  {
    m_cString = env->GetStringUTFChars(m_jString.get(), 0);
  }
}

CString::CString(JNIEnv* env, A3M_CHAR8 const* string) :
  m_jString(env),
  m_cString(0)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }

  // Create Java string and get the corresponding C-string.
  if (string)
  {
    m_jString.reset(env->NewStringUTF(string));
    m_cString = env->GetStringUTFChars(m_jString.get(), 0);
  }
}

CString::~CString()
{
  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    A3M_LOG_ERROR("Failed to acquire JNIEnv", 0);
    return;
  }

  if (m_cString)
  {
    env->ReleaseStringUTFChars(m_jString.get(), m_cString);
  }
}

jstring CString::getJString() const
{
  return m_jString.get();
}

const char* CString::getString() const
{
  return m_cString;
}

bool CString::isValid() const
{
  return (m_cString != 0);
}

/*****************************************************************************
 *                               CByteArray                                  *
 *****************************************************************************/

CByteArray::CByteArray(JNIEnv* env, jbyteArray byteArray) :
  m_jByteArray(env, byteArray),
  m_cByteArray(0),
  // JNI_ABORT specifies that the array contents should not be copied back
  // into the Java byte array object.
  m_jMode(JNI_ABORT)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }

  // Try to acquire contents of the array as a C-byte array.
  if (m_jByteArray.get())
  {
    m_cByteArray = env->GetByteArrayElements(m_jByteArray.get(), 0);
  }
}

CByteArray::CByteArray(JNIEnv* env, jsize size) :
  m_jByteArray(env),
  m_cByteArray(0),
  // 0 specifies that the array contents should be copied back into the Java
  // byte array object.
  m_jMode(0)
{
  if (env->GetJavaVM(&m_vm) < 0)
  {
    A3M_LOG_ERROR("Failed to acquire JavaVM", 0);
    return;
  }
  env->GetJavaVM(&m_vm);

  // Create a new Java array.
  m_jByteArray.reset(env->NewByteArray(size));
  m_cByteArray = env->GetByteArrayElements(m_jByteArray.get(), 0);
}

CByteArray::~CByteArray()
{
  commit();
}

void CByteArray::commit()
{
  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    A3M_LOG_ERROR("Failed to acquire JNIEnv", 0);
    return;
  }

  // Release acquired C-array with appropriate mode.
  if (m_cByteArray)
  {
    env->ReleaseByteArrayElements(m_jByteArray.get(), m_cByteArray, m_jMode);
    m_cByteArray = 0;
  }
}

jbyteArray CByteArray::getJByteArray() const
{
  return m_jByteArray.get();
}

jbyte* CByteArray::getByteArray()
{
  return m_cByteArray;
}

jbyte const* CByteArray::getByteArray() const
{
  return m_cByteArray;
}

int CByteArray::getLength() const
{
  JNIEnv* env;
  if (m_vm->GetEnv(reinterpret_cast<void**>(&env), JNI_VERSION_1_6) != JNI_OK)
  {
    A3M_LOG_ERROR("Failed to acquire JNIEnv", 0);
    return 0;
  }

  if (isValid())
  {
    return env->GetArrayLength(m_jByteArray.get());
  }
  else
  {
    return 0;
  }
}

bool CByteArray::isValid() const
{
  return (m_cByteArray != 0);
}
