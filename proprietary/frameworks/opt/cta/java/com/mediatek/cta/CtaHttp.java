package com.mediatek.cta;

import com.android.okhttp.MediaType;
import com.android.okhttp.Protocol;
import com.android.okhttp.Request;
import com.android.okhttp.Response;
import com.android.okhttp.ResponseBody;
import com.android.okhttp.okio.Buffer;
import com.android.okhttp.okio.BufferedSource;

import java.util.List;

/**
 * CTA implementation
 */
final class CtaHttp {
    //OkHTTP
    public static boolean isMmsAndEmailSendingPermitted(Request request) {
      if (isMoMMS(request)) {
          if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_MMS","Send MMS")) {
              System.out.println("Fail to send due to user permission");
              return false;
          }
      } else if (isEmailSend(request)) {
          if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_EMAIL","Send emails")) {
              System.out.println("Fail to send due to user permission");
              return false;
          }
      }

      return true;
    }

    public boolean isMmsSendPdu(byte[] pdu) {

        int len = pdu.length;
        if (len >= 2) {
            //X-Mms-Message-Type: m-send-req (0x80)
            if ((pdu[0] & 0xFF) == 0x8C && (pdu[1] & 0xFF) == 0x80) {
                System.out.println("is MMS send PDU");
                return true;
            }
        }
        return false;
    }

    private static boolean isMoMMS(Request request) {
      final String mimetype = "application/vnd.wap.mms-message";

      if ("POST".equals(request.method())) {
          String userAgent = request.header("User-Agent");
          if (userAgent != null && userAgent.indexOf("MMS") != -1) {
              return true;
          } else {
              String contentType = request.header("Content-Type");
              if (contentType != null) {
                  if (contentType.indexOf(mimetype) != -1) {
                      System.out.println("is MMS send");
                      return true;
                  }
              }

              String acceptType = request.header("Accept");
              if (acceptType != null) {
                  if (acceptType.indexOf(mimetype) != -1) {
                      System.out.println("is MMS send");
                      return true;
                  }
              }

              List<String> contentTypes = request.headers().values("Content-Type");
              for (String value : contentTypes) {
                  if (value.indexOf(mimetype) != -1) {
                      System.out.println("is MMS send");
                      return true;
                  }
              }
          }
      }

      return false;
    }

    private static boolean isEmailSend(Request request) {
      final String mimetype = "application/vnd.ms-sync.wbxml";

       if ("POST".equals(request.method())
            || "PUT".equals(request.method())) {
          String contentType = request.header("Content-Type");
          if (contentType != null) {
              if (contentType.startsWith("message/rfc822")) {
                  System.out.println("is Email send");
                  return true;
              }
          }

          List<String> contentTypes = request.headers().values("Content-Type");
          for (String value : contentTypes) {
              if (value.startsWith("message/rfc822")) {
                  System.out.println("is Email send");
                  return true;
              }
          }
      }
      return false;
    }

    public static Response getBadResponse() {
      ResponseBody emptyResponseBody = new ResponseBody() {
          @Override public MediaType contentType() {
            return null;
          }
          @Override public long contentLength() {
            return 0;
          }
          @Override public BufferedSource source() {
            return new Buffer();
          }
      };

      Response badResponse = new Response.Builder()
          .protocol(Protocol.HTTP_1_1)
          .code(400)
          .message("User Permission is denied")
          .body(emptyResponseBody)
          .build();

      return badResponse;
    }

    //Socket
    public static boolean isSendingPermitted(int port) {
        System.out.println("port:" + port);
        if (port == 25 || port == 465 || port == 587) {
            System.out.println("port:" + port);
            if (!CtaUtils.enforceCheckPermission("com.mediatek.permission.CTA_SEND_EMAIL", "Send emails")) {
                System.out.println("Fail to send due to user permission");
                return false;
            }
        }

        return true;
    }
}
