package com.mediatek.common.wallpaper;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.content.res.Resources;
import android.content.Context;
/**
 * Default implementation of Plug-in definition of Wallpaper.
 */
public class DefaultWallpaperPlugin extends ContextWrapper implements IWallpaperPlugin {
    private static final String TAG = "DefaultWallpaperPlugin";

    /**
     * Constructs a new DefaultWallpaperPluginPlugin instance with Context.
     * @param context A Context object
     */
    public DefaultWallpaperPlugin(Context context) {
        super(context);
    }

    /**
     * @return Return the resources object of plug-in package.
     */
  @Override
    public Resources getPluginResources(Context context){
      Log.d(TAG, "into getPluginResources: null");
      return null;
  }

    /**
     * @return Return res id of default wallpaper resource.
     */
   @Override
    public int getPluginDefaultImage(){
      Log.d(TAG, "into getPluginDefaultImage: null");
      return 0;
  }

}
