package com.mediatek.cellbroadcastreceiver;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class CmasConfigManager {
    private static final String TAG = "[CMAS]CmasConfigManager";

    private static int sSettingProfileId = 0;
    private static int[] sAdditionalChannels = {};
    private static int[] sVibrationPattern = {};
    private static String sRingtone;
    private static boolean sUseFullVolume = false;

    public static final int PROFILE_ID_COMMON = 0;
    public static final int PROFILE_ID_TW     = 1;
    public static final int PROFILE_ID_TRA    = 2;
    public static final int PROFILE_ID_CHILE  = 3;

    public static void updateConfigInfos(Context context) {
        sSettingProfileId = context.getResources().getInteger(R.integer.cmas_setting_profile_id);
        sAdditionalChannels = context.getResources().getIntArray(R.array.additional_cbs_channels);
        sVibrationPattern = context.getResources().getIntArray(R.array.default_vibration_pattern);
        sRingtone = context.getResources().getString(R.string.defalut_alert_ringtone);
        sUseFullVolume = context.getResources().getBoolean(R.bool.use_full_volume);
    }

    public static int getSettingProfileId() {
        Log.d(TAG, "getSettingProfileId as " + sSettingProfileId);
        return sSettingProfileId;
    }

    public static int[] getAdditionalChannels() {
        return sAdditionalChannels;
    }

    public static int[] getVibrationPattern() {
        return sVibrationPattern;
    }

    public static String getRingtone() {
        return sRingtone;
    }

    public static boolean getUseFullVolume() {
        return sUseFullVolume;
    }

    public static boolean isTwProfile()  {
        return sSettingProfileId == PROFILE_ID_TW;
    }

    public static boolean isChileProfile() {
        return sSettingProfileId == PROFILE_ID_CHILE;
    }

    public static boolean isTraProfile() {
        return sSettingProfileId == PROFILE_ID_TRA;
    }

    public static boolean isAlertsEnabled(Context context) {
        boolean enableAlerts = true;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sSettingProfileId) {
        case PROFILE_ID_COMMON:
        case PROFILE_ID_CHILE:
        case PROFILE_ID_TW:
            enableAlerts = prefs.getBoolean(
                    CheckBoxAndSettingsPreference.KEY_ENABLE_CELLBROADCAST, true);
            break;
        case PROFILE_ID_TRA:
            enableAlerts = true;
            break;
        default:
            break;
        }
        return enableAlerts;
    }

    public static boolean isEtwsEnabled(Context context) {
        boolean enableAlerts = true;
        switch (sSettingProfileId) {
        case PROFILE_ID_COMMON:
        case PROFILE_ID_CHILE:
        case PROFILE_ID_TW:
            enableAlerts = true;
            break;
        case PROFILE_ID_TRA:
            enableAlerts = false;
            break;
        default:
            break;
        }
        return enableAlerts;
    }

    public static boolean isExerciseEnable(Context context) {
        boolean enableAlerts = true;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        switch (sSettingProfileId) {
        case PROFILE_ID_COMMON:
        case PROFILE_ID_CHILE:
        case PROFILE_ID_TW:
            enableAlerts = prefs.getBoolean(
                CellBroadcastConfigService.ENABLE_CMAS_EXERCISE_SUPPORT, false);
            break;
        case PROFILE_ID_TRA:
            enableAlerts = true;
            break;
        default:
            break;
        }
        return enableAlerts;
    }
}
