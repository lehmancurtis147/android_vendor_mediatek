/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.cellbroadcastreceiver;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.telephony.CellBroadcastMessage;
import android.telephony.SmsCbCmasInfo;
import android.telephony.SmsCbLocation;
import android.telephony.SmsCbMessage;
import android.telephony.SmsManager;
import android.util.Log;

import com.mediatek.cmas.ext.ICmasDuplicateMessageExt;
import com.mediatek.cmas.ext.ICmasMainSettingsExt;

import java.util.ArrayList;
import java.util.HashSet;

import mediatek.telephony.MtkSmsManager;

/**
 * This service manages the display and animation of broadcast messages.
 * Emergency messages display with a flashing animated exclamation mark icon,
 * and an alert tone is played when the alert is first shown to the user
 * (but not when the user views a previously received broadcast).
 */
public class CellBroadcastAlertService extends Service {
    private static final String TAG = "[CMAS]CellBroadcastAlertService";

    /** Intent action to display alert dialog/notification, after verifying the alert is new. */
    static final String SHOW_NEW_ALERT_ACTION = "cellbroadcastreceiver.SHOW_NEW_ALERT";

    public static final String CB_ROWID = "msg_rowid";

    private static final String PREF_NAME = "com.mediatek.cellbroadcastreceiver_preferences";

    /** Use the same notification ID for non-emergency alerts. */
    static final int NOTIFICATION_ID = 1;

    private static Comparer sCompareList = new Comparer();

    private static final int MSG_OVER_12HOURS = 301;
    private InternalHandler mTimeoutHandler;

    /**
     * internal handler for events.
     */
    private class InternalHandler extends Handler {
        public InternalHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_OVER_12HOURS:
                //delete message in modem
                MtkSmsManager smsManager = MtkSmsManager.getDefault();
                Log.d(TAG, "Before delete message, msgid = " + msg.arg1 + " sn = " + msg.arg2);
                smsManager.removeCellBroadcastMsg(msg.arg1, msg.arg2);
                break;

            default:
                break;
            }
        }
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if (Telephony.Sms.Intents.SMS_EMERGENCY_CB_RECEIVED_ACTION.equals(action) ||
                Telephony.Sms.Intents.SMS_CB_RECEIVED_ACTION.equals(action)) {
            startHandlerThread();
            handleCellBroadcast(intent);
        } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action)) {
            sCompareList.clear();
        } else {
            Log.e(TAG, "Unrecognized intent action: " + action);
        }
        return START_NOT_STICKY;
    }

    private void startHandlerThread() {
        HandlerThread handlerThread = new HandlerThread("CMASAlertServiceThread");
        handlerThread.start();
        Looper cmasLooper = getMainLooper(); //handlerThread.getLooper();
        if (cmasLooper != null) {
            mTimeoutHandler = new InternalHandler(cmasLooper);
        }
    }

    private void handleCellBroadcast(Intent intent) {
        Log.d(TAG, "handleCellBroadcast " + intent);
        Bundle extras = intent.getExtras();
        if (extras == null) {
            Log.e(TAG, "received SMS_CB_RECEIVED_ACTION with no extras!");
            return;
        }

        final SmsCbMessage message = (SmsCbMessage) extras.get("message");

        if (message == null) {
            Log.e(TAG, "received SMS_CB_RECEIVED_ACTION with no message extra");
            return;
        }

        final CellBroadcastMessage cbm = new CellBroadcastMessage(message);
        if (!isMessageEnabledByUser(cbm)) {
            Log.d(TAG, "ignoring alert of type " + cbm.getServiceCategory() + " by user preference");
            return;
        }

        ICmasMainSettingsExt optrMainSetting = (ICmasMainSettingsExt)
                CellBroadcastPluginManager.getCellBroadcastPluginObject(
                CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_MAIN_SETTINGS);

        if (optrMainSetting.needBlockMessageInEcbm()){
            Log.d(TAG, "ignoring EmergencyCallbackMode of type " + cbm.getServiceCategory());
            return;
        }

        boolean spanishAlerts = optrMainSetting.isSpanishAlert(message.getLanguageCode(),
        cbm.getServiceCategory());
        if (!spanishAlerts) {
            Log.d(TAG, "ignoring spanish alert of type " + cbm.getServiceCategory() +
            " by user preference");
            return;
        }

        final Intent alertIntent = new Intent(SHOW_NEW_ALERT_ACTION);
        alertIntent.setClass(this, CMASPresentationService.class);
        alertIntent.putExtra("message", cbm);

        // write to database on a background thread
        new CellBroadcastContentProvider.AsyncCellBroadcastTask(getContentResolver())
                .execute(new CellBroadcastContentProvider.CellBroadcastOperation() {
                    @Override
                    public boolean execute(CellBroadcastContentProvider provider) {
                        int res = ICmasDuplicateMessageExt.NEW_CMAS_PROCESS;
                        // ICmasDuplicateMessageExt optDetection = getOperatorInterfaceFordupulication();
                        ICmasDuplicateMessageExt optDetection = (ICmasDuplicateMessageExt)
                             CellBroadcastPluginManager.getCellBroadcastPluginObject(
                             CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_DUPLICATE_MESSAGE);
                        // Log.d(TAG, "execute, get ICmasDuplicateMessageExt " + optDetection);
                        if (optDetection != null && !message.isEtwsMessage()) {
                            res = optDetection.handleDuplicateMessage(provider, cbm);
                            Log.d(TAG, "res = " + res);
                        }

                        if (!sCompareList.add(cbm) && res != ICmasDuplicateMessageExt.OVER_12_HOURS_IDENTIFY_MSG && res != ICmasDuplicateMessageExt.PRESENT_CMAS_PROCESS) {
                            // reject the duplicate cmas in non power-off & non off-airplane
                            Log.d(TAG, "ignoring duplicate alert with " + cbm);
                            return false;
                        }

                        switch (res) {
                        case ICmasDuplicateMessageExt.OVER_12_HOURS_IDENTIFY_MSG:
                            //update db and show msg to user
                            long rowNewId = provider.getRowId(message);
                            Log.i(TAG, "rowId = " + rowNewId);

                            if (rowNewId > -1) {
                                provider.updateOldBroadcast(cbm, rowNewId);
                                alertIntent.putExtra(CB_ROWID, rowNewId);
                                startService(alertIntent);

                                mTimeoutHandler.sendMessageDelayed(mTimeoutHandler.obtainMessage(
                                        MSG_OVER_12HOURS, cbm.getServiceCategory(), cbm.getSerialNumber()), Comparer.REGARD_AS_NEW_TIMEOUT); //43200000:12 hours
                            }

                            return true;
                        case ICmasDuplicateMessageExt.NEW_CMAS_PROCESS: // NewCMASProcess
                            // handle update msg
                            if (!handleUpdatedCB(provider, cbm)) {
                                return true;
                            }
                            Log.d(TAG, "before insertNewBroadcast, sn " + cbm.getSerialNumber());
                            //if (provider.insertNewBroadcast(cbm)) {
                            long rowId = provider.addNewBroadcast(cbm);
                            if (rowId > -1) {
                                alertIntent.putExtra(CB_ROWID, rowId);
                                startService(alertIntent);
                                ICmasDuplicateMessageExt optDetect = (ICmasDuplicateMessageExt)
                                CellBroadcastPluginManager.getCellBroadcastPluginObject(
                                    CellBroadcastPluginManager.CELLBROADCAST_PLUGIN_TYPE_DUPLICATE_MESSAGE);
                                Log.d(TAG, "Delete message to receive again host: " +
                                        Comparer.REGARD_AS_NEW_TIMEOUT);
                                optDetect.sendDelayedMsgToDelete(mTimeoutHandler, cbm, MSG_OVER_12HOURS);

                                /*if it OP08, send delay message to delete the msg
                                if ((optDetect != null) && (optDetect.sendDelayedMsgToDelete())) {
                                    mTimeoutHandler.sendMessageDelayed(mTimeoutHandler.obtainMessage(
                                            MSG_OVER_12HOURS, cbm.getServiceCategory(), cbm.getSerialNumber()), Comparer.REGARD_AS_NEW_TIMEOUT); //12 hours
                                }
                                */

                                return true;
                            }
                            return false;
                        case ICmasDuplicateMessageExt.PRESENT_CMAS_PROCESS: // PresentCMASProcess
                            long id = provider.getRowId(message);
                            Log.d(TAG, "id = " + id);
                            if (id > -1) {
                                alertIntent.putExtra(CB_ROWID, id);
                            }
                            startService(alertIntent);
                            return true;
                        case ICmasDuplicateMessageExt.DISCARD_CMAS_PROCESS: // DiscardCMASProcess
                        default:
                            return true;
                        }
                    }
                });
    }

    private boolean handleUpdatedCB(CellBroadcastContentProvider provider
                                                  , CellBroadcastMessage cbm) {
        Cursor c = provider.getAllCellBroadcastCursor();
        if (c != null
           && c.getCount() > 0) {

        Log.d(TAG, "handleUpdatedCB Cursor not null");
        //Comparer oldList = Comparer.createFromCursor(c);

        String oldIDstr = null;
        if (!c.isClosed()) {
        Log.d(TAG, "handleUpdatedCB isClosed");
          oldIDstr = sCompareList.searchUpdatedCBFromCache(c, cbm);
        }
        if (oldIDstr != null) {
            Log.d(TAG, "Checking oldIDstr" + oldIDstr);
            if (oldIDstr.equals(Comparer.INVALID_UPDATE_CB)) {
                Log.d(TAG, "handleUpdatedCB " + oldIDstr);
                return false;
            }

            if (!provider.deleteBroadcast(Integer.valueOf(oldIDstr).intValue())) {
                Log.d(TAG, "error handleUpdateCB,failed to delete ID " + oldIDstr);
            }
        }
       }

        return true;
    }

    /**
     * Filter out broadcasts on the test channels that the user has not enabled,
     * and types of notifications that the user is not interested in receiving.
     * This allows us to enable an entire range of message identifiers in the
     * radio and not have to explicitly disable the message identifiers for
     * test broadcasts. In the unlikely event that the default shared preference
     * values were not initialized in CellBroadcastReceiverApp, the second parameter
     * to the getBoolean() calls match the default values in res/xml/preferences.xml.
     *
     * @param message the message to check
     * @return true if the user has enabled this message type; false otherwise
     */
    private boolean isMessageEnabledByUser(CellBroadcastMessage message) {
        Log.d(TAG, "isMessageEnabledByUser " + message.getServiceCategory());

        if (message.isEtwsMessage()) {
            return true;
        }

        if (message.isCmasMessage()) {
            //Log.d(TAG, "in isMessageEnabledByUser , CMASMessageClass " + message.getCmasMessageClass());
            switch (message.getCmasMessageClass()) {
                case SmsCbCmasInfo.CMAS_CLASS_PRESIDENTIAL_LEVEL_ALERT:
                if (CmasConfigManager.isTwProfile() &&
                        (message.getServiceCategory() == 919 ||
                         message.getServiceCategory() == 911)) {
                    Log.d(TAG, "isMessageEnabledByUser: Return value of 919/911 checkbox ");
                    return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                            CheckBoxAndSettingsPreference.KEY_ENABLE_ALERT_MESSAGE, true);
                }
                return true;
                case SmsCbCmasInfo.CMAS_CLASS_EXTREME_THREAT:
                    return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                            CheckBoxAndSettingsPreference.KEY_ENABLE_CMAS_EXTREME_ALERTS, true);
                case SmsCbCmasInfo.CMAS_CLASS_SEVERE_THREAT:
                    return PreferenceManager.getDefaultSharedPreferences(this).getBoolean(
                            CheckBoxAndSettingsPreference.KEY_ENABLE_CMAS_SEVERE_ALERTS, true);
                case SmsCbCmasInfo.CMAS_CLASS_CHILD_ABDUCTION_EMERGENCY:
                    return PreferenceManager.getDefaultSharedPreferences(this)
                            .getBoolean(CheckBoxAndSettingsPreference.KEY_ENABLE_CMAS_AMBER_ALERTS,
                            true);
                case SmsCbCmasInfo.CMAS_CLASS_REQUIRED_MONTHLY_TEST:
                  boolean resOfRmt = PreferenceManager.getDefaultSharedPreferences(this).
                  getBoolean(CellBroadcastConfigService.ENABLE_CMAS_RMT_SUPPORT, false);
                    Log.d(TAG, "in isMessageEnabledByUser , CMAS setting " + resOfRmt);
                    return resOfRmt;
                case SmsCbCmasInfo.CMAS_CLASS_CMAS_EXERCISE:
                    boolean resOfexe = CmasConfigManager.
                            isExerciseEnable(getApplicationContext());
                    Log.d(TAG, "in isMessageEnabledByUser , EXER setting " + resOfexe);
                    return resOfexe;
                case SmsCbCmasInfo.CMAS_CLASS_OPERATOR_DEFINED_USE:
                  boolean resOfCmsp = PreferenceManager.getDefaultSharedPreferences(this).
                  getBoolean(CellBroadcastConfigService.ENABLE_CMAS_OPERATOR_CHOICE_SUPPORT, false);
                    Log.d(TAG, "in isMessageEnabledByUser,OPERATOR_DEFINED setting " + resOfCmsp);
                    return resOfCmsp;
                default:
                    return true;    // presidential-level CMAS alerts are always enabled
            }
        }

        return false;    // other broadcast messages are always enabled
    }

    static Intent createDisplayMessageIntent(Context context, Class intentClass,
            ArrayList<CellBroadcastMessage> messageList) {
        // Trigger the list activity to fire up a dialog that shows the received messages
        Intent intent = new Intent(context, intentClass);
        intent.putParcelableArrayListExtra(CellBroadcastMessage.SMS_CB_MESSAGE_EXTRA, messageList);
        return intent;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;    // clients can't bind to this service
    }
}
