package com.mediatek.lbs.em2.ui;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

public class LbsUtility {

    //return true: success
    static public boolean write2File(Context context, String folder, String subFolder,
            String fileName, String data, boolean isAppendMode) {
        DataOutputStream dos;
        String fullFileName;

        File sdFile;
        File dirFile;
        fullFileName = "/sdcard" + File.separator + folder + File.separator;
        // Log.d("LocationEM", "Dump file path: " + fullFileName);

        dirFile = new File(fullFileName);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }

        if (subFolder != null) {
            fullFileName = fullFileName + "/" + subFolder;
            dirFile = new File(fullFileName);
            if(!dirFile.exists()){
                dirFile.mkdirs();
            }
        }

        fullFileName += "/" + fileName;
        dirFile = new File(fullFileName);
        try {
            if (isAppendMode)
                dos = new DataOutputStream(new FileOutputStream(fullFileName, true));
            else
                dos = new DataOutputStream(new FileOutputStream(dirFile));
            dos.writeBytes(data);
            dos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    static public String getTimeString(long milliseconds) {
            Date date = new Date(milliseconds);
            String str = (date.getYear() + 1900) + "/" + (date.getMonth() + 1) + "/" + date.getDate() +
                    " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                    //+ "." + String.format("%03d", milliseconds%1000);
            return str;
        }

    static public String getTimeString2(long milliseconds) {
        Date date = new Date(milliseconds);
        String str = String.format("%04d%02d%02d_%02d%02d%02d",
                (date.getYear() + 1900),
                (date.getMonth() + 1),
                date.getDate(),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds());
        return str;
    }

    static public String getTimeString3(long milliseconds) {
        Date date = new Date(milliseconds);
        String str = String.format("%02d/%02d/%04d %02d:%02d:%02d",
                (date.getMonth() + 1),
                date.getDate(),
                (date.getYear() + 1900),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds());
        return str;
    }

    static public String[] split(String str) {
        String[] result = null;
        try {
            String delims = "[,]";
            result = str.split(delims);
        } catch (Exception e) {
            Log.d("nmeaParser", "split:" + e);
        }
        return result;
    }


    static public int parseInt(String str) {
        int d = 0;
        if (str.equals("")) {
            return d;
        }
        String mStr = removeFirstZero(str);
        try
        {
            d = Integer.valueOf(mStr);
        }
        catch (Exception e) {
            Log.d("nmeaParser", "parseDouble:" + e);
        }
        return d;
    }

    static private String removeFirstZero(String record) {
        String result = record;
        int ind = 0;
        while (record.charAt(ind++) == '0') {
            if (ind == record.length())
                break;
        }
        if (ind != 0) {
            result = record.substring(--ind);
        }
        return result;
    }


}
