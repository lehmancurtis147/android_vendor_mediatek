package com.mediatek.mtklogger.controller;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.BTFWLogLevel;
import com.log.handler.LogHandlerUtils.LogType;

/**
 * @author MTK81255
 *
 */
public class BTHostLogController extends AbstractLogController {

    public static BTHostLogController sInstance = new BTHostLogController(LogType.BTHOST_LOG);

    private BTHostLogController(LogType logType) {
        super(logType);
    }

    public static BTHostLogController getInstance() {
        return sInstance;
    }

    /**
     * @param enalbe
     *            boolean
     * @return boolean
     */
    public boolean setBTHostDebuglogEnable(boolean enalbe) {
        return LogHandler.getInstance().setBTHostDebuglogEnable(enalbe);
    }

    /**
     * @param btFWLogLevel
     *            String
     * @return boolean
     */
    public boolean setBTFWLogLevel(String btFWLogLevel) {
        return LogHandler.getInstance()
                .setBTFWLogLevel(BTFWLogLevel.getBTFWLogLevelByID(btFWLogLevel));
    }

}
