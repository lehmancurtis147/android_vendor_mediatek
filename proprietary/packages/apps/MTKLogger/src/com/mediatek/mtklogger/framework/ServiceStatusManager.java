package com.mediatek.mtklogger.framework;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

import com.mediatek.mtklogger.MainActivity;
import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.R;
import com.mediatek.mtklogger.controller.LogControllerUtils;
import com.mediatek.mtklogger.utils.Utils;

/**
 * Supper class for each log instance, like network log, mobile log and modem. log
 */
public class ServiceStatusManager {
    private static final String TAG = Utils.TAG + "/ServiceStatusManager";

    private Service mService;
    private Notification.Builder mBuilder = null;
    private NotificationManager mNotificationManager = null;
    private static NotificationChannel sChannel = null;
    public static final int SERVICE_NOTIFICATION_ICON = R.drawable.notification;
    public static final int SERVICE_NOTIFICATION_ID = 365001;
    private static final String NOTIFICATION_CHANNEL_ID = "com.mediatek.mtklogger.notification";

    private ServiceStatus mLastServiceStatus;
    /**
     * @author MTK11515
     * ServiceStatus is MTKLogger service running state.
     */
    public static enum ServiceStatus {
        ONCREATE_DONE, ONSTARTCOMMAND_DONE, LOG_STARTING, LOG_STARTING_DONE,
        LOG_STOPPING, LOG_STOPPING_DONE,
        LOG_RESTARTING, LOG_RESTART_DONE, ONDESTROYING;
    }
    /**
     * @param service Service
     */
    public ServiceStatusManager(Service service) {
        mService = service;
    }
    /**
     * @param status ServiceStatus
     */
    public void statusChanged(ServiceStatus status) {
        Utils.logi(TAG, "statusChanged : " + status.toString()
                   + ", old status = " + mLastServiceStatus);
        String notificationMsg = "";
        switch (status) {
        case ONCREATE_DONE:
            notificationMsg = mService.getString(R.string.service_state_init);
            break;
         //NEED UPDATE LOG STATUS
        case ONSTARTCOMMAND_DONE:
        case LOG_STARTING_DONE:
        case LOG_STOPPING_DONE:
        case LOG_RESTART_DONE:
            notificationMsg = getLogStatusMessage();
            if (notificationMsg == null || notificationMsg.isEmpty()) {
                notificationMsg = mService.getString(R.string.service_state_all_log_stopped);
            }
            break;
        case LOG_STARTING:
            notificationMsg = mService.getString(R.string.service_state_starting_log);
            break;
        case LOG_STOPPING:
            notificationMsg = mService.getString(R.string.service_state_stopping_log);
            break;
        case LOG_RESTARTING:
            notificationMsg = mService.getString(R.string.service_state_restarting_log);
           break;
        case ONDESTROYING:
            mService.stopForeground(true);
            return;
        default:
           break;
        }
        mLastServiceStatus = status;
        if (!notificationMsg.isEmpty()) {
            showServiceNotification(notificationMsg);
        }
    }

    private String getLogStatusMessage() {
        String status = "";
        for (Integer logType : Utils.LOG_TYPE_SET) {
            if (Utils.CONNSYS_LOG_TYPE_SET.contains(logType)) {
                continue;
            }
            if (LogControllerUtils.getLogControllerInstance(logType).isLogRunning()) {
                status += mService.getString(Utils.LOG_NAME_MAP.get(logType)) + ",";
            }
        }
        if (LogControllerUtils.isConnsysLogRunning()) {
            status += mService.getString(Utils.LOG_NAME_MAP.get(Utils.LOG_TYPE_CONNSYSFW)) + ",";
        }
        if (status.isEmpty()) {
            return status;
        }
        if (status.endsWith(",")) {
            status = status.substring(0, status.length() - 1) + " ";
            status += status.contains(",")
                      ? mService.getString(R.string.notification_on_summary_suffixes)
                     : mService.getString(R.string.notification_on_summary_suffix);
        }
        status = status.replaceAll("Log,", ",");
        return status;
    }
    /**
     * void.
     */
    public void updateNotificationTime() {
        if (mBuilder != null) {
            mBuilder.setWhen(System.currentTimeMillis());
        }
    }
    /**
     * @param text String
     */
    public synchronized void showServiceNotification(String text) {
        if (!MyApplication.getInstance().getDefaultSharedPreferences()
                .getBoolean(Utils.KEY_PREFERENCE_NOTIFICATION_ENABLED, true)) {
            Utils.logw(TAG, "Notification is disabled, does not show any notification.");
            return;
        }
        if (mBuilder == null) {
            mBuilder =
                    new Notification.Builder(mService, getNotificationChannelId());
            mBuilder.setContentTitle(mService.getString(R.string.notification_title))
            .setSmallIcon(SERVICE_NOTIFICATION_ICON)
            .setOnlyAlertOnce(true);
        }
        if (!Utils.isAutoTest()) {
            Intent backIntent = new Intent(mService, MainActivity.class);
            backIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent contentIntent = PendingIntent.getActivity(mService, 0, backIntent, 0);
            mBuilder.setContentIntent(contentIntent);
        } else {
            mBuilder.setContentIntent(null);
            Utils.logw(TAG, "Do not allow open activity from status bar when isAutoTest!");
        }
        Utils.logw(TAG, "showServiceNotification(), context = " + text );
        mBuilder.setContentText(text);
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) mService
                                   .getSystemService(Context.NOTIFICATION_SERVICE);
        }
        mService.startForeground(SERVICE_NOTIFICATION_ID, mBuilder.build());
    }
    /**
     * void.
     */
    public void disappearServiceNotification() {
        if (mNotificationManager == null) {
            mNotificationManager = (NotificationManager) mService
                                   .getSystemService(Context.NOTIFICATION_SERVICE);
        }
        Utils.logw(TAG, "disappearServiceNotification()");
        mNotificationManager.cancel(SERVICE_NOTIFICATION_ID);
        mNotificationManager = null;
        mBuilder = null;
    }

    private static NotificationChannel createNotificationChannel() {
        if (sChannel != null) {
            return sChannel;
        }
        NotificationManager notificationManager = (NotificationManager) MyApplication.getInstance()
                           .getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        sChannel = new NotificationChannel(
                   NOTIFICATION_CHANNEL_ID, "MTKLogger", NotificationManager.IMPORTANCE_HIGH);
        notificationManager.createNotificationChannel(sChannel);
        return sChannel;
    }
    /**
     * @return String
     */
    public static String getNotificationChannelId() {
        if (sChannel == null) {
            createNotificationChannel();
        }
        return NOTIFICATION_CHANNEL_ID;
    }

}
