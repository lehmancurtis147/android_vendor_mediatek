package com.mediatek.mtklogger.controller;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.BTFWLogLevel;
import com.log.handler.LogHandlerUtils.LogType;
import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.settings.ConnsysLogSettings;

/**
 * @author MTK81255
 *
 */
public class ConnsysFWLogController extends AbstractLogController {
    public static ConnsysFWLogController sInstance =
            new ConnsysFWLogController(LogType.CONNSYSFW_LOG);

    private ConnsysFWLogController(LogType logType) {
        super(logType);
    }

    public static ConnsysFWLogController getInstance() {
        return sInstance;
    }

    @Override
    public boolean startLog(String logPath) {
        String configPath = getConfigPath();
        if (configPath != null && !configPath.isEmpty()) {
            logPath = configPath;
        }
        String btFWLogLevel = MyApplication.getInstance().getDefaultSharedPreferences()
                .getString(ConnsysLogSettings.KEY_BTFW_LOG_LEVEL, "2");
        return LogHandler.getInstance().startConnsysFWLog(logPath,
                BTFWLogLevel.getBTFWLogLevelByID(btFWLogLevel));
    }

}
