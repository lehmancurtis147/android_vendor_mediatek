package com.mediatek.mtklogger.controller;

import android.util.SparseArray;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.AbnormalEvent;
import com.log.handler.LogHandlerUtils.IAbnormalEventMonitor;
import com.log.handler.LogHandlerUtils.LogType;
import com.mediatek.mtklogger.utils.Utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author MTK81255
 *
 */
public class LogControllerUtils {

    private static final String TAG = Utils.TAG + "/LogControllerUtils";

    public static final SparseArray<LogType> LOG_TYPE_INT_TO_OBJECT = new SparseArray<LogType>();
    static {
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_MOBILE, LogType.MOBILE_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_MODEM, LogType.MODEM_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_NETWORK, LogType.NETWORK_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_MET, LogType.MET_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_GPSHOST, LogType.GPSHOST_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_CONNSYSFW, LogType.CONNSYSFW_LOG);
        LOG_TYPE_INT_TO_OBJECT.put(Utils.LOG_TYPE_BTHOST, LogType.BTHOST_LOG);
    }
    public static final Map<LogType, Integer> LOG_TYPE_OBJECT_TO_INT =
            new HashMap<LogType, Integer>();
    static {
        LOG_TYPE_OBJECT_TO_INT.put(LogType.MOBILE_LOG, Utils.LOG_TYPE_MOBILE);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.MODEM_LOG, Utils.LOG_TYPE_MODEM);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.NETWORK_LOG, Utils.LOG_TYPE_NETWORK);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.MET_LOG, Utils.LOG_TYPE_MET);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.GPSHOST_LOG, Utils.LOG_TYPE_GPSHOST);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.CONNSYSFW_LOG, Utils.LOG_TYPE_CONNSYSFW);
        LOG_TYPE_OBJECT_TO_INT.put(LogType.BTHOST_LOG, Utils.LOG_TYPE_BTHOST);
    }
    public static final Map<LogType, String> LOG_CONFIG_PATH_KEY = new HashMap<LogType, String>();
    static {
        LOG_CONFIG_PATH_KEY.put(LogType.MOBILE_LOG, "com.mediatek.log.mobile.path");
        LOG_CONFIG_PATH_KEY.put(LogType.MODEM_LOG, "com.mediatek.log.modem.path");
        LOG_CONFIG_PATH_KEY.put(LogType.NETWORK_LOG, "com.mediatek.log.network.path");
        LOG_CONFIG_PATH_KEY.put(LogType.MET_LOG, "com.mediatek.log.met.path");
        LOG_CONFIG_PATH_KEY.put(LogType.CONNSYSFW_LOG, "com.mediatek.log.connsysfw.path");
        LOG_CONFIG_PATH_KEY.put(LogType.GPSHOST_LOG, "com.mediatek.log.gpshost.path");
        LOG_CONFIG_PATH_KEY.put(LogType.BTHOST_LOG, "com.mediatek.log.bthost.path");
    }

    /**
     * @param logTypeInt
     *            int
     * @return Set<LogType>
     */
    public static Set<LogType> convertLogTypeToObject(int logTypeInt) {
        Set<LogType> logTypeSet = new HashSet<LogType>();
        for (int logType : Utils.LOG_TYPE_SET) {
            if ((logType & logTypeInt) == 0) {
                continue;
            }
            logTypeSet.add(LOG_TYPE_INT_TO_OBJECT.get(logType));
        }
        return logTypeSet;
    }

    /**
     * @param logTypeSet
     *            Set<LogType>
     * @return int
     */
    public static int convertLogTypeToInt(Set<LogType> logTypeSet) {
        int logTypeInt = 0;
        for (LogType logType : logTypeSet) {
            logTypeInt += LOG_TYPE_OBJECT_TO_INT.get(logType);
        }
        return logTypeInt;
    }

    /**
     *
     */
    public static void registerLogAbnormalEventMonitor() {
        LogHandler.getInstance().registerLogAbnormalEventMonitor(new IAbnormalEventMonitor() {
            @Override
            public void abnormalEvenHappened(LogType logType, AbnormalEvent abnormalEvent) {
            }
        });
    }

    /**
     * @param logTypeInt
     *            int
     * @return AbstractLogController
     */
    public static AbstractLogController getLogControllerInstance(int logTypeInt) {
        return getLogControllerInstance(LOG_TYPE_INT_TO_OBJECT.get(logTypeInt));
    }

    /**
     * @param logType
     *            LogType
     * @return AbstractLogController
     */
    public static AbstractLogController getLogControllerInstance(LogType logType) {
        switch (logType) {
        case MOBILE_LOG:
            return MobileLogController.getInstance();
        case MODEM_LOG:
            return ModemLogController.getInstance();
        case NETWORK_LOG:
            return NetworkLogController.getInstance();
        case CONNSYSFW_LOG:
            return ConnsysFWLogController.getInstance();
        case GPSHOST_LOG:
            return GPSHostLogController.getInstance();
        case BTHOST_LOG:
            return BTHostLogController.getInstance();
        case MET_LOG:
            return METLogController.getInstance();
        default:
            return null;
        }
    }

    /**
     * @return boolean
     */
    public static boolean isConnsysLogRunning() {
        boolean isConnsysLogRunning = false;
        for (int logType : Utils.CONNSYS_LOG_TYPE_SET) {
            isConnsysLogRunning = isTypeLogRunning(logType);
            if (isConnsysLogRunning) {
                break;
            }
        }
        Utils.logi(TAG, "<--isConnsysLogRunning()? " + isConnsysLogRunning);
        return isConnsysLogRunning;
    }

    /**
     * @param logType
     *            int
     * @return boolean
     */
    public static boolean isTypeLogRunning(int logType) {
        boolean isLogRunning = false;
        isLogRunning = LogControllerUtils.getLogControllerInstance(logType).isLogRunning();
        Utils.logv(TAG, "<--isLogRunning()? " + isLogRunning + ", logType = " + logType);
        return isLogRunning;
    }
}
