package com.mediatek.mtklogger.settings;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StatFs;
import android.preference.CheckBoxPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceManager;
import android.util.SparseArray;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import com.mediatek.mtklogger.R;
import com.mediatek.mtklogger.controller.LogControllerUtils;
import com.mediatek.mtklogger.controller.METLogController;
import com.mediatek.mtklogger.controller.ModemLogController;
import com.mediatek.mtklogger.framework.MTKLoggerServiceManager;
import com.mediatek.mtklogger.framework.MTKLoggerServiceManager.ServiceNullException;
import com.mediatek.mtklogger.permission.PermissionUtils;
import com.mediatek.mtklogger.utils.SelfdefinedSwitchPreference;
import com.mediatek.mtklogger.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author MTK81255
 *
 */
public class SettingsActivity extends PreferenceActivity
        implements OnPreferenceChangeListener, ISettingsActivity {

    private static final String TAG = Utils.TAG + "/SettingsActivity";
    public static final String KEY_MB_SWITCH = "mobilelog_switch";
    public static final String KEY_MD_SWITCH = "modemlog_switch";
    public static final String KEY_NT_SWITCH = "networklog_switch";
    public static final String KEY_MT_SWITCH = "metlog_switch";
    public static final String KEY_GPS_SWITCH = "gpslog_switch";
    public static final String KEY_CONNSYS_SWITCH = "connsyslog_switch";
    public static final String KEY_BTHOST_SWITCH = "bthostlog_switch";
    public static final SparseArray<String> KEY_LOG_SWITCH_MAP = new SparseArray<String>();
    static {
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_MOBILE, KEY_MB_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_MODEM, KEY_MD_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_NETWORK, KEY_NT_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_MET, KEY_MT_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_GPSHOST, KEY_GPS_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_CONNSYSFW, KEY_CONNSYS_SWITCH);
        KEY_LOG_SWITCH_MAP.put(Utils.LOG_TYPE_BTHOST, KEY_BTHOST_SWITCH);
    }

    public static final String KEY_ADVANCED_SETTINGS_CATEGORY = "advanced_settings_category";
    public static final String KEY_GENERAL_SETTINGS_CATEGORY = "general_settings_category";
    public static final String KEY_TAGLOG_ENABLE = "taglog_enable";
    public static final String KEY_ALWAYS_TAG_MODEM_LOG_ENABLE = "always_tag_modem_log_enable";
    public static final String KEY_ADVANCED_LOG_STORAGE_LOCATION = "log_storage_location";
    public static final String KEY_MET_LOG_ENABLE = "metlog_enable";

    private SelfdefinedSwitchPreference mMbSwitchPre;
    private SelfdefinedSwitchPreference mMdSwitchPre;
    private SelfdefinedSwitchPreference mNtSwitchPre;
    private SelfdefinedSwitchPreference mMTSwitchPre;
    private SelfdefinedSwitchPreference mConnsysSwitchPre;

    private CheckBoxPreference mTaglogEnable;
    private CheckBoxPreference mAlwaysTagModemLogEnable;
    private ListPreference mLogStorageLocationList;
    private CheckBoxPreference mMetLogEnable;

    private SharedPreferences mDefaultSharedPreferences;
    private SharedPreferences mSharedPreferences;
    private String mFilterFileInfoStr = null;

    private long mSdcardSize = 0;

    private boolean mIsRecording = false;

    private SettingsPreferenceFragement mPrefsFragement;

    private UpdateLogStorageEntriesTask mUpdateLogStorageEntriesTask =
            new UpdateLogStorageEntriesTask();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Utils.logd(TAG, "SettingsActivity onCreate() start");
        super.onCreate(savedInstanceState);
        mPrefsFragement = new SettingsPreferenceFragement(this, R.xml.settings);
        getFragmentManager().beginTransaction().replace(android.R.id.content, mPrefsFragement)
                .commit();
        mFilterFileInfoStr = null;
        new Thread(new Runnable() {
            @Override
            public void run() {
                mFilterFileInfoStr = ModemLogController.getInstance().getFilterFileInformation();
            }
        }).start();
    }

    private ListView mListView = null;

    @Override
    protected void onResume() {
        Utils.logd(TAG, "onResume()");
        if (mListView == null) {
            mListView = mPrefsFragement.getListView();
            mListView.setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position,
                        long id) {
                    Object item = mListView.getItemAtPosition(position);
                    if (mMdSwitchPre.equals(item)) {
                        SelfdefinedSwitchPreference sSwitchPre = (SelfdefinedSwitchPreference) item;
                        Utils.logd(TAG, "onCreateContextMenu sSwitchPre.getTitle() = "
                                + sSwitchPre.getTitle());
                        if (mMdSwitchPre.equals(sSwitchPre)) {
                            Utils.logi(TAG, "mFilterFileInfoStr = " + mFilterFileInfoStr);
                            String[] filterInfos = new String[] { "N/A", "N/A", "N/A" };
                            if (mFilterFileInfoStr == null || mFilterFileInfoStr.isEmpty()) {
                                Utils.logw(TAG, "The format for mFilterFileInfoStr is error!");
                            } else {
                                String[] filterInfoStrs = mFilterFileInfoStr.split(";");
                                int length = filterInfoStrs.length > 3 ? 3 : filterInfoStrs.length;
                                for (int i = 0; i < length; i++) {
                                    filterInfos[i] = filterInfoStrs[i];
                                }
                            }
                            String lineSeparator = System.getProperty("line.separator", "/n");
                            String filePath = getString(R.string.file_info_path) + lineSeparator
                                    + filterInfos[0] + lineSeparator + lineSeparator;
                            String modifiedTime =
                                    getString(R.string.file_info_modified_time) + lineSeparator
                                            + filterInfos[1] + lineSeparator + lineSeparator;
                            String fileSize = getString(R.string.file_info_size) + lineSeparator
                                    + filterInfos[2];
                            showLogSettingsInfoDialog(mMdSwitchPre.getTitle().toString(),
                                    filePath + modifiedTime + fileSize);
                        }
                    }
                    return true;
                }
            });
        }
        updateUI();
        super.onResume();
    }

    private void showLogSettingsInfoDialog(String titile, String inforStr) {
        Utils.logi(TAG,
                "showLogSettingsInfoDialog titile = " + titile + ", inforStr = " + inforStr);
        Builder builder = new AlertDialog.Builder(this).setTitle(titile).setMessage(inforStr)
                .setPositiveButton(android.R.string.yes, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        dialog.show();
    }

    @Override
    public void findViews() {
        mMbSwitchPre = (SelfdefinedSwitchPreference) mPrefsFragement.findPreference(KEY_MB_SWITCH);
        mMdSwitchPre = (SelfdefinedSwitchPreference) mPrefsFragement.findPreference(KEY_MD_SWITCH);
        mNtSwitchPre = (SelfdefinedSwitchPreference) mPrefsFragement.findPreference(KEY_NT_SWITCH);
        mMTSwitchPre = (SelfdefinedSwitchPreference) mPrefsFragement.findPreference(KEY_MT_SWITCH);
        mConnsysSwitchPre =
                (SelfdefinedSwitchPreference) mPrefsFragement.findPreference(KEY_CONNSYS_SWITCH);

        mTaglogEnable = (CheckBoxPreference) mPrefsFragement.findPreference(KEY_TAGLOG_ENABLE);
        mAlwaysTagModemLogEnable = (CheckBoxPreference) mPrefsFragement
                .findPreference(KEY_ALWAYS_TAG_MODEM_LOG_ENABLE);

        mLogStorageLocationList =
                (ListPreference) mPrefsFragement.findPreference(KEY_ADVANCED_LOG_STORAGE_LOCATION);
        mMetLogEnable = (CheckBoxPreference) mPrefsFragement.findPreference(KEY_MET_LOG_ENABLE);
    }

    @Override
    public void initViews() {
        mDefaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mSharedPreferences = getSharedPreferences(Utils.CONFIG_FILE_NAME, Context.MODE_PRIVATE);

        if (!Utils.BUILD_TYPE.equals("eng")) {
            Utils.logd(TAG, "initViews() BuildType is not eng.");
            mTaglogEnable.setChecked(mSharedPreferences.getBoolean(Utils.TAG_LOG_ENABLE, false));

        } else {
            mTaglogEnable.setChecked(mSharedPreferences.getBoolean(Utils.TAG_LOG_ENABLE, true));
        }
        mAlwaysTagModemLogEnable.setEnabled(mTaglogEnable.isChecked());

        // Hide AlwaysTagModemLog settings
        PreferenceCategory advancePreCategory =
                (PreferenceCategory) mPrefsFragement.findPreference(KEY_ADVANCED_SETTINGS_CATEGORY);
        advancePreCategory.removePreference(mAlwaysTagModemLogEnable);
        mAlwaysTagModemLogEnable = null;

        setDefaultEntry();
        setSdcardSize();
        updateUI();
        mUpdateLogStorageEntriesTask = new UpdateLogStorageEntriesTask();
        mUpdateLogStorageEntriesTask.execute();
    }

    private void setDefaultEntry() {
        List<CharSequence> entriesList = new ArrayList<CharSequence>();
        List<CharSequence> entryValuesList = new ArrayList<CharSequence>();
        entriesList.add(getString(Utils.LOG_PHONE_STORAGE));
        entryValuesList.add(Utils.LOG_PHONE_STORAGE_KEY);
        mLogStorageLocationList
                .setEntries(entriesList.toArray(new CharSequence[entriesList.size()]));
        mLogStorageLocationList
                .setEntryValues(entryValuesList.toArray(new CharSequence[entryValuesList.size()]));
        mLogStorageLocationList.setValue(Utils.LOG_PHONE_STORAGE_KEY);
        mLogStorageLocationList.setSummary(Utils.LOG_PHONE_STORAGE);
    }

    @Override
    public void setListeners() {
        mMbSwitchPre.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Utils.logd(TAG, "mMbSwitchPre onPreferenceClick");
                Intent intent = new Intent(SettingsActivity.this, MobileLogSettings.class);
                intent.putExtra(Utils.SETTINGS_IS_SWITCH_CHECKED, mMbSwitchPre.isChecked());
                setSdcardSize();
                intent.putExtra(Utils.SDCARD_SIZE, mSdcardSize);
                startActivity(intent);
                return true;
            }
        });

        mMbSwitchPre.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (Boolean) newValue;
                Utils.logd(TAG, "mMbSwitchPre onPreferenceChange = " + checked);
                // Force new enable status persist manually, to cover plug out
                // battery event
                mDefaultSharedPreferences.edit().putBoolean(KEY_MB_SWITCH + "_bak", checked)
                        .apply();
                return true;
            }
        });

        mMdSwitchPre.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Utils.logd(TAG, "mMdSwitchPre onPreferenceClick");
                final Intent intent = new Intent(SettingsActivity.this, ModemLogSettings.class);
                intent.putExtra(Utils.SETTINGS_IS_SWITCH_CHECKED, mMdSwitchPre.isChecked());
                setSdcardSize();
                intent.putExtra(Utils.SDCARD_SIZE, mSdcardSize);
                startActivity(intent);
                return true;
            }
        });

        mMdSwitchPre.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (Boolean) newValue;
                Utils.logd(TAG, "mMdSwitchPre onPreferenceChange = " + checked);
                // Force new enable status persist manually, to cover plug out
                // battery event
                mDefaultSharedPreferences.edit().putBoolean(KEY_MD_SWITCH + "_bak", checked)
                        .apply();
                return true;
            }
        });

        mNtSwitchPre.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Utils.logd(TAG, "mNtSwitchPre onPreferenceClick");
                Intent intent = new Intent(SettingsActivity.this, NetworkLogSettings.class);
                intent.putExtra(Utils.SETTINGS_IS_SWITCH_CHECKED, mNtSwitchPre.isChecked());
                setSdcardSize();
                intent.putExtra(Utils.SDCARD_SIZE, mSdcardSize);
                startActivity(intent);
                return true;
            }
        });

        mNtSwitchPre.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (Boolean) newValue;
                Utils.logd(TAG, "mNtSwitchPre onPreferenceChange = " + checked);
                // Force new enable status persist manually, to cover plug out
                // battery event
                mDefaultSharedPreferences.edit().putBoolean(KEY_NT_SWITCH + "_bak", checked)
                        .apply();
                return true;
            }
        });

        mMTSwitchPre.setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference arg0) {
                Utils.logd(TAG, "mMTSwitchPre onPreferenceClick");
                Intent intent = new Intent(SettingsActivity.this, MetLogSettings.class);
                intent.putExtra(Utils.SETTINGS_IS_SWITCH_CHECKED, mMTSwitchPre.isChecked());
                setSdcardSize();
                intent.putExtra(Utils.SDCARD_SIZE, mSdcardSize);
                startActivity(intent);
                return true;
            }

        });

        mMTSwitchPre.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                boolean checked = (Boolean) newValue;
                Utils.logd(TAG, "mMTSwitchPre onPreferenceChange = " + checked);
                // Force new enable status persist manually, to cover plug out
                // battery event
                mDefaultSharedPreferences.edit().putBoolean(KEY_MT_SWITCH + "_bak", checked)
                        .apply();
                return true;
            }
        });

        mConnsysSwitchPre.setOnPreferenceClickListener(new OnPreferenceClickListener() {

            @Override
            public boolean onPreferenceClick(Preference arg0) {
                Utils.logd(TAG, "mConnsysSwitchPre onPreferenceClick");
                Intent intent = new Intent(SettingsActivity.this, ConnsysLogSettings.class);
                intent.putExtra(Utils.SETTINGS_IS_SWITCH_CHECKED, mConnsysSwitchPre.isChecked());
                setSdcardSize();
                intent.putExtra(Utils.SDCARD_SIZE, mSdcardSize);
                startActivity(intent);
                return true;
            }

        });

        mConnsysSwitchPre.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                try {
                    MTKLoggerServiceManager.getInstance().getService().setBTFirmwareLogLevel();
                } catch (ServiceNullException e) {
                    Utils.logw(TAG, "Service is null!");
                }
                return true;
            }
        });

        mTaglogEnable.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference arg0) {
                mSharedPreferences.edit()
                        .putBoolean(Utils.TAG_LOG_ENABLE, mTaglogEnable.isChecked())
                        .remove(Utils.KEY_MODEM_EXCEPTION_PATH)// When enable
                                                               // taglog, clear
                                                               // old data
                        .apply();
                if (mAlwaysTagModemLogEnable != null) {
                    mAlwaysTagModemLogEnable.setEnabled(mTaglogEnable.isChecked());
                }
                if (mTaglogEnable.isChecked()) {
                    Utils.logi(TAG, "Request storage permission for taglog enable.");
                    PermissionUtils.requestStoragePermissions();
                }
                return true;
            }
        });
        if (mMetLogEnable != null) {
            mMetLogEnable.setOnPreferenceClickListener(new OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference arg0) {
                    boolean isMETLogFeatureSupport =
                            METLogController.getInstance().isMETLogFeatureSupport();
                    if (isMETLogFeatureSupport) {
                        mSharedPreferences.edit()
                                .putBoolean(Utils.MET_LOG_ENABLE, mMetLogEnable.isChecked())
                                .apply();
                        updateUI();
                    } else {
                        showMETLogNotSupportWarningDialog();
                    }
                    return true;
                }
            });
        }

        mLogStorageLocationList.setOnPreferenceChangeListener(this);
    }

    private void showMETLogNotSupportWarningDialog() {
        Utils.logi(TAG, "Show METLog Not Support Warning Dialog.");
        // String message = getString(reason == 0 ? R.string.not_support_metlog_dialog_message
        // : R.string.not_support_metlog_dialog_message_timeout);
        String message = getString(R.string.not_support_metlog_dialog_message);
        Builder builder = new AlertDialog.Builder(this)
                .setTitle(R.string.not_support_metlog_dialog_title).setMessage(message)
                .setPositiveButton(android.R.string.yes, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mMetLogEnable.setChecked(false);
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        dialog.show();
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        Utils.logi(TAG,
                "Preference Change Key : " + preference.getKey() + " newValue : " + newValue);

        if (preference.getKey().equals(KEY_ADVANCED_LOG_STORAGE_LOCATION)) {
            String oldValue = Utils.LOG_PHONE_STORAGE_KEY;
            if (Utils.LOG_PATH_TYPE_EXTERNAL_SD.equals(Utils.getModemLogPathType())
                    && Utils.LOG_PATH_TYPE_INTERNAL_SD.equals(Utils.getLogPathType())) {
                oldValue = Utils.LOG_MODEM_TO_SD_CARD_KEY;
            } else {
                oldValue = Utils.LOG_PATH_TYPE_EXTERNAL_SD.equals(Utils.getLogPathType())
                        ? Utils.LOG_SD_CARD_KEY : Utils.LOG_PHONE_STORAGE_KEY;
            }
            if (oldValue.equals(newValue.toString())) {
                return true;
            }
            if (!Utils.LOG_PHONE_STORAGE_KEY.equals(newValue.toString())) {
                showLogSettingsInfoDialog(getString(R.string.sdcard_need_format_dialog_title),
                        getString(R.string.sdcard_need_format_dialog_message));
            }
            if (Utils.LOG_MODEM_TO_SD_CARD_KEY.equals(newValue)) {
                preference.setSummary(Utils.LOG_MODEM_TO_SD_CARD);
                Utils.setLogPathType(Utils.LOG_PATH_TYPE_INTERNAL_SD);
                Utils.setModemLogPathType(Utils.LOG_PATH_TYPE_EXTERNAL_SD);
            } else {
                preference.setSummary(Utils.LOG_SD_CARD_KEY.equals(newValue.toString())
                        ? Utils.LOG_SD_CARD : Utils.LOG_PHONE_STORAGE);
                String logPathType = Utils.LOG_SD_CARD_KEY.equals(newValue.toString())
                        ? Utils.LOG_PATH_TYPE_EXTERNAL_SD : Utils.LOG_PATH_TYPE_INTERNAL_SD;
                Utils.setLogPathType(logPathType);
                Utils.setModemLogPathType(logPathType);
            }
            setSdcardSize();
        }
        return true;
    }

    private void setSdcardSize() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    StatFs statFs = new StatFs(Utils.getCurrentLogPath());
                    long blockSize = statFs.getBlockSizeLong() / 1024;
                    mSdcardSize = statFs.getBlockCountLong() * blockSize / 1024;
                } catch (IllegalArgumentException e) {
                    Utils.loge(TAG,
                            "setSdcardSize() : StatFs error, maybe currentLogPath is invalid");
                    mSdcardSize = 0;
                }
            }
        }).run();
    }

    /**
     * @author MTK81255
     *
     */
    private class UpdateLogStorageEntriesTask extends AsyncTask<Void, Void, Void> {

        List<CharSequence> mEntriesList = new ArrayList<CharSequence>();
        List<CharSequence> mEntryValuesList = new ArrayList<CharSequence>();

        @Override
        protected Void doInBackground(Void... params) {
            // First check whether storage is mounted
            String internalPath = Utils.getLogPath(Utils.LOG_PATH_TYPE_INTERNAL_SD);
            if (internalPath != null && !internalPath.isEmpty()) {
                mEntriesList.add(getString(Utils.LOG_PHONE_STORAGE));
                mEntryValuesList.add(Utils.LOG_PHONE_STORAGE_KEY);
            }

            String externalPath = Utils.getLogPath(Utils.LOG_PATH_TYPE_EXTERNAL_SD);
            if (externalPath != null && !externalPath.isEmpty()) {
                mEntriesList.add(getString(Utils.LOG_SD_CARD));
                mEntryValuesList.add(Utils.LOG_SD_CARD_KEY);
            }
            if (internalPath != null && !internalPath.isEmpty() && externalPath != null
                    && !externalPath.isEmpty()) {
                mEntriesList.add(getString(Utils.LOG_MODEM_TO_SD_CARD));
                mEntryValuesList.add(Utils.LOG_MODEM_TO_SD_CARD_KEY);
            }
            return null;
        }

        // This is called when doInBackground() is finished
        @Override
        protected void onPostExecute(Void result) {
            setLogStorageEntries(mEntriesList, mEntryValuesList);
        }
    }

    /**
     * @param entriesList
     *            List<CharSequence>
     * @param entryValuesList
     *            List<CharSequence>
     */
    private void setLogStorageEntries(List<CharSequence> entriesList,
            List<CharSequence> entryValuesList) {
        if (entriesList.size() == 0) {
            mLogStorageLocationList.setEnabled(false);
            return;
        }
        mLogStorageLocationList.setEnabled(!mIsRecording);
        mLogStorageLocationList.setEntries(null);
        mLogStorageLocationList.setEntryValues(null);
        mLogStorageLocationList
                .setEntries(entriesList.toArray(new CharSequence[entriesList.size()]));
        mLogStorageLocationList
                .setEntryValues(entryValuesList.toArray(new CharSequence[entryValuesList.size()]));
        if (Utils.LOG_PATH_TYPE_INTERNAL_SD.equals(Utils.getLogPathType())
                && Utils.LOG_PATH_TYPE_EXTERNAL_SD.equals(Utils.getModemLogPathType())) {
            mLogStorageLocationList.setValue(Utils.LOG_MODEM_TO_SD_CARD_KEY);
            mLogStorageLocationList.setSummary(Utils.LOG_MODEM_TO_SD_CARD);
            return;
        }
        String logPathTypeKey = Utils.LOG_PATH_TYPE_EXTERNAL_SD.equals(Utils.getLogPathType())
                ? Utils.LOG_SD_CARD_KEY : Utils.LOG_PHONE_STORAGE_KEY;
        mLogStorageLocationList.setValue(logPathTypeKey);
        mLogStorageLocationList.setSummary(Utils.LOG_SD_CARD_KEY.equals(logPathTypeKey)
                ? Utils.LOG_SD_CARD : Utils.LOG_PHONE_STORAGE);
    }

    private void updateUI() {
        Utils.logi(TAG, "updateUI()");

        for (Integer logType : Utils.LOG_TYPE_SET) {
            boolean isRecording;

            isRecording = LogControllerUtils.isTypeLogRunning(logType);

            if (isRecording) {
                mIsRecording = true;
                break;
            }
        }
        mMbSwitchPre.setEnabled(!mIsRecording);
        mMdSwitchPre.setEnabled(!mIsRecording);
        mNtSwitchPre.setEnabled(!mIsRecording);
        mMTSwitchPre.setEnabled(!mIsRecording);
        mConnsysSwitchPre.setEnabled(!mIsRecording);

        CharSequence[] logStorageEntries = mLogStorageLocationList.getEntries();
        if ((logStorageEntries == null) || (logStorageEntries.length == 0)) {
            Utils.logw(TAG, "Log storage entry is null or empty, disable storage set item");
            mLogStorageLocationList.setEnabled(false);
        } else {
            mLogStorageLocationList.setEnabled(!mIsRecording);
        }

        mMbSwitchPre.setChecked(mDefaultSharedPreferences.getBoolean(KEY_MB_SWITCH, true));
        mMdSwitchPre.setChecked(mDefaultSharedPreferences.getBoolean(KEY_MD_SWITCH, true));
        mNtSwitchPre.setChecked(mDefaultSharedPreferences.getBoolean(KEY_NT_SWITCH, true));

        mMTSwitchPre.setChecked(mDefaultSharedPreferences.getBoolean(KEY_MT_SWITCH, true));
        mConnsysSwitchPre
                .setChecked(mDefaultSharedPreferences.getBoolean(KEY_CONNSYS_SWITCH, true));

        PreferenceCategory advancePreCategory =
                (PreferenceCategory) mPrefsFragement.findPreference(KEY_GENERAL_SETTINGS_CATEGORY);

        if (mSharedPreferences.getBoolean(Utils.MET_LOG_ENABLE, false) != true) {
            advancePreCategory.removePreference(mMTSwitchPre);

        } else {
            advancePreCategory.addPreference(mMTSwitchPre);
        }
        if (mMetLogEnable != null) {
            mMetLogEnable.setEnabled(!mIsRecording);
            mMetLogEnable.setChecked(mSharedPreferences.getBoolean(Utils.MET_LOG_ENABLE, false));
        }
    }

}
