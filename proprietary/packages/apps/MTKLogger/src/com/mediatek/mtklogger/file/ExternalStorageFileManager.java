package com.mediatek.mtklogger.file;

import android.content.ContentResolver;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.DocumentsContract.Document;

import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * @author MTK81255
 *
 */
public class ExternalStorageFileManager {
    private static final String TAG = Utils.TAG + "/ExternalStorageFileManager";

    public static final String AUTHORITY_EXTERNAL_STORAGE = "com.android.externalstorage.documents";
    public static final String FILE_PATH_INDEX = "storage/";

    public static ContentResolver sContentResolver =
            MyApplication.getInstance().getApplicationContext().getContentResolver();

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean delete(File file) {
        Utils.logi(TAG, "deleteFile(), file.getAbsolutePath() = " + file.getAbsolutePath());
        Uri uri = getUri(file);
        if (uri == null) {
            return false;
        }
        Utils.logi(TAG, "Uri: " + uri.toString());
        try {
            return DocumentsContract.deleteDocument(sContentResolver, uri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SecurityException se) {
            se.printStackTrace();
        }
        return false;
    }

    /**
     * @param file
     *            File
     * @return Uri
     */
    public static Uri getUri(File file) {
        // source : /storage/3558-110E/mtklog/netlog/NTLog_2018_0620_142338
        String filePath = file.getAbsolutePath();
        int externalStroageIndex = filePath.indexOf(FILE_PATH_INDEX);
        if (externalStroageIndex == -1) {
            return null;
        }
        String docmentId =
                filePath.substring(externalStroageIndex + FILE_PATH_INDEX.length());
        docmentId = docmentId.replaceFirst("/", ":");
        Utils.logi(TAG, "getUri(), docmentId: " + docmentId);
        return DocumentsContract.buildDocumentUri(AUTHORITY_EXTERNAL_STORAGE, docmentId);
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean createNewFile(File file) {
        Uri parentDocumentUri = getUri(file.getParentFile());
        Utils.logi(TAG, "createNewFile(), parentDocumentUri: " + parentDocumentUri.toString()
                + ", file.getName() = "
                + file.getName());
        try {
            Uri fileUri = DocumentsContract
                    .createDocument(sContentResolver, parentDocumentUri, "", file.getName());
            return fileUri != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean mkdir(File file) {
        try {
            Uri parentDocumentUri = getUri(file.getParentFile());
            Utils.logi(TAG, "mkdir(), parentDocumentUri: " + parentDocumentUri.toString()
                    + ", file.getName() = " + file.getName());
            Uri fileUri = DocumentsContract.createDocument(sContentResolver, parentDocumentUri,
                    Document.MIME_TYPE_DIR, file.getName());
            return fileUri != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param file
     *            File
     * @return boolean
     */
    public static boolean mkdirs(File file) {
        File parentFile = file.getParentFile();
        if (parentFile != null && !parentFile.exists()) {
            mkdirs(parentFile);
        }
        return mkdir(file);
    }

    /**
     * @param sourceFile
     *            File
     * @param targetFile
     *            File
     * @return boolean
     */
    public static boolean move(File sourceFile, File targetFile) {
        Uri sourceDocumentUri = getUri(sourceFile);
        Uri sourceParentDocumentUri = getUri(sourceFile.getParentFile());
        if (!targetFile.getParentFile().exists()) {
            mkdirs(targetFile.getParentFile());
        }
        Uri targetParentDocumentUri = getUri(targetFile.getParentFile());
        Utils.logi(TAG,
                "move(), sourceDocumentUri = " + sourceDocumentUri.toString()
                        + ", sourceParentDocumentUri = " + sourceParentDocumentUri.toString()
                        + ", targetParentDocumentUri = " + targetParentDocumentUri.toString());
        try {
            Uri fileUri = DocumentsContract.moveDocument(sContentResolver, sourceDocumentUri,
                    sourceParentDocumentUri, targetParentDocumentUri);
            return fileUri != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param sourceFile
     *            File
     * @param targetFile
     *            File
     * @return boolean
     */
    public static boolean rename(File sourceFile, File targetFile) {
        Utils.logi(TAG, "rename() sourceFile = " + sourceFile.getAbsolutePath()
                + ", targetFile = " + targetFile.getAbsolutePath());
        Uri sourceDocumentUri = getUri(sourceFile);
        try {
            Uri fileUri = DocumentsContract.renameDocument(sContentResolver, sourceDocumentUri,
                    targetFile.getName());
            return fileUri != null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * @param file
     *            File
     * @return FileOutputStream
     */
    public static FileOutputStream getFileOutputStream(File file) {
        Utils.logi(TAG, "getFileOutputStream(), file = " + file.getAbsolutePath());
        if (!file.exists()) {
            if (!createNewFile(file)) {
                Utils.logw(TAG, "getFileOutputStream createFile failed, just return null!");
                return null;
            }
        }
        try {
            ParcelFileDescriptor pfd = MyApplication.getInstance().getContentResolver()
                    .openFileDescriptor(getUri(file), "w");
            return new FileOutputStream(pfd.getFileDescriptor());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}
