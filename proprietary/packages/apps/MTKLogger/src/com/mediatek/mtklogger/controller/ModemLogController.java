package com.mediatek.mtklogger.controller;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.view.WindowManager;

import com.log.handler.LogHandler;
import com.log.handler.LogHandlerUtils.CCBBufferGearID;
import com.log.handler.LogHandlerUtils.IModemEEMonitor;
import com.log.handler.LogHandlerUtils.LogType;
import com.log.handler.LogHandlerUtils.ModemLogMode;
import com.log.handler.LogHandlerUtils.ModemLogStatus;
import com.mediatek.mtklogger.MyApplication;
import com.mediatek.mtklogger.R;
import com.mediatek.mtklogger.settings.ModemLogSettings;
import com.mediatek.mtklogger.utils.Utils;

import java.io.File;

/**
 * @author MTK81255
 *
 */
public class ModemLogController extends AbstractLogController {
    private static final String TAG = Utils.TAG + "/ModemLogController";

    private SharedPreferences mDefaultSharedPreferences =
            MyApplication.getInstance().getDefaultSharedPreferences();
    private static final String ADB_COMMAND_FORCE_MODEM_ASSERT = "force_modem_assert";
    private static final String ADB_COMMAND_MODEM_AUTO_RESET = "modem_auto_reset_";
    private static final String ADB_COMMAND_SET_FILE_SIZE = "set_file_size_";
    private Handler mHandler;
    private static final int MSG_MODEM_EE_HAPPENED = 0;
    private boolean mIsModemResetDialogShowing = false;

    public static ModemLogController sInstance = new ModemLogController(LogType.MODEM_LOG);

    public static ModemLogController getInstance() {
        return sInstance;
    }

    protected ModemLogController(LogType logType) {
        super(logType);
        HandlerThread handlerThread = new HandlerThread("modemlogHandler");
        handlerThread.start();
        mHandler = new ModemLogHandler(handlerThread.getLooper());
        LogHandler.getInstance().registerModemEEMonitor(mModemEEMonitor);
    }

    /**
     * @author MTK81255
     *
     */
    class ModemLogHandler extends Handler {
        ModemLogHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MSG_MODEM_EE_HAPPENED:
                if (msg.obj != null && msg.obj instanceof String) {
                    String modemEEPath = (String) msg.obj;
                    showMemoryDumpDoneDialog(modemEEPath);
                } else {
                    Utils.logw(TAG, "The modemEEPath is null or formart is error!");
                }
                break;
            default:
                Utils.logw(TAG, "The msg.what = " + msg.what + " is not support!");
            }
        }
    }

    private IModemEEMonitor mModemEEMonitor = new IModemEEMonitor() {
        @Override
        public void modemEEHappened(String modemEEPath) {
            Message msg = mHandler.obtainMessage(MSG_MODEM_EE_HAPPENED);
            msg.obj = modemEEPath;
            msg.sendToTarget();
        }
    };

    private void showMemoryDumpDoneDialog(String modemEEPath) {
        Utils.logi(TAG, "-->showMemoryDumpDone(), modemEEPath = " + modemEEPath
                + ", isModemResetDialogShowing=" + mIsModemResetDialogShowing);
        if (mDefaultSharedPreferences.getBoolean(ModemLogSettings.KEY_MD_AUTORESET, false)) {
            Utils.logd(TAG, "Auto reset modem, does not need showMemoryDumpDoneDialog!");
            LogHandler.getInstance().resetModem();
            mIsModemResetDialogShowing = false;
            return;
        }
        if (mIsModemResetDialogShowing) {
            Utils.logd(TAG, "Modem reset dialog is already showing, just return");
            return;
        }
        Utils.logi(TAG, "Show memory dump done dialog.");
        Context context = MyApplication.getInstance().getApplicationContext();
        String message = context.getText(R.string.memorydump_done).toString() + modemEEPath;
        Builder builder = new AlertDialog.Builder(context)
                .setTitle(context.getText(R.string.dump_warning).toString()).setMessage(message)
                .setPositiveButton(android.R.string.yes, new OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Utils.logd(TAG, "Click OK in memory dump done dialog");
                        LogHandler.getInstance().resetModem();
                        Utils.logd(TAG, "After confirm, no need to show reset dialog next time");
                        mIsModemResetDialogShowing = false;
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY);
        dialog.setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Utils.logd(TAG, "Press cancel in memory dump done dialog");
                LogHandler.getInstance().resetModem();
                Utils.logd(TAG, "After cancel, no need to show reset dialog next time");
                mIsModemResetDialogShowing = false;
            }
        });
        mIsModemResetDialogShowing = true;
        dialog.show();
    }

    /**
     * @param logPath
     *            String
     * @return boolean
     */
    public boolean reconnectToModemLog(String logPath) {
        return startLog(logPath);
    }

    @Override
    public boolean startLog(String logPath) {
        String configPath = getConfigPath();
        if (configPath != null && !configPath.isEmpty()) {
            logPath = configPath;
        }
        return LogHandler.getInstance().startModemLog(logPath,
                ModemLogMode.getModemLogModeById(getCurrentMode()));
    }

    private String getCurrentMode() {
        return mDefaultSharedPreferences.getString(Utils.KEY_MD_MODE_1, Utils.MODEM_MODE_SD);
    }

    private boolean setCurrentMode(String newMode, String modemType) {
        Utils.logi(TAG, "-->setCurrentMode(), newMode=" + newMode + "; modemType=" + modemType);
        if (Utils.MODEM_MODE_IDLE.equals(newMode) || Utils.MODEM_MODE_USB.equals(newMode)
                || Utils.MODEM_MODE_SD.equals(newMode) || Utils.MODEM_MODE_PLS.equals(newMode)) {
            if (mDefaultSharedPreferences != null) {
                Utils.logv(TAG, "Persist new modem log mode");
                if (Utils.sAvailableModemList.size() == 1 || modemType.equals("1")) {
                    mDefaultSharedPreferences.edit().putString(Utils.KEY_MD_MODE_1, newMode)
                            .apply();
                } else {
                    mDefaultSharedPreferences.edit().putString(Utils.KEY_MD_MODE_2, newMode)
                            .apply();
                }
                return true;
            } else {
                Utils.loge(TAG, "mDefaultSharedPreferences is null");
            }
        } else {
            Utils.logw(TAG, "Unsupported log mode");
        }
        return false;
    }

    @Override
    public boolean dealWithADBCommand(String cmd) {
        if (cmd.startsWith(ADB_COMMAND_FORCE_MODEM_ASSERT)) {
            return LogHandler.getInstance().forceModemAssert();
        } else if (cmd.startsWith(Utils.ADB_COMMAND_SWITCH_MODEM_LOG_MODE)) {
            String[] tmpArray = cmd.split(",");
            if (tmpArray.length == 2) {
                String newModeStr = "";
                boolean setModeSuccess = false;
                if (tmpArray[0].startsWith(Utils.ADB_COMMAND_SWITCH_MODEM_LOG_MODE + "_")) {
                    newModeStr = tmpArray[0]
                            .substring(Utils.ADB_COMMAND_SWITCH_MODEM_LOG_MODE.length() + 1);
                    setModeSuccess = setCurrentMode(newModeStr, tmpArray[1]);
                } else {
                    newModeStr = tmpArray[1];
                    setModeSuccess = setCurrentMode(newModeStr, "1");
                    if (Utils.sAvailableModemList.size() == 2) {
                        setModeSuccess = setCurrentMode(newModeStr, "3");
                    }
                }
                if (setModeSuccess && getAutoStartValue()) {
                    setBootupLogSaved(true);
                }
                return true;
            } else {
                Utils.loge(TAG, "Invalid configuration from adb command");
                return true;
            }
        } else if (cmd.startsWith(Utils.ADB_COMMAND_SET_MODEM_LOG_SIZE)) {
            String[] tmpArray = cmd.split(",");
            if (tmpArray.length == 2) {
                String newlogSize =
                        tmpArray[0].substring(Utils.ADB_COMMAND_SET_MODEM_LOG_SIZE.length() + 1);
                int logSize = 600;
                try {
                    logSize = Integer.parseInt(newlogSize);
                } catch (NumberFormatException nfe) {
                    Utils.logw(TAG,
                            "The format for newlogSize is error! newlogSize = " + newlogSize);
                    logSize = 600;
                }
                return setLogRecycleSize(logSize);
            } else {
                Utils.loge(TAG, "Invalid mdlog size configuration from adb command");
                return true;
            }
        } else if (cmd.startsWith(ADB_COMMAND_MODEM_AUTO_RESET)) {
            String newValue = cmd.substring(cmd.length() - 1);
            if (newValue.equals("0") || newValue.equals("1")) {
                mDefaultSharedPreferences.edit()
                        .putBoolean(ModemLogSettings.KEY_MD_AUTORESET, newValue.equals("1"))
                        .apply();
            } else {
                Utils.logw(TAG, "Unsupported adb command modem_auto_reset value");
            }
            return true;
        } else if (cmd.startsWith(ADB_COMMAND_SET_FILE_SIZE)) {
            // adb shell am broadcast -a com.mediatek.mtklogger.ADB_CMD -e cmd_name
            // set_file_size_200 --ei cmd_target 2
            String newLogFileSizeStr = cmd.substring(ADB_COMMAND_SET_FILE_SIZE.length());
            Utils.logi(TAG, "set_file_size_ newLogFileSizeStr = " + newLogFileSizeStr);
            int logFileSize = 200;
            try {
                logFileSize = Integer.parseInt(newLogFileSizeStr);
            } catch (NumberFormatException nfe) {
                logFileSize = 200;
            }
            if (logFileSize < 50) {
                Utils.logw(TAG, "The min size for modem log file is 50M.");
                logFileSize = 50;
            }
            if (logFileSize > 200) {
                Utils.logw(TAG, "The max size for modem log file is 200M.");
                logFileSize = 200;
            }
            return setModemLogFileSize(logFileSize);
        } else {
            Utils.logw(TAG, "not official adb command:" + cmd);
            return LogHandler.getInstance().sendCommandToModemLog(cmd);
        }
    }

    private boolean getAutoStartValue() {
        boolean defaultValue = Utils.DEFAULT_CONFIG_LOG_AUTO_START_MAP.get(Utils.LOG_TYPE_MODEM);
        if (mDefaultSharedPreferences != null) {
            defaultValue = mDefaultSharedPreferences.getBoolean(Utils.KEY_START_AUTOMATIC_MODEM,
                    defaultValue);
        }
        Utils.logv(TAG, " getAutoStartValue(), value=" + defaultValue);
        return defaultValue;
    }

    @Override
    public boolean setBootupLogSaved(boolean enable) {
        return LogHandler.getInstance().setBootupLogSaved(enable,
                ModemLogMode.getModemLogModeById(getCurrentMode()));
    }

    @Override
    public String getRunningLogPath() {
        Utils.logd(TAG, "-->getRunningLogPath(), mLogType = " + mLogType);
        if (!isLogRunning()) {
            return null;
        }
        if (Utils.sAvailableModemList.size() == 0) {
            return super.getRunningLogPath();
        }
        String runningLogPath = null;
        for (int modemIndex : Utils.sAvailableModemList) {
            if (modemIndex == (Utils.MODEM_LOG_K2_INDEX + Utils.C2KLOGGER_INDEX)
                    && Utils.isDenaliMd3Solution()) {
                String c2kModemPath = MyApplication.getInstance().getSharedPreferences()
                        .getString(Utils.KEY_C2K_MODEM_LOGGING_PATH, "");
                Utils.logd(TAG, "c2kModemExceptionPath = " + c2kModemPath);
                File c2kModemFile = new File(c2kModemPath);
                if (null != c2kModemFile && c2kModemFile.exists()) {
                    if (runningLogPath != null) {
                        runningLogPath += ";" + c2kModemFile.getAbsolutePath();
                    } else {
                        runningLogPath = c2kModemFile.getAbsolutePath();
                    }
                }
                continue;
            }
            String modemLogFolder =
                    Utils.MODEM_INDEX_FOLDER_MAP.get(modemIndex + Utils.MODEM_LOG_K2_INDEX);
            String logPath = Utils.getCurrentLogPath() + Utils.MTKLOG_PATH + modemLogFolder;

            File fileTree = new File(logPath + File.separator + Utils.LOG_TREE_FILE);
            File logFile = Utils.getLogFolderFromFileTree(fileTree, false);
            if (null != logFile && logFile.exists()) {
                if (runningLogPath != null) {
                    runningLogPath += ";" + logFile.getAbsolutePath();
                } else {
                    runningLogPath = logFile.getAbsolutePath();
                }
            }
        }
        Utils.logi(TAG, "<--getRunningLogPath(), runningLogPath = " + runningLogPath);
        return runningLogPath;
    }

    /**
     * Return the current running stage which may affect UI display behaviors, for example, if modem
     * log is dumping, the whole UI should be disabled. By default, detail log instance will not
     * affect the global UI
     *
     * @return ModemLogStatus
     */
    public ModemLogStatus getLogStatus() {
        return LogHandler.getInstance().getModemLogStatus();
    }

    /**
     * @return boolean
     */
    public boolean notifyUSBModeChanged() {
        return LogHandler.getInstance().notifyUSBModeChanged();
    }

    /**
     * @param enable
     *            boolean
     * @return boolean
     */
    public boolean setSaveGPSLocationToModemLog(boolean enable) {
        return LogHandler.getInstance().setSaveGPSLocationToModemLog(enable);
    }

    public boolean isSaveGPSLocationFeatureSupport() {
        return LogHandler.getInstance().isSaveGPSLocationFeatureSupport();
    }

    public boolean isCCBBufferFeatureSupport() {
        return LogHandler.getInstance().isCCBBufferFeatureSupport();
    }

    /**
     * @param size
     *            int
     * @return boolean
     */
    public boolean setModemLogFileSize(int size) {
        return LogHandler.getInstance().setModemLogFileSize(size);
    }

    /**
     * @return String
     */
    public String triggerPLSModeFlush() {
        return LogHandler.getInstance().triggerModemLogPLSModeFlush();
    }

    public String getFilterFileInformation() {
        return LogHandler.getInstance().getModemLogFilterFileInformation();
    }

    /**
     * @param id
     *            CCBBufferGearID
     * @return boolean
     */
    public boolean setCCBBufferGearID(CCBBufferGearID id) {
        return LogHandler.getInstance().setCCBBufferGearID(id);
    }

    /**
     * @param name
     *            String
     * @return boolean
     */
    public boolean setCCBBufferGearID(String name) {
        return LogHandler.getInstance()
                .setCCBBufferGearID(CCBBufferGearID.getCCBBufferGearIDByName(name));
    }

}
