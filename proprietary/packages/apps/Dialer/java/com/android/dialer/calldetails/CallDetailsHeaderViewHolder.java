/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.dialer.calldetails;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.telecom.PhoneAccount;
import android.text.BidiFormatter;
import android.text.TextDirectionHeuristics;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.util.Log;
import com.android.dialer.calldetails.CallDetailsActivity.AssistedDialingNumberParseWorker;
import com.android.dialer.calldetails.CallDetailsEntries.CallDetailsEntry;
import com.android.dialer.calllogutils.CallbackActionHelper.CallbackAction;
import com.android.dialer.common.Assert;
import com.android.dialer.common.LogUtil;
import com.android.dialer.common.concurrent.DialerExecutor.FailureListener;
import com.android.dialer.common.concurrent.DialerExecutor.SuccessListener;
import com.android.dialer.compat.telephony.TelephonyManagerCompat;
import com.android.dialer.contactphoto.ContactPhotoManager;
import com.android.dialer.dialercontact.DialerContact;
import com.android.dialer.logging.InteractionEvent;
import com.android.dialer.logging.Logger;
import com.mediatek.dialer.ext.ExtensionManager;
import com.mediatek.dialer.util.DialerVolteUtils;

import java.util.ArrayList;

/** ViewHolder for Header/Contact in {@link CallDetailsActivity}. */
public class CallDetailsHeaderViewHolder extends RecyclerView.ViewHolder
    implements OnClickListener, FailureListener {
 private static final String TAG = CallDetailsHeaderViewHolder.class.getSimpleName();
  private final CallDetailsHeaderListener callDetailsHeaderListener;
  private final ImageView callbackButton;
  private final TextView nameView;
  private final TextView numberView;
  private final TextView networkView;
  private final QuickContactBadge contactPhoto;
  private final Context context;
  private final TextView assistedDialingInternationalDirectDialCodeAndCountryCodeText;
  private final RelativeLayout assistedDialingContainer;

  private DialerContact contact;
  private @CallbackAction int callbackAction;
  ///M:[VoLTE ConfCallLog]
  private ArrayList<String> confNumbers;

  public/*M*/ CallDetailsHeaderViewHolder(View container, CallDetailsHeaderListener callDetailsHeaderListener) {
    super(container);
    context = container.getContext();
    callbackButton = container.findViewById(R.id.call_back_button);
    nameView = container.findViewById(R.id.contact_name);
    numberView = container.findViewById(R.id.phone_number);
    networkView = container.findViewById(R.id.network);
    contactPhoto = container.findViewById(R.id.quick_contact_photo);
    assistedDialingInternationalDirectDialCodeAndCountryCodeText =
        container.findViewById(R.id.assisted_dialing_text);
    assistedDialingContainer = container.findViewById(R.id.assisted_dialing_container);

    assistedDialingContainer.setOnClickListener(
        callDetailsHeaderListener::openAssistedDialingSettings);

    callbackButton.setOnClickListener(this);
    this.callDetailsHeaderListener = callDetailsHeaderListener;
    Logger.get(context)
        .logQuickContactOnTouch(
            contactPhoto, InteractionEvent.Type.OPEN_QUICK_CONTACT_FROM_CALL_DETAILS, true);
  }

  private boolean hasAssistedDialingFeature(Integer features) {
    return (features & TelephonyManagerCompat.FEATURES_ASSISTED_DIALING)
        == TelephonyManagerCompat.FEATURES_ASSISTED_DIALING;
  }

  void updateAssistedDialingInfo(CallDetailsEntry callDetailsEntry) {

    if (callDetailsEntry != null && hasAssistedDialingFeature(callDetailsEntry.getFeatures())) {
      showAssistedDialingContainer(true);
      callDetailsHeaderListener.createAssistedDialerNumberParserTask(
          new CallDetailsActivity.AssistedDialingNumberParseWorker(),
          this::updateAssistedDialingText,
          this::onFailure);

    } else {
      showAssistedDialingContainer(false);
    }
  }

  public/*M*/ void showAssistedDialingContainer(boolean shouldShowContainer) {
    if (shouldShowContainer) {
      assistedDialingContainer.setVisibility(View.VISIBLE);
    } else {
      LogUtil.i(
          "CallDetailsHeaderViewHolder.updateAssistedDialingInfo",
          "hiding assisted dialing ui elements");
      assistedDialingContainer.setVisibility(View.GONE);
    }
  }

  private void updateAssistedDialingText(Integer countryCode) {

    // Try and handle any poorly formed inputs.
    if (countryCode <= 0) {
      onFailure(new IllegalStateException());
      return;
    }

    LogUtil.i(
        "CallDetailsHeaderViewHolder.updateAssistedDialingText", "Updating Assisted Dialing Text");
    assistedDialingInternationalDirectDialCodeAndCountryCodeText.setText(
        context.getString(
            R.string.assisted_dialing_country_code_entry, String.valueOf(countryCode)));
  }

  @Override
  public void onFailure(Throwable unused) {
    assistedDialingInternationalDirectDialCodeAndCountryCodeText.setText(
        R.string.assisted_dialing_country_code_entry_failure);
  }

  /** Populates the contact info fields based on the current contact information. */
  void updateContactInfo(DialerContact contact,@CallbackAction int callbackAction) {
    updateContactInfo(contact, callbackAction, null);
  }

  /** Populates the contact info fields based on the current contact information. */
  public/*M*/ void updateContactInfo(DialerContact contact, @CallbackAction int callbackAction,
      ArrayList<String> confNumbers) {
    this.contact = contact;
    this.confNumbers = confNumbers;
    ContactPhotoManager.getInstance(context)
        .loadDialerThumbnailOrPhoto(
            contactPhoto,
            contact.getContactUri().isEmpty() ? null : Uri.parse(contact.getContactUri()),
            contact.getPhotoId(),
            contact.getPhotoUri().isEmpty() ? null : Uri.parse(contact.getPhotoUri()),
            contact.getNameOrNumber(),
            contact.getContactType());

    ///M: [VoLTE ConfCallLog] @{
    if(this.confNumbers != null) {
      nameView.setText(context.getString(R.string.conference_call_name));
    } else {
    ///@}
      nameView.setText(contact.getNameOrNumber());
    }
    if (!TextUtils.isEmpty(contact.getDisplayNumber())) {
      numberView.setVisibility(View.VISIBLE);
      // set callerNumber text show direction LTR @{
      final BidiFormatter formatter = BidiFormatter.getInstance();
      String secondaryInfo =
          TextUtils.isEmpty(contact.getNumberLabel())
              ? contact.getDisplayNumber()
              : context.getString(
                  com.android.contacts.common.R.string.call_subject_type_and_number,
                  formatter.unicodeWrap(contact.getNumberLabel(),
                            TextDirectionHeuristics.FIRSTSTRONG_LTR),
                  formatter.unicodeWrap(contact.getDisplayNumber(),
                            TextDirectionHeuristics.FIRSTSTRONG_LTR));
      numberView.setText(formatter.unicodeWrap(secondaryInfo.toString()));
      /// @}
    } else {
      numberView.setVisibility(View.GONE);
      numberView.setText(null);
    }

    if (!TextUtils.isEmpty(contact.getSimDetails().getNetwork())) {
      networkView.setVisibility(View.VISIBLE);
      networkView.setText(contact.getSimDetails().getNetwork());
      if (contact.getSimDetails().getColor() != PhoneAccount.NO_HIGHLIGHT_COLOR) {
        networkView.setTextColor(contact.getSimDetails().getColor());
      }
    }
    setCallbackAction(callbackAction);
  }

  private void setCallbackAction(@CallbackAction int callbackAction) {
    this.callbackAction = callbackAction;
    switch (callbackAction) {
      case CallbackAction.DUO:
      case CallbackAction.IMS_VIDEO:
        callbackButton.setVisibility(View.VISIBLE);
        callbackButton.setImageResource(R.drawable.quantum_ic_videocam_vd_theme_24);
        break;
      case CallbackAction.VOICE:
        callbackButton.setVisibility(View.VISIBLE);
        callbackButton.setImageResource(R.drawable.quantum_ic_call_vd_theme_24);
        break;
      case CallbackAction.NONE:
        callbackButton.setVisibility(View.GONE);
        break;
        // /M: [Volte ConfCall] Support launch volte conference call @{
      case CallbackAction.CONFERENCE:
        if (confNumbers != null) {
          if (DialerVolteUtils.isVolteConfCallEnable(context)) {
            Log.d(TAG, "is a conference call, volte true, show call button");
            callbackButton.setVisibility(View.VISIBLE);
          } else {
            Log.d(TAG, "is a conference call, volte false, hide call button");
            callbackButton.setVisibility(View.GONE);
          }
        }
        break;// / @}
      default:
        throw Assert.createIllegalStateFailException("Invalid action: " + callbackAction);
    }

    /// M: for OP01 and presence plugin. @{
    ExtensionManager.getCallDetailExtension().setCallbackAction(
        callbackButton, callbackAction, contact.getNumber());
    /// @}
  }

  @Override
  public void onClick(View view) {
    if (view == callbackButton) {
      switch (callbackAction) {
        case CallbackAction.IMS_VIDEO:
          callDetailsHeaderListener.placeImsVideoCall(contact.getNumber()
            + contact.getPostDialDigits());///M:add post dial digits
          break;
        case CallbackAction.DUO:
          callDetailsHeaderListener.placeDuoVideoCall(contact.getNumber());
          break;
        case CallbackAction.VOICE:
          callDetailsHeaderListener.placeVoiceCall(
              contact.getNumber(), contact.getPostDialDigits());
          break;
           // /M: [Volte ConfCall] Support launch volte conference call @{
        case CallbackAction.CONFERENCE:
          if (confNumbers != null) {
            if (DialerVolteUtils.isVolteConfCallEnable(context)) {
              Log.d(TAG, "is a conference call, volte true, show call button");
              callDetailsHeaderListener.placeConferenceCall(confNumbers);
            }
          }
          break;// / @}
        case CallbackAction.NONE:
        default:
          throw Assert.createIllegalStateFailException("Invalid action: " + callbackAction);
      }
    } else {
      throw Assert.createIllegalStateFailException("View OnClickListener not implemented: " + view);
    }
  }

  /** Listener for the call details header */
 public/*M*/ interface CallDetailsHeaderListener {

    /** Places an IMS video call. */
    void placeImsVideoCall(String phoneNumber);

    /** Places a Duo video call. */
    void placeDuoVideoCall(String phoneNumber);

    /** Place a traditional voice call. */
    void placeVoiceCall(String phoneNumber, String postDialDigits);

    /** Access the Assisted Dialing settings * */
    void openAssistedDialingSettings(View view);

    void createAssistedDialerNumberParserTask(
        AssistedDialingNumberParseWorker worker,
        SuccessListener<Integer> onSuccess,
        FailureListener onFailure);
        ///M: [Volte ConfCall] Support launch volte conference call
        void placeConferenceCall(ArrayList<String> numbers);
  }
}
