package com.mediatek.sensorhub.ui;

import com.mediatek.sensorhub.settings.MtkSensor;
import com.mediatek.sensorhub.settings.Utils;

public class InPocketSensorActivity extends BaseTriggerSensorActivity {

    public InPocketSensorActivity() {
        super(MtkSensor.STRING_TYPE_IN_POCKET);
    }
}
