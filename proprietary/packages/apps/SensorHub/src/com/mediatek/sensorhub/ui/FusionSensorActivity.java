package com.mediatek.sensorhub.ui;

import android.content.Intent;
import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceScreen;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class FusionSensorActivity extends BaseActivity {

    private static final String TAG = "FusionSensorActivity";

    public FusionSensorActivity() {
        super("FusionSensorActivity");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.orginal_gestures_pref);
        setTitle(R.string.fusion_sensor_title);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
    }

    private void initializeAllPreferences() {
        for (String type : Utils.fusionSensorType) {
            Sensor sensor = mSensorKeyMap.get(type);
            if (sensor != null) {
                Utils.createPreference(Utils.TYPE_PREFERENCE, sensor.getName(),
                        type, getPreferenceScreen(), this);
            }
        }
    }

    private void updatePreferenceStatus() {
        for (String type : Utils.fusionSensorType) {
            Preference preference = findPreference(type);
            if (preference != null) {
                preference
                .setSummary(Utils
                        .getSensorStatus(type)
                        ? R.string.running_summary : R.string.space_summary);
            }
        }
    }

    public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
        String sensorType = preference.getKey();
        Log.d(TAG, "onPreferenceTreeClick : " + preference.getTitle() + sensorType);

        Intent intent = new Intent();
        if (Sensor.STRING_TYPE_GAME_ROTATION_VECTOR.equals(sensorType)) {
            intent.setClass(this, GameRotationSensorActivity.class);
        } else if (Sensor.STRING_TYPE_ORIENTATION.equals(sensorType)) {
            intent.setClass(this, OrientationSensorActivity.class);
        } else {
            intent = null;
        }
        if (intent != null) {
            startActivity(intent);
        }
        return true;
    }

}
