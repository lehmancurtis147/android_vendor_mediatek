package com.mediatek.sensorhub.ui;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.SwitchPreference;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class BaseTriggerSensorActivity extends BaseActivity implements OnPreferenceChangeListener,
        OnSharedPreferenceChangeListener {

    private static final String TAG = "BaseTriggerSensorActivity";
    public static final String KEY_ENABLE_NOTIFY = "enable_notify";
    public static final String KEY_AUTO_ENABLED = "auto_enabled";
    private SwitchPreference mSensorPreference;
    private SwitchPreference mEnableNotifiSwitch;
    private SwitchPreference mAutoEnabledSwitch;
    private String mNotifyStatusKey;
    private String mAutoEnabledStatusKey;
    private String mSensorType;

    public BaseTriggerSensorActivity(String sensorType) {
        super(sensorType);
        mNotifyStatusKey = sensorType + Utils.KEY_NOTIFY_STATUS_SUFFIX;
        mAutoEnabledStatusKey = sensorType + Utils.KEY_AUTO_TRIGGER_STATUS_SUFFIX;
        mSensorType = sensorType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.trigger_sensor_pref);
        initializeAllPreferences();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updatePreferenceStatus();
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        Utils.getSharedPreferences(this, Utils.SHARED_PREF_SENSOR_HUB)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    private void initializeAllPreferences() {
        mSensorPreference = (SwitchPreference) Utils.createPreference(Utils.TYPE_SWITCH,
                mSensorKeyMap.get(mSensorType).getName(), mSensorType,
                getPreferenceScreen(), this);
        mSensorPreference.setOrder(-1);
        mEnableNotifiSwitch = (SwitchPreference) findPreference(KEY_ENABLE_NOTIFY);
        mEnableNotifiSwitch.setOnPreferenceChangeListener(this);
        mAutoEnabledSwitch = (SwitchPreference) findPreference(KEY_AUTO_ENABLED);
        mAutoEnabledSwitch.setOnPreferenceChangeListener(this);
    }

    private void updatePreferenceStatus() {
        mSensorPreference.setChecked(Utils.getSensorStatus(mSensorType));
        mEnableNotifiSwitch.setChecked(Utils.getSensorStatus(mNotifyStatusKey));
        mAutoEnabledSwitch.setChecked(Utils.getSensorStatus(mAutoEnabledStatusKey));
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        boolean bNewValue = (Boolean) newValue;
        if (preference == mSensorPreference) {
            Utils.setSensorStatus(mSensorType, bNewValue);
            if (mBound) {
                mSensorService.registerSensor(preference.getKey(), bNewValue);
            }
        } else if (preference == mEnableNotifiSwitch) {
            Utils.setSensorStatus(mNotifyStatusKey, bNewValue);
        } else if (preference == mAutoEnabledSwitch) {
            Utils.setSensorStatus(mAutoEnabledStatusKey, bNewValue);
        }
        return true;
    }

    @Override
    public void onSensorChanged(float[] value) {
        mSensorPreference.setSummary(String.valueOf(value[0]));
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (mSensorType.equals(key)) {
            boolean status = sharedPreferences.getBoolean(key, false);
            Log.d(TAG, "onSharedPreferenceChanged : " + key + " status " + status);
            mSensorPreference.setChecked(status);
        }
    }
}
