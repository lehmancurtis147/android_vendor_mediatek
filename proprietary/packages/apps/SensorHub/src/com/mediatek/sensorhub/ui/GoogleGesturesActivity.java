package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.mediatek.sensorhub.settings.Utils;

import java.util.HashMap;

public class GoogleGesturesActivity extends PreferenceActivity {
    private static final HashMap<String, Sensor> mSensorKeyMap = Utils.getSensorKeyMap();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.google_gestures_pref);
        for (String type : Utils.orginalGestureType) {
            Sensor sensor = mSensorKeyMap.get(type);
            if (sensor == null) {
                getPreferenceScreen().removePreference(findPreference(type));
            }
        }
    }

}
