package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;

import com.mediatek.sensorhub.settings.Utils;

public class MotionDetectSensorActivity extends BaseTriggerSensorActivity {

    public MotionDetectSensorActivity() {
        super(Sensor.STRING_TYPE_MOTION_DETECT);
    }
}
