package com.mediatek.sensorhub.ui;

import android.hardware.Sensor;
import android.util.Log;

import com.mediatek.sensorhub.settings.Utils;

public class MagneticFieldSensorActivity extends CustomerSensorBaseActivity {

    public MagneticFieldSensorActivity() {
        super(Sensor.STRING_TYPE_MAGNETIC_FIELD);
    }

    @Override
    public void onAccuracyChanged(int accuracy) {
        Log.d("SensorEventListenerService", "accuracy : " + accuracy);
        mSensorSwitch.setSummary(mSensorSwitch.getSummary() + getString(R.string.accuracy_string)
                + accuracy);
    }
}
