package com.mediatek.engineermode;

import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;

import java.util.NoSuchElementException;

import vendor.mediatek.hardware.engineermode.V1_1.IEmd;

/**
 * Common functions.
 */
public class EmUtils {
    public static final String TAG = "EmUtils";
    public static Toast mToast = null;
    public static IEmd mEmHIDLService = null;
    public static Phone mPhone = null;
    public static Phone mPhoneSlot1 = null;
    public static Phone mPhoneSlot2 = null;
    public static Phone mPhoneMain = null;
    public static Phone mPhoneCdma = null;

    public static Phone getmPhone(int phoneid) {
        switch (phoneid) {
            case -1:
                mPhoneMain = PhoneFactory.getPhone(ModemCategory.getCapabilitySim());
                mPhone = mPhoneMain;
                break;
            case PhoneConstants.SIM_ID_1:
                if (mPhoneSlot1 == null) {
                    mPhoneSlot1 = PhoneFactory.getPhone(PhoneConstants.SIM_ID_1);
                }
                mPhone = mPhoneSlot1;
                break;
            case PhoneConstants.SIM_ID_2:
                if (mPhoneSlot2 == null) {
                    mPhoneSlot2 = PhoneFactory.getPhone(PhoneConstants.SIM_ID_2);
                }
                mPhone = mPhoneSlot2;
                break;
            case 0xff:
                mPhoneCdma = ModemCategory.getCdmaPhone();
                mPhone = mPhoneCdma;
                break;
        }
        Elog.v(TAG, "getmPhone,phoneid = " + phoneid);
        return mPhone;
    }

    public static void invokeOemRilRequestStringsEm(int phoneid, String[] command, Message
            response) {
        try {
            getmPhone(phoneid).invokeOemRilRequestStrings(command, response);
        } catch (Exception e) {
            Elog.v(TAG, e.getMessage());
            Elog.v(TAG, "get phone invokeOemRilRequestStrings failed");
        }
    }

    public static void invokeOemRilRequestStringsEm(String[] command, Message
            response) {
        invokeOemRilRequestStringsEm(-1, command, response);
    }

    public static void invokeOemRilRequestStringsEm(boolean isCdma, String[] command, Message
            response) {
        if (isCdma)
            invokeOemRilRequestStringsEm(0xff, command, response);
        else
            invokeOemRilRequestStringsEm(-1, command, response);
    }

    public static void invokeOemRilRequestRawEm(int phoneid, byte[] command, Message
            response) {
        try {
            getmPhone(phoneid).invokeOemRilRequestRaw(command, response);
        } catch (Exception e) {
            Elog.v(TAG, e.getMessage());
            Elog.v(TAG, "get phone invokeOemRilRequestRaw failed");
        }
    }

    public static void invokeOemRilRequestRawEm(byte[] command, Message
            response) {
        invokeOemRilRequestRawEm(-1, command, response);
    }

    public static IEmd getEmHidlService() {
        Elog.v(TAG, "getEmHidlService ...");
        if (mEmHIDLService == null) {
            Elog.v(TAG, "getEmHidlService init...");
            try {
                mEmHIDLService = IEmd.getService("EmHidlServer", true);
            } catch (RemoteException e) {
                e.printStackTrace();
                Elog.e(TAG, "EmHIDLConnection exception1 ...");
            } catch (NoSuchElementException e) {
                e.printStackTrace();
                Elog.e(TAG, "EmHIDLConnection exception2 ...");
            }
        }
        return mEmHIDLService;
    }

    public static void rebootModem() {
        try {
            getmPhone(-1).invokeOemRilRequestStrings(new String[]{"SET_TRM", "2"}, null);
        } catch (Exception e) {
            Elog.v(TAG, e.getMessage());
            Elog.v(TAG, "rebootModem SET_TRM 2 failed");
        }
        Elog.d(TAG, "rebootModem SET_TRM 2");
    }

    public static String systemPropertyGet(String property_name, String default_value) {
        String property_value = "";
        try {
            property_value = SystemProperties.get(property_name, default_value);
        } catch (Exception e) {
            Elog.e(TAG, "EmUtils systemPropertyGet failed");
        }

        return property_value;
    }

    public static boolean systemPropertySet(String property_name, String set_value) {
        try {
            SystemProperties.set(property_name, set_value);
            return true;
        }catch (Exception e){
            Elog.e(TAG, "EmUtils systemPropertySet failed :" + property_name);
            return false;
        }
    }

    public static void showToast(String msg, int time) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(EmApplication.getContext(), msg, time);
        mToast.show();
    }

    public static void showToast(int msg_id, int time) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(EmApplication.getContext(), msg_id, time);
        mToast.show();
    }

    public static void showToast(int msg_id) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(EmApplication.getContext(), msg_id, Toast.LENGTH_SHORT);
        mToast.show();
    }

    public static void showToast(String msg) {
        if (mToast != null) {
            mToast.cancel();
        }
        mToast = Toast.makeText(EmApplication.getContext(), msg, Toast.LENGTH_SHORT);
        mToast.show();
    }

    public static void setAirplaneModeEnabled(boolean enabled) {
        Elog.d(TAG, "setAirplaneModEnabled = " + enabled);
        final ConnectivityManager mgr =
                (ConnectivityManager) EmApplication.getContext().getSystemService(EmApplication
                        .getContext().CONNECTIVITY_SERVICE);
        mgr.setAirplaneMode(enabled);
    }

    public static void initPoweroffmdMode(boolean enabled, boolean RFonly) {
        Elog.d(TAG, "initPoweroffmdMode: " + enabled + ",RFonly: " + RFonly);
        systemPropertySet("vendor.ril.test.poweroffmd", RFonly ? "0" : "1");
        systemPropertySet("vendor.ril.testmode", enabled ? "1" : "0");
    }


    public static boolean ifAirplaneModeEnabled() {
        boolean isAirplaneModeOn = Settings.System.getInt(EmApplication.getContext()
                .getContentResolver(), Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        Elog.d(TAG, "isAirplaneModeOn: " + isAirplaneModeOn);
        return isAirplaneModeOn;
    }

    public static void writeSharedPreferences(String preferencesName, String key, String value) {
        final SharedPreferences preferences = EmApplication.getContext().
                getSharedPreferences(preferencesName, EmApplication.getContext().MODE_PRIVATE);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String readSharedPreferences(String preferencesName,
                                               String key, String default_value) {
        String value = "";
        SharedPreferences preferences = EmApplication.getContext().
                getSharedPreferences(preferencesName, EmApplication.getContext().MODE_PRIVATE);
        value = preferences.getString(key, default_value);
        return value;
    }


}
