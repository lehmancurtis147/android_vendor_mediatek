package com.mediatek.engineermode.iotconfig;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class XCAPConfigFragment extends Fragment implements OnItemClickListener {
    protected static final String TAG = "Iot/XCAPConfigFragment";
    public static final int UNKNOW = -1;
    protected static final int DEFAULT_VALUE_XCAP_READY = 0;
    protected static final int SET_VALUE_XCAP_OK = 1;
    protected static final int SET_VALUE_XCAP_FAIL = 2;
    protected static final int RESET_VALUE_XCAP_OK = 3;
    protected static final int XCAP_ACTION_GET = 0;
    protected static final int XCAP_ACTION_SET = 1;
    protected static final int XCAP_ACTION_RESET = 2;
    protected static final String DEFAULT_VALUE_XCAP_INTEGER = "-1";
    protected static final String DEFAULT_VALUE_XCAP_BOOLEAN = "1";
    protected static final String DEFAULT_VALUE_XCAP_STRING = "";

    private ListView listView;
    private Button btnSave;
    private XcapSettingAdapter adapter;
    private String[] mXcapBoolCfg;
    private LinkedHashMap<String, String> XcapItems;
    private HashMap<String, String> XcapItemTypes;
    private Button btnCancel;
    private Phone mPhone;

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case DEFAULT_VALUE_XCAP_READY:
                updateUI((List<XCAPModel>) msg.obj);
                break;
            case SET_VALUE_XCAP_OK:
                updateUI((List<XCAPModel>) msg.obj);
                Toast.makeText(getActivity(), "Set values ok !",
                        Toast.LENGTH_SHORT).show();
                break;
            case RESET_VALUE_XCAP_OK:
                updateUI((List<XCAPModel>) msg.obj);
                Toast.makeText(getActivity(), "Reset values ok !",
                        Toast.LENGTH_SHORT).show();
                break;
            case SET_VALUE_XCAP_FAIL:
                Toast.makeText(getActivity(), "Set values failed !",
                        Toast.LENGTH_SHORT).show();
                new AlertDialog.Builder(getActivity())
                        .setMessage("Set values failed!")
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                            int id) {
                                        dialog.cancel();
                                    }
                                }).show();
            default:
                break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        mPhone = PhoneFactory.getDefaultPhone();
        if (FeatureSupport.is93Modem()) {
            mXcapBoolCfg = new String[] { "Support Media Tag",
                    "Support CFNL", "Enable TMPI",
                    "Enable XCAP Cache", "Support Put CF Root",
                    "Support Timer In CFNRY", "Support Put Clir Root",
                    "Need Quotation Mark", "SSL Trust", "Gzip Support"};
            XcapItems = new LinkedHashMap<String, String>() {
                {
                    put("Bool Config", IotConfigConstant.FK_SS_BOOLCONFIG);
                    put("Bool Value", IotConfigConstant.FK_SS_BOOLVALUE);
                    put("XCAP Protocol", IotConfigConstant.FK_SS_XCAP_PROTOCOL);
                    put("GBA Protocol", IotConfigConstant.FK_SS_GBA_PROTOCOL);
                    put("XCAP Root", IotConfigConstant.FK_SS_NAFHOST);
                    put("GBA BSF URL", IotConfigConstant.FK_SS_BSFHOST);
                    put("GBA BSF Path", IotConfigConstant.FK_SS_BSFURLPATH);
                    put("AUID", IotConfigConstant.FK_SS_AUID);
                    put("Digest User ID", IotConfigConstant.FK_SS_DIGEST_ID);
                    put("Digest User Password", IotConfigConstant.FK_SS_DIGEST_PWD);
                    put("IMEI Header", IotConfigConstant.FK_SS_IMEIHEADER);
                    put("GBA Type", IotConfigConstant.FK_SS_GBA_TYPE);
                    put("XCAP Port", IotConfigConstant.FK_SS_XCAP_PORT);
                    put("GBA Port", IotConfigConstant.FK_SS_GBA_PORT);
                    put("Media Type", IotConfigConstant.FK_SS_MEDIATYPE);
                    put("URL Encoding",IotConfigConstant.FK_SS_URL_ENCODING);
                }
            };
            XcapItemTypes = new HashMap<String, String>() {
                {
                    put("Bool Config", IotConfigConstant.BOOLEANTYPE);
                    put("Bool Value", IotConfigConstant.BOOLEANTYPE);
                    put("XCAP Protocol", IotConfigConstant.STRINGTYPE);
                    put("GBA Protocol", IotConfigConstant.STRINGTYPE);
                    put("XCAP Root", IotConfigConstant.STRINGTYPE);
                    put("GBA BSF URL", IotConfigConstant.STRINGTYPE);
                    put("GBA BSF Path", IotConfigConstant.STRINGTYPE);
                    put("AUID", IotConfigConstant.STRINGTYPE);
                    put("Digest User ID", IotConfigConstant.STRINGTYPE);
                    put("Digest User Password", IotConfigConstant.STRINGTYPE);
                    put("IMEI Header", IotConfigConstant.STRINGTYPE);
                    put("GBA Type", IotConfigConstant.STRINGTYPE);
                    put("XCAP Port", IotConfigConstant.INTEGERTYPE);
                    put("GBA Port", IotConfigConstant.INTEGERTYPE);
                    put("Media Type", IotConfigConstant.INTEGERTYPE);
                    put("URL Encoding",IotConfigConstant.INTEGERTYPE);
                }
            };

        } else {
            mXcapBoolCfg = new String[] { "Not support XCAP", "Use http protocol",
                    "Handle 409 exception", "Fill complete forward to",
                    "Use namespace prefix", "Need to re-reg ims",
                    "Need to append country code", "Support media tag",
                    "Update whole CLIR", "Query whole Simserv", "Disable etag",
                    "http error to unknown host", "Use xcap PDN",
                    "Use internet PDN", "Noreply timer inside CF rule",
                    "Support time slot", "Save whole node",
                    "Set CFNL when set CFNRc", "Not support CFNL setting" };
            XcapItems = new LinkedHashMap<String, String>() {
                {
                    put("Bool Config", IotConfigConstant.FK_SS_BOOLCONFIG);
                    put("Bool Value", IotConfigConstant.FK_SS_BOOLVALUE);
                    put("Element content type", IotConfigConstant.FK_SS_CONTENTTYPE);
                    put("AUID", IotConfigConstant.FK_SS_AUID);
                    put("XCAP Root", IotConfigConstant.FK_SS_XCAPROOT);
                    put("RuleID CFU", IotConfigConstant.FK_SS_RELEID_CFU);
                    put("RuleID CFB", IotConfigConstant.FK_SS_RELEID_CFB);
                    put("RuleID CFNRy", IotConfigConstant.FK_SS_RELEID_CFNRY);
                    put("RuleID CFNRc", IotConfigConstant.FK_SS_RELEID_CFNRC);
                    put("RuleID CFNL", IotConfigConstant.FK_SS_RELEID_CFNL);
                    put("Digest user ID", IotConfigConstant.FK_SS_DIGEST_ID);
                    put("Digest Password", IotConfigConstant.FK_SS_DIGEST_PWD);
                    put("XCAP port", IotConfigConstant.FK_SS_XCAPPORT);
                    put("Media type", IotConfigConstant.FK_SS_MEDIATYPE);
                    put("Data keep alive timer", IotConfigConstant.FK_SS_ALIVETIMER);
                    put("Data connection time out",
                            IotConfigConstant.FK_SS_REQTIMER);
                    put("Disconnect data PDN timer",
                            IotConfigConstant.FK_SS_CDTIMER);
                    put("Gba Bsf server Url",
                            IotConfigConstant.FK_SS_GBA_BSFSERVERURL);
                    put("Gba trust all",
                            IotConfigConstant.FK_SS_GBA_ENABLEBATRUSTALL);
                    put("Gba force run",
                            IotConfigConstant.FK_SS_GBA_ENABLEBAFORCERUN);
                    put("Gba type", IotConfigConstant.FK_SS_GBA_GBATYPE);
                }
            };
            XcapItemTypes = new HashMap<String, String>() {
                {
                    put("Bool Config", IotConfigConstant.BOOLEANTYPE);
                    put("Bool Value", IotConfigConstant.BOOLEANTYPE);
                    put("Element content type", IotConfigConstant.STRINGTYPE);
                    put("AUID", IotConfigConstant.STRINGTYPE);
                    put("XCAP Root", IotConfigConstant.STRINGTYPE);
                    put("RuleID CFU", IotConfigConstant.STRINGTYPE);
                    put("RuleID CFB", IotConfigConstant.STRINGTYPE);
                    put("RuleID CFNRy", IotConfigConstant.STRINGTYPE);
                    put("RuleID CFNRc", IotConfigConstant.STRINGTYPE);
                    put("RuleID CFNL", IotConfigConstant.STRINGTYPE);
                    put("Digest user ID", IotConfigConstant.STRINGTYPE);
                    put("Digest Password", IotConfigConstant.STRINGTYPE);
                    put("XCAP port", IotConfigConstant.INTEGERTYPE);
                    put("Media type", IotConfigConstant.INTEGERTYPE);
                    put("Data keep alive timer", IotConfigConstant.INTEGERTYPE);
                    put("Data connection time out", IotConfigConstant.INTEGERTYPE);
                    put("Disconnect data PDN timer", IotConfigConstant.INTEGERTYPE);
                    put("Gba Bsf server Url", IotConfigConstant.STRINGTYPE);
                    put("Gba trust all", IotConfigConstant.BOOLEANTYPE);
                    put("Gba force run", IotConfigConstant.BOOLEANTYPE);
                    put("Gba type", IotConfigConstant.STRINGTYPE);
                }
            };
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.iot_xcap_config, null);
        listView = (ListView) view.findViewById(R.id.xcap_list);
        btnSave = (Button) view.findViewById(R.id.btnSave);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        adapter = new XcapSettingAdapter(getActivity(), getEmptyModel());
        listView.setAdapter(adapter);
        btnSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new LoadModelThread(XCAP_ACTION_SET).start();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new LoadModelThread(XCAP_ACTION_RESET).start();
                ;
            }
        });
        return view;
    }

    public void hideInputMethod(View view) {
        if (getActivity() != null && view == null) {
            view = getActivity().getCurrentFocus();
        }
        if (view != null) {
            view.clearFocus();
            InputMethodManager imm = (InputMethodManager)getActivity()
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive()) {
                imm.hideSoftInputFromWindow(view.getWindowToken(),  0);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isVisibleToUser) {
            hideInputMethod(null);
        }
    }

    public class LoadModelThread extends Thread {
        private int action;

        public LoadModelThread(int action) {
            this.action = action;
        }

        public void run() {
            Message msg = new Message();
            switch (action) {
            case XCAP_ACTION_GET:
                msg.obj = getModel();
                msg.what = DEFAULT_VALUE_XCAP_READY;
                break;
            case XCAP_ACTION_SET:
                if (setModel()) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    msg.obj = getModel();
                    msg.what = SET_VALUE_XCAP_OK;
                } else {
                    msg.what = SET_VALUE_XCAP_FAIL;
                }
                break;
            case XCAP_ACTION_RESET:
                msg.obj = resetModel();
                msg.what = RESET_VALUE_XCAP_OK;
                break;
            default:
                msg.what = UNKNOW;
                break;
            }
            mHandler.sendMessage(msg);
        }

    }

    public void updateUI(List<XCAPModel> updateModel) {
        adapter.clear();
        adapter.addAll(updateModel);
        adapter.notifyDataSetChanged();
    }

    private boolean setModel() {
        int mBoolConfig = 0;
        int mBoolValue = 0;
        List<XCAPModel> mList = adapter.getList();
        Elog.d(TAG, "[setModel]listSize : " + mList.size());
        for (int i = 0; i < mList.size(); i++) {
            if (i < mXcapBoolCfg.length) {
                boolean itemSelected = false;
                if (mList.get(i).isConfiged()) {
                    if (mList.get(i).isSelected()) {
                        mBoolValue |= (1 << i);
                    } else {
                        mBoolValue &= ~(1 << i);
                    }
                    mBoolConfig |= (1 << i);
                } else {
                    mBoolConfig &= ~(1 << i);
                }
            } else {
                if (mPhone != null) {
                    if (!"".equals(mList.get(i).getValue())) {
                        ((MtkGsmCdmaPhone)mPhone).setSuppServProperty
                            (XcapItems.get(mList.get(i).getName()),mList.get(i).getValue());
                    }
                }
            }
        }
        Elog.d(TAG, "[setModel]BoolValue: " + mBoolValue
                + " BoolConfig: " + mBoolConfig);
        if (mPhone != null) {
            ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(IotConfigConstant.FK_SS_BOOLVALUE, "" + mBoolValue);
            ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(IotConfigConstant.FK_SS_BOOLCONFIG, "" + mBoolConfig);
        }
        return true;
    }

    private List<XCAPModel> resetModel() {
        ArrayList<XCAPModel> mList = new ArrayList<XCAPModel>();
        if (mPhone != null) {
            ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(IotConfigConstant.FK_SS_BOOLVALUE, "0");
            ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(IotConfigConstant.FK_SS_BOOLCONFIG, "0");
        }
        for (int i = 0; i < (mXcapBoolCfg.length); i++) {
            mList.add(new XCAPModel(mXcapBoolCfg[i],
                    IotConfigConstant.BOOLEANTYPE));
        }
        for (String str : XcapItems.keySet()) {
            if (str.equals("Bool Config") || str.equals("Bool Value")) {
                continue;
            }
            if (XcapItemTypes.get(str).equals(IotConfigConstant.STRINGTYPE)) {
                if (mPhone != null) {
                    ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(XcapItems.get(str),
                                                                DEFAULT_VALUE_XCAP_STRING);
                }
                mList.add(new XCAPModel(str, SystemProperties.get(XcapItems
                        .get(str)), XcapItemTypes.get(str)));
            } else if (XcapItemTypes.get(str).equals(
                    IotConfigConstant.INTEGERTYPE)) {
                if (mPhone != null) {
                    ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(XcapItems.get(str),
                                                String.valueOf(DEFAULT_VALUE_XCAP_INTEGER));
                }
                mList.add(new XCAPModel(str, SystemProperties.get(XcapItems
                        .get(str)), XcapItemTypes.get(str)));
            } else {
                if (mPhone != null) {
                    ((MtkGsmCdmaPhone)mPhone).setSuppServProperty(XcapItems.get(str),
                                                                DEFAULT_VALUE_XCAP_BOOLEAN);
                }
                mList.add(new XCAPModel(str, SystemProperties.get(
                        XcapItems.get(str)).equals("1") ? true : false,
                        XcapItemTypes.get(str)));
            }
        }
        return mList;
    }

    private List<XCAPModel> getEmptyModel() {
        ArrayList<XCAPModel> mList = new ArrayList<XCAPModel>();
        for (int i = 0; i < (mXcapBoolCfg.length); i++) {
            mList.add(new XCAPModel(mXcapBoolCfg[i],
                    IotConfigConstant.BOOLEANTYPE));
        }
        for (String str : XcapItems.keySet()) {
            if (str.equals("Bool Config") || str.equals("Bool Value")) {
                continue;
            }
            mList.add(new XCAPModel(str, XcapItemTypes.get(str)));

        }
        return mList;
    }

    private List<XCAPModel> getModel() {
        ArrayList<XCAPModel> mList = new ArrayList<XCAPModel>();
        String boolValue = Integer.toBinaryString(Integer
                .valueOf(SystemProperties.get(
                        IotConfigConstant.FK_SS_BOOLVALUE, "0")));
        String boolConfig = Integer.toBinaryString(Integer
                .valueOf(SystemProperties.get(
                        IotConfigConstant.FK_SS_BOOLCONFIG, "0")));
        Elog.d(TAG, "[getModel]BoolValue: " + boolValue
                + " BoolConfig: " + boolConfig);
        char[] results = new char[boolValue.length()];
        char[] configs = new char[boolConfig.length()];
        results = boolValue.toCharArray();
        configs = boolConfig.toCharArray();
        for (int i = 0; i < (mXcapBoolCfg.length); i++) {
            boolean itemConfiged = false;
            if (i < configs.length && configs[configs.length - 1 - i] == ('1')) {
                itemConfiged = true;
            }
            boolean itemSelected = false;
            if (i < results.length && results[results.length - 1 - i] == ('1')) {
                itemSelected = true;
            }
            mList.add(new XCAPModel(mXcapBoolCfg[i], itemConfiged,
                    itemSelected, IotConfigConstant.BOOLEANTYPE));
        }
        for (String str : XcapItems.keySet()) {
            if (str.equals("Bool Config") || str.equals("Bool Value")) {
                continue;
            }
            if (XcapItemTypes.get(str).equals(IotConfigConstant.BOOLEANTYPE)) {
                boolean itemSelected = false;
                if (SystemProperties.get(XcapItems.get(str), "1").equals("1")) {
                    itemSelected = true;
                }
                mList.add(new XCAPModel(str, itemSelected, XcapItemTypes
                        .get(str)));
            } else {
                mList.add(new XCAPModel(str, SystemProperties.get(
                        XcapItems.get(str), ""), XcapItemTypes.get(str)));
            }
        }
        return mList;
    }

    @Override
    public void onResume() {
        // TODO Auto-generated method stub
        new LoadModelThread(XCAP_ACTION_GET).start();
        super.onResume();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
            long id) {
        // TODO Auto-generated method stub

    }

}

class XcapSettingAdapter extends ArrayAdapter<XCAPModel> {

    private List<XCAPModel> list;
    private final Activity context;
    int listPosititon;

    public XcapSettingAdapter(Activity context, List<XCAPModel> list) {
        super(context, R.layout.iot_xcap_row, list);
        this.context = context;
        this.list = list;
    }

    public void refresh(List<XCAPModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public List<XCAPModel> getList() {
        return this.list;
    }

    static class ViewHolder {
        protected TextView text;
        protected CheckBox checkbox;
        protected EditText value;
        protected Button btnSet;
    }

    private OnClickListener onClickLinstener = new OnClickListener() {

        @Override
        public void onClick(View v) {
            int getPosition = (Integer) v.getTag();
            showConfirmDialog(v, getPosition);
        }
    };

    public void showConfirmDialog(final View view, final int getPosition) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder
                .setCancelable(false)
                .setMessage(
                        context.getResources().getString(
                                R.string.set_xcap_dialog_message))
                .setTitle(
                        context.getResources().getString(
                                R.string.set_xcap_dialog_title))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        list.get(getPosition).setConfiged(true);
                        list.get(getPosition).setSelected(false);
                        notifyDataSetChanged();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                list.get(getPosition).setConfiged(false);
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listPosititon = position;
        ViewHolder viewHolder = null;
        if (convertView == null) {
            LayoutInflater inflator = context.getLayoutInflater();
            convertView = inflator.inflate(R.layout.iot_xcap_row, null);
            viewHolder = new ViewHolder();
            viewHolder.text = (TextView) convertView
                    .findViewById(R.id.xcap_label);
            viewHolder.checkbox = (CheckBox) convertView
                    .findViewById(R.id.xcap_check);
            viewHolder.value = (EditText) convertView
                    .findViewById(R.id.xcap_value);
            viewHolder.btnSet = (Button) convertView
                    .findViewById(R.id.set_xcap);

            viewHolder.btnSet.setOnClickListener(onClickLinstener);

            viewHolder.checkbox
                    .setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                        @Override
                        public void onCheckedChanged(CompoundButton buttonView,
                                boolean isChecked) {
                            int getPosition = (Integer) buttonView.getTag();
                            list.get(getPosition).setSelected(
                                    buttonView.isChecked());
                        }
                    });
            viewHolder.value.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start,
                        int before, int count) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start,
                        int count, int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                    list.get(position).setValue(s.toString());
                }
            });

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.xcap_label, viewHolder.text);
            convertView.setTag(R.id.xcap_value, viewHolder.value);
            convertView.setTag(R.id.xcap_check, viewHolder.checkbox);
            convertView.setTag(R.id.set_xcap, viewHolder.btnSet);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.checkbox.setTag(position); // This line is important.
        viewHolder.btnSet.setTag(position);
        viewHolder.value.setTag(position);
        viewHolder.text.setTag(position);

        viewHolder.text.setText((list.get(position).getName() + "("
                + list.get(position).getType() + ")").replace("("
                + IotConfigConstant.BOOLEANTYPE + ")", "?"));
        LinearLayout booleanLayout = (LinearLayout) convertView
                .findViewById(R.id.xcap_check_layout);
        if (list.get(position).getValue() != null
                && !list.get(position).getValue()
                        .equals(XCAPConfigFragment.DEFAULT_VALUE_XCAP_INTEGER)) {
            viewHolder.value.setText(list.get(position).getValue() + "");
        } else {
            viewHolder.value.setText("");
        }
        if (list.get(position).getType().equals(IotConfigConstant.BOOLEANTYPE)) {

            booleanLayout.setVisibility(View.VISIBLE);
            viewHolder.value.setVisibility(View.GONE);
            if (list.get(position).isConfiged()) {
                viewHolder.checkbox.setVisibility(View.VISIBLE);
                viewHolder.btnSet.setVisibility(View.INVISIBLE);
                viewHolder.checkbox.setChecked(list.get(position).isSelected());
            } else {
                viewHolder.checkbox.setVisibility(View.INVISIBLE);
                viewHolder.btnSet.setVisibility(View.VISIBLE);
            }
        } else {
            booleanLayout.setVisibility(View.GONE);
            viewHolder.value.setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}
