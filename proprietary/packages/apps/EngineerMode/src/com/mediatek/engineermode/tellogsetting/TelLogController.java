package com.mediatek.engineermode.tellogsetting;

import android.content.Context;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;

import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.ShellExe;
import com.mediatek.internal.telephony.IMtkTelephonyEx;

import java.io.IOException;

/**
 * Class to control telephony log.
 *
 */
public class TelLogController {

    private static final String TAG = "telLogSetting";

    private static final String TEL_LOG_PROP = "persist.vendor.log.tel_dbg";
    private static final String LOG_MUCH_PROP = "persist.vendor.logmuch";
    private static final String LOG_INTERNAL_PROP = "ro.vendor.mtklog_internal";

    private static final String PROP_ENABLE = "1";
    private static final String PROP_DISABLE = "0";
    private static final String LOG_MUCH_ENABLE = "true";
    private static final String LOG_MUCH_DISABLE = "false";
//    private static boolean sLogInternal = PROP_ENABLE.equals(
//                                              SystemProperties.get(LOG_INTERNAL_PROP));

    private IMtkTelephonyEx mTelEx  = null;

    /**
     * Constructor function.
     */
    public TelLogController() {
        mTelEx = IMtkTelephonyEx.Stub.asInterface(
                ServiceManager.getService("phoneEx"));

    }

    boolean getTelLogStatus() {
        String value = SystemProperties.get(TEL_LOG_PROP);
        Elog.i(TAG, "TEL_LOG_PROP: " + value);
        return (PROP_ENABLE.equals(value));
    }

    boolean switchTelLog(boolean enable) {
        Elog.i(TAG, "switchTelLog " + enable);
        String telLogProTar = enable ? PROP_ENABLE : PROP_DISABLE;
        String logMuchProTar = enable ? LOG_MUCH_DISABLE : LOG_MUCH_ENABLE;
        SystemProperties.set(TEL_LOG_PROP, telLogProTar);

//        if (sLogInternal) {
            SystemProperties.set(LOG_MUCH_PROP, logMuchProTar);
//        }

        if (mTelEx != null) {
            try {
                mTelEx.setTelLog(enable);
                Elog.i(TAG, "telEx.setTelLog:" + enable);
            } catch (RemoteException e) {
                Elog.e(TAG, "ITelephonyEx RemoteException");
                return false;
            }
        } else {
            return false;
        }

        return true;
    }

}