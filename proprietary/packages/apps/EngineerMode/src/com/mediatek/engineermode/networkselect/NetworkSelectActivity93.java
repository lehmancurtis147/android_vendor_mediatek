package com.mediatek.engineermode.networkselect;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.AsyncResult;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telephony.RadioAccessFamily;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.mediatek.engineermode.Elog;
import com.mediatek.engineermode.EmUtils;
import com.mediatek.engineermode.FeatureSupport;
import com.mediatek.engineermode.ModemCategory;
import com.mediatek.engineermode.R;


/**
 * For setting network mode.
 *
 * @author mtk54043
 */
public class NetworkSelectActivity93 extends Activity implements OnCheckedChangeListener {
    public static final String FK_MTK_C2K_CAPABILITY = "persist.vendor.radio.disable_c2k_cap";
    private static final String TAG = "NetworkMode93";
    private static final int EVENT_QUERY_NETWORKMODE_DONE = 101;
    private static final int EVENT_SET_NETWORKMODE_DONE = 102;
    private static final int EVENT_QUERY_EHRPD_ENABLE_DONE = 103;
    private static final int EVENT_SET_EHRPD_ENABLE_DONE = 104;
    private static final int REBOOT_DIALOG = 2000;
    private static final int INDEX_WCDMA_PREFERRED = 0;
    private static final int INDEX_GSM_ONLY = 1;
    private static final int INDEX_WCDMA_ONLY = 2;
    private static final int INDEX_TDSCDMA_ONLY = 3;
    private static final int INDEX_GSM_WCDMA_AUTO = 4;
    private static final int INDEX_GSM_TDSCDMA_AUTO = 5;
    private static final int INDEX_LTE_ONLY = 6;
    private static final int INDEX_LTE_GSM_UMTS = 7;
    private static final int INDEX_LTE_UMTS = 8;
    private static final int INDEX_CDMA_EVDO = 9;
    private static final int INDEX_CDMA_ONLY = 10;
    private static final int INDEX_EVDO_ONLY = 11;
    private static final int INDEX_LTE_CDMA_EVDO_UMTS_GSM = 12;
    private static final int INDEX_LTE_CDMA_EVDO = 13;
    private static final int INDEX_CDMA_EVDO_UMTS_GSM = 14;
    private static final int INDEX_EM_UI_NOT_SUPPORT_TYPE = INDEX_CDMA_EVDO_UMTS_GSM + 1;

    private static final int WCDMA_PREFERRED = Phone.NT_MODE_WCDMA_PREF; // 0
    private static final int GSM_ONLY = Phone.NT_MODE_GSM_ONLY; // 1
    private static final int WCDMA_ONLY = Phone.NT_MODE_WCDMA_ONLY; // 2
    private static final int TDSCDMA_ONLY = Phone.NT_MODE_TDSCDMA_ONLY; // 13
    private static final int GSM_WCDMA_AUTO = Phone.NT_MODE_GSM_UMTS; // 3
    private static final int GSM_TDSCDMA_AUTO = Phone.NT_MODE_TDSCDMA_GSM;
    private static final int LTE_ONLY = Phone.NT_MODE_LTE_ONLY; // 11
    private static final int LTE_GSM_WCDMA = Phone.NT_MODE_LTE_GSM_WCDMA; // 9
    private static final int LTE_WCDMA = Phone.NT_MODE_LTE_WCDMA; // 12
    private static final int CDMA_EVDO = Phone.NT_MODE_CDMA; // 4
    private static final int CDMA_ONLY = Phone.NT_MODE_CDMA_NO_EVDO; // 5
    private static final int EVDO_ONLY = Phone.NT_MODE_EVDO_NO_CDMA; // 6
    private static final int LTE_CDMA_EVDO_GSM_WCDMA = Phone.NT_MODE_LTE_CDMA_EVDO_GSM_WCDMA;// 10
    private static final int LTE_CDMA_EVDO = Phone.NT_MODE_LTE_CDMA_AND_EVDO; // 8
    private static final int CDMA_EVDO_GSM_WCDMA = Phone.NT_MODE_GLOBAL;// 7
    private static final int LTE_GSM_WCDMA_PREFERRED = 31;
    // RILConstants.NETWORK_MODE_LTE_GSM_WCDMA_PREF;

    private static final int CARD_TYPE_GSM_ONLY = 1;
    private static final int CARD_TYPE_GSM_CDMA = 2;
    private static final int CARD_TYPE_CDMA_ONLY = 3;
    int mSimType;
    private String[] mCardTypeValues = new String[]{"gsm_only_card", "gsm_cdma_card",
            "cdma_only_card"};
    private String[] mNetworkTypeLabel = new String[]{
            "GSM/WCDMA (WCDMA preferred)",
            "GSM only",
            "WCDMA only",
            "TD-SCDMA only",
            "GSM/WCDMA (auto)",
            "GSM/TD-SCDMA(auto)",
            "LTE only",
            "LTE/UMTS/GSM",
            "LTE/UMTS",
            "CDMA/EVDO",
            "CDMA only",
            "EVDO only",
            "LTE/CDMA/EVDO/UMTS/GSM",
            "LTE/CDMA/EVDO",
            "CDMA/EVDO/UMTS/GSM",
            "EM UI not support this type"
    };
    private Phone mPhone = null;
    private int mModemType;
    private SimCardInfo mSimCard;
    private boolean mFirstEntry = true;
    private boolean mEHRPDFirstEnter = true;
    private String[] network_mode_labels;
    private int mSubId = 1;
    private int[] mNetworkTypeValues = new int[]{WCDMA_PREFERRED, GSM_ONLY, WCDMA_ONLY,
            TDSCDMA_ONLY, GSM_WCDMA_AUTO, GSM_TDSCDMA_AUTO,
            LTE_ONLY, LTE_GSM_WCDMA, LTE_WCDMA,
            CDMA_EVDO, CDMA_ONLY, EVDO_ONLY, LTE_CDMA_EVDO_GSM_WCDMA, LTE_CDMA_EVDO,
            CDMA_EVDO_GSM_WCDMA};
    private int mCurrentSelected = 0;
    private Spinner mPreferredNetworkSpinner = null;
    private CheckBox mCbDisableC2kCapabilit = null;
    private CheckBox mDisableeHRPDCheckBox = null;
    private int selectNetworkMode;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            AsyncResult ar;
            switch (msg.what) {
                case EVENT_QUERY_NETWORKMODE_DONE:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception == null) {
                        int[] data = (int[]) ar.result;
                        if ((data == null) || (data.length <= 0)) {
                            Elog.e(TAG, "data error");
                            return;
                        }
                        int type = data[0];

                        int index = findSpinnerIndexByType(type);
                        Elog.d(TAG, "Get Preferred Type = " + type);
                        Elog.d(TAG, "Index = " + index);
                        if (index >= 0 && index < mPreferredNetworkSpinner.getCount()) {
                            mCurrentSelected = index;
                            mPreferredNetworkSpinner.setSelection(index, true);
                            Elog.d(TAG, "The NetworkSpinner show: " + index
                                    + " " + mNetworkTypeLabel[index]);
                        } else {
                            Elog.d(TAG, "Netwok select not support the type: " + type);
                            showToast("Netwok select not support the type: " + type);
                            mCurrentSelected = INDEX_EM_UI_NOT_SUPPORT_TYPE;
                            mPreferredNetworkSpinner.setSelection(INDEX_EM_UI_NOT_SUPPORT_TYPE,
                                    true);
                        }
                    } else {
                        Elog.d(TAG, "query_preferred_failed");
                        showToast("query_preferred_failed");
                    }
                    break;
                case EVENT_SET_NETWORKMODE_DONE:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception != null) {
                        showToast("set the network to : " + selectNetworkMode + " failed");
                        Elog.d(TAG, "Set Network Mode to " + selectNetworkMode + " failed!");
                        if (mPhone != null) {
                            mPhone.getPreferredNetworkType(obtainMessage
                                    (EVENT_QUERY_NETWORKMODE_DONE));
                        }
                    } else {
                        showToast("set the network to : " + selectNetworkMode + " succeed");
                        Elog.d(TAG, "Set Network Mode to " + selectNetworkMode
                                + " succeed!");
                    }
                    break;
                case EVENT_QUERY_EHRPD_ENABLE_DONE:
                    ar = (AsyncResult) msg.obj;

                    if (ar.exception == null) {
                        if (ar.result != null && ar.result instanceof String[]) {
                            String data[] = (String[]) ar.result;

                            if ((data.length > 0) && (data[0] != null)) {
                                Elog.d(TAG, "data[0]:" + data[0]);

                                if (data[0].equals("+EHRPD:0")) {
                                    mEHRPDFirstEnter = true;
                                    mDisableeHRPDCheckBox.setChecked(true);
                                } else {
                                    mEHRPDFirstEnter = false;
                                    mDisableeHRPDCheckBox.setChecked(false);
                                }
                            }
                        }
                    } else {
                        showToast(getResources().getString(R.string.query_eHRPD_state_fail));
                        Elog.d(TAG, getResources().getString(R.string.query_eHRPD_state_fail));
                    }
                    break;
                case EVENT_SET_EHRPD_ENABLE_DONE:
                    ar = (AsyncResult) msg.obj;
                    if (ar.exception != null) {
                        showToast(getResources().getString(R.string.set_eHRPD_state_fail));
                        Elog.d(TAG, getResources().getString(R.string.set_eHRPD_state_fail));
                    } else {
                        showToast(getResources().getString(R.string.set_eHRPD_state_succeed));
                        Elog.d(TAG, getResources().getString(R.string.set_eHRPD_state_succeed));
                    }
                    break;
                default:
                    break;
            }
        }
    };
    private OnItemSelectedListener mPreferredNetworkListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView parent, View v, int pos, long id) {
            Elog.d(TAG, "onItemSelected = " + pos + ",mCurrentSelected = " + mCurrentSelected
                    + " " + mNetworkTypeLabel[pos]);

            if (mCurrentSelected == pos) {
                Elog.d(TAG, "listener being invoked by setSelection, return ");
                return; // avoid listener being invoked by setSelection()
            }

            mCurrentSelected = pos;

            Message msg = mHandler.obtainMessage(EVENT_SET_NETWORKMODE_DONE);

            selectNetworkMode = mNetworkTypeValues[pos];
            Elog.d(TAG, "selectNetworkMode to: " + selectNetworkMode);

            Settings.Global.putInt(getContentResolver(),
                    Settings.Global.PREFERRED_NETWORK_MODE + mSubId, selectNetworkMode);
            if (mPhone != null) {
                mPhone.setPreferredNetworkType(selectNetworkMode, msg);
            }
        }

        @Override
        public void onNothingSelected(AdapterView parent) {
        }
    };

    private void showToast(String type) {
        Toast.makeText(this, type, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.networkmode_switching);
        mPreferredNetworkSpinner = (Spinner) findViewById(R.id.networkModeSwitching);
        findViewById(R.id.network_mode_set_hint).setVisibility(View.GONE);

        findViewById(R.id.disable_eHRPD).setVisibility(View.GONE);
        network_mode_labels = getResources().getStringArray(R.array.network_mode_labels_93);

        mSimType = getIntent().getIntExtra("mSimType", PhoneConstants.SIM_ID_1);
        Elog.i(TAG, "mSimType " + mSimType);

        boolean isCdma = ModemCategory.isCdma();
        if (isCdma) {
            mCbDisableC2kCapabilit = (CheckBox) findViewById(R.id.disable_c2k_capability);
            mCbDisableC2kCapabilit.setVisibility(View.VISIBLE);
            mCbDisableC2kCapabilit.setOnCheckedChangeListener(this);
            handleQueryCdmaCapability();

            mDisableeHRPDCheckBox = (CheckBox) findViewById(R.id.disable_eHRPD);
            mDisableeHRPDCheckBox.setOnCheckedChangeListener(this);
        } else {
            findViewById(R.id.disable_eHRPD).setVisibility(View.GONE);
        }

        CustomAdapter adapter =
                new CustomAdapter(this, android.R.layout.simple_spinner_item,
                        network_mode_labels);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPreferredNetworkSpinner.setAdapter(adapter);
        mPreferredNetworkSpinner.setOnItemSelectedListener(mPreferredNetworkListener);
        if (TelephonyManager.getDefault().getPhoneCount() > 1) {
            mPhone = PhoneFactory.getPhone(mSimType);
        } else {
            mPhone = PhoneFactory.getDefaultPhone();
        }

        if (isCdma) {
            queryeHRPDStatus();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        int mSimType = getIntent().getIntExtra("mSimType", PhoneConstants.SIM_ID_1);
        Elog.i(TAG, "mSimType " + mSimType);
        mSimCard = new SimCardInfo(mSimType);

        if (!ModemCategory.isSimReady(mSimCard.getSimType())) {
            EmUtils.showToast("The card is not ready,please check it");
            Elog.w(TAG, "The card is not ready,please check it");
            mPreferredNetworkSpinner.setEnabled(false);
            return;
        }

        mSubId = ModemCategory.getSubIdBySlot(mSimCard.getSimType());
        if (mSubId < 0) {
            Elog.w(TAG, "Invalid sub id");
            return;
        } else {
            mPreferredNetworkSpinner.setEnabled(true);
        }

        mModemType = ModemCategory.getModemType(); // modem type: 1:FDD 2:TDD
        Elog.d(TAG, "mModemType = " + mModemType);

        if (mPhone != null) {
            mPhone.getPreferredNetworkType(mHandler
                    .obtainMessage(EVENT_QUERY_NETWORKMODE_DONE));
        } else {
            Elog.w(TAG, "mPhone = null");
        }
    }


    private int findSpinnerIndexByType(int type) {
        // Not WCDMA preferred for TD
        // if (type == WCDMA_PREFERRED && mModemType == ModemCategory.MODEM_TD)
        // {
        // type = GSM_WCDMA_AUTO;
        // }
        // set tdscdma_GSM_auto ,framework return WCDMA_PREFERRED or
        // GSM_WCDMA_AUTO
        if ((type == WCDMA_PREFERRED || type == GSM_WCDMA_AUTO) &&
                (mModemType == ModemCategory.MODEM_TD) &&
                mSimCard.isCapabilitySim()) {
            type = GSM_TDSCDMA_AUTO;
        }
        // set tdscdma_only, return wcdma only mapping to tdscdma_only
        if (type == WCDMA_ONLY && mModemType == ModemCategory.MODEM_TD
                && mSimCard.isCapabilitySim()) {
            type = TDSCDMA_ONLY;
        }
        // Not support WCDMA preferred
        if (type == WCDMA_PREFERRED
                && !FeatureSupport.isSupported(FeatureSupport.FK_WCDMA_PREFERRED)) {
            type = GSM_WCDMA_AUTO;
        }
        // Consider LTE_GSM_WCDMA_PREFERRED as same with LTE_GSM_WCDMA
        if (type == LTE_GSM_WCDMA_PREFERRED) {
            type = LTE_GSM_WCDMA;
        }
        for (int i = 0; i < mNetworkTypeValues.length; i++) {
            if (mNetworkTypeValues[i] == type && isAvailable(i)) {
                return i;
            }
        }
        return -1;
    }

    private boolean isAvailable(int index) {

        if (INDEX_EM_UI_NOT_SUPPORT_TYPE == index) {
            return false;
        }

        // for main card
        if (mSimCard.isCapabilitySim()) {

            // for cdma only card, remove the other items except cdma items
            if (mSimCard.getCardType() == CARD_TYPE_CDMA_ONLY
                    && (index == INDEX_CDMA_ONLY
                    || index == INDEX_EVDO_ONLY
                    || index == INDEX_CDMA_EVDO)) {
                return true;
            } else if (mSimCard.getCardType() == CARD_TYPE_CDMA_ONLY) {
                return false;
            }
            // for GSM only card, remove the cdma items
            else if (mSimCard.getCardType() == CARD_TYPE_GSM_ONLY
                    && (index == INDEX_CDMA_ONLY
                    || index == INDEX_EVDO_ONLY
                    || index == INDEX_CDMA_EVDO
                    || index == INDEX_LTE_CDMA_EVDO
                    || index == INDEX_LTE_CDMA_EVDO_UMTS_GSM
                    || index == INDEX_CDMA_EVDO_UMTS_GSM)) {
                return false;
            }

            // for GSM_CDMA double card
            // for MODEM_TD(tdscdma), remove the WCDMA items
            if (mModemType == ModemCategory.MODEM_TD
                    && (index == INDEX_WCDMA_PREFERRED || index == INDEX_WCDMA_ONLY
                    || index == INDEX_GSM_WCDMA_AUTO)) {
                return false;
            }
            // for MODEM_FDD(Wcdma), remove the TDSCDMA items
            if (mModemType == ModemCategory.MODEM_FDD
                    && (index == INDEX_TDSCDMA_ONLY || index == INDEX_GSM_TDSCDMA_AUTO)) {
                return false;
            }

            // for None LTE project,remove all of lte items
            if (!ModemCategory.isLteSupport()
                    && (index == INDEX_LTE_ONLY || index == INDEX_LTE_GSM_UMTS
                    || index == INDEX_LTE_UMTS
                    || index == INDEX_LTE_CDMA_EVDO_UMTS_GSM
                    || index == INDEX_LTE_CDMA_EVDO)) {
                return false;
            }
            // for not support wcdma_preferred project,remove the
            // wcdma_preferred item
            if (!FeatureSupport.isSupported(FeatureSupport.FK_WCDMA_PREFERRED)
                    && index == INDEX_WCDMA_PREFERRED) {
                return false;
            }

            return true;
        } else { // for sub card

            // for cdma only card, remove all of items except cdma only item
            if (mSimCard.getCardType() == CARD_TYPE_CDMA_ONLY
                    && index == INDEX_CDMA_ONLY) {
                return true;
            } else if (mSimCard.getCardType() == CARD_TYPE_CDMA_ONLY) {
                return false;
            }
            // for GSM only card, remove the cdma items
            else if (mSimCard.getCardType() == CARD_TYPE_GSM_ONLY
                    && (index == INDEX_CDMA_ONLY
                    || index == INDEX_EVDO_ONLY
                    || index == INDEX_CDMA_EVDO
                    || index == INDEX_LTE_CDMA_EVDO
                    || index == INDEX_LTE_CDMA_EVDO_UMTS_GSM
                    || index == INDEX_CDMA_EVDO_UMTS_GSM)) {
                return false;
            }
            // for GSM_CDMA double card

            //support L+L LTE
            if (mSimCard.isLteCapabilityViceSim()) {
                if (index == INDEX_LTE_ONLY || index == INDEX_LTE_GSM_UMTS
                        || index == INDEX_LTE_UMTS || index == INDEX_LTE_CDMA_EVDO_UMTS_GSM
                        || index == INDEX_LTE_CDMA_EVDO) {
                    return true;
                }
            } else {
                if (index == INDEX_LTE_ONLY || index == INDEX_LTE_GSM_UMTS
                        || index == INDEX_LTE_UMTS || index == INDEX_LTE_CDMA_EVDO_UMTS_GSM
                        || index == INDEX_LTE_CDMA_EVDO) {
                    return false;
                }
            }
            //supoort W
            if (mSimCard.isWCapabilityViceSim()) {
                if (index == INDEX_TDSCDMA_ONLY || index == INDEX_GSM_TDSCDMA_AUTO) {
                    return false;
                }
                if (!FeatureSupport.isSupported(FeatureSupport.FK_WCDMA_PREFERRED)
                        && index == INDEX_WCDMA_PREFERRED) {
                    return false;
                }
                return true;
            } else {
                if (index == INDEX_GSM_ONLY || index == INDEX_CDMA_ONLY
                        || index == INDEX_CDMA_EVDO || index == INDEX_CDMA_EVDO_UMTS_GSM) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    private void handleQueryCdmaCapability() {

        String enabled = SystemProperties.get(FK_MTK_C2K_CAPABILITY, "0");

        if ("1".equals(enabled)) {
            Elog.d(TAG, "QueryCdmaCapability set true");
            mFirstEntry = true;
            mCbDisableC2kCapabilit.setChecked(true);
        } else {
            Elog.d(TAG, "QueryCdmaCapability set false");
            mFirstEntry = false;
            mCbDisableC2kCapabilit.setChecked(false);
        }
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        AlertDialog.Builder builder = null;
        switch (id) {
            case REBOOT_DIALOG:
                return new AlertDialog.Builder(this).setTitle(
                        "Disable c2k capability").setMessage("Please reboot the phone!")
                        .setPositiveButton("OK", null).create();
        }
        return dialog;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub
        if (buttonView.getId() == R.id.disable_c2k_capability) {
            if (mFirstEntry == true) {
                mFirstEntry = false;
                return;
            }
            try {
                if (mCbDisableC2kCapabilit.isChecked()) {
                    EmUtils.getEmHidlService().setDisableC2kCap("1");
                } else {
                    EmUtils.getEmHidlService().setDisableC2kCap("0");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Elog.e(TAG, "set property failed ...");
            }
            showDialog(REBOOT_DIALOG);
        } else if (buttonView.getId() == R.id.disable_eHRPD) {
            if (mEHRPDFirstEnter == true) {
                mEHRPDFirstEnter = false;
                return;
            }
            mDisableeHRPDCheckBox.setChecked(isChecked);
            seteHRPDStatus(isChecked ? 0 : 1);
        }
    }

    private void queryeHRPDStatus() {
        String[] cmd = new String[3];
        cmd[0] = "AT+eHRPD?";
        cmd[1] = "+EHRPD:";
        cmd[2] = "DESTRILD:C2K";
        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);

        sendAtCommand(cmd_s, EVENT_QUERY_EHRPD_ENABLE_DONE);
    }

    private void seteHRPDStatus(int state) {
        String atCommand = null;
        switch (state) {
            case 0:
                atCommand = "AT+eHRPD=0";
                break;
            case 1:
                atCommand = "AT+eHRPD=1";
                break;
        }

        String[] cmd = new String[3];
        cmd[0] = atCommand;
        cmd[1] = "";
        cmd[2] = "DESTRILD:C2K";
        String[] cmd_s = ModemCategory.getCdmaCmdArr(cmd);

        sendAtCommand(cmd_s, EVENT_SET_EHRPD_ENABLE_DONE);
    }

    private void sendAtCommand(String[] command, int msg) {
        Elog.d(TAG, "sendAtCommand: " + command[0]);
        EmUtils.invokeOemRilRequestStringsEm(mSimType, command, mHandler.obtainMessage(msg));
    }

    /**
     * Adapter.
     */
    public class CustomAdapter extends ArrayAdapter<String> {
        /**
         * Constructor.
         *
         * @param context            Context
         * @param textViewResourceId Resource id
         * @param objects            Objects
         */
        public CustomAdapter(Context context, int textViewResourceId, String[] objects) {
            super(context, textViewResourceId, objects);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View v = null;
            Elog.d(TAG, "isAvailable: " + position + " is " + isAvailable(position));
            if (!isAvailable(position)) {
                TextView tv = new TextView(getContext());
                tv.setVisibility(View.GONE);
                tv.setHeight(0);
                v = tv;
            } else {
                v = super.getDropDownView(position, null, parent);
            }
            return v;
        }
    }

    public class SimCardInfo {
        private int mSimType = PhoneConstants.SIM_ID_1;
        private boolean isCapabilitySim = ModemCategory.isCapabilitySim(this.mSimType);
        private boolean isWCapabilityViceSim = ModemCategory.CheckViceSimWCapability(this.mSimType);
        private boolean isLteCapabilityViceSim = ModemCategory.checkViceSimCapability(
                mSimType, RadioAccessFamily.RAF_LTE);
        private int mCardType = CARD_TYPE_GSM_ONLY;
        ;

        public SimCardInfo(int mSimType) {
            super();
            setSimType(mSimType);
        }

        public int getCardType() {
            return mCardType;
        }

        private void setCardType(int mCardType) {
            this.mCardType = mCardType;
        }

        public boolean isLteCapabilityViceSim() {
            return isLteCapabilityViceSim;
        }

        private void setLteCapabilityViceSim(boolean isLteCapabilityViceSim) {
            this.isLteCapabilityViceSim = isLteCapabilityViceSim;
        }

        public boolean isWCapabilityViceSim() {
            return isWCapabilityViceSim;
        }

        private void setWCapabilityViceSim(boolean isWCapabilityViceSim) {
            this.isWCapabilityViceSim = isWCapabilityViceSim;
        }

        public int getSimType() {
            return mSimType;
        }

        public void setSimType(int mSimType) {
            this.mSimType = mSimType;
            setCapabilitySim(ModemCategory.isCapabilitySim(mSimType));
            setWCapabilityViceSim(ModemCategory.CheckViceSimWCapability(this.mSimType));
            setLteCapabilityViceSim(ModemCategory.checkViceSimCapability(
                    mSimType, RadioAccessFamily.RAF_LTE));
            setCardType(getCardType(mSimType)); // card type:1: GSM only 2:GSM_CDMA 3:CDMA only
            Elog.d(TAG, "card type = " + mCardTypeValues[this.mCardType - 1]);

        }

        public boolean isCapabilitySim() {
            return this.isCapabilitySim;
        }

        private void setCapabilitySim(boolean isCapabilitySim) {
            this.isCapabilitySim = isCapabilitySim;
        }

        private int getCardType(int mSimType) {
            int type = CARD_TYPE_GSM_ONLY;
            String[] Cardtype = ModemCategory.getSupportCardType(mSimType);
            if (Cardtype != null) {
                boolean iscCt3gDualMode = ModemCategory.isCt3gDualMode(mSimType);
                String Cardtypes = "";
                for (int i = 0; i < Cardtype.length; i++) {
                    Cardtypes += Cardtype[i] + " ";
                }
                Elog.d(TAG, "SimCardType = " + Cardtypes + ",iscCt3gDualMode = " + iscCt3gDualMode);
                if ((Cardtypes.contains("USIM") && Cardtypes.contains("CSIM"))
                        || iscCt3gDualMode == true) {
                    type = CARD_TYPE_GSM_CDMA;
                } else if (Cardtypes.contains("USIM") && Cardtypes.contains("RUIM")) {
                    type = CARD_TYPE_GSM_CDMA;
                } else if (Cardtypes.contains("CSIM") && Cardtypes.contains("SIM")) {
                    type = CARD_TYPE_GSM_CDMA;
                } else if (!Cardtypes.contains("RUIM") && !Cardtypes.contains("CSIM")) {
                    type = CARD_TYPE_GSM_ONLY;
                } else if (!(Cardtypes.contains("SIM") && !Cardtypes.contains("USIM"))
                        && iscCt3gDualMode == false) {
                    type = CARD_TYPE_CDMA_ONLY;
                } else
                    Elog.w(TAG, "the card type is unknow!");
            } else
                Elog.w(TAG, "there has no card insert,default type is GSM_only");
            return type;
        }
    }

}
