# Copyright Statement:
#
# This software/firmware and related documentation ("MediaTek Software") are
# protected under relevant copyright laws. The information contained herein
# is confidential and proprietary to MediaTek Inc. and/or its licensors.
# Without the prior written permission of MediaTek inc. and/or its licensors,
# any reproduction, modification, use or disclosure of MediaTek Software,
# and information contained herein, in whole or in part, shall be strictly prohibited.

# MediaTek Inc. (C) 2010. All rights reserved.
#
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
# THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
# RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
# AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
# NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
# SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
# CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
# CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
# AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
# MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
# The following software/firmware and/or related documentation ("MediaTek Software")
# have been modified by MediaTek Inc. All revisions are subject to any receiver's
# applicable license agreements with MediaTek Inc.

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_PACKAGE_NAME := EngineerMode
LOCAL_PRIVATE_PLATFORM_APIS := true
LOCAL_PRIVILEGED_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_CERTIFICATE := platform
LOCAL_PROGUARD_ENABLED := disabled
LOCAL_MODULE_TAGS := optional
LOCAL_STATIC_JAVA_LIBRARIES := android-support-v4
LOCAL_JAVA_LIBRARIES += telephony-common ims-common mediatek-common
LOCAL_SRC_FILES := $(call all-java-files-under, src)

JAVA_SRC_DIR := src/com/mediatek/engineermode

ifeq ($(MTK_EM_AOSP_FW_SUPPORT), no)

# Files only for eng/user_debug load
ifeq ($(TARGET_BUILD_VARIANT), user)

    ENG_ONLY_TEL_FILE := $(call all-java-files-under, $(JAVA_SRC_DIR)/amrwb) \
                         $(call all-java-files-under, $(JAVA_SRC_DIR)/sbp) \
                         $(call all-java-files-under, $(JAVA_SRC_DIR)/u3phy) \
                         $(call all-java-files-under, $(JAVA_SRC_DIR)/simswitch) \
                         $(call all-java-files-under, $(JAVA_SRC_DIR)/modemfilter) \
                         $(call all-java-files-under, $(JAVA_SRC_DIR)/iatype) \

    LOCAL_SRC_FILES := $(filter-out $(ENG_ONLY_TEL_FILE), $(LOCAL_SRC_FILES))

    ENG_ONLY_HARDWARE_FILE := $(call all-java-files-under, $(JAVA_SRC_DIR)/desense) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/io) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/memory) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/power) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/touchscreen) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/usb) \
                              $(JAVA_SRC_DIR)/UartUsbSwitch.java
    LOCAL_SRC_FILES := $(filter-out $(ENG_ONLY_HARDWARE_FILE), $(LOCAL_SRC_FILES))

    ENG_ONLY_LOCATION_FILE := $(call all-java-files-under, $(JAVA_SRC_DIR)/cwtest) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/desenseat) \
                              $(call all-java-files-under, $(JAVA_SRC_DIR)/clkqualityat)
    LOCAL_SRC_FILES := $(filter-out $(ENG_ONLY_LOCATION_FILE), $(LOCAL_SRC_FILES))

    ENG_ONLY_LOG_DEBUG_FILE := $(JAVA_SRC_DIR)/BatteryLog.java \
                               $(call all-java-files-under, $(JAVA_SRC_DIR)/modemwarning) \
                               $(call all-java-files-under, $(JAVA_SRC_DIR)/modemresetdelay)
    LOCAL_SRC_FILES := $(filter-out $(ENG_ONLY_LOG_DEBUG_FILE), $(LOCAL_SRC_FILES))

    ENG_ONLY_OTHERS_FILE := $(call all-java-files-under, $(JAVA_SRC_DIR)/swla) \
                            $(call all-java-files-under, $(JAVA_SRC_DIR)/usbacm) \
                            $(call all-java-files-under, $(JAVA_SRC_DIR)/spc)
    LOCAL_SRC_FILES := $(filter-out $(ENG_ONLY_OTHERS_FILE), $(LOCAL_SRC_FILES))
endif


MTK_FRAMEWORK_LOCATION_FILE  :=  $(call all-java-files-under, $(JAVA_SRC_DIR)/worldmode_aosp)
LOCAL_SRC_FILES := $(filter-out $(MTK_FRAMEWORK_LOCATION_FILE), $(LOCAL_SRC_FILES))

LOCAL_STATIC_JAVA_LIBRARIES += \
    com.mediatek.mdml \
    com.mediatek.mdml com.mediatek.icdcodec \
    vendor.mediatek.hardware.netdagent-V1.0-java \
    vendor.mediatek.hardware.engineermode-V1.1-java \
    vendor.mediatek.hardware.lbs-V1.0-java \
    wfo-common \

LOCAL_JAVA_LIBRARIES += mediatek-framework mediatek-telephony-base mediatek-telephony-common mediatek-telecom-common
LOCAL_JNI_SHARED_LIBRARIES := libicdcodec_jni
endif


ifneq ($(MTK_EM_AOSP_FW_SUPPORT), no)
MTK_ENGINEERMODE_APP := yes
MTK_CUSTOM_USERLOAD_ENGINEERMODE := yes
BOARD_USES_MTK_AUDIO := true
MTK_WLAN_SUPPORT := yes
MTK_MCF_SUPPORT := yes
MTK_AUDIO_TUNING_TOOL_VERSION := V2.2
MTK_FM_SUPPORT := yes
MTK_AGPS_APP := yes
MTK_GPS_SUPPORT := yes

LOCAL_STATIC_JAVA_LIBRARIES += \
    vendor.mediatek.hardware.engineermode-V1.1-java \
    vendor.mediatek.hardware.lbs-V1.0-java \
    vendor.mediatek.hardware.netdagent-V1.0-java \
    wfo-common \
    com.mediatek.mdml

MTK_FRAMEWORK_LOCATION_FILE := $(call all-java-files-under, $(JAVA_SRC_DIR)/worldphone) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/siminfo) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/iatype) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/rttn) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/apc) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/networkinfo) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/cfu) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/worldmode)\
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/tellogsetting) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/voicesettings) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/wfdsettings) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/mdmdiagnosticinfo) \
                                $(call all-java-files-under, $(JAVA_SRC_DIR)/iotconfig) \
                                $(JAVA_SRC_DIR)/GPRS.java


LOCAL_SRC_FILES := $(filter-out $(MTK_FRAMEWORK_LOCATION_FILE), $(LOCAL_SRC_FILES))
endif

include $(BUILD_PACKAGE)

include $(call all-makefiles-under,$(LOCAL_PATH))

