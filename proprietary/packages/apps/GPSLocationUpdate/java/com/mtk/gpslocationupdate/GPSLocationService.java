package com.mediatek.gpslocationupdate;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import com.mediatek.apmonitor.*;

public class GPSLocationService extends Service {
    private float mDistance = 0;
    private float mAttimuth = 0;
    private float mDopplerSpeed = 0;
    private LocationManager mLocationManager = null;
    private static final String TAG = "GPSLocationService";
    private ApmNative mNative = new ApmNative();
    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        // TODO Auto-generated method stub
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO Auto-generated method stub
        Log.d(TAG, "onStartCommand");
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocationManager == null) {
            Log.d(TAG, "ERR: mLocationManager is null");
        }
        startGps();
        return super.onStartCommand(intent, flags, startId);
    }

    private void startGps() {
        Log.d(TAG, "startGps");

        if (getApplicationContext().checkSelfPermission(
            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "No GPS permission");
            return;
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0,
                                                mLocationListener);
        Log.d(TAG, "Registered");
    }

    private void stopGps() {
        Log.d(TAG, "stopGps");

        if (getApplicationContext().checkSelfPermission(
            Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
            getApplicationContext().checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "No GPS permission");
            return;
        }
        mLocationManager.removeUpdates(mLocationListener);
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            if (location == null) {
                Log.d(TAG, "ERR: onLocationChanged get null");
            } else {
                float distance, atimuth, speed;
                distance = location.getAccuracy();
                atimuth = location.getBearing();
                speed = location.getSpeed();

                Log.d(TAG, "onLocationChanged: mDistance = " + mDistance +
                                          ", mAttimuth = " + mAttimuth +
                                          ", mDopplerSpeed = " + mDopplerSpeed);

                if (mDistance != distance && mAttimuth != atimuth && mDopplerSpeed != speed) {
                    if (mNative.isKpiEnabled(ApmNative.KPI_TYPE_GPS_LOCATION_INFO)) {
                        ApmNative.ApmGpsLocationInfo obj = mNative.new ApmGpsLocationInfo();
                        obj.distance = distance;
                        obj.atimuth = atimuth;
                        obj.dopplerSpeed = speed;
                        mNative.send(ApmNative.KPI_TYPE_GPS_LOCATION_INFO, obj);
                        mDistance = distance;
                        mAttimuth = atimuth;
                        mDopplerSpeed = speed;
                     }
                }
           }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }

        @Override
        public void onProviderEnabled(String provider) {
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    };
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        stopGps();
        super.onDestroy();
    }
}
