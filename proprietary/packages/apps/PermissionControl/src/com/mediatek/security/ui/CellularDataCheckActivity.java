package com.mediatek.security.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AppOpsManager;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mediatek.security.R;
import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.service.CellularDataCheckHelper;
import com.mediatek.security.service.PermControlUtils;

public class CellularDataCheckActivity extends Activity implements
        View.OnClickListener {
    protected String TAG = "CellularDataCheckActivity";
    private static String TAG_ONE = "CellularDataCheckActivity";
    private Button mDenyBT;
    private Button mGrantBT;
    private CellularDataCheckHelper mCellularDataCheckHelper;
    private final IBinder mToken = new Binder();
    private static String mCurrentCheckPkg;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFinishOnTouchOutside(false);
        setContentView((ManualLayoutFrame) LayoutInflater.from(this).inflate(
                R.layout.notify_activity_customview, null));
        Intent intent = getIntent();
        if (intent == null) {
            Log.e(TAG, "null intent");
            finish();
            return;
        }
        String pkgName = intent.getStringExtra(PermControlUtils.PACKAGE_NAME);
        String permName = intent
                .getStringExtra(PermControlUtils.PERMISSION_NAME);
        int uid = intent.getIntExtra(PermControlUtils.UID, -1);
        if (pkgName == null || permName == null || uid == -1) {
            Log.e(TAG, "invalue extras");
            finish();
            return;
        }
        mCellularDataCheckHelper = new CellularDataCheckHelper(this,
                new CheckedPermRecord(pkgName, permName, CheckedPermRecord.STATUS_FIRST_CHECK), uid);

        TextView messageText = (TextView) findViewById(R.id.message);
        mDenyBT = (Button) findViewById(R.id.permission_deny_button);
        mGrantBT = (Button) findViewById(R.id.permission_allow_button);
        mDenyBT.setOnClickListener(this);
        mGrantBT.setOnClickListener(this);
        String label = PermControlUtils.getApplicationName(this, pkgName);
        String msg = getString(R.string.notify_dialog_msg_body, label, permName);
        messageText.setText(msg);
        TextView promptText = (TextView) findViewById(R.id.prompt);
        promptText.setText(R.string.prompt);
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
        setOverlayAllowed(false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        setOverlayAllowed(true);
    }

    private void setOverlayAllowed(boolean allowed) {
        AppOpsManager appOpsManager = getSystemService(AppOpsManager.class);
        if (appOpsManager != null) {
            appOpsManager.setUserRestriction(AppOpsManager.OP_SYSTEM_ALERT_WINDOW,
                    !allowed, mToken);
        }
    }

    @Override
    public void finish() {
        finishCheck();
        super.finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onStop() {
        finishCheck();
        super.onStop();
    }

    @Override
    public void onClick(View arg0) {
        boolean enable = false;
        if (arg0.getId() == R.id.permission_allow_button) {
            enable = true;
        }
        mCellularDataCheckHelper.handleClick(enable);
        Log.d(TAG, "onClick " + " enable = " + enable);
        finish();
    }

    protected void finishCheck() {
        synchronized(CellularDataCheckActivity.class) {
            if (mCurrentCheckPkg != null) {
                Log.d(TAG, "finishCheck remove + " + mCurrentCheckPkg);
                mCurrentCheckPkg = null;
            } else {
                Log.e(TAG, "finishCheck error + " + mCurrentCheckPkg);
            }
        }
    }

    public synchronized static boolean canCheck(String pkgName) {
        boolean can = false;
        if (mCurrentCheckPkg == null) {
            mCurrentCheckPkg = pkgName;
            Log.d(TAG_ONE, "canCheck, add " + pkgName);
            can = true;
        }
        Log.d(TAG_ONE, "canCheck " + can);
        return can;
    }

    public synchronized static String getCueerntPkg() {
        return mCurrentCheckPkg == null?null:new String(mCurrentCheckPkg);
    }
}
