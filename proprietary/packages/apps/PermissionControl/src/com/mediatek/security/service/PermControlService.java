package com.mediatek.security.service;

import android.app.ActivityManager;
import android.app.ActivityManager.AppTask;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.ActivityManagerNative;
import android.app.ActivityThread;
import android.app.IAppTask;
import android.app.AlertDialog;
import android.app.Service;
import android.app.StatusBarManager;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.ServiceManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserHandle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Pair;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.net.INetdEventCallback;

import com.mediatek.net.connectivity.IMtkIpConnectivityMetrics;
import com.mediatek.security.R;
import com.mediatek.security.datamanager.CheckedPermRecord;
import com.mediatek.security.datamanager.DatabaseManager;
import com.mediatek.security.datamanager.DatabaseManager.InitLevel;
import com.mediatek.security.ui.CellularDataCheckActivity;
import com.mediatek.security.ui.CellularDataCheckActivityTwo;
import com.mediatek.security.ui.AutoBootManager;

import java.util.ArrayList;
import java.util.List;

public class PermControlService extends AsyncService implements
        OnClickListener, OnDismissListener, OnShowListener {

    public PermControlService() {
        super("PermControlService");
    }

    private static final int MSG_RESET = 101;
    private static final int MSG_SHOW_CONF_DLG = MSG_RESET + 2;
    private static final int EXTRA_TIMER = 5000;
    private AlertDialog mAlertDlg;
    private CellularDataCheckHelper mCellularDataCheckHelper;
    private Object mUserConfirmLock = new Object();

    private static final String TAG = "PermControlService";
    private boolean mIsRegisted = false;

    private static final String CELLULAR_DATA_REQUEST = "com.mediatek.network.socketconn";
    private volatile boolean mPendingCellular = false;
    private IMtkIpConnectivityMetrics mIpConnectivityMetrics;
    private BroadcastReceiver mPmControlReceiver = new PermControlReceiver();
    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                String action = intent.getAction();
                if (CELLULAR_DATA_REQUEST.equals(action)) {
                    if (PermControlUtils.isCellularDataControlOn(context)
                        &&(!mPendingCellular)) {
                         int uid = intent
                          .getIntExtra(PermControlUtils.UID, -1);
                        Log.d(TAG, "onReceive, action = " + action
                            + "uid = " + uid);
                        mPendingCellular = true;
                        handleMessage(MSG_INTENT, intent);
                    }
                }
            }
        }
    };

    private INetdEventCallback mNetdEventCallback = new INetdEventCallback.Stub(){
        private void handleNetdEvent(int uid) {
            Log.i(TAG, "handleNetdEvent uid:" + uid + "mPendingCellular " + mPendingCellular);
            if (!mPendingCellular) {
                Intent intent = new Intent();
                intent.setAction(CELLULAR_DATA_REQUEST);
                intent.putExtra(PermControlUtils.UID, uid);
                sendBroadcast(intent);
                PermControlUtils.updateCtaAppStatus(uid,false);
            }
        }
        @Override
        public void onDnsEvent(String hostname, String[] ipAddress, int ipAddressesCount,
               long timeStamp, int uid){
            Log.i(TAG, "onDnsEvent uid:" + uid);
            handleNetdEvent(uid);
        }

        @Override
        public void onConnectEvent(String ipAddr, int port, long timestamp, int uid) {
            Log.i(TAG, "onConnectEvent uid:" + uid);
            handleNetdEvent(uid);
        }

        @Override
        public void onPrivateDnsValidationEvent(int uid, String ipAddress, String hostname,
            boolean validated) {
            Log.i(TAG, "onPrivateDnsValidationEvent uid:" + uid);
        }
    };

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MSG_SHOW_CONF_DLG) {
                handleConfirmDlgMsg((CheckedPermRecord )msg.obj);
            } else {
                Log.e("@M_" + TAG, "handleMessage unkown:" + msg.what);
            }
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("@M_" + TAG, "onCreate()");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    // for increase the adj of process so set service in foreground
    private void setServiceInforeground() {
        startForeground(PermControlUtils.NOTIFY_FOREGROUND_ID,
                PermControlUtils.getNotification(this));
    }

    private void setStatusBarEnableStatus(boolean enabled) {
        Log.i("@M_" + TAG, "setStatusBarEnableStatus(" + enabled + ")");
        StatusBarManager statusBarManager;
        statusBarManager = (StatusBarManager) getSystemService(Context.STATUS_BAR_SERVICE);
        if (statusBarManager != null) {
            if (enabled) {
                statusBarManager.disable(StatusBarManager.DISABLE_NONE);
            } else {
                statusBarManager.disable(StatusBarManager.DISABLE_EXPAND
                        | StatusBarManager.DISABLE_RECENT
                        | StatusBarManager.DISABLE_HOME);
            }
        } else {
            Log.e("@M_" + TAG, "Fail to get status bar instance");
        }
    }

    @Override
    protected void onHandleMessage(Message message) {
        switch (message.what) {
        case MSG_INTENT:
            Intent intent = (Intent) message.obj;
            if (intent != null) {
                String action = intent.getAction();
                Log.d(TAG, "onHandleMessage() action = " + action);
                if (action == null) {
                    initService();
                } else if (CELLULAR_DATA_REQUEST.equals(action)) {
                    handleCellularDataRequest(intent);
                } else if (PermControlUtils.ACTION_PACKAGE_UPDATE.equals(action)){
                    String pkg = intent.getStringExtra(PermControlUtils.PACKAGE_NAME);
                    boolean isAdd = intent
                          .getBooleanExtra(PermControlReceiver.IS_PACKAGE_ADD, false);
                    Log.d(TAG, "ACTION_PACKAGE_UPDATE, pkg = " + pkg
                            + "isAdd = " + isAdd);

                    updateAutoBootList(this,pkg);
                    sendAutoBootUpdateBroadcast(pkg);
                    initDatabaseManager(InitLevel.CELLULAR_DATA_OFF);
                    if (isAdd) {
                        DatabaseManager.add(PermControlService.this, pkg);
                    } else {
                        DatabaseManager.delete(pkg);
                    }
                    sendCellularUpdateBroadcast(pkg);
                }
            } else {
                // Handle if service is killed
                Log.d("@M_" + TAG,
                        "intent = null servie is killed and relaunched by system");
                initService();
            }
            break;
        default:
            Log.e("@M_" + TAG, "do not know the message " + message.what);
            initService();
        }
    }

    private void updateAutoBootList(Context context,String pkgName) {
        AutoBootManager BootManager = null;
        if (pkgName != null && pkgName.length() != 0) {
             CheckedPermRecord info = new CheckedPermRecord(pkgName, "",
             true);
             BootManager = AutoBootManager.getInstance(context, UserHandle.myUserId());
             if (BootManager != null && !BootManager.mIsUsed ) {
                 if(BootManager.getBootList() == null){
                     BootManager.initAutoBootList();
                 }
                 BootManager.setPgkBootPolicy(info);
                 BootManager.saveToFile();

              }
         }
    }
    @Override
    public synchronized void onDestroy() {
        super.onDestroy();
        Log.d("@M_" + TAG, "onDestroy");
        stopForeground(true);
        if (mIsRegisted) {
            unregisterReceiver(mReceiver);
            unregisterReceiver(mPmControlReceiver);
            mIsRegisted = false;
        }
        setStatusBarEnableStatus(true);
        if (mIpConnectivityMetrics != null) {
            try {
                mIpConnectivityMetrics.unregisterMtkNetdEventCallback();
            } catch (RemoteException e){
                e.printStackTrace();
            }
        }
        DatabaseManager.deInitDataBase();
    }
    private IMtkIpConnectivityMetrics getIMtkIpConnectivityMetrics() {
        return IMtkIpConnectivityMetrics.Stub.asInterface(
        ServiceManager.getService("mtkconnmetrics"));
    }

    private synchronized void initService() {
        Log.d("@M_" + TAG, "initService");
        if(PermControlUtils
                .isCellularDataControlOn(this)){
            initDatabaseManager(InitLevel.CELLULAR_DATA_ON);
            try {
                mIpConnectivityMetrics = getIMtkIpConnectivityMetrics();
                //register callback
                mIpConnectivityMetrics.registerMtkNetdEventCallback(mNetdEventCallback);
            } catch (RemoteException e){
                e.printStackTrace();
            }
        }
        PermControlUtils.createNotificationChannel(this);
        setServiceInforeground();
        registerReceiver();
    }

    private synchronized void initDatabaseManager(InitLevel level) {
        DatabaseManager.initDataBase(this, level);
        sendCellularUpdateBroadcast(null);
    }

    private void registerReceiver() {
        if (!mIsRegisted) {
            IntentFilter intentFilter = new IntentFilter();
            intentFilter.addAction(CELLULAR_DATA_REQUEST);
            registerReceiver(mReceiver, intentFilter);
            IntentFilter intentFilterPm = new IntentFilter();
            intentFilterPm.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_AVAILABLE);
            intentFilterPm.addAction(Intent.ACTION_EXTERNAL_APPLICATIONS_UNAVAILABLE);
            intentFilterPm.addAction(Intent.ACTION_PACKAGE_REMOVED);
            intentFilterPm.addAction(Intent.ACTION_PACKAGE_ADDED);
            intentFilterPm.addDataScheme("package");
            registerReceiver(mPmControlReceiver, intentFilterPm);
            mIsRegisted = true;
        }
    }
    private void sendAutoBootUpdateBroadcast(String pkg) {
        Intent intent = new Intent(PermControlUtils.PERM_CONTROL_AUTOBOOT_UPDATE);
        if (pkg != null) {
            intent.putExtra(PermControlUtils.PACKAGE_NAME, pkg);
        }
        sendBroadcast(intent);
    }
    private void sendCellularUpdateBroadcast(String pkg) {
        Intent intent = new Intent(PermControlUtils.CELLULAR_DATA_UPDATE);
        if (pkg != null) {
            intent.putExtra(PermControlUtils.PACKAGE_NAME, pkg);
        }
        sendBroadcast(intent);
    }

    /*
     * Because the system dialog need to show in main thread of service so show
     * the dialog via a handler
     */
    private void showConfirmDlg(CheckedPermRecord record) {
        Message msg = Message.obtain();
        msg.what = MSG_SHOW_CONF_DLG;
        msg.obj = record;
        mHandler.sendMessage(msg);
    }

    /*
     * Synchronized the function of handleCheckCase, whenever one permission
     * confirm thread hold the lock other permission thread need to wait
     * previous release otherwise wait
     */
    private synchronized void handleCheckCase(CheckedPermRecord record,
            int uid ) {
        Log.d("@M_" + TAG, "handleCheckCase(), mPendingCellular=" + mPendingCellular);
        synchronized (mUserConfirmLock) {
            try {
                mCellularDataCheckHelper =
                        new CellularDataCheckHelper(this, record, uid);
                showConfirmDlg(record);
                // add extra timer as the time counter is not accurate, in some
                // case the lock
                // may wake up before time counter up to 20s
                mUserConfirmLock.wait();
                Log.d("@M_" + TAG, "release the lock");
            } catch (InterruptedException e) {
                Log.d("@M_" + TAG, "error");
            }
        }
    }

    private Pair<String, String> getCurrentActivity() {
        final String SYSTEM_UI = "com.android.systemui";
        final String LAUNCH3 = "com.android.launcher3";
        String activeActivity = "";
        String visibleActivity = "";
        ActivityManager am = getSystemService(ActivityManager.class);
        if (am != null) {
            List<String> foregroundPkgs = new ArrayList<String>();
            List<RunningAppProcessInfo> infos = am.getRunningAppProcesses();
            if (infos != null) {
                for (RunningAppProcessInfo info : infos) {
                    if (info.importance == RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                        if (info.pkgList != null) {
                            for (String pkg : info.pkgList) {
                                foregroundPkgs.add(pkg);
                            }
                        }
                    }
                }
            }

            List<RunningTaskInfo> tasks = am.getRunningTasks(50);
            if (tasks != null) {
                for (RunningTaskInfo info : tasks) {
                    if (info.baseActivity == null) {
                        continue;
                    }
                    String pgkName = info.baseActivity.getPackageName();
                    if (pgkName.equals(SYSTEM_UI) || pgkName.equals(LAUNCH3)) {
                        continue;
                    }
                    if (foregroundPkgs.contains(pgkName)) {
                        if (activeActivity.length() == 0) {
                            activeActivity = pgkName;
                        } else if (visibleActivity.length() == 0){
                            visibleActivity = pgkName;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        StringBuilder builder = new StringBuilder();
        builder.append("getCurrentActivity, foreground:");
        builder.append(activeActivity);
        builder.append(", ");
        builder.append(", visible:");
        builder.append(visibleActivity);
        builder.append(", ");
        Log.d(TAG, builder.toString());
        return new Pair<String, String>(activeActivity, visibleActivity);
    }

    private void handleCellularDataRequest(Intent intent) {
        try {
            int uid = intent.getIntExtra(PermControlUtils.UID, -1);
            String packageName = null;
            Log.d(TAG, "handleCellularDataRequest");
            PackageManager pm = getPackageManager();
            String[] packages = pm.getPackagesForUid(uid);
            int length = packages == null ? 0 : packages.length;
            for (int i = 0; i < length; i++) {
                try {
                    PackageInfo info = pm.getPackageInfo(packages[i],
                            PackageManager.GET_PERMISSIONS);

                    packageName = info.packageName;
                    if (!(packageName == null || packageName.length() == 0)) {
                        break;
                    }
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
            }
            if (packageName == null || packageName.length() == 0) {
                Log.e(TAG, "can't get package name for uid " + uid);
                return;
            }

            CheckedPermRecord record = DatabaseManager
                    .getCellularDataRecord(packageName);
            if (record == null
                    || record.getStatus() == CheckedPermRecord.STATUS_DENIED
                    || record.getStatus() == CheckedPermRecord.STATUS_GRANTED) {

                if(record == null) {
                    Log.e(TAG, "record == null");
                } else {
                    Log.e(TAG, "status == " + record.getStatus() );
                }
                return;
            }

            if (packageName.equals(CellularDataCheckActivity.getCueerntPkg())
                    || packageName.equals(CellularDataCheckActivityTwo
                            .getCueerntPkgTwo())) {
                return;
            }

            Pair<String, String> activites = getCurrentActivity();
            if (activites.first.length() != 0 && activites.second.length() != 0) {
                Intent intent1 = new Intent();
                intent1.putExtra(PermControlUtils.PACKAGE_NAME,
                        record.mPackageName);
                String permission = record.mPermission;
                if (record.mPermissionId != -1) {
                    permission = getString(record.mPermissionId);
                }
                intent1.putExtra(PermControlUtils.PERMISSION_NAME,
                        permission);
                intent1.putExtra(PermControlUtils.UID, uid);
                if (CellularDataCheckActivity.canCheck(packageName)) {
                    intent1.setAction("com.mediatek.security.CHECK_CELLULAR_DATA");
                } else if (CellularDataCheckActivityTwo
                        .canCheckTwo(packageName)) {
                    intent1.setAction("com.mediatek.security.CHECK_CELLULAR_DATA_TWO");
                } else {
                    return;
                }
                if (packageName.equals(activites.first)) {
                    startActivity(intent1);
                } else if (packageName.equals(activites.second)) {
                    intent1.addFlags(Intent.FLAG_ACTIVITY_LAUNCH_ADJACENT);
                    startActivity(intent1);
                } else {
                    handleCheckCase(record, uid);
                }
            } else {
                handleCheckCase(record, uid);
            }
        } finally {
            mPendingCellular = false;
        }
    }

    /**
     * true for grant and false for deny
     *
     * @param status
     *            enable or not the permission
     */
    public void releaseLock() {
        synchronized (mUserConfirmLock) {
            mUserConfirmLock.notifyAll();
        }
    }

    /**
     * Show a system confirm dialog from service
     *
     * @param record
     *            the PermissionRecord data type
     * @param flag
     *            the flag of the PermissionRecord
     */
    private void handleConfirmDlgMsg(CheckedPermRecord record) {
        Log.d("@M_" + TAG, "Show confirm dialog");
        ContextThemeWrapper themeWrapper = new ContextThemeWrapper(this,
                R.style.PermissionDialog);
        Context context = themeWrapper;

        Builder builder = new AlertDialog.Builder(context);
        //builder.setTitle(R.string.notify_dialog_title);
        builder.setPositiveButton(R.string.accept_perm, this);
        builder.setNegativeButton(R.string.deny_perm, this);
        builder.setCancelable(false);

        LayoutInflater inflater = LayoutInflater.from(context);
        final View view = inflater.inflate(R.layout.notify_dialog_customview, null);
        builder.setView(view);

        TextView messageText = (TextView) view.findViewById(R.id.message);

        String label = PermControlUtils.getApplicationName(this,
                record.mPackageName);
        String permission = record.mPermission;
        if (record.mPermissionId != -1) {
            permission = getString(record.mPermissionId);
        }
        String msg = getString(R.string.notify_dialog_msg_body, label,
                permission);
        messageText.setText(msg);
        TextView promptText = (TextView) view.findViewById(R.id.prompt);
        promptText.setText(R.string.prompt);
        mAlertDlg = builder.create();
        mAlertDlg.setOnDismissListener(this);
        mAlertDlg.setOnShowListener(this);
        mAlertDlg.getWindow().setType(
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        setStatusBarEnableStatus(false);

        mAlertDlg.show();
        //updateCount(COUNT_DOWN_TIMER);
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        boolean enable = false;
        if (which == DialogInterface.BUTTON_POSITIVE) {
            enable = true;
        } else if (which == DialogInterface.BUTTON_NEGATIVE) {
            enable = false;
        }
        mCellularDataCheckHelper.handleClick(enable);
        Log.d("@M_" + TAG,
                "Click dialog button with check box " + " enable = " + enable);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        Log.d("@M_" + TAG, "Dialog dimissed");
        setStatusBarEnableStatus(true);
        releaseLock();
    }

    @Override
    public void onShow(DialogInterface dialo) {
        Log.e(TAG, "onShow, mPendingCellular = " + mPendingCellular);
        if (mPendingCellular) {
            AlertDialog dialog = (AlertDialog) dialo;
            final Button negBT = dialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            final Button posBT = dialog.getButton(DialogInterface.BUTTON_POSITIVE);

            negBT.setClickable(false);
            posBT.setClickable(false);

            final long time = 500;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    SystemClock.sleep(time);
                    negBT.setClickable(true);
                    posBT.setClickable(true);
                }
            });
            t.start();
        }
    }
}
