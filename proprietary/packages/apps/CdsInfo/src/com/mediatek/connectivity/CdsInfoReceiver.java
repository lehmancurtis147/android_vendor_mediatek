/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mediatek.connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;



public class CdsInfoReceiver extends BroadcastReceiver {
    private static final String TAG = "CdsInfoReceiver";

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        mContext = context;

        if (action != null) {
            Log.d(TAG, "action:" + action);
        }

        if (Intent.ACTION_BOOT_COMPLETED.equals(action)) {
            checkEmbmsSrv();
        } else if (action.equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
            boolean enabled = intent.getBooleanExtra("state", false);
            if (!enabled) {
                checkEmbmsSrv();
            }
        } else {
            Log.e(TAG, "Received unknown intent: " + action);
        }
    }

    private void checkEmbmsSrv() {
        SharedPreferences dataStore = mContext.getSharedPreferences(CdsEmbmsConsts.EMBMS_FILE, 0);
        boolean isEmbmsAuto = dataStore.getBoolean(CdsEmbmsConsts.EMBMS_AUTO_ENABLED, false);

        if (isEmbmsAuto) {
            Log.i(TAG, "checkEmbmsSrv");
            Intent i = new Intent(mContext, CdsEmbmsService.class);
            i.putExtra(CdsEmbmsConsts.BOOT, true);
            mContext.startService(i);
        }
    }

}