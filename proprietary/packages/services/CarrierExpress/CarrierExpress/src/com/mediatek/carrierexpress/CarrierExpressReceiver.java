package com.mediatek.carrierexpress;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemProperties;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.android.internal.telephony.IccCardConstants;
import com.android.internal.telephony.PhoneConstants;

public class CarrierExpressReceiver extends BroadcastReceiver {
    private final String TAG = "CarrierExpressServiceImpl";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "CarrierExpressReceiver, onReceiver(), action=" + action);

        if ("no".equals(SystemProperties.get("ro.vendor.mtk_carrierexpress_pack", "no"))) {
            Log.i(TAG, "Carrier Express not supported !");
            return;
        }

        if ("android.telephony.action.SIM_APPLICATION_STATE_CHANGED".equals(action)) {
            // Try to start CXP service process when SIM application state changed
            intent.setClass(context, CarrierExpressApp.class);
            context.startService(intent);
        } else if ("android.intent.action.USER_SWITCHED".equals(action)) {
            CarrierExpressServiceImpl.mSystemUserId
                = intent.getIntExtra(Intent.EXTRA_USER_HANDLE, -1);
            Intent cxp_intnet = new Intent(CarrierExpressServiceImpl.CXP_INTERNAL_USER_SWITCHED);
            context.sendBroadcast(cxp_intnet);
            Log.d(TAG, "CarrierExpressReceiver, USER SWITCHED, current user ID: "
                + CarrierExpressServiceImpl.mSystemUserId);
        } else if ("android.intent.action.BOOT_COMPLETED".equals(action)) {
            intent.setClass(context, CarrierExpressApp.class);
            context.startService(intent);
        }
    }
}

