/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2015. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

package com.mediatek.wfo.impl;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationRequest;
import android.location.Geocoder;

import android.net.ConnectivityManager;
import android.net.Network;
import android.net.Uri;

import android.os.AsyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemProperties;

import android.provider.Settings;
import android.telephony.Rlog;
import android.telephony.PhoneStateListener;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.lang.System;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.android.internal.telephony.TelephonyIntents;
import com.mediatek.internal.telephony.MtkPhoneConstants;

import com.mediatek.wfo.ril.MwiRIL;


public class WfcLocationHandler extends Handler {
    private static final String TAG = "WfcLocationHandler";
    private static final int MAX_VALID_SIM_COUNT = 4;

    // Geo Location: the payload defined as LocationInfo and handled in handleLocationInfo().
    private static final int MSG_REG_IMSA_REQUEST_GEO_LOCATION_INFO = 96009;
    private static final int MSG_REG_IMSA_RESPONSE_GETO_LOCATION_INFO = 91030;
    private static int MAX_NUM_OF_GET_LOCATION_TASKS_THREAD = 3;
    private static int REQUEST_GEOLOCATION_FROM_NETWORK_TIMEOUT = 60000;

    /* Event Message definition for hanlder */
    private static final int BASE = 3000;
    public static final int EVENT_GET_LOCATION_REQUEST                 = BASE + 0;
    private static final int EVENT_GET_LAST_KNOWN_LOCATION             = BASE + 1;
    private static final int EVENT_HANDLE_NETWORK_LOCATION_RESPONSE    = BASE + 2;
    private static final int EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE = BASE + 3;
    private static final int EVENT_SET_LOCATION_INFO                   = BASE + 4;
    private static final int EVENT_SET_COUNTRY_CODE                    = BASE + 5;
    private static final int EVENT_DIALING_E911                        = BASE + 6;
    private static final int EVENT_RETRY_GET_LOCATION_REQUEST          = BASE + 7;
    private static final int EVENT_ALL_RETRY_GET_LOCATION_REQUST       = BASE + 8;
    private static final int EVENT_LOCATION_MODE_CHANGED               = BASE + 9;

    private static final int RESPONSE_SET_LOCATION_INFO                = BASE + 100;

    private static int NETWORK_LOCATION_UPDATE_TIME = 5000;

    private static final String ACTION_ECC_IN_PROGRESS = "android.intent.action.ECC_IN_PROGRESS";

    private Context mContext;
    private int mSimCount;
    private TelephonyManager mTelephonyManager;
    private CallStateListener mCallStateListener = new CallStateListener();

    private ArrayList<LocationInfo> mLocationInfoQueue = new ArrayList<LocationInfo>();

    private ArrayList<LocationInfo> mNetworkLocationTasks = new ArrayList<LocationInfo>();
    private LocationManager mLocationManager;
    private Geocoder mGeoCoder;
    private LocationListenerImp mLocationListener = new LocationListenerImp();


    // For prevent set the same country code to MD.
    private String mPlmnCountryCode = "";

    private String mWifiMacAddr = "";

    private WfcHandler mWfcHandler;
    private WifiPdnHandler mWifiPdnHandler;
    private MwiRIL[] mMwiRil;

    private boolean mIsEccInProgresss = false;

    private boolean mNetworkAvailable = false;

    private static final int REQUEST_LOCATION_RETRY_TIMEOUT = 5000;
    private ArrayList <Message> mPendingLocationRequest = new ArrayList<>();
    private Object mLocationRequestLock = new Object();

    private boolean mLocationTimeout = false;
    private Object mLocationTimeoutLock = new Object();

    // log task
    private static final String PROP_FORCE_DEBUG_KEY = "persist.vendor.log.tel_dbg";
    private static final boolean USR_BUILD = TextUtils.equals(Build.TYPE, "user")
            || TextUtils.equals(Build.TYPE, "userdebug");
    private static final boolean TELDBG = (SystemProperties.getInt(PROP_FORCE_DEBUG_KEY, 0) == 1);

    class CallStateListener extends PhoneStateListener {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            log("onCallStateChanged state=" + state + " incomingNumber=" + incomingNumber);
            if (state == TelephonyManager.CALL_STATE_OFFHOOK && mIsEccInProgresss) {
                log("E911 is dialing");
                if (!mWifiPdnHandler.isWifiConnected()) {
                    log("Wi-Fi isn't connected");
                    return;
                }
                obtainMessage(EVENT_DIALING_E911).sendToTarget();
            }
        }
    }

    private class LocationListenerImp implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            log("onLocationChanged: " + location);
            cancelNetworkGeoLocationRequest();
            synchronized (mLocationTimeoutLock) {
                mLocationTimeout = false;
            }
            // Remove get last known location if callback within 10s
            log("removeMessages: EVENT_GET_LAST_KNOWN_LOCATION");
            removeMessages(EVENT_GET_LAST_KNOWN_LOCATION);

            obtainMessage(EVENT_HANDLE_NETWORK_LOCATION_RESPONSE, 0, 0, location)
                .sendToTarget();
        }

        @Override
        public void onProviderDisabled(String provider) {
            log("onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            log("onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            log("onStatusChanged: " + provider + ", status=" + status);
        }
    }

    public class LocationInfo {
        public int mSimIdx;
        public int mAccountId;
        public int mBroadcastFlag;

        public double mLatitude;
        public double mLongitude;
        public double mAccuracy;
        public String mMethod = "";
        public String mCity = "";
        public String mState = "";
        public String mZip = "";
        public String mCountryCode = "";

        LocationInfo(int simIdx, int accountId,
                     int broadcastFlag, double latitude, double longitude, double accuracy) {
            mSimIdx = simIdx;
            mAccountId = accountId;
            mBroadcastFlag = broadcastFlag;
            mLatitude = latitude;
            mLongitude = longitude;
            mAccuracy = accuracy;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("[LocationInfo objId: ");
            sb.append(System.identityHashCode(this));
            sb.append(", phoneId: " + mSimIdx);
            sb.append(", transationId: " + mAccountId);
            sb.append(", latitude: " + mLatitude);
            sb.append(", longitude: " + mLongitude);
            sb.append(", accuracy: " + mAccuracy);
            sb.append(", method: " + mMethod);
            sb.append(", city: " + mCity);
            sb.append(", state: " + mState);
            sb.append(", zip: " + mZip);
            sb.append(", countryCode: " + mCountryCode);
            return sb.toString();
        }
    }

    @Override
    public void handleMessage(Message msg) {
        LocationInfo info;

        log("handleMessage: msg= " + messageToString(msg));
        switch (msg.what) {
            case EVENT_GET_LOCATION_REQUEST:
                if (!mWifiPdnHandler.isWifiConnected() && !mNetworkAvailable) {
                    log("Wi-Fi isn't connected and network unavailable.");
                    addRetryLocationRequest(msg);
                    return;
                }
                handleLocationRequest(msg);
                break;
            case EVENT_RETRY_GET_LOCATION_REQUEST:
                handleRetryLocationRequest(msg);
                break;
            case EVENT_ALL_RETRY_GET_LOCATION_REQUST:
                handleAllRetryLocationRequest();
                break;
            case EVENT_HANDLE_NETWORK_LOCATION_RESPONSE:
            case EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE: {
                Location location = (Location) msg.obj;
                handleNetworkLocationUpdate(location);
                break;
            }
            case EVENT_SET_LOCATION_INFO: {
                LocationInfo locationInfo = (LocationInfo) msg.obj;
                setLocationInfo(locationInfo);
                break;
            }
            case EVENT_SET_COUNTRY_CODE:
                String iso = (String) msg.obj;
                if (!TextUtils.isEmpty(iso)) {
                    if (TextUtils.isEmpty(mPlmnCountryCode)) {
                        // First camp on network, set country to MD
                        setCountryCode(iso);
                    } else if (!TextUtils.isEmpty(iso) && !iso.equals(mPlmnCountryCode) &&
                            mWifiPdnHandler.isWifiConnected()) {
                        // Country code changed, query network location
                        dispatchLocationRequest(new LocationInfo(0, 0, 0, 0, 0, 0));
                    }
                    mPlmnCountryCode = iso;
                }
                break;
            case EVENT_LOCATION_MODE_CHANGED: //fall through
            case EVENT_DIALING_E911:
                // Try to get network location and update to MD
                dispatchLocationRequest(new LocationInfo(0, 0, 0, 0, 0, 0));
                break;
            case EVENT_GET_LAST_KNOWN_LOCATION:
                synchronized (mLocationTimeoutLock) {
                    mLocationTimeout = true;
                }
                if (mWfcHandler != null) {
                    mWfcHandler.onLocationTimeout();
                } else {
                    Rlog.e(TAG, "EVENT_GET_LAST_KNOWN_LOCATION: WfcHandler null");
                }
                info = (LocationInfo) msg.obj;
                getLastKnownLocation(info);
                cancelNetworkGeoLocationRequest();
                break;
            case RESPONSE_SET_LOCATION_INFO:
                // Do nothing
                break;
            default:
                break;
        }
    }

    private String messageToString(Message msg) {
        switch (msg.what) {

            case EVENT_GET_LOCATION_REQUEST:
                return "EVENT_GET_LOCATION_REQUEST";
            case EVENT_HANDLE_NETWORK_LOCATION_RESPONSE:
                return "EVENT_HANDLE_NETWORK_LOCATION_RESPONSE";
            case EVENT_SET_LOCATION_INFO:
                return "EVENT_SET_LOCATION_INFO";
            case EVENT_SET_COUNTRY_CODE:
                return "EVENT_SET_COUNTRY_CODE";
            case EVENT_GET_LAST_KNOWN_LOCATION:
                return "EVENT_GET_LAST_KNOWN_LOCATION";
            case EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE:
                return "EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE";
            case EVENT_DIALING_E911:
                return "EVENT_DIALING_E911";
            case RESPONSE_SET_LOCATION_INFO:
                return "RESPONSE_SET_LOCATION_INFO";
            case EVENT_RETRY_GET_LOCATION_REQUEST:
                return "EVENT_RETRY_GET_LOCATION_REQUEST";
            case EVENT_ALL_RETRY_GET_LOCATION_REQUST:
                return "EVENT_ALL_RETRY_GET_LOCATION_REQUST";
            case EVENT_LOCATION_MODE_CHANGED:
                return "EVENT_LOCATION_MODE_CHANGED";
            default:
                return "UNKNOWN";
        }
    }

    public Handler getHandler() {
        return this;
    }

    public WfcLocationHandler(Context context, WfcHandler wfcHandler,
        WifiPdnHandler wifiPdnHandler, int simCount, Looper looper, MwiRIL[] mwiRil) {

        super(looper);
        mContext = context;
        mWfcHandler = wfcHandler;
        mWifiPdnHandler = wifiPdnHandler;
        mSimCount = (simCount <= MAX_VALID_SIM_COUNT) ? simCount : MAX_VALID_SIM_COUNT;
        mMwiRil = mwiRil;

        mGeoCoder = new Geocoder(mContext, Locale.US);
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        mTelephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        mTelephonyManager.listen(mCallStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        registerForBroadcast();
        registerIndication();
        registerDefaultNetwork();
    }

    protected void log(String s) {
        if (!USR_BUILD || TELDBG) {
            Rlog.d(TAG, s);
        }
    }

    private void addRetryLocationRequest(Message msg) {
        synchronized(mLocationRequestLock) {
            AsyncResult ar = (AsyncResult) msg.obj;
            String[] result = (String[]) ar.result;
            String[] retryRet = new String[6];
            String[] delayRet = new String[6];
            System.arraycopy(result, 0, retryRet, 0, result.length);
            System.arraycopy(result, 0, delayRet, 0, result.length);
            AsyncResult retryAr = new AsyncResult(null, retryRet, null);
            AsyncResult delayAr = new AsyncResult(null, delayRet, null);

            Message retryMsg = obtainMessage(EVENT_RETRY_GET_LOCATION_REQUEST, retryAr);
            mPendingLocationRequest.add(retryMsg);
            log("add, current PendingLocationRequest size: " + mPendingLocationRequest.size());
            Message delayMsg = obtainMessage(EVENT_RETRY_GET_LOCATION_REQUEST, delayAr);
            sendMessageDelayed(delayMsg, REQUEST_LOCATION_RETRY_TIMEOUT);
        }
    }

    private void handleRetryLocationRequest(Message msg) {
        synchronized(mLocationRequestLock) {
            log("Current PendingLocationRequest size: " + mPendingLocationRequest.size());
            if (mWifiPdnHandler.isWifiConnected() || mNetworkAvailable) {
                handleLocationRequest(msg);
            } else {
                log("Network not available, ignore EVENT_RETRY_GET_LOCATION_REQUEST.");
            }
        }
    }

    private void handleAllRetryLocationRequest() {
        log("handleAllRetryLocationRequest mPendingLocationRequest.size(): " + mPendingLocationRequest.size());
        synchronized(mLocationRequestLock) {
            if (hasMessages(EVENT_RETRY_GET_LOCATION_REQUEST)) {
                removeMessages(EVENT_RETRY_GET_LOCATION_REQUEST);
            }
            for (Message msg : mPendingLocationRequest) {
                if (mWifiPdnHandler.isWifiConnected() || mNetworkAvailable) {
                    handleLocationRequest(msg);
                } else {
                    log("Network not available, ignore EVENT_RETRY_GET_LOCATION_REQUEST.");
                }
            }
            mPendingLocationRequest.clear();
        }
    }

    private void registerDefaultNetwork() {
        ConnectivityManager cm = ConnectivityManager.from(mContext);
        cm.registerDefaultNetworkCallback(new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                log("NetworkCallback.onAvailable()");
                mNetworkAvailable = true;
                sendEmptyMessage(EVENT_ALL_RETRY_GET_LOCATION_REQUST);
            }

            @Override
            public void onLost(Network network) {
                log("NetworkCallback.onLost()");
                mNetworkAvailable = false;
            }
        });
    }


    /** Geo Location **/
    private void handleLocationRequest(Message msg) {
        if (null == msg.obj) {
            Rlog.e(TAG, "handleLocationInfo(): msg.obj is null");
            return;
        }
        AsyncResult ar = (AsyncResult) msg.obj;
        String[] result = (String[]) ar.result;

        if (null == result) {
            Rlog.e(TAG, "handleLocationInfo(): result is null");
            return;
        }

        try {
            int accId = Integer.parseInt(result[0]);
            int broadcastFlag = Integer.parseInt(result[1]);
            double latitude = Double.parseDouble(result[2]);
            double longitude = Double.parseDouble(result[3]);
            double accuracy = Double.parseDouble(result[4]);
            int simIdx = Integer.parseInt(result[5]);


            LocationInfo locationInfo = new LocationInfo(
                simIdx, accId, broadcastFlag, latitude, longitude, accuracy);

            log("handleGeoLocationRequest(): " + locationInfo);
            dispatchLocationRequest(locationInfo);
        } catch (Exception e) {
            log("handleGeoLocationRequest()[" + result.length + "]" + result[0] + " " +
                result[1] + " " +result[2] + " " + result[3] + " " + result[4] + " " + result[5]);
        }

    }

    private void dispatchLocationRequest(LocationInfo info) {
        double latitude = info.mLatitude;
        double longitude = info.mLongitude;
        double accuracy = info.mAccuracy;
        // if no gps signal, use Wifi location
        if (latitude == 0 && longitude == 0 && accuracy == 0) {
            proccessLocationFromNetwork(info);
        } else {
            cancelNetworkGeoLocationRequest();
            synchronized (mLocationTimeoutLock) {
                mLocationTimeout = false;
            }
            log("removeMessages: EVENT_GET_LAST_KNOWN_LOCATION");
            removeMessages(EVENT_GET_LAST_KNOWN_LOCATION);
            info.mMethod = "GPS";
            mLocationInfoQueue.add(info);
            pollLocationInfo();
        }
        log("dispatchLocationRequest(): " + info.mMethod);
    }

    private void handleNetworkLocationUpdate(Location location) {
        if (location == null) {
            log("network location get null, unexpected result");
            return;
        }

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        log("update all LocationInfo with (" + latitude + ", " + longitude + ")");

        ArrayList<LocationInfo> duplicatedInfo = new ArrayList<LocationInfo>();
        for (LocationInfo locationInfo : mNetworkLocationTasks) {
            locationInfo.mLatitude = latitude;
            locationInfo.mLongitude = longitude;
            locationInfo.mAccuracy = 1.0;
            log("Get network location, send EVENT_MSG_REQUEST_GEO_LOCATION for"
                + " transationId-" + locationInfo.mAccountId);
            duplicatedInfo.clear();

            for (LocationInfo gpsLocationInfo : mLocationInfoQueue) {
                if (gpsLocationInfo.mAccountId == locationInfo.mAccountId) {
                    duplicatedInfo.add(gpsLocationInfo);
                }
            }
            for (LocationInfo gpsLocationInfo : duplicatedInfo) {
                mLocationInfoQueue.remove(gpsLocationInfo);
            }
            mLocationInfoQueue.add(locationInfo);
        }

        pollLocationInfo();
        mNetworkLocationTasks.clear();
    }

    private void proccessLocationFromNetwork(LocationInfo info) {
        info.mMethod = "Network";
        mNetworkLocationTasks.add(info);
        if (!requestGeoLocationFromNetworkLocation()) {
            mNetworkLocationTasks.remove(info);
            log("requestGeoLocationFromNetworkLocation failed");
            // Only country code is valid, set to MD
            setLocationInfo(info);
        }
        if (!hasMessages(EVENT_GET_LAST_KNOWN_LOCATION)) {
            // Trying to get last known location if no locationChanges callback after 10s
            log("Add 10s delayed message: EVENT_GET_LAST_KNOWN_LOCATION");
            sendMessageDelayed(obtainMessage(EVENT_GET_LAST_KNOWN_LOCATION, 0, 0, info),
                                            REQUEST_GEOLOCATION_FROM_NETWORK_TIMEOUT);
        }
    }

    private void pollLocationInfo() {
        if (mLocationInfoQueue.isEmpty()) {
            log("No GeoLocation task");
            return;
        }

        // Avoid race condition
        List<LocationInfo> LocationInfoQueueCopy = new ArrayList<LocationInfo>(mLocationInfoQueue);
        mLocationInfoQueue.clear();

        new Thread(new Runnable() {
            @Override
            public void run() {
                for (LocationInfo gpsLocationInfo : LocationInfoQueueCopy) {
                    LocationInfo res = getGeoLocationFromLatLong(gpsLocationInfo);
                    obtainMessage(EVENT_SET_LOCATION_INFO, 0, 0, res).sendToTarget();
                }
            }
        }).start();

    }

    private void setLocationInfo(LocationInfo info) {
        if (TextUtils.isEmpty(info.mState)) {
            info.mState = "Unknown";
        }
        // Use lastest PLMN country code
        if (!TextUtils.isEmpty(mPlmnCountryCode) && TextUtils.length(info.mCountryCode) != 2) {
            info.mCountryCode = mPlmnCountryCode;
        } else if (TextUtils.length(info.mCountryCode) == 2) {
            // Update country code from location
            mPlmnCountryCode = info.mCountryCode;
        }
        log("setLocationInfo info=" + info + ", mPlmnCountryCode:" + mPlmnCountryCode);
        log("setLocationInfo getUeWlanMacAddr=" + WifiPdnHandler.getUeWlanMacAddr());

        Message result = obtainMessage(RESPONSE_SET_LOCATION_INFO);
        getMwiRil().setLocationInfo(
            Integer.toString(info.mAccountId),
            Integer.toString(info.mBroadcastFlag),
            String.valueOf(info.mLatitude),
            String.valueOf(info.mLongitude),
            String.valueOf(info.mAccuracy),
            info.mMethod,
            info.mCity,
            info.mState,
            info.mZip,
            info.mCountryCode,
            WifiPdnHandler.getUeWlanMacAddr(), result);
        pollLocationInfo();
    }
    /**
     * This function will be executed in worker thread.
     */
    private LocationInfo getGeoLocationFromLatLong(LocationInfo location) {
        if (mGeoCoder == null) {
            log("getGeoLocationFromLatLong: empty geoCoder, return an empty location");
            return location;
        }

        if (!mGeoCoder.isPresent()) {
            log("getGeoLocationFromLatLong: this system has no GeoCoder implementation!!");
            return location;
        }

        double lat = location.mLatitude;
        double lng = location.mLongitude;

        List<Address> lstAddress = null;
        try {
            lstAddress = mGeoCoder.getFromLocation(lat, lng, 1);
        } catch (IOException e) {
            log("mGeoCoder.getFromLocation throw exception:" + e);
        } catch (Exception e2) {
            log("mGeoCoder.getFromLocation throw exception:" + e2);
        }

        if (lstAddress == null || lstAddress.isEmpty()) {
            log("getGeoLocationFromLatLong: get empty address");
            return location;
        }

        location.mCity = lstAddress.get(0).getLocality();
        if (TextUtils.isEmpty(location.mCity)) {
            location.mCity = lstAddress.get(0).getSubAdminArea();
        }
        location.mState = lstAddress.get(0).getAdminArea();  // 'CA';
        if (TextUtils.isEmpty(location.mState)) {
            location.mState = lstAddress.get(0).getCountryName();
        }
        location.mZip = lstAddress.get(0).getPostalCode();
        location.mCountryCode = lstAddress.get(0).getCountryCode();

        log("getGeoLocationFromLatLong: location=" + location);
        return location;
    }

    private boolean getLastKnownLocation(LocationInfo info) {
        log("getLastKnownLocation");

        if (mLocationManager == null) {
            log("getLastKnownLocation: empty locationManager, return");
            return false;
        }

        // Avoid "provider doesn't exist: network" JE
        if (mLocationManager.getProvider(LocationManager.GPS_PROVIDER) == null) {
            log("getGeoLocationFromNetworkLocation: GPS_PROVIDER doesn't exist or not ready");
            return false;
        }

        // Get cached GPS location
        Location gpsLocation =
            mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        if (gpsLocation != null) {

            log("GPS location: " + gpsLocation);
            if (System.currentTimeMillis() - gpsLocation.getTime() < 1800000) {
                obtainMessage(
                        EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE, 0, 0, gpsLocation)
                        .sendToTarget();
                return true;
            }
        }

        // Avoid "provider doesn't exist: network" JE
        if (mLocationManager.getProvider(LocationManager.NETWORK_PROVIDER) == null) {
            log("getGeoLocationFromNetworkLocation: NETWORK_PROVIDER doesn't exist or not ready");
            return false;
        }


        // Get cached network location
        Location networkLocation =
            mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (networkLocation != null) {

            log("Network location: " + networkLocation);
            if (System.currentTimeMillis() - networkLocation.getTime() < 1800000) {
                obtainMessage(
                        EVENT_HANDLE_LAST_KNOWN_LOCATION_RESPONSE, 0, 0, networkLocation)
                        .sendToTarget();
                return true;
            }
        }

        log("getLastKnownLocation: no last known location");
        // Maybe only country code is valid, set to MD
        setLocationInfo(info);
        return false;
    }

    private boolean requestGeoLocationFromNetworkLocation() {
        log("requestGeoLocationFromNetworkLocation");
        if (mLocationManager == null) {
            log("getGeoLocationFromNetworkLocation: empty locationManager, return");
            return false;
        }

        // Avoid "provider doesn't exist: network" JE
        if (mLocationManager.getProvider(LocationManager.NETWORK_PROVIDER) == null) {
            log("getGeoLocationFromNetworkLocation: provider doesn't exist or not ready yet");
            return false;
        }

        // No nned check provider because wfo is in whitelist
        //mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        LocationRequest request = LocationRequest.createFromDeprecatedProvider(
                LocationManager.NETWORK_PROVIDER, NETWORK_LOCATION_UPDATE_TIME /*minTime*/,
                0 /*minDistance*/, false/*oneShot*/);
        request.setHideFromAppOps(true);
        mLocationManager.requestLocationUpdates(request, mLocationListener, this.getLooper());
        log("requestGeoLocationFromNetworkLocation: request networkLocation update");

        return true;
    }

    private void cancelNetworkGeoLocationRequest() {
        log("cancelNetworkGeoLocationRequest");
        if (mLocationManager == null) {
            log("cancelNetworkGeoLocationRequest: empty locationManager, return");
            return;
        }
        mLocationManager.removeUpdates(mLocationListener);
    }

    private void utGeoLocationRequest() {
        LocationInfo locationInfo = new LocationInfo(
            0, 8, 0, 212.0, 147.0, 1);
        dispatchLocationRequest(locationInfo);
    }

    private void utNetworkLocationRequest() {
        LocationInfo locationInfo = new LocationInfo(
            0, 8, 0, 0.0, 0.0, 0.0);
        dispatchLocationRequest(locationInfo);
    }

    /**
     * to get main capability phone id.
     *
     * @return The phone id with highest capability.
     */
    private int getMainCapabilityPhoneId() {
        int phoneId = SystemProperties.getInt(MtkPhoneConstants.PROPERTY_CAPABILITY_SWITCH, 1) - 1;
        if (phoneId < 0 || phoneId >= TelephonyManager.getDefault().getPhoneCount()) {
            phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        }
        log("getMainCapabilityPhoneId = " + phoneId);
        return phoneId;
    }

    private MwiRIL getMwiRil() {
        int phoneId = getMainCapabilityPhoneId();

        if (phoneId == SubscriptionManager.INVALID_PHONE_INDEX) {
            return null;
        } else {
            return mMwiRil[phoneId];
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent == null || intent.getAction() == null) {
                return;
            }
            log("onReceive action:" + intent.getAction());
            if (intent.getAction().equals(TelephonyIntents.ACTION_LOCATED_PLMN_CHANGED)) {
                String lowerCaseCountryCode = (String) intent.getExtra(TelephonyIntents.EXTRA_ISO);
                if (lowerCaseCountryCode != null) {
                    String isoCountryCode = lowerCaseCountryCode.toUpperCase();
                    log("ACTION_LOCATED_PLMN_CHANGED, iso: " + isoCountryCode);
                    obtainMessage(EVENT_SET_COUNTRY_CODE, isoCountryCode).sendToTarget();
                } else {
                    log("iso country code is null");
                }
            } else if (intent.getAction().equals(ACTION_ECC_IN_PROGRESS)) {
                mIsEccInProgresss = intent.getBooleanExtra("in_progress", false);
                log("ACTION_ECC_IN_PROGRESS mIsEccInProgresss: " + mIsEccInProgresss);
            } else if (intent.getAction().equals(LocationManager.MODE_CHANGED_ACTION)) {
                boolean isNlpEnabled
                        = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                synchronized (mLocationTimeoutLock) {
                    log("LocationManager.MODE_CHANGED_ACTION isNlpEnabled: " + isNlpEnabled
                            + ", location timeout = " + mLocationTimeout);
                    if (mLocationTimeout && isNlpEnabled) {
                        obtainMessage(EVENT_LOCATION_MODE_CHANGED).sendToTarget();
                    }
                }
            }
        }
    };

    private void setCountryCode(String iso) {
        LocationInfo info = new LocationInfo(0, 0, 0, 0, 0, 0);
        info.mCountryCode = iso;
        setLocationInfo(info);
    }

    private void registerForBroadcast() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(TelephonyIntents.ACTION_LOCATED_PLMN_CHANGED);
        filter.addAction(ACTION_ECC_IN_PROGRESS);
        filter.addAction(LocationManager.MODE_CHANGED_ACTION);
        mContext.registerReceiver(mReceiver, filter);
    }

    private void registerIndication() {

        for(int i = 0 ; i < mSimCount ; i++) {
            mMwiRil[i].registerRequestGeoLocation(this,
                    EVENT_GET_LOCATION_REQUEST, null);
        }
    }
}
