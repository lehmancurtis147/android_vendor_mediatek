/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */
package com.mediatek.settings;

import android.content.Context;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.provider.Settings;
import android.telecom.TelecomManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.android.internal.telephony.Phone;
import com.android.internal.telephony.PhoneConstants;
import com.android.internal.telephony.PhoneFactory;
import com.android.phone.PhoneGlobals;
import com.android.phone.PhoneUtils;

import com.mediatek.ims.internal.MtkImsManager;
import com.mediatek.internal.telephony.IMtkTelephonyEx;
import com.mediatek.internal.telephony.MtkGsmCdmaPhone;
import com.mediatek.internal.telephony.MtkPhoneConstants;
import com.mediatek.internal.telephony.RadioCapabilitySwitchUtil;
import com.mediatek.phone.ext.ExtensionManager;
import com.mediatek.phone.PhoneFeatureConstants.FeatureOption;
import com.mediatek.settings.CallSettingUtils;
import com.mediatek.settings.cdma.TelephonyUtilsEx;
import com.mediatek.telephony.MtkTelephonyManagerEx;

import java.util.List;

import mediatek.telephony.MtkCarrierConfigManager;

/**
 * Telephony utils class.
 */
public class TelephonyUtils {
    private static final String TAG = "TelephonyUtils";
    public static final String USIM = "USIM";

    private final static String ONE = "1";
    public static final String ACTION_NETWORK_CHANGED =
            "com.mediatek.intent.action.ACTION_NETWORK_CHANGED";

    /**
     * Check if the subscription card is USIM or SIM.
     * @param context using for query phone
     * @param subId according to the phone
     * @return true if is USIM card
     */
    public static boolean isUSIMCard(Context context, int subId) {
        log("isUSIMCard()... subId = " + subId);
        String type = MtkTelephonyManagerEx.getDefault().getIccCardType(subId);
        log("isUSIMCard()... type = " + type);
        return USIM.equals(type);
    }

    /**
     * Check if the sim state is ready or not.
     * @param slot sim slot
     * @return true if sim state is ready.
     */
    public static boolean isSimStateReady(int slot) {
        boolean isSimStateReady = false;
        isSimStateReady = TelephonyManager.SIM_STATE_READY == TelephonyManager.
                getDefault().getSimState(slot);
        log("isSimStateReady: "  + isSimStateReady);
        return isSimStateReady;
    }

    /**
     * Check if all radio is off.
     * @param context Context
     * @return true if all radio on
     */
    public static boolean isAllRadioOff(Context context) {
        boolean result = true;
        boolean airplaneModeOn = isAirplaneModeOn(context);
        int subId;
        List<SubscriptionInfo> activeSubList = PhoneUtils.getActiveSubInfoList();
        for (int i = 0; i < activeSubList.size(); i++) {
            subId = activeSubList.get(i).getSubscriptionId();
            if (isRadioOn(subId, context)) {
                result = false;
                break;
            }
        }
        return result || airplaneModeOn;
    }

    /**
     * check the radio is on or off by sub id.
     *
     * @param subId the sub id
     * @param context Context
     * @return true if radio on
     */
    public static boolean isRadioOn(int subId, Context context) {
        log("[isRadioOn]subId:" + subId);
        boolean isRadioOn = false;
        final ITelephony iTel = ITelephony.Stub.asInterface(
                ServiceManager.getService(Context.TELEPHONY_SERVICE));
        if (iTel != null && PhoneUtils.isValidSubId(subId)) {
            try {
                isRadioOn = iTel.isRadioOnForSubscriber(subId, context.getPackageName());
            } catch (RemoteException e) {
                log("[isRadioOn] failed to get radio state for sub " + subId);
                isRadioOn = false;
            }
        } else {
            log("[isRadioOn]failed to check radio");
        }
        log("[isRadioOn]isRadioOn:" + isRadioOn);

        return isRadioOn && !isAirplaneModeOn(PhoneGlobals.getInstance());
    }

    /**
     * Return whether the project is Gemini or not.
     * @return If Gemini, return true, else return false
     */
    public static boolean isGeminiProject() {
        boolean isGemini = TelephonyManager.getDefault().isMultiSimEnabled();
        log("isGeminiProject : " + isGemini);
        return isGemini;
    }

    /**
     * Add for [MTK_Enhanced4GLTE].
     * Get the phone is inCall or not.
     * @param context Context
     * @return true if in call
     */
    public static boolean isInCall(Context context) {
        TelecomManager manager = (TelecomManager) context.getSystemService(
                Context.TELECOM_SERVICE);
        boolean inCall = false;
        if (manager != null) {
            inCall = manager.isInCall();
        }
        log("[isInCall] = " + inCall);
        return inCall;
    }

    /**
     * Add for [MTK_Enhanced4GLTE].
     * Get the phone is inCall or not.
     * @param context Context
     * @return true if airplane mode on
     */
    public static boolean isAirplaneModeOn(Context context) {
        return Settings.Global.getInt(context.getContentResolver(),
                Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
    }

    ///M: Add for [VoLTE_SS] @{
    /**
     * Get whether the IMS is IN_SERVICE.
     * @param context Context
     * @param subId the sub which one user selected.
     * @return true if the ImsPhone is IN_SERVICE, else false.
     */
    public static boolean isImsServiceAvailable(Context context, int subId) {
        boolean available = false;
        if (PhoneUtils.isValidSubId(subId)) {
             available = MtkTelephonyManagerEx.getDefault().isImsRegistered(subId);
        }
        log("isImsServiceAvailable[" + subId + "], available = " + available);
        return available;
    }

    /**
     * Return whether the phone is hot swap or not.
     * @param originaList old subscription info list
     * @param currentList current subscription info list
     * @return If hot swap, return true, else return false
     */
    public static boolean isHotSwapHanppened(List<SubscriptionInfo> originaList,
            List<SubscriptionInfo> currentList) {
        boolean result = false;
        if (originaList.size() != currentList.size()) {
            return true;
        }
        for (int i = 0; i < currentList.size(); i++) {
            SubscriptionInfo currentSubInfo = currentList.get(i);
            SubscriptionInfo originalSubInfo = originaList.get(i);
            if (!(currentSubInfo.getIccId()).equals(originalSubInfo.getIccId())) {
                result = true;
                break;
            } else {
                result = false;
            }
        }

        log("isHotSwapHanppened : " + result);
        return result;
    }

    /**
     * Return whether the project is support WCDMA Preferred.
     * @return If support, return true, else return false
     */
    public static boolean isWCDMAPreferredSupport() {
        String isWCDMAPreferred = SystemProperties.get("ro.vendor.mtk_rat_wcdma_preferred");
        if (TextUtils.isEmpty(isWCDMAPreferred)) {
            log("isWCDMAPreferredSupport : false; isWCDMAPreferred is empty. ");
            return false;
        }
        log("isWCDMAPreferredSupport : " + isWCDMAPreferred);
        return "1".equals(isWCDMAPreferred);
    }

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    /**
     * Return whether the project is support TDD data only.
     * @return If support, return true, else return false
     */
    public static boolean isMtkTddDataOnlySupport() {
        boolean isSupport = ONE.equals(SystemProperties.get(
                "ro.vendor.mtk_tdd_data_only_support")) ? true : false;
        Log.d(TAG, "isMtkTddDataOnlySupport(): " + isSupport);
        return isSupport;
    }

    /**
     * Return whether the project is support CT LTE TDD test.
     * @return If support, return true, else return false
     */
    public static boolean isCTLteTddTestSupport() {
        String[] type = MtkTelephonyManagerEx.getDefault().getSupportCardType(
                PhoneConstants.SIM_ID_1);
        if (type == null) {
            return false;
        }
        boolean isUsimOnly = false;
        if ((type.length == 1) && ("USIM".equals(type[0]))) {
            isUsimOnly = true;
        }
        return FeatureOption.isMtkSvlteSupport() && isUsimOnly;
    }

    /**
     * Get the phone id with main capability.
     * @return correct phone id
     */
    public static int getMainCapabilityPhoneId() {
        int phoneId = SubscriptionManager.INVALID_PHONE_INDEX;
        IMtkTelephonyEx iTelEx = IMtkTelephonyEx.Stub.asInterface(
                ServiceManager.getService("phoneEx"));
        if (iTelEx != null) {
            try {
                phoneId = iTelEx.getMainCapabilityPhoneId();
            } catch (RemoteException e) {
                log("getMainCapabilityPhoneId: remote exception");
            }
        } else {
            log("IMtkTelephonyEx service not ready!");
            phoneId = RadioCapabilitySwitchUtil.getMainCapabilityPhoneId();
        }
        log("getMainCapabilityPhoneId: phoneId = " + phoneId);
        return phoneId;
    }

    /**
     * When SS from VoLTE we should make the Mobile Data Connection open, if don't open,
     * the query will fail, so we should give users a tip, tell them how to get SS successfully.
     * This function is get the point, whether we should show a tip to user. Conditions:
     * 1. VoLTE condition / CMCC support VoLTE card, no mater IMS enable/not
     * 2. Mobile Data Connection is not enable
     * @param subId the given subId
     * @return true if should show tip, else false.
     */
    public static boolean shouldShowOpenMobileDataDialog(Context context, int subId) {
        boolean result = false;
        if (!PhoneUtils.isValidSubId(subId)) {
            log("[shouldShowOpenMobileDataDialog] invalid subId!!!  " + subId);
            return result;
        }

        PersistableBundle carrierConfig =
                   PhoneGlobals.getInstance().getCarrierConfigForSubId(subId);
        /// M: For plug-in Migration @{
        if (!ExtensionManager.getCallFeaturesSettingExt().
                needShowOpenMobileDataDialog(context, subId) || !carrierConfig.
                getBoolean(MtkCarrierConfigManager.MTK_KEY_SHOW_OPEN_MOBILE_DATA_DIALOG_BOOL)) {
            return result;
        }
        /// @}

        Phone phone = PhoneUtils.getPhoneUsingSubId(subId);
        int phoneId = phone.getPhoneId();
        MtkGsmCdmaPhone gsmCdmaphone =
                (MtkGsmCdmaPhone) PhoneFactory.getPhone(phoneId);
        // 1. Ims is registered, VoLTE condition
        // 2. Support UT and PS prefer (CNOP VoLTE)
        // 3. CT VoLTE enable and is CT 4G sim, the condition 2 can support
        // 4. Smart Fren sim, the condition 1 can support
        if (isImsServiceAvailable(context, subId)
                || (CallSettingUtils.isUtSupport(subId) &&
                (gsmCdmaphone.getCsFallbackStatus() == MtkPhoneConstants.UT_CSFB_PS_PREFERRED))) {
            log("[shouldShowOpenMobileDataDialog] ss query need mobile data connection!");
            ///M: When wfc registered, no need check mobile data because SS can go over wifi. @{
            boolean isWfcEnabled = ((TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE)).isWifiCallingAvailable();
            if (isWfcEnabled && !CallSettingUtils.isCmccOrCuCard(subId)) {
                return result;
            }
            /// @}
            // Data should open or not for singe volte.
            if (!CallSettingUtils.isMobileDataEnabled(subId)) {
                result = true;
            /// M: Add for dual volte feature, data traffic dialog @{
            } else if ((subId != SubscriptionManager.getDefaultDataSubscriptionId())
                && isSupportDualVolte(subId)) {
                result = true;
            /// @}
            } else if (!MtkTelephonyManagerEx.getDefault().isInHomeNetwork(subId)
                //is Network Roaming and special operator card
                && CallSettingUtils.isCmccOrCuCard(subId)
                && !phone.getDataRoamingEnabled()) {
                //M: Add for data roaming tips
                //When CMCC Network is Roaming and data roaming not enabled,
                //we should also give user tips to turn it on.
                log("[shouldShowOpenMobileDataDialog] network is roaming!");
                result = true;
            }
        }
        log("[shouldShowOpenMobileDataDialog] subId: " + subId + ",result: " + result);
        return result;
    }

    /// M: Add for dual volte feature @{
    /**
     * M: Return if the sim card supports dual volte. @{
     * @param subId sub id identify the sim card
     * @return true if the sim card supports dual volte
     */
    public static boolean isSupportDualVolte(int subId) {
        boolean result = false;
        result = MtkImsManager.isSupportMims() &&
                (CallSettingUtils.isCmccOrCuCard(subId) || (TelephonyUtilsEx.isCtVolteEnabled() &&
                TelephonyUtilsEx.isCt4gSim(subId)));
        return result ;
    }
    /// @}
}
