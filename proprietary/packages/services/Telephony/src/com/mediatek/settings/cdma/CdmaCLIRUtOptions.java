package com.mediatek.settings.cdma;

import android.app.ActionBar;
import android.os.Bundle;
import android.preference.PreferenceScreen;
import android.util.Log;
import android.view.MenuItem;

import com.android.internal.telephony.Phone;
import com.android.phone.CLIRListPreference;
import com.android.phone.CallFeaturesSetting;
import com.android.phone.PhoneGlobals;
import com.android.phone.R;
import com.android.phone.SubscriptionInfoHelper;
import com.android.phone.TimeConsumingPreferenceActivity;

/**
 * Caller id UT options for cdma card.
 */
public class CdmaCLIRUtOptions extends TimeConsumingPreferenceActivity
        implements PhoneGlobals.SubInfoUpdateListener {
    private static final String LOG_TAG = "CdmaCLIRUtOptions";

    private static final String BUTTON_CLIR_KEY  = "button_clir_key";

    private CLIRListPreference mCLIRPreference;
    private Phone mPhone;
    private SubscriptionInfoHelper mSubscriptionInfoHelper;

    @Override
    protected void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        addPreferencesFromResource(R.xml.mtk_cdma_clir_options);

        mSubscriptionInfoHelper = new SubscriptionInfoHelper(this, getIntent());
        mSubscriptionInfoHelper.setActionBarTitle(
                getActionBar(), getResources(), R.string.mtk_caller_id_with_label);
        mPhone = mSubscriptionInfoHelper.getPhone();

        /// M: Add for MTK hotswap @{
        PhoneGlobals.getInstance().addSubInfoUpdateListener(this);
        if (mPhone == null) {
            Log.d(LOG_TAG, "onCreate: mPhone is null, finish!!!");
            finish();
            return;
        }
        /// @}

        PreferenceScreen prefSet = getPreferenceScreen();
        mCLIRPreference = (CLIRListPreference) prefSet.findPreference(BUTTON_CLIR_KEY);

        /// M: adjust the waiting dialog show time firstly
        mIsForeground = true;

        if (icicle == null) {
            Log.d(LOG_TAG, "start to init ");
            mCLIRPreference.init(this, false, mPhone);
        } else {
            Log.d(LOG_TAG, "restore stored states");
            mCLIRPreference.init(this, true, mPhone);
            int[] clirArray = icicle.getIntArray(mCLIRPreference.getKey());
            if (clirArray != null) {
                Log.d(LOG_TAG, "onCreate:  clirArray[0]="
                        + clirArray[0] + ", clirArray[1]=" + clirArray[1]);
                mCLIRPreference.handleGetCLIRResult(clirArray);
            } else {
                mCLIRPreference.init(this, false, mPhone);
            }
        }

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            // android.R.id.home will be triggered in onOptionsItemSelected()
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ///M: Need check mCLIRButton null firstly
        if (mCLIRPreference != null && mCLIRPreference.clirArray != null) {
            outState.putIntArray(mCLIRPreference.getKey(), mCLIRPreference.clirArray);
        }
    }

    @Override
    public void onDestroy() {
        PhoneGlobals.getInstance().removeSubInfoUpdateListener(this);
        super.onDestroy();
    }

    @Override
    public void handleSubInfoUpdate() {
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        if (itemId == android.R.id.home) {  // See ActionBar#setDisplayHomeAsUpEnabled()
            CallFeaturesSetting.goUpToTopLevelSetting(this, mSubscriptionInfoHelper);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
