package com.android.phone;

import static com.android.phone.TimeConsumingPreferenceActivity.RESPONSE_ERROR;

import android.content.Context;
import android.os.AsyncResult;
import android.os.Handler;
import android.os.Message;
import android.os.PersistableBundle;
import android.preference.ListPreference;
import android.telephony.CarrierConfigManager;
import android.util.AttributeSet;
import android.util.Log;

import com.android.internal.telephony.CommandException;
import com.android.internal.telephony.CommandsInterface;
import com.android.internal.telephony.Phone;
import com.mediatek.phone.ext.ExtensionManager;
import com.mediatek.settings.CallSettingUtils;
import com.mediatek.settings.CallSettingUtils.DialogType;

import mediatek.telephony.MtkCarrierConfigManager;

/**
 * {@link ListPreference} for CLIR (Calling Line Identification Restriction).
 * Right now this is used for "Caller ID" setting.
 */
public class CLIRListPreference extends ListPreference {
    private static final String LOG_TAG = "CLIRListPreference";
    private final boolean DBG = true; //(PhoneGlobals.DBG_LEVEL >= 2);

    private final MyHandler mHandler = new MyHandler();
    private Phone mPhone;
    private TimeConsumingPreferenceListener mTcpListener;

    ///M: Revise to public, so MTK file can access the values.
    public int clirArray[];

    public CLIRListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CLIRListPreference(Context context) {
        this(context, null);
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        /// Add for [VoLTE_SS] data open or roaming @{
        DialogType type = CallSettingUtils.getDialogTipsType(getContext(), mPhone.getSubId());
        if (type  == DialogType.DATA_OPEN || type  == DialogType.DATA_ROAMING) {
            CallSettingUtils.showDialogTips(getContext(), mPhone.getSubId(), type, null);
            return;
        }
        /// @}
        super.onDialogClosed(positiveResult);

        mPhone.setOutgoingCallerIdDisplay(findIndexOfValue(getValue()),
                mHandler.obtainMessage(MyHandler.MESSAGE_SET_CLIR));
        if (mTcpListener != null) {
            mTcpListener.onStarted(this, false);
        }
    }

    /**
     * M: CLIR init and revise to public for access.
     * @param listener TimeConsumingPreferenceListener
     * @param skipReading Skip reading or not
     * @param phone The phone
     */
    public void init(
            TimeConsumingPreferenceListener listener, boolean skipReading, Phone phone) {
        /// M: Add for plug-in, to escape CLIR initialization @{
        if (ExtensionManager.getCallFeaturesSettingExt().escapeCLIRInit()) {
            Log.d(LOG_TAG, "init: escape");
            return;
        }
        /// @}
        mPhone = phone;
        mTcpListener = listener;
        if (!skipReading) {
            Log.i(LOG_TAG, "init: requesting CLIR");
            mPhone.getOutgoingCallerIdDisplay(mHandler.obtainMessage(MyHandler.MESSAGE_GET_CLIR,
                    MyHandler.MESSAGE_GET_CLIR, MyHandler.MESSAGE_GET_CLIR));
            if (mTcpListener != null) {
                mTcpListener.onStarted(this, true);
            }
        }
    }

    /**
     * M: Handle result and revise to public for access.
     * @param tmpClirArray Temp CLIR result array
     */
    public void handleGetCLIRResult(int tmpClirArray[]) {
        clirArray = tmpClirArray;
        final boolean enabled =
                tmpClirArray[1] == 1 || tmpClirArray[1] == 3 || tmpClirArray[1] == 4;
        /// Enable CLIR by operator request, default value is true. @{
        if (needToEnableClirSetting(getContext(), mPhone.getSubId())) {
            setEnabled(enabled);
        } else {
            setEnabled(false);
        }
        /// @}

        // set the value of the preference based upon the clirArgs.
        int value = CommandsInterface.CLIR_DEFAULT;
        switch (tmpClirArray[1]) {
            case 1: // Permanently provisioned
            case 3: // Temporary presentation disallowed
            case 4: // Temporary presentation allowed
                switch (tmpClirArray[0]) {
                    case 1: // CLIR invoked
                        value = CommandsInterface.CLIR_INVOCATION;
                        break;
                    case 2: // CLIR suppressed
                        value = CommandsInterface.CLIR_SUPPRESSION;
                        break;
                    case 0: // Network default
                    default:
                        value = CommandsInterface.CLIR_DEFAULT;
                        break;
                }
                break;
            case 0: // Not Provisioned
            case 2: // Unknown (network error, etc)
            default:
                value = CommandsInterface.CLIR_DEFAULT;
                break;
        }
        setValueIndex(value);

        // set the string summary to reflect the value
        int summary = R.string.sum_default_caller_id;
        switch (value) {
            case CommandsInterface.CLIR_SUPPRESSION:
                summary = R.string.sum_show_caller_id;
                break;
            case CommandsInterface.CLIR_INVOCATION:
                summary = R.string.sum_hide_caller_id;
                break;
            case CommandsInterface.CLIR_DEFAULT:
                summary = R.string.sum_default_caller_id;
                break;
        }
        setSummary(summary);
    }

    private class MyHandler extends Handler {
        static final int MESSAGE_GET_CLIR = 0;
        static final int MESSAGE_SET_CLIR = 1;

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_GET_CLIR:
                    handleGetCLIRResponse(msg);
                    break;
                case MESSAGE_SET_CLIR:
                    handleSetCLIRResponse(msg);
                    break;
            }
        }

        private void handleGetCLIRResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;

            if (msg.arg2 == MESSAGE_SET_CLIR) {
                mTcpListener.onFinished(CLIRListPreference.this, false);
            } else {
                mTcpListener.onFinished(CLIRListPreference.this, true);
            }
            clirArray = null;
            if (ar.exception != null) {
                Log.i(LOG_TAG, "handleGetCLIRResponse: ar.exception=" + ar.exception);
                /// M: Add for [CMCC_VoLTE_SS] @{
                if (ar.exception instanceof CommandException) {
                    handleCommandException((CommandException) ar.exception);
                } else {
                    /// Like ImsException and other exception, and we can't handle it
                    /// the same way as a CommandException.
                    mTcpListener.onError(CLIRListPreference.this, RESPONSE_ERROR);
                }
                /// @}
            } else if (ar.userObj instanceof Throwable) {
                Log.i(LOG_TAG, "handleGetCLIRResponse: ar.throwable=" + ar.userObj);
                mTcpListener.onError(CLIRListPreference.this, RESPONSE_ERROR);
            } else {
                int clirArray[] = (int[]) ar.result;
                if (clirArray.length != 2) {
                    mTcpListener.onError(CLIRListPreference.this, RESPONSE_ERROR);
                } else {
                    /// M: Add for plug-in ALPS02113837 @{
                    ExtensionManager.getCallFeaturesSettingExt()
                            .resetImsPdnOverSSComplete(getContext(), msg.arg2);
                    /// @}
                    Log.i(LOG_TAG, "handleGetCLIRResponse: CLIR successfully queried,"
                                + " clirArray[0]=" + clirArray[0]
                                + ", clirArray[1]=" + clirArray[1]);
                    handleGetCLIRResult(clirArray);
                }
            }
        }

        private void handleSetCLIRResponse(Message msg) {
            AsyncResult ar = (AsyncResult) msg.obj;

            if (ar.exception != null) {
                Log.d(LOG_TAG, "handleSetCLIRResponse: ar.exception=" + ar.exception);
                //setEnabled(false);
            }
            Log.d(LOG_TAG, "handleSetCLIRResponse: re get");

            mPhone.getOutgoingCallerIdDisplay(obtainMessage(MESSAGE_GET_CLIR,
                    MESSAGE_SET_CLIR, MESSAGE_SET_CLIR, ar.exception));
        }
    }

    // -------------------------------MTK-----------------------------
    /**
     * Enable clir setting or not.
     * @param context context
     * @param subId subId
     * @return true if clir setting should be enabled
     */
    private boolean needToEnableClirSetting(Context context, int subId) {
        boolean enableClirSetting = true;
        CarrierConfigManager configMgr = (CarrierConfigManager) context
                .getSystemService(Context.CARRIER_CONFIG_SERVICE);
        PersistableBundle b = configMgr.getConfigForSubId(subId);
        if (b != null) {
            enableClirSetting = b.getBoolean(
                    MtkCarrierConfigManager.MTK_KEY_SHOW_CLIR_SETTING_BOOL);
        }
        Log.d(LOG_TAG, "enableClirSetting:" + enableClirSetting);
        return enableClirSetting;
    }

    private void handleCommandException(CommandException exception) {
        // Add disable preference
        setEnabled(false);
        setSummary("");
        if (exception.getCommandError() != CommandException.Error.REQUEST_NOT_SUPPORTED) {
            mTcpListener.onException(CLIRListPreference.this, exception);
        } else {
            Log.d(LOG_TAG, "receive REQUEST_NOT_SUPPORTED CLIR !!");
        }
    }
}
