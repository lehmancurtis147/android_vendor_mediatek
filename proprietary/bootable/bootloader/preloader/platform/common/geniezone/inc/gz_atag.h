/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef GZ_ATAG_H
#define GZ_ATAG_H

#include "typedefs.h"

/* gz configs */
#define EL2_BOOT_DISABLE        (1 << 0)
#define EL2_REMAP_ENABLE        (1 << 1)

/* gz info atag */
struct boot_tag_gz_info {
    u32 gz_configs;
    u32 lk_addr;
    u32 build_variant;
};

/* gz boot atag */
#define RPMB_KEY_SIZE           (32)
struct boot_tag_gz_param {
    u8  hwuid[ME_IDENTITY_LEN];
    u64 modemMteeShareMemPA;
    u32 modemMteeShareMemSize;
    u8  rpmb_key[RPMB_KEY_SIZE];
};

/* gz platform atag */
#define GZ_PLAT_FLAGS_REMAP_ENABLE (1 << 0)
#define GZ_PLAT_FLAGS_GIC_V2       (1 << 1)
#define GZ_PLAT_FLAGS_GIC_V3       (1 << 2)
#define GZ_PLAT_FLAGS_EMI_MPU_EN   (1 << 3)
#define GZ_PLAT_FLAGS_DEV_MPU_EN   (1 << 4)
#define GZ_PLAT_FLAGS_PWRAP_EN     (1 << 5)
#define GZ_PLAT_FLAGS_ERASE_ATAG   (1 << 31)
#define GZ_INVALID_MPU_REGION_ID   (0x4E4F5054)

#define GZ_PLAT_TAG_SIZE (128)
struct boot_tag_gz_platform {
    u32 uart_base;
    u32 cpuxgpt_base;
    u32 gicd_base;
    u32 gicr_base;
    u32 remap_domain;
    u64 ddr_remap_offset;
    u64 io_remap_offset;
    u64 sec_io_remap_offset;
    u64 exec_start_offset;
    u64 flags;
    u32 pwrap_base;
    u32 reserve_mem_size;
    u32 prot_mem_mpu_region;
    u32 rtc_base;
    u32 mcucfg_base;
    u32 reserved[12];
} __attribute__((packed));

#endif /* end of GZ_ATAG_H */
