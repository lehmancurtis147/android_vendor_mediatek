/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef GZ_INIT_H
#define GZ_INIT_H

#include "typedefs.h"
#include "blkdev.h"

#define GZ_KERNEL_LOAD_OFFSET   (0X38000000) /* GZ Code Start */

#define GZ_DRAM_SIZE            (0x8000000)  /* 128MB */
#define GZ_ALIGNMENT            (0x200000)   /* 2MB alignment */
#define GZ_ADDR_MAX             (0x80000000) /* Upper limit */

#define GZ_MD_SHM_SIZE          (0x10000)    /* 64KB */
#define GZ_MD_SHM_ALIGNMENT     (0x2000000)  /* 64MB alignment */
#define GZ_MD_SHM_ADDR_MAX      (0x80000000) /* Upper limit */

#define EL2_BOOTING_DISABLED    (0x4e6f475a) /* ascii nogz */

/* extern variables */
extern u32 gz_md_shm_addr;
extern u32 gz_md_shm_size;
extern u32 build_variant;

/* extern APIs */
unsigned int is_booting_el2(void);
int bldr_load_gz_part(blkdev_t *bdev, const char *part_name);
u32 gz_config_info_atag(unsigned *ptr);
u32 gz_config_boot_atag(unsigned *ptr);
u32 gz_config_platform_atag(unsigned *ptr);
void gz_pre_init(void);
void gz_post_init(void);
u64 gz_get_jump_addr(void);
int gz_de_init(void);
u32 gz_get_load_addr(u32 maddr);

#endif /* end of GZ_INIT_H */
