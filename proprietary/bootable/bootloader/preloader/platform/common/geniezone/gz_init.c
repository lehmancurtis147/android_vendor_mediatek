/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "typedefs.h"
#include "platform.h"
#include "download.h"
#include "meta.h"
#include "sec.h"
#include "partition_api.h"
#include "dram_buffer.h"
#include "wdt.h"
#include "emi_mpu_mt.h"
#if CFG_ATF_SUPPORT
#include "tz_init.h"
#endif

#include <gz_init.h>
#include <gz_atag.h>
#if CFG_GZ_REMAP
#include <gz_remap.h>
#endif
#include "mt_rtc_hw.h"

/*============================================================================*/
/* DEBUG MACROS                                                               */
/*============================================================================*/
#define GZ_DEBUG
#ifdef GZ_DEBUG
#define DBG_MSG(str, ...) do {pal_log_debug(str, ##__VA_ARGS__);} while(0)
#define DBG_INFO(str, ...) do {pal_log_info(str, ##__VA_ARGS__);} while(0)
#define DBG_ERR(str, ...) do {pal_log_err(str, ##__VA_ARGS__);} while(0)
#else
#define DBG_MSG(str, ...) do {} while(0)
#define DBG_INFO(str, ...) do {pal_log_info(str, ##__VA_ARGS__);} while(0)
#define DBG_ERR(str, ...) do {pal_log_err(str, ##__VA_ARGS__);} while(0)
#endif

/*============================================================================*/
/* CONSTAND DEFINITIONS                                                       */
/*============================================================================*/
#define MOD "GZINIT"
#define MBLOCK_RELEASE mblock_create

/*============================================================================*/
/* GLOBAL VARIABLES                                                           */
/*============================================================================*/
#if CFG_BOOT_ARGUMENT
#define bootarg g_dram_buf->bootarg
#endif
#if CFG_BOOT_ARGUMENT_BY_ATAG
extern unsigned int g_uart;
#endif

/*============================================================================*/
/* INTERNAL VARIABLES                                                         */
/*============================================================================*/
u32 gz_start_addr = 0;
u32 gz_md_shm_addr = 0;
u32 gz_md_shm_size = 0;
u32 build_variant = 0; /* 1:user; 2:userdebug; 3:eng */

/*============================================================================*/
/* ENUMERATIONS                                                               */
/*============================================================================*/
typedef enum {
    GZ_OK = 0,
    GZ_PART_LOAD_FAIL = 1,
    GZ_PART_INVALID_ADDR = 2,
    GZ_PART_INVALID_SIZE = 3,
    GZ_PART_DECRYPT_FAIL = 4,
    GZ_PART_NOT_FOUND = 5,
    GZ_PART_HEADER_NOT_FOUND = 6,
    GZ_BOOT_DISABLE = 7,
    GZ_BOOT_DEV_NOT_FOUND = 8,
    GZ_RESERVE_MEM_FAIL = 9,
} E_GZ_FUNC_RET;

static u32 get_boot_configs(void)
{
    u32 val = 0;

    if (!is_booting_el2())
        val |= EL2_BOOT_DISABLE;

#if CFG_GZ_REMAP
    val |= EL2_REMAP_ENABLE;
#endif

    DBG_INFO("[%s] GZ CONFIGS = 0x%x\n", MOD, val);
    return val;
}

static u32 get_build_variant(void)
{
    /* default treat it as user load */
    u32 variant = 1;

#ifdef TARGET_BUILD_VARIANT_USER
    variant = 1;
#endif

#ifdef TARGET_BUILD_VARIANT_USERDEBUG
    variant = 2;
#endif

#ifdef TARGET_BUILD_VARIANT_ENG
    variant = 3;
#endif

    return variant;
}

static u32 get_remap_domain(void)
{
#if CFG_GZ_REMAP
    return GZ_REMAP_VMDOMAIN_GZ;
#else
    return 0x0;
#endif
}

static u64 get_ddr_remap_offset(void)
{
#if CFG_GZ_REMAP
    u64 offset = gz_remap_ddr_get_offset(GZ_REMAP_VMID_GZ);
    return (offset & 0x8000000000000000ULL) ? 0x0LL : offset;
#else
    return 0x0LL;
#endif
}

static u64 get_io_remap_offset(void)
{
#if CFG_GZ_REMAP
    u64 offset = gz_remap_io_get_offset(GZ_REMAP_VMID_GZ);
    return (offset & 0x8000000000000000ULL) ? 0x0LL : offset;
#else
    return 0x0LL;
#endif
}

static u64 get_sec_io_remap_offset(void)
{
#if CFG_GZ_REMAP
    u64 offset = gz_remap_sec_io_get_offset(GZ_REMAP_VMID_GZ);
    return (offset & 0x8000000000000000ULL) ? 0x0LL : offset;
#else
    return 0x0LL;
#endif
}

static u32 get_load_offset_without_remap(void)
{
    u32 gz_load_offset = (gz_start_addr - GZ_KERNEL_LOAD_OFFSET);

    DBG_INFO("[%s] GZ exec load offset: 0x%x\n", MOD, gz_load_offset);
    return gz_load_offset;
}

/* Vendor customization.
 * 1: disable gz booting
 * 0: enable gz booting
 */
static int gz_booting_disable_cfg(void)
{
    /* default is to enable EL2 booting */
    return 0;
}

static void gz_set_boot_disabled(void)
{
    gz_start_addr = EL2_BOOTING_DISABLED;
    gz_md_shm_addr = 0;
    gz_md_shm_size = 0;
}

#define img_hdr_buf (g_dram_buf->img_hdr_buf)
#define CMP_EQ(a,b) (a==b)
static int gz_part_existence_check(void)
{
    ssize_t read_len;
    ssize_t try_len = sizeof(part_hdr_t);

    read_len = partition_read("gz1", 0, (uint8_t *)img_hdr_buf, try_len);
    if (CMP_EQ(read_len, try_len))
        return GZ_OK;

    DBG_ERR("[%s] invalid 'gz1' part length: %d\n", MOD, read_len);

    read_len = partition_read("gz2", 0, (uint8_t *)img_hdr_buf, try_len);
    if (CMP_EQ(read_len, try_len))
        return GZ_OK;

    DBG_ERR("[%s] invalid 'gz2' part length: %d\n", MOD, read_len);
    gz_set_boot_disabled();
    return GZ_PART_HEADER_NOT_FOUND;
}

static int gz_reserve_memory(void)
{
    gz_start_addr = ((u32)mblock_reserve_ext(&bootarg.mblock_info,
        GZ_DRAM_SIZE, GZ_ALIGNMENT, GZ_ADDR_MAX, 0, "gz"));
    if(!gz_start_addr) {
        return GZ_RESERVE_MEM_FAIL;
    }

    DBG_INFO("[%s] allocate gz ram 0x%x\n", MOD, gz_start_addr);
    return GZ_OK;
}

static int gz_md_shm_reserve(u32 *reserved_addr, u32 *reserved_size)
{
    u32 start_addr = 0;

    start_addr = mblock_reserve_ext(&bootarg.mblock_info, GZ_MD_SHM_SIZE,
        GZ_MD_SHM_ALIGNMENT, GZ_MD_SHM_ADDR_MAX, 0, "gz-md-shm");
    if (!start_addr) {
        return GZ_RESERVE_MEM_FAIL;
    }

    *reserved_addr = start_addr;
    *reserved_size = GZ_MD_SHM_SIZE;

    DBG_INFO("[%s] allocate gz-md shared memory 0x%x\n", MOD, start_addr);
    return GZ_OK;
}

unsigned int is_booting_el2(void)
{
	if (gz_start_addr == EL2_BOOTING_DISABLED)
		return 0;

	return 1; /* booting el2 */
}

int bldr_load_gz_part(blkdev_t *bdev, const char *part_name)
{
    static u32 offset = 0;
    u32 size;
    int ret;
    part_t *part = part_get((char *)part_name);
    u32 addr = gz_get_load_addr(0);

    if (!is_booting_el2()) {
        DBG_INFO("[%s] EL2_BOOTING_DISABLED, skip load gz %s\n", MOD, __func__);
        return GZ_OK;
    }

    ret = part_load(bdev, part, &addr, offset, &size);
    if (ret) {
        DBG_ERR("[%s] %s part. ATF load fail\n", MOD, part_name);
        return GZ_PART_LOAD_FAIL;
    }
	/* check if target address defined by image is the same as mblock reserved address */
	if (addr != gz_start_addr) {
		DBG_ERR("[%s] %s part. error: target addr = 0x%x but reserved addr = 0x%x\n",
			MOD, part_name, addr, gz_start_addr);
		return GZ_PART_INVALID_ADDR;
	}
	/* check if the size of GZ image loaded exceeds the reserved size */
	if (size > GZ_DRAM_SIZE) {
		DBG_ERR("[%s] %s part. error: image size = 0x%x but reserved size = 0x%x\n",
			MOD, part_name, size, GZ_DRAM_SIZE);
		return GZ_PART_INVALID_SIZE;
	}

#if CFG_GZ_NEED_DESCRAMBLE
	ret = descramble(gz_start_addr, size);
	if (ret) {
		DBG_ERR("[%s] error descramble: ret = %d\n", MOD, ret);
		return GZ_PART_DECRYPT_FAIL;
    }
#endif

    return GZ_OK;
}

u32 gz_config_info_atag(unsigned *ptr)
{
    boot_tag *tags = (boot_tag *)ptr;

    if (!is_booting_el2())
        return 0;

    tags->hdr.size = boot_tag_size(boot_tag_gz_info);
    tags->hdr.tag  = BOOT_TAG_GZ_INFO;
    tags->u.gz_info.gz_configs = get_boot_configs();
    tags->u.gz_info.lk_addr = CFG_UBOOT_MEMADDR;
    tags->u.gz_info.build_variant= build_variant;

    return tags->hdr.size;
}

u32 gz_config_boot_atag(unsigned *ptr)
{
    boot_tag *tags = (boot_tag *)ptr;
    int ret;

    if (!is_booting_el2())
        return 0;

    tags = (boot_tag *)ptr;
    tags->hdr.size = boot_tag_size(boot_tag_gz_param);
    tags->hdr.tag  = BOOT_TAG_GZ_PARAM;
    tags->u.gz_param.modemMteeShareMemPA = gz_md_shm_addr;
    tags->u.gz_param.modemMteeShareMemSize = gz_md_shm_size;

    ret = tee_get_hwuid(tags->u.gz_param.hwuid, ME_IDENTITY_LEN);
    if (ret != 0) {
        DBG_ERR("[%s] gz hwuid fail, ret=%d\n", MOD, ret);
    }
#if 0
    pal_log_info("gz hwuid done\n");
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.hwuid[0], tags->u.gz_param.hwuid[1],
        tags->u.gz_param.hwuid[2], tags->u.gz_param.hwuid[3]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.hwuid[4], tags->u.gz_param.hwuid[5],
        tags->u.gz_param.hwuid[6], tags->u.gz_param.hwuid[7]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.hwuid[8], tags->u.gz_param.hwuid[9],
        tags->u.gz_param.hwuid[10], tags->u.gz_param.hwuid[11]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.hwuid[12], tags->u.gz_param.hwuid[13],
        tags->u.gz_param.hwuid[14], tags->u.gz_param.hwuid[15]);
#endif

    ret = rpmb_get_key(tags->u.gz_param.rpmb_key, RPMB_KEY_SIZE);
    if (ret != 0) {
        DBG_ERR("[%s] gz rpmb_key fail, ret=%d\n", MOD, ret);
    }
#if 0
    pal_log_info("gz rpmb_key done(partial value)\n");
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.rpmb_key[0], tags->u.gz_param.rpmb_key[1],
        tags->u.gz_param.rpmb_key[2], tags->u.gz_param.rpmb_key[3]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.rpmb_key[4], tags->u.gz_param.rpmb_key[5],
        tags->u.gz_param.rpmb_key[6], tags->u.gz_param.rpmb_key[7]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.rpmb_key[8], tags->u.gz_param.rpmb_key[9],
        tags->u.gz_param.rpmb_key[10], tags->u.gz_param.rpmb_key[11]);
    pal_log_info("0x%x 0x%x 0x%x 0x%x\n",
        tags->u.gz_param.rpmb_key[12], tags->u.gz_param.rpmb_key[13],
        tags->u.gz_param.rpmb_key[14], tags->u.gz_param.rpmb_key[15]);
#endif

    return tags->hdr.size;
}

u32 gz_config_platform_atag(unsigned *ptr)
{
    boot_tag *tags = (boot_tag *)ptr;
    int ret;

    COMPILE_ASSERT(GZ_PLAT_TAG_SIZE == sizeof(struct boot_tag_gz_platform));

    if (!is_booting_el2())
        return 0;

    tags = (boot_tag *)ptr;
    tags->hdr.size = boot_tag_size(boot_tag_gz_platform);
    tags->hdr.tag  = BOOT_TAG_GZ_PLAT;

#if CFG_BOOT_ARGUMENT_BY_ATAG
    tags->u.gz_plat.uart_base = g_uart;
#elif CFG_BOOT_ARGUMENT && !CFG_BOOT_ARGUMENT_BY_ATAG
    tags->u.gz_plat.uart_base = bootarg.log_port;
#else
    tags->u.gz_plat.uart_base = CFG_UART_LOG;
#endif
    /* Switch log port to UART2 while uart meta connected */
    if (g_boot_mode == META_BOOT && g_meta_com_type == META_UART_COM)
        tags->u.gz_plat.uart_base = UART2;

    tags->u.gz_plat.cpuxgpt_base = CPUXGPT_BASE;
    tags->u.gz_plat.gicd_base = GICD_BASE;
    tags->u.gz_plat.gicr_base = GICR_BASE;
    tags->u.gz_plat.pwrap_base = PWRAP_REMAP_BASE;
    tags->u.gz_plat.rtc_base = RTC_SEC_BASE;
    tags->u.gz_plat.mcucfg_base = MCUCFG_BASE;
    tags->u.gz_plat.reserve_mem_size = GZ_DRAM_SIZE;
    tags->u.gz_plat.remap_domain = get_remap_domain();
    tags->u.gz_plat.ddr_remap_offset = get_ddr_remap_offset();
    tags->u.gz_plat.io_remap_offset = get_io_remap_offset();
    tags->u.gz_plat.sec_io_remap_offset = get_sec_io_remap_offset();
    tags->u.gz_plat.exec_start_offset = get_ddr_remap_offset();
    tags->u.gz_plat.exec_start_offset += get_load_offset_without_remap();
#ifdef GZ_PROT_SHARE_REGION_ID
    tags->u.gz_plat.prot_mem_mpu_region = GZ_PROT_SHARE_REGION_ID;
#else
    tags->u.gz_plat.prot_mem_mpu_region = GZ_INVALID_MPU_REGION_ID;
#endif
    tags->u.gz_plat.flags = 0x0LL;
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_ERASE_ATAG;
#if CFG_GZ_REMAP
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_REMAP_ENABLE;
#endif
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_GIC_V3;
#if CFG_GZ_EMI_MPU
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_EMI_MPU_EN;
#endif
#if CFG_DEVMPU_SUPPORT
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_DEV_MPU_EN;
#endif
#if CFG_GZ_PWRAP_ENABLE
    tags->u.gz_plat.flags |= GZ_PLAT_FLAGS_PWRAP_EN;
#endif

    DBG_INFO("[%s] gz uart: 0x%x\n", MOD, tags->u.gz_plat.uart_base);
    DBG_INFO("[%s] gz platform flags: 0x%llx\n", MOD, tags->u.gz_plat.flags);
    return tags->hdr.size;
}

void gz_pre_init()
{
    int ret;

    build_variant = get_build_variant();

    ret = gz_part_existence_check();
    if (ret) {
        DBG_ERR("[%s] gz part check failed: %d!\n", MOD, ret);
        return;
    }

    ret = gz_reserve_memory();
    if (ret) {
        DBG_ERR("[%s] GZ fatal error...\n", MOD);
        ASSERT(0);
    }

    ret = gz_md_shm_reserve(&gz_md_shm_addr, &gz_md_shm_size);
    if (ret) {
        DBG_ERR("[%s] GZ MD shared mmeory fatal error...\n", MOD);
        ASSERT(0);
    }
}

int gz_de_init(void)
{
    if (gz_booting_disable_cfg()) {
        int ret;
        DBG_INFO("[%s] skip load gz(%s)\n", MOD, __func__);
        if (gz_md_shm_addr!=0 && gz_md_shm_size!=0) {
            ret = MBLOCK_RELEASE(&bootarg.mblock_info, &bootarg.orig_dram_info, (u64)gz_md_shm_addr, (u64)gz_md_shm_size);
        }
        if (ret) {
            DBG_ERR("[%s] GZ MD release shared mmeory fatal error...\n", MOD);
            ASSERT(0);
        }
        if (gz_start_addr!=0) {
            ret = MBLOCK_RELEASE(&bootarg.mblock_info, &bootarg.orig_dram_info, (u64)gz_start_addr, GZ_DRAM_SIZE);
        }
        if (ret) {
            DBG_ERR("[%s] GZ release fatal error...\n", MOD);
            ASSERT(0);
        }
        gz_set_boot_disabled();
        return 1;
    }
    return 0;
}

void gz_post_init(void)
{
    if (!is_booting_el2())
        return;

#if CFG_GZ_REMAP
    if (gz_remap_init() != GZ_REMAP_SUCCESS) {
        DBG_ERR("[%s] GZ remap initialization fatal error ...\n", MOD);
        ASSERT(0);
    }
    gz_remap_dump_config();
#endif

#ifdef GZ_OS_MPU_REGION_ID
    tz_sec_mem_init(gz_start_addr, gz_start_addr + GZ_DRAM_SIZE - 1, GZ_OS_MPU_REGION_ID);
    DBG_INFO("[%s] set gz memory protection : 0x%x, 0x%x (%d)\n", MOD, gz_start_addr,
        gz_start_addr + GZ_DRAM_SIZE - 1, GZ_OS_MPU_REGION_ID);
#else
    DBG_INFO("[%s] GZ OS region is not protected!\n", MOD);
#endif

#ifdef GZ_MD_SHM_MPU_REGION_ID
    tz_sec_mem_init(gz_md_shm_addr, gz_md_shm_addr + gz_md_shm_size - 1, GZ_MD_SHM_MPU_REGION_ID);
    DBG_INFO("[%s] set gz-md share memory protection : 0x%x, 0x%x (%d)\n", MOD, gz_md_shm_addr,
        gz_md_shm_addr + gz_md_shm_size - 1, GZ_MD_SHM_MPU_REGION_ID);
#else
    DBG_INFO("[%s] GZ-MD shared region is not protected!\n", MOD);
#endif
}

u64 gz_get_jump_addr(void)
{
    u64 addr64;

    addr64 = gz_start_addr;
#if CFG_GZ_REMAP
    addr64 += get_ddr_remap_offset();
#endif

    return addr64;
}

u32 gz_get_load_addr(u32 maddr)
{
    DBG_INFO("[%s] GZ load start: 0x%x (maddr: 0x%x)\n", MOD, gz_start_addr,
        maddr);
    return gz_start_addr;
}
