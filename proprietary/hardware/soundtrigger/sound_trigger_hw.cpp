/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "sound_trigger_hw_default"

#include <errno.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <log/log.h>
#include <fcntl.h>
#include <dlfcn.h>
#include <hardware/hardware.h>
#include <system/sound_trigger.h>
#include <hardware/sound_trigger.h>
//#include <media/AudioSystem.h>
#include <cutils/properties.h>
#include "soundtrigger.h"
#include "hardware_legacy/AudioMTKHardwareInterface.h"
#include <unistd.h>
#include <soundtriggerAssert.h>
#include <sys/ioctl.h>
#include <AudioType.h>
#include <audio_lock.h>
#if MTK_VOW_SUPPORT
#include "voiceunlock2/include/VowEngine_AP_testing.h"
#endif  // #if MTK_VOW_SUPPORT

using namespace android;
#define HAL_LIBRARY_PATH1 "/system/lib/hw"
#define HAL_LIBRARY_PATH2 "/vendor/lib/hw"
#define AUDIO_HAL_PREFIX "audio.primary"
#define PLATFORM_ID "ro.board.platform"
#define BOARD_PLATFORM_ID "ro.board.platform"
AudioMTKHardwareInterface *gAudioMTKHardware = NULL;
static String8 keyMTK_VOW_ENABLE = String8("MTK_VOW_ENABLE=1");
static String8 keyMTK_VOW_DISABLE = String8("MTK_VOW_ENABLE=0");
static String8 keyMTK_VOW_MIC_TYPE = String8("MTK_VOW_MIC_TYPE=");

AudioMTKStreamInInterface *gAudioMTKStreamIn = NULL;
#define SAMPLES1                       (160)  /* 10ms for 16k sample rate and 1 channel */
#define SAMPLES2                       (240)  /* 15ms for 16k sample rate and 1 channel */
#define VOW_VOICE_RECORD_THRESHOLD     (2560) /* 80ms */
#define VOW_VOICE_RECORD_BIG_THRESHOLD (8000) /* 250ms */
#define VOW_TIMES_OF_BIG_THRESHOLD     (64000 / 8000)
#define VOW_SAMPLE_RATE                (16000)
#define VOW_VOICE_SIZE                 (0x1F400)  //128000 = 4sec, unit:byte


void *AudioHwhndl = NULL;
#define ALLOCATE_MEMORY_SIZE_MAX (60 * 1024) // the max memory size can be allocated

#ifdef VOW_DATA_READ_PCMFILE
unsigned int read_filecnt = 0;
#endif

#ifdef VOW_RECOG_PCMFILE
unsigned int recog_filecnt = 0;
#endif

static int soundtrigger_dlopen()
{
    if (AudioHwhndl == NULL) {
        char prop[PATH_MAX];
        char path[PATH_MAX];
        do {
            if (property_get(PLATFORM_ID, prop, NULL) == 0) {
                snprintf(path, sizeof(path), "%s/%s.default.so",
                         HAL_LIBRARY_PATH1, prop);
                if (access(path, R_OK) == 0) { break; }


                if (access(path, R_OK) == 0) { break; }
            } else {
                snprintf(path, sizeof(path), "%s/%s.%s.so",
                         HAL_LIBRARY_PATH1, AUDIO_HAL_PREFIX, prop);
                if (access(path, R_OK) == 0) { break; }

                snprintf(path, sizeof(path), "%s/%s.%s.so",
                         HAL_LIBRARY_PATH2, AUDIO_HAL_PREFIX, prop);
                if (access(path, R_OK) == 0) { break; }

                if (property_get(BOARD_PLATFORM_ID, prop, NULL) == 0) {
                    snprintf(path, sizeof(path), "%s/%s.default.so",
                             HAL_LIBRARY_PATH1, prop);
                    if (access(path, R_OK) == 0) { break; }

                    snprintf(path, sizeof(path), "%s/%s.default.so",
                             HAL_LIBRARY_PATH2, prop);
                    if (access(path, R_OK) == 0) { break; }
                } else {
                    snprintf(path, sizeof(path), "%s/%s.%s.so",
                             HAL_LIBRARY_PATH1, AUDIO_HAL_PREFIX, prop);
                    if (access(path, R_OK) == 0) { break; }

                    snprintf(path, sizeof(path), "%s/%s.%s.so",
                             HAL_LIBRARY_PATH2, AUDIO_HAL_PREFIX, prop);
                    if (access(path, R_OK) == 0) { break; }
                }
            }
        } while (0);

        ALOGD("Load %s", path);
        AudioHwhndl = dlopen(path, RTLD_NOW);
        const char *dlsym_error1 = dlerror();
        ALOGE("dlerror()=%s", dlsym_error1);
        if (AudioHwhndl == NULL) {
            ALOGE("-DL open AudioHwhndl path [%s] fail", path);
            return -ENOSYS;
        } else {
            create_AudioMTKHw *func1 = (create_AudioMTKHw *)dlsym(AudioHwhndl, "createMTKAudioHardware");
            ALOGD("%s %d func1 %p", __FUNCTION__, __LINE__, func1);
            if (func1 == NULL) {
                ALOGE("-dlsym createMTKAudioHardware fail");
                /* close dlopen */
                dlclose(AudioHwhndl);
                AudioHwhndl = NULL;
                return -ENOSYS;
            }
            gAudioMTKHardware = func1();
            ALOGD("dlopen success gAudioMTKHardware");
        }
    }
    return 0;
}

static const struct sound_trigger_properties hw_properties = {
        "The Soundtrigger HAL Project", // implementor
        "Sound Trigger stub HAL", // description
        1, // version
        { 0xed7a7d60, 0xc65e, 0x11e3, 0x9be4, { 0x00, 0x02, 0xa5, 0xd5, 0xc5, 0x1b } }, // uuid
        1, // max_sound_models
        1, // max_key_phrases
        1, // max_users
        RECOGNITION_MODE_VOICE_TRIGGER, // recognition_modes
        true, // capture_transition
        0, // max_buffer_ms
        false, // concurrent_capture
        false, // trigger_in_event
        0 // power_consumption_mw
};

struct stub_sound_trigger_device {
    struct sound_trigger_hw_device device;
    sound_model_handle_t model_handle;
    recognition_callback_t recognition_callback;
    void *recognition_cookie;
    sound_model_callback_t sound_model_callback;
    void *sound_model_cookie;
    pthread_t callback_thread;
    pthread_mutex_t lock;
    pthread_cond_t  cond;
};

enum vow_model_type {
    VOW_SPEAKER_MODE,
    VOW_INITIAL_MODE,
    VOW_MODEL_MODE_NUM
};

enum voice_wakeup {
    VOICE_UNLOCK,
    VOICE_WAKEUP_NO_RECOGNIZE,
    VOICE_WAKEUP_RECOGNIZE,
    VOICE_WAKE_UP_MODE_NUM
};

struct vow_recog_device_info {
    pthread_t vow_recog_thread_id;
    pthread_t vow_data_read_thread_id;
    unsigned int buffer_write; // unit: byte
    unsigned int buffer_read;  // unit: byte
    short *pVoiceBuffer;  //unit: word
    bool data_read_stop;
    bool already_read;
    //struct stub_sound_trigger_device *device;
    sound_model_handle_t model_handle;
    recognition_callback_t recognition_callback;
    void *recognition_cookie;
    pthread_mutex_t lock;
};
int voice_recognize_running_state = -1;
int voice_data_read_running_state = -1;
int P25_Recog_result = 0;
unsigned int P25_entry_count = 0;
unsigned int P25_recog_ok_count = 0;
//unsigned int mBargeinStatus;
#if MTK_VOW_SUPPORT
ParsedDataCollection parsed_model_data;
#endif  // #if MTK_VOW_SUPPORT

int mFd = -1;
int mphrase_extrasid = -1;
int stoprecognition = -1;
int sound_trigger_running_state = -1;
char *pPhase2_ModelInfo;
char *pPhase25_ModelInfo;
bool phase2_testing_flag = false;
bool phase25_testing_flag = false;

alock_t *g_phase25_lock;

struct vow_eint_data_struct_t m_sINTData;

//#ifdef VOW_PHASE25_RECOGNIZE

static void *voice_data_read_thread_loop(void *context)
{
    struct vow_recog_device_info *tinfo = (struct vow_recog_device_info *)context;
    unsigned int nRead, nNeed = 0;
    unsigned int read_loop = 0;
    unsigned int free_size = 0;
    unsigned int write_size = 0;
#ifdef VOW_DATA_READ_PCMFILE
    char filenameL[] = "/sdcard/record_audio.pcm";
    char filename1[] = "/sdcard/record_audio";
    char filename2[] = ".pcm";
    char filename[40];
    FILE *pReadFile;
    char buffer[4];
#endif  // #ifdef VOW_DATA_READ_PCMFILE

    if (context == 0) {
        ALOGI("%s, context == 0", __func__);
        goto exit;
    }
    if (tinfo == 0) {
        ALOGI("%s, tinfo == 0", __func__);
        goto exit;
    }
    if (stoprecognition) {
        ALOGI("%s, stoprecognition already going", __func__);
        goto exit;
    }

    voice_data_read_running_state = 1;

    if (gAudioMTKStreamIn == NULL) {
        ALOGI("%s, openInputStream error", __func__);
        goto exit;
    }
    /* Set parameter with AUDIO_SOURCE_HOTWORD */
    gAudioMTKStreamIn->setParameters(String8("input_source=1999"));
#ifdef VOW_DATA_READ_PCMFILE
    sprintf(buffer, "%d", read_filecnt);
    strcpy(filename, filename1);
    strcpy(&filename[strlen(filename)], buffer);
    strcpy(&filename[strlen(filename)], filename2);
    ALOGI("filename=%s, len=%d", filename, strlen(filename));
    pReadFile = fopen(filename, "wb+");
    if (NULL == pReadFile) {
        ALOGI("%s, fopen error", __func__);
        goto exit;
    }
    read_filecnt++;
#endif  // #ifdef VOW_DATA_READ_PCMFILE
    ALOGV("%s, stoprecognition=%d, data_read_stop=%d",
          __func__,
          stoprecognition,
          (unsigned int)tinfo->data_read_stop);
    ALOGV("%s, buffer_write=%d, buffer_read=%d, data_read_stop=%d",
          __func__,
          tinfo->buffer_write,
          tinfo->buffer_read,
          (unsigned int)tinfo->data_read_stop);
    nNeed = 0;
    read_loop = 0;
    while ((!stoprecognition) && (!tinfo->data_read_stop)) {
        if (tinfo->buffer_write >= tinfo->buffer_read) {
            free_size = VOW_VOICE_SIZE - tinfo->buffer_write + tinfo->buffer_read;
        } else {
            free_size = tinfo->buffer_read - tinfo->buffer_write;
        }
        write_size = (read_loop < VOW_TIMES_OF_BIG_THRESHOLD)?
                     VOW_VOICE_RECORD_BIG_THRESHOLD : VOW_VOICE_RECORD_THRESHOLD;
        read_loop++;
        if (free_size < write_size) {
            // buffer overflow
            ALOGI("%s, voice buffer overflow!", __func__);
            write_size = free_size;
        }

        nRead = gAudioMTKStreamIn->read(&tinfo->pVoiceBuffer[(tinfo->buffer_write >> 1)],
                                        write_size);
/*
        ioctl(mFd, VOW_GET_BARGEIN_STATUS, &bargein_status);
        if (mBargeinStatus != bargein_status) {
            mBargeinStatus = bargein_status;
            ALOGI("[P2.5+bargin]bargein_status=%d", mBargeinStatus);
            //VOWE_testing_setArgument(vowe_argid_isBargeInOn, mBargeinStatus);
        }
*/
        if (nRead == 0) {
            ALOGI("StreamIn Rec error");
            break;
        }
        nNeed += nRead;
        ALOGV("nRead=%d, nNeed=%d, read_loop=%d", nRead, nNeed, read_loop);
#ifdef VOW_DATA_READ_PCMFILE
        fwrite(&tinfo->pVoiceBuffer[(tinfo->buffer_write >> 1)], write_size, 1, pReadFile);
#endif  // #ifdef VOW_DATA_READ_PCMFILE
        /* Update buffer_write index */
        tinfo->buffer_write += write_size;
        ALOGV("buf_w=%d", tinfo->buffer_write);
        if (tinfo->buffer_write >= VOW_VOICE_SIZE) {
            tinfo->buffer_write = 0;
        }
        tinfo->already_read = true;
        SIGNAL_ALOCK(g_phase25_lock);
    }
    ALOGI("%s, nNeed=%d", __func__, nNeed);

#ifdef VOW_DATA_READ_PCMFILE
    fclose(pReadFile);
#endif  // #ifdef VOW_DATA_READ_PCMFILE

exit:
    voice_data_read_running_state = 0;
    tinfo->already_read = false;
    ALOGI("%s, exit", __func__);
    pthread_exit(NULL);
}

static void *voice_recognize_thread_loop(void *context)
{
    struct vow_recog_device_info *tinfo = (struct vow_recog_device_info *)context;
    short *pbuffer;
    short *pRecogBuf;
    int i;
    unsigned int size = 0;         // unit: byte
    unsigned int reserv_size = 0;  // unit: byte
    unsigned int bottom_size = 0;  // unit: byte
    unsigned int read_idx = 0;     // unit: byte
#if MTK_VOW_SUPPORT
    int recog_ret = 0;
#endif  // #if MTK_VOW_SUPPORT
    bool bFirstFrame = true;
    unsigned int nNeed = 0;
    int lock_ret = 0;
    int stop_own_thread = 0;
    int Inputformat = AUDIO_FORMAT_PCM_16_BIT;
    uint32_t Inputdevice = AUDIO_DEVICE_IN_BUILTIN_MIC;
    uint32_t Inputchannel = AUDIO_CHANNEL_IN_MONO;
    uint32_t InputsampleRate = VOW_SAMPLE_RATE;
    int status = 0;
    unsigned int temp_result = 0;
#ifdef VOW_RECOG_PCMFILE
    char filenameL[] = "/sdcard/record_recog_audio.pcm";
    char filename1[] = "/sdcard/record_recog_audio";
    char filename2[] = ".pcm";
    char filename[40];
    FILE *pRecogFile;
    char buffer[4];
#endif  // #ifdef VOW_RECOG_PCMFILE

    P25_Recog_result = 0;

    if (context == 0) {
        ALOGI("%s, context == 0", __func__);
        goto exit;
    }
    if (tinfo == 0) {
        ALOGI("%s, tinfo == 0", __func__);
        goto exit;
    }
    if (stoprecognition) {
        ALOGI("%s, stoprecognition already going", __func__);
        goto exit;
    }
    if (gAudioMTKStreamIn != NULL) {
        ALOGI("%s, openInputStream is not NULL", __func__);
        goto exit;
    }
    voice_recognize_running_state = 1;

#ifdef VOW_RECOG_PCMFILE
    sprintf(buffer, "%d", recog_filecnt);
    ALOGI("1. buffer=%s", buffer);
    strcpy(filename, filename1);
    ALOGI("2. filename=%s, len=%d", filename, strlen(filename));
    strcpy(&filename[strlen(filename)], buffer);
    ALOGI("3. filename=%s, len=%d", filename, strlen(filename));
    strcpy(&filename[strlen(filename)], filename2);
    ALOGI("4. filename=%s, len=%d", filename, strlen(filename));
    pRecogFile = fopen(filename, "wb+");
    if (NULL == pRecogFile) {
        ALOGI("%s, fopen error", __func__);
        goto exit;
    }
    recog_filecnt++;
#endif  // #ifdef VOW_RECOG_PCMFILE

    tinfo->data_read_stop = false;

    /* Open Input Stream */
    if (gAudioMTKHardware != NULL) {
        gAudioMTKStreamIn = gAudioMTKHardware->openInputStreamWithFlags(Inputdevice,
                                                                        &Inputformat,
                                                                        &Inputchannel,
                                                                        &InputsampleRate,
                                                                        &status,
                                                                        (audio_in_acoustics_t)0,
                                                                        AUDIO_INPUT_FLAG_RAW);
        ALOGI("openInputStream=%p", gAudioMTKStreamIn);
    }

    pthread_create(&tinfo->vow_data_read_thread_id, (const pthread_attr_t *) NULL,
                   voice_data_read_thread_loop, tinfo);

    ALOGI("%s, ready to read", __func__);
    nNeed = 0;
    i = 0;
#if MTK_VOW_SUPPORT
    if (VOWE_AP_testing_reset() == vowe_bad) {
        ALOGI("[ASP API]reset error");
    }
#endif  // #if MTK_VOW_SUPPORT
    bFirstFrame = true;
    stop_own_thread = 0;
    pbuffer = new short[SAMPLES2];  // for combine data use
    while (!stoprecognition && !stop_own_thread) {
        lock_ret = WAIT_ALOCK_MS(g_phase25_lock, 250);
        //ALOGI("lock_ret=%d", lock_ret);
        if (lock_ret == -ETIMEDOUT) {
            ALOGV("wait signal timeout: %d", lock_ret);
            ALOGI("voice_data_read_running_state = %d, already_read = %d", voice_data_read_running_state,
                                                                           tinfo->already_read);
            break;
        }

        while (1) {
            size = bFirstFrame ? (SAMPLES2 << 1) : (SAMPLES1 << 1);  // unit: byte
            ALOGV("w/r = %d/%d", tinfo->buffer_write, tinfo->buffer_read);

            if (tinfo->already_read) {
                if (tinfo->buffer_write >= tinfo->buffer_read) {
                    reserv_size = tinfo->buffer_write - tinfo->buffer_read;
                } else {
                    reserv_size = VOW_VOICE_SIZE - tinfo->buffer_read + tinfo->buffer_write;
                }
            } else {
                reserv_size = 0;
            }
            if (reserv_size >= size) {
                // make sure there is enough data to input first frame
                if (bFirstFrame) {
                    bFirstFrame = false;
                }
                bottom_size = (VOW_VOICE_SIZE - tinfo->buffer_read);
                if (bottom_size >= size) {  // no need to combine data
                    pRecogBuf = &tinfo->pVoiceBuffer[(tinfo->buffer_read >> 1)];
                    read_idx = tinfo->buffer_read + size;
                } else {  // need to combine down and top data
                    memcpy(&pbuffer[0], &tinfo->pVoiceBuffer[(tinfo->buffer_read >> 1)], bottom_size);
                    memcpy(&pbuffer[bottom_size], &tinfo->pVoiceBuffer[0], size - bottom_size);
                    pRecogBuf = pbuffer;
                    read_idx = size - bottom_size;
                }
                i++;
                nNeed += size;
#ifdef VOW_RECOG_PCMFILE
                fwrite(pRecogBuf, size, 1, pRecogFile);
#endif  // #ifdef VOW_RECOG_PCMFILE

#if MTK_VOW_SUPPORT
                //ALOGD("pRecogBuf=%d", *pRecogBuf);
                if (VOWE_AP_testing_inputMic(0, pRecogBuf, (size >> 1)) == vowe_bad) {
                    ALOGI("[P2.5]InputMic error:sz=%d", (size >> 1));
                    stop_own_thread = 1;
                    break;
                }
                ALOGV("[P2.5]reg+, sz=%d", (size >> 1));
                recog_ret = VOWE_AP_testing_recognize();

                ALOGV("==>read %d", i);
                if (recog_ret == vowe_bad) {
                    ALOGI("[P2.5]Recog error, check flow");
                    temp_result = RECOG_BAD;

                    stop_own_thread = 1;
                    break;
                } else if (recog_ret == vowe_reject) {
                    ALOGI("[P2.5]check keyword fail");
                    temp_result = RECOG_FAIL;
                    stop_own_thread = 1;
                    break;
                } else if (recog_ret == vowe_accept) {
                    char *data;
                    struct sound_trigger_phrase_recognition_event *event;

                    ALOGI("[P2.5]check keyword success");
                    pthread_mutex_lock(&tinfo->lock);
                    data = (char *)calloc(1, sizeof(struct sound_trigger_phrase_recognition_event) + 1);
                    event = (struct sound_trigger_phrase_recognition_event *)data;
                    event->common.status = RECOGNITION_STATUS_SUCCESS;
                    event->common.type = SOUND_MODEL_TYPE_KEYPHRASE;
                    event->common.model = tinfo->model_handle;
                    event->common.capture_available = true;
                    event->common.audio_config = AUDIO_CONFIG_INITIALIZER;
                    event->common.audio_config.sample_rate = 16000;
                    event->common.audio_config.channel_mask = AUDIO_CHANNEL_IN_MONO;
                    event->common.audio_config.format = AUDIO_FORMAT_PCM_16_BIT;
                    event->num_phrases = 1;
                    event->phrase_extras[0].id = mphrase_extrasid;
                    event->phrase_extras[0].recognition_modes = RECOGNITION_MODE_VOICE_TRIGGER;
                    event->phrase_extras[0].confidence_level = 100;
                    event->phrase_extras[0].num_levels = 1;
                    event->phrase_extras[0].levels[0].level = 100;
                    event->phrase_extras[0].levels[0].user_id = 0;
                    event->common.data_offset = sizeof(struct sound_trigger_phrase_recognition_event);
                    event->common.data_size = 1;
                    if (tinfo->recognition_callback != NULL) {
                        tinfo->recognition_callback(&event->common, tinfo->recognition_cookie);
                    }
                    ALOGI("phrase_extras[0].id %d", mphrase_extrasid);
                    ALOGI("capture_available %d", event->common.capture_available);
                    ALOGI("%s send callback model %d", __func__, tinfo->model_handle);
                    free(data);
                    pthread_mutex_unlock(&tinfo->lock);
                    temp_result = RECOG_PASS;
                    stop_own_thread = 1;
                    break;
                }
#endif  // #if MTK_VOW_SUPPORT
                /* Update buffer_read index */
                tinfo->buffer_read = read_idx;
                ALOGV("buf_r=%d", tinfo->buffer_read);
            } else {
                /* there is no enough stream in data, need wait for next receive */
                break;
            }
        }
    }
    ALOGI("%s, nNeed=%d, loop=%d", __func__, nNeed, i);
#ifdef VOW_RECOG_PCMFILE
    fclose(pRecogFile);
#endif  // #ifdef VOW_RECOG_PCMFILE
    delete[] pbuffer;

    /* notice data_read thread to stop */
    tinfo->data_read_stop = true;
    ALOGI("+pthread_join_read_data");
    pthread_join(tinfo->vow_data_read_thread_id, NULL);
    ALOGI("-pthread_join_read_data");

    /* Close Input Stream */
    if (gAudioMTKStreamIn != NULL) {
        gAudioMTKStreamIn->standby();
        ALOGI("[P2.5] 1. standby ok");
    }

    if (gAudioMTKHardware != NULL) {
        gAudioMTKHardware->closeInputStream(gAudioMTKStreamIn);
        ALOGI("%s, clear gAudioMTKStreamIn", __func__);
        gAudioMTKStreamIn = NULL;
        ALOGI("[P2.5] 2. closeInputStream ok");
    }

exit:
    voice_recognize_running_state = 0;
    ALOGI("%s, exit", __func__);
    P25_Recog_result = temp_result;
    pthread_exit(NULL);
}
//#endif  // #ifdef VOW_PHASE25_RECOGNIZE

static void *callback_thread_loop(void *context)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)context;
    bool excute_recognize_pass = false;

    ALOGI("%s", __func__);
    prctl(PR_SET_NAME, (unsigned long)"sound trigger callback", 0, 0, 0);
    pthread_mutex_lock(&stdev->lock);
    if (stdev->recognition_callback == NULL) {
        ALOGI("return: recognition_callback = NULL");
        pthread_mutex_unlock(&stdev->lock);
        goto exit;
    }
    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY);
    }
    ALOGI("mFd%d", mFd);
    if (mFd < 0) {
        ALOGI("open device fail!%s\n", strerror(errno));
    }
    pthread_mutex_unlock(&stdev->lock);
    m_sINTData.eint_status = VOW_EINT_FAIL;
    ALOGI("1.VoiceWakeup interrupt status, status:%d ,stoprecognition:%d, sound_trigger_running_state:%d",
           m_sINTData.eint_status, stoprecognition, sound_trigger_running_state);
    while (!stoprecognition) {
        if (mFd >= 0) {
            read(mFd, &m_sINTData, sizeof(struct vow_eint_data_struct_t));

            ALOGI("2.VoiceWakeup interrupt status, status:%d, stoprecognition:%d",
                  m_sINTData.eint_status, stoprecognition);
            if (m_sINTData.eint_status == VOW_EINT_PASS) {
                excute_recognize_pass = true;
                if (phase25_testing_flag == true) {
                    struct vow_recog_device_info *tinfo;

                    /* Allocate memory for pthread_create() arguments */
                    tinfo = (struct vow_recog_device_info *)calloc(1, sizeof(struct vow_recog_device_info));
                    if (tinfo == NULL) {
                        ALOGE("%s, calloc error", __func__);
                    } else {
                        tinfo->vow_recog_thread_id = 0;
                        tinfo->vow_data_read_thread_id = 0;
                        tinfo->buffer_write = 0;
                        tinfo->buffer_read = 0;
                        tinfo->already_read = false;
                        tinfo->pVoiceBuffer = new short[(VOW_VOICE_SIZE >> 1)];
                        tinfo->model_handle = stdev->model_handle;
                        tinfo->recognition_callback = stdev->recognition_callback;
                        tinfo->recognition_cookie = stdev->recognition_cookie;
                        tinfo->lock = stdev->lock;
                        pthread_create(&tinfo->vow_recog_thread_id, (const pthread_attr_t *) NULL,
                                       voice_recognize_thread_loop, tinfo);
                        ALOGI("+pthread_join_recog");
                        pthread_join(tinfo->vow_recog_thread_id, NULL);
                        ALOGI("-pthread_join_recog");
                        delete[] tinfo->pVoiceBuffer;
                        free(tinfo);
                    }
                    P25_entry_count++;
                    if (P25_Recog_result == RECOG_PASS) {
                        P25_recog_ok_count++;
                        ALOGI("[P2.5]enter_P25_cnt: %d, enter_P3_cnt: %d\n",
                              P25_entry_count, P25_recog_ok_count);
                    } else {
                        ALOGI("[P2.5]Recog is not accept:%d", P25_Recog_result);
                        excute_recognize_pass = false;
                    }
                }
                if (excute_recognize_pass) {

                    if (phase25_testing_flag == true) {
                        pthread_mutex_lock(&stdev->lock);
                    } else {
                        char *data;
                        struct sound_trigger_phrase_recognition_event *event;

                        pthread_mutex_lock(&stdev->lock);
                        data = (char *)calloc(1, sizeof(struct sound_trigger_phrase_recognition_event) + 1);
                        event = (struct sound_trigger_phrase_recognition_event *)data;
                        event->common.status = RECOGNITION_STATUS_SUCCESS;
                        event->common.type = SOUND_MODEL_TYPE_KEYPHRASE;
                        event->common.model = stdev->model_handle;
                        event->common.capture_available = true;
                        event->common.audio_config = AUDIO_CONFIG_INITIALIZER;
                        event->common.audio_config.sample_rate = 16000;
                        event->common.audio_config.channel_mask = AUDIO_CHANNEL_IN_MONO;
                        event->common.audio_config.format = AUDIO_FORMAT_PCM_16_BIT;
                        event->num_phrases = 1;
                        event->phrase_extras[0].id = mphrase_extrasid;
                        event->phrase_extras[0].recognition_modes = RECOGNITION_MODE_VOICE_TRIGGER;
                        event->phrase_extras[0].confidence_level = 100;
                        event->phrase_extras[0].num_levels = 1;
                        event->phrase_extras[0].levels[0].level = 100;
                        event->phrase_extras[0].levels[0].user_id = 0;
                        event->common.data_offset = sizeof(struct sound_trigger_phrase_recognition_event);
                        event->common.data_size = 1;
                        if (stdev->recognition_callback != NULL) {
                            stdev->recognition_callback(&event->common, stdev->recognition_cookie);
                        }
                        ALOGI("phrase_extras[0].id %d", mphrase_extrasid);
                        ALOGI("capture_available %d", event->common.capture_available);
                        ALOGI("%s send callback model %d", __func__, stdev->model_handle);
                        free(data);
                    }
                    sound_trigger_running_state = 0;
                    stdev->recognition_callback = NULL;
                    stdev->callback_thread = 0;
                    pthread_mutex_unlock(&stdev->lock);
                    pthread_exit(NULL);
                    break;
                }
            } else if ((m_sINTData.eint_status == VOW_EINT_DISABLE) && (stoprecognition == VOW_EINT_RETRY)) {
                pthread_mutex_lock(&stdev->lock);
                ALOGD("sound_trigger callback m_sINTData.eint_status %d stoprecognition %d",
                      m_sINTData.eint_status, stoprecognition);
                sound_trigger_running_state = 0;
                stdev->recognition_callback = NULL;
                stdev->callback_thread = 0;
                pthread_mutex_unlock(&stdev->lock);
                pthread_exit(NULL);
                break;
            }
        } else {
            usleep(300 * 1000); // sleep 300msec
        }
    }
exit:
    pthread_mutex_lock(&stdev->lock);
    sound_trigger_running_state = 0;
    stdev->recognition_callback = NULL;
    stdev->callback_thread = 0;
    ALOGI("Exit sound_trigger callback_thread_loop");
    pthread_mutex_unlock(&stdev->lock);
    pthread_exit(NULL);
    return NULL;
}

static int stdev_get_properties(const struct sound_trigger_hw_device *dev,
                                struct sound_trigger_properties *properties)
{
    //struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    (void)dev; // no-op use of dev

    ALOGI("%s", __func__);
    if (properties == NULL) {
        return -EINVAL;
    }
    memcpy(properties, &hw_properties, sizeof(struct sound_trigger_properties));
    return 0;
}

static int stdev_load_sound_model(const struct sound_trigger_hw_device *dev,
                                  struct sound_trigger_sound_model *sound_model,
                                  sound_model_callback_t callback,
                                  void *cookie,
                                  sound_model_handle_t *handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    sound_trigger_uuid_t uuid;

    pthread_mutex_lock(&stdev->lock);
    if (handle == NULL || sound_model == NULL) {
        status = -EINVAL;
        pthread_mutex_unlock(&stdev->lock);
        return status;
    }
    if (sound_model->data_size == 0 ||
        sound_model->data_offset < sizeof(struct sound_trigger_sound_model)) {
        status = -EINVAL;
        pthread_mutex_unlock(&stdev->lock);
        return status;
    }
    if (sound_model->type == SOUND_MODEL_TYPE_GENERIC) {
        ALOGI("SOUND_MODEL_TYPE_GENERIC is not allow for soundtrigger HAL return");
        status = -EINVAL;
        pthread_mutex_unlock(&stdev->lock);
        return status;
    }
    uuid = sound_model->uuid;
    ALOGI("%s, stdev:%p, model type:%d, uuid.timeHi:%d", __func__, stdev, sound_model->type, uuid.timeHiAndVersion);
    if (stdev->model_handle == 1) {
        status = -ENOSYS;
        pthread_mutex_unlock(&stdev->lock);
        return status;
    }
    char *data = (char *)sound_model + sound_model->data_offset;
    ALOGI("Load model: data size %d, data offset %d, data1 %d - %d",
          sound_model->data_size, sound_model->data_offset, data[0], data[sound_model->data_size - 1]);
    stdev->model_handle = 1;
    stdev->recognition_callback = NULL;
    stdev->sound_model_callback = callback;
    stdev->sound_model_cookie = cookie;

    *handle = stdev->model_handle;

    //load initial vow
    ALOGI("Initial VOW");
    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY);
    }
    if (mFd < 0) {
        ALOGI("open device fail!%s\n", strerror(errno));
    } else {
        if (ioctl(mFd, VOW_SET_CONTROL, (unsigned long)VOWControlCmd_Init) < 0) {
            ALOGI("IOCTL VOW_SET_CONTROL ERR");
        }
    }
    ALOGV("Start Load init Model");
    if (uuid.node[0]=='M'
     && uuid.node[1]=='T'
     && uuid.node[2]=='K'
     && uuid.node[3]=='I'
     && uuid.node[4]=='N'
     && uuid.node[5]=='C') {
        //load init model
#if MTK_VOW_SUPPORT
        ParsedDataSizes vow_rtn_parsed_model_info;
        FileData vow_model_info;

        if ((phase2_testing_flag == true) || (phase25_testing_flag == true)) {
            ALOGI("phase2(%d) & phase2.5(%d) model not null", (int)phase2_testing_flag,
                                                              (int)phase25_testing_flag);
            status = -EINVAL;
            pthread_mutex_unlock(&stdev->lock);
            return status;
        }

        vow_model_info.length = sound_model->data_size;
        vow_model_info.beginPtr = data;

        if (VOWE_AP_testing_getModelSize(&vow_rtn_parsed_model_info, vow_model_info) == vowe_bad) {
            ALOGI("[ASP API] getModelSize error");
            status = -EINVAL;
            pthread_mutex_unlock(&stdev->lock);
            return status;
        }

        ALOGI("AP ModelSize = %d, DSP ModelSize = %d", vow_rtn_parsed_model_info.apSize,
                                                       vow_rtn_parsed_model_info.dspSize);

        if (vow_rtn_parsed_model_info.dspSize > ALLOCATE_MEMORY_SIZE_MAX) {
            ALOGI("the dsp memory size need to allocate is more than 60K!!!");
            status = -EINVAL;
            pthread_mutex_unlock(&stdev->lock);
            return status;
        }
        if (vow_rtn_parsed_model_info.dspSize > 0) {
            pPhase2_ModelInfo = new char[vow_rtn_parsed_model_info.dspSize];
            if (pPhase2_ModelInfo == NULL) {
                ALOGI("p2 setVoiceUBMFile allocate memory fail!!!");
                status = -EINVAL;
                pthread_mutex_unlock(&stdev->lock);
                return status;
            }
            phase2_testing_flag = true;
        }
        if (vow_rtn_parsed_model_info.apSize > 0) {
            pPhase25_ModelInfo = new char[vow_rtn_parsed_model_info.apSize];
            if (pPhase25_ModelInfo == NULL) {
                ALOGI("p25+sid setVoiceUBMFile allocate memory fail!!!");
                status = -EINVAL;
                pthread_mutex_unlock(&stdev->lock);
                return status;
            }
            phase25_testing_flag = true;
        }
        if ((phase2_testing_flag == true) || (phase25_testing_flag == true)) {
            parsed_model_data.apData.length = vow_rtn_parsed_model_info.apSize;
            parsed_model_data.apData.beginPtr = pPhase25_ModelInfo;
            parsed_model_data.dspData.length = vow_rtn_parsed_model_info.dspSize;
            parsed_model_data.dspData.beginPtr = pPhase2_ModelInfo;
            ALOGV("AP = [%p]%d, DSP = [%p]%d", parsed_model_data.apData.beginPtr,
                                               parsed_model_data.apData.length,
                                               parsed_model_data.dspData.beginPtr,
                                               parsed_model_data.dspData.length);
            if (VOWE_AP_testing_parseModel(&parsed_model_data, vow_model_info) == vowe_bad) {
                ALOGI("[ASP API] parseModel error");
                pthread_mutex_unlock(&stdev->lock);
                return -ENOSYS;
            }

        }
        if (phase2_testing_flag == true) {
            if (mFd < 0) {
                mFd = open("/dev/vow", O_RDONLY);
            }
            if (mFd < 0) {
                ALOGI("open device fail!%s\n", strerror(errno));
            } else {
                struct vow_model_info_t update_model_info;

                update_model_info.id   = 0;
                update_model_info.size = (long)parsed_model_data.dspData.length;
                update_model_info.addr = (long)parsed_model_data.dspData.beginPtr;
                if (ioctl(mFd, VOW_SET_SPEAKER_MODEL, (unsigned long)&update_model_info) < 0) {
                    ALOGI("IOCTL VOW_SET_SPEAKER_MODEL ERR");
                }
            }
            delete[] pPhase2_ModelInfo;
            phase2_testing_flag = false;
        }
        if (phase25_testing_flag == true) {
            ALOGI("[P2.5]Start Load speaker Model");
            /* Load Phase2.5 lib */
            VOWE_AP_testing_init_parameters testing_init_info;
            testing_init_info.micNumber   = 1;
            testing_init_info.refNumber   = 0;
            testing_init_info.frameLength = 400;
            testing_init_info.frameShift  = 160;
            testing_init_info.debugFolder = NULL;
            testing_init_info.apParsedModelData.length = parsed_model_data.apData.length;
            testing_init_info.apParsedModelData.beginPtr = parsed_model_data.apData.beginPtr;
            if (VOWE_AP_testing_init(&testing_init_info) == vowe_bad) {
                ALOGI("[ASP API] AP TestInit error");
            }
        }
        /* reset counter */
        P25_entry_count = 0;
        P25_recog_ok_count = 0;

        ALOGI("Load speaker Model Finish");
#endif  // #if MTK_VOW_SUPPORT
    } else {
        if (mFd < 0) {
            mFd = open("/dev/vow", O_RDONLY);
        }
        if (mFd < 0) {
            ALOGI("open device fail!%s\n", strerror(errno));
        } else {
            struct vow_model_info_t update_model_info;

            update_model_info.id    = 0;
            update_model_info.size  = (long)sound_model->data_size;
            update_model_info.addr  = (long)data;
            if (ioctl(mFd, VOW_SET_SPEAKER_MODEL, (unsigned long)&update_model_info) < 0) {
                ALOGI("IOCTL VOW_SET_SPEAKER_MODEL ERR");
            }
            ALOGI("Load 3rd mode Finish");
        }
    }
    pthread_mutex_unlock(&stdev->lock);
    return status;
}

static int stdev_unload_sound_model(const struct sound_trigger_hw_device *dev,
                                    sound_model_handle_t handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    int cnt = 0;

    ALOGI("Start Unload Model");
    ALOGI("%s handle %d", __func__, handle);
    pthread_mutex_lock(&stdev->lock);
    if (handle != 1) {
        status = -EINVAL;
        goto exit;
    }
    if (stdev->model_handle == 0) {
        status = -ENOSYS;
        goto exit;
    }
    stdev->model_handle = 0;
    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY);
    }
    if (mFd < 0) {
        ALOGI("open device fail!%s", strerror(errno));
    } else {
        if (ioctl(mFd, VOW_CLR_SPEAKER_MODEL, 0) < 0) {
            ALOGI("IOCTL VOW_CLR_SPEAKER_MODEL ERR");
        }
    }
#if MTK_VOW_SUPPORT
    if (phase25_testing_flag == true) {
        if (VOWE_AP_testing_release() == vowe_bad) {
            ALOGI("[ASP API] AP release error");
        }
        if (pPhase25_ModelInfo != NULL) {
            ALOGI("Release P25 sid Model");
            delete[] pPhase25_ModelInfo;
        }
        phase25_testing_flag = false;
    }
#endif  // #if MTK_VOW_SUPPORT

    if (stdev->recognition_callback != NULL) {
        stdev->recognition_callback = NULL;
        pthread_mutex_unlock(&stdev->lock);
        cnt = 0;
        ALOGI("+start wait pth over");
        while (sound_trigger_running_state == 1) {
            usleep(500);
            cnt++;
            ASSERT(cnt < 2000);
        }
        ALOGI("-start wait pth over");
        pthread_mutex_lock(&stdev->lock);
    }
    ALOGI("Unload speaker Model Finish");

exit:
    pthread_mutex_unlock(&stdev->lock);
    return status;
}

static int stdev_start_recognition(const struct sound_trigger_hw_device *dev,
                                   sound_model_handle_t sound_model_handle,
                                   const struct sound_trigger_recognition_config *config,
                                   recognition_callback_t callback,
                                   void *cookie)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    int cnt = 0;

    ALOGI("+%s() sound model %d", __func__, sound_model_handle);
    pthread_mutex_lock(&stdev->lock);

    /* wait for thread close done, then do next start_recognition*/
    if ((stdev->recognition_callback != NULL) && (sound_trigger_running_state != 0)) {
        ALOGI("+ startrecognition wait for thread end");
        pthread_mutex_unlock(&stdev->lock);
        cnt = 0;
        while (sound_trigger_running_state != 0) {
            usleep(500);
            cnt++;
            ASSERT(cnt < 2000);
        }
        pthread_mutex_lock(&stdev->lock);
        ALOGI("- startrecognition wait for thread end");
    }

    if (stdev->model_handle != sound_model_handle) {
        ALOGI("ERROR_1");
        status = -ENOSYS;
        goto exit;
    }
    if (stdev->recognition_callback != NULL) {
        ALOGI("ERROR_2");
        status = -ENOSYS;
        goto exit;
    }
    ALOGV("Start setParameters");
    if (gAudioMTKHardware == NULL) {
        status = soundtrigger_dlopen();
        if (status == -ENOSYS) {
            ALOGI("%s, dlopen error", __func__);
            goto exit;
        }
    }
    if (gAudioMTKHardware != NULL) {
        gAudioMTKHardware->setParameters(keyMTK_VOW_ENABLE);
    }
    ALOGI("set param done");
    if (config->data_size != 0) {
        char *data = (char *)config + config->data_offset;

        ALOGI("%s data size %d data %d - %d", __func__,
              config->data_size, data[0], data[config->data_size - 1]);
    }

    mphrase_extrasid = config->phrases[0].id;
    ALOGI("startrecognition phrase_extras[0].id %d", mphrase_extrasid);
    stdev->recognition_callback = callback;
    stdev->recognition_cookie = cookie;
    ALOGI("stoprecognition: %d, sound_trigger_running_state:%d",
          stoprecognition, sound_trigger_running_state);
    if ((stoprecognition == 1) && (sound_trigger_running_state != 0)) {
        ALOGI("+ startrecognition wait for thread end ");
        pthread_mutex_unlock(&stdev->lock);
        cnt = 0;
        while (sound_trigger_running_state != 0) {
            usleep(500);
            cnt++;
            ASSERT(cnt < 2000);
        }
        pthread_mutex_lock(&stdev->lock);
        ALOGI("- startrecognition wait for thread end ");
    }
    if (g_phase25_lock == NULL) {
        NEW_ALOCK(g_phase25_lock);
        ALOGI("NEW ALOCK g_phase25_lock = %p", g_phase25_lock);
    }
    stoprecognition = 0;
    if (sound_trigger_running_state != 1) {
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
        pthread_create(&stdev->callback_thread, &attr,
                        callback_thread_loop, stdev);
        pthread_attr_destroy(&attr);

        sound_trigger_running_state = 1;
    }
#if MTK_VOW_SUPPORT
    if (phase25_testing_flag == true) {
        if (VOWE_AP_testing_setArgument(vowe_argid_printLog, 0) == vowe_bad) {
            ALOGI("[P2.5]SetArg error");
        }
        ALOGI("[P2.5]Lib_version:%s", VOWE_AP_testing_version());
    }
#endif  // #if MTK_VOW_SUPPORT
exit:
    pthread_mutex_unlock(&stdev->lock);
    ALOGI("-s%s()", __func__);
    return status;
}

static int stdev_stop_recognition(const struct sound_trigger_hw_device *dev,
                                 sound_model_handle_t sound_model_handle)
{
    struct stub_sound_trigger_device *stdev = (struct stub_sound_trigger_device *)dev;
    int status = 0;
    int cnt = 0;

    pthread_mutex_lock(&stdev->lock);
    ALOGI("+%s()", __func__);
    if (stdev->model_handle != sound_model_handle) {
        status = -ENOSYS;
        goto exit;
    }
    if (stdev->recognition_callback == NULL) {
        status = -ENOSYS;
        goto exit;
    }
    stoprecognition = 1;
    if (phase25_testing_flag == true) {
        pthread_mutex_unlock(&stdev->lock);
        cnt = 0;
        while ((voice_recognize_running_state == 1) || (voice_data_read_running_state == 1)) {
            usleep(500);
            cnt++;
            ASSERT(cnt < 2000);
        }
        pthread_mutex_lock(&stdev->lock);
        ALOGI("[P2.5]End, enter_P25_cnt: %d, enter_P3_cnt: %d\n",
              P25_entry_count, P25_recog_ok_count);
        P25_entry_count = 0;
        P25_recog_ok_count = 0;
    }

    if (gAudioMTKHardware != NULL) {
        gAudioMTKHardware->setParameters(keyMTK_VOW_DISABLE);
    }

    if (g_phase25_lock != NULL) {
        FREE_ALOCK(g_phase25_lock);
        ALOGI("FREE ALOCK g_phase25_lock = %p", g_phase25_lock);
    }

#ifdef VOW_DATA_READ_PCMFILE
    read_filecnt = 0;
#endif
#ifdef VOW_RECOG_PCMFILE
    recog_filecnt = 0;
#endif

    ALOGI("stdev_stop_recognition: sound model %d", sound_model_handle);
    if (stdev->recognition_callback != NULL) {
        stdev->recognition_callback = NULL;
    }
    ALOGI("start to call VOW_CHECK_STATUS");
    if (mFd < 0) {
        mFd = open("/dev/vow", O_RDONLY);
    }
    if (mFd < 0) {
        ALOGI("open device fail!%s", strerror(errno));
    } else {
        ioctl(mFd, VOW_CHECK_STATUS, 0);
    }
    pthread_mutex_unlock(&stdev->lock);
    ALOGI("+start wait pth over");
    cnt = 0;
    while (sound_trigger_running_state == 1) {
        usleep(500);
        cnt++;
        ASSERT(cnt < 2000);
    }
    ALOGI("-start wait pth over");
    pthread_mutex_lock(&stdev->lock);
exit:
    pthread_mutex_unlock(&stdev->lock);
    ALOGI("-%s()", __func__);
    return status;
}


static int stdev_close(hw_device_t *device)
{
    free(device);
    if (AudioHwhndl != NULL) {
        /* close dlopen */
        dlclose(AudioHwhndl);
        AudioHwhndl = NULL;
        gAudioMTKHardware = NULL;
    }
    return 0;
}

static int stdev_open(const hw_module_t* module, const char* name,
                     hw_device_t** device)
{
    struct stub_sound_trigger_device *stdev;

    if (strcmp(name, SOUND_TRIGGER_HARDWARE_INTERFACE) != 0)
        return -EINVAL;

    stdev = (struct stub_sound_trigger_device *)calloc(1, sizeof(struct stub_sound_trigger_device));
    if (!stdev)
        return -ENOMEM;

    stdev->device.common.tag = HARDWARE_DEVICE_TAG;
    stdev->device.common.version = SOUND_TRIGGER_DEVICE_API_VERSION_1_0;
    stdev->device.common.module = (struct hw_module_t *) module;
    stdev->device.common.close = stdev_close;
    stdev->device.get_properties = stdev_get_properties;
    stdev->device.load_sound_model = stdev_load_sound_model;
    stdev->device.unload_sound_model = stdev_unload_sound_model;
    stdev->device.start_recognition = stdev_start_recognition;
    stdev->device.stop_recognition = stdev_stop_recognition;

    pthread_mutex_init(&stdev->lock, (const pthread_mutexattr_t *) NULL);

    *device = &stdev->device.common;
    return 0;
}

static struct hw_module_methods_t hal_module_methods = {
    .open = stdev_open,
};

struct sound_trigger_module HAL_MODULE_INFO_SYM = {
    .common = {
        .tag = HARDWARE_MODULE_TAG,
        .module_api_version = SOUND_TRIGGER_MODULE_API_VERSION_1_0,
        .hal_api_version = HARDWARE_HAL_API_VERSION,
        .id = SOUND_TRIGGER_HARDWARE_MODULE_ID,
        .name = "MTK Audio HW HAL",
        .author = "MTK",
        .methods = &hal_module_methods,
        .dso = NULL,
        .reserved = {0},
    },
};
