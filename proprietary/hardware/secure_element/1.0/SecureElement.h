/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef ANDROID_HARDWARE_SECURE_ELEMENT_V1_0_SECUREELEMENT_H
#define ANDROID_HARDWARE_SECURE_ELEMENT_V1_0_SECUREELEMENT_H

#include <android/hardware/secure_element/1.0/ISecureElement.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>

#include <vendor/mediatek/hardware/radio/3.0/IRadio.h>
#include <vendor/mediatek/hardware/radio/3.0/ISERadioIndication.h>
#include <vendor/mediatek/hardware/radio/3.0/ISERadioResponse.h>

#include <pthread.h>

#define RESPONSE_QUEUE_SIZE (2)

namespace VENDOR_V3_0 = vendor::mediatek::hardware::radio::V3_0;


namespace android {
namespace hardware {
namespace secure_element {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

enum SERadioResponseType {
    SE_ICC_CARD_STATUS = 0,
    SE_ICC_GET_ATR,
    SE_ICC_TRANSMIT_APDU_BASIC,
    SE_ICC_OPEN_CHANNEL,
    SE_ICC_CLOSE_CHANNEL,
    SE_ICC_TRANSMIT_APDU_LOGICAL,
};

struct SERadioResponseData {
    ::android::hardware::radio::V1_0::RadioResponseInfo info;
    ::android::hardware::radio::V1_2::CardStatus cardStatus;
    ::android::hardware::radio::V1_0::IccIoResult result;
    int32_t channelId;
    hidl_vec<int8_t> selectResponse;
    hidl_string response;
};

struct SERadioResponseQueueItem {
    enum SERadioResponseType type;
    bool handled;
    struct SERadioResponseData data;
};

struct SERadioResponse;
struct SERadioIndication;

struct SecureElement : public ISecureElement, public hidl_death_recipient {
    SecureElement(const int& id);
    ~SecureElement();
    void notifyResponse();

    // Methods from ::android::hardware::secure_element::V1_0::ISecureElement follow.
    Return<void> init(const sp<::android::hardware::secure_element::V1_0::ISecureElementHalCallback>& clientCallback) override;
    Return<void> getAtr(getAtr_cb _hidl_cb) override;
    Return<bool> isCardPresent() override;
    Return<void> transmit(const hidl_vec<uint8_t>& data, transmit_cb _hidl_cb) override;
    Return<void> openLogicalChannel(const hidl_vec<uint8_t>& aid, uint8_t p2, openLogicalChannel_cb _hidl_cb) override;
    Return<void> openBasicChannel(const hidl_vec<uint8_t>& aid, uint8_t p2, openBasicChannel_cb _hidl_cb) override;
    Return<::android::hardware::secure_element::V1_0::SecureElementStatus> closeChannel(uint8_t channelNumber) override;

    // Methods from ::android::hidl::hardware::hidl_death_recipient follow.
    void serviceDied(uint64_t /* cookie */, const ::android::wp<::android::hidl::base::V1_0::IBase>& /* who */) override;

private:
    int mUiccId;
    hidl_string mSlotName;
    int32_t mSerial;
    sp<V1_0::ISecureElementHalCallback> mCallback_V1_0;
    sp<VENDOR_V3_0::IRadio> mMtkRadio;
    sp<SERadioResponse> mRadioRsp;
    sp<SERadioIndication> mRadioInd;
    pthread_mutex_t mMainMutex;
    ::android::hardware::radio::V1_0::CardState mCardState;

    // Private methods follow.
    bool transmitInternal(hidl_vec<uint8_t>& respVec, const hidl_vec<uint8_t>& data);
    bool getIccCardStatus();
    bool initRadioService();
    bool initResponseFunctions();
    void byteVectorToHexString(hidl_string& str, const hidl_vec<uint8_t>& vec, size_t offset, size_t length);
    void byteVectorToHexString(hidl_string& str, const hidl_vec<uint8_t>& vec);
    uint8_t hexCharToByte(const char& hex);
    void hexStringToByteVector(hidl_vec<uint8_t>& vec, const hidl_string& str, size_t offset, size_t length);
    void hexStringToByteVector(hidl_vec<uint8_t>& vec, const hidl_string& str);
    void iccIoResultToByteVector(hidl_vec<uint8_t>& vec, const ::android::hardware::radio::V1_0::IccIoResult& result);
    uint8_t clearChannelNumber(const uint8_t& cla);
    uint8_t parseChannelNumber(const uint8_t& cla);
    bool createSimApdu(::android::hardware::radio::V1_0::SimApdu& apdu, const hidl_vec<uint8_t>& data);
    int32_t getSerialAndIncrement();
    bool waitResponse(SERadioResponseData& data, enum SERadioResponseType type, int32_t serial);
};

struct SERadioResponse : public VENDOR_V3_0::ISERadioResponse {
    SERadioResponse(sp<SecureElement> se, const hidl_string& name);
    ~SERadioResponse();
    bool removeResponse(struct SERadioResponseData& data, enum SERadioResponseType type, int32_t serial);

    // Methods from ::vendor::mediatek::hardware::radio::V3_0::ISERadioResponse follow.
    Return<void> getIccCardStatusResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info, const ::android::hardware::radio::V1_2::CardStatus& cardStatus) override;
    Return<void> getATRResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info, const hidl_string& response) override;
    Return<void> iccTransmitApduBasicChannelResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info, const ::android::hardware::radio::V1_0::IccIoResult& result) override;
    Return<void> iccOpenLogicalChannelResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info, int32_t channelId, const hidl_vec<int8_t>& selectResponse) override;
    Return<void> iccCloseLogicalChannelResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info) override;
    Return<void> iccTransmitApduLogicalChannelResponse(const ::android::hardware::radio::V1_0::RadioResponseInfo& info, const ::android::hardware::radio::V1_0::IccIoResult& result) override;

private:
    sp<SecureElement> mSE;
    hidl_string mSlotName;
    hidl_array<SERadioResponseQueueItem, RESPONSE_QUEUE_SIZE> mQueue;
    uint8_t mQueueNextIndex;
    pthread_mutex_t mQueueMutex;

    // Private methods follow.
    void addToQueue(enum SERadioResponseType type, const SERadioResponseData& data);
};

struct SERadioIndication : public VENDOR_V3_0::ISERadioIndication {
    SERadioIndication(const SecureElement *se, hidl_string name);

private:
    const SecureElement *mSE;
    hidl_string mSlotName;
};

}  // namespace implementation
}  // namespace V1_0
}  // namespace secure_element
}  // namespace hardware
}  // namespace android

#endif  // ANDROID_HARDWARE_SECURE_ELEMENT_V1_0_SECUREELEMENT_H
