/* MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include "SecureElement.h"

#define LOG_TAG "MtkUiccSEHal"
#include <log/log.h>

#include "selog.h"
#include <stdio.h>
#include <string.h>

#define SUPPORT_BASIC_CHANNEL 0 /* MTK not support */

#define MAX_INSTANCE (4)
#define SLOT_NAME_LEN (8)

namespace android {
namespace hardware {
namespace secure_element {
namespace V1_0 {
namespace implementation {

SecureElement::SecureElement(const int& id) {
    if (id <= 0 || id > MAX_INSTANCE) {
        mUiccId = -1;

        SELOGE("not support UICC%d/SIM%d", id, id);
    } else {
        mUiccId = id;

        char name[SLOT_NAME_LEN];
        snprintf(name, SLOT_NAME_LEN, "se%d", mUiccId);
        mSlotName = name;

        mSerial = 0;
        mCallback_V1_0 = nullptr;
        mRadioInd = nullptr;
        mRadioRsp = nullptr;
        mMtkRadio = nullptr;

        mCardState = ::android::hardware::radio::V1_0::CardState::ABSENT;

        mMainMutex = PTHREAD_MUTEX_INITIALIZER;
        pthread_mutex_lock(&mMainMutex);

        SELOGD("created for UICC%d/SIM%d", mUiccId, mUiccId);
    }
}

SecureElement::~SecureElement() {
    SELOGD("destroy for UICC%d/SIM%d", mUiccId, mUiccId);
    if (mUiccId != -1) {
        pthread_mutex_unlock(&mMainMutex);
        pthread_mutex_destroy(&mMainMutex);
    }
    if (mMtkRadio != nullptr) {
        mMtkRadio->setResponseFunctionsSE(nullptr, nullptr);
        mRadioInd = nullptr;
        mRadioRsp = nullptr;
        mMtkRadio = nullptr;
    }
    mCallback_V1_0 = nullptr;
}

void SecureElement::notifyResponse() {
    pthread_mutex_unlock(&mMainMutex); // notify
}

// Methods from ::android::hardware::secure_element::V1_0::ISecureElement follow.
Return<void> SecureElement::init(const sp<::android::hardware::secure_element::V1_0::ISecureElementHalCallback>& clientCallback) {
    SELOGD("enter");

    if (clientCallback == nullptr) {
        SELOGD("clientCallback == nullptr");
        return Void();
    } else {
        if (mCallback_V1_0 != nullptr)
            mCallback_V1_0->unlinkToDeath(this);

        mCallback_V1_0 = clientCallback;
        if (!mCallback_V1_0->linkToDeath(this, 0)) {
            SELOGE("failed to register death notification");
            mCallback_V1_0 = nullptr;
        }
    }

    bool result = initRadioService();
    if (mCallback_V1_0 != nullptr) {
        if (result) {
            mCallback_V1_0->onStateChange(true);
        } else {
            SELOGD("radio is not ready");
            mCallback_V1_0->onStateChange(false);
        }
    }

    return Void();
}

Return<void> SecureElement::getAtr(getAtr_cb _hidl_cb) {
    hidl_vec<uint8_t> respVec;

    if (!initRadioService()) {
        SELOGD("radio is not ready");
        _hidl_cb(respVec);
        return Void();
    }

    int32_t serial = getSerialAndIncrement();
    SELOGD("[%d] > getAtr", serial);
    mMtkRadio->getATR(serial);

    struct SERadioResponseData respData;
    bool result = waitResponse(respData, SE_ICC_GET_ATR, serial);
    if (result == false) {
        SELOG_ALWAYS_FATAL("cannot get response [%d: #%d]", SE_ICC_GET_ATR, serial);
    } else {
        hexStringToByteVector(respVec, respData.response);
        SELOGD("[%d] < getAtr %s err=%d", serial, respData.response.c_str(), respData.info.error);
    }

    _hidl_cb(respVec);

    return Void();
}

Return<bool> SecureElement::isCardPresent() {
    if (!initRadioService()) {
        SELOGD("radio is not ready");
        return false;
    }

    getIccCardStatus();

    bool present = (mCardState == ::android::hardware::radio::V1_0::CardState::PRESENT);
    SELOGD("present = %d", present);

    return present;
}

Return<void> SecureElement::transmit(const hidl_vec<uint8_t>& data, transmit_cb _hidl_cb) {
    hidl_vec<uint8_t> respVec;

    hidl_string dataString;
    byteVectorToHexString(dataString, data);

    SELOGD("transmit %s", dataString.c_str());

    if (transmitInternal(respVec, data) == false)
        respVec.resize(0);

    _hidl_cb(respVec);

    return Void();
}

Return<void> SecureElement::openLogicalChannel(const hidl_vec<uint8_t>& aid, uint8_t p2, openLogicalChannel_cb _hidl_cb) {
    SecureElementStatus status = SecureElementStatus::IOERROR;
    LogicalChannelResponse response;
    response.channelNumber = 0xFF;

    if (!initRadioService()) {
        SELOGD("radio is not ready");

        _hidl_cb(response, status);
        return Void();
    }

    hidl_string aidString;
    byteVectorToHexString(aidString, aid);

    int32_t serial = getSerialAndIncrement();
    SELOGD("[%d] > open logical %s p2=%u", serial, aidString.c_str(), p2);
    mMtkRadio->iccOpenLogicalChannel(serial, aidString, (int32_t) p2);

    struct SERadioResponseData respData;
    bool result = waitResponse(respData, SE_ICC_OPEN_CHANNEL, serial);
    if (result == false) {
        SELOG_ALWAYS_FATAL("cannot get response [%d: #%d]", SE_ICC_OPEN_CHANNEL, serial);
    } else {
        switch (respData.info.error) {
        case ::android::hardware::radio::V1_0::RadioError::MISSING_RESOURCE:
            status = SecureElementStatus::CHANNEL_NOT_AVAILABLE;
            break;
        case ::android::hardware::radio::V1_0::RadioError::NO_SUCH_ELEMENT :
            status = SecureElementStatus::NO_SUCH_ELEMENT_ERROR;
            break;
        default:
            break;
        }

        response.channelNumber = respData.channelId;
        response.selectResponse.resize(respData.selectResponse.size());
        for (size_t i = 0; i < respData.selectResponse.size(); ++i)
            response.selectResponse[i] = (uint8_t) respData.selectResponse[i];

        hidl_string respString;
        byteVectorToHexString(respString, response.selectResponse);
        SELOGD("[%d] < open logical [%d]%s err=%d", serial, response.channelNumber, respString.c_str(), respData.info.error);

        if (respData.selectResponse.size() >= 2) {
            uint8_t sw1 = response.selectResponse[response.selectResponse.size() - 2];
            uint8_t sw2 = response.selectResponse[response.selectResponse.size() - 1];
            if (sw1 == 0x90 && sw2 == 0x00)
                status = SecureElementStatus::SUCCESS;
            else if (sw1 == 0x6A && sw2 == 0x86)
                status = SecureElementStatus::UNSUPPORTED_OPERATION;
        }
    }

    if (status != SecureElementStatus::SUCCESS)
        response.selectResponse.resize(0);

    _hidl_cb(response, status);

    return Void();
}

Return<void> SecureElement::openBasicChannel(const hidl_vec<uint8_t>& aid, uint8_t p2, openBasicChannel_cb _hidl_cb) {
#if SUPPORT_BASIC_CHANNEL

    hidl_vec<uint8_t> data;
    data.resize(aid.size() + 4);
    data[0] = 0x00;                 // basic channel
    data[1] = 0xA4;                 // INS
    data[2] = 0x04;                 // P1
    data[3] = p2;                   // P2
    data[4] = (uint8_t) aid.size(); // Lc (P3)
    for (size_t i = 0; i < aid.size(); ++i)
        data[5 + i] = aid[i];

    hidl_vec<uint8_t> respVec;

    hidl_string aidString;
    byteVectorToHexString(aidString, aid);

    SELOGD("open basic %s p2=%u", aidString.c_str(), p2);

    SecureElementStatus status = SecureElementStatus::IOERROR;
    if (transmitInternal(respVec, data) == true) {
        if (respVec.size() >= 2) {
            uint8_t sw1 = respVec[respVec.size() - 2];
            uint8_t sw2 = respVec[respVec.size() - 1];
            if (sw1 == 0x90 && sw2 == 0x00)
                status = SecureElementStatus::SUCCESS;
            else if ((sw1 == 0x6A && sw2 == 0x81) ||
                    (sw1 == 0x6A && sw2 == 0x84))
                status = SecureElementStatus::CHANNEL_NOT_AVAILABLE;
            else if ((sw1 == 0x6A && sw2 == 0x82) ||
                    (sw1 == 0x69 && sw2 == 0x85) ||
                    (sw1 == 0x69 && sw2 == 0x99))
                status = SecureElementStatus::NO_SUCH_ELEMENT_ERROR;
            else if (sw1 == 0x6A && sw2 == 0x86)
                status = SecureElementStatus::UNSUPPORTED_OPERATION;
        }
    }

    _hidl_cb(respVec, status);

#else /* SUPPORT_BASIC_CHANNEL */

    hidl_string aidString;
    byteVectorToHexString(aidString, aid);
    SELOGD("not support open basic %s p2=%u", aidString.c_str(), p2);

    hidl_vec<uint8_t> respVec;
    _hidl_cb(respVec, SecureElementStatus::CHANNEL_NOT_AVAILABLE);

#endif /* SUPPORT_BASIC_CHANNEL */

    return Void();
}

Return<::android::hardware::secure_element::V1_0::SecureElementStatus> SecureElement::closeChannel(uint8_t channelNumber) {
    SecureElementStatus status = SecureElementStatus::FAILED;

    if (!initRadioService()) {
        SELOGD("radio is not ready");

        return status;
    }

    int32_t serial = getSerialAndIncrement();
    SELOGD("[%d] > close [%u]", serial, channelNumber);
    mMtkRadio->iccCloseLogicalChannel(serial, (int32_t) channelNumber);

    struct SERadioResponseData respData;
    bool result = waitResponse(respData, SE_ICC_CLOSE_CHANNEL, serial);
    if (result == false) {
        SELOG_ALWAYS_FATAL("cannot get response [%d: #%d]", SE_ICC_CLOSE_CHANNEL, serial);
    } else {
        if (respData.info.error == ::android::hardware::radio::V1_0::RadioError::NONE)
            status = SecureElementStatus::SUCCESS;

        SELOGD("[%d] < close [%u] err=%d", serial, channelNumber, respData.info.error);
    }

    return status;
}


// Methods from ::android::hidl::hardware::hidl_death_recipient follow.
void SecureElement::serviceDied(uint64_t /* cookie */, const ::android::wp<::android::hidl::base::V1_0::IBase>& /* who */) {
    if (mCallback_V1_0 != nullptr) {
        SELOGD("unlinkToDeath");

        mCallback_V1_0->unlinkToDeath(this);
        mCallback_V1_0 = nullptr;
    }
}


// Private methods follow.
bool SecureElement::transmitInternal(hidl_vec<uint8_t>& respVec, const hidl_vec<uint8_t>& data) {
    if (!initRadioService()) {
        SELOGD("radio is not ready");

        return false;
    }

    hidl_string dataString;
    byteVectorToHexString(dataString, data);

    ::android::hardware::radio::V1_0::SimApdu apdu;
    if(createSimApdu(apdu, data) == false) {
        return false;
    }

    enum SERadioResponseType type;
    int32_t serial = getSerialAndIncrement();
    SELOGD("[%d] > transmit [%d] %s", serial, apdu.sessionId, dataString.c_str());
    if (apdu.sessionId != 0) {
        mMtkRadio->iccTransmitApduLogicalChannel(serial, apdu);
        type = SE_ICC_TRANSMIT_APDU_LOGICAL;
    } else {
        mMtkRadio->iccTransmitApduBasicChannel(serial, apdu);
        type = SE_ICC_TRANSMIT_APDU_BASIC;
    }

    struct SERadioResponseData respData;
    bool result = waitResponse(respData, type, serial);
    if (result == false) {
        SELOG_ALWAYS_FATAL("cannot get response [%d: #%d]", type, serial);
        return false;
    } else {
        iccIoResultToByteVector(respVec, respData.result);

        hidl_string respString;
        byteVectorToHexString(respString, respVec);
        SELOGD("[%d] < transmit [%d] %s err=%d", serial, apdu.sessionId, respString.c_str(), respData.info.error);
    }

    return true;
}

bool SecureElement::getIccCardStatus() {
    int32_t serial = getSerialAndIncrement();
    SELOGD("[%d] > getIccCardStatus", serial);
    mMtkRadio->getIccCardStatus(serial);

    struct SERadioResponseData respData;
    bool result = waitResponse(respData, SE_ICC_CARD_STATUS, serial);
    if (result == false) {
        SELOG_ALWAYS_FATAL("cannot get response [%d: #%d]", SE_ICC_CARD_STATUS, serial);
        return false;
    } else {
        mCardState = respData.cardStatus.base.cardState;

        SELOGD("[%d] < getIccCardStatus %d err=%d", serial, mCardState, respData.info.error);
    }

    return true;
}

bool SecureElement::initRadioService() {
    if (mMtkRadio == nullptr) {
        mMtkRadio = VENDOR_V3_0::IRadio::tryGetService(mSlotName);
        if (mMtkRadio == nullptr) {
            SELOGE("get MTK IRadio failed");
            return false;
        }
        if (initResponseFunctions() == false) {
            SELOGE("init ResponseFunctions failed");
            return false;
        }
        getIccCardStatus();
    }

    return true;
}

bool SecureElement::initResponseFunctions() {
    if (mMtkRadio == nullptr) {
        SELOGE("mMtkRadio == nullptr");
        return false;
    }

    mRadioRsp = new SERadioResponse(this, mSlotName);
    mRadioInd = new SERadioIndication(this, mSlotName);

    SELOGD("setResponseFunctionsSE (%p, %p)", mRadioRsp.get(), mRadioInd.get());
    mMtkRadio->setResponseFunctionsSE(mRadioRsp, mRadioInd);

    return true;
}

void SecureElement::byteVectorToHexString(hidl_string& str, const hidl_vec<uint8_t>& vec, size_t offset, size_t length) {
    if (length == 0) {
        str.clear();
        return;
    }

    char buff[length * 2 + 1];
    memset(buff, 0, sizeof(buff));
    for (size_t i = 0; i < length; ++i) {
        snprintf(buff + i * 2, 3, "%02X", vec[i + offset]);
    }
    str = buff;
}

void SecureElement::byteVectorToHexString(hidl_string& str, const hidl_vec<uint8_t>& vec) {
    byteVectorToHexString(str, vec, 0, vec.size());
}

uint8_t SecureElement::hexCharToByte(const char& hex) {
    if (hex >= 'a')
        return hex - 'a' + 10;
    else if (hex >= 'A')
        return hex - 'A' + 10;

    return hex - '0';
}

void SecureElement::hexStringToByteVector(hidl_vec<uint8_t>& vec, const hidl_string& str, size_t offset, size_t length) {
    size_t vecSize = (length - offset) / 2;
    vec.resize(vecSize);
    for (size_t i = 0; i < vecSize; ++i) {
        char hex1 = str.c_str()[i * 2 + offset];
        char hex2 = str.c_str()[i * 2 + 1 + offset];
        vec[i] = hexCharToByte(hex1) * 16 + hexCharToByte(hex2);
    }
}

void SecureElement::hexStringToByteVector(hidl_vec<uint8_t>& vec, const hidl_string& str) {
    hexStringToByteVector(vec, str, 0, str.size());
}

void SecureElement::iccIoResultToByteVector(hidl_vec<uint8_t>& vec, const ::android::hardware::radio::V1_0::IccIoResult& result) {
    char buff[result.simResponse.size() + 4 + 1];
    memset(buff, 0, sizeof(buff));
    snprintf(buff, sizeof(buff), "%s%02X%02X", result.simResponse.c_str(), result.sw1, result.sw2);
    hidl_string str = buff;
    hexStringToByteVector(vec, str);
}

uint8_t SecureElement::clearChannelNumber(const uint8_t& cla) {
    bool isFirstInterindustry = (cla & 0x40) == 0x00;
    if (isFirstInterindustry)
        return cla & 0xFC;
    else
        return cla & 0xF0;
}

uint8_t SecureElement::parseChannelNumber(const uint8_t& cla) {
    bool isFirstInterindustry = (cla & 0x40) == 0x00;
    if (isFirstInterindustry)
        return cla & 0x03;
    else
        return (cla & 0x0F) + 4;
}

bool SecureElement::createSimApdu(::android::hardware::radio::V1_0::SimApdu& apdu, const hidl_vec<uint8_t>& data) {
    if (data.size() < 4) {
        SELOGD("APDU data length is too short (%zu)", data.size());
        return false;
    }
    apdu.data.clear();

    apdu.sessionId = parseChannelNumber(data[0]); // channel
    apdu.cla = clearChannelNumber(data[0]);
    apdu.instruction = data[1];
    apdu.p1 = data[2];
    apdu.p2 = data[3];
    if (data.size() == 4) {
        apdu.p3 = -1; /* Case 1 */
    } else if (data.size() >= 5) {
        apdu.p3 = data[4]; /* Case 2 */

        if (data.size() > 5) /* Case 3 & 4 */
            byteVectorToHexString(apdu.data, data, 5, data.size() - 5);
    }

    SELOGD("[%d] cla=0x%02X ins=0x%02X p1=0x%02X p2=0x%02X p3=0x%02X data=%s",
        apdu.sessionId, apdu.cla, apdu.instruction, apdu.p1, apdu.p2, apdu.p3, apdu.data.c_str());

    return true;
}

int32_t SecureElement::getSerialAndIncrement() {
    return mSerial++;
}

bool SecureElement::waitResponse(SERadioResponseData& data, enum SERadioResponseType type, int32_t serial) {
    pthread_mutex_lock(&mMainMutex); // wait

    return mRadioRsp->removeResponse(data, type, serial);
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace secure_element
}  // namespace hardware
}  // namespace android
