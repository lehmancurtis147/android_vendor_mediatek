#######################################################
# [Note]
#   Must define the following environment variable
#   before use this makefile content
#
# my_platform
# - $(shell echo $(MTK_PLATFORM) | tr A-Z a-z )
#
#######################################################

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE       := tuner.config
LOCAL_MODULE_TAGS  := optional
LOCAL_MODULE_CLASS := ETC
LOCAL_SRC_FILES    := $(my_platform)/tuner.config
LOCAL_MODULE_PATH  := $(TARGET_OUT_VENDOR)/etc
LOCAL_PREFER_SOURCE_PATH := $(MTK_PATH_SOURCE)/hardware/gpu_nn/android-nn-driver/mtk_device
include $(MTK_PREBUILT)
