/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PreReleaseRequest"
//
#include <utils/Mutex.h> // android::Mutex
#include <utils/Condition.h>
//
#include <utils/Thread.h>
//
#include <sys/prctl.h>
#include <sys/resource.h>
#include <system/thread_defs.h>
//
#include <future>
//
#include <mtkcam3/pipeline/pipeline/IPipelineNode.h>
#include <mtkcam3/pipeline/prerelease/IPreReleaseRequest.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>
#include <mtkcam3/pipeline/hwnode/NodeId.h>
//
#include <mtkcam3/feature/eventCallback/EventCallbackMgr.h>
//
#include "MyUtils.h"
//
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::prerelease;
/******************************************************************************
 *
 ******************************************************************************/
#define PRERELEASEREQUEST_NAME       ("Cam@PreReleaseRequest")
#define MAX_PRE_RELEASE              (10)

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

/******************************************************************************
 *
 ******************************************************************************/
class PreReleaseRequestMgr : public IPreReleaseRequestMgr
{
    enum Status {
        RELEASE = 0,
        PRERELEASE
    };

    struct  RequestItem
    {
        uint32_t       frameNo;
        uint32_t       preReleaseUid;
        Status         status;
        int32_t        imgReaderID;
    };

public:
                PreReleaseRequestMgr() {mReqItems.clear();};
  virtual      ~PreReleaseRequestMgr() {};
  virtual sp<IPreReleaseRequest>
                createPreRelease(sp<PipelineContext> pContext);
  virtual bool  applyPreRelease(android::sp<IPipelineFrame>const& pFrame);
  virtual bool  applyRelease(android::sp<IPipelineFrame>const& pFrame);
  virtual void  setImageReaderID(uint32_t id);
  virtual void  setPreReleaseRequest(android::sp<IPreReleaseRequest> pPreRelease);
  virtual void  setRequestFinished(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID);
  virtual void  addRequest(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID);
  virtual void  uninit();

private:
    mutable android::Mutex                         mLock;
    wp<IPreReleaseRequest>                         mwpPreRelease;
    wp<IPreReleaseRequest>                         mwpPreReleaseP1; // for P1 uninit
    int32_t                                        mImgReaderID = -1;

    mutable android::Mutex                         mFutureLock;
    std::shared_future<void>                       mvFutures[MAX_PRE_RELEASE];
    uint32_t                                       mIdx = 0;
    uint32_t                                       mPreReleaseUid = 0;
    using RequestsT  = Vector<RequestItem>;
    RequestsT                                      mReqItems;
};


/******************************************************************************
 *
 ******************************************************************************/
class PreReleaseRequest : public IPreReleaseRequest
{

public:
                                         PreReleaseRequest(sp<PipelineContext> pContext, uint32_t id, uint32_t uid);
virtual                                 ~PreReleaseRequest(){};

public:
    virtual void                         start();
    virtual void                         uninit();
    virtual void                         waitUntilDrained();
    /* pipelineframe will call this API in ApplyPreRelease */
    virtual void                         applyPreRelease(
                                             sp<IPipelineFrame>const& pFrame
                                         );

protected:   /// data members
    mutable android::Mutex                   mLock;
    android::Condition                       mRequestCond;
    MINT32                                   mLogLevel;

    using PreReleaseBufferT  = KeyedVector<StreamId_T, sp<IStreamBuffer>>;
    using PreReleaseRequestT = KeyedVector<MUINT32, PreReleaseBufferT>;
    MBOOL                                    mbPreRelease = MFALSE;
    PreReleaseRequestT                       mRequests;

    sp<PipelineContext>                      mpContext;
    int32_t                                  mImgReaderID;
    uint32_t                                 mUid = 0;
};


/******************************************************************************
 *
 ******************************************************************************/
IPreReleaseRequestMgr *
IPreReleaseRequestMgr::
getInstance()
{
    static PreReleaseRequestMgr gPreReleaseMgr;
    return &gPreReleaseMgr;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IPreReleaseRequest>
PreReleaseRequestMgr::
createPreRelease(sp<PipelineContext> pContext)
{
    MY_LOGD("+");
    mPreReleaseUid += 1;
    sp<PreReleaseRequest> pPreRelease = new PreReleaseRequest(pContext, mImgReaderID, mPreReleaseUid);
    mwpPreRelease = pPreRelease;

    // create thread to exec pre-release
    {
        Mutex::Autolock _l(mFutureLock);
        mvFutures[mIdx] = std::async( std::launch::async, [](sp<IPreReleaseRequest> pPreRelease) {
                                        CAM_TRACE_NAME(PRERELEASEREQUEST_NAME);
                                        ::prctl(PR_SET_NAME, (unsigned long)PRERELEASEREQUEST_NAME, 0, 0, 0);
                                        //
                                        pPreRelease->waitUntilDrained();
                                      }, pPreRelease
                                    );

        mIdx = (mIdx + 1) % MAX_PRE_RELEASE;
    }
    MY_LOGD("-");
    return pPreRelease;
}


/******************************************************************************
 *
 ******************************************************************************/
bool
PreReleaseRequestMgr::
applyPreRelease(
   sp<IPipelineFrame>const& pFrame
)
{
    MY_LOGD("+ (%d)", pFrame->getRequestNo());
    Mutex::Autolock _l(mLock);
    sp<IPreReleaseRequest> pPreRelease = mwpPreRelease.promote();
    if (pPreRelease) {
        pPreRelease->applyPreRelease(pFrame);
    }
    MY_LOGD("- (%d)", pFrame->getRequestNo());
    return true;
}



/******************************************************************************
 *
 ******************************************************************************/
bool
PreReleaseRequestMgr::
applyRelease(
   sp<IPipelineFrame>const& pFrame
)
{
    Mutex::Autolock _l(mLock);

    if (mImgReaderID == -1){
        return false;
    }

    if (mReqItems.size() > 0){
        for(auto& item : mReqItems){
            if (item.frameNo == pFrame->getRequestNo() && item.preReleaseUid == mPreReleaseUid){
                MY_LOGD("already in mReqItems, skip this frame(%d)", pFrame->getRequestNo());
                return true;
            }
        }
        //add callback to queue until last pre-release finished
        RequestItem ReqItem = {pFrame->getRequestNo(), 0, Status::RELEASE, mImgReaderID};
        mReqItems.add(ReqItem);
        MY_LOGD("mReqItems size(%d)", mReqItems.size());
        return true;
    }

    {
        //callback to AP
        MY_LOGD("callback requestNo(%d), mImgReaderID(%d)", pFrame->getRequestNo(), mImgReaderID);
        EventCallbackMgr::getInstance()->onRequestFinishedCB(mImgReaderID, pFrame->getRequestNo(), 0, 0);
    }
    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequestMgr::
setImageReaderID(
    uint32_t id
)
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);
    mImgReaderID = id;
    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequestMgr::
setPreReleaseRequest(
    sp<IPreReleaseRequest> pPreRelease
)
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);
    mwpPreReleaseP1 = pPreRelease;

    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequestMgr::
uninit()
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);
    sp<IPreReleaseRequest> pPreRelease = mwpPreReleaseP1.promote();
    if (pPreRelease) {
        pPreRelease->uninit();
    }
    MY_LOGD("-");
}

/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequestMgr::
addRequest(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID)
{
    MY_LOGD("+");
    //Mutex::Autolock _l(mLock);
    RequestItem ReqItem = {frameNo, preReleaseUid, Status::PRERELEASE, imgReaderID};
    mReqItems.add(ReqItem);
    MY_LOGD("mReqItems size(%d)", mReqItems.size());
    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequestMgr::
setRequestFinished(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID)
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);

    for(auto& item : mReqItems){
        if (item.frameNo == frameNo && item.preReleaseUid == preReleaseUid){
            MY_LOGD("set status to release (%d)", Status::RELEASE);
            item.status = Status::RELEASE;
            break;
        }
    }
    // start to callback
    bool needCB = true;
    while(needCB == true && mReqItems.size() >0) {
        auto& item = mReqItems[0];
        if(item.status == Status::RELEASE){
            MY_LOGD("callback requestNo(%d)", item.frameNo);
            EventCallbackMgr::getInstance()->onRequestFinishedCB(item.imgReaderID, item.frameNo, 0, 0);
            mReqItems.removeAt(0);
            MY_LOGD("mReqItems size(%d)", mReqItems.size());
        }
        else{
            needCB = false;
        }
    }
    MY_LOGD("-");
}

/******************************************************************************
 *
 ******************************************************************************/
PreReleaseRequest::
PreReleaseRequest(sp<PipelineContext> pContext, uint32_t id, uint32_t uid)
{
    mLogLevel = property_get_int32("vendor.debug.camera.log", 0);
    if ( 0 == mLogLevel ) {
        mLogLevel = property_get_int32("vendor.debug.camera.log.PreReleaseRequest", 0);
    }
    mpContext = pContext;
    mImgReaderID = id;
    mUid = uid;
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequest::
start()
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);
    mbPreRelease = MTRUE;
    mRequestCond.broadcast();
    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequest::
uninit()
{
    MY_LOGD("+, uninit P1Node");
//    Mutex::Autolock _l(mLock);
    auto pNodeMain = mpContext->queryINodeActor(eNODEID_P1Node);
    if (pNodeMain != NULL) {
      pNodeMain->uninit();
    }
    MY_LOGD("main1 done");
    auto pNodeMain2 = mpContext->queryINodeActor(eNODEID_P1Node_main2);
    if (pNodeMain2 != NULL) {
      pNodeMain2->uninit();
    }
    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequest::
waitUntilDrained()
{
    MY_LOGD("+");
    Mutex::Autolock _l(mLock);
    while (mbPreRelease == MFALSE) {
        MY_LOGD("wait+");
        auto ret = mRequestCond.waitRelative(mLock, 10000000000); // timeout: 10 secs
        if (ret != OK) {
            MY_LOGW("timeout!!");
            mbPreRelease = MTRUE;
            break;
        }
        MY_LOGD("wait-");
    }

    MY_LOGD("Request size: %zu", mRequests.size());
    if (mRequests.size() > 0) {
        IPreReleaseRequestMgr::getInstance()->setPreReleaseRequest(this);
    }
    while(mRequests.size() > 0) {
        auto requestNo = mRequests.keyAt(0);
        auto preReleaseBuffers = mRequests.valueAt(0);
        for (size_t i = 0 ; i < preReleaseBuffers.size() ; i++) {
            auto streambuffer = preReleaseBuffers.valueAt(i);
            MY_LOGD("requestNo(%d), preReleaseBuffers.size(%zu), idx(%zu)", requestNo, preReleaseBuffers.size(), i);
            // wait streambuffer done
            if (streambuffer == nullptr){
                MY_LOGD("requestNo(%d) streambuffer is null, idx(%zu)", requestNo, i);
            }
            else{
                streambuffer->waitUserReleaseStatus(LOG_TAG);
            }
        }
        mRequests.removeItemsAt(0);
        IPreReleaseRequestMgr::getInstance()->setRequestFinished(requestNo, mUid, mImgReaderID);
        // callback to AP
        // EventCallbackMgr::getInstance()->onRequestFinishedCB(mImgReaderID, requestNo, 0, 0);
    }

    IPreReleaseRequestMgr::getInstance()->setPreReleaseRequest(nullptr);
    MY_LOGD("-");
}


/******************************************************************************
 *
 ******************************************************************************/
void
PreReleaseRequest::
applyPreRelease(
    sp<IPipelineFrame>const& pFrame
)
{
    auto frameNo = pFrame->getFrameNo();
    auto requestNo = pFrame->getRequestNo();
    MY_LOGD("+ R/F Num: %d/%d", requestNo, frameNo);
    Mutex::Autolock _l(mLock);

    if (mbPreRelease == MTRUE) {
        MY_LOGW("Something wrong");
        return;
    }

    if (mRequests.indexOfKey(requestNo) >= 0) {
        MY_LOGD("requestNo(%d) is already in", requestNo);
        return;
    }
    sp<IStreamInfoSet const> pIStreams, pOStreams;
    sp<IPipelineNodeMap const> pPipelineNodeMap = pFrame->getPipelineNodeMap();
    IStreamBufferSet& rStreamBufferSet = pFrame->getStreamBufferSet();

    for (size_t i = 0 ; i < pPipelineNodeMap->size() ; i++)
    {
        PreReleaseBufferT streambuffers;
        auto pNode = pPipelineNodeMap->nodeAt(i);
        auto nodeId = pNode->getNodeId();
        MERROR err = pFrame->queryIOStreamInfoSet(nodeId, pIStreams, pOStreams);
        if  ( OK != err ) {
            MY_LOGW("nodeId:%#" PRIxPTR " frameNo:%d queryIOStreamInfoSet", nodeId, frameNo);
            continue;
        }

        if  ( IStreamInfoSet const* pStreams = pIStreams.get() ) {
            {// I:Image
                sp<IStreamInfoSet::IMap<IImageStreamInfo> > pMap = pStreams->getImageInfoMap();
                for (size_t i = 0; i < pMap->size(); i++) {
                    StreamId_T const streamId = pMap->valueAt(i)->getStreamId();

                    sp<IStreamBuffer> pStreamBuffer = rStreamBufferSet.getImageBuffer(streamId, nodeId);
                    if (streambuffers.indexOfKey(streamId) < 0) {
                        streambuffers.add(streamId, pStreamBuffer);
                        //MY_LOGD("add input stream id: %ld",streamId);
                    }
                }
            }
        }
        else {
            MY_LOGE("nodeId:%#" PRIxPTR " frameNo:%d NULL IStreams", nodeId, frameNo);
        }
        //
        if  ( IStreamInfoSet const* pStreams = pOStreams.get() ) {
            {// O:Image
                sp<IStreamInfoSet::IMap<IImageStreamInfo> > pMap = pStreams->getImageInfoMap();
                for (size_t i = 0; i < pMap->size(); i++) {
                    StreamId_T const streamId = pMap->valueAt(i)->getStreamId();

                    sp<IStreamBuffer> pStreamBuffer = rStreamBufferSet.getImageBuffer(streamId, nodeId);
                    if (streambuffers.indexOfKey(streamId) < 0) {
                        streambuffers.add(streamId, pStreamBuffer);
                        //MY_LOGD("add output stream id: %ld",streamId);
                    }
                }
            }
        }
        else {
            MY_LOGE("nodeId:%#" PRIxPTR " frameNo:%d NULL OStreams", nodeId, frameNo);
        }
        mRequests.add(requestNo, streambuffers);
    }
    IPreReleaseRequestMgr::getInstance()->addRequest(requestNo, mUid, mImgReaderID);
    MY_LOGD("-");
}
