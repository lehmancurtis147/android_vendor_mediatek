/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-DeviceAdapter"
//
#include <impl/IHalDeviceAdapter.h>
//
#include <sys/prctl.h>
//
#include <future>
#include <vector>
//
#include <mtkcam/utils/LogicalCam/IHalLogicalDeviceList.h>
#include <mtkcam/drv/IHalSensor.h>
#include "IHal3AAdapter.h"
#include "MyUtils.h"
//
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::model;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("%d[HalDeviceAdapter::%s] " fmt, mId, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {
class HalDeviceAdapter : public IHalDeviceAdapter
{
protected:  ////            Data Members.
    int32_t                 mId = -1;       //logical device ID
    std::string const       mName;

    //init
    std::vector<int32_t>    mvPhySensorId;  //physical sensor ID (index)

    //open/close
    bool                    mIsOpen = false;

    //powerOn/powerOff
    std::vector<NSCam::IHalSensor*>             mvHalSensor;
    std::vector<::android::sp<IHal3AAdapter>>   mvHal3A;

public:     ////    Operations.

                    HalDeviceAdapter(int32_t id)
                        : mId(id)
                        , mName("HalDeviceAdapter:" + std::to_string(id))
                    {
                        MY_LOGD("%p %s", this, mName.c_str());
                    }

    virtual auto    init() -> bool
                    {
                        auto pHalDeviceList = MAKE_HalLogicalDeviceList();
                        if  ( CC_UNLIKELY(pHalDeviceList == nullptr) ) {
                            MY_LOGE("Bad pHalDeviceList");
                            return false;
                        }
                        //
                        mvPhySensorId = pHalDeviceList->getSensorId(mId);
                        return true;
                    }

    virtual auto    open() -> bool override
                    {
                        CAM_TRACE_NAME("Sensor creation");
                        auto pHalSensorList = MAKE_HalSensorList();
                        if  ( CC_UNLIKELY(pHalSensorList == nullptr) ) {
                            MY_LOGE("Bad HalSensorList");
                            return false;
                        }
                        //
                        bool ret = true;
                        for (size_t i = 0; i < mvPhySensorId.size(); i++) {
                            auto pSensor = pHalSensorList->createSensor(mName.c_str(), mvPhySensorId[i]);
                            mvHalSensor.push_back(pSensor);
                            if ( CC_UNLIKELY(pSensor == nullptr) ) {
                                ret = false;
                                MY_LOGE("Bad HalSensor - mvPhySensorId[%zu]=%d", i, mvPhySensorId[i]);
                            }
                        }

                        if  (CC_UNLIKELY(!ret)) {
                            MY_LOGE("Fail on open(). [TODO] let's clean up resources");
                        }
                        mIsOpen = ret;
                        return ret;
                    }

    virtual auto    close() -> void override
                    {
                        CAM_TRACE_NAME("Sensor destruction");
                        for (size_t i = 0; i < mvHalSensor.size(); i++) {
                            mvHalSensor[i]->destroyInstance(mName.c_str());
                        }
                        mvHalSensor.clear();
                        mvHal3A.clear();
                        mIsOpen = false;
                    }

    virtual auto    powerOn() -> bool override
                    {
                        CAM_TRACE_NAME("LogicalDev powerOn");
                        NSCam::Utils::CamProfile profile(__FUNCTION__, mName.c_str());
                        if  ( CC_UNLIKELY(!mIsOpen) ) {
                            MY_LOGE("Bad HalSensor");
                            return false;
                        }
                        // create thread to power on sensors
                        std::future<bool> future_initSensor =
                            std::async(std::launch::async,
                                [ this ]() {
                                    CAM_TRACE_NAME("Sensors powerOn");
                                    ::prctl(PR_SET_NAME, (unsigned long)"LogicalDevPoweron", 0, 0, 0);
                                    //
                                    for (size_t i = 0; i < mvPhySensorId.size(); i++)
                                    {
                                        MUINT const sensorIndex = mvPhySensorId[i];
                                        if ( (mvHalSensor[i] == nullptr) || (!mvHalSensor[i]->powerOn(mName.c_str(), 1, &sensorIndex)) )
                                        {
                                            return false;
                                        }
                                    }
                                    //
                                    return true;
                                }
                            );
                        // init 3A and poweron 3A
                        bool success_sensorPowerOn = false;
                        bool success_init3A = true;
                        for (size_t i = 0; i < mvPhySensorId.size(); i++)
                        {
                            mvHal3A.push_back(IHal3AAdapter::create(mvPhySensorId[i], mName.c_str()));
                            profile.print("3A Hal -");
                        }

                        //(3) Wait for Sensor
                        {
                            success_sensorPowerOn = future_initSensor.get();
                            if  ( ! success_sensorPowerOn ) {
                                return false;
                            }
                            profile.print("Sensor powerOn -");
                        }

                        //(4) Notify 3A of Power On
                        for (size_t i = 0; i < mvHal3A.size(); i++)
                        {
                            if (mvHal3A[i] != nullptr)
                            {
                                mvHal3A[i]->notifyPowerOn();
                            }
                            else
                            {
                                success_init3A = false;
                                break;
                            }
                        }
                        profile.print("3A notifyPowerOn -");

                        return  ( success_init3A && success_sensorPowerOn );
                    }

    virtual auto    powerOff() -> void override
                    {
                        CAM_TRACE_NAME("LogicalDev powerOff");
                        for (size_t i = 0; i < mvPhySensorId.size(); i++)
                        {
                            MUINT const sensorIndex = mvPhySensorId[i];
                            mvHal3A[i]->notifyPowerOff();
                            mvHalSensor[i]->powerOff(mName.c_str(), 1, &sensorIndex);
                        }
                    }

    virtual auto    getPhysicalSensorId(
                        std::vector<int32_t>& rvSensorId
                    ) const -> bool override
                    {
                        if ( mvPhySensorId.empty() )
                            return false;
                        rvSensorId = mvPhySensorId;
                        return true;
                    }

};


/******************************************************************************
 *
 ******************************************************************************/
auto
IHalDeviceAdapter::
create(int32_t id) -> android::sp<IHalDeviceAdapter>
{
    android::sp<HalDeviceAdapter> p = new HalDeviceAdapter(id);
    if  ( CC_UNLIKELY(p == nullptr) ) {
        CAM_LOGE("Fail on new HalSensorAdapter");
        return nullptr;
    }

    if  ( CC_UNLIKELY(!p->init()) ) {
        p = nullptr;
        return nullptr;
    }
    return p;
}


};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

