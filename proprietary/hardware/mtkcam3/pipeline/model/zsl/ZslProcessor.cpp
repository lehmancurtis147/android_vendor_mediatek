/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "MtkCam/ZslProc"

#include "ZslProcessor.h"
#include "MyUtils.h"
#include "mtkcam/drv/iopipe/CamIO/IHalCamIO.h"
#include "mtkcam/utils/metadata/client/mtk_metadata_tag.h"
#include "mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h"
#include "mtkcam3/pipeline/utils/streaminfo/MetaStreamInfo.h"
#include "mtkcam3/pipeline/hwnode/StreamId.h"


using namespace NSCam::v3::pipeline::model;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

#define MY_LOGD1(...)               MY_LOGD_IF((mLogLevel>=1),__VA_ARGS__)
#define MY_LOGD2(...)               MY_LOGD_IF((mLogLevel>=2),__VA_ARGS__)
#define MY_LOGD3(...)               MY_LOGD_IF((mLogLevel>=3),__VA_ARGS__)

// public
#if 0       // performance check
#define FUNC_START_PUBLIC                                           \
        NSCam::Utils::CamProfile profile(__FUNCTION__, LOG_TAG);    \
        profile.print("+ %s", __FUNCTION__);
#define FUNC_END_PUBLIC       profile.print("-")
#elif 1     // functional check
#define FUNC_START_PUBLIC   MY_LOGD("+")
#define FUNC_END_PUBLIC     MY_LOGD("-")
#else
#define FUNC_START_PUBLIC
#define FUNC_END_PUBLIC
#endif

// protected/private
#if 1
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif

#define abs(a) (((a) < 0) ? -(a) : (a))

#define CHECK_ERROR(_err_)                                \
    do {                                                  \
        MERROR const err = (_err_);                       \
        if( err != OK ) {                                 \
            MY_LOGE("err:%d(%s)", err, ::strerror(-err)); \
            return err;                                   \
        }                                                 \
    } while(0)

#ifndef RETURN_ERROR_IF_NOT_OK
#define RETURN_ERROR_IF_NOT_OK(_expr_, fmt, arg...)                     \
            do {                                                                \
                int const err = (_expr_);                                       \
                if( CC_UNLIKELY(err != 0) ) {                                   \
                    MY_LOGE("err:%d(%s) - " fmt, err, ::strerror(-err), ##arg); \
                    return err;                                                 \
                }                                                               \
            } while(0)
#endif

#ifndef RETURN_IF_NOT_OK
#define RETURN_IF_NOT_OK(_expr_, fmt, arg...)                     \
            do {                                                                \
                int const err = (_expr_);                                       \
                if( CC_UNLIKELY(err != 0) ) {                                   \
                    MY_LOGE("err:%d(%s) - " fmt, err, ::strerror(-err), ##arg); \
                    return;                                                 \
                }                                                               \
            } while(0)
#endif

// TO-DO: move to custom folder
#define ZSLShutterDelayTimeDiffMs     0

#define MAX_TOLERANCE_SHUTTERDELAY_MS   (50L)

#define ZSLForcePolicyDisable         0xefffffff
#define ZSLForceTimeoutDisable        -1

#define ZSL_PENDINGLIST_MAX             (5)

/******************************************************************************
 *
 ******************************************************************************/
ZslProcessor::
ZslProcessor(int32_t openId, std::string const& name)
    : mOpenId(openId)
    , mUserName(name)
    , mLogLevel(0)
    , mpHBC(NULL)
    , mPendingReqTimeMs(0)
    , mLastTimeMS(0)
    , mZSLTimeDiffMs(0)
    , mLastSelBufReq(0)
{
    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("vendor.debug.camera.log.pipelinemodel.zsl", 0);
    }
    mZSLForcePolicy = ::property_get_int32("vendor.debug.camera.zsl.policy",ZSLForcePolicyDisable); //
    mZSLTimeDiffMs = ::property_get_int64("vendor.debug.camera.zsl.timestamp",ZSLShutterDelayTimeDiffMs);
    mZSLForceTimeOutMs = ::property_get_int64("vendor.debug.camera.zsl.timeout",ZSLForceTimeoutDisable);
    int64_t default_zsl_ts_diff = ::property_get_int64("ro.vendor.camera3.zsl.default", 0);
    int64_t project_zsl_ts_diff = ::property_get_int64("ro.vendor.camera3.zsl.project", 0);
    int64_t user_zsl_ts_diff    = ::property_get_int64("vendor.debug.camera3.zsl.timestamp",ZSLShutterDelayTimeDiffMs);
    mZSLTimeDiffMs = default_zsl_ts_diff + project_zsl_ts_diff + user_zsl_ts_diff;
    MY_LOGI("zsl timestamp diff(%" PRId64 ") = default(%" PRId64 ")+project(%" PRId64 ")+user(%" PRId64 ")",
            mZSLTimeDiffMs, default_zsl_ts_diff, project_zsl_ts_diff, user_zsl_ts_diff);
}


/******************************************************************************
 *
 ******************************************************************************/
android::sp<IZslProcessor>
IZslProcessor::
createInstance(
    int32_t openId,
    string const& name
)
{
    sp<ZslProcessor> pZslProcessor = new ZslProcessor(openId, name);
    if ( CC_UNLIKELY( ! pZslProcessor.get() ) ) {
        MY_LOGE("create zsl processor instance fail");
        return nullptr;
    }

    return pZslProcessor;
}

/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZslProcessor::
configure(
     shared_ptr<InputZslConfigParams>const& in,
     shared_ptr<OutputZslConfigParams>& out
)
{
    FUNC_START_PUBLIC;
    {
        Mutex::Autolock _l(mLock);
        // check if there is pending request
        if( mPendingRequest.size()>=ZSL_PENDINGLIST_MAX ) {
            MY_LOGE("there is ZSL pending request not finish yet, size(%zu), 1st req no: %u",
                    mPendingRequest.size(), mPendingRequest.begin()->first);
            return INVALID_OPERATION;
        }

        // clean old HBC record and create new HBC instance
        mpHBC = IHistoryBufferContainer::createInstance(mOpenId,mUserName);
        if(mpHBC == NULL) {
            MY_LOGE("create HistoryBufferContainer failed");
            return BAD_VALUE;
        }

        // Save config parameter and output provider instance
        mCfgInfoSet = in->pInfoSet;
        mvUserId = in->vUserId;
        mvParsedStreamInfo_P1 = *(in->pParsedStreamInfo_P1);
        mPipelineModelCallback = in->pPipelineModelCallback;
        for(size_t i=0;i<mvUserId.size();i++)
        {
            MY_LOGD("mvUserId[%zu] = %#" PRIxPTR , i ,mvUserId.at(i));
        }

        out = make_shared<OutputZslConfigParams>();
        out->pProvider = mpHBC;
        MY_LOGD("Output mpHBC = %p",out->pProvider.get());

        // config HistoryBufferContainer
        CHECK_ERROR(mpHBC->beginConfigStreams(mCfgInfoSet));
        CHECK_ERROR(mpHBC->endConfigStreams());
    }
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
android::sp<IStreamBufferProvider>
ZslProcessor::
getStreamBufferProvider() const
{
    Mutex::Autolock _l(mLock);
    return mpHBC;
}


/******************************************************************************
 *
 ******************************************************************************/
void
ZslProcessor::
dumpPending()
{
    size_t count = 0;
    for ( auto it=mPendingRequest.begin(); it!=mPendingRequest.end(); ++it, ++count ) {
        MY_LOGI("[%zu/%zu]:(req:%u-%p)", count, mPendingRequest.size(),
                it->first, it->second.get());
        dumpInputZslRequestParams(it->second);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
void
ZslProcessor::
dumpInputZslRequestParams(shared_ptr<InputZslRequestParams> const &rpParams)
{
    MY_LOGI("CapParams(%p)", rpParams->pCapParams.get());
    MY_LOGI("FrameParams(%p:%zu)", &rpParams->pFrameParams, rpParams->pFrameParams.size());
    for ( size_t i=0; i<rpParams->pFrameParams.size(); ++i ) {
        auto& pAppBuffers = rpParams->pFrameParams[i]->spAppImageStreamBuffers;
        for ( auto it=pAppBuffers->vAppImage_Output_Proc.begin();
                it!=pAppBuffers->vAppImage_Output_Proc.end(); ++it ) {
            MY_LOGI("frame[%zu/%zu], output_proc(%#" PRIx64 ":%s:%p)",
                    i, rpParams->pFrameParams.size(), it->first,
                    it->second->getStreamInfo()->getStreamName(), it->second.get() );
        }
        MY_LOGI("frame[%zu/%zu], raw16(%p)",
                i, rpParams->pFrameParams.size(),
                pAppBuffers->pAppImage_Output_RAW16.get() );
        MY_LOGI("frame[%zu/%zu], jpeg(%p)",
                i, rpParams->pFrameParams.size(),
                pAppBuffers->pAppImage_Jpeg.get() );

    }

}


/******************************************************************************
 *
 ******************************************************************************/
android::status_t
ZslProcessor::
submitZslRequest(
     shared_ptr<InputZslRequestParams>const& in,
     shared_ptr<OutputZslRequestParams>& out
)
{
    FUNC_START_PUBLIC;
    {
        Mutex::Autolock _l(mLock);
        if(mpHBC == NULL)
        {
            MY_LOGE("mpHBC not init yet");
            return NO_INIT;
        }
        // not allow new request if there is pending request
        if( mPendingRequest.size()>=ZSL_PENDINGLIST_MAX && in->pCapParams->needZslFlow )
        {
            MY_LOGE("get new ZSL request(%u) but pending list is full(%zu), 1st req no: %u",
                    in->pFrameParams[0]->requestNo, mPendingRequest.size(),
                    mPendingRequest.begin()->first );
            return INVALID_OPERATION;
        }
        // not allow to process non-Zsl request
        if((mPendingRequest.empty()) && (in->pCapParams->needZslFlow == false))
        {
            MY_LOGW("This is not a ZSL request (req no: %d)",in->pFrameParams[0]->requestNo);
            return INVALID_OPERATION;
        }

        auto processReq = [&] (
            shared_ptr<OutputZslRequestParams>& out,
            shared_ptr<InputZslRequestParams> const& pZslReqParam,
            bool isPending
        ) -> int
        {
            MY_LOGI("out(%p) in(%p) isPending(%s)", out.get(), pZslReqParam.get(),
                    isPending? "true" : "false" );

            uint32_t reqNo = pZslReqParam->pFrameParams[0]->requestNo;
            shared_ptr<policy::pipelinesetting::RequestOutputParams> pCapParams = pZslReqParam->pCapParams;
            status_t ret = OK;
            Vector<BufferSet_T> vZSLBufSets;
            // 1) Get buffer from HBC
            ret = selectBuf_Locked(reqNo, pCapParams, vZSLBufSets);
            if(ret == OK)
            {
                shared_ptr<OutputZslFlowRequest> pOutZslFlowReq(new OutputZslFlowRequest());
                pOutZslFlowReq->zslRequestNo = reqNo;
                // output IPipeline frame.
                CHECK_ERROR(buildFrame_Locked(pZslReqParam,vZSLBufSets,pOutZslFlowReq->vFrame));

                // remove this pending request & add InflightZslRequest just for record
                if ( isPending )
                    mPendingRequest.erase(reqNo);
                mvInflightZslRequestNo.push_back(reqNo);

                // prepare output data
                out->bZslFlow = true;
                out->vZslFlowRequest.insert(make_pair(reqNo,pOutZslFlowReq));
                // out->bNonZslFlow = false;
            }
            else if( ret==NOT_ENOUGH_DATA )
            {
                if ( isPending ) {
                    MY_LOGW("pending request: failed to select HBC buffer, err:%d(%s)", ret, ::strerror(-ret));
                    return ret;
                }
                uint32_t reqNo_in = pZslReqParam->pFrameParams[0]->requestNo;
                // prepare output data
                // out->bZslFlow = false;
                out->bNonZslFlow = true;
                std::shared_ptr<OutputNonZslFlowRequest> outResult(new OutputNonZslFlowRequest());
                outResult->nonZslRequestNo = reqNo_in;
                outResult->pFrameParams = pZslReqParam->pFrameParams;
                outResult->pCapParams = pZslReqParam->pCapParams;
                out->vNonZslFlowRequest.insert(make_pair(reqNo_in,outResult));

                // if there is no pending request, add one pending request
                if(mPendingRequest.size()<ZSL_PENDINGLIST_MAX)
                {
                    mPendingRequest.insert(
                        pair<uint32_t, std::shared_ptr<InputZslRequestParams>>
                        (reqNo, pZslReqParam));
                    // MY_LOGI("push request params:%p into pendinglist(%zu)", pZslReqParam.get(), mPendingRequest.size());
                    // dumpPending();
                    // make shutter callback
                    callBackShutter(reqNo, mLastTimeMS);
                }
                // if there is pending request, just exit
                return ret;
            }
            else
            {
                MY_LOGE("failed to select HBC buffer, err:%d(%s)", ret, ::strerror(-ret));
                return ret;
            }
            return OK;
        };

        out = make_shared<OutputZslRequestParams>();
        int ret = OK;
        while ( ! mPendingRequest.empty() ) {
            auto it = mPendingRequest.begin();
            // we will remove iter from pendinglist if it's ok.
            // so, we can always take first element in list as out target.
            ret = processReq(out, it->second, true);
            if ( ret!=OK ) {
                // consecutive pending request processing have the same buffer selection criterion.
                // in other words, we can skip following requests as we encounter first failure of
                // pending request.
                MY_LOGW("fail to process pending request(%u)->expected pipelineframe count: %zu",
                        it->first, it->second->pFrameParams.size());
                break;
            }
        }

        // process zsl capture request must fail if previous pending request fail.
        // still need to put this capture request into pending list.
        if ( in->pCapParams->needZslFlow ) {
            int res = processReq(out, in, false);
            if ( res==NO_INIT ) {
                MY_LOGW("has no history buffer, transfer to normal capture request directly");
                return NO_MEMORY;
            } else if ( res!=OK ) {
                MY_LOGW("fail to process zsl capture request(%u)->expected pipelineframe count: %zu",
                        in->pFrameParams[0]->requestNo, in->pFrameParams.size());
            }
        }

    }
    FUNC_END_PUBLIC;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
ZslProcessor::
hasPendingZslRequest() const
{
    Mutex::Autolock _l(mLock);
    bool ret = (!mPendingRequest.empty())? true: false;
    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZslProcessor::
onFrameUpdated(
    uint32_t const requestNo,
    MINTPTR  const userId,
    IPipelineBufferSetFrameControl::IAppCallback::Result const& result
)
{
//	    MY_LOGD("enter: requestNo = %d, userId = %#" PRIx64 " ",requestNo,userId);
    Mutex::Autolock _l(mLock);
    if(!mpHBC.get())
    {
        MY_LOGE("mpHBC not init yet");
        return;
    }
    status_t ret=OK;
    for(size_t i=0;i<mvUserId.size();i++)
    {
        if(mvUserId.at(i) == userId)
        {
            MY_LOGD("enqueMetaBuffers to HBC: requestNo = %u, frameNo = %d, userId = %#" PRIxPTR " ",requestNo,result.frameNo,userId);
            ret =mpHBC->enqueMetaBuffers(result.vAppOutMeta,
                                    result.vHalOutMeta,
                                    result.frameNo);
            if(ret != OK) {
                MY_LOGE("mpHBC->enqueMetaBuffers() failed (ret=%d, requestNo=%d)",ret,requestNo);
            }
            break;
        }
    }
    if(!mvInflightZslRequestNo.isEmpty())
    {
        for(Vector<uint32_t>::iterator iter = mvInflightZslRequestNo.begin();
            iter != mvInflightZslRequestNo.end(); ++iter)
        {
            if(*iter == requestNo)
            {
//	                MY_LOGD("update meta for ZSL capture requestNo = %u, frameNo = %d, userId = %#" PRIxPTR " ",requestNo,result.frameNo,userId);
                for(size_t i=0;i<result.vAppOutMeta.size();i++)
                {
                    const sp<IMetaStreamBuffer> pMetaStream = result.vAppOutMeta.itemAt(i);
                    MY_LOGD("vAppOutMeta[%zu] : streamId(0x%#" PRIx64 ":%s)",
                            i,
                            pMetaStream->getStreamInfo()->getStreamId(),
                            pMetaStream->getStreamInfo()->getStreamName());

                    if(pMetaStream->getStreamInfo()->getStreamId() ==
                        mvParsedStreamInfo_P1[0].pAppMeta_DynamicP1->getStreamId())
                    {
                        for(Vector<uint32_t>::iterator iter_FakeShutterReq = mvFakeShutterRequestNo.begin();
                            iter_FakeShutterReq != mvFakeShutterRequestNo.end(); ++iter_FakeShutterReq)
                        {
                            if(*iter_FakeShutterReq == requestNo)
                            {
                                // need to remove MTK_SENSOR_TIMESTAMP in vAppOutMeta
                                // use to avoid call back shutter twice
                                IMetadata* pP1OutAppMeta = pMetaStream->tryWriteLock(LOG_TAG);
                                pP1OutAppMeta->remove(MTK_SENSOR_TIMESTAMP);
                                pMetaStream->unlock(LOG_TAG,pP1OutAppMeta);
                                MY_LOGD("vAppOutMeta[%zu] : streamId(0x%#" PRIx64 ":%s) remove MTK_SENSOR_TIMESTAMP tag (0x%x)",
                                        i, pMetaStream->getStreamInfo()->getStreamId(),
                                        pMetaStream->getStreamInfo()->getStreamName(),
                                        MTK_SENSOR_TIMESTAMP);
                                mvFakeShutterRequestNo.erase(iter_FakeShutterReq);
                                break;
                            }
                        }
                    }
                }

                // call HBC to release buffer usage and clear record of pending request
                if((result.nAppOutMetaLeft == 0)&&
                    (result.nHalOutMetaLeft == 0))
                {
                    MY_LOGD("get last meta data, finish InflightZslRequest (pending req no:%d)",
                        requestNo);
                    android::Vector<BufferSet_T> vDummy;
                    mpHBC->returnBufferSets(vDummy,requestNo);
                    mvInflightZslRequestNo.erase(iter);
                }
                break;
            }
        }
    }
//	    FUNC_END_PUBLIC;
    return;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZslProcessor::
flush(
    shared_ptr<OutputZslRequestParams>& out
)
{
    FUNC_START_PUBLIC;
    Mutex::Autolock _l(mLock);

    out = make_shared<OutputZslRequestParams>();
    out->bZslFlow = false;
    out->bNonZslFlow = true;
    if( mPendingRequest.size() )
    {
        for ( auto it = mPendingRequest.begin(); it!=mPendingRequest.end(); ++it ) {
            std::shared_ptr<InputZslRequestParams> pZslReqParam = it->second;
            android::sp<IPipelineFrame> spPipelineFrame;
            std::shared_ptr<InputZslBuildPipelineFrameParams> spFrameParams;
            for(size_t i = 0;i<pZslReqParam->pFrameParams.size();i++)
            {
                if(pZslReqParam->pFrameParams[i]->bDummyFrame == MFALSE)
                {
                    spFrameParams = pZslReqParam->pFrameParams[i];  // submit main frame only
                    break;
                }
            }
            InputZslBuildPipelineFrameParams* pFrameParams = spFrameParams.get();
            if(pFrameParams == nullptr)
            {
                MY_LOGE("get BuildPipelineFrameInputParams ptr = NULL");
                return;
            }

            // if there is pending request, just build an dummy pipeline frame using BuildPipelineFrameInputParams
            // enque to pipeline context and wait pipeline default session to flush it
            BuildPipelineFrameInputParams const params =  {
                .requestNo = pFrameParams->requestNo,
                .bReprocessFrame = MFALSE,
                .pAppImageStreamBuffers = pFrameParams->spAppImageStreamBuffers.get(),
                .pAppMetaStreamBuffers  = (pFrameParams->AppMetaStreamBuffers.empty())? nullptr : &pFrameParams->AppMetaStreamBuffers,
                .pHalImageStreamBuffers = nullptr,
                .pHalMetaStreamBuffers  = (pFrameParams->HalMetaStreamBuffers.empty())? nullptr : &pFrameParams->HalMetaStreamBuffers,
                .pvUpdatedImageStreamInfo = &pFrameParams->vUpdatedImageStreamInfo,
                .pnodeSet = &pFrameParams->nodeSet,
                .pnodeIOMapImage = &pFrameParams->nodeIOMapImage,
                .pnodeIOMapMeta = &pFrameParams->nodeIOMapMeta,
                .pRootNodes = &pFrameParams->RootNodes,
                .pEdges = &pFrameParams->Edges,
                .pCallback = pFrameParams->pCallback,
                .pPipelineContext = pFrameParams->pPipelineContext
            };
            RETURN_IF_NOT_OK( buildPipelineFrame(spPipelineFrame, params),
                                    "buildPipelineFrame fail - requestNo:%u",
                                    pFrameParams->requestNo );

            std::shared_ptr<OutputZslFlowRequest> pOutZslFlowReq(new OutputZslFlowRequest());
            uint32_t reqNo = pZslReqParam->pFrameParams[0]->requestNo;
            pOutZslFlowReq->zslRequestNo = reqNo;
            pOutZslFlowReq->vFrame.push_back(spPipelineFrame);

            out->bZslFlow = true;
            out->vZslFlowRequest.insert(make_pair(reqNo,pOutZslFlowReq));
            out->bNonZslFlow = false;
        }
        mPendingRequest.clear();
    }

    FUNC_END_PUBLIC;
    return;
}

/******************************************************************************
 *
 ******************************************************************************/
void
ZslProcessor::
setBufferEnqueCnt(
    MUINT32 const iRequestNo,
    size_t iStreamCnt,
    size_t iMetaCnt
    )
{
    if(mpHBC.get())
        mpHBC->setBufferEnqueCnt(iRequestNo,iStreamCnt,iMetaCnt);
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ZslProcessor::
dump() -> void
{
    MY_LOGD("[TODO] Not Implement yet");
}

/******************************************************************************
 *      Implementation
 ******************************************************************************/

void
ZslProcessor::
callBackShutter( uint32_t reqNo,                   // in
                           int64_t shutterTimeStamp)//,        // in
{
    sp<IPipelineModelCallback> pCallback;
    pCallback = mPipelineModelCallback.promote();
    if ( CC_UNLIKELY(! pCallback.get()) ) {
        MY_LOGE("can not promote pCallback for shutter");
        return;
    }

    UserOnFrameUpdated params;
    params.requestNo = reqNo;
    params.userId = mvUserId[0];
    params.nOutMetaLeft = 1;

    // just get any stream info from record
    sp<IMetaStreamInfo> pStreamInfo = mvParsedStreamInfo_P1[0].pHalMeta_DynamicP1;

    // generate sp<IMetaStreamBuffer> with only MTK_SENSOR_TIMESTAMP
    // content is shutterTimeStamp (last buffer timestamp)
    sp<HalMetaStreamBuffer> pShutterMetaBuffer =
        HalMetaStreamBufferAllocatorT(pStreamInfo.get())();

    IMetadata* meta = pShutterMetaBuffer->tryWriteLock(LOG_TAG);
    IMetadata::setEntry<MINT64>( meta, MTK_SENSOR_TIMESTAMP, shutterTimeStamp*1000000);
    pShutterMetaBuffer->unlock(LOG_TAG,meta);

#if 1
    // sp<IUsersManager::IUserGraph> pUserGraph = pShutterMetaBuffer->createGraph();
    // IUsersManager::User user;
    // user.mUserId = mvUserId[0];
    // user.mCategory = IUsersManager::Category::CONSUMER;
    // pUserGraph->addUser(user);
    // pShutterMetaBuffer->enqueUserGraph(pUserGraph.get());
    pShutterMetaBuffer->finishUserSetup();
    // pShutterMetaBuffer->markUserStatus(mvUserId[0],IUsersManager::UserStatus::RELEASE);
#endif

    params.vOutMeta.push_back(pShutterMetaBuffer);
    MY_LOGD("call back fake shutter (req:%u, timestamp:%" PRId64 ")",reqNo, shutterTimeStamp);
    mvFakeShutterRequestNo.push_back(reqNo);
    pCallback->onFrameUpdated(params);

}

status_t
ZslProcessor::
buildFrame_Locked(shared_ptr<InputZslRequestParams> pZslReqParam,      // in
                            Vector<BufferSet_T>&           vZSLBufSets,       // in
                            vector<sp<IPipelineFrame>>&        vFrame)            // out
{
    size_t i, real_frame = 0;;
    for(i=0; i<pZslReqParam->pFrameParams.size(); i++)
    {
        std::shared_ptr<InputZslBuildPipelineFrameParams>& pFrameParams =
                            pZslReqParam->pFrameParams[i];
        if(pFrameParams->bDummyFrame)
        {
            continue;
        }
        // prepare hal image buffer for each frame
        std::vector<android::sp<HalImageStreamBuffer>> vpHalImg;
        prepareHalImg(vZSLBufSets[real_frame].vImageSet,vpHalImg);

        // prepare app/hal meta buffer for each frame
        MBOOL bSubFrame = (real_frame==0)? MFALSE : MTRUE;
        std::vector<android::sp<IMetaStreamBuffer>> vpAppMetaZsl;
        std::vector<android::sp<HalMetaStreamBuffer>> vpHalMetaZsl;
        prepareMeta( bSubFrame,
                    pFrameParams->AppMetaStreamBuffers,
                    pFrameParams->HalMetaStreamBuffers,
                    vZSLBufSets[real_frame].vMetaSet,
                    vpAppMetaZsl,
                    vpHalMetaZsl);

        android::sp<IPipelineFrame> pPipelineFrame;
        BuildPipelineFrameInputParams const params = {
            .requestNo = pFrameParams->requestNo,
            .bReprocessFrame = MTRUE,
            .pAppImageStreamBuffers = pFrameParams->spAppImageStreamBuffers.get(),
            .pAppMetaStreamBuffers  = &vpAppMetaZsl,
            .pHalImageStreamBuffers = &vpHalImg,
            .pHalMetaStreamBuffers  = &vpHalMetaZsl,
            .pvUpdatedImageStreamInfo = &pFrameParams->vUpdatedImageStreamInfo,
            .pnodeSet = &pFrameParams->nodeSet,
            .pnodeIOMapImage = &pFrameParams->nodeIOMapImage,
            .pnodeIOMapMeta = &pFrameParams->nodeIOMapMeta,
            .pRootNodes = &pFrameParams->RootNodes,
            .pEdges = &pFrameParams->Edges,
            .pCallback = pFrameParams->pCallback,
            .pPipelineContext = pFrameParams->pPipelineContext
        };
        RETURN_ERROR_IF_NOT_OK( buildPipelineFrame(pPipelineFrame, params),
                                "buildPipelineFrame fail - requestNo:%u, frame:%zu",
                                pFrameParams->requestNo,real_frame);
        vFrame.push_back(pPipelineFrame);
        real_frame++;
    }
    return OK;
}

void
ZslProcessor::
prepareHalImg(const ImageSet_T&  vImageSet,        // in
                      vector<sp<HalImageStreamBuffer>>& vpHalImg)    // out
{
    for(size_t i=0;i<vImageSet.size();i++)
    {
        android::sp<HalImageStreamBuffer> pHalImg = vImageSet.valueAt(i);
        auto pStreamInfo = pHalImg->getStreamInfo();
        // reset hal image status and user manager
        pHalImg->clearStatus();
        sp<IUsersManager> pUsersManager = new UsersManager(pStreamInfo->getStreamId(),
                                                           pStreamInfo->getStreamName() );
        pHalImg->setUsersManager(pUsersManager);
        MY_LOGD("reset hal image usersmanager of streamId(0x%#" PRIx64 ":%s)->UsersManager(%p)",
                pStreamInfo->getStreamId(), pStreamInfo->getStreamName(), pUsersManager.get() );
        vpHalImg.push_back(pHalImg);
    }
}

void
ZslProcessor::
prepareMeta(MBOOL   /* bSubFrame */,  // in
                   const vector<sp<IMetaStreamBuffer>>& vpAppMeta,         // in
                   const vector<sp<HalMetaStreamBuffer>>& vpHalMeta,       // in
                   const MetaSet_T&     vMetaSet,           // in
                   vector<sp<IMetaStreamBuffer>>& vpAppMetaZsl,       // out
                   vector<sp<HalMetaStreamBuffer>>& vpHalMetaZsl)     // out
{
    size_t i;

    // 1. prepare ZSL app control meta from ZSL request input
    for(i=0;i<vpAppMeta.size();i++) {
        // MY_LOGI("[%zu/%zu]:%s", i, vpAppMeta.size(), vpAppMeta[i]->getStreamInfo()->getStreamName());
        vpAppMetaZsl.push_back(vpAppMeta[i]);
    }

    // 2. prepare ZSL hal control meta from ZSL request input
    for(i=0;i<vpHalMeta.size();i++) {
        // MY_LOGI("[%zu/%zu]:%s", i, vpHalMeta.size(), vpHalMeta[i]->getStreamInfo()->getStreamName());
        vpHalMetaZsl.push_back(vpHalMeta[i]);
    }

    // 3. prepare ZSL app/hal result meta from HBC meta
    for(i=0;i<vMetaSet.size();i++)
    {
        android::sp<IMetaStreamBuffer> pStreamBuffer = vMetaSet.valueAt(i);
        StreamId_T streamId = pStreamBuffer->getStreamInfo()->getStreamId();
        android::sp<IMetaStreamInfo> spStreamInfo = mCfgInfoSet->getMetaInfoFor(streamId);
        sp<HalMetaStreamBuffer> pMetaBuffer;

        // append P1 control meta data to P1 outpout Hal Meta data
        appendToP1OutHalMeta(pStreamBuffer,spStreamInfo,vpHalMeta,pMetaBuffer);
        // append app meta from Meta:App:Control to App:Meta:DynamicP1_main1
        appendToP1OutAppMeta(vpAppMeta, pMetaBuffer);

        MBOOL pushed = MFALSE;
        for(size_t j=0;j<mvParsedStreamInfo_P1.size();j++)
        {
            if(streamId == mvParsedStreamInfo_P1[j].pAppMeta_DynamicP1->getStreamId())
            {
                MY_LOGD("MetaStreamBuffer Push to AppMeta: streamId(0x%#" PRIx64 ":%s)",
                    streamId, spStreamInfo->getStreamName());
                vpAppMetaZsl.push_back(pMetaBuffer);
                pushed = MTRUE;
                break;
            }
            else if (streamId == mvParsedStreamInfo_P1[j].pHalMeta_DynamicP1->getStreamId())
            {
                MY_LOGD("MetaStreamBuffer Push to HalMeta: streamId(0x%#" PRIx64 ":%s)",
                    streamId, spStreamInfo->getStreamName());
                vpHalMetaZsl.push_back(pMetaBuffer);
                pushed = MTRUE;
                break;
            }
        }
        if(pushed == MFALSE)
        {
            MY_LOGW("Get unknown IMetaStreamBuffer, streamId(0x%#" PRIx64 ":%s)",
                streamId, spStreamInfo->getStreamName());
        }
    }
}

void
ZslProcessor::
appendToP1OutAppMeta(const vector<sp<IMetaStreamBuffer>>& vpAppMeta,         // in
                                 sp<HalMetaStreamBuffer>& pMetaBuffer                   // in/out
                                 )
{
    StreamId_T streamId = pMetaBuffer->getStreamInfo()->getStreamId();
    if(streamId == mvParsedStreamInfo_P1[0].pAppMeta_DynamicP1->getStreamId())
    {
        uint32_t postProcessingTags[] = {
            MTK_CONTROL_CAPTURE_INTENT,
            MTK_NOISE_REDUCTION_MODE,
            MTK_COLOR_CORRECTION_ABERRATION_MODE,
            MTK_COLOR_CORRECTION_MODE,
            MTK_TONEMAP_MODE,
            MTK_SHADING_MODE,
            MTK_HOT_PIXEL_MODE,
            MTK_EDGE_MODE
        };
        for(size_t i=0;i<vpAppMeta.size();i++)
        {
            if(vpAppMeta[i]->getStreamInfo()->getStreamId() == eSTREAMID_META_APP_CONTROL)
            {
                IMetadata* pAppInMeta = vpAppMeta[i]->tryReadLock(LOG_TAG);
                IMetadata* pAppOutMeta = pMetaBuffer->tryWriteLock(LOG_TAG);
                for(size_t j = 0; j < sizeof(postProcessingTags) / sizeof(uint32_t); j++)
                {
                    IMetadata::IEntry const entry_rd = pAppInMeta->entryFor(postProcessingTags[j]);
                    if(! entry_rd.isEmpty())
                    {
                        MUINT8 data = entry_rd.itemAt(0, Type2Type<MUINT8>());
                        IMetadata::IEntry entry_wt = pAppOutMeta->takeEntryFor(postProcessingTags[j]);
                        entry_wt.replaceItemAt(0, data, Type2Type<MUINT8>());
                        pAppOutMeta->update(postProcessingTags[j],entry_wt);
                        MY_LOGD("streamId(0x%#" PRIx64 ":%s) : update metadata : %u ==> %u",
                            streamId, pMetaBuffer->getStreamInfo()->getStreamName(),
                            postProcessingTags[j], data);
                    }
                }
                pMetaBuffer->unlock(LOG_TAG,pAppOutMeta);
                vpAppMeta[i]->unlock(LOG_TAG,pAppInMeta);
                break;
            }
        }
    }
}


void
ZslProcessor::
appendToP1OutHalMeta(android::sp<IMetaStreamBuffer>& pStreamBuffer,  // in
                                  android::sp<IMetaStreamInfo>& spStreamInfo,    // in
                                 const vector<sp<HalMetaStreamBuffer>>& vpHalMeta,       // in
                                 sp<HalMetaStreamBuffer>& pMetaBuffer           // out
                                 )
{
    StreamId_T streamId = pStreamBuffer->getStreamInfo()->getStreamId();
    IMetadata* pMeta = pStreamBuffer->tryReadLock(LOG_TAG);
    MBOOL bAppend = MFALSE;
    // append P1 control meta data to P1 outpout Hal Meta data
    for(size_t j=0;j<mvParsedStreamInfo_P1.size();j++)
    {
        if(streamId == mvParsedStreamInfo_P1[j].pHalMeta_DynamicP1->getStreamId())
        {
            IMetadata p1OutHalMeta(*pMeta);
            for(size_t k=0;k<vpHalMeta.size();k++)
            {
                if(vpHalMeta[k]->getStreamInfo()->getStreamId() ==
                    mvParsedStreamInfo_P1[j].pHalMeta_Control->getStreamId())
                {
                    IMetadata* pP1CtrlMeta = vpHalMeta[k]->tryReadLock(LOG_TAG);
                    p1OutHalMeta += *pP1CtrlMeta;
                    vpHalMeta[k]->unlock(LOG_TAG,pP1CtrlMeta);
                    MY_LOGD("append P1 IN control meta data (streamId: 0x%#" PRIx64 ") to P1 Out hal dynamic meta data (streamId: 0x%#" PRIx64 ")",
                            mvParsedStreamInfo_P1[j].pHalMeta_Control->getStreamId(), streamId );
                    break;
                }
            }
            pMetaBuffer = HalMetaStreamBufferAllocatorT(spStreamInfo.get())(p1OutHalMeta);
            bAppend = MTRUE;
        }
    }

    // other meta data do not append, just copy from HBC buffer record
    if(bAppend == MFALSE)
    {
        pMetaBuffer = HalMetaStreamBufferAllocatorT(spStreamInfo.get())(*pMeta);
    }
    pStreamBuffer->unlock(LOG_TAG,pMeta);
}

status_t
ZslProcessor::
selectBuf_Locked(uint32_t reqNo,        // in
                           shared_ptr<policy::pipelinesetting::RequestOutputParams> pCapParams, // in
                           Vector<BufferSet_T>& vBufSetsOut)    // out
{
    Vector<BufferSet_T> vHBC_Buf_Sets;
    int32_t ZslPolicy = (mZSLForcePolicy == (int32_t)ZSLForcePolicyDisable)?
            pCapParams->zslPolicyParams.mPolicy : mZSLForcePolicy;
    int64_t ReqTimeOut = (mZSLForceTimeOutMs == ZSLForceTimeoutDisable)?
            pCapParams->zslPolicyParams.mTimeouts : mZSLForceTimeOutMs;
    size_t reqFrameSz = pCapParams->subFrames.size() + 1;    // sub frame size + main frame size
    MY_LOGI("+ : reqFrameSz:%zu, req:%u, policy:0x%x", reqFrameSz, reqNo, ZslPolicy);

    // 1: Get buffer from HBC
    status_t ret;
    ret = mpHBC->acquireBufferSets(vHBC_Buf_Sets,reqNo,mCfgInfoSet);

    if ( ret==NO_INIT ) {
        MY_LOGW("has no history buffer in HBC");
        return ret;
    } else if ( ret!=OK ) {
        // FIX ME:
        if ( mLastTimeMS==0 ) {
            MY_LOGW("no available history buffer w/o lastTimestamp");
            return NO_INIT;
        }
        if ( mPendingRequest.size()>=ZSL_PENDINGLIST_MAX ) {
            MY_LOGE("out of memory w/ full pending size: %zu", mPendingRequest.size());
            return ret;
        } else {
            MY_LOGW("out of memory w/ pending size: %zu", mPendingRequest.size());
            return NOT_ENOUGH_DATA;
        }
    }

    // always update Pending request time stamp.
    // (in case there is pending request but can not get any HBC buffer)
    auto lastTimestamp = vHBC_Buf_Sets.size()?
                         getTimeStamp(vHBC_Buf_Sets[vHBC_Buf_Sets.size()-1].vMetaSet) : 0;
    if((mPendingReqTimeMs == 0) && (!mPendingRequest.empty()))
        mPendingReqTimeMs = lastTimestamp;
    mLastTimeMS = lastTimestamp;

    // 1-1: check HBC buffer size
    if(vHBC_Buf_Sets.size()<reqFrameSz)
    {
        MY_LOGW("Can not get enough buffer from HBC (HBC buf size:%zu, reqFrameSz=%zu)",
            vHBC_Buf_Sets.size(), reqFrameSz);

        // update Pending request time stamp
        if(mPendingRequest.empty())
        {
            mPendingReqTimeMs = vHBC_Buf_Sets.isEmpty()? 0 : lastTimestamp;
        }

        // No need to check policy, cancel buffer to HBC directly
        mpHBC->cancelBufferSets(vHBC_Buf_Sets,reqNo);
        return NOT_ENOUGH_DATA;
    }

    // 1-2: check timestamp (ms) policy, select buffer from specified timestamp
    size_t TimeSelBuf = 0;
    int64_t ReqTimeStampDiff = 0;
    if(ZslPolicy & eZslPolicy_ZeroShutterDelay)
    {
        MY_LOGI("shutter difference from policy(%" PRId64 ") or setting(%" PRId64 ")", pCapParams->zslPolicyParams.mTimestamp, mZSLTimeDiffMs);
        ReqTimeStampDiff = (pCapParams->zslPolicyParams.mTimestamp == -1)?
                            mZSLTimeDiffMs : pCapParams->zslPolicyParams.mTimestamp;
        selectBufByTimeStamp(vHBC_Buf_Sets,ReqTimeStampDiff,TimeSelBuf);
    }

    MBOOL bContinuousFrame = (ZslPolicy&eZslPolicy_ContinuousFrame)? MTRUE : MFALSE;
    MBOOL bPD_ProcessedRaw = (ZslPolicy&eZslPolicy_PD_ProcessedRaw)? MTRUE : MFALSE;
    Vector<size_t> vBuffer_Select;
    // 2: check frame quality policy
    for(size_t i=TimeSelBuf;i<vHBC_Buf_Sets.size();i++)
    {
        MY_LOGD("check quality for buffer[%zu], req:%u",i,vHBC_Buf_Sets[i].mRefReqNo);
        if(mLastSelBufReq>=vHBC_Buf_Sets[i].mRefReqNo)
        {
            MY_LOGD("do not use selected buffer again (mLastSelBufReq = %u, vHBC_Buf_Sets[%zu].mRefReqNo = %u)",
                mLastSelBufReq, i, vHBC_Buf_Sets[i].mRefReqNo);
            continue;
        }
        if(checkQuality(vHBC_Buf_Sets[i].vMetaSet,ZslPolicy) == MTRUE)
        {
            vBuffer_Select.push_back(i);
            MY_LOGI("buffer[%zu/%zu] (req:%u) sellected", i, vHBC_Buf_Sets.size(), vHBC_Buf_Sets[i].mRefReqNo);
        }
        // if need ContinuousFrame & there is policy violation, reset collection
        // PD_ProcessedRaw do not consider ContinuousFrame policy
        else if((bContinuousFrame) && (bPD_ProcessedRaw == MFALSE))
        {
            vBuffer_Select.clear();
            MY_LOGD("clear sellect record (bContinuousFrame=%d, bPD_ProcessedRaw=%d): buffer (%zu) sellected (req:%u)",
                bContinuousFrame,bPD_ProcessedRaw,i,vHBC_Buf_Sets[i].mRefReqNo);
        }
        if(vBuffer_Select.size() == reqFrameSz)
            break;
    }
    if(vBuffer_Select.size() == reqFrameSz)
    {
        // 3-1: Get enough buffer for ZSL
        // cancel unused buffer to HBC
        size_t buf_sel_cnt = 0;
        for( size_t i=0;i<vHBC_Buf_Sets.size();i++)
        {
            if( buf_sel_cnt==vBuffer_Select.size() || vBuffer_Select[buf_sel_cnt] != i)
            {
                // cancel unused buffer to HBC one by one
                MY_LOGD("cancelBufferSet(%zu) - req(%u)", i, reqNo);
                mpHBC->cancelBufferSet(vHBC_Buf_Sets.editItemAt(i),reqNo);
            }
            else
            {
                // MY_LOGD("buffer[%zu/%zu] (req:%u) sellected, do not cancel", i, vHBC_Buf_Sets[i].mRefReqNo);
                mLastSelBufReq = vHBC_Buf_Sets[i].mRefReqNo;
                buf_sel_cnt++;
            }
        }
        // output to selected ZSL buffer
        for(size_t i=0;i<reqFrameSz;i++)
            vBufSetsOut.push_back(vHBC_Buf_Sets[vBuffer_Select[i]]);
        MY_LOGI("- : Get enough buffer from HBC (reqFrameSz:%zu, req:%u, policy:0x%x)",
            reqFrameSz,reqNo,ZslPolicy);
        return OK;
    }
    else
    {
        int64_t  RecentReqTimeMs = getTimeStamp(vHBC_Buf_Sets[vHBC_Buf_Sets.size()-1].vMetaSet);
        int64_t  PendingReqTimeMs = (mPendingRequest.empty())? RecentReqTimeMs : mPendingReqTimeMs;
        // 3-2: if timeout, just collect lastest frame to generate pipeline frame
        if((RecentReqTimeMs - PendingReqTimeMs) >= ReqTimeOut)
        {
            MY_LOGW("enter ZSL request timeout(%" PRId64 " >= %" PRId64 ") check",(RecentReqTimeMs - PendingReqTimeMs),ReqTimeOut);
//	            mpHBC->cancelBufferSets(vHBC_Buf_Sets,reqNo);
//	            return NO_MEMORY;
            // sellect buffer for timeout (still need to check PD Raw sensor policy)
            size_t Buf_cnt =0;
            while(vBufSetsOut.size()<reqFrameSz)
            {
                if(ZslPolicy & eZslPolicy_PD_ProcessedRaw)
                {
                    if(isPDProcessRawBuffer(vHBC_Buf_Sets[Buf_cnt].vMetaSet))
                        vBufSetsOut.push_back(vHBC_Buf_Sets[Buf_cnt]);
                    else
                        mpHBC->cancelBufferSet(vHBC_Buf_Sets.editItemAt(Buf_cnt),reqNo);
                }
                else
                {
                    vBufSetsOut.push_back(vHBC_Buf_Sets[Buf_cnt]);
                }
                Buf_cnt ++;
                if(Buf_cnt>=vHBC_Buf_Sets.size())
                    break;
            }
            for(size_t i=Buf_cnt;i<vHBC_Buf_Sets.size();i++)
                mpHBC->cancelBufferSet(vHBC_Buf_Sets.editItemAt(i),reqNo);
            if(vBufSetsOut.size() < reqFrameSz)
            {
                MY_LOGW("- : ZSL request timeout(%" PRId64 " >= %" PRId64 "), but can not get enough buffer (BufSz:%zu < reqFrameSz:%zu), still pending (ZslReq:%u,lastReq:%u,policy:0x%x)",
                    (RecentReqTimeMs - PendingReqTimeMs),ReqTimeOut,vBufSetsOut.size(),reqFrameSz,
                    reqNo,vHBC_Buf_Sets[0].mRefReqNo,ZslPolicy);
                for(size_t i=0;i<vBufSetsOut.size();i++)
                    mpHBC->cancelBufferSet(vBufSetsOut.editItemAt(i),reqNo);
                return NOT_ENOUGH_DATA;
            }
            else
            {
                MY_LOGW("- : ZSL request timeout(%" PRId64 " >= %" PRId64 "), get latest frame directly (ZslReq:%u,lastReq:%u,policy:0x%x)",
                    (RecentReqTimeMs - PendingReqTimeMs),ReqTimeOut,reqNo,vHBC_Buf_Sets[0].mRefReqNo,ZslPolicy);
                return OK;
            }
        }
        else
        {
            // update Pending request time stamp
            if(mPendingRequest.empty())
                mPendingReqTimeMs = RecentReqTimeMs;

            // 3-3: not enough buffer, cancel all buffer to HBC
            // into pending request process and wait next submit to check
            mpHBC->cancelBufferSets(vHBC_Buf_Sets,reqNo);
            MY_LOGD("- : can not get enough buffer from HBC (reqFrameSz:%zu, req:%u, policy:0x%x)",
                reqFrameSz,reqNo,ZslPolicy);

            return NOT_ENOUGH_DATA;
        }
    }
}


void
ZslProcessor::
selectBufByTimeStamp(Vector<BufferSet_T>& vHBC_Buf_Sets,  // in
                                int64_t ReqTimeStampDiff,           // in
                                size_t& TimeSelBuf)                 // out
{
    int64_t BufTimeStamp = 0, LastBufTimeStamp = 0;
    int64_t ReqTimeStamp = (mPendingRequest.empty())?
        (getTimeStamp(vHBC_Buf_Sets[vHBC_Buf_Sets.size()-1].vMetaSet)) : mPendingReqTimeMs;
    int64_t SelTimeStamp = ReqTimeStamp - ReqTimeStampDiff;

    bool found = false;
    for(size_t i=0;i<vHBC_Buf_Sets.size();i++)
    {
        BufTimeStamp = getTimeStamp(vHBC_Buf_Sets[i].vMetaSet);
        MY_LOGD("[%zu/%zu] frm(%u) bufTS(%" PRId64 ") >= SelTS(%" PRId64 ")=reqTS(%" PRId64 ")-reqTSDiff(%" PRId64 ")",
                i, vHBC_Buf_Sets.size(), vHBC_Buf_Sets[i].mRefReqNo,
                BufTimeStamp, SelTimeStamp, ReqTimeStamp, ReqTimeStampDiff);

        int64_t shutter_delay = BufTimeStamp-SelTimeStamp;
        if( shutter_delay>=0 && !found )
        {
            TimeSelBuf = i;
            if ( i==0 && shutter_delay>MAX_TOLERANCE_SHUTTERDELAY_MS  ) {
                MY_LOGW("not enough depth of history buffer: delay(%" PRId64 ") vs. tolerance(%" PRId64 ")",
                        shutter_delay, MAX_TOLERANCE_SHUTTERDELAY_MS );
            } else if ( i>0 ) {
                LastBufTimeStamp = getTimeStamp(vHBC_Buf_Sets[i-1].vMetaSet);
                if( shutter_delay>abs(LastBufTimeStamp - SelTimeStamp))
                    TimeSelBuf = (i-1);
                MY_LOGI("check prev: lastTS(%" PRId64 ")-delay(%" PRId64 ") vs. bufTS(%" PRId64 ")-delay(%" PRId64 ")",
                        LastBufTimeStamp, LastBufTimeStamp-SelTimeStamp,
                        BufTimeStamp, shutter_delay );
            }
            found = true;
        }
    }
    if ( !found ) {
        MY_LOGW("future timestamp! use lastest one(%" PRId64 ")", BufTimeStamp);
        TimeSelBuf = vHBC_Buf_Sets.size()-1;
    }
    MY_LOGI("zsl select[%zu/%zu]:%" PRId64 ,
            TimeSelBuf, vHBC_Buf_Sets.size(), getTimeStamp(vHBC_Buf_Sets[TimeSelBuf].vMetaSet));
}

MBOOL
ZslProcessor::
checkQuality(const MetaSet_T &rvResult,int32_t ZslPolicy)
{
    MBOOL ret = MTRUE;
    if(ZslPolicy & eZslPolicy_AfState)
    {
        ret = isAfOkBuffer(rvResult);
        if(ret == MFALSE)   return ret;
    }
    if(ZslPolicy & eZslPolicy_AeState)
    {
        ret = isAeOkBuffer(rvResult);
        if(ret == MFALSE)   return ret;
    }
    if(ZslPolicy & eZslPolicy_DualFrameSync)
    {
        ret = isFameSyncOkBuffer(rvResult);
        if(ret == MFALSE)   return ret;
    }
    if(ZslPolicy & eZslPolicy_PD_ProcessedRaw)
    {
        ret = isPDProcessRawBuffer(rvResult);
        if(ret == MFALSE)   return ret;
    }

    return ret;
}

MBOOL
ZslProcessor::
isAfOkBuffer(const MetaSet_T &rvResult)
{
    MUINT8 afMode=0, afState=0, lensState=0;
    // MBOOL ret = MTRUE;
    MBOOL bFind = MFALSE;
    // get af info
    for( size_t i = 0; i < rvResult.size(); i++ )
    {
        if(rvResult.valueAt(i)->getStreamInfo()->getStreamId() ==
            mvParsedStreamInfo_P1[0].pAppMeta_DynamicP1->getStreamId())
        {
            // get AF & LENS state from MTK_CONTROL_AF_STATE & MTK_LENS_STATE
            IMetadata* meta = rvResult.valueAt(i)->tryReadLock(LOG_TAG);
            IMetadata::IEntry const eAfMode    = meta->entryFor(MTK_CONTROL_AF_MODE);
            IMetadata::IEntry const eAfState   = meta->entryFor(MTK_CONTROL_AF_STATE);
            IMetadata::IEntry const eLensState = meta->entryFor(MTK_LENS_STATE);
            if( !eAfMode.isEmpty() && !eAfState.isEmpty() && !eLensState.isEmpty() )
            {
                afMode    = eAfMode.itemAt(0, Type2Type<MUINT8>());
                afState   = eAfState.itemAt(0, Type2Type<MUINT8>());
                lensState = eLensState.itemAt(0, Type2Type<MUINT8>());
                bFind = true;
                MY_LOGD("find AF metadata in streamId(0x%#" PRIx64 ":%s) afMode(%d) afState(%d) lensState(%d)",
                        rvResult.valueAt(i)->getStreamInfo()->getStreamId(),
                        rvResult.valueAt(i)->getStreamInfo()->getStreamName(),
                        afMode, afState, lensState);
                // rvResult.valueAt(i)->unlock(LOG_TAG,meta);
                // break;
            }
            rvResult.valueAt(i)->unlock(LOG_TAG,meta);
            break;
        }
    }
    if(!bFind)
    {
        MY_LOGW("Can't Find MTK_CONTROL_AF_STATE or MTK_LENS_STATE");
        return MFALSE;
    }
    //
    if ( afMode == MTK_CONTROL_AF_MODE_AUTO ||
         afMode == MTK_CONTROL_AF_MODE_MACRO )
    {
        if ( afState == MTK_CONTROL_AF_STATE_INACTIVE ||
             afState == MTK_CONTROL_AF_STATE_PASSIVE_SCAN ||
             afState == MTK_CONTROL_AF_STATE_ACTIVE_SCAN  ||
             lensState == MTK_LENS_STATE_MOVING )
        {
            if ( afState == MTK_CONTROL_AF_STATE_INACTIVE &&
                 lensState == MTK_LENS_STATE_STATIONARY )
            {
                // modification1: no statinary count in hal3 version.
                // this information would be passed by af/lens module if needed.
                MY_LOGD("AF ok: afMode(%d) afState(%d) lensState(%d)", afMode, afState, lensState);
                return true;
            }
            MY_LOGW("AF fail: afMode(%d) afState(%d) lensState(%d)", afMode, afState, lensState);
            return false;
        }
    } else if ( afMode == MTK_CONTROL_AF_MODE_CONTINUOUS_VIDEO ||
                afMode == MTK_CONTROL_AF_MODE_CONTINUOUS_PICTURE ) {
        if ( afState == MTK_CONTROL_AF_STATE_INACTIVE ||
             afState == MTK_CONTROL_AF_STATE_PASSIVE_SCAN ||
             afState == MTK_CONTROL_AF_STATE_ACTIVE_SCAN  ||
             lensState == MTK_LENS_STATE_MOVING )
        {
            // modification2: no inactive count in hal3 version.
            // we do not need to count inactive count  default afstate is NOT_FOCUS
            if ( afState == MTK_CONTROL_AF_STATE_INACTIVE &&
                 lensState == MTK_LENS_STATE_STATIONARY )
            {
                // modification1: no statinary count in hal3 version.
                // this information would be passed by af/lens module if needed.
                MY_LOGD("AF ok: afMode(%d) afState(%d) lensState(%d)", afMode, afState, lensState);
                return true;
            }
            MY_LOGW("AF fail: afMode(%d) afState(%d) lensState(%d)", afMode, afState, lensState);
            return false;
        }
    }
    MY_LOGD("AF ok: afMode(%d) afState(%d) lensState(%d)", afMode, afState, lensState);
    return true;
}

MBOOL
ZslProcessor::
isAeOkBuffer(const MetaSet_T &rvResult)
{
    MUINT8 aeState=0;
    // MBOOL ret = MTRUE;
    MBOOL bFind = MFALSE;
    // get AE info
    for( size_t i = 0; i < rvResult.size(); i++ )
    {
        if(rvResult.valueAt(i)->getStreamInfo()->getStreamId() ==
            mvParsedStreamInfo_P1[0].pAppMeta_DynamicP1->getStreamId())
        {
            IMetadata* meta = rvResult.valueAt(i)->tryReadLock(LOG_TAG);
            IMetadata::IEntry const entry = meta->entryFor(MTK_CONTROL_AE_STATE);
            if(! entry.isEmpty())
            {
                aeState = entry.itemAt(0, Type2Type<MUINT8>());
                bFind = MTRUE;
                rvResult.valueAt(i)->unlock(LOG_TAG,meta);
                MY_LOGD("find AE metadata in streamId(0x%#" PRIx64 ":%s)",
                        rvResult.valueAt(i)->getStreamInfo()->getStreamId(),
                        rvResult.valueAt(i)->getStreamInfo()->getStreamName());
                break;
            }
            rvResult.valueAt(i)->unlock(LOG_TAG,meta);
            break;
        }
    }
    if(!bFind)
    {
        MY_LOGW("Can't Find MTK_CONTROL_AE_STATE");
        return MFALSE;
    }

    // if AE CONVERGED or locked, it is ae ok buffer
    if((aeState == MTK_CONTROL_AE_STATE_CONVERGED)||
        (aeState == MTK_CONTROL_AE_STATE_LOCKED))
    {
        MY_LOGD("AE ok: aeState = %d",aeState);
        return MTRUE;
    }

    MY_LOGW("AE failed: aeState = %d",aeState);
    return MFALSE;
}

MBOOL
ZslProcessor::
isFameSyncOkBuffer(const MetaSet_T &rvResult)
{
    MINT64 frameSyncState = 0;
    // MBOOL ret = MTRUE;
    MBOOL bFind = MFALSE;
    //
    for( size_t i = 0; i < rvResult.size(); i++ )
    {
        if(rvResult.valueAt(i)->getStreamInfo()->getStreamId() ==
            mvParsedStreamInfo_P1[0].pHalMeta_DynamicP1->getStreamId())
        {
            IMetadata* meta = rvResult.valueAt(i)->tryReadLock(LOG_TAG);
            IMetadata::IEntry const entry = meta->entryFor(MTK_FRAMESYNC_RESULT);
            if(! entry.isEmpty())
            {
                frameSyncState = entry.itemAt(0, Type2Type<MINT64>());
                bFind = MTRUE;
                rvResult.valueAt(i)->unlock(LOG_TAG,meta);
                break;
            }
            rvResult.valueAt(i)->unlock(LOG_TAG,meta);
            break;
        }
    }
    if(!bFind)
    {
        MY_LOGW("Can't Find MTK_FRAMESYNC_RESULT");
        return MFALSE;
    }
    //
    if(frameSyncState == MTK_FRAMESYNC_RESULT_PASS)
    {
        MY_LOGD("framesync ok: frameSyncState = %" PRId64 ,frameSyncState);
        return MTRUE;
    }
    MY_LOGW("framesync failed: frameSyncState = %" PRId64 ,frameSyncState);
    return MFALSE;
}


MBOOL
ZslProcessor::
isPDProcessRawBuffer(const MetaSet_T &rvResult)
{
    MINT32 iRawType = 0;
    // MBOOL ret = MTRUE;
    MBOOL bFind = MFALSE;
    //
    for( size_t i = 0; i < rvResult.size(); i++ )
    {
        if(rvResult.valueAt(i)->getStreamInfo()->getStreamId() ==
            mvParsedStreamInfo_P1[0].pHalMeta_DynamicP1->getStreamId())
        {
            IMetadata* meta = rvResult.valueAt(i)->tryReadLock(LOG_TAG);
            IMetadata::IEntry const entry = meta->entryFor(MTK_P1NODE_RAW_TYPE);
            if(! entry.isEmpty())
            {
                iRawType = entry.itemAt(0, Type2Type<MINT32>());
                bFind = MTRUE;
                rvResult.valueAt(i)->unlock(LOG_TAG,meta);
                break;
            }
            rvResult.valueAt(i)->unlock(LOG_TAG,meta);
            break;
        }
    }
    if(!bFind)
    {
        MY_LOGW("Can't Find MTK_P1NODE_RAW_TYPE");
        return MFALSE;
    }
    //
    if(iRawType == NSIoPipe::NSCamIOPipe::EPipe_PROCESSED_RAW)
    {
        MY_LOGD("iRawType ok: iRawType = %d",iRawType);
        return MTRUE;
    }
    MY_LOGW("iRawType skip: iRawType = %d",iRawType);
    return MFALSE;
}



MINT64
ZslProcessor::
getTimeStamp(const MetaSet_T &rvResult)
{
    MINT64 SensorTimestamp = 0;
    for( size_t i = 0; i < rvResult.size(); i++ )
    {
        IMetadata* meta = rvResult.valueAt(i)->tryReadLock(LOG_TAG);
        IMetadata::IEntry const entry = meta->entryFor(MTK_SENSOR_TIMESTAMP);
        if(! entry.isEmpty())
        {
            SensorTimestamp = entry.itemAt(0, Type2Type<MINT64>());
            rvResult.valueAt(i)->unlock(LOG_TAG,meta);
            break;
        }
        rvResult.valueAt(i)->unlock(LOG_TAG,meta);
    }
    return (SensorTimestamp/1000000);
}
