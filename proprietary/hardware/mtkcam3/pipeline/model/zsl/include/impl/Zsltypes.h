/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_TYPES_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_TYPES_H_

#include <mtkcam3/pipeline/model/types.h>
#include <mtkcam3/pipeline/model/IPipelineModelCallback.h>
#include <mtkcam3/pipeline/policy/types.h>
#include <mtkcam3/pipeline/policy/IPipelineSettingPolicy.h>
#include <mtkcam3/pipeline/pipeline/IPipelineNode.h>
#include <mtkcam3/pipeline/utils/streambuf/IStreamBufferProvider.h>
#include "impl/PipelineFrameBuilder.h"

#include <unordered_map>
#include <vector>
#include <utils/RefBase.h>

/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/******************************************************************************
 *  Configuration Parameters, used by IPipelineModel::configure()
 ******************************************************************************/
struct InputZslConfigParams
{
    android::sp<IStreamInfoSet>                         pInfoSet = nullptr;
    std::vector<MINTPTR>                                vUserId;
    std::vector<ParsedStreamInfo_P1>   const*           pParsedStreamInfo_P1;
    android::wp<IPipelineModelCallback>                 pPipelineModelCallback = nullptr;
};

struct OutputZslConfigParams
{
    /**
     *  external module needs this I/F for image streambuffer acquisition.
     */
    android::sp<Utils::IStreamBufferProvider>           pProvider = nullptr;
};

/******************************************************************************
 *  Request Parameters, used by IPipelineModel::submitRequest()
 ******************************************************************************/
struct InputZslBuildPipelineFrameParams;
struct InputZslRequestParams
{

    /**
     *  Required from Capture Setting Policy.
     */
    std::shared_ptr<policy::pipelinesetting::RequestOutputParams>  pCapParams  = nullptr;

    /**
     *  To generate pipeline frame.
     */
    std::vector<std::shared_ptr<InputZslBuildPipelineFrameParams>>  pFrameParams;

};

struct InputZslBuildPipelineFrameParams
{
    using HalImageStreamBuffer = NSCam::v3::Utils::HalImageStreamBuffer;
    using HalMetaStreamBuffer = NSCam::v3::Utils::HalMetaStreamBuffer;
    using IOMapSet = NSCam::v3::NSPipelineContext::IOMapSet;

    MBOOL                                       bDummyFrame = MFALSE;
    /**
     * Request no.
     */
    uint32_t                                    requestNo = 0;

    /**
     * App image stream buffers.
     */
    std::shared_ptr<ParsedAppImageStreamBuffers>    spAppImageStreamBuffers = nullptr;

    /**
     * App meta stream buffers.
     */
    std::vector<android::sp<IMetaStreamBuffer>> AppMetaStreamBuffers;


    /**
     * Hal meta stream buffers.
     */
    std::vector<android::sp<HalMetaStreamBuffer>> HalMetaStreamBuffers;

    /**
     * Updated image stream info.
     */
    std::unordered_map<StreamId_T, android::sp<IImageStreamInfo>>
                                                vUpdatedImageStreamInfo;

    /**
     * IOMapSet for all pipeline nodes.
     */
    std::vector<NodeId_T>                       nodeSet;
    std::unordered_map<NodeId_T, IOMapSet>      nodeIOMapImage;
    std::unordered_map<NodeId_T, IOMapSet>      nodeIOMapMeta;

    /**
     * The root nodes of a pipeline.
     */
    NSPipelineContext::NodeSet                  RootNodes;

    /**
     * The edges to connect pipeline nodes.
     */
    NSPipelineContext::NodeEdgeSet              Edges;

    /**
     * RequestBuilder's callback.
     */
    android::wp<NSPipelineContext::RequestBuilder::AppCallbackT>
                                                pCallback;
    /**
     * Pipeline context
     */
    android::sp<NSPipelineContext::PipelineContext>
                                                pPipelineContext;

};


struct OutputZslFlowRequest;
struct OutputNonZslFlowRequest;
struct OutputZslRequestParams
{
    /**
     *  Output Zsl/Non-Zsl Flow Request.
     *  The key of map is request number;
     *  the value for zsd flow might be multiple pipeline frame, user MUST submitRequest in order.
     *  the value for non-zsd flow MUST map to single input params, user might check the input
     *  params to detemine generate single or multiple pipeline frames, the same flow as
     *  NON-ZSD MODE.
     */

    bool                                                bZslFlow = false;
    std::map< uint32_t, std::shared_ptr<OutputZslFlowRequest> >
                                                        vZslFlowRequest;

    bool                                                bNonZslFlow = false;
    std::map< uint32_t, std::shared_ptr<OutputNonZslFlowRequest> >
                                                        vNonZslFlowRequest;
};

struct OutputZslFlowRequest
{
    /**
     *  Request number.
     */
    uint32_t                                            zslRequestNo = 0;

    /**
     *  vector of IPipelineFrame generated from this module.
     */
    std::vector<android::sp<IPipelineFrame>>                     vFrame;
};

struct OutputNonZslFlowRequest
{
    /**
     *  Request number (failed to use zsd buffers)
     */
    uint32_t                                            nonZslRequestNo = 0;

    /**
     *  Required from Capture Setting Policy.
     */
    std::shared_ptr<policy::pipelinesetting::RequestOutputParams>  pCapParams  = nullptr;

    /**
     *  Let user to generate pipeline frame by itself.
     */
    std::vector<std::shared_ptr<InputZslBuildPipelineFrameParams>>  pFrameParams;

};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_INCLUDE_IMPL_TYPES_H_

