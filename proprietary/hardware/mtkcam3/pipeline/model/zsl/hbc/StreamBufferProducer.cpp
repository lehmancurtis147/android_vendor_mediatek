/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/SB_provider"
//
#include "MyUtils.h"
#include "StreamBufferProducer.h"
//
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam3/pipeline/stream/IStreamInfo.h>
#include <mtkcam3/pipeline/utils/streambuf/StreamBufferProvider.h>
//

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGD1(...)               MY_LOGD_IF((mLogLevel>=1),__VA_ARGS__)
#define MY_LOGD2(...)               MY_LOGD_IF((mLogLevel>=2),__VA_ARGS__)
#define MY_LOGD3(...)               MY_LOGD_IF((mLogLevel>=3),__VA_ARGS__)
//
#if 0
#define FUNC_START     MY_LOGD("%p:+", this)
#define FUNC_END       MY_LOGD("%p:-", this)
#else
#define FUNC_START
#define FUNC_END
#endif

#define ACQUIRE_BUFFER_HEAP_TIMEOUT     (1*1000)

/******************************************************************************
 *
 ******************************************************************************/
StreamBufferProducer::
StreamBufferProducer(
    sp<IImageStreamInfo> pStreamInfo,
    sp<BufferPool> pPool
)
    : mLogLevel()
    , mLock()
    , mCond()
    //
    , mpStreamInfo(pStreamInfo)
    , mpBufferPool(pPool)
    , mbInited(false)
    , mMaxNumberOfBuffers(pStreamInfo->getMaxBufNum())
    //
    , mBufMapLock()
    , mBufferMap()
{
    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("vendor.debug.camera.log.streambufferprovider", 0);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::onLastStrongRef(const void* /*id*/) -> void
{
    FUNC_START;
    clear();
    FUNC_END;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
initProducer() -> android::status_t
{
    FUNC_START;
    Mutex::Autolock _l(mLock);
    status_t err = OK;
    MY_LOGD1("inited(%s)", (mbInited==true)? "TRUE":"FALSE" );
    if ( !mbInited ) {
        err = allocateBufferFromPoolLocked();
        mbInited = true;
    }
    FUNC_END;
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
allocateBufferFromPoolLocked() -> android::status_t
{
    FUNC_START;
    //
    if ( mpBufferPool==nullptr ) {
        sp<BufferPool> pPool = new BufferPool(mpStreamInfo);
        if ( pPool ==nullptr ) {
            MY_LOGE( "Producer(%p) cannot create BufferPool of stream(%#" PRIx64 ":%s)",
                     this, mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName() );
            return NO_MEMORY;
        }
        mpBufferPool = pPool;
    }
    //
    status_t err = OK;
    err = mpBufferPool->allocateBuffer( mpStreamInfo->getStreamName(),
                                        mMaxNumberOfBuffers,
                                        mpStreamInfo->getMinInitBufNum() );
    if ( err!=OK ) {
        MY_LOGE( "Producer(%p) cannot allocate buffer from pool(%p)",
                 this, mpBufferPool.get() );
        return err;
    }
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
#define CHECK_POOL()                                                \
    do {                                                            \
        if ( mpBufferPool==nullptr ) {                              \
            MY_LOGE("no BufferPool of stream(%#" PRIx64 ":%s)",     \
                    mpStreamInfo->getStreamId(),                    \
                    mpStreamInfo->getStreamName() );                \
            return UNKNOWN_ERROR;                                   \
        }                                                           \
    } while(0)

/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
acquireStreamBuffer(
    uint32_t const iRequestNo,
    sp<HalImageStreamBuffer>& rpStreamBuffer,
    sp<IStreamBufferProvider> pProvider
) -> status_t
{
    FUNC_START;
    //
    sp<IImageBufferHeap> pHeap;
    {
        Mutex::Autolock _l(mLock);
        CHECK_POOL();
        //
        if ( OK!=mpBufferPool->acquireFromPool(mpStreamInfo->getStreamName(), iRequestNo, pHeap) ) {
            status_t err = mCond.waitRelative(mLock, ACQUIRE_BUFFER_HEAP_TIMEOUT);
            if ( OK!=err ) {
                MY_LOGE("timeout to wait user release buffer...");
                return NO_MEMORY;
            }
            if ( OK!=mpBufferPool->acquireFromPool(mpStreamInfo->getStreamName(), iRequestNo, pHeap) ) {
                MY_LOGE("cannot acquire from pool after timeout.");
                return NO_MEMORY;
            }
        }
    }
    //
    rpStreamBuffer = new HalImageStreamBufferProvider(mpStreamInfo, pHeap, pProvider);
    if ( rpStreamBuffer==nullptr ) {
        MY_LOGE("cannot new HalImageStreamBuffer. Something wrong...");
        return NO_MEMORY;
    }
    //
    {
        MY_LOGD2("acquire - buffer:%p pHeap:%p", rpStreamBuffer.get(), pHeap.get());
        Mutex::Autolock _l(mBufMapLock);
        mBufferMap.add(rpStreamBuffer.get(), pHeap);
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
releaseStreamBuffer(
    sp<HalImageStreamBuffer> rpStreamBuffer,
    MUINT32  bBufStatus
) -> status_t
{
    FUNC_START;
    //
    sp<IImageBufferHeap> rpHeap;
    {
        Mutex::Autolock _l(mBufMapLock);
        rpHeap = mBufferMap.editValueFor( rpStreamBuffer.get() );
        mBufferMap.removeItem( rpStreamBuffer.get() );
    }
    //
    {
        Mutex::Autolock _l(mLock);
        CHECK_POOL();
        MY_LOGD2("release - bBufStatus:%d pHeap:%p", bBufStatus, rpHeap.get());
        mpBufferPool->releaseToPool(mpStreamInfo->getStreamName(), -1, rpHeap);
        mCond.signal();
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
setMaxBufferCount(
    size_t maxNumberOfBuffers
) -> status_t
{
    FUNC_START;
    {
        Mutex::Autolock _l(mBufMapLock);
        mMaxNumberOfBuffers = maxNumberOfBuffers;
        if ( mpBufferPool.get() )
            mpBufferPool->updateBufferCount(mpStreamInfo->getStreamName(), maxNumberOfBuffers);
    }
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
setBufferPool(
    sp<IBufferPool> pPool
) -> status_t
{
    FUNC_START;
    {
        Mutex::Autolock _l(mBufMapLock);
        if ( ! mBufferMap.isEmpty() ) {
            MY_LOGE("stream(%#" PRIx64 ":%s) are in-use",
            mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName() );
            return UNKNOWN_ERROR;
        }
    }
    //
    {
        Mutex::Autolock _l(mLock);
        mpBufferPool = pPool;
        mbInited = false;
    }
    //
    FUNC_END;
    return OK;

}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
hasAcquiredMaxBuffer() -> bool
{
    FUNC_START;
    //
    Mutex::Autolock _l(mBufMapLock);
    return mBufferMap.size()==mMaxNumberOfBuffers;
    //
    FUNC_END;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
clear() -> void
{
    FUNC_START;
    MY_LOGD( "clear Producer(%p) of stream(%#" PRIx64 ":%s) pool(%p)->size(%zu)",
             this, mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName(),
             mpBufferPool.get(), mBufferMap.size() );
    //
    sp<IImageBufferHeap> rpHeap;
    {
        Mutex::Autolock _l(mBufMapLock);
        for ( size_t i=0; i<mBufferMap.size(); ++i ) {
            rpHeap = mBufferMap.editValueAt(i);
            if ( rpHeap!=nullptr ) {
                Mutex::Autolock _l(mLock);
                MY_LOGD1("release (%zu/%zu) - pHeap:%p", i, mBufferMap.size(), rpHeap.get() );
                mpBufferPool->releaseToPool(mpStreamInfo->getStreamName(), -1, rpHeap);
            }
        }
        mBufferMap.clear();
    }
    //
    FUNC_END;
    // return;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
StreamBufferProducer::
dump() -> void
{
    FUNC_START;
    Mutex::Autolock _l(mBufMapLock);
    MY_LOGI( "dump Producer(%p) stream(%#" PRIx64 ":%s) pool(%p)->size(%zu)",
             this, mpStreamInfo->getStreamId(), mpStreamInfo->getStreamName(),
             mpBufferPool.get(), mBufferMap.size() );
    String8 str = String8::format("Acquired Buffer: ");
    for ( size_t i=0; i<mBufferMap.size(); ++i ) {
        str += String8::format( "[%zu/%zu](%p:%p); ", i, mBufferMap.size(),
                                mBufferMap.keyAt(i), mBufferMap.valueAt(i).get() );
    }
    MY_LOGI("%s", str.string());
    //
    mpBufferPool->dumpPool();
    FUNC_END;
}