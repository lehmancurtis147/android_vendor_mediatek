/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/HBC"
//
#include "MyUtils.h"
#include "HistoryBufferContainerImp.h"
//
#include <set>
//
#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/String8.h>
//


#define MAX_HISTORY_DEPTH           (8)

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::Utils;
using namespace NSCam::v3::Utils;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGD1(...)               MY_LOGD_IF((mLogLevel>=1),__VA_ARGS__)
#define MY_LOGD2(...)               MY_LOGD_IF((mLogLevel>=2),__VA_ARGS__)
#define MY_LOGD3(...)               MY_LOGD_IF((mLogLevel>=3),__VA_ARGS__)

// public
#if 0       // performance check
#define FUNC_START_PUBLIC                                           \
        NSCam::Utils::CamProfile profile(__FUNCTION__, LOG_TAG);    \
        profile.print("+ %s", __FUNCTION__);
#define FUNC_END_PUBLIC       profile.print("-")
#elif 0     // functional check
#define FUNC_START_PUBLIC   MY_LOGD("+")
#define FUNC_END_PUBLIC     MY_LOGD("-")
#else
#define FUNC_START_PUBLIC
#define FUNC_END_PUBLIC
#endif

// protected/private
#if 0
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif


#define CHECK_ERROR(_err_)                                \
    do {                                                  \
        MERROR const err = (_err_);                       \
        if( err != OK ) {                                 \
            MY_LOGE("err:%d(%s)", err, ::strerror(-err)); \
            return err;                                   \
        }                                                 \
    } while(0)

const char* HistoryBufferContainerImp::kStatusNames[HistoryBufferContainerImp::NUM_STATUS+1] =
{
    "UNINITED",
    "INITED",
    "DEQUED",
    "ENQUED",
    "PARTIAL_ACQUIRED",
    "ACQUIRED",
    "RETURNED",
    "UNKNOWN"
};

/******************************************************************************
 *
 ******************************************************************************/
auto
IHistoryBufferContainer::
createInstance(
    int32_t const deviceId,
    std::string const& name
) -> sp<IHistoryBufferContainer>
{
    FUNC_START_PUBLIC;
    sp<HistoryBufferContainerImp> pHBC = new HistoryBufferContainerImp(deviceId, name);
    if ( pHBC==0 ) {
        MY_LOGE("fail to instatiate HistoryBufferContainerImp");
        return nullptr;
    }
    //
    FUNC_END_PUBLIC;
    return pHBC;
}


/******************************************************************************
 *
 ******************************************************************************/
HistoryBufferContainerImp::
HistoryBufferContainerImp(
    int32_t const deviceId,
    std::string const& name
)
    : mDeviceId(deviceId)
    , mName(name)
    , mLogLevel()
    //
    , mCfgSet()
    , mMaxBufNum(MAX_HISTORY_DEPTH)
    //
    , mLock()
    , mCond()
    //
    , mBufferMap()
    , mProducerMap()
    , mBufferSourceReq()
    //
    , mEnqueCnt()
    , mAcquiredMap()
{
    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("vendor.debug.camera.log.hbc", 0);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
beginConfigStreams(
    const sp<IStreamInfoSet>& rpInfoSet
) -> status_t
{
    FUNC_START_PUBLIC;
    //
    {
        Mutex::Autolock _l(mLock);
        for( size_t i=0; i<rpInfoSet->getImageInfoNum(); i++ ) {
            auto pInfo = rpInfoSet->getImageInfoAt(i);
            auto streamId = pInfo->getStreamId();
            sp<StreamBufferProducer> pProducer = new StreamBufferProducer(pInfo);
            if (pProducer == nullptr){
                MY_LOGE("create stream buffer producer failed");
                return NO_MEMORY;
            }
            mProducerMap.add( streamId, pProducer );
            MY_LOGD( "create producer(%p) of stream(%#" PRIx64 ":%s:%p)",
                     pProducer.get(), pInfo->getStreamId(), pInfo->getStreamName(), pInfo.get() );
            mCfgSet.imageSet.add(streamId, pInfo);
            mMaxBufNum = (pInfo->getMaxBufNum()>mMaxBufNum)? pInfo->getMaxBufNum() : mMaxBufNum;
        }
        //
        for( size_t i=0; i<rpInfoSet->getMetaInfoNum(); i++ ) {
            auto pInfo = rpInfoSet->getMetaInfoAt(i);
            auto streamId = pInfo->getStreamId();
            mCfgSet.metaSet.add(streamId, pInfo);
            mMaxBufNum = (pInfo->getMaxBufNum()>mMaxBufNum)? pInfo->getMaxBufNum() : mMaxBufNum;
        }
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
endConfigStreams() -> status_t
{
    FUNC_START_PUBLIC;
    //
    {
        Mutex::Autolock _l(mLock);
        for( size_t i=0; i<mProducerMap.size(); i++ ) {
            auto pProducer = mProducerMap.editValueAt(i);
            auto pInfo     = mCfgSet.imageSet.valueFor( mProducerMap.keyAt(i) );
            MY_LOGD( "init producer(%p) of (%#" PRIx64 ":%s:%p)",
                     pProducer.get(), pInfo->getStreamId(), pInfo->getStreamName(), pInfo.get() );
            CHECK_ERROR( pProducer->initProducer() );
        }
        //
        dumpProducerInfoLocked();
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
setBufferSize(
    const KeyedVector<StreamId_T, size_t>& rvBufferSize
) -> status_t
{
    FUNC_START_PUBLIC;
    //
    Mutex::Autolock _l(mLock);
    for ( size_t i=0; i<rvBufferSize.size(); ++i ) {
        auto streamId  = rvBufferSize.keyAt(i);
        auto pProducer =  mProducerMap.editValueFor(streamId);
        CHECK_ERROR( pProducer->setMaxBufferCount(rvBufferSize.valueAt(i)) );
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
setBufferPool(
    const KeyedVector<StreamId_T, sp<IBufferPool> >& rvBufferPool
) -> status_t
{
    FUNC_START_PUBLIC;
    // producerSet avoid double initProducer() of different streams w/ the same BufferPool
    std::set<StreamBufferProducer*> producerSet;
    {
        Mutex::Autolock _l(mLock);
        for( size_t i=0; i<rvBufferPool.size(); i++ ) {
            auto pProducer = mProducerMap.editValueFor( rvBufferPool.keyAt(i) );
            // setBufferPool() clears inited status of StreamBufferProvider
            // initProducer() allocates initial size of BufferPool
            CHECK_ERROR( pProducer->setBufferPool( rvBufferPool.valueAt(i) ) );
            producerSet.insert(pProducer.get());
        }
        //
        for ( auto pProducer : producerSet ) {
            CHECK_ERROR( pProducer->initProducer() );
        }
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
enqueMetaBuffers(
    const Vector< sp<IMetaStreamBuffer> >& rvAppResult,
    const Vector< sp<IMetaStreamBuffer> >& rvHalResult,
    uint32_t const requestNo
)-> status_t
{
    auto copyMetaToHBS = [&]
        (const Vector<sp<IMetaStreamBuffer>>& rvMeta, sp<HistoryBufferSet_T> pHBS) -> void
    {
        for ( size_t i=0; i<rvMeta.size(); ++i ) {
            auto pInfo    = rvMeta.itemAt(i)->getStreamInfo();
            auto streamId = pInfo->getStreamId();
            if ( mCfgSet.metaSet.indexOfKey(streamId)<0 )
                continue;
            //
            auto pSBuffer = rvMeta.itemAt(i);
            pHBS->vMetaItem.add( streamId, MetaItem_T{pSBuffer, eSTATUS_ENQUED, /*false*/} );
            pHBS->availableMeta++;
        }
    };

    auto updateStatus = [&](sp<HistoryBufferSet_T> pHBS) -> bool
    {
        if ( pHBS->availableMeta  == mCfgSet.metaSet.size() &&
             ( mCfgSet.imageSet.size()==0 ||
             ( mCfgSet.imageSet.size() && pHBS->availableImage==pHBS->vImageItem.size() ) ) )
        {
            pHBS->status = eSTATUS_ENQUED;
            return true;
        }
        return false;
    };
    //
    FUNC_START_PUBLIC;
    //
    Mutex::Autolock _l(mLock);
    // check if release or re-init HistoryBufferSet or not
    ssize_t index = mBufferMap.indexOfKey(requestNo);
    if ( index < 0 ) {
        // we don't need this in real case, because there exists at least one image for zsl mode.
        // we might use size of image buffer pool to limit HBC's size.
        // if ( mBufferMap.size()==mMaxBufNum )
        //     CHECK_ERROR( releaseOneHistoryBufferLocked() );
        CHECK_ERROR( initOneHistoryBufferLocked(requestNo) );
    }
    auto pHBS = mBufferMap.editValueFor(requestNo);
    //
    copyMetaToHBS(rvAppResult, pHBS);
    copyMetaToHBS(rvHalResult, pHBS);
    //
    // update status
    switch ( pHBS->status )
    {
        case eSTATUS_INITED:
        {
            pHBS->status = eSTATUS_DEQUED;
            updateStatus(pHBS);
        } break;
        case eSTATUS_DEQUED:
        {
            updateStatus(pHBS);
        } break;
        // case eSTATUS_ENQUED:
        // case eSTATUS_PARTIAL_ACQUIRED:
        // case eSTATUS_ACQUIRED:
        // case eSTATUS_RETURNED
        default:
        {
            MY_LOGW("unexpected state(%s) of request(%u)", kStatusNames[pHBS->status], requestNo);
        }
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
getAvailableRequestList(
    Vector<uint32_t>& rvRequestNo,
    sp<IStreamInfoSet> pRequiredStreamSet
) -> status_t
{
    FUNC_START_PUBLIC;

    if ( mBufferMap.size()==0 ) {
        // it means that never recieve history buffer.
        return NO_INIT;
    }
    Vector<uint32_t> vNotReadyList;
    //  separate full/partial acquisition for speeding-up full acquisition
    if ( pRequiredStreamSet==nullptr ) {
        for( size_t i=0; i<mBufferMap.size(); ++i ) {
            Mutex::Autolock _l(mLock);
            //
            auto pHBS = mBufferMap.editValueAt(i);
            if ( pHBS->status == eSTATUS_ENQUED && ! pHBS->error  ) {
                rvRequestNo.add( mBufferMap.keyAt(i) );
            }
        }
    } else {
        for( size_t i=0; i<mBufferMap.size(); ++i ) {
            Mutex::Autolock _l(mLock);
            //
            bool bAllEnqued = true;
            auto pHBS = mBufferMap.editValueAt(i);
            auto pImageMap = pRequiredStreamSet->getImageInfoMap();
            for ( size_t j=0; j<pImageMap->size(); ++j ) {
                auto  streamId  = pRequiredStreamSet->getImageInfoAt(j)->getStreamId();
                auto& imageItem = pHBS->vImageItem.valueFor(streamId);
                if ( imageItem.status != eSTATUS_ENQUED || imageItem.error ) {
                    MY_LOGW( "[req:%u] stream(%#" PRIx64 ":%s) not ready -> status(%s) error(%s)",
                             mBufferMap.keyAt(i), streamId,
                             pRequiredStreamSet->getImageInfoAt(j)->getStreamName(),
                             kStatusNames[imageItem.status], (imageItem.error)?"true":"false" );
                    bAllEnqued = false;
                    // break;
                }
            }
            // if ( !bAllEnqued )
            //     continue;
            //
            auto pMetaMap = pRequiredStreamSet->getMetaInfoMap();
            for ( size_t j=0; j<pMetaMap->size(); ++j ) {
                auto  streamId = pRequiredStreamSet->getMetaInfoAt(j)->getStreamId();
                auto& metaItem = pHBS->vMetaItem.valueFor(streamId);
                if ( metaItem.status != eSTATUS_ENQUED ) {
                    MY_LOGW( "[req:%u] stream(%#" PRIx64 ":%s) not ready -> status(%s) error(%s)",
                             mBufferMap.keyAt(i), streamId,
                             pRequiredStreamSet->getMetaInfoAt(j)->getStreamName(),
                             kStatusNames[metaItem.status], (metaItem.error)?"true":"false" );
                    bAllEnqued = false;
                    // break;
                }
            }
            if ( bAllEnqued )
                rvRequestNo.add(mBufferMap.keyAt(i));
            else
                vNotReadyList.add(mBufferMap.keyAt(i));
        }
    }
    String8 str = String8::format("request total(%zu) available(%zu): ",
                                  rvRequestNo.size(), mBufferMap.size() );
    for ( size_t i=0; i<rvRequestNo.size(); ++i )
        str += String8::format("[%zu:%u]; ", i, rvRequestNo[i]);
    str += String8::format(" fail(%zu): ", vNotReadyList.size());
    for ( size_t i=0; i<vNotReadyList.size(); ++i )
        str += String8::format("[%zu:%u]; ", i, vNotReadyList[i]);
    MY_LOGI("%s", str.string());
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
acquireBufferSetsOfList(
    const Vector<uint32_t>& rvRequestNo,
    Vector<BufferSet_T>& rvBufSet,
    uint32_t const newRequestNo,
    sp<IStreamInfoSet> pRequiredStreamSet
) -> status_t
{
    auto copyFromHBS = []
        (BufferSet_T& rBufSet, sp<HistoryBufferSet_T> pHBS) -> void
    {
        for ( size_t i=0; i<pHBS->vImageItem.size(); ++i ) {
            rBufSet.vImageSet.add( pHBS->vImageItem.keyAt(i),
                                   pHBS->vImageItem.valueAt(i).pSBuffer );
        }
        //
        for ( size_t i=0; i<pHBS->vMetaItem.size(); ++i ) {
            rBufSet.vMetaSet.add( pHBS->vMetaItem.keyAt(i),
                                  pHBS->vMetaItem.valueAt(i).pSBuffer );
        }
    };

    auto addRelation = [&]
        (uint32_t const refReqNo, uint32_t const newReqNo, BufferSet_T& rBufSet) -> void
    {
        if ( mAcquiredMap.indexOfKey(newReqNo)<0 )
            mAcquiredMap.add(newReqNo, new AcquiredData_T(newReqNo) );
        //
        auto pData = mAcquiredMap.editValueFor(newReqNo);
        pData->mReqNoToMetas.add(refReqNo, eSTATUS_ACQUIRED);
        for ( size_t i=0; i<rBufSet.vImageSet.size(); ++i ) {
            pData->mBufferToReqNo.add( rBufSet.vImageSet.editValueAt(i).get(), refReqNo );
            auto pSBuffer = rBufSet.vImageSet.editValueAt(i);
            if ( pData->mReqNoToBuffers.indexOfKey( refReqNo )<0 )
                pData->mReqNoToBuffers.add(refReqNo, Vector<HalImageStreamBuffer *>() );
            pData->mReqNoToBuffers.editValueFor( refReqNo ).add( pSBuffer.get() );
            mBufferSourceReq.replaceValueFor(pSBuffer.get(), newReqNo);
        }
    };
    //
    FUNC_START_PUBLIC;
    //
    if ( pRequiredStreamSet==nullptr ) {
        for ( size_t i=0; i<rvRequestNo.size(); ++i ) {
            uint32_t const refReqNo = rvRequestNo[i];
            //
            Mutex::Autolock _l(mLock);
            auto pHBS = mBufferMap.valueFor(refReqNo);
            if ( pHBS.get() && pHBS->status != eSTATUS_ENQUED && ! pHBS->error ) {
                // currently, multiple-frames acquire will return OK if any HistoryBuffer is ready.
                // this case should only appear for double acquisition.
                MY_LOGW( "ref. request(%u)'s status is [%s](NOT ENQUED) error(%s)",
                         refReqNo, kStatusNames[pHBS->status], (pHBS->error)?"true":"false");
                continue;
            }
            //
            auto& bufSet = rvBufSet.editItemAt( rvBufSet.add() );
            copyFromHBS( bufSet, pHBS );
            bufSet.mRefReqNo = refReqNo;
            pHBS->updateStatus(eSTATUS_ACQUIRED);
            addRelation( refReqNo, newRequestNo, bufSet );
        }
    } else {
        for ( size_t i=0; i<rvRequestNo.size(); ++i ) {
            Mutex::Autolock _l(mLock);
            if ( acquireBuffersByRequiredSetLocked(rvBufSet, rvRequestNo[i], pRequiredStreamSet)==OK ) {
                addRelation( rvRequestNo[i], newRequestNo, rvBufSet.editTop() );
            }
        }
    }
    //
    FUNC_END_PUBLIC;
    return (rvBufSet.size())? OK : NO_MEMORY;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
acquireBufferSets(
    Vector<BufferSet_T>& rvBufSet,
    uint32_t const newRequestNo,
    sp<IStreamInfoSet> pRequiredStreamSet
) -> status_t
{
    FUNC_START_PUBLIC;
    // user does not assign reference request list
    // HBC will get all available list and and acquire from whole list.
    Vector<uint32_t> vRequest;
    CHECK_ERROR( getAvailableRequestList(vRequest, pRequiredStreamSet) );
    CHECK_ERROR( acquireBufferSetsOfList(vRequest, rvBufSet, newRequestNo, pRequiredStreamSet) );
    //
    if(rvBufSet.size() == 0)
        dumpAllHistoryBufferLocked();
    FUNC_END_PUBLIC;
    return (rvBufSet.size())? OK : NO_MEMORY;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
acquireBuffersByRequiredSetLocked(
    Vector<BufferSet_T>& rvBufSet,
    uint32_t const refReqNo,
    android::sp<IStreamInfoSet> pRequiredStreamSet
) -> status_t
{
    FUNC_START;
    auto updateStatusOfBufSet = [](BufferSet_T& bufSet, sp<HistoryBufferSet_T> pHBS,Status_T status) -> void
    {
        for ( size_t i=0; i<bufSet.vImageSet.size(); ++i ) {
            pHBS->vImageItem.editValueFor(bufSet.vImageSet.keyAt(i)).status = status;
        }
        //
        for ( size_t i=0; i<bufSet.vMetaSet.size(); ++i ) {
            pHBS->vMetaItem.editValueFor(bufSet.vMetaSet.keyAt(i)).status = status;
        }
    };

    auto pHBS = mBufferMap.valueFor(refReqNo);
    if(!pHBS.get())
    {
        MY_LOGW("cannot acquire HistoryBufferSet_T (request : %d)", refReqNo );
        return NO_MEMORY;
    }
    //
    bool bAllEnqued = true;
    auto& bufSet = rvBufSet.editItemAt( rvBufSet.add() );
    //
    auto pImageMap = pRequiredStreamSet->getImageInfoMap();
    for ( size_t j=0; j<pImageMap->size(); ++j ) {
        auto streamId    = pRequiredStreamSet->getImageInfoAt(j)->getStreamId();
        auto& vImageItem = pHBS->vImageItem;
        if ( vImageItem.indexOfKey(streamId)<0 ) {
            MY_LOGW("cannot acquire image stream(%#" PRIx64 ")", streamId );
            bAllEnqued = false;
            break;
        }
        auto& imageItem = vImageItem.editValueFor(streamId);
        if ( imageItem.status == eSTATUS_ENQUED && ! imageItem.error ) {
            imageItem.status =  eSTATUS_ACQUIRED;
            bufSet.vImageSet.add( streamId, imageItem.pSBuffer );
        } else {
            bAllEnqued = false;
            break;
        }
    }
    //
    auto pMetaMap = pRequiredStreamSet->getMetaInfoMap();
    for ( size_t j=0; j<pMetaMap->size(); ++j ) {
        auto streamId   = pRequiredStreamSet->getMetaInfoAt(j)->getStreamId();
        auto& vMetaItem = pHBS->vMetaItem;
        if ( vMetaItem.indexOfKey(streamId)<0 ) {
            MY_LOGW("cannot acquire meta stream(%#" PRIx64 ")", streamId );
            bAllEnqued = false;
            break;
        }
        auto& metaItem  = vMetaItem.editValueFor(streamId);
        if ( metaItem.status == eSTATUS_ENQUED && ! metaItem.error ) {
            metaItem.status =  eSTATUS_ACQUIRED;
            bufSet.vMetaSet.add( streamId, metaItem.pSBuffer );
        } else {
            bAllEnqued = false;
            break;
        }
    }
    if ( !bAllEnqued ) {
        MY_LOGW("fail partial acquired of request(%u)!", refReqNo);
        updateStatusOfBufSet(bufSet, pHBS, eSTATUS_ENQUED);
        // pHBS->updateStatus(eSTATUS_ENQUED);
        rvBufSet.pop();
        return NO_MEMORY;
    }

    bufSet.mRefReqNo = refReqNo;
    // update PARTIAL_ACQUIRED only in frame status.
    pHBS->status = eSTATUS_PARTIAL_ACQUIRED;
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
cancelBufferSets(
    Vector<BufferSet_T>& rvBufSet,
    uint32_t const newRequestNo
) -> void
{
    FUNC_START_PUBLIC;
    //
    for ( size_t i=0; i<rvBufSet.size(); ++i )
        cancelBufferSet(rvBufSet.editItemAt(i), newRequestNo);
    rvBufSet.clear();
    //
    FUNC_END_PUBLIC;
    // return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
cancelBufferSet(
    BufferSet_T& rBufSet,
    uint32_t const newRequestNo
) -> void
{
    FUNC_START_PUBLIC;
    Mutex::Autolock _l(mLock);
    uint32_t const reqNo = rBufSet.mRefReqNo;
    auto pData = mAcquiredMap.editValueFor(newRequestNo);
    for ( size_t i=0; i<pData->mReqNoToMetas.size(); ++i ) {
        if ( pData->mReqNoToMetas.keyAt(i)==reqNo ) {
            pData->mReqNoToMetas.removeItemsAt(i);
            break;
        }
    }
    pData->mReqNoToBuffers.removeItem(reqNo);
    MY_LOGD1("cancel buffers of refReq(%u) newReq(%u)", reqNo, newRequestNo);
    auto pHBS = mBufferMap.valueFor(reqNo);
    if(pHBS.get())
    {
        switch ( pHBS->status )
        {
            case eSTATUS_ACQUIRED:
            case eSTATUS_PARTIAL_ACQUIRED:
            {
                pHBS->updateStatus(eSTATUS_ENQUED);
                ImageItemMap_T& vImageItem = pHBS->vImageItem;
                for(size_t i=0; i<vImageItem.size(); i++)
                {
                    ImageItem_T ImageItem = vImageItem.editValueAt(i);
                    if(ImageItem.status == eSTATUS_ACQUIRED)
                        ImageItem.status = eSTATUS_ENQUED;
                }
                MetaItemMap_T& vMetaItem = pHBS->vMetaItem;
                for(size_t i=0; i<vMetaItem.size(); i++)
                {
                    MetaItem_T MetaItem = vMetaItem.editValueAt(i);
                    if(MetaItem.status == eSTATUS_ACQUIRED)
                        MetaItem.status = eSTATUS_ENQUED;
                }
            } break;
            // case eSTATUS_DEQUED:
            // case eSTATUS_ENQUED:
            // case eSTATUS_RETURNED:
            default:
            {
                MY_LOGW("have not yet fully or partial acquired before of request(%u)", reqNo);
            }
        }
    }
    // clear
    rBufSet.vImageSet.clear();
    rBufSet.vMetaSet.clear();

    FUNC_END_PUBLIC;
    // return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
inline auto
HistoryBufferContainerImp::
isRemovableAcquiredData(android::sp<AcquiredData_T> pData) -> bool
{
    return ( pData->mStatus == eSTATUS_RETURNED && pData->mReqNoToBuffers.isEmpty() );
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
returnBufferSets(
    android::Vector<BufferSet_T>& /*rvBufSet*/,
    uint32_t const newRequestNo
) -> void
{
    FUNC_START_PUBLIC;
    //
    Mutex::Autolock _l(mLock);
    MY_LOGI("return %u", newRequestNo);
    #if 0
    auto updateMeta = [](const MetaSet_T& rMetaSet, sp<HistoryBufferSet_T> pHBS, Status_T status) -> void
    {
        for ( size_t i=0; i<rMetaSet.size(); ++i ) {
            auto streamId = rMetaSet.keyAt(i);
            auto pSBuffer = rMetaSet.valueAt(i);
            pHBS->vMetaItem.add(streamId, MetaItem_T{pSBuffer, status, /*false*/});
            pHBS->availableMeta++;
        }
    };
    #endif
    //
    // MUST be called only one times
    if ( mAcquiredMap.indexOfKey(newRequestNo)<0 ) {
        MY_LOGW("not found of reqNo(%u) in aquired map", newRequestNo);
        return;
    }

    // this operation will remove all data of new request (zsd request)
    auto pData = mAcquiredMap.editValueFor(newRequestNo);
    for ( size_t i=0; i<pData->mReqNoToMetas.size(); ++i ) {
        uint32_t const refReqNo = pData->mReqNoToMetas.keyAt(i);
        auto pRefHBS = mBufferMap.editValueFor(refReqNo);
        for ( size_t j=0; j<pRefHBS->vMetaItem.size(); ++j ) {
            pRefHBS->vMetaItem.editValueAt(j).status = eSTATUS_ENQUED;
        }

        if ( pData->mReqNoToBuffers.indexOfKey(refReqNo)<0 ) // means all meta and image back to refHBS.
            pRefHBS->updateStatus(eSTATUS_ENQUED);
    }
    pData->mReqNoToMetas.clear();
    pData->mStatus = eSTATUS_RETURNED;
    //
    if ( isRemovableAcquiredData(pData) ) {
        mAcquiredMap.removeItem(newRequestNo);
    }
    //
    FUNC_END_PUBLIC;
    // return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
clear(uint32_t const requestNo) -> void
{
    FUNC_START_PUBLIC;
    Mutex::Autolock _l(mLock);
    auto pHBS = mBufferMap.valueFor(requestNo);
    if(pHBS.get())
    {
        releaseHistoryBufferLocked(pHBS);
        mBufferMap.removeItem(requestNo);
        if ( mEnqueCnt.indexOfKey(requestNo) >= 0 )
            mEnqueCnt.removeItem(requestNo);
    }
    FUNC_END_PUBLIC;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
clear() -> void
{
    FUNC_START_PUBLIC;
    //
    Mutex::Autolock _l(mLock);
    for ( size_t i=0; i<mBufferMap.size(); ++i ) {
        releaseHistoryBufferLocked(mBufferMap.valueAt(i));
    }
    mBufferMap.clear();
    mAcquiredMap.clear();
    mEnqueCnt.clear();
    //
    FUNC_END_PUBLIC;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dump() -> void
{
    Mutex::Autolock _l(mLock);
    dumpLocked();
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
HistoryBufferContainerImp::
dequeStreamBuffer(
    MUINT32 const iRequestNo,
    sp<IImageStreamInfo> const pStreamInfo,
    sp<HalImageStreamBuffer> &rpStreamBuffer
)
{
    FUNC_START_PUBLIC;
    //
    {
        Mutex::Autolock _l(mLock);
        //
        auto streamId  = pStreamInfo->getStreamId();
        auto pProducer = mProducerMap.valueFor(streamId);
        // This StreamBufferProvider has provided max buffer -> release one set HistoryBuffer
        // Might pre-release after each time enque!
        while ( pProducer->hasAcquiredMaxBuffer() ) {
            CHECK_ERROR( releaseOneHistoryBufferLocked() );
        }
        //
        CHECK_ERROR( pProducer->acquireStreamBuffer(iRequestNo, rpStreamBuffer, this) );
        if ( mBufferMap.indexOfKey(iRequestNo)<0 ) {
            // MY_LOGW("should not happen, there should exist one AppControl Meta at least");
            initOneHistoryBufferLocked(iRequestNo);
        }
        auto pHBS = mBufferMap.editValueFor(iRequestNo);
        auto& imageMap = pHBS->vImageItem;
        imageMap.replaceValueFor(streamId, ImageItem_T{rpStreamBuffer, eSTATUS_DEQUED, /*false*/ });
        mBufferSourceReq.add(rpStreamBuffer.get(), iRequestNo);
        pHBS->status = eSTATUS_DEQUED;
        //
        MY_LOGD2( "req(%u): HBC(%p) id(%#" PRIx64 ") ImageItem(%p).SBuffer(%p)",
                  iRequestNo, pHBS.get(),
                  streamId, &imageMap.editValueFor(streamId), rpStreamBuffer.get());
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
HistoryBufferContainerImp::
enqueStreamBuffer(
    sp<IImageStreamInfo> const pStreamInfo __unused,
    sp<HalImageStreamBuffer> rpStreamBuffer,
    MUINT32  bBufStatus
)
{
    FUNC_START_PUBLIC;
    //
    {
        Mutex::Autolock _l(mLock);
        if ( mBufferSourceReq.indexOfKey(rpStreamBuffer.get()) < 0 ) {
            MY_LOGE("cannot find this StreamBuffer from map: %p", rpStreamBuffer.get());
            return UNKNOWN_ERROR;
        }
        //
        auto requestNo = mBufferSourceReq.valueFor(rpStreamBuffer.get());
        MBOOL bEnqueToAcquireStream = MFALSE;
        if ( mAcquiredMap.size() && mAcquiredMap.indexOfKey(requestNo)>=0 ) {
            bEnqueToAcquireStream = MTRUE;
            // previous acquired by user.
            if(enqueStreamBufferOfAcquiredDataLocked(rpStreamBuffer, requestNo) != OK)
                bEnqueToAcquireStream = MFALSE;
        }

        if(bEnqueToAcquireStream == MFALSE)
        {
            // normal streaming request.
            auto pHBS = mBufferMap.editValueFor(requestNo);
            bool error = static_cast<bool>(bBufStatus & STREAM_BUFFER_STATUS::ERROR);
            if (error) {
                MY_LOGW("error of enqued buffer %p (reqNo:%u)", rpStreamBuffer.get(), requestNo);
                pHBS->error |= error;
            }
            //
            auto  streamId  = rpStreamBuffer->getStreamId();
            auto& imageItem = pHBS->vImageItem.editValueFor(streamId);
            auto  status    = pHBS->status;
            switch ( status )
            {
                case eSTATUS_DEQUED:
                case eSTATUS_ACQUIRED:
                {
                    imageItem.status = (Status_T)(status+1);
                    imageItem.error  = error;
                    pHBS->availableImage++;
                    size_t iStreamEnqueCnt = pHBS->vImageItem.size();
                    size_t iMetaEnqueCnt = mCfgSet.metaSet.size();
                    if ( mEnqueCnt.indexOfKey(requestNo) >= 0 ) {
                        android::sp<EnqueCnt_T> pEnqueCnt = mEnqueCnt.valueFor(requestNo);
                        iStreamEnqueCnt = pEnqueCnt->iStreamCnt;
                        iMetaEnqueCnt = pEnqueCnt->iMetaCnt;
                        MY_LOGD1( "req(%u): change to use specific enque count (iStreamEnqueCnt=%u, iMetaEnqueCnt=%u)",
                                  requestNo, iStreamEnqueCnt, iMetaEnqueCnt);
                    }

                    if ( pHBS->availableImage == iStreamEnqueCnt &&
                         pHBS->availableMeta  == iMetaEnqueCnt ) {
                        // image compared with count of dequed buffers for partial acquiring flow.
                        pHBS->status = (Status_T)(status+1);
                    }
                } break;
                //
                default:
                {
                    MY_LOGW("unexpected state(%s)", kStatusNames[status]);
                    imageItem.status = eSTATUS_ENQUED;
                    pHBS->availableImage++;
                }
            }
            MY_LOGD2( "req(%u): status(%s) id(%#" PRIx64 ") image(available:%zu-acquired:%zu) meta(available:%zu-cfg:%zu)",
                      requestNo, kStatusNames[status], streamId, pHBS->availableImage, pHBS->vImageItem.size(),
                      pHBS->availableMeta, mCfgSet.metaSet.size() );
        }
    }
    //
    FUNC_END_PUBLIC;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
void
HistoryBufferContainerImp::
setBufferEnqueCnt(
    MUINT32 const iRequestNo,
    size_t iStreamCnt,
    size_t iMetaCnt
)
{
    FUNC_START;
    MY_LOGD1( "req(%u): iStreamEnqueCnt=%u, iMetaEnqueCnt=%u",
              iRequestNo, iStreamCnt, iMetaCnt);

    Mutex::Autolock _l(mLock);
    sp<EnqueCnt_T> pItem = new EnqueCnt_T;
    pItem->iStreamCnt = iStreamCnt;
    pItem->iMetaCnt = iMetaCnt;

    mEnqueCnt.add(iRequestNo, pItem);
    FUNC_END;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
enqueStreamBufferOfAcquiredDataLocked(
    sp<HalImageStreamBuffer> rpStreamBuffer,
    uint32_t const newRequestNo
) -> status_t
{
    FUNC_START;
    //
    auto pData = mAcquiredMap.editValueFor(newRequestNo);
    if ( pData->mBufferToReqNo.indexOfKey(rpStreamBuffer.get())<0 ) {
        MY_LOGD("can not find acquired buffer %p (reqNo:%u)", rpStreamBuffer.get(), newRequestNo);
        return UNKNOWN_ERROR;
    }
    // update status of previous HBS
    auto refReqNo = pData->mBufferToReqNo.valueFor(rpStreamBuffer.get());
    if(mBufferMap.indexOfKey(refReqNo)<0)
    {
        MY_LOGD("can not find enqued buffer (mBufferMap can not find request:%u)", newRequestNo);
        return UNKNOWN_ERROR;
    }
    auto pRefHBS  = mBufferMap.editValueFor(refReqNo);
    auto streamId = rpStreamBuffer->getStreamInfo()->getStreamId();
    pRefHBS->vImageItem.editValueFor(streamId).status = eSTATUS_ENQUED;
    //
    pData->mBufferToReqNo.removeItem(rpStreamBuffer.get());
    auto& vSBuffer = pData->mReqNoToBuffers.editValueFor(refReqNo);
    for ( size_t i=0; i<vSBuffer.size(); ++i ) {
        if ( vSBuffer[i] == rpStreamBuffer.get()  ) {
            vSBuffer.removeAt(i);
            break;
        }
    }
    if ( vSBuffer.isEmpty() ) {
        pData->mReqNoToBuffers.removeItem(refReqNo);
        if ( pData->mReqNoToMetas.indexOfKey(refReqNo)<0 ) // means all meta and image back to refHBS.
            pRefHBS->updateStatus(eSTATUS_ENQUED);
    }
    if ( isRemovableAcquiredData(pData) ) {
        mAcquiredMap.removeItem(newRequestNo);
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
initOneHistoryBufferLocked(
    uint32_t const requestNo,
    Status_T status
) -> status_t
{
    FUNC_START;
    //
    sp<HistoryBufferSet_T> pItem = new HistoryBufferSet_T;
    pItem->status = status;
    mBufferMap.add(requestNo, pItem);
    MY_LOGD2("init one history buffer in map of request: %u", requestNo);
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
releaseOneHistoryBufferLocked() ->status_t
{
    FUNC_START;
    //
    bool bDone = false;
    for ( size_t i=0; i<mBufferMap.size(); ++i ) {
        auto pHBS = mBufferMap.valueAt(i);
        MY_LOGD2("historybufferset(%p) status:%s",
                pHBS.get(), kStatusNames[pHBS->status]);
        if ( pHBS->status == eSTATUS_DEQUED ||
             pHBS->status == eSTATUS_PARTIAL_ACQUIRED ||
             pHBS->status == eSTATUS_ACQUIRED ) {
            continue;
        }
        // remove enque count for request in mEnqueCnt
        uint32_t requestNo = mBufferMap.keyAt(i);
        if ( mEnqueCnt.indexOfKey(requestNo) >= 0 )
        {
            MY_LOGD1("remove mEnqueCnt item for request: %u",requestNo);
            mEnqueCnt.removeItem(requestNo);
        }
        //
        CHECK_ERROR( releaseHistoryBufferLocked(pHBS) );
        mBufferMap.removeItemsAt(i);
        bDone = true;
        break;
    }
    //
    if ( !bDone ) {
        MY_LOGE("cannot release one set history buffer...");
        dumpAllHistoryBufferLocked();
        return UNKNOWN_ERROR;
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
releaseHistoryBufferLocked(
    sp<HistoryBufferSet_T> pHBS
) -> status_t
{
    FUNC_START;
    //
    for ( size_t j=0; j<pHBS->vImageItem.size(); ++j ) {
        auto  streamId  = pHBS->vImageItem.keyAt(j);
        auto& imageItem = pHBS->vImageItem.valueAt(j);
        //
        auto  pProducer = mProducerMap.valueFor(streamId);
        MY_LOGD2("release SB(%p) of producer(%p)", imageItem.pSBuffer.get(), pProducer.get());
        CHECK_ERROR( pProducer->releaseStreamBuffer(imageItem.pSBuffer, imageItem.error ) );
        mBufferSourceReq.removeItem( imageItem.pSBuffer.get() );
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dumpLocked() -> void
{
    dumpProducerInfoLocked();
    dumpAllHistoryBufferLocked();
    dumpAcquiredLocked();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dumpProducerInfoLocked() -> void
{
    for ( size_t i=0; i<mProducerMap.size(); ++i ) {
        auto pProducer = mProducerMap.editValueAt(i);
        MY_LOGI( "[%zu/%zu] Producer(%p)", i, mProducerMap.size(), pProducer.get() );
        pProducer->dump();
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dumpAllHistoryBufferLocked() -> void
{
    for ( size_t i=0; i<mBufferMap.size(); ++i ) {
        auto pHBS = mBufferMap[i];
        MY_LOGI( "[%zu/%zu] HistoryBufferSet(%p) reqNo(%u) status(%s)", i, mBufferMap.size(),
                 pHBS.get(), mBufferMap.keyAt(i), kStatusNames[mBufferMap.editValueAt(i)->status] );
        dumpHistoryBufferLocked(pHBS);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dumpHistoryBufferLocked(android::sp<HistoryBufferSet_T> pTarget) -> void
{
    String8 str;
    if ( pTarget->vImageItem.size() ) {
        str += String8::format( "\tImage(size:%zu):", pTarget->vImageItem.size() );
        for ( size_t j=0; j<pTarget->vImageItem.size(); ++j ) {
            auto pStreamInfo = pTarget->vImageItem[j].pSBuffer->getStreamInfo();
            str += String8::format(" [%zu]:(%#" PRIx64 ":%s):(SB:%p; status:%s; errer:%s);",
                                   j, pStreamInfo->getStreamId(), pStreamInfo->getStreamName(),
                                   pTarget->vImageItem[j].pSBuffer.get(),
                                   kStatusNames[pTarget->vImageItem[j].status],
                                   (pTarget->vImageItem[j].error)? "true":"false" );
        }
    }
    //
    if ( pTarget->vMetaItem.size() ) {
        str += String8::format( "\tMeta(size:%zu):", pTarget->vMetaItem.size() );
        for ( size_t j=0; j<pTarget->vMetaItem.size(); ++j ) {
            auto pStreamInfo = pTarget->vMetaItem[j].pSBuffer->getStreamInfo();
            str += String8::format(" [%zu]:(%#" PRIx64 ":%s):(SB:%p; status:%s; errer:%s);",
                                   j, pStreamInfo->getStreamId(), pStreamInfo->getStreamName(),
                                   pTarget->vMetaItem[j].pSBuffer.get(),
                                   kStatusNames[pTarget->vMetaItem[j].status],
                                   (pTarget->vMetaItem[j].error)? "true":"false" );
        }
    }
    //
    MY_LOGI("%s", str.string());
}


/******************************************************************************
 *
 ******************************************************************************/
auto
HistoryBufferContainerImp::
dumpAcquiredLocked() -> void
{
    MY_LOGI("inflight acquired data(%zu):", mAcquiredMap.size());
    for ( size_t i=0; i<mAcquiredMap.size(); ++i ) {
        auto pData = mAcquiredMap.editValueAt(i);
        MY_LOGI("newReqNo(%u) meta-status(%s)", pData->mNewRequestNo, kStatusNames[pData->mStatus]);
        for ( size_t j=0; j<pData->mReqNoToBuffers.size(); ++j ) {
            String8 str = String8::format("\t[%zu] refReqNo(%u): ", j, pData->mReqNoToBuffers.keyAt(j));
            for ( size_t k=0; k<pData->mReqNoToBuffers[j].size(); ++k ) {
                str += String8::format("stream(%#" PRIx64 ":%s:%p); ",
                    pData->mReqNoToBuffers[j].itemAt(k)->getStreamId(),
                    pData->mReqNoToBuffers[j].itemAt(k)->getStreamInfo()->getStreamName(),
                    pData->mReqNoToBuffers[j].itemAt(k) );
            }
            MY_LOGI("%s", str.string());
        }
    }
}
