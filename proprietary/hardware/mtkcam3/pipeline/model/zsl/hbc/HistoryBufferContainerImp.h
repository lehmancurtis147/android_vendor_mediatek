/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_HBC_HISTORYBUFFERCONTAINERIMP_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_HBC_HISTORYBUFFERCONTAINERIMP_H_
//
#include "StreamBufferProducer.h"

#include <impl/IHistoryBufferContainer.h>

#include <utils/RefBase.h>
#include <utils/Vector.h>

#include <vector>
//

/******************************************************************************
 *
 ******************************************************************************/
//
namespace NSCam {
namespace v3 {


class HistoryBufferContainerImp
    : public IHistoryBufferContainer
{
public:   //// Instantiation.
                    HistoryBufferContainerImp(
                        int32_t const deviceId,
                        std::string const& name
                    );

                    ~HistoryBufferContainerImp() {};

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IHistoryBufferContainer Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Interfaces.
    virtual auto    beginConfigStreams(
                        const android::sp<IStreamInfoSet>& rpInfoSet
                    ) -> android::status_t;

    virtual auto    endConfigStreams() -> android::status_t;

    virtual auto    setBufferSize(
                        const android::KeyedVector<StreamId_T, size_t>& rvBufferSize
                    ) -> android::status_t;

    virtual auto    setBufferPool(
                        const android::KeyedVector<StreamId_T, android::sp<IBufferPool> >& rvBufferPool
                    ) -> android::status_t;

    virtual auto    enqueMetaBuffers(
                        const android::Vector< android::sp<IMetaStreamBuffer> >& rvAppResult,
                        const android::Vector< android::sp<IMetaStreamBuffer> >& rvHalResult,
                        uint32_t const requestNo
                    ) -> android::status_t;

    virtual auto    getAvailableRequestList(
                        android::Vector<uint32_t>& rvRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t;

    virtual auto    acquireBufferSetsOfList(
                        const android::Vector<uint32_t>& rvRequestNo,
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t;

    virtual auto    acquireBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet = nullptr
                    ) -> android::status_t;

    virtual auto    cancelBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo
                    ) -> void;

    virtual auto    cancelBufferSet(
                        BufferSet_T& rBufSet,
                        uint32_t const newRequestNo
                    ) -> void;

    virtual auto    returnBufferSets(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const newRequestNo
                    ) -> void;

    virtual auto    clear(uint32_t const requestNo) -> void;

    virtual auto    clear() -> void;

    virtual auto    dump() -> void;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IStreamBufferProvider Interface.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Interfaces.

    /**
     * deque StreamBuffer.
     *
     * @param[in] iRequestNo: the request number for using this StreamBuffer.
     *                   The callee might implement sync mechanism.
     *
     * @param[in] pStreamInfo: a non-null strong pointer for describing the
                          properties of image stream.
     *
     * @param[out] rpStreamBuffer: a reference to a newly created StreamBuffer.
     *                     The callee must promise its value by deque/allocate
     *                     image buffer heap for generating StreamBuffer.
     *
     * @return 0 indicates success; non-zero indicates an error code.
     */
    virtual MERROR  dequeStreamBuffer(
                        MUINT32 const iRequestNo,
                        android::sp<IImageStreamInfo> const pStreamInfo,
                        android::sp<HalImageStreamBuffer> &rpStreamBuffer
                    );

    /**
     * enque StreamBuffer.
     *
     * @param[in] pStreamInfo: a non-null strong pointer for describing the
                          properties of image stream.
     *
     * @param[in] rpStreamBuffer: a StreamBuffer to be destroyed.
     *                     The callee must enque/destroy the appended image buffer heap.
     *
     * @param[in] bBufStatus: the status of StreamBuffer.
     *
     * @return 0 indicates success; non-zero indicates an error code.
     */
    virtual MERROR  enqueStreamBuffer(
                        android::sp<IImageStreamInfo> const pStreamInfo,
                        android::sp<HalImageStreamBuffer> rpStreamBuffer,
                        MUINT32  bBufStatus
                    );

    virtual void   setBufferEnqueCnt(
                        MUINT32 const iRequestNo,
                        size_t iStreamCnt,
                        size_t iMetaCnt
                    );


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definition.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    definition.
    enum Status_T
    {
        eSTATUS_UNINITED,
        eSTATUS_INITED,
        eSTATUS_DEQUED,
        eSTATUS_ENQUED,
        eSTATUS_PARTIAL_ACQUIRED,
        eSTATUS_ACQUIRED,
        eSTATUS_RETURNED,
        NUM_STATUS,
    };
    static const char* kStatusNames[];

    typedef android::KeyedVector<StreamId_T, android::sp<IMetaStreamInfo> >
                    MetaInfoSet_T;
    typedef android::KeyedVector<StreamId_T, android::sp<IImageStreamInfo> >
                    ImageInfoSet_T;

    struct CfgInfoSet_T {
        ImageInfoSet_T  imageSet;
        MetaInfoSet_T   metaSet;
    };

    struct ImageItem_T
    {
        android::sp<HalImageStreamBuffer> pSBuffer = nullptr;
        Status_T                          status   = eSTATUS_UNINITED;
        bool                              error    = false;
    };

    struct MetaItem_T
    {
        android::sp<IMetaStreamBuffer>    pSBuffer = nullptr;
        Status_T                          status   = eSTATUS_UNINITED;
        bool                              error    = false;
    };

    struct EnqueCnt_T  : android::RefBase
    {
        size_t iStreamCnt = 3;
        size_t iMetaCnt = 2;
    };

    typedef android::DefaultKeyedVector<StreamId_T, ImageItem_T >
                      ImageItemMap_T;
    typedef android::DefaultKeyedVector<StreamId_T, MetaItem_T >
                      MetaItemMap_T;

    struct HistoryBufferSet_T : android::RefBase
    {
        ImageItemMap_T  vImageItem;
        MetaItemMap_T   vMetaItem;
        Status_T        status = eSTATUS_UNINITED;
        bool            error = false;
        //
        size_t          availableImage = 0;
        size_t          availableMeta  = 0;
        //
        void        updateStatus(Status_T value)
                    {
                        for ( size_t i=0; i<vImageItem.size(); ++i ) {
                            vImageItem.editValueAt(i).status = value;
                        }
                        for ( size_t i=0; i<vMetaItem.size(); ++i ) {
                            vMetaItem.editValueAt(i).status = value;
                        }
                        status = value;
                    }
        //
        bool        checkStatus(Status_T value)
                    {
                        int ret = 0;
                        for ( size_t i=0; i<vImageItem.size(); ++i ) {
                            ret |= (vImageItem[i].status^value);
                        }
                        for ( size_t i=0; i<vMetaItem.size(); ++i ) {
                            ret |= (vMetaItem[i].status^value);
                        }
                        ret |= (status^value);
                        return ret==0;
                    }
    };

protected:  ////    operations.
    virtual auto    initOneHistoryBufferLocked(
                        uint32_t const requestNo,
                        Status_T status = eSTATUS_INITED
                    ) -> android::status_t;

    virtual auto    releaseOneHistoryBufferLocked(/*timeout*/) -> android::status_t;

    virtual auto    releaseHistoryBufferLocked(
                        android::sp<HistoryBufferSet_T> pHBS
                    ) -> android::status_t;

    virtual auto    acquireBuffersByRequiredSetLocked(
                        android::Vector<BufferSet_T>& rvBufSet,
                        uint32_t const refReqNo,
                        android::sp<IStreamInfoSet> pRequiredStreamSet
                    ) -> android::status_t;

    virtual auto    enqueStreamBufferOfAcquiredDataLocked(
                        android::sp<HalImageStreamBuffer> rpStreamBuffer,
                        uint32_t const newRequestNo
                    ) -> android::status_t;

protected:  ////    definitions. (acquired data: zsl request)
    struct AcquiredData_T : android::RefBase
    {
                    AcquiredData_T(uint32_t requestNo) : mNewRequestNo(requestNo) {}
        //
        uint32_t                    mNewRequestNo;
        Status_T                    mStatus = eSTATUS_ACQUIRED;

        // value is "refRequestNo". add when acuqire
        // remove when cancel or image enqueStreamBuffer
        android::KeyedVector<HalImageStreamBuffer*, uint32_t>
                                    mBufferToReqNo;
        android::KeyedVector<uint32_t, android::Vector<HalImageStreamBuffer*> >
                                    mReqNoToBuffers;
        android::KeyedVector<uint32_t, Status_T>
                                    mReqNoToMetas;
    };

protected:  ////    operations. (acquired data: zsl request)
    virtual auto    isRemovableAcquiredData(android::sp<AcquiredData_T> pData) -> bool;

protected:  ////    debug
    virtual auto    dumpLocked() -> void;
    virtual auto    dumpProducerInfoLocked() -> void;
    virtual auto    dumpAllHistoryBufferLocked() -> void;
    virtual auto    dumpHistoryBufferLocked(android::sp<HistoryBufferSet_T> pTarget) -> void;
    virtual auto    dumpAcquiredLocked() -> void;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    Instantiation data
    MINT32 const                    mDeviceId = -1;
    std::string const               mName;
    MINT32                          mLogLevel = 0;

protected:  ////    Configuration data
    CfgInfoSet_T                    mCfgSet;
    size_t                          mMaxBufNum = 0;

protected:  ////    Request data
    android::Mutex                  mLock;
    android::Condition              mCond;

    android::DefaultKeyedVector<uint32_t, android::sp<HistoryBufferSet_T> >
                                    mBufferMap;
    android::KeyedVector<StreamId_T, android::sp<StreamBufferProducer> >
                                    mProducerMap;
    // record each image streambuffer and its request number.
    // the request number would be checked when enque image streambuffer to find HistoryBufferSet_T
    // from mBufferMap.
    android::KeyedVector<HalImageStreamBuffer*, uint32_t>
                                    mBufferSourceReq;

    android::KeyedVector<uint32_t, android::sp<EnqueCnt_T>>
                                    mEnqueCnt;

protected:  ////    Request data (zsl request)
    android::KeyedVector<uint32_t, android::sp<AcquiredData_T> >
                                    mAcquiredMap;

};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_ZSL_HBC_HISTORYBUFFERCONTAINERIMP_H_

