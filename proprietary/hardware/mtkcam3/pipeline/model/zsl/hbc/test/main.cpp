/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "MtkCam/test_HBC"
//
#include "../BufferPool.h"
#include "../MyUtils.h"

#include <impl/IHistoryBufferContainer.h>

#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam3/pipeline/utils/streaminfo/MetaStreamInfo.h>
#include <mtkcam3/pipeline/utils/streaminfo/IStreamInfoSetControl.h>

#include <utility>
#include <vector>

using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;

/******************************************************************************
 *
 ******************************************************************************/
//
#if 0
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif

//
#define TEST(cond, result)          do { if ( (cond) == (result) ) { printf("Pass\n"); } else { printf("Failed\n"); } }while(0)
#define FUNCTION_IN     MY_LOGD_IF(1, "+");
//
#define VAL_ALIGN(val, align)    (((val) + ((align) - 1)) & ~((align) - 1))

#define EXPECT_OK(_err_)                                  \
    do {                                                  \
        MERROR const err = (_err_);                       \
        if( err != OK ) {                                 \
            MY_LOGE("err:%d(%s)", err, ::strerror(-err)); \
            return err;                                   \
        }                                                 \
    } while(0)

#define EXPECT_ERROR(_err_)                               \
    do {                                                  \
        MERROR const err = (_err_);                       \
        if( err == OK ) {                                 \
            MY_LOGE("err:%d(%s)", err, ::strerror(-err)); \
            return UNKNOWN_ERROR;                         \
        }                                                 \
    } while(0)

#define EXPECT_OBJ(_obj_)                                 \
    do {                                                  \
        if( _obj_ == nullptr ) {                          \
            MY_LOGE("nullptr occurs!");                   \
            return DEAD_OBJECT;                           \
        }                                                 \
    } while(0)

#define EXPECT_NULL_OBJ(_obj_)                            \
    do {                                                  \
        if( _obj_ != nullptr ) {                          \
            MY_LOGE("must be nullptr!");                  \
            return UNKNOWN_ERROR;                         \
        }                                                 \
    } while(0)

#define EXPECT_TRUE(_val_)                                \
    do {                                                  \
        if( _val_ != true ) {                             \
            MY_LOGE("must be true!");                     \
            return UNKNOWN_ERROR;                         \
        }                                                 \
    } while(0)

/******************************************************************************
 *
 ******************************************************************************/

namespace {
    typedef IHistoryBufferContainer::BufferSet_T            BufferSet_T;

    enum STREAM_ID
    {
        STREAM_ID_IMAGE_START,
        STREAM_ID_IMAGE1            = STREAM_ID_IMAGE_START,
        STREAM_ID_IMAGE2,
        //
        STREAM_ID_META_START,
        STREAM_ID_APPMETACONTROL    = STREAM_ID_META_START,
        STREAM_ID_HALMETACONTROL,
        STREAM_ID_APPMETARESULT,
        STREAM_ID_HALMETARESULT,
        //
        NUM_OF_STREAMS
    };

    static const char* kStreamNames[NUM_OF_STREAMS+1] =
    {
        "IMAGE1",
        "IMAGE2",
        "APP_META_CONTROL",
        "HAL_META_CONTROL",
        "APP_META_RESULT",
        "HAL_META_RESULT",
        "UNKNOWN"
    };

    // instance
    sp<IHistoryBufferContainer>                             mpHBC;
    // configure params
    DefaultKeyedVector<StreamId_T, sp<IImageStreamInfo> >   mvImageInfo;
    DefaultKeyedVector<StreamId_T, sp<IMetaStreamInfo> >    mvMetaInfo;
    // run-time buffers
    uint32_t                                                mSerailNo = 0;
    KeyedVector<uint32_t, KeyedVector<StreamId_T, sp<HalImageStreamBuffer> > >  mvImageSBMap;
    KeyedVector<uint32_t, KeyedVector<StreamId_T, sp<IMetaStreamBuffer> > >     mvMetaSBMap;


    // default image setting, excluding streamId/streamName.
    MUINT32         mImageType      = 0;
    size_t          mMaxBufNum      = 10;
    size_t          mMinInitBufNum  = 2;
    MUINT           mUsage          = 0;
    MINT            mImgFormat      = eImgFmt_YV12;
    MSize           mImageSize      = MSize(640,480);
    MUINT32         mTransform      = 0;
    bool            mLog            = true;
}; // namespace

// historybuffercontainer module basic configuration test.
int test_basic_configure();

// historybuffercontainer module advanced configuration test.
int test_advanced_configure(int type=1);

// configure historybuffercontainer module w/ image&meta streaminfo.
// the default configure params are in namespace.
int test_configure_with(int imgNum=1, int metaNum=1, int type=0);

void dump(const char* pre_str=nullptr);
void dump(BufferSet_T& rBufSet, const char* pre_str=nullptr);
void dump(Vector<BufferSet_T>& rvBufSet, const char* pre_str=nullptr);

// image only deque/enque operation test.
int test_image_enque_deque_ops();
int deque_image_from_historybuffercontainer(size_t count=1, bool auto_counter=true);
int deque_specific_image_from_historybuffercontainer(uint32_t serialNo);
int enque_image_to_historybuffercontainer(size_t count=1);

// meta only deque/enque operation test.
int test_meta_enque_deque_ops();
int enque_meta_to_historybuffercontainer(size_t count=1, bool auto_counter=true);
int enque_specific_meta_to_historybuffercontainer(uint32_t serialNo);


// image & meta streaming deque/enque operation test.
int test_streaming_enque_deque_ops(int type=0);
int async_streaming_ops(size_t count);
int streaming_ops(size_t count);

// image & meta capture acquire/cancel/return operation test.
int test_capture_aquire_cancel_ops();
int test_capture_aquire_return_ops();
int capture_acquire_cancel_ops(size_t count);
int capture_acquire_return_ops(size_t count);
int capture_return_images(BufferSet_T& rvBufSet);

int test_capture_partial_aquire_cancel_ops();
int test_capture_partial_aquire_return_ops();
int capture_partial_acquire_cancel_ops(size_t count, sp<IStreamInfoSet> pPartialSet=nullptr);
int capture_partial_acquire_return_ops(size_t count, sp<IStreamInfoSet> pPartialSet=nullptr);



/******************************************************************************
 *
 ******************************************************************************/
void help()
{
    printf("Camera3 HistoryBufferContainer <test>\n");
}

/******************************************************************************
 *
 ******************************************************************************/
void introduction()
{

}

/******************************************************************************
 *
 ******************************************************************************/
void help(const char* str, bool bForce=false)
{
    if ( mLog || bForce  ) {
        printf("%s\n", str);
    }
    MY_LOGD("%s", str);
}

/******************************************************************************
 *
 ******************************************************************************/
void dump(const char* pre_str)
{
    if ( pre_str!=nullptr )
        help(pre_str);
    //
    if ( mvImageSBMap.size() ) {
        for ( size_t i=0; i<mvImageSBMap.size(); ++i ) {
            String8 str = String8::format("request(%u:%zu@%zu): ", mvImageSBMap.keyAt(i), i, mvImageSBMap.size());
            auto vSB = mvImageSBMap.valueAt(i);
            for( size_t j=0; j<vSB.size(); ++j ) {
                auto streamInfo = vSB.valueAt(j)->getStreamInfo();
                str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", j, vSB.valueAt(j).get(),
                                       streamInfo->getStreamId(), streamInfo->getStreamName() );
            }
            help(str.string());
        }
    }
    //
    if ( mvMetaSBMap.size() ) {
        for ( size_t i=0; i<mvMetaSBMap.size(); ++i ) {
            String8 str = String8::format("request(%u:%zu@%zu): ", mvMetaSBMap.keyAt(i), i, mvMetaSBMap.size());
            auto vSB = mvMetaSBMap.valueAt(i);
            for( size_t j=0; j<vSB.size(); ++j ) {
                auto streamInfo = vSB.valueAt(j)->getStreamInfo();
                str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", j, vSB.valueAt(j).get(),
                                       streamInfo->getStreamId(), streamInfo->getStreamName() );
            }
            help(str.string());
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
void dump(BufferSet_T& rBufSet, const char* pre_str)
{
    if ( pre_str!=nullptr )
        help(pre_str);
    //
    String8 str;
    if ( rBufSet.vImageSet.size() || rBufSet.vMetaSet.size() )
        str += String8::format("\t(reqNo:%u)-", rBufSet.mRefReqNo);

    if ( rBufSet.vImageSet.size() )
        str += String8::format("\tImage: ");
    for ( size_t j=0; j<rBufSet.vImageSet.size(); ++j ) {
        auto pStreamInfo = rBufSet.vImageSet.valueAt(j)->getStreamInfo();
        str += String8::format("[%zu/%zu](%#" PRIx64 ":%s:%p); ", j, rBufSet.vImageSet.size(),
                               rBufSet.vImageSet.keyAt(j), pStreamInfo->getStreamName(),
                               rBufSet.vImageSet.valueAt(j).get() );
    }
    //
    if ( rBufSet.vMetaSet.size() )
        str += String8::format("\tMeta: ");
    for ( size_t j=0; j<rBufSet.vMetaSet.size(); ++j ) {
        auto pStreamInfo = rBufSet.vMetaSet.valueAt(j)->getStreamInfo();
        str += String8::format("[%zu/%zu](%#" PRIx64 ":%s:%p); ", j, rBufSet.vMetaSet.size(),
                               rBufSet.vMetaSet.keyAt(j), pStreamInfo->getStreamName(),
                               rBufSet.vMetaSet.valueAt(j).get() );
    }
    help(str.string());
}


/******************************************************************************
 *
 ******************************************************************************/
void dump(Vector<BufferSet_T>& rvBufSet, const char* pre_str)
{
    if ( pre_str!=nullptr )
        help(pre_str);
    //
    size_t size = rvBufSet.size();
    String8 str = String8::format("bufferset vector size(%zu): ", size );
    help(str.string());
    for ( size_t i=0; i<rvBufSet.size(); ++i ) {
        if ( rvBufSet[i].vImageSet.size() || rvBufSet[i].vMetaSet.size() ) {
            dump(rvBufSet.editItemAt(i));
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IImageStreamInfo>
createImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize,
    MUINT32             transform
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
#define addBufPlane(planes, height, stride)                                         \
        do{                                                                         \
            size_t _height = (size_t)(height);                                      \
            size_t _stride = (size_t)(stride);                                      \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride };    \
            planes.push_back(bufPlane);                                             \
        } while(0)
    switch( imgFormat ) {
        case eImgFmt_YV12:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            break;
        case eImgFmt_NV21:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w);
            break;
        case eImgFmt_YUY2:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w << 1);
            break;
        default:
            MY_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<IImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, transform
                );

    if( pStreamInfo == NULL ) {
        MY_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_basic_configure()
{
    help("Basic Configuration test");
    for ( int i=0; i<=STREAM_ID_META_START; ++i ) {
        for ( int j=0; j<=NUM_OF_STREAMS-STREAM_ID_META_START; ++j ) {
            String8 str = String8::format("Configuration test with image(%d) meta(%d)", i, j);
            help(str.string());
            EXPECT_OK( test_configure_with(i, j, 0) );
            mpHBC->dump();
            mpHBC = nullptr;
        }
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_advanced_configure(int type)
{
    String8 str = String8::format("Advanced Configuration test (type:%d)", type);
    help(str.string());
    EXPECT_OK( test_configure_with(2, 2, type) );
    mpHBC->dump();
    mpHBC = nullptr;
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int resetBufferPool()
{
    // create new streaminfo for assign BufferPool allocation.
    // modify buffer size of max numbers & min inited numbers.
    sp<IImageStreamInfo> pImageInfo =
        createImageStreamInfo(kStreamNames[STREAM_ID_IMAGE_START], STREAM_ID_IMAGE_START,
                              mImageType, mMaxBufNum+5, mMaxBufNum+5, mUsage,
                              mImgFormat, mImageSize, mTransform);
    sp<BufferPool> pPool = new BufferPool(pImageInfo);
    //
    KeyedVector<StreamId_T, sp<IBufferPool> > vPool;
    for( size_t i=0; i<mvImageInfo.size(); ++i ) {
        vPool.add(mvImageInfo.keyAt(i), pPool);
    }
    EXPECT_OK( mpHBC->setBufferPool(vPool) );
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int resetBufferSize()
{
    KeyedVector<StreamId_T, size_t> vSize;
    for( size_t i=0; i<mvImageInfo.size(); ++i ) {
        auto streamId = mvImageInfo.keyAt(i);
        vSize.add(streamId, mMaxBufNum+3+i);
        EXPECT_OK( mpHBC->setBufferSize(vSize) );
    }
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_configure_with(int imgNum, int metaNum, int type)
{
    // invalid arguments check.
    if ( imgNum  > STREAM_ID_META_START ||
         metaNum > (NUM_OF_STREAMS-STREAM_ID_META_START) )
        return BAD_VALUE;
    // cleare previous setting.
    mvImageInfo.clear();
    mvMetaInfo.clear();
    mSerailNo = 0;
    mvImageSBMap.clear();
    mvMetaSBMap.clear();
    //
    // image / meta streaminfo preparation.
    for ( int i=0; i<imgNum; ++i ) {
        sp<IImageStreamInfo> pImageInfo =
            createImageStreamInfo(kStreamNames[STREAM_ID_IMAGE_START+i], STREAM_ID_IMAGE_START+i,
                                  mImageType, mMaxBufNum, mMinInitBufNum, mUsage,
                                  mImgFormat, mImageSize, mTransform);
        mvImageInfo.add(STREAM_ID_IMAGE_START+i, pImageInfo);
    }
    for ( int i=0; i<metaNum; ++i ) {
        sp<IMetaStreamInfo> pMetaInfo =
            new MetaStreamInfo(kStreamNames[STREAM_ID_META_START+i], STREAM_ID_META_START+i,
                               mImageType, mMaxBufNum, mMinInitBufNum);
        mvMetaInfo.add(STREAM_ID_META_START+i, pMetaInfo);
    }
    //
    // arguments preparation.
    sp<IStreamInfoSetControl> pInfoSetCtrl = IStreamInfoSetControl::create();
    for ( size_t i=0; i<mvImageInfo.size(); ++i )
        pInfoSetCtrl->editAppImage().addStream(mvImageInfo[i]);
    for ( size_t i=0; i<mvMetaInfo.size(); ++i )
        pInfoSetCtrl->editAppMeta().addStream(mvMetaInfo[i]);
    //
    // module test & dump / clear.
    mpHBC = IHistoryBufferContainer::createInstance(0, LOG_TAG);
    EXPECT_OBJ( mpHBC.get() );
    EXPECT_OK( mpHBC->beginConfigStreams(pInfoSetCtrl) );
    //
    if ( type==2 ) {
        help("resetBufferPool() +");
        EXPECT_OK( resetBufferPool() );
        help("resetBufferPool() -");
    }
    //
    EXPECT_OK( mpHBC->endConfigStreams() );
    //
    if ( type==1 ) {
        help("resetBufferSize() +");
        EXPECT_OK( resetBufferSize() );
        help("resetBufferSize() -");
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int deque_specific_image_from_historybuffercontainer(uint32_t serialNo)
{
    FUNC_START;
    sp<IStreamBufferProvider> pSBProvider = mpHBC;
    EXPECT_OBJ( pSBProvider.get() );
    // for ( uint32_t req=0; req<count; ++req ) {
        String8 str = String8::format("req(%u): ", serialNo);
        KeyedVector<StreamId_T, sp<HalImageStreamBuffer> > vSB;
        bool newSB = false;
        for( size_t i=0; i<mvImageInfo.size(); ++i ) {
            usleep(5*1000);
            sp<HalImageStreamBuffer> pSB;
            // EXPECT_OK( mpHBC->dequeStreamBuffer(serialNo, mvImageInfo.valueAt(i), pSB) );
            // EXPECT_OBJ( pSB.get() );
            if ( mpHBC->dequeStreamBuffer(serialNo, mvImageInfo.valueAt(i), pSB)!=OK || pSB==0 ) {
                // str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                //                        mvImageInfo.valueAt(i)->getStreamId(), mvImageInfo.valueAt(i)->getStreamName() );
                continue;
            }
            newSB = true;
            vSB.add( mvImageInfo.keyAt(i), pSB);
            str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                                   mvImageInfo.valueAt(i)->getStreamId(), mvImageInfo.valueAt(i)->getStreamName() );
        }
        if ( newSB ) {
            mvImageSBMap.add(serialNo, vSB);
        }
        help(str.string());
        //
        // if ( auto_counter )
        //     mSerailNo++;
    // }
    //
    FUNC_END;
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
int deque_image_from_historybuffercontainer(size_t count, bool auto_counter)
{
    FUNC_START;
    sp<IStreamBufferProvider> pSBProvider = mpHBC;
    EXPECT_OBJ( pSBProvider.get() );
    for ( uint32_t req=0; req<count; ++req ) {
        String8 str = String8::format("req(%u): ", mSerailNo);
        KeyedVector<StreamId_T, sp<HalImageStreamBuffer> > vSB;
        bool newSB = false;
        for( size_t i=0; i<mvImageInfo.size(); ++i ) {
            usleep(5*1000);
            sp<HalImageStreamBuffer> pSB;
            // EXPECT_OK( mpHBC->dequeStreamBuffer(mSerailNo, mvImageInfo.valueAt(i), pSB) );
            // EXPECT_OBJ( pSB.get() );
            if ( mpHBC->dequeStreamBuffer(mSerailNo, mvImageInfo.valueAt(i), pSB)!=OK || pSB==0 ) {
                // str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                //                        mvImageInfo.valueAt(i)->getStreamId(), mvImageInfo.valueAt(i)->getStreamName() );
                continue;
            }
            newSB = true;
            vSB.add( mvImageInfo.keyAt(i), pSB);
            str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                                   mvImageInfo.valueAt(i)->getStreamId(), mvImageInfo.valueAt(i)->getStreamName() );
        }
        if ( newSB ) {
            mvImageSBMap.add(mSerailNo, vSB);
        }
        help(str.string());
        //
        if ( auto_counter )
            mSerailNo++;
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int enque_image_to_historybuffercontainer(size_t count)
{
    FUNC_START;
    if ( count > mvImageSBMap.size() )
        count = mvImageSBMap.size();
    //
    sp<IStreamBufferProvider> pSBProvider = mpHBC;
    EXPECT_OBJ( pSBProvider.get() );
    for ( size_t i=0; i<count; ++i ) {
        String8 str = String8::format("remove %zu-th SBs from map: ", i);
        auto vSB = mvImageSBMap.editValueAt(i);
        for ( size_t j=0; j<vSB.size(); ++j ) {
            auto pStreamInfo = vSB.editValueAt(j)->getStreamInfo();
            str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", j, vSB.valueAt(j).get(),
                                   pStreamInfo->getStreamId(), pStreamInfo->getStreamName() );
            // currently, not use pStreamInfo...
            sp<IImageStreamInfo> const pInfo = mvImageInfo.valueFor(vSB.keyAt(j));
            EXPECT_OK( mpHBC->enqueStreamBuffer( pInfo, vSB.editValueAt(j),
                                                 STREAM_BUFFER_STATUS::WRITE_OK ) );
        }
        // mvImageSBMap.removeItemsAt(i);
        help(str.string());
    }
    mvImageSBMap.removeItemsAt(0, count);
    //
    FUNC_END;
    return OK;
}


#define TEST_QUE_COUNT          (10)
/******************************************************************************
 *
 ******************************************************************************/
int test_image_enque_deque_ops()
{
    // basic configuration with default settings of image:2 and meta:0
    EXPECT_OK( test_configure_with(2,0,0) );
    // now, use mpHBC to test with request stage functioanlity.
    mpHBC->dump();
    String8 str;
    help("start of image enque/deque ops test...");
    //
    // deque from HistoryBufferContainer
    help("\ndeque ops...");
    EXPECT_OK( deque_image_from_historybuffercontainer(TEST_QUE_COUNT+2) );
    // help("dump local map after deque...\n");
    dump("\ndump local map after deque...");
    mpHBC->dump();
    //
    // enque to HistoryBufferContainer
    help("\nenque ops...");
    EXPECT_OK( enque_image_to_historybuffercontainer(2) );
    // help("dump local map after enque...");
    dump("\ndump local map after enque...");
    mpHBC->dump();
    //
    // re-deque from HistoryBufferContainer
    help("\nre-deque ops...");
    EXPECT_OK( deque_image_from_historybuffercontainer(4) );
    // help("dump local map after re-deque...");
    dump("\ndump local map after re-deque...");
    mpHBC->dump();
    //
    mvImageSBMap.clear();
    EXPECT_TRUE( mvImageSBMap.size()==0 );
    mpHBC = nullptr;
    help("end of image enque/deque ops test");
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int enque_specific_meta_to_historybuffercontainer(uint32_t serialNo)
{
    FUNC_START;
    // Vector<BufferSet_T> vBufferSet;
    // for ( uint32_t req=0; req<count; ++req ) {
        String8 str = String8::format("req(%u): ", serialNo);
        KeyedVector<StreamId_T, sp<IMetaStreamBuffer> > vSB;
        Vector<sp<IMetaStreamBuffer> > vApp;
        Vector<sp<IMetaStreamBuffer> > vHal;
        for ( size_t i=0; i<mvMetaInfo.size(); ++i ) {
            sp<HalMetaStreamBuffer> pSB =
                HalMetaStreamBuffer::Allocator(mvMetaInfo.valueAt(i).get())();
            str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                                   mvMetaInfo.valueAt(i)->getStreamId(), mvMetaInfo.valueAt(i)->getStreamName() );
            vSB.add( mvMetaInfo.keyAt(i), pSB );
            if ( i==0 )
                vApp.push_back(pSB);
            else
                vHal.push_back(pSB);
        }
        mvMetaSBMap.add(serialNo, vSB);
        mpHBC->enqueMetaBuffers(vApp, vHal, serialNo);
        help(str.string());
        //
        // if ( auto_counter )
        //     mSerailNo++;
    // }
    //
    FUNC_END;
    return OK;
}



/******************************************************************************
 *
 ******************************************************************************/
int enque_meta_to_historybuffercontainer(size_t count, bool auto_counter)
{
    FUNC_START;
    // Vector<BufferSet_T> vBufferSet;
    for ( uint32_t req=0; req<count; ++req ) {
        String8 str = String8::format("req(%u): ", mSerailNo);
        KeyedVector<StreamId_T, sp<IMetaStreamBuffer> > vSB;
        Vector<sp<IMetaStreamBuffer> > vApp;
        Vector<sp<IMetaStreamBuffer> > vHal;
        for ( size_t i=0; i<mvMetaInfo.size(); ++i ) {
            sp<HalMetaStreamBuffer> pSB =
                HalMetaStreamBuffer::Allocator(mvMetaInfo.valueAt(i).get())();
            str += String8::format("[%zu]%p(%#" PRIx64 ":%s); ", i, pSB.get(),
                                   mvMetaInfo.valueAt(i)->getStreamId(), mvMetaInfo.valueAt(i)->getStreamName() );
            vSB.add( mvMetaInfo.keyAt(i), pSB );
            if ( i==0 )
                vApp.push_back(pSB);
            else
                vHal.push_back(pSB);
        }
        mpHBC->enqueMetaBuffers(vApp, vHal, mSerailNo);
        help(str.string());
        //
        if ( auto_counter )
            mSerailNo++;
    }
    //
    FUNC_END;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_meta_enque_deque_ops()
{
    // basic configuration with default settings of image:0 and meta:4
    EXPECT_OK( test_configure_with(0,4,0) );
    // now, use mpHBC to test with request stage functioanlity.
    mpHBC->dump();
    help("start of meta enque/deque ops test...");
    //
    // enque to HistoryBufferContainer
    help("\nenque meta ops...");
    EXPECT_OK( enque_meta_to_historybuffercontainer(TEST_QUE_COUNT+2) );
    // help("dump local map after enque...\n");
    dump("\ndump local map after enque...");
    mpHBC->dump();
    //
    // enque to HistoryBufferContainer again...
    help("\nenque meta ops again...");
    EXPECT_OK( enque_meta_to_historybuffercontainer(5) );
    // help("dump local map after enque again...");
    dump("\ndump local map after enque again...");
    mpHBC->dump();
    //
    mvMetaSBMap.clear();
    EXPECT_TRUE( mvMetaSBMap.size()==0 );
    mpHBC = nullptr;
    help("end of meta enque/deque ops test");
    return OK;

}

#define REQ_P1ENQ_DEPTH                         (2)
#define P1ENQ_P1DEQ_DEPTH                       (2)
/******************************************************************************
 *
 ******************************************************************************/
int async_streaming_ops(size_t count)
{
    // for each request:
    size_t vec_size = count + REQ_P1ENQ_DEPTH + P1ENQ_P1DEQ_DEPTH+1;

    std::vector<int> v_enqueMeta(vec_size,  -1);
    std::vector<int> v_dequeImage(vec_size, -1);
    std::vector<int> v_enqueImage(vec_size, -1);

    // count: 10
    // vec_size: 15
    // vec          0   1   2   3   4   5   6   7   8   9   10  11  12  13  14
    // deque image          0   1   2   3   4   5   6   7   8   9
    // enque meta                   0   1   2   3   4   5   6   7   8   9
    // enque image                      0   1   2   3   4   5   6   7   8   9

    for ( size_t i=0; i<count; ++i ) {
        // v_enqueMeta[i] = (int)i;
        // actually, meta will callback before image enque...
        v_dequeImage[i+REQ_P1ENQ_DEPTH] = (int)i;
        v_enqueImage[i+P1ENQ_P1DEQ_DEPTH+P1ENQ_P1DEQ_DEPTH+1] = (int)i;
        v_enqueMeta[i+P1ENQ_P1DEQ_DEPTH+P1ENQ_P1DEQ_DEPTH] = (int)i;
    }

    for ( size_t i=0; i<vec_size; ++i ) {
        // 2. deque image buffers using IStreamBufferProvider I/F (T+2)
        if ( v_dequeImage[i]>=0 )
            EXPECT_OK( deque_specific_image_from_historybuffercontainer(v_dequeImage[i]));

        // 3. empty processing (pass1) (T+2->T+4)

        // 4. enque image buffers using IStreamBufferProvider I/F (T+4)
        if ( v_enqueImage[i]>=0 )
            EXPECT_OK( enque_image_to_historybuffercontainer(1) );
        // 5. enque meta result (T+4)
        if ( v_enqueMeta[i]>=0 )   // increase counter because time chart
            EXPECT_OK( enque_specific_meta_to_historybuffercontainer(v_enqueMeta[i]));
        mpHBC->dump();
    }
    //
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
int streaming_ops(size_t count)
{
    // for each request:
    for ( size_t i=0; i<count; ++i ) {
        // 1. enque app_control (T) -> no need
        // 2. deque image buffers using IStreamBufferProvider I/F (T+2)
        EXPECT_OK( deque_image_from_historybuffercontainer(1, false) );
        // 3. empty processing (pass1) (T+2->T+4)
        // 4. enque image buffers using IStreamBufferProvider I/F (T+4)
        EXPECT_OK( enque_image_to_historybuffercontainer(1) );
        // 5. enque meta result (T+4)
        EXPECT_OK( enque_meta_to_historybuffercontainer(1, true) );
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_streaming_enque_deque_ops(int type)
{
    // basic configuration with default settings of image:2 and meta:4
    // simulate real case configuration
    // image: imgo + rrzo
    // meta: app_control + p1_app_result + p1_hal_result
    if ( type==0 ) {
        EXPECT_OK( test_configure_with(2,4,0) );
        mpHBC->dump();
        // request stage, for each request:
        EXPECT_OK( streaming_ops(TEST_QUE_COUNT) );
        // help("after streaming requests");
        dump("\nafter streaming requests");
        // mpHBC->dump();
        //
        EXPECT_OK( streaming_ops(15) );
        // help("after another 5 streaming requests...");
        dump("\nafter another 5 streaming requests...");
    }
    else if ( type==1 ) {
        EXPECT_OK( test_configure_with(2,4,0) );
        mpHBC->dump();
        // request stage, for each request:
        EXPECT_OK( async_streaming_ops(TEST_QUE_COUNT) );
        dump("\nafter async streaming test...");
    }
    // mpHBC->dump();
    mpHBC = nullptr;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int capture_acquire_cancel_ops(size_t count)
{
    Vector<uint32_t> vReqNo;
    mpHBC->getAvailableRequestList(vReqNo);
    String8 str = String8::format("available request(%zu): ", vReqNo.size() );
    for ( size_t i=0; i<vReqNo.size(); ++i ) {
        str += String8::format("[%u@%zu];", vReqNo[i], i);
    }
    help(str.string());
    //
    // acquire/cancel test:
    uint32_t serialNo = vReqNo[vReqNo.size()-1]+1;
    //
    if ( count==1 ) {
        Vector<BufferSet_T> vBufferSet, vBufferSet2;
        Vector<uint32_t> vReqAcquired;
        vReqAcquired.add(vReqNo[0]);
        // acquire one
        EXPECT_OK(  mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet, serialNo) );
        EXPECT_TRUE(vBufferSet.size()==1);
        dump(vBufferSet, "\nacquire-1:");
        // double acquire the same serialNo
        EXPECT_ERROR( mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet2, serialNo+1) );
        EXPECT_TRUE(vBufferSet2.size()==0);
        dump(vBufferSet2, "\nacquire-2:");
        //
        // cancel one
        mpHBC->cancelBufferSet(vBufferSet.editItemAt(0), serialNo);
        dump(vBufferSet, "\ncancel-1:");
    } else if ( count>1 ) {
        serialNo++;
        Vector<BufferSet_T> vBufferSet_Multiple;
        // acquire multiple (first one has been acquired before)
        EXPECT_OK(  mpHBC->acquireBufferSets(vBufferSet_Multiple, serialNo) );
        EXPECT_TRUE(vBufferSet_Multiple.size()>0);
        dump(vBufferSet_Multiple, "\nacquire-multiple:");
        mpHBC->cancelBufferSets(vBufferSet_Multiple, serialNo);
        // help("cancel-3:");
        dump(vBufferSet_Multiple, "\ncancel-multiple:");
    } else {
        MY_LOGE("invalid parameter(count:%zu)!", count);
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int capture_return_images(BufferSet_T& rBufSet)
{
    sp<IStreamBufferProvider> pSBProvider = mpHBC;
    KeyedVector<StreamId_T, sp<HalImageStreamBuffer> > vSB;
    auto& vImageSet = rBufSet.vImageSet;
    MY_LOGD("meta(%zu) image(%zu)", rBufSet.vMetaSet.size(), rBufSet.vImageSet.size());
    // not need to deque. directly use from acquire.
    for ( size_t i=0; i<vImageSet.size(); ++i )
        vSB.add(vImageSet.keyAt(i), vImageSet.valueAt(i));
    //
    for ( size_t i=0; i<vSB.size(); ++i ) {
        sp<IImageStreamInfo> const pInfo = mvImageInfo.valueFor(vSB.keyAt(i));
        pSBProvider->enqueStreamBuffer( pInfo, vSB.editValueAt(i), STREAM_BUFFER_STATUS::WRITE_OK);
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int capture_acquire_return_ops(size_t count)
{
    Vector<uint32_t> vReqNo;
    mpHBC->getAvailableRequestList(vReqNo);
    String8 str = String8::format("available request(%zu): ", vReqNo.size() );
    for ( size_t i=0; i<vReqNo.size(); ++i ) {
        str += String8::format("[%u@%zu];", vReqNo[i], i);
    }
    help(str.string());
    //
    // acquire/return test:
    uint32_t serialNo = vReqNo[vReqNo.size()-1]+1;
    //
    if ( count==1 ) {
        Vector<BufferSet_T> vBufferSet;
        Vector<uint32_t> vReqAcquired;
        vReqAcquired.add(vReqNo[0]);
        // acquire one
        EXPECT_OK( mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet, serialNo) );
        EXPECT_TRUE(vBufferSet.size()==1);
        dump(vBufferSet, "\nacquire-1:");
#if 1
        mpHBC->dump();
        mpHBC->returnBufferSets(vBufferSet, serialNo);
        help("return");
        mpHBC->dump();
        EXPECT_OK( capture_return_images(vBufferSet.editItemAt(0) ) );    // image
        help("enque image");
        mpHBC->dump();
#else
        EXPECT_OK( capture_return_images(vBufferSet.editItemAt(0) ) );    // image
        mpHBC->returnBufferSets(vBufferSet, serialNo);
#endif
    } else if ( count>1 ) {
        serialNo++;
        Vector<BufferSet_T> vBufferSet, vBufferSet2;
        // acquire multiple [0,1,2,3]
        EXPECT_OK( mpHBC->acquireBufferSets( vBufferSet, serialNo) );
        EXPECT_TRUE( vBufferSet.size()>0 );
        dump(vBufferSet, "\nacquire-multiple:");

        // after flow decision...
        // cancel [2]; return [0,1][3,4,...]=>call only one times
        vBufferSet2.add( vBufferSet.editItemAt(2) );
        dump(vBufferSet2, "\ncancel-one:");
        mpHBC->cancelBufferSets(vBufferSet2, serialNo);
        vBufferSet.removeItemsAt(2);
        //
        mpHBC->returnBufferSets(vBufferSet, serialNo);
        help("return");
        mpHBC->dump();
        for ( size_t i=0; i<vBufferSet.size(); ++i ) {
            EXPECT_OK( capture_return_images( vBufferSet.editItemAt(i) ) );
        }
        help("enque image");
        mpHBC->dump();
    } else {
        MY_LOGE("invalid parameter(count:%zu)!", count);
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_capture_aquire_cancel_ops()
{
    // basic configuration with default settings of image:2 and meta:4
    // simulate real case configuration
    // image: imgo + rrzo
    // meta: app_control + p1_app_result + p1_hal_result
    EXPECT_OK( test_configure_with(2,4,0) );
    mpHBC->dump();
    // request stage, for each request:
    EXPECT_OK( streaming_ops(TEST_QUE_COUNT+5) );
    dump("\nsingle acquire/cancel test (full)");
    mpHBC->dump();
    EXPECT_OK( capture_acquire_cancel_ops(1) );
    //
    dump("\nmultiple acquire/cancel test (full)");
    EXPECT_OK( capture_acquire_cancel_ops(4) );
    mpHBC->dump();
    //
    mpHBC = nullptr;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_capture_aquire_return_ops()
{
    // basic configuration with default settings of image:2 and meta:4
    // simulate real case configuration
    // image: imgo + rrzo
    // meta: app_control + p1_app_result + p1_hal_result
    EXPECT_OK( test_configure_with(2,4,0) );
    mpHBC->dump();
    // request stage, for each request:
    EXPECT_OK( streaming_ops(TEST_QUE_COUNT+5) );
    mpHBC->dump();
    //
    dump("\nsingle acquire/return test (full)");
    EXPECT_OK( capture_acquire_return_ops(1) );
    mpHBC->dump();
    //
    dump("\nmultiple acquire/return test (full)");
    EXPECT_OK( capture_acquire_return_ops(4) );
    mpHBC->dump();
    //
    mpHBC = nullptr;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int capture_partial_acquire_cancel_ops(size_t count, sp<IStreamInfoSet> pPartialSet)
{
    Vector<uint32_t> vReqNo;
    mpHBC->getAvailableRequestList(vReqNo, pPartialSet);
    String8 str = String8::format("available request(%zu): ", vReqNo.size() );
    for ( size_t i=0; i<vReqNo.size(); ++i ) {
        str += String8::format("[%u@%zu];", vReqNo[i], i);
    }
    help(str.string());
    //
    // acquire/cancel test:
    uint32_t serialNo = vReqNo[vReqNo.size()-1]+1;
    //
    if ( count==1 ) {
        Vector<BufferSet_T> vBufferSet, vBufferSet2;
        Vector<uint32_t> vReqAcquired;
        vReqAcquired.add(vReqNo[0]);
        // acquire one
        EXPECT_OK(  mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet, serialNo, pPartialSet) );
        EXPECT_TRUE(vBufferSet.size()==1);
        dump(vBufferSet, "\nacquire-1:");
        // double acquire the same serialNo
        EXPECT_ERROR( mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet2, serialNo+1, pPartialSet) );
        EXPECT_TRUE(vBufferSet2.size()==0);
        dump(vBufferSet2, "\nacquire-2:");
        mpHBC->dump();
        //
        // cancel one
        mpHBC->cancelBufferSet(vBufferSet.editItemAt(0), serialNo);
        dump(vBufferSet, "\ncancel-1:");
        mpHBC->dump();
    } else if ( count>1 ) {
        serialNo++;
        Vector<BufferSet_T> vBufferSet_Multiple;
        // acquire multiple (first one has been acquired before)
        EXPECT_OK(  mpHBC->acquireBufferSets(vBufferSet_Multiple, serialNo, pPartialSet) );
        EXPECT_TRUE(vBufferSet_Multiple.size()>0);
        dump(vBufferSet_Multiple, "\nacquire-multiple:");
        mpHBC->dump();

        mpHBC->cancelBufferSets(vBufferSet_Multiple, serialNo);
        dump(vBufferSet_Multiple, "\ncancel-multiple:");
        mpHBC->dump();
    } else {
        MY_LOGE("invalid parameter(count:%zu)!", count);
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int capture_partial_acquire_return_ops(size_t count, sp<IStreamInfoSet> pPartialSet)
{
    Vector<uint32_t> vReqNo;
    mpHBC->getAvailableRequestList(vReqNo);
    String8 str = String8::format("available request(%zu): ", vReqNo.size() );
    for ( size_t i=0; i<vReqNo.size(); ++i ) {
        str += String8::format("[%u@%zu];", vReqNo[i], i);
    }
    help(str.string());
    //
    // acquire/return test:
    uint32_t serialNo = vReqNo[vReqNo.size()-1]+1;
    //
    if ( count==1 ) {
        Vector<BufferSet_T> vBufferSet;
        Vector<uint32_t> vReqAcquired;
        vReqAcquired.add(vReqNo[0]);
        // acquire one
        EXPECT_OK( mpHBC->acquireBufferSetsOfList(vReqAcquired, vBufferSet, serialNo, pPartialSet) );
        EXPECT_TRUE(vBufferSet.size()==1);
        dump(vBufferSet, "\nacquire-1:");
#if 1
        mpHBC->dump();
        mpHBC->returnBufferSets(vBufferSet, serialNo);
        help("return");
        mpHBC->dump();
        EXPECT_OK( capture_return_images(vBufferSet.editItemAt(0) ) );    // image
        help("enque image");
        mpHBC->dump();
#else
        EXPECT_OK( capture_return_images(vBufferSet.editItemAt(0) ) );    // image
        mpHBC->returnBufferSets(vBufferSet, serialNo);
#endif
    } else if ( count>1 ) {
        serialNo++;
        Vector<BufferSet_T> vBufferSet, vBufferSet2;
        // acquire multiple [0,1,2,3]
        EXPECT_OK( mpHBC->acquireBufferSets( vBufferSet, serialNo, pPartialSet) );
        EXPECT_TRUE( vBufferSet.size()>0 );
        dump(vBufferSet, "\nacquire-multiple:");

        // after flow decision...
        // cancel [2]; return [0,1][3,4,...]=>call only one times
        vBufferSet2.add( vBufferSet.editItemAt(2) );
        dump(vBufferSet2, "\ncancel-one:");
        mpHBC->cancelBufferSets(vBufferSet2, serialNo);
        vBufferSet.removeItemsAt(2);
        //
        mpHBC->returnBufferSets(vBufferSet, serialNo);
        help("return");
        mpHBC->dump();
        for ( size_t i=0; i<vBufferSet.size(); ++i ) {
            EXPECT_OK( capture_return_images(vBufferSet.editItemAt(i) ) );
        }
        help("enque image");
        mpHBC->dump();
    } else {
        MY_LOGE("invalid parameter(count:%zu)!", count);
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_capture_partial_aquire_cancel_ops()
{
    // basic configuration with default settings of image:2 and meta:4
    // simulate real case configuration
    // image: imgo + rrzo
    // meta: app_control + p1_app_result + p1_hal_result
    EXPECT_OK( test_configure_with(2,4,0) );
    mpHBC->dump();
    // prepare partial stream set.
    // image / meta streaminfo preparation.
    sp<IStreamInfoSetControl> pInfoSetCtrl = IStreamInfoSetControl::create();
    // for ( size_t i=0; i<mvImageInfo.size()-1; ++i )
        pInfoSetCtrl->editAppImage().addStream(mvImageInfo[0]);
    for ( size_t i=0; i<mvMetaInfo.size(); ++i )
        pInfoSetCtrl->editAppMeta().addStream(mvMetaInfo[i]);
    //
    // request stage, for each request:
    EXPECT_OK( streaming_ops(TEST_QUE_COUNT+5) );
    dump("\nsingle partial acquire/cancel test (full)");
    mpHBC->dump();
    EXPECT_OK( capture_partial_acquire_cancel_ops(1, pInfoSetCtrl) );
    //
    dump("\nmultiple partial acquire/cancel test (full)");
    EXPECT_OK( capture_partial_acquire_cancel_ops(4, pInfoSetCtrl) );
    //
    mpHBC = nullptr;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int test_capture_partial_aquire_return_ops()
{
    // basic configuration with default settings of image:2 and meta:4
    // simulate real case configuration
    // image: imgo + rrzo
    // meta: app_control + p1_app_result + p1_hal_result
    EXPECT_OK( test_configure_with(2,4,0) );
    mpHBC->dump();
    // prepare partial stream set.
    // image / meta streaminfo preparation.
    sp<IStreamInfoSetControl> pInfoSetCtrl = IStreamInfoSetControl::create();
    // for ( size_t i=0; i<mvImageInfo.size()-1; ++i )
        pInfoSetCtrl->editAppImage().addStream(mvImageInfo[0]);
    for ( size_t i=0; i<mvMetaInfo.size(); ++i )
        pInfoSetCtrl->editAppMeta().addStream(mvMetaInfo[i]);
    //
    // request stage, for each request:
    EXPECT_OK( streaming_ops(TEST_QUE_COUNT+5) );
    mpHBC->dump();
    //
    dump("\nsingle partial acquire/return test (full)");
    EXPECT_OK( capture_partial_acquire_return_ops(1, pInfoSetCtrl) );
    mpHBC->dump();
    //
    dump("\nmultiple partial acquire/return test (full)");
    EXPECT_OK( capture_partial_acquire_return_ops(4, pInfoSetCtrl) );
    mpHBC->dump();
    //
    mpHBC = nullptr;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
int main(int argc, char** argv)
{
    help();
    if (argc < 3) return 0;

    int t_scen = atoi(argv[1]);
    int t_case = atoi(argv[2]);

#define VALIDATE_RESULT(_err_, _scen_, _case_)                          \
    do {                                                                \
        String8 result = String8::format("[%d,%d]", _scen_, _case_);    \
        if (_err_==OK) {                                                \
            result += String8::format("->PASS");                        \
        } else {                                                        \
            result += String8::format("->FAIL");                        \
        }                                                               \
        help(result.string(), true);                                    \
    } while (0)

    String8 str = String8::format("============================================================\n");
    str += String8::format("start test: %d ; %d \n", t_scen, t_case );
    str += String8::format("============================================================\n");
    help(str.string());

    // start test.
    // configuration test
    if ( t_scen == 1 ) {
        if ( t_case == 1 )
            VALIDATE_RESULT( test_basic_configure(), 1, 1 );
        else if ( t_case == 2 )
            VALIDATE_RESULT( test_advanced_configure(1), 1, 2 );
        else if ( t_case == 3 )
            VALIDATE_RESULT( test_advanced_configure(2), 1, 3 );
        else {
            VALIDATE_RESULT( test_basic_configure(), 1, 1 );
            VALIDATE_RESULT( test_advanced_configure(1), 1, 2 );
            VALIDATE_RESULT( test_advanced_configure(2), 1, 3 );
        }
    }
    // streaming enque/deque test
    else if ( t_scen == 2 ) {
        if ( t_case == 1 )
            // image enque/deque test
            VALIDATE_RESULT( test_image_enque_deque_ops(), 2, 1  );
        else if ( t_case == 2 )
            // metadata enque/deque test
            VALIDATE_RESULT( test_meta_enque_deque_ops(), 2, 2 );
        else if ( t_case == 3 )
            // image & metadata streaming enque/deque test
            VALIDATE_RESULT( test_streaming_enque_deque_ops(0), 2, 3 );
        else if ( t_case == 4 )
            // image & metadata streaming enque/deque test
            VALIDATE_RESULT( test_streaming_enque_deque_ops(1), 2, 4 );
        else {
            VALIDATE_RESULT( test_image_enque_deque_ops(), 2, 1 );
            VALIDATE_RESULT( test_meta_enque_deque_ops(), 2, 2 );
            VALIDATE_RESULT( test_streaming_enque_deque_ops(0), 2, 3 );
            VALIDATE_RESULT( test_streaming_enque_deque_ops(1), 2, 4 );
        }
    }
    // capture test
    else if ( t_scen == 3 ) {
        if ( t_case == 1 )
            // fully acquire/cancel test (one, multiple)
            VALIDATE_RESULT( test_capture_aquire_cancel_ops(), 3, 1 );
        else if ( t_case == 2 )
            // fully acquire/return test (one, multiple)
            VALIDATE_RESULT( test_capture_aquire_return_ops(), 3, 2 );
        else if ( t_case == 3 )
            // partial acquire/cancel test (one, multiple)
            VALIDATE_RESULT( test_capture_partial_aquire_cancel_ops(), 3, 3 );
        else if ( t_case == 4 )
            // partial acquire/return test (one, multiple)
            VALIDATE_RESULT( test_capture_partial_aquire_return_ops(), 3, 4 );
        else {
        VALIDATE_RESULT( test_capture_aquire_cancel_ops(), 3, 1 );
        VALIDATE_RESULT( test_capture_aquire_return_ops(), 3, 2 );
        VALIDATE_RESULT( test_capture_partial_aquire_cancel_ops(), 3, 3 );
        VALIDATE_RESULT( test_capture_partial_aquire_return_ops(), 3, 4 );
        }
    } else if ( t_scen==0 && t_case==0 ) {
        mLog = false;
        //
        help("configuration test", true);
        VALIDATE_RESULT( test_basic_configure(), 1, 1 );
        VALIDATE_RESULT( test_advanced_configure(1), 1, 2 );
        VALIDATE_RESULT( test_advanced_configure(2), 1, 3 );
        //
        help("\nstreaming enque/deque test", true);
        VALIDATE_RESULT( test_image_enque_deque_ops(), 2, 1 );
        VALIDATE_RESULT( test_meta_enque_deque_ops(), 2, 2 );
        VALIDATE_RESULT( test_streaming_enque_deque_ops(0), 2, 3 );
        VALIDATE_RESULT( test_streaming_enque_deque_ops(1), 2, 4 );
        //
        help("\ncapture test", true);
        VALIDATE_RESULT( test_capture_aquire_cancel_ops(), 3, 1 );
        VALIDATE_RESULT( test_capture_aquire_return_ops(), 3, 2 );
        VALIDATE_RESULT( test_capture_partial_aquire_cancel_ops(), 3, 3 );
        VALIDATE_RESULT( test_capture_partial_aquire_return_ops(), 3, 4 );
        //
        mLog = true;
    } else {
        help("not defined test scenario/case!");
    }
    help("end of test");
#undef VALIDATE_RESULT

    return 0;
}

