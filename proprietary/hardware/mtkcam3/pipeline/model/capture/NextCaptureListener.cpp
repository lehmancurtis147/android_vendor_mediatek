/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-NextCaptureListener"
//
#include "NextCaptureListener.h"
//
#include "MyUtils.h"
//
#include <impl/ResultUpdateHelper.h>
//
#include <mtkcam3/pipeline/utils/streaminfo/MetaStreamInfo.h>
#include <mtkcam3/pipeline/utils/streambuf/StreamBuffers.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v3::Utils;
typedef NSCam::v3::Utils::HalMetaStreamBuffer::Allocator  HalMetaStreamBufferAllocatorT;
/******************************************************************************
 *
 ******************************************************************************/
auto
INextCaptureListener::
createInstance(
    int32_t openId,
    std::string const& name,
    CtorParams  const& rCtorParams
) -> android::sp<INextCaptureListener>
{
    sp<NextCaptureListener> pInstance = new NextCaptureListener(openId, name, rCtorParams);
    if  ( CC_UNLIKELY(pInstance==nullptr) ) {
        MY_LOGE("create instance fail");
        return nullptr;
    }

    return pInstance;
}


/******************************************************************************
 *
 ******************************************************************************/
NextCaptureListener::
NextCaptureListener(int32_t openId, std::string const& name, INextCaptureListener::CtorParams const& rCtorParams)
    : mOpenId(openId)
    , mUserName(name)
    , mLogLevel(0)
    , mMaxJpegNum(rCtorParams.maxJpegNum)
    , mInFlightJpeg(0)
    , mpPipelineModelCallback(rCtorParams.pCallback)
{

      mpStreamInfo = new MetaStreamInfo(
                            "Meta:App:Callback",
                            eSTREAMID_META_APP_DYNAMIC_CALLBACK,
                            eSTREAMTYPE_META_OUT,
                            0
                        );
//    mLogLevel = ::property_get_int32("vendor.debug.camera.log", 0);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
NextCaptureListener::
onCaptureInFlightUpdated(
    CaptureInFlightUpdated const& params
) -> void
{
    Mutex::Autolock _l(mMutex);
    mInFlightJpeg = params.inFlightJpegCount;
    if (mlRequestNo.size() > 0)
    {
        size_t nextCaptureCnt = mMaxJpegNum - mInFlightJpeg;
        if (nextCaptureCnt > mlRequestNo.size()) nextCaptureCnt = mlRequestNo.size();

        List<uint32_t>::iterator item = mlRequestNo.begin();
        uint32_t requestNo;
        while ( item != mlRequestNo.end() && nextCaptureCnt > 0) {
            requestNo = *item;
            onNextCaptureUpdated(requestNo);
            nextCaptureCnt--;
            item = mlRequestNo.erase(item);
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
NextCaptureListener::
onNextCaptureCallBack(
    MUINT32   requestNo
)
{
    Mutex::Autolock _l(mMutex);
    if (mlRequestNo.size() > 0 || mInFlightJpeg >= mMaxJpegNum)
    {
        MY_LOGD("(in-flight, maxJpegNum) = (%d,%d), pending requestNo: %d", mInFlightJpeg, mMaxJpegNum, requestNo);
        mlRequestNo.push_back(requestNo);
    }
    else
    {
        onNextCaptureUpdated(requestNo);
    }
}

auto
NextCaptureListener::
onNextCaptureUpdated(uint32_t requestNo)
-> void
{
    CAM_TRACE_NAME(__FUNCTION__);
    MY_LOGD("NextCapture requestNo %d", requestNo);
    sp<IPipelineModelCallback> pCallback;
    pCallback = mpPipelineModelCallback.promote();
    if ( CC_UNLIKELY(! pCallback.get()) ) {
        MY_LOGE("can not promote pCallback for NextCapture");
        return;
    }

    // generate sp<IMetaStreamBuffer> with only MTK_CONTROL_CAPTURE_NEXT_READY
    sp<HalMetaStreamBuffer> pNextCaptureMetaBuffer =
        HalMetaStreamBufferAllocatorT(mpStreamInfo.get())();

    IMetadata* meta = pNextCaptureMetaBuffer->tryWriteLock(LOG_TAG);
    IMetadata::setEntry<MINT32>( meta, MTK_CONTROL_CAPTURE_NEXT_READY, 1);
    pNextCaptureMetaBuffer->unlock(LOG_TAG,meta);
    pNextCaptureMetaBuffer->finishUserSetup();
    ResultUpdateHelper(mpPipelineModelCallback, requestNo, pNextCaptureMetaBuffer, false);
}
