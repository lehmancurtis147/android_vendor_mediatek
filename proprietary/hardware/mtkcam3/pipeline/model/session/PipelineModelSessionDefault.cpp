/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PipelineModelSessionDefault"
//
#include "PipelineModelSessionDefault.h"
//
#include <impl/ControlMetaBufferGenerator.h>
#include <impl/PipelineContextBuilder.h>
#include <impl/PipelineFrameBuilder.h>
//
#include <mtkcam3/pipeline/hwnode/NodeId.h>
#include <mtkcam3/pipeline/utils/streaminfo/IStreamInfoSetControl.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam3/pipeline/prerelease/IPreReleaseRequest.h>
//
#include "MyUtils.h"

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::pipeline::prerelease;

#define ThisNamespace   PipelineModelSessionDefault


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
makeInstance(
    std::string const& name,
    CtorParams const& rCtorParams __unused
) -> android::sp<IPipelineModelSession>
{
    android::sp<ThisNamespace> pSession = new ThisNamespace(name, rCtorParams);
    if  ( CC_UNLIKELY(pSession==nullptr) ) {
        CAM_LOGE("[%s] Bad pSession", __FUNCTION__);
        return nullptr;
    }

    int const err = pSession->configure();
    if  ( CC_UNLIKELY(err != 0) ) {
        CAM_LOGE("[%s] err:%d(%s) - Fail on configure()", __FUNCTION__, err, ::strerror(-err));
        return nullptr;
    }

    return pSession;
}


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
ThisNamespace(
    std::string const& name,
    CtorParams const& rCtorParams)
    : PipelineModelSessionBasic(name, rCtorParams)
    , mpZslProcessor(nullptr)
{
}


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
~ThisNamespace()
{
    // uninit P1Node for BGService's requirement
    IPreReleaseRequestMgr::getInstance()->uninit();
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
getCurrentPipelineContext() const -> android::sp<PipelineContext>
{
    android::RWLock::AutoRLock _l(mRWLock_PipelineContext);
    return mCurrentPipelineContext;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
getCaptureInFlightRequest() -> android::sp<ICaptureInFlightRequest>
{
    return mpCaptureInFlightRequest;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
getZslProcessor() -> android::sp<IZslProcessor>
{
    return mpZslProcessor;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configure() -> int
{
    return PipelineModelSessionBasic::configure();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
onConfig_Capture() -> int
{
    // config ZSL
    std::shared_ptr<OutputZslConfigParams> pZslConfigOutput;
    MY_LOGI("ZSL mode enable = %d", mConfigInfo2->mIsZSLMode);
    if (mConfigInfo2->mIsZSLMode)
    {
        configZSL(pZslConfigOutput);
    }

    // create capture related instances, MUST be after FeatureSettingPolicy
    configureCaptureInFlight(mConfigInfo2->mCaptureFeatureSetting.maxAppJpegStreamNum);

    //bgservice
    configBGService();

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
onConfig_BeforeBuildingPipelineContext() -> int
{
    // some feature needs some information which get from config policy update.
    // And, it has to do related before build pipeline context.
    // This interface will help to do this.
    RETURN_ERROR_IF_NOT_OK(updateBeforeBuildPipelineContext(), "updateBeforeBuildPipelineContext fail");

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateBeforeBuildPipelineContext() -> int
{
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
onRequest_Reconfiguration(
    std::shared_ptr<ConfigInfo2>& pConfigInfo2 __unused,
    policy::pipelinesetting::RequestOutputParams const& reqOutput __unused,
    std::shared_ptr<ParsedAppRequest>const& pRequest __unused
) -> int
{
    RETURN_ERROR_IF_NOT_OK( processReconfiguration(reqOutput, pConfigInfo2, pRequest->requestNo),
            "[requestNo:%u] processReconfiguration", pRequest->requestNo);

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
processReconfiguration(
    pipelinesetting::RequestOutputParams const& rcfOutputParam __unused,
    std::shared_ptr<ConfigInfo2>& pConfigInfo2 __unused,
    MUINT32 requestNo __unused
) -> int
{
    if(!rcfOutputParam.needReconfiguration)
    {
        return OK;
    }
    MY_LOGW("[TODO] needReconfiguration - Not Implement");
    return BAD_VALUE;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
processOneEvaluatedFrame(
    uint32_t& lastFrameNo,
    uint32_t frameType,
    policy::pipelinesetting::RequestResultParams const& reqResult,
    policy::pipelinesetting::RequestOutputParams const& reqOutput,
    std::shared_ptr<IMetadata> pAppMetaControl,
    std::shared_ptr<ParsedAppRequest> request,
    std::shared_ptr<ConfigInfo2> pConfigInfo2,
    android::sp<PipelineContext> pPipelineContext
) -> int
{
    int res = OK;

    // App Meta stream buffers
    std::vector<android::sp<IMetaStreamBuffer>> vAppMeta;
    res = generateControlAppMetaBuffer(
            &vAppMeta,
            (frameType == eFRAMETYPE_MAIN) ? request->pAppMetaControlStreamBuffer : nullptr,
            pAppMetaControl.get(), reqResult.additionalApp.get(),
            pConfigInfo2->mParsedStreamInfo_NonP1.pAppMeta_Control.get());
    RETURN_ERROR_IF_NOT_OK( res, "[requestNo:%u] generateControlAppMetaBuffer", request->requestNo );

    // Hal Meta stream buffers
    // handle multicam case, add more then one hal metadata.
    std::vector<android::sp<HalMetaStreamBuffer>> vHalMeta;
    for (size_t i = 0;
        (i < reqResult.additionalHal.size() && i < pConfigInfo2->mvParsedStreamInfo_P1.size());
        i++)
    {
        res = generateControlHalMetaBuffer(
                &vHalMeta,
                reqResult.additionalHal[i].get(),
                pConfigInfo2->mvParsedStreamInfo_P1[i].pHalMeta_Control.get());
        RETURN_ERROR_IF_NOT_OK( res, "[requestNo:%u] generateControlHalMetaBuffer, i:%zu", request->requestNo, i );
    }


    BuildPipelineFrameInputParams const params = {
        .requestNo = request->requestNo,
        .pAppImageStreamBuffers = (frameType == eFRAMETYPE_MAIN ? request->pParsedAppImageStreamBuffers.get() : nullptr),
        .pAppMetaStreamBuffers  = (vAppMeta.empty() ? nullptr : &vAppMeta),
        .pHalImageStreamBuffers = nullptr,
        .pHalMetaStreamBuffers  = (vHalMeta.empty() ? nullptr : &vHalMeta),
        .pvUpdatedImageStreamInfo = &(reqResult.vUpdatedImageStreamInfo),
        .pnodeSet = &reqResult.nodeSet,
        .pnodeIOMapImage = &(reqResult.nodeIOMapImage),
        .pnodeIOMapMeta = &(reqResult.nodeIOMapMeta),
        .pRootNodes = &(reqResult.roots),
        .pEdges = &(reqResult.edges),
        .pCallback = (frameType == eFRAMETYPE_MAIN ? this : nullptr),
        .pPipelineContext = pPipelineContext
    };


    auto pZslProcessor = getZslProcessor();
    // non-zsl flow
    if ( ! pZslProcessor.get() )
    {
        android::sp<IPipelineFrame> pPipelineFrame;
        RETURN_ERROR_IF_NOT_OK( buildPipelineFrame(pPipelineFrame, params),
                "[requestNo:%u] buildPipelineFrame", request->requestNo );
        lastFrameNo = pPipelineFrame->getFrameNo();
        RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(pPipelineFrame),
                "[requestNo:%u] PipelineContext::queue", request->requestNo );
    }
    // zsl flow (set first zsl request)
    else if (reqOutput.needZslFlow)
    {
        enqueZslBuildFrameParam(params, frameType);
        if (mZSLBuildFrameParam.size() == (reqOutput.subFrames.size()+reqOutput.preDummyFrames.size()+reqOutput.postDummyFrames.size()+1))
        {
            MY_LOGD("[requestNo:%u] submitZslReq", request->requestNo);
            RETURN_ERROR_IF_NOT_OK( submitZslReq(reqOutput, pPipelineContext, lastFrameNo),
                    "[requestNo:%u] submitZslReq", request->requestNo);
        }
    }
    // normal streaming or normal capture with zsl enable
    else
    {
        // check pending request
        if ((frameType == eFRAMETYPE_MAIN)&&(pZslProcessor->hasPendingZslRequest()))
        {
            MY_LOGD("[requestNo:%u] process zsl pending request", request->requestNo);
            enqueZslBuildFrameParam(params, frameType);
            RETURN_ERROR_IF_NOT_OK( submitZslReq(reqOutput, pPipelineContext, lastFrameNo),
                    "[requestNo:%u] submitZslReq", request->requestNo);
        }
        android::sp<IPipelineFrame> pPipelineFrame;
        RETURN_ERROR_IF_NOT_OK( buildPipelineFrame(pPipelineFrame, params),
                "[requestNo:%u] buildPipelineFrame", request->requestNo );
        if ((frameType == eFRAMETYPE_PREDUMMY) || (frameType == eFRAMETYPE_POSTDUMMY) || (frameType == eFRAMETYPE_SUB))
            pZslProcessor->setBufferEnqueCnt(pPipelineFrame->getFrameNo(), mZSLConfigStreamCnt, 0);
        lastFrameNo = pPipelineFrame->getFrameNo();
        RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(pPipelineFrame),
                "[requestNo:%u] PipelineContext::queue", request->requestNo );
    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
beginFlush() -> int
{
    auto pPipelineContext = getCurrentPipelineContext();
    RETURN_ERROR_IF_NULLPTR( pPipelineContext, OK, "No current pipeline context" );
    // create a thread to handle pre-release request
    auto pPreRelease = IPreReleaseRequestMgr::getInstance()->createPreRelease(pPipelineContext);
    flushZslPendingReq(pPipelineContext);
    RETURN_ERROR_IF_NOT_OK( pPipelineContext->flush(), "PipelineContext::flush()" );
    // PS: Need to be called in endFlush()
    pPreRelease->start();
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
ThisNamespace::
updateFrame(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result
)
{
    if(mpZslProcessor.get())
        mpZslProcessor->onFrameUpdated(requestNo,userId,result);

    if (result.bFrameEnd) {
        mpCaptureInFlightRequest->removeRequest(requestNo);
        mpScenarioCtrl->checkIfNeedExitBoost(result.frameNo, false);
        return;
    }

    StreamId_T streamId = -1L;
    {
        android::RWLock::AutoRLock _l(mRWLock_ConfigInfo2);
        streamId = mConfigInfo2->mvParsedStreamInfo_P1[0].pHalMeta_DynamicP1->getStreamId();
    }
    auto timestampStartOfFrame = determineTimestampSOF(streamId, result.vHalOutMeta);
    updateFrameTimestamp(requestNo, userId, result, timestampStartOfFrame);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateFrameTimestamp(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result,
    int64_t timestampStartOfFrame
) -> void
{
    //ZSL flow


    PipelineModelSessionBase::updateFrameTimestamp(requestNo, userId, result, timestampStartOfFrame);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configureCaptureInFlight(const int maxJpegNum) -> int
{
    mpCaptureInFlightRequest = ICaptureInFlightRequest::createInstance(
                            mStaticInfo.pPipelineStaticInfo->openId,
                            mSessionName);
    if(!mpCaptureInFlightRequest.get())
    {
        MY_LOGE("fail to create CaptureInFlighRequest");
        return ~OK;
    }
    INextCaptureListener::CtorParams ctorParams;
    ctorParams.maxJpegNum  = maxJpegNum;
    ctorParams.pCallback   = mPipelineModelCallback;

    mpNextCaptureListener = INextCaptureListener::createInstance(
                            mStaticInfo.pPipelineStaticInfo->openId,
                            mSessionName, ctorParams);
    if (mpNextCaptureListener.get())
    {
        mpCaptureInFlightRequest->registerListener(mpNextCaptureListener);
    }
    else
    {
        MY_LOGE("fail to create NextCaptureListener");
        return ~OK;
    }

    return OK;
}

void
ThisNamespace::
configBGService()
{
    auto pPreReleaseRequestMgr = IPreReleaseRequestMgr::getInstance();
    if (!pPreReleaseRequestMgr){
        return;
    }

    //TODO: when IT done, change default value to 0
    MINT32 bgserviceMode = property_get_int32("vendor.debug.camera.bgservice.mode", 0);
    //force off bgservice
    if (bgserviceMode == 2){
        IPreReleaseRequestMgr::getInstance()->setImageReaderID(-1);
        return;
    }
    //force enable bgservice
    else if(bgserviceMode == 1){
        IPreReleaseRequestMgr::getInstance()->setImageReaderID(0);
        return;
    }

    auto const& pParsedAppConfiguration = mStaticInfo.pUserConfiguration->pParsedAppConfiguration;
    MINT32 prerelease = -1;
    MINT32 imageReaderID = -1;
    if (IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_BGSERVICE_FEATURE_PRERELEASE, prerelease)){
        if (IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_BGSERVICE_FEATURE_IMAGEREADERID, imageReaderID)){
            MY_LOGD("isSupportedBGPrerelease=%d, imageReaderID=%d", prerelease==MTK_BGSERVICE_FEATURE_PRERELEASE_MODE_ON, imageReaderID);
            pPreReleaseRequestMgr->setImageReaderID(imageReaderID);
        }
    }
    else{
        MY_LOGD("can not get prerelease mode from sessionParms, bgserviceMode=%d", bgserviceMode);
        //reset the ImageReaderID
        IPreReleaseRequestMgr::getInstance()->setImageReaderID(imageReaderID);
    }
}
/******************************************************************************
 *  Internal Operations for ZSL.
 ******************************************************************************/
void
ThisNamespace::
configZSL(
    std::shared_ptr<OutputZslConfigParams>& pZslConfigOutput
)
{
    // check enable zsl or not (0:off; 1:0n; 2:desided by app<TO-DO>)
    int32_t ZSLOpen = ::property_get_int32("vendor.debug.camera.zsl.enable",1);
    if(ZSLOpen == 1)
    {
        mpZslProcessor = IZslProcessor::createInstance(
                            mStaticInfo.pPipelineStaticInfo->openId,
                            mSessionName);
        if(!mpZslProcessor.get())
        {
            MY_LOGE("fail to create ZSLProcessor (ZSLOpen = %d)",ZSLOpen);
            return;
        }
    }
    else if(ZSLOpen == 0)
        mpZslProcessor = nullptr;

    if(mpZslProcessor.get())
    {
        std::shared_ptr<InputZslConfigParams> pZslConfigInput(new InputZslConfigParams());

        // create stream info set with all p1 output streams
        pZslConfigInput->pInfoSet = createZslCfgStreamInfo();

        //  save p1 (main1/main2) node id to zsl
        pZslConfigInput->vUserId.push_back(eNODEID_P1Node);
        if(mStaticInfo.pPipelineStaticInfo->isDualDevice)
            pZslConfigInput->vUserId.push_back(eNODEID_P1Node_main2);

        // save P1 node output stream info
        pZslConfigInput->pParsedStreamInfo_P1 = &mConfigInfo2->mvParsedStreamInfo_P1;
        pZslConfigInput->pPipelineModelCallback = mPipelineModelCallback;

        android::status_t ret = mpZslProcessor->configure(pZslConfigInput,pZslConfigOutput);
        if( ret != android::OK)
        {
            MY_LOGE("fail to config ZSLProcessor (ret = %d), release ZslProcessor",ret);
            mpZslProcessor = nullptr;
        }
    }
}

android::sp<NSCam::v3::IStreamInfoSet>
ThisNamespace::
createZslCfgStreamInfo()
{
    // creat IStreamInfoSet for ZslProcessor & HistoryBufferContainer
    android::sp<SimpleStreamInfoSetControl> pStreamInfoSet = new SimpleStreamInfoSetControl();
    SimpleStreamInfoSetControl::Map<IMetaStreamInfo>&  MetaMap = pStreamInfoSet->editMeta();
    SimpleStreamInfoSetControl::Map<IImageStreamInfo>&  ImgMap = pStreamInfoSet->editImage();

    // Zsl will save followed stream into buffer pool
    for(size_t i=0; i<mConfigInfo2->mvParsedStreamInfo_P1.size();i++)
    {
        // add p1 out meta
        if(mConfigInfo2->mvParsedStreamInfo_P1[i].pAppMeta_DynamicP1.get())
            MetaMap.addStream(mConfigInfo2->mvParsedStreamInfo_P1[i].pAppMeta_DynamicP1);
        if(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalMeta_DynamicP1.get())
            MetaMap.addStream(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalMeta_DynamicP1);
        // add p1 out image
        if(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Imgo.get())
            ImgMap.addStream(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Imgo);
        if(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Rrzo.get())
            ImgMap.addStream(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Rrzo);
        if(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Lcso.get())
            ImgMap.addStream(mConfigInfo2->mvParsedStreamInfo_P1[i].pHalImage_P1_Lcso);
    }
    mZSLConfigStreamCnt = ImgMap.size();
    mZSLConfigMetaCnt = MetaMap.size();

    if(MetaMap.isEmpty() || ImgMap.isEmpty())
    {
        MY_LOGW("ZSL collect stream info too few (meta:%zu , image:%zu)",MetaMap.size(),ImgMap.size());
    }
    for( size_t i=0; i<pStreamInfoSet->getImageInfoNum(); i++ )
    {
        auto pInfo = pStreamInfoSet->getImageInfoAt(i);
        MY_LOGD( "config ZSL image of stream(%#" PRIx64 ":%s:%p:%d:%zu)",
                 pInfo->getStreamId(), pInfo->getStreamName(), pInfo.get(),pInfo->getStreamType(), pInfo->getMaxBufNum());
    }
    for( size_t i=0; i<pStreamInfoSet->getMetaInfoNum(); i++ )
    {
        auto pInfo = pStreamInfoSet->getMetaInfoAt(i);
        MY_LOGD( "config ZSL meta of stream(%#" PRIx64 ":%s:%p:%d:%zu)",
                 pInfo->getStreamId(), pInfo->getStreamName(), pInfo.get(),pInfo->getStreamType(), pInfo->getMaxBufNum());
    }

    return pStreamInfoSet;
}

int
ThisNamespace::
submitZslReq(
    pipelinesetting::RequestOutputParams const& ReqOutParams,
    const android::sp<PipelineContext>&  pPipelineContext,
    MUINT32& lastFrameNo)
{
    std::shared_ptr<OutputZslRequestParams> pZslReqOutput = nullptr;
    android::status_t   zsl_submit_st = -1;

    // submit Zsl request
    std::shared_ptr<InputZslRequestParams> pZslReqInput(new InputZslRequestParams());
    pZslReqInput->pCapParams = std::make_shared<policy::pipelinesetting::RequestOutputParams>(ReqOutParams);
    pZslReqInput->pFrameParams = mZSLBuildFrameParam;
    zsl_submit_st = mpZslProcessor->submitZslRequest(pZslReqInput,pZslReqOutput);
    mZSLBuildFrameParam.clear();

    // if there is IPipelineFrame output, enque into pipelineContext
    // if not, there will be a pending request in ZslProcessor
    if((zsl_submit_st == android::OK) &&
        (pZslReqOutput != nullptr))
    {
        if(pZslReqOutput->bZslFlow)
        {
            std::map< uint32_t, std::shared_ptr<OutputZslFlowRequest> >::iterator it;
            for(it = pZslReqOutput->vZslFlowRequest.begin(); it != pZslReqOutput->vZslFlowRequest.end(); it++)
            {
                MY_LOGD("submit ZSL request- requestNo:%u",it->first );
                for(size_t i=0;i<it->second->vFrame.size();i++)
                {
                    lastFrameNo = it->second->vFrame.at(i)->getFrameNo();
                    RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(it->second->vFrame.at(i)),
                                            "PipelineContext::queue fail - requestNo:%u (%zu/%zu)",
                                            it->first, i, it->second->vFrame.size());
                }
            }
        }
        if(pZslReqOutput->bNonZslFlow)
        {
            if(ReqOutParams.needZslFlow)
            {
                MY_LOGD("add ZSL pending request- requestNo:%u",pZslReqInput->pFrameParams[0]->requestNo);
            }
            else
            {
                MY_LOGD("check ZSL pending request by requestNo:%u",pZslReqInput->pFrameParams[0]->requestNo );
            }
        }
    }
    else if((zsl_submit_st == android::NO_MEMORY)&&(!mpZslProcessor->hasPendingZslRequest()))
    {
        MY_LOGW("There is no HBC buffer available for requestNo:%u, change to normal capture",
                pZslReqInput->pFrameParams[0]->requestNo );
        for(size_t i=0;i<pZslReqInput->pFrameParams.size();i++)
        {
            std::shared_ptr<InputZslBuildPipelineFrameParams>& pFrameParams =
                                pZslReqInput->pFrameParams[i];
            android::sp<IPipelineFrame> pPipelineFrame;
            BuildPipelineFrameInputParams const params = {
                .requestNo = pFrameParams->requestNo,
                .bReprocessFrame = MFALSE,
                .pAppImageStreamBuffers = pFrameParams->spAppImageStreamBuffers.get(),
                .pAppMetaStreamBuffers  = &pFrameParams->AppMetaStreamBuffers,
                .pHalImageStreamBuffers = nullptr,
                .pHalMetaStreamBuffers  = &pFrameParams->HalMetaStreamBuffers,
                .pvUpdatedImageStreamInfo = &pFrameParams->vUpdatedImageStreamInfo,
                .pnodeSet = &pFrameParams->nodeSet,
                .pnodeIOMapImage = &pFrameParams->nodeIOMapImage,
                .pnodeIOMapMeta = &pFrameParams->nodeIOMapMeta,
                .pRootNodes = &pFrameParams->RootNodes,
                .pEdges = &pFrameParams->Edges,
                .pCallback = pFrameParams->pCallback,
                .pPipelineContext = pFrameParams->pPipelineContext
            };
            RETURN_ERROR_IF_NOT_OK( buildPipelineFrame(pPipelineFrame, params),
                                    "buildPipelineFrame fail - requestNo:%u : [%zu]",
                                    pFrameParams->requestNo,i);
            RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(pPipelineFrame),
                                    "PipelineContext::queue fail - requestNo:%u : [%zu]",
                                    pFrameParams->requestNo,i);
        }
    }

    return 0;
}

int
ThisNamespace::
flushZslPendingReq(
    const  android::sp<PipelineContext>&  pPipelineContext
)
{
    if((mpZslProcessor.get()) && (mpZslProcessor->hasPendingZslRequest()))
    {
        std::shared_ptr<OutputZslRequestParams> pZslReqOutput = nullptr;
        mpZslProcessor->flush(pZslReqOutput);
        if( (pZslReqOutput != nullptr) &&
            (pZslReqOutput->bZslFlow))
        {
            std::map< uint32_t, std::shared_ptr<OutputZslFlowRequest> >::iterator it;
            for(it = pZslReqOutput->vZslFlowRequest.begin(); it != pZslReqOutput->vZslFlowRequest.end(); it++)
            {
                MY_LOGD("submit ZSL pending request when flush- requestNo:%u",it->first );
                for(size_t i=0;i<it->second->vFrame.size();i++)
                {
                    RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(it->second->vFrame.at(i)),
                                            "PipelineContext::queue pending zsl req failed when flush - requestNo:%u (%zu/%zu)",
                                            it->first, i, it->second->vFrame.size());
                }
            }
        }
    }
    return 0;
}

void
ThisNamespace::
enqueZslBuildFrameParam(
    const BuildPipelineFrameInputParams&  params,
    int frameType
)
{
    std::shared_ptr<InputZslBuildPipelineFrameParams> pZslBuildFrameInput(new InputZslBuildPipelineFrameParams());

    pZslBuildFrameInput->bDummyFrame = ((frameType == eFRAMETYPE_MAIN)||(frameType == eFRAMETYPE_SUB))?
                                        MFALSE : MTRUE;
    pZslBuildFrameInput->requestNo = params.requestNo;

    if(params.pAppImageStreamBuffers!=nullptr)
    {
        pZslBuildFrameInput->spAppImageStreamBuffers =
                std::make_shared<ParsedAppImageStreamBuffers>(*params.pAppImageStreamBuffers);
    }
    if(params.pAppMetaStreamBuffers!=nullptr)
        pZslBuildFrameInput->AppMetaStreamBuffers = *params.pAppMetaStreamBuffers;
    if(params.pHalMetaStreamBuffers!=nullptr)
        pZslBuildFrameInput->HalMetaStreamBuffers = *params.pHalMetaStreamBuffers;

    pZslBuildFrameInput->vUpdatedImageStreamInfo = *params.pvUpdatedImageStreamInfo;
    pZslBuildFrameInput->nodeSet = *params.pnodeSet;

    pZslBuildFrameInput->nodeIOMapImage = *params.pnodeIOMapImage;
    pZslBuildFrameInput->nodeIOMapMeta = *params.pnodeIOMapMeta;

    pZslBuildFrameInput->RootNodes = *params.pRootNodes;
    pZslBuildFrameInput->Edges = *params.pEdges;

    pZslBuildFrameInput->pCallback = params.pCallback;
    pZslBuildFrameInput->pPipelineContext = params.pPipelineContext;

    mZSLBuildFrameParam.push_back(pZslBuildFrameInput);
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
ThisNamespace::
onNextCaptureCallBack(
    MUINT32   requestNo,
    MINTPTR   nodeId __unused
)
{
    mpNextCaptureListener->onNextCaptureCallBack(requestNo);
}

