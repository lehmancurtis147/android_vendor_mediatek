/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONDEFAULT_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONDEFAULT_H_
//
#include "PipelineModelSessionBasic.h"
//
#include <utils/RWLock.h>
//
#include <impl/ICaptureInFlightRequest.h>
#include <impl/INextCaptureListener.h>
//
#include <impl/IZslProcessor.h>
#include <mtkcam3/pipeline/stream/IStreamInfo.h>


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/******************************************************************************
 *
 ******************************************************************************/
class PipelineModelSessionDefault
    : public PipelineModelSessionBasic
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    private data members.
    android::sp<IZslProcessor>      mpZslProcessor;     // to control ZSL flow
    size_t                          mZSLConfigStreamCnt;
    size_t                          mZSLConfigMetaCnt;
    std::vector<std::shared_ptr<InputZslBuildPipelineFrameParams>> mZSLBuildFrameParam;

    // Capture In Flight Request
    android::sp<ICaptureInFlightRequest> mpCaptureInFlightRequest;
    android::sp<INextCaptureListener>    mpNextCaptureListener;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Internal Operations for ZSL.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:
    void configZSL(
            std::shared_ptr<OutputZslConfigParams>& pZslConfigOutput
            );
    android::sp<IStreamInfoSet>   createZslCfgStreamInfo();
    int submitZslReq(
            policy::pipelinesetting::RequestOutputParams const& ReqOutParams,
            const android::sp<PipelineContext>&  pPipelineContext,
            MUINT32& lastFrameNo
            );
    int flushZslPendingReq(
            const android::sp<PipelineContext>&  pPipelineContext
            );
    void enqueZslBuildFrameParam(
        const BuildPipelineFrameInputParams&  params,
        int frameType
        );
    void configBGService();

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Internal Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    Template Methods: member access

    virtual auto    getCurrentPipelineContext() const -> android::sp<PipelineContext> override;
    virtual auto    getCaptureInFlightRequest() -> android::sp<ICaptureInFlightRequest> override;
    virtual auto    getZslProcessor() -> android::sp<IZslProcessor> override;

protected:  ////    Template Methods: called by configure()
    virtual auto    onConfig_Capture() -> int;
    virtual auto    onConfig_BeforeBuildingPipelineContext() -> int;

protected:  ////    Template Methods: called by submitOneRequest()
    virtual auto    onRequest_Reconfiguration(
                        std::shared_ptr<ConfigInfo2>& pConfigInfo2,
                        policy::pipelinesetting::RequestOutputParams const& reqOutput,
                        std::shared_ptr<ParsedAppRequest>const& pRequest
                    ) -> int override;

    virtual auto    processReconfiguration(
                        policy::pipelinesetting::RequestOutputParams const& rcfOutputParam,
                        std::shared_ptr<ConfigInfo2>& pConfigInfo2,
                        MUINT32 requestNo
                    ) -> int;

protected:  ////    Operations (Configuration Stage).

    // create capture related instance
    virtual auto    configureCaptureInFlight(const int maxJpegNum) -> int;

protected:  ////    Operations (Request Stage).

    virtual auto    processOneEvaluatedFrame(
                        uint32_t& lastFrameNo,
                        uint32_t frameType /*eFRAMETYPE_xxx*/,
                        policy::pipelinesetting::RequestResultParams const& reqResult,
                        policy::pipelinesetting::RequestOutputParams const& reqOutput,
                        std::shared_ptr<IMetadata> pAppMetaControl,
                        std::shared_ptr<ParsedAppRequest> request,
                        std::shared_ptr<ConfigInfo2> pConfigInfo2,
                        android::sp<PipelineContext> pPipelineContext
                    ) -> int override;

protected:  ////    Operations.

    virtual auto    updateFrameTimestamp(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result,
                        int64_t timestampStartOfFrame
                    ) -> void;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces (called by Session Factory).
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Instantiation.
    static  auto    makeInstance(
                        std::string const& name,
                        CtorParams const& rCtorParams
                        ) -> android::sp<IPipelineModelSession>;

                    PipelineModelSessionDefault(
                        std::string const& name,
                        CtorParams const& rCtorParams);
    virtual         ~PipelineModelSessionDefault();

public:     ////    Configuration.
    virtual auto    configure() -> int override;
    virtual auto    updateBeforeBuildPipelineContext() -> int;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineBufferSetFrameControl::IAppCallback Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual MVOID   updateFrame(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result
                    ) override;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineModelSession Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual auto    beginFlush() -> int override;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  NSPipelineContext::DataCallbackBase Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual MVOID   onNextCaptureCallBack(
                        MUINT32   requestNo,
                        MINTPTR   nodeId
                    ) override;
};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONDEFAULT_H_

