/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONAPPRAW16REPROCESS_H_
#define _MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONAPPRAW16REPROCESS_H_
//
#include "PipelineModelSessionBasic.h"
//
#include <list>
#include <mutex>
#include <set>
#include <type_traits>
//
#include <utils/RWLock.h>
//
#include <mtkcam3/pipeline/pipeline/IPipelineNode.h>
#include <mtkcam3/pipeline/pipeline/IPipelineBufferSetFrameControl.h>


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/******************************************************************************
 *
 ******************************************************************************/
class PipelineModelSessionAppRaw16Reprocess
    : public PipelineModelSessionBasic
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Definitions.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    using HalImageStreamBuffer = NSCam::v3::Utils::HalImageStreamBuffer;
    using HalMetaStreamBuffer = NSCam::v3::Utils::HalMetaStreamBuffer;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Data Members.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    private request info (changable)

    static constexpr uint32_t BAD_REQUEST_NO = (uint32_t)-1;
    uint32_t                mReprocessRequestNo = BAD_REQUEST_NO;
    mutable std::mutex      mReprocessRequestNoLock;

    using HistoryFrameContainerT = std::list<android::sp<IPipelineFrame>>;
    HistoryFrameContainerT  mHistoryFrameContainer;
    std::mutex              mHistoryFrameContainerLock;

    #if 0
    // History JPEG control metadata
    IMetadata::IEntry       mJPEG_GPS_COORDINATES;
    IMetadata::IEntry       mJPEG_GPS_PROCESSING_METHOD;
    IMetadata::IEntry       mJPEG_GPS_TIMESTAMP;
    IMetadata::IEntry       mJPEG_ORIENTATION;
    IMetadata::IEntry       mJPEG_QUALITY;
    IMetadata::IEntry       mJPEG_THUMBNAIL_QUALITY;
    IMetadata::IEntry       mJPEG_THUMBNAIL_SIZE;
    #endif

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Internal Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
protected:  ////    Template Methods.

    virtual auto    getCurrentPipelineContext() const -> android::sp<PipelineContext> override;

protected:  ////    Template Methods: called by submitRequest()
    virtual auto    submitOneRequest(
                        std::shared_ptr<ParsedAppRequest>const& request
                    ) -> int override;

protected:  ////    Operations (Request Stage).

    virtual auto    processOneEvaluatedFrame(
                        uint32_t& lastFrameNo,
                        uint32_t frameType /*eFRAMETYPE_xxx*/,
                        policy::pipelinesetting::RequestResultParams const& reqResult,
                        policy::pipelinesetting::RequestOutputParams const& reqOutput,
                        std::shared_ptr<IMetadata> pAppMetaControl,
                        std::shared_ptr<ParsedAppRequest> request,
                        std::shared_ptr<ConfigInfo2> pConfigInfo2,
                        android::sp<PipelineContext> pPipelineContext
                    ) -> int override;

protected:  ////    Operations
                    struct HandleReprocessRequestParams
                    {
                        std::vector<android::sp<HalMetaStreamBuffer>>*      pvHalMeta = nullptr;
                        std::vector<android::sp<HalImageStreamBuffer>>*     pvHalImage = nullptr;
                        bool                                isReprocessRequest = false;
                        policy::pipelinesetting::RequestResultParams const* pReqResult = nullptr;
                        std::shared_ptr<IMetadata>          pAppMetaControl;
                        std::shared_ptr<ParsedAppRequest>   pRequest;
                        std::shared_ptr<ConfigInfo2>        pConfigInfo2;
                    };
    auto            handleReprocessRequest(HandleReprocessRequestParams const& params) -> void;
    auto            checkJpegControlMetadata(IMetadata* pMetadata) const -> void;
    auto            prepareReprocessHalBuffers(HandleReprocessRequestParams const& params) -> void;
    auto            selectHistoryFrameForReprocess(
                        android::sp<IPipelineBufferSetFrameControl>& pTargetFrameControl,
                        HistoryFrameContainerT& historyFrames,
                        int64_t requestTimestamp,
                        uint32_t requestNo
                    ) -> void;

                    struct TransferBuffersFromFrameParams
                    {
                        std::vector<android::sp<HalMetaStreamBuffer>>*      pvHalMeta = nullptr;
                        std::vector<android::sp<HalImageStreamBuffer>>*     pvHalImage = nullptr;
                        android::sp<IPipelineBufferSetFrameControl>         pHistoryFrameControl = nullptr;
                        policy::pipelinesetting::RequestResultParams const* pReqResult = nullptr;
                        std::shared_ptr<ParsedAppRequest>   pRequest;
                        std::shared_ptr<ConfigInfo2>        pConfigInfo2;
                    };
    auto            transferBuffersFromFrame(TransferBuffersFromFrameParams const& params) -> void;

protected:  ////    Operations
                    struct HandleKeepFrameForReprocess
                    {
                        std::shared_ptr<IMetadata>          pAppMetaControl;
                        android::sp<IPipelineFrame>         pPipelineFrame;
                        bool                                isKeepForReprocess = false;
                        bool                                isReprocessRequest = false;
                    };
    auto            handleKeepFrameForReprocess(HandleKeepFrameForReprocess const& params) -> void;
    virtual auto    keepFrameForReprocess(HandleKeepFrameForReprocess const& params) -> void;

protected:  ////    Operations (History Frame Container).
    virtual auto    acquireAllHistoryFrames() -> HistoryFrameContainerT;

protected:  ////    Operations (Reprocess Request Number).
    auto            setReprocessRequestNo(uint32_t requestNo) -> void;
    auto            resetReprocessRequestNo(uint32_t requestNo) -> void;
    auto            debugReprocessRequestResult(
                        uint32_t requestNo,
                        android::Vector<android::sp<IMetaStreamBuffer>>const& vAppOutMeta
                    ) const -> void;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Interfaces (called by Session Factory).
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////    Instantiation.
    static  auto    makeInstance(
                        std::string const& name,
                        CtorParams const& rCtorParams
                        ) -> android::sp<IPipelineModelSession>;

                    PipelineModelSessionAppRaw16Reprocess(
                        std::string const& name,
                        CtorParams const& rCtorParams);

public:     ////    Configuration.
    virtual auto    configure() -> int;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineModelSession Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual auto    beginFlush() -> int override;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IPipelineBufferSetFrameControl::IAppCallback Interfaces.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:     ////
    virtual MVOID   updateFrame(
                        MUINT32 const requestNo,
                        MINTPTR const userId,
                        Result const& result
                    ) override;

};


/******************************************************************************
 *
 ******************************************************************************/
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_PIPELINE_MODEL_SESSION_PIPELINEMODELSESSIONAPPRAW16REPROCESS_H_

