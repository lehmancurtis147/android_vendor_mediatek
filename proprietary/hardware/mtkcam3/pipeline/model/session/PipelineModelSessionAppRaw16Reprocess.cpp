/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PPLMSession-AppRaw16Reprocess"
//
#include "PipelineModelSessionAppRaw16Reprocess.h"
//
#include <impl/ControlMetaBufferGenerator.h>
#include <impl/PipelineFrameBuilder.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam3/pipeline/hwnode/NodeId.h>
//
#include "MyUtils.h"

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::Utils;

#define ThisNamespace   PipelineModelSessionAppRaw16Reprocess


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
makeInstance(
    std::string const& name,
    CtorParams const& rCtorParams __unused
) -> android::sp<IPipelineModelSession>
{
    android::sp<ThisNamespace> pSession = new ThisNamespace(name, rCtorParams);
    if  ( CC_UNLIKELY(pSession==nullptr) ) {
        CAM_LOGE("[%s] Bad pSession", __FUNCTION__);
        return nullptr;
    }

    int const err = pSession->configure();
    if  ( CC_UNLIKELY(err != 0) ) {
        CAM_LOGE("[%s] err:%d(%s) - Fail on configure()", __FUNCTION__, err, ::strerror(-err));
        return nullptr;
    }

    return pSession;
}


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
ThisNamespace(
    std::string const& name,
    CtorParams const& rCtorParams)
    : PipelineModelSessionBasic(name, rCtorParams)
{
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
getCurrentPipelineContext() const -> android::sp<PipelineContext>
{
    android::RWLock::AutoRLock _l(mRWLock_PipelineContext);
    return mCurrentPipelineContext;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configure() -> int
{
    return PipelineModelSessionBasic::configure();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
submitOneRequest(
    std::shared_ptr<ParsedAppRequest>const& request __unused
) -> int
{
    return PipelineModelSessionBasic::submitOneRequest(request);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
processOneEvaluatedFrame(
    uint32_t& lastFrameNo,
    uint32_t frameType,
    policy::pipelinesetting::RequestResultParams const& reqResult,
    policy::pipelinesetting::RequestOutputParams const& reqOutput __unused,
    std::shared_ptr<IMetadata> pAppMetaControl,
    std::shared_ptr<ParsedAppRequest> request,
    std::shared_ptr<ConfigInfo2> pConfigInfo2,
    android::sp<PipelineContext> pPipelineContext
) -> int
{
    bool isReprocessRequest = (request->pParsedAppImageStreamInfo->pAppImage_Input_RAW16 != nullptr);
    bool isKeepForReprocess = [=]() -> bool {
        // Check Vendor TAG? (from pAppMetaControl)
        uint8_t controlCaptureKeepForReprocess = 0;
        return (request->pParsedAppImageStreamInfo->pAppImage_Output_RAW16 != nullptr)
            && (frameType == eFRAMETYPE_MAIN)
            && (IMetadata::getEntry(pAppMetaControl.get(), MTK_CONTROL_CAPTURE_KEEP_FOR_REPROCESS, controlCaptureKeepForReprocess))
            && (0 != controlCaptureKeepForReprocess)
                ;
    }();
    MY_LOGF_IF((frameType != eFRAMETYPE_MAIN && isReprocessRequest),
                "[requestNo:%u] Reprocess request must be a main frame", request->requestNo);

    int res = OK;

    // App Meta stream buffers
    std::vector<android::sp<IMetaStreamBuffer>> vAppMeta;
    res = generateControlAppMetaBuffer(
            &vAppMeta,
            (frameType == eFRAMETYPE_MAIN) ? request->pAppMetaControlStreamBuffer : nullptr,
            pAppMetaControl.get(), reqResult.additionalApp.get(),
            pConfigInfo2->mParsedStreamInfo_NonP1.pAppMeta_Control.get());
    RETURN_ERROR_IF_NOT_OK( res, "[requestNo:%u] generateControlAppMetaBuffer", request->requestNo );

    // Hal Meta stream buffers
    // handle multicam case, add more then one hal metadata.
    std::vector<android::sp<HalMetaStreamBuffer>> vHalMeta;
    for (size_t i = 0;
        (i < reqResult.additionalHal.size() && i < pConfigInfo2->mvParsedStreamInfo_P1.size());
        i++)
    {
        res = generateControlHalMetaBuffer(
                &vHalMeta,
                reqResult.additionalHal[i].get(),
                pConfigInfo2->mvParsedStreamInfo_P1[i].pHalMeta_Control.get());
        RETURN_ERROR_IF_NOT_OK( res, "[requestNo:%u] generateControlHalMetaBuffer, i:%zu", request->requestNo, i );
    }

    // Hal Image stream buffers
    std::vector<android::sp<HalImageStreamBuffer>> vHalImage;

    // Prepare HAL stream buffers for this Reprocess Request if it is.
    handleReprocessRequest(HandleReprocessRequestParams{
        .pvHalMeta          = &vHalMeta,
        .pvHalImage         = &vHalImage,
        .isReprocessRequest = isReprocessRequest,
        .pReqResult         = &reqResult,
        .pAppMetaControl    = pAppMetaControl,
        .pRequest           = request,
        .pConfigInfo2       = pConfigInfo2,
    });

    BuildPipelineFrameInputParams const params = {
        .requestNo = request->requestNo,
        .bReprocessFrame = isReprocessRequest,
        .pAppImageStreamBuffers = (frameType == eFRAMETYPE_MAIN ? request->pParsedAppImageStreamBuffers.get() : nullptr),
        .pAppMetaStreamBuffers  = (vAppMeta.empty() ? nullptr : &vAppMeta),
        .pHalImageStreamBuffers = (vHalImage.empty() ? nullptr : &vHalImage),
        .pHalMetaStreamBuffers  = (vHalMeta.empty() ? nullptr : &vHalMeta),
        .pvUpdatedImageStreamInfo = &(reqResult.vUpdatedImageStreamInfo),
        .pnodeSet = &reqResult.nodeSet,
        .pnodeIOMapImage = &(reqResult.nodeIOMapImage),
        .pnodeIOMapMeta = &(reqResult.nodeIOMapMeta),
        .pRootNodes = &(reqResult.roots),
        .pEdges = &(reqResult.edges),
        .pCallback = (frameType == eFRAMETYPE_MAIN ? this : nullptr),
        .pPipelineContext = pPipelineContext
    };

    {
        // Build a pipeline frame.
        android::sp<IPipelineFrame> pPipelineFrame;
        RETURN_ERROR_IF_NOT_OK( buildPipelineFrame(pPipelineFrame, params),
                "[requestNo:%u] buildPipelineFrame", request->requestNo );
        lastFrameNo = pPipelineFrame->getFrameNo();

        // Keep this frame before queuing it to pipeline.
        handleKeepFrameForReprocess(HandleKeepFrameForReprocess{
            .pAppMetaControl    = pAppMetaControl,
            .pPipelineFrame     = pPipelineFrame,
            .isKeepForReprocess = isKeepForReprocess,
            .isReprocessRequest = isReprocessRequest,
        });

        MY_LOGI_IF(isReprocessRequest,
                "[requestNo:%u frameNo:%u] reprocess frame: being queued to the pipeline",
                pPipelineFrame->getRequestNo(), pPipelineFrame->getFrameNo());

        // Queue the frame to the pipeline.
        RETURN_ERROR_IF_NOT_OK( pPipelineContext->queue(pPipelineFrame),
                "[requestNo:%u] PipelineContext::queue", request->requestNo );
    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
beginFlush() -> int
{
    // Flush all history frames
    {
        HistoryFrameContainerT history = acquireAllHistoryFrames();
        if ( ! history.empty() ) {
            MY_LOGI("Flush all history frames: #%zu in total", history.size());
            for (auto& frame : history) {
                if (CC_LIKELY( frame != nullptr )) {
                    MY_LOGI("[requestNo:%u frameNo:%u] flush...", frame->getRequestNo(), frame->getFrameNo());
                    frame.clear();
                }
            }
            history.clear();
        }
    }

    return PipelineModelSessionBasic::beginFlush();
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
ThisNamespace::
updateFrame(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result
)
{
    debugReprocessRequestResult(requestNo, result.vAppOutMeta);
    if (result.bFrameEnd) {
        mpScenarioCtrl->checkIfNeedExitBoost(result.frameNo, false);
        resetReprocessRequestNo(requestNo);
        return;
    }

    StreamId_T streamId = -1L;
    {
        android::RWLock::AutoRLock _l(mRWLock_ConfigInfo2);
        streamId = mConfigInfo2->mvParsedStreamInfo_P1[0].pHalMeta_DynamicP1->getStreamId();
    }
    auto timestampStartOfFrame = determineTimestampSOF(streamId, result.vHalOutMeta);
    updateFrameTimestamp(requestNo, userId, result, timestampStartOfFrame);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
handleReprocessRequest(HandleReprocessRequestParams const& params) -> void
{
    if ( params.isReprocessRequest ) {
        setReprocessRequestNo(params.pRequest->requestNo);
        prepareReprocessHalBuffers(params);
        checkJpegControlMetadata(params.pAppMetaControl.get());
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
prepareReprocessHalBuffers(HandleReprocessRequestParams const& params) -> void
{
    auto requestNo = params.pRequest->requestNo;

    // For reprocessing, pick up one history frame which has the same timestamp as the request's.
    //
    // 1) the timestamp from request.
    int64_t requestTimestamp = 0;
    bool ret = IMetadata::getEntry(params.pAppMetaControl.get(), MTK_SENSOR_TIMESTAMP, requestTimestamp);
    MY_LOGF_IF(!ret, "[requestNo:%u] reprocess request has no android.sensor.timestamp", requestNo);
    //MY_LOGI("[requestNo:%u] reprocess request SENSOR_TIMESTAMP:%" PRId64 "", requestNo, requestTimestamp);

    // 2) find the history frame with the same timestamp.
    android::sp<IPipelineBufferSetFrameControl> pTargetHistoryFrameControl = nullptr;
    HistoryFrameContainerT historyFrames;
    selectHistoryFrameForReprocess(pTargetHistoryFrameControl, historyFrames, requestTimestamp, requestNo);


    if (CC_UNLIKELY( pTargetHistoryFrameControl == nullptr )) {
        MY_LOGW("[requestNo:%u SENSOR_TIMESTAMP:%" PRId64 "] "
                "cannot hit any history frame for reprocessing", requestNo, requestTimestamp);
    }
    else {
        MY_LOGI("[requestNo:%u SENSOR_TIMESTAMP:%" PRId64 "] "
                "hit History Frame(requestNo:%u frameNo:%u) for reprocessing",
                requestNo, requestTimestamp,
                pTargetHistoryFrameControl->getRequestNo(),
                pTargetHistoryFrameControl->getFrameNo());

        // [transfer] pHistoryFrameControl -> vHalMeta, vHalImage
        transferBuffersFromFrame(TransferBuffersFromFrameParams{
            .pvHalMeta              = params.pvHalMeta,
            .pvHalImage             = params.pvHalImage,
            .pHistoryFrameControl   = pTargetHistoryFrameControl,
            .pReqResult             = params.pReqResult,
            .pRequest               = params.pRequest,
            .pConfigInfo2           = params.pConfigInfo2,
        });
    }

    // Clean up all history frames.
    historyFrames.clear();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
selectHistoryFrameForReprocess(
    android::sp<IPipelineBufferSetFrameControl>& pTargetFrameControl,
    HistoryFrameContainerT& historyFrames,
    int64_t requestTimestamp,
    uint32_t requestNo
) -> void
{
    // 1) get all history frames.
    historyFrames = acquireAllHistoryFrames();
    MY_LOGI("[requestNo:%u] History frames:#%zu", requestNo, historyFrames.size());
    MY_LOGF_IF(historyFrames.empty(), "[requestNo:%u] empty history container", requestNo);

    // 2) find the history frame with the same timestamp.
    for (auto const& historyFrame : historyFrames)
    {
        MY_LOGF_IF(historyFrame==nullptr,
            "[requestNo:%u] nullptr historyFrame", requestNo);
        auto pFrameControl = IPipelineBufferSetFrameControl::castFrom(historyFrame.get());
        MY_LOGF_IF(pFrameControl==nullptr,
            "[requestNo:%u] IPipelineBufferSetFrameControl::castFrom() history frame(requestNo:%u frameNo:%u)",
            requestNo, historyFrame->getRequestNo(), historyFrame->getFrameNo());

        auto const historyTimestamp = pFrameControl->tryGetSensorTimestamp();
        if ( historyTimestamp == requestTimestamp ) {
            pTargetFrameControl = pFrameControl;
            break;//hit
        }
        if (CC_UNLIKELY( 0==historyTimestamp )) {
            MY_LOGW("History Frame(requestNo:%u frameNo:%u): timestamp==0",
                historyFrame->getRequestNo(), historyFrame->getFrameNo());
        }
        else {
            MY_LOGW("History Frame(requestNo:%u frameNo:%u): timestamp==%" PRId64 "",
                historyFrame->getRequestNo(), historyFrame->getFrameNo(), historyTimestamp);
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
transferBuffersFromFrame(TransferBuffersFromFrameParams const& params) -> void
{
    auto const& pConfigInfo2 = params.pConfigInfo2;
    auto        pReqResult = (params.pReqResult);
    auto&       pHistoryFrameControl = params.pHistoryFrameControl;
    auto&       vHalMeta  = *(params.pvHalMeta);
    auto&       vHalImage = *(params.pvHalImage);

    /**
     *  P1 output stream buffers contain:
     *   (1) App meta
     *   (2) App image
     *   (3) Hal meta
     *   (4) Hal image
     *
     *  For reprocess frames:
     *  - (1) and (2) are not needed to keep in HAL during preview since applications will
     *    send them to HAL via createReprocessCaptureRequest (TotalCaptureResult inputResult).
     *  - (3) and (4) may be needed for raw-to-yuv tuning.
     *
     *  How to get the stream IDs of (3) and (4)?
     *  - IOMap shows all streams needed for this reprocess frame.
     *  - Input streams of P2 Node from IOMAP are the candidates (Don't use P1 IOMAP).
     *  - P1 HAL meta control streams must be ignored.
     */

    //Input streams of P2 Node from IOMAP
    auto collectStreamId_P2NodeIOMapInput = [](auto const& nodeIOMapSet) {
        std::set<StreamId_T> set;
        for (auto const& v : nodeIOMapSet) {
            switch (v.first)
            {
            case eNODEID_P2CaptureNode:
            case eNODEID_P2StreamNode:
                {
                auto const& iomapSet = v.second;
                for (size_t i = 0; i < iomapSet.size(); i++) {
                    auto insertStreamId = [=, &set](auto const& streamSet) {
                        for (size_t j = 0; j < streamSet.size(); j++) {
                            auto streamId = streamSet[j];
                                set.insert(streamId);
                        }
                    };
                    insertStreamId(iomapSet[i].vIn);
                }
                }break;
            default:
                break;
            }
        }
        return set;
    };

    // Transfer stream buffers from the given frame (i.e. "pFrameControl") to "out".
    auto transferBuffers = [](
        auto const& pFrameControl,                      // from
        auto& out,                                      // to
        std::set<StreamId_T> const& filteredStreamIdSet,// candidate stream id
        std::set<StreamId_T> const& ignoredStreamIdSet  // ignored stream id
    )
    {
        using android_sp_StreamBufferT = typename std::remove_reference<decltype(*out.data())>::type;
        android::Vector<android_sp_StreamBufferT> vStreamBuffer;
        pFrameControl->transferPendingReleaseBuffers(vStreamBuffer);

        for (size_t i = 0; i < vStreamBuffer.size(); i++) {
            auto pBuffer = vStreamBuffer[i].get();
            if (CC_LIKELY( pBuffer != nullptr )) {
                auto streamId = pBuffer->getStreamInfo()->getStreamId();
                if (  ignoredStreamIdSet.find(streamId) == ignoredStreamIdSet.end() //not ignored
                  && filteredStreamIdSet.find(streamId) != filteredStreamIdSet.end()//candidates
                   )
                {
                    out.push_back(pBuffer);
                    CAM_LOGI("Attach HAL stream: %#" PRIx64 " %s", streamId, pBuffer->getName());
                }
                else
                {
                    CAM_LOGI("Ignore HAL stream: %#" PRIx64 " %s", streamId, pBuffer->getName());
                }
            }
        }
    };

    // [transfer] History P1 HAL Meta (from pTargetFrameControl) -> vHalMeta
    transferBuffers(
        pHistoryFrameControl, vHalMeta,
        collectStreamId_P2NodeIOMapInput(pReqResult->nodeIOMapMeta),
        [=](){
            //Ignore P1 HAL meta control streams.
            std::set<StreamId_T> ignoredStreamId;
            for (auto const& v : pConfigInfo2->mvParsedStreamInfo_P1)
               ignoredStreamId.insert( v.pHalMeta_Control->getStreamId() );
            return ignoredStreamId;
        }()
    );

    // [transfer] History P1 HAL Image (from pTargetFrameControl) -> vHalImage
    transferBuffers(
        pHistoryFrameControl, vHalImage,
        collectStreamId_P2NodeIOMapInput(pReqResult->nodeIOMapImage),
        {}
    );

}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
checkJpegControlMetadata(IMetadata* pMetadata) const -> void
{
#define JPEG_ENTRY(_tag_)                               \
    do {                                                \
        auto const& entry = pMetadata->entryFor(_tag_); \
        if (CC_UNLIKELY( entry.isEmpty() )) {           \
            MY_LOGW("NO " #_tag_ );                      \
          /*pMetadata->update(_tag_, _entryToUpdate_);*/\
        }                                               \
    } while (0)

    if (CC_LIKELY( pMetadata != nullptr ))
    {
        JPEG_ENTRY( MTK_JPEG_GPS_COORDINATES );
        JPEG_ENTRY( MTK_JPEG_GPS_PROCESSING_METHOD );
        JPEG_ENTRY( MTK_JPEG_GPS_TIMESTAMP );
        JPEG_ENTRY( MTK_JPEG_ORIENTATION );
        JPEG_ENTRY( MTK_JPEG_QUALITY );
        JPEG_ENTRY( MTK_JPEG_THUMBNAIL_QUALITY );
        JPEG_ENTRY( MTK_JPEG_THUMBNAIL_SIZE );
    }

#undef JPEG_ENTRY
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
handleKeepFrameForReprocess(HandleKeepFrameForReprocess const& params) -> void
{
    if ( params.isKeepForReprocess ) {
        MY_LOGF_IF(params.isReprocessRequest,
            "[requestNo:%u frameNo:%u] shouldn't keep a reprocess request",
            params.pPipelineFrame->getRequestNo(),
            params.pPipelineFrame->getFrameNo());

        keepFrameForReprocess(params);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
keepFrameForReprocess(HandleKeepFrameForReprocess const& params) -> void
{
    auto& pPipelineFrame = params.pPipelineFrame;

    MY_LOGI("[requestNo:%u frameNo:%u] kept for reprocess",
            pPipelineFrame->getRequestNo(), pPipelineFrame->getFrameNo());

    auto pFrameControl = IPipelineBufferSetFrameControl::castFrom(pPipelineFrame.get());
    MY_LOGF_IF(pFrameControl==nullptr,
            "[requestNo:%u frameNo:%u] IPipelineBufferSetFrameControl::castFrom",
            pPipelineFrame->getRequestNo(), pPipelineFrame->getFrameNo());

    pFrameControl->configureInformationKeeping(
        true/*keepTimestamp*/,
        true/*keepHalImage*/,
        true/*keepHalMeta*/,
        false/*keepAppMeta*/);
    {
        std::lock_guard<std::mutex> _l(mHistoryFrameContainerLock);
        mHistoryFrameContainer.push_back(pPipelineFrame);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
acquireAllHistoryFrames() -> HistoryFrameContainerT
{
    HistoryFrameContainerT history;
    {
        std::lock_guard<std::mutex> _l(mHistoryFrameContainerLock);
        //All elements are transfered to the local variable "history".
        //mHistoryFrameContainer is empty after the splice.
        history.splice(history.end(), mHistoryFrameContainer);
    }
    return history;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
setReprocessRequestNo(uint32_t requestNo) -> void
{
    std::lock_guard<std::mutex> _l(mReprocessRequestNoLock);
    mReprocessRequestNo = requestNo;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
resetReprocessRequestNo(uint32_t requestNo) -> void
{
    std::lock_guard<std::mutex> _l(mReprocessRequestNoLock);
    if ( requestNo == mReprocessRequestNo ) {
        mReprocessRequestNo = BAD_REQUEST_NO;
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
debugReprocessRequestResult(
    uint32_t requestNo,
    android::Vector<android::sp<IMetaStreamBuffer>>const& vAppOutMeta
) const -> void
{
    bool isReprocessRequest = [=](){
        std::lock_guard<std::mutex> _l(mReprocessRequestNoLock);
        return ( requestNo == mReprocessRequestNo );
    }();

    if ( isReprocessRequest ) {
        for ( size_t i = 0; i < vAppOutMeta.size(); i++ ) {
            if ( auto pStreamBuffer = vAppOutMeta[i].get() ) {
                if ( auto pMetadata = pStreamBuffer->tryReadLock(LOG_TAG) ) {
                    int64_t timestamp = 0;
                    if ( IMetadata::getEntry(pMetadata, MTK_SENSOR_TIMESTAMP, timestamp) ) {
                        MY_LOGI("[requestNo:%u] reprocess request has a callback SENSOR_TIMESTAMP:%" PRId64 "", requestNo, timestamp);
                    }
                    pStreamBuffer->unlock(LOG_TAG, pMetadata);
                }
            }
        }
    }
}

