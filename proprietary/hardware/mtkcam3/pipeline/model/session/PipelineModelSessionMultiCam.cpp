/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PipelineModelSessionMultiCam"
//
#include "PipelineModelSessionMultiCam.h"
//
#include "MyUtils.h"
//
#include <camera_custom_stereo.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/LogicalCam/IHalLogicalDeviceList.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>
#include <mtkcam3/feature/stereo/hal/stereo_setting_provider.h>
#include <kd_imgsensor_define.h>    // camsv sensor setting
#include <bandwidth_control.h>      // bwc
#include <mtkcam/drv/IHwSyncDrv.h>  // hwsync
#include <mtkcam/aaa/IHal3A.h>      // query ae init shutter

#include <mtkcam/utils/hw/IScenarioControlV3.h>

#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam3/pipeline/utils/streaminfo/MetaStreamInfo.h>

#if '1'== MTKCAM_HAVE_CAM_MANAGER
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/hw/CamManager.h>
#endif

#if (MTKCAM_HAVE_AEE_FEATURE == 1)
#include <aee.h>
#endif

#define UPDATE_FRAME_DEBUG 0
/******************************************************************************
 *
 ******************************************************************************/
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v1::Stereo;

#define ThisNamespace   PipelineModelSessionMultiCam
/******************************************************************************
 *
 ******************************************************************************/
MultiCamMetadataUtility::
~MultiCamMetadataUtility()
{
}
/******************************************************************************
 *
 ******************************************************************************/
bool
MultiCamMetadataUtility::
split(
    PipelineModelSessionBase::Result& original,
    PipelineModelSessionBase::Result& residue,
    std::unordered_map<int32_t, PipelineModelSessionBase::Result>& result,
    std::unordered_map<int64_t, int32_t>& groupSet
)
{
    if(result.size() != groupSet.size())
    {
        CAM_LOGE("[PipelineModelSessionMultiCam] size not consisted (%zu:%zu)", result.size(), groupSet.size());
        return false;
    }
    //
    auto getGroupId = [&groupSet](int64_t streamId)
    {
        auto iter = groupSet.find(streamId);
        // first element will be default set
        if(iter == groupSet.end())
        {
            return -1;
        }
        return iter->second;
    };
    auto addAppLeftCount = [&result](int32_t ignore_id = -1)
    {
        for(auto it_2 = result.begin(); it_2 != result.end();)
        {
            if(it_2->first != ignore_id)
            {
                it_2->second.nAppOutMetaLeft++;
            }
            ++it_2;
        }
        return true;
    };
    // init left count
    residue.nAppOutMetaLeft = original.nAppOutMetaLeft;
    residue.nHalOutMetaLeft = original.nHalOutMetaLeft;
    residue.frameNo = original.frameNo;
    for(auto it = result.begin(); it != result.end();)
    {
        it->second.nAppOutMetaLeft = original.nAppOutMetaLeft;
        it->second.nHalOutMetaLeft = original.nHalOutMetaLeft;
        it->second.frameNo = original.frameNo;
    }
    // split app metadata
    for(auto it = original.vAppOutMeta.begin(); it != original.vAppOutMeta.end();)
    {
        auto groupId = getGroupId((*it)->getStreamInfo()->getStreamId());
        if(groupId != -1)
        {
            auto result_iter = result.find(groupId);
            if(result_iter != result.end())
            {
                result_iter->second.vAppOutMeta.push_back(*it);
                // add left count to other.
                addAppLeftCount(groupId);
                // add residue count
                residue.nAppOutMetaLeft++;
            }
            else
            {
                CAM_LOGE("[PipelineModelSessionMultiCam] find group id, but cannot find in result app queue.");
                // no group id find, add to residue.
                residue.vAppOutMeta.push_back(*it);
                // add left count to other.
                addAppLeftCount();
            }
        }
        else
        {
            // no group id find, add to residue.
            residue.vAppOutMeta.push_back(*it);
            // add left count to other.
            addAppLeftCount();
        }
        ++it;
    }
    // split hal metadata
    for(auto it = original.vHalOutMeta.begin(); it != original.vHalOutMeta.end();)
    {
        auto groupId = getGroupId((*it)->getStreamInfo()->getStreamId());
        if(groupId != -1)
        {
            auto result_iter = result.find(groupId);
            if(result_iter != result.end())
            {
                result_iter->second.vHalOutMeta.push_back(*it);
                // add left count to other.
                addAppLeftCount(groupId);
                // add residue count
                residue.nAppOutMetaLeft++;
            }
            else
            {
                CAM_LOGE("[PipelineModelSessionMultiCam] find group id, but cannot find in result app queue.");
                // no group id find, add to residue.
                residue.vHalOutMeta.push_back(*it);
                // add left count to other.
                addAppLeftCount();
            }
        }
        else
        {
            // no group id find, add to residue.
            residue.vHalOutMeta.push_back(*it);
            // add left count to other.
            addAppLeftCount();
        }
        ++it;
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
MultiCamMetadataUtility::
filterResultByResultKey(
    int32_t sensorId,
    const IMetadata& src,
    IMetadata& dst
)
{
    // get static metadata
    auto metadataProvider = NSMetadataProviderManager::valueForByDeviceId(sensorId);
    // get available result key
    auto entry = metadataProvider->getMtkStaticCharacteristics().entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
    if(!entry.isEmpty())
    {
        // do filter
        for(size_t i=0;i<entry.count();++i)
        {
            auto tag = entry.itemAt(i, Type2Type<MINT32>());
            auto tag_entry = src.entryFor(tag);
            if(!tag_entry.isEmpty())
            {
                dst.update(tag_entry.tag(), tag_entry);
            }
        }
        //manual add
        {
            auto tag_entry = src.entryFor(MTK_SENSOR_ROLLING_SHUTTER_SKEW);
            if(!tag_entry.isEmpty())
            {
                dst.update(tag_entry.tag(), tag_entry);
            }
        }
    }
    else
    {
        CAM_LOGD("available result key not exist, ignore filter.");
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
makeInstance(
    std::string const& name,
    CtorParams const& rCtorParams __unused
) -> android::sp<IPipelineModelSession>
{
    android::sp<ThisNamespace> pSession = new ThisNamespace(name, rCtorParams);
    if  ( CC_UNLIKELY(pSession==nullptr) ) {
        CAM_LOGE("[%s] Bad pSession", __FUNCTION__);
        return nullptr;
    }

    int const err = pSession->configure();
    if  ( CC_UNLIKELY(err != 0) ) {
        CAM_LOGE("[%s] err:%d(%s) - Fail on configure()", __FUNCTION__, err, ::strerror(-err));
        return nullptr;
    }

    return pSession;
}


/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
ThisNamespace(
    std::string const& name,
    CtorParams const& rCtorParams)
    : PipelineModelSessionDefault(name, rCtorParams)
{
    auto const& pParsedAppConfiguration = mStaticInfo.pUserConfiguration->pParsedAppConfiguration;
    auto const& pParsedDualCamInfo = pParsedAppConfiguration->pParsedDualCamInfo;
    if(pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::Feature)
    {
        mMultiCamFeatureMode = pParsedDualCamInfo->mDualFeatureMode;
        if(MTK_MULTI_CAM_FEATURE_MODE_VSDOF == mMultiCamFeatureMode)
        {
            thermal_policy_name = "thermal_policy_03";
            mDualFeatureConfiguration = std::bind(&ThisNamespace::configure_vsdof, this);
            mDualFeatureFrameUpdate = std::bind(
                                            &ThisNamespace::updateFrame_vsdof,
                                            this,
                                            std::placeholders::_1,
                                            std::placeholders::_2,
                                            std::placeholders::_3);
        }
    }
    else if(pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::MultiCamControl)
    {
        thermal_policy_name = "thermal_policy_03";
        mDualFeatureConfiguration = std::bind(&ThisNamespace::configure_multicam, this);
        mDualFeatureFrameUpdate = std::bind(
                                        &ThisNamespace::updateFrame_multicam,
                                        this,
                                        std::placeholders::_1,
                                        std::placeholders::_2,
                                        std::placeholders::_3);
        MY_LOGD("multicam control");
    }
    else
    {
        MY_LOGE("should not happened (%d)", pParsedDualCamInfo->mDualDevicePath);
    }
    int value = property_get_int32("vendor.debug.camera.dualsession", 0);
    if(value)
        mbShowLog = true;
    else
        mbShowLog = false;
}
/******************************************************************************
 *
 ******************************************************************************/
ThisNamespace::
~ThisNamespace()
{
    MY_LOGD("reset value");
    // 1. reset stereo setting provider
    StereoSettingProvider::setStereoFeatureMode(0);
    // 2. if sensor sync enable, it has to reset.
    if(SensorSyncType::CALIBRATED == mSensorSyncType)
    {
        bool ret = setSensorSyncToSensorDriver(false);
        if(!ret)
        {
            MY_LOGA("setSensorSyncToSensorDriver faile");
        }
    }
    // 3. reset log
    setLogLevelToEngLoad(0, 0);
    // 4. reset thermal polocy

#if '1'== MTKCAM_HAVE_CAM_MANAGER
    if(!thermal_policy_name.empty())
    {
        Utils::CamManager::getInstance()->setThermalPolicy(thermal_policy_name.c_str(), 0);
        //property_set("vendor.thermal.manager.data.dis-policy", thermal_policy_name.c_str());
    }
#endif
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configure() -> int
{
    if(mStaticInfo.pPipelineStaticInfo->sensorId.size() > 2)
    {
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
        aee_system_exception(
            LOG_TAG,
            NULL,
            DB_OPT_DEFAULT,
            "\nCRDISPATCH_KEY:vsdof not support more than 2 sensors.");
#endif
    }
    if(mDualFeatureConfiguration)
    {
        mDualFeatureConfiguration();
    }

    // call base
    PipelineModelSessionDefault::configure();
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateBeforeBuildPipelineContext() -> int
{
    auto ret = UNKNOWN_ERROR;
    auto queryAeInitShutter = [](
                                MUINT32 const& openId,
                                uint32_t const& sensorMode,
                                MUINT32 &initShutter)
    {
        auto queryResult = UNKNOWN_ERROR;
        auto mp3A = MAKE_Hal3A(openId, LOG_TAG);

        if(CC_UNLIKELY(!mp3A)){
            return queryResult;
        }

        mp3A->setSensorMode(sensorMode);
        mp3A->send3ACtrl(NS3Av3::E3ACtrl_GetInitExposureTime, reinterpret_cast<MINTPTR>(&initShutter), NULL);

        mp3A->destroyInstance(LOG_TAG);
        return OK;
    };
    auto setHwSyncParam = [](
                            MUINT32 const& dvfsLevel,
                            MUINT32 const& sensorDevId,
                            MUINT32 const& aeInitShutter)
    {
        HWSyncDrv* pHwSync = MAKE_HWSyncDrv();
        if(CC_UNLIKELY(!pHwSync)){
            return UNKNOWN_ERROR;
        }
        MUINT32 ret = pHwSync->sendCommand(
                            HW_SYNC_CMD_ENUM::HW_SYNC_CMD_SET_PARA,
                            sensorDevId,
                            dvfsLevel,      // mmdvfs level
                            aeInitShutter   // init shutter time
        );
        pHwSync->destroyInstance();
        if(ret != 0) return UNKNOWN_ERROR;
        else return OK;
    };
    // check dvfs status.
    MUINT32 dvfsLevel = 0;
    int curScen=0, curBoost=0, featureFlag = 0;
    int64_t lastFrameNo = 0;
    if(mpScenarioCtrl != nullptr)
    {
        mpScenarioCtrl->getCurrentStatus(curScen, curBoost, lastFrameNo, featureFlag);
        if(FEATURE_CFG_IS_ENABLED(featureFlag, IScenarioControlV3::FEATURE_VSDOF_PREVIEW))
        {
            dvfsLevel = 3;
            MY_LOGD("DVFS is set to high");
        }
        // set hwsync param, before build pipeline context.
        MUINT32 aeInitShutter = 33000;
        // get open id list
        auto pHalDeviceList = MAKE_HalLogicalDeviceList();
        auto openIdList = pHalDeviceList->getSensorId(mStaticInfo.pPipelineStaticInfo->openId);
        if(CC_UNLIKELY(!mConfigInfo2))
        {
            MY_LOGE("mConfigInfo2 is null");
            goto lbExit;
        }
        if(openIdList.size() != mConfigInfo2->mvSensorSetting.size())
        {
            MY_LOGE("something wrong, please check sensor list and sensor setting.");
            goto lbExit;
        }
        for(unsigned long i=0;i<openIdList.size();++i)
        {
            RETURN_ERROR_IF_NOT_OK(
                                    queryAeInitShutter(
                                                    openIdList[i],
                                                    mConfigInfo2->mvSensorSetting[i].sensorMode,
                                                    aeInitShutter),
                                    "query ae init shutter fail");

            MY_LOGD("setup hwsync driver + , openId(%d), sensorDev(%d), dvfsLevel(%d), aeInitShutter(%d)",
                openIdList[i],
                mvSensorDevIdList[i],
                dvfsLevel,
                aeInitShutter
            );
            // 2. set param to hwsync
            RETURN_ERROR_IF_NOT_OK(
                                    setHwSyncParam(
                                                    dvfsLevel,
                                                    mvSensorDevIdList[i],
                                                    aeInitShutter
                                                    ),
                                    "set hwsync param fail");
            MY_LOGD("setup hwsync driver -");
        }
    }
    // call base
    PipelineModelSessionDefault::updateBeforeBuildPipelineContext();
    ret = OK;
lbExit:
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configure_vsdof() -> bool
{
    auto const& pParsedAppConfiguration = mStaticInfo.pUserConfiguration->pParsedAppConfiguration;
    auto const& pParsedAppImageStreamInfo = mStaticInfo.pUserConfiguration->pParsedAppImageStreamInfo;
    // 1. get multi-cam feature mode
    MINT32 stereoMode = E_STEREO_FEATURE_CAPTURE|E_STEREO_FEATURE_VSDOF;
    MINT32 forcemode = property_get_int32( "vendor.camera.stereo.mode", -1);
    if(forcemode == -1)
    {
        forcemode = getStereoModeType();
    }
    if(forcemode == 0)
    {
        MY_LOGI("vsdof");
        stereoMode = E_STEREO_FEATURE_CAPTURE | E_STEREO_FEATURE_VSDOF;
    }
    else if(forcemode == 1)
    {
        MY_LOGI("force 3rd flow");
        stereoMode = E_STEREO_FEATURE_THIRD_PARTY;
    }
    else if(forcemode == 2)
    {
        MY_LOGI("force tk depth");
        stereoMode = E_STEREO_FEATURE_MTK_DEPTHMAP;
    }
    else
    {
        MY_LOGA("not support");
        return false;
    }
    // 2. get preview size for multi-cam
    MSize preview_size(0, 0);
    IMetadata::IEntry const entry = pParsedAppConfiguration->sessionParams.entryFor(MTK_VSDOF_FEATURE_PREVIEW_SIZE);
    if(!entry.isEmpty())
    {
        preview_size.w = entry.itemAt(0, Type2Type<MINT32>());
        preview_size.h = entry.itemAt(1, Type2Type<MINT32>());
    }
    else
    {
        int32_t maxSizeArea = 0;
        sp<IImageStreamInfo> maxStreamInfo = nullptr;
        for(auto& streamInfo : pParsedAppImageStreamInfo->vAppImage_Output_Proc)
        {
            switch(streamInfo.second->getImgFormat())
            {
                case eImgFmt_YV12:
                    int32_t sizeArea = streamInfo.second->getImgSize().w *
                                        streamInfo.second->getImgSize().h;
                    if(sizeArea > maxSizeArea)
                    {
                        maxStreamInfo = streamInfo.second;
                    }
                break;
            }
            preview_size.w = maxStreamInfo->getImgSize().w;
            preview_size.h = maxStreamInfo->getImgSize().h;
        }
    }
    prepareSensorObject();
    { // is need?
        StereoSettingProvider::setLogicalDeviceID(mStaticInfo.pPipelineStaticInfo->openId);
    }
    MINT32 iPreviewMode = -1;
    IMetadata::getEntry<MINT32>(
                    &pParsedAppConfiguration->sessionParams,
                    MTK_VSDOF_FEATURE_PREVIEW_MODE,
                    iPreviewMode);
    bool bPortraitMode = (iPreviewMode == MTK_VSDOF_FEATURE_PREVIEW_MODE_HALF)?true:false;
    StereoSettingProvider::setStereoFeatureMode(stereoMode, bPortraitMode);
    StereoSettingProvider::setPreviewSize(preview_size);
    MY_LOGD("stereo mode(%d) preview(%dx%d) portrait(%d)",
                                        stereoMode,
                                        preview_size.w,
                                        preview_size.h,
                                        bPortraitMode);

    // 3. check sync related information
    IHalLogicalDeviceList* pHalDeviceList;
    pHalDeviceList = MAKE_HalLogicalDeviceList();
    mSensorSyncType = pHalDeviceList->getSyncType(mStaticInfo.pPipelineStaticInfo->openId);
    if(SensorSyncType::CALIBRATED == mSensorSyncType)
    {
        bool ret = setSensorSyncToSensorDriver(true);
        if(!ret)
        {
            MY_LOGA("setSensorSyncToSensorDriver faile");
            return false;
        }
    }

    // 4. update black list and set log level for vsdof
    if(MTK_MULTI_CAM_FEATURE_MODE_VSDOF == mMultiCamFeatureMode)
    {
        // for p1_main2 node, no needs to callback app metadata.
        mvStreamId_BlackList.push_back(eSTREAMID_META_APP_DYNAMIC_01_MAIN2); // app
        mvStreamId_BlackList.push_back(eSTREAMID_META_PIPE_DYNAMIC_01_MAIN2);// hal
        // update log level (for dual cam case, it will print more log than single cam.)
        setLogLevelToEngLoad(1, 1, 20000);
    }

    // 5. set vsdof thermal policy
#if '1'== MTKCAM_HAVE_CAM_MANAGER
    if(!thermal_policy_name.empty())
    {
        Utils::CamManager::getInstance()->setThermalPolicy(thermal_policy_name.c_str(), 1);
        //property_set("vendor.thermal.manager.data.en-policy", thermal_policy_name.c_str()); 
    }
#endif

    // 6. check if needs to use multi-thread to create pipeline.
    if(::usingMultithreadForPipelineContext())
    {
        mbUsingMultiThreadToBuildPipelineContext = true;
    }
    else
    {
        mbUsingMultiThreadToBuildPipelineContext = false;
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
configure_multicam() -> bool
{
    // 1. prepare sensor object
    prepareSensorObject();

    // 2. check sync related information
    IHalLogicalDeviceList* pHalDeviceList;
    pHalDeviceList = MAKE_HalLogicalDeviceList();
    mSensorSyncType = pHalDeviceList->getSyncType(mStaticInfo.pPipelineStaticInfo->openId);
    if(SensorSyncType::CALIBRATED == mSensorSyncType)
    {
        bool ret = setSensorSyncToSensorDriver(true);
        if(!ret)
        {
            MY_LOGA("setSensorSyncToSensorDriver faile");
            return false;
        }
    }

    // 3. update black list and set log level
    // workaround: logical multi-cam need p1_main2 dynamic
    {
        // for p1_main2 node, no needs to callback app metadata.
        mvStreamId_BlackList.push_back(eSTREAMID_META_APP_DYNAMIC_01_MAIN2); // app
        mvStreamId_BlackList.push_back(eSTREAMID_META_PIPE_DYNAMIC_01_MAIN2);// hal
        mvStreamId_BlackList.push_back(eSTREAMID_META_APP_DYNAMIC_02_MAIN2);// p2 streaming app
        // update log level (for dual cam case, it will print more log than single cam.)
        setLogLevelToEngLoad(1, 1, 20000);
    }

    // 4. set vsdof thermal policy
#if '1'== MTKCAM_HAVE_CAM_MANAGER
    if(!thermal_policy_name.empty())
    {
        Utils::CamManager::getInstance()->setThermalPolicy(thermal_policy_name.c_str(), 1);
        //property_set("vendor.thermal.manager.data.en-policy", thermal_policy_name.c_str());
    }
#endif

    // 5. set feature mode
    {
        MY_LOGI("set feature mode for multicam");
        MINT32 stereoMode = E_STEREO_FEATURE_MULTI_CAM;
        StereoSettingProvider::setLogicalDeviceID(mStaticInfo.pPipelineStaticInfo->openId);
        StereoSettingProvider::setStereoFeatureMode(stereoMode, false);
    }

    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateFrame_vsdof(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result
) -> bool
{
#if UPDATE_FRAME_DEBUG
    auto dumpInfo = [this, userId, requestNo](Result const& result)
    {
        android::String8 strInfo("");
        strInfo += String8::format("u(%d) r(%d) f(%ld) appOutMetaLeft(%zu) halOutMetaLest(%zu) e(%d)", userId, requestNo, result.frameNo, result.nAppOutMetaLeft, result.nHalOutMetaLeft, result.bFrameEnd);
        for(auto item:result.vAppOutMeta)
        {
            strInfo += String8::format("a(%s)", item->getStreamInfo()->getStreamName());
        }
        for(auto item:result.vHalOutMeta)
        {
            strInfo += String8::format("h(%s)", item->getStreamInfo()->getStreamName());
        }
        MY_LOGD("%s", strInfo.string());
    };
#endif
    auto isMain2Stream = [this](int64_t streamId)
    {
        auto it = std::find(mvStreamId_BlackList.begin(), mvStreamId_BlackList.end(), streamId);
        if(it != mvStreamId_BlackList.end())
            return true;
        else
            return false;
    };
    Result result_main2; // always store main2 app & main2 hal
    Result result_other;
    bool needMain2Callback = false;
    bool needOtherCallback = false;
    result_main2.nAppOutMetaLeft = result.nAppOutMetaLeft;
    result_main2.nHalOutMetaLeft = result.nHalOutMetaLeft;
    result_other.nAppOutMetaLeft = result.nAppOutMetaLeft;
    result_other.nHalOutMetaLeft = result.nHalOutMetaLeft;
    result_main2.frameNo = result.frameNo;
    result_other.frameNo = result.frameNo;
#if UPDATE_FRAME_DEBUG
    MY_LOGD("================original");
    dumpInfo(result);
#endif
    // app
    for(auto it = result.vAppOutMeta.begin(); it != result.vAppOutMeta.end();)
    {
        if(isMain2Stream((*it)->getStreamInfo()->getStreamId()))
        {
            result_main2.vAppOutMeta.push_back(*it);
            needMain2Callback = true;
        }
        else
        {
            result_other.vAppOutMeta.push_back(*it);
            needOtherCallback = true;
        }
        ++it;
    }
    // hal
    for(auto it = result.vHalOutMeta.begin(); it != result.vHalOutMeta.end();)
    {
        if(isMain2Stream((*it)->getStreamInfo()->getStreamId()))
        {
            result_main2.vHalOutMeta.push_back(*it);
            needMain2Callback = true;
        }
        else
        {
            result_other.vHalOutMeta.push_back(*it);
            needOtherCallback = true;
        }
        ++it;
    }
    if(!needOtherCallback && needMain2Callback) // pure main2 metadata callback
    {
        result_main2.bFrameEnd = result.bFrameEnd;
        result_other.bFrameEnd = false;
    }
    else if((needOtherCallback && needMain2Callback) ||
            (needOtherCallback && !needMain2Callback))
    {
        result_main2.bFrameEnd = false; // split frame always set frame end is false.
        result_other.bFrameEnd = result.bFrameEnd;
    }
    // callback split package first.
    if(needMain2Callback)
    {
#if UPDATE_FRAME_DEBUG
        MY_LOGD("================main2 metadata package");
        dumpInfo(result_main2);
#endif
        if(mpZslProcessor.get())
            mpZslProcessor->onFrameUpdated(requestNo,userId,result_main2);
    }
    if(needOtherCallback)
    {
#if UPDATE_FRAME_DEBUG
        MY_LOGD("================other metadata package");
        dumpInfo(result_other);
#endif
        result_other.bFrameEnd = result.bFrameEnd;
        if(mbShowLog)
        {
            MINT32 warning = -1;
            for(auto& metadata:result_other.vAppOutMeta)
            {
                IMetadata* buf = metadata->tryReadLock(LOG_TAG);
                if(buf != nullptr)
                {
                    if(IMetadata::getEntry<MINT32>(buf, MTK_STEREO_FEATURE_WARNING, warning))
                    {
                        MY_LOGD("[%#" PRIxPTR " :%d] stereo warning(%x)", userId, requestNo, warning);
                    }
                    metadata->unlock(LOG_TAG, buf);
                }
            }
        }
        PipelineModelSessionDefault::updateFrame(requestNo, userId, result_other);
    }
    // it may contain app and hal metadata is empty case.
    // just callback directly.
    if(!needMain2Callback && !needOtherCallback)
    {
        PipelineModelSessionDefault::updateFrame(requestNo, userId, result);
    }
    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateFrame_multicam(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result
) -> bool
{
#if UPDATE_FRAME_DEBUG
    auto dumpInfo = [this, userId, requestNo](Result const& result)
    {
        android::String8 strInfo("");
        strInfo += String8::format("u(%d) r(%d) f(%ld) appOutMetaLeft(%zu) halOutMetaLest(%zu) e(%d)", userId, requestNo, result.frameNo, result.nAppOutMetaLeft, result.nHalOutMetaLeft, result.bFrameEnd);
        for(auto item:result.vAppOutMeta)
        {
            strInfo += String8::format("a(%s)", item->getStreamInfo()->getStreamName());
        }
        for(auto item:result.vHalOutMeta)
        {
            strInfo += String8::format("h(%s)", item->getStreamInfo()->getStreamName());
        }
        MY_LOGD("%s", strInfo.string());
    };
#endif
    auto isMain2Stream = [this](int64_t streamId)
    {
        auto it = std::find(mvStreamId_BlackList.begin(), mvStreamId_BlackList.end(), streamId);
        if(it != mvStreamId_BlackList.end())
            return true;
        else
            return false;
    };
    auto isNeedCallbackPhysicalAppMetadata = [this, &result](int32_t sensorId)
    {
        auto iter = std::find(result.vPhysicalCameraSetting.begin(),
                              result.vPhysicalCameraSetting.end(),
                              sensorId);
        if(iter != result.vPhysicalCameraSetting.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    };
    // no metadata, just last frame callback case.
    if((result.vAppOutMeta.size() == 0) &&
       (result.vHalOutMeta.size() == 0) && result.bFrameEnd)
    {
        MY_LOGD("u(%#" PRIxPTR ") r(%d) callback full",
                                        userId,
                                        requestNo);
        // frame end will be set to true in this moment.
        PipelineModelSessionDefault::updateFrame(requestNo, userId, result);
        return true;
    }
    Result result_main2; // always store main2 app & main2 hal
    Result result_other;
    result_main2.nAppOutMetaLeft = result.nAppOutMetaLeft;
    result_main2.nHalOutMetaLeft = result.nHalOutMetaLeft;
    result_other.nAppOutMetaLeft = result.nAppOutMetaLeft;
    result_other.nHalOutMetaLeft = result.nHalOutMetaLeft;
    result_main2.frameNo = result.frameNo;
    result_other.frameNo = result.frameNo;
    android::String8 debugLog("");
    debugLog.appendFormat("u(%#" PRIxPTR ") r(%d) result.app(%zu) result.hal(%zu)", userId, requestNo, result.vAppOutMeta.size(), result.vHalOutMeta.size());
#if UPDATE_FRAME_DEBUG
    MY_LOGD("================original");
    dumpInfo(result);
#endif
    // app
    for(auto it = result.vAppOutMeta.begin(); it != result.vAppOutMeta.end();)
    {
        // debug
        IMetadata* buf = (*it)->tryReadLock(LOG_TAG);
        if(buf != nullptr)
        {
            debugLog.appendFormat("s(%#" PRIx64 ") app metadata size(%d) ",
                        (*it)->getStreamInfo()->getStreamId(),
                        buf->count());
                        (*it)->unlock(LOG_TAG, buf);
        }
        // debug
        if(isMain2Stream((*it)->getStreamInfo()->getStreamId()))
        {
            result_main2.vAppOutMeta.push_back(*it);
        }
        else
        {
            result_other.vAppOutMeta.push_back(*it);
        }
        ++it;
    }
    // hal
    for(auto it = result.vHalOutMeta.begin(); it != result.vHalOutMeta.end();)
    {
        if(isMain2Stream((*it)->getStreamInfo()->getStreamId()))
        {
            result_main2.vHalOutMeta.push_back(*it);
        }
        else
        {
            result_other.vHalOutMeta.push_back(*it);
        }
        ++it;
    }
    // frame end condition check
    bool needMain1Callback = ((result_other.vAppOutMeta.size() != 0) || (result_other.vHalOutMeta.size() != 0));
    bool needMain2Callback = ((result_main2.vAppOutMeta.size() != 0) || (result_main2.vHalOutMeta.size() != 0));
    bool needCallbackPhysicalAppMetadata_main1 = isNeedCallbackPhysicalAppMetadata(mStaticInfo.pUserConfiguration->vPhysicCameras[0]);
    bool needCallbackPhysicalAppMetadata_main2 = isNeedCallbackPhysicalAppMetadata(mStaticInfo.pUserConfiguration->vPhysicCameras[1]);
    debugLog.appendFormat("physic 1(%d) physic 2(%d) appleft(%d) halleft(%d) frameEnd(%d) ",
                    needCallbackPhysicalAppMetadata_main1,
                    needCallbackPhysicalAppMetadata_main2,
                    result.nAppOutMetaLeft,
                    result.nHalOutMetaLeft,
                    result.bFrameEnd
                    );
    // case 1: main1 needs callback, main2 doesn't.
    if(needMain1Callback && !needMain2Callback)
    {
        debugLog.appendFormat("callback main1 [%d:%zu:%zu], main2 no callback [%d:%zu:%zu] ",
                                        needMain1Callback,
                                        result_other.vAppOutMeta.size(),
                                        result_other.vHalOutMeta.size(),
                                        needMain2Callback,
                                        result_main2.vAppOutMeta.size(),
                                        result_main2.vHalOutMeta.size());
        result_other.bFrameEnd = result.bFrameEnd;
        debugLog.appendFormat("main1 size(a%zu:h%zu) main2 size(a%zu:h%zu) ",
                                            result_other.nAppOutMetaLeft,
                                            result_other.nHalOutMetaLeft,
                                            result_main2.nAppOutMetaLeft,
                                            result_main2.nHalOutMetaLeft);
    }
    else if(!needMain1Callback && needMain2Callback)
    {
        debugLog.appendFormat("no callback main1 [%d:%zu:%zu], main2 callback [%d:%zu:%zu] ",
                                        needMain1Callback,
                                        result_other.vAppOutMeta.size(),
                                        result_other.vHalOutMeta.size(),
                                        needMain2Callback,
                                        result_main2.vAppOutMeta.size(),
                                        result_main2.vHalOutMeta.size());
        result_main2.bFrameEnd = result.bFrameEnd;
        debugLog.appendFormat("main1 size(a%zu:h%zu) main2 size(a%zu:h%zu) ",
                                            result_other.nAppOutMetaLeft,
                                            result_other.nHalOutMetaLeft,
                                            result_main2.nAppOutMetaLeft,
                                            result_main2.nHalOutMetaLeft);
    }
    else if(needMain1Callback && needMain2Callback)
    {
        debugLog.appendFormat("callback main1 [%d:%zu:%zu], main2 callback [%d:%zu:%zu] ",
                                        needMain1Callback,
                                        result_other.vAppOutMeta.size(),
                                        result_other.vHalOutMeta.size(),
                                        needMain2Callback,
                                        result_main2.vAppOutMeta.size(),
                                        result_main2.vHalOutMeta.size());
        // main1 callback first, so needs to add main2 metadata count.
        result_other.nAppOutMetaLeft += result_main2.vAppOutMeta.size();
        result_other.nHalOutMetaLeft += result_main2.vHalOutMeta.size();
        MY_LOGD("main1 size(a%zu:h%zu) main2 size(a%zu:h%zu) ",
                                            result_other.nAppOutMetaLeft,
                                            result_other.nHalOutMetaLeft,
                                            result_main2.nAppOutMetaLeft,
                                            result_main2.nHalOutMetaLeft);
        // just main1 callback needs set frame end flag.
        // because, other camera callback will not check frameEnd flag.
        result_other.bFrameEnd = false;
        result_main2.bFrameEnd = result.bFrameEnd;
    }
    else
    {
        // ignore, it means app & hal are empty, and it not frame end.
    }
    // contain main1 case
    if((result_other.vAppOutMeta.size() != 0) ||
       (result_other.vHalOutMeta.size() != 0))
    {
        if(needCallbackPhysicalAppMetadata_main1)
        {
            for(auto& item : result_other.vAppOutMeta)
            {
                IMetadata filteredMetadata;
                sp<IMetaStreamInfo> pStreamInfo = new NSCam::v3::Utils::MetaStreamInfo(
                                    "Meta:App:Physical_Main1",
                                    item->getStreamInfo()->getStreamId(),
                                    eSTREAMTYPE_META_OUT,
                                    1);
                auto srcMeta = item->tryReadLock(LOG_TAG);
                MultiCamMetadataUtility::filterResultByResultKey(
                                    mStaticInfo.pUserConfiguration->vPhysicCameras[0],
                                    *srcMeta,
                                    filteredMetadata);
                item->unlock(LOG_TAG, srcMeta);
                android::sp<NSCam::v3::IMetaStreamBuffer> filteredMetaStreamBuf =
                            HalMetaStreamBuffer::Allocator(pStreamInfo.get())(filteredMetadata);
                filteredMetaStreamBuf->finishUserSetup();
                result_other.vPhysicalOutMeta.add(
                                mStaticInfo.pUserConfiguration->vPhysicCameras[0],
                                filteredMetaStreamBuf);
            }
        }
        MY_LOGD("debug info (%s) send main1 (%d)", debugLog.string(), result_other.vPhysicalOutMeta.size());
        PipelineModelSessionDefault::updateFrame(requestNo, userId, result_other);
    }
    // contain main2 case
    if((result_main2.vAppOutMeta.size() != 0) ||
       (result_main2.vHalOutMeta.size() != 0))
    {
        if(mpZslProcessor.get())
            mpZslProcessor->onFrameUpdated(requestNo,userId,result_main2);
        if(needCallbackPhysicalAppMetadata_main2)
        {
            for(auto& item : result_main2.vAppOutMeta)
            {
                IMetadata filteredMetadata;
                sp<IMetaStreamInfo> pStreamInfo = new NSCam::v3::Utils::MetaStreamInfo(
                                    "Meta:App:Physical_Main2",
                                    item->getStreamInfo()->getStreamId(),
                                    eSTREAMTYPE_META_OUT,
                                    1);
                auto srcMeta = item->tryReadLock(LOG_TAG);
                MultiCamMetadataUtility::filterResultByResultKey(
                                    mStaticInfo.pUserConfiguration->vPhysicCameras[1],
                                    *srcMeta,
                                    filteredMetadata);
                item->unlock(LOG_TAG, srcMeta);
                android::sp<NSCam::v3::IMetaStreamBuffer> filteredMetaStreamBuf =
                            HalMetaStreamBuffer::Allocator(pStreamInfo.get())(filteredMetadata);
                filteredMetaStreamBuf->finishUserSetup();
                result_main2.vPhysicalOutMeta.add(
                                mStaticInfo.pUserConfiguration->vPhysicCameras[1],
                                filteredMetaStreamBuf);
            }
            // clear app out metadata
            // cannot set vAppOutMeta in Main2 metadata in updateFrameTimestamp.
            // otherwise, it will cause shutter check fail in camera framework.
            result_main2.vAppOutMeta.clear();
            MY_LOGD("debug info (%s) send main2 (%d)", debugLog.string(), result_main2.vPhysicalOutMeta.size());
            updateFrameTimestamp(requestNo, userId, result_main2, -1);
        }
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
prepareSensorObject() -> bool
{
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    for(auto& sensorId : mStaticInfo.pPipelineStaticInfo->sensorId)
    {
        // 1. build sensor dev id list.
        MUINT32 sensorDevId = pHalSensorList->querySensorDevIdx(
                                sensorId);
        mvSensorDevIdList.push_back(sensorDevId);
        // 2. create sensor hal interface.
        auto sensorHalInterface = pHalSensorList->createSensor(
                                            LOG_TAG,
                                            sensorId);
        mvSensorHalInterface.push_back(sensorHalInterface);
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
setSensorSyncToSensorDriver(bool enable) -> bool
{
    bool ret = true;
    MINT retSensorCmd = 0;
    bool dualCamMode = (enable) ? true : false;
    MUINT32 syncMode = SENSOR_MASTER_SYNC_MODE;
    MY_LOGD("enable(%d)", enable);
    IHalLogicalDeviceList* pHalDeviceList;
    pHalDeviceList = MAKE_HalLogicalDeviceList();
    MUINT32 masterDevId =
            pHalDeviceList->getSensorSyncMasterDevId(mStaticInfo.pPipelineStaticInfo->openId);
    if(masterDevId == 0xFF)
    {
        MY_LOGW("cannot support sensor sync");
        goto lbExit;
    }
    if((mvSensorHalInterface.size() < 2) &&
        mvSensorHalInterface.size() != mvSensorDevIdList.size())
    {
        MY_LOGA("mvSensorHalInterface less than 2 / sensorInterface not equal to sensorDevId(%zu:%zu)",
                    mvSensorHalInterface.size(),
                    mvSensorDevIdList.size());
        ret = false;
        goto lbExit;
    }
    // 1. assign master/slave mode to sensor driver.
    for(unsigned long i=0;i<mvSensorHalInterface.size();++i)
    {
        if(enable)
        {
            if(mvSensorDevIdList[i] == masterDevId)
            {
                syncMode = SENSOR_MASTER_SYNC_MODE;
            }
            else
            {
                syncMode = SENSOR_SLAVE_SYNC_MODE;
            }
        }
        else
        {
            syncMode = SENSOR_MASTER_SYNC_MODE;
        }
        retSensorCmd = mvSensorHalInterface[i]->sendCommand(
                                    mvSensorDevIdList[i],
                                    SENSOR_CMD_SET_SENSOR_SYNC_MODE,
                                    (MUINTPTR)&syncMode,
                                    0 /* unused */,
                                    0 /* unused */);
        if(retSensorCmd != 0)
        {
            ret = false;
            MY_LOGA("sendCommand (SENSOR_CMD_SET_SENSOR_SYNC_MODE)(dev:%d) fail", mvSensorDevIdList[i]);
            goto lbExit;
        }
    }
    // 2. set dual cam mode to sensor driver.
    // only set this value to main sensor.
    retSensorCmd = mvSensorHalInterface[0]->sendCommand(
                            mvSensorDevIdList[0],
                            SENSOR_CMD_SET_DUAL_CAM_MODE,
                            (MUINTPTR)&dualCamMode,
                            0 /* unused */,
                            0 /* unused */);
    if(retSensorCmd != 0)
    {
        ret = false;
        MY_LOGA("sendCommand (SENSOR_CMD_SET_DUAL_CAM_MODE) fail");
        goto lbExit;
    }
lbExit:
    return ret;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
ThisNamespace::
updateFrame(
    MUINT32 const requestNo,
    MINTPTR const userId,
    Result const& result
) -> MVOID
{
    if(mDualFeatureFrameUpdate)
    {
        mDualFeatureFrameUpdate(requestNo, userId, result);
    }
    else
    {
        MY_LOGE("mDualFeatureFrameUpdate not set");
        PipelineModelSessionDefault::updateFrame(requestNo, userId, result);
    }
    return;
}