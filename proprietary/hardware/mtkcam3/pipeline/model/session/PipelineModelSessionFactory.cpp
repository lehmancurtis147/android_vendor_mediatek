/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PipelineModelSession-Factory"
//
#include <impl/IPipelineModelSession.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam3/pipeline/policy/IPipelineSettingPolicy.h>
#include "PipelineModelSessionDefault.h"
#include "PipelineModelSessionSMVR.h"
#include "PipelineModelSessionMultiCam.h"
#include "PipelineModelSession4Cell.h"
#include "PipelineModelSessionStreaming.h"
#include "PipelineModelSessionAppRaw16Reprocess.h"
#include "MyUtils.h"

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::v3::pipeline::policy::pipelinesetting;


/******************************************************************************
 *
 ******************************************************************************/
static
auto
parseAppStreamInfo(std::shared_ptr<PipelineUserConfiguration> config __unused) -> bool
{
    auto const& out = config->pParsedAppImageStreamInfo;

    MSize maxStreamSize = MSize(0, 0);
    MSize maxYuvSize = MSize(0, 0);
    for (auto const& s : config->vImageStreams)
    {
        bool isOut = false;
        auto const& pStreamInfo = s.second;
        if  ( CC_LIKELY(pStreamInfo != nullptr) )
        {
            switch  (pStreamInfo->getImgFormat())
            {
            case eImgFmt_RAW16: //(deprecated) It should be converted to
                                // the real unpack format by app stream manager.
            case eImgFmt_BAYER8_UNPAK:
            case eImgFmt_BAYER10_UNPAK:
            case eImgFmt_BAYER12_UNPAK:
            case eImgFmt_BAYER14_UNPAK:
            case eImgFmt_BAYER15_UNPAK:
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_OUT) {
                    isOut = true;
                    out->pAppImage_Output_RAW16 = pStreamInfo;
                }
                else
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_IN) {
                    out->pAppImage_Input_RAW16 = pStreamInfo;
                }
                break;
                //
            case eImgFmt_BLOB://AS-IS: should be removed in the future
            case eImgFmt_JPEG://TO-BE: Jpeg Capture
                isOut = true;
                out->pAppImage_Jpeg = pStreamInfo;
                break;
                //
            case eImgFmt_YV12:
            case eImgFmt_NV21:
            case eImgFmt_YUY2:
            case eImgFmt_Y8:
            case eImgFmt_Y16:
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_OUT) {
                    isOut = true;
                    out->vAppImage_Output_Proc.emplace(s.first, pStreamInfo);
                    if  (   ! out->hasVideoConsumer
                        &&  ( pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER )
                        )
                    {
                        out->hasVideoConsumer = true;
                        out->videoImageSize = pStreamInfo->getImgSize();
                        out->hasVideo4K = ( out->videoImageSize.w*out->videoImageSize.h > 8000000 )? true : false;
                    }
                    if  ( maxYuvSize.size() <= pStreamInfo->getImgSize().size() ) {
                        maxYuvSize = pStreamInfo->getImgSize();
                    }
                }
                else
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_IN) {
                    out->pAppImage_Input_Yuv = pStreamInfo;
                }
                break;
                //
            case eImgFmt_CAMERA_OPAQUE:
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_OUT) {
                    isOut = true;
                    out->pAppImage_Output_Priv = pStreamInfo;
                }
                else
                if  (pStreamInfo->getStreamType() == eSTREAMTYPE_IMAGE_IN) {
                    out->pAppImage_Input_Priv = pStreamInfo;
                }
                break;
                //
            default:
                CAM_LOGE("[%s] Unsupported format:0x%x", __FUNCTION__, pStreamInfo->getImgFormat());
                break;
            }
        }

        //if (isOut) //[TODO] why only for output?
        {
            if  ( maxStreamSize.size() <= pStreamInfo->getImgSize().size() ) {
                maxStreamSize = pStreamInfo->getImgSize();
            }
        }

    }

    out->maxImageSize = maxStreamSize;
    out->maxYuvSize = maxYuvSize;

    return true;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
convertToUserConfiguration(
    const PipelineStaticInfo&       rPipelineStaticInfo __unused,
    const UserConfigurationParams&  rUserConfigurationParams __unused
) -> std::shared_ptr<PipelineUserConfiguration>
{
    auto pParsedAppConfiguration = std::make_shared<ParsedAppConfiguration>();
    if  ( CC_UNLIKELY(pParsedAppConfiguration == nullptr) ) {
        CAM_LOGE("[%s] Fail on make_shared<ParsedAppConfiguration>", __FUNCTION__);
        return nullptr;
    }

    auto pParsedAppImageStreamInfo = std::make_shared<ParsedAppImageStreamInfo>();
    if  ( CC_UNLIKELY(pParsedAppImageStreamInfo == nullptr) ) {
        CAM_LOGE("[%s] Fail on make_shared<ParsedAppImageStreamInfo>", __FUNCTION__);
        return nullptr;
    }

    auto pParsedDualCamInfo = std::make_shared<ParsedDualCamInfo>();
    if  ( CC_UNLIKELY(pParsedDualCamInfo == nullptr) ) {
        CAM_LOGE("[%s] Fail on make_shared<ParsedDualCamInfo>", __FUNCTION__);
        return nullptr;
    }

    auto out = std::make_shared<PipelineUserConfiguration>();
    if  ( CC_UNLIKELY(out == nullptr) ) {
        CAM_LOGE("[%s] Fail on make_shared<PipelineUserConfiguration>", __FUNCTION__);
        return nullptr;
    }

    pParsedAppConfiguration->operationMode = rUserConfigurationParams.operationMode;
    pParsedAppConfiguration->sessionParams = rUserConfigurationParams.sessionParams;
    pParsedAppConfiguration->isConstrainedHighSpeedMode = (pParsedAppConfiguration->operationMode == 1/*CONSTRAINED_HIGH_SPEED_MODE*/);
    {
        const char* key = "persist.vendor.camera3.operationMode.superNightMode";
        //this property is not defined if it's 0 (normal mode).
        int32_t const operationMode_superNightMode = ::property_get_int32(key, 0);
        if (operationMode_superNightMode != 0) {
            CAM_LOGI("%s=%#x", key, operationMode_superNightMode);
            pParsedAppConfiguration->isSuperNightMode = ((uint32_t)operationMode_superNightMode == pParsedAppConfiguration->operationMode);
        }
        else {
            pParsedAppConfiguration->isSuperNightMode = false;
        }
    }
    // check dual path
    if(rPipelineStaticInfo.isDualDevice)
    {
        // 1. check contain dual feature in session param.
        MINT32 mode = -1;
        MINT32 forceFeatureMode = ::property_get_int32("vendor.camera.forceFeatureMode", -1);
        MY_LOGD("normal flow (dual cam) forceFeatureMode(%d)", forceFeatureMode);
        if(forceFeatureMode > -1)
        {
            pParsedDualCamInfo->mDualDevicePath = NSCam::v3::pipeline::policy::DualDevicePath::Feature;
            pParsedDualCamInfo->mDualFeatureMode = forceFeatureMode;
        }
        else if(IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_MULTI_CAM_FEATURE_MODE, mode))
        {
            pParsedDualCamInfo->mDualDevicePath = NSCam::v3::pipeline::policy::DualDevicePath::Feature;
            pParsedDualCamInfo->mDualFeatureMode = mode;
        }
        else if(rUserConfigurationParams.vPhysicCameras.size() > 0)
        {
            pParsedDualCamInfo->mDualDevicePath = NSCam::v3::pipeline::policy::DualDevicePath::MultiCamControl;
        }
        else
        {
            pParsedDualCamInfo->mDualDevicePath = NSCam::v3::pipeline::policy::DualDevicePath::Single;
        }
    }
    out->pParsedAppConfiguration = pParsedAppConfiguration;
    out->pParsedAppConfiguration->pParsedDualCamInfo = pParsedDualCamInfo;
    out->pParsedAppImageStreamInfo = pParsedAppImageStreamInfo;
    out->vImageStreams = rUserConfigurationParams.vImageStreams;
    out->vMetaStreams = rUserConfigurationParams.vMetaStreams;
    out->vMinFrameDuration = rUserConfigurationParams.vMinFrameDuration;
    out->vStallFrameDuration = rUserConfigurationParams.vStallFrameDuration;
    out->vPhysicCameras = rUserConfigurationParams.vPhysicCameras;

    parseAppStreamInfo(out);

    return out;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
decidePipelineModelSession(
    IPipelineModelSessionFactory::CreationParams const& creationParams,
    std::shared_ptr<PipelineUserConfiguration>const& pUserConfiguration,
    std::shared_ptr<IPipelineSettingPolicy>const& pSettingPolicy
) -> android::sp<IPipelineModelSession>
{
    auto convertTo_CtorParams = [=]() {
        return PipelineModelSessionBase::CtorParams{
            .staticInfo = {
                .pPipelineStaticInfo    = creationParams.pPipelineStaticInfo,
                .pUserConfiguration     = pUserConfiguration,
            },
            .debugInfo = {
                .pErrorPrinter          = creationParams.pErrorPrinter,
                .pWarningPrinter        = creationParams.pWarningPrinter,
                .pDebugPrinter          = creationParams.pDebugPrinter,
            },
            .pPipelineModelCallback     = creationParams.pPipelineModelCallback,
            .pPipelineSettingPolicy     = pSettingPolicy,
        };
    };


    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    //  [Session Policy] decide which session
    //  Add special sessions below...
    //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    ////////////////////////////////////////////////////////////////////////////
    //  Super Night Mode Capture
    ////////////////////////////////////////////////////////////////////////////
    if (pUserConfiguration->pParsedAppConfiguration->isSuperNightMode) {
        CAM_LOGI("[%s] Super Night Mode", __FUNCTION__);
        return PipelineModelSessionAppRaw16Reprocess::makeInstance("SuperNightMode/", convertTo_CtorParams());
    }

    auto const operationMode = pUserConfiguration->pParsedAppConfiguration->operationMode;
    switch  ( operationMode )
    {
    ////////////////////////////////////////////////////////////////////////////
    //
    ////////////////////////////////////////////////////////////////////////////
    case 0/* NORMAL_MODE - [FIXME] hard-code */:{
        if(creationParams.pPipelineStaticInfo->is4CellSensor)
        {
            MY_LOGI("4Cell");
            return PipelineModelSession4Cell::makeInstance("4Cell/", convertTo_CtorParams());
        }
        if(creationParams.pPipelineStaticInfo->isVhdrSensor)
        {
            MY_LOGI("Streaming");
            return PipelineModelSessionStreaming::makeInstance("Streaming/", convertTo_CtorParams());
        }
        if(creationParams.pPipelineStaticInfo->isDualDevice)
        {
            auto const& pParsedAppConfiguration     = pUserConfiguration->pParsedAppConfiguration;
            auto const& pParsedDualCamInfo          = pParsedAppConfiguration->pParsedDualCamInfo;
            if(pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::Feature)
            {
                if(MTK_MULTI_CAM_FEATURE_MODE_VSDOF == pParsedDualCamInfo->mDualFeatureMode)
                {
                    MY_LOGI("Vsdof");
                    return PipelineModelSessionMultiCam::makeInstance(
                                                            std::string("Vsdof/"),
                                                            convertTo_CtorParams());
                }
            }
            else if(pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::MultiCamControl)
            {
                MY_LOGI("multicam");
                return PipelineModelSessionMultiCam::makeInstance(std::string("Dual/"),
                                                            convertTo_CtorParams());
            }
        }
        }break;

    ////////////////////////////////////////////////////////////////////////////
    //  Session: SMVR
    ////////////////////////////////////////////////////////////////////////////
    case 1/* CONSTRAINED_HIGH_SPEED_MODE */:{
        //  Session SMVR
        MY_LOGI("SMVR");
        return PipelineModelSessionSMVR::makeInstance(convertTo_CtorParams());
        }break;

    ////////////////////////////////////////////////////////////////////////////
    default:{
        CAM_LOGE("[%s] Unsupported operationMode:0x%x", __FUNCTION__, operationMode);
        return nullptr;
        }break;
    }
    ////////////////////////////////////////////////////////////////////////////
    //  Session: Default
    ////////////////////////////////////////////////////////////////////////////
    MY_LOGI("Default");
    return PipelineModelSessionDefault::makeInstance("Default/", convertTo_CtorParams());
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IPipelineModelSessionFactory::
createPipelineModelSession(
    CreationParams const& params __unused
) -> android::sp<IPipelineModelSession>
{
    #undef  CHECK_PTR
    #define CHECK_PTR(_ptr_, _msg_) \
        if  ( CC_UNLIKELY(_ptr_ == nullptr) ) { \
            CAM_LOGE("[%s] nullptr pointer - %s", __FUNCTION__, _msg_); \
            return nullptr; \
        }

    //  (1) validate input parameters.
    CHECK_PTR(params.pPipelineStaticInfo, "pPipelineStaticInfo");
    CHECK_PTR(params.pUserConfigurationParams, "pUserConfigurationParams");
    CHECK_PTR(params.pErrorPrinter, "pErrorPrinter");
    CHECK_PTR(params.pWarningPrinter, "pWarningPrinter");
    CHECK_PTR(params.pDebugPrinter, "pDebugPrinter");
    CHECK_PTR(params.pPipelineModelCallback, "pPipelineModelCallback");

    //  (2) convert to UserConfiguration
    auto pUserConfiguration = convertToUserConfiguration(
        *params.pPipelineStaticInfo,
        *params.pUserConfigurationParams
    );
    CHECK_PTR(pUserConfiguration, "convertToUserConfiguration");

    //  (3) pipeline policy
    auto pSettingPolicy = IPipelineSettingPolicyFactory::createPipelineSettingPolicy(
        IPipelineSettingPolicyFactory::CreationParams{
            .pPipelineStaticInfo        = params.pPipelineStaticInfo,
            .pPipelineUserConfiguration = pUserConfiguration,
    });
    CHECK_PTR(pSettingPolicy, "Fail on createPipelineSettingPolicy");

    //  (4) pipeline session
    auto pSession = decidePipelineModelSession(params, pUserConfiguration, pSettingPolicy);
    CHECK_PTR(pSession, "Fail on decidePipelineModelSession");

    return pSession;
}

