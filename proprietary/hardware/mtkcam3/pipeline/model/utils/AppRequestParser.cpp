/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-AppRequestParser"

#include <impl/AppRequestParser.h>

#include "MyUtils.h"

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::model;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {


/******************************************************************************
 *
 ******************************************************************************/
static auto getDebugLogLevel() -> int32_t
{
    return ::property_get_int32("persist.vendor.debug.camera.log", 0);
}
static int32_t gLogLevel = getDebugLogLevel();


/******************************************************************************
 *
 ******************************************************************************/
static void print(android::Printer& printer, ParsedAppRequest const& o)
{
    printer.printFormatLine(".requestNo=%u", o.requestNo);

    if  (auto p = o.pParsedAppImageStreamBuffers) {
        android::String8 os;
        os += "App image stream buffers=";
        os += toString(*p);
        printer.printLine(os.c_str());
    }

}


/******************************************************************************
 *
 ******************************************************************************/
static void dumpToLog(
    ParsedAppRequest const& o,
    android_LogPriority logPriority
)
{
    android::LogPrinter logPrinter(LOG_TAG, logPriority, "");
    logPrinter.printLine("ParsedAppRequest=");
    print(logPrinter, o);
}


/******************************************************************************
 *
 ******************************************************************************/
static auto categorizeImgStream(
    ParsedAppRequest* out,
    std::unordered_map<StreamId_T, sp<IImageStreamBuffer>> const& inMap,
    bool isInput
) -> int
{
    auto const& pParsedAppImageStreamBuffers = out->pParsedAppImageStreamBuffers;
    auto const& pParsedAppImageStreamInfo = out->pParsedAppImageStreamInfo;
    MSize maxStreamSize = MSize(0, 0);
    for(auto const& it : inMap)
    {
        auto const& pStreamBuffer = it.second;
        auto const& pStreamInfo = const_cast<IImageStreamInfo*>(pStreamBuffer->getStreamInfo());
        if ( CC_LIKELY(pStreamInfo != nullptr) )
        {
            switch( pStreamInfo->getImgFormat() )
            {
                case eImgFmt_RAW16: //(deprecated) It should be converted to
                                    // the real unpack format by app stream manager.
                case eImgFmt_BAYER8_UNPAK:
                case eImgFmt_BAYER10_UNPAK:
                case eImgFmt_BAYER12_UNPAK:
                case eImgFmt_BAYER14_UNPAK:
                case eImgFmt_BAYER15_UNPAK:
                    if (isInput)
                    {
                        pParsedAppImageStreamBuffers->pAppImage_Input_RAW16 = pStreamBuffer;
                        pParsedAppImageStreamInfo->pAppImage_Input_RAW16 = pStreamInfo;
                    }
                    else
                    {
                        pParsedAppImageStreamBuffers->pAppImage_Output_RAW16 = pStreamBuffer;
                        pParsedAppImageStreamInfo->pAppImage_Output_RAW16 = pStreamInfo;
                    }
                    break;
                case eImgFmt_CAMERA_OPAQUE:
                    if (isInput)
                    {
                        pParsedAppImageStreamBuffers->pAppImage_Input_Priv = pStreamBuffer;
                        pParsedAppImageStreamInfo->pAppImage_Input_Priv = pStreamInfo;
                    }
                    else
                    {
                        pParsedAppImageStreamBuffers->pAppImage_Output_Priv = pStreamBuffer;
                        pParsedAppImageStreamInfo->pAppImage_Output_Priv = pStreamInfo;
                    }
                    break;
                    //
                case eImgFmt_BLOB://AS-IS: should be removed in the future
                case eImgFmt_JPEG://TO-BE: Jpeg Capture
                    pParsedAppImageStreamBuffers->pAppImage_Jpeg = pStreamBuffer;
                    pParsedAppImageStreamInfo->pAppImage_Jpeg = pStreamInfo;
                    if (pStreamInfo->getImgSize().size() > maxStreamSize.size())
                    {
                        maxStreamSize = pStreamInfo->getImgSize();
                    }
                    break;
                    //
                case eImgFmt_YV12:
                case eImgFmt_NV21:
                case eImgFmt_YUY2:
                case eImgFmt_Y8:
                case eImgFmt_Y16:
                    if (isInput)
                    {
                        pParsedAppImageStreamBuffers->pAppImage_Input_Yuv = pStreamBuffer;
                        pParsedAppImageStreamInfo->pAppImage_Input_Yuv = pStreamInfo;
                    }
                    else
                    {
                        pParsedAppImageStreamBuffers->vAppImage_Output_Proc.emplace(it.first, pStreamBuffer);
                        pParsedAppImageStreamInfo->vAppImage_Output_Proc.emplace(it.first, pStreamInfo);
                        if  (   ! pParsedAppImageStreamInfo->hasVideoConsumer
                                &&  ( pStreamInfo->getUsageForConsumer() & GRALLOC_USAGE_HW_VIDEO_ENCODER )
                            )
                        {
                                pParsedAppImageStreamInfo->hasVideoConsumer = true;
                                pParsedAppImageStreamInfo->videoImageSize = pStreamInfo->getImgSize();
                                pParsedAppImageStreamInfo->hasVideo4K = ( pParsedAppImageStreamInfo->videoImageSize.w*pParsedAppImageStreamInfo->videoImageSize.h > 8000000 )? true : false;
                        }
                        if (pStreamInfo->getImgSize().size() > maxStreamSize.size())
                        {
                            maxStreamSize = pStreamInfo->getImgSize();
                        }

                    }
                    break;
                    //
                default:
                    MY_LOGE("Unsupported format:0x%x", pStreamInfo->getImgFormat());
                    break;
            }
        }
    }
    pParsedAppImageStreamInfo->maxImageSize = maxStreamSize;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto parseAppRequest(
    ParsedAppRequest* out __unused,
    UserRequestParams const* in __unused
) -> int
{
    if  (CC_UNLIKELY(!out || !in)) {
        MY_LOGE("bad arguments - out:%p in:%p", out, in);
        return -EINVAL;
    }

    out->requestNo                    = in->requestNo;
    out->pAppMetaControlStreamBuffer  = in->vIMetaBuffers.begin()->second;
    out->pParsedAppImageStreamBuffers = std::make_shared<ParsedAppImageStreamBuffers>();
    out->pParsedAppImageStreamInfo    = std::make_shared<ParsedAppImageStreamInfo>();
    out->pParsedAppMetaControl        = std::make_shared<policy::ParsedMetaControl>();
    if  ( CC_UNLIKELY( ! out->pParsedAppImageStreamBuffers
                    || ! out->pParsedAppImageStreamInfo
                    || ! out->pParsedAppMetaControl ) )
    {
        MY_LOGE("Fail on make_shared - pParsedAppImageStreamBuffers:%p pParsedAppImageStreamInfo:%p pParsedAppMetaControl:%p",
            out->pParsedAppImageStreamBuffers.get(), out->pParsedAppImageStreamInfo.get(), out->pParsedAppMetaControl.get());
        return -EINVAL;
    }

    ////////////////////////////////////////////////////////////////////////////
    //  pParsedAppImageStreamBuffers
    //  pParsedAppImageStreamInfo

    categorizeImgStream(out, in->vIImageBuffers, true);
    categorizeImgStream(out, in->vOImageBuffers, false);

    ////////////////////////////////////////////////////////////////////////////
    //  pParsedAppMetaControl

    auto const& pAppControl = out->pAppMetaControlStreamBuffer;
    IMetadata* pMetadata = pAppControl->tryReadLock(LOG_TAG);
    if ( CC_UNLIKELY( ! pMetadata ) ) {
        MY_LOGE("bad metadata(%p) SBuffer(%p)", pMetadata, pAppControl.get() );
        pAppControl->unlock(LOG_TAG, pMetadata);
        return -EINVAL;
    }

    auto const& pParsedCtrl = out->pParsedAppMetaControl;
    pParsedCtrl->repeating = pAppControl->isRepeating();

    {
        IMetadata::IEntry const entry = pMetadata->entryFor(MTK_CONTROL_AE_TARGET_FPS_RANGE);
        if ( entry.count() == 2 ) {
            pParsedCtrl->control_aeTargetFpsRange[0] = entry.itemAt(0, Type2Type<int32_t>());
            pParsedCtrl->control_aeTargetFpsRange[1] = entry.itemAt(1, Type2Type<int32_t>());
        }
    }

#define PARSE_META_CONTROL(_tag_, _value_)                              \
    do {                                                                \
        IMetadata::IEntry const entry = pMetadata->entryFor(_tag_);     \
        if ( !entry.isEmpty() )                                         \
            _value_ = entry.itemAt(0, Type2Type<decltype(_value_)>());  \
    } while(0)

    PARSE_META_CONTROL(MTK_CONTROL_CAPTURE_INTENT,           pParsedCtrl->control_captureIntent);
    PARSE_META_CONTROL(MTK_CONTROL_ENABLE_ZSL,               pParsedCtrl->control_enableZsl);
    PARSE_META_CONTROL(MTK_CONTROL_MODE,                     pParsedCtrl->control_mode);
    PARSE_META_CONTROL(MTK_CONTROL_SCENE_MODE,               pParsedCtrl->control_sceneMode);
    PARSE_META_CONTROL(MTK_CONTROL_VIDEO_STABILIZATION_MODE, pParsedCtrl->control_videoStabilizationMode);
    PARSE_META_CONTROL(MTK_CONTROL_CAPTURE_REMOSAIC_EN,      pParsedCtrl->control_remosaicEn);
#undef PARSE_META_CONTROL

    pAppControl->unlock(LOG_TAG, pMetadata);

    ////////////////////////////////////////////////////////////////////////////

    if  ( CC_UNLIKELY(gLogLevel >= 2) )
    {
        dumpToLog(*out, ANDROID_LOG_INFO);
    }

    return OK;
}
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

