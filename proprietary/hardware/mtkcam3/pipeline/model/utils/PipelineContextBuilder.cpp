/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-PipelineContextBuilder"

#include <impl/PipelineContextBuilder.h>
//
#include <mtkcam/drv/IHalSensor.h>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include <camera_custom_eis.h>
//
#include <mtkcam3/pipeline/hwnode/NodeId.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>
#include <mtkcam3/pipeline/hwnode/P1Node.h>
#include <mtkcam3/pipeline/hwnode/P2StreamingNode.h>
#include <mtkcam3/pipeline/hwnode/P2CaptureNode.h>
#include <mtkcam3/pipeline/hwnode/FDNode.h>
#include <mtkcam3/pipeline/hwnode/JpegNode.h>
#include <mtkcam3/pipeline/hwnode/RAW16Node.h>
#include <mtkcam3/pipeline/hwnode/PDENode.h>
//
#include <mtkcam3/pipeline/utils/SyncHelper/ISyncHelper.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
#include <mtkcam3/feature/stereo/StereoCamEnum.h>
#include <mtkcam3/feature/stereo/hal/stereo_setting_provider.h>
//
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
#include <aee.h>
#endif
//
#include "MyUtils.h"
#include <mtkcam3/feature/fsc/fsc_defs.h>


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::model;
using namespace NSCam::EIS;
using namespace NSCam::Utils;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::NSPipelineContext;
using NSCam::FSC::EFSC_MODE_MASK_FSC_EN;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
//
#define FUNC_START  MY_LOGD("+")
#define FUNC_END    MY_LOGD("-")

/******************************************************************************
 *
 ******************************************************************************/
#if (MTKCAM_HAVE_AEE_FEATURE == 1)
    #define TRIGGER_AEE(_ops_)                            \
    do {                                                     \
        aee_system_exception(                             \
            LOG_TAG,                                      \
            NULL,                                         \
            DB_OPT_DEFAULT,                               \
            _ops_);                                       \
    } while(0)
#else
    #define TRIGGER_AEE(_ops_)
#endif

#define CHECK_ERROR(_err_, _ops_)                                \
    do {                                                  \
        int const err = (_err_);                          \
        if( CC_UNLIKELY(err != 0) ) {                     \
            MY_LOGE("err:%d(%s) ops:%s", err, ::strerror(-err), _ops_); \
            TRIGGER_AEE(_ops_);                           \
            return err;                                   \
        }                                                 \
    } while(0)


/******************************************************************************
 *
 ******************************************************************************/
namespace {


struct InternalCommonInputParams
{
    PipelineStaticInfo const*                   pPipelineStaticInfo = nullptr;
    PipelineUserConfiguration const*            pPipelineUserConfiguration = nullptr;
    android::sp<IResourceConcurrency>           pP1NodeResourceConcurrency = nullptr;
    bool const                                  bIsReConfigure;
};


}//namespace


/******************************************************************************
 *
 ******************************************************************************/
#define add_stream_to_set(_set_, _IStreamInfo_)                                 \
    do {                                                                        \
        if( _IStreamInfo_.get() ) { _set_.add(_IStreamInfo_->getStreamId()); }  \
    } while(0)
//
#define setImageUsage( _IStreamInfo_, _usg_ )                                   \
    do {                                                                        \
        if( _IStreamInfo_.get() ) {                                             \
            builder.setImageStreamUsage( _IStreamInfo_->getStreamId(), _usg_ ); \
        }                                                                       \
    } while(0)


/******************************************************************************
 *
 ******************************************************************************/
template <class DstStreamInfoT, class SrcStreamInfoT>
static void add_meta_stream(
    StreamSet& set,
    DstStreamInfoT& dst,
    SrcStreamInfoT const& src
)
{
    if ( src != nullptr ) {
        dst = src;
        set.add(src->getStreamId());
    }
}


template <class DstStreamInfoT, class SrcStreamInfoT>
static void add_image_stream(
    StreamSet& set,
    DstStreamInfoT& dst,
    SrcStreamInfoT const& src
)
{
    if ( src != nullptr ) {
        dst = src;
        set.add(src->getStreamId());
    }
}


template <class DstStreamInfoT, class SrcStreamInfoT>
static void add_image_stream(
    StreamSet& set,
    android::Vector<DstStreamInfoT>& dst,
    SrcStreamInfoT const& src
)
{
    if ( src != nullptr ) {
        dst.push_back(src);
        set.add(src->getStreamId());
    }
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_Streams(
    sp<PipelineContext> pContext,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    android::sp<IStreamBufferProvider>   pZSLProvider,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    InternalCommonInputParams const* pCommon
)
{
    FUNC_START;
    CAM_TRACE_CALL();
    auto const& pPipelineStaticInfo         = pCommon->pPipelineStaticInfo;
    auto const& pPipelineUserConfiguration  = pCommon->pPipelineUserConfiguration;
#define BuildStream(_type_, _IStreamInfo_)                                     \
    do {                                                                       \
        if( _IStreamInfo_.get() ) {                                            \
            int err = 0;                                                       \
            if ( 0 != (err = StreamBuilder(_type_, _IStreamInfo_)              \
                    .build(pContext)) )                                        \
            {                                                                  \
                MY_LOGE("StreamBuilder fail stream %#" PRIx64 " of type %d",   \
                    _IStreamInfo_->getStreamId(), _type_);                     \
                return err;                                                    \
            }                                                                  \
        }                                                                      \
    } while(0)

#define BuildStreamWithProvider(_type_, _IStreamInfo_, _buf_provider_)              \
    do {                                                                            \
        if(_buf_provider_.get())                                                   \
        {                                                                           \
            if( _IStreamInfo_.get() ) {                                            \
                int err = 0;                                                       \
                if ( 0 != (err = StreamBuilder(_type_, _IStreamInfo_)              \
                        .setProvider(_buf_provider_)                               \
                        .build(pContext)) )                                        \
                {                                                                  \
                    MY_LOGE("StreamBuilder fail stream %#" PRIx64 " of type %d",   \
                        _IStreamInfo_->getStreamId(), _type_);                     \
                    return err;                                                    \
                }                                                                  \
            }                                                                      \
        }                                                                          \
        else {                                                                      \
            BuildStream(_type_, _IStreamInfo_);                                     \
        }                                                                           \
    } while(0)

    auto const& pParsedAppImageStreamInfo = pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    // Non-P1
    // app meta stream
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_Control);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2CaptureNode);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicFD);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicJpeg);
    BuildStream(eStreamType_META_APP, pParsedStreamInfo_NonP1->pAppMeta_DynamicRAW16);
    // hal meta stream
    BuildStream(eStreamType_META_HAL, pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode);
    BuildStream(eStreamType_META_HAL, pParsedStreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode);
    BuildStream(eStreamType_META_HAL, pParsedStreamInfo_NonP1->pHalMeta_DynamicPDE);

    // hal image stream
    BuildStream(eStreamType_IMG_HAL_POOL   , pParsedStreamInfo_NonP1->pHalImage_FD_YUV);

    int32_t enable = property_get_int32("vendor.jpeg.rotation.enable", 1);
    int32_t imgo_use_pool = pPipelineStaticInfo->isSupportBurstCap
                         && (*pParsedStreamInfo_P1).size() > 0
                         && (pPipelineUserConfiguration->pParsedAppImageStreamInfo->maxYuvSize.size() > (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rrzo->getImgSize().size());

    MY_LOGD("Jpeg Rotation enable: %d", enable);
    if ( ! (enable & 0x1) )
        BuildStream(eStreamType_IMG_HAL_POOL   , pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);
    else
        BuildStream(eStreamType_IMG_HAL_RUNTIME, pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);

    BuildStream(eStreamType_IMG_HAL_RUNTIME, pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV);

    // P1
    for (size_t i = 0; i < (*pParsedStreamInfo_P1).size(); i++)
    {
        auto& info = (*pParsedStreamInfo_P1)[i];
        MY_LOGD("index : %zu", i);
        BuildStream(eStreamType_META_APP, info.pAppMeta_DynamicP1);
        BuildStream(eStreamType_META_HAL, info.pHalMeta_Control);
        BuildStream(eStreamType_META_HAL, info.pHalMeta_DynamicP1);
        MY_LOGD("Build P1 stream");
        if(pZSLProvider.get())
        {
            BuildStreamWithProvider(eStreamType_IMG_HAL_PROVIDER, info.pHalImage_P1_Imgo, pZSLProvider);
            BuildStreamWithProvider(eStreamType_IMG_HAL_PROVIDER, info.pHalImage_P1_Rrzo, pZSLProvider);
            BuildStreamWithProvider(eStreamType_IMG_HAL_PROVIDER, info.pHalImage_P1_Lcso, pZSLProvider);
        }
        else
        {
            BuildStream(imgo_use_pool ? eStreamType_IMG_HAL_POOL : eStreamType_IMG_HAL_RUNTIME, info.pHalImage_P1_Imgo);
            BuildStream(eStreamType_IMG_HAL_POOL, info.pHalImage_P1_Rrzo);
            BuildStream(eStreamType_IMG_HAL_POOL, info.pHalImage_P1_Lcso);
        }
        BuildStream(eStreamType_IMG_HAL_POOL, info.pHalImage_P1_Rsso);
        BuildStream(eStreamType_IMG_HAL_POOL, info.pHalImage_P1_FDYuv);
        MY_LOGD("New: p1 full raw(%p); resized raw(%p), pZSLProvider(%p)",
                 info.pHalImage_P1_Imgo.get(), info.pHalImage_P1_Rrzo.get(),pZSLProvider.get());
    }

    //  app image stream
    for( const auto& n : pParsedAppImageStreamInfo->vAppImage_Output_Proc ) {
        BuildStream(eStreamType_IMG_APP, n.second);
    }
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Input_Yuv);
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Input_Priv);
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Output_Priv);
    //
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Input_RAW16);
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Output_RAW16);
    BuildStream(eStreamType_IMG_APP, pParsedAppImageStreamInfo->pAppImage_Jpeg);
    //
#undef BuildStream
#undef BuildStreamWithProvider
    FUNC_END;
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
template <class INITPARAM_T, class CONFIGPARAM_T>
static
bool
compareParamsLocked_P1Node(
    INITPARAM_T const& initParam1,  INITPARAM_T const& initParam2,
    CONFIGPARAM_T const& cfgParam1, CONFIGPARAM_T const& cfgParam2
)
{
    FUNC_START;
    if ( initParam1.openId != initParam2.openId ||
         initParam1.nodeId != initParam2.nodeId ||
         strcmp(initParam1.nodeName, initParam2.nodeName) )
        return false;
    //
    if ( cfgParam1.sensorParams.mode        != cfgParam2.sensorParams.mode ||
         cfgParam1.sensorParams.size        != cfgParam2.sensorParams.size ||
         cfgParam1.sensorParams.fps         != cfgParam2.sensorParams.fps ||
         cfgParam1.sensorParams.pixelMode   != cfgParam2.sensorParams.pixelMode ||
         cfgParam1.sensorParams.vhdrMode    != cfgParam2.sensorParams.vhdrMode )
        return false;
    //
    if ( ! cfgParam1.pInAppMeta.get()  || ! cfgParam2.pInAppMeta.get() ||
         ! cfgParam1.pOutAppMeta.get() || ! cfgParam2.pOutAppMeta.get() ||
         ! cfgParam1.pOutHalMeta.get() || ! cfgParam2.pOutHalMeta.get() ||
         cfgParam1.pInAppMeta->getStreamId()  != cfgParam2.pInAppMeta->getStreamId() ||
         cfgParam1.pOutAppMeta->getStreamId() != cfgParam2.pOutAppMeta->getStreamId() ||
         cfgParam1.pOutHalMeta->getStreamId() != cfgParam2.pOutHalMeta->getStreamId() )
        return false;
    //
    if ( ! cfgParam1.pOutImage_resizer.get() || ! cfgParam2.pOutImage_resizer.get() ||
        cfgParam1.pOutImage_resizer->getStreamId() != cfgParam2.pOutImage_resizer->getStreamId() )
        return false;
    //
    if ( cfgParam1.pOutImage_lcso.get() != cfgParam2.pOutImage_lcso.get()
        || cfgParam1.enableLCS != cfgParam2.enableLCS){
        return false;
    }
    //
    if ( cfgParam1.pOutImage_rsso.get() != cfgParam2.pOutImage_rsso.get()
        || cfgParam1.enableRSS != cfgParam2.enableRSS){
        return false;
    }
    //
    if ( cfgParam1.pvOutImage_full.size() != cfgParam2.pvOutImage_full.size() )
        return false;
    //
    for ( size_t i=0; i<cfgParam1.pvOutImage_full.size(); i++ ) {
        bool bMatch = false;
        for ( size_t j=0; j<cfgParam2.pvOutImage_full.size(); j++ ) {
            if ( cfgParam1.pvOutImage_full.itemAt(i)->getStreamId() == cfgParam2.pvOutImage_full.itemAt(i)->getStreamId() )
            {
                bMatch = false;
                break;
            }
        }
        if ( !bMatch )
            return false;
    }
    //
    FUNC_END;

    return false;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_P1Node(
    sp<PipelineContext> pContext,
    android::sp<PipelineContext> const& pOldPipelineContext,
    StreamingFeatureSetting const* pStreamingFeatureSetting,
    PipelineNodesNeed const* pPipelineNodesNeed,
    ParsedStreamInfo_P1 const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    SensorSetting const* pSensorSetting,
    P1HwSetting const* pP1HwSetting,
    size_t const idx,
    uint32_t batchSize,
    bool bMultiDevice,
    bool bMultiCam_CamSvPath,
    InternalCommonInputParams const* pCommon,
    MBOOL isReConfig
)
{
    typedef P1Node                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;

    auto const& pPipelineStaticInfo         = pCommon->pPipelineStaticInfo;
    auto const& pPipelineUserConfiguration  = pCommon->pPipelineUserConfiguration;
    auto const& pParsedAppConfiguration     = pPipelineUserConfiguration->pParsedAppConfiguration;
    auto const& pParsedAppImageStreamInfo   = pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    auto const& pParsedDualCamInfo          = pParsedAppConfiguration->pParsedDualCamInfo;
    //
    auto const physicalSensorId = pPipelineStaticInfo->sensorId[idx];
    //
    int32_t initRequest = property_get_int32("vendor.debug.camera.pass1initrequestnum", 0);
    //
    NodeId_T nodeId = eNODEID_P1Node;
    if (idx == 1)
    {
        nodeId = eNODEID_P1Node_main2;
    }
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = physicalSensorId;
        initParam.nodeId = nodeId;
        initParam.nodeName = "P1Node";
    }

    StreamSet inStreamSet;
    StreamSet outStreamSet;
    NodeT::ConfigParams cfgParam;
    {
        NodeT::SensorParams sensorParam(
        /*mode     : */pSensorSetting->sensorMode,
        /*size     : */pSensorSetting->sensorSize,
        /*fps      : */pSensorSetting->sensorFps,
        /*pixelMode: */pP1HwSetting->pixelMode,
        /*vhdrMode : */pStreamingFeatureSetting->vhdrMode
        );
        //
        add_meta_stream ( inStreamSet, cfgParam.pInAppMeta,         pParsedStreamInfo_NonP1->pAppMeta_Control);
        add_meta_stream ( inStreamSet, cfgParam.pInHalMeta,         pParsedStreamInfo_P1->pHalMeta_Control);
        add_meta_stream (outStreamSet, cfgParam.pOutAppMeta,        pParsedStreamInfo_P1->pAppMeta_DynamicP1);
        add_meta_stream (outStreamSet, cfgParam.pOutHalMeta,        pParsedStreamInfo_P1->pHalMeta_DynamicP1);

        add_image_stream(outStreamSet, cfgParam.pOutImage_resizer,  pParsedStreamInfo_P1->pHalImage_P1_Rrzo);
        add_image_stream(outStreamSet, cfgParam.pOutImage_lcso,     pParsedStreamInfo_P1->pHalImage_P1_Lcso);
        add_image_stream(outStreamSet, cfgParam.pOutImage_rsso,     pParsedStreamInfo_P1->pHalImage_P1_Rsso);
        if  ( auto pImgoStreamInfo = pParsedStreamInfo_P1->pHalImage_P1_Imgo.get() )
        {
            // P1Node need imgoConfig to config driver
            auto const& setting = pP1HwSetting->imgoConfig;

            IImageStreamInfo::BufPlanes_t bufPlanes;
            auto infohelper = IHwInfoHelperManager::get()->getHwInfoHelper(physicalSensorId);
            MY_LOGF_IF(infohelper==nullptr, "getHwInfoHelper");
            bool ret = infohelper->getDefaultBufPlanes_Imgo(bufPlanes, setting.format, setting.imageSize);
            MY_LOGF_IF(!ret, "IHwInfoHelper::getDefaultBufPlanes_Imgo");

            auto pStreamInfo =
                ImageStreamInfoBuilder(pImgoStreamInfo)
                .setBufPlanes(bufPlanes)
                .setImgFormat(setting.format)
                .setImgSize(setting.imageSize)
                .build();
            MY_LOGI("%s", pStreamInfo->toString().c_str());
            add_image_stream(outStreamSet, cfgParam.pvOutImage_full, pStreamInfo);
        }
        if  ( auto pRaw16StreamInfo = pParsedAppImageStreamInfo->pAppImage_Output_RAW16.get() )
        {
            // P1Node is in charge of outputing App RAW16 if Raw16Node is not configured.
            if ( ! pPipelineNodesNeed->needRaw16Node )
            {
                MY_LOGI("P1Node << App Image Raw16 %s", pRaw16StreamInfo->toString().c_str());
                add_image_stream(outStreamSet, cfgParam.pvOutImage_full, pRaw16StreamInfo);
            }
        }
        cfgParam.enableLCS          = pParsedStreamInfo_P1->pHalImage_P1_Lcso.get() ? MTRUE : MFALSE;
        cfgParam.enableRSS          = (pParsedStreamInfo_P1->pHalImage_P1_Rsso.get() && idx == 0) ? MTRUE : MFALSE;
        cfgParam.enableFSC          = (pStreamingFeatureSetting->fscMode & EFSC_MODE_MASK_FSC_EN) ? MTRUE : MFALSE;
        // for CCT dump
        {
            int debugProcRaw = property_get_int32("vendor.debug.camera.cfg.ProcRaw", -1);
            if(debugProcRaw > 0)
            {
                MY_LOGD("set vendor.debug.camera.ProcRaw(%d) => 0:config pure raw  1:config processed raw",debugProcRaw);
                cfgParam.rawProcessed = debugProcRaw;
            }
        }
        // for 4cell multi-thread capture
        if(pCommon->pP1NodeResourceConcurrency != nullptr)
            cfgParam.pResourceConcurrency = pCommon->pP1NodeResourceConcurrency;
        //
        cfgParam.pStreamPool_resizer = NULL;
        cfgParam.pStreamPool_full    = NULL;
        // cfgParam.pStreamPool_resizer = mpHalImage_P1_ResizerRaw.get() ?
        //    pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00) : NULL;
        // cfgParam.pStreamPool_full = mpHalImage_P1_Raw.get() ?
        //    pContext->queryImageStreamPool(eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00) : NULL;
        if (idx == 0)
        {
            add_image_stream( inStreamSet, cfgParam.pInImage_yuv,       pParsedAppImageStreamInfo->pAppImage_Input_Yuv);
            add_image_stream( inStreamSet, cfgParam.pInImage_raw,       pParsedAppImageStreamInfo->pAppImage_Input_RAW16);
            add_image_stream( inStreamSet, cfgParam.pInImage_opaque,    pParsedAppImageStreamInfo->pAppImage_Input_Priv);
            add_image_stream(outStreamSet, cfgParam.pOutImage_opaque,   pParsedAppImageStreamInfo->pAppImage_Output_Priv);
        }
        cfgParam.sensorParams = sensorParam;
        {
            int64_t ImgoId;
            int64_t RrzoId;
            int64_t LcsoId;
            int64_t RssoId;
            switch (idx)
            {
                case 0:
                    ImgoId = eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00;
                    RrzoId = eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00;
                    LcsoId = eSTREAMID_IMAGE_PIPE_RAW_LCSO_00;
                    RssoId = eSTREAMID_IMAGE_PIPE_RAW_RSSO_00;
                    break;
                case 1:
                    ImgoId = eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_01;
                    RrzoId = eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01;
                    LcsoId = eSTREAMID_IMAGE_PIPE_RAW_LCSO_01;
                    RssoId = eSTREAMID_IMAGE_PIPE_RAW_RSSO_01;
                    break;
                default:
                    MY_LOGE("not support p1 node number large than 2");
                    break;
            }
            cfgParam.pStreamPool_resizer = pParsedStreamInfo_P1->pHalImage_P1_Rrzo.get() ?
                pContext->queryImageStreamPool(RrzoId) : NULL;
            cfgParam.pStreamPool_lcso    = pParsedStreamInfo_P1->pHalImage_P1_Lcso.get() ?
                pContext->queryImageStreamPool(LcsoId) : NULL;
            cfgParam.pStreamPool_rsso    = pParsedStreamInfo_P1->pHalImage_P1_Rsso.get() ?
                pContext->queryImageStreamPool(RssoId) : NULL;
            cfgParam.pStreamPool_full    = pParsedStreamInfo_P1->pHalImage_P1_Imgo.get() ?
                pContext->queryImageStreamPool(ImgoId) : NULL;
        }
        MBOOL needLMV = (pParsedAppImageStreamInfo->hasVideoConsumer || pStreamingFeatureSetting->bNeedLMV ) &&
                        !pParsedAppConfiguration->isConstrainedHighSpeedMode;
        cfgParam.enableEIS = (needLMV && idx == 0) ? MTRUE : MFALSE;
        MY_LOGD("enableEIS:%d", cfgParam.enableEIS);
        cfgParam.packedEisInfo = const_cast<NSCam::EIS::EisInfo*>(&pStreamingFeatureSetting->eisInfo)->toPackedData();
        // cfgParam.eisMode = mParams->mEISMode;
        //
        if ( pParsedAppImageStreamInfo->hasVideo4K )
            cfgParam.receiveMode = NodeT::REV_MODE::REV_MODE_CONSERVATIVE;
        // config initframe
        if (pCommon->bIsReConfigure)
        {
            MY_LOGD("Is Reconfig flow, force init request = 0");
            initRequest = 0;
        }
        else if (initRequest == 0)
        {
            if(IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_CONFIGURE_SETTING_INIT_REQUEST, initRequest))
            {
                MY_LOGD("APP set init frame : %d, if not be zero, force it to be 4", initRequest);
                if (initRequest != 0)
                {
                    initRequest = 4; // force 4
                }
            }
        }
        cfgParam.initRequest = initRequest;
        //
        cfgParam.burstNum = batchSize;
        //
        std::shared_ptr<NSCamHW::HwInfoHelper> infohelper = std::make_shared<NSCamHW::HwInfoHelper>(physicalSensorId);
        bool ret = infohelper != nullptr && infohelper->updateInfos();
        if ( CC_UNLIKELY(!ret) ) {
            MY_LOGE("HwInfoHelper");
        }
        if ( infohelper->getDualPDAFSupported(pSensorSetting->sensorMode) ) {
            cfgParam.enableDualPD = MTRUE;
            if ( ! bMultiDevice )
            {
                cfgParam.disableFrontalBinning = MTRUE;
            }
            MY_LOGD("PDAF supported for sensor mode:%d - enableDualPD:%d disableFrontalBinning:%d",
                    pSensorSetting->sensorMode, cfgParam.enableDualPD, cfgParam.disableFrontalBinning);
        }
        // set multi-cam config info
        if(bMultiDevice)
        {
            int32_t enableSync = property_get_int32("vendor.multicam.sync.enable", 1);
            cfgParam.pSyncHelper = (enableSync) ? pContext->getMultiCamSyncHelper() : nullptr;
            cfgParam.enableFrameSync = MTRUE;
            cfgParam.tgNum = 2;
            // for multi-cam case, it has to set quality high.
            // Only ISP 5.0 will check this value.
            cfgParam.resizeQuality = NSCam::v3::P1Node::RESIZE_QUALITY_H;
            if(pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::Feature)
            {
                // for vsdof, it has to check EIS.
                // If EIS on, force set EIS to main1.
                if(MTK_MULTI_CAM_FEATURE_MODE_VSDOF == pParsedDualCamInfo->mDualFeatureMode)
                {
                    if(cfgParam.enableEIS && idx == 0)
                    {
                        cfgParam.forceSetEIS = MTRUE;
                    }
                    else
                    {
                        cfgParam.forceSetEIS = MFALSE;
                    }
                }
            }
            // for dual cam case, if rrzo is null, it means camsv path.
            if(bMultiCam_CamSvPath)
            {
                IMetadata::setEntry<MBOOL>(&cfgParam.cfgHalMeta, MTK_STEREO_WITH_CAMSV, MTRUE);
            }
        }
        // config P1 for ISP 6.0
        MY_LOGE("Need to do config P1 for ISP 6.0 face detection!!!!!!!");
    }
    //
    sp<NodeActorT> pNode;
    if (isReConfig) {
        MY_LOGD("mvhdr reconfig(%d) and config P1 node only", isReConfig);
        pContext->queryNodeActor(nodeId, pNode);
        pNode->getNodeImpl()->config(cfgParam);
    }else{
        {
            sp<NodeActorT> pOldNode;
            MBOOL bHasOldNode = (pOldPipelineContext.get() && OK == pOldPipelineContext->queryNodeActor(nodeId, pOldNode));
            if(bHasOldNode)
                pOldNode->getNodeImpl()->uninit(); // must uninit old P1 before call new P1 config
            //
            pNode = new NodeActorT( NodeT::createInstance() );
        }
    }
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    // ISP 6.0
    add_stream_to_set(outStreamSet, pParsedStreamInfo_P1->pHalImage_P1_FDYuv);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    if (idx == 0)
    {
        setImageUsage(cfgParam.pInImage_yuv,        eBUFFER_USAGE_HW_CAMERA_READ);
        setImageUsage(cfgParam.pInImage_raw,        eBUFFER_USAGE_SW_READ_OFTEN);
        setImageUsage(cfgParam.pInImage_opaque,     eBUFFER_USAGE_SW_READ_OFTEN);
        setImageUsage(cfgParam.pOutImage_opaque,    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    }
    //
    for (size_t i = 0; i < cfgParam.pvOutImage_full.size(); i++) {
        setImageUsage(cfgParam.pvOutImage_full[i],  eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    }
    setImageUsage(cfgParam.pOutImage_resizer,   eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(cfgParam.pOutImage_lcso,      eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(cfgParam.pOutImage_rsso,      eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    // ISP 6.0
    setImageUsage(pParsedStreamInfo_P1->pHalImage_P1_FDYuv   , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_P2SNode(
    sp<PipelineContext> pContext,
    StreamingFeatureSetting const* pStreamingFeatureSetting,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t batchSize,
    uint32_t useP1NodeCount __unused,
    bool bHasMonoSensor __unused,
    InternalCommonInputParams const* pCommon
)
{
    typedef P2StreamingNode         NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    auto const& pPipelineStaticInfo         = pCommon->pPipelineStaticInfo;
    auto const& pPipelineUserConfiguration  = pCommon->pPipelineUserConfiguration;
    auto const& pParsedAppConfiguration     = pPipelineUserConfiguration->pParsedAppConfiguration;
    auto const& pParsedAppImageStreamInfo   = pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    auto const& pParsedDualCamInfo          = pParsedAppConfiguration->pParsedDualCamInfo;
    //
    NodeId_T const nodeId = eNODEID_P2StreamNode;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "P2StreamNode";
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pPipelineStaticInfo->sensorId[i]);
        }
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta    = pParsedStreamInfo_NonP1->pAppMeta_Control;
        cfgParam.pInAppRetMeta = (*pParsedStreamInfo_P1)[0].pAppMeta_DynamicP1;
        cfgParam.pInHalMeta    = (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1;
        cfgParam.pOutAppMeta   = pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode;
        cfgParam.pOutAppPhysicalMeta       = pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1;
        cfgParam.pOutAppPhysicalMeta_Sub   = pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2;
        cfgParam.pOutHalMeta   = pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode;
        //
        if( (*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo.get() )
            cfgParam.pvInFullRaw.push_back((*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo);
        //
        cfgParam.pInResizedRaw = (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rrzo;
        //
        cfgParam.pInLcsoRaw = (*pParsedStreamInfo_P1)[0].pHalImage_P1_Lcso;
        //
        cfgParam.pInRssoRaw = (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rsso;
        //
        if (useP1NodeCount > 1)
        {
            cfgParam.pInAppRetMeta_Sub = (*pParsedStreamInfo_P1)[1].pAppMeta_DynamicP1;
            cfgParam.pInHalMeta_Sub    = (*pParsedStreamInfo_P1)[1].pHalMeta_DynamicP1;
            if ( (*pParsedStreamInfo_P1)[1].pHalImage_P1_Imgo.get() )
            {
                cfgParam.pvInFullRaw_Sub.push_back((*pParsedStreamInfo_P1)[1].pHalImage_P1_Imgo);
            }
            //
            cfgParam.pInResizedRaw_Sub = (*pParsedStreamInfo_P1)[1].pHalImage_P1_Rrzo;
            //
            cfgParam.pInLcsoRaw_Sub = (*pParsedStreamInfo_P1)[1].pHalImage_P1_Lcso;
            //
            cfgParam.pInRssoRaw_Sub = (*pParsedStreamInfo_P1)[1].pHalImage_P1_Rsso;
        }
        //
        if( pParsedAppImageStreamInfo->pAppImage_Output_Priv.get() )
        {
            cfgParam.pvInOpaque.push_back(pParsedAppImageStreamInfo->pAppImage_Output_Priv);
        }
        //
        for( const auto& n : pParsedAppImageStreamInfo->vAppImage_Output_Proc )
        {
            cfgParam.vOutImage.push_back(n.second);
        }
        //
        if( pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV.get() )
        {
            cfgParam.vOutImage.push_back(pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);
        }
        if( pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV.get() )
        {
            cfgParam.vOutImage.push_back(pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV);
        }
        //
        if( pParsedStreamInfo_NonP1->pHalImage_FD_YUV.get() )
        {
            cfgParam.pOutFDImage = pParsedStreamInfo_NonP1->pHalImage_FD_YUV;
        }
        //
        cfgParam.burstNum = batchSize;

        if (pStreamingFeatureSetting->bSupportCZ)
        {
            cfgParam.customOption |= P2StreamingNode::CUSTOM_OPTION_CLEAR_ZOOM_SUPPORT;
        }
        if (pStreamingFeatureSetting->bSupportDRE)
        {
            cfgParam.customOption |= P2StreamingNode::CUSTOM_OPTION_DRE_SUPPORT;
        }
    }

    P2Common::UsageHint p2Usage;
    {
        p2Usage.mP2NodeType = P2Common::P2_NODE_COMMON;
        p2Usage.m3DNRMode   = pStreamingFeatureSetting->nr3dMode;
        p2Usage.mFSCMode   = pStreamingFeatureSetting->fscMode;
        p2Usage.mUseTSQ     = pStreamingFeatureSetting->bEnableTSQ;
        p2Usage.mTP = pStreamingFeatureSetting->supportedScenarioFeatures;
        p2Usage.mAppSessionMeta = pParsedAppConfiguration->sessionParams;

        p2Usage.mPackedEisInfo = const_cast<NSCam::EIS::EisInfo*>(&pStreamingFeatureSetting->eisInfo)->toPackedData();
        if( pParsedAppImageStreamInfo->hasVideoConsumer )
        {
            p2Usage.mAppMode = P2Common::APP_MODE_VIDEO;
        }
        if( pParsedAppConfiguration->operationMode == 1/* CONSTRAINED_HIGH_SPEED_MODE */ )
        {
            p2Usage.mAppMode = P2Common::APP_MODE_HIGH_SPEED_VIDEO;
        }
        if( (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rrzo != NULL )
        {
            p2Usage.mStreamingSize = (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rrzo->getImgSize();
        }
        if(useP1NodeCount > 1)
        {
            // for dual cam device, it has to config other parameter.
            // 1. set all resized raw image size
            // if rrzo stream info is nullptr, set imgo size
            MSize maxStreamingSize = MSize(0, 0);
            MSize tempSize;
            for (size_t i = 0; i < useP1NodeCount; i++)
            {
                if ( (*pParsedStreamInfo_P1)[i].pHalImage_P1_Rrzo != nullptr )
                {
                    tempSize = (*pParsedStreamInfo_P1)[i].pHalImage_P1_Rrzo->getImgSize();
                    p2Usage.mResizedRawMap.insert(
                                    std::pair<MUINT32, MSize>(
                                            pPipelineStaticInfo->sensorId[i],
                                            tempSize));
                }
                else
                {
                    MY_LOGD("rrzo stream info is null, set imgo size to resized raw map.");
                    tempSize = (*pParsedStreamInfo_P1)[i].pHalImage_P1_Imgo->getImgSize();
                    p2Usage.mResizedRawMap.insert(
                                    std::pair<MUINT32, MSize>(
                                            pPipelineStaticInfo->sensorId[i],
                                            tempSize));
                }
                if(tempSize.w > maxStreamingSize.w) maxStreamingSize.w = tempSize.w;
                if(tempSize.h > maxStreamingSize.h) maxStreamingSize.h = tempSize.h;
            }
            // for multi-cam case, it has to get max width and height for streaming size.
            p2Usage.mStreamingSize = maxStreamingSize;
            // 2. set feature mode
            p2Usage.mDualFeatureMode = StereoSettingProvider::getStereoFeatureMode();
            // 3. set sensor module mode
            p2Usage.mSensorModule = (bHasMonoSensor) ?  NSCam::v1::Stereo::BAYER_AND_MONO :  NSCam::v1::Stereo::BAYER_AND_BAYER;
            MY_LOGI("sensor module(%d) multicamFeatureMode(%d)", p2Usage.mSensorModule, p2Usage.mDualFeatureMode);
        }
        p2Usage.mOutCfg.mMaxOutNum  = cfgParam.vOutImage.size();
        for(auto&& out : cfgParam.vOutImage)
        {
            if(out->getImgSize().w > p2Usage.mStreamingSize.w || out->getImgSize().h > p2Usage.mStreamingSize.h)
            {
                p2Usage.mOutCfg.mHasLarge = MTRUE;
            }
                p2Usage.mOutCfg.mHasPhysical =
                    (pParsedDualCamInfo->mDualDevicePath == NSCam::v3::pipeline::policy::DualDevicePath::MultiCamControl);
        }

        MY_LOGI("operation_mode=%d p2_type=%d p2_node_type=%d app_mode=%d 3dnr_mode=0x%x fsc_mode=0x%x eis_mode=0x%x eis_factor=%d stream_size=%dx%d",
                pParsedAppConfiguration->operationMode, p2Usage.mP2NodeType, p2Usage.mP2NodeType, p2Usage.mAppMode,
                p2Usage.m3DNRMode, p2Usage.mFSCMode, pStreamingFeatureSetting->eisInfo.mode,
                pStreamingFeatureSetting->eisInfo.factor, p2Usage.mStreamingSize.w, p2Usage.mStreamingSize.h);
    }
    cfgParam.mUsageHint = p2Usage;

    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance(P2StreamingNode::PASS2_STREAM, p2Usage) );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pAppMeta_Control);
    for (size_t i = 0; i < useP1NodeCount; i++)
    {
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pAppMeta_DynamicP1);
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pHalMeta_DynamicP1);
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pHalImage_P1_Imgo);
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pHalImage_P1_Rrzo);
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pHalImage_P1_Lcso);
        add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[i].pHalImage_P1_Rsso);
    }
    add_stream_to_set(inStreamSet, pParsedAppImageStreamInfo->pAppImage_Output_Priv);
    //
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV);
    //
    for( const auto& n : pParsedAppImageStreamInfo->vAppImage_Output_Proc )
        add_stream_to_set(outStreamSet, n.second);
    //
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pHalImage_FD_YUV);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(pParsedAppImageStreamInfo->pAppImage_Output_Priv, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    for (size_t i = 0; i < useP1NodeCount; i++)
    {
        setImageUsage((*pParsedStreamInfo_P1)[i].pHalImage_P1_Imgo, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        setImageUsage((*pParsedStreamInfo_P1)[i].pHalImage_P1_Rrzo, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        setImageUsage((*pParsedStreamInfo_P1)[i].pHalImage_P1_Lcso, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
        setImageUsage((*pParsedStreamInfo_P1)[i].pHalImage_P1_Rsso, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    }
    //
    for( const auto& n : pParsedAppImageStreamInfo->vAppImage_Output_Proc )
        setImageUsage(n.second, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    setImageUsage(pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV     , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(pParsedStreamInfo_NonP1->pHalImage_FD_YUV       , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_P2CNode(
    sp<PipelineContext> pContext,
    CaptureFeatureSetting const* pCaptureFeatureSetting,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t useP1NodeCount,
    InternalCommonInputParams const* pCommon
)
{
    typedef P2CaptureNode           NodeT;
    typedef NodeActor< NodeT >      NodeActorT;

    auto const& pPipelineStaticInfo         = pCommon->pPipelineStaticInfo;
    auto const& pPipelineUserConfiguration  = pCommon->pPipelineUserConfiguration;
    auto const& pParsedAppImageStreamInfo   = pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    auto const& pParsedAppConfiguration     = pPipelineUserConfiguration->pParsedAppConfiguration;
    MSize postviewSize(0,0);
    //
    NodeId_T const nodeId = eNODEID_P2CaptureNode;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "P2CaptureNode";
        // according p1 node count to add open id.
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pPipelineStaticInfo->sensorId[i]);
        }
    }
    IMetadata::IEntry PVentry = pParsedAppConfiguration->sessionParams.entryFor(MTK_CONTROL_CAPTURE_POSTVIEW_SIZE);
    if( !PVentry.isEmpty() )
    {
        postviewSize = PVentry.itemAt(0, Type2Type<MSize>());
        MY_LOGD("AP set post view size : %dx%d", postviewSize.w, postviewSize.h);
    }
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    NodeT::ConfigParams cfgParam;
    {
        add_meta_stream (outStreamSet, cfgParam.pOutAppMeta,    pParsedStreamInfo_NonP1->pAppMeta_DynamicP2CaptureNode);
        add_meta_stream (outStreamSet, cfgParam.pOutHalMeta,    pParsedStreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode);
        add_meta_stream ( inStreamSet, cfgParam.pInAppMeta,     pParsedStreamInfo_NonP1->pAppMeta_Control);

        add_meta_stream ( inStreamSet, cfgParam.pInAppRetMeta,  (*pParsedStreamInfo_P1)[0].pAppMeta_DynamicP1);
        add_meta_stream ( inStreamSet, cfgParam.pInHalMeta,     (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1);

        if (pParsedAppImageStreamInfo->pAppImage_Input_RAW16 != nullptr) {
            MY_LOGF_IF((*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo!=nullptr,
                "P2CaptureNode doesn't support 2 full-size inputs (App:RAW16 & Hal:Imgo)");
            add_image_stream( inStreamSet, cfgParam.pInFullRaw, pParsedAppImageStreamInfo->pAppImage_Input_RAW16);
        }
        else {
            add_image_stream( inStreamSet, cfgParam.pInFullRaw, (*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo);
        }
        add_image_stream( inStreamSet, cfgParam.pInResizedRaw,  (*pParsedStreamInfo_P1)[0].pHalImage_P1_Rrzo);
        add_image_stream( inStreamSet, cfgParam.pInLcsoRaw,     (*pParsedStreamInfo_P1)[0].pHalImage_P1_Lcso);
        //
        #if 1 // capture node not support main2 yet
        if (useP1NodeCount > 1) // more than 1 p1node needs to add additional info.
        {
            add_meta_stream ( inStreamSet, cfgParam.pInAppRetMeta2, (*pParsedStreamInfo_P1)[1].pAppMeta_DynamicP1);
            add_meta_stream ( inStreamSet, cfgParam.pInHalMeta2,    (*pParsedStreamInfo_P1)[1].pHalMeta_DynamicP1);

            add_image_stream( inStreamSet, cfgParam.pInFullRaw2,    (*pParsedStreamInfo_P1)[1].pHalImage_P1_Imgo);
            add_image_stream( inStreamSet, cfgParam.pInResizedRaw2, (*pParsedStreamInfo_P1)[1].pHalImage_P1_Rrzo);
            add_image_stream( inStreamSet, cfgParam.pInLcsoRaw2,    (*pParsedStreamInfo_P1)[1].pHalImage_P1_Lcso);
        }
        #endif
        //
        add_image_stream( inStreamSet, cfgParam.pInFullYuv,     pParsedAppImageStreamInfo->pAppImage_Input_Yuv);
        add_image_stream( inStreamSet, cfgParam.vpInOpaqueRaws, pParsedAppImageStreamInfo->pAppImage_Input_Priv);
        add_image_stream( inStreamSet, cfgParam.vpInOpaqueRaws, pParsedAppImageStreamInfo->pAppImage_Output_Priv);
        //
        cfgParam.pOutPostViewYuv = nullptr;
        for( const auto& n : pParsedAppImageStreamInfo->vAppImage_Output_Proc )
        {
            if (postviewSize == n.second->getImgSize() && cfgParam.pOutPostViewYuv == nullptr)
            {
                add_image_stream(outStreamSet, cfgParam.pOutPostViewYuv, n.second);
            }
            else
            {
                add_image_stream(outStreamSet, cfgParam.vpOutImages, n.second);
            }
        }
        //
        add_image_stream(outStreamSet, cfgParam.pOutJpegYuv,        pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);
        add_image_stream(outStreamSet, cfgParam.pOutThumbnailYuv,   pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV);
    }
    //
    MBOOL isSupportedBGPreRelease = MFALSE;
    IMetadata::IEntry BGPentry = pParsedAppConfiguration->sessionParams.entryFor(MTK_BGSERVICE_FEATURE_PRERELEASE);
    if( !BGPentry.isEmpty() )
    {
        isSupportedBGPreRelease = (BGPentry.itemAt(0, Type2Type<MINT32>()) == MTK_BGSERVICE_FEATURE_PRERELEASE_MODE_ON);
        MY_LOGD("Is supported background preRelease : %d", isSupportedBGPreRelease);
    }
    //
    P2Common::Capture::UsageHint p2Usage;
    {
        p2Usage.mSupportedScenarioFeatures = pCaptureFeatureSetting->supportedScenarioFeatures;
        p2Usage.mIsSupportedBGPreRelease = isSupportedBGPreRelease;
    }
    cfgParam.mUsageHint = p2Usage;
    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance(P2CaptureNode::PASS2_TIMESHARING, p2Usage) );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(cfgParam.pInFullYuv  , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    for (size_t i = 0; i < cfgParam.vpInOpaqueRaws.size(); i++) {
        setImageUsage(cfgParam.vpInOpaqueRaws[i], eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    }
    //
    setImageUsage(cfgParam.pInFullRaw,    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(cfgParam.pInResizedRaw, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(cfgParam.pInLcsoRaw,    eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);

    setImageUsage(cfgParam.pInFullRaw2,   eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(cfgParam.pInResizedRaw2,eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(cfgParam.pInLcsoRaw2,   eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    //
    setImageUsage(cfgParam.pOutPostViewYuv, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    for (size_t i = 0; i < cfgParam.vpOutImages.size(); i++) {
        setImageUsage(cfgParam.vpOutImages[i], eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    }
    //
    setImageUsage(cfgParam.pOutJpegYuv,      eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    setImageUsage(cfgParam.pOutThumbnailYuv, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_FdNode(
    sp<PipelineContext> pContext,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t useP1NodeCount,
    InternalCommonInputParams const* pCommon
)
{
    typedef FdNode                  NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_FDNode;
    auto const& pPipelineStaticInfo = pCommon->pPipelineStaticInfo;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pCommon->pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "FDNode";
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pCommon->pPipelineStaticInfo->sensorId[i]);
        }
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta    = pParsedStreamInfo_NonP1->pAppMeta_Control;
        cfgParam.pOutAppMeta   = pParsedStreamInfo_NonP1->pAppMeta_DynamicFD;
        if ( pPipelineStaticInfo->isP1DirectFDYUV )
        {
            cfgParam.pInHalMeta    = ((*pParsedStreamInfo_P1)[0]).pHalMeta_DynamicP1;
            cfgParam.vInImage      = ((*pParsedStreamInfo_P1)[0]).pHalImage_P1_FDYuv;
        }
        else
        {
            cfgParam.pInHalMeta    = pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode;
            cfgParam.vInImage      = pParsedStreamInfo_NonP1->pHalImage_FD_YUV;
        }
    }
    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance() );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, cfgParam.pInAppMeta);
    add_stream_to_set(inStreamSet, cfgParam.pInHalMeta);
    add_stream_to_set(inStreamSet, cfgParam.vInImage);
    //
    add_stream_to_set(outStreamSet, cfgParam.pOutAppMeta);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(cfgParam.vInImage , eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_JpegNode(
    sp<PipelineContext> pContext,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t useP1NodeCount,
    InternalCommonInputParams const* pCommon
)
{
    typedef JpegNode                NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    auto const& pParsedAppImageStreamInfo = pCommon->pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    auto const& pParsedAppConfiguration   = pCommon->pPipelineUserConfiguration->pParsedAppConfiguration;
    //
    NodeId_T const nodeId = eNODEID_JpegNode;
    //
    MBOOL isSupportedBGPrerelease = MFALSE;
    IMetadata::IEntry BGPentry = pParsedAppConfiguration->sessionParams.entryFor(MTK_BGSERVICE_FEATURE_PRERELEASE);
    if( !BGPentry.isEmpty() )
    {
        isSupportedBGPrerelease = (BGPentry.itemAt(0, Type2Type<MINT32>()) == MTK_BGSERVICE_FEATURE_PRERELEASE_MODE_ON);
        MY_LOGD("Is supported background prerelease : %d", isSupportedBGPrerelease);
    }
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pCommon->pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "JpegNode";
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pCommon->pPipelineStaticInfo->sensorId[i]);
        }
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta        = pParsedStreamInfo_NonP1->pAppMeta_Control;
        cfgParam.pInHalMeta_capture        = pParsedStreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode;
        cfgParam.pInHalMeta_streaming       = pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode;
        cfgParam.pOutAppMeta       = pParsedStreamInfo_NonP1->pAppMeta_DynamicJpeg;
        cfgParam.pInYuv_Main       = pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV;
        cfgParam.pInYuv_Thumbnail  = pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV;
        cfgParam.pOutJpeg          = pParsedAppImageStreamInfo->pAppImage_Jpeg;
        cfgParam.bIsPreReleaseEnable = isSupportedBGPrerelease;
    }
    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance() );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pAppMeta_Control);
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode);
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pHalMeta_DynamicP2StreamNode);
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV);
    add_stream_to_set(inStreamSet, pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV);
    //
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pAppMeta_DynamicJpeg);
    add_stream_to_set(outStreamSet, pParsedAppImageStreamInfo->pAppImage_Jpeg);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage(pParsedStreamInfo_NonP1->pHalImage_Jpeg_YUV, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(pParsedStreamInfo_NonP1->pHalImage_Thumbnail_YUV, eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ);
    setImageUsage(pParsedAppImageStreamInfo->pAppImage_Jpeg, eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_WRITE);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_Raw16Node(
    sp<PipelineContext> pContext,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t useP1NodeCount,
    InternalCommonInputParams const* pCommon
)
{
    typedef RAW16Node               NodeT;
    typedef NodeActor< NodeT >      NodeActorT;
    auto const& pParsedAppImageStreamInfo = pCommon->pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    //
    NodeId_T const nodeId = eNODEID_RAW16;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pCommon->pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "Raw16Node";
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pCommon->pPipelineStaticInfo->sensorId[i]);
        }
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInAppMeta = (*pParsedStreamInfo_P1)[0].pAppMeta_DynamicP1;
        cfgParam.pInHalMeta = (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1;
        cfgParam.pOutAppMeta  = pParsedStreamInfo_NonP1->pAppMeta_DynamicRAW16;
    }
    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance() );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo);
    add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1);
    add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[0].pAppMeta_DynamicP1);
    //
    add_stream_to_set(outStreamSet, pParsedAppImageStreamInfo->pAppImage_Output_RAW16);
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pAppMeta_DynamicRAW16);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage((*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo, eBUFFER_USAGE_SW_READ_OFTEN);
    setImageUsage(pParsedAppImageStreamInfo->pAppImage_Output_RAW16, eBUFFER_USAGE_SW_WRITE_OFTEN);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_PDENode(
    sp<PipelineContext> pContext,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    uint32_t useP1NodeCount,
    InternalCommonInputParams const* pCommon
)
{
    typedef PDENode               NodeT;
    typedef NodeActor< NodeT >    NodeActorT;
    //
    NodeId_T const nodeId = eNODEID_PDENode;
    //
    NodeT::InitParams initParam;
    {
        initParam.openId = pCommon->pPipelineStaticInfo->sensorId[0];
        initParam.nodeId = nodeId;
        initParam.nodeName = "PDENode";
        for (size_t i = 1; i < useP1NodeCount; i++)
        {
            initParam.subOpenIdList.push_back(pCommon->pPipelineStaticInfo->sensorId[i]);
        }
    }
    NodeT::ConfigParams cfgParam;
    {
        cfgParam.pInP1HalMeta = (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1;
        cfgParam.pOutHalMeta  = pParsedStreamInfo_NonP1->pHalMeta_DynamicPDE;
        cfgParam.pInRawImage  = (*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo;
    }
    //
    sp<NodeActorT> pNode = new NodeActorT( NodeT::createInstance() );
    pNode->setInitParam(initParam);
    pNode->setConfigParam(cfgParam);
    //
    StreamSet inStreamSet;
    StreamSet outStreamSet;
    //
    add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo);
    add_stream_to_set(inStreamSet, (*pParsedStreamInfo_P1)[0].pHalMeta_DynamicP1);
    //
    add_stream_to_set(outStreamSet, pParsedStreamInfo_NonP1->pHalMeta_DynamicPDE);
    //
    NodeBuilder builder(nodeId, pNode);
    builder
        .addStream(NodeBuilder::eDirection_IN, inStreamSet)
        .addStream(NodeBuilder::eDirection_OUT, outStreamSet);
    //
    setImageUsage((*pParsedStreamInfo_P1)[0].pHalImage_P1_Imgo, eBUFFER_USAGE_SW_READ_OFTEN);
    //
    int err = builder.build(pContext);
    if( err != OK )
        MY_LOGE("build node %#" PRIxPTR " failed", nodeId);
    return err;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_Nodes(
    sp<PipelineContext> pContext,
    android::sp<PipelineContext> const& pOldPipelineContext,
    StreamingFeatureSetting const* pStreamingFeatureSetting,
    CaptureFeatureSetting const* pCaptureFeatureSetting,
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1,
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1,
    PipelineNodesNeed const* pPipelineNodesNeed,
    std::vector<SensorSetting> const* pSensorSetting,
    std::vector<P1HwSetting> const* pvP1HwSetting,
    uint32_t batchSize,
    InternalCommonInputParams const* pCommon
)
{
    CAM_TRACE_CALL();

    auto const& pPipelineStaticInfo = pCommon->pPipelineStaticInfo;

    uint32_t useP1NodeCount = 0;
    bool bMultiCam_CamSvPath = false;
    bool isReConfig          = false;
    for( const auto n : pPipelineNodesNeed->needP1Node ) {
        if (n) useP1NodeCount++;
    }

    // if useP1NodeCount more than 1, it has to create sync helper
    // and assign to p1 config param.
    if(useP1NodeCount > 1)
    {
        // 1. set sync helper.
        typedef NSCam::v3::Utils::Imp::ISyncHelper  MultiCamSyncHelper;
        sp<MultiCamSyncHelper> pSyncHelper = MultiCamSyncHelper::createInstance();
        if(pSyncHelper.get())
        {
            pContext->setMultiCamSyncHelper(pSyncHelper);
        }
        // 2. check may use camsv flow or not.
        for(auto& p1_streamInfo:(*pParsedStreamInfo_P1))
        {
            if(p1_streamInfo.pHalImage_P1_Rrzo == nullptr)
            {
                bMultiCam_CamSvPath = true;
            }
        }
    }

    for(size_t i = 0; i < pPipelineNodesNeed->needP1Node.size(); i++) {
        if (pPipelineNodesNeed->needP1Node[i]) {
            CHECK_ERROR( configContextLocked_P1Node(
                            pContext,
                            pOldPipelineContext,
                            pStreamingFeatureSetting,
                            pPipelineNodesNeed,
                            &(*pParsedStreamInfo_P1)[i],
                            pParsedStreamInfo_NonP1,
                            &(*pSensorSetting)[i],
                            &(*pvP1HwSetting)[i],
                            i,
                            batchSize,
                            useP1NodeCount > 1,
                            bMultiCam_CamSvPath,
                            pCommon,
                            isReConfig),
                      "\nconfigContextLocked_P1Node");
        }
    }
    if( pPipelineNodesNeed->needP2StreamNode ) {
        bool hasMonoSensor = false;
        for(auto const v : pPipelineStaticInfo->sensorRawType) {
            if(SENSOR_RAW_MONO == v) {
                hasMonoSensor = true;
                break;
            }
        }
        CHECK_ERROR( configContextLocked_P2SNode(
                            pContext,
                            pStreamingFeatureSetting,
                            pParsedStreamInfo_P1,
                            pParsedStreamInfo_NonP1,
                            batchSize,
                            useP1NodeCount,
                            hasMonoSensor,
                            pCommon),
                      "\nconfigContextLocked_P2SNode");
    }
    if( pPipelineNodesNeed->needP2CaptureNode ) {
        CHECK_ERROR( configContextLocked_P2CNode(
                            pContext,
                            pCaptureFeatureSetting,
                            pParsedStreamInfo_P1,
                            pParsedStreamInfo_NonP1,
                            useP1NodeCount,
                            pCommon),
                      "\nconfigContextLocked_P2CNode");
    }
    if( pPipelineNodesNeed->needFDNode ) {
        CHECK_ERROR( configContextLocked_FdNode(
                            pContext,
                            pParsedStreamInfo_P1,
                            pParsedStreamInfo_NonP1,
                            useP1NodeCount,
                            pCommon),
                      "\nconfigContextLocked_FdNode");
    }
    if( pPipelineNodesNeed->needJpegNode ) {
        CHECK_ERROR( configContextLocked_JpegNode(
                            pContext,
                            pParsedStreamInfo_NonP1,
                            useP1NodeCount,
                            pCommon),
                      "\nconfigContextLocked_JpegNode");
    }
    if( pPipelineNodesNeed->needRaw16Node ) {
        CHECK_ERROR( configContextLocked_Raw16Node(
                            pContext,
                            pParsedStreamInfo_P1,
                            pParsedStreamInfo_NonP1,
                            useP1NodeCount,
                            pCommon),
                      "\nconfigContextLocked_Raw16Node");
    }
    if( pPipelineNodesNeed->needPDENode ) {
        CHECK_ERROR( configContextLocked_PDENode(
                            pContext,
                            pParsedStreamInfo_P1,
                            pParsedStreamInfo_NonP1,
                            useP1NodeCount,
                            pCommon),
                     "\nconfigContextLocked_PDENode");
    }
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
int
configContextLocked_Pipeline(
    sp<PipelineContext> pContext,
    PipelineTopology const* pPipelineTopology
)
{
    CAM_TRACE_CALL();
    //
    CHECK_ERROR(
            PipelineBuilder()
            .setRootNode(pPipelineTopology->roots)
            .setNodeEdges(pPipelineTopology->edges)
            .build(pContext),
            "\nPipelineBuilder.build(pContext)");
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace model {
auto buildPipelineContext(
    android::sp<PipelineContext>& out,
    BuildPipelineContextInputParams const& in
) -> int
{
    InternalCommonInputParams const common{
        .pPipelineStaticInfo        = in.pPipelineStaticInfo,
        .pPipelineUserConfiguration = in.pPipelineUserConfiguration,
        .pP1NodeResourceConcurrency = in.pP1NodeResourceConcurrency,
        .bIsReConfigure             = in.bIsReconfigure,
    };

    //
    if ( in.pOldPipelineContext.get() ) {
        MY_LOGD("old PipelineContext - getStrongCount:%d", in.pOldPipelineContext->getStrongCount());
        CHECK_ERROR(in.pOldPipelineContext->waitUntilNodeDrained(
                        0x01),
                    "\npipelineContext->waitUntilNodeDrained");
    }

    //
    android::sp<PipelineContext> pNewPipelineContext = PipelineContext::create(in.pipelineName.c_str());
    CHECK_ERROR(pNewPipelineContext->beginConfigure(
                    in.pOldPipelineContext),
                "\npipelineContext->beginConfigure");

    // config Streams
    CHECK_ERROR(configContextLocked_Streams(
                    pNewPipelineContext,
                    in.pParsedStreamInfo_P1,
                    in.pZSLProvider,
                    in.pParsedStreamInfo_NonP1,
                    &common),
                "\nconfigContextLocked_Streams");

    // config Nodes
    CHECK_ERROR(configContextLocked_Nodes(
                    pNewPipelineContext,
                    in.pOldPipelineContext,
                    in.pStreamingFeatureSetting,
                    in.pCaptureFeatureSetting,
                    in.pParsedStreamInfo_P1,
                    in.pParsedStreamInfo_NonP1,
                    in.pPipelineNodesNeed,
                    in.pSensorSetting,
                    in.pvP1HwSetting,
                    in.batchSize,
                    &common),
                "\nconfigContextLocked_Nodes");

    // config pipeline
    CHECK_ERROR(configContextLocked_Pipeline(
                    pNewPipelineContext,
                    in.pPipelineTopology),
                "\npipelineContext->configContextLocked_Pipeline");
    //
    CHECK_ERROR(pNewPipelineContext->setDataCallback(
                    in.pDataCallback),
                "\npipelineContext->setDataCallback");
    //
    {
        static constexpr char key[] = "vendor.debug.camera.pipelinecontext.build.multithread";
        int32_t bUsingMultiThreadToBuildPipelineContext = ::property_get_int32(key, -1);
        if (CC_UNLIKELY(-1 != bUsingMultiThreadToBuildPipelineContext)) {
            MY_LOGD("%s=%d", key, bUsingMultiThreadToBuildPipelineContext);
        }
        else {
            bUsingMultiThreadToBuildPipelineContext = in.bUsingMultiThreadToBuildPipelineContext;
        }
        CHECK_ERROR(pNewPipelineContext->endConfigure(
                        bUsingMultiThreadToBuildPipelineContext),
                    "\npipelineContext->endConfigure");
    }

    out = pNewPipelineContext;
    return OK;
}

auto reconfigureP1ForPipelineContext(
    android::sp<PipelineContext>& pContext,
    BuildPipelineContextInputParams const& in
) -> int
{
    InternalCommonInputParams const common{
        .pPipelineStaticInfo        = in.pPipelineStaticInfo,
        .pPipelineUserConfiguration = in.pPipelineUserConfiguration,
        .pP1NodeResourceConcurrency = in.pP1NodeResourceConcurrency,
        .bIsReConfigure             = in.bIsReconfigure,
    };


    android::sp<PipelineContext> const& pOldPipelineContext      = in.pOldPipelineContext;
    StreamingFeatureSetting const* pStreamingFeatureSetting      = in.pStreamingFeatureSetting;
    std::vector<ParsedStreamInfo_P1> const* pParsedStreamInfo_P1 = in.pParsedStreamInfo_P1;
    ParsedStreamInfo_NonP1 const* pParsedStreamInfo_NonP1        = in.pParsedStreamInfo_NonP1;
    PipelineNodesNeed const* pPipelineNodesNeed                  = in.pPipelineNodesNeed;
    std::vector<SensorSetting> const* pSensorSetting             = in.pSensorSetting;
    std::vector<P1HwSetting> const* pvP1HwSetting                = in.pvP1HwSetting;
    uint32_t batchSize                                           = in.batchSize;
    bool bMultiCam_CamSvPath                                     = false;
    InternalCommonInputParams const* pCommon                     = &common;
    bool isReConfig                                              = true;

    uint32_t useP1NodeCount = 0;
    for( const auto n : pPipelineNodesNeed->needP1Node ) {
        if (n) useP1NodeCount++;
    }

    for(size_t i = 0; i < pPipelineNodesNeed->needP1Node.size(); i++) {
        if (pPipelineNodesNeed->needP1Node[i]) {
            CHECK_ERROR( configContextLocked_P1Node(
                            pContext,
                            pOldPipelineContext,
                            pStreamingFeatureSetting,
                            pPipelineNodesNeed,
                            &(*pParsedStreamInfo_P1)[i],
                            pParsedStreamInfo_NonP1,
                            &(*pSensorSetting)[i],
                            &(*pvP1HwSetting)[i],
                            i,
                            batchSize,
                            useP1NodeCount > 1,
                            bMultiCam_CamSvPath,
                            pCommon,
                            isReConfig),
                      "\nconfigContextLocked_P1Node");
        }
    }
    MY_LOGD("mvhdr reconfig: config P1 only/ needP1Node_size(%d)", useP1NodeCount);
    return OK;
}
};  //namespace model
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

