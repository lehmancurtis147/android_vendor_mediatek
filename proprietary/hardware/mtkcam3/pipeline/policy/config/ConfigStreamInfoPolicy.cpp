/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-ConfigStreamInfoPolicy"

#include <mtkcam3/pipeline/policy/IConfigStreamInfoPolicy.h>
#include "ConfigStreamInfoPolicy.h"
//
#include <array>
//
#include <mtkcam3/pipeline/hwnode/StreamId.h>
#include <mtkcam/aaa/IIspMgr.h>
//
#include "MyUtils.h"


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {


/******************************************************************************
 *
 ******************************************************************************/
static std::string getP1StreamNamePostfix(size_t i)
{
    switch (i)
    {
    case 0: return std::string("main1");
    case 1: return std::string("main2");
    default:
        break;
    }
    return std::string("na");
}


/******************************************************************************
 *
 ******************************************************************************/
static constexpr StreamId_T getP1StreamId(size_t i, std::array<StreamId_T, 2>const& a)
{
    if (CC_UNLIKELY(i >= a.size())) {
        MY_LOGE("not support p1 node number large than 2");
        return eSTREAMID_NO_STREAM;
    }
    return a[i];
}


#define GETP1STREAMID(_type_, ...) \
    static constexpr StreamId_T getStreamId_##_type_(size_t i) { return getP1StreamId(i, { __VA_ARGS__ }); }

GETP1STREAMID(P1AppMetaDynamic,
    eSTREAMID_META_APP_DYNAMIC_01,
    eSTREAMID_META_APP_DYNAMIC_01_MAIN2
)

GETP1STREAMID(P1HalMetaDynamic,
    eSTREAMID_META_PIPE_DYNAMIC_01,
    eSTREAMID_META_PIPE_DYNAMIC_01_MAIN2
)

GETP1STREAMID(P1HalMetaControl,
    eSTREAMID_META_PIPE_CONTROL,
    eSTREAMID_META_PIPE_CONTROL_MAIN2
)

GETP1STREAMID(P1Imgo,
    eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_00,
    eSTREAMID_IMAGE_PIPE_RAW_OPAQUE_01
)

GETP1STREAMID(P1Rrzo,
    eSTREAMID_IMAGE_PIPE_RAW_RESIZER_00,
    eSTREAMID_IMAGE_PIPE_RAW_RESIZER_01
)

GETP1STREAMID(P1Lcso,
    eSTREAMID_IMAGE_PIPE_RAW_LCSO_00,
    eSTREAMID_IMAGE_PIPE_RAW_LCSO_01
)

GETP1STREAMID(P1Rsso,
    eSTREAMID_IMAGE_PIPE_RAW_RSSO_00,
    eSTREAMID_IMAGE_PIPE_RAW_RSSO_01
)


/******************************************************************************
 *
 ******************************************************************************/
auto P1MetaStreamInfoBuilder::setP1AppMetaDynamic_Default() -> P1MetaStreamInfoBuilder&
{
    setStreamName(std::string("App:Meta:DynamicP1_")+getP1StreamNamePostfix(mIndex));
    setStreamId(getStreamId_P1AppMetaDynamic(mIndex));
    setStreamType(eSTREAMTYPE_META_OUT);
    setMaxBufNum(10);
    setMinInitBufNum(1);

    return *this;
}


auto P1MetaStreamInfoBuilder::setP1HalMetaDynamic_Default() -> P1MetaStreamInfoBuilder&
{
    setStreamName(std::string("Hal:Meta:P1:Dynamic_")+getP1StreamNamePostfix(mIndex));
    setStreamId(getStreamId_P1HalMetaDynamic(mIndex));
    setStreamType(eSTREAMTYPE_META_INOUT);
    setMaxBufNum(10);
    setMinInitBufNum(1);

    return *this;
}


auto P1MetaStreamInfoBuilder::setP1HalMetaControl_Default() -> P1MetaStreamInfoBuilder&
{
    setStreamName(std::string("Hal:Meta:Control_")+getP1StreamNamePostfix(mIndex));
    setStreamId(getStreamId_P1HalMetaControl(mIndex));
    setStreamType(eSTREAMTYPE_META_IN);
    setMaxBufNum(10);
    setMinInitBufNum(1);

    return *this;
}


/******************************************************************************
 *
 ******************************************************************************/
auto P1ImageStreamInfoBuilder::setP1Imgo_Default(
    size_t maxBufNum,
    P1HwSetting const& rP1HwSetting
) -> P1ImageStreamInfoBuilder&
{
    auto const& setting = rP1HwSetting.imgoDefaultRequest;
    MINT const imgFormat = setting.format;
    MSize const& imgSize = setting.imageSize;
    IImageStreamInfo::BufPlanes_t defaultBufPlanes;
    bool ret = mHwInfoHelper->getDefaultBufPlanes_Imgo(defaultBufPlanes, imgFormat, imgSize);
    MY_LOGE_IF(!ret, "IHwInfoHelper::getDefaultBufPlanes_Imgo");

    setStreamName((std::string("Hal:Image:P1:Fullraw_")+getP1StreamNamePostfix(mIndex)));
    setStreamId(getStreamId_P1Imgo(mIndex));
    setStreamType(eSTREAMTYPE_IMAGE_INOUT);
    setMaxBufNum(maxBufNum);
    setMinInitBufNum(0);
    setUsageForAllocator(0);
    setAllocImgFormat(imgFormat);
    setAllocBufPlanes(defaultBufPlanes);

    setImgFormat(imgFormat);
    setImgSize(imgSize);
    setBufOffset({0});
    setBufPlanes(defaultBufPlanes);
    return *this;
}


auto P1ImageStreamInfoBuilder::setP1Rrzo_Default(
    size_t maxBufNum,
    P1HwSetting const& rP1HwSetting
) -> P1ImageStreamInfoBuilder&
{
    auto const& setting = rP1HwSetting.rrzoDefaultRequest;
    MINT const imgFormat = setting.format;
    MSize const& imgSize = setting.imageSize;
    IImageStreamInfo::BufPlanes_t defaultBufPlanes;
    bool ret = mHwInfoHelper->getDefaultBufPlanes_Rrzo(defaultBufPlanes, imgFormat, imgSize);
    MY_LOGE_IF(!ret, "IHwInfoHelper::getDefaultBufPlanes_Rrzo");

    setStreamName((std::string("Hal:Image:P1:Resizeraw_")+getP1StreamNamePostfix(mIndex)));
    setStreamId(getStreamId_P1Rrzo(mIndex));
    setStreamType(eSTREAMTYPE_IMAGE_INOUT);
    setMaxBufNum(maxBufNum);
    setMinInitBufNum(0);
    setUsageForAllocator(0);
    setAllocImgFormat(imgFormat);
    setAllocBufPlanes(defaultBufPlanes);

    setImgFormat(imgFormat);
    setImgSize(imgSize);
    setBufOffset({0});
    setBufPlanes(defaultBufPlanes);
    return *this;
}


auto P1ImageStreamInfoBuilder::setP1Lcso_Default(
    size_t maxBufNum
) -> P1ImageStreamInfoBuilder&
{
    // p1: lcso size
    MUINT const usage = 0;
    NS3Av3::LCSO_Param lcsoParam;
    if ( auto ispMgr = MAKE_IspMgr() ) {
        ispMgr->queryLCSOParams(lcsoParam);
    }
    else {
        MY_LOGE("Query IIspMgr FAILED!");
    }

    setStreamName((std::string("Hal:Image:LCSraw_")+getP1StreamNamePostfix(mIndex)));
    setStreamId(getStreamId_P1Lcso(mIndex));
    setStreamType(eSTREAMTYPE_IMAGE_INOUT);
    setMaxBufNum(maxBufNum);
    setMinInitBufNum(1);
    setUsageForAllocator(usage);
    setAllocImgFormat(lcsoParam.format);
    setAllocBufPlanes(toBufPlanes(lcsoParam.stride, lcsoParam.format, lcsoParam.size));

    ImageBufferInfo imageBufferInfo;
    imageBufferInfo.bufOffset.push_back(0);
    imageBufferInfo.bufPlanes = toBufPlanes(lcsoParam.stride, lcsoParam.format, lcsoParam.size);
    imageBufferInfo.imgFormat = lcsoParam.format;
    imageBufferInfo.imgSize   = lcsoParam.size;
    setImageBufferInfo(imageBufferInfo);
    return *this;
}


auto P1ImageStreamInfoBuilder::setP1Rsso_Default(
    size_t maxBufNum,
    P1HwSetting const& rP1HwSetting
) -> P1ImageStreamInfoBuilder&
{
    MINT imgFormat = eImgFmt_STA_BYTE;
    MSize const& imgSize = rP1HwSetting.rssoSize;
    size_t const stride  = imgSize.w;

    setStreamName((std::string("Hal:Image:RSSO_")+getP1StreamNamePostfix(mIndex)));
    setStreamId(getStreamId_P1Rsso(mIndex));
    setStreamType(eSTREAMTYPE_IMAGE_INOUT);
    setMaxBufNum(maxBufNum);
    setMinInitBufNum(1);
    setUsageForAllocator(eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE);
    setAllocImgFormat(imgFormat);
    setAllocBufPlanes(toBufPlanes(stride, imgFormat, imgSize));

    ImageBufferInfo imageBufferInfo;
    imageBufferInfo.bufOffset.push_back(0);
    imageBufferInfo.bufPlanes = toBufPlanes(stride, imgFormat, imgSize);
    imageBufferInfo.imgFormat = imgFormat;
    imageBufferInfo.imgSize   = imgSize;
    setImageBufferInfo(imageBufferInfo);
    return *this;
}

auto P1ImageStreamInfoBuilder::setP1FDYuv_Default(
    size_t maxBufNum,
    P1HwSetting const& rP1HwSetting
) -> P1ImageStreamInfoBuilder&
{
    MINT imgFormat = eImgFmt_YUY2;
    MSize const& imgSize = rP1HwSetting.fdyuvSize;
    size_t const stride  = imgSize.w << 1;

    setStreamName((std::string("Hal:Image:P1FDYuv_")+getP1StreamNamePostfix(mIndex)));
    setStreamId(eSTREAMID_IMAGE_FD);
    setStreamType(eSTREAMTYPE_IMAGE_INOUT);
    setMaxBufNum(maxBufNum);
    setMinInitBufNum(1);
    setUsageForAllocator(eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE);
    setAllocImgFormat(imgFormat);
    setAllocBufPlanes(toBufPlanes(stride, imgFormat, imgSize));

    ImageBufferInfo imageBufferInfo;
    imageBufferInfo.bufOffset.push_back(0);
    imageBufferInfo.bufPlanes = toBufPlanes(stride, imgFormat, imgSize);
    imageBufferInfo.imgFormat = imgFormat;
    imageBufferInfo.imgSize   = imgSize;
    setImageBufferInfo(imageBufferInfo);
    return *this;
}

auto P1ImageStreamInfoBuilder::toBufPlanes(
    size_t stride,
    MINT imgFormat,
    MSize const& imgSize
) -> IImageStreamInfo::BufPlanes_t
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
    bool ret = mHwInfoHelper->getDefaultBufPlanes_Pass1(bufPlanes, imgFormat, imgSize, stride);
    MY_LOGE_IF(!ret, "IHwInfoHelper::getDefaultBufPlanes_Pass1");
    return bufPlanes;
}


/******************************************************************************
 *
 ******************************************************************************/
static
sp<ImageStreamInfo>
createImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
#define addBufPlane(planes, height, stride)                                      \
        do{                                                                      \
            size_t _height = (size_t)(height);                                   \
            size_t _stride = (size_t)(stride);                                   \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
            planes.push_back(bufPlane);                                          \
        }while(0)
    switch( imgFormat ) {
        case eImgFmt_YV12:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            break;
        case eImgFmt_NV21:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w);
            break;
        case eImgFmt_YUY2:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w << 1);
            break;
        default:
            MY_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, 0
                );

    if( pStreamInfo == NULL ) {
        MY_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}

/******************************************************************************
 *
 ******************************************************************************/
static
sp<ImageStreamInfo>
createImageStreamInfo_YuvJpeg(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
    MSize alignSize = imgSize;
#define addBufPlane(planes, height, stride)                                      \
        do{                                                                      \
            size_t _height = (size_t)(height);                                   \
            size_t _stride = (size_t)(stride);                                   \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
            planes.push_back(bufPlane);                                          \
        }while(0)
    switch( imgFormat ) {
        case eImgFmt_YV12:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            break;
        case eImgFmt_NV21:
            // 16x16 alignment for Jpeg hw input yuv contraint
            if (imgSize.w > 0 && imgSize.h > 0) {
                alignSize.w = (imgSize.w + 15) & (~15);
                alignSize.h = (imgSize.h + 15) & (~15);
            }
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w);
            addBufPlane(bufPlanes , alignSize.h >> 1 , alignSize.w);
            break;
        case eImgFmt_YUY2:
            // 32x8 alignment for Jpeg hw input yuv contraint
            if (imgSize.w > 0 && imgSize.h > 0) {
                alignSize.w = (imgSize.w + 15) & (~15);
                alignSize.h = (imgSize.h + 7) & (~7);
            }
            MY_LOGD("Gary debug : imgSize.w : %d, imgSize.h : %d, alignSize.w : %d, alignSize.h : %d", imgSize.w, imgSize.h, alignSize.w, alignSize.h);
            addBufPlane(bufPlanes , alignSize.h      , alignSize.w << 1);
            break;
        default:
            MY_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, 0
                );

    if( pStreamInfo == NULL ) {
        MY_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}

/******************************************************************************
 *
 ******************************************************************************/
static
MVOID
evaluatePreviewSize(
    PipelineUserConfiguration const* pPipelineUserConfiguration,
    MSize &rSize
)
{
    sp<IImageStreamInfo> pStreamInfo;
    int consumer_usage = 0;
    int allocate_usage = 0;
    int maxheight      = rSize.h;
    int prevwidth      = 0;
    int prevheight     = 0;

    for( const auto& n : pPipelineUserConfiguration->pParsedAppImageStreamInfo->vAppImage_Output_Proc )
    {
        if  ( (pStreamInfo = n.second) != 0 ) {
            consumer_usage = pStreamInfo->getUsageForConsumer();
            allocate_usage = pStreamInfo->getUsageForAllocator();
            MY_LOGD("consumer : %X, allocate : %X", consumer_usage, allocate_usage);
            if(consumer_usage & GRALLOC_USAGE_HW_TEXTURE) {
                prevwidth = pStreamInfo->getImgSize().w;
                prevheight = pStreamInfo->getImgSize().h;
                break;
            }
            if(consumer_usage & GRALLOC_USAGE_HW_VIDEO_ENCODER) {
                continue;
            }
            prevwidth = pStreamInfo->getImgSize().w;
            prevheight = pStreamInfo->getImgSize().h;
        }
    }
    if(prevwidth == 0 || prevheight == 0)
        return ;
    rSize.h = prevheight * rSize.w / prevwidth;
    if(maxheight < rSize.h) {
        MY_LOGW("Warning!!,  scaled preview height(%d) is larger than max height(%d)", rSize.h, maxheight);
        rSize.h = maxheight;
    }
    MY_LOGD("evaluate preview size : %dx%d", prevwidth, prevheight);
    MY_LOGD("FD buffer size : %dx%d", rSize.w, rSize.h);
}


/**
 * default version
 */
FunctionType_Configuration_StreamInfo_P1 makePolicy_Configuration_StreamInfo_P1_Default()
{
    return [](Configuration_StreamInfo_P1_Params const& params) -> int {
        auto pvOut = params.pvOut;
        auto pvP1HwSetting = params.pvP1HwSetting;
        auto pvP1DmaNeed = params.pvP1DmaNeed;
        auto pPipelineNodesNeed = params.pPipelineNodesNeed;
        auto pPipelineStaticInfo = params.pPipelineStaticInfo;
        auto pPipelineUserConfiguration = params.pPipelineUserConfiguration;
        auto const& pParsedAppConfiguration   __unused = pPipelineUserConfiguration->pParsedAppConfiguration;
        auto const& pParsedAppImageStreamInfo __unused = pPipelineUserConfiguration->pParsedAppImageStreamInfo;

        for(size_t i = 0; i < pPipelineNodesNeed->needP1Node.size(); i++)
        {
            ParsedStreamInfo_P1 config;
            if (pPipelineNodesNeed->needP1Node[i])
            {
                //MetaData
                {
                    P1MetaStreamInfoBuilder builder(i);
                    builder.setP1AppMetaDynamic_Default();
                    config.pAppMeta_DynamicP1 = builder.build();
                }
                {
                    P1MetaStreamInfoBuilder builder(i);
                    builder.setP1HalMetaDynamic_Default();
                    config.pHalMeta_DynamicP1 = builder.build();
                }
                {
                    P1MetaStreamInfoBuilder builder(i);
                    builder.setP1HalMetaControl_Default();
                    config.pHalMeta_Control = builder.build();
                }

                std::shared_ptr<IHwInfoHelper> infohelper = IHwInfoHelperManager::get()->getHwInfoHelper(pPipelineStaticInfo->sensorId[i]);
                MY_LOGF_IF(infohelper==nullptr, "getHwInfoHelper");

                MINT32 p1_out_buffer_size = 10;
                #if (MTKCAM_HAVE_MFB_SUPPORT!=0)
                p1_out_buffer_size = 10;        // if support MFLL, set buffer size to 10
                #else
                p1_out_buffer_size = 8;         // if not support MFLL, set buffer size to 8
                bool is_low_mem = property_get_bool("ro.config.low_ram", false);
                if(is_low_mem)
                    p1_out_buffer_size = 6;     // For low memory project, set buffer size to 6
                #endif

                MINT32 P1_streamBufCnt = ::property_get_int32("vendor.debug.camera.p1.streamBufCnt",p1_out_buffer_size);
                MY_LOGD("P1 out image buffer size = %d",P1_streamBufCnt);
                //IMGO
                if ((*pvP1DmaNeed)[i] & P1_IMGO)
                {
                    P1ImageStreamInfoBuilder builder(i, infohelper);
                    builder.setP1Imgo_Default(P1_streamBufCnt, (*pvP1HwSetting)[i]);
                    config.pHalImage_P1_Imgo = builder.build();
                }
                //RRZO
                if ((*pvP1DmaNeed)[i] & P1_RRZO)
                {
                    P1ImageStreamInfoBuilder builder(i, infohelper);
                    builder.setP1Rrzo_Default(P1_streamBufCnt, (*pvP1HwSetting)[i]);
                    config.pHalImage_P1_Rrzo = builder.build();
                }
                //LCSO
                if ((*pvP1DmaNeed)[i] & P1_LCSO)
                {
                    P1ImageStreamInfoBuilder builder(i, infohelper);
                    builder.setP1Lcso_Default(P1_streamBufCnt);
                    config.pHalImage_P1_Lcso = builder.build();
                }
                //RSSO
                if ((*pvP1DmaNeed)[i] & P1_RSSO)
                {
                    P1ImageStreamInfoBuilder builder(i, infohelper);
                    builder.setP1Rsso_Default(P1_streamBufCnt, (*pvP1HwSetting)[i]);
                    config.pHalImage_P1_Rsso = builder.build();
                }
                // fd yuv
                if ((*pvP1DmaNeed)[i] & P1_FDYUV)
                {
                    P1ImageStreamInfoBuilder builder(i, infohelper);
                    builder.setP1FDYuv_Default(P1_streamBufCnt, (*pvP1HwSetting)[i]);
                    config.pHalImage_P1_FDYuv = builder.build();
                }
            }
            pvOut->push_back(std::move(config));
        }

        return OK;
    };
}


/**
 * default version
 */
FunctionType_Configuration_StreamInfo_NonP1 makePolicy_Configuration_StreamInfo_NonP1_Default()
{
    return [](Configuration_StreamInfo_NonP1_Params const& params) -> int {
        auto pOut = params.pOut;
        auto pPipelineNodesNeed = params.pPipelineNodesNeed;
        auto pCaptureFeatureSetting = params.pCaptureFeatureSetting;
        auto pPipelineStaticInfo = params.pPipelineStaticInfo;
        auto pPipelineUserConfiguration = params.pPipelineUserConfiguration;
        auto const& pParsedAppImageStreamInfo = pPipelineUserConfiguration->pParsedAppImageStreamInfo;

        pOut->pAppMeta_Control = pPipelineUserConfiguration->vMetaStreams.begin()->second;

        if (pPipelineNodesNeed->needP2StreamNode)
        {
            pOut->pAppMeta_DynamicP2StreamNode =
                    new MetaStreamInfo(
                        "App:Meta:DynamicP2",
                        eSTREAMID_META_APP_DYNAMIC_02,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            pOut->pHalMeta_DynamicP2StreamNode =
                    new MetaStreamInfo(
                        "Hal:Meta:P2:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_02,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }
        if (pPipelineNodesNeed->needP2CaptureNode)
        {
            pOut->pAppMeta_DynamicP2CaptureNode =
                    new MetaStreamInfo(
                        "App:Meta:DynamicP2Cap",
                        eSTREAMID_META_APP_DYNAMIC_02_CAP,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            pOut->pHalMeta_DynamicP2CaptureNode =
                    new MetaStreamInfo(
                        "Hal:Meta:P2C:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_02_CAP,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }
        if (pPipelineNodesNeed->needFDNode)
        {
            pOut->pAppMeta_DynamicFD =
                    new MetaStreamInfo(
                        "App:Meta:FD",
                        eSTREAMID_META_APP_DYNAMIC_FD,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            if ( !pPipelineStaticInfo->isP1DirectFDYUV )
            {
                // FD YUV
                //MSize const size(640, 480); //FIXME: hard-code here?
                MSize size(640, 480);
                // evaluate preview size
                evaluatePreviewSize(pPipelineUserConfiguration, size);

                MY_LOGD("evaluate FD buffer size : %dx%d", size.w, size.h);

                MINT const format = eImgFmt_YUY2;//eImgFmt_YV12;
                MUINT const usage = 0;

                pOut->pHalImage_FD_YUV =
                createImageStreamInfo(
                        "Hal:Image:FD",
                        eSTREAMID_IMAGE_FD,
                        eSTREAMTYPE_IMAGE_INOUT,
                        5, 1,
                        usage, format, size
                    );
            }
        }
        if (pPipelineNodesNeed->needJpegNode && pParsedAppImageStreamInfo->pAppImage_Jpeg != nullptr)
        {
            pOut->pAppMeta_DynamicJpeg =
                    new MetaStreamInfo(
                        "App:Meta:Jpeg",
                        eSTREAMID_META_APP_DYNAMIC_JPEG,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );

            uint32_t const maxJpegNum = ( pCaptureFeatureSetting )?
                                        pCaptureFeatureSetting->maxAppJpegStreamNum : 1;
            // Jpeg YUV
            {
                MSize const& size = pParsedAppImageStreamInfo->pAppImage_Jpeg->getImgSize();
#if MTK_CAM_YUV420_JPEG_ENCODE_SUPPORT
                MINT const format = eImgFmt_NV21;
#else
                MINT const format = eImgFmt_YUY2;
#endif
                MUINT const usage = 0;
                pOut->pHalImage_Jpeg_YUV =
                        createImageStreamInfo_YuvJpeg(
                                "Hal:Image:YuvJpeg",
                                eSTREAMID_IMAGE_PIPE_YUV_JPEG_00,
                                eSTREAMTYPE_IMAGE_INOUT,
                                maxJpegNum, 0,
                                usage, format, size
                            );
            }
            // Thumbnail YUV
            {
                MSize const size(-1L, -1L); //unknown now
                MINT const format = eImgFmt_YUY2;
                MUINT const usage = 0;
                pOut->pHalImage_Thumbnail_YUV =
                    createImageStreamInfo(
                            "Hal:Image:YuvThumbnail",
                            eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL_00,
                            eSTREAMTYPE_IMAGE_INOUT,
                            maxJpegNum, 0,
                            usage, format, size
                        );
            }
        }

        if (pPipelineNodesNeed->needPDENode)
        {
            pOut->pHalMeta_DynamicPDE =
                new MetaStreamInfo(
                        "Hal:Meta:PDE:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_PDE,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }

        if (pPipelineNodesNeed->needRaw16Node)
        {
            pOut->pAppMeta_DynamicRAW16 =
                new MetaStreamInfo(
                        "App:Meta:Raw16:Dynamic",
                        eSTREAMID_META_APP_DYNAMIC_RAW16,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }

        return OK;
    };
}


};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

