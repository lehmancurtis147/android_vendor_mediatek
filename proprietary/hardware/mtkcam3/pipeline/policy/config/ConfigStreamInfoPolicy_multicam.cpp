/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-ConfigStreamInfoPolicy_Multicam"

#include <mtkcam3/pipeline/policy/IConfigStreamInfoPolicy.h>
#include "ConfigStreamInfoPolicy.h"
//
#include <array>
//
#include <mtkcam3/pipeline/hwnode/StreamId.h>
#include <mtkcam/aaa/IIspMgr.h>
//
#include "MyUtils.h"


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {

/******************************************************************************
 *
 ******************************************************************************/
static
sp<ImageStreamInfo>
createImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize
)
{
    IImageStreamInfo::BufPlanes_t bufPlanes;
#define addBufPlane(planes, height, stride)                                      \
        do{                                                                      \
            size_t _height = (size_t)(height);                                   \
            size_t _stride = (size_t)(stride);                                   \
            IImageStreamInfo::BufPlane bufPlane= { _height * _stride, _stride }; \
            planes.push_back(bufPlane);                                          \
        }while(0)
    switch( imgFormat ) {
        case eImgFmt_YV12:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w >> 1);
            break;
        case eImgFmt_NV21:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w);
            addBufPlane(bufPlanes , imgSize.h >> 1 , imgSize.w);
            break;
        case eImgFmt_YUY2:
            addBufPlane(bufPlanes , imgSize.h      , imgSize.w << 1);
            break;
        default:
            MY_LOGE("format not support yet %d", imgFormat);
            break;
    }
#undef  addBufPlane

    sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
                streamName,
                streamId,
                streamType,
                maxBufNum, minInitBufNum,
                usageForAllocator, imgFormat, imgSize, bufPlanes, 0
                );

    if( pStreamInfo == NULL ) {
        MY_LOGE("create ImageStream failed, %s, %#" PRIx64,
                streamName, streamId);
    }

    return pStreamInfo;
}

/******************************************************************************
 *
 ******************************************************************************/
static
MVOID
evaluatePreviewSize(
    PipelineUserConfiguration const* pPipelineUserConfiguration,
    MSize &rSize
)
{
    sp<IImageStreamInfo> pStreamInfo;
    int consumer_usage = 0;
    int allocate_usage = 0;
    int maxheight      = rSize.h;
    int prevwidth      = 0;
    int prevheight     = 0;

    for( const auto& n : pPipelineUserConfiguration->pParsedAppImageStreamInfo->vAppImage_Output_Proc )
    {
        if  ( (pStreamInfo = n.second) != 0 ) {
            consumer_usage = pStreamInfo->getUsageForConsumer();
            allocate_usage = pStreamInfo->getUsageForAllocator();
            MY_LOGD("consumer : %X, allocate : %X", consumer_usage, allocate_usage);
            if(consumer_usage & GRALLOC_USAGE_HW_TEXTURE) {
                prevwidth = pStreamInfo->getImgSize().w;
                prevheight = pStreamInfo->getImgSize().h;
                break;
            }
            if(consumer_usage & GRALLOC_USAGE_HW_VIDEO_ENCODER) {
                continue;
            }
            prevwidth = pStreamInfo->getImgSize().w;
            prevheight = pStreamInfo->getImgSize().h;
        }
    }
    if(prevwidth == 0 || prevheight == 0)
        return ;
    rSize.h = prevheight * rSize.w / prevwidth;
    if(maxheight < rSize.h) {
        MY_LOGW("Warning!!,  scaled preview height(%d) is larger than max height(%d)", rSize.h, maxheight);
        rSize.h = maxheight;
    }
    MY_LOGD("evaluate preview size : %dx%d", prevwidth, prevheight);
    MY_LOGD("FD buffer size : %dx%d", rSize.w, rSize.h);
}

/**
 * multi cam version
 */
FunctionType_Configuration_StreamInfo_NonP1 makePolicy_Configuration_StreamInfo_NonP1_Multicam()
{
    return [](Configuration_StreamInfo_NonP1_Params const& params) -> int {
        auto pOut = params.pOut;
        auto pPipelineNodesNeed = params.pPipelineNodesNeed;
        auto pCaptureFeatureSetting = params.pCaptureFeatureSetting;
        auto pPipelineStaticInfo = params.pPipelineStaticInfo;
        auto pPipelineUserConfiguration = params.pPipelineUserConfiguration;
        auto const& pParsedAppImageStreamInfo = pPipelineUserConfiguration->pParsedAppImageStreamInfo;
        auto const& vPhysicCameras = pPipelineUserConfiguration->vPhysicCameras;

        pOut->pAppMeta_Control = pPipelineUserConfiguration->vMetaStreams.begin()->second;

        if (pPipelineNodesNeed->needP2StreamNode)
        {
            pOut->pAppMeta_DynamicP2StreamNode =
                    new MetaStreamInfo(
                        "App:Meta:DynamicP2",
                        eSTREAMID_META_APP_DYNAMIC_02,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            pOut->pHalMeta_DynamicP2StreamNode =
                    new MetaStreamInfo(
                        "Hal:Meta:P2:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_02,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
            // create main1, main2 dynamic meta stream info
            for(size_t i = 0; i < vPhysicCameras.size(); i++)
            {
                if (vPhysicCameras[i] == pPipelineStaticInfo->sensorId[0])
                {
                    pOut->pAppMeta_DynamicP2StreamNode_Main1 =
                        new MetaStreamInfo(
                            "App:Meta:DynamicP2_MAIN1",
                            eSTREAMID_META_APP_DYNAMIC_02_MAIN1,
                            eSTREAMTYPE_META_OUT,
                            10, 1
                            );
                } else if (vPhysicCameras[i] == pPipelineStaticInfo->sensorId[1])
                {
                    pOut->pAppMeta_DynamicP2StreamNode_Main2 =
                        new MetaStreamInfo(
                            "App:Meta:DynamicP2_MAIN2",
                            eSTREAMID_META_APP_DYNAMIC_02_MAIN2,
                            eSTREAMTYPE_META_OUT,
                            10, 1
                            );
                }
            }
        }
        if (pPipelineNodesNeed->needP2CaptureNode)
        {
            pOut->pAppMeta_DynamicP2CaptureNode =
                    new MetaStreamInfo(
                        "App:Meta:DynamicP2Cap",
                        eSTREAMID_META_APP_DYNAMIC_02_CAP,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            pOut->pHalMeta_DynamicP2CaptureNode =
                    new MetaStreamInfo(
                        "Hal:Meta:P2C:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_02_CAP,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }
        if (pPipelineNodesNeed->needFDNode)
        {
            pOut->pAppMeta_DynamicFD =
                    new MetaStreamInfo(
                        "App:Meta:FD",
                        eSTREAMID_META_APP_DYNAMIC_FD,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );
            // FD YUV
            //MSize const size(640, 480); //FIXME: hard-code here?
            MSize size(640, 480);
            // evaluate preview size
            evaluatePreviewSize(pPipelineUserConfiguration, size);

            MY_LOGD("evaluate FD buffer size : %dx%d", size.w, size.h);

            MINT const format = eImgFmt_YUY2;//eImgFmt_YV12;
            MUINT const usage = 0;

            pOut->pHalImage_FD_YUV =
            createImageStreamInfo(
                    "Hal:Image:FD",
                    eSTREAMID_IMAGE_FD,
                    eSTREAMTYPE_IMAGE_INOUT,
                    5, 1,
                    usage, format, size
                );
        }
        if (pPipelineNodesNeed->needJpegNode && pParsedAppImageStreamInfo->pAppImage_Jpeg != nullptr)
        {
            pOut->pAppMeta_DynamicJpeg =
                    new MetaStreamInfo(
                        "App:Meta:Jpeg",
                        eSTREAMID_META_APP_DYNAMIC_JPEG,
                        eSTREAMTYPE_META_OUT,
                        10, 1
                        );

            uint32_t const maxJpegNum = ( pCaptureFeatureSetting )?
                                        pCaptureFeatureSetting->maxAppJpegStreamNum : 1;
            // Jpeg YUV
            {
                MSize const& size = pParsedAppImageStreamInfo->pAppImage_Jpeg->getImgSize();
                MINT const format = eImgFmt_YUY2;
                MUINT const usage = 0;
                pOut->pHalImage_Jpeg_YUV =
                        createImageStreamInfo(
                                "Hal:Image:YuvJpeg",
                                eSTREAMID_IMAGE_PIPE_YUV_JPEG_00,
                                eSTREAMTYPE_IMAGE_INOUT,
                                maxJpegNum, 0,
                                usage, format, size
                            );
            }
            // Thumbnail YUV
            {
                MSize const size(-1L, -1L); //unknown now
                MINT const format = eImgFmt_YUY2;
                MUINT const usage = 0;
                pOut->pHalImage_Thumbnail_YUV =
                    createImageStreamInfo(
                            "Hal:Image:YuvThumbnail",
                            eSTREAMID_IMAGE_PIPE_YUV_THUMBNAIL_00,
                            eSTREAMTYPE_IMAGE_INOUT,
                            maxJpegNum, 0,
                            usage, format, size
                        );
            }
        }

        if (pPipelineNodesNeed->needPDENode)
        {
            pOut->pHalMeta_DynamicPDE =
                new MetaStreamInfo(
                        "Hal:Meta:PDE:Dynamic",
                        eSTREAMID_META_PIPE_DYNAMIC_PDE,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }

        if (pPipelineNodesNeed->needRaw16Node)
        {
            pOut->pAppMeta_DynamicRAW16 =
                new MetaStreamInfo(
                        "App:Meta:Raw16:Dynamic",
                        eSTREAMID_META_APP_DYNAMIC_RAW16,
                        eSTREAMTYPE_META_INOUT,
                        10, 1
                        );
        }

        return OK;
    };
}

};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

