/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "mtkcam-FeatureSettingPolicy"
//
#include <algorithm>
#include <tuple>
// MTKCAM
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
#include <mtkcam/utils/hw/HwInfoHelper.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/LogicalCam/IHalLogicalDeviceList.h>
// MTKCAM3
#include <mtkcam3/feature/hdrDetection/Defs.h>
#include <mtkcam3/feature/eis/eis_ext.h>
#include <mtkcam3/feature/3dnr/3dnr_defs.h>
#include <mtkcam3/feature/utils/FeatureProfileHelper.h>
#include <mtkcam3/feature/fsc/fsc_defs.h>

#include <camera_custom_eis.h>
#include <camera_custom_3dnr.h>
#include <camera_custom_fsc.h>
// dual cam
#include <mtkcam/aaa/ISync3A.h>
#include <mtkcam3/feature/stereo/hal/stereo_size_provider.h>
#include <mtkcam3/feature/stereo/hal/stereo_common.h>
#include <camera_custom_stereo.h>
#include <isp_tuning.h>
//
#include "FeatureSettingPolicy.h"
#include <mtkcam/utils/hw/IScenarioControlV3.h>
//
#include "MyUtils.h"
#define __DEBUG // enable function scope debug
#ifdef __DEBUG
#define FUNCTION_SCOPE          auto __scope_logger__ = create_scope_logger(__FUNCTION__)
#include <memory>
#include <functional>
static std::shared_ptr<char> create_scope_logger(const char* functionName)
{
    char* pText = const_cast<char*>(functionName);
    CAM_LOGD_IF(pText, "[%s] + ", pText);
    return std::shared_ptr<char>(pText, [](char* p){ CAM_LOGD_IF(p, "[%s] -", p); });
}
#else
#define FUNCTION_SCOPE          do{}while(0)
#endif
/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::NSPipelinePlugin;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::pipeline::policy::featuresetting;
using namespace NSCam::v3::pipeline::policy::scenariomgr;
using namespace NSCam::v3::pipeline::model;
using namespace NS3Av3; //IHal3A
using namespace NSCamHW; //HwInfoHelper
using namespace NSCam::NR3D;
using namespace NSCam::FSC;
using namespace NSCam::v3::pipeline::model;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)

// TODO: rename
#define SENSOR_INDEX_MAIN   (0)
#define SENSOR_INDEX_SUB1   (1)
#define SENSOR_INDEX_SUB2   (2)
// TODO: disable thsi flag before MP
#define DEBUG_FEATURE_SETTING_POLICY  (0)
#define DEBUG_EISEM  (0)
#define DEBUG_EIS30  (0)
#ifndef NR3D_SUPPORTED
#define FORCE_3DNR   (0)
#else
#define FORCE_3DNR   (1)
#endif
#define DEBUG_TSQ    (0)
#define DEBUG_VHDR      (0)
#define DEBUG_APP_HDR   (-1)
#define DEBUG_DUMMY_HDR (1)


/******************************************************************************
 *
 ******************************************************************************/
template<typename TPlugin>
class PluginWrapper
{
public:
    using PluginPtr      = typename TPlugin::Ptr;
    using ProviderPtr    = typename TPlugin::IProvider::Ptr;
    using InterfacePtr   = typename TPlugin::IInterface::Ptr;
    using SelectionPtr   = typename TPlugin::Selection::Ptr;
    using Selection      = typename TPlugin::Selection;
    using Property       = typename TPlugin::Property;
    using ProviderPtrs   = std::vector<ProviderPtr>;
public:
    PluginWrapper(const std::string& name, MINT32 iOpenId, MINT32 iOpenId2 = -1);
    ~PluginWrapper();
public:
    auto getName() const -> const std::string&;
    auto isKeyFeatureExisting(MINT64 combinedKeyFeature, MINT64& keyFeature) const -> MBOOL;
    auto tryGetKeyFeature(MINT64 combinedKeyFeature, MINT64& keyFeature, MINT8& keyFeatureIndex) const -> MBOOL;
    auto getProvider(MINT64 combinedKeyFeature, MINT64& keyFeature) -> ProviderPtr;
    auto getProviders() -> ProviderPtrs;
    auto createSelection() const -> SelectionPtr;
    auto offer(Selection& sel) const -> MVOID;
    auto keepSelection(const uint32_t requestNo, ProviderPtr& providerPtr, SelectionPtr& sel) -> MVOID;
    auto pushSelection() -> MVOID;
    auto cancel() -> MVOID;
private:
    using ProviderPtrMap = std::unordered_map<MUINT64, ProviderPtr>;
    using SelectionPtrMap = std::unordered_map<ProviderPtr, std::vector<SelectionPtr>>;
private:
    const std::string       mName;
    const MINT32            mOpenId1;
    const MINT32            mOpenId2;
    PluginPtr               mInstancePtr;
    ProviderPtrs            mProviderPtrsSortedByPriorty;
    SelectionPtrMap         mTempSelectionPtrMap;
    InterfacePtr            mInterfacePtr;
};

/******************************************************************************
 *
 ******************************************************************************/
template<typename TPlugin>
PluginWrapper<TPlugin>::
PluginWrapper(const std::string& name, MINT32 iOpenId, MINT32 iOpenId2)
: mName(name)
, mOpenId1(iOpenId)
, mOpenId2(iOpenId2)
{
    FUNCTION_SCOPE;
    MY_LOGD("ctor, name:%s, openId:%d, openId2:%d",
        mName.c_str(), mOpenId1, mOpenId2);
    mInstancePtr = TPlugin::getInstance(mOpenId1, mOpenId2);
    if (mInstancePtr) {
        mInterfacePtr = mInstancePtr->getInterface();
        auto& providers = mInstancePtr->getProviders();
        mProviderPtrsSortedByPriorty = providers;
        std::sort(mProviderPtrsSortedByPriorty.begin(), mProviderPtrsSortedByPriorty.end(),
            [] (const ProviderPtr& p1, const ProviderPtr& p2) {
                return p1->property().mPriority > p2->property().mPriority;
            }
        );

        for (auto& provider : mProviderPtrsSortedByPriorty) {
            const Property& property = provider->property();
            MY_LOGD("find provider... name:%s, algo(%#" PRIx64"), priority(0x%x)", property.mName, property.mFeatures, property.mPriority);
        }
    }
    else {
        MY_LOGW("cannot get instance for %s features strategy", mName.c_str());
    }
}

template<typename TPlugin>
PluginWrapper<TPlugin>::
~PluginWrapper()
{
    MY_LOGD("dtor, name:%s, openId:%d, openId2:%d",
        mName.c_str(), mOpenId1, mOpenId2);
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
getName() const -> const std::string&
{
    return mName;
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
isKeyFeatureExisting(MINT64 combinedKeyFeature, MINT64& keyFeature) const -> MBOOL
{
    MINT8  keyFeatureIndex = 0;
    return tryGetKeyFeature(combinedKeyFeature, keyFeature, keyFeatureIndex);
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
tryGetKeyFeature(MINT64 combinedKeyFeature, MINT64& keyFeature, MINT8& keyFeatureIndex) const -> MBOOL
{
    for(MUINT8 i=0; i < mProviderPtrsSortedByPriorty.size(); i++) {
        auto providerPtr = mProviderPtrsSortedByPriorty.at(i);
        keyFeature = providerPtr->property().mFeatures;
        if ((keyFeature & combinedKeyFeature) != 0) {
            keyFeatureIndex = i;
            return MTRUE;
        }
    }

    // if no plugin found, must hint no feature be chose.
    keyFeature = 0;
    keyFeatureIndex = 0;
    return MFALSE;
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
getProvider(MINT64 combinedKeyFeature, MINT64& keyFeature) -> ProviderPtr
{
    MINT8  keyFeatureIndex = 0;
    return tryGetKeyFeature(combinedKeyFeature, keyFeature, keyFeatureIndex) ? mProviderPtrsSortedByPriorty[keyFeatureIndex] : nullptr;
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
getProviders() -> ProviderPtrs
{
    ProviderPtrs ret;
    ret = mProviderPtrsSortedByPriorty;
    return std::move(ret);
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
createSelection() const -> SelectionPtr
{
    return mInstancePtr->createSelection();
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
offer(Selection& sel) const -> MVOID
{
    mInterfacePtr->offer(sel);
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
keepSelection(const uint32_t requestNo, ProviderPtr& providerPtr, SelectionPtr& sel) -> MVOID
{
    if (mTempSelectionPtrMap.find(providerPtr) != mTempSelectionPtrMap.end()) {
        mTempSelectionPtrMap[providerPtr].push_back(sel);
        MY_LOGD("%s: selection size:%zu, requestNo:%u",getName().c_str(), mTempSelectionPtrMap[providerPtr].size(), requestNo);
    }
    else {
        std::vector<SelectionPtr> vSelection;
        vSelection.push_back(sel);
        mTempSelectionPtrMap[providerPtr] = vSelection;
        MY_LOGD("%s: new selection size:%zu, requestNo:%u", getName().c_str(), mTempSelectionPtrMap[providerPtr].size(), requestNo);
    }
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
pushSelection() -> MVOID
{
    for (auto item : mTempSelectionPtrMap) {
        auto providerPtr = item.first;
        auto vSelection  = item.second;
        MY_LOGD("%s: selection size:%zu", getName().c_str(), vSelection.size());
        for (auto sel : vSelection) {
            mInstancePtr->pushSelection(providerPtr, sel);
        }
        vSelection.clear();
    }
    mTempSelectionPtrMap.clear();
}

template<typename TPlugin>
auto
PluginWrapper<TPlugin>::
cancel() -> MVOID
{
    for (auto item : mTempSelectionPtrMap) {
        auto providerPtr = item.first;
        auto vSelection  = item.second;
        if (providerPtr.get()) {
            //providerPtr->cancel();
        }
        MY_LOGD("%s: selection size:%zu", getName().c_str(), vSelection.size());
        vSelection.clear();
    }
    mTempSelectionPtrMap.clear();
}

#define DEFINE_PLUGINWRAPER(CLASSNAME, PLUGINNAME)                                                      \
class FeatureSettingPolicy::CLASSNAME final: public PluginWrapper<NSCam::NSPipelinePlugin::PLUGINNAME>  \
{                                                                                                       \
public:                                                                                                 \
    CLASSNAME(MINT32 iOpenId, MINT32 iOpenId2 = -1)                                                     \
    : PluginWrapper<NSCam::NSPipelinePlugin::PLUGINNAME>(#PLUGINNAME, iOpenId, iOpenId2)                \
    {                                                                                                   \
    }                                                                                                   \
}
DEFINE_PLUGINWRAPER(MFPPluginWrapper, MultiFramePlugin);
DEFINE_PLUGINWRAPER(RawPluginWrapper, RawPlugin);
DEFINE_PLUGINWRAPER(YuvPluginWrapper, YuvPlugin);
DEFINE_PLUGINWRAPER(BokehPluginWraper, BokehPlugin);
DEFINE_PLUGINWRAPER(DepthPluginWraper, DepthPlugin);
DEFINE_PLUGINWRAPER(FusionPluginWraper, FusionPlugin);
#undef DEFINE_PLUGINWRAPER

/******************************************************************************
 *
 ******************************************************************************/
FeatureSettingPolicy::
FeatureSettingPolicy(
    CreationParams const& params
)
    :mPolicyParams(params)
{
    FUNCTION_SCOPE;
    MY_LOGI("create feature setting policy instance for openId(%d), sensors_count(%zu), is_dual_cam(%d)",
            mPolicyParams.pPipelineStaticInfo->openId,
            mPolicyParams.pPipelineStaticInfo->sensorId.size(),
            mPolicyParams.pPipelineStaticInfo->isDualDevice);
    mbDebug = ::property_get_int32("vendor.debug.camera.featuresetting.log", DEBUG_FEATURE_SETTING_POLICY);
    // forced feature strategy for debug
    mForcedKeyFeatures = ::property_get_int64("vendor.debug.featuresetting.keyfeature", -1);
    mForcedFeatureCombination = ::property_get_int64("vendor.debug.featuresetting.featurecombination", -1);

    // get multiple frames key features from  multi-frame plugin providers
    auto mainSensorId = mPolicyParams.pPipelineStaticInfo->sensorId[SENSOR_INDEX_MAIN];
    //
    mMFPPluginWrapperPtr = std::make_shared<MFPPluginWrapper>(mainSensorId);
    mRawPluginWrapperPtr = std::make_shared<RawPluginWrapper>(mainSensorId);
    mYuvPluginWrapperPtr = std::make_shared<YuvPluginWrapper>(mainSensorId);
    if (mPolicyParams.pPipelineStaticInfo->isDualDevice) {
        auto& pParsedAppConfiguration = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration;
        auto& pParsedDualCamInfo = pParsedAppConfiguration->pParsedDualCamInfo;
        mDualDevicePath = pParsedDualCamInfo->mDualDevicePath;
        if(pParsedDualCamInfo->mDualDevicePath == DualDevicePath::Feature)
        {
            miMultiCamFeatureMode = pParsedDualCamInfo->mDualFeatureMode;
            MY_LOGD("current multicam feature mode (%d)", miMultiCamFeatureMode);
            if(miMultiCamFeatureMode == MTK_MULTI_CAM_FEATURE_MODE_VSDOF)
            {
                // previe bind function
                mMultiCamStreamingUpdater =
                                        std::bind(
                                            &FeatureSettingPolicy::updateVsDofStreamingData,
                                            this,
                                            std::placeholders::_1,
                                            std::placeholders::_2);
                // capture setting
                const int32_t mainSensorId = mPolicyParams.pPipelineStaticInfo->sensorId[SENSOR_INDEX_MAIN];
                const int32_t sub1SensorId = mPolicyParams.pPipelineStaticInfo->sensorId[SENSOR_INDEX_SUB1];
                //
                mBokehPluginWraperPtr = std::make_shared<BokehPluginWraper>(mainSensorId, sub1SensorId);
                mDepthPluginWraperPtr = std::make_shared<DepthPluginWraper>(mainSensorId, sub1SensorId);
                mFusionPluginWraperPtr = std::make_shared<FusionPluginWraper>(mainSensorId, sub1SensorId);
            }
            else
            {
                MY_LOGE("not support, please check it");
            }
        }
        else if(pParsedDualCamInfo->mDualDevicePath == DualDevicePath::MultiCamControl)
        {
            MY_LOGD("multicam flow.");
            mMultiCamStreamingUpdater =
                            std::bind(
                                &FeatureSettingPolicy::updateMultiCamStreamingData,
                                this,
                                std::placeholders::_1,
                                std::placeholders::_2);
        }
        else
        {
            MY_LOGD("using single flow");
        }
    }
    // create Hal3A for 3A info query
    std::shared_ptr<IHal3A> hal3a
    (
        MAKE_Hal3A(mPolicyParams.pPipelineStaticInfo->sensorId[SENSOR_INDEX_MAIN], LOG_TAG),
        [](IHal3A* p){ if(p) p->destroyInstance(LOG_TAG); }
    );
    if (hal3a.get()) {
        mHal3a = hal3a;
    }
    else {
        MY_LOGE("MAKE_Hal3A failed, it nullptr");
    }
}

auto
FeatureSettingPolicy::
collectParsedStrategyInfo(
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    //FUNCTION_SCOPE;
    bool ret = true;
    // collect parsed info for common strategy (only per-frame requirement)
    // Note: It is per-frames query behavior here.
    // TODO: collect parsed info for common strategy
    // collect parsed info for capture feature strategy
    if (in->needP2CaptureNode) {
        if (CC_UNLIKELY(in->pRequest_ParsedAppMetaControl == nullptr)) {
            MY_LOGW("cannot get ParsedMetaControl, invalid nullptr");
        }
        else {
            parsedInfo.isZslModeOn = mConfigInputParams.isZslMode;
            parsedInfo.isZslFlowOn = in->pRequest_ParsedAppMetaControl->control_enableZsl;
        }
        // obtain latest real iso information for capture strategy
        {
            CaptureParam_T captureParam;
            auto hal3a = mHal3a;
            if (hal3a.get()) {
                std::lock_guard<std::mutex> _l(mHal3aLocker);
                ExpSettingParam_T expParam;
                hal3a->send3ACtrl(E3ACtrl_GetExposureInfo,  (MINTPTR)&expParam, 0);
                hal3a->send3ACtrl(E3ACtrl_GetExposureParam, (MINTPTR)&captureParam, 0);
            }
            else {
                MY_LOGE("create IHal3A instance failed! cannot get current real iso for strategy");
                ::memset(&captureParam, 0, sizeof(CaptureParam_T));
                ret = false;
            }
            parsedInfo.realIso = captureParam.u4RealISO;
            parsedInfo.exposureTime = captureParam.u4Eposuretime; //us

            // query flash status from Hal3A
            int isFlashOn = 0;
            hal3a->send3ACtrl(NS3Av3::E3ACtrl_GetIsFlashOnCapture, (MINTPTR)&isFlashOn, 0);
            if (isFlashOn) {
                parsedInfo.isFlashOn = true;
            }
        }
        // get info from AppControl Metadata
        {
            auto pAppMetaControl = in->pRequest_AppControl;
            MUINT8 aeState = MTK_CONTROL_AE_STATE_INACTIVE;
            MUINT8 aeMode = MTK_CONTROL_AE_MODE_OFF;
            if (!IMetadata::getEntry<MUINT8>(pAppMetaControl, MTK_CONTROL_AE_MODE, aeMode)) {
                MY_LOGW("get metadata MTK_CONTROL_AE_MODE failed! cannot get current  state for strategy");
            }
            else {
                MINT32 manualIso = 0;
                MINT64 manualExposureTime = 0;
                if (aeMode  == MTK_CONTROL_AE_MODE_OFF) {
                    IMetadata::getEntry<MINT32>(pAppMetaControl, MTK_SENSOR_SENSITIVITY, manualIso);
                    IMetadata::getEntry<MINT64>(pAppMetaControl, MTK_SENSOR_EXPOSURE_TIME, manualExposureTime);
                    if (manualIso > 0 && manualExposureTime > 0) {
                        MY_LOGI("it is manual iso(%d)/exposure(%" PRId64 " ns) as capture feature strategy info.",
                                manualIso, manualExposureTime);
                        parsedInfo.isAppManual3A = MTRUE;
                        parsedInfo.realIso = manualIso;
                        parsedInfo.exposureTime = manualExposureTime/1000; //ns to us
                    }
                    else {
                        MY_LOGW("invaild manual iso(%d)/exposure(%" PRId64 ") for manual AE", manualIso, manualExposureTime);
                        // choose the previous default preview 3A info as capture feature strategy
                    }
                }
            }
            if (!IMetadata::getEntry<MUINT8>(pAppMetaControl, MTK_CONTROL_AE_STATE, aeState)) {
                MY_LOGD("get metadata MTK_CONTROL_AE_STATE failed! cannot get current flash state for strategy");
            }
            if (aeMode  == MTK_CONTROL_AE_MODE_ON_ALWAYS_FLASH ||
                aeState == MTK_CONTROL_AE_STATE_FLASH_REQUIRED) {
                parsedInfo.isFlashOn = true;
            }
            MINT32 cshot = 0;
            if (IMetadata::getEntry<MINT32>(pAppMetaControl, MTK_CSHOT_FEATURE_CAPTURE, cshot)) {
                if (cshot) parsedInfo.isCShot = true;
            }
        }
        // check
        if (in->pRequest_AppImageStreamInfo->pAppImage_Input_Yuv) {
            parsedInfo.isYuvReprocess = true;
        }
        if (in->pRequest_AppImageStreamInfo->pAppImage_Input_Priv || in->pRequest_AppImageStreamInfo->pAppImage_Input_RAW16) {
            parsedInfo.isRawReprocess = true;
        }

        //after doing capture, vhdr need to add dummy frame
        if(ret && mVhdrInfo.curAppHdrMode == HDRMode::VIDEO_ON){
            mVhdrInfo.bIsDoCapture = true;
            MY_LOGD("[vhdrDummyFrames] (vhdr_on): after doing capture, vhdr need to add dummy frame");
        }
        MY_LOGD("strategy info for capture feature(isZsl(mode:%d, flow:%d), isCShot:%d, isFlashOn:%d, iso:%d, shutterTimeUs:%d), reprocess(raw:%d, yuv:%d)",
                parsedInfo.isZslModeOn, parsedInfo.isZslFlowOn, parsedInfo.isCShot, parsedInfo.isFlashOn, parsedInfo.realIso, parsedInfo.exposureTime,
                parsedInfo.isRawReprocess, parsedInfo.isYuvReprocess);
    }
    // collect parsed strategy info for stream feature
    if (in->needP2StreamNode) {
        // Note: It is almost per-frames query behavior here.
        // TODO: collect parsed strategy info for stream feature
    }
    return ret;
}

auto
FeatureSettingPolicy::
getCaptureP1DmaConfig(
    uint32_t &needP1Dma,
    RequestInputParams const* in,
    uint32_t sensorIndex = 0
) -> bool
{
    bool ret = true;
    // IMGO
    if ((*(in->pConfiguration_StreamInfo_P1))[sensorIndex].pHalImage_P1_Imgo != nullptr) {
        needP1Dma |= P1_IMGO;
    }
    else {
        MY_LOGI("The current pipeline config without IMGO output");
    }
    // RRZO
    if ((*(in->pConfiguration_StreamInfo_P1))[sensorIndex].pHalImage_P1_Rrzo != nullptr) {
        needP1Dma |= P1_RRZO;
    }
    else {
        MY_LOGI("The current pipeline config without RRZO output");
    }
    // LCSO
    if ((*(in->pConfiguration_StreamInfo_P1))[sensorIndex].pHalImage_P1_Lcso != nullptr) {
        needP1Dma |= P1_LCSO;
    }
    else {
        MY_LOGD("The current pipeline config without LCSO output");
    }
    if ( !(needP1Dma & (P1_IMGO|P1_RRZO)) ) {
        MY_LOGW("P1Dma output without source buffer, sensorIndex(%u)", sensorIndex);
        ret = false;
    }
    return ret;
}

auto
FeatureSettingPolicy::
updateRequestResultParams(
    std::shared_ptr<RequestResultParams> &requestParams,
    MetadataPtr pOutMetaApp_Additional,
    MetadataPtr pOutMetaHal_Additional,
    uint32_t needP1Dma,
    uint32_t sensorIndex,
    MINT64 featureCombination,
    MINT32 requestIndex,
    MINT32 requestCount
) -> bool
{
    //FUNCTION_SCOPE;
    auto sensorNum = mPolicyParams.pPipelineStaticInfo->sensorId.size();
    if (sensorIndex >= sensorNum) {
        MY_LOGE("sensorIndex:%u is out of current open sensor num:%zu", sensorIndex, sensorNum);
        return false;
    }
    auto sensorId = mPolicyParams.pPipelineStaticInfo->sensorId[sensorIndex];
    MY_LOGD_IF(2 <= mbDebug, "updateRequestResultParams for sensorIndex:%u (physical sensorId:%d)", sensorIndex, sensorId);
    MetadataPtr pOutMetaApp = std::make_shared<IMetadata>();
    MetadataPtr pOutMetaHal = std::make_shared<IMetadata>();
    if (pOutMetaApp_Additional.get() != nullptr) {
        *pOutMetaApp += *pOutMetaApp_Additional;
    }
    if (pOutMetaHal_Additional.get() != nullptr) {
        *pOutMetaHal += *pOutMetaHal_Additional;
    }
    //check ISP profile
    MUINT8 ispProfile = 0;
    if (IMetadata::getEntry<MUINT8>(pOutMetaHal.get(), MTK_3A_ISP_PROFILE, ispProfile)) {
        MY_LOGD_IF(2 <= mbDebug, "updated isp profile(%d)", ispProfile);
    }
    else {
        MY_LOGD_IF(2 <= mbDebug, "no updated isp profile in pOutMetaHal");
    }
    if (featureCombination) {
        MY_LOGD_IF(2 <= mbDebug, "update featureCombination=%#" PRIx64"", featureCombination);
        IMetadata::setEntry<MINT64>(pOutMetaHal.get(), MTK_FEATURE_CAPTURE, featureCombination);
    }
    if (requestIndex || requestCount) {
        MY_LOGD_IF(2 <= mbDebug, "update MTK_HAL_REQUEST_INDEX=%d, MTK_HAL_REQUEST_COUNT=%d", requestIndex, requestCount);
        IMetadata::setEntry<MINT32>(pOutMetaHal.get(), MTK_HAL_REQUEST_INDEX, requestIndex);
        IMetadata::setEntry<MINT32>(pOutMetaHal.get(), MTK_HAL_REQUEST_COUNT, requestCount);
    }
    if (CC_UNLIKELY(2 <= mbDebug)) {
        (*pOutMetaApp).dump();
        (*pOutMetaHal).dump();
    }
    const MBOOL isMainSensorIndex = (sensorIndex == SENSOR_INDEX_MAIN);
    if (requestParams.get() == nullptr) {
        MY_LOGD_IF(2 <= mbDebug, "no frame setting, create a new one");
        requestParams = std::make_shared<RequestResultParams>();
        // App Metadata, just main sensor has the app metadata
        if(isMainSensorIndex) {
            requestParams->additionalApp = pOutMetaApp;
        };
        // HAl Metadata
        requestParams->additionalHal.push_back(pOutMetaHal);
        //P1Dma requirement
        if (sensorIndex >= requestParams->needP1Dma.size()) {
            MY_LOGD_IF(2 <= mbDebug, "resize needP1Dma size to compatible with sensor index:%u", sensorIndex);
            requestParams->needP1Dma.resize(sensorIndex+1);
        }
        requestParams->needP1Dma[sensorIndex] |= needP1Dma;
    }
    else {
        MY_LOGD_IF(2 <= mbDebug, "frame setting has been created by previous policy, update it");
        // App Metadata, just main sensor has the app metadata
        if (isMainSensorIndex) {
            if (requestParams->additionalApp.get() != nullptr) {
                MY_LOGD_IF(2 <= mbDebug, "[App] append the setting");
                *(requestParams->additionalApp) += *pOutMetaApp;
            }
            else {
                MY_LOGD_IF(2 <= mbDebug, "no app metadata, use the new one");
                requestParams->additionalApp = pOutMetaApp;
            }
        }
        // HAl Metadata
        MY_LOGD_IF(2 <= mbDebug, "[Hal] metadata size(%zu) %d",
                    requestParams->additionalHal.size(),
                    sensorIndex);
        auto additionalHalSize = requestParams->additionalHal.size();
        if(sensorIndex >= additionalHalSize)
        {
            MY_LOGD_IF(2 <= mbDebug, "resize hal meta size to compatible with sensor index:%u", sensorIndex);
            requestParams->additionalHal.resize(sensorIndex+1, nullptr);
            requestParams->additionalHal[sensorIndex] = pOutMetaHal;
        }
        else if (requestParams->additionalHal[sensorIndex].get() != nullptr) {
            MY_LOGD_IF(2 <= mbDebug, "[Hal] append the setting");
            *(requestParams->additionalHal[sensorIndex]) += *pOutMetaHal;
        }
        else
        {
            requestParams->additionalHal[sensorIndex] = pOutMetaHal;
        }
        //P1Dma requirement
        if (sensorIndex >= requestParams->needP1Dma.size()) {
            MY_LOGD_IF(2 <= mbDebug, "resize needP1Dma size to compatible with sensor index:%u", sensorIndex);
            requestParams->needP1Dma.resize(sensorIndex+1);
        }
        requestParams->needP1Dma[sensorIndex] |= needP1Dma;
    }
    return true;
}

auto
FeatureSettingPolicy::
queryPolicyState(
    Policy::State& state,
    uint32_t sensorIndex,
    ParsedStrategyInfo const& parsedInfo,
    RequestOutputParams const* out,
    RequestInputParams const* in
) -> bool
{
    int32_t sensorId = mPolicyParams.pPipelineStaticInfo->sensorId[sensorIndex];

    state.mZslPoolReady = parsedInfo.isZslModeOn;
    state.mZslRequest   = parsedInfo.isZslFlowOn;
    state.mFlashFired   = parsedInfo.isFlashOn;
    state.mExposureTime = parsedInfo.exposureTime;
    state.mRealIso      = parsedInfo.realIso;
    state.mAppManual3A  = parsedInfo.isAppManual3A;
    // check manual 3A setting.
    if (out->mainFrame.get()) {
        IMetadata const* pAppMeta = out->mainFrame->additionalApp.get();
        MUINT8 aeMode = MTK_CONTROL_AE_MODE_ON;
        if (IMetadata::getEntry<MUINT8>(pAppMeta, MTK_CONTROL_AE_MODE, aeMode) &&
            aeMode  == MTK_CONTROL_AE_MODE_OFF)
        {
            MINT32 manualIso = 0;
            MINT64 manualExposureTime = 0;
            if (IMetadata::getEntry<MINT32>(pAppMeta, MTK_SENSOR_SENSITIVITY, manualIso) &&
                IMetadata::getEntry<MINT64>(pAppMeta, MTK_SENSOR_EXPOSURE_TIME, manualExposureTime) &&
                manualIso > 0 && manualExposureTime > 0)
            {
                state.mRealIso        = manualIso;
                state.mExposureTime   = manualExposureTime/1000; //ns to us
                MY_LOGI("capture frame setting has been set as manual iso(%u -> %u)/exposure(%u -> %u)us by previous pluign",
                        parsedInfo.realIso, state.mRealIso, parsedInfo.exposureTime, state.mExposureTime);
            }
            else {
                MY_LOGD("it is not manual iso(%d)/exposure(%" PRId64 " ns)", manualIso, manualExposureTime);
            }
        }
        else {
            MY_LOGD_IF(mbDebug, "it is not manual ae mode(%d)", aeMode);
        }
    }
    else {
        MY_LOGD_IF(mbDebug, "no need to check mainFram info(%p)", out->mainFrame.get());
    }

    // get sensor info (the info is after reconfigure if need)
    state.mSensorMode   = out->sensorMode[sensorIndex];
    uint32_t needP1Dma = 0;
    if (!getCaptureP1DmaConfig(needP1Dma, in, sensorIndex) ){
        MY_LOGE("P1Dma output is invalid: 0x%X", needP1Dma);
        return false;
    }
    HwInfoHelper helper(sensorId);
    if (!helper.updateInfos()) {
        MY_LOGE("HwInfoHelper cannot properly update infos");
        return false;
    }
    //
    uint32_t pixelMode = 0;
    MINT32 sensorFps = 0;
    if (!helper.getSensorSize(state.mSensorMode, state.mSensorSize) ||
        !helper.getSensorFps(state.mSensorMode, sensorFps) ||
        !helper.queryPixelMode(state.mSensorMode, sensorFps, pixelMode)) {
        MY_LOGE("cannot get params about sensor");
        return false;
    }
    //
    int32_t bitDepth = 10;
    helper.getRecommendRawBitDepth(bitDepth);
    //
    MINT format = 0;
    size_t stride = 0;
    if (needP1Dma & P1_IMGO) {
        // use IMGO as source for capture
        if (!helper.getImgoFmt(bitDepth, format) ||
            !helper.alignPass1HwLimitation(pixelMode, format, true/*isImgo*/, state.mSensorSize, stride) ) {
            MY_LOGE("cannot query raw buffer info about imgo");
            return false;
        }
    }
    else {
        // use RRZO as source for capture
        auto rrzoSize = (*(in->pConfiguration_StreamInfo_P1))[sensorIndex].pHalImage_P1_Rrzo->getImgSize();
        MY_LOGW("no IMGO buffer, use RRZO size(%d, %d) as capture source image (for better quality, not suggest to use RRZO to capture)",
                rrzoSize.w, rrzoSize.h);
    }
    MY_LOGD("zslPoolReady:%d, zslRequest:%d, flashFired:%d, appManual3A(%d), exposureTime:%u, realIso:%u",
        state.mZslPoolReady, state.mZslRequest, state.mFlashFired, state.mAppManual3A, state.mExposureTime, state.mRealIso);
    MY_LOGD("sensor(Id:%d, mode:%u, size(%d, %d))",
        sensorId, state.mSensorMode,
        state.mSensorSize.w, state.mSensorSize.h);
    return true;
}

auto
FeatureSettingPolicy::
updatePolicyDecision(
    RequestOutputParams* out,
    uint32_t sensorIndex,
    Policy::Decision const& decision,
    RequestInputParams const* in __unused
) -> bool
{
    out->needZslFlow = decision.mZslEnabled;
    out->zslPolicyParams.mPolicy    |= decision.mZslPolicy.mPolicy;
    out->zslPolicyParams.mTimestamp = decision.mZslPolicy.mTimestamp;
    out->zslPolicyParams.mTimeouts  = decision.mZslPolicy.mTimeouts;
    if (out->needZslFlow) {
        MY_LOGD("update needZslFlow(%d), zsl policy(0x%X), timestamp:%" PRId64 ", timeouts:%" PRId64 "",
                out->needZslFlow, out->zslPolicyParams.mPolicy,
                out->zslPolicyParams.mTimestamp, out->zslPolicyParams.mTimeouts);
    }
    if (decision.mSensorMode != SENSOR_SCENARIO_ID_UNNAMED_START) {
        out->sensorMode[sensorIndex] = decision.mSensorMode;
        MY_LOGD("feature request sensorMode:%d", decision.mSensorMode);
    }

    // update setting for boostControl
    updateBoostControl(out, decision.mBoostControl);

    return true;
}

auto
FeatureSettingPolicy::
updateDualCamRequestOutputParams(
    RequestOutputParams* out,
    MetadataPtr pOutMetaApp_Additional,
    MetadataPtr pOutMetaHal_Additional,
    uint32_t mainCamP1Dma,
    uint32_t sub1CamP1Dma,
    MINT64 featureCombination
) -> bool
{
    FUNCTION_SCOPE;

    if (out->needZslFlow == MTRUE) {
        out->zslPolicyParams.mPolicy |= eZslPolicy_DualFrameSync;
    }
    else {
        MY_LOGI("non-zsl flow, needZslFlow:%d", out->needZslFlow);
    }
    //
    BoostControl boostControl;
    boostControl.boostScenario = IScenarioControlV3::Scenario_ContinuousShot;
    FEATURE_CFG_ENABLE_MASK(boostControl.featureFlag, IScenarioControlV3::FEATURE_STEREO_CAPTURE);
    updateBoostControl(out, boostControl);
    MY_LOGD("update boostControl for DualCam Capture (boostScenario:0x%X, featureFlag:0x%X)",
            boostControl.boostScenario, boostControl.featureFlag);
    // update mainFrame
    // main1 mainFrame
    updateRequestResultParams(
        out->mainFrame,
        pOutMetaApp_Additional,
        pOutMetaHal_Additional,
        mainCamP1Dma,
        SENSOR_INDEX_MAIN,
        featureCombination);
    // main2 mainFrame
    updateRequestResultParams(
        out->mainFrame,
        nullptr, // sub sensor no need to set app metadata
        pOutMetaHal_Additional,
        sub1CamP1Dma,
        SENSOR_INDEX_SUB1,
        featureCombination);
    // update subFrames
    MY_LOGD("update subFrames size(%zu)", out->subFrames.size());
    for (size_t i = 0; i < out->subFrames.size(); i++) {
        auto subFrame = out->subFrames[i];
        if (subFrame.get()) {
            MY_LOGI("subFrames[%zu] has existed(addr:%p)", i,subFrame.get());
            // main1 subFrame
            updateRequestResultParams(
                subFrame,
                pOutMetaApp_Additional,
                pOutMetaHal_Additional,
                mainCamP1Dma,
                SENSOR_INDEX_MAIN,
                featureCombination);
            // main2 subFrame
            updateRequestResultParams(
                subFrame,
                nullptr, // sub sensor no need to set app metadata
                pOutMetaHal_Additional,
                sub1CamP1Dma,
                SENSOR_INDEX_SUB1,
                featureCombination);
        }
        else {
            MY_LOGE("subFrames[%zu] is invalid", i);
        }

    }
    // update preDummyFrames
    MY_LOGD("update preDummyFrames size(%zu)", out->preDummyFrames.size());
    for (size_t i = 0; i < out->preDummyFrames.size(); i++) {
        auto preDummyFrame = out->preDummyFrames[i];
        if (preDummyFrame.get()) {
            MY_LOGE("preDummyFrames[%zu] has existed(addr:%p)",i, preDummyFrame.get());
            // main1 subFrame
            updateRequestResultParams(
                preDummyFrame,
                pOutMetaApp_Additional,
                pOutMetaHal_Additional,
                mainCamP1Dma,
                SENSOR_INDEX_MAIN,
                featureCombination);
            // main2 subFrame
            updateRequestResultParams(
                preDummyFrame,
                nullptr, // sub sensor no need to set app metadata
                pOutMetaHal_Additional,
                sub1CamP1Dma,
                SENSOR_INDEX_SUB1,
                featureCombination);
        }
        else {
            MY_LOGE("preDummyFrames[%zu] is invalid", i);
        }
    }
    // update preDummyFrames
    MY_LOGD("update postDummyFrames size(%zu)", out->postDummyFrames.size());
    for (size_t i = 0; i < out->postDummyFrames.size(); i++) {
        auto postDummyFrame = out->postDummyFrames[i];
        if (postDummyFrame.get()) {
            MY_LOGI("postDummyFrames[%zu] has existed(addr:%p)", i, postDummyFrame.get());
            // main1 subFrame
            updateRequestResultParams(
                postDummyFrame,
                pOutMetaApp_Additional,
                pOutMetaHal_Additional,
                mainCamP1Dma,
                SENSOR_INDEX_MAIN,
                featureCombination);
            // main2 subFrame
            updateRequestResultParams(
                postDummyFrame,
                nullptr, // sub sensor no need to set app metadata
                pOutMetaHal_Additional,
                sub1CamP1Dma,
                SENSOR_INDEX_SUB1,
                featureCombination);
        }
        else
        {
            MY_LOGE("postDummyFrames[%zu] is invalid", i);
        }
    }
    return true;
};

auto
FeatureSettingPolicy::
updateVhdrDummyFrames(
    RequestOutputParams* out,
    RequestInputParams const* in
) -> bool
{
    if(mVhdrInfo.bIsDoCapture == MTRUE && (mVhdrInfo.DummyCount >= 1)){
        for (MINT32 i = 0; i < mVhdrInfo.DummyCount; i++) {
            uint32_t camP1Dma = 0;
            uint32_t sensorIndex = SENSOR_INDEX_MAIN;
            if ( !getCaptureP1DmaConfig(camP1Dma, in, SENSOR_INDEX_MAIN) ){
                MY_LOGE("[vhdrDummyFrames] main P1Dma output is invalid: 0x%X", camP1Dma);
                return MFALSE;
            }

            MetadataPtr pAppDummy_Additional = std::make_shared<IMetadata>();
            MetadataPtr pHalDummy_Additional = std::make_shared<IMetadata>();

            //
            std::shared_ptr<RequestResultParams> preDummyFrame = nullptr;
            updateRequestResultParams(
                      preDummyFrame,
                      pAppDummy_Additional,
                      pHalDummy_Additional,
                      camP1Dma,
                      sensorIndex);
            //
            out->preDummyFrames.push_back(preDummyFrame);
        }
        mVhdrInfo.bIsDoCapture = MFALSE;

        MY_LOGD("[vhdrDummyFrames] stream request frames count(dummycount(%d) mainFrame:%d, subFrames:%zu, preDummyFrames:%zu, postDummyFrames:%zu)",
                mVhdrInfo.DummyCount ,(out->mainFrame.get() != nullptr), out->subFrames.size(),
                 out->preDummyFrames.size(), out->postDummyFrames.size());
    }
    return true;
}

auto
FeatureSettingPolicy::
strategyMultiFramePlugin(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64& foundFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    auto provider = mMFPPluginWrapperPtr->getProvider(combinedKeyFeature, foundFeature);
    if (provider) {
        // for MultiFramePlugin key feature (ex: HDR, MFNR, 3rd party multi-frame algo,etc )
        // negotiate and query feature requirement
        uint32_t mainCamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ){
            MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
            return false;
        }
        auto pAppMetaControl = in->pRequest_AppControl;
        auto property =  provider->property();
        auto pSelection = mMFPPluginWrapperPtr->createSelection();
        MFP_Selection& sel = *pSelection;
        sel.mRequestIndex = 0;
        mMFPPluginWrapperPtr->offer(sel);
        // update app metadata for plugin reference
        MetadataPtr pInMetaApp = std::make_shared<IMetadata>(*pAppMetaControl);
        sel.mIMetadataApp.setControl(pInMetaApp);
        // update previous Hal ouput for plugin reference
        if (out->mainFrame.get()) {
            auto pHalMeta = out->mainFrame->additionalHal[0];
            if (pHalMeta) {
                MetadataPtr pInMetaHal = std::make_shared<IMetadata>(*pHalMeta);
                sel.mIMetadataHal.setControl(pInMetaHal);
            }
        }
        // query state  for plugin provider negotiate
        if (!queryPolicyState(
                    sel.mState,
                    SENSOR_INDEX_MAIN,
                    parsedInfo, out, in)) {
            MY_LOGE("cannot query state for plugin provider negotiate!");
            return false;
        }
        if (provider->negotiate(sel) == OK && sel.mRequestCount > 0) {
            MY_LOGD("MultiFrame request count : %d", sel.mRequestCount);
            if (!updatePolicyDecision( out, SENSOR_INDEX_MAIN, sel.mDecision, in)) {
                MY_LOGW("update config info failed!");
                return false;
            }
            if (CC_LIKELY(sel.mDecision.mProcess)) {
                mMFPPluginWrapperPtr->keepSelection(in->requestNo, provider, pSelection);
            }
            else {
                MY_LOGD("%s(%s) bypass process, only decide frames requirement",
                        mMFPPluginWrapperPtr->getName().c_str(), property.mName);
            }
            MetadataPtr pOutMetaApp_Additional = sel.mIMetadataApp.getAddtional();
            MetadataPtr pOutMetaHal_Additional = sel.mIMetadataHal.getAddtional();
            updateRequestResultParams(
                    out->mainFrame,
                    pOutMetaApp_Additional,
                    pOutMetaHal_Additional,
                    mainCamP1Dma,
                    SENSOR_INDEX_MAIN,
                    featureCombination,
                    0,
                    sel.mRequestCount);

            auto getDummyFrames = [this]
            (
                RequestOutputParams* out,
                MFP_Selection& sel,
                const uint32_t camP1Dma,
                const uint32_t sensorIndex
            ) -> MBOOL
            {
                // get preDummyFrames if the key feature requiresd
                MY_LOGD("preDummyFrames count:%d", sel.mFrontDummy);
                if (sel.mFrontDummy > 0) {
                    for (MINT32 i = 0; i < sel.mFrontDummy; i++) {
                        MetadataPtr pAppDummy_Additional = sel.mIMetadataApp.getDummy();
                        MetadataPtr pHalDummy_Additional = sel.mIMetadataHal.getDummy();
                        std::shared_ptr<RequestResultParams> preDummyFrame = nullptr;
                        updateRequestResultParams(
                                preDummyFrame,
                                pAppDummy_Additional,
                                pHalDummy_Additional,
                                camP1Dma,
                                sensorIndex);
                        //
                        out->preDummyFrames.push_back(preDummyFrame);
                    }
                }
                // get postDummyFrames if the key feature requiresd
                MY_LOGD("postDummyFrames count:%d", sel.mPostDummy);
                if (sel.mPostDummy > 0) {
                    for (MINT32 i = 0; i < sel.mPostDummy; i++) {
                        MetadataPtr pAppDummy_Additional = sel.mIMetadataApp.getDummy();
                        MetadataPtr pHalDummy_Additional = sel.mIMetadataHal.getDummy();
                        std::shared_ptr<RequestResultParams> postDummyFrame = nullptr;
                        updateRequestResultParams(
                                postDummyFrame,
                                pAppDummy_Additional,
                                pHalDummy_Additional,
                                camP1Dma,
                                sensorIndex);
                        //
                        out->postDummyFrames.push_back(postDummyFrame);
                    }
                }
                return true;
            };
            // get the dummy frames if the first main negotiate return the front/rear dummy info.
            getDummyFrames(out, sel, mainCamP1Dma, SENSOR_INDEX_MAIN);
            for (uint32_t i = 1; i < sel.mRequestCount; i++)
            {
                auto pSubSelection = mMFPPluginWrapperPtr->createSelection();
                if (CC_LIKELY(sel.mDecision.mProcess)) {
                    mMFPPluginWrapperPtr->keepSelection(in->requestNo, provider, pSubSelection);
                }
                else {
                    MY_LOGD("%s(%s) bypass process, only decide frames requirement",
                            mMFPPluginWrapperPtr->getName().c_str(), property.mName);
                }
                MFP_Selection& subsel = *pSubSelection;
                subsel.mRequestIndex = i;
                mMFPPluginWrapperPtr->offer(subsel);
                subsel.mIMetadataApp.setControl(pInMetaApp);
                // update previous Hal ouput for plugin reference
                if (out->mainFrame.get()) {
                    auto pHalMeta = out->mainFrame->additionalHal[0];
                    if (pHalMeta) {
                        MetadataPtr pInMetaHal = std::make_shared<IMetadata>(*pHalMeta);
                        sel.mIMetadataHal.setControl(pInMetaHal);
                    }
                }
                provider->negotiate(subsel);
                // add metadata
                pOutMetaApp_Additional = subsel.mIMetadataApp.getAddtional();
                pOutMetaHal_Additional = subsel.mIMetadataHal.getAddtional();
                std::shared_ptr<RequestResultParams> subFrame = nullptr;
                auto subFrameIndex = i - 1;
                if (out->subFrames.size() > subFrameIndex) {
                    MY_LOGI("subFrames size(%zu), subFrames[%d] has existed(addr:%p)",
                            out->subFrames.size(), subFrameIndex, (out->subFrames[subFrameIndex]).get());
                    subFrame = out->subFrames[subFrameIndex];
                    updateRequestResultParams(
                            subFrame,
                            pOutMetaApp_Additional,
                            pOutMetaHal_Additional,
                            mainCamP1Dma,
                            SENSOR_INDEX_MAIN,
                            featureCombination,
                            i,
                            sel.mRequestCount);
                    //
                    out->subFrames[i] = subFrame;
                }
                else {
                    MY_LOGD("subFrames size(%zu), no subFrames[%d], must ceate a new one", out->subFrames.size(), subFrameIndex);
                    updateRequestResultParams(
                            subFrame,
                            pOutMetaApp_Additional,
                            pOutMetaHal_Additional,
                            mainCamP1Dma,
                            SENSOR_INDEX_MAIN,
                            featureCombination,
                            i,
                            sel.mRequestCount);
                    //
                    out->subFrames.push_back(subFrame);
                }
                // get the dummy frames if the subframes negotiate return the front/rear dummy info.
                getDummyFrames(out, subsel, mainCamP1Dma, SENSOR_INDEX_MAIN);
            }
            MY_LOGD("%s(%s), trigger provider(mRequestCount:%d) for foundFeature(%#" PRIx64")",
                    mMFPPluginWrapperPtr->getName().c_str(), property.mName, sel.mRequestCount, foundFeature);
        }
        else {
            MY_LOGD("%s(%s), no need to trigger provider(mRequestCount:%d) for foundFeature(%#" PRIx64")",
                    mMFPPluginWrapperPtr->getName().c_str(), property.mName, sel.mRequestCount, foundFeature);
            return false;
        }
    }
    else
    {
        MY_LOGD_IF(mbDebug, "no provider for multiframe key feature(%#" PRIx64")", combinedKeyFeature);
    }

    return true;
}

auto
FeatureSettingPolicy::
strategySingleRawPlugin(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64& foundFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    auto provider = mRawPluginWrapperPtr->getProvider(combinedKeyFeature, foundFeature);
    if (provider) {
        // for RawPlugin key feature (ex: SW 4Cell) negotiate and query feature requirement
        uint32_t mainCamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ){
            MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
            return false;
        }
        auto pAppMetaControl = in->pRequest_AppControl;
        auto property =  provider->property();
        auto pSelection = mRawPluginWrapperPtr->createSelection();
        Raw_Selection& sel = *pSelection;
        mRawPluginWrapperPtr->offer(sel);
        // update app metadata for plugin reference
        MetadataPtr pInMetaApp = std::make_shared<IMetadata>(*pAppMetaControl);
        sel.mIMetadataApp.setControl(pInMetaApp);
        // update previous Hal ouput for plugin reference
        if (out->mainFrame.get()) {
            auto pHalMeta = out->mainFrame->additionalHal[0];
            if (pHalMeta) {
                MetadataPtr pInMetaHal = std::make_shared<IMetadata>(*pHalMeta);
                sel.mIMetadataHal.setControl(pInMetaHal);
            }
        }
        // query state  for plugin provider strategy
        if (!queryPolicyState(
                    sel.mState,
                    SENSOR_INDEX_MAIN,
                    parsedInfo, out, in)) {
            MY_LOGE("cannot query state for plugin provider negotiate!");
            return false;
        }
        if (provider->negotiate(sel) == OK) {
            if (!updatePolicyDecision( out, SENSOR_INDEX_MAIN, sel.mDecision, in)) {
                MY_LOGW("update config info failed!");
                return false;
            }
            //
            if (CC_LIKELY(sel.mDecision.mProcess)) {
                mRawPluginWrapperPtr->keepSelection(in->requestNo, provider, pSelection);
            }
            else {
                MY_LOGD("%s(%s) bypass process, only decide frames requirement",
                        mRawPluginWrapperPtr->getName().c_str(), property.mName);
            }
            MetadataPtr pOutMetaApp_Additional = sel.mIMetadataApp.getAddtional();
            MetadataPtr pOutMetaHal_Additional = sel.mIMetadataHal.getAddtional();
            updateRequestResultParams(
                    out->mainFrame,
                    pOutMetaApp_Additional,
                    pOutMetaHal_Additional,
                    mainCamP1Dma,
                    SENSOR_INDEX_MAIN,
                    featureCombination);
            //
            MY_LOGD("%s(%s), trigger provider for foundFeature(%#" PRIx64")",
                mRawPluginWrapperPtr->getName().c_str(), property.mName, foundFeature);
        }
        else {
            MY_LOGD("%s(%s), no need to trigger provider for foundFeature(%#" PRIx64")",
                mRawPluginWrapperPtr->getName().c_str(), property.mName, foundFeature);
            return false;
        }
    }
    else
    {
        MY_LOGD_IF(mbDebug, "no provider for single raw key feature(%#" PRIx64")", combinedKeyFeature);
    }

    return true;
}

#if 0
auto
FeatureSettingPolicy::
strategySingleYuvPlugin(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64& foundFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    auto provider = mYuvPluginWrapperPtr->getProvider(combinedKeyFeature, foundFeature);
    if (provider) {
        uint32_t mainCamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ){
            MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
            return false;
        }
        auto pAppMetaControl = in->pRequest_AppControl;
        auto property =  provider->property();
        auto pSelection = mYuvPluginWrapperPtr->createSelection();
        Yuv_Selection& sel = *pSelection;
        mYuvPluginWrapperPtr->offer(sel);
        // update App Metadata ouput for plugin reference
        MetadataPtr pInMetaApp = std::make_shared<IMetadata>(*pAppMetaControl);
        sel.mIMetadataApp.setControl(pInMetaApp);
        // update previous Hal ouput for plugin reference
        if (out->mainFrame.get()) {
            auto pHalMeta = out->mainFrame->additionalHal[0];
            if (pHalMeta) {
                MetadataPtr pInMetaHal = std::make_shared<IMetadata>(*pHalMeta);
                sel.mIMetadataHal.setControl(pInMetaHal);
            }
        }
        // query state  for plugin provider strategy
        if (!queryPolicyState(
                    sel.mState,
                    SENSOR_INDEX_MAIN,
                    parsedInfo, out, in)) {
            MY_LOGE("cannot query state for plugin provider negotiate!");
            return false;
        }
        if (provider->negotiate(sel) == OK) {
            if (!updatePolicyDecision( out, SENSOR_INDEX_MAIN, sel.mDecision, in)) {
                MY_LOGW("update config info failed!");
                return false;
            }
            if (CC_LIKELY(sel.mDecision.mProcess)) {
                mYuvPluginWrapperPtr->keepSelection(in->requestNo, provider, pSelection);
            }
            else {
                MY_LOGD("%s(%s) bypass process, only decide frames requirement",
                        mYuvPluginWrapperPtr->getName().c_str(), property.mName);
            }
            MetadataPtr pOutMetaApp_Additional = sel.mIMetadataApp.getAddtional();
            MetadataPtr pOutMetaHal_Additional = sel.mIMetadataHal.getAddtional();
            updateRequestResultParams(
                    out->mainFrame,
                    pOutMetaApp_Additional,
                    pOutMetaHal_Additional,
                    mainCamP1Dma,
                    SENSOR_INDEX_MAIN,
                    featureCombination);
            //
            MY_LOGD("%s(%s), trigger provider for foundFeature(%#" PRIx64")",
                mYuvPluginWrapperPtr->getName().c_str(), property.mName, foundFeature);
        }
        else {
            MY_LOGD("%s(%s), no need to trigger provider for foundFeature(%#" PRIx64")",
                mYuvPluginWrapperPtr->getName().c_str(), property.mName, foundFeature);
            return false;
        }
    }
    else
    {
        MY_LOGD_IF(mbDebug, "no provider for single yuv key feature(%#" PRIx64")", combinedKeyFeature);
    }

    return true;
}
#endif

auto
FeatureSettingPolicy::
strategyDualCamPlugin(
    MINT64 combinedKeyFeature __unused, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64& foundFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo __unused,
    RequestInputParams const* in
) -> bool
{
    FUNCTION_SCOPE;
    MINT64 depthKeyFeature = 0;
    MINT64 bokehKeyFeature = 0;
    MINT64 fusionKeyFeature = 0;
    const MBOOL isDualCamVSDoFMode = (miMultiCamFeatureMode == MTK_MULTI_CAM_FEATURE_MODE_VSDOF);
    if (isDualCamVSDoFMode) {
        uint32_t mainCamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ) {
            MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
            return false;
        }
        uint32_t sub1CamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(sub1CamP1Dma, in, SENSOR_INDEX_SUB1) ) {
            MY_LOGE("sub1 P1Dma output is invalid: 0x%X", sub1CamP1Dma);
            return false;
        }

        const MBOOL hasFeatureVSDoF = mDepthPluginWraperPtr->isKeyFeatureExisting(featureCombination, depthKeyFeature)
                                    && mBokehPluginWraperPtr->isKeyFeatureExisting(featureCombination, bokehKeyFeature);
        const MBOOL hasFeatureFusion = mFusionPluginWraperPtr->isKeyFeatureExisting(featureCombination, fusionKeyFeature);
        if (hasFeatureVSDoF || hasFeatureFusion) {
            MY_LOGD("update DualCam request output params, depth:%#" PRIx64 ", bokeh:%#" PRIx64 ", fusion:%#" PRIx64,
                depthKeyFeature, bokehKeyFeature, fusionKeyFeature);

            // TODO: update additional metadata for dual cam
            MetadataPtr pOutMetaApp_Additional = std::make_shared<IMetadata>();
            MetadataPtr pOutMetaHal_Additional = std::make_shared<IMetadata>();
            updateDualCamRequestOutputParams(
                            out,
                            pOutMetaApp_Additional,
                            pOutMetaHal_Additional,
                            mainCamP1Dma,
                            sub1CamP1Dma,
                            featureCombination);
        }
    }
    else if(mDualDevicePath == DualDevicePath::MultiCamControl)
    {
        uint32_t mainCamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ) {
            MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
            return false;
        }
        uint32_t sub1CamP1Dma = 0;
        if ( !getCaptureP1DmaConfig(sub1CamP1Dma, in, SENSOR_INDEX_SUB1) ) {
            MY_LOGE("sub1 P1Dma output is invalid: 0x%X", sub1CamP1Dma);
            return false;
        }

        // TODO: update additional metadata for dual cam
        MetadataPtr pOutMetaApp_Additional = std::make_shared<IMetadata>();
        MetadataPtr pOutMetaHal_Additional = std::make_shared<IMetadata>();
        updateDualCamRequestOutputParams(
                        out,
                        pOutMetaApp_Additional,
                        pOutMetaHal_Additional,
                        mainCamP1Dma,
                        sub1CamP1Dma,
                        featureCombination);
    }
    else
    {
        MY_LOGD("doesn't find any feature, not dualcam(vsdof mode/multicam)");
    }
    //
    foundFeature = depthKeyFeature|bokehKeyFeature|fusionKeyFeature;
    if (foundFeature) {
        MY_LOGD("found feature(%#" PRIx64") for dual cam", foundFeature);
    }
    return true;
}

auto
FeatureSettingPolicy::
strategyNormalSingleCapture(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    if (out->subFrames.size() > 0) {
        MY_LOGD_IF(mbDebug, "frames setting has been updated by multiframe plugin");
        return true;
    }

    FUNCTION_SCOPE;

    // general single frame capture's sub feature combination and requirement
    uint32_t mainCamP1Dma = 0;
    if ( !getCaptureP1DmaConfig(mainCamP1Dma, in, SENSOR_INDEX_MAIN) ){
        MY_LOGE("main P1Dma output is invalid: 0x%X", mainCamP1Dma);
        return false;
    }
    // zsl policy for general single frame capture
    if (!parsedInfo.isFlashOn  && !parsedInfo.isAppManual3A &&
        parsedInfo.isZslModeOn && parsedInfo.isZslFlowOn) {
        if (parsedInfo.isCShot) {
            out->needZslFlow = true;
            out->zslPolicyParams.mPolicy = eZslPolicy_None;
            out->zslPolicyParams.mTimeouts  = 1000; //ms
        }
        else if (out->zslPolicyParams.mPolicy == eZslPolicy_None) {
            out->needZslFlow = true;
            out->zslPolicyParams.mPolicy |= eZslPolicy_AfState|eZslPolicy_ZeroShutterDelay;
            out->zslPolicyParams.mTimeouts  = 1000; //ms
        }
        else {
            MY_LOGI("zslPolicyParams has been set by plugins (mPolicy:0x%X, mTimeouts:%" PRId64 "",
                    out->zslPolicyParams.mPolicy, out->zslPolicyParams.mTimeouts);
        }
    }
    else {
        MY_LOGD("not support Zsl due to (isFlashOn:%d, isManual3A:%d, isZslModeOn:%d, isZslFlowOn:%d)",
            parsedInfo.isFlashOn, parsedInfo.isAppManual3A, parsedInfo.isZslModeOn, parsedInfo.isZslFlowOn);
    }
    //
    if (parsedInfo.isCShot) {
        // boot scenario for CShot.
        BoostControl boostControl;
        boostControl.boostScenario = IScenarioControlV3::Scenario_ContinuousShot;
        updateBoostControl(out, boostControl);
        MY_LOGD("update boostControl for CShot (boostScenario:0x%X, featureFlag:0x%X)",
                boostControl.boostScenario, boostControl.featureFlag);
    }

    // update request result (frames metadata)
    updateRequestResultParams(
        out->mainFrame,
        nullptr, /* no additional metadata from provider*/
        nullptr, /* no additional metadata from provider*/
        mainCamP1Dma,
        SENSOR_INDEX_MAIN,
        featureCombination);

    MY_LOGD_IF(mbDebug, "trigger single frame feature:%#" PRIx64", feature combination:%#" PRIx64"",
            combinedKeyFeature, featureCombination);
    return true;
}

auto
FeatureSettingPolicy::
dumpRequestOutputParams(
    RequestOutputParams* out,
    bool forcedEnable = false
) -> bool
{
    // TODO: refactoring for following code
    if (CC_UNLIKELY(mbDebug || forcedEnable)) {
        FUNCTION_SCOPE;
        // dump sensor mode
        for (unsigned int i=0; i<out->sensorMode.size(); i++) {
            MY_LOGD("sensor(index:%d): sensorMode(%d)", i, out->sensorMode[i]);
        }

        // dump boostControl setting
        for (auto& control : out->vboostControl) {
            MY_LOGD("current boostControl(size:%zu): boostScenario(0x%X), featureFlag(0x%X)",
                    out->vboostControl.size(), control.boostScenario, control.featureFlag);
        }

        MY_LOGD("ZslPolicyParams, mPolicy:0x%X, mTimestamp:%" PRId64 ", mTimeouts:%" PRId64 "",
            out->zslPolicyParams.mPolicy, out->zslPolicyParams.mTimestamp, out->zslPolicyParams.mTimeouts);

        // dump frames count
        MY_LOGD("capture request frames count(mainFrame:%d, subFrames:%zu, preDummyFrames:%zu, postDummyFrames:%zu)",
                (out->mainFrame.get() != nullptr), out->subFrames.size(),
                out->preDummyFrames.size(), out->postDummyFrames.size());

        // dump MTK_FEATURE_CAPTURE info
        MINT64 featureCombination = 0;
        if (out->mainFrame.get() && IMetadata::getEntry<MINT64>(out->mainFrame->additionalHal[0].get(), MTK_FEATURE_CAPTURE, featureCombination)) {
            MY_LOGD("mainFrame featureCombination=%#" PRIx64"", featureCombination);
        }
        else {
            MY_LOGW("mainFrame w/o featureCombination");
        }

        if(out->mainFrame.get()) {
            for(size_t index = 0; index < out->mainFrame->needP1Dma.size(); index++) {
                MY_LOGD("needP1Dma, index:%zu, value:%d", index, out->mainFrame->needP1Dma[index]);
            }
            for(size_t index = 0; index < out->mainFrame->additionalHal.size(); index++) {
                MY_LOGD("dump addition hal metadata for index:%zu, count:%u", index, out->mainFrame->additionalHal[index]->count());
                out->mainFrame->additionalHal[index]->dump();
            }
            MY_LOGD("dump addition app metadata");
            out->mainFrame->additionalApp->dump();
        }
        else
        {
            MY_LOGE("failed to get main fram");
        }

        featureCombination = 0;
        for (size_t i = 0; i < out->subFrames.size(); i++) {
            auto subFrame = out->subFrames[i];
            if (subFrame.get() && IMetadata::getEntry<MINT64>(subFrame->additionalHal[0].get(), MTK_FEATURE_CAPTURE, featureCombination)) {
                MY_LOGD("subFrame[%zu] featureCombination=%#" PRIx64"", i, featureCombination);
            }
            else {
                MY_LOGW("subFrame[%zu] w/o featureCombination=%#" PRIx64"", i, featureCombination);
            }
        }

        // reconfig & zsl info.
        MY_LOGD("needReconfiguration:%d, zsl(need:%d, policy:0x%X, timestamp:%" PRId64 ", timeouts:%" PRId64 ")",
                out->needReconfiguration, out->needZslFlow, out->zslPolicyParams.mPolicy, out->zslPolicyParams.mTimestamp, out->zslPolicyParams.mTimeouts);
    }
    return true;
}

auto
FeatureSettingPolicy::
updatePluginSelection(
    bool isFeatureTrigger
) -> bool
{
    const MBOOL isDualCamVSDoFMode = (miMultiCamFeatureMode == MTK_MULTI_CAM_FEATURE_MODE_VSDOF);
    if (isFeatureTrigger) {
        mMFPPluginWrapperPtr->pushSelection();
        mRawPluginWrapperPtr->pushSelection();
        mYuvPluginWrapperPtr->pushSelection();
        if (isDualCamVSDoFMode) {
            mBokehPluginWraperPtr->pushSelection();
            mDepthPluginWraperPtr->pushSelection();
            mFusionPluginWraperPtr->pushSelection();
        }
    }
    else {
        mMFPPluginWrapperPtr->cancel();
        mRawPluginWrapperPtr->cancel();
        mYuvPluginWrapperPtr->cancel();
        if (isDualCamVSDoFMode) {
            mBokehPluginWraperPtr->cancel();
            mDepthPluginWraperPtr->cancel();
            mFusionPluginWraperPtr->cancel();
        }
    }

    return true;
}

auto
FeatureSettingPolicy::
strategyCaptureFeature(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    MINT64 featureCombination, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    FUNCTION_SCOPE;
    MY_LOGD("strategy for combined key feature(%#" PRIx64"), feature combination(%#" PRIx64")",
            combinedKeyFeature, featureCombination);

    if (CC_UNLIKELY(mForcedKeyFeatures >= 0)) {
        combinedKeyFeature = mForcedKeyFeatures;
        MY_LOGW("forced key feature(%#" PRIx64")", combinedKeyFeature);
    }
    if (CC_UNLIKELY(mForcedFeatureCombination >= 0)) {
        featureCombination = mForcedFeatureCombination;
        MY_LOGW("forced feature combination(%#" PRIx64")", featureCombination);
    }

    RequestOutputParams temp_out;
    if (out->mainFrame.get()) {
        MY_LOGI("clear previous invalid frames setting");
        out->mainFrame = nullptr;
        out->subFrames.clear();
        out->preDummyFrames.clear();
        out->postDummyFrames.clear();
    }
    temp_out = *out;
    //
    MINT64 foundFeature = 0;
    if (combinedKeyFeature) { /* not MTK_FEATURE_NORMAL */
        MINT64 checkFeatures = combinedKeyFeature;
        //
        do {
            if (!strategySingleRawPlugin(checkFeatures, featureCombination, foundFeature, &temp_out, parsedInfo, in)) {
                MY_LOGD("no need to trigger feature(%#" PRIx64") for features(key:%#" PRIx64", combined:%#" PRIx64")",
                        foundFeature, combinedKeyFeature, featureCombination);
                return false;
            }
            checkFeatures &= ~foundFeature;
        } while (foundFeature && checkFeatures); // to find next raw plugin until no foundfeature(==0)
        //
        MY_LOGD_IF(checkFeatures, "continue to find next plugin for %#" PRIx64"", checkFeatures);
        //
        if (!strategyMultiFramePlugin(checkFeatures, featureCombination, foundFeature, &temp_out, parsedInfo, in)) {
            MY_LOGD("no need to trigger feature(%#" PRIx64") for features(key:%#" PRIx64", combined:%#" PRIx64")",
                    foundFeature, combinedKeyFeature, featureCombination);
            return false;
        }
        checkFeatures &= ~foundFeature;
        //
        if (checkFeatures) {
            MY_LOGD("some key features(%#" PRIx64") still not found for features(%#" PRIx64")",
                    checkFeatures, combinedKeyFeature);
            return false;
        }
    }
    else {
        MY_LOGI("no combinated key feature, use default normal single capture");
    }
    // update basic requirement
    if (!strategyNormalSingleCapture(combinedKeyFeature, featureCombination, &temp_out, parsedInfo, in)) {
        MY_LOGW("update capture setting failed!");
        return false;
    }
    //
    if (parsedInfo.isCShot || parsedInfo.isYuvReprocess || parsedInfo.isRawReprocess) {
        MY_LOGD("no need dummy frames, isCShot(%d), isYuvReprocess(%d), isRawReprocess(%d)",
                parsedInfo.isCShot, parsedInfo.isYuvReprocess, parsedInfo.isRawReprocess);
    }
    else { // check and update dummy frames requirement for perfect 3A stable...
        updateCaptureDummyFrames(combinedKeyFeature, &temp_out, parsedInfo, in);
    }
    //
    if (mPolicyParams.pPipelineStaticInfo->isDualDevice) { // dual cam device only
        if (!strategyDualCamPlugin(combinedKeyFeature, featureCombination, foundFeature, &temp_out, parsedInfo, in)) {
            MY_LOGD("no need to trigger feature(%#" PRIx64") for features(key:%#" PRIx64", combined:%#" PRIx64")",
                    foundFeature, combinedKeyFeature, featureCombination);
            return false;
        }
    }

    // update result
    *out = temp_out;

    return true;
}

auto
FeatureSettingPolicy::
updateCaptureDummyFrames(
    MINT64 combinedKeyFeature, /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    RequestOutputParams* out,
    const ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> void
{
    FUNCTION_SCOPE;

    int8_t preDummyCount = 0;
    int8_t postDummyCount = 0;

    if (out->preDummyFrames.size() || out->postDummyFrames.size()) {
        MY_LOGI("feature(%#" PRIx64") has choose dummy frames(pre:%zu, post:%zu)",
                combinedKeyFeature, out->preDummyFrames.size(), out->postDummyFrames.size());
        return;
    }
    //
    auto hal3a = mHal3a;
    if (hal3a.get() == nullptr) {
        MY_LOGW("cannot get hal3a, it is nullptr!");
        return;
    }

    // lambda for choose maximum count
    auto updateDummyCount = [&preDummyCount, &postDummyCount]
    (
        int8_t preCount,
        int8_t postCount
    ) -> void
    {
        preDummyCount = std::max(preDummyCount, preCount);
        postDummyCount = std::max(postDummyCount, postCount);
    };

    // lambda to check manual 3A
    auto isManual3aSetting = []
    (
        IMetadata const* pAppMeta,
        IMetadata const* pHalMeta
    ) -> bool
    {
        if (pAppMeta && pHalMeta) {
            // check manual AE (method.1)
            MUINT8 aeMode = MTK_CONTROL_AE_MODE_ON;
            if (IMetadata::getEntry<MUINT8>(pAppMeta, MTK_CONTROL_AE_MODE, aeMode)) {
                if (aeMode == MTK_CONTROL_AE_MODE_OFF) {
                    MY_LOGD("get MTK_CONTROL_AE_MODE(%d), it is manual AE", aeMode);
                    return true;
                }
            }
            // check manual AE (method.2)
            IMetadata::Memory capParams;
            capParams.resize(sizeof(CaptureParam_T));
            if (IMetadata::getEntry<IMetadata::Memory>(pHalMeta, MTK_3A_AE_CAP_PARAM, capParams)) {
                MY_LOGD("get MTK_3A_AE_CAP_PARAM, it is manual AE");
                return true;
            }
            // check manual AW
            MUINT8 awLock = MFALSE;
            IMetadata::getEntry<MUINT8>(pAppMeta, MTK_CONTROL_AWB_LOCK, awLock);
            if (awLock) {
                MY_LOGD("get MTK_CONTROL_AWB_LOCK(%d), it is manual AE", awLock);
                return true;
            }
        }
        else {
            MY_LOGW("no metadata(app:%p, hal:%p) to query hint", pAppMeta, pHalMeta);
        }

        return false;
    };
    //
    bool bIsManual3A = false;
    if (CC_LIKELY(out->mainFrame.get())) {
        IMetadata const* pAppMeta = out->mainFrame->additionalApp.get();
        IMetadata const* pHalMeta = out->mainFrame->additionalHal[SENSOR_INDEX_MAIN].get();
        bIsManual3A = isManual3aSetting(pAppMeta, pHalMeta);
    }
    else {
        MY_LOGD("no metadata info due to no mainFrame");
    }
    //
    if (bIsManual3A) {
        // get manual 3a delay frames count from 3a hal
        MUINT32 delayedFrames = 0;
        {
            std::lock_guard<std::mutex> _l(mHal3aLocker);
            hal3a->send3ACtrl(E3ACtrl_GetCaptureDelayFrame, reinterpret_cast<MINTPTR>(&delayedFrames), 0);
        }
        MY_LOGD("delayedFrames count:%d due to manual 3A", delayedFrames);
        //
        updateDummyCount(0, delayedFrames);
    }

    if (parsedInfo.isFlashOn) {
        MUINT32 cntBefore = 0;
        MUINT32 cntAfter = 0;
        hal3a->send3ACtrl(E3ACtrl_GetFlashCapDummyCnt, reinterpret_cast<MINTPTR>(&cntBefore), reinterpret_cast<MINTPTR>(&cntAfter));
        updateDummyCount(cntBefore, cntAfter);
        MY_LOGD("dummy frames count(pre:%d, post:%d) due to flash on", cntBefore, cntAfter);
    }
    MY_LOGD("dummy frames result(pre:%d, post:%d)", preDummyCount, postDummyCount);

    uint32_t camP1Dma = 0;
    uint32_t sensorIndex = SENSOR_INDEX_MAIN;
    if ( !getCaptureP1DmaConfig(camP1Dma, in, SENSOR_INDEX_MAIN) ){
        MY_LOGE("main P1Dma output is invalid: 0x%X", camP1Dma);
        return;
    }

    // update preDummyFrames
    for (MINT32 i = 0; i < preDummyCount; i++) {
        MetadataPtr pAppDummy_Additional = std::make_shared<IMetadata>();
        MetadataPtr pHalDummy_Additional = std::make_shared<IMetadata>();
        // update info for pre-dummy frames for flash stable
        IMetadata::setEntry<MUINT8>(pAppDummy_Additional.get(), MTK_CONTROL_AE_MODE, MTK_CONTROL_AE_MODE_OFF);
        IMetadata::setEntry<MINT64>(pAppDummy_Additional.get(), MTK_SENSOR_EXPOSURE_TIME, 33333333);
        IMetadata::setEntry<MINT32>(pAppDummy_Additional.get(), MTK_SENSOR_SENSITIVITY, 1000);
        //
        std::shared_ptr<RequestResultParams> preDummyFrame = nullptr;
        updateRequestResultParams(
                preDummyFrame,
                pAppDummy_Additional,
                pHalDummy_Additional,
                camP1Dma,
                sensorIndex);
        //
        out->preDummyFrames.push_back(preDummyFrame);
    }

    // update postDummyFrames
    for (MINT32 i = 0; i < postDummyCount; i++) {
        MetadataPtr pAppDummy_Additional = std::make_shared<IMetadata>();
        MetadataPtr pHalDummy_Additional = std::make_shared<IMetadata>();
        // update info for post-dummy(delay) frames to restore 3A for preview stable
        IMetadata::setEntry<MBOOL>(pHalDummy_Additional.get(), MTK_3A_AE_RESTORE_CAPPARA, 1);
        //
        std::shared_ptr<RequestResultParams> postDummyFrame = nullptr;
        updateRequestResultParams(
                postDummyFrame,
                pAppDummy_Additional,
                pHalDummy_Additional,
                camP1Dma,
                sensorIndex);
        //
        out->postDummyFrames.push_back(postDummyFrame);
    }

    // check result
    if (out->preDummyFrames.size() || out->postDummyFrames.size()) {
        MY_LOGI("feature(%#" PRIx64") append dummy frames(pre:%zu, post:%zu) due to isFlashOn(%d), isManual3A(%d)",
                combinedKeyFeature, out->preDummyFrames.size(), out->postDummyFrames.size(),
                parsedInfo.isFlashOn, bIsManual3A);

        if (out->needZslFlow) {
            MY_LOGW("not support Zsl buffer due to isFlashOn(%d) or isManual3A(%d)", parsedInfo.isFlashOn, bIsManual3A);
            out->needZslFlow = false;
        }
    }

    return;
}

auto
FeatureSettingPolicy::
toTPIDualHint(
    ScenarioHint &hint
) -> void
{
    hint.isDualCam = mPolicyParams.pPipelineStaticInfo->isDualDevice;
    hint.isVSDoFMode = (miMultiCamFeatureMode == MTK_MULTI_CAM_FEATURE_MODE_VSDOF);
    int stereoMode = StereoSettingProvider::getStereoFeatureMode();
    if( hint.isDualCam )
    {
        if( stereoMode & v1::Stereo::E_STEREO_FEATURE_VSDOF )
            hint.mDualFeatureMode = DualFeatureMode_MTK_VSDOF;
        else if( stereoMode & v1::Stereo::E_STEREO_FEATURE_MTK_DEPTHMAP )
            hint.mDualFeatureMode = DualFeatureMode_HW_DEPTH;
        else if( stereoMode & v1::Stereo::E_STEREO_FEATURE_THIRD_PARTY )
            hint.mDualFeatureMode = DualFeatureMode_YUV;
        else
            hint.mDualFeatureMode = DualFeatureMode_NONE;
    }
}

auto
FeatureSettingPolicy::
updateBoostControl(
    RequestOutputParams* out,
    const BoostControl& boostControl
) -> bool
{
    bool bUpdated = false;
    bool ret = true;

    if (boostControl.boostScenario != IScenarioControlV3::Scenario_None) { // check valid
        for (auto& control : out->vboostControl) {
            if (control.boostScenario == boostControl.boostScenario) {
                int32_t originalFeatureFlag = control.featureFlag;
                control.featureFlag |= boostControl.featureFlag;
                MY_LOGD("update boostControl(size:%zu): boostScenario(0x%X), featureFlag(0x%X|0x%X => 0x%X)",
                        out->vboostControl.size(), control.boostScenario, originalFeatureFlag, boostControl.featureFlag, control.featureFlag);
                bUpdated = true;
            }
            else {
                MY_LOGD_IF(mbDebug, "current boostControl(size:%zu): boostScenario(0x%X), featureFlag(0x%X)",
                        out->vboostControl.size(), control.boostScenario, control.featureFlag);
            }
        }
        //
        if (!bUpdated) {
            out->vboostControl.push_back(boostControl);
            MY_LOGD("add boostControl(size:%zu): boostScenario(0x%X), featureFlag(0x%X)",
                    out->vboostControl.size(), boostControl.boostScenario, boostControl.featureFlag);
        }
        ret = true;
    }
    else {
        if (boostControl.featureFlag != IScenarioControlV3::FEATURE_NONE) {
            MY_LOGW("invalid boostControl(boostScenario:0x%X, featureFlag:0x%X)",
                    boostControl.boostScenario, boostControl.featureFlag);
            ret = false;
        }
        else {
            MY_LOGD_IF(mbDebug, "no need to update boostControl(boostScenario:0x%X, featureFlag:0x%X)",
                    boostControl.boostScenario, boostControl.featureFlag);
            ret = true;
        }
    }

    return ret;
}

auto
FeatureSettingPolicy::
evaluateCaptureSetting(
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo,
    RequestInputParams const* in
) -> bool
{
    FUNCTION_SCOPE;

    MY_LOGD("capture req#:%u", in->requestNo);

    ScenarioFeatures scenarioFeatures;
    CaptureScenarioConfig scenarioConfig;
    ScenarioHint scenarioHint;
    toTPIDualHint(scenarioHint);
    scenarioHint.operationMode = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration->operationMode;
    scenarioHint.isCShot = parsedInfo.isCShot;
    scenarioHint.isYuvReprocess = parsedInfo.isYuvReprocess;
    scenarioHint.isRawReprocess = parsedInfo.isRawReprocess;
    //TODO:
    //scenarioHint.captureScenarioIndex = ? /* hint from vendor tag */
    int32_t openId = mPolicyParams.pPipelineStaticInfo->openId;
    auto pAppMetadata = in->pRequest_AppControl;

    int32_t scenario = -1;
    if (!get_capture_scenario(scenario, scenarioFeatures, scenarioConfig, openId, scenarioHint, pAppMetadata)) {
        MY_LOGE("cannot get capture scenario openId:%d", openId);
        return false;
    }
    else {
        MY_LOGD("find scenario:%s for (openId:%d, scenario:%d)",
                scenarioFeatures.scenarioName.c_str(), openId, scenario);
    }

    bool isFeatureTrigger = false;
    for (auto featureSet : scenarioFeatures.vFeatureSet) {
        // evaluate key feature plugin and feature combination for feature strategy policy.
        if (strategyCaptureFeature( featureSet.feature, featureSet.featureCombination, out, parsedInfo, in )) {
            isFeatureTrigger = true;
            MY_LOGI("trigger feature:%s(%#" PRIx64"), feature combination:%s(%#" PRIx64")",
                    featureSet.featureName.c_str(),
                    static_cast<MINT64>(featureSet.feature),
                    featureSet.featureCombinationName.c_str(),
                    static_cast<MINT64>(featureSet.featureCombination));
            updatePluginSelection(isFeatureTrigger);
            break;
        }
        else{
            isFeatureTrigger = false;
            MY_LOGD("no need to trigger feature:%s(%#" PRIx64"), feature combination:%s(%#" PRIx64")",
                    featureSet.featureName.c_str(),
                    static_cast<MINT64>(featureSet.feature),
                    featureSet.featureCombinationName.c_str(),
                    static_cast<MINT64>(featureSet.featureCombination));
            updatePluginSelection(isFeatureTrigger);
        }
    }
    dumpRequestOutputParams(out, true);

    if (!isFeatureTrigger) {
        MY_LOGE("no feature can be triggered!");
        return false;
    }

    MY_LOGD("capture request frames count(mainFrame:%d, subFrames:%zu, preDummyFrames:%zu, postDummyFrames:%zu)",
            (out->mainFrame.get() != nullptr), out->subFrames.size(),
            out->preDummyFrames.size(), out->postDummyFrames.size());
    return true;
}

auto
FeatureSettingPolicy::
isNeedIsoReconfig(
    HDRMode* apphdrMode,
    uint32_t recodingMode
) -> bool
{
    {

        if(recodingMode == MTK_FEATUREPIPE_VIDEO_RECORD)
        {
            if(mVhdrInfo.IsoSwitchModeStatus == eSwitchMode_LowLightLvMode)
                *apphdrMode = HDRMode::OFF;

            MY_LOGD("Has Recording and  no need iso reconfig recodingMode(%d)IsoSwitchModeStatus(%d) apphdrMode(%hhu)",
                recodingMode, mVhdrInfo.IsoSwitchModeStatus, *apphdrMode);

            return true;
        }

        auto hal3a = mHal3a;
        if (hal3a.get())
        {
            int iIsoThresholdStable1 = -1;   //for low iso (ex: 2800)
            int iIsoThresholdStable2 = -1;   //for high iso (ex: 5600)
            {
                std::lock_guard<std::mutex> _l(mHal3aLocker);
                hal3a->send3ACtrl(E3ACtrl_GetISOThresStatus, (MINTPTR)&iIsoThresholdStable1, (MINTPTR)&iIsoThresholdStable2);
            }

            MY_LOGD_IF(mVhdrInfo.bVhdrDebugMode, "Iso-reconfig status: apphdrMode(%hhu) isothreshold(%d,%d) recording(%d)",
                *apphdrMode, iIsoThresholdStable1, iIsoThresholdStable2, recodingMode);

            if(mVhdrInfo.IsoSwitchModeStatus == eSwitchMode_HighLightMode)
            {
                // when 3HDR feature on (from AP setting)
                // original enviroment : high light (low iso) (binning mode)
                // new enviroment : low light (high iso) and stable
                // need to change to binning mode (from 3hdr mode)
                // if iIsoThresholdStable2 == 0: now iso bigger than ISO 5600 & stable
                if(iIsoThresholdStable2 == 0) //orginal:3hdr mode, so need to check iIsoThresholdStable2 for ISO5600
                {
                    mVhdrInfo.IsoSwitchModeStatus = eSwitchMode_LowLightLvMode;
                    *apphdrMode = HDRMode::OFF;
                    MY_LOGD("need Iso-reconfig: IsoSwitchModeStatus_h->l (%d) isothreshold(%d,%d) apphdrMode(%hhu)",
                        mVhdrInfo.IsoSwitchModeStatus, iIsoThresholdStable1, iIsoThresholdStable2, *apphdrMode);
                }
            }
            else if(mVhdrInfo.IsoSwitchModeStatus == eSwitchMode_LowLightLvMode)
            {
                // when 3HDR feature on (from AP setting)
                // original enviroment : low light (high iso) (binning mode)
                // new enviroment : high light (low iso) and stable
                // need to change to 3hdr mode (from binning mode)
                // if iIsoThresholdStable1 == 1: now iso smaller than ISO 2800 & stable
                *apphdrMode = HDRMode::OFF;
                if(iIsoThresholdStable1 == 1) //orginal:binning mode, so need to check iIsoThresholdStable1 for ISO2800 (1:smaller than 2800)
                {
                    mVhdrInfo.IsoSwitchModeStatus = eSwitchMode_HighLightMode;
                    *apphdrMode = HDRMode::ON;
                    MY_LOGD("need Iso-reconfig: IsoSwitchModeStatus_l->h (%d) isothreshold(%d,%d) apphdrMode(%hhu)",
                        mVhdrInfo.IsoSwitchModeStatus, iIsoThresholdStable1, iIsoThresholdStable1, *apphdrMode);
                }
            }
            else
            {
                    mVhdrInfo.IsoSwitchModeStatus = eSwitchMode_HighLightMode;
                    MY_LOGD("not need Iso-reconfig: IsoSwitchModeStatus_Undefined (%d) isothreshold(%d,%d) apphdrMode(%hhu)",
                        mVhdrInfo.IsoSwitchModeStatus, iIsoThresholdStable1, iIsoThresholdStable1, *apphdrMode);
            }
            return true;
        }
        else
        {
            MY_LOGE("create IHal3A instance failed! cannot get current real iso for strategy");
            mVhdrInfo.IsoSwitchModeStatus = eSwitchMode_HighLightMode;
            return false;
        }
    }
}

auto
FeatureSettingPolicy::
updateStreamData(
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo __unused,
    RequestInputParams const* in
) -> bool
{
    //FUNCTION_SCOPE;
    // 1. Update stream state : decide App Mode
    int32_t recordState = -1;
    uint32_t AppMode = 0;
    bool isRepeating = in->pRequest_ParsedAppMetaControl->repeating;
    IMetadata const* pAppMetaControl = in->pRequest_AppControl;
    if (IMetadata::getEntry<MINT32>(pAppMetaControl, MTK_STREAMING_FEATURE_RECORD_STATE, recordState) )
    {   // App has set recordState Tag
        if(recordState == MTK_STREAMING_FEATURE_RECORD_STATE_PREVIEW)
        {
            if(in->pRequest_AppImageStreamInfo->hasVideoConsumer)
                AppMode = MTK_FEATUREPIPE_VIDEO_STOP;
            else
                AppMode = MTK_FEATUREPIPE_VIDEO_PREVIEW;
        }
        else
        {
            AppMode = mConfigOutputParams.StreamingParams.mLastAppInfo.appMode;
            MY_LOGW("Unknown or Not Supported app recordState(%d), use last appMode=%d",
                    recordState, mConfigOutputParams.StreamingParams.mLastAppInfo.appMode);
        }
    }
    else
    {   // App has NOT set recordState Tag
        // (slow motion has no repeating request)
        // no need process slow motion because use different policy?
        if( isRepeating /*|| isHighspped*/)
        {
            if(in->pRequest_AppImageStreamInfo->hasVideoConsumer)
                AppMode = MTK_FEATUREPIPE_VIDEO_RECORD;
            else if (in->Configuration_HasRecording)
                AppMode = MTK_FEATUREPIPE_VIDEO_PREVIEW;
            else
                AppMode = MTK_FEATUREPIPE_PHOTO_PREVIEW;
        }
        else
        {
            AppMode = mConfigOutputParams.StreamingParams.mLastAppInfo.appMode;
        }
    }

    MetadataPtr pOutMetaHal = std::make_shared<IMetadata>();
    IMetadata::setEntry<MINT32>(pOutMetaHal.get(), MTK_FEATUREPIPE_APP_MODE, AppMode);
    // 2. Update EIS data
    auto isEISOn = [&] (void) -> bool
    {
        MUINT8 appEisMode = 0;
        MINT32 advEisMode = 0;
        if( ( IMetadata::getEntry<MUINT8>(pAppMetaControl, MTK_CONTROL_VIDEO_STABILIZATION_MODE, appEisMode) &&
              appEisMode == MTK_CONTROL_VIDEO_STABILIZATION_MODE_ON ) ||
            ( IMetadata::getEntry<MINT32>(pAppMetaControl, MTK_EIS_FEATURE_EIS_MODE, advEisMode) &&
                 advEisMode == MTK_EIS_FEATURE_EIS_MODE_ON ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    };
    if( isEISOn() )
    {
        IMetadata::setEntry<MSize>(pOutMetaHal.get(), MTK_EIS_VIDEO_SIZE, in->pRequest_AppImageStreamInfo->videoImageSize);
    }

    if( AppMode != mConfigOutputParams.StreamingParams.mLastAppInfo.appMode ||
        recordState != mConfigOutputParams.StreamingParams.mLastAppInfo.recordState ||
        isEISOn() != mConfigOutputParams.StreamingParams.mLastAppInfo.eisOn )
    {
        MY_LOGI("AppInfo changed:appMode(%d=>%d),recordState(%d=>%d),eisOn(%d=>%d)",
                mConfigOutputParams.StreamingParams.mLastAppInfo.appMode, AppMode,
                mConfigOutputParams.StreamingParams.mLastAppInfo.recordState, recordState,
                mConfigOutputParams.StreamingParams.mLastAppInfo.eisOn, isEISOn());
        mConfigOutputParams.StreamingParams.mLastAppInfo.appMode = AppMode;
        mConfigOutputParams.StreamingParams.mLastAppInfo.recordState = recordState;
        mConfigOutputParams.StreamingParams.mLastAppInfo.eisOn = isEISOn();
    }

    // 2. Decide override timestamp mechanism or not
    if( mConfigOutputParams.StreamingParams.bEnableTSQ )
    {
        //MBOOL needOverrideTime = isEisQEnabled(pipelineParam.currentAdvSetting);
        IMetadata::setEntry<MBOOL>(pOutMetaHal.get(), MTK_EIS_NEED_OVERRIDE_TIMESTAMP, 1);
        //MY_LOGD("TSQ ON");
    }
    uint32_t needP1Dma = 0;
    if ((*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Rrzo != nullptr)
    {
        needP1Dma |= P1_RRZO;
    }
    auto needImgo = [this] (MSize imgSize, MSize rrzoSize) -> int
    {
        // if isZslMode=true, must output IMGO for ZSL buffer pool
        return (imgSize.w > rrzoSize.w) || (imgSize.h > rrzoSize.h) || mConfigInputParams.isZslMode;
    };
    if ((*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Imgo != nullptr &&
        needImgo(in->maxP2StreamSize, (*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Rrzo->getImgSize()))
    {
        needP1Dma |= P1_IMGO;
    }
    if ((*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Lcso != nullptr)
    {
        needP1Dma |= P1_LCSO;
    }
    if ((*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Rsso != nullptr)
    {
        needP1Dma |= P1_RSSO;
    }
    // SMVR
    if (mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration->isConstrainedHighSpeedMode)
    {
        IMetadata::IEntry const& entry = pAppMetaControl->entryFor(MTK_CONTROL_AE_TARGET_FPS_RANGE);
        if  ( entry.isEmpty() )
        {
            MY_LOGW("no MTK_CONTROL_AE_TARGET_FPS_RANGE");
        }
        else
        {
            MINT32 i4MinFps = entry.itemAt(0, Type2Type< MINT32 >());
            MINT32 i4MaxFps = entry.itemAt(1, Type2Type< MINT32 >());
            MINT32 postDummyReqs = i4MinFps == 30 ? (i4MaxFps/i4MinFps -1) : 0;
            MUINT8 fps = i4MinFps == 30  ? MTK_SMVR_FPS_30 :
                         i4MinFps == 120 ? MTK_SMVR_FPS_120 :
                         i4MinFps == 240 ? MTK_SMVR_FPS_240 :
                         i4MinFps == 480 ? MTK_SMVR_FPS_480 :
                         i4MinFps == 960 ? MTK_SMVR_FPS_960 : MTK_SMVR_FPS_30;

            MY_LOGD_IF(2 <= mbDebug, "SMVR: i4MinFps=%d, i4MaxFps=%d, postDummyReqs=%d",
                i4MinFps, i4MaxFps, postDummyReqs);

            IMetadata::setEntry<MUINT8>(pOutMetaHal.get(), MTK_HAL_REQUEST_SMVR_FPS, fps);
            if (postDummyReqs)
            {
                std::shared_ptr<RequestResultParams> postDummyFrame = nullptr;
                updateRequestResultParams(postDummyFrame, nullptr, nullptr, needP1Dma, SENSOR_INDEX_MAIN);
                for (MINT32 i = 0 ; i < postDummyReqs ; i++) {
                    out->postDummyFrames.push_back(postDummyFrame);
                }
            }
        }
    }

    //vhdr config proflie and MTK_3A_HDR_MODE
    MINT32 fMask = ProfileParam::FMASK_NONE;
    MUINT32 vhdrMode = SENSOR_VHDR_MODE_NONE;
    MUINT8 profile = 0;
    HDRMode apphdrMode = HDRMode::OFF;
    MINT32  apphdrModeInt = 0;
    MINT32 forceAppHdrMode = mVhdrInfo.bVhdrDebugMode ?
        ::property_get_int32("vendor.debug.camera.hal3.appHdrMode", DEBUG_APP_HDR) : DEBUG_APP_HDR;

    if(isEISOn())
    {
        fMask |= ProfileParam::FMASK_EIS_ON;
    }

    if( IMetadata::getEntry<MINT32>(pAppMetaControl, MTK_HDR_FEATURE_HDR_MODE, apphdrModeInt))
    {
        mVhdrInfo.UiAppHdrMode = static_cast<HDRMode>((MUINT8)apphdrModeInt);
    }
    else
    {
        MY_LOGD("Get UiAppMeta:hdrMode Fail ");
    }

    auto isUiVhdrOn = [&] (void) -> bool
    {
        if(mVhdrInfo.bVhdrDebugMode && forceAppHdrMode >= 0)
        {
            apphdrMode = static_cast<HDRMode>((MUINT8)forceAppHdrMode);
        }
        else
        {
            apphdrMode = mVhdrInfo.UiAppHdrMode;
        }

        if (apphdrMode == HDRMode::VIDEO_ON || apphdrMode == HDRMode::VIDEO_AUTO)
        {
            return true;
        }
        else
        {
            return false;
        }
    };

    if(isUiVhdrOn())
    {
        //Iso reconfig
        isNeedIsoReconfig(&apphdrMode, AppMode);

        vhdrMode = mVhdrInfo.cfgVhdrMode;

        //after doing capture, vhdr need to add dummy frame
        updateVhdrDummyFrames(out, in);
    }

    mVhdrInfo.lastAppHdrMode = mVhdrInfo.curAppHdrMode;
    mVhdrInfo.curAppHdrMode = apphdrMode;
    MY_LOGD_IF(mVhdrInfo.bVhdrDebugMode, "updateStreamData vhdrMode:%d, lastAppHdrMode:%hhu, curAppHdrMode:%hhu, UiAppHdrMode:%hhu IsoSwitchModeStatus:%d",
        vhdrMode, mVhdrInfo.lastAppHdrMode, mVhdrInfo.curAppHdrMode, mVhdrInfo.UiAppHdrMode, mVhdrInfo.IsoSwitchModeStatus);

    // update HDR mode to 3A
    IMetadata::setEntry<MUINT8>(pOutMetaHal.get(), MTK_3A_HDR_MODE, static_cast<MUINT8>(apphdrMode));

    // prepare Stream Size
    auto rrzoSize = (*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Rrzo->getImgSize();

    ProfileParam profileParam(
        rrzoSize,//stream size
        vhdrMode,//vhdrmode
        0,       //sensormode
        ProfileParam::FLAG_NONE, // TODO set flag by isZSDPureRawStreaming or not
        fMask
    );

    // Prepare query Feature Streaming ISP Profile
    if (FeatureProfileHelper::getStreamingProf(profile, profileParam))
    {
        IMetadata::setEntry<MUINT8>(pOutMetaHal.get(), MTK_3A_ISP_PROFILE, profile);
    }

    return updateRequestResultParams(out->mainFrame,
                              nullptr,
                              pOutMetaHal,
                              needP1Dma,
                              SENSOR_INDEX_MAIN,
                              0, 0, 0);
}

auto
FeatureSettingPolicy::
evaluateStreamSetting(
    RequestOutputParams* out,
    ParsedStrategyInfo& parsedInfo __unused,
    RequestInputParams const* in __unused,
    bool enabledP2Capture
) -> bool
{
    //FUNCTION_SCOPE;
    if (enabledP2Capture) {
        /**************************************************************
         * NOTE:
         * In this stage,
         * MTK_3A_ISP_PROFILE and sensor setting has been configured
         * for capture behavior.
         *************************************************************/
        // stream policy with capture policy and behavior.
        // It may occurs some quality limitation duting captue behavior.
        // For example, change sensor mode, 3A sensor setting, P1 ISP profile.
        // Capture+streaming feature combination policy
        // TODO: implement for customized streaming feature setting evaluate with capture behavior
        MY_LOGW("not yet implement for stream feature setting evaluate with capture behavior");
    }
    else {
        // TODO: only porting some streaming feature, temporary.
        // not yet implement all stream feature setting evaluate
        updateStreamData(out, parsedInfo, in);
    }
    MY_LOGD_IF(2 <= mbDebug, "stream request frames count(mainFrame:%d, subFrames:%zu, preDummyFrames:%zu, postDummyFrames:%zu)",
            (out->mainFrame.get() != nullptr), out->subFrames.size(),
            out->preDummyFrames.size(), out->postDummyFrames.size());
    return true;
}

auto
FeatureSettingPolicy::
evaluateReconfiguration(
    RequestOutputParams* out,
    RequestInputParams const* in
) -> bool
{
    //FUNCTION_SCOPE;
    out->needReconfiguration = false;
    out->reconfigCategory = ReCfgCtg::NO;
    for (unsigned int i=0; i<in->sensorMode.size(); i++) {
        if (in->sensorMode[i] != out->sensorMode[i]) {
            MY_LOGD("sensor(index:%d): sensorMode(%d --> %d) is changed", i, in->sensorMode[i], out->sensorMode[i]);
            out->needReconfiguration = true;
        }

        if(mVhdrInfo.curAppHdrMode == mVhdrInfo.lastAppHdrMode) {
            MY_LOGD_IF(mVhdrInfo.bVhdrDebugMode, "App hdrMode no change: Last(%hhu) - Cur(%hhu)", mVhdrInfo.lastAppHdrMode, mVhdrInfo.curAppHdrMode);
        }
        else {
            // app hdrMode change
            if(mVhdrInfo.curAppHdrMode == HDRMode::VIDEO_ON || mVhdrInfo.curAppHdrMode == HDRMode::VIDEO_AUTO
                || mVhdrInfo.lastAppHdrMode == HDRMode::VIDEO_ON || mVhdrInfo.lastAppHdrMode == HDRMode::VIDEO_AUTO) {
                MY_LOGD("App hdrMode change: Last(%hhu) - Cur(%hhu), need reconfig for vhdr", mVhdrInfo.lastAppHdrMode, mVhdrInfo.curAppHdrMode);
                out->needReconfiguration = true;
                out->reconfigCategory = ReCfgCtg::STREAMING;
            }
            else {
                MY_LOGD("App hdrMode change: Last(%hhu) - Cur(%hhu), no need reconfig", mVhdrInfo.lastAppHdrMode, mVhdrInfo.curAppHdrMode);
            }
        }

        MINT32 forceReconfig = ::property_get_bool("vendor.debug.camera.hal3.pure.reconfig.test", -1);
        if(forceReconfig == 1){
            out->needReconfiguration = true;
            out->reconfigCategory = ReCfgCtg::STREAMING;
        }
        else if(forceReconfig == 0){
            out->needReconfiguration = false;
            out->reconfigCategory = ReCfgCtg::NO;
        }

        // sensor mode is not the same as preview default (cannot execute zsl)
        if (out->needReconfiguration == true ||
            mDefaultConfig.sensorMode[i] != out->sensorMode[i]) {
            out->needZslFlow = false;
            out->zslPolicyParams.mPolicy = eZslPolicy_None;
            MY_LOGD("must reconfiguration, capture new frames w/o zsl flow");
        }
    }
    // zsl policy debug
    if (out->needZslFlow) {
        MY_LOGD("needZslFlow(%d), zsl policy(0x%X), timestamp:%" PRId64 ", timeouts:%" PRId64 "",
                out->needZslFlow, out->zslPolicyParams.mPolicy,
                out->zslPolicyParams.mTimestamp, out->zslPolicyParams.mTimeouts);
    }
    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
auto
FeatureSettingPolicy::
evaluateCaptureConfiguration(
    ConfigurationOutputParams* out __unused,
    ConfigurationInputParams const* in __unused
) -> bool
{
    FUNCTION_SCOPE;

    // query features by scenario during config
    ScenarioFeatures scenarioFeatures;
    CaptureScenarioConfig scenarioConfig;
    ScenarioHint scenarioHint;
    toTPIDualHint(scenarioHint);
    scenarioHint.operationMode = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration->operationMode;
    //TODO:
    //scenarioHint.captureScenarioIndex = ?   /* hint from vendor tag */
    //scenarioHint.streamingScenarioIndex = ? /* hint from vendor tag */
    int32_t openId = mPolicyParams.pPipelineStaticInfo->openId;
    auto pAppMetadata = in->pSessionParams;

    int32_t scenario = -1;
    if (!get_capture_scenario(scenario, scenarioFeatures, scenarioConfig, openId, scenarioHint, pAppMetadata)) {
        MY_LOGE("cannot get capture scenario openId:%d", openId);
        return false;
    }
    else {
        MY_LOGD("find scenario:%s for (openId:%d, scenario:%d)",
                scenarioFeatures.scenarioName.c_str(), openId, scenario);
    }

    for (auto featureSet : scenarioFeatures.vFeatureSet) {
        MY_LOGI("scenario(%s) support feature:%s(%#" PRIx64"), feature combination:%s(%#" PRIx64")",
                scenarioFeatures.scenarioName.c_str(),
                featureSet.featureName.c_str(),
                static_cast<MINT64>(featureSet.feature),
                featureSet.featureCombinationName.c_str(),
                static_cast<MINT64>(featureSet.featureCombination));
        out->CaptureParams.supportedScenarioFeatures |= featureSet.featureCombination;
    }
    MY_LOGD("support features:%#" PRIx64"", out->CaptureParams.supportedScenarioFeatures);

    // query maximum zsl buffer count from multiframe features
    for (auto iter : mMFPPluginWrapperPtr->getProviders()) {
        const MultiFramePlugin::Property& property =  iter->property();
        MY_LOGD("provider algo(%#" PRIx64"), maxZslBufferNum:%u", property.mFeatures, property.mZsdBufferMaxNum);
        if (property.mZsdBufferMaxNum > out->CaptureParams.maxZslBufferNum) {
            out->CaptureParams.maxZslBufferNum =  property.mZsdBufferMaxNum;
        }
    }
    // update scenarioconfig
    if (CC_UNLIKELY(scenarioConfig.maxAppJpegStreamNum) == 0) {
        MY_LOGW("invalid maxAppJpegStreamNum(%d), set to 1 at least", scenarioConfig.maxAppJpegStreamNum);
        out->CaptureParams.maxAppJpegStreamNum = 1;
    }
    else {
        out->CaptureParams.maxAppJpegStreamNum = scenarioConfig.maxAppJpegStreamNum;
    }
    if (CC_UNLIKELY(scenarioConfig.maxAppRaw16OutputBufferNum) == 0) {
        MY_LOGW("invalid maxAppRaw16OutputBufferNum(%d), set to 1 at least", scenarioConfig.maxAppRaw16OutputBufferNum);
        out->CaptureParams.maxAppRaw16OutputBufferNum = 1;
    }
    else {
        out->CaptureParams.maxAppRaw16OutputBufferNum = scenarioConfig.maxAppRaw16OutputBufferNum;
    }
    MY_LOGI("maxAppJpegStreamNum:%u, maxZslBufferNum:%u, maxAppRaw16OutputBufferNum:%u",
            out->CaptureParams.maxAppJpegStreamNum, out->CaptureParams.maxZslBufferNum, out->CaptureParams.maxAppRaw16OutputBufferNum);

    return true;
}

auto
FeatureSettingPolicy::
evaluateStreamConfiguration(
    ConfigurationOutputParams* out,
    ConfigurationInputParams const* in __unused
) -> bool
{
    FUNCTION_SCOPE;
    auto const& pParsedAppConfiguration   = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration;
    auto const& pParsedAppImageStreamInfo = mPolicyParams.pPipelineUserConfiguration->pParsedAppImageStreamInfo;
    MINT32 forceEisEMMode    = ::property_get_int32("vendor.debug.eis.EMEnabled", DEBUG_EISEM);
    MINT32 forceEis30        = ::property_get_int32("vendor.debug.camera.hal3.eis30", DEBUG_EIS30);
    MINT32 forceTSQ          = ::property_get_int32("vendor.debug.camera.hal3.tsq", DEBUG_TSQ);
    MINT32 force3DNR         = ::property_get_int32("vendor.debug.camera.hal3.3dnr", FORCE_3DNR);//default on

    if(::needControlMmdvfs() && (miMultiCamFeatureMode == MTK_MULTI_CAM_FEATURE_MODE_VSDOF))
    {
        MY_LOGD("vsdof enable bwc control");
        out->StreamingParams.BWCScenario = IScenarioControlV3::Scenario_ContinuousShot;
        FEATURE_CFG_ENABLE_MASK(
                        out->StreamingParams.BWCFeatureFlag,
                        IScenarioControlV3::FEATURE_VSDOF_PREVIEW);
    }
    // query features by scenario during config
    ScenarioFeatures scenarioFeatures;
    ScenarioHint scenarioHint;
    toTPIDualHint(scenarioHint);
    scenarioHint.operationMode = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration->operationMode;
    //TODO:
    //scenarioHint.captureScenarioIndex = ?   /* hint from vendor tag */
    //scenarioHint.streamingScenarioIndex = ? /* hint from vendor tag */
    int32_t openId = mPolicyParams.pPipelineStaticInfo->openId;
    auto pAppMetadata = in->pSessionParams;

    int32_t scenario = -1;
    if (!get_streaming_scenario(scenario, scenarioFeatures, openId, scenarioHint, pAppMetadata)) {
        MY_LOGE("cannot get streaming scenario openId:%d", openId);
        return false;
    }
    else {
        MY_LOGD("find scenario:%s for (openId:%d, scenario:%d)",
                scenarioFeatures.scenarioName.c_str(), openId, scenario);
    }

    for (auto featureSet : scenarioFeatures.vFeatureSet) {
        MY_LOGI("scenario(%s) support feature:%s(%#" PRIx64"), feature combination:%s(%#" PRIx64")",
                scenarioFeatures.scenarioName.c_str(),
                featureSet.featureName.c_str(),
                static_cast<MINT64>(featureSet.feature),
                featureSet.featureCombinationName.c_str(),
                static_cast<MINT64>(featureSet.featureCombination));
        out->StreamingParams.supportedScenarioFeatures |= featureSet.featureCombination;
    }
    MY_LOGD("support features:%#" PRIx64"", out->StreamingParams.supportedScenarioFeatures);

    // config vhdr
    evaluateVhdrConfiguration(out, in);

    // ISP6.0 FD
    if ( pParsedAppConfiguration->operationMode != 1/* CONSTRAINED_HIGH_SPEED_MODE */ && in->isP1DirectFDYUV )
    {
        if (::property_get_int32("vendor.debug.camera.fd.disable", 0) == 0)
        {
            out->StreamingParams.bNeedP1FDYUV = 1;
        }
    }

    //3DNR
    out->StreamingParams.nr3dMode = 0;
    MINT32 e3DnrMode = MTK_NR_FEATURE_3DNR_MODE_OFF;
    MBOOL isAPSupport3DNR = MFALSE;
    if (IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_NR_FEATURE_3DNR_MODE, e3DnrMode) &&
        e3DnrMode == MTK_NR_FEATURE_3DNR_MODE_ON) {
        isAPSupport3DNR = MTRUE;
    }
    if (force3DNR) {
        out->StreamingParams.nr3dMode |= E3DNR_MODE_MASK_UI_SUPPORT;
    }
    if (::property_get_int32("vendor.debug.camera.3dnr.level", 0)) {
        out->StreamingParams.nr3dMode |= E3DNR_MODE_MASK_HAL_FORCE_SUPPORT;
    }
    if ( pParsedAppConfiguration->operationMode == 1/* CONSTRAINED_HIGH_SPEED_MODE */ )
    {
        out->StreamingParams.nr3dMode = 0;
    }

    if( E3DNR_MODE_MASK_ENABLED(out->StreamingParams.nr3dMode, (E3DNR_MODE_MASK_UI_SUPPORT | E3DNR_MODE_MASK_HAL_FORCE_SUPPORT))) {
        if (::property_get_int32("vendor.debug.3dnr.sl2e.enable", 1)) { // sl2e: default on, need use metadata?
            out->StreamingParams.nr3dMode |= E3DNR_MODE_MASK_SL2E_EN;
        }

        if ((::property_get_int32("vendor.debug.3dnr.rsc.limit", 0) == 0) || isAPSupport3DNR) {
            MUINT32 nr3d_mask = NR3DCustom::USAGE_MASK_NONE;
            if (pParsedAppConfiguration->operationMode==1/* CONSTRAINED_HIGH_SPEED_MODE */) {
                nr3d_mask |= NR3DCustom::USAGE_MASK_HIGHSPEED;
            }
            if (mPolicyParams.pPipelineStaticInfo->isDualDevice) {
                nr3d_mask |= NR3DCustom::USAGE_MASK_DUAL_ZOOM;
            }
            if (NR3DCustom::isEnabledRSC(nr3d_mask)) {
                out->StreamingParams.nr3dMode |= E3DNR_MODE_MASK_RSC_EN;
            }
        }
    }
    MY_LOGD("3DNR mode : %d, meta c(%d), force(%d) ap(%d)", out->StreamingParams.nr3dMode, pParsedAppConfiguration->sessionParams.count(),
        force3DNR, isAPSupport3DNR);

    //FSC
    out->StreamingParams.fscMode = EFSC_MODE_MASK_FSC_NONE;
    if (pParsedAppImageStreamInfo->hasVideoConsumer) {//video mode
        MBOOL support_AF = MFALSE;
        auto pHal3A = mHal3a;
        NS3Av3::FeatureParam_T r3ASupportedParam;
        {
            std::lock_guard<std::mutex> _l(mHal3aLocker);
            if(pHal3A.get() && pHal3A->send3ACtrl(NS3Av3::E3ACtrl_GetSupportedInfo, reinterpret_cast<MINTPTR>(&r3ASupportedParam), 0))
            {
                support_AF = (r3ASupportedParam.u4MaxFocusAreaNum > 0);
            }
            else {
                MY_LOGW("Cannot query AF ability from 3A");
            }
        }

        MUINT32 fsc_mask = FSCCustom::USAGE_MASK_NONE;
        if (pParsedAppConfiguration->operationMode==1/* CONSTRAINED_HIGH_SPEED_MODE */)
        {
            fsc_mask |= FSCCustom::USAGE_MASK_HIGHSPEED;
        }
        if (mPolicyParams.pPipelineStaticInfo->isDualDevice) {
            fsc_mask |= FSCCustom::USAGE_MASK_DUAL_ZOOM;
        }
        if (FSCCustom::isEnabledFSC(fsc_mask) && support_AF)
        {
            out->StreamingParams.fscMode |= EFSC_MODE_MASK_FSC_EN;
            if (::property_get_int32(FSC_DEBUG_ENABLE_PROPERTY, 0))
                out->StreamingParams.fscMode |= EFSC_MODE_MASK_DEBUG_LEVEL;
            if (::property_get_int32(FSC_SUBPIXEL_ENABLE_PROPERTY, 1)) // default on
                out->StreamingParams.fscMode |= EFSC_MODE_MASK_SUBPIXEL_EN;
        }
    }

    // CR
    out->StreamingParams.bSupportCZ = ::property_get_int32("vendor.camera.mdp.cz.enable", 0);
    // DRE
    if (EIS_MODE_IS_EIS_30_ENABLED(out->StreamingParams.eisInfo.mode))
    {
        out->StreamingParams.bSupportDRE = 0;
    }
    else
    {
        out->StreamingParams.bSupportDRE = ::property_get_int32("vendor.camera.mdp.dre.enable", 0);
    }
    MY_LOGD_IF(2 <= mbDebug, "support(CZ=%d, DRE=%d), hasVideoConsumer=%d, EIS_30_ENABLED: %d",
        out->StreamingParams.bSupportCZ, out->StreamingParams.bSupportDRE,
        pParsedAppImageStreamInfo->hasVideoConsumer, EIS_MODE_IS_EIS_30_ENABLED(out->StreamingParams.eisInfo.mode));

    // EIS
    MUINT8 appEisMode = 0;
    MINT32 advEisMode = 0;
    if( !IMetadata::getEntry<MUINT8>(&pParsedAppConfiguration->sessionParams, MTK_CONTROL_VIDEO_STABILIZATION_MODE, appEisMode) )
    {
        MY_LOGD("No MTK_CONTROL_VIDEO_STABILIZATION_MODE in sessionParams");
    }
    if( !IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_EIS_FEATURE_EIS_MODE, advEisMode) )
    {
        MY_LOGD("No MTK_EIS_FEATURE_EIS_MODE in sessionParams");
    }

    out->StreamingParams.eisExtraBufNum = 0;

    if ( pParsedAppImageStreamInfo->hasVideoConsumer &&
         pParsedAppConfiguration->operationMode != 1 /* CONSTRAINED_HIGH_SPEED_MODE */ &&
         ( advEisMode == MTK_EIS_FEATURE_EIS_MODE_ON || forceEis30 ) &&
         !forceEisEMMode )
    {
        out->StreamingParams.bIsEIS = MTRUE;
        MUINT32 eisMask = EISCustom::USAGE_MASK_NONE;
        MUINT32 videoCfg = EISCustom::VIDEO_CFG_FHD;
        if (out->StreamingParams.vhdrMode != SENSOR_VHDR_MODE_NONE) {
            eisMask |= EISCustom::USAGE_MASK_VHDR;
        }
        if (pParsedAppImageStreamInfo->hasVideo4K) {
            eisMask |= EISCustom::USAGE_MASK_4K2K;
            videoCfg = EISCustom::VIDEO_CFG_4K2K;
        }

        if( mPolicyParams.pPipelineStaticInfo->openId != 0 )
        {
            eisMask |= EISCustom::USAGE_MASK_MULTIUSER;
        }

        if ( EISCustom::isSupportAdvEIS_HAL3() || forceEis30 ) {
            // must be TK app
            out->StreamingParams.eisInfo.mode = EISCustom::getEISMode(eisMask);
            // FSC+ only support EIS3.0
            if (EFSC_FSC_ENABLED(out->StreamingParams.fscMode))
            {
                //EIS1.2 per-frame on/off
                if(!EIS_MODE_IS_EIS_30_ENABLED(out->StreamingParams.eisInfo.mode) && !EIS_MODE_IS_EIS_12_ENABLED(out->StreamingParams.eisInfo.mode)) {
                    MY_LOGI("disable FSC due to combine with EIS 2.x version!");
                    out->StreamingParams.fscMode = EFSC_MODE_MASK_FSC_NONE;
                }
            }
            if (EFSC_FSC_ENABLED(out->StreamingParams.fscMode)) {
                eisMask |= EISCustom::USAGE_MASK_FSC;
            }
            out->StreamingParams.eisInfo.factor =
                EIS_MODE_IS_EIS_12_ENABLED(out->StreamingParams.eisInfo.mode) ?
                EISCustom::getEIS12Factor() : EISCustom::getEISFactor(videoCfg, eisMask);
            out->StreamingParams.eisInfo.queueSize = EISCustom::getForwardFrames(videoCfg);
            out->StreamingParams.eisInfo.startFrame = EISCustom::getForwardStartFrame();
            out->StreamingParams.eisInfo.lossless = EISCustom::isEnabledLosslessMode();
            out->StreamingParams.bEnableTSQ = MTRUE;
            if (forceTSQ == 2) { // force disable tsq
                out->StreamingParams.bEnableTSQ = MFALSE;
            }
            if ( !out->StreamingParams.bEnableTSQ ) {
                out->StreamingParams.eisExtraBufNum = EISCustom::getForwardFrames(videoCfg);
            }
        }
        else {
            out->StreamingParams.eisInfo.mode = (1<<EIS_MODE_EIS_12);
            out->StreamingParams.eisInfo.factor = EISCustom::getEIS12Factor();
            out->StreamingParams.bEnableTSQ = MFALSE;
        }
    }
    else if( pParsedAppConfiguration->operationMode != 1 /* CONSTRAINED_HIGH_SPEED_MODE */ &&
             EISCustom::isSupportAdvEIS_HAL3() && forceEisEMMode )
    {
        out->StreamingParams.bIsEIS = MFALSE;
        out->StreamingParams.eisInfo.mode = (1<<EIS_MODE_CALIBRATION);
        out->StreamingParams.fscMode = EFSC_MODE_MASK_FSC_NONE;
        out->StreamingParams.eisInfo.factor = 100;
        out->StreamingParams.bEnableTSQ = MFALSE;
    }
    else if( pParsedAppConfiguration->operationMode == 1 /* CONSTRAINED_HIGH_SPEED_MODE */ )
    {
        out->StreamingParams.bIsEIS = MFALSE;
        out->StreamingParams.eisInfo.mode = 0;
        out->StreamingParams.eisInfo.factor = 100;
        out->StreamingParams.bEnableTSQ = MFALSE;
    }
    else
    {
        out->StreamingParams.bIsEIS = MTRUE;
        out->StreamingParams.eisInfo.mode = (1<<EIS_MODE_EIS_12);
        out->StreamingParams.eisInfo.factor = EISCustom::getEIS12Factor();
        out->StreamingParams.bEnableTSQ = MFALSE;
    }

    out->StreamingParams.minRrzoEisW = 1280;

    // LMV
    if( pParsedAppConfiguration->operationMode != 1 /* CONSTRAINED_HIGH_SPEED_MODE */ &&
        ( E3DNR_MODE_MASK_ENABLED(out->StreamingParams.nr3dMode, (E3DNR_MODE_MASK_UI_SUPPORT | E3DNR_MODE_MASK_HAL_FORCE_SUPPORT)) ||
          out->StreamingParams.eisInfo.mode) )
    {
        out->StreamingParams.bNeedLMV = true;
    }

    // RSS
    if( pParsedAppConfiguration->operationMode != 1 /* CONSTRAINED_HIGH_SPEED_MODE */ &&
        ( ( EIS_MODE_IS_EIS_30_ENABLED(out->StreamingParams.eisInfo.mode) &&
            EIS_MODE_IS_EIS_IMAGE_ENABLED(out->StreamingParams.eisInfo.mode) ) ||
          ( E3DNR_MODE_MASK_ENABLED(out->StreamingParams.nr3dMode, (E3DNR_MODE_MASK_UI_SUPPORT | E3DNR_MODE_MASK_HAL_FORCE_SUPPORT)) &&
            E3DNR_MODE_MASK_ENABLED(out->StreamingParams.nr3dMode, E3DNR_MODE_MASK_RSC_EN) ) ) )
    {
        out->StreamingParams.bNeedRSS = true;
    }

    MY_LOGD("AppEis: %d, AdvEis: %d. Lmv: %d, Rss : %d, isEis : %d, Eis mode : %d, Eis factor : %d, TSQ : %d, mEisExtraBufNum : %d, hasVideoConsumer : %d, operation : %d, AdvEIS support : %d, EM : %d",
            appEisMode, advEisMode,
            out->StreamingParams.bNeedLMV, out->StreamingParams.bNeedRSS,
            out->StreamingParams.bIsEIS, out->StreamingParams.eisInfo.mode, out->StreamingParams.eisInfo.factor, out->StreamingParams.bEnableTSQ, out->StreamingParams.eisExtraBufNum,
            pParsedAppImageStreamInfo->hasVideoConsumer, pParsedAppConfiguration->operationMode,
            EISCustom::isSupportAdvEIS_HAL3(), forceEisEMMode);
    return true;
}

auto
FeatureSettingPolicy::
evaluateVhdrConfiguration(
    ConfigurationOutputParams* out,
    ConfigurationInputParams const* in __unused
) -> bool
{
    FUNCTION_SCOPE;
    mVhdrInfo.bVhdrDebugMode = ::property_get_bool("vendor.debug.camera.hal3.vhdr", DEBUG_VHDR);            //for debug
    mVhdrInfo.DummyCount     = ::property_get_int32("vendor.debug.camera.hal3.dummycount", DEBUG_DUMMY_HDR);// for dummyconunt debug after doing capture
    MINT32 forceAppHdrMode   = ::property_get_int32("vendor.debug.camera.hal3.appHdrMode", DEBUG_APP_HDR);  // for force on vhdr and need to open debug property

    auto const& pParsedAppConfiguration   = mPolicyParams.pPipelineUserConfiguration->pParsedAppConfiguration;
    //get vhdr type(z,i,m vhdr)
    {
        sp<IMetadataProvider> metaProvider = NSMetadataProviderManager::valueFor(mPolicyParams.pPipelineStaticInfo->sensorId[0]);
        if(metaProvider == NULL) {
            MY_LOGE("Can not get metadata provider for search vhdr mode!! set vhdrMode to none");
        }
        else
        {
            IMetadata staMeta = metaProvider->getMtkStaticCharacteristics();
            IMetadata::IEntry availVhdrEntry = staMeta.entryFor(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES);
            for (size_t i = 0 ; i < availVhdrEntry.count() ; i++) {
                if (availVhdrEntry.itemAt(i, Type2Type<MINT32>()) != SENSOR_VHDR_MODE_NONE) {
                    mVhdrInfo.cfgVhdrMode = (MUINT32)availVhdrEntry.itemAt(i, Type2Type<MINT32>());
                    MY_LOGD("vhdr type(%u)", mVhdrInfo.cfgVhdrMode);
                    break;
                }
            }
            if (mVhdrInfo.cfgVhdrMode == SENSOR_VHDR_MODE_NONE) {
                MY_LOGD("Can not get supported vhdr mode from MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES! (maybe FO not set?), set vhdrMode to none");
            }
        }
    }

    //getHDRMode(on,off..)
    HDRMode appHdrMode = HDRMode::OFF;
    MINT32 hdrModeInt = 0;
    if(mVhdrInfo.bVhdrDebugMode && forceAppHdrMode >= 0)
    {
        // Force set app hdrMode
        appHdrMode = static_cast<HDRMode>((MUINT8)forceAppHdrMode);
        mVhdrInfo.curAppHdrMode = appHdrMode;
    }
    else if(!mVhdrInfo.bFirstConfig)
    {
        // After first configure, using CurAppHdrMode
        appHdrMode = mVhdrInfo.curAppHdrMode;
    }
    else if(IMetadata::getEntry<MINT32>(&pParsedAppConfiguration->sessionParams, MTK_HDR_FEATURE_SESSION_PARAM_HDR_MODE, hdrModeInt))
    {
        appHdrMode = static_cast<HDRMode>((MUINT8)hdrModeInt);
        mVhdrInfo.curAppHdrMode = appHdrMode;
        MY_LOGW("first config vhdr(%hhu)", mVhdrInfo.curAppHdrMode);
    }
    else
    {
        IMetadata::IEntry test_entry = pParsedAppConfiguration->sessionParams.entryFor(MTK_HDR_FEATURE_SESSION_PARAM_HDR_MODE);
        MY_LOGW("Get appConfig sessionParams appHdrMode fail: test_entry.count(%d)", test_entry.count());
    }
    mVhdrInfo.bFirstConfig = MFALSE;

    MY_LOGD("StreamConfig: bFirstConfig(%d) forceAppHdrMode(%d), curAppHdrMode(%hhu), appHdrMode(%hhu)",
            mVhdrInfo.bFirstConfig, forceAppHdrMode, mVhdrInfo.curAppHdrMode, appHdrMode);

    //setVHDRMode to P1
    out->StreamingParams.vhdrMode = SENSOR_VHDR_MODE_NONE;
    if (appHdrMode == HDRMode::VIDEO_ON || appHdrMode == HDRMode::VIDEO_AUTO) {
        out->StreamingParams.vhdrMode = mVhdrInfo.cfgVhdrMode;
        MY_LOGD("vhdr on(%d)", out->StreamingParams.vhdrMode);
    }
    else
    {
        out->StreamingParams.vhdrMode = SENSOR_VHDR_MODE_NONE;
        MY_LOGD("vhdr off(%d)", out->StreamingParams.vhdrMode);
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
auto
FeatureSettingPolicy::
evaluateConfiguration(
    ConfigurationOutputParams* out,
    ConfigurationInputParams const* in
) -> int
{
    FUNCTION_SCOPE;
    // check input setting is valid
    if (CC_UNLIKELY(in->pSessionParams == nullptr)) {
        CAM_LOGE("pSessionParams is invalid nullptr");
        return -ENODEV;
    }
    //
    if (CC_UNLIKELY(!evaluateCaptureConfiguration(out, in))) {
        CAM_LOGE("evaluate capture configuration failed!");
        return -ENODEV;
    }
    if (CC_UNLIKELY(!evaluateStreamConfiguration(out, in))) {
        CAM_LOGE("evaluate stream configuration failed!");
        return -ENODEV;
    }
    // Default connfig params for feature strategy.
    mConfigInputParams = *in;
    mConfigOutputParams = *out;
    //
    // TODO: implement ommon feature configuration here.
    // 1. P1 IMGO, RRZO, LCSO, RSSO configuration
    //    and those cache buffer account for zsd flow
    return OK;
}

bool FeatureSettingPolicy::
updateMultiCamStreamingData(
    RequestOutputParams* out __unused,
    RequestInputParams const* in __unused
)
{
    auto const& sensorIdList = mPolicyParams.pPipelineStaticInfo->sensorId;
    // 1. create temp hal metadata for dualcam
    MetadataPtr pOutMetaHal_main1 = std::make_shared<IMetadata>();
    MetadataPtr pOutMetaHal_main2 = std::make_shared<IMetadata>();
    auto queryNeedP1DmaResult = [&in](int index)
    {
        uint32_t needP1Dma = 0;

        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Rrzo != nullptr)
        {
            needP1Dma |= P1_RRZO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Imgo != nullptr)
        {
            needP1Dma |= P1_IMGO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Lcso != nullptr)
        {
            needP1Dma |= P1_LCSO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Rsso != nullptr)
        {
            needP1Dma |= P1_RSSO;
        }
        return needP1Dma;
    };

    // 2. set sync info
    MINT64 hwsyncToleranceTime = ::getFrameSyncToleranceTime();
    MINT32 syncFailBehavior = MTK_FRAMESYNC_FAILHANDLE_CONTINUE;
    // 2.a update main1 frame
    {
        // set sync id to main2
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_ID, sensorIdList[1]);
        // set tolerance time
        IMetadata::setEntry<MINT64>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_TOLERANCE, hwsyncToleranceTime);
        // set sync fail behavior
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_FAILHANDLE, syncFailBehavior);
        // set isp profile
        IMetadata::setEntry<MUINT8>(pOutMetaHal_main1.get(), MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    }
    // 2.b update main2 frame
    {
        // set sync id to main2
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_ID, sensorIdList[0]);
        // set tolerance time
        IMetadata::setEntry<MINT64>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_TOLERANCE, hwsyncToleranceTime);
        // set sync fail behavior
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_FAILHANDLE, syncFailBehavior);
        // set isp profile
        IMetadata::setEntry<MUINT8>(pOutMetaHal_main2.get(), MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    }
    // 3. enable 3A sync
    {
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_SYNC2A_MODE, ISync3AMgr::E_SYNC2A_MODE_VSDOF_BY_FRAME);
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_STEREO_SYNC2A_MODE, ISync3AMgr::E_SYNC2A_MODE_VSDOF_BY_FRAME);
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_SYNCAF_MODE, ISync3AMgr::E_SYNCAF_MODE_ON);
    }
    // 4. each frame need enable sync
    {
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_HW_FRM_SYNC_MODE, 1);
    }
    updateRequestResultParams(out->mainFrame,
                          nullptr,
                          pOutMetaHal_main1,
                          queryNeedP1DmaResult(0),
                          SENSOR_INDEX_MAIN,
                          0, 0, 0);
    updateRequestResultParams(out->mainFrame,
                          nullptr,
                          pOutMetaHal_main2,
                          queryNeedP1DmaResult(1),
                          SENSOR_INDEX_SUB1,
                          0, 0, 0);
    return true;
}

bool
FeatureSettingPolicy::
updateVsDofStreamingData(
    RequestOutputParams* out __unused,
    RequestInputParams const* in __unused
)
{
    auto const& sensorIdList = mPolicyParams.pPipelineStaticInfo->sensorId;
    // 1. create temp hal metadata for dualcam
    MetadataPtr pOutMetaHal_main1 = std::make_shared<IMetadata>();
    MetadataPtr pOutMetaHal_main2 = std::make_shared<IMetadata>();
    auto queryNeedP1DmaResult = [&in](int index)
    {
        uint32_t needP1Dma = 0;

        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Rrzo != nullptr)
        {
            needP1Dma |= P1_RRZO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Imgo != nullptr)
        {
            needP1Dma |= P1_IMGO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Lcso != nullptr)
        {
            needP1Dma |= P1_LCSO;
        }
        if ((*(in->pConfiguration_StreamInfo_P1))[index].pHalImage_P1_Rsso != nullptr)
        {
            needP1Dma |= P1_RSSO;
        }
        return needP1Dma;
    };
    // 2. update master & slave for p2 streaming node.
    IMetadata::IEntry tag(MTK_STEREO_SYNC2A_MASTER_SLAVE);
    for(auto& id:sensorIdList)
    {
        tag.push_back(id, Type2Type<MINT32>());
    }
    pOutMetaHal_main1->update(tag.tag(), tag);
    pOutMetaHal_main2->update(tag.tag(), tag);

    // 3. set sync info
    MINT64 hwsyncToleranceTime = ::getFrameSyncToleranceTime();
    MINT32 syncFailBehavior = MTK_FRAMESYNC_FAILHANDLE_CONTINUE;
    // 3.a update main1 frame
    {
        // set sync id to main2
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_ID, sensorIdList[1]);
        // set tolerance time
        IMetadata::setEntry<MINT64>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_TOLERANCE, hwsyncToleranceTime);
        // set sync fail behavior
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_FRAMESYNC_FAILHANDLE, syncFailBehavior);
        // set isp profile
        IMetadata::setEntry<MUINT8>(pOutMetaHal_main1.get(), MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    }
    // 3.b update main2 frame
    {
        // set sync id to main2
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_ID, sensorIdList[0]);
        // set tolerance time
        IMetadata::setEntry<MINT64>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_TOLERANCE, hwsyncToleranceTime);
        // set sync fail behavior
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_FRAMESYNC_FAILHANDLE, syncFailBehavior);
        // set isp profile
        IMetadata::setEntry<MUINT8>(pOutMetaHal_main2.get(), MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    }
    // 4. enable 3A sync
    {
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_SYNC2A_MODE, ISync3AMgr::E_SYNC2A_MODE_VSDOF_BY_FRAME);
        IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_STEREO_SYNC2A_MODE, ISync3AMgr::E_SYNC2A_MODE_VSDOF_BY_FRAME);
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_SYNCAF_MODE, ISync3AMgr::E_SYNCAF_MODE_ON);
    }
    // 5. each frame need enable sync
    {
        IMetadata::setEntry<MINT32>(pOutMetaHal_main1.get(), MTK_STEREO_HW_FRM_SYNC_MODE, 1);
    }
    // 6. update stream for VSDOF (main2 no needs output app dynamic)
    if(mbNeedReplaceStream)
    {
        mbNeedReplaceStream = MFALSE;
    }
    // 7. update crop info
    {
        MRect main1_crop;
        MRect main2_crop;
        queryVsdofSensorCropRect(in, main1_crop, main2_crop);
        IMetadata::setEntry<MRect>(pOutMetaHal_main1.get(), MTK_P1NODE_SENSOR_CROP_REGION, main1_crop);
        IMetadata::setEntry<MRect>(pOutMetaHal_main1.get(), MTK_3A_PRV_CROP_REGION, main1_crop);
        IMetadata::setEntry<MRect>(pOutMetaHal_main2.get(), MTK_P1NODE_SENSOR_CROP_REGION, main2_crop);
        IMetadata::setEntry<MRect>(pOutMetaHal_main2.get(), MTK_3A_PRV_CROP_REGION, main2_crop);
    }
    // 8. if camsv path, it has to set hal metadata to covert mono img to yuv.
    {
        if ((*(in->pConfiguration_StreamInfo_P1))[SENSOR_INDEX_SUB1].pHalImage_P1_Rrzo == nullptr)
        {
            IMetadata::setEntry<MINT32>(pOutMetaHal_main2.get(), MTK_ISP_P2_IN_IMG_FMT, 5);
        }
    }
    updateRequestResultParams(out->mainFrame,
                          nullptr,
                          pOutMetaHal_main1,
                          queryNeedP1DmaResult(0),
                          SENSOR_INDEX_MAIN,
                          0, 0, 0);
    updateRequestResultParams(out->mainFrame,
                          nullptr,
                          pOutMetaHal_main2,
                          queryNeedP1DmaResult(1),
                          SENSOR_INDEX_SUB1,
                          0, 0, 0);
    return true;
}

bool
FeatureSettingPolicy::
queryVsdofSensorCropRect(
    RequestInputParams const* in __unused,
    MRect& sensor1 __unused,
    MRect& sensor2 __unused
)
{
    bool ret = false;
    // get rrzo format
    auto main1RrzoStreamInfo = (*(in->pConfiguration_StreamInfo_P1))[0].pHalImage_P1_Rrzo;
    auto main2RrzoStreamInfo = (*(in->pConfiguration_StreamInfo_P1))[1].pHalImage_P1_Rrzo;
    // get sensor crop
    MUINT32 q_stride;
    NSCam::MSize size;
    // main1
    if(main1RrzoStreamInfo != nullptr)
    {
        StereoSizeProvider::getInstance()->getPass1Size(
                StereoHAL::eSTEREO_SENSOR_MAIN1,
                (EImageFormat)main1RrzoStreamInfo->getImgFormat(),
                NSImageio::NSIspio::EPortIndex_RRZO,
                StereoHAL::eSTEREO_SCENARIO_CAPTURE, // in this mode, stereo only support zsd.
                (MRect&)sensor1,
                size,
                q_stride);
    }
    // main2
    if(main2RrzoStreamInfo != nullptr)
    {
        StereoSizeProvider::getInstance()->getPass1Size(
                StereoHAL::eSTEREO_SENSOR_MAIN2,
                (EImageFormat)main2RrzoStreamInfo->getImgFormat(),
                NSImageio::NSIspio::EPortIndex_RRZO,
                StereoHAL::eSTEREO_SCENARIO_CAPTURE, // in this mode, stereo only support zsd.
                (MRect&)sensor2,
                size,
                q_stride);
    }
    else
    {
        // if rrzo is null, asign imgo crop size to main2
        auto imgoStreamInfo = (*(in->pConfiguration_StreamInfo_P1))[1].pHalImage_P1_Imgo;
        if(imgoStreamInfo != nullptr)
        {
            StereoSizeProvider::getInstance()->getPass1Size(
                    StereoHAL::eSTEREO_SENSOR_MAIN2,
                    (EImageFormat)imgoStreamInfo->getImgFormat(),
                    NSImageio::NSIspio::EPortIndex_IMGO,
                    StereoHAL::eSTEREO_SCENARIO_CAPTURE, // in this mode, stereo only support zsd.
                    (MRect&)sensor2,
                    size,
                    q_stride);
        }
        else
        {
            MY_LOGE("should not happened.");
            ret = false;
            goto lbExit;
        }
    }
#if 0
    MY_LOGD("StereoSizeProvider => sensor crop main1(%d,%d,%dx%d), main2(%d,%d,%dx%d)",
        sensor1.p.x,
        sensor1.p.y,
        sensor1.s.w,
        sensor1.s.h,
        sensor2.p.x,
        sensor2.p.y,
        sensor2.s.w,
        sensor2.s.h
    );
#endif
    ret = true;
lbExit:
    return ret;
}

auto
FeatureSettingPolicy::
evaluateRequest(
    RequestOutputParams* out,
    RequestInputParams const* in
) -> int
{
    //FUNCTION_SCOPE;
    // check setting valid.
    if CC_UNLIKELY(out == nullptr || in == nullptr) {
        CAM_LOGE("invalid in(%p), out(%p) for evaluate", in, out);
        return -ENODEV;
    }
    auto sensorModeSize = in->sensorMode.size();
    auto sensorIdSize = mPolicyParams.pPipelineStaticInfo->sensorId.size();
    if (sensorModeSize != sensorIdSize) {
        CAM_LOGE("input sesnorMode size(%zu) != sensorId(%zu), cannot strategy the feature policy correctly",
                sensorModeSize, sensorIdSize);
        return -ENODEV;
    }
    // keep first request config as default setting (ex: defualt sensor mode).
    if (mDefaultConfig.bInit == false) {
        MY_LOGI("keep the first request config as default config");
        mDefaultConfig.sensorMode = in->sensorMode;
        mDefaultConfig.bInit = true;
    }
    // use the default setting, features will update it later.
    out->sensorMode = mDefaultConfig.sensorMode;
    ParsedStrategyInfo parsedInfo;
    if (!collectParsedStrategyInfo(parsedInfo, in)) {
        MY_LOGE("collectParsedStrategyInfo failed!");
        return -ENODEV;
    }
    // P2 capture feature policy
    if (in->needP2CaptureNode) {
        if (!evaluateCaptureSetting(out, parsedInfo, in)) {
            MY_LOGE("evaluateCaptureSetting failed!");
            return -ENODEV;
        }
    }
    // P2 streaming feature policy
    if (in->needP2StreamNode) {
        if (!evaluateStreamSetting(out, parsedInfo, in, in->needP2CaptureNode)) {
            MY_LOGE("evaluateStreamSetting failed!");
            return -ENODEV;
        }
        // update dual cam preview
        if (mPolicyParams.pPipelineStaticInfo->isDualDevice) {
            if(mMultiCamStreamingUpdater)
            {
                mMultiCamStreamingUpdater(out, in);
            }
        }
    }
    // update needReconfiguration info.
    if (!evaluateReconfiguration(out, in)) {
        MY_LOGE("evaluateReconfiguration failed!");
        return -ENODEV;
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace featuresetting {
auto createFeatureSettingPolicyInstance(
    CreationParams const& params
) -> std::shared_ptr<IFeatureSettingPolicy>
{
    FUNCTION_SCOPE;
    // check the policy params is valid.
    if (CC_UNLIKELY(params.pPipelineStaticInfo.get() == nullptr)) {
        CAM_LOGE("pPipelineStaticInfo is invalid nullptr");
        return nullptr;
    }
    if (CC_UNLIKELY(params.pPipelineUserConfiguration.get() == nullptr)) {
        CAM_LOGE("pPipelineUserConfiguration is invalid nullptr");
        return nullptr;
    }
    int32_t openId = params.pPipelineStaticInfo->openId;
    if (CC_UNLIKELY(openId < 0)) {
        CAM_LOGE("openId is invalid(%d)", openId);
        return nullptr;
    }
    if (CC_UNLIKELY(params.pPipelineStaticInfo->sensorId.empty())) {
        CAM_LOGE("sensorId is empty(size:%zu)", params.pPipelineStaticInfo->sensorId.size());
        return nullptr;
    }
    for (unsigned int i=0; i<params.pPipelineStaticInfo->sensorId.size(); i++) {
        int32_t sensorId = params.pPipelineStaticInfo->sensorId[i];
        CAM_LOGD("sensorId[%d]=%d", i, sensorId);
        if (CC_UNLIKELY(sensorId < 0)) {
            CAM_LOGE("sensorId is invalid(%d)", sensorId);
            return nullptr;
        }
    }
    // you have got an instance for feature setting policy.
    return std::make_shared<FeatureSettingPolicy>(params);
}
};  //namespace
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
