/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-RequestMetadataPolicy"

#include "RequestMetadataPolicy.h"
//
#include "MyUtils.h"
//
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>
/******************************************************************************
 *
 ******************************************************************************/
using namespace android;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace requestmetadata {


/******************************************************************************
 *
 ******************************************************************************/
RequestMetadataPolicy_Default::
RequestMetadataPolicy_Default(
    CreationParams const& params
)
    :mPolicyParams(params)
{
    for (size_t i = 0; i < params.pPipelineStaticInfo->sensorId.size(); i++)
    {
        mvTargetRrzoSize.push_back(MSize(0, 0));
    }
    mUniqueKey = NSCam::Utils::TimeTool::getReadableTime();
}


/******************************************************************************
 *
 ******************************************************************************/
auto
RequestMetadataPolicy_Default::
evaluateRequest(
    EvaluateRequestParams const& params
) -> int
{
    // update request unique key
    if (params.pAdditionalHal->size() > 0)
    {
        IMetadata::IEntry entry(MTK_PIPELINE_UNIQUE_KEY);
        IMetadata::IEntry entry1 = (*params.pAdditionalHal)[0]->entryFor(MTK_PIPELINE_UNIQUE_KEY);
        if (entry1.isEmpty())
        {
            entry.push_back(mUniqueKey, Type2Type<MINT32>());
        }
        else
        {
            entry = entry1;
        }
        for(size_t i=0;i<params.pAdditionalHal->size();++i){
            (*params.pAdditionalHal)[i]->update(entry.tag(), entry);
        }
    }

    // update request id
    {
        IMetadata::IEntry entry(MTK_PIPELINE_REQUEST_NUMBER);
        entry.push_back(params.requestNo, Type2Type<MINT32>());
        for(size_t i=0;i<params.pAdditionalHal->size();++i){
            (*params.pAdditionalHal)[i]->update(entry.tag(), entry);
        }
    }

    // common metadata
    {
        MINT64 iMinFrmDuration = 0;
        for (auto const& it : params.pRequest_AppImageStreamInfo->vAppImage_Output_Proc)
        {
            StreamId_T const streamId = it.first;
            auto minFrameDuration = mPolicyParams.pPipelineUserConfiguration->vMinFrameDuration.find(streamId);
            if ( CC_UNLIKELY(minFrameDuration->second < 0) ) {
                MY_LOGE("Request App stream %#" PRIx64 "have not configured yet", streamId);
                continue;
            }
            iMinFrmDuration = ( minFrameDuration->second > iMinFrmDuration ) ?
                                minFrameDuration->second : iMinFrmDuration;
        }
        MY_LOGD( "The min frame duration is %" PRId64, iMinFrmDuration);
        IMetadata::IEntry entry(MTK_P1NODE_MIN_FRM_DURATION);
        entry.push_back(iMinFrmDuration, Type2Type<MINT64>());
        for(size_t i=0;i<params.pAdditionalHal->size();++i){
            (*params.pAdditionalHal)[i]->update(entry.tag(), entry);
        }
    }
    MUINT8 bRepeating = (MUINT8) params.pRequest_ParsedAppMetaControl->repeating;
    {
        IMetadata::IEntry entry(MTK_HAL_REQUEST_REPEAT);
        entry.push_back(bRepeating, Type2Type< MUINT8 >());
        for(size_t i=0;i<params.pAdditionalHal->size();++i)
            (*params.pAdditionalHal)[i]->update(entry.tag(), entry);
        MY_LOGD("Control AppMetadata is repeating(%d)", bRepeating);
    }
    {
        if ( params.isZSLMode
          || params.pRequest_AppImageStreamInfo->pAppImage_Jpeg != nullptr
          || params.pRequest_AppImageStreamInfo->pAppImage_Output_Priv != nullptr
          || params.needExif
           )
        {
            MY_LOGD("set MTK_HAL_REQUEST_REQUIRE_EXIF = 1");
            IMetadata::IEntry entry2(MTK_HAL_REQUEST_REQUIRE_EXIF);
            entry2.push_back(1, Type2Type<MUINT8>());
            for(size_t i=0;i<params.pAdditionalHal->size();++i)
                (*params.pAdditionalHal)[i]->update(entry2.tag(), entry2);
        }

        for(size_t i=0;i<params.pAdditionalHal->size();++i)
        {
            IMetadata::IEntry entry3(MTK_HAL_REQUEST_SENSOR_SIZE);
            entry3.push_back((*params.pSensorSize)[i], Type2Type<MSize>());
            (*params.pAdditionalHal)[i]->update(entry3.tag(), entry3);
        }
    }

    {
        for(size_t i = 0; i < params.pAdditionalHal->size(); ++i)
        {
            if (!params.needReconfigure)
            {
                if (!bRepeating || mvTargetRrzoSize[i].size() == 0)
                {
                    IMetadata::IEntry Crop = (*params.pAdditionalHal)[i]->entryFor(MTK_P1NODE_SENSOR_CROP_REGION);
                    if( Crop.isEmpty() ) {
                        Crop = params.pRequest_AppControl->entryFor(MTK_SCALER_CROP_REGION);
                        if (Crop.isEmpty())
                        {
                            MY_LOGW("cannot get scaler crop region, index : %zu", i);
                            continue;
                        }
                    }
                    MSize rrzoBufSize = params.RrzoSize[i];
                    MRect cropRegion = Crop.itemAt(0, Type2Type<MRect>());
                    #define ALIGN16(x) (((x) + 15) & ~(15))
                    if((cropRegion.s.w * rrzoBufSize.h) > (cropRegion.s.h * rrzoBufSize.w))
                    {
                        MINT32 temp = rrzoBufSize.h;
                        rrzoBufSize.h = ALIGN16(rrzoBufSize.w * cropRegion.s.h / cropRegion.s.w);
                        if (rrzoBufSize.h > temp)
                        {
                            rrzoBufSize.h = temp;
                        }
                    }
                    else
                    {
                        MINT32 temp = rrzoBufSize.w;
                        rrzoBufSize.w = ALIGN16(rrzoBufSize.h * cropRegion.s.w / cropRegion.s.h);
                        if (rrzoBufSize.w > temp)
                        {
                            rrzoBufSize.w = temp;
                        }
                    }
                    #undef ALIGN16
                    //mvTargetRrzoSize.push_back(rrzoBufSize);
                    mvTargetRrzoSize[i].w = rrzoBufSize.w;
                    mvTargetRrzoSize[i].h = rrzoBufSize.h;
                }
                IMetadata::IEntry Rrzotag(MTK_P1NODE_RESIZER_SET_SIZE);
                Rrzotag.push_back(mvTargetRrzoSize[i], Type2Type<MSize>());
                (*params.pAdditionalHal)[i]->update(MTK_P1NODE_RESIZER_SET_SIZE, Rrzotag);
            }
            else
            {
                mvTargetRrzoSize[i].w = 0;
                mvTargetRrzoSize[i].h = 0;
            }
        }
    }

    if (params.pRequest_AppImageStreamInfo->pAppImage_Output_RAW16 == nullptr && params.pSensorId != nullptr)
    {
        do
        {
            {
                IMetadata::IEntry Crop = (*params.pAdditionalHal)[0]->entryFor(MTK_P1NODE_SENSOR_CROP_REGION);
                if( !(Crop.isEmpty()) )
                {
                    break;
                }
                int Id = (*(params.pSensorId))[0];
                MRect ActiveArray;
                sp<IMetadataProvider> pMetadataProvider = NSMetadataProviderManager::valueFor(Id);
                if( ! pMetadataProvider.get() ) {
                    CAM_LOGE(" ! pMetadataProvider.get(), id : %d ", Id);
                    break;
                }

                IMetadata static_meta = pMetadataProvider->getMtkStaticCharacteristics();
                {
                    IMetadata::IEntry active_array_entry = static_meta.entryFor(MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION);
                    if( !active_array_entry.isEmpty() ) {
                        ActiveArray = active_array_entry.itemAt(0, Type2Type<MRect>());
                    } else {
                        CAM_LOGE("no static info: MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION");
                        break;
                    }
                }
                Crop = params.pRequest_AppControl->entryFor(MTK_SCALER_CROP_REGION);
                if (Crop.isEmpty())
                {
                    MY_LOGW("cannot get scaler crop region, index : %d", 0);
                    break;
                }
                //MSize sensorSize = (*params.pSensorSize)[i];
                MRect cropRegion = Crop.itemAt(0, Type2Type<MRect>());

                if (cropRegion.p.x == 0 && cropRegion.p.y == 0 && cropRegion.s.w == ActiveArray.s.w && cropRegion.s.h == ActiveArray.s.h)
                {
                    cropRegion.p.x = 2;
                    cropRegion.p.y = 2;
                    cropRegion.s.w = (*params.pSensorSize)[0].w - 4;
                    cropRegion.s.h = (*params.pSensorSize)[0].h - 4;
                    IMetadata::setEntry<MRect>((*params.pAdditionalHal)[0].get(), MTK_P1NODE_SENSOR_CROP_REGION, cropRegion);
                }

            }
        } while(0);

    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto makePolicy_RequestMetadata_Default(
    CreationParams const& params
) -> std::shared_ptr<IRequestMetadataPolicy>
{
    return std::make_shared<RequestMetadataPolicy_Default>(params);
}


/******************************************************************************
 *
 ******************************************************************************/
RequestMetadataPolicy_DebugDump::
RequestMetadataPolicy_DebugDump(
    CreationParams const& params
)
    :mPolicyParams(params)
{
}


/******************************************************************************
 *
 ******************************************************************************/
auto
RequestMetadataPolicy_DebugDump::
evaluateRequest(
    EvaluateRequestParams const& params
) -> int
{
    if ( CC_LIKELY(mPolicyParams.pRequestMetadataPolicy != nullptr) )
    {
        mPolicyParams.pRequestMetadataPolicy->evaluateRequest(params);
    }

    //
    int debugRayType = property_get_int32("vendor.debug.camera.raw.type", -1);
    if(debugRayType >= 0)
    {
        MY_LOGD("set vendor.debug.camera.raw.type(%d) => MTK_P1NODE_RAW_TYPE(%d)  0:processed-raw 1:pure-raw",debugRayType,debugRayType);
        IMetadata::IEntry entry(MTK_P1NODE_RAW_TYPE);
        entry.push_back(debugRayType, Type2Type< int >());
        for(size_t i=0;i<params.pAdditionalHal->size();++i)
            (*params.pAdditionalHal)[i]->update(entry.tag(), entry);
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
auto makePolicy_RequestMetadata_DebugDump(
    CreationParams const& params
) -> std::shared_ptr<IRequestMetadataPolicy>
{
    return std::make_shared<RequestMetadataPolicy_DebugDump>(params);
}
};  //namespace requestmetadata
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

