/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-P2NodeDecisionPolicy"

#include <mtkcam3/pipeline/policy/IP2NodeDecisionPolicy.h>
//
#include "MyUtils.h"


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::pipeline::policy;
using namespace NSCam::v3::pipeline::policy::p2nodedecision;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)

#define MAX_IMAGE_SIZE(_target, _compete)      \
    do {                                       \
        if (_compete.size() > _target.size() ) \
            _target = _compete;                \
    } while (0)                                \
/******************************************************************************
 *
 ******************************************************************************/
static
auto
checkIsCaptureControl(
    IMetadata const* pRequest_AppControl,
    ParsedMetaControl const* pRequest_ParsedAppMetaControl
) -> bool
{
    bool isStillCap = false;

    if ( pRequest_ParsedAppMetaControl ) {
        switch (pRequest_ParsedAppMetaControl->control_captureIntent)
        {
        case MTK_CONTROL_CAPTURE_INTENT_STILL_CAPTURE:
            isStillCap = true;
            break;
            //
        case static_cast< uint8_t>(-1L):{//invalid parsed cache; use metadata
            IMetadata::IEntry const& CapIntent = pRequest_AppControl->entryFor(MTK_CONTROL_CAPTURE_INTENT);
            isStillCap = ( !CapIntent.isEmpty() && MTK_CONTROL_CAPTURE_INTENT_STILL_CAPTURE == CapIntent.itemAt(0, Type2Type<MUINT8>()));
            }break;
            //
        default:
            break;
        }
    }

    return isStillCap;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
isCustomCapStream(
    IMetadata const* pRequest_AppControl __unused,
    android::sp<IImageStreamInfo> pImgInfo __unused
) -> bool
{
    return false;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
isCustomPrvStream(
    IMetadata const* pRequest_AppControl __unused,
    android::sp<IImageStreamInfo> pImgInfo __unused
) -> bool
{
    return false;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
hasCustomStreamingOut(
    RequestInputParams const* in __unused
) -> bool
{
    return false;
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
checkOnlyCapture(
    RequestInputParams const* in
) -> bool
{
    auto const pRequest_AppImageStreamInfo = in->pRequest_AppImageStreamInfo;

    bool ret = false;
    if (in->hasP2StreamNode == false)
    {
        return true;
    }
    if (!(pRequest_AppImageStreamInfo->hasVideoConsumer))
    {
        if ( pRequest_AppImageStreamInfo->pAppImage_Jpeg != nullptr
          || pRequest_AppImageStreamInfo->pAppImage_Input_Yuv != nullptr
          || pRequest_AppImageStreamInfo->pAppImage_Input_Priv != nullptr
          || checkIsCaptureControl(in->pRequest_AppControl, in->pRequest_ParsedAppMetaControl))
        {
            ret = true;
        }
    }
    return ret && !(hasCustomStreamingOut(in));
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
handleFD(
    RequestOutputParams* out,
    RequestInputParams const* in
) -> void
{
    //  FD is enable only when:
    //  - A FD request is sent from users, and
    //  - P2StreamNode is enabled due to other output image streams.
    if ( in->isFdEnabled ) {
        out->vImageStreamId_from_StreamNode.push_back(in->pConfiguration_StreamInfo_NonP1->pHalImage_FD_YUV->getStreamId());
        out->needP2StreamNode = true;
    }
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
handleAllImageStream_ExceptFD(
    std::vector<StreamId_T>& vImageId,
    MSize&                   maxSize,
    RequestInputParams const* in
) -> void
{
    auto const pConfiguration_StreamInfo_NonP1 = in->pConfiguration_StreamInfo_NonP1;
    auto const pRequest_AppImageStreamInfo = in->pRequest_AppImageStreamInfo;

    for (auto const& it : pRequest_AppImageStreamInfo->vAppImage_Output_Proc) {
        vImageId.push_back(it.first);
        MAX_IMAGE_SIZE(maxSize, it.second->getImgSize());
    }
    if (pRequest_AppImageStreamInfo->pAppImage_Jpeg != nullptr) {
        vImageId.push_back(pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV->getStreamId());
        MAX_IMAGE_SIZE(maxSize, pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV->getImgSize());
        if (in->needThumbnail)
        {
            vImageId.push_back(pConfiguration_StreamInfo_NonP1->pHalImage_Thumbnail_YUV->getStreamId());
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
decideStreamOut(
    RequestOutputParams* out,
    RequestInputParams const* in
) -> void
{
    auto const pConfiguration_StreamInfo_NonP1 = in->pConfiguration_StreamInfo_NonP1;
    auto const pConfiguration_StreamInfo_P1    = in->pConfiguration_StreamInfo_P1;
    auto const pRequest_AppControl = in->pRequest_AppControl;
    auto const pRequest_AppImageStreamInfo = in->pRequest_AppImageStreamInfo;

    auto& needP2CaptureNode = out->needP2CaptureNode;
    auto& needP2StreamNode = out->needP2StreamNode;


    needP2CaptureNode = false;
    needP2StreamNode = false;
    {
        auto& imageIds_Capture = out->vImageStreamId_from_CaptureNode;
        auto& imageIds_Stream = out->vImageStreamId_from_StreamNode;

        for (auto const& it : pRequest_AppImageStreamInfo->vAppImage_Output_Proc)
        {
            if (isCustomCapStream(pRequest_AppControl, it.second))
            {
                needP2CaptureNode = true;
                imageIds_Capture.push_back(it.first);
                MAX_IMAGE_SIZE(out->maxP2CaptureSize, it.second->getImgSize());
            }
            else if (isCustomPrvStream(pRequest_AppControl, it.second))
            {
                needP2StreamNode = true;
                imageIds_Stream.push_back(it.first);
                MAX_IMAGE_SIZE(out->maxP2StreamSize, it.second->getImgSize());
            }
            else if ( ((pRequest_AppImageStreamInfo->pAppImage_Jpeg != nullptr)
                    || checkIsCaptureControl(pRequest_AppControl, in->pRequest_ParsedAppMetaControl))
                    && !pRequest_AppImageStreamInfo->hasVideoConsumer )
            {
                needP2CaptureNode = true;
                imageIds_Capture.push_back(it.first);
                MAX_IMAGE_SIZE(out->maxP2CaptureSize, it.second->getImgSize());
            }
            else
            {
                needP2StreamNode = true;
                imageIds_Stream.push_back(it.first);
                MAX_IMAGE_SIZE(out->maxP2StreamSize, it.second->getImgSize());
            }
        }
        if (pRequest_AppImageStreamInfo->pAppImage_Jpeg != nullptr)
        {
            StreamId_T streamId_Thumbnail_YUV = (in->needThumbnail) ? pConfiguration_StreamInfo_NonP1->pHalImage_Thumbnail_YUV->getStreamId() : (-1);

            auto const& stream_Jpeg_YUV = pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV;
            if (pRequest_AppImageStreamInfo->hasVideoConsumer &&
                stream_Jpeg_YUV->getImgSize().size() <= pConfiguration_StreamInfo_P1->pHalImage_P1_Rrzo->getImgSize().size())
            {
                needP2StreamNode = true;
                imageIds_Stream.push_back(stream_Jpeg_YUV->getStreamId());
                MAX_IMAGE_SIZE(out->maxP2StreamSize, stream_Jpeg_YUV->getImgSize());
                if  ( streamId_Thumbnail_YUV >= 0 ) {
                    imageIds_Stream.push_back(streamId_Thumbnail_YUV);
                }
            }
            else
            {
                needP2CaptureNode = true;
                imageIds_Capture.push_back(stream_Jpeg_YUV->getStreamId());
                MAX_IMAGE_SIZE(out->maxP2CaptureSize, stream_Jpeg_YUV->getImgSize());
                if  ( streamId_Thumbnail_YUV >= 0 ) {
                    imageIds_Capture.push_back(streamId_Thumbnail_YUV);
                }
            }
        }

        handleFD(out, in);
    }

    // metadata
    {
        auto& metaIds_Capture = out->vMetaStreamId_from_CaptureNode;
        auto& metaIds_Stream = out->vMetaStreamId_from_StreamNode;

        if (needP2CaptureNode)
        {
            metaIds_Capture.push_back(pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode->getStreamId());
            metaIds_Capture.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2CaptureNode->getStreamId());
        }
        if (needP2StreamNode)
        {
            metaIds_Stream.push_back(pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2StreamNode->getStreamId());
            if (!needP2CaptureNode)
            {
                metaIds_Stream.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode->getStreamId());
                if (pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1)
                {
                    metaIds_Stream.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1->getStreamId());
                }
                if (pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2)
                {
                    metaIds_Stream.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2->getStreamId());
                }
            }
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
static
auto
evaluateRequest(
    RequestOutputParams& out,
    RequestInputParams const& in
) -> int
{
    auto const pConfiguration_StreamInfo_NonP1 = in.pConfiguration_StreamInfo_NonP1;

    if (!(in.hasP2CaptureNode) && !(in.hasP2StreamNode))
    {
        out.needP2CaptureNode = false;
        out.needP2StreamNode = false;
        MY_LOGD("didn't have p2 node.....");
        return OK;
    }
    if (!(in.hasP2CaptureNode))
    {
        MY_LOGD("Only use P2S node");

        handleAllImageStream_ExceptFD(out.vImageStreamId_from_StreamNode, out.maxP2StreamSize, &in);

        out.needP2CaptureNode = false;
        out.needP2StreamNode = !out.vImageStreamId_from_StreamNode.empty();
        handleFD(&out, &in);

        if  ( out.needP2StreamNode )
        {
            out.vMetaStreamId_from_StreamNode.push_back(pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2StreamNode->getStreamId());
            out.vMetaStreamId_from_StreamNode.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode->getStreamId());
            if (pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1)
            {
                out.vMetaStreamId_from_StreamNode.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main1->getStreamId());
            }
            if (pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2)
            {
                out.vMetaStreamId_from_StreamNode.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2StreamNode_Main2->getStreamId());
            }
        }
        return OK;
    }
    if ( checkOnlyCapture(&in) )
    {
        MY_LOGD("Only use P2C node");
        handleAllImageStream_ExceptFD(out.vImageStreamId_from_CaptureNode, out.maxP2StreamSize, &in);

        out.needP2CaptureNode = !out.vImageStreamId_from_CaptureNode.empty();
        out.needP2StreamNode = false;
        handleFD(&out, &in);

        if  ( out.needP2CaptureNode )
        {
            out.vMetaStreamId_from_CaptureNode.push_back(pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode->getStreamId());
            out.vMetaStreamId_from_CaptureNode.push_back(pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicP2CaptureNode->getStreamId());
        }
        if  ( out.needP2StreamNode )
        {
            out.vMetaStreamId_from_StreamNode.push_back(pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2StreamNode->getStreamId());
        }
        return OK;
    }
    decideStreamOut(&out, &in);
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {


/**
 * Make a function target as a policy - default version
 */
FunctionType_P2NodeDecisionPolicy makePolicy_P2NodeDecision_Default()
{
    return evaluateRequest;
}


};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

