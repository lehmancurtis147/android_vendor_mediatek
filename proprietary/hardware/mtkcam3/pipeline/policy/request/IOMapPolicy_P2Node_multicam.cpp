/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-P2NodeIOMapPolicyMC"

#include <mtkcam3/pipeline/policy/IIOMapPolicy.h>
//
#include <mtkcam3/pipeline/hwnode/NodeId.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>

#include <bitset>
#include <map>
#include <unordered_map>
#include <utility>
//
#include "MyUtils.h"

#define MAX_SUPPORT_CAMERA 2
/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam::v3::NSPipelineContext;
using namespace NSCam::v3::pipeline::policy::iomap;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {

static
bool
dumpIOMap(IOMap &iomap)
{
    android::String8 val("");
    for(int i=0;i<iomap.sizeIn();i++)
    {
        val.appendFormat("in(%" PRIx64 ")", iomap.vIn[i]);
    }
    for(int i=0;i<iomap.sizeOut();i++)
    {
        val.appendFormat("out(%" PRIx64 ")", iomap.vOut[i]);
    }
    MY_LOGD("%s", val.string());
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
static
bool
getIsNeedImgo(StreamId_T streamid, RequestInputParams const& in, MSize rrzoSize)
{
    #define IS_IMGO(size, threshold) (size.w > threshold.w || size.h > threshold.h)
    for( const auto& n : in.pRequest_AppImageStreamInfo->vAppImage_Output_Proc ) {
        if (streamid == n.first)
        {
            return IS_IMGO(n.second->getImgSize(), rrzoSize);
        }
    }
    #undef IS_IMGO

    if ( streamid == in.pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV->getStreamId() )
        return true;

    if ( streamid == in.pConfiguration_StreamInfo_NonP1->pHalImage_Thumbnail_YUV->getStreamId() )
        return true;

    if ( streamid == in.pConfiguration_StreamInfo_NonP1->pHalImage_FD_YUV->getStreamId() )
        return false;

    MY_LOGW("Cannot find streamId : %lld and should not be here, check input parameter please", streamid);
    return false;
}

/******************************************************************************
 *
 ******************************************************************************/
static
bool
isPhysicalStream(StreamId_T streamid, RequestInputParams const& in, bool isMain)
{
    //
    if (in.pPipelineStaticInfo->sensorId.size() <= 1) {
        MY_LOGE("Sensor size <=1, no need to check  physical stream.");
        return false;
    }
    //
    for( const auto& n : in.pRequest_AppImageStreamInfo->vAppImage_Output_Proc ) {
        if (streamid == n.first)
        {
            char const* sensorId = n.second->getPhysicalCameraId();
            // 0-length string is not a physical output stream
            if (strlen(sensorId) == 0) {
                return false;
            }
            //
            if (atoi(sensorId) == in.pPipelineStaticInfo->sensorId[isMain? 0 : 1]) {
                return true;
            }
        }
    }
    return false;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
add_Logical_Multicam_IOMap_In(StreamId_T streamid, IOMap & logicalIOMap, IOMap & physicalIOMap)
{
    logicalIOMap.addIn(streamid);
    physicalIOMap.addIn(streamid);
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
add_Logical_Multicam_IOMap_Out(StreamId_T streamid,
    RequestInputParams const& in,
    IOMap & logicalIOMap,
    IOMap & mainIOMap,
    IOMap & subIOMap
)
{
    if (isPhysicalStream(streamid, in, true /*main*/)) {
        mainIOMap.addOut(streamid);
    } else if (isPhysicalStream(streamid, in, false /*sub*/)) {
        subIOMap.addOut(streamid);
    } else {
        logicalIOMap.addOut(streamid);
    }
    return OK;
}

/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_P2StreamNode_multicam(
    RequestOutputParams& out,
    RequestInputParams const& in
)
{
    if ( !in.pRequest_PipelineNodesNeed->needP2StreamNode || !in.isMainFrame )
    {
        MY_LOGD("No need P2StreamNode");
        return OK;
    }

    auto const& needP1Node = in.pRequest_PipelineNodesNeed->needP1Node;
    auto const& needP1Dma = *in.pRequest_NeedP1Dma;

    IOMap logicalImgoMap, logicalRrzoMap, logicalMetaMap, logicalSubMetaMap;
    IOMap physicalMain1ImgoMap, physicalMain1RrzoMap, physicalMain1MetaMap, physicalMain1SubMetaMap;
    IOMap physicalMain2ImgoMap, physicalMain2RrzoMap, physicalMain2MetaMap, physicalMain2SubMetaMap;
    sp<IImageStreamInfo> main1RrzoInfo;
    bool bMainHasImgo = false, bMainHasRrzo = false;
    for (size_t i = 0; i < needP1Node.size(); i++)
    {
        if ( ! needP1Node[i] ) {
            continue;
        }

        auto& p1Info = (*in.pConfiguration_StreamInfo_P1)[i];
        const auto need_P1Dma = needP1Dma[i];
        const MBOOL isMain = (i == 0);

        if (isMain)
        {
            sp<IImageStreamInfo> imgoInfo = in.pRequest_AppImageStreamInfo->pAppImage_Output_Priv.get()
                                                ? in.pRequest_AppImageStreamInfo->pAppImage_Output_Priv
                                                : p1Info.pHalImage_P1_Imgo;

            main1RrzoInfo = p1Info.pHalImage_P1_Rrzo;

            if (need_P1Dma & P1_IMGO) {
                add_Logical_Multicam_IOMap_In(imgoInfo->getStreamId(), logicalImgoMap, physicalMain1ImgoMap);
                bMainHasImgo = true;
            }

            if (need_P1Dma & P1_RRZO) {
                add_Logical_Multicam_IOMap_In(main1RrzoInfo->getStreamId(), logicalRrzoMap, physicalMain1RrzoMap);
                bMainHasRrzo = true;
            }
        }
        else
        {
            sp<IImageStreamInfo> subRrzoInfo = p1Info.pHalImage_P1_Rrzo;
            sp<IImageStreamInfo> subImgoInfo = p1Info.pHalImage_P1_Imgo;
            if (subRrzoInfo != nullptr)
            {
                if ((need_P1Dma & P1_RRZO) && bMainHasImgo) {
                    add_Logical_Multicam_IOMap_In(subRrzoInfo->getStreamId(), logicalImgoMap, physicalMain2ImgoMap);
                }
                if ((need_P1Dma & P1_RRZO) && bMainHasRrzo) {
                    add_Logical_Multicam_IOMap_In(subRrzoInfo->getStreamId(), logicalRrzoMap, physicalMain2RrzoMap);
                }
            }
            else if (subImgoInfo != nullptr)
            {
                if ((need_P1Dma & P1_IMGO) && bMainHasImgo) {
                    add_Logical_Multicam_IOMap_In(subImgoInfo->getStreamId(), logicalImgoMap, physicalMain2ImgoMap);
                }
                if ((need_P1Dma & P1_IMGO) && bMainHasRrzo) {
                    add_Logical_Multicam_IOMap_In(subImgoInfo->getStreamId(), logicalRrzoMap, physicalMain2RrzoMap);
                }
            }
        }

        if (need_P1Dma & P1_LCSO) {
            if (isMain) {
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Lcso->getStreamId(), logicalImgoMap, physicalMain1ImgoMap);
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Lcso->getStreamId(), logicalRrzoMap, physicalMain1RrzoMap);
            } else {
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Lcso->getStreamId(), logicalImgoMap, physicalMain2ImgoMap);
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Lcso->getStreamId(), logicalRrzoMap, physicalMain2RrzoMap);
            }
        }

        if (need_P1Dma & P1_RSSO) {
            if (isMain) {
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Rsso->getStreamId(), logicalImgoMap, physicalMain1ImgoMap);
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Rsso->getStreamId(), logicalRrzoMap, physicalMain1RrzoMap);
            } else {
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Rsso->getStreamId(), logicalImgoMap, physicalMain2ImgoMap);
                add_Logical_Multicam_IOMap_In(p1Info.pHalImage_P1_Rsso->getStreamId(), logicalRrzoMap, physicalMain2RrzoMap);
            }
        }

        logicalMetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
        logicalMetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        logicalSubMetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
        logicalSubMetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        if (isMain)
        {
            physicalMain1MetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
            physicalMain1MetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
            physicalMain1SubMetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
            physicalMain1SubMetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        }
        else
        {
            physicalMain2MetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
            physicalMain2MetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
            physicalMain2SubMetaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
            physicalMain2SubMetaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        }
    }

    if (!(bMainHasImgo || bMainHasRrzo))
    {
        MY_LOGE("No Imgo or Rrzo");
        return OK;
    }

    auto const vImageStreamId = *in.pvImageStreamId_from_StreamNode;
    if (!bMainHasImgo)
    {
        for (size_t i = 0; i < vImageStreamId.size(); i++)
        {
            add_Logical_Multicam_IOMap_Out(vImageStreamId[i], in, logicalRrzoMap, physicalMain1RrzoMap, physicalMain2RrzoMap);
        }
    }
    else if (!bMainHasRrzo)
    {
        for (size_t i = 0; i < vImageStreamId.size(); i++)
        {
            add_Logical_Multicam_IOMap_Out(vImageStreamId[i], in, logicalImgoMap, physicalMain1ImgoMap, physicalMain2ImgoMap);
        }
    }
    else
    {
        for (size_t i = 0; i < vImageStreamId.size(); i++)
        {
            if ( getIsNeedImgo(vImageStreamId[i], in, main1RrzoInfo->getImgSize()) )
            {
                add_Logical_Multicam_IOMap_Out(vImageStreamId[i], in, logicalImgoMap, physicalMain1ImgoMap, physicalMain2ImgoMap);
            }
            else
            {
                add_Logical_Multicam_IOMap_Out(vImageStreamId[i], in, logicalRrzoMap, physicalMain1RrzoMap, physicalMain2RrzoMap);
            }
        }
    }

    logicalMetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    logicalSubMetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    physicalMain1MetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    physicalMain1SubMetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    physicalMain2MetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    physicalMain2SubMetaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
    for( const auto n : (*in.pvMetaStreamId_from_StreamNode) )
    {
        if (n == eSTREAMID_META_APP_DYNAMIC_02_MAIN1) {
            physicalMain1MetaMap.addOut(n);
        } else if (n == eSTREAMID_META_APP_DYNAMIC_02_MAIN2) {
            physicalMain2MetaMap.addOut(n);
        } else if (n == eSTREAMID_META_APP_DYNAMIC_02 ) {
            if (logicalImgoMap.sizeOut() > 0 || logicalRrzoMap.sizeOut() > 0) {
                logicalMetaMap.addOut(n);
            }
        } else {
            if (logicalImgoMap.sizeOut() > 0 || logicalRrzoMap.sizeOut() > 0) {
                logicalMetaMap.addOut(n);
            } else if (physicalMain1RrzoMap.sizeOut() > 0 || physicalMain1ImgoMap.sizeOut() > 0) {
                physicalMain1MetaMap.addOut(n);
            } else {
                physicalMain2MetaMap.addOut(n);
            }
        }
    }

    IOMapSet P2ImgIO, P2MetaIO;
    if (logicalImgoMap.sizeOut() > 0)
    {
        dumpIOMap(logicalImgoMap);
        P2ImgIO.add(logicalImgoMap);
        out.bP2UseImgo = true;
    }
    if (logicalRrzoMap.sizeOut() > 0)
    {
        dumpIOMap(logicalRrzoMap);
        P2ImgIO.add(logicalRrzoMap);
    }
    // if logical imgo/rrzo map size > 0, add logical metadata iomap
    auto logicalIOMapSize = (logicalImgoMap.sizeOut() + logicalRrzoMap.sizeOut());
    if(logicalIOMapSize > 0)
    {
        dumpIOMap(logicalMetaMap);
        P2MetaIO.add(logicalMetaMap);
        if(logicalIOMapSize > 1)
        {
            dumpIOMap(logicalSubMetaMap);
            P2MetaIO.add(logicalSubMetaMap);
        }
    }
    // physical main1
    if (physicalMain1RrzoMap.sizeOut() > 0)
    {
        dumpIOMap(physicalMain1RrzoMap);
        P2ImgIO.add(physicalMain1RrzoMap);
    }
    if (physicalMain1ImgoMap.sizeOut() > 0)
    {
        dumpIOMap(physicalMain1ImgoMap);
        P2ImgIO.add(physicalMain1ImgoMap);
    }
    // if physical main1 imgo/rrzo map size > 0, add physical metadata iomap
    auto physicalIOMapSize_main1 = (physicalMain1RrzoMap.sizeOut() + physicalMain1ImgoMap.sizeOut());
    if(physicalIOMapSize_main1 > 0)
    {
        dumpIOMap(physicalMain1MetaMap);
        P2MetaIO.add(physicalMain1MetaMap);
        if(physicalIOMapSize_main1 > 1)
        {
            dumpIOMap(physicalMain1SubMetaMap);
            P2MetaIO.add(physicalMain1SubMetaMap);
        }
    }
    // physical main2
    if (physicalMain2RrzoMap.sizeOut() > 0)
    {
        dumpIOMap(physicalMain2RrzoMap);
        P2ImgIO.add(physicalMain2RrzoMap);
    }
    if (physicalMain2ImgoMap.sizeOut() > 0)
    {
        dumpIOMap(physicalMain2ImgoMap);
        P2ImgIO.add(physicalMain2ImgoMap);
    }
    // if physical main2 imgo/rrzo map size > 0, add physical metadata iomap
    auto physicalIOMapSize_main2 = (physicalMain2RrzoMap.sizeOut() + physicalMain2ImgoMap.sizeOut());
    if(physicalIOMapSize_main2 > 0)
    {
        dumpIOMap(physicalMain2MetaMap);
        P2MetaIO.add(physicalMain2MetaMap);
        if(physicalIOMapSize_main2 > 1)
        {
            dumpIOMap(physicalMain2SubMetaMap);
            P2MetaIO.add(physicalMain2SubMetaMap);
        }
    }

    (*out.pNodeIOMapImage)[eNODEID_P2StreamNode] = P2ImgIO;
    (*out.pNodeIOMapMeta) [eNODEID_P2StreamNode] = P2MetaIO;

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_P2CaptureNode_multicam(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    if ( !in.pRequest_PipelineNodesNeed->needP2CaptureNode )
    {
        MY_LOGD("No need P2CaptureNode");
        return OK;
    }
    IOMap logicalImgoMap, metaMap;
    std::vector<IOMap> physicalImageIOMapList;
    std::vector<IOMap> physicalMetaIOMapList;
    std::bitset<MAX_SUPPORT_CAMERA> physicalOutputBitset;
    // mapping table, stream id and sensor index.
    std::unordered_map<StreamId_T, int32_t> physicalStreamIdList;
    bool bImgo = false;
    sp<IImageStreamInfo> imgoInfo;
    auto const& needP1Node = in.pRequest_PipelineNodesNeed->needP1Node;
    auto const& needP1Dma = *in.pRequest_NeedP1Dma;
    // check contain physical stream output or not
    {
        physicalOutputBitset.reset();
        auto const vImageStreamId = *in.pvImageStreamId_from_CaptureNode;
        // only check main frame.
        if(in.isMainFrame)
        {
            // stream
            for(size_t i = 0; i < vImageStreamId.size(); ++i)
            {
                if(isPhysicalStream(vImageStreamId[i], in, true /* main */))
                {
                    physicalOutputBitset.set(0, 1);
                    physicalStreamIdList.insert({vImageStreamId[i], 0});
                }
                else if(isPhysicalStream(vImageStreamId[i], in, false /* sub */))
                {
                    physicalOutputBitset.set(1, 1);
                    physicalStreamIdList.insert({vImageStreamId[i], 1});
                }
            }
        }
    }

    if(needP1Node.size() != physicalOutputBitset.size())
    {
        MY_LOGE("maybe need to check flow");
    }

    for (size_t i = 0; i < needP1Node.size(); i++)
    {
        if ( ! needP1Node[i] ) {
            continue;
        }
        // if need output physical stream, add empty IOMap to physicalImageIOMapList.
        if(physicalOutputBitset.test(i))
        {
            physicalImageIOMapList.push_back(IOMap());
            physicalMetaIOMapList.push_back(IOMap());
        }

        auto& p1Info = (*in.pConfiguration_StreamInfo_P1)[i];
        if (i == 0)
        {
            if (in.pRequest_AppImageStreamInfo->pAppImage_Output_Priv.get())
            {
                imgoInfo = in.pRequest_AppImageStreamInfo->pAppImage_Output_Priv;
            }
            else if (in.pRequest_AppImageStreamInfo->pAppImage_Input_Priv.get())
            {
                imgoInfo = in.pRequest_AppImageStreamInfo->pAppImage_Input_Priv;
            }
            else if (in.pRequest_AppImageStreamInfo->pAppImage_Input_Yuv.get())
            {
                imgoInfo = in.pRequest_AppImageStreamInfo->pAppImage_Input_Yuv;
            }
            else
            {
                imgoInfo = p1Info.pHalImage_P1_Imgo;
            }
        }
        else
        {
            imgoInfo = p1Info.pHalImage_P1_Imgo;
        }

        auto const need_P1Dma = needP1Dma[i];

        if (need_P1Dma & P1_IMGO) {
            logicalImgoMap.addIn(imgoInfo->getStreamId());
            // if need physical out, add to IOMap
            if(physicalOutputBitset.test(i))
            {
                physicalImageIOMapList[i].addIn(imgoInfo->getStreamId());
            }
            bImgo = true;
        }

        if (need_P1Dma & P1_RRZO) {
            logicalImgoMap.addIn(p1Info.pHalImage_P1_Rrzo->getStreamId());
            // if need physical out, add to IOMap
            if(physicalOutputBitset.test(i))
            {
                physicalImageIOMapList[i].addIn(p1Info.pHalImage_P1_Rrzo->getStreamId());
            }
        }

        if (need_P1Dma & P1_LCSO) {
            logicalImgoMap.addIn(p1Info.pHalImage_P1_Lcso->getStreamId());
            // if need physical out, add to IOMap
            if(physicalOutputBitset.test(i))
            {
                physicalImageIOMapList[i].addIn(p1Info.pHalImage_P1_Lcso->getStreamId());
            }
        }

        if (need_P1Dma & P1_RRZO) {
            logicalImgoMap.addIn(p1Info.pHalImage_P1_Rrzo->getStreamId());
            // if need physical out, add to IOMap
            if(physicalOutputBitset.test(i))
            {
                physicalImageIOMapList[i].addIn(p1Info.pHalImage_P1_Rrzo->getStreamId());
            }
        }

        metaMap.addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
        metaMap.addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        if(physicalOutputBitset.test(i))
        {
            physicalMetaIOMapList[i].addIn(p1Info.pAppMeta_DynamicP1->getStreamId());
            physicalMetaIOMapList[i].addIn(p1Info.pHalMeta_DynamicP1->getStreamId());
        }
    }

    auto const vImageStreamId = *in.pvImageStreamId_from_CaptureNode;

    if (!bImgo)
    {
        MY_LOGE("No Imgo");
        return OK;
    }
    if (in.isMainFrame)
    {
        for (size_t i = 0; i < vImageStreamId.size(); i++)
        {
            auto iter = physicalStreamIdList.find(vImageStreamId[i]);
            if(iter != physicalStreamIdList.end())
            {
                physicalImageIOMapList[iter->second].addOut(vImageStreamId[i]);
            }
            else
            {
                logicalImgoMap.addOut(vImageStreamId[i]);
            }
        }
    }

    {
        metaMap.addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
        for(size_t i=0;i<physicalMetaIOMapList.size();++i)
        {
            if(physicalMetaIOMapList[i].sizeOut() > 0)
            {
                physicalMetaIOMapList[i].addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId());
            }
        }
    }

    for( const auto n : (*in.pvMetaStreamId_from_CaptureNode) )
    {
        metaMap.addOut(n);
        for(size_t i=0;i<physicalMetaIOMapList.size();++i)
        {
            if(physicalMetaIOMapList[i].sizeOut() > 0)
            {
                physicalMetaIOMapList[i].addIn(n);
            }
        }
    }

    IOMapSet P2ImgIO, P2MetaIO;
    // jpeg image
    dumpIOMap(logicalImgoMap);
    dumpIOMap(metaMap);
    P2ImgIO.add(logicalImgoMap);
    P2MetaIO.add(metaMap);
    (*out.pNodeIOMapImage)[eNODEID_P2CaptureNode] = IOMapSet().add(logicalImgoMap);
    (*out.pNodeIOMapMeta) [eNODEID_P2CaptureNode] = IOMapSet().add(metaMap);
    // physical output
    for(size_t i=0;i<physicalMetaIOMapList.size();++i)
    {
        if(physicalMetaIOMapList[i].sizeIn() == 0 ||
           physicalMetaIOMapList[i].sizeOut() == 0 ||
           physicalImageIOMapList[i].sizeIn() == 0 ||
           physicalImageIOMapList[i].sizeOut() == 0)
        {
            MY_LOGE("[build IOMap fail] maybe something wrong pmi(%zu) pmo(%zu) pii(%zu) pio(%zu)",
                    physicalMetaIOMapList[i].sizeIn(),
                    physicalMetaIOMapList[i].sizeOut(),
                    physicalImageIOMapList[i].sizeIn(),
                    physicalImageIOMapList[i].sizeOut());
            //break;
        }
        dumpIOMap(physicalImageIOMapList[i]);
        dumpIOMap(physicalMetaIOMapList[i]);
        P2ImgIO.add(physicalImageIOMapList[i]);
        P2MetaIO.add(physicalMetaIOMapList[i]);
    }
    (*out.pNodeIOMapImage)[eNODEID_P2CaptureNode] = P2ImgIO;
    (*out.pNodeIOMapMeta) [eNODEID_P2CaptureNode] = P2MetaIO;

    return OK;
}


/******************************************************************************
 * Make a function target as a policy - multicam version
 ******************************************************************************/
FunctionType_IOMapPolicy_P2Node makePolicy_IOMap_P2Node_multicam()
{
    return [](
        RequestOutputParams& out,
        RequestInputParams const& in
    ) -> int
    {
        evaluateRequest_P2StreamNode_multicam(out, in);
        evaluateRequest_P2CaptureNode_multicam(out, in);
        return OK;
    };
}

};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

