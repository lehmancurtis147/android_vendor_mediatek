/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-NonP2NodeIOMapPolicy"

#include <mtkcam3/pipeline/policy/IIOMapPolicy.h>
//
#include <mtkcam3/pipeline/hwnode/NodeId.h>
#include <mtkcam3/pipeline/hwnode/StreamId.h>
//
#include "MyUtils.h"


/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam::v3::NSPipelineContext;
using namespace NSCam::v3::pipeline::policy::iomap;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_Jpeg(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    if (  !in.pRequest_PipelineNodesNeed->needJpegNode
       || !in.pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV.get()
       || !in.isMainFrame )
    {
        MY_LOGD("No need Jpeg node");
        return OK;
    }
    //
    StreamId_T streamid = in.pConfiguration_StreamInfo_NonP1->pHalImage_Jpeg_YUV->getStreamId();
    bool isStreamNode = false;
    for( const auto n : (*in.pvImageStreamId_from_StreamNode) )
    {
        if (n == streamid)
        {
            isStreamNode = true;
            break;
        }
    }
    //
    IOMap JpegMap;
    JpegMap.addIn(streamid);
    if (in.pRequest_HalImage_Thumbnail_YUV != NULL)
    {
        JpegMap.addIn(in.pRequest_HalImage_Thumbnail_YUV->getStreamId());
    }
    JpegMap.addOut(in.pRequest_AppImageStreamInfo->pAppImage_Jpeg->getStreamId());
    //
    (*out.pNodeIOMapImage)[eNODEID_JpegNode] = IOMapSet().add(JpegMap);
    (*out.pNodeIOMapMeta) [eNODEID_JpegNode] = IOMapSet().add(IOMap()
                                              .addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId())
                                              .addIn(isStreamNode ? in.pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2StreamNode->getStreamId()
                                                                  : in.pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2CaptureNode->getStreamId())
                                              .addOut(in.pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicJpeg->getStreamId()));
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_FD(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    if ( !in.pRequest_PipelineNodesNeed->needFDNode || !in.isMainFrame )
    {
        MY_LOGD("No need FD node");
        return OK;
    }
    //
    if ( in.pPipelineStaticInfo->isP1DirectFDYUV )
    {
        (*out.pNodeIOMapImage)[eNODEID_FDNode] = IOMapSet().add(IOMap().addIn((*(in.pConfiguration_StreamInfo_P1))[0].pHalImage_P1_FDYuv->getStreamId()));
        (*out.pNodeIOMapMeta) [eNODEID_FDNode] = IOMapSet().add(IOMap()
                                            .addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId())
                                            .addIn((*(in.pConfiguration_StreamInfo_P1))[0].pHalMeta_DynamicP1->getStreamId())
                                            .addOut(in.pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicFD->getStreamId()));
    }
    else
    {
        (*out.pNodeIOMapImage)[eNODEID_FDNode] = IOMapSet().add(IOMap().addIn(in.pConfiguration_StreamInfo_NonP1->pHalImage_FD_YUV->getStreamId()));
        (*out.pNodeIOMapMeta) [eNODEID_FDNode] = IOMapSet().add(IOMap()
                                            .addIn(in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId())
                                            .addIn(in.pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicP2StreamNode->getStreamId())
                                            .addOut(in.pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicFD->getStreamId()));
    }

    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_PDE(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    if ( !in.pRequest_PipelineNodesNeed->needPDENode || !in.isMainFrame )
    {
        MY_LOGD("No need PDE node");
        return OK;
    }
    //
    (*out.pNodeIOMapImage)[eNODEID_PDENode] = IOMapSet().add(IOMap().addIn((*in.pConfiguration_StreamInfo_P1)[0].pHalImage_P1_Imgo->getStreamId()));
    (*out.pNodeIOMapMeta) [eNODEID_PDENode] = IOMapSet().add(IOMap()
                                             .addIn((*in.pConfiguration_StreamInfo_P1)[0].pHalMeta_DynamicP1->getStreamId())
                                             .addOut(in.pConfiguration_StreamInfo_NonP1->pHalMeta_DynamicPDE->getStreamId()));
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_Raw16(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    if ( !in.pRequest_PipelineNodesNeed->needRaw16Node || !in.isMainFrame )
    {
        MY_LOGD("No need Raw16 node");
        return OK;
    }
    (*out.pNodeIOMapImage)[eNODEID_RAW16] = IOMapSet().add(IOMap()
                                              .addIn((*in.pConfiguration_StreamInfo_P1)[0].pHalImage_P1_Imgo->getStreamId())
                                              .addOut(in.pRequest_AppImageStreamInfo->pAppImage_Output_RAW16->getStreamId()));
    (*out.pNodeIOMapMeta) [eNODEID_RAW16] = IOMapSet().add(IOMap()
                                             .addIn((*in.pConfiguration_StreamInfo_P1)[0].pHalMeta_DynamicP1->getStreamId())
                                             .addIn((*in.pConfiguration_StreamInfo_P1)[0].pAppMeta_DynamicP1->getStreamId())
                                             .addOut(in.pConfiguration_StreamInfo_NonP1->pAppMeta_DynamicRAW16->getStreamId()));
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
static
MERROR
evaluateRequest_Pass1(
    RequestOutputParams const& out,
    RequestInputParams const& in
)
{
    auto addOut = [](IOMap& iomap, auto const& pStream) {
        if ( pStream != nullptr ) {
            iomap.addOut(pStream->getStreamId());
            return true;
        }
        return false;
    };

    auto pRequest_AppImageStreamInfo = in.pRequest_AppImageStreamInfo;
    auto pRequest_PipelineNodesNeed = in.pRequest_PipelineNodesNeed;
    auto const& needP1Node = in.pRequest_PipelineNodesNeed->needP1Node;
    auto const& needP1Dma = *in.pRequest_NeedP1Dma;
    for (size_t i = 0; i < needP1Node.size(); i++)
    {
        bool isSkip =  ( ! needP1Node[i] )
                    || CC_UNLIKELY( i >= needP1Dma.size() )
                    || CC_UNLIKELY( i >= (*in.pConfiguration_StreamInfo_P1).size() )
                        ;
        if ( isSkip ) {
            continue;
        }

        auto const need_P1Dma = needP1Dma[i];
        auto const& configP1Streams = (*in.pConfiguration_StreamInfo_P1)[i];

        NodeId_T nodeId = 0;
        IOMap imageMap;
        switch (i)
        {
        case 0:{
            nodeId = eNODEID_P1Node;

            bool canRRZO = true;
            bool canLCSO = true;
            bool canRSSO = true;
            bool canFDYUV= true;

            // IMGO
            {
                IImageStreamInfo* pImgoInput = nullptr;
                IImageStreamInfo* pImgoOutput = nullptr;
                ////////////////////////////////////////////////////////////////
                // Input/Output App stream
                if ( in.isMainFrame )
                {
                    ////////////////////////////////////////////////////////////
                    // Input App stream
                    if ( auto p = pRequest_AppImageStreamInfo->pAppImage_Input_Yuv.get() ) {
                        pImgoInput = p;
                        canRRZO = canLCSO = canRSSO = canFDYUV = false;
                    }
                    else
                    if ( auto p = pRequest_AppImageStreamInfo->pAppImage_Input_Priv.get() ) {
                        pImgoInput = p;
                        canRRZO = canLCSO = canRSSO = canFDYUV = false;
                    }
                    else
                    if ( auto p = pRequest_AppImageStreamInfo->pAppImage_Input_RAW16.get() ) {
                        pImgoInput = p;
                        canRRZO = canLCSO = canRSSO = canFDYUV = false;
                    }
                    ////////////////////////////////////////////////////////////
                    // Output App stream
                    else
                    if ( auto p = pRequest_AppImageStreamInfo->pAppImage_Output_Priv.get() )
                    {// P1Node directly outputs App private stream.
                        pImgoOutput = p;
                    }
                    else
                    if (    false == pRequest_PipelineNodesNeed->needRaw16Node
                        &&  (pImgoOutput = pRequest_AppImageStreamInfo->pAppImage_Output_RAW16.get())
                       )
                    {// P1Node directly outputs App RAW16 stream: (if no Raw16Node).
                    }
                }

                ////////////////////////////////////////////////////////////////
                // Output Hal stream (IMGO)
                if ( pImgoOutput == nullptr && pImgoInput == nullptr ) {
                    if ( (need_P1Dma & P1_IMGO) ) {
                        pImgoOutput = (*in.pConfiguration_StreamInfo_P1)[i].pHalImage_P1_Imgo.get();
                    }
                }

                ////////////////////////////////////////////////////////////////
                if ( pImgoOutput != nullptr ) {
                    imageMap.addOut(pImgoOutput->getStreamId());
                }
                else
                if ( pImgoInput != nullptr ) {
                    imageMap.addIn(pImgoInput->getStreamId());
                }
            }

            if ( canRRZO && (need_P1Dma & P1_RRZO) ) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Rrzo );
            }
            if ( canLCSO && (need_P1Dma & P1_LCSO) ) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Lcso );
            }
            if ( canRSSO && (need_P1Dma & P1_RSSO) ) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Rsso );
            }
            if ( canFDYUV && (need_P1Dma & P1_FDYUV) ) {
                addOut(imageMap, configP1Streams.pHalImage_P1_FDYuv );
            }

            }break;

        default:
        case 1:/* P1 Main2 */{
            nodeId = eNODEID_P1Node_main2;

            if (need_P1Dma & P1_IMGO) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Imgo );
            }
            if (need_P1Dma & P1_RRZO) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Rrzo );
            }
            if (need_P1Dma & P1_LCSO) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Lcso );
            }
            if (need_P1Dma & P1_RSSO) {
                addOut(imageMap, configP1Streams.pHalImage_P1_Rsso );
            }
            }break;
        }

        // dummy frame does not need image IOMap
        if ( ! in.isDummyFrame )
            (*out.pNodeIOMapImage)[nodeId] = IOMapSet().add(imageMap);

        (*out.pNodeIOMapMeta) [nodeId] = IOMapSet().add(IOMap()
                                        .addIn( in.pConfiguration_StreamInfo_NonP1->pAppMeta_Control->getStreamId())
                                        .addIn( configP1Streams.pHalMeta_Control->getStreamId())
                                        .addOut(configP1Streams.pAppMeta_DynamicP1->getStreamId())
                                        .addOut(configP1Streams.pHalMeta_DynamicP1->getStreamId()));
    }
    return OK;
}


/******************************************************************************
 * Make a function target as a policy - default version
 ******************************************************************************/
FunctionType_IOMapPolicy_NonP2Node makePolicy_IOMap_NonP2Node_Default()
{
    return [](
        RequestOutputParams const& out,
        RequestInputParams const& in
    ) -> int
    {
        evaluateRequest_Jpeg (out, in);
        evaluateRequest_FD   (out, in);
        evaluateRequest_PDE  (out, in);
        evaluateRequest_Raw16(out, in);
        evaluateRequest_Pass1(out, in);
        return OK;
    };
}


};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

