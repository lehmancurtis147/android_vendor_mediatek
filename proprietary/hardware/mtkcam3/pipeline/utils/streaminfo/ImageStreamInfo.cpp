/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/StreamInfo"
//
#include <inttypes.h>
//
#include <cutils/compiler.h>
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam3/pipeline/utils/streaminfo/ImageStreamInfo.h>
//
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;


/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)


/******************************************************************************
 *
 ******************************************************************************/
ImageStreamInfoBuilder::
ImageStreamInfoBuilder(IImageStreamInfo const* pInfo)
    : mStreamName(pInfo->getStreamName())
    , mStreamId(pInfo->getStreamId())
    , mStreamType(pInfo->getStreamType())
    , mMaxBufNum(pInfo->getMaxBufNum())
    , mMinInitBufNum(pInfo->getMinInitBufNum())
    , mUsageForAllocator(pInfo->getUsageForAllocator())
    , mAllocImgFormat(pInfo->getAllocImgFormat())
    , mAllocBufPlanes(pInfo->getAllocBufPlanes())
    , mTransform(pInfo->getTransform())
    , mDataSpace(pInfo->getDataSpace())
    , mImageBufferInfo(pInfo->getImageBufferInfo())
{
}


/******************************************************************************
 *
 ******************************************************************************/
auto
ImageStreamInfoBuilder::
build() const -> android::sp<IImageStreamInfo>
{
    android::sp<ImageStreamInfo>
        pStreamInfo = new ImageStreamInfo(
            mStreamName.c_str(),
            mStreamId,
            mStreamType,
            mMaxBufNum,
            mMinInitBufNum,
            mUsageForAllocator,
            mAllocImgFormat,
            mAllocBufPlanes,
            mTransform,
            mDataSpace,
            mImageBufferInfo
        );

    if (CC_UNLIKELY(pStreamInfo==nullptr)) {
        MY_LOGE(
            "Fail on build a new ImageStreamInfo: %#" PRIx64 "(%s)",
            mStreamId, mStreamName.c_str()
        );
    }
    return pStreamInfo;
}


/******************************************************************************
 *
 ******************************************************************************/
ImageStreamInfo::
ImageStreamInfo(
    char const*             streamName,
    StreamId_T              streamId,
    MUINT32                 streamType,
    size_t                  maxBufNum,
    size_t                  minInitBufNum,
    MUINT                   usageForAllocator,
    MINT                    allocImgFormat,
    BufPlanes_t const&      allocBufPlanes,
    MUINT32                 transform,
    MUINT32                 dataSpace,
    ImageBufferInfo const&  imageBufferInfo,
    SecureInfo const&       secureInfo
)
    : IImageStreamInfo()
    , mImp(
        streamName, streamId, streamType, maxBufNum, minInitBufNum
    )
    , mUsageForAllocator(usageForAllocator)
    , mAllocImgFormat(allocImgFormat)
    , mvAllocBufPlanes(allocBufPlanes)
    , mTransform(transform)
    , mDataSpace(dataSpace)
    , mSecureInfo(secureInfo)
    , mImageBufferInfo(imageBufferInfo)
{
}


/******************************************************************************
 *
 ******************************************************************************/
ImageStreamInfo::
ImageStreamInfo(
    char const*         streamName,
    StreamId_T          streamId,
    MUINT32             streamType,
    size_t              maxBufNum,
    size_t              minInitBufNum,
    MUINT               usageForAllocator,
    MINT                imgFormat,
    MSize const&        imgSize,
    BufPlanes_t const&  bufPlanes,
    MUINT32             transform,
    MUINT32             dataSpace,
    SecureInfo const&   secureInfo
)
    : IImageStreamInfo()
    , mImp(
        streamName, streamId, streamType, maxBufNum, minInitBufNum
    )
    , mUsageForAllocator(usageForAllocator)
    , mAllocImgFormat(imgFormat)
    , mvAllocBufPlanes(bufPlanes)
    , mTransform(transform)
    , mDataSpace(dataSpace)
    , mSecureInfo(secureInfo)
{
    mImageBufferInfo.bufOffset.push_back(0);
    mImageBufferInfo.bufPlanes  = bufPlanes;
    mImageBufferInfo.imgFormat  = imgFormat;
    mImageBufferInfo.imgSize    = imgSize;
}


/******************************************************************************
 *
 ******************************************************************************/
char const*
ImageStreamInfo::
getStreamName() const
{
    return  mImp.getStreamName();
}


/******************************************************************************
 *
 ******************************************************************************/
IStreamInfo::StreamId_T
ImageStreamInfo::
getStreamId() const
{
    return  mImp.getStreamId();
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT32
ImageStreamInfo::
getStreamType() const
{
    return  mImp.getStreamType();
}


/******************************************************************************
 *
 ******************************************************************************/
size_t
ImageStreamInfo::
getMaxBufNum() const
{
    return  mImp.getMaxBufNum();
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
ImageStreamInfo::
setMaxBufNum(size_t count)
{
    mImp.setMaxBufNum(count);
}


/******************************************************************************
 *
 ******************************************************************************/
size_t
ImageStreamInfo::
getMinInitBufNum() const
{
    return  mImp.getMinInitBufNum();
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT64
ImageStreamInfo::
getUsageForConsumer() const
{
    return 0;
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT64
ImageStreamInfo::
getUsageForAllocator() const
{
    return mUsageForAllocator;
}


/******************************************************************************
 *
 ******************************************************************************/
MINT
ImageStreamInfo::
getImgFormat() const
{
    return mImageBufferInfo.imgFormat;
}


/******************************************************************************
 *
 ******************************************************************************/
MSize
ImageStreamInfo::
getImgSize() const
{
    return mImageBufferInfo.imgSize;
}


/******************************************************************************
 *
 ******************************************************************************/
IImageStreamInfo::BufPlanes_t const&
ImageStreamInfo::
getBufPlanes() const
{
    return mImageBufferInfo.bufPlanes;
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT32
ImageStreamInfo::
getTransform() const
{
    return mTransform;
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
ImageStreamInfo::
setTransform(MUINT32 transform)
{

    mTransform = transform;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT32
ImageStreamInfo::
getDataSpace() const
{
    return mDataSpace;
}


/******************************************************************************
 *
 ******************************************************************************/
MBOOL
ImageStreamInfo::
isSecure() const
{
    return mSecureInfo.type != SecType::mem_normal;
}


/******************************************************************************
 *
 ******************************************************************************/
SecureInfo const&
ImageStreamInfo::
getSecureInfo() const
{
    return mSecureInfo;
}


/******************************************************************************
 *
 ******************************************************************************/
android::String8
ImageStreamInfo::
toString() const
{
    auto toString_BufPlanes = [](const BufPlanes_t& o) {
        android::String8 os;
        os += "[";
        for (size_t i = 0; i < o.size(); i++) {
            auto const& plane = o[i];
            os += String8::format(" %zu/%zu", plane.rowStrideInBytes, plane.sizeInBytes);
        }
        os += " ]";
        return os;
    };

    auto toString_BufOffset = [](const std::vector<size_t>& o) {
        android::String8 os;
        os += "[";
        for (size_t i = 0; i < o.size(); i++) {
            os += String8::format(" %zu", o[i]);
        }
        os += " ]";
        return os;
    };


    android::String8 os;
    os += android::String8::format("%#" PRIx64 "", getStreamId());

    // default/request settings
    os += android::String8::format(" %4dx%-4d", getImgSize().w, getImgSize().h);

    os += android::String8::format(" ImgFormat:%#x(%s)",
        getImgFormat(),
        NSCam::Utils::Format::queryImageFormatName(getImgFormat()).c_str());

    os += " BufPlanes(strides/sizeInBytes):";
    os += toString_BufPlanes(getBufPlanes());

    os += " bufOffset:";
    os += toString_BufOffset(getImageBufferInfo().bufOffset);

    os += android::String8::format(" t:%d", getTransform());

    // alloc settings
    os += android::String8::format(" maxBufNum:%zu minInitBufNum:%zu",
        getMaxBufNum(), getMinInitBufNum());

    os += android::String8::format(" %s", getStreamName());

    os += android::String8::format(" AllocImgFormat:%#x(%s)",
        getAllocImgFormat(),
        NSCam::Utils::Format::queryImageFormatName(getAllocImgFormat()).c_str());

    os += " AllocBufPlanes(strides/sizeInBytes):";
    os += toString_BufPlanes(getAllocBufPlanes());

    return os;
}


/******************************************************************************
 *
 ******************************************************************************/
char const*
ImageStreamInfo::
getPhysicalCameraId() const
{
    return  mPhysicalCameraId.string();
}

/******************************************************************************
 *
 ******************************************************************************/
MINT
ImageStreamInfo::
getAllocImgFormat() const
{
    return mAllocImgFormat;
}


/******************************************************************************
 *
 ******************************************************************************/
IImageStreamInfo::BufPlanes_t const&
ImageStreamInfo::
getAllocBufPlanes() const
{
    return mvAllocBufPlanes;
}


/******************************************************************************
 *
 ******************************************************************************/
ImageBufferInfo const&
ImageStreamInfo::
getImageBufferInfo() const
{
    return mImageBufferInfo;
}
