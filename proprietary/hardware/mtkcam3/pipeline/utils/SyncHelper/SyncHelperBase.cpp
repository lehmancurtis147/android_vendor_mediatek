/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/SyncHelperBase"
//
#include <mtkcam/utils/std/Log.h>
#include <utils/String8.h>
#include <cutils/properties.h>

#include "SyncHelperBase.h"

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

//
#if 1
#define FUNC_START     MY_LOGD("+")
#define FUNC_END       MY_LOGD("-")
#else
#define FUNC_START
#define FUNC_END
#endif

using namespace NSCam::v3::Utils::Imp;
/******************************************************************************
 *
 ******************************************************************************/
Timer::
Timer()
{
    pre_time = std::chrono::system_clock::now();
}
/******************************************************************************
 *
 ******************************************************************************/
Timer::
~Timer()
{
}
/******************************************************************************
 *
 ******************************************************************************/
std::chrono::duration<double>
Timer::
getTimeDiff()
{
    auto cur_Time = std::chrono::system_clock::now();
    std::chrono::duration<double> diff = cur_Time - pre_time;
    pre_time = cur_Time;
    return diff;
}
/******************************************************************************
 *
 ******************************************************************************/
android::sp<ISyncHelperBase> ISyncHelperBase::createInstance() {

    return new SyncHelperBase();
}

/******************************************************************************
 *
 ******************************************************************************/
SyncHelperBase::SyncHelperBase()
    :mUserCounter(0)
{
    {
        Mutex::Autolock lock(mFlushLock);
        mbFlush = false;
    }
    mLogLevel = property_get_int32("vendor.multicam.sync.log", 0);
}

SyncHelperBase::~SyncHelperBase()
{
}


/******************************************************************************
 *
 ******************************************************************************/
status_t SyncHelperBase::start(int CamId) {
    Mutex::Autolock lock(mContextMapLock);
    status_t err = NO_ERROR;
    mUserCounter++;
    mContextMap.add(CamId, shared_ptr<SyncContext>(new SyncContext()));
    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t SyncHelperBase::stop(int CamId __unused) {
    Mutex::Autolock lock(mContextMapLock);
    status_t err = NO_ERROR;
    mUserCounter--;
    if (mUserCounter == 0)
        mContextMap.clear();
    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t SyncHelperBase::init(int CamId __unused) {
    status_t err = NO_ERROR;

    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t SyncHelperBase::uninit(int CamId __unused) {
    status_t err = NO_ERROR;

    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
syncEnqHW(
    SyncParam &sParam
)
{
    status_t err = NO_ERROR;
    std::vector<int> searchCamIdByTargetInSyncQueue;
    if(isFlush())
    {
        MY_LOGI("[r%d:f%d] already flush, skip",
                                        sParam.mRequestNum,
                                        sParam.mFrameNum);
        return err;
    }
    if(!enque_sync_check(sParam, searchCamIdByTargetInSyncQueue))
    {
        MY_LOGD("[r%d:f%d] CamId = %d wait+ q:t(%zu:%zu)\n",
                                sParam.mRequestNum,
                                sParam.mFrameNum,
                                sParam.mCamId,
                                sParam.mSyncCams.size(),
                                searchCamIdByTargetInSyncQueue.size());
        do_sync_wait(sParam.mCamId);
        MY_LOGD("[r%d:f%d] CamId = %d wait-\n",
                                sParam.mRequestNum,
                                sParam.mFrameNum,
                                sParam.mCamId);
    }
    else
    {
        // clear first: to avoid timing issue in heavy loading CPU project
        for(auto& camId : sParam.mSyncCams)
        {
            earse_sync_queue_by_cam_id(camId);
        }
        earse_sync_queue_by_cam_id(sParam.mCamId);
        // waiting target already in sync queue.
        for(auto& camId : searchCamIdByTargetInSyncQueue)
        {
            do_sync_post(camId);
            auto diff = mSyncTimer.getTimeDiff();
            MY_LOGI("[r%d:f%d] all cam ready CamId = %d, postCamId = %d! diff = %lf\n",
                            sParam.mRequestNum,
                            sParam.mFrameNum,
                            sParam.mCamId,
                            camId,
                            diff.count());
        }
    }

    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
syncResultCheck(
    SyncParam &sParam
)
{
    status_t err = NO_ERROR;

    std::vector<int> searchCamIdByTargetInResultQueue;
    if(isFlush())
    {
        MY_LOGI("[r%d:f%d] already flush, skip",
                                        sParam.mRequestNum,
                                        sParam.mFrameNum);
        sParam.mSyncResult = false;
        return err;
    }
    shared_ptr<SyncContext> context1;
    shared_ptr<SyncContext> context2;
    android::String8 strInfo("");

    add_frameno_to_map(sParam);
    process_drop_frame_check(sParam);
    {
        context1 = getSyncContext(sParam.mCamId);
        if(context1->mSyncDrop)
        {
            MY_LOGD_IF(mLogLevel >= 2, "[r%d:f%d] CamId = %d drop.",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId);
            context1->mSyncDrop = false; // reset
            remove_frameno_from_map(sParam, true);
            sParam.mSyncResult = false;
            return err;
        }
    }

    if(!result_sync_check(sParam, searchCamIdByTargetInResultQueue))
    {
        MY_LOGD("[r%d:f%d] CamId = %d wait+ q:t(%zu:%zu)",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId,
                                    sParam.mSyncCams.size(),
                                    searchCamIdByTargetInResultQueue.size());
        do_result_wait(sParam.mCamId);
        // if current wait cam needs to drop, it has to earse itself in sync queue.
        if(context1->mSyncDrop)
        {
            MY_LOGD_IF(mLogLevel >= 2, "[r%d:f%d] CamId = %d drop.",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId);
            context1->mSyncDrop = false; // reset
            remove_frameno_from_map(sParam, true);
            sParam.mSyncResult = false;
            return err;
        }
        MY_LOGD("[r%d:f%d] CamId = %d wait-",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId);
    }
    else
    {
        MY_LOGD_IF(0, "[r%d:f%d] CamId = %d ready +",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId);
        // to avoid timing issue in heavy loading CPU project
        for (auto& syncTargetCamId:sParam.mSyncCams)
        {
            earse_result_queue_by_cam_id(syncTargetCamId);
        }
        earse_result_queue_by_cam_id(sParam.mCamId);
        // before do sem post, compute result first.
        // avoid heavy cpu loading case.
        {
            bool syncResult = true;
            // do result check
            int64_t context1_timestamp = 0;
            int64_t context2_timestamp = 0;
            get_timestamp(sParam.mCamId, sParam.mFrameNum, context1_timestamp);
            for (auto& syncTargetCamId:sParam.mSyncCams)
            {
                context2 = getSyncContext(syncTargetCamId);
                get_timestamp(syncTargetCamId, sParam.mFrameNum, context2_timestamp);
                syncResult &= get_timestamp_check_result(
                                                    context1_timestamp,
                                                    context2_timestamp,
                                                    sParam.mSyncTolerance);
                strInfo += String8::format("CamId = %d, time1=%" PRId64 "ns, ret = %d, synID = %d, time2=%" PRId64 "ns,\
                    tolerance=%" PRId64 "us\n", sParam.mCamId, context1_timestamp, syncResult,
                    syncTargetCamId, context2_timestamp, sParam.mSyncTolerance);
            }
            add_timestamp_check_result(sParam.mCamId, sParam.mFrameNum, sParam.mSyncCams.size(), syncResult);
            // update timer, only print log when all cam ready.
            auto diff = mResultTimer.getTimeDiff();
            strInfo += String8::format("sync check diff = %lf", diff.count());
            MY_LOGI("[r%d:f%d] %s",
                                sParam.mRequestNum,
                                sParam.mFrameNum,
                                strInfo.string());
            // after add check result, remove frame number. (CPU issue)
            remove_frameno_from_map(sParam, true);
        }
        for (auto& syncTargetCamId:sParam.mSyncCams)
        {
            do_result_post(syncTargetCamId);
        }
        MY_LOGD_IF(0, "[r%d:f%d] CamId = %d ready -",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum,
                                    sParam.mCamId);
    }
    if(isFlush())
    {
        MY_LOGI("[r%d:f%d] already flush, no need to check",
                                    sParam.mRequestNum,
                                    sParam.mFrameNum);
        remove_frameno_from_map(sParam, true);
        sParam.mSyncResult = false;
        return err;
    }

    bool syncResult = false;
    get_timestamp_check_result(sParam.mCamId, sParam.mFrameNum, syncResult);
    sParam.mSyncResult = syncResult;
    remove_timestamp_check_result(sParam.mCamId, sParam.mFrameNum);

    return err;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
flush(
    int CamId
)
{
    Mutex::Autolock lock(mFlushLock);
    mbFlush = true;
    MY_LOGI("[%d] flush +", CamId);
    {
        Mutex::Autolock lock2(mSyncQLock);
        MY_LOGD("[%d] sync quesize(%zu)", CamId, mSyncQueue.size());
        for(auto& item:mSyncQueue)
        {
            MY_LOGD("[%d] post sync id(%d)", CamId, item);
            do_sync_post(item);
        }
        mSyncQueue.clear();
    }
    {
        Mutex::Autolock lock2(mResultQLock);
        MY_LOGD("[%d] result quesize(%zu)", CamId, mResultQueue.size());
        for(auto& item:mResultQueue)
        {
            MY_LOGD("[%d] post result id(%d)", CamId, item);
            do_result_post(item);
        }
        mResultQueue.clear();
    }
    MY_LOGI("[%d] flush -", CamId);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
add_frameno_to_map(
    SyncParam &sParam
)
{
    Mutex::Autolock lock(mCamMapLock);
    auto index = mCamMapByFrameNo.indexOfKey(sParam.mFrameNum);
    if(index>=0)
    {
        mCamMapByFrameNo.editValueAt(index)++;
    }
    else
    {
        mCamMapByFrameNo.add(sParam.mFrameNum, 1);
    }
    MY_LOGD("camid(%d) frameno(%d) size(%d)",
                                sParam.mCamId,
                                sParam.mFrameNum,
                                mCamMapByFrameNo.valueAt(index));
    add_timestamp(sParam.mCamId, sParam.mFrameNum, sParam.mReslutTimeStamp);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
remove_frameno_from_map(
    SyncParam &sParam,
    bool forceRemove
)
{
    Mutex::Autolock lock(mCamMapLock);
    auto index = mCamMapByFrameNo.indexOfKey(sParam.mFrameNum);
    int size = -1;
    if(index>=0)
    {
        mCamMapByFrameNo.editValueAt(index)--;
        if(mCamMapByFrameNo.valueAt(index) == 0 || forceRemove)
        {
            for(auto& camId:sParam.mSyncCams)
            {
                remove_timestamp(camId, sParam.mFrameNum);
            }
            remove_timestamp(sParam.mCamId, sParam.mFrameNum);
        }
        size = mCamMapByFrameNo.valueAt(index);
    }
    MY_LOGD("camid(%d) frameno(%d) size(%d) force(%d)",
                                sParam.mCamId,
                                sParam.mFrameNum,
                                size,
                                forceRemove);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
enque_sync_check(
    SyncParam &sParam,
    std::vector<int> &searchSyncResult
)
{
    Mutex::Autolock lock(mSyncQLock);
    for (auto& syncTargetCamId : sParam.mSyncCams)
    {
        auto findTargetCamInSyncQueue = std::find(mSyncQueue.begin(), mSyncQueue.end(), syncTargetCamId);
        if(findTargetCamInSyncQueue != mSyncQueue.end())
        {
            searchSyncResult.push_back(*findTargetCamInSyncQueue);
        }
    }
    if(searchSyncResult.size() != sParam.mSyncCams.size())
    {
        // this cam needs to other cam ready.
        mSyncQueue.push_back(sParam.mCamId);
        return false;
    }
    else
    {
        return true;
    }
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
earse_sync_queue_by_cam_id(
    int CamId
)
{
    Mutex::Autolock lock(mSyncQLock);
    mSyncQueue.erase(std::remove(mSyncQueue.begin(), mSyncQueue.end(), CamId), mSyncQueue.end());
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
do_sync_wait(
    int CamId
)
{
    mContextMapLock.lock();
    auto context = mContextMap.valueFor(CamId);
    mContextMapLock.unlock();
    if(context)
    {
        sem_wait(&(context->mSyncSem));
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
do_sync_post(
    int CamId
)
{
    mContextMapLock.lock();
    auto context = mContextMap.valueFor(CamId);
    mContextMapLock.unlock();
    if(context)
    {
        sem_post(&(context->mSyncSem));
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
result_sync_check(
    SyncParam &sParam,
    std::vector<int> &searchSyncResult
)
{
    Mutex::Autolock lock(mResultQLock);
    for (auto& syncTargetCamId : sParam.mSyncCams)
    {
        auto findTargetCamInResultQueue = std::find(mResultQueue.begin(), mResultQueue.end(), syncTargetCamId);
        if(findTargetCamInResultQueue != mResultQueue.end())
        {
            searchSyncResult.push_back(*findTargetCamInResultQueue);
        }
    }
    if(searchSyncResult.size() != sParam.mSyncCams.size())
    {
        mResultQueue.push_back(sParam.mCamId);
        return false;
    }
    else
    {
        return true;
    }
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
earse_result_queue_by_cam_id(
    int CamId
)
{
    Mutex::Autolock lock(mResultQLock);
    mResultQueue.erase(std::remove(mResultQueue.begin(), mResultQueue.end(), CamId), mResultQueue.end());
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
do_result_wait(
    int CamId
)
{
    mContextMapLock.lock();
    auto context = mContextMap.valueFor(CamId);
    mContextMapLock.unlock();
    if(context)
    {
        MY_LOGD_IF(mLogLevel >= 2, "camid: %d", CamId);
        sem_wait(&(context->mResultSem));
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
SyncHelperBase::
do_result_post(
    int CamId
)
{
    mContextMapLock.lock();
    auto context = mContextMap.valueFor(CamId);
    mContextMapLock.unlock();
    if(context)
    {
        MY_LOGD_IF(mLogLevel >= 2, "camid: %d", CamId);
        sem_post(&(context->mResultSem));
    }
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
get_timestamp_check_result(
    int64_t time1,
    int64_t time2,
    int64_t tolerance
)
{
    int timeStamp_ms_main1 = time1/1000000;
    int timeStamp_ms_main2 = time2/1000000;
    int tolerance_ms = tolerance/1000;
    int64_t diff = abs(timeStamp_ms_main1 - timeStamp_ms_main2);
    if (diff<=tolerance_ms)
        return true;
    else
        return false;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
add_timestamp_check_result(
    int CamId,
    int frameNo,
    std::size_t initRefCount,
    bool result
)
{
    Mutex::Autolock lock(mCamSyncResultLock);
    MY_LOGD_IF(mLogLevel >= 2, "[%d] frameNo: %d result: %d initRefCount: %zu",
                                    CamId,
                                    frameNo,
                                    result,
                                    initRefCount);
    mCamSyncResultByFrameNo.add(frameNo, {.ref_count = initRefCount, .result = result});
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
get_timestamp_check_result(
    int CamId,
    int frameNo,
    bool &result
)
{
    Mutex::Autolock lock(mCamSyncResultLock);
    result = mCamSyncResultByFrameNo.valueFor(frameNo).result;
    MY_LOGD_IF(mLogLevel >= 2, "[%d] frameNo: %d result: %d", CamId, frameNo, result);
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
remove_timestamp_check_result(
    int CamId,
    int frameNo
)
{
    Mutex::Autolock lock(mCamSyncResultLock);
    ssize_t index = mCamSyncResultByFrameNo.indexOfKey(frameNo);
    if(index >= 0)
    {
        if((mCamSyncResultByFrameNo.editValueFor(frameNo).ref_count--) == 0)
        {
            MY_LOGD_IF(mLogLevel >= 2, "[%d] frameNo: %d", CamId, frameNo);
            mCamSyncResultByFrameNo.removeItem(frameNo);
        }
    }
    return true;
}

/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
process_drop_frame_check(
    SyncParam &sParam
)
{
    Mutex::Autolock lock(mCamMapLock);
    shared_ptr<SyncContext> pContext = nullptr;
    for(auto& camId:sParam.mSyncCams)
    {
        pContext = getSyncContext(camId);
        for(size_t i=0;i<pContext->mTimeStampList.size();++i)
        {
            if(sParam.mFrameNum > pContext->mTimeStampList.keyAt(i))
            {
                MY_LOGD_IF(1, "[%d] great Find request and need to kick.(CamId:%d kick num:%d cur num: %d)",
                                        sParam.mCamId, camId, pContext->mTimeStampList.keyAt(i), sParam.mFrameNum);
                // post sync target
                pContext->mSyncDrop = true;
                earse_result_queue_by_cam_id(camId);
                do_result_post(camId);
            }
            if(sParam.mFrameNum < pContext->mTimeStampList.keyAt(i))
            {
                MY_LOGD_IF(1, "[%d] less Find, drop self.(CamId:%d kick num:%d cur num: %d)",
                                        sParam.mCamId, camId, sParam.mFrameNum, pContext->mTimeStampList.keyAt(i));
                // post sync target
                shared_ptr<SyncContext> pContext_cur = getSyncContext(sParam.mCamId);
                pContext_cur->mSyncDrop = true;
            }
            if(sParam.mFrameNum == pContext->mTimeStampList.keyAt(i))
            {
                MY_LOGD_IF(mLogLevel >= 2, "[%d] Find .(cur num: %d)", sParam.mCamId, sParam.mFrameNum);
            }
        }
    }
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
add_timestamp(
    int CamId,
    int frameNo,
    int64_t &timestamp
)
{
    shared_ptr<SyncContext> pContext = getSyncContext(CamId);
    MY_LOGD_IF(mLogLevel >= 2, "camid: %d frame no: %d timestamp: %" PRId64 , CamId, frameNo, timestamp);
    pContext->mTimeStampList.add(frameNo, timestamp);
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
remove_timestamp(
    int CamId,
    int frameNo
)
{
    MY_LOGD_IF(mLogLevel >= 2, "camid: %d frame no: %d" , CamId, frameNo);
    shared_ptr<SyncContext> pContext = getSyncContext(CamId);
    pContext->mTimeStampList.removeItem(frameNo);
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
get_timestamp(
    int CamId,
    int frameNo,
    int64_t &timestamp
)
{
    shared_ptr<SyncContext> pContext = getSyncContext(CamId);
    timestamp = pContext->mTimeStampList.valueFor(frameNo);
    return true;
}
/******************************************************************************
 *
 ******************************************************************************/
shared_ptr<SyncContext>
SyncHelperBase::
getSyncContext(
    int CamId
)
{
    Mutex::Autolock lock(mContextMapLock);
    return mContextMap.valueFor(CamId);
}
/******************************************************************************
 *
 ******************************************************************************/
bool
SyncHelperBase::
isFlush()
{
    Mutex::Autolock lock(mFlushLock);
    return mbFlush;
}