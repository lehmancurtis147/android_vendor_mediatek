/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include <string.h>
#include "P2_MWData.h"
#include "P2_DebugControl.h"

namespace P2
{

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    MWInfo
#define P2_TRACE        TRACE_MW_INFO
#include "P2_LogHeader.h"

MWInfo::MWInfo(const NSCam::v3::P2StreamingNode::ConfigParams &param)
{
    TRACE_FUNC_ENTER();
    initMetaInfo(IN_APP, param.pInAppMeta);
    initMetaInfo(IN_APP_PHY, param.pInAppPhysicalMeta);
    initMetaInfo(IN_P1_APP, param.pInAppRetMeta);
    initMetaInfo(IN_P1_HAL, param.pInHalMeta);
    initMetaInfo(IN_APP_PHY_2, param.pInAppPhysicalMeta_Sub);
    initMetaInfo(IN_P1_APP_2, param.pInAppRetMeta_Sub);
    initMetaInfo(IN_P1_HAL_2, param.pInHalMeta_Sub);
    initMetaInfo(OUT_APP, param.pOutAppMeta);
    initMetaInfo(OUT_HAL, param.pOutHalMeta);
    initMetaInfo(OUT_APP_PHY, param.pOutAppPhysicalMeta);
    initMetaInfo(OUT_APP_PHY_2, param.pOutAppPhysicalMeta_Sub);
    initImgInfo(IN_OPAQUE, param.pvInOpaque);
    initImgInfo(IN_FULL, param.pvInFullRaw);
    initImgInfo(IN_RESIZED, param.pInResizedRaw);
    initImgInfo(IN_LCSO, param.pInLcsoRaw);
    initImgInfo(IN_RSSO, param.pInRssoRaw);
    initImgInfo(IN_OPAQUE_2, param.pvInOpaque_Sub);
    initImgInfo(IN_FULL_2, param.pvInFullRaw_Sub);
    initImgInfo(IN_RESIZED_2, param.pInResizedRaw_Sub);
    initImgInfo(IN_LCSO_2, param.pInLcsoRaw_Sub);
    initImgInfo(IN_RSSO_2, param.pInRssoRaw_Sub);
    initImgInfo(IN_REPROCESS, param.pInYuvImage);
    initImgInfo(OUT_YUV, param.vOutImage);
    initImgInfo(OUT_FD, param.pOutFDImage);
    mBurstNum = param.burstNum;
    mCustomOption = param.customOption;
    TRACE_FUNC_EXIT();
}

MWInfo::MWInfo(const NSCam::v3::P2CaptureNode::ConfigParams &param)
{
    TRACE_FUNC_ENTER();
    initMetaInfo(IN_APP, param.pInAppMeta);
    initMetaInfo(IN_P1_APP, param.pInAppRetMeta);
    initMetaInfo(IN_P1_HAL, param.pInHalMeta);
    initMetaInfo(IN_P1_APP_2, param.pInAppRetMeta2);
    initMetaInfo(IN_P1_HAL_2, param.pInHalMeta2);
    initMetaInfo(OUT_APP, param.pOutAppMeta);
    initMetaInfo(OUT_HAL, param.pOutHalMeta);
    initImgInfo(IN_OPAQUE, param.vpInOpaqueRaws);
    initImgInfo(IN_FULL, param.pInFullRaw);
    initImgInfo(IN_RESIZED, param.pInResizedRaw);
    initImgInfo(IN_LCSO, param.pInLcsoRaw);
    initImgInfo(IN_FULL_2, param.pInFullRaw2);
    initImgInfo(IN_RESIZED_2, param.pInResizedRaw2);
    initImgInfo(IN_LCSO_2, param.pInLcsoRaw2);
    initImgInfo(IN_REPROCESS, param.pInFullYuv);
    initImgInfo(OUT_YUV, param.vpOutImages);
    initImgInfo(OUT_JPEG_YUV, param.pOutJpegYuv);
    initImgInfo(OUT_THN_YUV, param.pOutThumbnailYuv);
    initImgInfo(OUT_POSTVIEW, param.pOutPostViewYuv);
    mCustomOption = param.uCustomOption;
    TRACE_FUNC_EXIT();
}
MWInfo::~MWInfo()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL MWInfo::isValid(const ILog &log) const
{
    TRACE_S_FUNC_ENTER(log);
    MBOOL ret = MTRUE;
    MBOOL hasFull = hasImg(IN_FULL);
    MBOOL hasResized = hasImg(IN_RESIZED);
    MBOOL hasFD = hasImg(OUT_FD);
    MBOOL hasYUV = hasImg(OUT_YUV) || hasImg(OUT_JPEG_YUV);
    MBOOL hasInApp = hasMeta(IN_APP);
    MBOOL hasInHal = hasMeta(IN_P1_HAL);
    if( !(hasFull || hasResized) ||
        !(hasFD || hasYUV) ||
        !hasInApp || !hasInHal )
    {
        MY_S_LOGW(log, "missing necessary stream: full(%d) resized(%d) fd(%d) yuv(%d) inApp(%d) inHal(%d)", hasFull, hasResized, hasFD, hasYUV, hasInApp, hasInHal);
        ret = MFALSE;
    }
    if(!ret || log.getLogLevel() || true)
    {
        this->print(log);
    }
    TRACE_S_FUNC_EXIT(log);
    return ret;
}

sp<IMetaStreamInfo> MWInfo::findMetaInfo(ID_META id) const
{
    TRACE_FUNC_ENTER();
    sp<IMetaStreamInfo> info;
    auto it = mMetaInfoMap.find(id);
    if( it != mMetaInfoMap.end() && it->second.size() )
    {
        info = it->second[0];
    }
    TRACE_FUNC_EXIT();
    return info;
}

sp<IImageStreamInfo> MWInfo::findImgInfo(ID_IMG id) const
{
    TRACE_FUNC_ENTER();
    sp<IImageStreamInfo> info;
    auto it = mImgInfoMap.find(id);
    if( it != mImgInfoMap.end() && it->second.size() )
    {
        info = it->second[0];
    }
    TRACE_FUNC_EXIT();
    return info;
}

ID_META MWInfo::toMetaID(StreamId_T sID) const
{
    ID_META id = ID_META_INVALID;
    auto it = mMetaIDMap.find(sID);
    if( it != mMetaIDMap.end() )
    {
        id = it->second;
    }
    return id;
}

ID_IMG MWInfo::toImgID(StreamId_T sID) const
{
    ID_IMG id = ID_IMG_INVALID;
    auto it = mImgIDMap.find(sID);
    if( it != mImgIDMap.end() )
    {
        id = it->second;
    }
    return id;
}

IMG_TYPE MWInfo::getImgType(StreamId_T sID) const
{
    IMG_TYPE type = IMG_TYPE_EXTRA;
    auto it = mSIDTypeMap.find(sID);
    if( it != mSIDTypeMap.end() )
    {
        type = it->second;
    }
    return type;
}

MBOOL MWInfo::isCaptureIn(StreamId_T sID) const
{
    MBOOL ret = MFALSE;
    auto it = mImgIDMap.find(sID);
    if( it != mImgIDMap.end() )
    {
        ret = (it->second == IN_OPAQUE) ||
              (it->second == IN_FULL) ||
              (it->second == IN_REPROCESS);
    }
    return ret;
}

MUINT32 MWInfo::getBurstNum() const
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return mBurstNum;
}

MUINT32 MWInfo::getCustomOption() const
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return mCustomOption;
}

MBOOL MWInfo::supportClearZoom() const
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return mCustomOption & NSCam::v3::P2Common::CUSTOM_OPTION_CLEAR_ZOOM_SUPPORT;
}

MBOOL MWInfo::supportDRE() const
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return mCustomOption & NSCam::v3::P2Common::CUSTOM_OPTION_DRE_SUPPORT;
}

MVOID MWInfo::print(const ILog &log) const
{
    TRACE_S_FUNC_ENTER(log);
    for( auto &info : P2Img::InfoMap )
    {
        std::vector<sp<IImageStreamInfo>> mwInfo = findStreamInfo(info.first);
        for( unsigned i = 0, size = mwInfo.size(); i < size; ++i )
        {
            sp<IImageStreamInfo> img = mwInfo[i];
            const char *name = "NA";
            StreamId_T id = 0;
            MUINT64 allocator = 0, consumer = 0;
            MINT imgFmt = 0;
            MSize imgSize;
            IMG_TYPE type = IMG_TYPE_EXTRA;
            const char *physicalId = "NA";
            if( img != NULL )
            {
                //name = img->getStreamName();
                id = img->getStreamId();
                allocator = img->getUsageForAllocator();
                consumer = img->getUsageForConsumer();
                imgFmt = img->getImgFormat();
                imgSize = img->getImgSize();
                if(img->getPhysicalCameraId() != NULL)
                {
                    physicalId = img->getPhysicalCameraId();
                }
                if(mSIDTypeMap.count(id) != 0)
                {
                    type = mSIDTypeMap.at(id);
                }
            }
            MY_S_LOGD(log, "StreamInfo: [img:0x%09" PRIx64 "] (A/C:0x%09" PRIx64 "/0x%09" PRIx64 ") %s[%d]/%s (%dx%d) (fmt:0x%08x) type(%s) phy(%s)",
                    id, allocator, consumer, info.second.name.c_str(), i, name, imgSize.w, imgSize.h, imgFmt, ImgType2Name(type), physicalId);
        }
    }
    for( auto &info : P2Meta::InfoMap )
    {
        std::vector<sp<IMetaStreamInfo>> mwInfo = findStreamInfo(info.first);
        for( unsigned i = 0, size = mwInfo.size(); i < size; ++i )
        {
            sp<IMetaStreamInfo> meta = mwInfo[i];
            const char *name = "NA";
            StreamId_T id = 0;
            if( meta != NULL )
            {
                name = meta->getStreamName();
                id = meta->getStreamId();
            }
            MY_S_LOGD(log, "StreamInfo: [meta:0x%09" PRIx64 "] %s[%d]/%s", id, info.second.name.c_str(), i, name);
        }
    }
    TRACE_S_FUNC_EXIT(log);
}

MVOID MWInfo::initMetaInfo(ID_META id, const sp<IMetaStreamInfo> &info)
{
    TRACE_FUNC_ENTER();
    if( info != NULL )
    {
        mMetaInfoMap[id].push_back(info);
        mMetaIDMap[info->getStreamId()] = id;
    }
    TRACE_FUNC_EXIT();
}

MVOID MWInfo::initMetaInfo(ID_META id, const Vector<sp<IMetaStreamInfo>> &infos)
{
    TRACE_FUNC_ENTER();
    for( auto &&info : infos )
    {
        if( info != NULL )
        {
            mMetaInfoMap[id].push_back(info);
            mMetaIDMap[info->getStreamId()] = id;
        }
    }
    TRACE_FUNC_EXIT();
}

MVOID MWInfo::initImgInfo(ID_IMG id, const sp<IImageStreamInfo> &info)
{
    TRACE_FUNC_ENTER();
    if( info != NULL )
    {
        mImgInfoMap[id].push_back(info);
        mImgIDMap[info->getStreamId()] = id;
        if(id == OUT_FD)
        {
            mSIDTypeMap[info->getStreamId()] = IMG_TYPE_FD;
        }
    }
    TRACE_FUNC_EXIT();
}

MVOID MWInfo::initImgInfo(ID_IMG id, const Vector<sp<IImageStreamInfo>> &infos)
{
    TRACE_FUNC_ENTER();
    std::list<StreamId_T> hwCompIds;
    std::list<StreamId_T> hwTextureIds;
    std::list<StreamId_T> hwEncodeIds;
    for( auto &&info : infos )
    {
        if( info != NULL )
        {
            MUINT32 usage = info->getUsageForAllocator();
            StreamId_T sId = info->getStreamId();

            mImgInfoMap[id].push_back(info);
            mImgIDMap[info->getStreamId()] = id;
            if(strlen(info->getPhysicalCameraId()) > 0)
            {
                mSIDTypeMap[sId] = IMG_TYPE_PHYSICAL;
            }
            if(usage & GRALLOC_USAGE_HW_COMPOSER) hwCompIds.push_back(sId);
            if(usage & GRALLOC_USAGE_HW_TEXTURE) hwTextureIds.push_back(sId);
            if(usage & GRALLOC_USAGE_HW_VIDEO_ENCODER) hwEncodeIds.push_back(sId);
        }
    }

    auto getUnOccupiedID = [&]
        (
            std::list<StreamId_T> &idList,
            StreamId_T &out
        )
        -> bool
        {
            for(auto && id : idList)
            {
                if(mSIDTypeMap.count(id) == 0)
                {
                    out = id;
                    return true;
                }
            }
            return false;
        };

    // Record -> Display -> Unknown
    StreamId_T sID = (StreamId_T)(-1);
    if(getUnOccupiedID(hwEncodeIds, sID))
    {
        mSIDTypeMap[sID] = IMG_TYPE_RECORD;
    }

    if(getUnOccupiedID(hwCompIds, sID))
    {
        mSIDTypeMap[sID] = IMG_TYPE_DISPLAY;
    }
    else if(getUnOccupiedID(hwTextureIds, sID))
    {
        mSIDTypeMap[sID] = IMG_TYPE_DISPLAY;
    }

    TRACE_FUNC_EXIT();
}

std::vector<sp<IMetaStreamInfo>> MWInfo::findStreamInfo(ID_META id) const
{
    auto it = mMetaInfoMap.find(id);
    return (it != mMetaInfoMap.end()) ?
           it->second : std::vector<sp<IMetaStreamInfo>>();
}

std::vector<sp<IImageStreamInfo>> MWInfo::findStreamInfo(ID_IMG id) const
{
    auto it = mImgInfoMap.find(id);
    return (it != mImgInfoMap.end()) ?
           it->second : std::vector<sp<IImageStreamInfo>>();
}

MBOOL MWInfo::hasMeta(ID_META id) const
{
    auto &&it = mMetaInfoMap.find(id);
    return it != mMetaInfoMap.end() && it->second.size();
}

MBOOL MWInfo::hasImg(ID_IMG id) const
{
    auto &&it = mImgInfoMap.find(id);
    return it != mImgInfoMap.end() && it->second.size();
}

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    MWMeta
#define P2_TRACE        TRACE_MW_META
#include "P2_LogHeader.h"

MWMeta::MWMeta(const ILog &log, const P2Pack &p2Pack, const sp<MWFrame> &frame, const StreamId_T &streamID, IO_DIR dir, const META_INFO &info)
    : P2Meta(log, p2Pack, info.id)
    , mMWFrame(frame)
    , mReleaseToken(frame != NULL ? frame->getReleaseToken() : NULL)
    , mStreamID(streamID)
    , mDir(dir)
    , mStatus(IO_STATUS_INVALID)
    , mMetadata(NULL)
    , mLockedMetadata(NULL)
    , mMetadataCopy(NULL)
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "acquireMeta");
    mStreamBuffer = mMWFrame->acquireMetaStream(mStreamID);
    if( mStreamBuffer != NULL )
    {
        mLockedMetadata = mMWFrame->acquireMeta(mStreamBuffer, mDir);
    }
    P2_CAM_TRACE_END(TRACE_ADVANCED);
    if( mLockedMetadata != NULL)
    {
        if( dir == IO_DIR_IN &&
            (info.flag & IO_FLAG_COPY) )
        {
            if( dir == IO_DIR_IN &&
                (info.flag & IO_FLAG_COPY) )
            {
                mMetadataCopy = new IMetadata();
                *mMetadataCopy = *mLockedMetadata;
                mMetadata = mMetadataCopy;
            }
            else
            {
                mMetadata = mLockedMetadata;
            }
            mStatus = IO_STATUS_READY;
            TRACE_S_FUNC(mLog, "meta=%p count= %d", mMetadata, mMetadata->count());
        }
        else
        {
            mMetadata = mLockedMetadata;
        }
        mStatus = IO_STATUS_READY;
        TRACE_S_FUNC(mLog, "meta=%p count= %d", mMetadata, mMetadata->count());
    }
    TRACE_S_FUNC_EXIT(mLog);
}

MWMeta::~MWMeta()
{
    TRACE_S_FUNC_ENTER(mLog, "name(%s), count(%d)",
                       (mStreamBuffer != NULL) ? mStreamBuffer->getName() : "??",
                       (mLockedMetadata != NULL) ? mLockedMetadata->count() : -1);
    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "~MWMeta->releaseMeta");
    MWFrame::releaseMeta(mReleaseToken, mStreamBuffer, mLockedMetadata);
    mMetadata = mLockedMetadata = NULL;
    MWFrame::releaseMetaStream(mReleaseToken, mStreamBuffer, mDir, mStatus);
    mStreamBuffer = NULL;
    P2_CAM_TRACE_END(TRACE_ADVANCED);
    if( mMWFrame != NULL )
    {
        mMWFrame->notifyRelease();
        mMWFrame = NULL;
    }

    if(mMetadataCopy != NULL)
    {
        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "~MWMetag->freeCopyMeta");
        delete mMetadataCopy;
        mMetadataCopy = NULL;
        P2_CAM_TRACE_END(TRACE_ADVANCED);
    }
    TRACE_S_FUNC_EXIT(mLog);
}

StreamId_T MWMeta::getStreamID() const
{
    return mStreamID;
}

MBOOL MWMeta::isValid() const
{
    return (mMetadata != NULL);
}

IO_DIR MWMeta::getDir() const
{
    return mDir;
}

MVOID MWMeta::updateResult(MBOOL result)
{
    TRACE_S_FUNC_ENTER(mLog);
    if( (mDir & IO_DIR_OUT) &&
        mStatus != IO_STATUS_INVALID )
    {
        mStatus = result ? IO_STATUS_OK : IO_STATUS_ERROR;
    }
    TRACE_S_FUNC_EXIT(mLog);
}


IMetadata* MWMeta::getIMetadataPtr() const
{
    return mMetadata;
}

IMetadata::IEntry MWMeta::getEntry(MUINT32 tag) const
{
    IMetadata::IEntry entry;
    if( mMetadata )
    {
        entry = mMetadata->entryFor(tag);
    }
    return entry;
}

MBOOL MWMeta::setEntry(MUINT32 tag, const IMetadata::IEntry &entry)
{
    MBOOL ret = MFALSE;
    if( mMetadata )
    {
        ret = (mMetadata->update(tag, entry) == OK);
    }
    return ret;
}

MVOID MWMeta::detach()
{
    TRACE_S_FUNC_ENTER(mLog, "name(%s)", (mStreamBuffer != NULL) ? mStreamBuffer->getName() : "??");
    if( mMWFrame != NULL )
    {
        mMWFrame->preReleaseMetaStream(mStreamBuffer, mDir);
        mMWFrame->notifyRelease();
        mMWFrame = NULL;
    }
    TRACE_S_FUNC_EXIT(mLog);
}

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    MWImg
#define P2_TRACE        TRACE_MW_IMG
#include "P2_LogHeader.h"

MWImg::MWImg(const ILog &log, const P2Pack &p2Pack, const sp<MWFrame> &frame, const StreamId_T &streamID, IO_DIR dir, IMG_TYPE type, const IMG_INFO &info, MUINT32 debugIndex, MBOOL needSWRW)
    : P2Img(log, p2Pack, info.id, debugIndex)
    , mMWFrame(frame)
    , mReleaseToken(frame != NULL ? frame->getReleaseToken() : NULL)
    , mStreamID(streamID)
    , mDir(dir)
    , mStatus(IO_STATUS_INVALID)
    , mTransform(0)
    , mUsage(0)
    , mType(type)
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "acquireImage");
    mStreamBuffer = mMWFrame->acquireImageStream(mStreamID);
    if( mStreamBuffer != NULL )
    {
        MBOOL isOpaque = (info.id == IN_OPAQUE);
        mImageBuffers = mMWFrame->acquireImage(mStreamBuffer, dir, needSWRW, isOpaque);
    }
    P2_CAM_TRACE_END(TRACE_ADVANCED);
    if( mImageBuffers.size() )
    {
        size_t size = mImageBuffers.size();
        mImageBufferPtrs.resize(size, NULL);
        for( size_t i = 0; i < size; ++i )
        {
            mImageBufferPtrs[i] = mImageBuffers[i].get();
        }
        mFirstImageBuffer = mImageBufferPtrs[0];
        if( mFirstImageBuffer )
        {
            mTransform = mStreamBuffer->getStreamInfo()->getTransform();
            mUsage = mStreamBuffer->getStreamInfo()->getUsageForAllocator();
            mStatus = IO_STATUS_READY;
        }
    }
    TRACE_S_FUNC_EXIT(mLog);
}

MWImg::~MWImg()
{
    TRACE_S_FUNC_ENTER(mLog);
    processPlugin();
    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "~MWImg->releaseImage");
    MWFrame::releaseImage(mReleaseToken, mStreamBuffer, mImageBuffers);
    mFirstImageBuffer = NULL;
    mImageBufferPtrs.clear();
    mImageBuffers.clear();
    MWFrame::releaseImageStream(mReleaseToken, mStreamBuffer, mDir, mStatus);
    mStreamBuffer = NULL;
    P2_CAM_TRACE_END(TRACE_ADVANCED);
    if( mMWFrame != NULL )
    {
        mMWFrame->notifyRelease();
        mMWFrame = NULL;
    }
    TRACE_S_FUNC_EXIT(mLog);
}

StreamId_T MWImg::getStreamID() const
{
    return mStreamID;
}

MBOOL MWImg::isValid() const
{
    return (mFirstImageBuffer != NULL);
}

IO_DIR MWImg::getDir() const
{
    return mDir;
}

MVOID MWImg::registerPlugin(const std::list<sp<P2ImgPlugin>> &plugin)
{
    TRACE_S_FUNC_ENTER(mLog);
    mPlugin = plugin;
    TRACE_S_FUNC_EXIT(mLog);
}

MVOID MWImg::updateResult(MBOOL result)
{
    TRACE_S_FUNC_ENTER(mLog);
    if( (mDir & IO_DIR_OUT) &&
        mStatus != IO_STATUS_INVALID )
    {
        mStatus = result ? IO_STATUS_OK : IO_STATUS_ERROR;
    }
    TRACE_S_FUNC_EXIT(mLog);
}

IImageBuffer* MWImg::getIImageBufferPtr() const
{
    return mFirstImageBuffer;
}

std::vector<IImageBuffer*> MWImg::getIImageBufferPtrs() const
{
    return mImageBufferPtrs;
}

MUINT32 MWImg::getIImageBufferPtrCount() const
{
    return mImageBufferPtrs.size();
}

MUINT32 MWImg::getTransform() const
{
    return mTransform;
}

MUINT32 MWImg::getUsage() const
{
    return mUsage;
}

MBOOL MWImg::isDisplay() const
{
    //return (mUsage & (GRALLOC_USAGE_HW_COMPOSER|GRALLOC_USAGE_HW_TEXTURE));
    return mType == IMG_TYPE_DISPLAY;
}

MBOOL MWImg::isRecord() const
{
    //return mUsage & GRALLOC_USAGE_HW_VIDEO_ENCODER;
    return mType == IMG_TYPE_RECORD;
}

MBOOL MWImg::isPhysicalStream() const
{
    return mType == IMG_TYPE_PHYSICAL;
}

MVOID MWImg::detach()
{
    TRACE_S_FUNC_ENTER(mLog, "name(%s)", (mStreamBuffer != NULL) ? mStreamBuffer->getName() : "??");
    if( mMWFrame != NULL )
    {
        mMWFrame->preReleaseImageStream(mStreamBuffer, mDir);
        mMWFrame->notifyRelease();
        mMWFrame = NULL;
    }
    TRACE_S_FUNC_EXIT(mLog);
}

IMG_TYPE MWImg::getImgType() const
{
    return mType;
}

MBOOL MWImg::isCapture() const
{
    return !(mUsage & (GRALLOC_USAGE_HW_COMPOSER | GRALLOC_USAGE_HW_VIDEO_ENCODER));
}

MVOID MWImg::processPlugin() const
{
    TRACE_S_FUNC_ENTER(mLog);
    if( mStatus != IO_STATUS_ERROR && mStatus != IO_STATUS_INVALID)
    {
        for( auto plugin : mPlugin )
        {
            plugin->onPlugin(this);
        }
    }
    TRACE_S_FUNC_EXIT(mLog);
}

} // namespace P2
