/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "P2_Param.h"

#define DUMP_PATH "/data/vendor/p2_dump"

using NSCam::TuningUtils::FILE_DUMP_NAMING_HINT;
using NSCam::TuningUtils::RAW_PORT_IMGO;
using NSCam::TuningUtils::RAW_PORT_RRZO;
using NSCam::TuningUtils::YUV_PORT_WDMAO;
using NSCam::TuningUtils::YUV_PORT_WROTO;
using NSCam::TuningUtils::YUV_PORT_UNDEFINED;

namespace P2
{

#define META_INFO_1(id, dir, name, flag) \
    { id, META_INFO(id, id, dir, name, flag) }
#define META_INFO_2(id, dir, name, flag) \
    { id##_2, META_INFO(id##_2, id, dir, name "_2", flag) }

#define IMG_INFO_1(id, dir, name, flag) \
    { id, IMG_INFO(id, id, dir, name, flag) }
#define IMG_INFO_2(id, dir, name, flag) \
    { id##_2, IMG_INFO(id##_2, id, dir, name "_2", flag) }

META_INFO::META_INFO()
{
}

META_INFO::META_INFO(ID_META sID, ID_META sMirror, IO_DIR sDir, const std::string &sName, MUINT32 sFlag)
    : id(sID)
    , mirror(sMirror)
    , dir(sDir)
    , name(sName)
    , flag(sFlag)
{
}

IMG_INFO::IMG_INFO()
{
}

IMG_INFO::IMG_INFO(ID_IMG sID, ID_IMG sMirror, IO_DIR sDir, const std::string &sName, MUINT32 sFlag)
    : id(sID)
    , mirror(sMirror)
    , dir(sDir)
    , name(sName)
    , flag(sFlag)
{
}

const std::unordered_map<ID_META, META_INFO> P2Meta::InfoMap =
{
    META_INFO_1(IN_APP,     IO_DIR_IN,  "inApp",    IO_FLAG_DEFAULT ),
    META_INFO_1(IN_APP_PHY, IO_DIR_IN,  "inAppPhy", IO_FLAG_DEFAULT ),
    META_INFO_1(IN_P1_APP,  IO_DIR_IN,  "inP1App",  IO_FLAG_COPY    ),
    META_INFO_1(IN_P1_HAL,  IO_DIR_IN,  "inP1Hal",  IO_FLAG_COPY    ),
    META_INFO_1(OUT_APP,    IO_DIR_OUT, "outApp",   IO_FLAG_DEFAULT ),
    META_INFO_1(OUT_HAL,    IO_DIR_OUT, "outHal",   IO_FLAG_DEFAULT ),
    META_INFO_1(OUT_APP_PHY,IO_DIR_OUT, "outAppPhy",IO_FLAG_DEFAULT ),

    META_INFO_2(IN_APP_PHY, IO_DIR_IN,  "inAppPhy", IO_FLAG_DEFAULT ),
    META_INFO_2(IN_P1_APP,  IO_DIR_IN,  "inP1App",  IO_FLAG_COPY    ),
    META_INFO_2(IN_P1_HAL,  IO_DIR_IN,  "inP1Hal",  IO_FLAG_COPY    ),
    META_INFO_2(OUT_APP_PHY,IO_DIR_OUT, "outAppPhy",IO_FLAG_DEFAULT ),
};

const std::unordered_map<ID_IMG, IMG_INFO> P2Img::InfoMap =
{
    IMG_INFO_1(IN_REPROCESS, IO_DIR_IN,  "inReprocess",  IO_FLAG_DEFAULT),
    IMG_INFO_1(IN_OPAQUE,    IO_DIR_IN,  "inOpaque",     IO_FLAG_DEFAULT),
    IMG_INFO_1(IN_FULL,      IO_DIR_IN,  "inFull",       IO_FLAG_DEFAULT),
    IMG_INFO_1(IN_RESIZED,   IO_DIR_IN,  "inResized",    IO_FLAG_DEFAULT),
    IMG_INFO_1(IN_LCSO,      IO_DIR_IN,  "inLCSO",       IO_FLAG_DEFAULT),
    IMG_INFO_1(IN_RSSO,      IO_DIR_IN,  "inRSSO",       IO_FLAG_DEFAULT),
    IMG_INFO_1(OUT_FD,       IO_DIR_OUT, "outFD",        IO_FLAG_DEFAULT),
    IMG_INFO_1(OUT_THN_YUV,  IO_DIR_OUT, "outThumbnailYUV", IO_FLAG_DEFAULT),
    IMG_INFO_1(OUT_JPEG_YUV, IO_DIR_OUT, "outJpegYUV",   IO_FLAG_DEFAULT),
    IMG_INFO_1(OUT_YUV,      IO_DIR_OUT, "outYUV",       IO_FLAG_DEFAULT),
    IMG_INFO_1(OUT_POSTVIEW, IO_DIR_OUT, "outPostView",  IO_FLAG_DEFAULT),

    IMG_INFO_2(IN_OPAQUE,    IO_DIR_IN,  "inOpaque",     IO_FLAG_DEFAULT),
    IMG_INFO_2(IN_FULL,      IO_DIR_IN,  "inFull",       IO_FLAG_DEFAULT),
    IMG_INFO_2(IN_RESIZED,   IO_DIR_IN,  "inResized",    IO_FLAG_DEFAULT),
    IMG_INFO_2(IN_LCSO,      IO_DIR_IN,  "inLCSO",       IO_FLAG_DEFAULT),
    IMG_INFO_2(IN_RSSO,      IO_DIR_IN,  "inRSSO",       IO_FLAG_DEFAULT),
};

const std::unordered_map<ID_META, ID_META> P2InIDMap::MainMeta =
{
    {IN_APP,        IN_APP},
    {IN_P1_APP,     IN_P1_APP},
    {IN_P1_HAL,     IN_P1_HAL},
    {IN_APP_PHY,    IN_APP_PHY},
    {OUT_APP_PHY,   OUT_APP_PHY},
};

const std::unordered_map<ID_IMG, ID_IMG> P2InIDMap::MainImg =
{
    {IN_REPROCESS,  IN_REPROCESS},
    {IN_OPAQUE,     IN_OPAQUE},
    {IN_FULL,       IN_FULL},
    {IN_RESIZED,    IN_RESIZED},
    {IN_LCSO,       IN_LCSO},
    {IN_RSSO,       IN_RSSO},
};

const std::unordered_map<ID_META, ID_META> P2InIDMap::SubMeta =
{
    {IN_APP,        IN_APP},
    {IN_P1_APP,     IN_P1_APP_2},
    {IN_P1_HAL,     IN_P1_HAL_2},
    {IN_APP_PHY,    IN_APP_PHY_2},
    {OUT_APP_PHY,   OUT_APP_PHY_2},
};

const std::unordered_map<ID_IMG, ID_IMG> P2InIDMap::SubImg =
{
    {IN_REPROCESS,  IN_REPROCESS},
    {IN_OPAQUE,     IN_OPAQUE_2},
    {IN_FULL,       IN_FULL_2},
    {IN_RESIZED,    IN_RESIZED_2},
    {IN_LCSO,       IN_LCSO_2},
    {IN_RSSO,       IN_RSSO_2},
};

P2InIDMap::P2InIDMap(const std::vector<MUINT32>& sensorIDList, const MUINT32 mainSensorID)
    : mMainSensorID(mainSensorID)
{
    for(MUINT32 sId : sensorIDList)
    {
        if(sId == mainSensorID)
        {
            mSensor2MetaID[sId] = MainMeta;
            mSensor2ImgID[sId] = MainImg;
        }
        else
        {
            // TODO If more than 2 sensors, need add logic here
            mSensor2MetaID[sId] = SubMeta;
            mSensor2ImgID[sId] = SubImg;
        }
    }
}

ID_META P2InIDMap::getMetaID(const MUINT32 sensorID, const ID_META inID)
{
    return mSensor2MetaID[sensorID][inID];
}
ID_IMG P2InIDMap::getImgID(const MUINT32 sensorID, const ID_IMG inID)
{
    return mSensor2ImgID[sensorID][inID];
}

MBOOL P2InIDMap::isEmpty(const MUINT32 sensorID)
{
    return (mSensor2ImgID[sensorID].empty() || mSensor2MetaID[sensorID].empty());
}

const META_INFO& P2Meta::getMetaInfo(ID_META id)
{
    static const META_INFO sInvalid =
        {ID_META_INVALID, ID_META_INVALID, IO_DIR_UNKNOWN, "invalid", IO_FLAG_INVALID};

    auto it = P2Meta::InfoMap.find(id);
    return (it != P2Meta::InfoMap.end()) ? it->second : sInvalid;
}

const IMG_INFO& P2Img::getImgInfo(ID_IMG id)
{
    static const IMG_INFO sInvalid =
        {ID_IMG_INVALID, ID_IMG_INVALID, IO_DIR_UNKNOWN, "invalid", IO_FLAG_INVALID};

    auto it = P2Img::InfoMap.find(id);
    return (it != P2Img::InfoMap.end()) ? it->second : sInvalid;
}

const char* P2Meta::getName(ID_META id)
{
    const char* name = "unknown";
    auto it = P2Meta::InfoMap.find(id);
    if( it != P2Meta::InfoMap.end() )
    {
        name = it->second.name.c_str();
    }
    return name;
}

const char* P2Img::getName(ID_IMG id)
{
    const char* name = "unknown";
    auto it = P2Img::InfoMap.find(id);
    if( it != P2Img::InfoMap.end() )
    {
        name = it->second.name.c_str();
    }
    return name;
}

P2MetaSet::P2MetaSet()
    : mHasOutput(MFALSE)
{
}

P2Meta::P2Meta(const ILog &log, const P2Pack &p2Pack, ID_META id)
    : mLog(log)
    , mP2Pack(p2Pack)
    , mMetaID(id)
{
}

ID_META P2Meta::getID() const
{
    return mMetaID;
}

#define P2_CLASS_TAG   P2Img
#define P2_TRACE   TRACE_P2_IMG
#include "P2_LogHeader.h"

P2Img::P2Img(const ILog &log, const P2Pack &p2Pack, ID_IMG id, MUINT32 debugIndex)
    : mLog(log)
    , mP2Pack(p2Pack)
    , mImgID(id)
    , mDebugIndex(debugIndex)
{
}

ID_IMG P2Img::getID() const
{
    return mImgID;
}

const char* P2Img::getHumanName() const
{
    const char* name = getName(mImgID);
    if( mImgID == OUT_FD )
    {
        name = "fd";
    }
    else if( mImgID == OUT_YUV )
    {
        name = isDisplay() ?  "display" :
               isRecord() ?   "record" :
                              "previewCB";
    }
    return name;
}

IMG_TYPE P2Img::getImgType() const
{
    return IMG_TYPE_EXTRA;
}

MSize P2Img::getImgSize() const
{
    MSize size(0, 0);
    if( isValid() )
    {
        IImageBuffer *img = getIImageBufferPtr();
        if( img )
        {
            size = img->getImgSize();
        }
    }
    return size;
}

MSize P2Img::getTransformSize() const
{
    MSize size(0, 0);
    if( isValid() )
    {
        IImageBuffer *img = getIImageBufferPtr();
        if( img )
        {
            size = img->getImgSize();
            if( getTransform() & eTransform_ROT_90 )
            {
                size = MSize(size.h, size.w);
            }
        }
    }
    return size;
}

const char * P2Img::Fmt2Name(MINT fmt)
{
    switch(fmt)
    {
    case NSCam::eImgFmt_RGBA8888:          return "rgba";
    case NSCam::eImgFmt_RGB888:            return "rgb";
    case NSCam::eImgFmt_RGB565:            return "rgb565";
    case NSCam::eImgFmt_STA_BYTE:          return "byte";
    case NSCam::eImgFmt_YVYU:              return "yvyu";
    case NSCam::eImgFmt_UYVY:              return "uyvy";
    case NSCam::eImgFmt_VYUY:              return "vyuy";
    case NSCam::eImgFmt_YUY2:              return "yuy2";
    case NSCam::eImgFmt_YV12:              return "yv12";
    case NSCam::eImgFmt_YV16:              return "yv16";
    case NSCam::eImgFmt_NV16:              return "nv16";
    case NSCam::eImgFmt_NV61:              return "nv61";
    case NSCam::eImgFmt_NV12:              return "nv12";
    case NSCam::eImgFmt_NV21:              return "nv21";
    case NSCam::eImgFmt_I420:              return "i420";
    case NSCam::eImgFmt_I422:              return "i422";
    case NSCam::eImgFmt_Y800:              return "y800";
    case NSCam::eImgFmt_BAYER8:            return "bayer8";
    case NSCam::eImgFmt_BAYER10:           return "bayer10";
    case NSCam::eImgFmt_BAYER12:           return "bayer12";
    case NSCam::eImgFmt_BAYER14:           return "bayer14";
    case NSCam::eImgFmt_FG_BAYER8:         return "fg_bayer8";
    case NSCam::eImgFmt_FG_BAYER10:        return "fg_bayer10";
    case NSCam::eImgFmt_FG_BAYER12:        return "fg_bayer12";
    case NSCam::eImgFmt_FG_BAYER14:        return "fg_bayer14";
    default:                               return "unknown";
    };

}

MVOID P2Img::dumpBuffer() const
{
    IImageBuffer *buffer = this->getIImageBufferPtr();
    if( buffer )
    {
        MUINT32 stride, pbpp, ibpp, width, height, size;
        MINT format = buffer->getImgFormat();
        stride = buffer->getBufStridesInBytes(0);
        pbpp = buffer->getPlaneBitsPerPixel(0);
        ibpp = buffer->getImgBitsPerPixel();
        size = buffer->getBufSizeInBytes(0);
        pbpp = pbpp ? pbpp : 8;
        width = stride * 8 / pbpp;
        width = width ? width : 1;
        ibpp = ibpp ? ibpp : 8;
        height = size / width;
        if( buffer->getPlaneCount() == 1 )
        {
            height = height * 8 / ibpp;
        }

        char path[256];
        snprintf(path, sizeof(path), DUMP_PATH "/%04d_%02d_%s_%dx%d_%d.%s.bin",
            mLog.getLogFrameID(), mDebugIndex, getHumanName(), width, height, stride,Fmt2Name(format));
        buffer->saveToFile(path);
    }
}

MVOID P2Img::dumpNddBuffer() const
{
    IImageBuffer *buffer = this->getIImageBufferPtr();
    if( buffer )
    {
        char filename[256] = {0};

        IMG_INFO info = getImgInfo(mImgID);

        FILE_DUMP_NAMING_HINT hint;
        hint = mP2Pack.getSensorData().mNDDHint;
        extract(&hint, buffer);

        switch(info.mirror)
        {
            case IN_FULL:
                genFileName_RAW(filename, sizeof(filename), &hint, RAW_PORT_IMGO);
                break;

            case IN_FULL_2:
                genFileName_RAW(filename, sizeof(filename), &hint, RAW_PORT_IMGO, info.name.c_str());
                break;

            case IN_RESIZED:
                genFileName_RAW(filename, sizeof(filename), &hint, RAW_PORT_RRZO);
                break;

            case IN_RESIZED_2:
                genFileName_RAW(filename, sizeof(filename), &hint, RAW_PORT_RRZO, info.name.c_str());
                break;

           case IN_LCSO:
               genFileName_LCSO(filename, sizeof(filename), &hint);
               break;

           case IN_LCSO_2:
               genFileName_LCSO(filename, sizeof(filename), &hint, info.name.c_str());
               break;

           case OUT_YUV:
               {
                   if( this->isDisplay())
                   {
                       genFileName_YUV(filename, sizeof(filename), &hint, YUV_PORT_WDMAO);
                   }
                   else if( this->isRecord())
                   {
                       genFileName_YUV(filename, sizeof(filename), &hint, YUV_PORT_WROTO);
                   }
                   else
                   {
                       genFileName_YUV(filename, sizeof(filename), &hint,YUV_PORT_UNDEFINED);
                   }
               }
               break;
           default:
               break;

        };

        if( filename[0] )
        {
            MY_S_LOGD(mP2Pack.mLog, "dump to: %s", filename);
            buffer->saveToFile(filename);
        }
    }
}

MINT32 P2Img::getMagic3A() const
{
    return mP2Pack.getSensorData().mMagic3A;
}

MBOOL P2ImgPlugin::onPlugin(const sp<P2Img> &img)
{
    return onPlugin(img.get());
}

MBOOL isValid(const P2Meta *meta)
{
    return (meta != NULL) && meta->isValid();
}

MBOOL isValid(const P2Img *img)
{
    return (img != NULL) && img->isValid();
}

MBOOL isValid(const sp<P2Meta> &meta)
{
    return (meta != NULL) && meta->isValid();
}

MBOOL isValid(const sp<P2Img> &img)
{
    return (img != NULL) && img->isValid();
}

IMetadata* toIMetadataPtr(const sp<P2Meta> &meta)
{
    return meta != NULL ? meta->getIMetadataPtr() : NULL;
}

IImageBuffer* toIImageBufferPtr(const sp<P2Img> &img)
{
    return img != NULL ? img->getIImageBufferPtr() : NULL;
}

} // namespace P2
