/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "P2_Param.h"
#include "P2_CaptureProcessor.h"
#include "P2_DebugControl.h"

using namespace NSCam;
using namespace NSCam::NSPipelinePlugin;
using namespace NSCam::NSCamFeature::NSFeaturePipe::NSCapture;

#define P2_CAPTURE_THREAD_NAME "p2_capture"

namespace P2
{

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    ReleaseBundle
#define P2_TRACE        TRACE_RELEASE_BUNDLE
#include "P2_LogHeader.h"

struct ReleaseBundle : public virtual RefBase
{
    ReleaseBundle(const char* pName, const sp<IP2Frame>& pP2Frame, const ILog& log)
        : mName(pName)
        , mpP2Frame(pP2Frame)
        , mLog(log)
    {
        TRACE_S_FUNC_ENTER(mLog, "name:%s", mName.c_str());
        TRACE_S_FUNC_EXIT(mLog, "name:%s", mName.c_str());
    }

    ~ReleaseBundle()
    {
        TRACE_S_FUNC_ENTER(mLog, "name:%s", mName.c_str());
        sp<IP2Frame> pP2Frame = mpP2Frame.promote();
        if (pP2Frame != NULL) {
            pP2Frame->endBatchRelease();
            pP2Frame->beginBatchRelease();
        }
        else {
            MY_S_LOGW(mLog, "falied to promote, name:%s", mName.c_str());
        }

        TRACE_S_FUNC_EXIT(mLog, "name:%s", mName.c_str());
    };
    const std::string&  mName;
    wp<IP2Frame>        mpP2Frame;
    ILog                mLog;
};

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    P2BufferHandle
#define P2_TRACE        TRACE_P2_BUFFER_HANDLE
#include "P2_LogHeader.h"

class P2BufferHandle : public virtual BufferHandle
{
public:
    P2BufferHandle(const sp<P2Request>& pRequest, ID_IMG id, sp<ReleaseBundle> pBundle, const ILog& log)
        : mpRequest(pRequest)
        , mpP2Img(NULL)
        , mImgId(id)
        , mpImageBuffer(NULL)
        , mpReleaseBundle(pBundle)
        , mLog(log)
    {
        TRACE_S_FUNC_ENTER(mLog, "imgId:0x%" PRIx32, mImgId);
        TRACE_S_FUNC_EXIT(mLog);
    }

    P2BufferHandle(const sp<P2Request> pRequest, sp<P2Img> pP2Img)
        : mpRequest(pRequest)
        , mpP2Img(pP2Img)
        , mImgId(OUT_YUV)
        , mpImageBuffer(NULL)
    {
        TRACE_S_FUNC_ENTER(mLog, "imgId:0x%" PRIx32, mImgId);
        TRACE_S_FUNC_EXIT(mLog);
    }

    virtual MERROR acquire(MINT usage)
    {
        (void)usage;
        TRACE_S_FUNC_ENTER(mLog, "imgId:0x%" PRIx32 "usage:0x%" PRIx32, mImgId, usage);
        if (mpRequest->isValidImg(mImgId)) {
            mpP2Img = mpRequest->getImg(mImgId);
            mpP2Img->updateResult(MTRUE);
            mpImageBuffer = mpP2Img->getIImageBufferPtr();
        }
        else {
            MY_S_LOGW(mLog, "falied to acquire, imgId:0x%" PRIx32, mImgId);
            return BAD_VALUE;
        }
        TRACE_S_FUNC_EXIT(mLog);
        return OK;
    }

    virtual IImageBuffer* native()
    {
        return mpImageBuffer;
    }

    virtual void release()
    {
        TRACE_S_FUNC_ENTER(mLog, "imgId:0x%" PRIx32, mImgId);
        mpImageBuffer = NULL;
        mpP2Img = NULL;

        if (mpRequest != NULL) {
            // For now, do not support OUT_YUV early release
            if (mImgId != OUT_YUV)
                mpRequest->releaseImg(mImgId);
            mpReleaseBundle = NULL;
        }
        TRACE_S_FUNC_EXIT(mLog);
    }

    virtual MUINT32 getTransform()
    {
        if (mpP2Img == NULL)
            return 0;
        return mpP2Img->getTransform();
    }

    virtual ~P2BufferHandle()
    {
        TRACE_S_FUNC_ENTER(mLog, "imgId:0x%" PRIx32, mImgId);
        if (mpRequest != NULL) {
            MY_S_LOGD(mLog, "buffer(0x%" PRIx32 ") not released", mImgId);
            release();
        }
        TRACE_S_FUNC_EXIT(mLog);
    }

private:
    sp<P2Request>       mpRequest;
    sp<P2Img>           mpP2Img;
    ID_IMG              mImgId;
    IImageBuffer*       mpImageBuffer;
    sp<ReleaseBundle>   mpReleaseBundle;
    ILog                mLog;
};

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    P2MetadataHandle
#define P2_TRACE        TRACE_P2_METADATA_HANDLE
#include "P2_LogHeader.h"

class P2MetadataHandle : public virtual MetadataHandle
{
public:
    P2MetadataHandle(const sp<P2Request>& pRequest, ID_META id, const ILog& log)
        : mpRequest(pRequest)
        , mpP2Meta(NULL)
        , mMetaId(id)
        , mpMetadata(NULL)
        , mLog(log)
    {
        TRACE_S_FUNC_ENTER(mLog, "metaId:0x%" PRIx32, mMetaId);
        TRACE_S_FUNC_EXIT(mLog);
    }

    virtual MERROR acquire()
    {
        TRACE_S_FUNC_ENTER(mLog, "metaId:0x%" PRIx32, mMetaId);
        if (mpRequest->isValidMeta(mMetaId)) {
            mpP2Meta = mpRequest->getMeta(mMetaId);
            mpP2Meta->updateResult(MTRUE);
            mpMetadata = mpP2Meta->getIMetadataPtr();
        }
        else
        {
            MY_S_LOGW(mLog, "falied to acquire, metaId:0x%" PRIx32, mMetaId);
            return BAD_VALUE;
        }
        TRACE_S_FUNC_EXIT(mLog);
        return OK;
    }

    virtual IMetadata* native()
    {
        return mpMetadata;
    }

    virtual void release()
    {
        TRACE_S_FUNC_ENTER(mLog, "metaId:0x%" PRIx32, mMetaId);
        if( mpRequest != NULL ) {
            mpRequest->releaseMeta(mMetaId);
            mpRequest = NULL;
        }
        mpMetadata = NULL;
        mpP2Meta = NULL;
        TRACE_S_FUNC_EXIT(mLog);
    }

    virtual ~P2MetadataHandle()
    {
        TRACE_S_FUNC_ENTER(mLog, "metaId:0x%" PRIx32, mMetaId);
        if (mpRequest != NULL) {
            MY_S_LOGW(mLog, "metadata(%d) not released", mMetaId);
            release();
        }
        TRACE_S_FUNC_EXIT(mLog);
    }

private:
    sp<P2Request>   mpRequest;
    sp<P2Meta>      mpP2Meta;
    ID_META         mMetaId;
    IMetadata*      mpMetadata;
    ILog            mLog;
};

#include "P2_DebugControl.h"
#define P2_CLASS_TAG    CaptureProcessor
#define P2_TRACE        TRACE_CAPTURE_PROCESSOR
#include "P2_LogHeader.h"

CaptureProcessor::CaptureProcessor()
    : Processor(P2_CAPTURE_THREAD_NAME)
    , mAbortingRequestNo(-1)
    , mLastRequestNo(-1)
    , mLastFrameCount(0)
    , mLastFrameIndex(0)
    , mStatus(STATUS_None)
{
    MY_LOG_FUNC_ENTER();

    mDebugDrop = property_get_int32("vendor.camera.debug.p2c.drop", -1);

    MY_LOG_FUNC_EXIT();
}

CaptureProcessor::~CaptureProcessor()
{
    TRACE_S_FUNC_ENTER(mLog);
    this->uninit();
    TRACE_S_FUNC_EXIT(mLog);
}

MBOOL CaptureProcessor::onInit(const P2InitParam &param)
{
    ILog log = param.mP2Info.mLog;
    TRACE_S_FUNC_ENTER(log);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:init()");
    MBOOL ret = MTRUE;
    mP2Info = param.mP2Info;
    mLog = mP2Info.mLog;

    P2_CAM_TRACE_BEGIN(TRACE_DEFAULT, "P2_Capture:FeaturePipe create & init");

    const MBOOL isDualCam = (mP2Info.getConfigInfo().mAllSensorID.size() > 1);
    const MINT32 sensorID = isDualCam ? mP2Info.getConfigInfo().mAllSensorID[0] : mP2Info.getConfigInfo().mMainSensorID;
    const MINT32 sensorID2 = isDualCam ? mP2Info.getConfigInfo().mAllSensorID[1] : -1;
    MY_S_LOGD(mLog, "create captureFeaturePipe, isDualCam:%d, sensorID:%d, sensorID2:%d", isDualCam, sensorID, sensorID2);
    ICaptureFeaturePipe::UsageHint usage = ICaptureFeaturePipe::UsageHint();
    usage.mSupportedScenarioFeatures = param.mP2Info.getConfigInfo().mUsageHint.mTP;
    MY_S_LOGD(mLog, "supportSceneroiFeatures:0x%" PRIx64, usage.mSupportedScenarioFeatures);
    mpFeaturePipe = ICaptureFeaturePipe::createInstance(
                        sensorID,
                        usage,
                        sensorID2);

    if (mpFeaturePipe == NULL) {
        MY_S_LOGE(mLog, "OOM: cannot create FeaturePipe");
    } else {
        mpFeaturePipe->init();
    }

    mpCallback = new CaptureRequestCallback(this);
    mpFeaturePipe->setCallback(mpCallback);

    P2_CAM_TRACE_END(TRACE_DEFAULT);
    TRACE_S_FUNC_EXIT(log);
    return ret;
}

MVOID CaptureProcessor::onUninit()
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:uninit()");

    mpFeaturePipe->uninit();
    mpFeaturePipe = NULL;

    TRACE_S_FUNC_EXIT(mLog);
}

MVOID CaptureProcessor::onThreadStart()
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:threadStart()");
    TRACE_S_FUNC_EXIT(mLog);
}

MVOID CaptureProcessor::onThreadStop()
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:threadStop()");
    TRACE_S_FUNC_EXIT(mLog);
}

MBOOL CaptureProcessor::onConfig(const P2ConfigParam &param)
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:config()");
    mP2Info = param.mP2Info;
    TRACE_S_FUNC_EXIT(mLog);
    return MTRUE;
}

MBOOL CaptureProcessor::onEnque(const sp<P2FrameRequest> &pP2Frame)
{
    const ILog log = spToILog(pP2Frame);
    TRACE_S_FUNC_ENTER(log);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:enque()");
    MBOOL ret = MTRUE;

    std::vector<sp<P2Request>> pP2Requests = pP2Frame->extractP2Requests();

    sp<P2Request> pRequest;
    for (auto& p : pP2Requests) {
        if (pRequest == NULL) {
            pRequest = p;
        } else {
            MY_S_LOGE(log, "Not support multiple p2 requests");
            p->beginBatchRelease();
            p->releaseResource(P2Request::RES_ALL);
            p->endBatchRelease();
            break;
        }
    }

    if (pRequest == NULL) {
        MY_S_LOGE(log, "P2Request is NULL!");
        return MFALSE;
    }
    // pDetachP2Request is same as pRequest, expect that doesn't hold a MWFrame
    const sp<P2Request> pDetachP2Request = pRequest->makeDetachP2Request();

    const P2FrameData& rFrameData = pRequest->mP2Pack.getFrameData();
    MINT32 requestNo = rFrameData.mMWFrameRequestNo;
    MINT32 frameCount = 0;
    MINT32 frameIndex = 0;
    sp<ICaptureFeatureRequest> pCapRequest = mpFeaturePipe->acquireRequest();
    pCapRequest->addParameter(PID_REQUEST_NUM, requestNo);
    pCapRequest->addParameter(PID_FRAME_NUM, rFrameData.mMWFrameNo);


    MINT64 feature = 0;
    if (pRequest->isValidMeta(IN_P1_HAL)) {
        sp<P2Meta> meta = pRequest->getMeta(IN_P1_HAL);
        if (tryGet<MINT32>(meta, MTK_HAL_REQUEST_COUNT, frameCount) &&
            tryGet<MINT32>(meta, MTK_HAL_REQUEST_INDEX, frameIndex)) {
            pCapRequest->addParameter(PID_FRAME_COUNT, frameCount);
            pCapRequest->addParameter(PID_FRAME_INDEX, frameIndex);
        }

        if (tryGet<MINT64>(meta, MTK_FEATURE_CAPTURE, feature)) {

            MY_S_LOGI(log, "Index/Count:%d/%d Feature:0x%" PRIx64, frameIndex, frameCount, feature);

            if (feature & MTK_FEATURE_REMOSAIC)
                pCapRequest->addFeature(FID_REMOSAIC);
            if (feature & MTK_FEATURE_NR)
                pCapRequest->addFeature(FID_NR);
            if (feature & MTK_FEATURE_FB)
                pCapRequest->addFeature(FID_FB);
            if (feature & MTK_FEATURE_ABF)
                pCapRequest->addFeature(FID_ABF);
            if (feature & MTK_FEATURE_AINR)
                pCapRequest->addFeature(FID_AINR);
            if (feature & MTK_FEATURE_MFNR)
                pCapRequest->addFeature(FID_MFNR);
            if (feature & MTK_FEATURE_HDR)
                pCapRequest->addFeature(FID_HDR);
            if (feature & TP_FEATURE_FB)
                pCapRequest->addFeature(FID_FB_3RD_PARTY);
            if (feature & TP_FEATURE_MFNR)
                pCapRequest->addFeature(FID_MFNR_3RD_PARTY);
            if (feature & TP_FEATURE_HDR)
                pCapRequest->addFeature(FID_HDR_3RD_PARTY);
            if (feature & MTK_FEATURE_DRE)
                pCapRequest->addFeature(FID_DRE);
            if (feature & MTK_FEATURE_CZ)
                pCapRequest->addFeature(FID_CZ);
        }
    }

    if (pRequest->isValidMeta(IN_APP)) {
        sp<P2Meta> meta = pRequest->getMeta(IN_APP);
        MINT32 trigger;
        if (tryGet<MINT32>(meta, MTK_CONTROL_CAPTURE_EARLY_NOTIFICATION_TRIGGER, trigger))
        {
            trigger = trigger > 0 ? 1 : 0;
            pCapRequest->addParameter(PID_ENABLE_NEXT_CAPTURE, trigger);
        }
    }


    // Error handling of multiple frames
    {
        MBOOL bDropRequest = MFALSE;


        // Deubg
        if (frameIndex == mDebugDrop) {
            MY_S_LOGW(log, "Force to abort, Index/Count:%d/%d", frameIndex, frameCount);
            abortRequest(requestNo);
            mAbortingRequestNo = requestNo;
            bDropRequest = MTRUE;
        // All frames should have full-size input
        } else if (frameCount > 1 && !pRequest->isValidImg(IN_FULL)) {
            MY_S_LOGW(log, "Multi-frame request has no IN_FULL(%d). Abort request.",
                    pRequest->isValidImg(IN_FULL));
            abortRequest(requestNo);
            mAbortingRequestNo = requestNo;
            bDropRequest = MTRUE;
        // Abort the rest frames
        } else if (mAbortingRequestNo == requestNo) {
            MY_S_LOGW(log, "Request is aborting, Index/Count:%d/%d", frameIndex, frameCount);
            bDropRequest = MTRUE;
        } else {
            if (mLastRequestNo == requestNo) {
                if (mLastFrameIndex + 1 != frameIndex) {
                    // out-of-order frame or previous frame dropped
                    MY_S_LOGW(log, "Out-of-order Frame Index/Count:%d/%d", frameIndex, frameCount);
                    abortRequest(requestNo);
                    mAbortingRequestNo = requestNo;
                    bDropRequest = MTRUE;
                }
            } else {
                if (mLastFrameCount != 0 && mLastFrameCount != mLastFrameIndex + 1) {
                    // Previous blending request is not completed
                    MY_S_LOGW(log, "Abort Previous Request Num/Index/Count:%d/%d/%d",
                            mLastRequestNo, mLastFrameIndex, mLastFrameCount);
                    abortRequest(mLastRequestNo);
                    mAbortingRequestNo = mLastRequestNo;
                }
            }
        }

        if (bDropRequest) {
            pRequest->beginBatchRelease();
            pRequest->releaseResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            return MFALSE;
        }

        mLastRequestNo = requestNo;
        mLastFrameCount = frameCount;
        mLastFrameIndex = frameIndex;
    }

    // Flow of dropping frame
    {
        if (!pRequest->isValidImg(IN_FULL) && !pRequest->isValidImg(IN_OPAQUE) && !pRequest->isValidImg(IN_REPROCESS)) {
            MY_S_LOGW(log, "Request has no IN_FULL(%d),IN_OPAQUE(%d) ,or IN_REPROCESS(%d). Skip frame.",
                    pRequest->isValidImg(IN_FULL),
                    pRequest->isValidImg(IN_OPAQUE),
                    pRequest->isValidImg(IN_REPROCESS));
            pRequest->beginBatchRelease();
            pRequest->releaseResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            return MFALSE;
        }

        if (frameIndex < 1 && !pRequest->hasOutput()) {
            MY_S_LOGW(log, "Non-blending request has no output(%d)", pRequest->hasOutput());
            pRequest->beginBatchRelease();
            pRequest->releaseResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            return MFALSE;
        }
    }

    // Metadata
    auto MapMetadata = [&](ID_META id, CaptureMetadataID metaId) -> void {
        if (pRequest->isValidMeta(id))
            pCapRequest->addMetadata(metaId, new P2MetadataHandle(pDetachP2Request, id, log));
    };

    MapMetadata(IN_P1_APP,  MID_MAN_IN_P1_DYNAMIC);
    MapMetadata(IN_P1_HAL,  MID_MAN_IN_HAL);
    MapMetadata(IN_APP,     MID_MAN_IN_APP);
    MapMetadata(OUT_APP,    MID_MAN_OUT_APP);
    MapMetadata(OUT_HAL,    MID_MAN_OUT_HAL);

    // Image
    auto MapBuffer = [&](ID_IMG id, CaptureBufferID bufId, const sp<ReleaseBundle>& pBundle) -> MBOOL {
        if (pRequest->isValidImg(id)) {
            pCapRequest->addBuffer(bufId, new P2BufferHandle(pDetachP2Request, id, pBundle, log));
            return MTRUE;
        }
        return MFALSE;
    };

    sp<ReleaseBundle> pReleaseRaw = new ReleaseBundle("releaseRaw", pRequest, log);
    MBOOL hasOpaque = MapBuffer(IN_OPAQUE, BID_MAN_IN_FULL, pReleaseRaw);
    MBOOL hasRaw = MapBuffer(IN_FULL, BID_MAN_IN_FULL, pReleaseRaw);
    if (hasRaw && hasOpaque)
        MY_S_LOGW(log, "have opaque and raw input, using raw as input");

    MapBuffer(IN_RESIZED,   BID_MAN_IN_RSZ, pReleaseRaw);
    MapBuffer(IN_LCSO,      BID_MAN_IN_LCS, pReleaseRaw);
    MapBuffer(IN_REPROCESS, BID_MAN_IN_YUV, NULL);
    MapBuffer(OUT_JPEG_YUV, BID_MAN_OUT_JPEG, NULL);
    MapBuffer(OUT_THN_YUV,  BID_MAN_OUT_THUMBNAIL, NULL);

    sp<ReleaseBundle> pReleasePostview = new ReleaseBundle("releasePostview", pRequest, log);
    MapBuffer(OUT_POSTVIEW, BID_MAN_OUT_POSTVIEW, pReleasePostview);

    size_t n = pRequest->mImgOutArray.size();
    if (n > 2) {
        MY_S_LOGE(log, "can NOT support more than 2 yuv streams: %zu", n);
        n = 2;
    }
    for (size_t i = 0; i < n; i++) {
        pCapRequest->addBuffer(BID_MAN_OUT_YUV00 + i,
                new P2BufferHandle(pDetachP2Request, pDetachP2Request->mImgOutArray[i]));
    }
    // Trigger Batch Release
    pRequest->beginBatchRelease();

    const MBOOL isDualCam = (mP2Info.getConfigInfo().mAllSensorID.size() > 1);
    if (isDualCam)
    {
        // Feature
        MY_S_LOGD(log, "ducam feature support");
        {
            if (feature & MTK_FEATURE_DEPTH)
                pCapRequest->addFeature(FID_DEPTH);
            if (feature & MTK_FEATURE_BOKEH)
                pCapRequest->addFeature(FID_BOKEH);
            if (feature & TP_FEATURE_DEPTH)
                pCapRequest->addFeature(FID_DEPTH_3RD_PARTY);
            if (feature & TP_FEATURE_BOKEH)
                pCapRequest->addFeature(FID_BOKEH_3RD_PARTY);
            if (feature & TP_FEATURE_FUSION)
                pCapRequest->addFeature(FID_FUSION_3RD_PARTY);
            if (feature & TP_FEATURE_HDR_DC)
                pCapRequest->addFeature(FID_HDR2_3RD_PARTY);
            if (feature & TP_FEATURE_PUREBOKEH)
                pCapRequest->addFeature(FID_PUREBOKEH_3RD_PARTY);

        }
        // Meta
        {
            MapMetadata(IN_P1_APP_2, MID_SUB_IN_P1_DYNAMIC);
            MapMetadata(IN_P1_HAL_2, MID_SUB_IN_HAL);
        }
        // Image
        {
            MapBuffer(IN_FULL_2, BID_SUB_IN_FULL, MFALSE);
            MapBuffer(IN_RESIZED_2, BID_SUB_IN_RSZ, MFALSE);
            MapBuffer(IN_LCSO_2, BID_SUB_IN_LCS, MFALSE);
        }
    }

    // Request Pair
    {
        Mutex::Autolock _l(mPairLock);
        auto& rPair = mRequestPairs.editItemAt(mRequestPairs.add());
        rPair.mpCapRequest = pCapRequest;
        rPair.mpP2Request = pRequest;
        //
        const MBOOL bgPreRelease = mP2Info.getConfigInfo().mUsageHint.mBGPreRelease;
        const MBOOL isFlushStatus = mStatus == STATUS_Flush;
        MY_S_LOGD(log, "add request to requestPairs(size:%zu), capReq-R/F Num:%d/%d, isFlushStatus:%d, bgPreRelease:%d",
               mRequestPairs.size(), pCapRequest->getRequestNo(), pCapRequest->getFrameNo(), isFlushStatus, bgPreRelease);
        if(isFlushStatus && bgPreRelease) {
            pRequest->detachResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            rPair.mpP2Request = pRequest->makeDetachP2Request();
        }
    }

    MY_S_LOGD(log, "enque request to captureFeaturePipe, capReq-R/F Num:%d/%d", pCapRequest->getRequestNo(), pCapRequest->getFrameNo());
    mpFeaturePipe->enque(pCapRequest);

    TRACE_S_FUNC_EXIT(log);
    return ret;
}

MVOID CaptureProcessor::abortRequest(MINT32 reqrstNo)
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:abort()");

    Vector<sp<ICaptureFeatureRequest>> vpCapRequests;
    {
        Mutex::Autolock _l(mPairLock);
        auto it = mRequestPairs.begin();
        for (; it != mRequestPairs.end(); it++) {

            sp<P2Request>& pP2Request = (*it).mpP2Request;
            const P2FrameData& rFrameData = pP2Request->mP2Pack.getFrameData();
            sp<ICaptureFeatureRequest>& pCapRequest = (*it).mpCapRequest;

            if (rFrameData.mMWFrameRequestNo == reqrstNo) {
                vpCapRequests.push_back(pCapRequest);
            }
        }

    }

    mLastRequestNo = 0;
    mLastFrameCount = 0;
    mLastFrameIndex = 0;

    for (auto& pCapRequest : vpCapRequests) {
        MY_S_LOGW(mLog, "abort request, R/F Num: %d/%d",
                pCapRequest->getRequestNo(),
                pCapRequest->getFrameNo());
        mpFeaturePipe->abort(pCapRequest);
    }
    TRACE_S_FUNC_EXIT(mLog);
}

MVOID CaptureProcessor::onNotifyFlush()
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:notifyFlush()");
    const MBOOL bgPreRelease = mP2Info.getConfigInfo().mUsageHint.mBGPreRelease;
    MY_S_LOGD(mLog, "flush +, bgPreRelease:%d", bgPreRelease);
    if( bgPreRelease ) {
        Mutex::Autolock _l(mPairLock);
        for(size_t i = 0; i < mRequestPairs.size(); ++i)
        {
            const sp<P2Request>& pRequest = mRequestPairs[i].mpP2Request;
            const sp<ICaptureFeatureRequest>& pCapRequest = mRequestPairs[i].mpCapRequest;
            //
            const ILog log = spToILog(pRequest);
            pRequest->detachResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            auto& rPair = mRequestPairs.editItemAt(i);
            rPair.mpP2Request = pRequest->makeDetachP2Request();
            MY_S_LOGD(log, "detach request all resource, index:%zu/%zu, capReq-R/F Num:%d/%d",
                i, mRequestPairs.size(), pCapRequest->getRequestNo(), pCapRequest->getFrameNo());
        }
        mStatus = STATUS_Flush;
    }
    else {
        if (mpFeaturePipe != NULL) {
            mpFeaturePipe->flush();
        }
    }
    MY_S_LOGD(mLog, "flush -");
    TRACE_S_FUNC_EXIT(mLog);
}

MVOID CaptureProcessor::onWaitFlush()
{
    TRACE_S_FUNC_ENTER(mLog);
    P2_CAM_TRACE_NAME(TRACE_DEFAULT, "P2_Capture:waitFlush()");
    TRACE_S_FUNC_EXIT(mLog);
}

CaptureRequestCallback::CaptureRequestCallback(CaptureProcessor* pProcessor)
    : mpProcessor(pProcessor)
{

}

void CaptureRequestCallback::onContinue(sp<ICaptureFeatureRequest> pCapRequest)
{
    TRACE_S_FUNC_ENTER(mpProcessor->mLog);
    Mutex::Autolock _l(mpProcessor->mPairLock);
    auto it = mpProcessor->mRequestPairs.begin();
    for (; it != mpProcessor->mRequestPairs.end(); it++) {
        if ((*it).mpCapRequest == pCapRequest) {
            sp<P2Request>& pRequest = (*it).mpP2Request;
            const ILog log = spToILog(pRequest);
            pRequest->notifyNextCapture();
            MY_S_LOGD(log, "notify next capture, requestPairsSize:%zu, capReq-R/F Num:%d/%d",
                mpProcessor->mRequestPairs.size(), pCapRequest->getRequestNo(), pCapRequest->getFrameNo());
            break;
        }
    }
    TRACE_S_FUNC_EXIT(mpProcessor->mLog);
}

void CaptureRequestCallback::onAborted(sp<ICaptureFeatureRequest> pCapRequest)
{
    TRACE_S_FUNC_ENTER(mpProcessor->mLog);
    Mutex::Autolock _l(mpProcessor->mPairLock);
    auto it = mpProcessor->mRequestPairs.begin();
    for (; it != mpProcessor->mRequestPairs.end(); it++) {
        if ((*it).mpCapRequest == pCapRequest) {
            sp<P2Request>& pRequest = (*it).mpP2Request;
            const ILog log = spToILog(pRequest);
            pRequest->updateResult(MFALSE);
            pRequest->updateMetaResult(MFALSE);
            pRequest->releaseResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            mpProcessor->mRequestPairs.erase(it);
            MY_S_LOGD(log, "earse request for aborting, requestPairsSize:%zu, capReq-R/F Num:%d/%d",
                mpProcessor->mRequestPairs.size(), pCapRequest->getRequestNo(), pCapRequest->getFrameNo());
            break;
        }
    }
    TRACE_S_FUNC_EXIT(mpProcessor->mLog);
}

void CaptureRequestCallback::onCompleted(sp<ICaptureFeatureRequest> pCapRequest, MERROR ret)
{
    TRACE_S_FUNC_ENTER(mpProcessor->mLog);
    Mutex::Autolock _l(mpProcessor->mPairLock);
    auto it = mpProcessor->mRequestPairs.begin();
    for (; it != mpProcessor->mRequestPairs.end(); it++) {
        if ((*it).mpCapRequest == pCapRequest) {
            sp<P2Request>& pRequest = (*it).mpP2Request;
            const ILog log = spToILog(pRequest);
            pRequest->updateResult(ret == OK);
            pRequest->updateMetaResult(ret == OK);
            pRequest->releaseResource(P2Request::RES_ALL);
            pRequest->endBatchRelease();
            mpProcessor->mRequestPairs.erase(it);
            MY_S_LOGD(log, "earse request for completing, requestPairsSize:%zu, capReq-R/F Num:%d/%d",
                mpProcessor->mRequestPairs.size(), pCapRequest->getRequestNo(), pCapRequest->getFrameNo());
            break;
        }
    }
    TRACE_S_FUNC_EXIT(mpProcessor->mLog);
}

} // namespace P2
