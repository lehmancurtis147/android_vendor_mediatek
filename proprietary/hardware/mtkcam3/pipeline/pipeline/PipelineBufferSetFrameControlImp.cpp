/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/pipeline"
//
#include <iomanip>
#include <sstream>
//
#include "MyUtils.h"
#include "PipelineBufferSetFrameControlImp.h"
#include <mtkcam3/pipeline/hwnode/NodeId.h>
// [Bg service]
#include <mtkcam3/pipeline/prerelease/IPreReleaseRequest.h>
//
using namespace android;
using namespace NSCam;
using namespace NSCam::v3;
using namespace NSCam::v3::Utils;
using namespace NSCam::v3::NSPipelineBufferSetFrameControlImp;
// [Bg service]
using namespace NSCam::v3::pipeline::prerelease;

#define MAIN_CLASS_NAME PipelineBufferSetFrameControlImp

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if (            (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if (            (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if (            (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( CC_UNLIKELY(cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
//
#define LOGLEVEL                    0
#define MY_LOG1(...)                MY_LOGD_IF(CC_UNLIKELY(1<=LOGLEVEL), __VA_ARGS__)
#define MY_LOG2(...)                MY_LOGD_IF(CC_UNLIKELY(2<=LOGLEVEL), __VA_ARGS__)


/******************************************************************************
 *
 ******************************************************************************/
static long getDurationInUS(struct timespec const& t1, struct timespec const& t2)
{
    struct timespec diff;
    if (t2.tv_nsec-t1.tv_nsec < 0) {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec - 1;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec + 1000000000;
    } else {
        diff.tv_sec  = t2.tv_sec - t1.tv_sec;
        diff.tv_nsec = t2.tv_nsec - t1.tv_nsec;
    }
    return (diff.tv_sec * 1000000.0 + diff.tv_nsec / 1000.0);
}


/******************************************************************************
 *
 ******************************************************************************/
static std::string getFrameLifetimeLog(struct timespec const& start, struct timespec const& end)
{
    std::string os;
    auto pLogTool = NSCam::Utils::LogTool::get();
    if ( CC_LIKELY(pLogTool) ) {
        os += "{";
        os += pLogTool->convertToFormattedLogTime(&start);
        if ( 0 != end.tv_sec || 0 != end.tv_nsec ) {
            os += " -> ";
            os += pLogTool->convertToFormattedLogTime(&end);
            os += " (";
            os += std::to_string(getDurationInUS(start, end));
            os += "us)";
        }
        os += "}";
    }
    return os;
}


/******************************************************************************
 *
 ******************************************************************************/
static std::string toString(const IPipelineFrame::ImageInfoIOMapSet& o)
{
    std::ostringstream oss;
    oss << "{ ";
    for (size_t i = 0; i < o.size(); i++) {
        auto const& iomap = o[i];
        oss << "( ";
        for (size_t j = 0; j < iomap.vIn.size(); j++) {
            auto const& streamId = iomap.vIn.keyAt(j);
            oss << "0x" << std::setbase(16) << streamId << " ";
        }
        oss << "-> ";
        for (size_t j = 0; j < iomap.vOut.size(); j++) {
            auto const& streamId = iomap.vOut.keyAt(j);
            oss << "0x" << std::setbase(16) << streamId << " ";
        }
        oss << ")";
    }
    oss << " }";
    return oss.str();
}


/******************************************************************************
 *
 ******************************************************************************/
static std::string toString(const IPipelineFrame::MetaInfoIOMapSet& o)
{
    std::ostringstream oss;
    oss << "{ ";
    for (size_t i = 0; i < o.size(); i++) {
        auto const& iomap = o[i];
        oss << "( ";
        for (size_t j = 0; j < iomap.vIn.size(); j++) {
            auto const& streamId = iomap.vIn.keyAt(j);
            oss << "0x" << std::setbase(16) << streamId << " ";
        }
        oss << "-> ";
        for (size_t j = 0; j < iomap.vOut.size(); j++) {
            auto const& streamId = iomap.vOut.keyAt(j);
            oss << "0x" << std::setbase(16) << streamId << " ";
        }
        oss << ")";
    }
    oss << " }";
    return oss.str();
}


/******************************************************************************
 *
 ******************************************************************************/
static void releaseHalImageSet(HalImageSetT& aHalImageSet)
{
    HalImageSetT::iterator it = aHalImageSet.begin();
    for (; it != aHalImageSet.end(); it++) {
        if  (CC_LIKELY( (*it) != 0 )) {
            (*it)->releaseBuffer();
        }
    }
    aHalImageSet.clear();
}


/******************************************************************************
 *
 ******************************************************************************/
static void releaseHalMetaSet(HalMetaSetT& aHalMetaSet)
{
    HalMetaSetT::iterator it = aHalMetaSet.begin();
    for (; it != aHalMetaSet.end(); it++) {
        if  (CC_LIKELY( (*it) != 0 )) {
            (*it)->releaseBuffer();
        }
    }
    aHalMetaSet.clear();
}


/******************************************************************************
 *
 ******************************************************************************/
IPipelineBufferSetFrameControl*
IPipelineBufferSetFrameControl::
create(
    MUINT32 requestNo,
    MUINT32 frameNo,
    MBOOL bReporcessFrame,
    android::wp<IAppCallback>const& pAppCallback,
    IPipelineStreamBufferProvider const* pBufferProvider,
    android::wp<IPipelineNodeCallback> pNodeCallback
)
{
    if (CC_UNLIKELY( pNodeCallback == NULL )) {
        MY_LOGE("IPipelineNodeCallback should not be NULL!");
        return NULL;
    }
    return new MAIN_CLASS_NAME(requestNo, frameNo, bReporcessFrame, pAppCallback, pBufferProvider, pNodeCallback);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
IPipelineBufferSetFrameControl::
castFrom(IPipelineFrame* pPipelineFrame) -> IPipelineBufferSetFrameControl*
{
    if (CC_UNLIKELY( pPipelineFrame == nullptr )) {
        return nullptr;
    }

    if (CC_UNLIKELY( 0 != ::strcmp(pPipelineFrame->getMagicName(), IPipelineBufferSetFrameControl::magicName()) )) {
        return nullptr;
    }

    return reinterpret_cast<MAIN_CLASS_NAME*>(pPipelineFrame->getMagicInstance());
}


/******************************************************************************
 *
 ******************************************************************************/
MAIN_CLASS_NAME::
MAIN_CLASS_NAME(
    MUINT32 requestNo,
    MUINT32 frameNo,
    MBOOL bReporcessFrame,
    android::wp<IAppCallback>const& pAppCallback,
    IPipelineStreamBufferProvider const* pBufferProvider,
    android::wp<IPipelineNodeCallback> pNodeCallback
)
    : mFrameNo(frameNo)
    , mRequestNo(requestNo)
    , mbReprocessFrame(bReporcessFrame)
    , mRWLock()
    , mpAppCallback(pAppCallback)
    , mListeners()
    //
    , mBufferProvider(const_cast<IPipelineStreamBufferProvider*>(pBufferProvider))
    , mpPipelineCallback(pNodeCallback)
    , mpStreamInfoSet(0)
    , mpNodeMap(0)
    , mpPipelineNodeMap(0)
    , mpPipelineDAG(0)
    //
    , mItemMapLock()
    , mNodeStatusMap()
    , mpReleasedCollector(new ReleasedCollector)
    , mpItemMap_AppImage(new ItemMap_AppImageT(mpReleasedCollector.get()))
    , mpItemMap_AppMeta (new ItemMap_AppMetaT (mpReleasedCollector.get()))
    , mpItemMap_HalImage(new ItemMap_HalImageT(mpReleasedCollector.get()))
    , mpItemMap_HalMeta (new ItemMap_HalMetaT (mpReleasedCollector.get()))
    //
{
    NSCam::Utils::LogTool::get()->getCurrentLogTime(&mTimestampFrameCreated);
    ::memset(&mTimestampFrameDone, 0, sizeof(mTimestampFrameDone));
}


/******************************************************************************
 *
 ******************************************************************************/
void
MAIN_CLASS_NAME::
onLastStrongRef(const void* /*id*/)
{
    //  Force to release all pending buffers.
    {
        HalImageSetT aHalImageSet;
        HalMetaSetT  aHalMetaSet;
        {
            android::Mutex::Autolock _l(mInformationKeeperLock);

            aHalImageSet = mPendingRelease_HalImage.set;
            mPendingRelease_HalImage.set.clear();

            aHalMetaSet = mPendingRelease_HalMeta.set;
            mPendingRelease_HalMeta.set.clear();
        }

        if ( ! aHalImageSet.empty() || ! aHalMetaSet.empty() ) {
            MY_LOGD("[requestNo:%u frameNo:%u] Force to release all pending buffers. "
                    "HalImage:#%zu HalImage:#%zu",
                    getRequestNo(), getFrameNo(), aHalImageSet.size(), aHalMetaSet.size());

            releaseHalImageSet(aHalImageSet);
            releaseHalMetaSet(aHalMetaSet);
        }
    }

    if  (CC_UNLIKELY(
            (0 != mpItemMap_AppImage->mNonReleasedNum)
        ||  (0 != mpItemMap_AppMeta->mNonReleasedNum)
        ||  (0 != mpItemMap_HalImage->mNonReleasedNum)
        ||  (0 != mpItemMap_HalMeta->mNonReleasedNum)
        ))
    {
        MY_LOGW(
            "[requestNo:%u frameNo:%u] buffers are not completely released: #(AppImage, AppMeta, HalImage, HalMeta)=(%zd %zd %zd %zd)",
            getRequestNo(), getFrameNo(),
            mpItemMap_AppImage->mNonReleasedNum,
            mpItemMap_AppMeta->mNonReleasedNum,
            mpItemMap_HalImage->mNonReleasedNum,
            mpItemMap_HalMeta->mNonReleasedNum
        );

        auto printMap = [](android::Printer& printer, auto const& map){
            for (size_t i = 0; i < map.size(); i++) {
                auto const& pItem = map.itemAt(i);
                if (CC_LIKELY( pItem != nullptr && pItem->getUsersManager() )) {
                    pItem->getUsersManager()->dumpState(printer);
                }
            }
        };

        android::LogPrinter logPrinter(LOG_TAG, ANDROID_LOG_WARN, "[onLastStrongRef] ");
        printMap(logPrinter, *mpItemMap_AppMeta);
        printMap(logPrinter, *mpItemMap_HalMeta);
        printMap(logPrinter, *mpItemMap_AppImage);
        printMap(logPrinter, *mpItemMap_HalImage);
    }

    sp<IAppCallback> pAppCallback = mpAppCallback.promote();
    if  (CC_UNLIKELY( pAppCallback == 0 )) {
        MY_LOGW("Cannot promote AppCallback for requestNo:%u frameNo:%u", getRequestNo(), getFrameNo());
    }
    else {
        MY_LOGD("requestNo:%u frameNo:%u frame end", getRequestNo(), getFrameNo());
        IAppCallback::Result result =
        {
            .frameNo         = getFrameNo(),
            .nAppOutMetaLeft = 0,
            .vAppOutMeta     = Vector< sp<IMetaStreamBuffer> >(),
            .nHalOutMetaLeft = 0,
            .vHalOutMeta     = Vector< sp<IMetaStreamBuffer> >(),
            .bFrameEnd       = true,
            .vPhysicalOutMeta = android::KeyedVector<int, android::sp<IMetaStreamBuffer>>(),
            .vPhysicalCameraSetting = std::vector<int32_t>()
        };
        pAppCallback->updateFrame(getRequestNo(), 0, result);
    }
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
attachListener(
    wp<IPipelineFrameListener>const& pListener,
    MVOID* pCookie
)
{
    RWLock::AutoWLock _l(mRWLock);
    //
    mListeners.push_back(MyListener(pListener, pCookie));
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IPipelineNodeMap const>
MAIN_CLASS_NAME::
getPipelineNodeMap() const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    sp<IPipelineNodeMap const> p = mpPipelineNodeMap.promote();
    //
    MY_LOGE_IF(
        p==0,
        "requestNo:%u frameNo:%u Bad PipelineNodeMap: wp:%p promote:%p - %s",
        getRequestNo(), getFrameNo(), mpPipelineNodeMap.unsafe_get(), p.get(),
        getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
    );
    //
    return p;
}


/******************************************************************************
 *
 ******************************************************************************/
IPipelineDAG const&
MAIN_CLASS_NAME::
getPipelineDAG() const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    MY_LOGE_IF(
        mpPipelineDAG==0,
        "requestNo:%u frameNo:%u NULL PipelineDAG - %s",
        getRequestNo(), getFrameNo(),
        getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
    );
    return *mpPipelineDAG;
}


/******************************************************************************
 *
 ******************************************************************************/
android::sp<IPipelineDAG>
MAIN_CLASS_NAME::
getPipelineDAGSp()
{
    RWLock::AutoRLock _l(mRWLock);
    //
    MY_LOGE_IF(
        mpPipelineDAG==0,
        "requestNo:%u frameNo:%u NULL PipelineDAG - %s",
        getRequestNo(), getFrameNo(),
        getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
    );
    return mpPipelineDAG;
}


/******************************************************************************
 *
 ******************************************************************************/
IStreamInfoSet const&
MAIN_CLASS_NAME::
getStreamInfoSet() const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    MY_LOGE_IF(
        mpStreamInfoSet==0,
        "requestNo:%u frameNo:%u NULL StreamInfoSet - %s",
        getRequestNo(), getFrameNo(),
        getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
    );
    return *mpStreamInfoSet;
}


/******************************************************************************
 *
 ******************************************************************************/
IStreamBufferSet&
MAIN_CLASS_NAME::
getStreamBufferSet() const
{
    RWLock::AutoRLock _l(mRWLock);
    return *const_cast<MAIN_CLASS_NAME*>(this);
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IPipelineNodeCallback>
MAIN_CLASS_NAME::
getPipelineNodeCallback() const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    sp<IPipelineNodeCallback> p = mpPipelineCallback.promote();
    //
    MY_LOGE_IF(
        p==0,
        "requestNo:%u frameNo:%u Bad PipelineNodeCallback: wp:%p promote:%p - %s",
        getRequestNo(), getFrameNo(), mpPipelineCallback.unsafe_get(), p.get(),
        getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
    );
    return p;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setNodeMap(
    android::sp<IPipelineFrameNodeMapControl> value
)
{
    if  (CC_UNLIKELY( value == 0)) {
        MY_LOGE("requestNo:%u frameNo:%u - NULL value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    if  (CC_UNLIKELY( value->isEmpty() )) {
        MY_LOGE("requestNo:%u frameNo:%u - Empty value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    RWLock::AutoWLock _l(mRWLock);
    mpNodeMap = value;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setPipelineNodeMap(
    android::sp<IPipelineNodeMap const> value
)
{
    if  (CC_UNLIKELY( value == 0)) {
        MY_LOGE("requestNo:%u frameNo:%u - NULL value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    if  (CC_UNLIKELY( value->isEmpty() )) {
        MY_LOGE("requestNo:%u frameNo:%u - Empty value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    RWLock::AutoWLock _l(mRWLock);
    mpPipelineNodeMap = value;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setPipelineDAG(android::sp<IPipelineDAG> value)
{
    if  (CC_UNLIKELY( value == 0)) {
        MY_LOGE("requestNo:%u frameNo:%u - NULL value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    RWLock::AutoWLock _l(mRWLock);
    mpPipelineDAG = value;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setStreamInfoSet(android::sp<IStreamInfoSet const> value)
{
    if  (CC_UNLIKELY( value == 0)) {
        MY_LOGE("requestNo:%u frameNo:%u - NULL value", getRequestNo(), getFrameNo());
        return BAD_VALUE;
    }
    //
    RWLock::AutoWLock _l(mRWLock);
    mpStreamInfoSet = value;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
auto
MAIN_CLASS_NAME::
configureInformationKeeping(
    bool keepTimestamp,
    bool keepHalImage,
    bool keepHalMeta,
    bool keepAppMeta
) -> void
{
    android::Mutex::Autolock _l(mInformationKeeperLock);

    mPendingRelease_HalImage.isPendingToRelease = keepHalImage;
    mPendingRelease_HalMeta.isPendingToRelease = keepHalMeta;
    mPendingRelease_AppMeta.isPendingToRelease = keepAppMeta || keepTimestamp;

    MY_LOGD_IF((keepTimestamp || keepHalImage || keepHalMeta || keepAppMeta),
        "[requestNo:%u frameNo:%u] pending release configuration: "
        "keepTimestamp:%d keepHalImage:%d keepHalMeta:%d keepAppMeta:%d",
        getRequestNo(), getFrameNo(), keepTimestamp,
        keepHalImage, keepHalMeta, keepAppMeta);
}


/******************************************************************************
 *
 ******************************************************************************/
auto
MAIN_CLASS_NAME::
transferPendingReleaseBuffers(
    android::Vector<android::sp<HalImageStreamBufferT>>& out
) -> void
{
    {
        android::Mutex::Autolock _l(mInformationKeeperLock);
        out = mPendingRelease_HalImage.set;
        mPendingRelease_HalImage.set.clear();
    }

    for (size_t i = 0; i < out.size(); i++) {
        auto const& pStreamBuf = out[i];
        if (CC_LIKELY( pStreamBuf != nullptr )) {
#if 1
            if (CC_UNLIKELY( pStreamBuf->hasStatus(STREAM_BUFFER_STATUS::ERROR) )) {
                MY_LOGW("[requestNo:%u frameNo:%u] error-stauts: %s", getRequestNo(), getFrameNo(), pStreamBuf->toString().c_str());
            }
#endif
            //reset UsersManager before transfering ownership.
            pStreamBuf->reset();
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
MAIN_CLASS_NAME::
transferPendingReleaseBuffers(
    android::Vector<android::sp<HalMetaStreamBufferT>>& out
) -> void
{
    {
        android::Mutex::Autolock _l(mInformationKeeperLock);
        out = mPendingRelease_HalMeta.set;
        mPendingRelease_HalMeta.set.clear();
    }

    for (size_t i = 0; i < out.size(); i++) {
        auto const& pStreamBuf = out[i];
        if (CC_LIKELY( pStreamBuf != nullptr )) {
#if 1
            if (CC_UNLIKELY( pStreamBuf->hasStatus(STREAM_BUFFER_STATUS::ERROR) )) {
                MY_LOGW("[requestNo:%u frameNo:%u] error-stauts: %s", getRequestNo(), getFrameNo(), pStreamBuf->toString().c_str());
            }
#endif
            //reset UsersManager before transfering ownership.
            pStreamBuf->reset();
        }
    }
}


/******************************************************************************
 *
 ******************************************************************************/
auto
MAIN_CLASS_NAME::
tryGetSensorTimestamp() const -> int64_t
{
    android::Mutex::Autolock _l(mInformationKeeperLock);

    if ( 0 != mSensorTimestamp ) {
        return mSensorTimestamp;
    }

    if ( 0 == mSensorTimestamp ) {
        // try to get from App Meta output stream buffers.
        auto const& set = mPendingRelease_AppMeta.set;
        for (size_t i = 0; i < set.size(); i++) {
            auto pStreamBuffer = set[i].get();
            if (CC_LIKELY( pStreamBuffer != nullptr )) {
                bool hit = false;

                auto pMetadata = pStreamBuffer->tryReadLock(LOG_TAG);
                if (CC_LIKELY( pMetadata != nullptr )) {
                    hit = IMetadata::getEntry(pMetadata, MTK_SENSOR_TIMESTAMP, mSensorTimestamp);
                    pStreamBuffer->unlock(LOG_TAG, pMetadata);
                }

                if (hit)
                    break;
            }
        }
    }

    MY_LOGW_IF(0==mSensorTimestamp,
        "[requestNo:%u frameNo:%u] Timestamp=0(not ready?) keepAppMeta:%d",
        getRequestNo(), getFrameNo(), mPendingRelease_AppMeta.isPendingToRelease);

    return mSensorTimestamp;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setPhysicalCameraSetting(
    std::vector<int32_t> const& physicalCameraSetting
)
{
    RWLock::AutoWLock _l(mRWLock);
    mvPhysicalCameraSetting = physicalCameraSetting;
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
queryIOStreamInfoSet(
    NodeId_T const& nodeId,
    sp<IStreamInfoSet const>& rIn,
    sp<IStreamInfoSet const>& rOut
) const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    if  (CC_UNLIKELY( mpNodeMap == 0 )) {
        MY_LOGE("requestNo:%u frameNo:%u NULL node map", getRequestNo(), getFrameNo());
        rIn = 0;
        rOut = 0;
        return NO_INIT;
    }
    //
    sp<IPipelineFrameNodeMapControl::INode> pNode = mpNodeMap->getNodeFor(nodeId);
    if  (CC_UNLIKELY( pNode == 0 )) {
        MY_LOGE("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " not found", getRequestNo(), getFrameNo(), nodeId);
        rIn = 0;
        rOut = 0;
        return NAME_NOT_FOUND;
    }
    //
    rIn = pNode->getIStreams();
    rOut= pNode->getOStreams();
    //
    if  (CC_UNLIKELY( rIn == 0 || rOut == 0 )) {
        MY_LOGE("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " IStreams:%p OStreams:%p", getRequestNo(), getFrameNo(), nodeId, rIn.get(), rOut.get());
        return NO_INIT;
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
queryInfoIOMapSet(
    NodeId_T const& nodeId,
    InfoIOMapSet& rIOMapSet
) const
{
    RWLock::AutoRLock _l(mRWLock);
    //
    if  (CC_UNLIKELY( mpNodeMap == 0 )) {
        MY_LOGE("requestNo:%u frameNo:%u NULL node map", getRequestNo(), getFrameNo());
        return NO_INIT;
    }
    //
    sp<IPipelineFrameNodeMapControl::INode> pNode = mpNodeMap->getNodeFor(nodeId);
    if  (CC_UNLIKELY( pNode == 0 )) {
        MY_LOGE("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " not found", getRequestNo(), getFrameNo(), nodeId);
        return NAME_NOT_FOUND;
    }
    //
    rIOMapSet = pNode->getInfoIOMapSet();
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
startConfiguration()
{
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
finishConfiguration()
{
    RWLock::AutoWLock _lRWLock(mRWLock);
    Mutex::Autolock _lItemMapLock(mItemMapLock);
    //
    if  (CC_UNLIKELY( mpNodeMap == 0 || mpNodeMap->isEmpty() )) {
        MY_LOGE("Empty NodeMap: %p", mpNodeMap.get());
        return NO_INIT;
    }
    //
    if  (CC_UNLIKELY( mpStreamInfoSet == 0 ))
    {
        MY_LOGE("StreamInfoSet:%p", mpStreamInfoSet.get());
        return NO_INIT;
    }
    //
    if  (CC_UNLIKELY( mpPipelineDAG == 0 || mpPipelineNodeMap == 0 ))
    {
        MY_LOGE("PipelineDAG:%p PipelineNodeMap:%p", mpPipelineDAG.get(), mpPipelineNodeMap.unsafe_get());
        return NO_INIT;
    }
    //
    mpReleasedCollector->finishConfiguration(
            *mpItemMap_AppImage,
            *mpItemMap_AppMeta,
            *mpItemMap_HalImage,
            *mpItemMap_HalMeta
            );
    //
    mNodeStatusMap.setCapacity(mpNodeMap->size());
    for (size_t i = 0; i < mpNodeMap->size(); i++)
    {
        sp<NodeStatus> pNodeStatus = new NodeStatus;
        //
        IPipelineFrameNodeMapControl::INode* pNode = mpNodeMap->getNodeAt(i);
        NodeId_T const nodeId = pNode->getNodeId();
        {
            sp<IStreamInfoSet const> pStreams = pNode->getIStreams();
            //I:Meta
            for (size_t j = 0; j < pStreams->getMetaInfoNum(); j++)
            {
                sp<IStreamInfo> pStreamInfo = pStreams->getMetaInfoAt(j);
                StreamId_T const streamId = pStreamInfo->getStreamId();
                //
                sp<NodeStatus::IO> pIO = new NodeStatus::IO;
                pNodeStatus->mISetMeta.push_back(pIO);
                pIO->mMapItem = getMetaMapItemLocked(streamId);
                MY_LOGF_IF(pIO->mMapItem==0, "No I meta item for streamId:%#" PRIx64, streamId);
            }
            //I:Image
            for (size_t j = 0; j < pStreams->getImageInfoNum(); j++)
            {
                sp<IStreamInfo> pStreamInfo = pStreams->getImageInfoAt(j);
                StreamId_T const streamId = pStreamInfo->getStreamId();
                //
                sp<NodeStatus::IO> pIO = new NodeStatus::IO;
                pNodeStatus->mISetImage.push_back(pIO);
                pIO->mMapItem = getImageMapItemLocked(streamId);
                MY_LOGF_IF(pIO->mMapItem==0, "No I image item for streamId:%#" PRIx64, streamId);
            }
        }
        {
            sp<IStreamInfoSet const> pStreams = pNode->getOStreams();
            //O:Meta
            for (size_t j = 0; j < pStreams->getMetaInfoNum(); j++)
            {
                sp<IStreamInfo> pStreamInfo = pStreams->getMetaInfoAt(j);
                StreamId_T const streamId = pStreamInfo->getStreamId();
                //
                sp<NodeStatus::IO> pIO = new NodeStatus::IO;
                pNodeStatus->mOSetMeta.push_back(pIO);
                pIO->mMapItem = getMetaMapItemLocked(streamId);
                MY_LOGF_IF(pIO->mMapItem==0, "No O meta item for streamId:%#" PRIx64, streamId);
            }
            //O:Image
            for (size_t j = 0; j < pStreams->getImageInfoNum(); j++)
            {
                sp<IStreamInfo> pStreamInfo = pStreams->getImageInfoAt(j);
                StreamId_T const streamId = pStreamInfo->getStreamId();
                //
                sp<NodeStatus::IO> pIO = new NodeStatus::IO;
                pNodeStatus->mOSetImage.push_back(pIO);
                pIO->mMapItem = getImageMapItemLocked(streamId);
                MY_LOGF_IF(pIO->mMapItem==0, "No O image item for streamId:%#" PRIx64, streamId);
            }
        }
        //
        if  (CC_LIKELY(
                ! pNodeStatus->mISetMeta.empty()
            ||  ! pNodeStatus->mOSetMeta.empty()
            ||  ! pNodeStatus->mISetImage.empty()
            ||  ! pNodeStatus->mOSetImage.empty()
            ))
        {
            mNodeStatusMap.add(nodeId, pNodeStatus);
            mNodeStatusMap.mInFlightNodeCount++;
            //
            MY_LOG1(
                "nodeId:%#" PRIxPTR " Image:I/O#=%zu/%zu Meta:I/O#=%zu/%zu",
                nodeId,
                pNodeStatus->mISetImage.size(), pNodeStatus->mOSetImage.size(),
                pNodeStatus->mISetMeta.size(), pNodeStatus->mOSetMeta.size()
            );
        }
    }
    //
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IUsersManager>
MAIN_CLASS_NAME::
findSubjectUsersLocked(
    StreamId_T streamId
)   const
{
#define _IMPLEMENT_(_map_) \
    { \
        ssize_t const index = _map_->indexOfKey(streamId); \
        if  ( 0 <= index ) { \
            return _map_->usersManagerAt(index); \
        } \
    }

    _IMPLEMENT_(mpItemMap_AppImage);
    _IMPLEMENT_(mpItemMap_AppMeta);
    _IMPLEMENT_(mpItemMap_HalImage);
    _IMPLEMENT_(mpItemMap_HalMeta);

#undef  _IMPLEMENT_

    MY_LOGW("[requestNo:%u frameNo:%u] streamId:%#" PRIx64 " not found", getRequestNo(), getFrameNo(), streamId);
    return NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
template <class ItemMapT>
sp<typename ItemMapT::IStreamBufferT>
MAIN_CLASS_NAME::
getBufferLockedImp(
    StreamId_T streamId,
    UserId_T userId,
    ItemMapT const& rMap
)   const
{
    if  ( 0 == rMap.mNonReleasedNum ) {
        MY_LOGW_IF(
            1,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] "
            "mNonReleasedNum==0",
            getRequestNo(), getFrameNo(), streamId
        );
        return NULL;
    }
    //
    sp<typename ItemMapT::ItemT> pItem = rMap.getItemFor(streamId);
    if  ( pItem == 0 ) {
        MY_LOGW_IF(
            0,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] "
            "cannot find from map",
            getRequestNo(), getFrameNo(), streamId
        );
        return NULL;
    }
    //
    if  ( ! pItem->mBitStatus.hasBit(eBUF_STATUS_ACQUIRE) ) {
        if  (CC_UNLIKELY( pItem->mBitStatus.hasBit(eBUF_STATUS_ACQUIRE_FAILED) )) {
            pItem->mUsersManager->markUserStatus(userId, IUsersManager::UserStatus::RELEASE);
            MY_LOGW(
                "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] Failure in previous acquiring buffer",
                getRequestNo(), getFrameNo(), streamId
                );
            return NULL;
        }
        MY_LOGF_IF(pItem->mBuffer!=0, "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] Non-null buffer but non-acquired status:%#x", getRequestNo(), getFrameNo(), streamId, pItem->mBitStatus.value);
        sp<IPipelineStreamBufferProvider> pBufferProvider = mBufferProvider.promote();
        if  (CC_UNLIKELY( pBufferProvider == 0 )) {
            MY_LOGE(
                "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] Fail to promote buffer provider:%p",
                getRequestNo(), getFrameNo(), streamId, mBufferProvider.unsafe_get()
            );
            return NULL;
        }
        //
        struct Helper
        {
            struct Params
                            {
                MUINT32 const requestNo;
                UserId_T userId;
                IPipelineStreamBufferProvider* pBufferProvider;
            };
            static  MERROR  acquireStreamBuffer(Params& rParams, ItemMap_HalImageT::MapValueT pItem)
                            {
                                MERROR err = rParams.pBufferProvider->acquireHalStreamBuffer(
                                        rParams.requestNo,
                                        pItem->mStreamInfo,
                                        pItem->mBuffer
                                        );
                                if  ( OK == err && pItem->mBuffer != 0 ) {
                                    pItem->mBuffer->setUsersManager(pItem->mUsersManager);
                                    pItem->mBitStatus.markBit(eBUF_STATUS_ACQUIRE);
                                }
                                else {
                                    pItem->mBitStatus.markBit(eBUF_STATUS_ACQUIRE_FAILED);
                                    pItem->mUsersManager->markUserStatus(
                                            rParams.userId,
                                            IUsersManager::UserStatus::RELEASE
                                            );
                                }
                                return err;
                            }

            static  MERROR  acquireStreamBuffer(Params&, ItemMap_HalMetaT::MapValueT)    { return INVALID_OPERATION; }
            static  MERROR  acquireStreamBuffer(Params&, ItemMap_AppMetaT::MapValueT)    { return INVALID_OPERATION; }
            static  MERROR  acquireStreamBuffer(Params&, ItemMap_AppImageT::MapValueT)   { return INVALID_OPERATION; }
        };
        CAM_TRACE_FMT_BEGIN("acquireStreamBuffer sID%#" PRIx64 ,streamId);
        NSCam::Utils::CamProfile profile(__FUNCTION__, "acquireStreamBuffer");
        typename Helper::Params param = { getFrameNo(), userId, pBufferProvider.get() };
        MERROR err = Helper::acquireStreamBuffer(param, pItem);
        profile.print_overtime(10, "[requestNo:%u frameNo:%u streamId:%#" PRIxPTR "]", getRequestNo(), getFrameNo(), streamId);
        CAM_TRACE_FMT_END();
        if  ( OK != err || pItem->mBuffer == 0 ) {
            #if 0
            MY_LOGE(
                "[requestNo:%u frameNo:%u streamId:%#" PRIxPTR "] mBuffer:%p err:%d(%s)",
                getRequestNo(), getFrameNo(), streamId, pItem->mBuffer.get(), err, ::strerror(-err)
            );
            #endif
            pItem->mBuffer = NULL;
            return NULL;
        }
    }
    //
    if  (CC_UNLIKELY( pItem->mBuffer == 0 )) {
        MY_LOGW(
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] "
            "mBitStatus(%#x) pValue->mBuffer == 0",
            getRequestNo(), getFrameNo(), streamId, pItem->mBitStatus.value
        );
        return NULL;
    }
    //
    return pItem->mBuffer;
}


/******************************************************************************
 *
 ******************************************************************************/
template <class ItemMapT>
sp<typename ItemMapT::IStreamBufferT>
MAIN_CLASS_NAME::
getBufferLocked(
    StreamId_T streamId,
    UserId_T userId,
    ItemMapT const& rMap
)   const
{
    sp<typename ItemMapT::IStreamBufferT>
    pBuffer = getBufferLockedImp(streamId, userId, rMap);
    //
    if  ( pBuffer == 0 ) {
        MY_LOGW_IF(
            0,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 " userId:%#" PRIxPTR "] NULL buffer",
            getRequestNo(), getFrameNo(), streamId, userId
        );
        return NULL;
    }

    /**
     * The buffer is NOT available if all users have released this buffer
     * (so as to be marked as released).
     */
    if  (CC_UNLIKELY( OK == pBuffer->haveAllUsersReleased() )) {
        MY_LOGW_IF(
            1,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 " userId:%#" PRIxPTR "] "
            "all users released this buffer",
            getRequestNo(), getFrameNo(), streamId, userId
        );
        return NULL;
    }

    /**
     * For a specific stream buffer (associated with a stream Id), a user (with
     * a unique user Id) could successfully acquire the buffer from this buffer
     * set only if all users ahead of this user have pre-released or released
     * the buffer.
     */
    if  (CC_UNLIKELY( OK != pBuffer->haveAllUsersReleasedOrPreReleased(userId) )) {
        MY_LOGW_IF(
            1,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 " userId:%#" PRIxPTR "] "
            "not all of prior users release or pre-release this buffer",
            getRequestNo(), getFrameNo(), streamId, userId
        );
        return NULL;
    }

    return pBuffer;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IMetaStreamBuffer>
MAIN_CLASS_NAME::
getMetaBuffer(StreamId_T streamId, UserId_T userId) const
{
    sp<IMetaStreamBuffer> p;
    //
    Mutex::Autolock _lItemMapLock(mItemMapLock);
    //
    p = getBufferLocked(streamId, userId, *mpItemMap_HalMeta);
    if  ( p != 0 ) {
        return p;
    }
    //
    p = getBufferLocked(streamId, userId, *mpItemMap_AppMeta);
    if  ( p != 0 ) {
        return p;
    }
    //
    return NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IImageStreamBuffer>
MAIN_CLASS_NAME::
getImageBuffer(StreamId_T streamId, UserId_T userId) const
{
    sp<IImageStreamBuffer> p;
    //
    Mutex::Autolock _lItemMapLock(mItemMapLock);
    //
    p = getBufferLocked(streamId, userId, *mpItemMap_HalImage);
    if  ( p != 0 ) {
        return p;
    }
    //
    p = getBufferLocked(streamId, userId, *mpItemMap_AppImage);
    if  ( p != 0 ) {
        return p;
    }
    //
    return NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT32
MAIN_CLASS_NAME::
markUserStatus(
    StreamId_T const streamId,
    UserId_T userId,
    MUINT32 eStatus
)
{
    android::Mutex::Autolock _l(mItemMapLock);
    //
    sp<IUsersManager> pSubjectUsers = findSubjectUsersLocked(streamId);
    if  (CC_UNLIKELY( pSubjectUsers == 0 )) {
        return NAME_NOT_FOUND;
    }
    //
    return pSubjectUsers->markUserStatus(userId, eStatus);
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
MAIN_CLASS_NAME::
setUserReleaseFence(
    StreamId_T const streamId,
    UserId_T userId,
    MINT releaseFence
)
{
    android::Mutex::Autolock _l(mItemMapLock);
    //
    sp<IUsersManager> pSubjectUsers = findSubjectUsersLocked(streamId);
    if  (CC_UNLIKELY( pSubjectUsers == 0 )) {
        return NAME_NOT_FOUND;
    }
    //
    return pSubjectUsers->setUserReleaseFence(userId, releaseFence);
}


/******************************************************************************
 *
 ******************************************************************************/
MUINT
MAIN_CLASS_NAME::
queryGroupUsage(
    StreamId_T const streamId,
    UserId_T userId
)   const
{
    android::Mutex::Autolock _l(mItemMapLock);
    //
    sp<IUsersManager> pSubjectUsers = findSubjectUsersLocked(streamId);
    if  (CC_UNLIKELY( pSubjectUsers == 0 )) {
        return 0;
    }
    //
    return pSubjectUsers->queryGroupUsage(userId);
}


/******************************************************************************
 *
 ******************************************************************************/
MINT
MAIN_CLASS_NAME::
createAcquireFence(
    StreamId_T const streamId,
    UserId_T userId
)   const
{
    android::Mutex::Autolock _l(mItemMapLock);
    //
    sp<IUsersManager> pSubjectUsers = findSubjectUsersLocked(streamId);
    if  (CC_UNLIKELY( pSubjectUsers == 0 )) {
        return -1;
    }
    //
    return pSubjectUsers->createAcquireFence(userId);
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IMyMap::IItem>
MAIN_CLASS_NAME::
getMapItemLocked(
    StreamId_T streamId,
    IMyMap const& rItemMap
)   const
{
    android::sp<IMyMap::IItem>const& pItem = rItemMap.itemFor(streamId);
    if  ( pItem == 0 ) {
        MY_LOGW_IF(
            0,
            "[requestNo:%u frameNo:%u streamId:%#" PRIx64 "] "
            "cannot find from map",
            getRequestNo(), getFrameNo(), streamId
        );
        return NULL;
    }
    return pItem;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IMyMap::IItem>
MAIN_CLASS_NAME::
getMetaMapItemLocked(StreamId_T streamId) const
{
    sp<IMyMap::IItem> p;
    //
    p = getMapItemLocked(streamId, *mpItemMap_HalMeta);
    if  ( p != 0 ) {
        return p;
    }
    //
    p = getMapItemLocked(streamId, *mpItemMap_AppMeta);
    if  ( p != 0 ) {
        return p;
    }
    //
    return NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
sp<IMyMap::IItem>
MAIN_CLASS_NAME::
getImageMapItemLocked(StreamId_T streamId) const
{
    sp<IMyMap::IItem> p;
    //
    p = getMapItemLocked(streamId, *mpItemMap_HalImage);
    if  ( p != 0 ) {
        return p;
    }
    //
    p = getMapItemLocked(streamId, *mpItemMap_AppImage);
    if  ( p != 0 ) {
        return p;
    }
    //
    return NULL;
}


/******************************************************************************
 *
 ******************************************************************************/
struct MAIN_CLASS_NAME::NodeStatusUpdater
{
public:     ////                    Definitions.
    typedef NodeStatus::IOSet       IOSet;

public:     ////                    Data Members.
    MUINT32 const                   mFrameNo;

public:
    NodeStatusUpdater(MUINT32 frameNo)
        : mFrameNo(frameNo)
    {
    }

    MBOOL
    run(
        NodeId_T const nodeId,
        NodeStatusMap& rNodeStatusMap,
        android::BitSet32& rNodeStatusUpdated
    )
    {
        MBOOL isAnyUpdate = MFALSE;
        //
        ssize_t const index = rNodeStatusMap.indexOfKey(nodeId);
        if  (CC_UNLIKELY( index < 0 )) {
            MY_LOGE("frameNo:%u nodeId:%#" PRIxPTR " not found", mFrameNo, nodeId);
            return MFALSE;
        }
        //
        sp<NodeStatus> pNodeStatus = rNodeStatusMap.valueAt(index);
        if  (CC_UNLIKELY( pNodeStatus == 0 )) {
            MY_LOGE("frameNo:%u nodeId:%#" PRIxPTR " NULL buffer", mFrameNo, nodeId);
            return MFALSE;
        }
        //
        // O Image
        if  ( updateNodeStatus(nodeId, pNodeStatus->mOSetImage) ) {
            isAnyUpdate = MTRUE;
            rNodeStatusUpdated.markBit(IPipelineFrameListener::eMSG_ALL_OUT_IMAGE_BUFFERS_RELEASED);
            MY_LOG2("frameNo:%u nodeId:%#" PRIxPTR " O Image Buffers Released", mFrameNo, nodeId);
        }
        // I Image
        if  ( updateNodeStatus(nodeId, pNodeStatus->mISetImage) ) {
            isAnyUpdate = MTRUE;
            MY_LOG2("frameNo:%u nodeId:%#" PRIxPTR " I Image Buffers Released", mFrameNo, nodeId);
        }
        // O Meta
        if  ( updateNodeStatus(nodeId, pNodeStatus->mOSetMeta) ) {
            isAnyUpdate = MTRUE;
            rNodeStatusUpdated.markBit(IPipelineFrameListener::eMSG_ALL_OUT_META_BUFFERS_RELEASED);
            MY_LOG2("frameNo:%u nodeId:%#" PRIxPTR " O Meta Buffers Released", mFrameNo, nodeId);
        }
        // I Meta
        if  ( updateNodeStatus(nodeId, pNodeStatus->mISetMeta) ) {
            isAnyUpdate = MTRUE;
            MY_LOG2("frameNo:%u nodeId:%#" PRIxPTR " I Meta Buffers Released", mFrameNo, nodeId);
        }

        //
        // Is it a new node with all buffers released?
        if  (
                isAnyUpdate
            &&  pNodeStatus->mOSetImage.empty()
            &&  pNodeStatus->mISetImage.empty()
            &&  pNodeStatus->mOSetMeta.empty()
            &&  pNodeStatus->mISetMeta.empty()
            )
        {
            rNodeStatusMap.mInFlightNodeCount--;
        }
        //
        return isAnyUpdate;
    }

protected:

    MBOOL
    updateNodeStatus(NodeId_T const nodeId, IOSet& rIOSet)
    {
        if  ( rIOSet.mNotified ) {
            return MFALSE;
        }
        //
        IOSet::iterator it = rIOSet.begin();
        for (; it != rIOSet.end();) {
            sp<IMyMap::IItem> pMapItem = (*it)->mMapItem;
            //
            if ( ! pMapItem->getUsersManager()->isActive() ) {
                /**
                 * Remove if it's not active.
                 * We can remove this item from this user since an inactive item
                 * must be associated with no users.
                 *
                 * It happens if this item has been reset before (since all users have released it).
                 */
                it = rIOSet.erase(it);
                continue;
            }
            //
            MY_LOG1("haveAllProducerUsersReleased(%d) haveAllProducerUsersReleasedOrPreReleased(%d)",
            (OK == pMapItem->getUsersManager()->haveAllProducerUsersReleased()),
            (OK == pMapItem->getUsersManager()->haveAllProducerUsersReleasedOrPreReleased()));
            if  ( (OK == pMapItem->getUsersManager()->haveAllProducerUsersReleased()) ||
                  (OK == pMapItem->getUsersManager()->haveAllProducerUsersReleasedOrPreReleased())) {
                pMapItem->handleProducersReleased();
            }
            //
            //  Check to see if this user "nodeId" has released.
            MUINT32 const status = pMapItem->getUsersManager()->getUserStatus(nodeId);
            MY_LOG1("nodeId(%x) status(%d) release(%d) UserStatus::RELEASE(%d) pre_release(%d) UserStatus::PRE_RELEASE(%d)",
            nodeId,
            status,
            IUsersManager::UserStatus::RELEASE,
            ((status & IUsersManager::UserStatus::RELEASE)),
            IUsersManager::UserStatus::PRE_RELEASE,
            ((status & IUsersManager::UserStatus::PRE_RELEASE)));
            if  ( (status & IUsersManager::UserStatus::RELEASE) ||
                  (status & IUsersManager::UserStatus::PRE_RELEASE)) {
                //
                it = rIOSet.erase(it);   //remove if released
                //
                if  ( (OK == pMapItem->getUsersManager()->haveAllUsersReleased()) ||
                      (OK == pMapItem->getUsersManager()->haveAllUsersReleasedOrPreReleased())) {
                    pMapItem->handleAllUsersReleased();
                }
            }
            else
            {
                ++it;
                continue;
            }
        }
        //
        if  ( rIOSet.empty() ) {
            rIOSet.mNotified = MTRUE;
            return MTRUE;
        }
        //
        return MFALSE;
    }

}; // end struct MAIN_CLASS_NAME::NodeStatusUpdater


/******************************************************************************
 *
 ******************************************************************************/
MVOID
MAIN_CLASS_NAME::
handleReleasedBuffers(UserId_T userId, sp<IAppCallback> pAppCallback)
{
    HalImageSetT aHalImageSet;
    HalMetaSetT  aHalMetaSet;
    AppMetaSetT  aAppMetaSetO;
    AppMetaSetT  aHalMetaSetO; // note: use AppMetaSetT in purpose.
    ssize_t      aAppMetaNumO;
    ssize_t      aHalMetaNumO;
    {
        android::Mutex::Autolock _l(mpReleasedCollector->mLock);
        //
        aHalImageSet = mpReleasedCollector->mHalImageSet_AllUsersReleased;
        mpReleasedCollector->mHalImageSet_AllUsersReleased.clear();
        aHalMetaSet = mpReleasedCollector->mHalMetaSet_AllUsersReleased;
        mpReleasedCollector->mHalMetaSet_AllUsersReleased.clear();
        //
        aAppMetaSetO = mpReleasedCollector->mAppMetaSetO_ProducersReleased;
        mpReleasedCollector->mAppMetaSetO_ProducersReleased.clear();
        aAppMetaNumO = mpReleasedCollector->mAppMetaNumO_ProducersInFlight;
        //
        aHalMetaSetO = mpReleasedCollector->mHalMetaSetO_ProducersReleased;
        mpReleasedCollector->mHalMetaSetO_ProducersReleased.clear();
        aHalMetaNumO = mpReleasedCollector->mHalMetaNumO_ProducersInFlight;
    }
    //
    //  Callback to App.
    {
        if  (CC_UNLIKELY( pAppCallback == 0 )) {
            MY_LOGW("Cannot promote AppCallback for requestNo:%u frameNo:%u, userId:%#" PRIxPTR, getRequestNo(), getFrameNo(), userId);
        }
        else {
            MY_LOG1("requestNo:%u frameNo:%u userId:%#" PRIxPTR " OAppMeta#(left:%zd this:%zu)", getRequestNo(), getFrameNo(), userId, aAppMetaNumO, aAppMetaSetO.size());
            IAppCallback::Result result =
            {
                getFrameNo(),
                aAppMetaNumO,
                aAppMetaSetO,
                aHalMetaNumO,
                aHalMetaSetO,
                false,
                android::KeyedVector<int, android::sp<IMetaStreamBuffer>>(),
                mvPhysicalCameraSetting
            };
            pAppCallback->updateFrame(getRequestNo(), userId, result);
        }

        if ( mPendingRelease_AppMeta.isPendingToRelease ) {
            android::Mutex::Autolock _l(mInformationKeeperLock);
            mPendingRelease_AppMeta.set.appendVector(aAppMetaSetO);
        }

        aAppMetaSetO.clear();
        aHalMetaSetO.clear();
    }
    //
    //  Release to Hal? Right now or pending?
    if ( mPendingRelease_HalImage.isPendingToRelease ) {
        android::Mutex::Autolock _l(mInformationKeeperLock);
        mPendingRelease_HalImage.set.appendVector(aHalImageSet);
    }
    else {
        releaseHalImageSet(aHalImageSet);
    }

    if ( mPendingRelease_HalMeta.isPendingToRelease ) {
        android::Mutex::Autolock _l(mInformationKeeperLock);
        mPendingRelease_HalMeta.set.appendVector(aHalMetaSet);
    }
    else {
        releaseHalMetaSet(aHalMetaSet);
    }

}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
MAIN_CLASS_NAME::
applyRelease(UserId_T userId)
{
    NodeId_T const nodeId = userId;
    sp<IAppCallback> pAppCallback;
    List<MyListener> listeners;
    BitSet32 nodeStatusUpdated;
    NodeStatusUpdater updater(getFrameNo());
    //
    MY_LOG1("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " +", getRequestNo(), getFrameNo(), nodeId);
    //
    {
        RWLock::AutoWLock _lRWLock(mRWLock);
        Mutex::Autolock _lMapLock(mItemMapLock);
        //
        //  Update
        MBOOL isAnyUpdate = updater.run(nodeId, mNodeStatusMap, nodeStatusUpdated);
        //
        // Is the entire frame released?
        if  ( isAnyUpdate && 0 == mNodeStatusMap.mInFlightNodeCount )
        {
            nodeStatusUpdated.markBit(IPipelineFrameListener::eMSG_FRAME_RELEASED);
            //
            NSCam::Utils::LogTool::get()->getCurrentLogTime(&mTimestampFrameDone);
            //
#if 1
//          mpPipelineNodeMap = 0;
//          mpPipelineDAG = 0;
            mpStreamInfoSet = 0;
#endif
            MY_LOG1(
                "Done requestNo:%u frameNo:%u @ nodeId:%#" PRIxPTR " - %s",
                getRequestNo(), getFrameNo(), nodeId,
                getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
            );
            // [Bg service] send to PreRleaseRequestMgr to notify this request is end to AP.            //check frame has jpegNode
            ssize_t const index = mNodeStatusMap.indexOfKey(eNODEID_JpegNode);
            if  (CC_UNLIKELY( index >= 0 )) {
                IPreReleaseRequestMgr::getInstance()->applyRelease(this);
            }

        }
        //
        if  ( ! nodeStatusUpdated.isEmpty() ) {
            listeners = mListeners;
        }
        //
        pAppCallback = mpAppCallback.promote();
    }
    //
    //
    handleReleasedBuffers(userId, pAppCallback);
    //
    //  Callback to listeners if needed.
    if  ( ! nodeStatusUpdated.isEmpty() )
    {
        NSCam::Utils::CamProfile profile(__FUNCTION__, "IPipelineBufferSetFrameControl");
        //
        List<MyListener>::iterator it = listeners.begin();
        for (; it != listeners.end(); ++it) {
            sp<MyListener::IListener> p = it->mpListener.promote();
            if  (CC_UNLIKELY( p == 0 )) {
                continue;
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_ALL_OUT_META_BUFFERS_RELEASED) ) {
                MY_LOG2("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " O Meta Buffers Released", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    nodeId,
                    IPipelineFrameListener::eMSG_ALL_OUT_META_BUFFERS_RELEASED,
                    it->mpCookie
                );
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_ALL_OUT_IMAGE_BUFFERS_RELEASED) ) {
                MY_LOG2("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " O Image Buffers Released", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    nodeId,
                    IPipelineFrameListener::eMSG_ALL_OUT_IMAGE_BUFFERS_RELEASED,
                    it->mpCookie
                );
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_FRAME_RELEASED) ) {
                MY_LOG2("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " Frame Done", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    IPipelineFrameListener::eMSG_FRAME_RELEASED,
                    it->mpCookie
                );
            }
        }
        //
        profile.print_overtime(3, "notify listeners (nodeStatusUpdated:%#x)", nodeStatusUpdated.value);
    }
    //
    MY_LOG1("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " -", getRequestNo(), getFrameNo(), nodeId);
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
MAIN_CLASS_NAME::
applyPreRelease(UserId_T userId)
{
    NodeId_T const nodeId = userId;
    sp<IAppCallback> pAppCallback;
    List<MyListener> listeners;
    BitSet32 nodeStatusUpdated;
    NodeStatusUpdater updater(getFrameNo());
    MY_LOGD("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " +", getRequestNo(), getFrameNo(), nodeId);

    // [Bg service] send to PreRleaseRequestMgr to notify this request is end to AP.
    // call applyPreRelease directly, this function will check double entry case.
    // applyPreRelease should be trigged before
    IPreReleaseRequestMgr::getInstance()->applyPreRelease(this);
    {
        RWLock::AutoWLock _lRWLock(mRWLock);
        Mutex::Autolock _lMapLock(mItemMapLock);
        //
        //  Update
        MBOOL isAnyUpdate = updater.run(nodeId, mNodeStatusMap, nodeStatusUpdated);
        MY_LOGD("isAnyUpdate:%d mInFlightNodeCount:%d", isAnyUpdate, mNodeStatusMap.mInFlightNodeCount);

        if  ( isAnyUpdate && 0 == mNodeStatusMap.mInFlightNodeCount )
        {
            nodeStatusUpdated.markBit(IPipelineFrameListener::eMSG_FRAME_RELEASED);
            //
            NSCam::Utils::LogTool::get()->getCurrentLogTime(&mTimestampFrameDone);
            //
            MY_LOG1(
                "Done requestNo:%u frameNo:%u @ nodeId:%#" PRIxPTR " - %s",
                getRequestNo(), getFrameNo(), nodeId,
                getFrameLifetimeLog(mTimestampFrameCreated, mTimestampFrameDone).c_str()
            );
        }

        //
        if  ( ! nodeStatusUpdated.isEmpty() ) {
            listeners = mListeners;
        }
        //
        pAppCallback = mpAppCallback.promote();
    }

    HalImageSetT aHalImageSet;
    HalMetaSetT  aHalMetaSet;
    AppMetaSetT  aAppMetaSetO;
    AppMetaSetT  aHalMetaSetO; // note: use AppMetaSetT in purpose.
    ssize_t      aAppMetaNumO;
    ssize_t      aHalMetaNumO;
    {
        android::Mutex::Autolock _l(mpReleasedCollector->mLock);
        //
        aHalImageSet = mpReleasedCollector->mHalImageSet_AllUsersReleased;
        mpReleasedCollector->mHalImageSet_AllUsersReleased.clear();
        aHalMetaSet = mpReleasedCollector->mHalMetaSet_AllUsersReleased;
        mpReleasedCollector->mHalMetaSet_AllUsersReleased.clear();
        //
        aAppMetaSetO = mpReleasedCollector->mAppMetaSetO_ProducersReleased;
        mpReleasedCollector->mAppMetaSetO_ProducersReleased.clear();
        aAppMetaNumO = mpReleasedCollector->mAppMetaNumO_ProducersInFlight;
        //
        aHalMetaSetO = mpReleasedCollector->mHalMetaSetO_ProducersReleased;
        mpReleasedCollector->mHalMetaSetO_ProducersReleased.clear();
        aHalMetaNumO = mpReleasedCollector->mHalMetaNumO_ProducersInFlight;
    }
    //  Callback to App.
    {
        if  (CC_UNLIKELY( pAppCallback == 0 )) {
            MY_LOGW("Cannot promote AppCallback for requestNo:%u frameNo:%u, userId:%#" PRIxPTR, getRequestNo(), getFrameNo(), userId);
        }
        else {
            MY_LOG1("requestNo:%u frameNo:%u userId:%#" PRIxPTR " OAppMeta#(left:%zd this:%zu)", getRequestNo(), getFrameNo(), userId, aAppMetaNumO, aAppMetaSetO.size());
            IAppCallback::Result result =
            {
                getFrameNo(),
                aAppMetaNumO,
                aAppMetaSetO,
                aHalMetaNumO,
                aHalMetaSetO,
                false,
                android::KeyedVector<int, android::sp<IMetaStreamBuffer>>(),
                mvPhysicalCameraSetting
            };
            pAppCallback->updateFrame(getRequestNo(), userId, result);
        }
        aAppMetaSetO.clear();
        aHalMetaSetO.clear();
    }
    //  Callback to listeners if needed.
    if  ( ! nodeStatusUpdated.isEmpty() )
    {
        NSCam::Utils::CamProfile profile(__FUNCTION__, "IPipelineBufferSetFrameControl");
        //
        List<MyListener>::iterator it = listeners.begin();
        for (; it != listeners.end(); ++it) {
            sp<MyListener::IListener> p = it->mpListener.promote();
            if  (CC_UNLIKELY( p == 0 )) {
                continue;
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_ALL_OUT_META_BUFFERS_RELEASED) ) {
                MY_LOG1("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " O Meta Buffers Released", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    nodeId,
                    IPipelineFrameListener::eMSG_ALL_OUT_META_BUFFERS_RELEASED,
                    it->mpCookie
                );
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_ALL_OUT_IMAGE_BUFFERS_RELEASED) ) {
                MY_LOG1("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " O Image Buffers Released", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    nodeId,
                    IPipelineFrameListener::eMSG_ALL_OUT_IMAGE_BUFFERS_RELEASED,
                    it->mpCookie
                );
            }
            //
            if  ( nodeStatusUpdated.hasBit(IPipelineFrameListener::eMSG_FRAME_RELEASED) ) {
                MY_LOG1("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " Frame Done", getRequestNo(), getFrameNo(), nodeId);
                p->onPipelineFrame(
                    getFrameNo(),
                    IPipelineFrameListener::eMSG_FRAME_RELEASED,
                    it->mpCookie
                );
                // [Bg service] send to PreRleaseRequestMgr to notify this request is end to AP.
                // auto pPreReleaseRequestMgr = IPreReleaseRequestMgr::getInstance();
                // if(pPreReleaseRequestMgr)
                // {
                //     pPreReleaseRequestMgr->applyPreRelease(this);
                // }
                // [Bg service] end
            }
        }
        //
        profile.print_overtime(3, "notify listeners (nodeStatusUpdated:%#x)", nodeStatusUpdated.value);
    }
    //
    MY_LOGD("requestNo:%u frameNo:%u nodeId:%#" PRIxPTR " - ", getRequestNo(), getFrameNo(), nodeId);
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
MAIN_CLASS_NAME::
logDebugInfo(std::string&& str)
{
    DebugLog x;
    x.str = std::move(str);
    NSCam::Utils::LogTool::get()->getCurrentLogTime(&x.timestamp);

    RWLock::AutoWLock _l(mDebugLogListRWLock);
    mDebugLogList.push_back(std::move(x));
}


/******************************************************************************
 *
 ******************************************************************************/
MVOID
MAIN_CLASS_NAME::
dumpState(android::Printer& printer __unused, const std::vector<std::string>& options __unused) const
{
    auto pLogTool = NSCam::Utils::LogTool::get();
    {
        /**
         *  frame:2208(r2446) 08-03 05:49:16.659 -> 08-03 05:49:16.700 (33ms) reprocess
         */

        android::String8 os;

        os += android::String8::format("frame:%u(r%u) ", mFrameNo, mRequestNo);

        if ( pLogTool ) {

            os += pLogTool->convertToFormattedLogTime(&mTimestampFrameCreated).c_str();

            if (mRWLock.tryReadLock() == OK) {
                if ( 0 != mTimestampFrameDone.tv_sec || 0 != mTimestampFrameDone.tv_nsec ) {
                    os += " -> ";
                    os += pLogTool->convertToFormattedLogTime(&mTimestampFrameDone).c_str();
                    os += " (";
                    os += std::to_string(getDurationInUS(mTimestampFrameCreated, mTimestampFrameDone)).c_str();
                    os += "us)";
                }
                mRWLock.unlock();
            }
        }

        {
            android::Mutex::Autolock _l(mInformationKeeperLock);
            if (0 != mSensorTimestamp) {
                os += android::String8::format(" shutter:%" PRIu64 "", mSensorTimestamp);
            }
        }

        if (mbReprocessFrame) {
            os += " reprocess";
        }

        printer.printLine(os.c_str());
    }
    const std::string prefix{"    "};
    android::PrefixPrinter prefixPrinter(printer, prefix.c_str());
    {
        /**
         * .root={ 0x1 } .edges={ (0x1 -> 0x4) (0x1 -> 0x5) (0x4 -> 0x13) (0x5 -> 0x6) }
         */
        android::String8 os;

        auto pPipelineDAG = mpPipelineDAG;
        android::Vector<IPipelineDAG::NodeObj_T> roots = pPipelineDAG->getRootNode();
        if ( roots.size() ) {
            os += ".root={ ";
            for (size_t i = 0; i < roots.size(); i++) {
                auto const& v = roots[i];
                os += android::String8::format("%#" PRIxPTR " ", v.id);
            }
            os += "}";
        }
        android::Vector<IPipelineDAG::Edge> edges;
        if ( OK == pPipelineDAG->getEdges(edges) ) {
            os += " .edges={ ";
            for (size_t i = 0; i < edges.size(); i++) {
                auto const& v = edges[i];
                os += android::String8::format("(%#" PRIxPTR " -> %#" PRIxPTR ") ", v.src, v.dst);
            }
            os += "}";
        }

        prefixPrinter.printLine(os.c_str());
    }
    {
        /**
         * .iomap(image)
         *   <nodeId 0x1>={ ( -> 0x100000001 0x100000002 0x100000003 ) }
         *   <nodeId 0x4>={ ( 0x100000001 0x100000002 0x100000003 -> 0x2 0x100000009 0x10000000a ) }
         *   <nodeId 0x5>={ ( 0x100000002 0x100000003 -> 0x10000000b ) }
         *   <nodeId 0x6>={ ( 0x10000000b -> ) }
         *   <nodeId 0x13>={ ( 0x100000009 0x10000000a -> 0x1 ) }
         * .iomap(meta)
         *   <nodeId 0x1>={ ( 0x80000000 0x10000000d -> 0x10000000f 0x100000015 ) }
         *   <nodeId 0x4>={ ( 0x80000000 0x10000000f 0x100000015 -> 0x100000012 0x100000018 ) }
         *   <nodeId 0x5>={ ( 0x80000000 0x10000000f 0x100000015 -> 0x100000011 ) }
         *   <nodeId 0x6>={ ( 0x80000000 0x100000011 -> 0x100000019 ) }
         *   <nodeId 0x13>={ ( 0x80000000 0x100000012 -> 0x10000001a ) }
         */
        auto pNodeMap = mpNodeMap;
        std::vector<std::string> os1; os1.push_back(".iomap(image)");
        std::vector<std::string> os2; os2.push_back(".iomap(meta)");
        for (size_t i = 0; i < pNodeMap->size(); i++) {
            if ( auto pNode = pNodeMap->getNodeAt(i) ) {
                auto nodeId = pNode->getNodeId();
                auto const& infoIomapSet = pNode->getInfoIOMapSet();
                if ( infoIomapSet.mImageInfoIOMapSet.size() ) {
                    std::ostringstream oss;
                    oss << "  <nodeId 0x" << std::setbase(16) << nodeId << ">="
                        << toString(infoIomapSet.mImageInfoIOMapSet);
                    os1.push_back(oss.str());
                }
                if ( infoIomapSet.mMetaInfoIOMapSet.size() ) {
                    std::ostringstream oss;
                    oss << "  <nodeId 0x" << std::setbase(16) << nodeId << ">="
                        << toString(infoIomapSet.mMetaInfoIOMapSet);
                    os2.push_back(oss.str());
                }
            }
        }
        for (auto const& v : os1) { prefixPrinter.printLine(v.c_str()); }
        for (auto const& v : os2) { prefixPrinter.printLine(v.c_str()); }
    }
    {
        /**
         * { 09-04 13:30:19.190 (X) -> 0x1 }
         * { 09-04 13:30:19.192 0x1 -> 0x4 }
         * { 09-04 13:30:19.201 0x1 -> 0x5 }
         * { 09-04 13:30:19.252 0x5 -> 0x6 }
         * { 09-04 13:30:19.260 0x6 -> (X) }
         */
        RWLock::AutoRLock _l(mDebugLogListRWLock);
        for (auto&& v : mDebugLogList) {
            std::string os;
            os += "{ ";
            if ( pLogTool ) {
                os += pLogTool->convertToFormattedLogTime(&v.timestamp);
                os += " ";
            }
            os += v.str;
            os += " }";
            prefixPrinter.printLine(os.c_str());
        }
    }

    if (mItemMapLock.timedLock(100000000 /* 100ms */) == OK) {

        auto printMap = [](android::Printer& printer, auto const& map){
            for (size_t i = 0; i < map.size(); i++) {
                auto const& pItem = map.itemAt(i);
                if (CC_LIKELY( pItem != nullptr && pItem->getUsersManager() )) {
                    pItem->getUsersManager()->dumpState(printer);
                }
            }
        };

        printMap(prefixPrinter, *mpItemMap_AppMeta);
        printMap(prefixPrinter, *mpItemMap_HalMeta);
        printMap(prefixPrinter, *mpItemMap_AppImage);
        printMap(prefixPrinter, *mpItemMap_HalImage);

        mItemMapLock.unlock();
    }

    // Pending Release
    {
        auto printSet = [](android::Printer& printer, auto const& set){
            for (size_t i = 0; i < set.size(); i++) {
                auto const& pStreamBuf = set[i];
                if (CC_LIKELY( pStreamBuf != nullptr )) {
                    printer.printFormatLine("    %s", pStreamBuf->toString().c_str());
                }
            }
        };

        android::Mutex::Autolock _l(mInformationKeeperLock);
        if ( ! mPendingRelease_HalImage.set.empty()
          || ! mPendingRelease_HalMeta.set.empty() )
        {
            prefixPrinter.printLine("Pending Release");
            printSet(prefixPrinter, mPendingRelease_HalImage.set);
            printSet(prefixPrinter, mPendingRelease_HalMeta.set);
        }
    }
}

