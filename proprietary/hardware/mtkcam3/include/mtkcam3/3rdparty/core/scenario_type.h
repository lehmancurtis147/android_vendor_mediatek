/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_3RDPARTY_SCENARIOTYPE_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_3RDPARTY_SCENARIOTYPE_H_
//
#include <mtkcam/def/common.h>
#include <mtkcam3/3rdparty/mtk/mtk_scenario_type.h>
#include <mtkcam3/3rdparty/mtk/mtk_feature_type.h>
#include <mtkcam3/3rdparty/customer/customer_scenario_type.h>
#include <mtkcam3/3rdparty/customer/customer_feature_type.h>
//
#include <vector>
#include <string>

// add feature set by camera scenario
#define CAMERA_SCENARIO_START(SCENARIO_NAME)                              \
{                                                                         \
    SCENARIO_NAME,                                                        \
    {                                                                     \
        #SCENARIO_NAME,                                                   \
        {

#define ADD_CAMERA_FEATURE_SET(FEATURE, FEATURE_COMBINATION)              \
        { #FEATURE, #FEATURE_COMBINATION, FEATURE, FEATURE_COMBINATION },

#define CAMERA_SCENARIO_END                                               \
        }                                                                 \
    }                                                                     \
},

/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace scenariomgr {

enum DualFeatureMode
{
    DualFeatureMode_NONE = 0,
    DualFeatureMode_YUV,
    DualFeatureMode_HW_DEPTH,
    DualFeatureMode_MTK_VSDOF,
};

struct ScenarioHint
{
    // pipeline session operation mode
    uint32_t operationMode = 0;

    bool    isDualCam = false;
    DualFeatureMode mDualFeatureMode = DualFeatureMode_NONE;

    // hint for capture scenario
    bool    isYuvReprocess = false;
    bool    isRawReprocess = false;
    bool    isCShot     = false;
    bool    isVSDoFMode = false;
    int32_t captureScenarioIndex = -1; // force to get indicated scenario by vendor tag

    // hint for streaming scenario
    // TODO: add others hint to choose streaming scenario
    int32_t streamingScenarioIndex = -1; // force to get indicated scenario by vendor tag
};

struct CaptureScenarioConfig
{
    // buffer count
    uint32_t maxAppRaw16OutputBufferNum = 1;
    uint32_t maxAppJpegStreamNum        = 1;
};

struct FeatureSet
{
    std::string featureName;            // for debug
    std::string featureCombinationName; // for debug
    MUINT64 feature = 0;
    MUINT64 featureCombination = 0;
};

struct ScenarioFeatures
{
    std::string scenarioName; // for debug
    std::vector<FeatureSet> vFeatureSet;
};

};  //namespace scenariomgr
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_3RDPARTY_SCENARIOTYPE_H_

