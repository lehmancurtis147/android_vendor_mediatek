/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_PRERELEASEREQUEST_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_PRERELEASEREQUEST_H_
//
#include <utils/RefBase.h>
#include <utils/Errors.h>
#include <mtkcam/def/common.h>
#include <mtkcam/utils/fwk/MtkCamera.h>
#include <mtkcam3/pipeline/pipeline/IPipelineNode.h>
#include <mtkcam3/pipeline/pipeline/PipelineContext.h>

using NSCam::v3::IPipelineFrame;
using NSCam::v3::NSPipelineContext::PipelineContext;
/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace prerelease {

class IPreReleaseRequest
    : public android::RefBase
{
public:
    virtual void                         start() = 0;
    virtual void                         uninit() = 0;
    virtual void                         waitUntilDrained() = 0;
    /* pipelineframe will call this API in ApplyPreRelease */
    virtual void                         applyPreRelease(
                                             android::sp<IPipelineFrame>const& pFrame
                                         ) = 0;
};

class IPreReleaseRequestMgr
    : public android::RefBase
{
public:
    static IPreReleaseRequestMgr*           getInstance();
    virtual android::sp<IPreReleaseRequest>
                                            createPreRelease(android::sp<PipelineContext> pContext)   = 0;
    virtual bool                            applyRelease(android::sp<IPipelineFrame>const& pFrame) = 0;
    virtual bool                            applyPreRelease(android::sp<IPipelineFrame>const& pFrame) = 0;
    virtual void                            setImageReaderID(uint32_t id) = 0;
    virtual void                            setPreReleaseRequest(android::sp<IPreReleaseRequest> pPreRelease) = 0;
    virtual void                            setRequestFinished(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID);
    virtual void                            addRequest(uint32_t frameNo, uint32_t preReleaseUid, int32_t imgReaderID);
    virtual void                            uninit() = 0;
};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace prerelease
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_PRERELEASEREQUEST_H_

