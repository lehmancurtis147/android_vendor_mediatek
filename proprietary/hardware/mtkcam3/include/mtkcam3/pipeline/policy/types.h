/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_TYPES_H_
#define _MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_TYPES_H_
//
#include <mtkcam3/pipeline/stream/IStreamBuffer.h>
#include <mtkcam3/pipeline/pipeline/PipelineContext.h>
#include <mtkcam/def/common.h>  // for mtkcam3/feature/eis/EisInfo.h
#include <mtkcam3/feature/eis/EisInfo.h>
//
#include <map>
#include <memory>
#include <unordered_map>
#include <vector>


/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {

////////////////////////////////////////////////////////////////////////////////

/**
 *  Pipeline static information.
 *
 *  The following information is static and unchanged forever, regardless of
 *  any operation (e.g. open or configure).
 */
struct PipelineStaticInfo
{
    /**
     *  Logical device open id
     */
    int32_t                                     openId = -1;

    /**
     *  Physical sensor id (0, 1, 2)
     */
    std::vector<int32_t>                        sensorId;

    /**
     *  Sensor raw type.
     *
     *  SENSOR_RAW_xxx in mtkcam/include/mtkcam/drv/IHalSensor.h
     */
    std::vector<uint32_t>                       sensorRawType;

    /**
     *
     */
    bool                                        isDualDevice = false;

    /**
     *  Type3 PD sensor without PD hardware (ISP3.0)
     */
    bool                                        isType3PDSensorWithoutPDE = false;

    /**
     *  is 4-Cell sensor
     */
    bool                                        is4CellSensor = false;

    /**
     *  is VHDR sensor
     */
    bool                                        isVhdrSensor = false;
    /**
     *  is P1 direct output FD YUV (ISP6.0)
     */
    bool                                        isP1DirectFDYUV = false;

    /**
     *  is support burst capture or not
     */
    bool                                        isSupportBurstCap = false;

};


struct ParsedAppConfiguration;
struct ParsedAppImageStreamInfo;
struct ParsedDualCamInfo;

/**
 *  Pipeline user configuration
 *
 *  The following information is given and set up at the configuration stage, and
 *  is never changed AFTER the configuration stage.
 */
struct PipelineUserConfiguration
{
    std::shared_ptr<ParsedAppConfiguration>     pParsedAppConfiguration;

    /**
     * Parsed App image stream info set
     *
     * It results from the raw data, i.e. vImageStreams.
     */
    std::shared_ptr<ParsedAppImageStreamInfo>   pParsedAppImageStreamInfo;

    /**************************************************************************
     * App image stream info set (raw data)
     **************************************************************************/

    /**
     * App image streams to configure.
     */
    std::unordered_map<StreamId_T, android::sp<IImageStreamInfo>>
                                                vImageStreams;

    /**
     * App meta streams to configure.
     */
    std::unordered_map<StreamId_T, android::sp<IMetaStreamInfo>>
                                                vMetaStreams;

    /**
     * App image streams min frame duration to configure.
     */
    std::unordered_map<StreamId_T, int64_t>     vMinFrameDuration;

    /**
     * App image streams stall frame duration to configure.
     */
    std::unordered_map<StreamId_T, int64_t>     vStallFrameDuration;

   /**
     * @param[in] physical camera id list
     */
    std::vector<int32_t>                        vPhysicCameras;

};

////////////////////////////////////////////////////////////////////////////////


/**
 * P1 DMA bitmask definitions
 *
 * Used in the following structures:
 *      IPipelineSettingPolicy.h: RequestResultParams::needP1Dma
 *      IIOMapPolicy.h: RequestInputParams::pRequest_NeedP1Dma
 *      IStreamInfoConfigurationPolicy.h: FunctionType_StreamInfoConfiguration_P1
 *
 */
enum : uint32_t
{
    P1_IMGO     = (0x01U << 0),
    P1_RRZO     = (0x01U << 1),
    P1_LCSO     = (0x01U << 2),
    P1_RSSO     = (0x01U << 3),
    // for ISP 60
    P1_FDYUV    = (0x01U << 4),
    P1_FULLYUV  = (0x01U << 5),
    P1_DEPTHYUV = (0x01U << 6),
    //
    P1_MASK     = 0x0F,
    P1_ISP6_0_MASK = 0xFF,
};


/**
 * Reconfig category enum definitions
 * For pipelineModelSession processReconfiguration use
 */
enum class ReCfgCtg: uint8_t
{
    NO          = 0,
    STREAMING,
    CAPTURE,
    NUM
};

/**
 *  Sensor Setting
 */
struct SensorSetting
{
    uint32_t                                    sensorMode = 0;
    uint32_t                                    sensorFps = 0;
    MSize                                       sensorSize;
};


static inline android::String8 toString(const SensorSetting& o __unused)
{
    android::String8 os = android::String8::format("{ .sensorMode=%d .sensorFps=%d .sensorSize=%dx%d }", o.sensorMode, o.sensorFps, o.sensorSize.w, o.sensorSize.h);
    return os;
};


/**
 *  DMA settings
 */
struct DmaSetting
{
    /**
     * Image format.
     */
    int32_t                                     format = 0;

    /**
     * Image resolution in pixel.
     */
    MSize                                       imageSize;

};


static inline android::String8 toString(const DmaSetting& o __unused)
{
    android::String8 os;
    os += "{";
    os += android::String8::format(" format:%#x", o.format);
    os += android::String8::format(" %dx%d", o.imageSize.w, o.imageSize.h);
    os += " }";
    return os;
};


/**
 *  Pass1-specific HW settings
 */
struct P1HwSetting
{
    /**
     * @param imgoConfig
     *        It is used for configuring P1Node. If we need to runtime change
     *        this dma setting, a worst setting (e.g. eImgFmt_BAYER10_UNPAK)
     *        must be set when configuring P1Node.
     *
     * @param imgoDefaultRequest
     *        It is used for default (streaming) request.
     */
    DmaSetting                                  imgoConfig;
    DmaSetting                                  imgoDefaultRequest;

    /**
     * @param rrzoDefaultRequest
     *        It is used for default (streaming) request.
     */
    DmaSetting                                  rrzoDefaultRequest;

    MSize                                       rssoSize;

    uint32_t                                    pixelMode = 0;
    bool                                        usingCamSV = false;
    // ISP6.0
    MSize                                       fdyuvSize;
};


static inline android::String8 toString(const P1HwSetting& o __unused)
{
    android::String8 os;
    os += "{";
    os += " .imgoConfig=";
    os += toString(o.imgoConfig);
    os += " .imgoDefaultRequest=";
    os += toString(o.imgoDefaultRequest);
    os += " .rrzoDefaultRequest=";
    os += toString(o.rrzoDefaultRequest);
    os += android::String8::format(" .rssoSize=%dx%d", o.rssoSize.w, o.rssoSize.h);
    os += android::String8::format(" .pixelMode=%u .usingCamSV=%d", o.pixelMode, o.usingCamSV);
    os += " }";
    return os;
};


/**
 *  Parsed metadata control request
 */
struct ParsedMetaControl
{
    bool                                        repeating = false;

    int32_t                                     control_aeTargetFpsRange[2]     = {0};//CONTROL_AE_TARGET_FPS_RANGE
    uint8_t                                     control_captureIntent           = static_cast< uint8_t>(-1L);//CONTROL_CAPTURE_INTENT
    uint8_t                                     control_enableZsl               = static_cast< uint8_t>(0);//CONTROL_ENABLE_ZSL
    uint8_t                                     control_mode                    = static_cast< uint8_t>(-1L);//CONTROL_MODE
    uint8_t                                     control_sceneMode               = static_cast< uint8_t>(-1L);//CONTROL_SCENE_MODE
    uint8_t                                     control_videoStabilizationMode  = static_cast< uint8_t>(-1L);//CONTROL_VIDEO_STABILIZATION_MODE
    MINT32                                      control_remosaicEn              = 0;//MTK_CONTROL_CAPTURE_REMOSAIC_EN

};


static inline android::String8 toString(const ParsedMetaControl& o __unused)
{
    android::String8 os;
    os += "{";
    os += android::String8::format(" repeating:%d", o.repeating);
    os += android::String8::format(" control.aeTargetFpsRange:%d,%d", o.control_aeTargetFpsRange[0], o.control_aeTargetFpsRange[1]);
    os += android::String8::format(" control.captureIntent:%d", o.control_captureIntent);
    os += android::String8::format(" control.enableZsl:%d", o.control_enableZsl);
    if ( static_cast< uint8_t>(-1L) != o.control_mode ) {
        os += android::String8::format(" control.mode:%d", o.control_mode);
    }
    if ( static_cast< uint8_t>(-1L) != o.control_sceneMode ) {
        os += android::String8::format(" control.sceneMode:%d", o.control_sceneMode);
    }
    if ( static_cast< uint8_t>(-1L) != o.control_videoStabilizationMode ) {
        os += android::String8::format(" control.videoStabilizationMode:%d", o.control_videoStabilizationMode);
    }
    os += " }";
    return os;
};


/**
 *  Parsed App configuration
 */
struct ParsedAppConfiguration
{
    /**
     * The operation mode of pipeline.
     * The caller must promise its value.
     */
    uint32_t                                    operationMode = 0;

    /**
     * Session wide camera parameters.
     *
     * The session parameters contain the initial values of any request keys that were
     * made available via ANDROID_REQUEST_AVAILABLE_SESSION_KEYS. The Hal implementation
     * can advertise any settings that can potentially introduce unexpected delays when
     * their value changes during active process requests. Typical examples are
     * parameters that trigger time-consuming HW re-configurations or internal camera
     * pipeline updates. The field is optional, clients can choose to ignore it and avoid
     * including any initial settings. If parameters are present, then hal must examine
     * their values and configure the internal camera pipeline accordingly.
     */
    IMetadata                                   sessionParams;

    /**
     * operationMode = 1
     *
     * StreamConfigurationMode::CONSTRAINED_HIGH_SPEED_MODE = 1
     * Refer to https://developer.android.com/reference/android/hardware/camera2/params/SessionConfiguration#SESSION_HIGH_SPEED
     */
    bool                                        isConstrainedHighSpeedMode = false;

    /**
     * operationMode: "persist.vendor.mtkcam.operationMode.superNightMode"
     *
     * Super night mode.
     */
    bool                                        isSuperNightMode = false;

    /**
     * Dual cam related info.
     */
    std::shared_ptr<ParsedDualCamInfo>          pParsedDualCamInfo;

};


/**
 *  Parsed App image stream info
 */
struct ParsedAppImageStreamInfo
{
    /**************************************************************************
     *  App image stream info set
     **************************************************************************/

    /**
     * Output streams for any processed (but not-stalling) formats
     *
     * Reference:
     * https://developer.android.com/reference/android/hardware/camera2/CameraCharacteristics.html#REQUEST_MAX_NUM_OUTPUT_PROC
     */
    std::unordered_map<StreamId_T, android::sp<IImageStreamInfo>>
                                                vAppImage_Output_Proc;

    /**
     * Input stream for yuv reprocessing
     */
    android::sp<IImageStreamInfo>               pAppImage_Input_Yuv;

    /**
     * Output stream for private reprocessing
     */
    android::sp<IImageStreamInfo>               pAppImage_Output_Priv;

    /**
     * Input stream for private reprocessing
     */
    android::sp<IImageStreamInfo>               pAppImage_Input_Priv;

    /**
     * Output stream for RAW16/DNG capture.
     */
    android::sp<IImageStreamInfo>               pAppImage_Output_RAW16;

    /**
     * Input stream for RAW16 reprocessing.
     */
    android::sp<IImageStreamInfo>               pAppImage_Input_RAW16;

    /**
     * Output stream for JPEG capture.
     */
    android::sp<IImageStreamInfo>               pAppImage_Jpeg;


    /**************************************************************************
     *  Parsed info
     **************************************************************************/

    /**
     * One of consumer usages of App image streams contains BufferUsage::VIDEO_ENCODER.
     */
    bool                                        hasVideoConsumer = false;

    /**
     * 4K video recording
     */
    bool                                        hasVideo4K = false;

    /**
     * The image size of video recording, in pixels.
     */
    MSize                                       videoImageSize;

    /**
     * The image size of app yuv out, in pixels.
     */
    MSize                                       maxYuvSize;

    /**
     * The max. image size of App image streams, in pixels, regardless of stream formats.
     */
    MSize                                       maxImageSize;

};

enum class DualDevicePath{
    /* Using dual camera device, but it does not set feature mode in session parameter or
     * set camera id to specific stream.
     */
    Single,
    /* Using dual camera device, and set "MTK_MULTI_CAM_FEATURE_MODE" to session parameter.
     */
    Feature,
    /* Using dual camera device, set physical camera id to specific stream.
     */
    MultiCamControl,
};

struct ParsedDualCamInfo
{
    /**
     * Dual device pipeline path.
     */
    DualDevicePath                              mDualDevicePath = DualDevicePath::Single;

    /**
     * dual cam feature which get from session param.
     */
    MINT32                                      mDualFeatureMode = -1;
};


/**
 *  (Non Pass1-specific) Parsed stream info
 */
struct ParsedStreamInfo_NonP1
{
    /******************************************
     *  app meta stream info
     ******************************************/
    android::sp<IMetaStreamInfo>                pAppMeta_Control;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicP2StreamNode;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicP2StreamNode_Main1;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicP2StreamNode_Main2;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicP2CaptureNode;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicFD;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicJpeg;
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicRAW16;


    /******************************************
     *  hal meta stream info
     ******************************************/
    android::sp<IMetaStreamInfo>                pHalMeta_DynamicP2StreamNode;
    android::sp<IMetaStreamInfo>                pHalMeta_DynamicP2CaptureNode;
    android::sp<IMetaStreamInfo>                pHalMeta_DynamicPDE;


    /******************************************
     *  hal image stream info
     ******************************************/

    /**
     *  Face detection.
     */
    android::sp<IImageStreamInfo>               pHalImage_FD_YUV = nullptr;

    /**
     *  The Jpeg orientation is passed to HAL at the request stage.
     *  Maybe we can create a stream set for every orientation at the configuration stage, but only
     *  one within that stream set can be passed to the configuration of pipeline context.
     */
    android::sp<IImageStreamInfo>               pHalImage_Jpeg_YUV;

    /**
     *  The thumbnail size is passed to HAL at the request stage.
     */
    android::sp<IImageStreamInfo>               pHalImage_Thumbnail_YUV;

};


/**
 *  (Pass1-specific) Parsed stream info
 */
struct ParsedStreamInfo_P1
{
    /******************************************
     *  app meta stream info
     ******************************************/
    /**
     *  Only one of P1Node can output this data.
     *  Why do we need more than one of this stream?
     */
    android::sp<IMetaStreamInfo>                pAppMeta_DynamicP1;


    /******************************************
     *  hal meta stream info
     ******************************************/
    android::sp<IMetaStreamInfo>                pHalMeta_Control;
    android::sp<IMetaStreamInfo>                pHalMeta_DynamicP1;


    /******************************************
     *  hal image stream info
     ******************************************/
    android::sp<IImageStreamInfo>               pHalImage_P1_Imgo;
    android::sp<IImageStreamInfo>               pHalImage_P1_Rrzo;
    android::sp<IImageStreamInfo>               pHalImage_P1_Lcso;
    android::sp<IImageStreamInfo>               pHalImage_P1_Rsso;
    /******************************************
     *  hal image stream info for ISP6.0
     ******************************************/
    android::sp<IImageStreamInfo>               pHalImage_P1_FDYuv = nullptr;
};


/**
 *  Pipeline nodes need.
 *  true indicates its corresponding pipeline node is needed.
 */
struct PipelineNodesNeed
{
    /**
     * [Note]
     * The index is shared, for example:
     *      needP1Node[index]
     *      PipelineStaticInfo::sensorId[index]
     */
    std::vector<bool>                           needP1Node;

    bool                                        needP2StreamNode = false;
    bool                                        needP2CaptureNode = false;

    bool                                        needFDNode = false;
    bool                                        needJpegNode = false;
    bool                                        needRaw16Node = false;
    bool                                        needPDENode = false;
};


static inline android::String8 toString(const PipelineNodesNeed& o __unused)
{
    android::String8 os;
    os += "{ ";
    for (size_t i = 0; i < o.needP1Node.size(); i++) {
        if ( o.needP1Node[i] )  { os += android::String8::format("P1Node[%zu] ", i); }
    }
    if ( o.needP2StreamNode )   { os += "P2StreamNode "; }
    if ( o.needP2CaptureNode )  { os += "P2CaptureNode "; }
    if ( o.needFDNode )         { os += "FDNode "; }
    if ( o.needJpegNode )       { os += "JpegNode "; }
    if ( o.needRaw16Node )      { os += "Raw16Node "; }
    if ( o.needPDENode )        { os += "PDENode "; }
    os += "}";
    return os;
};


/**
 *  Pipeline topology
 */
struct PipelineTopology
{
    using NodeSet       = NSCam::v3::NSPipelineContext::NodeSet;
    using NodeEdgeSet   = NSCam::v3::NSPipelineContext::NodeEdgeSet;

    /**
     * The root nodes of a pipeline.
     */
    NodeSet                                     roots;

    /**
     * The edges to connect pipeline nodes.
     */
    NodeEdgeSet                                 edges;

};


static inline android::String8 toString(const PipelineTopology& o __unused)
{
    android::String8 os;
    os += "{ ";
    os += ".root={";
    for( size_t i = 0; i < o.roots.size(); i++ ) {
        os += android::String8::format(" %#" PRIxPTR " ", o.roots[i]);
    }
    os += "}";
    os += ", .edges={";
    for( size_t i = 0; i < o.edges.size(); i++ ) {
        os += android::String8::format("(%#" PRIxPTR " -> %#" PRIxPTR ")",
            o.edges[i].src, o.edges[i].dst);
    }
    os += "}";
    return os;
};


/**
 * Streaming feature settings
 */
struct StreamingFeatureSetting
{
    struct AppInfo
    {
        int32_t recordState = -1;
        uint32_t appMode = 0;
        uint32_t eisOn = 0;
    };

    AppInfo                                     mLastAppInfo;
    /**
     * The vhdr mode is decided to enable or not at configuration stage.
     * SENSOR_VHDR_MODE_xxx defined in include/mtkcam/drv/IHalSensor.h
     *      SENSOR_VHDR_MODE_NONE = 0x0,
     *      SENSOR_VHDR_MODE_IVHDR = 0x1,
     *      SENSOR_VHDR_MODE_MVHDR = 0x2,
     *      SENSOR_VHDR_MODE_ZVHDR = 0x9,
     */
    uint32_t                                    vhdrMode = 0;

    uint32_t                                    nr3dMode = 0;
    uint32_t                                    fscMode = 0;
    bool                                        bNeedLMV = false;
    bool                                        bNeedRSS = false;
    bool                                        bIsEIS   = false;
    NSCam::EIS::EisInfo                         eisInfo;
    uint32_t                                    eisExtraBufNum = 0;
    uint32_t                                    minRrzoEisW    = 0;
    bool                                        bEnableTSQ     = false;
    uint32_t                                    BWCScenario = -1;
    uint32_t                                    BWCFeatureFlag = 0;
    //
    // hint support feature for dedicated scenario for P2 node init
    int64_t                                     supportedScenarioFeatures = 0; /*eFeatureIndexMtk and eFeatureIndexCustomer*/
    // CZ/DRE
    bool                                        bSupportCZ     = false;
    bool                                        bSupportDRE     = false;
    // ISP6.0
    bool                                        bNeedP1FDYUV = false;
};


/**
 * capture feature settings
 */
struct CaptureFeatureSetting
{
    /**
     * The max. buffer number of App RAW16 output image stream.
     *
     * App RAW16 output image stream could be used for RAW16 capture or reprocessing.
     *
     * This value is usually used for
     * ParsedAppImageStreamInfo::pAppImage_Output_RAW16::setMaxBufNum.
     */
    uint32_t                                    maxAppRaw16OutputBufferNum = 1;

    /**
     */
    uint32_t                                    maxAppJpegStreamNum     = 1;
    uint32_t                                    maxZslBufferNum         = 0;
    // uint32_t                                    maxMainYUVStreamNum     = 1;
    // uint32_t                                    maxThumbYUVStreamNum    = 1;
    //
    // hint support feature for dedicated scenario for P2 node init
    int64_t                                     supportedScenarioFeatures = 0; /*eFeatureIndexMtk and eFeatureIndexCustomer*/
};

/**
 * boost scenario control
 */
struct BoostControl
{
    int32_t                                    boostScenario = -1;
    uint32_t                                    featureFlag   = 0;
};

/**
 * ZSL policy
 */
enum eZslPolicy
{
    // bit 0~15:    preserved for image quality. select from metadata.
    // bitwise operation. the history buffer result must fullfile all requirements.
    eZslPolicy_None                 = 0x0,
    eZslPolicy_AfState              = 0x1 << 0,
    eZslPolicy_AeState              = 0x1 << 1,
    eZslPolicy_DualFrameSync        = 0x1 << 2,
    eZslPolicy_PD_ProcessedRaw      = 0x1 << 3,

    // bit 16~27:   preserved for zsl behavior.
    eZslPolicy_Behavior_MASK        = 0x0FFF0000,
    eZslPolicy_ContinuousFrame      = ( 0x1 << 0 ) << 16,
    eZslPolicy_ZeroShutterDelay     = ( 0x1 << 1 ) << 16,
    //
};

#define ZslBehaviorOf(PolicyType)   (PolicyType & eZslPolicy_Behavior_MASK)

struct ZslPolicyParams
{
    int32_t                                             mPolicy = eZslPolicy_None; /*eZslPolicy*/
    //
    int64_t                                             mTimestamp = -1;
    int64_t                                             mTimeouts = 2000;
};

/******************************************************************************
 *
 ******************************************************************************/
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam
#endif  //_MTK_HARDWARE_MTKCAM_INCLUDE_MTKCAM_PIPELINE_POLICY_TYPES_H_

