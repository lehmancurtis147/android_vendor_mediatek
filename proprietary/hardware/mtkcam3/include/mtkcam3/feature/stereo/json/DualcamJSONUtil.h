/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

//Notice:
// You must add "LOCAL_CPPFLAGS += -fexceptions" to your Android.mk for exception handling
#ifndef DUALCAM_JSON_UTIL_H_
#define DUALCAM_JSON_UTIL_H_

#include "json.hpp"
#include <fstream>
#include <sys/stat.h>
#include <unistd.h>
#include <mtkcam/utils/std/Log.h>
#include <camera_custom_stereo.h>
#include <camera_custom_logicaldevice.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wignored-qualifiers"
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include <mtkcam/drv/IHalSensor.h>
#pragma GCC diagnostic pop
#include <mtkcam3/feature/stereo/hal/stereo_setting_keys.h>

using json = nlohmann::json;

#define SETTING_FILE_PATH   "/sdcard/stereo_setting.json"
#define TUNING_FILE_PATH    "/sdcard/stereo_tuning.json"

#define SENSOR_DRVNAME_PREFIX std::string("SENSOR_DRVNAME_")

namespace StereoHAL {

enum ENUM_JSON_PARSE_STATUS
{
    E_JSON_PARSE_FAILED        = 0,
    E_JSON_PARSE_SUCCEED       = 1,
    E_JSON_PARSE_FROM_DEFAULT  = 1<<1,
    E_JSON_PARSE_FROM_EXTERNAL = 1<<2,
};

#define IS_JSON_FROM_EXTERNAL(status) (status & E_JSON_PARSE_FROM_EXTERNAL)
#define IS_JSON_FROM_DEFAULT(status) (status & E_JSON_PARSE_FROM_DEFAULT)

class DualcamJSONUtil
{
public:
    static bool HasMember(const json &obj, const char *key)
    {
        return (obj.find(key) != obj.end());
    }

    static bool HasMember(json &obj, const char *key)
    {
        return (obj.find(key) != obj.end());
    }

    static bool HasMember(const json &obj, std::string &key)
    {
        return (obj.find(key) != obj.end());
    }

    static bool HasMember(json &obj, std::string &key)
    {
        return (obj.find(key) != obj.end());
    }

    static int parseCustomSetting(json &output)
    {
        //Init
        int result = E_JSON_PARSE_SUCCEED;
        struct stat st;
        bool useCustomSetting = true;
        if(0 == stat(SETTING_FILE_PATH, &st)) {
            std::ifstream fin(SETTING_FILE_PATH);
            if(fin) {
                try
                {
                    output = json::parse(fin, nullptr, false);
                    result = E_JSON_PARSE_SUCCEED|E_JSON_PARSE_FROM_EXTERNAL;
                    useCustomSetting = false;
                }
                catch (json::exception& e)
                {
                    ALOGW("Parse error: %s", e.what());
                }

                if(!output.is_discarded()) {
                    useCustomSetting = false;
                }
            }

            if(useCustomSetting) {
                ALOGW("Cannot load setting from file, use default setting instead");
            }
        }

        if(useCustomSetting) {
            ALOGD("Parse DEFAULT_STEREO_SETTING in camera_custom_stereo_setting.h");
            try
            {
                output = json::parse(DEFAULT_STEREO_SETTING);
                result = E_JSON_PARSE_SUCCEED|E_JSON_PARSE_FROM_DEFAULT;
            }
            catch (json::exception& e)
            {
                ALOGW("Parse error: %s", e.what());
                result = E_JSON_PARSE_FAILED;
            }

            if(output.is_discarded()) {
                result = E_JSON_PARSE_FAILED;
            }
        }

        return result;
    }

    static int parseCustomTuning(json &output)
    {
        //Init
        int result = E_JSON_PARSE_SUCCEED;
        struct stat st;
        bool useCustomTuning = true;
        if(0 == stat(TUNING_FILE_PATH, &st)) {
            std::ifstream fin(TUNING_FILE_PATH);
            if(fin) {
                try
                {
                    output = json::parse(fin, nullptr, false);
                    result = E_JSON_PARSE_SUCCEED|E_JSON_PARSE_FROM_EXTERNAL;
                    useCustomTuning = false;
                }
                catch (json::parse_error& e)
                {
                    ALOGW("Parse error: %s", e.what());
                }

                if(!output.is_discarded()) {
                    useCustomTuning = false;
                }
            }

            if(useCustomTuning) {
                ALOGW("Cannot load tuning from file, use default tuning instead");
            }
        }

        if(useCustomTuning) {
            ALOGD("Parse DEFAULT_STEREO_TUNING in camera_custom_stereo_tuning.h");
            try
            {
                output = json::parse(DEFAULT_STEREO_TUNING);
                result = E_JSON_PARSE_SUCCEED|E_JSON_PARSE_FROM_DEFAULT;
            }
            catch (json::parse_error& e)
            {
                ALOGW("Parse error: %s", e.what());
                result = E_JSON_PARSE_FAILED;
            }

            if(output.is_discarded()) {
                result = E_JSON_PARSE_FAILED;
            }
        }

        return result;
    }

    static std::vector<LogicalSensorStruct> getLogicalDevices(void)
    {
        std::vector<struct LogicalSensorStruct> gCustomDevList;
        NSCam::IHalSensorList *pSensorList = MAKE_HalSensorList();
        if (NULL == pSensorList) {
            ALOGE("Cannot get sensor list");
            return gCustomDevList;
        }

        std::map<std::string, int> sensorNames;
        int32_t sensorCount = pSensorList->queryNumberOfSensors();
        for(int index = 0; index < sensorCount; ++index) {
            sensorNames[pSensorList->queryDriverName(index)] = index;
        }

        gCustomDevList.clear();

        json jsonObj;
        DualcamJSONUtil::parseCustomSetting(jsonObj);

        std::string name;
        for(auto &settingValue : jsonObj[CUSTOM_KEY_SENSOR_COMBINATIONS]) {
            if(DualcamJSONUtil::HasMember(settingValue, CUSTOM_KEY_LOGICAL_DEVICE)) {
                const json &logcalDeviceJson = settingValue[CUSTOM_KEY_LOGICAL_DEVICE];
                LogicalSensorStruct logicalDevice;
                std::string name = logcalDeviceJson[CUSTOM_KEY_NAME].get<std::string>();

                ::memset(logicalDevice.Name, 0, sizeof(logicalDevice.Name));
                strncpy(logicalDevice.Name, name.c_str(), std::min(sizeof(logicalDevice.Name), name.length()));
                ALOGD("Name: %s", logicalDevice.Name);

                logicalDevice.NumofCombinSensor = 0;
                logicalDevice.NumofDefinition   = 1;
                for(auto &sensor : settingValue[CUSTOM_KEY_SENSORS])
                {
                    name = sensor.get<std::string>();
                    if(std::string::npos == name.find(SENSOR_DRVNAME_PREFIX)) {
                        name = SENSOR_DRVNAME_PREFIX + name;
                    }

                    if(sensorNames.find(name) != sensorNames.end()) {
                        logicalDevice.Sensorlist.push_back(name);
                        ALOGD("Sensor %d: %s", logicalDevice.NumofCombinSensor, logicalDevice.Sensorlist[logicalDevice.NumofCombinSensor].c_str());
                        logicalDevice.NumofCombinSensor++;
                    } else {
                        ALOGD("Sensor %s not found in sensor list", sensor.get<std::string>().c_str());
                        break;
                    }
                }

                if(0 == settingValue[CUSTOM_KEY_SENSORS].size() ||
                   (size_t)logicalDevice.NumofCombinSensor < settingValue[CUSTOM_KEY_SENSORS].size())
                {
                    ALOGD("NumofCombinSensor %d != sensors in setting %zu", logicalDevice.NumofCombinSensor, settingValue[CUSTOM_KEY_SENSORS].size());
                    continue;
                }

                ALOGD("NumofCombinSensor %d", logicalDevice.NumofCombinSensor);

                logicalDevice.Feature = 0;
                for(auto &feature : logcalDeviceJson[CUSTOM_KEY_FEATURES])
                {
                    std::string name = feature.get<std::string>();
                    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

                    if(name == CUSTOM_KEY_VSDOF)
                    {
                        logicalDevice.Feature |= DEVICE_FEATURE_VSDOF;
                        ALOGD("%s", CUSTOM_KEY_VSDOF);
                    }
                    else if(name == CUSTOM_KEY_ZOOM)
                    {
                        logicalDevice.Feature |= DEVICE_FEATURE_ZOOM;
                        ALOGD("%s", CUSTOM_KEY_ZOOM);
                    }
                    else if(name == CUSTOM_KEY_DENOISE)
                    {
                        logicalDevice.Feature |= DEVICE_FEATURE_DENOISE;
                        ALOGD("%s", CUSTOM_KEY_DENOISE);
                    }
                    else if(name == CUSTOM_KEY_3RD_Party)
                    {
                        logicalDevice.Feature |= DEVICE_FEATURE_VSDOF;//DEVICE_FEATURE_3RD_PARTY;
                        ALOGD("%s(%s)", CUSTOM_KEY_VSDOF, CUSTOM_KEY_3RD_Party);
                    }
                    else if(name == CUSTOM_KEY_MTK_DEPTHMAP)
                    {
                        logicalDevice.Feature |= DEVICE_FEATURE_VSDOF;//DEVICE_FEATURE_MTK_DEPTHMAP;
                        ALOGD("%s(%s)", CUSTOM_KEY_VSDOF, CUSTOM_KEY_MTK_DEPTHMAP);
                    }
                    else
                    {
                        ALOGD("Unsupported feature: %s", name.c_str());
                    }
                }

                gCustomDevList.push_back(logicalDevice);
            }
        }

        return gCustomDevList;
    }
};

};
#endif