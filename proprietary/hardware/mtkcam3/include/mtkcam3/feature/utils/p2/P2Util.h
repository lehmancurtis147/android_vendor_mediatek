/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTKCAM_FEATURE_UTILS_P2_UTIL_H_
#define _MTKCAM_FEATURE_UTILS_P2_UTIL_H_

#include <mtkcam/aaa/IHalISP.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/def/Dip_Notify_datatype.h>
#include <DpDataType.h>

#include <mtkcam3/feature/utils/log/ILogger.h>
#include <mtkcam3/feature/utils/p2/P2Pack.h>
#include <mtkcam3/feature/utils/p2/P2IO.h>

using NSImageio::NSIspio::EPortIndex;

using NSCam::NSIoPipe::PortID;
using NSCam::NSIoPipe::QParams;
using NSCam::NSIoPipe::FrameParams;
using NSCam::NSIoPipe::Input;
using NSCam::NSIoPipe::Output;
using NSCam::NSIoPipe::EPortCapbility;
using NSCam::NSIoPipe::MCropRect;
using NSCam::NSIoPipe::MCrpRsInfo;
using NSCam::NSIoPipe::ExtraParam;
using NSCam::NSIoPipe::NSPostProc::ENormalStreamTag;
using NSCam::NSIoPipe::PQParam;

using NS3Av3::TuningParam;

//#define CROP_IMGO  1
//#define CROP_IMG2O 1
//#define CROP_IMG3O 1
//#define CROP_WDMAO 2
//#define CROP_WROTO 3


namespace NSCam {
namespace Feature {
namespace P2Util {

enum
{
    CROP_IMGO  = 1,
    CROP_IMG2O = 1,
    CROP_IMG3O = 1,
    CROP_WDMAO = 2,
    CROP_WROTO = 3,
};

enum DMAConstrainFlag
{
    DMACONSTRAIN_NONE             = 0,
    DMACONSTRAIN_2BYTEALIGN       = 1 << 0,  // p2s original usage
    DMACONSTRAIN_NOSUBPIXEL       = 1 << 1,  // disable MDP sub-pixel
};

class P2ObjPtr
{
public:
    _SRZ_SIZE_INFO_ *srz4 = NULL;
    NSCam::NSIoPipe::PQParam *pqParam = NULL;
    DpPqParam *pqWDMA = NULL;
    DpPqParam *pqWROT = NULL;
    MBOOL hasPQ = MTRUE;
};

class P2Obj
{
public:
    mutable _SRZ_SIZE_INFO_ srz4;
    mutable NSCam::NSIoPipe::PQParam pqParam;
    mutable DpPqParam pqWDMA;
    mutable DpPqParam pqWROT;

    P2ObjPtr toPtrTable() const
    {
        P2ObjPtr ptr;
        ptr.srz4 = &srz4;
        ptr.pqParam = &pqParam;
        ptr.pqWDMA = &pqWDMA;
        ptr.pqWROT = &pqWROT;
        ptr.hasPQ = MTRUE;
        return ptr;
    }
};


// Camera common function
MBOOL is4K2K(const MSize &size);
MCropRect getCropRect(const MRectF &rectF);

template <typename T>
MBOOL tryGet(const IMetadata &meta, MUINT32 tag, T &val)
{
    MBOOL ret = MFALSE;
    IMetadata::IEntry entry = meta.entryFor(tag);
    if( !entry.isEmpty() )
    {
        val = entry.itemAt(0, Type2Type<T>());
        ret = MTRUE;
    }
    return ret;
};
template <typename T>
MBOOL tryGet(const IMetadata *meta, MUINT32 tag, T &val)
{
    return (meta != NULL) ? tryGet<T>(*meta, tag, val) : MFALSE;
}
template <typename T>
MBOOL trySet(IMetadata &meta, MUINT32 tag, const T &val)
{
    MBOOL ret = MFALSE;
    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    ret = (meta.update(tag, entry) == android::OK);
    return ret;
}
template <typename T>
MBOOL trySet(IMetadata *meta, MUINT32 tag, const T &val)
{
    return (meta != NULL) ? trySet<T>(*meta, tag, val) : MFALSE;
}
template <typename T>
T getMeta(const IMetadata &meta, MUINT32 tag, const T &val)
{
    T temp;
    return tryGet(meta, tag, temp) ? temp : val;
}
template <typename T>
T getMeta(const IMetadata *meta, MUINT32 tag, const T &val)
{
    T temp;
    return tryGet(meta, tag, temp) ? temp : val;
}

// Tuning function
void* allocateRegBuffer(MBOOL zeroInit = MFALSE);
MVOID releaseRegBuffer(void* &buffer);
TuningParam makeTuningParam(const ILog &log, const P2Pack &p2Pack, NS3Av3::IHalISP *halISP, NS3Av3::MetaSet_T &inMetaSet, NS3Av3::MetaSet_T *pOutMetaSet, MBOOL resized, void *regBuffer, IImageBuffer *lcso);
MBOOL needSecureBuffer(const PortID &port);


// Metadata function
MVOID updateExtraMeta(const P2Pack &p2Pack, IMetadata &outHal);
MVOID updateDebugExif(const P2Pack &p2Pack, const IMetadata &inHal, IMetadata &outHal);
MBOOL updateCropRegion(IMetadata &outHal, const MRect &rect);

// QParams util function
const char* toName(MUINT32 index);
const char* toName(EPortIndex index);
const char* toName(const PortID &port);
const char* toName(const Input &input);
const char* toName(const Output &output);
MBOOL is(const PortID &port, EPortIndex index);
MBOOL is(const Input &input, EPortIndex index);
MBOOL is(const Output &output, EPortIndex index);
MBOOL is(const PortID &port, const PortID &rhs);
MBOOL is(const Input &input, const PortID &rhs);
MBOOL is(const Output &output, const PortID &rhs);
MVOID printQParams(const ILog &log, const QParams &params);
MVOID printTuningParam(const ILog &log, const TuningParam &tuning);

MVOID push_in(FrameParams &frame, const PortID &portID, IImageBuffer *buffer);
MVOID push_in(FrameParams &frame, const PortID &portID, const P2IO &in);
MVOID push_out(FrameParams &frame, const PortID &portID, IImageBuffer *buffer);
MVOID push_out(FrameParams &frame, const PortID &portID, IImageBuffer *buffer, EPortCapbility cap, MINT32 transform);
MVOID push_out(FrameParams &frame, const PortID &portID, const P2IO &out);
MVOID push_crop(FrameParams &frame, MUINT32 cropID, const MCropRect &crop, const MSize &size);
MVOID push_crop(FrameParams &frame, MUINT32 cropID, const MRectF &crop, const MSize &size, const MINT32 dmaConstrainFlag = (DMACONSTRAIN_2BYTEALIGN | DMACONSTRAIN_NOSUBPIXEL));

// QParams function
MVOID updateQParams(QParams &qparams, const P2Pack &p2Pack, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning);
QParams makeQParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj);
QParams makeQParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning);
MVOID updateFrameParams(FrameParams &frame, const P2Pack &p2Pack, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning);
FrameParams makeFrameParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj);
FrameParams makeFrameParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning);

// DParams uitil function
DpPqParam* makeDpPqParam(DpPqParam *param, const P2Pack &p2Pack, const Output &out);
DpPqParam* makeDpPqParam(DpPqParam *param, const P2Pack &p2Pack, const MUINT32 portCapabitity);

} // namespace P2Util
} // namespace Feature
} // namespace NSCam

#endif // _MTKCAM_FEATURE_UTILS_P2_UTIL_H_
