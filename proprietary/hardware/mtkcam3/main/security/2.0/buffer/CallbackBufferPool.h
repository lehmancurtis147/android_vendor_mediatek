/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _V2_0_BUFFER_CALLBACKBUFFERPOOL_H_
#define _V2_0_BUFFER_CALLBACKBUFFERPOOL_H_

#include <mtkcam3/pipeline/stream/IStreamInfo.h>

#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>

#include <mutex>
#include <condition_variable>
#include <thread>

#include "IBufferPool.h"

#include <utils/List.h>

namespace NSCam {
namespace security {
namespace V2_0 {

// ------------------------------------------------------------------------

class CallbackBufferPool final : public IBufferPool
{
public:
    CallbackBufferPool(
            const android::sp<NSCam::v3::IImageStreamInfo>& streamInfo,
            NSCam::SecType secType = NSCam::SecType::mem_normal,
            bool allocBuffer = true);

    ~CallbackBufferPool();

    // interface of NSCam::security::v2_0::IBufferPool
    MERROR acquireFromPool(
            const std::string& callerName, int32_t requestNo,
            android::sp<IImageBufferHeap>& bufferHeap,
            uint32_t& transform) override;

    MERROR releaseToPool(
            const std::string& callerName, int32_t requestNo,
            const android::sp<IImageBufferHeap>& bufferHeap,
            uint64_t timestamp, bool invalidBuffer) override;

    inline std::string poolName() const override;

    void dumpPool() const override;

    void setListener(const android::wp<IBufferPool::Listener>& listener) override;

    MERROR setMaxBufferCount(int bufferCount);

    MERROR attachBuffer(const android::sp<IImageBuffer>& buffer);

    MERROR returnBufferToPool(const std::string& callerName,
            const android::sp<IImageBufferHeap>& bufferHeap);

    android::sp<NSCam::v3::IImageStreamInfo> getStreamInfo() const;

    void flush();

    MERROR setForceNoNeedReturnBuffer(bool noNeedReturnBuffer);

private:
    android::sp<NSCam::v3::IImageStreamInfo> mStreamInfo;
    size_t mMaxBuffer;
    size_t mMinBuffer;

    mutable std::mutex mListenerLock;
    android::wp<IBufferPool::Listener> mListener;

    // a list of available image buffer heaps and image buffers in use
    mutable std::mutex mBufferHeapLock;
    std::condition_variable mBufferHeapCond;
    android::List<android::sp<IImageBufferHeap>> mAvailableBuf;
    android::List<android::sp<IImageBufferHeap>> mInUseBuf;

    bool mNoNeedReturnBuffer;
    bool mForceNoNeedReturnBuffer;

    NSCam::SecType mSecType;

    MERROR allocateBuffer();

    // image buffer heaps are removed if one of the followind consition is true:
    // 1. image buffer heaps from attachBuffer()
    // 2. mForceNoNeedReturnBuffer is true
    MERROR removeBufferFromPool(
            const std::string& callerName,
            const android::sp<IImageBufferHeap>& bufferHeap);

    android::sp<IImageBufferHeap> createImageBufferHeap();

    std::unique_ptr<std::thread> mBufferAllocationWorker;
    static void bufferAllocationWorker(CallbackBufferPool& self);
}; // class CallbackBufferPool

} // namespace V2_0
} // namespace security
} // namespace NSCam

#endif // _V2_0_BUFFER_CALLBACKBUFFERPOOL_H_
