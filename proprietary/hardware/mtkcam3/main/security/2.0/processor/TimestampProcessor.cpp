/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "TimestampProcessor"

#include "TimestampProcessor.h"

#include <mtkcam3/main/security/utils/Debug.h>

#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

using namespace android;
using namespace NSCam;
using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;

// ---------------------------------------------------------------------------

template <typename T>
inline MBOOL
tryGetMetadata(
    IMetadata* pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if( pMetadata == NULL ) {
        SEC_LOGW("pMetadata == NULL");
        return MFALSE;
    }

    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}

// ---------------------------------------------------------------------------

class TimestampProcessorImp final : public TimestampProcessor
{
public:
    void onResultReceived(
            const uint32_t requestNo, const StreamId_T streamId,
            const bool errorResult, const IMetadata& result) override;

    void onFrameEnd(uint32_t requestNo) override;

    String8 getUserName() override {
        return String8::format("TimestampProcessor"); }

    void onLastStrongRef( const void* /*id*/) override;

    bool registerCB(wp<ITimestampCallback> wpTimestampCB) override;

    TimestampProcessorImp(MINT32 openId);

    ~TimestampProcessorImp();

    MINT32 getOpenId() const { return mOpenId; }

protected:
    Vector< wp<ITimestampCallback> >    mvwpTimestampCB;

    MINT32        mLogLevel;
    MINT32        mOpenId;
    mutable Mutex mLock;
};

sp< TimestampProcessor >
TimestampProcessor::
createInstance(MINT32 openId)
{
    return new TimestampProcessorImp(openId);
}

TimestampProcessorImp::
TimestampProcessorImp(MINT32 openId)
    : mOpenId(openId)
{
    mLogLevel = ::property_get_int32("debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("debug.camera.log.TimeProcessor", 0);
    }
}

TimestampProcessorImp::
~TimestampProcessorImp()
{
    AutoLog();
}

void
TimestampProcessorImp::
onLastStrongRef(const void* /*id*/)
{
    AutoLog();
}

void
TimestampProcessorImp::
onResultReceived(const uint32_t requestNo, const StreamId_T streamId,
        const bool errorResult, const IMetadata& result)
{
    Mutex::Autolock _l(mLock);
    //
    int64_t timestamp = 0;
    if(errorResult == MFALSE)
    {
        #if MTK_CAM_DISPAY_FRAME_CONTROL_ON
        tryGetMetadata<MINT64>(const_cast<IMetadata*>(&result), MTK_P1NODE_FRAME_START_TIMESTAMP, timestamp);
        #else
        tryGetMetadata<MINT64>(const_cast<IMetadata*>(&result),  MTK_SENSOR_TIMESTAMP, timestamp);
        #endif
    }
    SEC_LOGD("streamID(%" PRIx64 ") reqNo(%d), errRes(%d), TS(%" PRId64 "), MS(%zu)",
            streamId, requestNo, errorResult, timestamp, mvwpTimestampCB.size());
    for(size_t i = 0; i<mvwpTimestampCB.size(); i++)
    {
        SEC_LOGD("i(%zu/%zu), reqNo(%d)",
                i,
                mvwpTimestampCB.size()-1,
                requestNo);
        sp<ITimestampCallback> spTimestampCB = mvwpTimestampCB[i].promote();
        if(spTimestampCB != 0)
        {
            spTimestampCB->doTimestampCallback(requestNo, errorResult, timestamp);
        }
    }
}

void TimestampProcessorImp::onFrameEnd(uint32_t requestNo)
{
    Mutex::Autolock _l(mLock);
    //
    for(size_t i = 0; i < mvwpTimestampCB.size(); i++) {
        sp<ITimestampCallback> spTimestampCB = mvwpTimestampCB[i].promote();
        if(spTimestampCB != 0) {
            spTimestampCB->onFrameEnd(requestNo);
        }
    }
}

bool
TimestampProcessorImp::
registerCB(wp<ITimestampCallback> wpTimestampCB)
{
    Mutex::Autolock _l(mLock);
    //
    if(wpTimestampCB == NULL)
    {
        SEC_LOGE("CB is NULL");
        return MFALSE;
    }
    //
    mvwpTimestampCB.push_back(wpTimestampCB);
    SEC_LOGD("size(%zu)",mvwpTimestampCB.size());
    return MTRUE;
}
