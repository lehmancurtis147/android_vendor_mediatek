/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define DEBUG_LOG_TAG "SecureCamera_test"

#include <mtkcam3/main/security/utils/Debug.h>
#include <utils/Errors.h>

#include "HandleImporter.h"

#include <condition_variable>
#include <chrono>

#include <CameraMetadata.h>
#include <android/hardware/camera/provider/2.4/ICameraProvider.h>
#include <android/hardware/camera/device/1.0/ICameraDevice.h>

#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCamera.h>
#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCameraClientCallback.h>

#include <mtkcam/def/ImageFormat.h>

#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>

#include <mtkcam/utils/std/TypeTraits.h>
#include <mtkcam3/main/security/utils/IonHelper.h>
#include <system/camera_metadata.h>

#ifdef USING_MTK_TEE
#include <uree/system.h>
#include <uree/mem.h>
#endif

//#define DUMP_BUFFER

#include <gtest/gtest.h>

using namespace ::android;
using namespace ::android::hardware::camera::common::V1_0;
using namespace vendor::mediatek::hardware::camera::security::V1_0;

using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_handle;
using ::android::hardware::camera::common::V1_0::helper::HandleImporter;
using ::NSCam::ISecureImageBufferHeap;
using ::android::hardware::camera::device::V3_2::CameraMetadata;

// ------------------------------------------------------------------------

static const uint32_t kOpenID = 0;
static constexpr uint64_t kCookie = 0;

// ------------------------------------------------------------------------

static const char* statusToString(const Status& s)
{
    switch(s)
    {
        case Status::OK:
            return "OK";
        case Status::ILLEGAL_ARGUMENT:
            return "ILLEGAL_ARGUMENT";
        case Status::CAMERA_IN_USE:
            return "CAMERA_IN_USE";
        case Status::MAX_CAMERAS_IN_USE:
            return "MAX_CAMERAS_IN_USE";
        case Status::METHOD_NOT_SUPPORTED:
            return "METHOD_NOT_SUPPORTED";
        case Status::OPERATION_NOT_SUPPORTED:
            return "OPERATION_NOT_SUPPORTED";
        case Status::CAMERA_DISCONNECTED:
            return "CAMERA_DISCONNECTED";
        case Status::INTERNAL_ERROR:
            return "INTERNAL_ERROR";
    }
    CAM_LOGW("Unexpected HAL status code %d", s);
    return "UNKNOWN_ERROR";
}

static status_t parseDeviceName(const std::string& name,
        uint16_t *major, uint16_t *minor, std::string *type, std::string *id)
{

    // Format must be "device@<major>.<minor>/<type>/<id>"

#define ERROR_MSG_PREFIX "%s: Invalid device name '%s'. " \
    "Should match 'device@<major>.<minor>/<type>/<id>' - "

    if (!major || !minor || !type || !id) return INVALID_OPERATION;

    // Verify starting prefix
    const char expectedPrefix[] = "device@";

    if (name.find(expectedPrefix) != 0) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "does not start with '%s'",
                __FUNCTION__, name.c_str(), expectedPrefix);
        return BAD_VALUE;
    }

    // Extract major/minor versions
    constexpr std::string::size_type atIdx = sizeof(expectedPrefix) - 2;
    std::string::size_type dotIdx = name.find('.', atIdx);
    if (dotIdx == std::string::npos) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "does not have @<major>. version section",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }
    std::string::size_type typeSlashIdx = name.find('/', dotIdx);
    if (typeSlashIdx == std::string::npos) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "does not have .<minor>/ version section",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }

    char *endPtr;
    errno = 0;
    long majorVal = strtol(name.c_str() + atIdx + 1, &endPtr, 10);
    if (errno != 0) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "cannot parse major version: %s (%d)",
                __FUNCTION__, name.c_str(), strerror(errno), errno);
        return BAD_VALUE;
    }
    if (endPtr != name.c_str() + dotIdx) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "major version has unexpected length",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }
    long minorVal = strtol(name.c_str() + dotIdx + 1, &endPtr, 10);
    if (errno != 0) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "cannot parse minor version: %s (%d)",
                __FUNCTION__, name.c_str(), strerror(errno), errno);
        return BAD_VALUE;
    }
    if (endPtr != name.c_str() + typeSlashIdx) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "minor version has unexpected length",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }
    if (majorVal < 0 || majorVal > UINT16_MAX || minorVal < 0 || minorVal > UINT16_MAX) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "major/minor version is out of range of uint16_t: %ld.%ld",
                __FUNCTION__, name.c_str(), majorVal, minorVal);
        return BAD_VALUE;
    }

    // Extract type and id

    std::string::size_type instanceSlashIdx = name.find('/', typeSlashIdx + 1);
    if (instanceSlashIdx == std::string::npos) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "does not have /<type>/ component",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }
    std::string typeVal = name.substr(typeSlashIdx + 1, instanceSlashIdx - typeSlashIdx - 1);

    if (instanceSlashIdx == name.size() - 1) {
        CAM_LOGE(ERROR_MSG_PREFIX
                "does not have an /<id> component",
                __FUNCTION__, name.c_str());
        return BAD_VALUE;
    }
    std::string idVal = name.substr(instanceSlashIdx + 1);

#undef ERROR_MSG_PREFIX

    *major = static_cast<uint16_t>(majorVal);
    *minor = static_cast<uint16_t>(minorVal);
    *type = typeVal;
    *id = idVal;

    return OK;
}

// ------------------------------------------------------------------------

/**
 * Adapter for HIDL HAL interface calls; calls into the HIDL HAL interfaces.
 */
class HalInterface : virtual public RefBase
{
public:
    HalInterface();
    virtual ~HalInterface();

    const sp<::android::hardware::hidl_death_recipient>
        getHidlDeathRecipient() const { return mClientCallback.get(); }

    const sp<ISecureCameraClientCallback> getISecureCameraClientCallback() const
    { return mClientCallback; }

private:
    uint64_t mHidlDeviceId;

    struct ClientCallback :
        virtual public ISecureCameraClientCallback,
        virtual public hardware::hidl_death_recipient
    {
        ClientCallback(const wp<HalInterface>& interface) : mInterface(interface) {}

        // Implementation of hidl_death_recipient interface
        void serviceDied(
                uint64_t cookie,
                const wp<hidl::base::V1_0::IBase>& who) override;

        // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
        virtual hardware::Return<void> onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream) override;

        wp<HalInterface> mInterface;
    };

    sp<ClientCallback> mClientCallback;
};

HalInterface::HalInterface()
    : mHidlDeviceId(0)
{
    // register the recipient for a notification if the hwbinder object goes away
    mClientCallback = new HalInterface::ClientCallback(this);
}

HalInterface::~HalInterface()
{
}

void HalInterface::ClientCallback::serviceDied(
        uint64_t cookie, const wp<hidl::base::V1_0::IBase>& who)
{
    (void) who;
    CAM_LOGI("SecureCamera has died");
    if (cookie != kCookie) {
        CAM_LOGW("%s: Unexpected serviceDied cookie %" PRIu64 ", expected %" PRIu32,
                __FUNCTION__, cookie, kCookie);
    }

    // TODO: do something here if necessary when the HAL server has gone

    // release HIDL interface
    auto parent = mInterface.promote();
    if (parent.get())
        parent->mClientCallback.clear();
}

hardware::Return<void> HalInterface::ClientCallback::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
    (void) status;
    std::ostringstream stringStream;
    stringStream << "onBufferAvailable format(0x" << std::hex << stream.format << ")";
    CAM_TRACE_NAME(stringStream.str().c_str());

    HandleImporter handleImporter;
    buffer_handle_t importedBuffer = buffer.getNativeHandle();
    handleImporter.importBuffer(importedBuffer);

    CAM_LOGD("%s: bufferId(%" PRIu64 ") FD(%d) imported FD(%d) dataSize(%llu) size(%dx%d) stride(%llu) format(0x%x)",
            __FUNCTION__, bufferId,
            buffer.getNativeHandle()->data[0], importedBuffer->data[0],
            stream.size, stream.width, stream.height, stream.stride, stream.format);

    // dump buffer here
    {
        NSCam::security::utils::IonHelper helper;
        const size_t dataSize(stream.size);
        const int sharedFD(importedBuffer->data[0]);

//  [EXAMPLE 1]: get virtual address from file descriptor
//        void *addr = helper.mmap(
//                NULL, dataSize, PROT_READ | PROT_WRITE, MAP_SHARED, sharedFD);
//
//        CAM_LOGD("%s: bufferAddr(%0x" PRIxPTR ") dataSize(%zu) sharedFD(%d)",
//                __FUNCTION__, addr, dataSize, sharedFD);
//
//        helper.munmap(addr, dataSize);

//  [EXAMPLE 2]: dump buffer
#ifdef DUMP_BUFFER
        helper.dumpBuffer(sharedFD, dataSize, stream.width, stream.height,
                stream.stride, stream.format, true);
#endif
    }

    handleImporter.freeBuffer(importedBuffer);

    return hardware::Void();
}

/**
 * Generic template of ISecureCameraClientCallback
 * TODO: pass function object as the business logic of onBufferAvailable()
 */
template <typename REF>
struct ClientCallback : virtual public ISecureCameraClientCallback
{
    ClientCallback(REF* parent) : mParent(parent) {}

private:
    // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
    virtual hardware::Return<void> onBufferAvailable(
            Status status, uint64_t bufferId,
            const hidl_handle& buffer, const Stream& stream) override;

    REF* mParent;
};

// ------------------------------------------------------------------------

/**
 * A testing base class that provids testing boilerplate.
 */
class TestBase : public ::testing::Test
{
protected:
    // You can do set-up work for each test here.
    TestBase() = default;

    // You can do clean-up work that doesn't throw exceptions here.
    virtual ~TestBase() = default;

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    // Code here will be called immediately after the constructor (right
    // before each test).
    virtual void SetUp();

    // Code here will be called immediately after each test (right
    // before the destructor).
    virtual void TearDown();
};

/**
 * This test fixture is for testing class ISecureCamera.
 */
class SecureCameraTest : public TestBase
{
protected:
    SecureCameraTest();
    virtual ~SecureCameraTest();

    // Objects declared here can be used by all tests in the test case for SecureCameraTest.
    sp<ISecureCamera> mHidlSecureCamera;
};

class SecureCameraCaptureTest : public SecureCameraTest,
                          public ::testing::WithParamInterface<int>
{
};

class SecureCameraPerformanceTest : public SecureCameraTest,
                                    virtual public RefBase
{
public:
    SecureCameraPerformanceTest();

    class ResultPrinter : public ::testing::EmptyTestEventListener
    {
        virtual void OnTestEnd(const ::testing::TestInfo& testInfo);
    };

    static std::pair<int64_t, std::mutex> sFirstCaptureLatencyInMillisecond;

protected:
    // full specializations of template friend
    friend class ClientCallback<SecureCameraPerformanceTest>;

    // Used for test case FirstCaptureLatency
    static const std::chrono::duration<int64_t> kFirstCaptureDoneTimeout;
    std::atomic<bool> mFirstCaptureDone;
    mutable std::mutex mFirstCaptureDoneLock;
    std::condition_variable mFirstCaptureDoneCondition;
};

/**
 * This test fixture is for testing class ISecureImageBufferHeap.
 */
class SecureImageBufferHeapTest : public TestBase
{
protected:
    const int kBufferUsage = NSCam::eBUFFER_USAGE_HW_CAMERA_READWRITE;
};

#ifdef USING_MTK_TEE
/**
 * This test fixture is for testing UREE library.
 */
class UREETest : public TestBase
{
protected:
    UREETest();

    const int kBufferUsage = NSCam::eBUFFER_USAGE_HW_CAMERA_READWRITE;
    std::string mMemSrvName;
};
#endif

void TestBase::SetUp()
{
    // Gets information about the currently running test.
    // Do NOT delete the returned object - it's managed by the UnitTest class.
    const ::testing::TestInfo* testInfo =
        ::testing::UnitTest::GetInstance()->current_test_info();
    CAM_LOGD("Begin test: %s.%s", testInfo->test_case_name(), testInfo->name());
}

void TestBase::TearDown()
{
    // Gets information about the currently running test.
    // Do NOT delete the returned object - it's managed by the UnitTest class.
    const ::testing::TestInfo* testInfo =
        ::testing::UnitTest::GetInstance()->current_test_info();
    CAM_LOGD("End test: %s.%s", testInfo->test_case_name(), testInfo->name());
}

SecureCameraTest::SecureCameraTest()
{
    // connect to secure camera
    mHidlSecureCamera = ISecureCamera::getService("internal/0");
    EXPECT_TRUE(mHidlSecureCamera != nullptr) << "failed to get secure camera interface";
}

SecureCameraTest::~SecureCameraTest()
{
    // disconnect from secure camera
    mHidlSecureCamera.clear();
}

SecureCameraPerformanceTest::SecureCameraPerformanceTest()
{
    std::atomic_init(&mFirstCaptureDone, false);
}

void SecureCameraPerformanceTest::ResultPrinter::OnTestEnd(
        const ::testing::TestInfo& testInfo)
{
    const std::string kFirstCaptureLatencyTestName("FirstCaptureLatency");
    if (kFirstCaptureLatencyTestName.compare(testInfo.name()) == 0)
    {
        auto& latency(SecureCameraPerformanceTest::sFirstCaptureLatencyInMillisecond);
        std::lock_guard<std::mutex> _l(latency.second);
        std::cout << "[----------] The first capture latency is " <<
            latency.first << " millisecond(s)" << std::endl;
    }
}

const std::chrono::duration<int64_t> SecureCameraPerformanceTest::kFirstCaptureDoneTimeout(std::chrono::seconds(5));
std::pair<int64_t, std::mutex> SecureCameraPerformanceTest::sFirstCaptureLatencyInMillisecond;

template <typename REF>
hardware::Return<void> ClientCallback<REF>::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
    std::ostringstream stringStream;
    stringStream << "onBufferAvailable format(0x" << std::hex << stream.format << ")";
    CAM_TRACE_NAME(stringStream.str().c_str());

    if (mParent == nullptr)
    {
        CAM_LOGW("Invalid secure camera test instance");
        return hardware::Void();
    }

    // NOTE: skip if the buffer format is not NV12 or the first frame has been received
    if (!(NSCam::EImageFormat::eImgFmt_NV12 == stream.format) || mParent->mFirstCaptureDone.load(std::memory_order_relaxed))
        return hardware::Void();

    // raise flag when receiving the first frame
    mParent->mFirstCaptureDone.store(true, std::memory_order_relaxed);
    mParent->mFirstCaptureDoneCondition.notify_one();

    CAM_LOGD("%s: bufferId(%" PRIu64 ") FD(%d) dataSize(%llu) size(%dx%d) stride(%llu) format(0x%x)",
            __FUNCTION__, bufferId,
            buffer->data[0],
            stream.size, stream.width, stream.height, stream.stride, stream.format);

    return hardware::Void();
}

#ifdef USING_MTK_TEE
UREETest::UREETest()
    : mMemSrvName("com.mediatek.geniezone.srv.mem")
{
}
#endif

// ------------------------------------------------------------------------

TEST_F(SecureCameraTest, LinkAndUnlinkHWBinderDeath)
{
    // link to death
    sp<HalInterface> halInterface = new HalInterface();
    hardware::Return<bool> linked =
        mHidlSecureCamera->linkToDeath(
                halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);

    EXPECT_TRUE(linked) << "Unable to link to SecureCamera death notifications";
    EXPECT_TRUE(linked.isOk())
        << "Transaction error in linking to SecureCamera death: "
        << linked.description().c_str();

    hardware::Return<bool> unlinked =
        mHidlSecureCamera->unlinkToDeath(halInterface->getHidlDeathRecipient());

    EXPECT_TRUE(unlinked) << "Unable to unlink from SecureCamera death notifications";
    EXPECT_TRUE(unlinked.isOk())
        << "Transaction error in unlinking from SecureCamera death: "
        << unlinked.description().c_str();
}

TEST_F(SecureCameraTest, GetCameraIdList)
{
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    // get camera id list
    std::vector<std::string> devices;
    hardware::Return<void> ret =
        mHidlSecureCamera->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        EXPECT_EQ(idStatus, Status::OK) << "get camera id list failed("
            << NSCam::toLiteral(err) << ")";
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk())
        << "Transaction error in getCameraIdList from SecureCamera : "
        << ret.description().c_str();

    for (auto&& str : devices)
        CAM_LOGD("available secure camera device(%s)", str.c_str());
}

TEST_F(SecureCameraTest, GetCameraStatusBeforeOpen)
{
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    // get camera status
    std::vector<std::string> secureDevices;
    hardware::Return<void> ret =
        mHidlSecureCamera->getSecureStatus([&err, &secureDevices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        err = idStatus;
        if (err == Status::CAMERA_IN_USE) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                secureDevices.push_back(cameraDeviceIDs[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk())
        << "Transaction error in getSecureStatus from SecureCamera : "
        << ret.description().c_str();

    CAM_LOGD("camera status is in %s mode",
            err != (Status::CAMERA_IN_USE) ? "NORMAL" : "SECURE");

    EXPECT_TRUE(secureDevices.empty())
        << "camera status should in NORMAL mode without opening any secure camera(s)";

    // list all secure camera(s) in use
    for (auto&& str : secureDevices)
        CAM_LOGD("Secure camera device in use(%s)", str.c_str());
}

TEST_P(SecureCameraCaptureTest, Capture)
{
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    // get camera id list
    std::vector<std::string> devices;
    hardware::Return<void> ret =
        mHidlSecureCamera->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        EXPECT_EQ(idStatus, Status::OK) << "get camera id list failed("
            << NSCam::toLiteral(err) << ")";
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk())
        << "Transaction error in getCameraIdList from SecureCamera : "
        << ret.description().c_str();

    ASSERT_TRUE(!devices.empty()) << "unavailable secure camera device";

    {
        // register callback (HIDL server to HIDL client)
        sp<HalInterface> halInterface = new HalInterface();
        hardware::Return<Status> status =
            mHidlSecureCamera->registerCallback(
                halInterface->getISecureCameraClientCallback());
        ASSERT_TRUE(status.isOk())
            << "Transaction error in registerCallback from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera registerCallback failed";
    }

    const hidl_string device(devices.front());
    const hidl_vec<hidl_string> deviceList({devices.front()});
    {
        // open the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->open(deviceList);
        ASSERT_TRUE(status.isOk())
            << "Transaction error in open from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera open failed(assertion)";
    }

    {
        // initialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->initialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in initialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera initialize failed!";
    }

    {
        // start capture
        hardware::Return<Status> status =
            mHidlSecureCamera->startCapture(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in startCapture from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera startCapture failed!";
    }
    // wait for a specific period
    CAM_LOGD("wait for %d second(s)", GetParam());
    usleep(GetParam() * 1000 * 1000);

    {
        // stop capture
        hardware::Return<Status> status =
            mHidlSecureCamera->stopCapture(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in stopCapture from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera stopCapture failed!";
    }

    {
        // uninitialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->uninitialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in uninitialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera uninitialize failed!";
    }

    {
        // close the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->close(deviceList);

        EXPECT_TRUE(status.isOk())
            << "Transaction error in close from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera close failed!";
    }
}
const int periodParam[] = {1, 5, 10};
INSTANTIATE_TEST_CASE_P(CAPTURE, SecureCameraCaptureTest, ::testing::ValuesIn(periodParam));

TEST_F(SecureCameraPerformanceTest, FirstCaptureLatency)
{
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    auto start = std::chrono::system_clock::now();

    // get camera id list
    std::vector<std::string> devices;
    hardware::Return<void> ret =
        mHidlSecureCamera->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        EXPECT_EQ(idStatus, Status::OK) << "get camera id list failed("
            << NSCam::toLiteral(err) << ")";
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk())
        << "Transaction error in getCameraIdList from SecureCamera : "
        << ret.description().c_str();

    ASSERT_TRUE(!devices.empty()) << "unavailable secure camera device";

    {
        // register callback (HIDL server to HIDL client)
        sp<ClientCallback<SecureCameraPerformanceTest>> clientCallback
            = new ClientCallback<SecureCameraPerformanceTest>(this);
        hardware::Return<Status> status =
            mHidlSecureCamera->registerCallback(clientCallback);
        ASSERT_TRUE(status.isOk())
            << "Transaction error in registerCallback from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera registerCallback failed";
    }

    const hidl_string device(devices.front());
    const hidl_vec<hidl_string> deviceList({devices.front()});
    {
        // open the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->open(deviceList);
        ASSERT_TRUE(status.isOk())
            << "Transaction error in open from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera open failed(assertion)";
    }

    {
        // initialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->initialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in initialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera initialize failed!";
    }

    {
        // start capture
        hardware::Return<Status> status =
            mHidlSecureCamera->startCapture(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in startCapture from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera startCapture failed!";
    }

    // wait until receiving the first capture result
    CAM_LOGD("wait for the first frame (time out is %" PRId64 " second(s))",
            kFirstCaptureDoneTimeout.count());

    std::unique_lock<std::mutex> _l(mFirstCaptureDoneLock);
    bool noTimeout = mFirstCaptureDoneCondition.wait_for(_l, kFirstCaptureDoneTimeout,
            [&]
            {
                // exit when receiving the first frame
                if (mFirstCaptureDone.load(std::memory_order_relaxed))
                    return true;

                return false;
            });

    {
        auto& latency(SecureCameraPerformanceTest::sFirstCaptureLatencyInMillisecond);
        std::lock_guard<std::mutex> _l(latency.second);

        latency.first = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now() - start).count();

        std::ostringstream stringStream;
        stringStream << "The first capture latency is " << latency.first << " millisecond(s)";
        CAM_LOGD("%s", stringStream.str().c_str());
    }

    EXPECT_TRUE(noTimeout)
        << "Timeout " << kFirstCaptureDoneTimeout.count() << " second(s) expires";

    {
        // stop capture
        hardware::Return<Status> status =
            mHidlSecureCamera->stopCapture(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in stopCapture from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera stopCapture failed!";
    }

    {
        // uninitialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->uninitialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in uninitialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera uninitialize failed!";
    }

    {
        // close the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->close(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in close from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera close failed!";
    }
}

TEST_F(SecureCameraTest, DeviceStaticInformation)
{
    // Get internal camera provider
    const std::string kProviderName("internal/0");

    // NOTE: the service name is defined in
    //       vendor/mediatek/proprietary/hardware/mtkcam/main/hal/service/service.cpp
    sp<hardware::camera::provider::V2_4::ICameraProvider> provider =
        hardware::camera::provider::V2_4::ICameraProvider::getService(kProviderName);
    EXPECT_TRUE(provider != nullptr) << "failed to get camera provider interface";

    CAM_LOGD("camera provider(%s) name(%s) discovered",
            hardware::camera::provider::V2_4::ICameraProvider::descriptor, kProviderName.c_str());

    // Get initial list of camera devices, if any
    hardware::Return<Status> status(Status::OK);
    std::vector<std::string> devices;
    hardware::Return<void> ret = provider->getCameraIdList([&status, &devices](
            Status idStatus,
            const hardware::hidl_vec<hardware::hidl_string>& cameraDeviceNames) {
        status = idStatus;
        if (status == Status::OK) {
            for (size_t i = 0; i < cameraDeviceNames.size(); i++) {
                devices.push_back(cameraDeviceNames[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk()) <<
        "Transaction error in getting camera ID list from provider '" <<
        kProviderName.c_str() << "'";

    EXPECT_TRUE(status == Status::OK) <<
        "Unable to query for camera devices from provider '" << kProviderName.c_str() << "'";

    // The orientation of the main front-facing camera image
    // NOTE: The value is the angle that the camera image needs to be rotated clockwise
    //       so it shows correctly on the display in its natural orientation.
    //       It must be 0, 90, 180, or 270.
    std::string kMainfrontFaceing("1");
    uint32_t mainFrontFacingOrientation = 0;

    // Enumerate internal camera device interfaces known to the camera provider
    // NOTE: The device instance names enumerated by the provider must be of the form
    //       device@<major>.<minor>/<type>/<id> where
    //       <major>/<minor> is the HIDL version of the interface. <id> is either a small
    //       incrementing integer for "internal" device types, with 0 being the main
    //       back-facing camera and 1 being the main front-facing camera, if they exist.
    //       Get the main front-facing camera
    for (const auto& device : devices)
    {
        // Extract major/minor versions, type and id
        uint16_t major, minor;
        std::string type, id;
        status_t res = parseDeviceName(device, &major, &minor, &type, &id);
        EXPECT_TRUE(res == android::OK) <<
            "Parse device name failed: " << strerror(-res) << " (" << res << ")";

        switch(major) {
            case 3: {
                if (id == kMainfrontFaceing)
                {
                    ::android::sp<::android::hardware::camera::device::V3_2::ICameraDevice> device3_x;
                    ALOGI("getCameraCharacteristics: Testing camera device %s", device.c_str());

                    ret = provider->getCameraDeviceInterface_V3_x(
                        device, [&](auto status, const auto& device3) {
                            ALOGI("getCameraDeviceInterface_V3_x returns status:%d", (int)status);
                            ASSERT_EQ(Status::OK, status);
                            ASSERT_NE(device3, nullptr);
                            device3_x = device3;
                        });
                    ASSERT_TRUE(ret.isOk());

                    ret = device3_x->getCameraCharacteristics([&status, this](Status s,
                                    ::android::hardware::camera::device::V3_2::CameraMetadata metadata) {
                                status = s;
                                if (s == Status::OK) {
                                    camera_metadata_t *buffer =
                                            reinterpret_cast<camera_metadata_t*>(metadata.data());
                                    size_t expectedSize = metadata.size();
                                    int res = validate_camera_metadata_structure(buffer, &expectedSize);
                                    if (res == OK || res == CAMERA_METADATA_VALIDATION_SHIFTED) {
                                        camera_metadata_ro_entry orientation;
                                        auto retcode = find_camera_metadata_ro_entry(buffer,
                                                ANDROID_SENSOR_ORIENTATION, &orientation);
                                        if ((0 == retcode) && (orientation.count > 0)) {
                                            CAM_LOGD("Main front-facing camera orientation(%" PRIu32 ")", orientation.data.i32[0]);
                                        } else {
                                            ALOGE("%s: Unable to find android.sensor.orientation static metadata", __FUNCTION__);
                                            //return NAME_NOT_FOUND;
                                            status = Status::INTERNAL_ERROR;
                                        }
                                    } else {
                                        ALOGE("%s: Malformed camera metadata received from HAL", __FUNCTION__);
                                        status = Status::INTERNAL_ERROR;
                                    }
                                }
                            });

                    ASSERT_TRUE(ret.isOk())
                    << "Transaction error getting camera characteristics for device: "
                    << ret.description().c_str();

                    ASSERT_EQ(status, Status::OK)
                    << "Unable to get camera characteristics for device: "
                    << statusToString(status);
                }
            }break;
            case 1: {
                // Query camera device static information
                // FIXME: check the major version of each camera device and operate on the highest version one
                sp<hardware::camera::device::V1_0::ICameraDevice> cameraInterface;
                ret = provider->getCameraDeviceInterface_V1_x(device, [&status, &cameraInterface](
                            Status s, sp<hardware::camera::device::V1_0::ICameraDevice> interface) {
                        status = s;
                        cameraInterface = interface;
                        });
                EXPECT_TRUE(ret.isOk()) <<
                    "Transaction error trying to obtain interface for camera device " <<
                    device.c_str() << ": " << ret.description().c_str();

                EXPECT_TRUE(status == Status::OK) <<
                    "Unable to obtain interface for camera device " <<
                            device.c_str() << ": " << statusToString(status);

                hardware::camera::device::V1_0::CameraInfo cameraInfo;
                ret = cameraInterface->getCameraInfo(
                        [&status, &cameraInfo](Status s, hardware::camera::device::V1_0::CameraInfo camInfo) {
                        status = s;
                        cameraInfo = camInfo;
                        });
                EXPECT_TRUE(ret.isOk()) <<
                    "Transaction error reading camera info from device " <<
                            id.c_str() << ": " << ret.description().c_str();

                EXPECT_TRUE(status == Status::OK) << statusToString(status);

                CAM_LOGD("Internal camera device(%s) major(%" PRIu16 ") minor(%" PRIu16 ") " \
                        "type(%s) id(%s) orientation(%" PRIu32 ")",
                        device.c_str(), major, minor, type.c_str(),
                        id.c_str(), cameraInfo.orientation);

                if (id == kMainfrontFaceing)
                {
                    mainFrontFacingOrientation = cameraInfo.orientation;
                    CAM_LOGD("Main front-facing camera orientation(%" PRIu32 ")", mainFrontFacingOrientation);
                }
            }break;
            default: {
                ALOGE("%s: Unsupported device version %d", __FUNCTION__, major);
                ADD_FAILURE();
            }break;
        }
    }

}

TEST_F(SecureCameraTest, CancelAndReleaseBeforeCapture)
{
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    // get camera id list
    std::vector<std::string> devices;
    hardware::Return<void> ret =
        mHidlSecureCamera->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        EXPECT_EQ(idStatus, Status::OK) << "get camera id list failed("
            << NSCam::toLiteral(err) << ")";
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });

    EXPECT_TRUE(ret.isOk())
        << "Transaction error in getCameraIdList from SecureCamera : "
        << ret.description().c_str();

    ASSERT_TRUE(!devices.empty()) << "unavailable secure camera device";

    {
        // register callback (HIDL server to HIDL client)
        sp<HalInterface> halInterface = new HalInterface();
        hardware::Return<Status> status =
            mHidlSecureCamera->registerCallback(
                halInterface->getISecureCameraClientCallback());
        ASSERT_TRUE(status.isOk())
            << "Transaction error in registerCallback from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera registerCallback failed";
    }

    const hidl_string device(devices.front());
    const hidl_vec<hidl_string> deviceList({devices.front()});
    {
        // open the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->open(deviceList);
        ASSERT_TRUE(status.isOk())
            << "Transaction error in open from SecureCamera : "
            << status.description().c_str();
        ASSERT_EQ(status, Status::OK)
            << "Secure camera open failed(assertion)";
    }

    {
        // initialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->initialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in initialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera initialize failed!";
    }

    {
        // uninitialize the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->uninitialize(deviceList);
        EXPECT_TRUE(status.isOk())
            << "Transaction error in uninitialize from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera uninitialize failed!";
    }

    {
        // close the first secure camera
        hardware::Return<Status> status =
            mHidlSecureCamera->close(deviceList);

        EXPECT_TRUE(status.isOk())
            << "Transaction error in close from SecureCamera : "
            << status.description().c_str();
        EXPECT_EQ(status, Status::OK)
            << "Secure camera close failed!";
    }
}

// TODO: rafactor to value parameterized tests
TEST_F(SecureImageBufferHeapTest, CreateImageBuffer)
{
    using namespace NSCam;

    EImageFormat format = eImgFmt_BAYER10;
    MSize size(1304, 980);
    size_t bufStridesInBytes[3] { 1632, 0, 0 };
    size_t bufBoundaryInBytes[3] {};
    size_t planeCount = 1;
    // Protected memory allocation
    SecType secType = SecType::mem_protected;
    const ::testing::TestInfo* testInfo =
        ::testing::UnitTest::GetInstance()->current_test_info();

    IImageBufferAllocator::ImgParam imgParam(
        format,
        size,
        bufStridesInBytes, bufBoundaryInBytes, planeCount);

    ISecureImageBufferHeap::AllocExtraParam extraParam(
            0/*nocache*/, 1/*security*/, 0/*coherence*/, false, secType/*SecType*/);

    sp<IImageBufferHeap> bufferHeap =
        ISecureImageBufferHeap::create(testInfo->test_case_name(), imgParam, extraParam);
    EXPECT_TRUE(bufferHeap != nullptr) << "create image buffer heap failed";

    ImgBufCreator creator(bufferHeap->getImgFormat());
    sp<IImageBuffer> buffer = bufferHeap->createImageBuffer(&creator);
}

TEST_F(SecureImageBufferHeapTest, LockAndUnlockImageBuffer)
{
    using namespace NSCam;

    EImageFormat format = eImgFmt_BAYER10;
    MSize size(1304, 980);
    size_t bufStridesInBytes[3] { 1632, 0, 0 };
    size_t bufBoundaryInBytes[3] {};
    size_t planeCount = 1;
    // Protected memory allocation
    SecType secType = SecType::mem_protected;
    const ::testing::TestInfo* testInfo =
        ::testing::UnitTest::GetInstance()->current_test_info();

    IImageBufferAllocator::ImgParam imgParam(
        format,
        size,
        bufStridesInBytes, bufBoundaryInBytes, planeCount);

    ISecureImageBufferHeap::AllocExtraParam extraParam(
            0/*nocache*/, 1/*security*/, 0/*coherence*/, false, secType/*SecType*/);

    sp<IImageBufferHeap> bufferHeap =
        ISecureImageBufferHeap::create(testInfo->test_case_name(), imgParam, extraParam);
    EXPECT_TRUE(bufferHeap != nullptr) << "create image buffer heap failed";

    ImgBufCreator creator(bufferHeap->getImgFormat());
    sp<IImageBuffer> buffer = bufferHeap->createImageBuffer(&creator);

    EXPECT_TRUE(buffer->lockBuf(testInfo->test_case_name(), kBufferUsage));

    {
        const auto fileDescriptor(buffer->getFD(0));
        const auto virtualAddress(buffer->getBufVA(0));
        const auto secureHandle(buffer->getBufPA(0));
        CAM_LOGD("FD(%d) va(0x%" PRIxPTR ") pa(0x%" PRIxPTR ")",
                fileDescriptor, virtualAddress, secureHandle);

        EXPECT_TRUE(fileDescriptor >= 0) << "invalid file descriptor";
        EXPECT_TRUE(virtualAddress == 0) << "virtual address should be 0";
        EXPECT_TRUE(secureHandle != 0)   << "invalid secure handle";
    }

    EXPECT_TRUE(buffer->unlockBuf(testInfo->test_case_name()));
}

#ifdef USING_MTK_TEE
TEST_F(UREETest, CreateAndSession)
{
    TZ_RESULT ret = TZ_RESULT_SUCCESS;
    UREE_SESSION_HANDLE memSrvSession;

    // create session for shared memory
    ret = UREE_CreateSession(mMemSrvName.c_str(), &memSrvSession);
    EXPECT_EQ(ret, TZ_RESULT_SUCCESS) << "create mem sesion failed(" << ret << ")";

    // close session for shared memory
    ret = UREE_CloseSession(memSrvSession);
    EXPECT_EQ(ret, TZ_RESULT_SUCCESS) << "close mem sesion failed(" << ret << ")";
}

TEST_F(UREETest, RegisterAndUnregisterSharedMemory)
{
    TZ_RESULT ret = TZ_RESULT_SUCCESS;
    UREE_SESSION_HANDLE memSrvSession;

    uint32_t sharedMemorySize = 8000000;

    // create session for shared memory
    ret = UREE_CreateSession(mMemSrvName.c_str(), &memSrvSession);
    EXPECT_EQ(ret, TZ_RESULT_SUCCESS) << "create mem sesion failed(" << ret << ")";

    // shared memory test
    {
        std::unique_ptr<char[], std::function<void(char[])>>
            sharedMemory(new char[sharedMemorySize], [](char memory[])
                    {
                        CAM_LOGD("release shared memory");
                        delete [] memory;
                    });
        CAM_LOGD("allocate shared memory: size(%u)", sharedMemorySize);

        UREE_SHAREDMEM_HANDLE sharedMemoryHandle;
        UREE_SHAREDMEM_PARAM sharedMemoryParam {
            .buffer = sharedMemory.get(), .size = sharedMemorySize };
        ret = UREE_RegisterSharedmem(
                memSrvSession, &sharedMemoryHandle, &sharedMemoryParam);
        EXPECT_EQ(ret, TZ_RESULT_SUCCESS)
            << "register shared memory to MTK TEE failed(" << ret << ")";

        ret = UREE_UnregisterSharedmem(memSrvSession, sharedMemoryHandle);
        EXPECT_EQ(ret, TZ_RESULT_SUCCESS)
            << "unregister shared memory from MTK TEE failed(" << ret << ")";
    }

    // close session for shared memory
    ret = UREE_CloseSession(memSrvSession);
    EXPECT_EQ(ret, TZ_RESULT_SUCCESS) << "close mem sesion failed(" << ret << ")";
}
#endif // USING_MTK_TEE

// ------------------------------------------------------------------------

int runGoogleTest()
{
    return RUN_ALL_TESTS();
}

int runTest()
{
    sp<ISecureCamera> hidlSecureCamera;

    //// secure camera test begin

    // connect to secure camera
    // NOTE: the service name is defined in
    //       vendor/mediatek/proprietary/hardware/mtkcam/main/hal/service/service.cpp
    hidlSecureCamera = ISecureCamera::getService("internal/0");
    if (hidlSecureCamera == nullptr) {
        CAM_LOGE("failed to get secure camera interface");
        return ::android::NO_INIT;
    }
    else {
        CAM_LOGD("get secure camera interface done");
    }

    sp<HalInterface> halInterface = new HalInterface();
    hardware::Return<bool> linked =
        hidlSecureCamera->linkToDeath(
                halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);
    if (!linked.isOk()) {
        CAM_LOGE("%s: Transaction error in linking to SecureCamera death: %s",
                __FUNCTION__, linked.description().c_str());
        return ::android::UNKNOWN_ERROR;
    } else if (!linked) {
        CAM_LOGW("%s: Unable to link to SecureCamera death notifications",
                __FUNCTION__);
    }

    // enable this part to check binder death notification
    //getchar();

    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    // get camera id list
    std::vector<std::string> devices;
    hardware::Return<void> ret =
        hidlSecureCamera->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });
    if (!ret.isOk()) {
        CAM_LOGE("Transaction error: getCameraIdList failed %s", ret.description().c_str());
    }
    for (auto&& str : devices)
        CAM_LOGD("available secure camera device(%s)", str.c_str());

    if (devices.empty())
    {
        CAM_LOGW("unavailable secure camera device, exit...");
        return ::android::BAD_VALUE;
    }

    const hidl_string device(devices.front());
    const hidl_vec<hidl_string> deviceList ({devices.front()});

    // get camera status
    std::vector<std::string> secure_devices;
    {
        hardware::Return<void> ret =
            hidlSecureCamera->getSecureStatus([&err, &secure_devices](
                ::android::hardware::camera::common::V1_0::Status idStatus,
                const hidl_vec<hidl_string>& cameraDeviceIDs) {
            err = idStatus;
            if (err == Status::CAMERA_IN_USE) {
                for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                    secure_devices.push_back(cameraDeviceIDs[i]);
                }
            } });
        if (!ret.isOk()) {
            CAM_LOGE("Transaction error: getSecureStatus failed %s", ret.description().c_str());
        }
        if (err == Status::CAMERA_IN_USE)
          CAM_LOGD("secure camera status is SECURE mode!!");
        else
          CAM_LOGD("secure camera status is not SECURE mode!!");

        for (auto&& str : secure_devices)
            CAM_LOGD("BUSY secure camera device(%s)", str.c_str());
    }

    {
        // register callback (HIDL server to HIDL client)
        hardware::Return<Status> status =
            hidlSecureCamera->registerCallback(
            halInterface->getISecureCameraClientCallback());
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: registerCallback failed %s", status.description().c_str());
            return ::android::UNKNOWN_ERROR;
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera registerCallback failed");
            return ::android::UNKNOWN_ERROR;
        }
    }

    {
        // open the first secure camera
        hardware::Return<Status> status =
            hidlSecureCamera->open(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: open failed %s", status.description().c_str());
            return ::android::UNKNOWN_ERROR;
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera open failed");
            return ::android::UNKNOWN_ERROR;
        }
    }

    {
        // get camera status 2
        std::vector<std::string> secure_devices2;

        hardware::Return<void> status =
            hidlSecureCamera->getSecureStatus([&err, &secure_devices2](
                    ::android::hardware::camera::common::V1_0::Status idStatus,
                    const hidl_vec<hidl_string>& cameraDeviceIDs) {
                err = idStatus;
                if (err == Status::CAMERA_IN_USE) {
                for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                secure_devices2.push_back(cameraDeviceIDs[i]);
                }
                } });
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: getSecureStatus failed %s", status.description().c_str());
        }
        if (err == Status::CAMERA_IN_USE)
            CAM_LOGD("secure camera status is SECURE mode!!");
        else
            CAM_LOGD("secure camera status is not SECURE mode!!");

        for (auto&& str : secure_devices2)
            CAM_LOGD("BUSY secure camera device(%s)", str.c_str());
    }

    {
        // initialize the first secure camera
        hardware::Return<Status> status =
            hidlSecureCamera->initialize(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: initialize failed %s", status.description().c_str());
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera initialize failed");
            return ::android::UNKNOWN_ERROR;
        }
    }

    {
        printf("Press ENTER to startCapture...\n");
        fflush(stdout);
        getchar();
    }

    {
        // start capture
        hardware::Return<Status> status =
            hidlSecureCamera->startCapture(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: startCapture failed %s", status.description().c_str());
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera startCapture failed");
            return ::android::UNKNOWN_ERROR;
        }
    }
    // TODO: get callback, consume and then return

    {
        printf("Press ENTER to stop...\n");
        fflush(stdout);
        getchar();
    }
    {
        // stop capture
        hardware::Return<Status> status =
            hidlSecureCamera->stopCapture(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: stopCapture failed %s", status.description().c_str());
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera stopCapture failed");
            return ::android::UNKNOWN_ERROR;
        }
    }

    {
        // uninitialize the first secure camera
        hardware::Return<Status> status =
            hidlSecureCamera->uninitialize(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: uninitialize failed %s", status.description().c_str());
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera uninitialize failed");
            return ::android::UNKNOWN_ERROR;
        }
    }

    {
        // close the first secure camera
        hardware::Return<Status> status =
            hidlSecureCamera->close(deviceList);
        if (!status.isOk()) {
            CAM_LOGE("Transaction error: close failed %s", status.description().c_str());
        }
        if (status != Status::OK) {
            CAM_LOGE("Secure camera close failed");
            return ::android::UNKNOWN_ERROR;
        }
    }
    // disconnect from secure camera
    hidlSecureCamera.clear();

    //// secure camera test end

    return ::android::OK;
}

void showUsage(const char *programName)
{
    std::printf("Usage: %s [OPTION]\n\n", programName);
    std::printf("DESCRIPTION\n");
    std::printf("\t-n\t\t\texecute interactive tests.\n");
    std::printf("\t-g [--help|-h|-?] \texecute Google tests.\n");
    std::printf("\t\t\t\t--help, -h or -? lists the supported flags and their usage\n");
}

int main(int argc, char **argv)
{
    if (argc == 1)
    {
        showUsage(argv[0]);
        return ::android::OK;
    }

    // parse the command line and remove all recognized Google Test flags
    // must call this function before calling RUN_ALL_TESTS()
    ::testing::InitGoogleTest(&argc, argv);

    // gets hold of the event listener list.
    ::testing::TestEventListeners& listeners =
        ::testing::UnitTest::GetInstance()->listeners();

    // adds a listener to the end.  Google Test takes the ownership.
    listeners.Append(new SecureCameraPerformanceTest::ResultPrinter);

    int opt;
    while ((opt = getopt(argc, argv, "gn")) != -1)
    {
        switch (opt)
        {
            case 'g':
                runGoogleTest();
                break;
            case 'n':
                runTest();
                break;
            default:
                break;
        }
    }

    return ::android::OK;
}
