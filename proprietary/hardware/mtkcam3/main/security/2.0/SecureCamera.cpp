/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "SecureCamera"

#include "SecureCamera.h"

#include "RgbPath.h"

#include <linux/ioctl.h>
#include <fcntl.h>

#include <mtkcam3/main/security/utils/BufferQueue.h>
#include <mtkcam3/main/security/utils/Debug.h>
#include <mtkcam/utils/std/TypeTraits.h>

#ifdef LEGACY_PATH // legacy path
#define RESIZED_RAW_OUTPUT
#endif

using namespace NSCam::security::V2_0;
using namespace NSCam::security::utils;

// ---------------------------------------------------------------------------

// the amount of ISP working buffers
static constexpr uint32_t kISPWorkingBufferCount = 3;

// ---------------------------------------------------------------------------

ISecureCamera* ISecureCamera::loadModule()
{
    return new SecureCamera();
}

// the class factories
extern "C" ISecureCamera* getSecureCamera()
{
    return ISecureCamera::loadModule();
}

// ---------------------------------------------------------------------------

Path IPath::sPath(Path::INVALID);

template <typename IMPL>
shared_ptr<IMPL> IPath::createInstance(int maxBuffers, Path path, IPath** interface)
{
    SEC_LOGD("create instance: maxBuffers(%d), path(%x)", maxBuffers, path);

    sPath = path;

    std::shared_ptr<IMPL> __impl(new IMPL(maxBuffers));
    *interface = __impl.get();

    return __impl;
}

Path IPath::getPath()
{
    return sPath;
}

// ---------------------------------------------------------------------------

SecureCamera::SecureCamera()
{
    AutoLog();
}

SecureCamera::~SecureCamera()
{
    AutoLog();
}

Result SecureCamera::open(Path path)
{
    AutoLog();
    mSecureCameraProxy.reset(new SecureCameraProxy(path));

    return OK;
}

Result SecureCamera::close()
{
    AutoLog();
    mSecureCameraProxy = nullptr;

    return OK;
}

Result SecureCamera::init()
{
    AutoLog();

    if (mSecureCameraProxy->init() != OK)
    {
        SEC_LOGE("camera init failed");
        unInit();
        return NO_INIT;
    }

    return OK;
}

Result SecureCamera::unInit()
{
    AutoLog();
    if (mSecureCameraProxy != nullptr)
      return mSecureCameraProxy->unInit();

    return OK;
}

Result SecureCamera::sendCommand(Command cmd, intptr_t arg1, intptr_t arg2)
{
    AutoLog();
    SEC_LOGD("sendCommand cmd(%d)", cmd);

    switch (cmd)
    {
        case Command::STREAMING_ON:
        {
            SEC_LOGD("STREAMING_ON");
            return mSecureCameraProxy->streamingOn();
        }
        case Command::STREAMING_OFF:
        {
            SEC_LOGD("STREAMING_OFF");
            return mSecureCameraProxy->streamingOff();
        }
        case Command::REGISTER_CB_FUNC:
        {
            SEC_LOGD("REGISTER_CB_FUNC");
            return mSecureCameraProxy->registerCallback(
                    reinterpret_cast<SecureCameraCallback*>(arg1),
                    reinterpret_cast<void*>(arg2));
        }
        case Command::SET_SRC_DEV:
        {
            SEC_LOGD("SET_SRC_DEV");
            return mSecureCameraProxy->setSrcDev(arg1);
        }
        default:
        {
            SEC_LOGE("unknown command(%u)", NSCam::toLiteral<Command>(cmd));
            return INVALID_OPERATION;
        }
    }
    return OK;
}

// ---------------------------------------------------------------------------

SecureCameraProxy::SecureCameraProxy(Path path)
    : mPath(nullptr)
{
    AutoLog();

    mRGBPath = IPath::createInstance<RgbPath>(kISPWorkingBufferCount, path, &mPath);

    if (!mPath)
        SEC_LOGE("create IPath failed");

    mFullSizedStream.reset(new BufferQueue(kISPWorkingBufferCount));

#ifdef RESIZED_RAW_OUTPUT
    mResizedStream.reset(new BufferQueue(kISPWorkingBufferCount));
#endif
}

SecureCameraProxy::~SecureCameraProxy()
{
    AutoLog();
    // TODO: reset IPath::sPath here
}

Result SecureCameraProxy::init()
{
    AutoLog();

    std::unordered_map<StreamID, Stream> streamMap;

    // NOTE: Full-sized stream must exist
    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        if (!mFullSizedStream)
        {
            SEC_LOGE("Invalid buffer queue");
            return NO_INIT;
        }
        streamMap.emplace(StreamID::FULL_RAW, mFullSizedStream);
    }

    {
        std::lock_guard<std::mutex> _l(mResizedStreamLock);
        if (mResizedStream)
            streamMap.emplace(StreamID::RESIZED_RAW, mResizedStream);
    }

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mPath)
        {
            SEC_LOGE("Invalid path");
            return NO_INIT;
        }
        mPath->init(streamMap);
    }

    return OK;
}

Result SecureCameraProxy::unInit()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (mPath)
        {
            // turn sensor off
            err = mPath->sensorPower(false);
            if (err != OK)
            {
                SEC_LOGE("power sensor off failed");
                return err;
            }

            err = mPath->unInit();
            if (err != OK)
            {
                SEC_LOGE("uninit failed");
                return err;
            }

            mPath->destroyInstance();
        }
    }

    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        // NOTE: If use_count returns 1, there are no other owners.
        if (mFullSizedStream && (mFullSizedStream.use_count() > 1))
            SEC_LOGW("BufferQueueFullSized is managed and not release elsewhere");

        mFullSizedStream = nullptr;
    }

    return err;
}

Result SecureCameraProxy::streamingOn()
{
    AutoLog();

    Result err = OK;

    // configure buffer queue parameters
    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (mPath->StreamingOn() != OK)
        {
            SEC_LOGE("preview start fail(err=0x%x)",err);
            return UNKNOWN_ERROR;
        }
    }

    return OK;
}

Result SecureCameraProxy::streamingOff()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mPath)
        {
            SEC_LOGE("Invalid path");
            return NO_INIT;
        }
        err = mPath->StreamingOff();
        SEC_LOGE_IF(err != OK, "streaming off failed");
    }

    return err;
}

Result SecureCameraProxy::registerCallback(SecureCameraCallback* cb, void* priv)
{
    AutoLog();

    if (CC_UNLIKELY(!mRGBPath))
    {
        SEC_LOGE("invalid RGB path");
        return NO_INIT;
    }
    // callback to RGB path is realized by the derived class
    mRGBPath->registerCallback(cb, priv);

    return OK;
}

Result SecureCameraProxy::setSrcDev(unsigned int openID)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mPathLock);
    if (!mPath)
    {
        SEC_LOGE("Invalid path");
        return NO_INIT;
    }

    Result err = OK;

    err = mPath->setSrcDev(openID);
    if (err != OK)
    {
        SEC_LOGE("set source camera device failed");
        return err;
    }

    // turn sensor on
    err = mPath->sensorPower(true);
    if (err != OK)
    {
        SEC_LOGE("power sensor on failed");
        return err;
    }

    return err;
}
