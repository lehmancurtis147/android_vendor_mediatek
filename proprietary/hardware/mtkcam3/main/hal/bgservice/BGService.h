#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_BGSERVICE_V1_0_BGSERVICE_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_BGSERVICE_V1_0_BGSERVICE_H

#include <vendor/mediatek/hardware/camera/bgservice/1.0/IBGService.h>
#include <hidl/MQDescriptor.h>
#include <hidl/Status.h>
#include <utils/Mutex.h> // android::Mutex

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace bgservice {
namespace V1_0 {
namespace implementation {

using ::android::hardware::hidl_array;
using ::android::hardware::hidl_memory;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::Return;
using ::android::hardware::Void;
using ::android::sp;

struct BGService : public IBGService {
    // Methods from ::vendor::mediatek::hardware::camera::bgservice::V1_0::IBGService follow.
    Return<int32_t> setEventCallback(int32_t ImgReaderId, const sp<::vendor::mediatek::hardware::camera::bgservice::V1_0::IEventCallback>& callback) override;

    // Methods from ::android::hidl::base::V1_0::IBase follow.

};

// FIXME: most likely delete, this is only for passthrough implementations
extern "C" IBGService* HIDL_FETCH_IBGService(const char* name);

/*
* Wrapper for calling BGServiceMgr
*/
class BGServiceWrap {

public:
    static BGServiceWrap* getInstance();

    Return<int32_t> setEventCallback(int32_t ImgReaderId,
                                     const sp<IEventCallback>& callback);

    bool onRequestCallback( int32_t const ImgReaderId,
                            int32_t  const frameNumber,
                            bool            status);

    static bool requestCallback( int32_t const ImgReaderId,
                                 int32_t  const frameNumber,
                                 bool            status);

private:
     BGServiceWrap() {};
    ~BGServiceWrap() {};



private:
    sp<IEventCallback>                       mEventCallback = nullptr;
    mutable android::Mutex                   mLock;
};// class BGServiceWrap


}  // namespace implementation
}  // namespace V1_0
}  // namespace bgservice
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor

#endif  // VENDOR_MEDIATEK_HARDWARE_CAMERA_BGSERVICE_V1_0_BGSERVICE_H
