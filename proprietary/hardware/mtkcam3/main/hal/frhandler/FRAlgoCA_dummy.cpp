
#define LOG_TAG "FRAlgoCA_dummy"

#include "FRAlgoCA_dummy.h"
#include <fr_err_def.h>

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>
#include <cutils/properties.h>

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <mtkcam/utils/std/Format.h>

#define DEBUG_FRALGOCA_DUMMY_LOG_LEVEL "debug.fralgoca_dummy.loglevel"
#define FRALGOCAAS_LOG_DEF_VALUE 1

#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif

#if 1
#define FUNC_START if (mLogLevel) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (mLogLevel) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif



#ifdef MTEE_USED
#ifdef MTEE_FR_SVC_NAME
    #undef MTEE_FR_SVC_NAME
#endif
#define MTEE_FR_SVC_NAME "com.mediatek.geniezone.fralgo"
#endif

// === mkdbg: debug usage ===
#define FRALGOCA_DUMMY_DUMP_BUFFER 1
#define FRALGOCA_DUMMY_LOG_ON 1
#define FRALGOCA_DUMMY_LOG_OFF 0
// === mkdbg: end

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

FRAlgoCA_dummy::FRAlgoCA_dummy(char const *pCaller, const FRCFG_ENUM frConfig)
    :FRAlgoCA(pCaller, frConfig)
{
    FUNC_START;
    mIsInited = 0;
    reset();
    FUNC_END;
}

FRAlgoCA_dummy::~FRAlgoCA_dummy()
{
    FUNC_START;
    reset();
    FUNC_END;
}

ISecureCameraClientCallback* FRAlgoCA_dummy::getSecureCameraClientCallback()
{
    if (mSecureCamCliCb == NULL)
    {
        return new FRAlgoCA_dummy::SecureCamClientCallback(this);
    }
    else
    {
        return mSecureCamCliCb.get();
    }
}

int FRAlgoCA_dummy::init()
{
    // step-1: create session
    // step-2: create SecureCam and call its init(..) function

    FUNC_START;

    Mutex::Autolock lock(mLock);

    int ret = 0;

    if (mIsInited != 0)
    {
        ALOGE("%s::FRAlgoCA is already inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_ALREADY_INITED; // already initied
    }

    if ((ret = FRAlgoCA::tee_init(MTEE_FR_SVC_NAME)) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::seccam_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::fralgo_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    mState = FRCA_STATE_ALIVE;
    mIsInited = 1;

    return FR_ERR_OK;

err_init:

    ALOGW("%s:: FRAlgoCA init failed %d", __FUNCTION__, ret);
    reset();

    FUNC_END;
    return ret;
}

int FRAlgoCA_dummy::uninit()
{
    // FR-custom
    FUNC_START;

    {
        Mutex::Autolock lock(mLock);
        mState = FRCA_STATE_UNINIT; // change uninit state first to stop FRWorkThread and OnBufferThread
    }

    Mutex::Autolock lock(mLock);
    reset();
    
    FUNC_END;
    return FR_ERR_OK;
}

void FRAlgoCA_dummy::reset()
{
    FUNC_START;
    FRAlgoCA::reset();

    // TODO: reset other member var here
    FUNC_END;
    return;
}

int FRAlgoCA_dummy::fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;
    int ret = FR_ERR_ALGO_OK;
    TZ_RESULT retTee;
    MTEEC_PARAM param[4];
    uint32_t types;

    HandleImporter gHandleImporter;
    buffer_handle_t importedBuffer = buffer.getNativeHandle();

    gHandleImporter.importBuffer(importedBuffer);

    ALOGD("%s: bufferId(%" PRIu64 ") FD(%d) imported FD(%d) dataSize(%llu) size(%dx%d) stride(%llu) format(0x%x)",
            __FUNCTION__, bufferId,
            buffer.getNativeHandle()->data[0], importedBuffer->data[0],
            stream.size, stream.width, stream.height, stream.stride, stream.format);


    if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
    {
        retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_SAVE_FEATURE, types, param);

        // TODO:
        // 1. prepare FRLib input/buffer and send to TA
        // 2. prepare dumpBuffer debug method here if any
        // default: param[0].value.b is for FRlib return value
        ret = param[0].value.b;

    }
    else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_COMPARE_FEATURE, types, param);
        // TODO:
        // 1. prepare param with FRLib input/buffer and send to TA
        // 2. prepare dumpBuffer debug method here if any

        // default: param[0].value.b is for FRlib return value
        ret = param[0].value.b;
    }
    else
    {
        // do nothing
        ALOGW("%s: wrong cmd: %d", __FUNCTION__, mCurrCmd);
        ret = FR_ERR_FRALGO_WRONG_CMD;
    }

    gHandleImporter.freeBuffer(importedBuffer);

    return FR_ERR_OK;

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

Return<void> FRAlgoCA_dummy::SecureCamClientCallback::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
// FR-custom
    ALOGD("%s +", __FUNCTION__);

    int ret = FR_ERR_OK;

    // === Error Handling ===
    if (status != Status::OK)
    {
        ALOGW("%s: !!!err!!!: status(%d), bufferId(%" PRIu64 ") buffer(%p) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
                __FUNCTION__, 
                status, 
                bufferId,
                buffer.getNativeHandle(),
                stream.size, 
                stream.width, 
                stream.height, 
                stream.stride, 
                stream.format);

        ALOGD("%s -: err: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_STATUS_ERROR", __FUNCTION__);
        return Void();
    }

    buffer_handle_t importedBuffer = buffer.getNativeHandle();
    if (buffer == nullptr || importedBuffer == NULL)
    {
        ALOGE("%s -: status: %d: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_BUF_ERROR", __FUNCTION__, status);
        return Void();
    }

    ALOGD("%s: status(%d), bufferId(%" PRIu64 ") buffer(%p, data[0]=%d, numFds=%d, numInts=%d) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
            __FUNCTION__,
            status, 
            bufferId,
            buffer.getNativeHandle(), importedBuffer->data[0], importedBuffer->numFds,  importedBuffer->numInts,
            stream.size,
            stream.width,
            stream.height,
            stream.stride,
            stream.format);

    // === Start Processing ===
    // Area where Mutex lock is needed
    const sp<FRAlgoCA> spFRAlgoCA_dummy = mwpFRAlgoCA_dummy.promote();
    if (spFRAlgoCA_dummy != NULL)
    {
        FRCA_STATE currState = spFRAlgoCA_dummy->getFRCAState();
        if (currState == FRCA_STATE_UNINIT || currState == FRCA_STATE_STOP_FEATURE)
        {
            ALOGD("%s: State: FRCA_STATE=%d, no need to process further", __FUNCTION__, currState);
            return Void();
        }

        Mutex::Autolock lock(spFRAlgoCA_dummy->mLock);

        ret = spFRAlgoCA_dummy->fralgo_prepareFRInput(bufferId, buffer, stream);
        if (ret != FR_ERR_OK)
        {
            ALOGE("%s: FRAlgoCA_AS::prepareFRInput err: %d", __FUNCTION__, ret);
        }

        // callback to FRHandler
        FRCA_CMD currCmd = spFRAlgoCA_dummy->getCurrCmd();
        spFRAlgoCA_dummy->callbackFRAlgoCAClient(currCmd, ret);
    }

    ALOGD("%s -", __FUNCTION__);

    return Void();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

