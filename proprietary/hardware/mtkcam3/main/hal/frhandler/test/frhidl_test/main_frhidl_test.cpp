/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "main_frhidl_test"

#include <mtkcam/utils/std/Log.h>

//#define DEBUG_LOG_TAG "main_frhidl_test"
//#include <mtkcam/main/security/utils/Debug.h>

#include <utils/Errors.h>

#include "HandleImporter.h"

#include <condition_variable>

//#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCamera.h>
//#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCameraClientCallback.h>

#include <vendor/mediatek/hardware/camera/frhandler/1.0/IFRHandler.h>
#include <vendor/mediatek/hardware/camera/frhandler/1.0/IFRClientCallback.h>

#include <mtkcam3/main/security/utils/IonHelper.h>

#include "FRHandler_def.h"
#include "fr_err_def.h"

using namespace ::android;
using namespace ::android::hardware::camera::common::V1_0;
using namespace vendor::mediatek::hardware::camera::security::V1_0;

using namespace vendor::mediatek::hardware::camera::frhandler::V1_0;

using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_handle;
using ::android::hardware::camera::common::V1_0::helper::HandleImporter;

#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif

#if 1
#define LOG_LEVEL 1
#define FUNC_START if (LOG_LEVEL >= 1) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (LOG_LEVEL >= 1) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif


// === Util function ===
void consumeUtilEnterKeyReached()
{
    char c = 'q';
    do {
        c = (char) getchar(); // consume '\n' key
        if (c != '\n')
        {
            printf("!!warning: NOT '\n' key, but '%c' !\n", c);
        }
    } while (c != '\n');

    return;
}

// === HalInterface ===
// ------------------------------------------------------------------------

static sp<IFRHandler> gHidlFRHandler = NULL;

static HandleImporter gHandleImporter;

static constexpr uint64_t kCookie = 0;
static FRCFG_ENUM gFRCfg = FRCFG_DUMMY;

#define TEST_USER_NAME "test111"
// ------------------------------------------------------------------------

class MyHidlDeathReceiver : public hardware::hidl_death_recipient
{
public:
    virtual void serviceDied(uint64_t cookie, const ::android::wp<::android::hidl::base::V1_0::IBase>& who)
    {
        (void) who;
        CAM_LOGI("SecureCamera died");
    }
};

class MyFRClientCallback : public IFRClientCallback
{
public:
    hardware::Return<void> onFRClientCallback(
                int32_t fr_cmd_id, int32_t errCode, const FRCbParam& frCbParam)
    {
        CAM_LOGI("%s invoked callled");
        return hardware::Void();
    }
};
/**
 * Adapter for HIDL HAL interface calls; calls into the HIDL HAL interfaces.
 */
class HalInterface : virtual public RefBase
{
public:
    HalInterface();
    virtual ~HalInterface();

    const sp<::android::hardware::hidl_death_recipient>
        getHidlDeathRecipient() const { return mClientCallback.get(); }

    const sp<IFRClientCallback> getFRClientCallback() const
    { return mClientCallback; }

private:
    uint64_t mHidlDeviceId;

    struct ClientCallback :
        virtual public IFRClientCallback,
        virtual public hardware::hidl_death_recipient
    {
        ClientCallback(const wp<HalInterface>& interface) : mInterface(interface) {}

        // Implementation of hidl_death_recipient interface
        virtual void serviceDied(
                uint64_t cookie,
                const wp<hidl::base::V1_0::IBase>& who) override;

        // Implementation of vendor::mediatek::hardware::camera::frhandler::V1_0::IFRClientCallback
        virtual hardware::Return<void> onFRClientCallback(
                int32_t fr_cmd_id, int32_t errCode, const FRCbParam& param) override;

        wp<HalInterface> mInterface;
    };

    sp<ClientCallback> mClientCallback;
};

HalInterface::HalInterface()
    : mHidlDeviceId(0)
{
    // register the recipient for a notification if the hwbinder object goes away
    mClientCallback = new HalInterface::ClientCallback(this);
}

HalInterface::~HalInterface()
{
}

void HalInterface::ClientCallback::serviceDied(
        uint64_t cookie, const wp<hidl::base::V1_0::IBase>& who)
{
    (void) who;
    CAM_LOGI("FRHandler has died");
    if (cookie != kCookie) {
        CAM_LOGW("%s: Unexpected serviceDied cookie %" PRIu64 ", expected %" PRIu32,
                __FUNCTION__, cookie, kCookie);
    }

    // TODO: do something here if necessary when the HAL server has gone

    // release HIDL interface
    auto parent = mInterface.promote();
    if (parent.get())
        parent->mClientCallback.clear();
}

hardware::Return<void> HalInterface::ClientCallback::onFRClientCallback(
                int32_t fr_cmd_id, int32_t errCode, const FRCbParam& frCbParam)
{
    CAM_LOGI("%s +", __FUNCTION__);

    buffer_handle_t importedBuffer = nullptr;

    if (frCbParam.buffer == nullptr)
    {
        CAM_LOGD("mkdbg: %s: fr_cmd_id: %d, errCodew: %d, frCbParam.buffer=nullptr, FpCbParam.stream=(%d, %d, %d, %d, %d)",
                __FUNCTION__, fr_cmd_id, errCode,
                frCbParam.stream.size, frCbParam.stream.width, frCbParam.stream.height, frCbParam.stream.stride, frCbParam.stream.format);
    }
    else
    {
        buffer_handle_t importedBuffer = frCbParam.buffer.getNativeHandle();
        gHandleImporter.importBuffer(importedBuffer);
        
        CAM_LOGD("mkdbg: %s: fr_cmd_id: %d, errCodew: %d, frCbParam.buffer=%p, FpCbParam.stream=(%d, %d, %d, %d, %d)",
                __FUNCTION__, fr_cmd_id, errCode, frCbParam.buffer.getNativeHandle(),
                frCbParam.stream.size, frCbParam.stream.width, frCbParam.stream.height, frCbParam.stream.stride, frCbParam.stream.format);
            // dump buffer here
        {
                NSCam::security::utils::IonHelper helper;
                const size_t dataSize(frCbParam.stream.size);
                const int sharedFD(importedBuffer->data[0]);
        
        //  [EXAMPLE 1]: get virtual address from file descriptor
        //        void *addr = helper.mmap(
        //                NULL, dataSize, PROT_READ | PROT_WRITE, MAP_SHARED, sharedFD);
        //
        //        CAM_LOGD("%s: bufferAddr(%0x" PRIxPTR ") dataSize(%zu) sharedFD(%d)",
        //                __FUNCTION__, addr, dataSize, sharedFD);
        //
        //        helper.munmap(addr, dataSize);
        
        //  [EXAMPLE 2]: dump buffer
        //        helper.dumpBuffer(sharedFD, dataSize, stream.width, stream.height,
        //                stream.stride, stream.format);
        }
            
        gHandleImporter.freeBuffer(importedBuffer);
    }

    CAM_LOGI("%s -", __FUNCTION__);

    return hardware::Void();
}

// ------------------------------------------------------------------------

int testSaveFeatureProcess()
{
        // connect to secure camera
        CAM_LOGI("=== mkdbg: IFRHandler getService test ===");
        gHidlFRHandler= IFRHandler::getService("internal/0");
        if (gHidlFRHandler == nullptr) {
            CAM_LOGE("failed to get FRHandler HIDL i/f");
            return ::android::NO_INIT;
        }
        else {
            CAM_LOGD("get FRHandler HIDL i/f done: gHidlFRHandler=%p", gHidlFRHandler.get());
        }
    
        ::android::hardware::camera::common::V1_0::Status err =
            ::android::hardware::camera::common::V1_0::Status::OK;
    
        CAM_LOGI("=== mkdbg: linkToDeath test ===");

        sp<HalInterface> halInterface = new HalInterface();
        CAM_LOGI("halInterface: %p, hal->deathRecipient=%p", halInterface.get(), halInterface->getHidlDeathRecipient().get());

#if 1 // ori
        hardware::Return<bool> linked =
            gHidlFRHandler->linkToDeath(
                    halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);
#else // test-only
        sp<MyHidlDeathReceiver> spHidlDeathRecipient = new MyHidlDeathReceiver();
        hardware::Return<bool> linked =
            gHidlFRHandler->linkToDeath(
                    spHidlDeathRecipient, /*cookie*/ kCookie);
#endif
        if (!linked.isOk()) {
            CAM_LOGE("%s: Transaction error in linking to FRHandler death: %s",
                    __FUNCTION__, linked.description().c_str());
            return ::android::UNKNOWN_ERROR;
        } else if (!linked) {
            CAM_LOGW("%s: Unable to link to FRHandler death notifications",
                    __FUNCTION__);
        }
    
        CAM_LOGI("=== mkdbg: IFRHandler::getFRImageFormat() test ===");
        // get camera id list
        std::vector<FRImageFormat> vecImageFormat;
        
        gHidlFRHandler->getFRImageFormat([&err, &vecImageFormat](
                ::android::hardware::camera::common::V1_0::Status idStatus,
                const hidl_vec<FRImageFormat>& vImageFormat) {
            err = idStatus;
    
            CAM_LOGD("mkdbg: getFRImageFormat CB: size: %d", vImageFormat.size());
    
            if (err == Status::OK) {
                for (size_t i = 0; i < vImageFormat.size(); i++) {
                    vecImageFormat.push_back(vImageFormat[i]);
                }
            } });
    
        int k = 0;
        for (auto&& imgFormat : vecImageFormat)
        {
            CAM_LOGD("mkdbg: loop_cnt=%d", ++k);
            CAM_LOGD("mkdbg: size: %d", imgFormat.stream.size);
            CAM_LOGD("mkdbg: stride: %d", imgFormat.stream.stride);
            CAM_LOGD("mkdbg: width: %d", imgFormat.stream.width);
            CAM_LOGD("mkdbg: height: %d", imgFormat.stream.height);
            CAM_LOGD("mkdbg: format: %d", imgFormat.stream.format);
        }
    
        if (vecImageFormat.empty())
        {
            CAM_LOGW("unavailable image format exit...");
            return ::android::BAD_VALUE;
        }
    
        // enable this part to check binder death notification
        //getchar();
    
        ::android::hardware::Return<int32_t> my_err_int = 0;
    
    // to avoid previous Monkey's failture, call uninit first
        my_err_int = gHidlFRHandler->uninit();
        CAM_LOGI("=== mkdbg: IFRHandler::uninit() test: ret =%d ===", (int32_t) my_err_int);
    
#if 1 // init w/ registerCallFunc
        my_err_int = gHidlFRHandler->init((int)gFRCfg, halInterface->getFRClientCallback());
        CAM_LOGI("=== mkdbg: IFRHandler::init() test: ret =%d ===", (int32_t) my_err_int);

    #if 0 // test-only
        sp<MyFRClientCallback> spFRClientCallback = new MyFRClientCallback();
        my_err_int = gHidlFRHandler->init((int)gFRCfg, spFRClientCallback);
        CAM_LOGI("=== mkdbg: IFRHandler::init() test: ret =%d ===", (int32_t) my_err_int);
    #endif
#else // init + registerCallback
        my_err_int = gHidlFRHandler->init((int)gFRCfg, NULL);
        my_err_int = gHidlFRHandler->registerCallback(halInterface->getFRClientCallback());
#endif
    
        CAM_LOGI("=== mkdbg: IFRHandler.registerCallback() test: ret=%d ===", (int32_t)my_err_int);
    
        my_err_int = gHidlFRHandler->setPowerMode(0);
        CAM_LOGI("=== mkdbg: IFRHandler.setPowerMode() test: ret=%d ===", (int32_t)my_err_int);
    
        {
            printf("Press any key to start SaveFeature process..\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }
    
        hidl_string userName(TEST_USER_NAME);
        my_err_int = gHidlFRHandler->startSaveFeature(userName);
        CAM_LOGI("=== mkdbg: IFRHandler.saveFeature() test: ret=%d ===", (int32_t)my_err_int);
    
        // !!NOTES: simulate callback twice after SaveFeature is called
        {
            FRCbParam tmpCbParam;
    
            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -1;
            tmpCbParam.stream.width = -1;
            tmpCbParam.stream.height = -1;
            tmpCbParam.stream.stride = -1;
            tmpCbParam.stream.format = -1;
    
            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_SAVE_FEATURE, FR_ERR_ALGO_RETURN_INVALID_ARGUMENT, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
    
            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -2;
            tmpCbParam.stream.width = -2;
            tmpCbParam.stream.height = -2;
            tmpCbParam.stream.stride = -2;
            tmpCbParam.stream.format = -2;
    
            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_SAVE_FEATURE, FR_ERR_ALGO_RETURN_NULLPTR_ERROR, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
        }
    
        {
            printf("Press any key to stopSaveFeature...\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }
    
        my_err_int = gHidlFRHandler->stopSaveFeature();
        CAM_LOGI("=== mkdbg: IFRHandler.startCompareFeature() test: ret=%d===", (int32_t)my_err_int);
    
        my_err_int = gHidlFRHandler->uninit();
        CAM_LOGI("=== mkdbg: IFRHandler::uninit() test: ret=%d ===", (int32_t)my_err_int);
        
        // TODO: get callback, consume and then return
        // disconnect from secure camera
        gHidlFRHandler.clear();
        CAM_LOGI("=== mkdbg: IFRHandler.clear() test: done ===");
        CAM_LOGI("=== mkdbg: END of %s===", LOG_TAG);
    
        return ::android::OK;
}

int testCompareFeatureProcess()
{
    FUNC_START;
    // connect to secure camera
    CAM_LOGI("=== mkdbg: IFRHandler getService test ===");
    gHidlFRHandler= IFRHandler::getService("internal/0");
    if (gHidlFRHandler == nullptr) {
        CAM_LOGE("failed to get FRHandler HIDL i/f");
        return ::android::NO_INIT;
    }
    else {
        CAM_LOGD("get FRHandler HIDL i/f done: gHidlFRHandler=%p", gHidlFRHandler.get());
    }


    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    CAM_LOGI("=== mkdbg: linkToDeath test ===");

    sp<HalInterface> halInterface = new HalInterface();
    CAM_LOGI("halInterface: %p, hal->deathRecipient=%p", halInterface.get(), halInterface->getHidlDeathRecipient().get());

    hardware::Return<bool> linked =
        gHidlFRHandler->linkToDeath(
                halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);
    if (!linked.isOk()) {
        CAM_LOGE("%s: Transaction error in linking to FRHandler death: %s",
                __FUNCTION__, linked.description().c_str());
        return ::android::UNKNOWN_ERROR;
    } else if (!linked) {
        CAM_LOGW("%s: Unable to link to FRHandler death notifications",
                __FUNCTION__);
    }

    CAM_LOGI("=== mkdbg: IFRHandler::getFRImageFormat() test ===");
    // get camera id list
    std::vector<FRImageFormat> vecImageFormat;
    
    gHidlFRHandler->getFRImageFormat([&err, &vecImageFormat](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<FRImageFormat>& vImageFormat) {
        err = idStatus;

        CAM_LOGD("mkdbg: getFRImageFormat CB: size: %d", vImageFormat.size());

        if (err == Status::OK) {
            for (size_t i = 0; i < vImageFormat.size(); i++) {
                vecImageFormat.push_back(vImageFormat[i]);
            }
        } });

    int k = 0;
    for (auto&& imgFormat : vecImageFormat)
    {
        CAM_LOGD("mkdbg: loop_cnt=%d", ++k);
        CAM_LOGD("mkdbg: size: %d", imgFormat.stream.size);
        CAM_LOGD("mkdbg: stride: %d", imgFormat.stream.stride);
        CAM_LOGD("mkdbg: width: %d", imgFormat.stream.width);
        CAM_LOGD("mkdbg: height: %d", imgFormat.stream.height);
        CAM_LOGD("mkdbg: format: %d", imgFormat.stream.format);
    }

    if (vecImageFormat.empty())
    {
        CAM_LOGW("unavailable image format exit...");
        return ::android::BAD_VALUE;
    }

    // enable this part to check binder death notification
    //getchar();

    ::android::hardware::Return<int32_t> my_err_int = 0;

// to avoid previous Monkey's failture, call uninit first
    my_err_int = gHidlFRHandler->uninit();
    CAM_LOGI("=== mkdbg: IFRHandler::unint() test: ret =%d ===", (int32_t) my_err_int);
    
#if 1 // init w/ registerCallFunc
    my_err_int = gHidlFRHandler->init((int)gFRCfg, halInterface->getFRClientCallback());
    CAM_LOGI("=== mkdbg: IFRHandler::init() test: ret =%d ===", (int32_t) my_err_int);
#else // init + registerCallback
    my_err_int = gHidlFRHandler->init((int)gFRCfg, NULL);
    my_err_int = gHidlFRHandler->registerCallback(halInterface->getFRClientCallback());
#endif
    
    CAM_LOGI("=== mkdbg: IFRHandler.registerCallback() test: ret=%d ===", (int32_t)my_err_int);

    my_err_int = gHidlFRHandler->setPowerMode(0);
    CAM_LOGI("=== mkdbg: IFRHandler.setPowerMode() test: ret=%d ===", (int32_t)my_err_int);
    
    {
        printf("Press any key to prepare start CompareFeature process....\n");
        fflush(stdout);
        char c = (char) getchar();
        if (c != '\n')
        {
            consumeUtilEnterKeyReached();
        }
    }

    hidl_string userName(TEST_USER_NAME);
    my_err_int = gHidlFRHandler->startCompareFeature(userName);
    CAM_LOGI("=== mkdbg: IFRHandler.startCompareFeature() test: ret=%d===", (int32_t)my_err_int);

    // !!NOTES: simulate callback twice after CompareFeature is called
    {
        FRCbParam tmpCbParam;

        tmpCbParam.buffer = nullptr;
        tmpCbParam.stream.size = -3;
        tmpCbParam.stream.width = -3;
        tmpCbParam.stream.height = -3;
        tmpCbParam.stream.stride = -3;
        tmpCbParam.stream.format = -3;

        my_err_int = gHidlFRHandler->simCbRequest(
                FRALGOCA_CMD_START_COMPARE_FEATURE, FR_ERR_ALGO_RETURN_MALLOC_FAILED, tmpCbParam);
        CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);

        tmpCbParam.buffer = nullptr;
        tmpCbParam.stream.size = -4;
        tmpCbParam.stream.width = -4;
        tmpCbParam.stream.height = -4;
        tmpCbParam.stream.stride = -4;
        tmpCbParam.stream.format = -4;

        my_err_int = gHidlFRHandler->simCbRequest(
                FRALGOCA_CMD_START_COMPARE_FEATURE, FR_ERR_ALGO_RETURN_NO_IMAGE_DATA, tmpCbParam);
        CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
    }

    {
        printf("Press any key to stop CompareFeature...\n");
        fflush(stdout);
        char c = (char) getchar();
        if (c != '\n')
        {
            consumeUtilEnterKeyReached();
        }
    }

    my_err_int = gHidlFRHandler->stopCompareFeature();
    CAM_LOGI("=== mkdbg: IFRHandler.stopCompareFeature() test: ret=%d ===", (int32_t)my_err_int);

    my_err_int = gHidlFRHandler->uninit();
    CAM_LOGI("=== mkdbg: IFRHandler::uninit() test: ret=%d ===", (int32_t)my_err_int);
    
    // TODO: get callback, consume and then return
    // disconnect from secure camera
    gHidlFRHandler.clear();
    CAM_LOGI("=== mkdbg: IFRHandler.clear() test: done ===");
    CAM_LOGI("=== mkdbg: END of %s===", LOG_TAG);

    FUNC_END;
    return ::android::OK;

}

int testDeleteFeatureProcess()
{
    FUNC_START;

    // connect to secure camera
    CAM_LOGI("=== mkdbg: IFRHandler getService test ===");
    gHidlFRHandler= IFRHandler::getService("internal/0");
    if (gHidlFRHandler == nullptr) {
        CAM_LOGE("failed to get FRHandler HIDL i/f");
        return ::android::NO_INIT;
    }
    else {
        CAM_LOGD("get FRHandler HIDL i/f done: gHidlFRHandler=%p", gHidlFRHandler.get());
    }

    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;

    CAM_LOGI("=== mkdbg: linkToDeath test ===");

    sp<HalInterface> halInterface = new HalInterface();
    CAM_LOGI("halInterface: %p, hal->deathRecipient=%p", halInterface.get(), halInterface->getHidlDeathRecipient().get());

    hardware::Return<bool> linked =
        gHidlFRHandler->linkToDeath(
                halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);
    if (!linked.isOk()) {
        CAM_LOGE("%s: Transaction error in linking to FRHandler death: %s",
                __FUNCTION__, linked.description().c_str());
        return ::android::UNKNOWN_ERROR;
    } else if (!linked) {
        CAM_LOGW("%s: Unable to link to FRHandler death notifications",
                __FUNCTION__);
    }

    CAM_LOGI("=== mkdbg: IFRHandler::getFRImageFormat() test ===");
    // get camera id list
    std::vector<FRImageFormat> vecImageFormat;
    
    gHidlFRHandler->getFRImageFormat([&err, &vecImageFormat](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<FRImageFormat>& vImageFormat) {
        err = idStatus;

        CAM_LOGD("mkdbg: getFRImageFormat CB: size: %d", vImageFormat.size());

        if (err == Status::OK) {
            for (size_t i = 0; i < vImageFormat.size(); i++) {
                vecImageFormat.push_back(vImageFormat[i]);
            }
        } });

    int k = 0;
    for (auto&& imgFormat : vecImageFormat)
    {
        CAM_LOGD("mkdbg: loop_cnt=%d", ++k);
        CAM_LOGD("mkdbg: size: %d", imgFormat.stream.size);
        CAM_LOGD("mkdbg: stride: %d", imgFormat.stream.stride);
        CAM_LOGD("mkdbg: width: %d", imgFormat.stream.width);
        CAM_LOGD("mkdbg: height: %d", imgFormat.stream.height);
        CAM_LOGD("mkdbg: format: %d", imgFormat.stream.format);
    }

    if (vecImageFormat.empty())
    {
        CAM_LOGW("unavailable image format exit...");
        return ::android::BAD_VALUE;
    }

    // enable this part to check binder death notification
    //getchar();

    ::android::hardware::Return<int32_t> my_err_int = 0;

// to avoid previous Monkey's failture, call uninit first
    my_err_int = gHidlFRHandler->uninit();
    CAM_LOGI("=== mkdbg: IFRHandler::unint() test: ret =%d ===", (int32_t) my_err_int);
    
#if 1 // init w/ registerCallFunc
        my_err_int = gHidlFRHandler->init((int)gFRCfg, halInterface->getFRClientCallback());
        CAM_LOGI("=== mkdbg: IFRHandler::init() test: ret =%d ===", (int32_t) my_err_int);
#else // init + registerCallback
        my_err_int = gHidlFRHandler->init((int)gFRCfg, NULL);
        my_err_int = gHidlFRHandler->registerCallback(halInterface->getFRClientCallback());
#endif

    CAM_LOGI("=== mkdbg: IFRHandler.registerCallback() test: ret=%d ===", (int32_t)my_err_int);

    {
        printf("Press any key to prepare start deleteFeature process....\n");
        fflush(stdout);
        char c = (char) getchar();
        if (c != '\n')
        {
            consumeUtilEnterKeyReached();
        }
    }

    hidl_string userName(TEST_USER_NAME);
    my_err_int = gHidlFRHandler->deleteFeature(userName);
    CAM_LOGI("=== mkdbg: IFRHandler.deleteFeature() test: ret=%d ===", (int32_t)my_err_int);

    my_err_int = gHidlFRHandler->uninit();
    CAM_LOGI("=== mkdbg: IFRHandler::uninit() test: ret=%d ===", (int32_t)my_err_int);
    
    // disconnect from secure camera
    gHidlFRHandler.clear();
    CAM_LOGI("=== mkdbg: IFRHandler.clear() test: done ===");
    CAM_LOGI("=== mkdbg: END of %s===", LOG_TAG);

    FUNC_END;

    return ::android::OK;
}

int testSaveCompareDeleteFeatureProcess()
{
        // connect to secure camera
        CAM_LOGI("=== mkdbg: IFRHandler getService test ===");
        gHidlFRHandler= IFRHandler::getService("internal/0");
        if (gHidlFRHandler == nullptr) {
            CAM_LOGE("failed to get FRHandler HIDL i/f");
            return ::android::NO_INIT;
        }
        else {
            CAM_LOGD("get FRHandler HIDL i/f done: gHidlFRHandler=%p", gHidlFRHandler.get());
        }
    
        ::android::hardware::camera::common::V1_0::Status err =
            ::android::hardware::camera::common::V1_0::Status::OK;
    
        CAM_LOGI("=== mkdbg: linkToDeath test ===");
    
        sp<HalInterface> halInterface = new HalInterface();
        CAM_LOGI("halInterface: %p, hal->deathRecipient=%p", halInterface.get(), halInterface->getHidlDeathRecipient().get());

        hardware::Return<bool> linked =
            gHidlFRHandler->linkToDeath(
                    halInterface->getHidlDeathRecipient(), /*cookie*/ kCookie);
        if (!linked.isOk()) {
            CAM_LOGE("%s: Transaction error in linking to FRHandler death: %s",
                    __FUNCTION__, linked.description().c_str());
            return ::android::UNKNOWN_ERROR;
        } else if (!linked) {
            CAM_LOGW("%s: Unable to link to FRHandler death notifications",
                    __FUNCTION__);
        }
    
        CAM_LOGI("=== mkdbg: IFRHandler::getFRImageFormat() test ===");
        // get camera id list
        std::vector<FRImageFormat> vecImageFormat;
        
        gHidlFRHandler->getFRImageFormat([&err, &vecImageFormat](
                ::android::hardware::camera::common::V1_0::Status idStatus,
                const hidl_vec<FRImageFormat>& vImageFormat) {
            err = idStatus;
    
            CAM_LOGD("mkdbg: getFRImageFormat CB: size: %d", vImageFormat.size());
    
            if (err == Status::OK) {
                for (size_t i = 0; i < vImageFormat.size(); i++) {
                    vecImageFormat.push_back(vImageFormat[i]);
                }
            } });
    
        int k = 0;
        for (auto&& imgFormat : vecImageFormat)
        {
            CAM_LOGD("mkdbg: loop_cnt=%d", ++k);
            CAM_LOGD("mkdbg: size: %d", imgFormat.stream.size);
            CAM_LOGD("mkdbg: stride: %d", imgFormat.stream.stride);
            CAM_LOGD("mkdbg: width: %d", imgFormat.stream.width);
            CAM_LOGD("mkdbg: height: %d", imgFormat.stream.height);
            CAM_LOGD("mkdbg: format: %d", imgFormat.stream.format);
        }
    
        if (vecImageFormat.empty())
        {
            CAM_LOGW("unavailable image format exit...");
            return ::android::BAD_VALUE;
        }
    
        // enable this part to check binder death notification
        //getchar();
    
        ::android::hardware::Return<int32_t> my_err_int = 0;
    
    // to avoid previous Monkey's failture, call uninit first
        my_err_int = gHidlFRHandler->uninit();
        CAM_LOGI("=== mkdbg: IFRHandler::unint() test: ret =%d ===", (int32_t) my_err_int);
    
#if 1 // init w/ registerCallFunc
        my_err_int = gHidlFRHandler->init((int)gFRCfg, halInterface->getFRClientCallback());
        CAM_LOGI("=== mkdbg: IFRHandler::init() test: ret =%d ===", (int32_t) my_err_int);
#else // init + registerCallback
        my_err_int = gHidlFRHandler->init((int)gFRCfg, NULL);
        my_err_int = gHidlFRHandler->registerCallback(halInterface->getFRClientCallback());
#endif
    
        CAM_LOGI("=== mkdbg: IFRHandler.registerCallback() test: ret=%d ===", (int32_t)my_err_int);
    
        my_err_int = gHidlFRHandler->setPowerMode(0);
        CAM_LOGI("=== mkdbg: IFRHandler.setPowerMode() test: ret=%d ===", (int32_t)my_err_int);
    
        {
            printf("Press any key to start SaveFeature process..\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }
    
        hidl_string userName(TEST_USER_NAME);
        my_err_int = gHidlFRHandler->startSaveFeature(userName);
        CAM_LOGI("=== mkdbg: IFRHandler.saveFeature() test: ret=%d ===", (int32_t)my_err_int);
    
        // !!NOTES: simulate callback twice after SaveFeature is called
        {
            FRCbParam tmpCbParam;
    
            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -1;
            tmpCbParam.stream.width = -1;
            tmpCbParam.stream.height = -1;
            tmpCbParam.stream.stride = -1;
            tmpCbParam.stream.format = -1;
    
            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_SAVE_FEATURE, FR_ERR_ALGO_RETURN_INVALID_ARGUMENT, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
    
            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -2;
            tmpCbParam.stream.width = -2;
            tmpCbParam.stream.height = -2;
            tmpCbParam.stream.stride = -2;
            tmpCbParam.stream.format = -2;
    
            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_SAVE_FEATURE, FR_ERR_ALGO_RETURN_NULLPTR_ERROR, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
        }
    
        {
            printf("Press any key to stopSaveFeature...\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }
        my_err_int = gHidlFRHandler->stopSaveFeature();

        {
            printf("Press any key to prepare start CompareFeature process....\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }

        CAM_LOGI("=== mkdbg: IFRHandler.startCompareFeature() test: ret=%d===", (int32_t)my_err_int);

// compare
        my_err_int = gHidlFRHandler->startCompareFeature(userName);
        CAM_LOGI("=== mkdbg: IFRHandler.startCompareFeature() test: ret=%d===", (int32_t)my_err_int);

        // !!NOTES: simulate callback twice after CompareFeature is called
        {
            FRCbParam tmpCbParam;

            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -3;
            tmpCbParam.stream.width = -3;
            tmpCbParam.stream.height = -3;
            tmpCbParam.stream.stride = -3;
            tmpCbParam.stream.format = -3;

            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_COMPARE_FEATURE, FR_ERR_ALGO_RETURN_MALLOC_FAILED, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);

            tmpCbParam.buffer = nullptr;
            tmpCbParam.stream.size = -4;
            tmpCbParam.stream.width = -4;
            tmpCbParam.stream.height = -4;
            tmpCbParam.stream.stride = -4;
            tmpCbParam.stream.format = -4;

            my_err_int = gHidlFRHandler->simCbRequest(
                    FRALGOCA_CMD_START_COMPARE_FEATURE, FR_ERR_ALGO_RETURN_NO_IMAGE_DATA, tmpCbParam);
            CAM_LOGI("=== mkdbg: IFRHandler.simCbRequest() test: ret=%d ===", (int32_t)my_err_int);
        }

        {
            printf("Press any key to stop CompareFeature...\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }

        my_err_int = gHidlFRHandler->stopCompareFeature();
        CAM_LOGI("=== mkdbg: IFRHandler.stopCompareFeature() test: ret=%d ===", (int32_t)my_err_int);

// delete
        {
            printf("Press any key to prepare start deleteFeature process....\n");
            fflush(stdout);
            char c = (char) getchar();
            if (c != '\n')
            {
                consumeUtilEnterKeyReached();
            }
        }
        
        my_err_int = gHidlFRHandler->deleteFeature(userName);
        CAM_LOGI("=== mkdbg: IFRHandler.deleteFeature() test: ret=%d ===", (int32_t)my_err_int);

        my_err_int = gHidlFRHandler->uninit();
        CAM_LOGI("=== mkdbg: IFRHandler::uninit() test: ret=%d ===", (int32_t)my_err_int);
        
        // TODO: get callback, consume and then return
        // disconnect from secure camera
        gHidlFRHandler.clear();
        CAM_LOGI("=== mkdbg: IFRHandler.clear() test: done ===");
        CAM_LOGI("=== mkdbg: END of %s===", LOG_TAG);
    
        return ::android::OK;
}

void printHelp_FRConfig()
{
    printf ("what do you want to test?\n");
    printf("0 --> FRCFG_DEFAULT \n");
    printf("1 --> FRCFG_DUMMY\n");
    printf("2 --> FRCFG_FPP_2D\n");
    printf("3 --> FRCFG_FPP_AS\n");
    printf("4 --> ST_FPP_2D\n");
    printf("q --> Exit\n");
    printf("others --> print help again\n");
    fflush(stdout);
}

void printHelp_test()
{
    printf ("what do you want to test?\n");
    printf("1 --> testSaveFeatureProcess\n");
    printf("2 --> testCompareFeatureProcess\n");
    printf("3 --> testDeleteFeatureProcess\n");
    printf("4 --> testSaveCompareDeleteFeatureProcess\n");
    printf("q --> Exit\n");
    printf("others --> print help again\n");
    fflush(stdout);
}


int main()
{
    printHelp_FRConfig();

    int ret = 0;
    char c = 'q';
    int is_exit_loop = 0;
    int is_frCfg_set = 0;
    printHelp_FRConfig();
    do 
    {
        c = (char) getchar();
        switch (c)
        {
            case '0':
                consumeUtilEnterKeyReached();
                gFRCfg = FRCFG_DEFAULT;
                is_frCfg_set = 1;
                printf("--> FRCFG_DEFAULT: %d is chosend !!", FRCFG_DEFAULT);
                break;
            case '1':
                consumeUtilEnterKeyReached();
                gFRCfg = FRCFG_DUMMY;
                is_frCfg_set = 1;
                printf("--> FRCFG_DUMMY: %d is chosend !!", FRCFG_DUMMY);
                break;
            case '2':
                consumeUtilEnterKeyReached();
                gFRCfg = FRCFG_FPP_2D;
                is_frCfg_set = 1;
                printf("--> FRCFG_FPP_2D: %d is chosend !!", FRCFG_FPP_2D);
                break;
            case '3':
                consumeUtilEnterKeyReached();
                gFRCfg = FRCFG_FPP_AS;
                is_frCfg_set = 1;
                printf("--> FRCFG_FPP_AS: %d is chosend !!", FRCFG_FPP_AS);
                break;
            case '4':
                consumeUtilEnterKeyReached();
                gFRCfg = FRCFG_ST_2D;
                is_frCfg_set = 1;
                printf("--> FRCFG_ST_2D: %d is chosend !!", FRCFG_ST_2D);
                break;
            case 'q':
                is_exit_loop = 1;
                break;
            case '\n':
                break;
            default:
                consumeUtilEnterKeyReached();
                break;
        }
        printHelp_FRConfig();
    } while (is_exit_loop == 0 && is_frCfg_set == 0);

    if (is_exit_loop)
    {
        return 0;
    }
    
    do
    {
        printHelp_test();
        c = (char) getchar();
        switch (c)
        {
            case '1':
                consumeUtilEnterKeyReached();
                ret = testSaveFeatureProcess();
                break;
            case '2':
                consumeUtilEnterKeyReached();
                ret = testCompareFeatureProcess();
                break;
            case '3':
                consumeUtilEnterKeyReached();
                ret = testDeleteFeatureProcess();
                break;
            case '4':
                consumeUtilEnterKeyReached();
                ret = testSaveCompareDeleteFeatureProcess();
                break;
            case 'q':
                is_exit_loop = 1;
                break;
            case '\n':
                break;
            default:
                consumeUtilEnterKeyReached();
                break;
        }
    } while (is_exit_loop == 0);

    return 0;
}

