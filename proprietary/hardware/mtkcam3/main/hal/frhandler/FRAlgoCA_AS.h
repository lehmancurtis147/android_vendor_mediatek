#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_AS_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_AS_H

#include <utils/RefBase.h> // android::RefBase

#include "FRAlgoCA.h"

#include "fr_ca_ta_debug.h"

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

// FR-custom
typedef struct FRAlgoASInputCollector {
public:
    typedef enum InputCollectState_ENUM {
        FRAIC_WAIT_FOR_IR = 0,
        FRAIC_WAIT_FOR_DEPTH,
        FRAIC_WAIT_FOR_ALGO_HANDLING,
        FRAIC_STOP
    } InputCollectState_ENUM;

    FRAlgoASInputCollector();
    ~FRAlgoASInputCollector();

    InputCollectState_ENUM getState();
    void setState(InputCollectState_ENUM state);
    int setIrData(FRCA_CMD_ENUM currCmd,  const int32_t srcType, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream);
    int setDepthData(FRCA_CMD_ENUM currCmd,  const int32_t srcType, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream);
	void releaseIrResource();
	void releaseDepthResource();
    void dumpBuffer();

	void setMemSrvSession(UREE_SESSION_HANDLE mMemSrvSession);

	int sim_allocIrNonSecureIonAndRegisterShm();
	int sim_freeIrNonSecureIonAndUnregisterShm();
	int sim_allocDepthNonSecureIonAndRegisterShm();
	int sim_freeDepthNonSecureIonAndUnregisterShm();

private:
    std::pair<int /* phyAddr */, unsigned int/*size*/> acquireIrNonSecureInfoByFile(FRCA_CMD_ENUM currCmd);
    void releaseIrNonSecureInfoByFile();
    std::pair<int /* phyAddr */, unsigned int/*size*/> acquireIrNonSecureInfo(const int shareFD, uint64_t size);
    void releaseIrNonSecureInfo();
    std::pair<int /* phyAddr */, unsigned int/*size*/> acquireIrSecureInfo(const int shareFD);
    void releaseIrSecureInfo();

	std::pair<int /* phyAddr */, unsigned int/*size*/> acquireDepthNonSecureInfoByFile(FRCA_CMD_ENUM currCmd);
	void releaseDepthNonSecureInfoByFile();
	std::pair<int /* phyAddr */, unsigned int/*size*/> acquireDepthNonSecureInfo(const int shareFD, uint64_t size);
	void releaseDepthNonSecureInfo();
	std::pair<int /* phyAddr */, unsigned int/*size*/> acquireDepthSecureInfo(const int shareFD);
	void releaseDepthSecureInfo();

#if FRAS_SIMULATION_NEEDED
#endif

public:
    InputCollectState_ENUM mState;
    int mLogLevel;
	UREE_SESSION_HANDLE mMemSrvSession;
    
    // ion related
    int mIonDevFd;

	// ir common:
    FRCaTaBuf mIrBuf;
    buffer_handle_t mIrImportedBuffer;
    ion_user_handle_t mIrIonHandle;
    HandleImporter mIrHandleImporter;

    // ir: non-secure
	unsigned char *mpIrMappedBuffer;
	unsigned int irMappedBufLen;
//    UREE_SHAREDMEM_HANDLE mIrNonSecureGzShmHandle;

    // ir: simlulation: nonSecure and nonSecureByFile
    UREE_SHAREDMEM_HANDLE mIrSimNonSecureGzShmHandle;
	unsigned int irSimMappedBufLen;
	char *mpIrSimMappedBuffer;
	ion_user_handle_t mIrSimAllocIonHandle;
	int mIrSimIonShareFD;

	// depth: common
    FRCaTaBuf mDepthBuf;
    HandleImporter mDepthHandleImporter;
    ion_user_handle_t mDepthIonHandle;
    buffer_handle_t mDepthImportedBuffer;

    // depth: non-secure
	char *mpDepthMappedBuffer;
	unsigned int depthMappedBufLen;
//    UREE_SHAREDMEM_HANDLE mDepthNonSecureGzShmHandle;

    // depth: simlulation: nonSecure and nonSecureByFile
	char *mpDepthSimMappedBuffer;
	unsigned int depthSimMappedBufLen;
    UREE_SHAREDMEM_HANDLE mDepthSimNonSecureGzShmHandle;
	ion_user_handle_t mDepthSimAllocIonHandle;
	int mDepthSimIonShareFD;

#if FRAS_SIMULATION_NEEDED
	// for simulation
#endif
} FRAlgoASInputCollector;


class FRAlgoCA_AS : public FRAlgoCA {

private:
    // FR-custom
    struct SecureCamClientCallback : virtual public ISecureCameraClientCallback
    {
        friend FRAlgoCA_AS;
        SecureCamClientCallback(const wp<FRAlgoCA_AS>& frAlgoCA_AS, uint64_t cookie=0) : mwpFRAlgoCA_AS(frAlgoCA_AS), mKCookie(cookie) {}
        // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
        virtual Return<void> onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream) override;

        wp<FRAlgoCA_AS> mwpFRAlgoCA_AS;
        // 
        const uint64_t mKCookie;
    };

public:
        // FR-custom: start
        FRAlgoCA_AS(const char *pCaller, const FRCFG_ENUM frConfig);
        virtual ~FRAlgoCA_AS();
        virtual int init();
        virtual int uninit();
        virtual void reset();
        virtual ISecureCameraClientCallback* getSecureCameraClientCallback();
        virtual int createFRWorkThread(); // every FRAlgoCA should have their own FRThreadLoop

        // FR-custom: end
protected:
        static void *FRWorkThreadLoop(void *arg);

private:
    // FR-custom
    int handleFeature();
    int fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream);

	int debug_allocIrFileDumpBufAndRegisterShm();
	int debug_freeIrFileDumpBufAndUnregisterShm();
	int debug_allocDepthFileDumpBufAndRegisterShm();
	int debug_freeDepthFileDumpBufAndUnregisterShm();
	int fileDump(const char *pFileName, int stride, char *pFileDumpBuf, FRCaTaBuf *pTargetBuf);

protected:
    // custom
    FRAlgoASInputCollector mFRAlgoIC;
    UREE_SESSION_HANDLE mMemSrvSession;

	char *mpIrFileDumpBuf;
	char *mpDepthFileDumpBuf;
    UREE_SHAREDMEM_HANDLE mIrFileDumpGzShmHandle;
    UREE_SHAREDMEM_HANDLE mDepthFileDumpGzShmHandle;
};


}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

#endif // VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_AS_H


