#include "FRHandler.h"
#ifdef LOG_TAG
#undef LOG_TAG
#endif

#define LOG_TAG "FRHandler"

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>
#include <cutils/properties.h>
#include "fr_err_def.h"

#define DEBUG_FRHANDLER_LOG_LEVEL "debug.frhand.loglevel"
#define FRHANDLER_LOG_DEF_VALUE 1


#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif

#if 1
#define FUNC_START if (mLogLevel) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (mLogLevel) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif

// mkdbg: added for simulation
#define FRHAND_FLOW_SIMULATION


/* hidl related */
// using ::android::hardware::Status;

using ::android::hardware::camera::common::V1_0::Status;

using namespace vendor::mediatek::hardware::camera::frhandler::V1_0;
using namespace vendor::mediatek::hardware::camera::security::V1_0;

// mkdbg: to delete
//using vendor::mediatek::hardware::camera::frhandler::FRCA_CMD_ENUM;

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {
namespace V1_0 {
namespace implementation {

/*
struct FRCbParam final {
    int64_t bufferId __attribute__ ((aligned(8)));
    ::android::hardware::hidl_handle buffer __attribute__ ((aligned(8)));
    ::vendor::mediatek::hardware::camera::security::V1_0::Stream stream __attribute__ ((aligned(8)));
}; 
struct Stream final {                                                                                                                                                                                       
    int64_t size __attribute__ ((aligned(8)));
    uint64_t stride __attribute__ ((aligned(8)));
    int32_t width __attribute__ ((aligned(4)));
    int32_t height __attribute__ ((aligned(4)));
    int32_t format __attribute__ ((aligned(4)));
};
*/

void FRHandler::FRAlgoCAClientCallback::onFRAlgoCAClientCallback(const int32_t fr_cmd_id, const int32_t errCode)
{
    ALOGD("%s +", __FUNCTION__);
    FRCbParam tmpCbParam; //!!NOTES: no use for now
    tmpCbParam.buffer = nullptr;
    tmpCbParam.stream.size = 0;
    tmpCbParam.stream.width = 0;
    tmpCbParam.stream.height = 0;
    tmpCbParam.stream.stride = 0;
    tmpCbParam.stream.format = 0;

    if (mwpFRHandler != NULL)
    {
        sp<FRHandler> spFRHandler = mwpFRHandler.promote();
        if (spFRHandler->mFRCliCallback != NULL)
        {
               spFRHandler->mFRCliCallback->onFRClientCallback (fr_cmd_id, errCode, tmpCbParam);
        }
        ALOGD("%s: cmd_id: %d, errCode=%d", __FUNCTION__, fr_cmd_id, errCode);
    }
    ALOGD("%s -", __FUNCTION__);
}


// Methods from IFRHandler follow.
Return<void> FRHandler::getFRImageFormat(getFRImageFormat_cb _hidl_cb) {
    // TODO implement
    FUNC_START;

    mVecFRImgFormat.clear(); // clean first

#ifdef FRHAND_FLOW_SIMULATION
    sim_getFRImageFormat(_hidl_cb);
#endif

    FUNC_END;
    
    return Void();
}

Return<int32_t> FRHandler::init(int frConfig, const sp<IFRClientCallback>& clientCallback) {

    int ret = FR_ERR_OK;
    mLogLevel = ::property_get_int32(DEBUG_FRHANDLER_LOG_LEVEL, FRHANDLER_LOG_DEF_VALUE);
    mIsInited = 0;

    FUNC_START;
    Mutex::Autolock lock(mLock);

    if (mIsInited)
    {
        ALOGE("FRHandler already inited: %d", -1);
        return FR_ERR_FRHANDLER_ALREADY_INITED;
    }

    mCurrCmd = FRALGOCA_CMD_NONE;

    // step1: create FRAlgoCA
    mpFRAlgoCA = FRAlgoCA::createInstance("FRHandler", (FRCFG_ENUM) frConfig);
    if (mpFRAlgoCA == NULL)
    {
        ret = FR_ERR_FRHANDLER_INIT_FAILED;
        ALOGE("mpFRAlgoCA createInstance failed");
        goto exit_init;
    }
    if ((ret=mpFRAlgoCA->init()) != 0)
    {
        ALOGE("mpFRAlgoCA->init() failed");
        goto exit_init;
    }

    // step2: create MTEE session
    mFRCliCallback = clientCallback;

    // step3: register callback for FRAlgo
    mFRAlgoCACliCallback = new FRHandler::FRAlgoCAClientCallback(this);
    mpFRAlgoCA->registerCallback(this->mFRAlgoCACliCallback);

    mIsInited = 1;

    FUNC_END;
    return ret;

exit_init:

    mpFRAlgoCA = NULL;
    return ret;
}

Return<int32_t> FRHandler::uninit() {
    int ret = FR_ERR_OK;
    FUNC_START;

    Mutex::Autolock lock(mLock);

    if (mIsInited == 0)
    {
        ALOGE("%s: FRAlgo not inited yet", __FUNCTION__);
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (mpFRAlgoCA != NULL)
    {
        if ((ret=mpFRAlgoCA->uninit()) != 0)
        {
            ALOGW("mpFRAlgoCA uninit() failed");
        }
    }
    mpFRAlgoCA = NULL;
    mFRCliCallback = NULL;
    mFRAlgoCACliCallback = NULL;

    mCurrCmd = FRALGOCA_CMD_NONE;

    FUNC_END;

    return ret;
}

Return<int32_t> FRHandler::registerCallback(const sp<IFRClientCallback>& clientCallback) {
    // TODO implement
    FUNC_START;

    Mutex::Autolock lock(mLock);

    mFRCliCallback = clientCallback;

#ifdef FRHAND_FLOW_SIMULATION
    FUNC_END;
    return sim_registerCallback(clientCallback);
#endif

    FUNC_END;
    return 0;
}

Return<int32_t> FRHandler::setPowerMode(int32_t powerMode) {
    FUNC_START;

    Mutex::Autolock lock(mLock);

    // TODO:
    FUNC_END;

    return FR_ERR_OK;
}

Return<int32_t> FRHandler::startSaveFeature(const hidl_string& userName) {
    FUNC_START;

    Mutex::Autolock lock(mLock);

    // === Error Handling ===
    if (mIsInited == 0 || mpFRAlgoCA == NULL || userName.empty())
    {
        ALOGE("!!err: %s: FRAlgo not inited yet", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (userName.empty())
    {
        ALOGE("!!err: %s: userName empty", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_USERNAME_NOT_ASSIGNED;
    }
    if (userName.size() > FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s: userName too long: larger than %d", __FUNCTION__, FRCATA_USER_MAX_NAME_LEN);
        FUNC_END;
       return FR_ERR_FRHANDLER_USERNAME_TOO_LONG;
    }

    if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE || mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        ALOGE("%s: !!err: wrong command, currCmd=%d", __FUNCTION__, mCurrCmd);
        FUNC_END;
        return FR_ERR_FRHANDLER_WRONG_CMD;
    }

    // === Start Processing ===

    mUserName = userName;

    ALOGE("%s: userName saved: %s", __FUNCTION__, mUserName.c_str());
    
    /* !!NOTES:
        arg1 --> c_str::userName
        art2 --> int::userNameLen
    */
    int ret = mpFRAlgoCA->sendCmd(FRALGOCA_CMD_START_SAVE_FEATURE, (intptr_t)mUserName.c_str(), (intptr_t)mUserName.size());
    if (ret == 0)
    {
        mCurrCmd = FRALGOCA_CMD_START_SAVE_FEATURE;
    }
    FUNC_END;
    return ret;
}

Return<int32_t> FRHandler::stopSaveFeature() {
    FUNC_START;
    Mutex::Autolock lock(mLock);

    if (mIsInited == 0 || mpFRAlgoCA == NULL)
    {
        ALOGE("!!err: %s: FRAlgo not inited yet", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (mCurrCmd != FRALGOCA_CMD_START_SAVE_FEATURE)
    {
        ALOGE("!!err: %s: wrong command, currCmd=%d", __FUNCTION__, mCurrCmd);
        FUNC_END;
        return FR_ERR_FRHANDLER_WRONG_CMD;
    }

    FUNC_END;
    /* !!NOTES:
        arg1 --> null
        art2 --> null
    */
    // just set mCurrCmd to FRALGOCA_CMD_STOP_SAVE_FEATURE w/o caring if sendCmd succeeds or not
    mCurrCmd = FRALGOCA_CMD_STOP_SAVE_FEATURE;
    return mpFRAlgoCA->sendCmd(FRALGOCA_CMD_STOP_SAVE_FEATURE, (intptr_t)0, (intptr_t)0);
}

Return<int32_t> FRHandler::deleteFeature(const hidl_string& userName) {
    FUNC_START;
    Mutex::Autolock lock(mLock);

    if (mIsInited == 0 || mpFRAlgoCA == NULL)
    {
        ALOGE("!!err: %s: FRAlgo not inited yet", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (userName.empty())
    {
        ALOGE("!!err: %s: userName empty", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_USERNAME_NOT_ASSIGNED;
    }
    if (userName.size() > FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s: userName too long: larger than %d", __FUNCTION__, FRCATA_USER_MAX_NAME_LEN);
        FUNC_END;
       return FR_ERR_FRHANDLER_USERNAME_TOO_LONG;
    }

    ALOGD("%s: the feature of User '%s' will be deleted", __FUNCTION__, userName.c_str());

    FUNC_END;
    /* !!NOTES:
        arg1 --> c_str::userName
        arg2 --> int::userNameLen
    */
//    return mpFRAlgoCA->sendCmd(FRALGOCA_CMD_DELETE_FEATURE, (intptr_t)mUserName.c_str(), (intptr_t)mUserName.size());
    //!!NOTES: don't use mUserName here because app might want to delete certain userName feature directly
    return mpFRAlgoCA->sendCmd(FRALGOCA_CMD_DELETE_FEATURE, (intptr_t)userName.c_str(), (intptr_t)userName.size());
}

Return<int32_t> FRHandler::startCompareFeature(const hidl_string& userName) {
    FUNC_START;
    Mutex::Autolock lock(mLock);

    if (mIsInited == 0 || mpFRAlgoCA == NULL)
    {
        ALOGE("%s: FRAlgo not inited yet", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (userName.empty())
    {
        ALOGE("!!err: %s: userName empty", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_USERNAME_NOT_ASSIGNED;
    }
    if (userName.size() > FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s: userName too long: larger than %d", __FUNCTION__, FRCATA_USER_MAX_NAME_LEN);
        FUNC_END;
       return FR_ERR_FRHANDLER_USERNAME_TOO_LONG;
    }


    if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE || mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        ALOGE("%s: !!err: wrong command, currCmd=%d", __FUNCTION__, mCurrCmd);
        FUNC_END;
        return FR_ERR_FRHANDLER_WRONG_CMD;
    }

    mUserName = userName;

    ALOGE("%s: userName to compare: %s", __FUNCTION__, mUserName.c_str());

    /* !!NOTES:
        arg1 --> c_str::userName
        art2 --> int::userNameLen
    */
    int ret = mpFRAlgoCA->sendCmd(FRALGOCA_CMD_START_COMPARE_FEATURE, (intptr_t)mUserName.c_str(), (intptr_t)mUserName.size());
    if (ret == 0)
    {
        mCurrCmd = FRALGOCA_CMD_START_COMPARE_FEATURE;
    }
    FUNC_END;
    return ret;
}

Return<int32_t> FRHandler::stopCompareFeature() {
    FUNC_START;
    Mutex::Autolock lock(mLock);

    if (mIsInited == 0 || mpFRAlgoCA == NULL)
    {
        ALOGE("%s: FRAlgo not inited yet", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_NOT_INITED;
    }

    if (mCurrCmd != FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        ALOGE("%s: !!err: wrong command, currCmd=%d", __FUNCTION__, mCurrCmd);
        FUNC_END;
        return FR_ERR_FRHANDLER_WRONG_CMD;
    }

    if (mUserName.empty())
    {
        ALOGD("%s: no userName assigned", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRHANDLER_USERNAME_NOT_ASSIGNED;
    }

    FUNC_END;
    /* !!NOTES:
        arg1 --> null
        art2 --> null
    */
    // just set mCurrCmd to FRALGOCA_CMD_STOP_COMPARE_FEATURE w/o caring if sendCmd succeeds or not
    mCurrCmd = FRALGOCA_CMD_STOP_COMPARE_FEATURE;
    return mpFRAlgoCA->sendCmd(FRALGOCA_CMD_STOP_COMPARE_FEATURE, (intptr_t)0, (intptr_t)0);
}

/* === Debug usage: start === */
Return<int32_t> FRHandler::simCbRequest(int32_t cmdID, int32_t errCode, const FRCbParam &cbParam)
{
    FUNC_START;

    if (mFRCliCallback != NULL)
    {
        mFRCliCallback->onFRClientCallback(cmdID, errCode, cbParam);
    }
    FUNC_END;

    return FR_ERR_OK;
}
/* === Debug usage: end === */


// Methods from ::android::hidl::base::V1_0::IBase follow.
IFRHandler* HIDL_FETCH_IFRHandler(const char* /* name */) {

    ALOGI("%s is called", __FUNCTION__);
#ifdef MTK_CAM_SECURITY_SUPPORT
    return new FRHandler();
#else
    return new DummyFRHandler();
#endif
}

#ifdef FRHAND_FLOW_SIMULATION

void FRHandler::sim_getFRImageFormat(getFRImageFormat_cb _hidl_cb)
{
    FUNC_START;

    FRImageFormat test;
    test.stream.size = 111;
    test.stream.stride = 222;
    test.stream.width = 333;
    test.stream.height = 444;
    test.stream.format = 555;

    mVecFRImgFormat.push_back(test);
    _hidl_cb(Status::OK, mVecFRImgFormat);

    FUNC_END;
    return;        
}


Return<int32_t> FRHandler::sim_registerCallback(const sp<IFRClientCallback>& clientCallback)
{
    FUNC_START;

    mFRCliCallback = clientCallback;

    FRCbParam testFRCbParam;
    testFRCbParam.buffer = nullptr;
    testFRCbParam.stream.size = 555;
    testFRCbParam.stream.stride = 444;
    testFRCbParam.stream.width = 333;
    testFRCbParam.stream.height = 222;
    testFRCbParam.stream.format = 111;

    mFRCliCallback->onFRClientCallback (111, 222, testFRCbParam);

    FUNC_END;

    return 0;
}

#endif // FRHAND_FLOW_SIMULATION


// ==== DummyFRHandler start ======
Return<void> DummyFRHandler::getFRImageFormat(getFRImageFormat_cb _hidl_cb)
{
    return Void();
}

Return<int32_t> DummyFRHandler::init(int32_t frConfig, const sp<IFRClientCallback>& clientCallback)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::uninit()
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::registerCallback(const sp<IFRClientCallback>& clientCallback)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::setPowerMode(int32_t powerMode)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::startSaveFeature(const hidl_string& userName)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::stopSaveFeature()
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::deleteFeature(const hidl_string& userName)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::startCompareFeature(const hidl_string& userName)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::stopCompareFeature()
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

Return<int32_t> DummyFRHandler::simCbRequest(int32_t cmdID, int32_t errCode, const FRCbParam &cbParam)
{
    return FR_ERR_CMD_NOT_IMPLEMETED;
}

// ==== DummyFRHandler: end

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware
}  // namespace mediatek
}  // namespace vendor
