
#define LOG_TAG "FRAlgoCA_AS"

#include "FRAlgoCA_AS.h"
#include <fr_err_def.h>

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>
#include <cutils/properties.h>

#include <errno.h>
#include <sys/mman.h>
#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>


#include <mtkcam/utils/std/Format.h>

#define DEBUG_FRALGOCA_AS_LOG_LEVEL "debug.fralgoca_as.loglevel"
#define FRALGOCAAS_LOG_DEF_VALUE 1

#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif
#if 1
#define FUNC_START if (1) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (1) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif

#ifdef MY_LOGD_IF
#undef MY_LOGD_IF
#endif
#define MY_LOGD_IF(cond,...)    do { if ( (cond) ) { ALOGD(__VA_ARGS__); } } while (0)

#ifdef MY_LOGW_IF
#undef MY_LOGW_IF
#endif
#define MY_LOGW_IF(cond,...)    do { if ( (cond) ) { ALOGW(__VA_ARGS__); } } while (0)

#ifdef MY_LOGE
#undef MY_LOGE
#endif
#define MY_LOGE(...)    do { ALOGE(__VA_ARGS__); } while (0)

#ifdef MTEE_USED
#ifdef MTEE_FR_SVC_NAME
#undef MTEE_FR_SVC_NAME
#endif

#define MTEE_FR_SVC_NAME "com.mediatek.geniezone.fralgo"
#endif

// === Static Function Declaration ===
static void printFRCaTaBuf(FRCaTaBuf *pFRCaTaBuf)
{
	if (pFRCaTaBuf == NULL)
    {
        return;
    }

	ALOGD("%s: BufInfo (fmt=0x%x, srcType=%d, debugCfg=0x%x, gzHandle=%d, size=%d(%dx%d), stride=%d",
	  __FUNCTION__,
	  pFRCaTaBuf->format, pFRCaTaBuf->srcType, pFRCaTaBuf->debugCfg, pFRCaTaBuf->gzHandle, pFRCaTaBuf->size, pFRCaTaBuf->width, pFRCaTaBuf->height, pFRCaTaBuf->stride);

	ALOGD("%s: BufInfo reserved parts = (%d, %d, %d, %d)",
	  __FUNCTION__,
	  pFRCaTaBuf->reserved3, pFRCaTaBuf->reserved4, pFRCaTaBuf->reserved5, pFRCaTaBuf->reserved6
	  );
}

static void printImage(const char *pBuffer, const int format, const int logLevel)
{
#if FRAS_CA_LOG_BUF_CONTENT
    if (pBuffer == NULL)
    {
        return;
    }

    int i = 0;

    MY_LOGD_IF(logLevel >= 1, "--- %s: fmt=0x%x, ptr=%p --- \n", __FUNCTION__, format, pBuffer);
    for (i = 0; i < 16*8; i+=8)
    {
        MY_LOGD_IF(logLevel >= 1, "%s: %.2x %.2x %.2x %.2x %.2x %.2x %.2x %.2x",
            __FUNCTION__,
            pBuffer[i], pBuffer[i+1], pBuffer[i+2], pBuffer[i+3],
            pBuffer[i+4], pBuffer[i+5], pBuffer[i+6], pBuffer[i+7]
            );
    }
#endif
}


static void fpp_read_file(char const* path, char* data) {
    FILE* fin = fopen(path, "rb");
    if (!fin) {
        fprintf(stderr, "failed to read %s\n", path);
        abort();
    }

    fseek(fin, 0, SEEK_END);
    size_t len = ftell(fin);
    rewind(fin);

    size_t readBytes= fread(data, 1, len, fin);
    ALOGD("%s: file(%s), fLen=%d, readBytes=%d", __FUNCTION__, path, len, readBytes);

    fclose(fin);
}

// === Static Function Declaration: end

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {


FRAlgoASInputCollector::FRAlgoASInputCollector()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    // resource to be init-ed before reset()
    mLogLevel = FRAS_CA_LOGLEVEL; //!!NOTES: default log level = 1
    mIrImportedBuffer = mDepthImportedBuffer = NULL;
    mIrIonHandle = mDepthIonHandle = 0;

    mIonDevFd = 0;

    mMemSrvSession = (UREE_SESSION_HANDLE)0;

    mIrBuf.srcType = FRAS_CA_SRC_TYPE_PROP_DEF_VAL;
    mDepthBuf.srcType = FRAS_CA_SRC_TYPE_PROP_DEF_VAL;

    mIrSimNonSecureGzShmHandle = 0;
    mpIrSimMappedBuffer = NULL;
    irSimMappedBufLen = 0;
    mIrSimIonShareFD = 0;
    mIrSimAllocIonHandle = 0;

    mDepthSimNonSecureGzShmHandle = 0;
    mpDepthSimMappedBuffer = NULL;
    depthSimMappedBufLen = 0;
    mDepthSimIonShareFD = 0;
    mDepthSimAllocIonHandle = 0;

    releaseIrResource();
    releaseDepthResource();
    mState = FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR;

    mIonDevFd = mt_ion_open(LOG_TAG);
    if (mIonDevFd < 0)
    {
        MY_LOGE("!!err: %s: open Ion device failed!", __FUNCTION__);
        mIonDevFd = -1;
    }
    FUNC_END;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return;
#endif
}

FRAlgoASInputCollector::~FRAlgoASInputCollector()
{
    FUNC_START;

    releaseIrResource();
    releaseDepthResource();

    // resource to be released after reset()
    if (mIonDevFd > 0)
    {
        ion_close(mIonDevFd);
        mIonDevFd = -1;
    }

    mMemSrvSession = (UREE_SESSION_HANDLE)0;
    mState = FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR;

    FUNC_END;
}

void FRAlgoASInputCollector::setMemSrvSession(UREE_SESSION_HANDLE memSrvSession)
{
    mMemSrvSession = memSrvSession;
    MY_LOGD_IF(mLogLevel >= 1, "%s: mMemSrvSession=%d", __FUNCTION__, mMemSrvSession);
}

void FRAlgoASInputCollector::releaseIrResource()
{
    FUNC_START;

    releaseIrNonSecureInfoByFile();
    releaseIrSecureInfo();
    releaseIrNonSecureInfo();

    if (mIrImportedBuffer != NULL)
    {
        mIrHandleImporter.freeBuffer(mIrImportedBuffer);
        MY_LOGD_IF(mLogLevel >= 1, "mIrImportedBuffer(%p) freed done", __FUNCTION__, mIrImportedBuffer);
    }
    mIrImportedBuffer = NULL;

    if (mIonDevFd > 0)
    {
        // free ionHandle
        mIrIonHandle = (ion_user_handle_t)0;
        //!!NOTES: mIonDevFd won't be freed until FRAlgoCollector destructor
    }

    memset((void*)&mIrBuf, 0, sizeof(FRCaTaBuf));
    mIrBuf.chksum = FRCATA_BUF_CHECKSUM;
    FUNC_END;
}

void FRAlgoASInputCollector::releaseDepthResource()
{
    FUNC_START;

    releaseDepthNonSecureInfoByFile();
    releaseDepthSecureInfo();
    releaseDepthNonSecureInfo();

    if (mDepthImportedBuffer != NULL)
    {
        mDepthHandleImporter.freeBuffer(mDepthImportedBuffer);
        MY_LOGD_IF(mLogLevel >= 1, "mDepthImportedBuffer(%p) freed done", __FUNCTION__, mDepthImportedBuffer);
    }
    mDepthImportedBuffer = NULL;

    if (mIonDevFd > 0)
    {
        // free ionHandle
        mDepthIonHandle = (ion_user_handle_t)0;
        //!!NOTES: mIonDevFd won't be freed until FRAlgoCollector destructor
    }

    memset((void*)&mDepthBuf, 0, sizeof(FRCaTaBuf));
    mDepthBuf.chksum = FRCATA_BUF_CHECKSUM;
    FUNC_END;
}


int FRAlgoASInputCollector::setIrData(
    FRCA_CMD_ENUM currCmd, const int32_t srcType, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream)
{
    FUNC_START;
    int ret = FR_ERR_OK;
    
    mIrBuf.bufferId = bufferId;
    TZ_RESULT retTee;

    if (hidl_buffer == nullptr)
    {
        MY_LOGE("!!err: %s: buffer should not be nullptr", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_IC_INVALID_ARG;
    }

    FRCaTaBuf *pTargetBuf = NULL;
    mIrImportedBuffer = hidl_buffer.getNativeHandle();
    mIrHandleImporter.importBuffer(mIrImportedBuffer);
    pTargetBuf = &mIrBuf;
    
    MY_LOGD_IF(mLogLevel >= 2, "%s: srcType=%d", __FUNCTION__, srcType);
    if (srcType == FRAS_SRC_TYPE_FILE)
    {
        auto info = acquireIrNonSecureInfoByFile(currCmd);
        pTargetBuf->gzHandle = info.first;

        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
            MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
            FUNC_END;
            return FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }

        uint32_t memorySize = static_cast<uint32_t>(info.second);
        if (memorySize == 0)
        {
            MY_LOGE("!!err: %s: memorySize is 0", __FUNCTION__);
            FUNC_END;
            return FR_ERR_FRALGO_IC_WRONG_SIZE;
        
        }
        if (info.second != memorySize)
        {
          MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
          FUNC_END;
          return FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        pTargetBuf->size = memorySize;

        //!!NOTES: return here because all TargetBuf info is already confirmed
        printFRCaTaBuf(pTargetBuf);
        FUNC_END;
        return FR_ERR_OK;
    }
    else if (srcType == FRAS_SRC_TYPE_SECURE)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: currCmd=%d, FRAS_SRC_TYPE_SECURE", __FUNCTION__, currCmd);

        auto secureInfo = acquireIrSecureInfo(mIrImportedBuffer->data[0]);
        pTargetBuf->gzHandle = secureInfo.first;
        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
            MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
            ret = FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }

        // check size
        uint32_t secureMemorySize = static_cast<uint32_t>(secureInfo.second);
        if (secureInfo.second != secureMemorySize)
        {
            MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != secureMemorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %llu, secureMemorySize: %d ", __FUNCTION__, stream.size, secureMemorySize);
        }

        pTargetBuf->size = secureMemorySize;
    }
    else
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: currCmd=%d, FRAS_SRC_TYPE_NON_SECURE", __FUNCTION__, currCmd);

        // normal path
        const uint64_t streamBufSize =
            ((stream.width * NSCam::Utils::Format::queryPlaneBitsPerPixel(stream.format, 0)) >> 3)*
            stream.height;
        auto info = acquireIrNonSecureInfo(mIrImportedBuffer->data[0], streamBufSize);
        pTargetBuf->gzHandle = info.first;
        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
            MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
            ret = FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }
        uint32_t memorySize = static_cast<uint32_t>(info.second);
        if (info.second != memorySize)
        {
            MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != memorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %llu, secureMemorySize: %d ", __FUNCTION__, stream.size, memorySize);
        }
        pTargetBuf->size = memorySize;
    }

    pTargetBuf->width = stream.width;
    pTargetBuf->height = stream.height;
    pTargetBuf->stride = stream.stride;
    pTargetBuf->format = stream.format;
    pTargetBuf->timestampHigh = 0; // !!NOTES: no need if hw-sync
    pTargetBuf->timestampLow = 0; // // !!NOTES: no need if hw-sync

    printFRCaTaBuf(pTargetBuf);
    FUNC_END;
    return ret;

}

int FRAlgoASInputCollector::setDepthData(
    FRCA_CMD_ENUM currCmd,  const int32_t srcType, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream)
{
    FUNC_START;
    int ret = FR_ERR_OK;
    
    TZ_RESULT retTee;

    if (hidl_buffer == nullptr)
    {
        MY_LOGE("!!err: %s: !!buffer should not be nullptr", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_IC_INVALID_ARG;
    }

    mDepthBuf.bufferId = bufferId;
    mDepthImportedBuffer = hidl_buffer.getNativeHandle();
    mDepthHandleImporter.importBuffer(mDepthImportedBuffer);

    FRCaTaBuf *pTargetBuf = &mDepthBuf;

    MY_LOGD_IF(mLogLevel >= 1, "%s: srcType=%d", __FUNCTION__, srcType);
    if (srcType == FRAS_SRC_TYPE_FILE)
    {
        auto info = acquireDepthNonSecureInfoByFile(currCmd);
        pTargetBuf->gzHandle = info.first;
        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
           MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
           FUNC_END;
           return FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }
        // check size
        uint32_t memorySize = static_cast<uint32_t>(info.second);
        if (memorySize == 0)
        {
            MY_LOGE("!!err: %s: memorySize is 0", __FUNCTION__);
            FUNC_END;
            return FR_ERR_FRALGO_IC_WRONG_SIZE;
        
        }
        if (info.second != memorySize)
        {
          MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
          FUNC_END;
          return FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        pTargetBuf->size = memorySize;

        //!!NOTES: return here because all TargetBuf is already confirmed
        printFRCaTaBuf(pTargetBuf);
        FUNC_END;
        return FR_ERR_OK;
    }
    else if (srcType == FRAS_SRC_TYPE_SECURE)
    {
        auto secureInfo = acquireDepthSecureInfo((mDepthImportedBuffer)->data[0]);
        pTargetBuf->gzHandle = secureInfo.first;
        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
            MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
            ret = FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }

        uint32_t secureMemorySize = static_cast<uint32_t>(secureInfo.second);
        if (secureInfo.second != secureMemorySize)
        {
            MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != secureMemorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %llu, secureMemorySize: %d ", __FUNCTION__, stream.size, secureMemorySize);
        }
        pTargetBuf->size = secureMemorySize;
    }
    else
    {
        // normal path
        const uint64_t streamBufSize =
            ((stream.width * NSCam::Utils::Format::queryPlaneBitsPerPixel(stream.format, 0)) >> 3)*
            stream.height;
        auto info = acquireDepthNonSecureInfo((mDepthImportedBuffer)->data[0], streamBufSize);
        pTargetBuf->gzHandle = info.first;
        // check handle
        if (pTargetBuf->gzHandle == 0)
        {
            MY_LOGE("!!err: %s: Target gzHandle(%d) is wrong", __FUNCTION__, pTargetBuf->gzHandle);
            ret = FR_ERR_FRALGO_IC_WRONG_GZ_HANDLE;
        }

        uint32_t memorySize = static_cast<uint32_t>(info.second);
        if (info.second != memorySize)
        {
            MY_LOGE("!!err: %s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != memorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %llu, secureMemorySize: %d ", __FUNCTION__, stream.size, memorySize);
        }
        pTargetBuf->size = memorySize;
    }
    
    pTargetBuf->width = stream.width;
    pTargetBuf->height = stream.height;
    pTargetBuf->stride = stream.stride;
    pTargetBuf->format = stream.format;
    pTargetBuf->timestampHigh = 0; // !!NOTES: no need if hw-sync
    pTargetBuf->timestampLow = 0; // // !!NOTES: no need if hw-sync

    printFRCaTaBuf(pTargetBuf);
    FUNC_END;
    return ret;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireIrNonSecureInfoByFile(
    FRCA_CMD_ENUM currCmd)
{
    FUNC_START;

    if (currCmd != FRALGOCA_CMD_START_SAVE_FEATURE && currCmd != FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        MY_LOGE("!!err: %s: wrong command: %d", __FUNCTION__, currCmd);
        FUNC_END;
        return std::pair<int, unsigned int>(0, 0);
    }
    int ret = FR_ERR_OK;
    const unsigned int IMG_WIDTH = FRAS_SRC_TYPE_IR_FILE_WIDTH;
    const unsigned int IMG_HEIGHT  = FRAS_SRC_TYPE_IR_FILE_HEIGHT;
    const unsigned int IMG_SIZE = IMG_WIDTH * IMG_HEIGHT * FRAS_SRC_TYPE_IR_BYTES_PER_PIXEL;

    if (mpIrSimMappedBuffer == NULL)
    {
        MY_LOGE("!!err: %s: mpIrSimMappedBuffer == NULL", __FUNCTION__);
        return std::pair<int, unsigned int>(0, 0);
    }

    // ir buffer
    if (currCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: reading /sdcard/facepp/ir1.raw\n", __FUNCTION__);
        fpp_read_file("/sdcard/facepp/ir1.raw", (char *)mpIrSimMappedBuffer);
    }
    else if (currCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: reading /sdcard/facepp/ir2.raw\n", __FUNCTION__);
        fpp_read_file("/sdcard/facepp/ir2.raw", (char *)mpIrSimMappedBuffer);
    }
    else
    {
        MY_LOGE("!!err: %s: error Cmd: %d", __FUNCTION__, currCmd);
    }

    printImage(mpIrSimMappedBuffer, FRAS_IR_COLOR_FORMAT, mLogLevel);

    mIrBuf.gzHandle = mIrSimNonSecureGzShmHandle;
    mIrBuf.width = IMG_WIDTH;
    mIrBuf.height = IMG_HEIGHT;
//    mIrBuf.stride = IMG_WIDTH; // make stride=width
    mIrBuf.stride = IMG_WIDTH*FRAS_SRC_TYPE_IR_BYTES_PER_PIXEL;
    mIrBuf.format = 0x9990; // unknown
    mIrBuf.timestampHigh = 0; // !!NOTES: no need if hw-sync
    mIrBuf.timestampLow = 0; // // !!NOTES: no need if hw-sync
    mIrBuf.srcType = FRAS_SRC_TYPE_FILE;

    FUNC_END;
    return std::pair<int, unsigned int>(mIrSimNonSecureGzShmHandle, IMG_SIZE);

err_acquireIrNonSecureInfoByFile:

    releaseIrNonSecureInfoByFile();

    FUNC_END;
    return std::pair<int, unsigned int>(0, 0);
}

void FRAlgoASInputCollector::releaseIrNonSecureInfoByFile()
{
    if (mIrBuf.srcType != FRAS_SRC_TYPE_FILE) 
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mIrBuf.srcType, FRAS_SRC_TYPE_FILE);
        return;
    }

    FUNC_START;

    // reset mpIrSimMappedBuffer
    if (mpIrSimMappedBuffer)
    {
        memset(mpIrSimMappedBuffer, 0xff, 4);
    }
    FUNC_END;
    return;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireDepthNonSecureInfoByFile(
    FRCA_CMD_ENUM currCmd)
{
    FUNC_START;

    if (currCmd != FRALGOCA_CMD_START_SAVE_FEATURE && currCmd != FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        MY_LOGE("!!err: %s: wrong command: %d", __FUNCTION__, currCmd);
        FUNC_END;
        return std::pair<int, unsigned int>(0, 0);
    }
    int ret = FR_ERR_OK;
    const unsigned int IMG_WIDTH = FRAS_SRC_TYPE_DEPTH_FILE_WIDTH;
    const unsigned int IMG_HEIGHT  = FRAS_SRC_TYPE_DEPTH_FILE_HEIGHT;
    const unsigned int IMG_SIZE = IMG_WIDTH * IMG_HEIGHT * FRAS_SRC_TYPE_DEPTH_BYTES_PER_PIXEL;

    // depth buffer
    if (mpDepthSimMappedBuffer == NULL)
    {
        MY_LOGE("!!err: %s: mpDepthSimMappedBuffer == NULL", __FUNCTION__);
        return std::pair<int, unsigned int>(0, 0);
    }

    if (currCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: reading /sdcard/facepp/depth.raw\n", __FUNCTION__);
        fpp_read_file("/sdcard/facepp/depth1.raw", (char *)mpDepthSimMappedBuffer);
    }
    else if (currCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: reading /sdcard/facepp/depth.raw\n", __FUNCTION__);
        fpp_read_file("/sdcard/facepp/depth2.raw", (char *)mpDepthSimMappedBuffer);
    }
    else
    {
        MY_LOGE("!!err: %s: error Cmd: %d", __FUNCTION__, currCmd);
    }
        
    printImage(mpDepthSimMappedBuffer, FRAS_DEPTH_COLOR_FORMAT, mLogLevel);

    mDepthBuf.gzHandle = mDepthSimNonSecureGzShmHandle;
    mDepthBuf.width = IMG_WIDTH;
    mDepthBuf.height = IMG_HEIGHT;
//    mDepthBuf.stride = IMG_WIDTH*FRAS_SRC_TYPE_DEPTH_BYTES_PER_PIXEL; // make stride=width
    mDepthBuf.stride = IMG_WIDTH*FRAS_SRC_TYPE_DEPTH_BYTES_PER_PIXEL;
    mDepthBuf.format = 0x9991; // unknown
    mDepthBuf.timestampHigh = 0; // !!NOTES: no need if hw-sync
    mDepthBuf.timestampLow = 0; // // !!NOTES: no need if hw-sync

    mDepthBuf.srcType = FRAS_SRC_TYPE_FILE;

    FUNC_END;
    return std::pair<int, unsigned int>(mDepthSimNonSecureGzShmHandle, IMG_SIZE);

err_acquireDepthNonSecureInfoByFile:

    releaseDepthNonSecureInfoByFile();

    FUNC_END;
    return std::pair<int, unsigned int>(0, 0);
}

void FRAlgoASInputCollector::releaseDepthNonSecureInfoByFile()
{
    if (mDepthBuf.srcType != FRAS_SRC_TYPE_FILE)
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mDepthBuf.srcType, FRAS_SRC_TYPE_FILE);
        return;
    }

    FUNC_START;

    // reset mpDepthSimMappedBuffer
    if (mpDepthSimMappedBuffer != NULL)
    {
        memset(mpDepthSimMappedBuffer, 0xff, 4);
    }

    FUNC_END;
    return;
}


std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireIrNonSecureInfo(
    const int shareFD, uint64_t size)
{
    FUNC_START;

// case: NG: need GZ-support UREE_RegisterSharedmem(normal-ion-mapped-memory)
#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    mIrBuf.srcType = FRAS_SRC_TYPE_NON_SECURE;
    FUNC_END;
    return std::pair<int, unsigned int>(mIrSimNonSecureGzShmHandle, size);

// case: normal-ion + memcpy to nonSecure buffer
#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
    ion_user_handle_t ionHandle = 0;

    if (mIonDevFd > 0)
    {
        int ret =  ion_import(mIonDevFd, shareFD, &ionHandle);
        MY_LOGD_IF(mLogLevel >= 1, "%s: mState: %d, ionHandle imported : %d", __FUNCTION__, mState, ionHandle);
    }
    mIrIonHandle = ionHandle;
    mpIrMappedBuffer = (unsigned char*) ion_mmap(mIonDevFd, 0, size, PROT_READ|PROT_WRITE, MAP_SHARED, shareFD, 0);
    long pageSize = sysconf(_SC_PAGESIZE);
    if (mpIrMappedBuffer == NULL || mpIrMappedBuffer == (void*) -1 || ((long)mpIrMappedBuffer%pageSize) != 0)
    {
        //!!NOTES: mapped buffer address needs to be 4096-byte aligned so GZ can get the correct memory starting addr
        MY_LOGE("!!err: %s: ion_mmap error: buffer=%p, pageSize=%ld", __FUNCTION__, mpIrMappedBuffer, pageSize);
        return std::pair<int, unsigned int>(0, 0);
    }
    irMappedBufLen = size;
    irSimMappedBufLen = size;

    // memcpy version due to ion_mapped buffer can't be registered to GZ sharemem
    MY_LOGD_IF(mLogLevel >= 1, "%s: mpIrSimMappedBuffer=%p, mpIrMappedBuffer=%p, size=%d",
        __FUNCTION__,
        mpIrSimMappedBuffer,
        mpIrMappedBuffer,
        size
        );

    if (mpIrSimMappedBuffer != NULL && mpIrMappedBuffer != NULL)
    {
        memcpy(mpIrSimMappedBuffer, mpIrMappedBuffer, size);
    }
    mIrBuf.srcType = FRAS_SRC_TYPE_NON_SECURE;

    printImage(mpIrSimMappedBuffer, FRAS_IR_COLOR_FORMAT, mLogLevel);

    FUNC_END;
    return std::pair<int, unsigned int>(mIrSimNonSecureGzShmHandle, size);

#else // !!NOTES: this case should not happen
    MY_LOGE("!!err: %s: should not happen, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
    return std::pair<int, unsigned int>(0, 0);
#endif

}

void FRAlgoASInputCollector::releaseIrNonSecureInfo()
{
    if (mIrBuf.srcType != FRAS_SRC_TYPE_NON_SECURE)
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mIrBuf.srcType, FRAS_SRC_TYPE_NON_SECURE);
        return;
    }
    FUNC_START;

// case: normal-ion simulution + UREE_RegisterSharedmem(ion-mappped-mapped-memory): NG: need GZ support
#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    // verify
    if (mpIrSimMappedBuffer != NULL)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: mpIrSimMappedBuffer=(%d,%d,%d,%d)",
            __FUNCTION__,
            mpIrSimMappedBuffer[0], mpIrSimMappedBuffer[1], mpIrSimMappedBuffer[2], mpIrSimMappedBuffer[3]
            );

        // reset buffer
        memset(mpIrSimMappedBuffer, 0xff, 4);
    }

// case: normal-ion-simulation case OR normal-ion REAL case
#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
    if (mpIrMappedBuffer != NULL)
    {
        ion_munmap(mIonDevFd, mpIrMappedBuffer, irMappedBufLen);
    }
    mpIrMappedBuffer = NULL;
    irMappedBufLen  = 0;

    if (mIrIonHandle > 0)
    {
        ion_free(mIonDevFd, mIrIonHandle);
    }
    mIrIonHandle = 0;

    // reset buffer
    #if FRAS_CA_REAL_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
    if (mpIrSimMappedBuffer)
    {
        memset(mpIrSimMappedBuffer, 0xff, 4);
    }
    #endif

#else // !!NOTES: this case should not happen
    MY_LOGE("!!err: %s: should not happen, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
#endif

    FUNC_END;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireDepthNonSecureInfo(
    const int shareFD, uint64_t size)
{
    FUNC_START;

    ion_user_handle_t ionHandle = 0;

#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    // do nothing 
    // because mpIrMappedBuffer is already inited in allocIrNoneSecureIonAndRegisterShm
    mDepthBuf.srcType = FRAS_SRC_TYPE_NON_SECURE;

    FUNC_END;
    return std::pair<int, unsigned int>(mDepthSimNonSecureGzShmHandle, size);
#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
 // normal-ion case
    if (mIonDevFd > 0)
    {
        int ret =  ion_import(mIonDevFd, shareFD, &ionHandle);
        MY_LOGD_IF(mLogLevel >= 1, "%s: mState: %d, ionHandle imported : %d", __FUNCTION__, mState, ionHandle);
    }
    mpDepthMappedBuffer = (char*)ion_mmap(mIonDevFd, 0, size, PROT_READ|PROT_WRITE, MAP_SHARED, shareFD, 0);
    long pageSize = sysconf(_SC_PAGESIZE);
    if (mpDepthMappedBuffer == NULL || mpDepthMappedBuffer == (void*) -1 || ((long)mpDepthMappedBuffer%pageSize) != 0)
    {
        //!!NOTES: mapped buffer address needs to be 4096-byte aligned so GZ can get the correct memory starting addr
        MY_LOGE("!!err: %s: ion_mmap error: buffer=%p, pageSize=%ld", __FUNCTION__, mpDepthMappedBuffer, pageSize);
        return std::pair<int, unsigned int>(0, 0);
    }

    depthMappedBufLen = size;
    depthSimMappedBufLen = size;

    // memcpy version due to ion_mapped buffer can't be registered to GZ sharemem
    MY_LOGD_IF(mLogLevel >= 1, "%s: mpDepthSimMappedBuffer=%p, mpDepthMappedBuffer=%p, size=%d",
        __FUNCTION__,
        mpDepthSimMappedBuffer,
        mpDepthMappedBuffer,
        size
        );

    if (mpDepthSimMappedBuffer != NULL && mpDepthMappedBuffer != NULL)
    {
        memcpy(mpDepthSimMappedBuffer, mpDepthMappedBuffer, size);
    }
    mDepthBuf.srcType = FRAS_SRC_TYPE_NON_SECURE;

    printImage(mpDepthSimMappedBuffer, FRAS_DEPTH_COLOR_FORMAT, mLogLevel);

    FUNC_END;
    return std::pair<int, unsigned int>(mDepthSimNonSecureGzShmHandle, size);

#else // !!NOTES: this case should not happen
    MY_LOGE("!!err: %s: should not happen, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
#endif
}

void FRAlgoASInputCollector::releaseDepthNonSecureInfo()
{
    if (mDepthBuf.srcType != FRAS_SRC_TYPE_NON_SECURE)
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mDepthBuf.srcType, FRAS_SRC_TYPE_NON_SECURE);
        return;
    }
    FUNC_START;

#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    // verify
    if (mpDepthSimMappedBuffer != NULL)
    {
        MY_LOGD_IF(mLogLevel >= 1, "%s: mpDepthSimMappedBuffer=(%d,%d,%d,%d)",
            __FUNCTION__,
            mpDepthSimMappedBuffer[0], mpDepthSimMappedBuffer[1], mpDepthSimMappedBuffer[2], mpDepthSimMappedBuffer[3]
            );
        // reset buffer
        memset(mpDepthSimMappedBuffer, 0xff, 4);
    }
#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
    if (mpDepthMappedBuffer != NULL)
    {
        ion_munmap(mIonDevFd, mpDepthMappedBuffer, depthMappedBufLen);
    }
    mpDepthMappedBuffer = NULL;
    depthMappedBufLen = 0;

    if (mDepthIonHandle > 0)
    {
        ion_free(mIonDevFd, mDepthIonHandle);
    }
    mDepthIonHandle = 0;

    // reset buffer
    #if FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
    if (mpDepthSimMappedBuffer)
    {
        memset(mpDepthSimMappedBuffer, 0xff, 4);
    }
    #endif
#else // !!NOTES: this case should not happen
    MY_LOGE("!!err: %s: should not happen, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
#endif
    
    FUNC_END;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireIrSecureInfo(const int shareFD)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;
    ion_user_handle_t ionHandle = 0;

    if (mIonDevFd > 0)
    {
        int ret =  ion_import(mIonDevFd, shareFD, &ionHandle);
        MY_LOGD_IF(mLogLevel >= 1, "%s: State: %d, ionHandle imported : %d", __FUNCTION__, mState, ionHandle);
    }
    mIrIonHandle = ionHandle;

    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = ionHandle;
    if (ion_custom_ioctl(mIonDevFd, ION_CMD_SYSTEM, &sys_data))
    {
        MY_LOGE("!!err: %s: ion_custom_ioctl failed to get secure handle", __FUNCTION__);
        FUNC_END;
        return std::pair<int, unsigned int>();
    }

    // transfer secure handle to gz secure handle
    TZ_RESULT ret;
    UREE_SHAREDMEM_HANDLE gz_shm_handle;
    ret = UREE_ION_TO_SHM_HANDLE(sys_data.get_phys_param.phy_addr, &gz_shm_handle);
    if (ret != TZ_RESULT_SUCCESS)
    {
        MY_LOGE("!!err: %s: UREE_ION_TO_SHM_HANDLE failed: ret=%d", __FUNCTION__, ret);
        FUNC_END;
        return std::pair<int, unsigned int>();
    }

    ALOGI("%s: Secure memory: ionHandle(%d), size(%lu), "
            "gzHandle(%d)",
            __FUNCTION__,
            sys_data.get_phys_param.handle, sys_data.get_phys_param.len,
            gz_shm_handle
            );

    mIrBuf.srcType = FRAS_SRC_TYPE_SECURE;

    FUNC_END;
    return std::pair<int, unsigned int>(gz_shm_handle, sys_data.get_phys_param.len);
#else // non-MTK_CAM_SECURITY_SUPPORT
    return std::pair<int, unsigned int>();
#endif
}

void FRAlgoASInputCollector::releaseIrSecureInfo()
{
    FUNC_START;
    if (mIrBuf.srcType != FRAS_SRC_TYPE_SECURE)
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mIrBuf.srcType, FRAS_SRC_TYPE_SECURE);
        FUNC_END;
        return;
    }

    if (mIrIonHandle > 0)
    {
        ion_free(mIonDevFd, mIrIonHandle);
    }
    FUNC_END;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgoASInputCollector::acquireDepthSecureInfo(const int shareFD)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;
    ion_user_handle_t ionHandle = 0;

    if (mIonDevFd > 0)
    {
        int ret =  ion_import(mIonDevFd, shareFD, &ionHandle);
        MY_LOGD_IF(mLogLevel >= 1, "%s: mState: %d, ionHandle imported : %d", __FUNCTION__, mState, ionHandle);
    }
    
    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = ionHandle;
    if (ion_custom_ioctl(mIonDevFd, ION_CMD_SYSTEM, &sys_data))
    {
        MY_LOGE("!!err: %s: ion_custom_ioctl failed to get secure handle", __FUNCTION__);
        FUNC_END;
        return std::pair<int, unsigned int>(0, 0);
    }

    mDepthIonHandle = ionHandle;

    // transfer secure handle to gz secure handle
    TZ_RESULT ret;
    UREE_SHAREDMEM_HANDLE gz_shm_handle;
    ret = UREE_ION_TO_SHM_HANDLE(sys_data.get_phys_param.phy_addr, &gz_shm_handle);
    if (ret != TZ_RESULT_SUCCESS)
    {
        MY_LOGE("!!err: %s: UREE_ION_TO_SHM_HANDLE failed: ret=%d", __FUNCTION__, ret);
        FUNC_END;
        return std::pair<int, unsigned int>(0, 0);
    }

    ALOGI("%s: Secure memory: ionHandle(%d), size(%lu), "
            "gzHandle(%d)",
            __FUNCTION__,
            sys_data.get_phys_param.handle, sys_data.get_phys_param.len,
            gz_shm_handle
            );

    mDepthBuf.srcType = FRAS_SRC_TYPE_SECURE;

    FUNC_END;
    return std::pair<int, unsigned int>(gz_shm_handle, sys_data.get_phys_param.len);
#else // non-MTK_CAM_SECURITY_SUPPORT
    return std::pair<int, unsigned int>(0, 0);
#endif
}

void FRAlgoASInputCollector::releaseDepthSecureInfo()
{
    if (mDepthBuf.srcType != FRAS_SRC_TYPE_SECURE)
    {
        ALOGW("%s: srcType wrong: %d, expected: %d", __FUNCTION__, mDepthBuf.srcType, FRAS_SRC_TYPE_SECURE);
        return;
    }

    FUNC_START;
    if (mDepthIonHandle > 0)
    {
        ion_free(mIonDevFd, mDepthIonHandle);
    }
    FUNC_END;
}

void FRAlgoASInputCollector::dumpBuffer()
{
#if 0
    NSCam::security::utils::IonHelper helper;
    size_t dataSize = 0;
    int sharedFD = 0;
    
    if (mIrImportedBuffer != NULL)
    {
        sharedFD = (mIrImportedBuffer)->data[0];

        MY_LOGD_IF(mLogLevel >= 1, %s: dump IR-Buf: sharedFD=%d, size=%d, %dx%d, stride=%d, format=%d ", 
            __FUNCTION__, 
            sharedFD, mIrBuf.size, mIrBuf.width, mIrBuf.height, mIrBuf.stride, mIrBuf.format);
            
        helper.dumpBuffer(sharedFD, 
            mIrBuf.size, mIrBuf.width, mIrBuf.height, mIrBuf.stride, mIrBuf.format, true);
    }
    if (mDepthImportedBuffer != NULL)
    {
        sharedFD = (mDepthImportedBuffer)->data[0];

        MY_LOGD_IF(mLogLevel >= 1, "%s: dump Depth-Buf: sharedFD=%d, size=%d, %dx%d, stride=%d, format=%d ", 
            __FUNCTION__, 
            sharedFD, mDepthBuf.size, mDepthBuf.width, mDepthBuf.height, mDepthBuf.stride, mDepthBuf.format);

        helper.dumpBuffer(sharedFD, 
            mDepthBuf.size, mDepthBuf.width, mDepthBuf.height, mDepthBuf.stride, mDepthBuf.format, true);
    }
#endif
}
FRAlgoASInputCollector::InputCollectState_ENUM FRAlgoASInputCollector::getState()
{
    return mState;
}

void FRAlgoASInputCollector::setState(FRAlgoASInputCollector::InputCollectState_ENUM state)
{
    MY_LOGD_IF(mLogLevel >= 1, "%s: oldState: %d, newState: %d", __FUNCTION__, mState, state);
    mState = state;
}

int FRAlgoASInputCollector::sim_allocIrNonSecureIonAndRegisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT

#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE // ps: NG: need GZ-support UREE_RegisterSharedmem(normal-ion mapped-memory)
    FUNC_START;
    mIrSimAllocIonHandle = 0;
    mIrSimIonShareFD = 0;

    const long pageSize = sysconf(_SC_PAGESIZE);
    int allocSize = FRAS_CA_IR_SRC_SHAREDMEM_SIZE;
	int ret = ion_alloc(mIonDevFd, allocSize, pageSize, ION_HEAP_MULTIMEDIA_MASK, 0, &mIrSimAllocIonHandle);
    if (ret)
    {
        MY_LOGE("!!err: %s: ion_alloc failed: ret=%d, allocSize=%ld, pageSize=%d",
            __FUNCTION__,
            ret, allocSize, pageSize);
        goto err_sim_allocIrNonSecureIonAndRegisterShm;
    }
    ret = ion_share(mIonDevFd, mIrSimAllocIonHandle, &mIrSimIonShareFD);
    if (ret)
    {
        MY_LOGE("!!err: %s: ion_share failed: ret=%d", __FUNCTION__, ret);
        goto err_sim_allocIrNonSecureIonAndRegisterShm;
    }
    mpIrSimMappedBuffer = (unsigned char*) ion_mmap(mIonDevFd, 0, allocSize, PROT_READ|PROT_WRITE, MAP_SHARED, mIrSimIonShareFD, 0);
    if (mpIrSimMappedBuffer == NULL || mpIrSimMappedBuffer == (void*) -1 || ((long)mpIrSimMappedBuffer%pageSize) != 0)
    {
        //!!NOTES: mapped buffer address needs to be 4096-byte aligned so GZ can get the correct memory starting addr
        MY_LOGE("!!err: %s: ion_mmap error: buffer=%p, pageSize=%ld", __FUNCTION__, mpIrSimMappedBuffer, pageSize);
        goto err_sim_allocIrNonSecureIonAndRegisterShm;
    }
    MY_LOGD_IF(mLogLevel >= 2, "%s: mpIrSimMappedBuffer=%p, allocSize=%d", __FUNCTION__, mpIrSimMappedBuffer, irSimMappedBufLen);

    // init memory
    if (mpIrSimMappedBuffer != NULL)
    {
        memset(mpIrSimMappedBuffer, 0xff, 4);
    }
    irSimMappedBufLen = FRAS_CA_IR_SRC_SHAREDMEM_SIZE;

    // translate to GZ param
    {
        // translate ion VA to ion PA first
        // TODO:
        // register
        UREE_SHAREDMEM_PARAM shm_param;
        shm_param.buffer = (void*) mpIrSimMappedBuffer;
        shm_param.size = irSimMappedBufLen;

        TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mIrSimNonSecureGzShmHandle, &shm_param);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_RegisterSharedmem failed: %d", __FUNCTION__, retTee);
            goto err_sim_allocIrNonSecureIonAndRegisterShm;
        }
        MY_LOGD_IF(mLogLevel >= 2, "%s: irMappedBufLen=%d, shm_param.size=%d", 
            __FUNCTION__, 
            irSimMappedBufLen, shm_param.size);
    }

    FUNC_END;
    return FR_ERR_OK;
err_sim_allocIrNonSecureIonAndRegisterShm:

    sim_freeIrNonSecureIonAndUnregisterShm();
    FUNC_END;
    return -1;

#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)

    FUNC_START;

    int ret = FR_ERR_OK;
    mpIrSimMappedBuffer = (char *)memalign(sysconf(_SC_PAGESIZE), FRAS_CA_IR_SRC_SHAREDMEM_SIZE);
    if (mpIrSimMappedBuffer == NULL)
    {
        MY_LOGE("!!err: %s: memalign mpIrSimMappedBuffer shared mem failed\n", __FUNCTION__);
        ret =FR_ERR_ALGO_RETURN_MALLOC_FAILED;
        goto err_sim_allocIrNonSecureIonAndRegisterShm;
    }
    irSimMappedBufLen = FRAS_CA_IR_SRC_SHAREDMEM_SIZE;
    MY_LOGD_IF(mLogLevel>= 2, "%s: memalign: mpIrSimMappedBuffer(%p)", __FUNCTION__, mpIrSimMappedBuffer);

    // translate to GZ param
    {
        // translate ion VA to ion PA first
        // register
        UREE_SHAREDMEM_PARAM shm_param;
        shm_param.buffer = (void*) mpIrSimMappedBuffer;
        shm_param.size = irSimMappedBufLen;

        TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mIrSimNonSecureGzShmHandle, &shm_param);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_RegisterSharedmem failed: %d", __FUNCTION__, retTee);
            goto err_sim_allocIrNonSecureIonAndRegisterShm;
        }
        MY_LOGD_IF(mLogLevel >= 2, "%s: mIrSimNonSecureGzShmHandle=%d, irMappedBufLen=%d, shm_param.size=%d", 
            __FUNCTION__, 
            mIrSimNonSecureGzShmHandle, irSimMappedBufLen, shm_param.size);
    }
    
    FUNC_END;
    return FR_ERR_OK;

err_sim_allocIrNonSecureIonAndRegisterShm:
    sim_freeIrNonSecureIonAndUnregisterShm();
    FUNC_END;
    return -1;

#else //!!NOTES: this should not happen
    FUNC_START
    MY_LOGD_IF(mLogLevel >= 2, "%s: do nothing due to FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
    FUNC_END;
    return FR_ERR_OK;
#endif

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoASInputCollector::sim_freeIrNonSecureIonAndUnregisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT

#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    FUNC_START;

    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mIrSimNonSecureGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mIrSimNonSecureGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(IR): ret=%d",__FUNCTION__, retTee);
        }
    }
    mIrSimNonSecureGzShmHandle = 0;

    if (mIonDevFd > 0)
    {
        if (mpIrSimMappedBuffer != NULL)
        {
            ion_munmap(mIonDevFd, mpIrSimMappedBuffer, irSimMappedBufLen);    
        }
        mpIrSimMappedBuffer = NULL;
        irSimMappedBufLen = 0;
        
        if (mIrSimIonShareFD > 0)
        {
            close(mIrSimIonShareFD);
        }
        mIrSimIonShareFD = 0;
        
        if (mIrSimAllocIonHandle > 0)
        {
            ion_free(mIonDevFd, mIrSimAllocIonHandle);
        }
        mIrSimAllocIonHandle = 0;
    }
    else
    {
        mpIrSimMappedBuffer = NULL;
        mDepthSimIonShareFD = 0;
        mIrSimIonShareFD = 0;
        mIrSimAllocIonHandle = 0;
    }


    FUNC_END;
    return FR_ERR_OK;

#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)

    FUNC_START;
    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mIrSimNonSecureGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mIrSimNonSecureGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(IR): ret=%d",__FUNCTION__, retTee);
        }
        else
        {
            MY_LOGD_IF(mLogLevel >= 2, "%s: UREE_UnregisterSharedmem ok", __FUNCTION__);
        }
    }
    mIrSimNonSecureGzShmHandle = 0;

    if (mpIrSimMappedBuffer)
    {
        free(mpIrSimMappedBuffer);
    }
    mpIrSimMappedBuffer = NULL;
    FUNC_END;
    return FR_ERR_OK;
#else // ori:
    FUNC_START
    MY_LOGD_IF(mLogLevel >= 2, "%s: do nothing due to FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
    FUNC_END;
    return FR_ERR_OK;
#endif

#else // non-MTK_CAM_SECURITY_SUPPORT

    return FR_ERR_CMD_NOT_IMPLEMETED;

#endif
}

int FRAlgoASInputCollector::sim_allocDepthNonSecureIonAndRegisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT

#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    FUNC_START;
    mDepthSimAllocIonHandle = 0;
    mDepthSimIonShareFD = 0;

    const long pageSize = sysconf(_SC_PAGESIZE);
    int allocSize = FRAS_CA_DEPTH_SRC_SHAREDMEM_SIZE;

	int ret = ion_alloc(mIonDevFd, allocSize, pageSize, ION_HEAP_MULTIMEDIA_MASK, 0, &mDepthSimAllocIonHandle);
    if (ret)
    {
        MY_LOGE("!!err: %s: ion_alloc failed: ret=%d, allocSize=%ld, pageSize=%d",
            __FUNCTION__,
            ret, allocSize, pageSize);
        goto err_sim_allocDepthNonSecureIonAndRegisterShm;
    }
    ret = ion_share(mIonDevFd, mDepthSimAllocIonHandle, &mDepthSimIonShareFD);
    if (ret)
    {
        MY_LOGE("!!err: %s: ion_share failed: ret=%d", __FUNCTION__, ret);
        goto err_sim_allocDepthNonSecureIonAndRegisterShm;
    }
    mpDepthSimMappedBuffer = (unsigned char*) ion_mmap(mIonDevFd, 0, allocSize, PROT_READ|PROT_WRITE, MAP_SHARED, mDepthSimIonShareFD, 0);
    if (mpDepthSimMappedBuffer == NULL || mpDepthSimMappedBuffer == (void*) -1 || ((long)mpDepthSimMappedBuffer%pageSize) != 0)
    {
        //!!NOTES: mapped buffer address needs to be 4096-byte aligned so GZ can get the correct memory starting addr
        MY_LOGE("!!err: %s: ion_mmap error: buffer=%p, pageSize=%ld", __FUNCTION__, mpDepthMappedBuffer, pageSize);
        goto err_sim_allocDepthNonSecureIonAndRegisterShm;
    }
    MY_LOGD_IF(mLogLevel >= 2, "%s: mpDepthSimMappedBuffer=%p, allocSize=%d", __FUNCTION__, mpDepthSimMappedBuffer, depthSimMappedBufLen);

    // init memory
    if (mpDepthSimMappedBuffer != NULL)
    {
        memset(mpDepthSimMappedBuffer, 0xff, 4);
    }
    depthSimMappedBufLen = FRAS_CA_DEPTH_SRC_SHAREDMEM_SIZE;

    // translate to GZ param
    {
        // translate ion VA to ion PA first
        // TODO:

        UREE_SHAREDMEM_PARAM shm_param;
        shm_param.buffer = (void*) mpDepthSimMappedBuffer;
        shm_param.size = depthSimMappedBufLen;

        TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mDepthSimNonSecureGzShmHandle, &shm_param);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_RegisterSharedmem failed: %d", __FUNCTION__, retTee);
            goto err_sim_allocDepthNonSecureIonAndRegisterShm;
        }
        MY_LOGD_IF(mLogLevel >= 2, "%s: mDepthSimNonSecureGzShmHandle=%d, depthMappedBufLen=%d, shm_param.size=%d", 
            __FUNCTION__, 
            mDepthSimNonSecureGzShmHandle,
            depthSimMappedBufLen, shm_param.size);
    }

    FUNC_END;
    return FR_ERR_OK;

err_sim_allocDepthNonSecureIonAndRegisterShm:

    sim_freeDepthNonSecureIonAndUnregisterShm();
    FUNC_END;
    return -1;

#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
    FUNC_START;

    int ret = FR_ERR_OK;
    mpDepthSimMappedBuffer = (char *)memalign(sysconf(_SC_PAGESIZE), FRAS_CA_DEPTH_SRC_SHAREDMEM_SIZE);
    if (mpDepthSimMappedBuffer == NULL)
    {
        MY_LOGE("!!err: %s: memalign mpDepthSimMappedBuffer shared mem failed\n", __FUNCTION__);
        ret =FR_ERR_ALGO_RETURN_MALLOC_FAILED;
        goto err_sim_allocDepthNonSecureIonAndRegisterShm;
    }
    depthSimMappedBufLen = FRAS_CA_DEPTH_SRC_SHAREDMEM_SIZE;

    MY_LOGD_IF(mLogLevel >= 2, "%s: memalign: mpDepthSimMappedBuffer(%p)", __FUNCTION__, mpDepthSimMappedBuffer);

    // translate to GZ param
    {
        UREE_SHAREDMEM_PARAM shm_param;
        shm_param.buffer = (void*) mpDepthSimMappedBuffer;
        shm_param.size = depthSimMappedBufLen;

        TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mDepthSimNonSecureGzShmHandle, &shm_param);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_RegisterSharedmem failed: %d", __FUNCTION__, retTee);
            goto err_sim_allocDepthNonSecureIonAndRegisterShm;
        }
        MY_LOGD_IF(mLogLevel >= 2, "%s: depthMappedBufLen=%d, shm_param.size=%d", __FUNCTION__, depthSimMappedBufLen, shm_param.size);
    }
    FUNC_END;
    return FR_ERR_OK;

err_sim_allocDepthNonSecureIonAndRegisterShm:
    FUNC_END;
    sim_freeDepthNonSecureIonAndUnregisterShm();
    FUNC_END;
    return -1;
#else //!!NOTES: this should not happen
    FUNC_START
    MY_LOGD_IF(mLogLevel >= 2, "%s: do nothing due to FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
    FUNC_END;
    return FR_ERR_OK;
#endif

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoASInputCollector::sim_freeDepthNonSecureIonAndUnregisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
#if FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE
    FUNC_START;

    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mDepthSimNonSecureGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mDepthSimNonSecureGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(Depth): ret=%d",__FUNCTION__, retTee);
        }
    }
    mDepthSimNonSecureGzShmHandle = 0;

    if (mIonDevFd > 0)
    {
        if (mpDepthSimMappedBuffer != NULL)
        {
            ion_munmap(mIonDevFd, mpDepthSimMappedBuffer, depthSimMappedBufLen);    
        }
        mpDepthSimMappedBuffer = NULL;
        
        depthSimMappedBufLen = 0;
        
        if (mDepthSimIonShareFD > 0)
        {
            close(mDepthSimIonShareFD);
        }
        mDepthSimIonShareFD = 0;
        
        if (mDepthSimAllocIonHandle > 0)
        {
            ion_free(mIonDevFd, mDepthSimAllocIonHandle);
        }
        mDepthSimAllocIonHandle = 0;
    }
    else
    {
        mpDepthSimMappedBuffer = NULL;
        depthSimMappedBufLen = 0;
        mDepthSimIonShareFD = 0;
        mDepthSimAllocIonHandle = 0;
    }

    FUNC_END;
    return FR_ERR_OK;
#elif (FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY || FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY)
    FUNC_START;
    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mDepthSimNonSecureGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mDepthSimNonSecureGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(Depth): ret=%d",__FUNCTION__, retTee);
        }
        else
        {
            MY_LOGD_IF(mLogLevel >= 2, "%s: UREE_UnregisterSharedmem ok", __FUNCTION__);
        }
    }
    mDepthSimNonSecureGzShmHandle = 0;

    if (mpDepthSimMappedBuffer)
    {
        free(mpDepthSimMappedBuffer);
    }
    mpDepthSimMappedBuffer = NULL;
    FUNC_END;
    return FR_ERR_OK;
#else // ori:
    FUNC_START
    MY_LOGD_IF(mLogLevel >= 2, "%s: do nothing due to FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE=%d, FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d, FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY=%d",
        __FUNCTION__,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_SHARE,
        FRAS_CA_SIM_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY,
        FRAS_CA_REAL_SECCAM_CB_BUF_NON_SECURE_ION_MEMCPY
        );
    FUNC_END;
    return FR_ERR_OK;
#endif

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

// === FRAlgoCA_AS API ===
FRAlgoCA_AS::FRAlgoCA_AS(char const *pCaller, const FRCFG_ENUM frConfig)
    :FRAlgoCA(pCaller, frConfig)
{
    FUNC_START;

    mpIrFileDumpBuf = mpDepthFileDumpBuf = NULL;
    mIrFileDumpGzShmHandle = mDepthFileDumpGzShmHandle = 0;
    mMemSrvSession = 0;

    mIsInited = 0;
    reset();
    FUNC_END;
}

FRAlgoCA_AS::~FRAlgoCA_AS()
{
    FUNC_START;
    reset();
    FUNC_END;
}

ISecureCameraClientCallback* FRAlgoCA_AS::getSecureCameraClientCallback()
{
    if (mSecureCamCliCb == NULL)
    {
        return new FRAlgoCA_AS::SecureCamClientCallback(this);
    }
    else
    {
        return mSecureCamCliCb.get();
    }
}

int FRAlgoCA_AS::init()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    // step-1: create session
    // step-2: create SecureCam and call its init(..) function
    // step-3: create thread to handle SecureCam callback and callback to App

    FUNC_START;

    TZ_RESULT retTee;
    const char *kMemSrvName = "com.mediatek.geniezone.srv.mem";

    mPropSeccamCbBufDump = property_get_int32(FRAS_SECCAM_CB_BUF_DUMP_PROP, FRAS_SECCAM_CB_BUF_DUMP_PROP_DEF_VAL);

    mPropNeedPrepFRInput = property_get_int32(FRAS_CA_NEED_PREP_FRINPUT_PROP, FRAS_CA_NEED_PREP_FRINPUT_DEF_VAL);
    mPropSrcType = property_get_int32(FRAS_CA_SRC_TYPE_PROP, FRAS_CA_SRC_TYPE_PROP_DEF_VAL);
    mPropCaLogLevel = property_get_int32(FRAS_CA_LOGLEVEL_PROP, FRAS_CA_LOGLEVEL_PROP_DEF_VAL);
    mPropCaCallTa = property_get_int32(FRAS_CA_CALL_TA_PROP, FRAS_CA_CALL_TA_PROP_DEF_VAL);

    mPropTaLogLevel = property_get_int32(FRAS_TA_LOGLEVEL_PROP, FRAS_CA_CALL_TA_PROP_DEF_VAL);
    mPropTaCallAlgo = property_get_int32(FRAS_TA_CALL_ALGO_PROP, FRAS_TA_CALL_ALGO_PROP_DEF_VALUE);
    mPropTaChkPolicy = property_get_int32(FRAS_TA_CHK_POLICY_PROP, FRAS_TA_CHK_POLICY_PROP_DEF_VALUE);

    ALOGD("%s: mPropNeedPrepFRInput=%d, mPropSeccamCbBufDump=%d, mPropSrcType=%d, mPropCaLogLevel=%d, mPropCaCallTa=%d, mPropTaLogLevel=%d, mPropTaCallAlgo=%d, mPropTaChkPolicy=%d",
        __FUNCTION__,
        mPropNeedPrepFRInput, mPropSeccamCbBufDump, mPropSrcType, mPropCaLogLevel, mPropCaCallTa, mPropTaLogLevel, mPropTaCallAlgo, mPropTaChkPolicy
        );

    Mutex::Autolock lock(mLock);

    int ret = 0;

    if (mIsInited != 0)
    {
        MY_LOGE("!!err: %s::FRAlgoCA is already inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_ALREADY_INITED; // already initied
    }

    if ((ret = FRAlgoCA::tee_init(MTEE_FR_SVC_NAME)) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::seccam_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::fralgo_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::workthread_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

// === private ===
    // create session for shared memory
    retTee = UREE_CreateSession(kMemSrvName, &mMemSrvSession);
    if (retTee != TZ_RESULT_SUCCESS)
    {
        MY_LOGE("!!err: %s: create mem sesion failed(%d)", __FUNCTION__, ret);
        goto err_init;
    }
    else
    {
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: create mem sesion OK, session=0x%x", __FUNCTION__, mMemSrvSession);
    }
    // FRAlgoInputCollector setting
    mFRAlgoIC.setMemSrvSession(mMemSrvSession);
    if ((ret=mFRAlgoIC.sim_allocIrNonSecureIonAndRegisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: sim_allocIrNonSecureIonAndRegisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    if ((ret=mFRAlgoIC.sim_allocDepthNonSecureIonAndRegisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: sim_allocDepthNonSecureIonAndRegisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    if ((ret=this->debug_allocIrFileDumpBufAndRegisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: debug_allocIrFileDumpBufAndRegisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    if ((ret=this->debug_allocDepthFileDumpBufAndRegisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: debug_allocDepthFileDumpBufAndRegisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    mFRAlgoIC.mLogLevel = mPropCaLogLevel;

    mState = FRCA_STATE_ALIVE;
    mIsInited = 1;

    return FR_ERR_OK;

err_init:

    ALOGW("%s:: FRAlgoCA init failed %d", __FUNCTION__, ret);
    reset();

    FUNC_END;
    return ret;

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif

}

int FRAlgoCA_AS::uninit()
{
    // FR-custom
    FUNC_START;
    {
        Mutex::Autolock lock(mLock);
        mState = FRCA_STATE_UNINIT; // change uninit state first to stop FRWorkThread and OnBufferThread
    }

    Mutex::Autolock lock(mLock);
    reset();

    FUNC_END;
    return FR_ERR_OK;
}

void FRAlgoCA_AS::reset()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
// FR-custom
    FUNC_START;
    int ret = FR_ERR_OK;
    TZ_RESULT retTee;

    FRAlgoCA::reset();

    mFRAlgoIC.releaseIrResource();
    mFRAlgoIC.releaseDepthResource();

//    if ((ret=mFRAlgoIC.freeSecCamIrSrcSharedMem()) != FR_ERR_OK)
    if ((ret=mFRAlgoIC.sim_freeIrNonSecureIonAndUnregisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: freeIrNonSecureIonAndUnregisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    //    if ((ret=mFRAlgoIC.freeSecCamDepthSrcSharedMem()) != FR_ERR_OK)
    if ((ret=mFRAlgoIC.sim_freeDepthNonSecureIonAndUnregisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: freeDepthNonSecureIonAndUnregisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    if ((ret=this->debug_freeIrFileDumpBufAndUnregisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: freeIrFileDumpBufAndUnregisterShm failed: ret=%d", __FUNCTION__, ret);
    }
    if ((ret=this->debug_freeDepthFileDumpBufAndUnregisterShm()) != FR_ERR_OK)
    {
        MY_LOGE("!!err: %s: freeDepthFileDumpBufAndUnregisterShm failed: ret=%d", __FUNCTION__, ret);
    }

    // close session for shared memory
    if (mMemSrvSession != (UREE_SESSION_HANDLE)0)
    {
        retTee = UREE_CloseSession(mMemSrvSession);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: close mem sesion failed(%d)", __FUNCTION__, retTee);
        }
    }
    mMemSrvSession = (UREE_SESSION_HANDLE)0;

    FUNC_END;
    return;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return;
#endif
}

int FRAlgoCA_AS::debug_allocIrFileDumpBufAndRegisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    // for IR src dump to file in REE

    int ret = FR_ERR_OK;
    mpIrFileDumpBuf = (char *)memalign(sysconf(_SC_PAGESIZE), FRAS_CA_IR_FILEDUMP_SHAREDMEM_SIZE);
    if (mpIrFileDumpBuf == NULL)
    {
        MY_LOGE("!!err: %s: memalign IR file dump shared mem failed\n", __FUNCTION__);
        ret =FR_ERR_ALGO_RETURN_MALLOC_FAILED;
        goto err_allocIrFileDumpBufAndRegisterShm;
    }
    MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: memalign: mpIrFileDumpBuf(%p)", __FUNCTION__, mpIrFileDumpBuf);

    // Register IR Shared Memory to FRAlgoTA
    {
        UREE_SHAREDMEM_PARAM sharedMemoryParam {
                .buffer = mpIrFileDumpBuf, 
                .size = FRAS_CA_IR_FILEDUMP_SHAREDMEM_SIZE
            };
        {
            TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mIrFileDumpGzShmHandle, &sharedMemoryParam);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                MY_LOGE("!!err: %s: IR UREE_RegisterSharedmem failed: ret=%d",__FUNCTION__, retTee);
                goto err_allocIrFileDumpBufAndRegisterShm;
            }
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: UREE_RegisterSharedmem OK: mIrFileDumpGzShmHandle(%d)", __FUNCTION__, mIrFileDumpGzShmHandle);
        }
    }

    FUNC_END;
    return FR_ERR_OK;

err_allocIrFileDumpBufAndRegisterShm:
    debug_freeIrFileDumpBufAndUnregisterShm();
    FUNC_END;
    return -1;

#else // non-MTK_CAM_SECURITY_SUPPORT
    return -1;
#endif
}


int FRAlgoCA_AS::debug_freeIrFileDumpBufAndUnregisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mIrFileDumpGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mIrFileDumpGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(IR): ret=%d",__FUNCTION__, retTee);
        }
        else
        {
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: UREE_UnregisterSharedmem ok", __FUNCTION__);
        }
    }
    mIrFileDumpGzShmHandle = 0;

    if (mpIrFileDumpBuf)
    {
        free(mpIrFileDumpBuf);
    }
    mpIrFileDumpBuf = NULL;

    FR_ERR_OK;
    return FR_ERR_OK;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoCA_AS::debug_allocDepthFileDumpBufAndRegisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    // for Depth src dump to file in REE

    int ret = FR_ERR_OK;
    mpDepthFileDumpBuf = (char *)memalign(sysconf(_SC_PAGESIZE), FRAS_CA_DEPTH_FILEDUMP_SHAREDMEM_SIZE);
    if (mpDepthFileDumpBuf == NULL)
    {
        MY_LOGE("!!err: %s: memalign Depth file dump shared mem failed\n", __FUNCTION__);
        ret =FR_ERR_ALGO_RETURN_MALLOC_FAILED;
        goto err_allocDepthFileDumpBufAndRegisterShm;
    }
    MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: memalign: mpDepthFileDumpBuf(%p)", __FUNCTION__, mpDepthFileDumpBuf);

    {
        // Register Depth Shared Memory to FRAlgoTA
        UREE_SHAREDMEM_PARAM sharedMemoryParam {
                .buffer = mpDepthFileDumpBuf, 
                .size = FRAS_CA_DEPTH_FILEDUMP_SHAREDMEM_SIZE
            };
        {
            TZ_RESULT retTee = UREE_RegisterSharedmem(mMemSrvSession, &mDepthFileDumpGzShmHandle, &sharedMemoryParam);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                MY_LOGE("!!err: %s: IR UREE_RegisterSharedmem failed: ret=%d",__FUNCTION__, retTee);
                goto err_allocDepthFileDumpBufAndRegisterShm;
            }
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: UREE_RegisterSharedmem OK: mDepthFileDumpGzShmHandle(%d)", __FUNCTION__, mDepthFileDumpGzShmHandle);
        }
    }

    FUNC_END;
    return FR_ERR_OK;

err_allocDepthFileDumpBufAndRegisterShm:

    debug_freeDepthFileDumpBufAndUnregisterShm();
    FUNC_END;
    return -1;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return -1;
#endif
}


int FRAlgoCA_AS::debug_freeDepthFileDumpBufAndUnregisterShm()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    // MTEE service finished using nonSecureSharedMem, now Unregister shared memory
    if (mDepthFileDumpGzShmHandle != 0)
    {
        TZ_RESULT retTee = UREE_UnregisterSharedmem(mMemSrvSession, mDepthFileDumpGzShmHandle);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            MY_LOGE("!!err: %s: UREE_UnregisterSharedmem failed(Depth): ret=%d",__FUNCTION__, retTee);
        }
        else
        {
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: UREE_UnregisterSharedmem ok", __FUNCTION__);
        }
    }
    mDepthFileDumpGzShmHandle = 0;

    if (mpDepthFileDumpBuf)
    {
        free(mpDepthFileDumpBuf);
    }
    mpDepthFileDumpBuf = NULL;

    FR_ERR_OK;
    return FR_ERR_OK;
#else
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoCA_AS::fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream)
{

#ifdef MTK_CAM_SECURITY_SUPPORT
// FR-custom
    FUNC_START;
    Mutex::Autolock lock(mLock);
    
    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: getting lock", __FUNCTION__);
    // === Error Checking ===
    if (mState == FRCA_STATE_UNINIT)
    {
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: State: FRCA_STATE_UNINIT, no need to process", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_WRONG_STATE;
    }

    if (mState == FRCA_STATE_SAVE_FEATURE_ONGOING || mState == FRCA_STATE_COMPARE_FEATURE_ONGOING)
    {
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: FRWorkThread is busy, state(%d), give up preparing FR input this time", __FUNCTION__, mState);
        FUNC_END;
        return FR_ERR_FRALGO_WRONG_STATE;
    }

    if (mState == FRCA_STATE_UNINIT || mState == FRCA_STATE_STOP_FEATURE)
    {
        ALOGD("%s: State(%d), no need to process further", __FUNCTION__, mState);
        FUNC_END;
        return FR_ERR_FRALGO_WRONG_STATE;
    }

    int isCopyIR = 0;
    FRAlgoASInputCollector::InputCollectState_ENUM currICState = mFRAlgoIC.getState();
//    if (currICState == FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR && stream.format == FRAS_IR_COLOR_FORMAT && stream.width == 1304)
    if (currICState == FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR && stream.format == FRAS_IR_COLOR_FORMAT)
    {
        // TODO: check stream.format (Depth: uint16)
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: IR buf to handle", __FUNCTION__);
        isCopyIR = 1;
    }
//    else if (currICState == FRAlgoASInputCollector::FRAIC_WAIT_FOR_DEPTH && stream.format == 0x2201 && stream.width == 1304)
    else if (currICState == FRAlgoASInputCollector::FRAIC_WAIT_FOR_DEPTH && stream.format == FRAS_DEPTH_COLOR_FORMAT)
    {
         // TODO: check stream.format (Depth: uint16)
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: Depth buf to handle", __FUNCTION__);
        isCopyIR = 0;
    }
    else
    {
        ALOGW("%s: wrong input: currICState=%d, stream(fmt=0x%x, %dx%d), ",
                __FUNCTION__, 
                currICState, stream.format, stream.width, stream.height);

        // reset state to FRAIC_WAIT_FOR_IR and release the related resource
        mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR);

        FUNC_END;
        return FR_ERR_FRALGO_WAIT_FOR_IR_BUF;
    }
    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: err chk", __FUNCTION__);

    // === Start Processing data ===
    int ret = FR_ERR_ALGO_OK;
    int oriFormat = 0;
    FRCaTaBuf *pTargetBuf = NULL;

    /* Assume buffer is correct */
    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: currCmd: %d, currICState: %d",  __FUNCTION__, mCurrCmd, currICState);

    if (isCopyIR)
    {
        // IR data in
        if ((ret=mFRAlgoIC.setIrData(mCurrCmd, mPropSrcType, bufferId, buffer, stream) != FR_ERR_OK))
        {
            MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: currCmd: %d, currICState: %d, setData fail: ret=%d, go back to WAIT_FOR_IR state",  
                __FUNCTION__, 
                mCurrCmd, currICState, ret);
            mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR);
            mFRAlgoIC.releaseIrResource();
            FUNC_END;
            return ret;
        }
        pTargetBuf = &mFRAlgoIC.mIrBuf;
        printFRCaTaBuf(pTargetBuf);
#if FRAS_CA_SHOULD_BE_RESTORED
        oriFormat = pTargetBuf->format;
        pTargetBuf->format = FRAS_IR_COLOR_FORMAT; // IR-type
#endif
    }
    else
    {
    	// collect DEPTH buf
    	if ((ret=mFRAlgoIC.setDepthData(mCurrCmd, mPropSrcType, bufferId, buffer, stream)) != FR_ERR_OK)
    	{
    		MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: currCmd: %d, currICState: %d, setData fail: ret=%d, go back to WAIT_FOR_IR state",  
    			__FUNCTION__, 
    			mCurrCmd, currICState, ret);
    		mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR);
            mFRAlgoIC.releaseDepthResource();
    		FUNC_END;
    		return ret;
    	}
        pTargetBuf = &mFRAlgoIC.mDepthBuf;
        printFRCaTaBuf(pTargetBuf);
#if FRAS_CA_SHOULD_BE_RESTORED
        oriFormat = pTargetBuf->format;
        pTargetBuf->format = FRAS_DEPTH_COLOR_FORMAT; // depth-type
#endif
    }

    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: set data done", __FUNCTION__);
    
    TZ_RESULT retTee = TZ_RESULT_SUCCESS;
    MTEEC_PARAM param[4];
    uint32_t types;
/* !!NOTES:
    	param[0]: value.a=#of-buffers, value.b=ret-err
    	param[1]: mem.buffer=[userName], mem.size=FRCATA_USER_MAX_NAME_LEN
    	paramp[2]: mem.buffer=[Depth-buf], mem.size=FRCATA_BUF_FIXED_SIZE
    		reserved6 --> IR-sharedmem-handle for myCompF_xxx
*/
	// set state to FRAIC_WAIT_FOR_ALGO_HANDLING
	param[0].value.a = FRCATA_AS_BUF_NUM_NEEDED; // # num of buffers
	param[0].value.b = FR_ERR_ALGO_OK; // --> FRAlgoTA ret-value

	param[1].mem.buffer = (void*) mUserName; // # num of buffers
	param[1].mem.size = FRCATA_USER_MAX_NAME_LEN;
	param[2].mem.buffer = (void*) pTargetBuf;
	param[2].mem.size = FRCATA_BUF_FIXED_SIZE;

    // === Prepare IR debug buffer ===
    if (mPropSeccamCbBufDump)
    {
        if (isCopyIR)
        {
            pTargetBuf->reserved6 = mIrFileDumpGzShmHandle;
        }
        else
        {
            pTargetBuf->reserved6 = mDepthFileDumpGzShmHandle;
        }
    }
    
    // Call TA service
    types = TZ_ParamTypes3(TZPT_VALUE_INOUT, TZPT_MEM_INPUT, TZPT_MEM_INOUT);

    if (mPropCaCallTa)
    {
        retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_COPY_SECURE_BUFFER, types, param);
        if (retTee != TZ_RESULT_SUCCESS)
        {
            ret = FR_ERR_TEE_SERVICE_CALL_FAILED;
            MY_LOGE("!!err: %s: TeeServiceCall FRTA_CMD_COPY_SECURE_BUFFER: failed: retTee=%d, retSvc=%d", __FUNCTION__, retTee, ret);
            if (isCopyIR)
            {
                mFRAlgoIC.releaseIrResource();
            }
            else
            {
                mFRAlgoIC.releaseDepthResource();
            }
            return ret;
        }
    }

    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: UREE_TeeServiceCall done", __FUNCTION__);

#if FRAS_CA_SHOULD_BE_RESTORED
    pTargetBuf->format = oriFormat; // IR-type
#endif
    
    // == PostProcessing

#if 1
    // --> Save IR, Depth file
    if (mPropSeccamCbBufDump)
    {
        // save the sharedMemoroy data to file
        char fileName[512];
        static int sIrSaveFCountFile = 0;
        static int sDepthSaveFCountFile = 0;
        static int sIrCompFCountFile = 0;
        static int sDepthCompFCountFile = 0;
        static int sUnknownCountFile = 0;
        int widthInBytes = 0;
        char *pBaseAddr = mpIrFileDumpBuf;
        int dumpFileFD = -1;
        if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
        {
            // save IR
            if (isCopyIR)
            {
                ::snprintf(fileName, sizeof(fileName), 
                    "%s/mySF_%s_%.3d_%dx%d_%zu.raw",
                    FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_IR, ++sIrSaveFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
                if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                     (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                     )
                {
                    widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
                }
                else
                {
                    widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
                }
                fileDump(fileName, widthInBytes, mpIrFileDumpBuf, pTargetBuf);
            }
            else
            {
                // save Depth
                ::snprintf(fileName, sizeof(fileName), 
                    "%s/mySF_%s_%.3d_%dx%d_%zu.raw",
                    FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_DEPTH, ++sDepthSaveFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
                widthInBytes = 0;
                if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                     (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                     )
                {
                    widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
                }
                else
                {
                    widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
                }
                
                fileDump(fileName, widthInBytes, mpDepthFileDumpBuf, pTargetBuf);
            }
        }
        else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
        {
            if (isCopyIR)
            {
                // save IR
                ::snprintf(fileName, sizeof(fileName), 
                "%s/myCF_%s_%.3d_%dx%d_%zu.raw", 
                FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_IR, ++sIrCompFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
                if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                     (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                     )
                {
                    widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
                }
                else
                {
                    widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
                }
                fileDump(fileName, widthInBytes, mpIrFileDumpBuf, pTargetBuf);
            }
            else
            {
                // save Depth
                ::snprintf(fileName, sizeof(fileName), 
                "%s/myCF_%s_%.3d_%dx%d_%zu.raw", 
                FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_DEPTH, ++sDepthCompFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
                widthInBytes = 0;
                if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                     (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                     )
                {
                    widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
                }
                else
                {
                    widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
                }
                
                fileDump(fileName, widthInBytes, mpDepthFileDumpBuf, pTargetBuf);
            }
        }
        else
        {
            MY_LOGE("!!err: %s: unknown command(%d), don't save file here", __FUNCTION__, mCurrCmd);
        }
    }
#endif

    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: buf to file dump done", __FUNCTION__);

    // --> Signal FRWorkThread to do handleFeature if necessary
    if (isCopyIR)
    {
        mFRAlgoIC.releaseIrResource();
        // no matter the return value is right or wrong, go back to wait for initial IR state
        mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_DEPTH);
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: to IR buffer copy DONE, wait for depth buffer", __FUNCTION__);
    }
    else
    {
        mFRAlgoIC.releaseDepthResource();
        // depth path
        if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
        {
            mState = FRCA_STATE_SAVE_FEATURE_ONGOING;
        }
        else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
        {
            mState = FRCA_STATE_COMPARE_FEATURE_ONGOING;
        }
        else
        {
            // do nothing
        }
        
    	mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_ALGO_HANDLING);
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: to singal FR working thread to work to do algo", __FUNCTION__);
        ::sem_post(&this->mSemDataIn);
    }

    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: timestamp: release buf done", __FUNCTION__);

    FUNC_END;
    return ret;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;

#endif
}

Return<void> FRAlgoCA_AS::SecureCamClientCallback::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
// FR-custom
    ALOGD("%s +", __FUNCTION__);

    // === Error Handling ===
    int ret = FR_ERR_OK;
    if (status != Status::OK)
    {
//        ALOGW("%s: status(%d), bufferId(%" PRIu64 ") buffer(%p) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
#if 1
        ALOGW("%s:  status(%d), format(0x%x), bufferId(%lld) buffer(%p) dataSize(%llu) size(%dx%d) stride(%d)",
                __FUNCTION__,
                status,
                stream.format,
                bufferId,
                buffer.getNativeHandle(),
                stream.size,
                stream.width,
                stream.height,
                stream.stride
                );
#else // ori:
        ALOGW("%s: status(%d), bufferId(%lld) buffer(%p) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
                __FUNCTION__, 
                status, 
                bufferId,
                buffer.getNativeHandle(),
                stream.size, 
                stream.width, 
                stream.height, 
                stream.stride, 
                stream.format);
#endif

        ALOGD("%s -: err: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_STATUS_ERROR", __FUNCTION__);
        ALOGD("%s -", __FUNCTION__);
        return Void();
    }

    buffer_handle_t importedBuffer = buffer.getNativeHandle();
    if (buffer == nullptr || importedBuffer == NULL)
    {
        MY_LOGE("!!err: %s -: status: %d: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_BUF_ERROR", __FUNCTION__, status);
        ALOGD("%s -", __FUNCTION__);
        return Void();
    }

//    ALOGD("%s: status(%d), bufferId(%" PRIu64 ") buffer(%p, data[0]=%d, numFds=%d, numInts=%d) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
#if 1
    ALOGD("%s: status(%d), format(0x%x), bufferId(%lld) buffer(%p, data[0]=%d, numFds=%d, numInts=%d) dataSize(%llu) size(%dx%d) stride(%d)",
            __FUNCTION__,
            status,
            stream.format,
            bufferId,
            buffer.getNativeHandle(), importedBuffer->data[0], importedBuffer->numFds,  importedBuffer->numInts,
            stream.size,
            stream.width,
            stream.height,
            stream.stride
            );

#else // ori:
    ALOGD("%s: status(%d), bufferId(%lld) buffer(%p, data[0]=%d, numFds=%d, numInts=%d) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
            __FUNCTION__,
            status,
            bufferId,
            buffer.getNativeHandle(), importedBuffer->data[0], importedBuffer->numFds,  importedBuffer->numInts,
            stream.size,
            stream.width,
            stream.height,
            stream.stride,
            stream.format);
#endif

    // === Start Processing ===
    // area where Mutex lock is needed
    const sp<FRAlgoCA> spFRAlgoCA_AS = mwpFRAlgoCA_AS.promote();
    if (spFRAlgoCA_AS != NULL)
    {
        FRCA_STATE currState = spFRAlgoCA_AS->getFRCAState();
        if (currState == FRCA_STATE_UNINIT || currState == FRCA_STATE_STOP_FEATURE)
        {
            ALOGD("%s: State(%d): FRCA_STATE_UNINIT or FRCA_STATE_STOP_FEATURE , no need to process", __FUNCTION__, currState);
            ALOGD("%s -", __FUNCTION__);
            return Void();
        }
        if (currState == FRCA_STATE_SAVE_FEATURE_ONGOING || currState == FRCA_STATE_COMPARE_FEATURE_ONGOING)
        {
            ALOGD("%s: FRWorkThread is busy, state(%d), give up preparing FR input this time", __FUNCTION__, currState);
            ALOGD("%s -", __FUNCTION__);
            return Void();
        }

        ALOGD("%s: going to call fralgo_prepareFRInput: currState=%d", __FUNCTION__, currState);

        if (spFRAlgoCA_AS->mPropNeedPrepFRInput)
        {
            ret = spFRAlgoCA_AS->fralgo_prepareFRInput(bufferId, buffer, stream);
            ALOGD("%s: fralgo_prepareFRInput DONE: currState=%d", __FUNCTION__, currState);
            if (ret != FR_ERR_OK)
            {
                ALOGE("!!err: %s: FRAlgoCA_AS::prepareFRInput err: %d", __FUNCTION__, ret);
                FRCA_CMD currCmd = spFRAlgoCA_AS->getCurrCmd();
                spFRAlgoCA_AS->callbackFRAlgoCAClient(currCmd, ret);
            }
        }
        else
        {
            ALOGD("%s: skip fralgo_prepareFRInput", __FUNCTION__);
        }
    }

    ALOGD("%s -", __FUNCTION__);
    return Void();
}

// === thread related  ===
void *FRAlgoCA_AS::FRWorkThreadLoop(void *arg) 
{
    ALOGD("%s +", __FUNCTION__); 
    FRAlgoCA_AS *_this = reinterpret_cast<FRAlgoCA_AS *>(arg);

    FRCA_STATE_ENUM eState = _this->getFRCAState();
    int ret = FR_ERR_OK;

    while (eState != FRCA_STATE_UNINIT)
    {
        ::sem_wait(&_this->mSemDataIn);
        eState = _this->getFRCAState();
        switch(eState)
        {
        case FRCA_STATE_ALIVE:
        case FRCA_STATE_SAVE_FEATURE_ONGOING:
        case FRCA_STATE_COMPARE_FEATURE_ONGOING:
        {
            ALOGD("%s: going to call handleFeature: currFRCAState: %d", __FUNCTION__, eState);
            ret = _this->handleFeature();
            ALOGD("%s: handleFeature DONE: currFRCAState: %d, ret=%d", __FUNCTION__, _this->getFRCAState(), ret);
        }
            break;
        case FRCA_STATE_UNINIT:
            ALOGD("%s: FRCA_STATE_UNINIT", __FUNCTION__);
            break;
        default:
            MY_LOGE("!!err: %s: State Error: %d", __FUNCTION__, eState);
            break;
        }
        eState = _this->getFRCAState();
    }
    ::sem_post(&_this->mSemThreadEnd);

    ALOGD("%s -", __FUNCTION__); 
    return NULL;

}  // namespace implementation

int FRAlgoCA_AS::createFRWorkThread() 
{
    FUNC_START;
    pthread_create(&mFRWorkThread, NULL, FRWorkThreadLoop, this); 
    FUNC_END;
    return 0;
}

int FRAlgoCA_AS::fileDump(const char *pFileName, int stride, char *pFileDumpBuf, FRCaTaBuf *pTargetBuf)
{
    FUNC_START;

    char *pBaseAddr = NULL;
    int dumpFileFD = -1;

    // save IR
    const int widthInBytes = stride;
    pBaseAddr = mpIrFileDumpBuf;
    dumpFileFD = ::open(pFileName, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
    if (dumpFileFD > 0)
    {
        for (int i = 0; i < pTargetBuf->height; i++)
        {
            ::write(dumpFileFD, pBaseAddr, widthInBytes);
            pBaseAddr += pTargetBuf->stride;
        }
        ::close(dumpFileFD);
    }
    else
    {
        MY_LOGE("!!err: %s: fail to open %s", __FUNCTION__, pFileName);
    }

    FUNC_END;
    return FR_ERR_OK;
}

int FRAlgoCA_AS::handleFeature()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    /* !!NOTES:
    param[0]: value.a=#of-buffers, value.b=ret-err
    param[1]: mem.buffer=[userName], mem.size=FRCATA_USER_MAX_NAME_LEN
    */
     // set state to FRAIC_WAIT_FOR_ALGO_HANDLING

    FUNC_START;

    Mutex::Autolock lock(mLock);
#if 0
    // === Save IR, Depth file ===
    if (mPropSeccamCbBufDump)
    {
        // save the sharedMemoroy data to file
        char fileName[512];
        static int sSaveFCountFile = 0;
        static int sCompFCountFile = 0;
        static int sUnknownCountFile = 0;
        int widthInBytes = 0;
        char *pBaseAddr = mpIrFileDumpBuf;
        int dumpFileFD = -1;
        FRCaTaBuf *pTargetBuf = NULL;
        if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
        {
            // save IR
            pTargetBuf = &(mFRAlgoIC.mIrBuf);
            ::snprintf(fileName, sizeof(fileName), 
                "%s/mySF_%s_%.3d_%dx%d_%zu.raw",
                FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_IR, ++sSaveFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
            if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                 (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                 )
            {
                widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
            }
            else
            {
                widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
            }
            fileDump(fileName, widthInBytes, mpIrFileDumpBuf, pTargetBuf);

            // save Depth
            pTargetBuf = &(mFRAlgoIC.mDepthBuf);
            ::snprintf(fileName, sizeof(fileName), 
                "%s/mySF_%s_%.3d_%dx%d_%zu.raw",
                FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_DEPTH, ++sSaveFCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
            widthInBytes = 0;
            if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                 (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                 )
            {
                widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
            }
            else
            {
                widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
            }
            
            fileDump(fileName, widthInBytes, mpDepthFileDumpBuf, pTargetBuf);
        }
        else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
        {
            // save IR
            pTargetBuf = &(mFRAlgoIC.mIrBuf);
            ::snprintf(fileName, sizeof(fileName), 
            "%s/myCF_%s_%.3d_%dx%d_%zu.raw", 
            FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_IR, ++sUnknownCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
            if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                 (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                 )
            {
                widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
            }
            else
            {
                widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
            }
            fileDump(fileName, widthInBytes, mpIrFileDumpBuf, pTargetBuf);

            // save Depth
            pTargetBuf = &(mFRAlgoIC.mDepthBuf);
            ::snprintf(fileName, sizeof(fileName), 
            "%s/myCF_%s_%.3d_%dx%d_%zu.raw", 
            FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_AS_DEPTH, ++sUnknownCountFile, pTargetBuf->width, pTargetBuf->height, pTargetBuf->stride);
            widthInBytes = 0;
            if ( (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE && pTargetBuf->srcType == FRAS_SRC_TYPE_FILE) ||
                 (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE && pTargetBuf->srcType ==  FRAS_SRC_TYPE_FILE)
                 )
            {
                widthInBytes = pTargetBuf->stride; // widthInBytes = stride = width
            }
            else
            {
                widthInBytes = (pTargetBuf->width * NSCam::Utils::Format::queryPlaneBitsPerPixel(pTargetBuf->format, 0)) >> 3;
            }
            
            fileDump(fileName, widthInBytes, mpDepthFileDumpBuf, pTargetBuf);

        }
        else
        {
            MY_LOGE("!!err: %s: unknown command(%d), don't save file here", __FUNCTION__, mCurrCmd);
        }
    }
#endif

    // === StartSaveFeature or StartCompareFeature ===
    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: +: getting lock", __FUNCTION__);

    int ret = FR_ERR_ALGO_OK;
    TZ_RESULT retTee;
    MTEEC_PARAM param[4];
    uint32_t types;

    param[0].value.a = FRCATA_AS_BUF_NUM_NEEDED; // # num of buffers
    param[0].value.b = FR_ERR_ALGO_OK; // --> FRAlgoTA ret-value
    param[1].mem.buffer = (void*) mUserName; // # num of buffers
    param[1].mem.size = FRCATA_USER_MAX_NAME_LEN;

    // Call TA service
    types = TZ_ParamTypes2(TZPT_VALUE_INOUT, TZPT_MEM_INPUT);
    FRCA_CMD currCmd = this->getCurrCmd();

    if (mPropCaCallTa)
    {
        // Call TA service
        if (currCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
        {
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: to call TA::FRTA_CMD_START_SAVE_FEATURE", __FUNCTION__);
            retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_SAVE_FEATURE, types, param);
            ret = param[0].value.b;
        }
        else if (currCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
        {
            MY_LOGD_IF(mPropCaLogLevel >= 2, "%s: to call TA::FRTA_CMD_START_COMPARE_FEATURE", __FUNCTION__);
            retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_COMPARE_FEATURE, types, param);
            ret = param[0].value.b;
        }
        else
        {
            // do nothing
            ALOGW("%s: wrong cmd: %d", __FUNCTION__, mCurrCmd);
            ret = FR_ERR_FRALGO_WRONG_CMD;
        }
    }

    MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: ret=%d, currCmd=%d", __FUNCTION__, ret, currCmd);

    FRCA_STATE_ENUM eState = this->getFRCAState();
    if (eState != FRCA_STATE_UNINIT)
    {
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: callback to FRHandler user: ret=%d, currCmd=%d, mState=%d", __FUNCTION__, ret, currCmd, mState);
        this->callbackFRAlgoCAClient(currCmd, ret);
        // !!NOTES: let onBufferAvail THREAD move on collecting buffers
        // 1. set state to FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR
        // 2. FRAlgoCA.mState to FRCA_STATE_SAVE_FEATURE to let OnBufferAvailable move on
        this->mFRAlgoIC.setState(FRAlgoASInputCollector::FRAIC_WAIT_FOR_IR);
        if (currCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
        {
            mState = FRCA_STATE_SAVE_FEATURE;
        }
        else if (currCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
        {
            mState = FRCA_STATE_COMPARE_FEATURE;
        }
        else
        {
            // do nothing
            ALOGW("%s: wrong cmd: %d", __FUNCTION__, mCurrCmd);
            ret = FR_ERR_FRALGO_WRONG_CMD;
        }
    }
    else
    {
        MY_LOGD_IF(mPropCaLogLevel >= 1, "%s: state(%d)", __FUNCTION__, eState);
    }

    FUNC_END;

    return ret;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

// === private API ===


}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware
}  // namespace hardware

