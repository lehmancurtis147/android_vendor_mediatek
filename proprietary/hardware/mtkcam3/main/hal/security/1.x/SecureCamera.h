/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_SECURITY_V1_0_SECURECAMERA_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_SECURITY_V1_0_SECURECAMERA_H

#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCamera.h>
#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCameraClientCallback.h>

#include <mtkcam3/main/security/2.0/ISecureCamera.h>
#include <mtkcam3/main/hal/ICameraDeviceManager.h>

#include <unordered_map>
#include <thread>

// ------------------------------------------------------------------------

class CpuCtrl;

// ------------------------------------------------------------------------

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace security {
namespace V1_0 {
namespace implementation {

using ::android::hardware::Return;
using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_handle;
using ::android::hardware::camera::common::V1_0::Status;

using ::vendor::mediatek::hardware::camera::security::V1_0::ISecureCamera;

// ------------------------------------------------------------------------

class CPUControl;

// ------------------------------------------------------------------------

static void resetCPUBoostTimeout(CPUControl& cpuControl);

class CPUControl
{
public:
    CPUControl() : mRequestExit(false) {};
    virtual ~CPUControl();
    void enableCPUBoost();
    inline void disableCPUBoost();

private:
    friend void resetCPUBoostTimeout(CPUControl& cpuControl);

    // A power HAL service wrapper that can enforce CPU boost
    std::pair<std::shared_ptr<CpuCtrl>, std::mutex> mCtrl;
    std::thread mWatchdog;
    mutable std::mutex mResetCPUTimeoutLock;
    std::condition_variable mCondResetCPUTimeout;
    std::atomic<bool> mRequestExit;

    void releaseWatchdog();
}; // class CPUControl

// ------------------------------------------------------------------------

struct SecureCamera :
       public ISecureCamera,
       public android::hardware::hidl_death_recipient
{
    SecureCamera();
    virtual ~SecureCamera();

    // Methods from ::vendor::mediatek::hardware::camera::security::V1_0::ISecureCamera follow.
    using getCameraIdList_cb = std::function<void(Status status,
            const hidl_vec<hidl_string>& cameraDeviceIDs)>;
    virtual Return<void> getCameraIdList(getCameraIdList_cb _hidl_cb) override;
    using getSecureStatus_cb = std::function<void(Status status,
            const hidl_vec<hidl_string>& secureDeviceInUsedIDs)>;
    virtual Return<void> getSecureStatus(getSecureStatus_cb _hidl_cb) override;
    virtual Return<Status> open(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> close(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> initialize(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> uninitialize(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> registerCallback(
            const ::android::sp<ISecureCameraClientCallback>& clientCallback) override;
    virtual Return<Status> startCapture(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> stopCapture(const hidl_vec<hidl_string>& ids) override;
    // Methods from ::android::hardware::hidl_death_recipient follow.
    virtual void serviceDied(
            uint64_t cookie, const ::android::wp<::android::hidl::base::V1_0::IBase>& who) override;

private:
    // device ids are decided and sorted in constructor.
    // For simplicity and efficiency, device ids can be regarded as READ ONLY
    // after construction.
    //
    // format: support sensor position/sensor id
    // sensor facing: 0 is for the camera device faces the same direction as
    //                the device's screen.
    //                1 is for the camera device faces the opposite direction as
    //                the device's screen.
    //     sensor id: the sensor found at the same facing,
    //                enumerated as 1, 2, 3, and so on.
    std::vector<hidl_string> mSecureCameraDeviceNames;

    // a pair of (sensor position, the rest parts of tokenized mSecureCameraDeviceNames)
    std::unordered_map<int, std::vector<std::string>> mSecureCameraDeviceNameTokens;

    mutable std::mutex mLock;
    void* mSecureCameraHandle;
    std::unique_ptr<NSCam::security::V2_0::ISecureCamera> mSecureCamera;
    void* mCameraProviderHandle;
    NSCam::ICameraDeviceManager* mDeviceManager;

    bool mIsSecureModeOnly;

    std::vector<std::string> mCameraDeviceNames;

    std::pair<::android::sp<ISecureCameraClientCallback>, std::mutex> mClientCallback;

    // +---------------+----------------+------------+
    // | Current State | Action         | Next State |
    // +---------------+----------------+------------+
    // | NONE          | open()         | OPEN       |
    // +---------------+----------------+------------+
    // | OPEN          | close()        | NONE       |
    // |               | initialize()   | INIT       |
    // +---------------+----------------+------------+
    // | INIT          | uninitialize() | OPEN       |
    // |               | startCapture() | CAPTURE    |
    // +---------------+----------------+------------+
    // | CAPTURE       | stopCapture()  | INIT       |
    // +---------------+----------------+------------+
    enum class HalState
    {
        NONE         = 0,
        OPEN         = 1,
        INIT         = 2,
        CAPTURE      = 3,
        ERROR        = 10,
    };

    std::pair<HalState, std::mutex> mState;

    // key_type is ION file descriptor contained in ImageBuffer.
    // value_type is a pointer points to native_handle,
    // which is created by native_handle_create() and
    // must be deleted by native_handle_delete().
    using BufferIdMap = std::unordered_map<uint64_t, buffer_handle_t>;
    std::pair<BufferIdMap, std::mutex> mBufferIdMap;

    std::unique_ptr<CPUControl> mCPUControl;

    friend NSCam::security::V2_0::types::Result bufferCallback(
            const NSCam::security::V2_0::types::Buffer& buffer);

    // openLocked() handles camera resource confliction to assure that
    // a camera device can be either in normal or secure mode but not both.
    Return<Status> openLocked(const hidl_vec<hidl_string>& ids);

    Return<void> closeLocked(const hidl_vec<hidl_string>& ids);

    // closeDeviceLocked() closes camera device
    Return<::android::status_t> closeDeviceLocked(const std::vector<std::string>& deviceList);
}; // struct SecureCamera

// ------------------------------------------------------------------------

struct DummySecureCamera :
       public ISecureCamera,
       public android::hardware::hidl_death_recipient
{
    virtual ~DummySecureCamera() = default;

    // Methods from ::vendor::mediatek::hardware::camera::security::V1_0::ISecureCamera follow.
    using getCameraIdList_cb = std::function<void(Status status,
            const hidl_vec<hidl_string>& cameraDeviceIDs)>;
    virtual Return<void> getCameraIdList(getCameraIdList_cb _hidl_cb) override;
    using getSecureStatus_cb = std::function<void(Status status,
            const hidl_vec<hidl_string>& secureDeviceInUsedIDs)>;
    virtual Return<void> getSecureStatus(getSecureStatus_cb _hidl_cb) override;
    virtual Return<Status> open(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> close(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> initialize(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> uninitialize(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> registerCallback(
            const ::android::sp<ISecureCameraClientCallback>& clientCallback) override;
    virtual Return<Status> startCapture(const hidl_vec<hidl_string>& ids) override;
    virtual Return<Status> stopCapture(const hidl_vec<hidl_string>& ids) override;
    // Methods from ::android::hardware::hidl_death_recipient follow.
    virtual void serviceDied(
            uint64_t cookie, const ::android::wp<::android::hidl::base::V1_0::IBase>& who) override;
}; // struct DummySecureCamera

// ------------------------------------------------------------------------

extern "C" ISecureCamera* HIDL_FETCH_ISecureCamera(const char* name);

} // namespace implementation
} // namespace V1_0
} // namespace security
} // namespace camera
} // namespace hardware
} // namespace mediatek
} // namespace vendor

#endif // VENDOR_MEDIATEK_HARDWARE_CAMERA_SECURITY_V1_0_SECURECAMERA_H
