/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "vendor.mediatek.hardware.camera.security@1.0-impl"

#include <cutils/compiler.h>

#include <mtkcam3/main/security/utils/IonHelper.h>
#include <mtkcam/utils/sys/CpuCtrl.h>
#include <mtkcam/utils/std/Log.h>

#include "SecureCamera.h"

#include "HandleImporter.h"

#include <dlfcn.h>

#include <vector>
#include <string>
#include <algorithm>

#ifdef __LP64__
#define MODULE_PATH "/vendor/lib64/libmtkcam_security.so"
#define CAM_PATH "/vendor/lib64/hw/android.hardware.camera.provider@2.4-impl-mediatek.so"
#else
#define MODULE_PATH "/vendor/lib/libmtkcam_security.so"
#define CAM_PATH "/vendor/lib/hw/android.hardware.camera.provider@2.4-impl-mediatek.so"
#endif

// NOTE: only for debugging purpose and cannot be built into release load
//#define DEBUG_BUFFER_DUMP

//#define ENABLE_CPU_BOOST

using namespace ::vendor::mediatek::hardware::camera::security::V1_0::implementation;

using ::android::hardware::camera::common::V1_0::helper::HandleImporter;

// ---------------------------------------------------------------------------

#ifdef MTK_SECAM_DISABLE_NORMAL_CAM
// NOTE: ISP driver can't run normal and secure mode concurrently
static const bool kNeedLockAllDevice = true;
#else
static const bool kNeedLockAllDevice = false;
#endif

// NOTE: If facing CPU thermal throttling or low-power issue, please enforce
//       a timeout condition kPerformanceBoostTimeout (> 0) here.
static constexpr int32_t kPerformanceBoostTimeoutInSecond = 3;

static constexpr std::chrono::milliseconds kPerformanceBoostTimeout =
    std::chrono::milliseconds(kPerformanceBoostTimeoutInSecond * 1000 / 3);

// ---------------------------------------------------------------------------

static std::vector<std::string> split(const char *str, char delimiter = ' ')
{
    std::vector<std::string> result;

    do
    {
        const char *begin = str;

        while (*str != delimiter && *str)
            str++;

        result.push_back(std::string(begin, str));
    } while (0 != *str++);

    return result;
}

static std::vector<std::string> deviceIDSplitter(const char *str, char delimiter = '/')
{
    return split(str, delimiter);
}

static auto mapToHidlCameraStatus(const ::android::status_t& status)
{
    switch (status)
    {
        case 0/*OK*/:                       return Status::OK;
        case -EINVAL/*BAD_VALUE*/:          return Status::ILLEGAL_ARGUMENT;
        case -EBUSY:                        return Status::CAMERA_IN_USE;
        case -EUSERS:                       return Status::MAX_CAMERAS_IN_USE;
        case -ENOSYS/*INVALID_OPERATION*/:  return Status::METHOD_NOT_SUPPORTED;
        case -EOPNOTSUPP:                   return Status::OPERATION_NOT_SUPPORTED;
        case -EPIPE/*DEAD_OBJECT*/:         return Status::CAMERA_DISCONNECTED;
        case -ENODEV/*NO_INIT*/:            return Status::INTERNAL_ERROR;
        default:                            return Status::INTERNAL_ERROR;
    }
}

// return true if the intersection of src and dst is not empty
static bool verifySecureCameraDeviceNames(
        const std::vector<hidl_string>& src, const std::vector<hidl_string>& sortedDst)
{
    CAM_LOGV("%s +", __FUNCTION__);

    std::vector<hidl_string> sortedSrc = src;
    std::sort(sortedSrc.begin(), sortedSrc.end());

    std::vector<hidl_string> intersection;
    std::set_intersection(sortedSrc.begin(), sortedSrc.end(),
                          sortedDst.begin(), sortedDst.end(),
                          std::back_inserter(intersection));

    CAM_LOGV("%s -", __FUNCTION__);

    return !intersection.empty();
}

// ---------------------------------------------------------------------------

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace security {
namespace V1_0 {
namespace implementation {

NSCam::security::V2_0::types::Result bufferCallback(
        const NSCam::security::V2_0::types::Buffer& buffer)
{
    auto* camera(reinterpret_cast<SecureCamera*>(buffer.priv));
    if (CC_UNLIKELY(!camera))
    {
        CAM_LOGE("invalid private information");
        return ::android::NO_INIT;
    }

    std::lock_guard<std::mutex> _l(camera->mClientCallback.second);

    if (CC_UNLIKELY(!camera->mClientCallback.first.get()))
    {
        CAM_LOGE("invalid client callback");
        return ::android::NO_INIT;
    }

    {
        uint64_t bufferId = buffer.addr.secureHandle.fd;

        native_handle *nativeHandle =
            [camera, bufferId, &buffer]
            {
                auto& bufferIdMap(camera->mBufferIdMap.first);
                std::lock_guard<std::mutex> _l(camera->mBufferIdMap.second);
                auto search = bufferIdMap.find(bufferId);
                if (search == bufferIdMap.end())
                {
                    // establish buffer mapping rules for a newly created handle
                    const int fd = buffer.addr.secureHandle.fd;

                    native_handle *handle = native_handle_create(1,0);
                    handle->data[0] = fd;
                    bufferIdMap[bufferId] = handle;

                    CAM_LOGD("%s: establish buffer mapping bufferId(%" PRIu64 ") FD(%d)",
                            __FUNCTION__, bufferId, fd);

                    return handle;
                }

                return const_cast<native_handle *>(search->second);
            }();

#ifdef DEBUG_BUFFER_DUMP
        // dump buffer here
        {
            NSCam::security::utils::IonHelper helper;
            const size_t dataSize(buffer.attribute.length);
            const int sharedFD(buffer.addr.secureHandle.fd);
            void *addr = helper.mmap(
                    NULL, dataSize, PROT_READ | PROT_WRITE, MAP_SHARED, sharedFD);

            CAM_LOGD("%s: bufferAddr(0x%" PRIxPTR ") dataSize(%zu) sharedFD(%d)",
                    __FUNCTION__, reinterpret_cast<uintptr_t>(addr), dataSize, sharedFD);

            helper.munmap(addr, dataSize);
        }
#endif

        CAM_LOGD("%s: requestNo(%d) bufferId(%" PRIu64 ") FD(%d) stride(%zu) size(%dx%d)" \
                " dataSize(%zu) format(0x%x)",
                __FUNCTION__,
                buffer.attribute.identifier.second,
                bufferId,
                buffer.addr.secureHandle.fd,
                buffer.attribute.stride,
                buffer.attribute.size.w,
                buffer.attribute.size.h,
                buffer.attribute.length,
                buffer.attribute.format);

        Stream stream
        {
            .size   = buffer.attribute.length,
            .stride = buffer.attribute.stride,
            .width  = buffer.attribute.size.w,
            .height = buffer.attribute.size.h,
            .format = buffer.attribute.format
        };

        // NOTE:
        // Due to the buffer callback thread of the HAL server process
        // is not the same as that of calling thread triggered from the HAL client process,
        // there exists a race condition of HIDL buffer lifecycle between
        // the HAL server process (which deletes the HIDL buffer) and
        // the HAL client process (which imports the HIDL buffer).
        //
        // To cope with such circumstance, we *IMPORT*/*FREE* the HIDL buffer before/after
        // sending to/returning from the HAL client process.
        // In this way, we can assure the HIDL buffer is *VALID" during the callback process.
        hidl_handle hidlHandle(nativeHandle);
        buffer_handle_t bufferHandle(&*hidlHandle);
        HandleImporter handleImporter;
        handleImporter.importBuffer(bufferHandle);
        CAM_LOGD("%s: FD(%" PRIu64 ") handle_FD(%d)",
                __FUNCTION__, bufferId, hidlHandle->data[0]);

        Return<void> ret =
            camera->mClientCallback.first->onBufferAvailable(
                    Status::OK, bufferId, hidlHandle, stream);
        if (!ret.isOk()) {
            CAM_LOGE("%s: Transaction error onBufferAvailable: %s",
                    __FUNCTION__, ret.description().c_str());
        }
        handleImporter.freeBuffer(bufferHandle);
    }

    return ::android::OK;
}

static void resetCPUBoostTimeout(CPUControl& cpuControl)
{
    CAM_LOGD("resetCPUBoostTimeout +");

    {
        std::unique_lock<std::mutex> _l(cpuControl.mCtrl.second);
        if (CC_UNLIKELY(!cpuControl.mCtrl.first))
        {
            CAM_LOGW("invalid CPUControl, do nothing...");
            return;
        }
    }

    std::cv_status status = std::cv_status::no_timeout;
    do {
        if (CC_UNLIKELY(cpuControl.mRequestExit.load(std::memory_order_relaxed)))
        {
            CAM_LOGD("watchdog exit now...");
            break;
        }

        {
            std::unique_lock<std::mutex> _l(cpuControl.mResetCPUTimeoutLock);
            status = cpuControl.mCondResetCPUTimeout.wait_for(
                    _l, kPerformanceBoostTimeout);
        }

        if (status == std::cv_status::timeout)
        {
            std::unique_lock<std::mutex> _l(cpuControl.mCtrl.second);
            cpuControl.mCtrl.first->resetTimeout(kPerformanceBoostTimeoutInSecond);
        }
    } while (true);

    CAM_LOGD("resetCPUBoostTimeout -");
}

// Methods from ::android::hidl::base::V1_0::IBase follow.
ISecureCamera* HIDL_FETCH_ISecureCamera(const char* /* name */)
{
    CAM_LOGI("ISecureCamera into %s", __FUNCTION__);
#ifdef MTK_CAM_SECURITY_SUPPORT
    return new SecureCamera();
#else
    return new DummySecureCamera();
#endif
}

} // namespace implementation
} // namespace V1_0
} // namespace security
} // namespace camera
} // namespace hardware
} // namespace mediatek
} // namespace vendor

// ---------------------------------------------------------------------------

SecureCamera::SecureCamera()
    : mSecureCameraHandle(nullptr),
      mCameraProviderHandle(nullptr),
      mDeviceManager(nullptr)
{
    // TODO: fixed values for front1 sensor, we should get this capability by platform
    mSecureCameraDeviceNames.push_back("0/1");

    // sort the elements in ascending order
    std::sort(mSecureCameraDeviceNames.begin(), mSecureCameraDeviceNames.end());

    mState.first = HalState::NONE;
    mIsSecureModeOnly = true;

    // translate device id list into map
    for (auto&& deviceID : mSecureCameraDeviceNames)
    {
        // split into tokens
        const auto& tokens(deviceIDSplitter(deviceID.c_str()));

        const int key(std::stoi(*tokens.begin()));

        // the first token is the key and the rest parts become values
        std::vector<std::string> subTokens(++tokens.begin(), tokens.end());

        if (mSecureCameraDeviceNameTokens.emplace(key, std::move(subTokens)).second == false)
            CAM_LOGA("cannot emplace token: key(%d)", key);
    }
}

SecureCamera::~SecureCamera()
{
    mSecureCameraDeviceNames.clear();
    mClientCallback.first.clear();
    mCameraDeviceNames.clear();

    if (mCameraProviderHandle)
        dlclose(mCameraProviderHandle);
}

Return<void> SecureCamera::getCameraIdList(getCameraIdList_cb _hidl_cb)
{
    CAM_LOGD("%s", __FUNCTION__);
    for (auto&& str : mSecureCameraDeviceNames)
        CAM_LOGD("%s", str.c_str());

    _hidl_cb(Status::OK, mSecureCameraDeviceNames);

    return ::android::hardware::Void();
}

Return<void> SecureCamera::getSecureStatus(getSecureStatus_cb _hidl_cb)
{
    std::vector<hidl_string> ids;

    CAM_LOGD("%s", __FUNCTION__);
    for (auto&& str : mSecureCameraDeviceNames)
        CAM_LOGD("%s", str.c_str());

    std::lock_guard<std::mutex> _l(mLock);

    if (mSecureCameraHandle)
      _hidl_cb(Status::CAMERA_IN_USE, mSecureCameraDeviceNames);
    else
      _hidl_cb(Status::OK, ids);

    return ::android::hardware::Void();
}

Return<::android::status_t> SecureCamera::closeDeviceLocked(
        const std::vector<std::string>& deviceList)
{
    ::android::status_t status = android::OK;

    if (mIsSecureModeOnly)
    {
        status = mDeviceManager->setSecureMode((uint32_t)NSCam::ICameraDeviceManager::ESecureModeStatus::SECURE_NONE);
        if (CC_UNLIKELY(android::OK != status))
        {
            CAM_LOGE("DeviceManager setSecureMode disable [%d, %s] failed", -status, ::strerror(-status));
            //return mapToHidlCameraStatus(status);
        }
        else
        {
            CAM_LOGD("DeviceManager setSecureMode disable success!!");
        }
    }
    else
    {
        for (auto&& deviceName : deviceList)
        {
            status = mDeviceManager->startCloseDevice(deviceName);
            if (android::OK != status)
                CAM_LOGE("startCloseDevice [%d %s]", -status, ::strerror(-status));
            status = mDeviceManager->finishCloseDevice(deviceName);
            if (android::OK != status)
                CAM_LOGE("finishCloseDevice [%d %s]", -status, ::strerror(-status));
            CAM_LOGW("finishCloseDevice %s success!", deviceName.c_str());
        }
    }
    return status;
}

Return<Status> SecureCamera::openLocked(const hidl_vec<hidl_string>& ids)
{
    Status ret = Status::OK;

    for (auto&& id : ids)
        CAM_LOGD("%s id(%s)", __FUNCTION__, id.c_str());

    // the types of the class factories
    typedef NSCam::security::V2_0::ISecureCamera* create_t();

    // load SecureCamera library
    mSecureCameraHandle = dlopen(MODULE_PATH, RTLD_NOW);
    if (!mSecureCameraHandle)
    {
        CAM_LOGE("cannot open camera");
        return Status::INTERNAL_ERROR;
    }

    // reset error
    dlerror();

    create_t* getSecureCamera =
        (create_t*)dlsym(mSecureCameraHandle, "getSecureCamera");
    const char* dlsym_error = dlerror();
    if (dlsym_error)
    {
        CAM_LOGE("cannot load symbol create instance %s", dlsym_error);
        dlclose(mSecureCameraHandle);
        mSecureCameraHandle = nullptr;
        return Status::INTERNAL_ERROR;
    }
    CAM_LOGD("loaded symbol create instance");

    mSecureCamera.reset(getSecureCamera());
    if (!mSecureCamera.get())
    {
        CAM_LOGE("get secure camera failed");
        dlclose(mSecureCameraHandle);
        mSecureCameraHandle = nullptr;
        return Status::INTERNAL_ERROR;
    }

    // FIXME: decide path dynamically
    mSecureCamera->open(NSCam::security::V2_0::types::Path::BAYER);

    // FIXME: remove hardcode
    if (::android::OK !=
            mSecureCamera->sendCommand(NSCam::security::V2_0::Command::SET_SRC_DEV,
                (intptr_t)0x02))
    {
        CAM_LOGE("set camera id failed");
        dlclose(mSecureCameraHandle);
        mSecureCameraHandle = nullptr;
        return Status::INTERNAL_ERROR;
    }

    return ret;
}

Return<Status> SecureCamera::open(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    for (auto&& device : mSecureCameraDeviceNameTokens)
        CAM_LOGD("sensor position(%d) sensor id(%s)",
                device.first, (device.second)[0].c_str());

    {
        std::lock_guard<std::mutex> _m(mClientCallback.second);
        if (CC_UNLIKELY(!mClientCallback.first.get()))
        {
            CAM_LOGE("invalid client callback");
            return Status::ILLEGAL_ARGUMENT;
        }
    }

    if (mSecureCameraHandle)
    {
        CAM_LOGE("SecureCamera is already opened!");
        return Status::CAMERA_IN_USE;
    }

    if (!mCameraProviderHandle)
    {
        mCameraProviderHandle = ::dlopen(CAM_PATH, RTLD_NOW);
        if (!mCameraProviderHandle)
        {
            char const *err_str = ::dlerror();
            CAM_LOGE("dlopen: %s error=%s", CAM_PATH, (err_str ? err_str : "unknown"));
        }
    }

    {
        const std::string symbol("getCameraDeviceManager");
        typedef NSCam::ICameraDeviceManager * (*pfnEntry_T)();
        pfnEntry_T pfnEntry = (pfnEntry_T)::dlsym(mCameraProviderHandle, symbol.c_str());
        if (!pfnEntry)
        {
            char const *err_str = ::dlerror();
            CAM_LOGE("dlsym: %s error=%s getModuleLib:%p",
                    symbol.c_str(),
                    (err_str ? err_str : "unknown"), mCameraProviderHandle);
            return Status::INTERNAL_ERROR;
        }
        mDeviceManager = pfnEntry();
    }

    if (!mDeviceManager)
    {
        CAM_LOGE("ICameraDeviceManager:getCameraDeviceManager return nullptr");
        return Status::INTERNAL_ERROR;
    }
    else
    {
        CAM_LOGD("ICameraDeviceManager:getCameraDeviceManager success!!");
    }

#ifdef ENABLE_CPU_BOOST
    // Enable CPU performance mode
    {
        mCPUControl.reset(new CPUControl());
        mCPUControl->enableCPUBoost();
    }
#endif

    auto& cameraDeviceNames(mCameraDeviceNames);

    if (mIsSecureModeOnly)
    {
        auto status = mDeviceManager->setSecureMode((uint32_t)NSCam::ICameraDeviceManager::ESecureModeStatus::SECURE_ONLY);
        if (CC_UNLIKELY(android::OK != status))
        {
            CAM_LOGE("DeviceManager setSecureMode enable [%d, %s] failed", -status, ::strerror(-status));
            return mapToHidlCameraStatus(status);
        }
        CAM_LOGD("DeviceManager setSecureMode enable success!!");
    }
    else
    {
        {
            std::vector<std::string> deviceNameList;
            auto res = mDeviceManager->getDeviceNameList(deviceNameList);

            cameraDeviceNames.clear();

            if (kNeedLockAllDevice)
            {
                for (auto&& deviceName : deviceNameList)
                {
                    CAM_LOGW("camera device list size(%zu), device(%s) added",
                            cameraDeviceNames.size(), deviceName.c_str());
                    cameraDeviceNames.push_back(std::move(deviceName));
                }
            }
            else
            {
                cameraDeviceNames.push_back(deviceNameList[1]);
                CAM_LOGW("camera device list size(%zu), device(%s) added",
                        cameraDeviceNames.size(), cameraDeviceNames[0].c_str());
            }
        }

        for (size_t i = 0; i < cameraDeviceNames.size(); i++)
        {
            auto status = mDeviceManager->startOpenDevice(cameraDeviceNames[i]);
            if (CC_UNLIKELY(android::OK != status))
            {
                ::android::status_t ret = status;
                CAM_LOGE("DeviceManager startOpenDevice %d failed", i);

                // if open device failed, then close devices which are already opened
                for (size_t j = 0; j < i; j++)
                {
                    status = mDeviceManager->startCloseDevice(cameraDeviceNames[j]);
                    if (android::OK != status)
                        CAM_LOGE("startCloseDevice [%d %s]", -status, ::strerror(-status));
                    status = mDeviceManager->finishCloseDevice(cameraDeviceNames[j]);
                    if (android::OK != status)
                        CAM_LOGE("finishCloseDevice [%d %s]", -status, ::strerror(-status));
                    CAM_LOGW("camera device list size(%zu)," \
                            " finishCloseDevice %s success!",
                            cameraDeviceNames.size(),cameraDeviceNames[j].c_str());
                }
                return mapToHidlCameraStatus(ret);
            }
            mDeviceManager->finishOpenDevice(cameraDeviceNames[i], false/*cancel*/);
            mDeviceManager->updatePowerOnDone();
            CAM_LOGW("cameraDeviceNames[%d] finishOpenDevice %s success!",
                    i, cameraDeviceNames[i].c_str());
        }
    }

    Status ret = openLocked(ids);
    if (Status::OK != ret)
    {
        auto status = closeDeviceLocked(cameraDeviceNames);
        if (android::OK != status)
            CAM_LOGE("closeDeviceLocked(%d): %s", -status, ::strerror(-status));
    }

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::OPEN;
    }

    return ret;
}

Return<void> SecureCamera::closeLocked(const hidl_vec<hidl_string>& ids)
{
    for (auto&& id : ids)
        CAM_LOGD("%s id(%s)", __FUNCTION__, id.c_str());

    ::android::status_t status = android::OK;
    {
        mSecureCamera->close();
        mSecureCamera = nullptr;

        if (mSecureCameraHandle)
            dlclose(mSecureCameraHandle);
        mSecureCameraHandle = nullptr;

        status = closeDeviceLocked(mCameraDeviceNames);
        if (android::OK != status)
            CAM_LOGE("closeDeviceLocked(%d): %s", -status, ::strerror(-status));
    }

    // NOTE: file descriptor contained in nativeHandle is the same as
    //       that in ImageBuffer instance, so we cannot close nativeHandle
    //       by native_handle_close().
    {
        auto& bufferIdMap(mBufferIdMap.first);
        std::lock_guard<std::mutex> _l(mBufferIdMap.second);
        for (auto it = bufferIdMap.begin(); it != bufferIdMap.end(); )
        {
            CAM_LOGD("%s: delete buffer mapping bufferId(%" PRIu64 ") FD(%d)",
                    __FUNCTION__, it->first, it->second->data[0]);

            native_handle_delete(const_cast<native_handle *>(it->second));
            it = bufferIdMap.erase(it);
        }
    }

    return ::android::hardware::Void();
}

Return<Status> SecureCamera::close(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return Status::INTERNAL_ERROR;
    }

    closeLocked(ids);

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::NONE;
    }

    {
        std::lock_guard<std::mutex> _m(mClientCallback.second);
        mClientCallback.first->unlinkToDeath(this);
        mClientCallback.first.clear();
    }

    // Disable CPU performance mode
    if (mCPUControl.get())
        mCPUControl->disableCPUBoost();

    return Status::OK;
}

Return<Status> SecureCamera::initialize(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return Status::INTERNAL_ERROR;
    }

    mSecureCamera->init();

    // register callback (HIDL server to implementation)
    using SecureCameraCallback = NSCam::security::V2_0::SecureCameraCallback;
     SecureCameraCallback callback =
         SecureCameraCallback::createCallback<NSCam::security::V2_0::PFN_CALLBACK_ADDR>(
                 NSCam::security::V2_0::CALLBACK_ADDR, bufferCallback);

    mSecureCamera->sendCommand(
            NSCam::security::V2_0::Command::REGISTER_CB_FUNC,
            reinterpret_cast<intptr_t>(&callback), reinterpret_cast<intptr_t>(this));

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::INIT;
    }

    return Status::OK;
}

Return<Status> SecureCamera::uninitialize(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return Status::INTERNAL_ERROR;
    }

    mSecureCamera->unInit();

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::OPEN;
    }

    return Status::OK;
}

Return<Status> SecureCamera::registerCallback(
        const ::android::sp<ISecureCameraClientCallback>& clientCallback)
{
    CAM_LOGD("%s callback(0x%" PRIxPTR ")",
            __FUNCTION__, reinterpret_cast<intptr_t>(clientCallback.get()));

    std::lock_guard<std::mutex> _l(mLock);

    {
        std::lock_guard<std::mutex> _m(mClientCallback.second);
        mClientCallback.first = clientCallback;

        ::android::hardware::Return<bool> ret = clientCallback->linkToDeath(
                                                        this, reinterpret_cast<uint64_t>(this));
        if (!ret.isOk())
        {
            CAM_LOGE("Transaction error in linking to ISecureCameraClientCallback death: %s",
                                                                     ret.description().c_str());
        }
        else if (!ret)
        {
            CAM_LOGE("Unable to link to ISecureCameraClientCallback death notifications");
        }
    }

    return Status::OK;
}

Return<Status> SecureCamera::startCapture(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return Status::INTERNAL_ERROR;
    }

    if (::android::OK !=
            mSecureCamera->sendCommand(NSCam::security::V2_0::Command::STREAMING_ON))
    {
        CAM_LOGE("startCapture failed");
        return Status::INTERNAL_ERROR;
    }

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::CAPTURE;
    }

    return Status::OK;
}

Return<Status> SecureCamera::stopCapture(const hidl_vec<hidl_string>& ids)
{
    if (CC_UNLIKELY(!verifySecureCameraDeviceNames(ids, mSecureCameraDeviceNames)))
    {
        CAM_LOGE("invalid device ID");
        for (auto&& id : ids)
            CAM_LOGE("%s id(%s)", __FUNCTION__, id.c_str());
        return Status::INTERNAL_ERROR;
    }

    std::lock_guard<std::mutex> _l(mLock);

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return Status::INTERNAL_ERROR;
    }

    if (::android::OK !=
            mSecureCamera->sendCommand(NSCam::security::V2_0::Command::STREAMING_OFF))
    {
        CAM_LOGE("stopCapture failed");
        return Status::INTERNAL_ERROR;
    }

    {
        std::lock_guard<std::mutex> _l(mState.second);
        mState.first = HalState::INIT;
    }

    return Status::OK;
}

void SecureCamera::serviceDied(
    uint64_t cookie,
    const ::android::wp<::android::hidl::base::V1_0::IBase>& who __unused)
{
    if (cookie != reinterpret_cast<uint64_t>(this)) {
        CAM_LOGE("Unexpected SecureCamera serviceDied cookie 0x%" PRIx64 ", expected %p",
                cookie, this);
    }
    CAM_LOGE("%s: SecureCamera serviceDied!!",__FUNCTION__);

    std::lock_guard<std::mutex> _l(mLock);

    {
        std::lock_guard<std::mutex> _m(mClientCallback.second);
        mClientCallback.first.clear();
    }

    if (CC_UNLIKELY(!mSecureCamera.get()))
    {
        CAM_LOGE("invalid secure camera");
        return;
    }

    {
        std::lock_guard<std::mutex> _l(mState.second);

        auto& state(mState.first);

        CAM_LOGD("%s: SecureCamera::serviceDied +, mState %d!!",__FUNCTION__, state);

        if (state == HalState::CAPTURE)
        {
            if (::android::OK !=
                    mSecureCamera->sendCommand(NSCam::security::V2_0::Command::STREAMING_OFF))
                CAM_LOGE("stopCapture failed");
            state = HalState::INIT;
        }

        if (state == HalState::INIT)
        {
            mSecureCamera->unInit();
            state = HalState::OPEN;
        }

        if (state == HalState::OPEN)
        {
            closeLocked(mSecureCameraDeviceNames);
            state = HalState::NONE;
        }
        CAM_LOGD("%s: SecureCamera::serviceDied -, mState %d!!",__FUNCTION__, state);
    }

    // Disable CPU performance mode
    if (mCPUControl.get())
        mCPUControl->disableCPUBoost();
}

// ---------------------------------------------------------------------------

Return<void> DummySecureCamera::getCameraIdList(getCameraIdList_cb _hidl_cb)
{
    std::vector<hidl_string> dummySecureCameraDeviceNames;
    _hidl_cb(Status::OPERATION_NOT_SUPPORTED, dummySecureCameraDeviceNames);
    return ::android::hardware::Void();
}

Return<void> DummySecureCamera::getSecureStatus(getSecureStatus_cb _hidl_cb)
{
    std::vector<hidl_string> dummyIds;
    _hidl_cb(Status::OPERATION_NOT_SUPPORTED, dummyIds);
    return ::android::hardware::Void();
}

Return<Status> DummySecureCamera::open(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::close(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::initialize(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::uninitialize(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::registerCallback(
        const ::android::sp<ISecureCameraClientCallback>& clientCallback __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::startCapture(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

Return<Status> DummySecureCamera::stopCapture(const hidl_vec<hidl_string>& ids __unused)
{
    return Status::OPERATION_NOT_SUPPORTED;
}

void DummySecureCamera::serviceDied(
    uint64_t cookie,
    const ::android::wp<::android::hidl::base::V1_0::IBase>& who __unused)
{
    if (cookie != reinterpret_cast<uint64_t>(this)) {
        CAM_LOGE("Unexpected DummySecureCamera serviceDied cookie 0x%" PRIx64 ", expected %p",
                cookie, this);
    }
    CAM_LOGE("%s: DummySecureCamera serviceDied!!",__FUNCTION__);
}

// ---------------------------------------------------------------------------

CPUControl::~CPUControl()
{
    releaseWatchdog();
}

void CPUControl::enableCPUBoost()
{
    {
        std::unique_lock<std::mutex> _l(mCtrl.second);

        auto& cpuControl(mCtrl.first);

        cpuControl.reset(CpuCtrl::createInstance(),
                [](auto cpuCtrl)
                {
                    CAM_LOGD("notify power HAL to disable performance mode");
                    cpuCtrl->disable();
                    cpuCtrl->uninit();
                    cpuCtrl->destroyInstance();
                });
        if (CC_LIKELY(cpuControl.get()))
        {
            cpuControl->init();
            cpuControl->cpuPerformanceMode(kPerformanceBoostTimeoutInSecond);
            CAM_LOGD("notify power HAL to enable performance mode");
        }
        else
        {
            CAM_LOGW("notify power HAL to enable performance mode failed");
        }

        mRequestExit.store(false, std::memory_order_relaxed);
    }

    // start watchdog if kPerformanceBoostTimeoutInSecond > 0
    if (kPerformanceBoostTimeoutInSecond > 0)
        mWatchdog = std::thread(resetCPUBoostTimeout, std::ref(*this));
}

inline void CPUControl::disableCPUBoost()
{
    releaseWatchdog();

    std::unique_lock<std::mutex> _l(mCtrl.second);
    mCtrl.first = nullptr;
}

void CPUControl::releaseWatchdog()
{
    CAM_LOGD("releaseWatchdog +");

    // kick and wait watchdog to exit
    if (mWatchdog.joinable())
    {
        mRequestExit.store(true, std::memory_order_relaxed);
        mCondResetCPUTimeout.notify_one();
        CAM_LOGD("wait watchdog to exit...");
        mWatchdog.join();
        CAM_LOGD("wait watchdog to exit done");
    }

    CAM_LOGD("releaseWatchdog -");
}
