/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-customer_scenario_mgr"
//
#include <cutils/properties.h>
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
#include <mtkcam/utils/std/common.h>
//
#include <mtkcam3/3rdparty/customer/customer_scenario_mgr.h>
//
#include <map>
#include <memory>
#include <vector>
#include <unordered_map>

#define USE_CUSTOMER_SCENARIO_MGR 0

/******************************************************************************
 *
 ******************************************************************************/
#define __DEBUG // enable function scope debug
#ifdef __DEBUG
#include <memory>
#define FUNCTION_SCOPE \
 auto __scope_logger__ = [](char const* f)->std::shared_ptr<const char>{ \
    CAM_LOGD("(%d)[%s] + ", ::gettid(), f); \
    return std::shared_ptr<const char>(f, [](char const* p){CAM_LOGD("(%d)[%s] -", ::gettid(), p);}); \
}(__FUNCTION__)
#else
#define FUNCTION_SCOPE
#endif

/******************************************************************************
 *
 ******************************************************************************/
using namespace android;
using namespace NSCam;
using namespace NSCam::NSPipelinePlugin;
using namespace NSCam::v3::pipeline::policy::scenariomgr;
/******************************************************************************
 *
 ******************************************************************************/

#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
//
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)

// ======================================================================================================
// TODO: Feature Combinations for customer
// #define  <feature combination>              (key feature         | post-processing features | ...)
//
// single cam capture feature combination
#define TP_FEATURE_COMBINATION_SINGLE          (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_HDR             (TP_FEATURE_HDR      | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_AINR            (MTK_FEATURE_AINR    | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_MFNR            (MTK_FEATURE_MFNR    | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_REMOSAIC        (MTK_FEATURE_REMOSAIC| MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_CSHOT           (NO_FEATURE_NORMAL   | MTK_FEATURE_CZ)
#define TP_FEATURE_COMBINATION_YUV_REPROCESS   (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_RAW_REPROCESS   (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_SUPER_NIGHT     (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| TP_FEATURE_FB| MTK_FEATURE_FB)
#define TP_FEATURE_COMBINATION_PRO             (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE)

// dual cam capture feature combination
// the VSDOF means the combination of Bokeh feature and Depth feature
#define TP_FEATURE_COMBINATION_TP_VSDOF           (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB| TP_FEATURE_VSDOF)
#define TP_FEATURE_COMBINATION_TP_VSDOF_HDR       (TP_FEATURE_HDR_DC   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB| TP_FEATURE_VSDOF)
#define TP_FEATURE_COMBINATION_TP_VSDOF_MFNR      (MTK_FEATURE_MFNR    | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB| TP_FEATURE_VSDOF)
#define TP_FEATURE_COMBINATION_TP_FUSION          (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB| TP_FEATURE_FUSION)
#define TP_FEATURE_COMBINATION_TP_PUREBOKEH       (NO_FEATURE_NORMAL   | MTK_FEATURE_NR| MTK_FEATURE_ABF| MTK_FEATURE_CZ| MTK_FEATURE_DRE| TP_FEATURE_FB| MTK_FEATURE_FB| TP_FEATURE_PUREBOKEH)

// streaming feature combination (TODO: it should be refined by streaming scenario feature)
#define TP_FEATURE_COMBINATION_VIDEO_NORMAL       (MTK_FEATURE_FB|TP_FEATURE_FB)
#define TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV     (MTK_FEATURE_FB|MTK_FEATURE_DUAL_YUV|TP_FEATURE_FB|TP_FEATURE_DUAL_YUV)
#define TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH (MTK_FEATURE_FB|MTK_FEATURE_DUAL_HWDEPTH|TP_FEATURE_FB|TP_FEATURE_DUAL_HWDEPTH)
#define TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF (MTK_FEATURE_FB|TP_FEATURE_FB)
// ======================================================================================================
//
/******************************************************************************
 *
 ******************************************************************************/

namespace NSCam {
namespace v3 {
namespace pipeline {
namespace policy {
namespace scenariomgr {

bool gForceCustomerScenarioMgr = ::property_get_int32("vendor.debug.camera.customer.scenario.force", USE_CUSTOMER_SCENARIO_MGR);
bool gIsLowMemoryDevice = ::property_get_bool("ro.config.low_ram", false);

// TODO: add scenario/feature set by openId order for camera scenario by customer
const std::vector<std::unordered_map<int32_t, ScenarioFeatures>>  gCustomerScenarioFeaturesMaps =
{
    // openId = 0
    {
        // capture
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_NORMAL)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR,       TP_FEATURE_COMBINATION_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_AINR,     TP_FEATURE_COMBINATION_AINR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,     TP_FEATURE_COMBINATION_MFNR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_REMOSAIC, TP_FEATURE_COMBINATION_REMOSAIC)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,    TP_FEATURE_COMBINATION_SINGLE)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR_DC, TP_FEATURE_COMBINATION_TP_VSDOF_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,  TP_FEATURE_COMBINATION_TP_VSDOF_MFNR)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_TP_PUREBOKEH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_CSHOT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_CSHOT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_YUV_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_RAW_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_SUPER_NIGHT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_PRO)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_PRO)
        CAMERA_SCENARIO_END
        //
        // streaming
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_NORMAL)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_NORMAL)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF)
        CAMERA_SCENARIO_END
    },
    // openId = 1
    {
        // capture
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_NORMAL)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR,       TP_FEATURE_COMBINATION_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_AINR,     TP_FEATURE_COMBINATION_AINR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,     TP_FEATURE_COMBINATION_MFNR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_REMOSAIC, TP_FEATURE_COMBINATION_REMOSAIC)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,    TP_FEATURE_COMBINATION_SINGLE)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR_DC, TP_FEATURE_COMBINATION_TP_VSDOF_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,  TP_FEATURE_COMBINATION_TP_VSDOF_MFNR)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_TP_PUREBOKEH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_CSHOT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_CSHOT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_YUV_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_RAW_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_SUPER_NIGHT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_PRO)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_PRO)
        CAMERA_SCENARIO_END
        //
        // streaming
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_NORMAL)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_NORMAL)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF)
        CAMERA_SCENARIO_END
    },
    // openId = 2
    {
        // capture
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_NORMAL)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR,       TP_FEATURE_COMBINATION_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,     TP_FEATURE_COMBINATION_MFNR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_REMOSAIC, TP_FEATURE_COMBINATION_REMOSAIC)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,    TP_FEATURE_COMBINATION_SINGLE)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR_DC,  TP_FEATURE_COMBINATION_TP_VSDOF_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,   TP_FEATURE_COMBINATION_TP_VSDOF_MFNR)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,  TP_FEATURE_COMBINATION_TP_PUREBOKEH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_CSHOT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_CSHOT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_YUV_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_RAW_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_SUPER_NIGHT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_PRO)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_PRO)
        CAMERA_SCENARIO_END
        //
        // streaming
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_NORMAL)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_NORMAL)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF)
        CAMERA_SCENARIO_END
    },
    // openId = 3
    {
        // capture
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_NORMAL)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR,       TP_FEATURE_COMBINATION_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,     TP_FEATURE_COMBINATION_MFNR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_REMOSAIC, TP_FEATURE_COMBINATION_REMOSAIC)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,    TP_FEATURE_COMBINATION_SINGLE)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR_DC,  TP_FEATURE_COMBINATION_TP_VSDOF_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,   TP_FEATURE_COMBINATION_TP_VSDOF_MFNR)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,  TP_FEATURE_COMBINATION_TP_PUREBOKEH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_CSHOT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_CSHOT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_YUV_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_RAW_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_SUPER_NIGHT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_PRO)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_PRO)
        CAMERA_SCENARIO_END
        //
        // streaming
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_NORMAL)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_NORMAL)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF)
        CAMERA_SCENARIO_END
    },
    // openId = 4
    {
        // capture
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_NORMAL)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR,       TP_FEATURE_COMBINATION_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,     TP_FEATURE_COMBINATION_MFNR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_REMOSAIC, TP_FEATURE_COMBINATION_REMOSAIC)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,    TP_FEATURE_COMBINATION_SINGLE)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM)
        ADD_CAMERA_FEATURE_SET(TP_FEATURE_HDR_DC,  TP_FEATURE_COMBINATION_TP_VSDOF_HDR)
        ADD_CAMERA_FEATURE_SET(MTK_FEATURE_MFNR,   TP_FEATURE_COMBINATION_TP_VSDOF_MFNR)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL,  TP_FEATURE_COMBINATION_TP_PUREBOKEH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_CSHOT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_CSHOT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_YUV_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_RAW_REPROCESS)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_SUPER_NIGHT)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(CUSTOMER_CAMERA_SCENARIO_CAPTURE_PRO)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_PRO)
        CAMERA_SCENARIO_END
        //
        // streaming
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_NORMAL)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_NORMAL)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_YUV)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWDEPTH)
        CAMERA_SCENARIO_END
        //
        CAMERA_SCENARIO_START(MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF)
        ADD_CAMERA_FEATURE_SET(NO_FEATURE_NORMAL, TP_FEATURE_COMBINATION_VIDEO_DUAL_HWVSDOF)
        CAMERA_SCENARIO_END
    },

    // TODO: add more for openId = N
    // ...
};

auto customer_get_capture_scenario(
    int32_t &scenario, /*eCameraScenario*/
    const ScenarioHint& scenarioHint,
    IMetadata const* pAppMetadata
) -> bool
{
    if (CC_UNLIKELY(pAppMetadata == nullptr)) {
        MY_LOGE("pAppMetadata is invalid nullptr!");
        return false;
    }

    FUNCTION_SCOPE;

    scenario = CUSTOMER_CAMERA_SCENARIO_UNKNOW;
    MY_LOGD("scenarioHint(isCShot:%d, isDualCam:%d, isVSDoFMode:%d, captureScenarioIndex:%d)",
            scenarioHint.isCShot, scenarioHint.isDualCam, scenarioHint.isVSDoFMode, scenarioHint.captureScenarioIndex);

    // TODO: customer can modified the logic/flow to decide the streaming scenario.
    if (scenarioHint.captureScenarioIndex > 0) {  // force by vendor tag (ex:Pro mode))
        MY_LOGI("forced captureScenarioIndex:%d", scenarioHint.captureScenarioIndex);
        scenario = scenarioHint.captureScenarioIndex;
    }
    else if (scenarioHint.operationMode == 0x8001 /*VENDOR_SUPER_NIGHT*/) {
        /*example code*/
        scenario = CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT;
    }
    else if (scenarioHint.isYuvReprocess) {
        MY_LOGI("Yuv reprocessing scenario!");
        scenario = MTK_CAMERA_SCENARIO_CAPTURE_YUV_REPROCESS;
    }
    else if (scenarioHint.isRawReprocess) {
        MY_LOGI("Raw reprocessing scenario!");
        scenario = MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS;
    }
    else if (scenarioHint.isCShot) { // CShot
        MY_LOGI("CShot scenario!");
        scenario = MTK_CAMERA_SCENARIO_CAPTURE_CSHOT;
    }
    else if (scenarioHint.isDualCam && scenarioHint.isVSDoFMode) {
        MY_LOGI("DualCam VSDoF scenario!");
        scenario = MTK_CAMERA_SCENARIO_CAPTURE_DUALCAM;
    }
    else {
        MY_LOGI("no dedicated scenario, normal scenario");
        scenario = MTK_CAMERA_SCENARIO_CAPTURE_NORMAL;
    }

    MY_LOGI("scenario:%d", scenario);
    return true;
}

auto customer_get_streaming_scenario(
    int32_t &scenario, /*eCameraScenario*/
    const ScenarioHint& scenarioHint,
    IMetadata const* pAppMetadata
) -> bool
{
    if (CC_UNLIKELY(pAppMetadata == nullptr)) {
        MY_LOGE("pAppMetadata is invalid nullptr!");
        return false;
    }

    FUNCTION_SCOPE;

    scenario = CUSTOMER_CAMERA_SCENARIO_UNKNOW;
    MY_LOGD("scenarioHint(isDualCam:%d(0x%x), streamingScenarioIndex:%d)",
            scenarioHint.isDualCam, scenarioHint.mDualFeatureMode, scenarioHint.streamingScenarioIndex);

    // TODO: customer can modified the logic/flow to decide the streaming scenario.
    if (scenarioHint.streamingScenarioIndex > 0) { // forced by vendor tag
        MY_LOGI("forced streamingScenarioIndex:%d", scenarioHint.streamingScenarioIndex);
        scenario = scenarioHint.streamingScenarioIndex;
    }
    else if (scenarioHint.isDualCam) {
        MY_LOGI("DualCam scenario!");
        switch( scenarioHint.mDualFeatureMode )
        {
        case DualFeatureMode_YUV:
            scenario = MTK_CAMERA_SCENARIO_STREAMING_DUAL_YUV; break;
        case DualFeatureMode_HW_DEPTH:
            scenario = MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWDEPTH; break;
        case DualFeatureMode_MTK_VSDOF:
            scenario = MTK_CAMERA_SCENARIO_STREAMING_DUAL_HWVSDOF; break;
        default:
            scenario = MTK_CAMERA_SCENARIO_STREAMING_NORMAL; break;
        }
    }
    else {
        MY_LOGI("no dedicated scenario, normal scenario");
        scenario = MTK_CAMERA_SCENARIO_STREAMING_NORMAL;
    }

    MY_LOGI("scenario:%d", scenario);
    return true;
}

auto customer_get_features_table_by_scenario(
    int32_t openId,
    int32_t const scenario, /*eCameraScenario*/
    ScenarioFeatures& scenarioFeatures
) -> bool
{
    FUNCTION_SCOPE;

    size_t tableSize = gCustomerScenarioFeaturesMaps.size();
    MY_LOGD("scenario:%d, table size:%zu", scenario, tableSize);

    if (openId >= static_cast<int32_t>(tableSize)) {
        MY_LOGE("cannot query featuresTable, openId(%d) is out of gCustomerScenarioFeaturesMaps size(%zu)",
                openId, tableSize);
        return false;
    }
    auto scenarioFeaturesMap = gCustomerScenarioFeaturesMaps[openId];

    auto iter_got = scenarioFeaturesMap.find(scenario);
    if ( iter_got != scenarioFeaturesMap.end()) {
        scenarioFeatures = iter_got->second;
        MY_LOGI("find features for scenario(%d : %s)", scenario, scenarioFeatures.scenarioName.c_str());
    }
    else {
        MY_LOGE("cannot find features for openId(%d), scenario(%d) in gScenarioFeaturesMap", openId, scenario);
        return false;
    }

    return true;
}

auto customer_get_capture_config_by_scenario(
    const int32_t scenario, /*eCameraScenario*/
    CaptureScenarioConfig& captureConfig
) -> bool
{

    switch (scenario) {
        case CUSTOMER_CAMERA_SCENARIO_CAPTURE_SUPER_NIGHT:
            captureConfig.maxAppJpegStreamNum = 8;
            captureConfig.maxAppRaw16OutputBufferNum = 17;
            break;
        case MTK_CAMERA_SCENARIO_CAPTURE_RAW_REPROCESS:
            captureConfig.maxAppJpegStreamNum = 8;
            captureConfig.maxAppRaw16OutputBufferNum = 3;
            break;
        default:
            if (gIsLowMemoryDevice) {
                MY_LOGI("it is low_ram project");
                captureConfig.maxAppJpegStreamNum = 10;
            }
            else {
                captureConfig.maxAppJpegStreamNum = 20; /* for best CShot performance*/
            }
            captureConfig.maxAppRaw16OutputBufferNum = 1;
            break;

    }

    return true;
}

bool customer_get_capture_scenario(
    int32_t &scenario,
    ScenarioFeatures &scenarioFeatures,
    CaptureScenarioConfig &captureConfig,
    int32_t openId,
    const ScenarioHint &scenarioHint,
    const IMetadata *pAppMetadata
)
{
    if( !gForceCustomerScenarioMgr ) {
        MY_LOGD("not support: USE_CUSTOMER_SCENARIO_MGR(%d), set forced(%d))", USE_CUSTOMER_SCENARIO_MGR, gForceCustomerScenarioMgr);
        return false;
    }

    return customer_get_capture_scenario(scenario, scenarioHint, pAppMetadata) &&
           customer_get_capture_config_by_scenario(scenario, captureConfig) &&
           customer_get_features_table_by_scenario(openId, scenario, scenarioFeatures);
}

bool customer_get_streaming_scenario(
    int32_t &scenario,
    ScenarioFeatures &scenarioFeatures,
    int32_t openId,
    const ScenarioHint &scenarioHint,
    const IMetadata *pAppMetadata
)
{
    if( !gForceCustomerScenarioMgr ) {
        MY_LOGD("not support: USE_CUSTOMER_SCENARIO_MGR(%d), set forced(%d))", USE_CUSTOMER_SCENARIO_MGR, gForceCustomerScenarioMgr);
        return false;
    }

    return customer_get_streaming_scenario(scenario, scenarioHint, pAppMetadata) &&
           customer_get_features_table_by_scenario(openId, scenario, scenarioFeatures);
}


};  //namespace scenariomgr
};  //namespace policy
};  //namespace pipeline
};  //namespace v3
};  //namespace NSCam

