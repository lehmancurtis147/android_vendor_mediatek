/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <mtkcam/utils/std/Format.h>
#include <mtkcam3/3rdparty/plugin/PipelinePlugin.h>
#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>

using namespace std;
using namespace NSCam::Utils;
/******************************************************************************
 *
 ******************************************************************************/
namespace NSCam {
namespace NSPipelinePlugin {


template<typename T>
typename PluginRegistry<T>::ProviderRegistry
PluginRegistry<T>::sProviderRegistry;

template<typename T>
typename PluginRegistry<T>::InterfaceRegistry
PluginRegistry<T>::sInterfaceRegistry;

template<typename T>
void PluginRegistry<T>::addProvider(ConstructProvider fnConstructor)
{
    ProviderRegistry& reg = ofProvider();
    reg.push_back(fnConstructor);
}

template<typename T>
void PluginRegistry<T>::addProvider(ConstructProvider fnConstructor, PLUGIN_ID_TYPE id)
{
    ProviderRegistry& reg = ofProvider();
    reg.push_back(ConstructProviderEntry(fnConstructor, id));
}

template<typename T>
void PluginRegistry<T>::addInterface(ConstructInterface fnConstructor)
{
    InterfaceRegistry& reg = ofInterface();
    reg.push_back(fnConstructor);
}

template class PluginRegistry<Raw>;
template class PluginRegistry<MultiFrame>;
template class PluginRegistry<Fusion>;
template class PluginRegistry<Yuv>;
template class PluginRegistry<Depth>;
template class PluginRegistry<Bokeh>;
template class PluginRegistry<Join>;

/******************************************************************************
* Template Implementation
******************************************************************************/

template<typename T>
std::map<MINT, typename PipelinePlugin<T>::WeakPtr>
PipelinePlugin<T>::mInstances;

template<typename T>
const std::vector<typename PipelinePlugin<T>::IProvider::Ptr>&
PipelinePlugin<T>::getProviders()
{
    typedef PluginRegistry<T> Registry;

    if (mpProviders.size() == 0) {
        for (auto r : Registry::ofProvider()) {
            typename IProvider::Ptr provider = r.mConstruct();
            provider->set(mOpenId, mOpenId2);
            mpProviders.push_back(provider);
        }
    }

    return mpProviders;
};

template<typename T>
const std::vector<typename PipelinePlugin<T>::IProvider::Ptr>&
PipelinePlugin<T>::getProviders(PLUGIN_ID_TYPE mask)
{
    typedef PluginRegistry<T> Registry;

    if (mpProviders.size() == 0) {
        for (auto r : Registry::ofProvider()) {
            if( !r.mID || (r.mID & mask) )
            {
                typename IProvider::Ptr provider = r.mConstruct();
                provider->set(mOpenId, mOpenId2);
                mpProviders.push_back(provider);
            }
        }
    }

    return mpProviders;
};

template<typename T>
typename PipelinePlugin<T>::Request::Ptr
PipelinePlugin<T>::createRequest()
{
    return std::make_shared<PipelinePlugin<T>::Request>();
}

template<typename T>
typename PipelinePlugin<T>::IInterface::Ptr
PipelinePlugin<T>::getInterface()
{
    typedef PluginRegistry<T> Registry;

    if (mpInterface == nullptr) {
        for (auto c : Registry::ofInterface()) {
            mpInterface = c();
            break;
        }
    }

    return mpInterface;
}

template<typename T>
const typename PipelinePlugin<T>::Selection&
PipelinePlugin<T>::getSelection(typename IProvider::Ptr provider)
{
    static Selection sel;
    auto intf = getInterface();
    if (intf != nullptr) {
        Selection sel;
        intf->offer(sel);
        provider->negotiate(sel);
    }

    return sel;
}

template<typename T>
typename PipelinePlugin<T>::Selection::Ptr
PipelinePlugin<T>::createSelection()
{
    return std::make_shared<PipelinePlugin<T>::Selection>();
}

template<typename T>
MVOID
PipelinePlugin<T>::pushSelection(typename IProvider::Ptr provider, typename Selection::Ptr selection)
{
    std::lock_guard<std::mutex> l(mMutex);
    mpSelections[provider].push(selection);
}

template<typename T>
typename PipelinePlugin<T>::Selection::Ptr
PipelinePlugin<T>::popSelection(typename IProvider::Ptr provider)
{
    std::lock_guard<std::mutex> l(mMutex);
    auto& selections = mpSelections[provider];
    if (selections.empty())
        return nullptr;

    auto sel = selections.front();
    selections.pop();

    return sel;
}

template<typename T>
typename PipelinePlugin<T>::Selection::Ptr
PipelinePlugin<T>::frontSelection(typename IProvider::Ptr provider)
{
    std::lock_guard<std::mutex> l(mMutex);
    auto& selections = mpSelections[provider];
    if (selections.empty())
        return nullptr;

    return selections.front();
}

template<typename T>
MVOID
PipelinePlugin<T>::dump(std::ostream& os)
{
    FieldPrinter printer = FieldPrinter(os);

    Selection sel;
    auto intf = getInterface();

    if (intf == nullptr) {
        os << "  [No Interface Registered]" << std::endl;
        return;
    }

    intf->offer(sel);
    os << "  [Interface Capability]" << std::endl;
    Reflector::ForEach(sel, printer);

    int n = 0;
    for (auto provider : getProviders()) {
        os << "  [Provider " << ++n << " Property]" << std::endl;
        Reflector::ForEach(provider->property(), printer);
    }
}

template<typename T>
typename PipelinePlugin<T>::Ptr
PipelinePlugin<T>::getInstance(MINT32 iOpenId, MINT32 iOpenId2)
{
    MINT index = iOpenId;
    // Hash to unique key
    if (iOpenId2 > 0)
        index += (iOpenId2 + 1) * 100;

    Ptr sp =  mInstances[index].lock();
    if (sp == nullptr) {
        sp = std::make_shared<PipelinePlugin<T>>(iOpenId, iOpenId2);
        mInstances[index] = sp;
    }

    return sp;
}

template<typename T>
PipelinePlugin<T>::~PipelinePlugin()
{
    mpInterface = nullptr;
    mpProviders.clear();
}

template class PipelinePlugin<Raw>;
template class PipelinePlugin<MultiFrame>;
template class PipelinePlugin<Fusion>;
template class PipelinePlugin<Yuv>;
template class PipelinePlugin<Depth>;
template class PipelinePlugin<Bokeh>;
template class PipelinePlugin<Join>;

/******************************************************************************
 *
 ******************************************************************************/
class MetadataSelection::Implementor
{
public:
    Implementor()
        : mRequired(MFALSE)
    {}

    MBOOL               mRequired;
    MetadataPtr         mControl;
    MetadataPtr         mAddtional;
    MetadataPtr         mDummy;
};

MetadataSelection::MetadataSelection()
    : mImpl(new Implementor())
{}

MetadataSelection::MetadataSelection(const MetadataSelection& ms)
    : mImpl(new Implementor())
{
    if (ms.mImpl != nullptr)
        *mImpl = *ms.mImpl;
}
 
MetadataSelection::~MetadataSelection()
{
    delete mImpl;
}

MetadataSelection& MetadataSelection::setRequired(MBOOL required)
{
    mImpl->mRequired = required;
    return *this;
}

MBOOL MetadataSelection::getRequired() const
{
    return mImpl->mRequired;
}

MetadataSelection& MetadataSelection::setControl(MetadataPtr control)
{
    mImpl->mControl = control;
    return *this;
}

MetadataSelection& MetadataSelection::setAddtional(MetadataPtr addtional)
{
    mImpl->mAddtional = addtional;
    return *this;
}

MetadataSelection& MetadataSelection::setDummy(MetadataPtr dummy)
{
    mImpl->mDummy = dummy;
    return *this;
}

MetadataPtr MetadataSelection::getControl() const
{
    return mImpl->mControl;
}

MetadataPtr MetadataSelection::getAddtional() const
{
    return mImpl->mAddtional;
}

MetadataPtr MetadataSelection::getDummy() const
{
    return mImpl->mDummy;
}

MVOID MetadataSelection::dump(std::ostream& os) const
{
    os << "{ }";
}

/******************************************************************************
 *
 ******************************************************************************/
class BufferSelection::Implementor
{
public:
    Implementor()
        : mRequired(MFALSE)
        , mOptional(MFALSE)
        , mSpecifiedSize(0, 0)
        , mAlignment(0, 0)
        , mDirtyFormats(MTRUE)
        , mDirtySizes(MTRUE)
    {}

    MBOOL               mRequired;
    MBOOL               mOptional;
    std::vector<MINT>   mAcceptedFormats;
    std::vector<MINT>   mAcceptedSizes;
    MSize               mSpecifiedSize;
    MSize               mAlignment;
    std::vector<MINT>   mSupportFormats;
    std::vector<MINT>   mSupportSizes;
    std::vector<MINT>   mFormats;
    std::vector<MINT>   mSizes;
    MBOOL               mDirtyFormats;
    MBOOL               mDirtySizes;
};

BufferSelection::BufferSelection()
    : mImpl(new Implementor())
{}

BufferSelection::BufferSelection(const BufferSelection& bs)
    : mImpl(new Implementor())
{
    if (bs.mImpl != nullptr)
        *mImpl = *bs.mImpl;
}

BufferSelection::~BufferSelection()
{
    delete mImpl;
}

BufferSelection& BufferSelection::setRequired(MBOOL required)
{
    mImpl->mRequired = required;
    return *this;
}

BufferSelection& BufferSelection::setOptional(MBOOL optional)
{
    mImpl->mOptional = optional;
    return *this;
}

MBOOL BufferSelection::getRequired() const
{
    return mImpl->mRequired;
}

MBOOL BufferSelection::getOptional() const
{
    return mImpl->mOptional;
}

BufferSelection& BufferSelection::addAcceptedFormat(MINT fmt)
{
    mImpl->mAcceptedFormats.push_back(fmt);
    mImpl->mDirtyFormats = MTRUE;
    return *this;
}

BufferSelection& BufferSelection::addAcceptedSize(MINT sz)
{
    mImpl->mAcceptedSizes.push_back(sz);
    mImpl->mDirtySizes = MTRUE;
    return *this;
}

BufferSelection& BufferSelection::setSpecifiedSize(const MSize& sz)
{
    mImpl->mSpecifiedSize = sz;
    return *this;
}

BufferSelection& BufferSelection::setAlignment(MUINT32 width, MUINT32 height)
{
    mImpl->mAlignment.w = width;
    mImpl->mAlignment.h = height;
    return *this;
}

const MSize& BufferSelection::getSpecifiedSize() const
{
    return mImpl->mSpecifiedSize;
}

MVOID BufferSelection::getAlignment(MUINT32& width, MUINT32& height) const
{
    width = mImpl->mAlignment.w;
    height = mImpl->mAlignment.h;
}

MBOOL BufferSelection::isValid() const
{
    return getSizes().size() > 0 && getFormats().size() > 0;
}


BufferSelection& BufferSelection::addSupportFormat(MINT fmt)
{
    mImpl->mSupportFormats.push_back(fmt);
    mImpl->mDirtyFormats = MTRUE;
    return *this;
}

BufferSelection& BufferSelection::addSupportSize(MINT sz)
{
    mImpl->mSupportSizes.push_back(sz);
    mImpl->mDirtySizes = MTRUE;
    return *this;
}

const vector<MINT>& BufferSelection::getFormats() const
{
    auto& formats = mImpl->mFormats;
    if (mImpl->mDirtyFormats) {
        formats.clear();

        auto& rSupport = mImpl->mSupportFormats;
        for (MINT fmt : mImpl->mAcceptedFormats) {
            if (find(rSupport.begin(), rSupport.end(), fmt) != rSupport.end()) {
                formats.push_back(fmt);
            }
        }
    }

    return formats;
}

const vector<MINT>& BufferSelection::getSizes() const
{
    auto& sizes = mImpl->mSizes;
    if (mImpl->mDirtySizes) {
        sizes.clear();

        auto& rSupport = mImpl->mSupportSizes;
        for (MINT fmt : mImpl->mAcceptedSizes) {
            if (find(rSupport.begin(), rSupport.end(), fmt) != rSupport.end()) {
                sizes.push_back(fmt);
            }
        }
    }

    return sizes;
}


MVOID BufferSelection::dump(std::ostream& os) const
{
    os << "{";

    // print support format
    bool firstElement = true;
    for (auto fmt : mImpl->mSupportFormats) {
        if (firstElement) {
            os << " Format:[";
            firstElement = false;
        } else
            os << " ";

        os << Format::queryImageFormatName(fmt);
    }
    if (!firstElement)
        os << "]";

    auto stringizeImageSize = [](MINT s) -> const char *
    {
        switch(s) {
            case eImgSize_Full:
                return "Full";
            case eImgSize_Resized:
                return "Resized";
            case eImgSize_Specified:
                return "Specified";
        }
        return "Unknown";
    };

    // print support size
    firstElement = true;
    for(auto v : mImpl->mSupportSizes) {
        if (firstElement) {
            os << " Size:[";
            firstElement = false;
        } else
            os << " ";

        os << stringizeImageSize(v);
    }
    if (!firstElement)
        os << "]";

    // print accept format
    firstElement = true;
    for(auto fmt : mImpl->mAcceptedFormats) {
        if (firstElement) {
            os << " Accepted format:[";
            firstElement = false;
        } else
            os << " ";

        os << Format::queryImageFormatName(fmt);
    }
    if (!firstElement)
        os << "]";

    // print accept size
    firstElement = true;
    for(auto v : mImpl->mAcceptedSizes) {
        if (firstElement) {
            os << " Accepted sizes:[";
            firstElement = false;
        } else
            os << " ";

        os << stringizeImageSize(v);
    }
    if (!firstElement)
        os << "]";


    // print specific size
    if (!!mImpl->mSpecifiedSize)
        os << " Specified:(" << mImpl->mSpecifiedSize.w << "x" <<  mImpl->mSpecifiedSize.h << ")";

    if (!!mImpl->mAlignment)
        os << " Align:(" << mImpl->mAlignment.w << "/" <<  mImpl->mAlignment.h << ")";
    os << " }";
}


/******************************************************************************
 * Object Printer
 ******************************************************************************/
std::ostream& operator<<(std::ostream& os, const std::shared_ptr<BufferHandle> hnd) {
    if (hnd == nullptr)
        return os << "{ null }";

    hnd->dump(os);
    return os;
}

std::ostream& operator<<(std::ostream& os, const std::shared_ptr<MetadataHandle> hnd) {
    if (hnd == nullptr)
        return os << "{ null }";

    hnd->dump(os);
    return os;
}

std::ostream& operator<<(std::ostream& os, const BufferSelection& sel) {
    sel.dump(os);
    return os;
}

std::ostream& operator<<(std::ostream& os, const MetadataSelection& sel) {
    sel.dump(os);
    return os;
}

/******************************************************************************
*
******************************************************************************/
};  //namespace NSPipelinePlugin
};  //namespace NSCam

