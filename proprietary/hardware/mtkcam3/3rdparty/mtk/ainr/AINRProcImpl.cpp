/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define LOG_TAG "AINRPlugin"
//
#include <mtkcam/utils/std/Log.h>
//
#include <stdlib.h>
#include <utils/Errors.h>
#include <utils/List.h>
#include <utils/RefBase.h>
#include <utils/String8.h>
#include <sstream>
//
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//
//
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
//
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/std/Format.h>
//
#include <mtkcam3/pipeline/hwnode/NodeId.h>

#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metastore/ITemplateRequest.h>
#include <mtkcam3/3rdparty/plugin/PipelinePlugin.h>
#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>
// MTKCAM
#include <mtkcam/aaa/IHal3A.h> // setIsp, CaptureParam_T
//
#include <mtkcam/utils/TuningUtils/FileDumpNamingRule.h> // tuning file naming
#include <sys/stat.h> // mkdir
#include <sys/prctl.h> //prctl set name
#include <mtkcam/drv/iopipe/SImager/ISImager.h>
#include <cutils/properties.h>
//
using namespace NSCam;
using namespace android;
using namespace std;
using namespace NSCam::NSPipelinePlugin;
using namespace NS3Av3;
using namespace NSCam::TuningUtils;
using namespace NSIoPipe;
/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
//
#define ASSERT(cond, msg)           do { if (!(cond)) { printf("Failed: %s\n", msg); return; } }while(0)
//
#define FUNCTION_IN                 MY_LOGD("%s +", __FUNCTION__)
#define FUNCTION_OUT                MY_LOGD("%s -", __FUNCTION__)
//systrace
#if 1
#ifndef ATRACE_TAG
#define ATRACE_TAG                           ATRACE_TAG_CAMERA
#endif
#include <utils/Trace.h>

#define AINR_TRACE_CALL()                      ATRACE_CALL()
#define AINR_TRACE_NAME(name)                  ATRACE_NAME(name)
#define AINR_TRACE_BEGIN(name)                 ATRACE_BEGIN(name)
#define AINR_TRACE_END()                       ATRACE_END()
#else
#define HDR_TRACE_CALL()
#define HDR_TRACE_NAME(name)
#define HDR_TRACE_BEGIN(name)
#define HDR_TRACE_END()
#endif

#define FUNCTION_SCOPE          auto __scope_logger__ = create_scope_logger(__FUNCTION__)

static std::shared_ptr<char> create_scope_logger(const char* functionName)
{
    bool bEnableLog = !!::property_get_int32("vendor.debug.camera.ainr.log", 0);
    char* pText = const_cast<char*>(functionName);
    CAM_LOGD_IF(bEnableLog, "[%s] + ", pText);
    return std::shared_ptr<char>(pText, [bEnableLog](char* p){ CAM_LOGD_IF(bEnableLog, "[%s] -", p); });
}

/******************************************************************************
*
******************************************************************************/
class AINRProcImpl : public MultiFramePlugin::IProvider
{
    typedef MultiFramePlugin::Property Property;
    typedef MultiFramePlugin::Selection Selection;
    typedef MultiFramePlugin::Request::Ptr RequestPtr;
    typedef MultiFramePlugin::RequestCallback::Ptr RequestCallbackPtr;

public:
    virtual void set(MINT32 iOpenId, MINT32 iOpenId2)
    {
        AINR_TRACE_CALL();
        FUNCTION_SCOPE;
        MY_LOGD("set openId:%d openId2:%d", iOpenId, iOpenId2);
        mOpenId = iOpenId;
    }

    virtual const Property& property()
    {
        AINR_TRACE_CALL();
        FUNCTION_SCOPE;
        static Property prop;
        static bool inited;

        if (!inited) {
            prop.mName              = "MTK AINR";
            prop.mFeatures          = MTK_FEATURE_AINR;
            prop.mThumbnailTiming   = eTiming_P2;
            prop.mPriority          = ePriority_Highest;
            prop.mZsdBufferMaxNum   = 8; // maximum frames requirement
            inited                  = MTRUE;
        }
        return prop;
    };

    //if capture number is 4, "negotiate" would be called 4 times
    virtual MERROR negotiate(Selection& sel)
    {
        AINR_TRACE_CALL();
        FUNCTION_SCOPE;

        if(CC_UNLIKELY(mEnable == 0)) {
            MY_LOGD("Force off AINR");
            return BAD_VALUE;
        }

        IMetadata* appInMeta = sel.mIMetadataApp.getControl().get();

        bool bNeedAINR = true;

        if(bNeedAINR) {
            if (mEnable) {
                MY_LOGD("Force on AINR");
            } else {
                MY_LOGD("No need to execute AINR");
                return BAD_VALUE;
            }
        }

        // create MFNRShotInfo
        if (sel.mRequestIndex == 0) {

            mZSDMode = sel.mState.mZslRequest && sel.mState.mZslPoolReady;
            mFlashOn = sel.mState.mFlashFired;
            mRealIso = sel.mState.mRealIso;
            mShutterTime = sel.mState.mExposureTime;

            if (CC_UNLIKELY( mFlashOn && mZSDMode )) {
                MY_LOGD("do not use ZSD buffers due to Flash MFNR");
                mZSDMode = MFALSE;
            }

            sel.mDecision.mZslEnabled = true;
            sel.mDecision.mZslPolicy.mPolicy = v3::pipeline::policy::eZslPolicy_AfState;
        }

        // AINR need 6 frames
        // TODO: Need to remove hard code
        sel.mRequestCount = 6;

        sel.mIBufferFull
            .setRequired(MTRUE)
            .addAcceptedFormat(eImgFmt_BAYER10)
            .addAcceptedSize(eImgSize_Full);

        sel.mIMetadataDynamic.setRequired(MTRUE);
        sel.mIMetadataApp.setRequired(MTRUE);
        sel.mIMetadataHal.setRequired(MTRUE);

        //Only main frame has output buffer
        // TODO: Need to set outputbuffer as unpack16 format
        if (sel.mRequestIndex == 0) {
            sel.mOBufferFull
                .setRequired(MTRUE)
                .addAcceptedFormat(eImgFmt_BAYER10)
                .addAcceptedSize(eImgSize_Full);

            sel.mOMetadataApp.setRequired(MTRUE);
            sel.mOMetadataHal.setRequired(MTRUE);
        } else {
            sel.mOBufferFull.setRequired(MFALSE);
            sel.mOMetadataApp.setRequired(MFALSE);
            sel.mOMetadataHal.setRequired(MFALSE);
        }

        return OK;
    };

    virtual void init()
    {
        FUNCTION_SCOPE;
    };

    virtual MERROR process(RequestPtr pRequest,
                           RequestCallbackPtr pCallback)
    {
        AINR_TRACE_CALL();
        FUNCTION_SCOPE;
        //set thread's name
        ::prctl(PR_SET_NAME, "AINRPlugin", 0, 0, 0);

        if (pRequest->mIBufferFull != nullptr) {
            IImageBuffer* pIImgBuffer = pRequest->mIBufferFull->acquire();
            MY_LOGD("[IN] Full image VA: 0x%p", pIImgBuffer->getBufVA(0));
            if (mDump){
                // dump input buffer
                String8 fileResultName;
                char    pre_filename[512];
                IMetadata* pHalMeta = pRequest->mIMetadataHal->acquire();

                extract(&mDumpNamingHint, pHalMeta);
                mDumpNamingHint.SensorDev = mOpenId;
                // TODO: Need to modify ISP profile
                mDumpNamingHint.IspProfile = 2; //EIspProfile_Capture;
                genFileName_TUNING(pre_filename, sizeof(pre_filename), &mDumpNamingHint);

                fileResultName = String8::format("%s_Input_%d_%dx%d.yuv"
                    , pre_filename
                    , pRequest->mRequestIndex
                    , pIImgBuffer->getImgSize().w
                    , pIImgBuffer->getImgSize().h);
                pIImgBuffer->saveToFile(fileResultName);
            }
        }

        if (pRequest->mOBufferFull != nullptr) {
            IImageBuffer* pOImgBuffer = pRequest->mOBufferFull->acquire();

            // throw the same input buffer back
            IImageBuffer* pIImgBuffer = nullptr;
            if (pRequest->mIBufferFull != nullptr)
                pIImgBuffer = pRequest->mIBufferFull->acquire();

            if(CC_LIKELY(pIImgBuffer && pOImgBuffer)) {
                MY_LOGD("[OUT] Full image VA: 0x%p", pOImgBuffer->getBufVA(0));
                for (size_t i = 0; i < pOImgBuffer->getPlaneCount(); i++)
                {
                    size_t planeBufSize = pOImgBuffer->getBufSizeInBytes(i);
                    MUINT8 *srcPtr = (MUINT8 *)pIImgBuffer->getBufVA(i);
                    void *dstPtr = (void *)pOImgBuffer->getBufVA(i);
                    memcpy(dstPtr, srcPtr, planeBufSize);
                }
            }
            //end memory copy
            if (mDump){
                // dump output buffer
                String8 fileResultName;
                char    pre_filename[512];

                IMetadata* pHalMeta = nullptr;
                if (pRequest->mIMetadataHal != nullptr){
                    pHalMeta = pRequest->mIMetadataHal->acquire();
                    extract(&mDumpNamingHint, pHalMeta);
                }
                mDumpNamingHint.SensorDev = mOpenId;
                mDumpNamingHint.IspProfile = 2; //EIspProfile_Capture;
                genFileName_TUNING(pre_filename, sizeof(pre_filename), &mDumpNamingHint);

                fileResultName = String8::format("%s_Output_%d_%dx%d.yuv"
                    , pre_filename
                    , pRequest->mRequestIndex
                    , pOImgBuffer->getImgSize().w
                    , pOImgBuffer->getImgSize().h);
                pOImgBuffer->saveToFile(fileResultName);
            }
        }

        mvRequests.push_back(pRequest);
        MY_LOGD("collected request(%d/%d)",
                pRequest->mRequestIndex+1,
                pRequest->mRequestCount);

        if (pRequest->mRequestIndex == pRequest->mRequestCount - 1)
        {
            MY_LOGD("have collected all requests");
            for (auto req : mvRequests) {
                MY_LOGD("callback request(%d/%d) %p",
                        req->mRequestIndex+1,
                        req->mRequestCount, pCallback.get());
                if (pCallback != nullptr) {
                    pCallback->onCompleted(req, 0);
                }
            }
            mvRequests.clear();
        }
        return 0;
    };

    virtual void abort(vector<RequestPtr>& pRequests)
    {
        AINR_TRACE_CALL();
        FUNCTION_SCOPE;
        bool bAbort = false;
        for (auto req : pRequests){
            bAbort = false;
            for (std::vector<RequestPtr>::iterator it = mvRequests.begin() ; it != mvRequests.end(); it++){
                if((*it) == req){
                    mvRequests.erase(it);
                    bAbort = true;
                    break;
                }
            }
            if (!bAbort){
                MY_LOGW("Desire abort request[%d] is not found", req->mRequestIndex);
            }
        }
    };

    virtual void uninit()
    {
        AINR_TRACE_CALL();
    };

    AINRProcImpl()
        : mOpenId(0)
        , mRealIso(0)
        , mShutterTime(0)
        , mZSDMode(MFALSE)
        , mFlashOn(MFALSE)
    {
        AINR_TRACE_CALL();
        // TODO: Need to set enable back to -1 after AI-NR code done
        mEnable = ::property_get_int32("vendor.debug.camera.ainr.enable", 0);
        mDump = !!::property_get_int32("vendor.debug.camera.ainr.dump", 0);

    };

    virtual ~AINRProcImpl()
    {
        AINR_TRACE_CALL();
    };

private:

    //
    int                          mOpenId;
    //
    int                          mEnable;
    bool                         mDump;
    // file dump hint
    FILE_DUMP_NAMING_HINT        mDumpNamingHint;
    // collect request
    std::vector<RequestPtr>      mvRequests;
    //
    MINT32                          mRealIso;
    MINT32                          mShutterTime;
    MBOOL                           mZSDMode;
    MBOOL                           mFlashOn;
};

REGISTER_PLUGIN_PROVIDER(MultiFrame, AINRProcImpl);

