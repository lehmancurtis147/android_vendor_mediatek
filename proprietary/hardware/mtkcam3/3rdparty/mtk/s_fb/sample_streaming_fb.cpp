/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include <cutils/properties.h>

using NSCam::NSPipelinePlugin::Interceptor;
using NSCam::NSPipelinePlugin::PipelinePlugin;
using NSCam::NSPipelinePlugin::PluginRegister;
using NSCam::NSPipelinePlugin::Join;
using NSCam::NSPipelinePlugin::JoinPlugin;

using namespace NSCam::NSPipelinePlugin;
using NSCam::MSize;

using NSCam::MERROR;
using NSCam::IImageBuffer;
using NSCam::IMetadata;

#ifdef LOG_TAG
#undef LOG_TAG
#endif // LOG_TAG
#define LOG_TAG "MtkCam/TPI_S_FB"
#include <log/log.h>
#include <android/log.h>

#define MY_LOGI(fmt, arg...)  ALOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)  ALOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)  ALOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)  ALOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define FUNCTION_IN   MY_LOGD("%s +", __FUNCTION__)
#define FUNCTION_OUT  MY_LOGD("%s -", __FUNCTION__)

class S_FB_Plugin : public JoinPlugin::IProvider
{
public:
    typedef JoinPlugin::Property Property;
    typedef JoinPlugin::Selection Selection;
    typedef JoinPlugin::Request::Ptr RequestPtr;
    typedef JoinPlugin::RequestCallback::Ptr RequestCallbackPtr;

public:
    S_FB_Plugin();
    ~S_FB_Plugin();
    const Property& property();
    void set(MINT32 openID1, MINT32 openID2);
    void init();
    void uninit();
    MERROR negotiate(Selection& sel);
    MERROR process(RequestPtr pRequest, RequestCallbackPtr pCallback);
    void abort(std::vector<RequestPtr> &pRequests);

private:
    MERROR getConfigSetting(Selection &sel);
    MERROR getP1Setting(Selection &sel);
    MERROR getP2Setting(Selection &sel);
    void copy(const IImageBuffer *in, IImageBuffer *out);
    void drawMask(IImageBuffer *buffer, float fx, float fy, float fw, float fh);

private:
    bool  mDisponly = false;
    bool  mInplace = false;
    int    mOpenID1 = 0;
    int    mOpenID2 = 0;
};

S_FB_Plugin::S_FB_Plugin()
{
    MY_LOGI("create S_FB plugin");
}

S_FB_Plugin::~S_FB_Plugin()
{
    MY_LOGI("destroy  S_FB plugin");
}

void S_FB_Plugin::set(MINT32 openID1, MINT32 openID2)
{
    MY_LOGD("set openID1:%d openID2:%d", openID1, openID2);
    mOpenID1 = openID1;
    mOpenID2 = openID2;
}

const S_FB_Plugin::Property& S_FB_Plugin::property()
{
    static Property prop;
    static bool inited;

    if (!inited) {
        prop.mName = "MTK FB";
        prop.mFeatures = MTK_FEATURE_FB;
        //prop.mInPlace = MTRUE;
        //prop.mFaceData = eFD_Current;
        //prop.mPosition = 0;
        inited = true;
    }
    return prop;
}

MERROR S_FB_Plugin::negotiate(Selection &sel)
{
    MERROR ret = OK;

    if( sel.mSelStage == eSelStage_CFG )
    {
        ret = getConfigSetting(sel);
    }
    else if( sel.mSelStage == eSelStage_P1 )
    {
        ret = getP1Setting(sel);
    }
    else if( sel.mSelStage == eSelStage_P2 )
    {
        ret = getP2Setting(sel);
    }

    return ret;
}

MERROR S_FB_Plugin::getConfigSetting(Selection &sel)
{
    mDisponly = property_get_bool("vendor.debug.tpi.s.fb.disponly", 0);
    mInplace = mDisponly || property_get_bool("vendor.debug.tpi.s.fb.inplace", 0);

    sel.mCfgOrder = 3;
    sel.mCfgJoinEntry = eJoinEntry_S_YUV;
    sel.mCfgInplace = mInplace;
    sel.mCfgEnableFD = MTRUE;
    sel.mCfgRun = property_get_bool("vendor.debug.tpi.s.fb", 0);
    sel.mIBufferMain1.setRequired(MTRUE);
    if( !mDisponly && property_get_bool("vendor.debug.tpi.s.fb.nv21", 0) )
    {
        sel.mIBufferMain1.addAcceptedFormat(NSCam::eImgFmt_NV21);
    }
    if( !mDisponly && property_get_bool("vendor.debug.tpi.s.fb.size", 0) )
    {
        sel.mIBufferMain1.setSpecifiedSize(MSize(640, 480));
    }
    sel.mOBufferMain1.setRequired(MTRUE);
    //sel.mIBufferMain1.addAcceptedFormat(eImgFmt_NV21);

    IMetadata *meta = sel.mIMetadataApp.getControl().get();
    MY_LOGD("sessionMeta=%p", meta);

    return OK;
}

MERROR S_FB_Plugin::getP1Setting(Selection &sel)
{
    (void)sel;
    return OK;
}

MERROR S_FB_Plugin::getP2Setting(Selection &sel)
{
    MBOOL run = MTRUE;
    sel.mP2Run = run;
    return OK;
}

void S_FB_Plugin::init()
{
    MY_LOGI("init S_FB plugin");
}

void S_FB_Plugin::uninit()
{
    MY_LOGI("uninit S_FB plugin");
}

void S_FB_Plugin::abort(std::vector<RequestPtr> &pRequests)
{
    (void)pRequests;
    MY_LOGD("uninit S_FB plugin");
};

MERROR S_FB_Plugin::process(RequestPtr pRequest, RequestCallbackPtr pCallback)
{
    (void)pCallback;
    MERROR ret = -EINVAL;
    MBOOL needRun = MFALSE;
    MBOOL needDebug = MFALSE;
    IImageBuffer *in = NULL, *out = NULL;

    needRun = MTRUE;
    MY_LOGD("enter FB_PLUGIN needRun=%d", needRun);

    if( needRun &&
        pRequest->mIBufferMain1 != NULL &&
        pRequest->mOBufferMain1 != NULL )
    {
        in = pRequest->mIBufferMain1->acquire();
        out = pRequest->mOBufferMain1->acquire();
        MY_LOGD("FB_PLUGIN in=%p out=%p", in, out);

        if( in && out )
        {
            MSize inSize = in->getImgSize();
            MSize outSize = out->getImgSize();
            unsigned inPlane = in->getPlaneCount();
            unsigned outPlane = out->getPlaneCount();

            if( needDebug )
            {
                MY_LOGD("in(%dx%d:%d)=%p out(%dx%d:%d)=%p",
                    inSize.w, inSize.h, inPlane, in,
                    outSize.w, outSize.h, outPlane, out);
            }

            if( !mInplace )
            {
                copy(in, out);
            }
            drawMask(out, 0.2, 0.2, 0.1, 0.1);

            // simulate delay
            usleep(5000);
        }

        pRequest->mIBufferMain1->release();
        pRequest->mOBufferMain1->release();
        ret = OK;
    }

    MY_LOGD("exit FB_PLUGIN ret=%d", ret);
    return ret;
}

void S_FB_Plugin::copy(const IImageBuffer *in, IImageBuffer *out)
{
    if( !in || !out )
    {
        return;
    }

    unsigned inPlane = in->getPlaneCount();
    unsigned outPlane = out->getPlaneCount();

    for( unsigned i = 0; i < inPlane && i < outPlane; ++i )
    {
        char *inPtr = (char*)in->getBufVA(i);
        char *outPtr = (char*)out->getBufVA(i);
        unsigned inStride = in->getBufStridesInBytes(i);
        unsigned outStride = out->getBufStridesInBytes(i);
        unsigned inBytes = in->getBufSizeInBytes(i);
        unsigned outBytes = out->getBufSizeInBytes(i);

        if( !inPtr || !outPtr || !inStride || !outStride )
        {
            continue;
        }

        if( inStride == outStride )
        {
            memcpy(outPtr, inPtr, std::min(inBytes, outBytes));
        }
        else
        {
            unsigned stride = std::min(inStride, outStride);
            unsigned height = std::min(inBytes/inStride, outBytes/outStride);
            for( unsigned y = 0; y < height; ++y )
            {
                memcpy(outPtr+y*outStride, inPtr+y*inStride, stride);
            }
        }
    }
}

void S_FB_Plugin::drawMask(IImageBuffer *buffer, float fx, float fy, float fw, float fh)
{
    // sample: modify output buffer
    if( buffer )
    {
        char *ptr = (char*)buffer->getBufVA(0);
        if( ptr )
        {
            char mask = 128;
            MSize size = buffer->getImgSize();
            int stride = buffer->getBufStridesInBytes(0);
            int y_from = fy * size.h;
            int y_to = (fy + fh) * size.h;
            int x = fx * size.w;
            int width = fw * size.w;

            y_to = std::max(0, std::min(y_to, size.h));
            y_from = std::max(0, std::min(y_from, y_to));
            x = std::max(0, std::min(x, size.w));
            width = std::max(0, std::min(width, size.w));

            for( int y = y_from; y < y_to; ++y )
            {
                memset(ptr+y*stride + x, mask, width);
            }
        }
    }
}

//REGISTER_PLUGIN_PROVIDER_DYNAMIC(Join, S_FB_Plugin, MTK_FEATURE_FB);
REGISTER_PLUGIN_PROVIDER(Join, S_FB_Plugin);
