/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <sstream>

#include "CaptureFeatureInference.h"
#include "CaptureFeatureNode.h"
#include "CaptureFeature_Common.h"

#include <utility>

#define PIPE_CLASS_TAG "Inference"
#define PIPE_TRACE TRACE_CAPTURE_FEATURE_INFER
#include <featurePipe/core/include/PipeLog.h>

using namespace NSCam::NSPipelinePlugin;
using namespace NSCam::Utils::Format;

//#define __DEBUG

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

CaptureFeatureInferenceData::CaptureFeatureInferenceData()
    : mDataCount(0)
    , mRequestIndex(0)
    , mRequestCount(1)
    , mFaceDateType(0)
    , mBoostType(0)
    , mPipeBufferCounter(0)
    , mRequiredFD(MFALSE)
    , mPerFrameFD(MFALSE)
{
    memset(&mInferredItems, NULL_TYPE, sizeof(mInferredItems));
    memset(&mNodeInput, -1, sizeof(mNodeInput));
    memset(&mNodeOutput, -1, sizeof(mNodeOutput));
}

MVOID CaptureFeatureInferenceData::addNodeIO(NodeID_T nodeId,
        Vector<SrcData>& vSrcData,
        Vector<DstData>& vDstData,
        Vector<MetadataID_T>& vMetadata,
        Vector<FeatureID_T>& vFeature,
        MBOOL forced)
{
    MUINT8 index;
    BitSet64 bitFeatures;
    for (auto src : vSrcData)
    {
        if (mInferredItems[src.mTypeId] == NULL_TYPE)
            continue;

        index = mInferredItems[src.mTypeId];
        DataItem& item = mDataItems[index];
        // the first user decides the format & size
        if (src.mFormat != 0)
            item.mFormat = src.mFormat;
        item.mSizeId = src.mSizeId;
        if (src.mSize != MSize(0,0))
            item.mSize = src.mSize;
        item.markReference(nodeId);
        bitFeatures.value |= item.mFeatures.value;
    }

    for (FeatureID_T featId : vFeature)
        bitFeatures.markBit(featId);

    mNodeMeta[nodeId] = vMetadata;

    for (auto dst : vDstData) {
        MINT redirect = -1;
        if (dst.mInPlace) {
            redirect = mInferredItems[dst.mTypeId];
            // Recursive
            DataItem& item = mDataItems[redirect];
            if (item.mRedirect >= 0)
                redirect = item.mRedirect;
        }
        index = addDataItem(nodeId, dst.mTypeId, NULL_BUFFER, bitFeatures);
        DataItem& item = mDataItems[index];
        item.mFormat = dst.mFormat;
        item.mSize = dst.mSize;
        item.mSizeId = dst.mSizeId;
        if (dst.mInPlace)
            item.mRedirect = redirect;
    }

    if (forced)
        mNodeUsed.markBit(nodeId);

}

MSize CaptureFeatureInferenceData::getSize(TypeID_T typeId)
{
    MUINT8 index = mInferredItems[typeId];
    DataItem& item = mDataItems[index];

    return item.mSize;
}

MVOID CaptureFeatureInferenceData::addSource(TypeID_T typeId, BufferID_T bufId, Format_T fmt, MSize size)
{
    MUINT8 index = addDataItem(NID_ROOT, typeId, bufId);
    DataItem& item = mDataItems[index];
    item.mFormat = fmt;
    item.mSize = size;
}

MVOID CaptureFeatureInferenceData::addTarget(TypeID_T typeId, BufferID_T bufId)
{
    int index = -1;
    size_t featureSize = 0;
    for (int i = 0; i < mDataCount ; i++) {
        DataItem& item = mDataItems[i];
        if (item.mTypeId != typeId)
            continue;

        if (item.mReferences.value != 0)
            continue;

        if (index == -1 || item.mFeatures.count() > featureSize) {
            featureSize = item.mFeatures.count();
            index = i;
        }
    }

    if (index != -1) {
        DataItem& item = mDataItems[index];
        // Output Buffer
        item.markReference(NID_ROOT);
        item.mSizeId = SID_ARBITRARY;
        item.mBufferId = bufId;
    }
}

MUINT8 CaptureFeatureInferenceData::addDataItem(
        NodeID_T nodeId, TypeID_T typeId, BufferID_T bufId, BitSet64 features)
{
    DataItem& item = mDataItems[mDataCount];
    item.mNodeId = nodeId;
    item.mTypeId = typeId;
    item.mFeatures.value |= features.value;
    item.mBufferId = bufId;

    mInferredType.markBit(typeId);
    // update the latest reference
    mInferredItems[typeId] = mDataCount;
    return mDataCount++;
}


MVOID CaptureFeatureInferenceData::dump()
{
    String8 strFeature;
    String8 strReference;
    String8 strInput;
    String8 strOutput;
    for (int i = 0; i < mDataCount; i++)
    {
        DataItem& item = mDataItems[i];

#ifndef __DEBUG
        if (item.mReferences.isEmpty())
            continue;
#endif
        for (NodeID_T nodeId = 0; nodeId < NUM_OF_NODE; nodeId++) {
            if (item.mReferences.hasBit(nodeId)) {
                if (strReference.length() > 0) {
                    strReference += ",";
                }
                strReference += NodeID2Name(nodeId);
            }
        }

        for (FeatureID_T featId = 0; featId < NUM_OF_FEATURE; featId++) {
            if (item.mFeatures.hasBit(featId)) {
                if (strFeature.length() > 0) {
                    strFeature += ",";
                }
                strFeature += FeatID2Name(featId);
            }
        }

        MY_LOGD("item[%d] node:[%s] buffer:[%d] type:[%s] feature:[%s] referred:[%s] size:[%s%s] format:[%s]%s",
                i,
                NodeID2Name(item.mNodeId),
                item.mBufferId,
                TypeID2Name(item.mTypeId),
                strFeature.string(),
                strReference.string(),
                SizeID2Name(item.mSizeId),
                (item.mSize != MSize(0,0))
                    ? String8::format("(%dx%d)", item.mSize.w, item.mSize.h).string() : "",
                (item.mFormat)
                    ? queryImageFormatName(item.mFormat).c_str() : "",
                (item.mRedirect >= 0)
                    ? String8::format(" redirect:[%d]", item.mRedirect) : "");
        strFeature.clear();
        strReference.clear();
    }
#ifdef __DEBUG
    for (PathID_T pathId = 0; pathId < NUM_OF_PATH; pathId++) {
        if (mPathUsed.hasBit(pathId))
            MY_LOGD("path: %s", PathID2Name(pathId));
    }

    for (NodeID_T nodeId = NID_ROOT + 1; nodeId < NUM_OF_NODE; nodeId++) {
        if (!mNodeUsed.hasBit(nodeId))
            continue;

        strInput.clear();
        strOutput.clear();
        for (TypeID_T typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
            if (mNodeInput[nodeId][typeId] >= 0) {
                if (strInput.length() > 0) {
                    strInput += ",";
                }
                strInput += String8::format("%d", mNodeInput[nodeId][typeId]);
            }
        }

        for (TypeID_T typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
            if (mNodeOutput[nodeId][typeId] >= 0) {
                if (strOutput.length() > 0) {
                    strOutput += ",";
                }
                strOutput += String8::format("%d", mNodeOutput[nodeId][typeId]);
            }
        }

        MY_LOGD("node:[%s] input:[%s] output:[%s]",
                NodeID2Name(nodeId),
                strInput.string(),
                strOutput.string());
    }
#endif
}

MVOID CaptureFeatureInferenceData::determine(sp<CaptureFeatureRequest> pRequest)
{
    PathID_T pathId;
    NodeID_T nodeId;
    TypeID_T typeId;
    BitSet32 tmp;

    CaptureFeatureRequest& req = *(pRequest.get());

    for (size_t i = 0; i < mDataCount; i++) {
        DataItem& item = mDataItems[i];
        if (item.mReferences.isEmpty())
            continue;

        // node output
        mNodeOutput[item.mNodeId][item.mTypeId] = i;
        mNodeUsed.markBit(item.mNodeId);
        // working buffer id
        if (item.mBufferId == NULL_BUFFER) {
            // In-place processing
            if (item.mRedirect >= 0) {
                DataItem& redirect = mDataItems[item.mRedirect];
                item.mBufferId = redirect.mBufferId;
            } else {
                item.mBufferId = PIPE_BUFFER_STARTER | (mPipeBufferCounter++);
                req.addPipeBuffer(
                        item.mBufferId,
                        item.mTypeId,
                        item.mSize,
                        item.mFormat);
            }
        }

        // node input
        tmp = item.mReferences;
        do
        {
            nodeId = tmp.firstMarkedBit();
            tmp.clearFirstMarkedBit();

            mNodeInput[nodeId][item.mTypeId] = i;
            // there is no path for repeating request
            auto revertRepeatNode = [](NodeID_T nodeId) -> NodeID_T {
                if (nodeId == NID_YUV_R1 || nodeId == NID_YUV_R2)
                    return NID_YUV;
                if (nodeId == NID_YUV2_R1 || nodeId == NID_YUV2_R2)
                    return NID_YUV2;
                return nodeId;
            };
            NodeID_T itemFrom = revertRepeatNode(item.mNodeId);
            NodeID_T itemTo = revertRepeatNode(nodeId);
            pathId = FindPath(itemFrom, itemTo);
            if (pathId != NULL_PATH)
                mPathUsed.markBit(pathId);
        } while (!tmp.isEmpty());
    }

    for (pathId = 0; pathId < NUM_OF_PATH; pathId++) {
        if (mPathUsed.hasBit(pathId)) {
            const NodeID_T* pNodeId = GetPath(pathId);
            NodeID_T src = pNodeId[0];
            NodeID_T dst = pNodeId[1];
            if (mNodeUsed.hasBit(src) && mNodeUsed.hasBit(dst))
                req.addPath(pathId);
            else
                mPathUsed.clearBit(pathId);
        }
    }

    Vector<BufferID_T> vInBufIDs;
    Vector<BufferID_T> vOutBufIDs;
    int index;
    for (nodeId = NID_ROOT + 1; nodeId < NUM_OF_NODE; nodeId++) {
        if (!mNodeUsed.hasBit(nodeId))
            continue;

        vInBufIDs.clear();
        vOutBufIDs.clear();

        for (typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
            index = mNodeInput[nodeId][typeId];
            if (index >= 0) {
                DataItem& item = mDataItems[index];
                vInBufIDs.push_back(item.mBufferId);
            }
        }

        for (typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
            index = mNodeOutput[nodeId][typeId];
            if (index >= 0) {
                DataItem& item = mDataItems[index];
                // check the buffer which is referred by a involved node
                if (item.mReferences.value & mNodeUsed.value)
                    vOutBufIDs.push_back(item.mBufferId);
            }
        }
        req.addNodeIO(nodeId, vInBufIDs, vOutBufIDs, mNodeMeta[nodeId]);
    }
}


CaptureFeatureInference::CaptureFeatureInference()
{
}

MVOID CaptureFeatureInference::addNode(NodeID_T nodeId, CaptureFeatureNode* pNode)
{
    mNodeMap.add(nodeId, pNode);
}

MERROR CaptureFeatureInference::evaluate(sp<CaptureFeatureRequest> pRequest)
{
    Timer timer;
    timer.start();

    CaptureFeatureRequest &rRequest = *(pRequest.get());

    auto GetMetadataPtr = [&](MetadataID_T metaId) -> shared_ptr<IMetadata>
    {
        auto pHandle = rRequest.getMetadata(metaId);
        if (pHandle == NULL)
            return NULL;

        IMetadata* pMetatada = pHandle->native();
        if (pMetatada == NULL)
            return NULL;

        return std::make_shared<IMetadata>(*pMetatada);
    };

    CaptureFeatureInferenceData data;
    data.mpIMetadataHal = GetMetadataPtr(MID_MAN_IN_HAL);
    data.mpIMetadataApp = GetMetadataPtr(MID_MAN_IN_APP);
    data.mpIMetadataDynamic = GetMetadataPtr(MID_MAN_IN_P1_DYNAMIC);

    data.mFeatures = rRequest.mFeatures;
    if (pRequest->hasParameter(PID_FRAME_INDEX))
        data.mRequestIndex = pRequest->getParameter(PID_FRAME_INDEX);
    if (pRequest->hasParameter(PID_FRAME_COUNT))
        data.mRequestCount = pRequest->getParameter(PID_FRAME_COUNT);

    auto addSource = [&](BufferID_T bufId, TypeID_T typeId)
    {
        if (rRequest.mBufferMap.indexOfKey(bufId) >= 0) {
            auto pBufHandle = rRequest.getBuffer(bufId);
            IImageBuffer* pImgBuf = pBufHandle->native();
            data.addSource(typeId, bufId,
                pImgBuf->getImgFormat(),
                pImgBuf->getImgSize());
        }
    };

    auto addTarget = [&](BufferID_T bufId, TypeID_T typeId)
    {
        if (rRequest.mBufferMap.indexOfKey(bufId) >= 0)
            data.addTarget(typeId, bufId);
    };

    // 1. add all given input buffers
    addSource(BID_MAN_IN_YUV,  TID_MAN_FULL_YUV);
    addSource(BID_MAN_IN_FULL, TID_MAN_FULL_RAW);
    addSource(BID_MAN_IN_RSZ,  TID_MAN_RSZ_RAW);
    addSource(BID_MAN_IN_LCS,  TID_MAN_LCS);
    addSource(BID_SUB_IN_FULL, TID_SUB_FULL_RAW);
    addSource(BID_SUB_IN_RSZ,  TID_SUB_RSZ_RAW);
    addSource(BID_SUB_IN_LCS,  TID_SUB_LCS);

    // 2. inference all possible outputs
    //    - Root node do not involve to inference. It has no specific node IO.
    for (NodeID_T nodeId = NID_ROOT + 1; nodeId < NUM_OF_NODE ; nodeId++) {
        if (mNodeMap.indexOfKey(nodeId) >= 0) {
            mNodeMap.valueFor(nodeId)->evaluate(nodeId, data);
        }
    }
    // 2-1. Refine the request's feature, probably be ignored by plugin negotiation
    rRequest.mFeatures = data.mFeatures;

    // 2-2. add face data type
    // note: when flag eFD_Current exist, there is FDNode inference result, and no need cache FD
    const MBOOL isNeededCachedFD = (!data.mFaceDateType.hasBit(eFD_Current) && data.mFaceDateType.hasBit(eFD_Cache));
    pRequest->addParameter(PID_FD_CACHED_DATA, isNeededCachedFD);

    // 2-3. boost
    const MBOOL isBoot = ((data.mBoostType & eBoost_CPU) != 0);
    if ( isBoot ) {
        std::stringstream ss;
        ss << "req#:" << pRequest->getRequestNo();
        pRequest->setBooster(IBooster::createInstance(ss.str()));
    }

    if (!data.mThumbnailTiming.isEmpty())
        pRequest->addParameter(PID_THUMBNAIL_TIMING, data.mThumbnailTiming.lastMarkedBit());

    // 3. add output buffers
    addTarget(BID_MAN_OUT_JPEG,      TID_JPEG);
    addTarget(BID_MAN_OUT_THUMBNAIL, TID_THUMBNAIL);
    addTarget(BID_MAN_OUT_POSTVIEW,  TID_POSTVIEW);
    addTarget(BID_MAN_OUT_YUV00,     TID_MAN_CROP1_YUV);
    addTarget(BID_MAN_OUT_YUV01,     TID_MAN_CROP2_YUV);

    // 4. determin final pathes, which contain all node's input and output
    data.determine(pRequest);

    timer.stop();
    MY_LOGI("timeconsuming: %d ms", timer.getElapsed());

    data.dump();

    return OK;
}



} // NSCapture
} // NSFeaturePipe
} // NSCamFeature
} // NSCam
