/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "CaptureFeatureRequest.h"
#include "CaptureFeature_Common.h"

#include <utility>
#include <algorithm>

#define PIPE_CLASS_TAG "Request"
#define PIPE_TRACE TRACE_CAPTURE_FEATURE_DATA
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam3/feature/DualCam/DualCam.Common.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

CaptureFeatureRequest::CaptureFeatureRequest()
{
    clear();
}

CaptureFeatureRequest::~CaptureFeatureRequest()
{
}

MVOID CaptureFeatureRequest::addBuffer(BufferID_T bufId, sp<BufferHandle> pBufHandle)
{
    mBufferMap.add(bufId, pBufHandle);
    TypeID_T typeId = NULL_TYPE;
    MBOOL bCrossInput = MFALSE;
    switch (bufId) {
    case BID_MAN_IN_FULL:
       typeId = TID_MAN_FULL_RAW;
       bCrossInput = MTRUE;
       break;
    case BID_MAN_IN_YUV:
       typeId = TID_MAN_FULL_YUV;
       bCrossInput = MTRUE;
       break;
    case BID_MAN_IN_LCS:
       typeId = TID_MAN_LCS;
       bCrossInput = MTRUE;
       break;
    case BID_MAN_IN_RSZ:
       typeId = TID_MAN_RSZ_RAW;
       bCrossInput = MTRUE;
       break;
    case BID_MAN_OUT_YUV00:
       typeId = TID_MAN_CROP1_YUV;
       break;
    case BID_MAN_OUT_YUV01:
       typeId = TID_MAN_CROP2_YUV;
       break;
    case BID_MAN_OUT_JPEG:
       typeId = TID_JPEG;
       break;
    case BID_MAN_OUT_THUMBNAIL:
       typeId = TID_THUMBNAIL;
       break;
    case BID_MAN_OUT_POSTVIEW:
       typeId = TID_POSTVIEW;
       break;
    case BID_MAN_OUT_DEPTH:
       typeId = TID_MAN_DEPTH;
       break;
    case BID_MAN_OUT_CLEAN:
       //TODO
       break;
    case BID_SUB_IN_FULL:
       typeId = TID_SUB_FULL_RAW;
       bCrossInput = MTRUE;
       break;
    case BID_SUB_IN_LCS:
       typeId = TID_SUB_LCS;
       bCrossInput = MTRUE;
       break;
    case BID_SUB_IN_RSZ:
       typeId = TID_SUB_RSZ_RAW;
       bCrossInput = MTRUE;
       break;
    case BID_SUB_OUT_YUV00:
       //TODO
       break;
    case BID_SUB_OUT_YUV01:
       //TODO
       break;
    default:
       MY_LOGE("unknown buffer id: %d", bufId);
    }


    BufferItem item;
    item.mTypeId = typeId;
    item.mCreated = MTRUE;
    item.mCrossInput = bCrossInput;
    mBufferItems.add(bufId, item);
}

sp<BufferHandle> CaptureFeatureRequest::getBuffer(BufferID_T bufId)
{
    sp<BufferHandle> pBuffer;

    // get input buffers from cross request
    auto spCrossRequest = mpCrossRequest.promote();
    if (spCrossRequest != NULL) {
        Mutex::Autolock lock(spCrossRequest->mBufferMutex);
        if (spCrossRequest->mBufferMap.indexOfKey(bufId) >= 0
         && spCrossRequest->mBufferItems.valueFor(bufId).mCrossInput) {

            pBuffer = spCrossRequest->mBufferMap.editValueFor(bufId);

            if (!spCrossRequest->mBufferItems.valueFor(bufId).mAcquired) {

                pBuffer->acquire();
                spCrossRequest->mBufferItems.editValueFor(bufId).mAcquired = MTRUE;
            }
//            MY_LOGD("get buffer(bufId %d), cross frameNo %d", bufId, spCrossRequest->getFrameNo());
            return pBuffer;
        }
    }
    {
        Mutex::Autolock lock(mBufferMutex);
        if (mBufferMap.indexOfKey(bufId) < 0)
            return NULL;

        pBuffer = mBufferMap.editValueFor(bufId);

        if (!mBufferItems.valueFor(bufId).mAcquired) {
            pBuffer->acquire();
            mBufferItems.editValueFor(bufId).mAcquired = MTRUE;
        }
//        MY_LOGD("get buffer(bufId %d), frameNo %d", bufId, getFrameNo());
    }

    return pBuffer;
}

MVOID CaptureFeatureRequest::setCrossRequest(sp<CaptureFeatureRequest> pRequest)
{
  mpCrossRequest = pRequest;
}

sp<CaptureFeatureRequest> CaptureFeatureRequest::getCrossRequest()
{
  return mpCrossRequest.promote();
}

MVOID CaptureFeatureRequest::addPipeBuffer(BufferID_T bufId, TypeID_T typeId, MSize& size, Format_T fmt)
{
    BufferItem item;
    item.mTypeId = typeId;
    item.mCreated = MFALSE;
    item.mSize = size;
    item.mFormat = fmt;
    mBufferItems.add(bufId, item);
}

MVOID CaptureFeatureRequest::addMetadata(MetadataID_T metaId, sp<MetadataHandle> pMetaHandle)
{
    MetadataItem item;
    MBOOL bCrossInput = MFALSE;
    switch (metaId) {
    case MID_MAN_IN_P1_DYNAMIC:
    case MID_MAN_IN_APP:
    case MID_MAN_IN_HAL:
    case MID_SUB_IN_P1_DYNAMIC:
    case MID_SUB_IN_HAL:
        bCrossInput = MTRUE;
        break;

    case MID_MAN_OUT_APP:
    case MID_MAN_OUT_HAL:
    case MID_SUB_OUT_APP:
    case MID_SUB_OUT_HAL:
        break;

    default:
       MY_LOGE("unknown metadata id: %d", metaId);
    }

    item.mpHandle = pMetaHandle;
    item.mCrossInput = bCrossInput;
    mMetadataItems.add(metaId, item);
}


sp<MetadataHandle> CaptureFeatureRequest::getMetadata(MetadataID_T metaId)
{
    sp<MetadataHandle> pMetadata;

    // get input buffers from cross request
    auto spCrossRequest = mpCrossRequest.promote();
    if (spCrossRequest != NULL) {
        Mutex::Autolock lock(spCrossRequest->mMetadataMutex);
        if (spCrossRequest->mMetadataItems.indexOfKey(metaId) >= 0
         && spCrossRequest->mMetadataItems.valueFor(metaId).mCrossInput) {

            pMetadata = spCrossRequest->mMetadataItems.editValueFor(metaId).mpHandle;

            if (!spCrossRequest->mMetadataItems.valueFor(metaId).mAcquired) {

                pMetadata->acquire();
                spCrossRequest->mMetadataItems.editValueFor(metaId).mAcquired = MTRUE;
            }

//            MY_LOGD("get metadata(metaId %d), cross frameNo %d", metaId, spCrossRequest->getFrameNo());

            return pMetadata;
        }
    }

    {
        Mutex::Autolock lock(mMetadataMutex);
        if (mMetadataItems.indexOfKey(metaId) < 0)
            return NULL;

        pMetadata = mMetadataItems.editValueFor(metaId).mpHandle;

        if (!mMetadataItems.valueFor(metaId).mAcquired) {
            pMetadata->acquire();
            mMetadataItems.editValueFor(metaId).mAcquired = MTRUE;
        }
//        MY_LOGD("get metadata(metaId %d), frameNo %d", metaId, getFrameNo());
    }
    return pMetadata;
}

MVOID CaptureFeatureRequest::addParameter(ParameterID_T paramId, MINT32 value)
{
    mParameter[paramId] = value;
}


MINT32 CaptureFeatureRequest::getParameter(ParameterID_T paramId)
{
    return mParameter[paramId];
}

MBOOL CaptureFeatureRequest::hasParameter(ParameterID_T paramId)
{
    return mParameter[paramId] >= 0;
}

MINT32 CaptureFeatureRequest::getRequestNo()
{
    return mParameter[PID_REQUEST_NUM];
}

MINT32 CaptureFeatureRequest::getFrameNo()
{
    return mParameter[PID_FRAME_NUM];
}

MVOID CaptureFeatureRequest::addPath(PathID_T pathId)
{
    const NodeID_T* nodeId =GetPath(pathId);
    NodeID_T src = nodeId[0];
    NodeID_T dst = nodeId[1];

    mNodePath[src][dst] = pathId;

    mTraverse.markBit(pathId);
}

MVOID CaptureFeatureRequest::traverse(PathID_T pathId)
{
    mTraverse.clearBit(pathId);
}

MBOOL CaptureFeatureRequest::isTraversed()
{
    return mTraverse.isEmpty();
}

MBOOL CaptureFeatureRequest::isSatisfied(NodeID_T nodeId)
{
    for (NodeID_T i = 0; i < NUM_OF_NODE; i++) {
        if (mNodePath[i][nodeId] == NULL_PATH)
            continue;

        PathID_T pathId = FindPath(i, nodeId);
        if (pathId == NULL_PATH)
            continue;

        if (mTraverse.hasBit(pathId))
            return MFALSE;
    }

    return MTRUE;
}

MBOOL CaptureFeatureRequest::isValid()
{
    return !hasParameter(PID_ABORTED) && !hasParameter(PID_FAILURE);
}

Vector<NodeID_T> CaptureFeatureRequest::getPreviousNodes(NodeID_T nodeId)
{
    Vector<NodeID_T> vec;
    for (int i = 0; i < NUM_OF_NODE; i++) {
        if (mNodePath[i][nodeId] != NULL_PATH) {
            vec.push_back(i);
        }
    }
    return vec;
}

Vector<NodeID_T> CaptureFeatureRequest::getNextNodes(NodeID_T nodeId)
{
    Vector<NodeID_T> vec;
    for (int i = 0; i < NUM_OF_NODE; i++) {
        if (mNodePath[nodeId][i] != NULL_PATH) {
            vec.push_back(i);
        }
    }
    return vec;
}

MVOID CaptureFeatureRequest::addFeature(FeatureID_T fid)
{
    mFeatures.markBit(fid);
}

MVOID CaptureFeatureRequest::setFeatures(MUINT64 features)
{
    mFeatures.value = features;
}

MBOOL CaptureFeatureRequest::hasFeature(FeatureID_T fid)
{
    return mFeatures.hasBit(fid);
}

MVOID CaptureFeatureRequest::clear()
{
    memset(mNodePath, NULL_PATH, sizeof(mNodePath));
    std::fill_n(mParameter, NUM_OF_TOTAL_PARAMETER, -1);
}

MVOID CaptureFeatureRequest::addNodeIO(
        NodeID_T nodeId, Vector<BufferID_T>& vInBufId, Vector<BufferID_T>& vOutBufId, Vector<MetadataID_T>& vMetaId)
{
    sp<CaptureFeatureNodeRequest> pNodeRequest = new CaptureFeatureNodeRequest(this);

    for (BufferID_T bufId : vInBufId) {
        if (mBufferItems.indexOfKey(bufId) < 0) {
            MY_LOGE("can not find buffer, id:%d", bufId);
            continue;
        }

        TypeID_T typeId = mBufferItems.valueFor(bufId).mTypeId;

        pNodeRequest->mITypeMap.add(typeId, bufId);
        mBufferItems.editValueFor(bufId).mReference++;
    }

    for (BufferID_T bufId : vOutBufId) {
        if (mBufferItems.indexOfKey(bufId) < 0) {
            MY_LOGE("can not find buffer, id:%d", bufId);
            continue;
        }

        TypeID_T typeId = mBufferItems.valueFor(bufId).mTypeId;

        pNodeRequest->mOTypeMap.add(typeId, bufId);
        mBufferItems.editValueFor(bufId).mReference++;
    }

    for (MetadataID_T metaId : vMetaId) {
        if (mMetadataItems.indexOfKey(metaId) < 0)
            continue;

        pNodeRequest->mMetadataSet.markBit(metaId);
        mMetadataItems.editValueFor(metaId).mReference++;
    }

    mNodeRequest.add(nodeId, pNodeRequest);

}

MVOID CaptureFeatureRequest::decBufferRef(BufferID_T bufId)
{
    auto spCrossRequest = mpCrossRequest.promote();
    if (spCrossRequest != NULL) {
        Mutex::Autolock lock(spCrossRequest->mBufferMutex);
        if (spCrossRequest->mBufferMap.indexOfKey(bufId) >= 0
         && spCrossRequest->mBufferItems.valueFor(bufId).mCrossInput) {

            Mutex::Autolock lock(mBufferMutex);
            if (mBufferItems.indexOfKey(bufId) < 0) {
                MY_LOGE("NO bufId(%d)", bufId);
                return;
            }
            // use mReference of self request
            MUINT32& ref =  mBufferItems.editValueFor(bufId).mReference;

            if (--ref <= 0) {
                sp<BufferHandle> pBuffer = spCrossRequest->mBufferMap.editValueFor(bufId);
//                MY_LOGD("release buffer(bufId %d), cross frameNo %d", bufId, spCrossRequest->getFrameNo());
                pBuffer->release();
                spCrossRequest->mBufferMap.removeItem(bufId);
            }
            return;
        }
    }

    {
        Mutex::Autolock lock(mBufferMutex);
        if (mBufferItems.indexOfKey(bufId) >= 0) {
            MUINT32& ref =  mBufferItems.editValueFor(bufId).mReference;

            if (--ref <= 0) {
                sp<BufferHandle> pBuffer = mBufferMap.editValueFor(bufId);
//                MY_LOGD("release buffer(bufId %d), frameNo %d", bufId, getFrameNo());
                pBuffer->release();
                mBufferMap.removeItem(bufId);
            }
        }
    }
}

MVOID CaptureFeatureRequest::decMetadataRef(MetadataID_T metaId)
{
    auto spCrossRequest = mpCrossRequest.promote();
    if (spCrossRequest != NULL) {
        Mutex::Autolock lock(spCrossRequest->mMetadataMutex);
        if (spCrossRequest->mMetadataItems.indexOfKey(metaId) >= 0
         && spCrossRequest->mMetadataItems.valueFor(metaId).mCrossInput) {

            if (mMetadataItems.indexOfKey(metaId) < 0) {
                MY_LOGE("NO metaId(%d)", metaId);
                return;
            }
            // use mReference of self request
            MUINT32& ref =  mMetadataItems.editValueFor(metaId).mReference;

            if (--ref <= 0) {
                auto& item = spCrossRequest->mMetadataItems.editValueFor(metaId);
                sp<MetadataHandle> pHandle = item.mpHandle;
//                MY_LOGD("release metadata(metaId %d), cross frameNo %d", metaId, spCrossRequest->getFrameNo());
                pHandle->release();
                item.mpHandle = NULL;
            }
            return;
        }
    }
    {
        Mutex::Autolock lock(mMetadataMutex);
        if (mMetadataItems.indexOfKey(metaId) >= 0) {
            MUINT32& ref =  mMetadataItems.editValueFor(metaId).mReference;

            if (--ref <= 0) {
                auto& item = mMetadataItems.editValueFor(metaId);
                sp<MetadataHandle> pHandle = item.mpHandle;
//                MY_LOGD("release metadata(metaId %d), frameNo %d", metaId, getFrameNo());
                pHandle->release();
                item.mpHandle = NULL;
            }
        }
    }
}

sp<CaptureFeatureNodeRequest> CaptureFeatureRequest::getNodeRequest(NodeID_T nodeId)
{
    if (mNodeRequest.indexOfKey(nodeId) >= 0)
        return mNodeRequest.valueFor(nodeId);

    return NULL;
}

MVOID CaptureFeatureRequest::decNodeReference(NodeID_T nodeId)
{
    if (mNodeRequest.indexOfKey(nodeId) < 0)
        return;
    sp<CaptureFeatureNodeRequest> pNodeReq = mNodeRequest.valueFor(nodeId);
    auto& rITypeMap = pNodeReq->mITypeMap;
    auto& rOTypeMap = pNodeReq->mOTypeMap;
    auto& rITypeReleased = pNodeReq->mITypeReleased;
    auto& rOTypeReleased = pNodeReq->mOTypeReleased;

    for (size_t i = 0; i < rITypeMap.size(); i++) {
        TypeID_T typeId = rITypeMap.keyAt(i);
        BufferID_T bufId = rITypeMap.valueAt(i);
        if (!rITypeReleased.hasBit(typeId)) {
            rITypeReleased.markBit(typeId);
            decBufferRef(bufId);
        }
    }

    for (size_t i = 0; i < rOTypeMap.size(); i++) {
        BufferID_T bufId = rOTypeMap.valueAt(i);
        TypeID_T typeId = rOTypeMap.keyAt(i);
        if (!rOTypeReleased.hasBit(typeId)) {
            rOTypeReleased.markBit(typeId);
            decBufferRef(bufId);
        }
    }

    for (MetadataID_T i = 0; i < NUM_OF_METADATA; i++) {
        if (pNodeReq->hasMetadata(i))
            decMetadataRef(i);
    }
    return;
}

MVOID CaptureFeatureRequest::dump()
{
    android::LogPrinter logPrinter(LOG_TAG, ANDROID_LOG_DEBUG, "RequestDump");
    dump(logPrinter);
}

MVOID CaptureFeatureRequest::dump(android::Printer& printer)
{
    if (hasParameter(PID_FRAME_INDEX) && hasParameter(PID_FRAME_COUNT)) {
        printer.printFormatLine("  [R/F Num: %d/%d Multi-Frame Index:%d Count:%d]",
                getRequestNo(),
                getFrameNo(),
                getParameter(PID_FRAME_INDEX),
                getParameter(PID_FRAME_COUNT));
    } else {
        printer.printFormatLine("  [R/F Num: %d/%d]", getRequestNo(), getFrameNo());
    }

    for (size_t i = 0; i < mNodeRequest.size(); i++)
    {
        NodeID_T nodeId = mNodeRequest.keyAt(i);
        const sp<CaptureFeatureNodeRequest> pNodeReq = mNodeRequest.valueAt(i);

        printer.printFormatLine("      NODE [%s]", NodeID2Name(nodeId));
        auto& rITypeMap = pNodeReq->mITypeMap;
        for (size_t j = 0; j < rITypeMap.size(); j++) {

            TypeID_T typeId = rITypeMap.keyAt(j);
            BufferID_T bufId = rITypeMap.valueAt(j);
            const sp<BufferHandle> pBuffer = this->getBuffer(bufId);

            if (pBuffer == NULL) {
                printer.printFormatLine("          INPUT-%zu   %-14s Released",
                        j, TypeID2Name(typeId));
            } else {
                printer.printFormatLine("          INPUT-%zu   %-14s Handle:0x%p",
                        j, TypeID2Name(typeId), pBuffer.get());
            }
        }

        auto& rOTypeMap = pNodeReq->mOTypeMap;
        for (size_t j = 0; j < rOTypeMap.size(); j++) {

            TypeID_T typeId = rOTypeMap.keyAt(j);
            BufferID_T bufId = rOTypeMap.valueAt(j);
            const sp<BufferHandle> pBuffer = this->getBuffer(bufId);
            if (pBuffer == NULL) {
                printer.printFormatLine("          OUTPUT-%zu  %-14s Released",
                        j, TypeID2Name(typeId));
            } else {
                printer.printFormatLine("          OUTPUT-%zu  %-14s Handle:0x%p",
                        j, TypeID2Name(typeId), pBuffer.get());
            }
        }

    }
}

BufferID_T CaptureFeatureNodeRequest::mapBufferID(TypeID_T typeId, Direction dir)
{
    BufferID_T bufId = NULL_BUFFER;
    if (dir == INPUT) {
        if (mITypeMap.indexOfKey(typeId) >= 0) {
            bufId = mITypeMap.valueFor(typeId);
            mIBufferMap.add(bufId, typeId);
        }
    } else {
        if (mOTypeMap.indexOfKey(typeId) >= 0) {
            bufId = mOTypeMap.valueFor(typeId);
            mOBufferMap.add(bufId, typeId);
        }
    }

    return bufId;
}

MBOOL CaptureFeatureNodeRequest::hasMetadata(MetadataID_T metaId)
{
    return mMetadataSet.hasBit(metaId);
}

IImageBuffer* CaptureFeatureNodeRequest::acquireBuffer(BufferID_T bufId)
{
    if (bufId == NULL_BUFFER)
        return NULL;

    if (mIBufferMap.indexOfKey(bufId) >= 0)
    {
        TypeID_T typeId = mIBufferMap.valueFor(bufId);
        if (mITypeAcquired.hasBit(typeId))
        {
            MY_LOGW("Input:%s already acquired", TypeID2Name(typeId));
        }
        mITypeAcquired.markBit(typeId);

    }
    else if (mOBufferMap.indexOfKey(bufId) >= 0)
    {
        TypeID_T typeId = mOBufferMap.valueFor(bufId);
        if (mOTypeAcquired.hasBit(typeId))
        {
            MY_LOGW("Output:%s already acquired", TypeID2Name(typeId));
        }
        mOTypeAcquired.markBit(typeId);
    }

    auto pBufferHandle = mpRequest->getBuffer(bufId);
    if (pBufferHandle == NULL)
        return NULL;

    return pBufferHandle->native();
}

MVOID CaptureFeatureNodeRequest::releaseBuffer(BufferID_T bufId)
{
    if (mIBufferMap.indexOfKey(bufId) >= 0)
    {
        TypeID_T typeId = mIBufferMap.valueFor(bufId);
        mITypeReleased.markBit(typeId);
    }
    else if (mOBufferMap.indexOfKey(bufId) >= 0)
    {
        TypeID_T typeId = mOBufferMap.valueFor(bufId);
        mOTypeReleased.markBit(typeId);
    }

    mpRequest->decBufferRef(bufId);
}

IMetadata* CaptureFeatureNodeRequest::acquireMetadata(MetadataID_T metaId)
{
    if (metaId == NULL_METADATA)
        return NULL;

    if (!hasMetadata(metaId))
        return NULL;

    sp<MetadataHandle> pMetaHandle = mpRequest->getMetadata(metaId);
    if (pMetaHandle == NULL)
        return NULL;

    return pMetaHandle->native();
}

MVOID CaptureFeatureNodeRequest::releaseMetadata(MetadataID_T metaId)
{
    if (!hasMetadata(metaId))
        return;

    mpRequest->decMetadataRef(metaId);
}

MUINT32 CaptureFeatureNodeRequest::getImageTransform(BufferID_T bufId) const
{
    auto pBuffer = mpRequest->getBuffer(bufId);
    if (pBuffer == NULL)
        return 0;

    return pBuffer->getTransform();
}

MINT CaptureFeatureRequest::getImageFormat(BufferID_T bufId)
{
    if (mBufferItems.indexOfKey(bufId) < 0) {
        MY_LOGE("can not find buffer ID:%d", bufId);
        return 0;
    }

    if (bufId & PIPE_BUFFER_STARTER) {
        return  mBufferItems.valueFor(bufId).mFormat;
    } else {
        auto pBufferHandle = this->getBuffer(bufId);
        if (pBufferHandle == NULL)
            return 0;
        return pBufferHandle->native()->getImgFormat();
    }
}

MSize CaptureFeatureRequest::getImageSize(BufferID_T bufId)
{
    if (mBufferItems.indexOfKey(bufId) < 0) {
        MY_LOGE("can not find buffer ID:%d", bufId);
        return MSize(0, 0);
    }

    if (bufId & PIPE_BUFFER_STARTER) {
        return mBufferItems.valueFor(bufId).mSize;
    } else {
        auto pBufferHandle = this->getBuffer(bufId);
        if (pBufferHandle == NULL)
            return MSize(0, 0);
        return pBufferHandle->native()->getImgSize();
    }
}

MVOID CaptureFeatureRequest::setBooster(IBoosterPtr boosterPtr)
{
    mBoosterPtr = std::move(boosterPtr);
}

MVOID CaptureFeatureRequest::enableBoost()
{
    if( mBoosterPtr != NULL)
        mBoosterPtr->enable();
}

MVOID CaptureFeatureRequest::disableBoost()
{
    if( mBoosterPtr != NULL)
        mBoosterPtr->disable();
}


} // NSCapture
} // NSFeaturePipe
} // NSCamFeature
} // NSCam
