/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_REQUEST_H_
#define _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_REQUEST_H_

#include "CaptureFeature_Common.h"

#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/KeyedVector.h>
#include <utils/BitSet.h>
#include <utils/Printer.h>

#include <mtkcam3/feature/featurePipe/ICaptureFeaturePipe.h>
#include <featurePipe/core/include/IIBuffer.h>

#include <featurePipe/core/include/WaitQueue.h>
#include "CaptureFeatureTimer.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

enum CaptureFeaturePathID {
    PID_ENQUE,
    PID_ROOT_TO_RAW,
    PID_ROOT_TO_P2A,
    PID_ROOT_TO_MULTIRAW,
    PID_RAW_TO_P2A,
    PID_MULTIRAW_TO_P2A,
    PID_P2A_TO_DEPTH,
    PID_P2A_TO_FUSION,
    PID_P2A_TO_MULTIYUV,
    PID_P2A_TO_YUV,
    PID_P2A_TO_YUV2,
    PID_P2A_TO_MDP,
    PID_P2A_TO_FD,
    PID_FD_TO_DEPTH,
    PID_FD_TO_FUSION,
    PID_FD_TO_MULTIFRAME,
    PID_FD_TO_YUV,
    PID_FD_TO_YUV2,
    PID_MULTIFRAME_TO_YUV,
    PID_MULTIFRAME_TO_YUV2,
    PID_MULTIFRAME_TO_BOKEH,
    PID_MULTIFRAME_TO_MDP,
    PID_FUSION_TO_YUV,
    PID_FUSION_TO_MDP,
    PID_DEPTH_TO_BOKEH,
    PID_YUV_TO_BOKEH,
    PID_YUV_TO_YUV2,
    PID_YUV_TO_MDP,
    PID_BOKEH_TO_YUV2,
    PID_BOKEH_TO_MDP,
    PID_YUV2_TO_MDP,
    PID_DEQUE,
    NUM_OF_PATH,
    NULL_PATH = 0xFF,
};

enum CaptureFeatureNodeID {
    NID_ROOT,
    NID_MULTIRAW,
    NID_RAW,
    NID_P2A,
    NID_FD,
    NID_MULTIYUV,
    NID_FUSION,
    NID_DEPTH,
    NID_YUV,
    NID_YUV_R1,         // For repeating
    NID_YUV_R2,         // For repeating
    NID_BOKEH,
    NID_YUV2,
    NID_YUV2_R1,        // For repeating
    NID_YUV2_R2,        // For repeating
    NID_MDP,
    NUM_OF_NODE,
    NULL_NODE = 0xFF,
};

enum CaptureFeatureBufferTypeID {
    TID_MAN_FULL_RAW,
    TID_MAN_FULL_YUV,
    TID_MAN_RSZ_RAW,
    TID_MAN_RSZ_YUV,
    TID_MAN_CROP1_YUV,  // Only for Output
    TID_MAN_CROP2_YUV,  // Only for Output
    TID_MAN_SPEC_YUV,
    TID_MAN_DEPTH,
    TID_MAN_LCS,
    TID_MAN_FD_YUV,
    TID_MAN_FD,         // Support per-frame computinf FD
    TID_SUB_FULL_RAW,
    TID_SUB_FULL_YUV,
    TID_SUB_RSZ_RAW,
    TID_SUB_RSZ_YUV,
    TID_SUB_LCS,
    //
    TID_POSTVIEW,
    TID_JPEG,
    TID_THUMBNAIL,
    NUM_OF_TYPE,
    NULL_TYPE = 0xFF,
};

// TODO: Should use EImageSize instead of SID
enum CaptureFeatureSizeID {
    SID_FULL,
    SID_RESIZED,
    SID_QUARTER,
    SID_SPECIFIED,
    SID_ARBITRARY,
    NUM_OF_SIZE,
    NULL_SIZE = 0xFF,
};

enum Direction {
    INPUT,
    OUTPUT,
};

typedef MUINT8 NodeID_T;
typedef MUINT8 TypeID_T;
typedef MUINT8 PathID_T;
typedef MUINT8 SizeID_T;
typedef MUINT32 Format_T;

#define PIPE_BUFFER_STARTER   (0x1 << 5)


class CaptureFeatureRequest;
class CaptureFeatureNodeRequest : public virtual android::RefBase
{
friend class CaptureFeatureRequest;
public:
    CaptureFeatureNodeRequest(CaptureFeatureRequest* pRequest)
        : mpRequest(pRequest)
    {}

    virtual BufferID_T              mapBufferID(TypeID_T, Direction);

    virtual MBOOL                   hasMetadata(MetadataID_T);

    //
    virtual IImageBuffer*           acquireBuffer(BufferID_T);
    virtual MVOID                   releaseBuffer(BufferID_T);
    virtual MUINT32                 getImageTransform(BufferID_T) const;
    virtual IMetadata*              acquireMetadata(MetadataID_T);
    virtual MVOID                   releaseMetadata(MetadataID_T);


    virtual ~CaptureFeatureNodeRequest() {}
private:

    CaptureFeatureRequest*              mpRequest;
    KeyedVector<TypeID_T, BufferID_T>   mITypeMap;
    KeyedVector<TypeID_T, BufferID_T>   mOTypeMap;
    KeyedVector<BufferID_T, TypeID_T>   mIBufferMap;
    KeyedVector<BufferID_T, TypeID_T>   mOBufferMap;
    BitSet32                            mITypeAcquired;
    BitSet32                            mOTypeAcquired;
    BitSet32                            mITypeReleased;
    BitSet32                            mOTypeReleased;
    BitSet32                            mMetadataSet;
};

enum CaptureFeaturePrivateParameter {
    PID_REQUEST_REPEAT      = NUM_OF_PARAMETER,
    PID_FD_CACHED_DATA,
    PID_MULTIFRAME_TYPE,
    PID_THUMBNAIL_TIMING,
    PID_ABORTED,
    PID_FAILURE,
    PID_FRAME_DROP_COUNT,
    PID_FRAME_DROP,
    NUM_OF_TOTAL_PARAMETER,
};

class CaptureFeatureRequest : public ICaptureFeatureRequest
{
friend class CaptureFeatureInference;
friend class CaptureFeatureInferenceData;
friend class CaptureFeaturePipe;

// Implementation of ICaptureFeatureRequest
public:
    CaptureFeatureRequest();

    virtual MVOID                   addBuffer(BufferID_T, sp<BufferHandle>);
    virtual MVOID                   addMetadata(MetadataID_T, sp<MetadataHandle>);

    // get the acquired buffer handle;
    virtual sp<BufferHandle>        getBuffer(BufferID_T);
    virtual sp<MetadataHandle>      getMetadata(MetadataID_T);

    virtual MVOID                   addFeature(FeatureID_T);
    virtual MBOOL                   hasFeature(FeatureID_T);
    virtual MVOID                   setFeatures(MUINT64);
    virtual MVOID                   addParameter(ParameterID_T, MINT32);

    virtual MINT32                  getParameter(ParameterID_T);
    virtual MBOOL                   hasParameter(ParameterID_T);
    virtual MINT32                  getRequestNo();
    virtual MINT32                  getFrameNo();

public:

    virtual MVOID                   setCrossRequest(sp<CaptureFeatureRequest> pRequest);
    virtual sp<CaptureFeatureRequest>
                                    getCrossRequest();

    virtual MVOID                   addPipeBuffer(BufferID_T, TypeID_T, MSize&, Format_T);
    virtual MVOID                   addPath(PathID_T);
    virtual MVOID                   traverse(PathID_T);
    virtual MBOOL                   isTraversed();
    virtual MBOOL                   isSatisfied(NodeID_T);
    virtual MBOOL                   isValid();
    virtual MVOID                   addNodeIO(NodeID_T, Vector<BufferID_T>&, Vector<BufferID_T>&, Vector<MetadataID_T>&);

    virtual MVOID                   decNodeReference(NodeID_T);
    virtual MVOID                   decBufferRef(BufferID_T);
    virtual MVOID                   decMetadataRef(MetadataID_T);
    virtual Vector<NodeID_T>        getPreviousNodes(NodeID_T);
    virtual Vector<NodeID_T>        getNextNodes(NodeID_T);

    virtual sp<CaptureFeatureNodeRequest>
                                    getNodeRequest(NodeID_T);
    virtual MVOID                   clear();
    virtual MVOID                   dump();
    virtual MVOID                   dump(android::Printer& printer);
    virtual                         ~CaptureFeatureRequest();

    // Query Buffer Info
    virtual MINT                    getImageFormat(BufferID_T);
    virtual MSize                   getImageSize(BufferID_T);
    CaptureFeatureTimer             mTimer;

    android::sp<RequestCallback>    mpCallback;
// boost operators
public:
    MVOID                           setBooster(IBoosterPtr boosterPtr);
    MVOID                           enableBoost();
    MVOID                           disableBoost();

private:

    struct BufferItem {
        BufferItem()
            : mAcquired(MFALSE)
            , mCreated(MFALSE)
            , mCrossInput(MFALSE)
            , mReference(0)
            , mTypeId(NULL_TYPE)
            , mSize(MSize(0, 0))
            , mFormat(0)
        { }

        MBOOL       mAcquired;
        MBOOL       mCreated;
        MBOOL       mCrossInput;
        MUINT32     mReference;
        TypeID_T    mTypeId;
        MSize       mSize;
        Format_T    mFormat;
    };


    struct MetadataItem {
        MetadataItem()
            : mAcquired(MFALSE)
            , mCrossInput(MFALSE)
            , mReference(0)
            , mpHandle(NULL)
        { }

        MBOOL       mAcquired;
        MBOOL       mCrossInput;
        MUINT32     mReference;
        sp<MetadataHandle>
                    mpHandle;
    };

private:
    wp<CaptureFeatureRequest>                   mpCrossRequest;
    KeyedVector<NodeID_T, sp<CaptureFeatureNodeRequest>>
                                                mNodeRequest;

    Mutex                                       mBufferMutex;
    KeyedVector<BufferID_T, BufferItem>         mBufferItems;
    KeyedVector<BufferID_T, sp<BufferHandle>>   mBufferMap;
    Mutex                                       mMetadataMutex;
    KeyedVector<MetadataID_T, MetadataItem>     mMetadataItems;
    android::BitSet64                           mFeatures;
    MINT32                                      mParameter[NUM_OF_TOTAL_PARAMETER];

    // The first index is source node ID, and the second index is destination node ID.
    // The value is path ID.
    MUINT8                                      mNodePath[NUM_OF_NODE][NUM_OF_NODE];

    // Record all pathes to traverse
    BitSet64                                    mTraverse;
    //
    IBoosterPtr                                 mBoosterPtr;

};

typedef android::sp<CaptureFeatureRequest> RequestPtr;

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_REQUEST_H_
