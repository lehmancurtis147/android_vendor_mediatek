/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <thread>
#include <chrono>
//
#include <utility>
#include "CaptureFeaturePipe.h"

#define PIPE_CLASS_TAG "Pipe"
#define PIPE_TRACE TRACE_CAPTURE_FEATURE_PIPE
#include <featurePipe/core/include/PipeLog.h>

#define NORMAL_STREAM_NAME "CaptureFeature"
#define THREAD_POSTFIX "@CapPipe"
#define APPEND_POSTFIX(name) (name THREAD_POSTFIX)
//
#define DUALCAM_NODE_THREAD_PRIORITY_OFFSET (-4)
//#define __DEBUG
using namespace NSCam::NSIoPipe::NSPostProc;
using namespace NSCam::NSPipelinePlugin;
using namespace android;


namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

namespace { // begin anonymous namespace

struct CaptureFeatureNodeConfig
{
    MINT32      mPolicy;
    MINT32      mPriorit;
};

using CaptureFeatureNodeConfigTable = std::map<NodeID_T, const CaptureFeatureNodeConfig>;
const CaptureFeatureNodeConfigTable gCaptureFeatureNodeConfigTable = { {NID_ROOT, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_RAW, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_P2A, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_FD, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_MULTIYUV, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_YUV, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_MDP, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_YUV2, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY}},
                                                                       {NID_DEPTH, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY + DUALCAM_NODE_THREAD_PRIORITY_OFFSET}},
                                                                       {NID_BOKEH, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY + DUALCAM_NODE_THREAD_PRIORITY_OFFSET}},
                                                                       {NID_FUSION, {SCHED_NORMAL, DEFAULT_CAMTHREAD_PRIORITY + DUALCAM_NODE_THREAD_PRIORITY_OFFSET}}};

inline CaptureFeatureNodeConfig getCaptureFeatureNodeConfig(NodeID_T nodeId)
{
    auto foundItem = gCaptureFeatureNodeConfigTable.find(nodeId);
    if(foundItem == gCaptureFeatureNodeConfigTable.end()) {
        MY_LOGW("failed to get node config and use defaule, nodeId:%d", nodeId);
        return { .mPolicy  = SCHED_NORMAL,
                 .mPriorit = DEFAULT_CAMTHREAD_PRIORITY };
    }
    return foundItem->second;
}

template<typename TCaptureNode, typename... Ts>
inline TCaptureNode* createCaptureFeatureNode(NodeID_T nodeId, const char* name, Ts&&... params)
{
    CaptureFeatureNodeConfig config = getCaptureFeatureNodeConfig(nodeId);
    return new TCaptureNode(nodeId, name, config.mPolicy, config.mPriorit, std::forward<Ts>(params)...);
}

template<typename TCaptureNode>
inline TCaptureNode* createCaptureFeatureNode(NodeID_T nodeId, const char* name)
{
    CaptureFeatureNodeConfig config = getCaptureFeatureNodeConfig(nodeId);
    return new TCaptureNode(nodeId, name, config.mPolicy, config.mPriorit);
}

} // end anonymous namespace


CaptureFeaturePipe::CaptureFeaturePipe(MINT32 sensorIndex, const UsageHint &usageHint, MINT32 sensorIndex2)
    : CamPipe<CaptureFeatureNode>("CaptureFeaturePipe")
    , mSensorIndex(sensorIndex)
    , mSensorIndex2(sensorIndex2)
{
    TRACE_FUNC_ENTER();

    mLogLevel           = ::property_get_int32("vendor.debug.camera.p2capture", 0);
    mDualCam            = mSensorIndex2 >= 0;
    mUsageHint          = usageHint;

    mpRootNode          = createCaptureFeatureNode<RootNode>(NID_ROOT, APPEND_POSTFIX("Root"));
    mpRAWNode           = createCaptureFeatureNode<RAWNode>(NID_RAW, APPEND_POSTFIX("Raw"));
    mpP2ANode           = createCaptureFeatureNode<P2ANode>(NID_P2A, APPEND_POSTFIX("P2A"));
    mpFDNode            = createCaptureFeatureNode<FDNode>(NID_FD, APPEND_POSTFIX("FD"));
    mpMultiFrameNode    = createCaptureFeatureNode<MultiFrameNode>(NID_MULTIYUV, APPEND_POSTFIX("MF"));
    mpYUVNode           = createCaptureFeatureNode<YUVNode>(NID_YUV, APPEND_POSTFIX("YUV"), mDualCam);
    mpMDPNode           = createCaptureFeatureNode<MDPNode>(NID_MDP, APPEND_POSTFIX("MDP"));

    // Dual
    if (mDualCam) {
        mpYUV2Node      = createCaptureFeatureNode<YUVNode>(NID_YUV2, APPEND_POSTFIX("YUV2"), mDualCam);
        mpDepthNode     = createCaptureFeatureNode<DepthNode>(NID_DEPTH, APPEND_POSTFIX("Depth"));
        mpBokehNode     = createCaptureFeatureNode<BokehNode>(NID_BOKEH, APPEND_POSTFIX("Bokeh"));
        mpFusionNode    = createCaptureFeatureNode<FusionNode>(NID_FUSION, APPEND_POSTFIX("Fusion"));
    } else {
        mpYUV2Node      = NULL;
        mpDepthNode     = NULL;
        mpBokehNode     = NULL;
        mpFusionNode    = NULL;
    }

    // Add node
    mpNodes.push_back(mpRootNode);
    mpNodes.push_back(mpRAWNode);
    mpNodes.push_back(mpP2ANode);
    mpNodes.push_back(mpFDNode);
    mpNodes.push_back(mpMultiFrameNode);
    mpNodes.push_back(mpYUVNode);
    mpNodes.push_back(mpMDPNode);

    if (mDualCam) {
        mpNodes.push_back(mpYUV2Node);
        mpNodes.push_back(mpFusionNode);
        mpNodes.push_back(mpDepthNode);
        mpNodes.push_back(mpBokehNode);
    }

    // Notice: the value of NodeID must follows the dependency of pipe flow
    mInference.addNode(NID_RAW, mpRAWNode);
    mInference.addNode(NID_MULTIRAW, mpMultiFrameNode);
    mInference.addNode(NID_P2A, mpP2ANode);
    mInference.addNode(NID_FD, mpFDNode);
    mInference.addNode(NID_MULTIYUV, mpMultiFrameNode);
    mInference.addNode(NID_YUV, mpYUVNode);
    mInference.addNode(NID_MDP, mpMDPNode);
    if (mDualCam) {
        mInference.addNode(NID_YUV2, mpYUV2Node);
        mInference.addNode(NID_FUSION, mpFusionNode);
        mInference.addNode(NID_DEPTH, mpDepthNode);
        mInference.addNode(NID_BOKEH, mpBokehNode);
    }

    mpCropCalculator = new CropCalculator(mSensorIndex, mLogLevel);

    mNodeSignal = new NodeSignal();
    if (mNodeSignal == NULL) {
        MY_LOGE("cannot create NodeSignal");
    }

    mDebuggee = std::make_shared<MyDebuggee>(this);
    if ( auto pDbgMgr = IDebuggeeManager::get() ) {
        mDebuggee->mCookie = pDbgMgr->attach(mDebuggee, 1);
    }

    TRACE_FUNC_EXIT();
}

CaptureFeaturePipe::~CaptureFeaturePipe()
{
    TRACE_FUNC_ENTER();

#define DELETE_NODE(node)       \
    do {                        \
        if (node == NULL)       \
            break;              \
        delete node;            \
        node = NULL;            \
    } while(0)

    DELETE_NODE(mpRootNode);
    DELETE_NODE(mpRAWNode);
    DELETE_NODE(mpP2ANode);
    DELETE_NODE(mpFDNode);
    DELETE_NODE(mpMultiFrameNode);
    DELETE_NODE(mpYUVNode);
    DELETE_NODE(mpMDPNode);
    if (mDualCam) {
        DELETE_NODE(mpYUV2Node);
        DELETE_NODE(mpDepthNode);
        DELETE_NODE(mpBokehNode);
        DELETE_NODE(mpFusionNode);
    }
#undef DELETE_NODE

    if ( mDebuggee != nullptr ) {
        if ( auto pDbgMgr = IDebuggeeManager::get() ) {
            pDbgMgr->detach(mDebuggee->mCookie);
        }
        mDebuggee = nullptr;
    }

    // must call dispose to free CamGraph
    this->dispose();
    MY_LOGD("destroy pipe(%p): sensorIndex=%d, sensorIndex2=%d",
        this, mSensorIndex, mSensorIndex2);
    TRACE_FUNC_EXIT();
}

void CaptureFeaturePipe::setSensorIndex(MINT32 sensorIndex, MINT32 sensorIndex2)
{
    TRACE_FUNC_ENTER();
    this->mSensorIndex = sensorIndex;
    this->mSensorIndex2 = sensorIndex2;
    TRACE_FUNC_EXIT();
}

MVOID CaptureFeaturePipe::init()
{
    TRACE_FUNC_ENTER();
    MBOOL ret;
    ret = PARENT_PIPE::init();
    TRACE_FUNC_EXIT();
}

MVOID CaptureFeaturePipe::uninit()
{
    TRACE_FUNC_ENTER();
    MBOOL ret;
    ret = PARENT_PIPE::uninit();
    TRACE_FUNC_EXIT();
}

MERROR CaptureFeaturePipe::enque(sp<ICaptureFeatureRequest> request)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if (request == NULL)
    {
        MY_LOGE("enque an empty request!");
        TRACE_FUNC_EXIT();
        return BAD_VALUE;
    }
    else
    {
        sp<CaptureFeatureRequest> pRequest = static_cast<CaptureFeatureRequest*>(request.get());
        // For the event of next capture
        pRequest->mpCallback = mpCallback;

        mInference.evaluate(pRequest);

        CaptureFeatureRequest& rRequest = *pRequest;

        for (size_t i = 0; i < rRequest.mBufferItems.size(); i++) {

            auto& key = rRequest.mBufferItems.keyAt(i);
            auto& item = rRequest.mBufferItems.editValueAt(i);

            if (!item.mCreated && !!item.mSize) {
                rRequest.mBufferMap.add(key,
                        new PipeBufferHandle(mpBufferPool,
                        item.mFormat,
                        item.mSize));
                item.mCreated = MTRUE;
            }
        }

        if (mLogLevel > 1)
            pRequest->dump();

        mNodeSignal->clearStatus(NodeSignal::STATUS_IN_FLUSH);
        {
            Mutex::Autolock _l(mRequestLock);
            mRunningRequests.push_back(pRequest);
        }
        pRequest->enableBoost();
        ret = CamPipe::enque(PID_ENQUE, pRequest);
    }
    TRACE_FUNC_EXIT();
    return ret ? OK : UNKNOWN_ERROR ;
}

MVOID CaptureFeaturePipe::abort(sp<ICaptureFeatureRequest> request)
{
    TRACE_FUNC_ENTER();
    if (request == NULL) {
        MY_LOGE("enque an empty request!");
    } else {
        sp<CaptureFeatureRequest> pRequest = static_cast<CaptureFeatureRequest*>(request.get());
        pRequest->addParameter(PID_ABORTED, 1);
        // Broadcast
        for (auto& pNode : mpNodes) {
            pNode->onAbort(pRequest);
        }
    }
    TRACE_FUNC_EXIT();
}

MBOOL CaptureFeaturePipe::flush()
{
    TRACE_FUNC_ENTER();
    MY_LOGD("Trigger flush");
    mNodeSignal->setStatus(NodeSignal::STATUS_IN_FLUSH);
    CamPipe::sync();
    mNodeSignal->clearStatus(NodeSignal::STATUS_IN_FLUSH);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL CaptureFeaturePipe::onInit()
{
    TRACE_FUNC_ENTER();
    MBOOL ret;
    ret = this->prepareNodeSetting() &&
          this->prepareNodeConnection() &&
          this->prepareBuffer();

    TRACE_FUNC_EXIT();
    return ret;
}

MVOID CaptureFeaturePipe::onUninit()
{
    TRACE_FUNC_ENTER();
    this->releaseBuffer();
    this->releaseNodeSetting();
    TRACE_FUNC_EXIT();
}

MBOOL CaptureFeaturePipe::onData(DataID id, RequestPtr& pRequest)
{
    TRACE_FUNC_ENTER();
    // NOTE: if this function caller is P2ANode, that will hang the P2 callback thread
    const MINT64 sleepTime = ::property_get_int32("vendor.debug.camera.p2capture.sleeptime", 0);
    if(sleepTime > 0)
    {
        auto tmp = std::chrono::milliseconds(sleepTime);
        MY_LOGD("sleep %lld ms +, R/F Num: %d/%d", tmp.count(), pRequest->getRequestNo(), pRequest->getFrameNo());
        std::this_thread::sleep_for(tmp);
        MY_LOGD("sleep %lld ms -, R/F Num: %d/%d", tmp.count(), pRequest->getRequestNo(), pRequest->getFrameNo());
    }
    //
    MY_LOGI("R/F Num: %d/%d - Finished Abort(%d)/Fail(%d)",
            pRequest->getRequestNo(),
            pRequest->getFrameNo(),
            pRequest->hasParameter(PID_ABORTED),
            pRequest->hasParameter(PID_FAILURE));
    {
        Mutex::Autolock _l(mRequestLock);

        auto it = mRunningRequests.begin();
        for (; it != mRunningRequests.end(); it++) {
            if (*it == pRequest) {
                mRunningRequests.erase(it);
                break;
            }
        }
    }
    pRequest->disableBoost();

    #define REQUEST_CALLBACK(pRequest, pCallback) \
        if (pRequest->hasParameter(PID_ABORTED)) \
            pCallback->onAborted(pRequest); \
        else if (pRequest->hasParameter(PID_FAILURE)) \
            pCallback->onCompleted(pRequest, UNKNOWN_ERROR); \
        else \
            pCallback->onCompleted(pRequest, OK);

    // check cross request
    sp<CaptureFeatureRequest> pCrossRequest = pRequest->getCrossRequest();
    if (pCrossRequest) {
        if (pCrossRequest == mpFinishCrossRequest) {  // callback cross requests at the same time
            MY_LOGD("Callback cross request: R/F Num: %d/%d - %d/%d",
            mpFinishCrossRequest->getRequestNo(),
            mpFinishCrossRequest->getFrameNo(),
            pRequest->getRequestNo(),
            pRequest->getFrameNo());
            REQUEST_CALLBACK(mpFinishCrossRequest, mpCallback);
            mpFinishCrossRequest = NULL;
            REQUEST_CALLBACK(pRequest, mpCallback);
        } else {  // keep first cross request
            mpFinishCrossRequest = pRequest;
            MY_LOGD("Keep cross request: R/F Num: %d/%d",
            mpFinishCrossRequest->getRequestNo(),
            mpFinishCrossRequest->getFrameNo());
        }
    } else {
        REQUEST_CALLBACK(pRequest, mpCallback);
    }
    #undef REQUEST_CALLBACK

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL CaptureFeaturePipe::prepareNodeSetting()
{
    TRACE_FUNC_ENTER();
    NODE_LIST::iterator it, end;
    for( it = mpNodes.begin(), end = mpNodes.end(); it != end; ++it )
    {
        (*it)->setSensorIndex(mSensorIndex, mSensorIndex2);
        (*it)->setNodeSignal(mNodeSignal);
        (*it)->setCropCalculator(mpCropCalculator);
        (*it)->setUsageHint(mUsageHint);
        if (mLogLevel > 0)
            (*it)->setLogLevel(mLogLevel);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL CaptureFeaturePipe::prepareNodeConnection()
{
    TRACE_FUNC_ENTER();

    // TODO: construct a data table to describe the connecting edges
#if 0
    for (size_t i = 0; i < NUM_OF_PATH; i++) {

    }
#endif
    this->connectData(PID_ROOT_TO_RAW, *mpRootNode, *mpRAWNode);
    this->connectData(PID_ROOT_TO_P2A, *mpRootNode, *mpP2ANode);
    this->connectData(PID_ROOT_TO_MULTIRAW, *mpRootNode, *mpMultiFrameNode);
    this->connectData(PID_RAW_TO_P2A, *mpRAWNode, *mpP2ANode);
    this->connectData(PID_MULTIRAW_TO_P2A, *mpMultiFrameNode, *mpP2ANode);
    this->connectData(PID_P2A_TO_MULTIYUV, *mpP2ANode, *mpMultiFrameNode);
    this->connectData(PID_P2A_TO_YUV, *mpP2ANode, *mpYUVNode);
    this->connectData(PID_FD_TO_MULTIFRAME, *mpFDNode, *mpMultiFrameNode);
    this->connectData(PID_FD_TO_YUV, *mpFDNode, *mpYUVNode);
    this->connectData(PID_P2A_TO_MDP, *mpP2ANode, *mpMDPNode);
    this->connectData(PID_P2A_TO_FD, *mpP2ANode, *mpFDNode);
    this->connectData(PID_MULTIFRAME_TO_YUV, *mpMultiFrameNode, *mpYUVNode);
    this->connectData(PID_MULTIFRAME_TO_MDP, *mpMultiFrameNode, *mpMDPNode);
    this->connectData(PID_YUV_TO_MDP, *mpYUVNode, *mpMDPNode);
    if (mDualCam) {
        this->connectData(PID_P2A_TO_DEPTH, *mpP2ANode, *mpDepthNode);
        this->connectData(PID_P2A_TO_FUSION, *mpP2ANode, *mpFusionNode);
        this->connectData(PID_P2A_TO_YUV2, *mpP2ANode, *mpYUV2Node);
        this->connectData(PID_FD_TO_DEPTH, *mpFDNode, *mpDepthNode);
        this->connectData(PID_FD_TO_FUSION, *mpFDNode, *mpFusionNode);
        this->connectData(PID_FD_TO_YUV2, *mpFDNode, *mpYUV2Node);
        this->connectData(PID_MULTIFRAME_TO_BOKEH, *mpMultiFrameNode, *mpBokehNode);
        this->connectData(PID_MULTIFRAME_TO_YUV2, *mpMultiFrameNode, *mpYUV2Node);
        this->connectData(PID_FUSION_TO_YUV, *mpFusionNode, *mpYUVNode);
        this->connectData(PID_FUSION_TO_MDP, *mpFusionNode, *mpMDPNode);
        this->connectData(PID_DEPTH_TO_BOKEH, *mpDepthNode, *mpBokehNode);
        this->connectData(PID_YUV_TO_BOKEH, *mpYUVNode, *mpBokehNode);
        this->connectData(PID_YUV_TO_YUV2, *mpYUVNode, *mpYUV2Node);
        this->connectData(PID_BOKEH_TO_YUV2, *mpBokehNode, *mpYUV2Node);
        this->connectData(PID_BOKEH_TO_MDP, *mpBokehNode, *mpMDPNode);
        this->connectData(PID_YUV2_TO_MDP, *mpYUV2Node, *mpMDPNode);
    }

    NODE_LIST::iterator it, end;
    for(it = mpNodes.begin(), end = mpNodes.end(); it != end; ++it) {
        this->connectData(PID_DEQUE, PID_DEQUE, **it, this);
    }

    this->setRootNode(mpRootNode);
    mpRootNode->registerInputDataID(PID_ENQUE);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL CaptureFeaturePipe::prepareBuffer()
{
    TRACE_FUNC_ENTER();
    mpBufferPool = new CaptureBufferPool("fpipe");
    mpBufferPool->init(Vector<BufferConfig>());

    mpP2ANode->setBufferPool(mpBufferPool);
    if (mDualCam) {
        mpDepthNode->setBufferPool(mpBufferPool);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

android::sp<IBufferPool> CaptureFeaturePipe::createFullImgPool(const char* name, MSize size)
{
    TRACE_FUNC_ENTER();

    android::sp<IBufferPool> fullImgPool;
    NSCam::EImageFormat format = eImgFmt_YV12;
    fullImgPool = ImageBufferPool::create(name, size.w, size.h, format, ImageBufferPool::USAGE_HW );

    TRACE_FUNC_EXIT();

    return fullImgPool;
}

MVOID CaptureFeaturePipe::releaseNodeSetting()
{
    TRACE_FUNC_ENTER();
    this->disconnect();
    TRACE_FUNC_EXIT();
}

MVOID CaptureFeaturePipe::releaseBuffer()
{
    TRACE_FUNC_ENTER();

    mpP2ANode->setBufferPool(NULL);
    if (mDualCam) {
        mpDepthNode->setBufferPool(NULL);
    }

    TRACE_FUNC_EXIT();
}


android::sp<ICaptureFeatureRequest>
CaptureFeaturePipe::acquireRequest()
{
    sp<CaptureFeatureRequest> pRequest = new CaptureFeatureRequest();
    return pRequest;
}

MVOID CaptureFeaturePipe::releaseRequest(sp<ICaptureFeatureRequest>)
{
}

MVOID CaptureFeaturePipe::setCallback(sp<RequestCallback> pCallback)
{
    mpCallback = pCallback;
}

const string CaptureFeaturePipe::MyDebuggee::mName {"NSCam::NSCamFeature::NSFeaturePipe::NSCapture::CaptureFeaturePipe"};

MVOID CaptureFeaturePipe::MyDebuggee::debug(android::Printer& printer, const std::vector<std::string>& options __unused)
{
    auto p = mContext.promote();
    if (CC_LIKELY(p != nullptr)) {
        p->dumpPlugin(printer);
        p->dumpRequest(printer);
    }
}

MVOID CaptureFeaturePipe::dumpPlugin(android::Printer& printer)
{
    #define DUMP_PLUGIN(type)                                               \
    do {                                                                        \
        type##Plugin::Ptr plugin = type##Plugin::getInstance(mSensorIndex);     \
        std::stringstream stream;                                               \
        stream << "== " << #type << "Plugin Dump ==" << std::endl;              \
        plugin->dump(stream);                                                   \
        printer.printLine(stream.str().c_str());                                     \
    } while(0)

    DUMP_PLUGIN(Raw);
    DUMP_PLUGIN(MultiFrame);
    DUMP_PLUGIN(Yuv);

    if (mSensorIndex2 >=0) {
        DUMP_PLUGIN(Fusion);
        DUMP_PLUGIN(Bokeh);
        DUMP_PLUGIN(Depth);
    }
}

MVOID CaptureFeaturePipe::dumpRequest(android::Printer& printer)
{
    printer.printLine("== Runing Request Dump ==");
    for (const auto& request :mRunningRequests) {
        request->dump(printer);
    }
}

PipeBufferHandle::PipeBufferHandle(sp<CaptureBufferPool> pBufferPool, Format_T format, MSize& size)
    : mpBufferPool(pBufferPool)
    , mpSmartBuffer(NULL)
    , mFormat(format)
    , mSize(size)
{

}

MERROR PipeBufferHandle::acquire(MINT usage)
{
    (void) usage;

    MY_LOGD("allocate image buffer(%dx%d) format(%d)",
         mSize.w, mSize.h, mFormat);

    mpSmartBuffer = mpBufferPool->getImageBuffer(
         mSize.w, mSize.h, mFormat);
    mpBufferPool = NULL;
    return OK;
}

IImageBuffer* PipeBufferHandle::native()
{
    if (mpSmartBuffer == NULL)
        return NULL;

    return mpSmartBuffer->mImageBuffer.get();
}

MVOID PipeBufferHandle::release()
{
    mpSmartBuffer = NULL;
}

MVOID PipeBufferHandle::dump(std::ostream& os) const
{
    (void) os;
}


MUINT32 PipeBufferHandle::getTransform()
{
    return 0;
}


} // NSCapture
} // NSFeaturePipe
} // NSCamFeature
} // NSCam
