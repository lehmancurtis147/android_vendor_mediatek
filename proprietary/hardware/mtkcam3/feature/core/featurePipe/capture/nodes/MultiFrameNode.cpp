/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "MultiFrameNode.h"

#define PIPE_CLASS_TAG "MultiFrameNode"
#define PIPE_TRACE TRACE_MULTIFRAME_NODE
#include <featurePipe/core/include/PipeLog.h>
#include <sstream>
#include "../CaptureFeaturePlugin.h"
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

using namespace NSCam::NSPipelinePlugin;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {


/******************************************************************************
*
******************************************************************************/
class MultiFrameInterface : public MultiFramePlugin::IInterface
{
public:
    virtual MERROR offer(MultiFramePlugin::Selection& sel)
    {
        sel.mRequestCount = 0;
        sel.mFrontDummy = 0;
        sel.mPostDummy = 0;

        sel.mIBufferFull
            .addSupportFormat(eImgFmt_NV12)
            .addSupportFormat(eImgFmt_YV12)
            .addSupportFormat(eImgFmt_YUY2)
            .addSupportFormat(eImgFmt_NV21)
            .addSupportFormat(eImgFmt_I420)
            .addSupportFormat(eImgFmt_I422)
            .addSupportFormat(eImgFmt_BAYER10)
            .addSupportFormat(eImgFmt_RAW16)
            .addSupportSize(eImgSize_Full)
            .addSupportSize(eImgSize_Specified);

        sel.mIBufferSpecified
            .addSupportFormat(eImgFmt_NV12)
            .addSupportFormat(eImgFmt_YV12)
            .addSupportFormat(eImgFmt_YUY2)
            .addSupportFormat(eImgFmt_NV21)
            .addSupportFormat(eImgFmt_I420)
            .addSupportFormat(eImgFmt_I422)
            .addSupportFormat(eImgFmt_Y8)
            .addSupportSize(eImgSize_Specified)
            .addSupportSize(eImgSize_Quarter);

        if (sel.mRequestIndex == 0) {
            sel.mOBufferFull
                .addSupportFormat(eImgFmt_NV12)
                .addSupportFormat(eImgFmt_YV12)
                .addSupportFormat(eImgFmt_YUY2)
                .addSupportFormat(eImgFmt_NV21)
                .addSupportFormat(eImgFmt_I420)
                .addSupportFormat(eImgFmt_I422)
                .addSupportFormat(eImgFmt_Y8)
                .addSupportFormat(eImgFmt_BAYER10)
                .addSupportFormat(eImgFmt_RAW16)
                .addSupportSize(eImgSize_Full);

            sel.mOBufferThumbnail
                .addSupportFormat(eImgFmt_NV12)
                .addSupportFormat(eImgFmt_YV12)
                .addSupportFormat(eImgFmt_YUY2)
                .addSupportFormat(eImgFmt_NV21)
                .addSupportSize(eImgSize_Arbitrary);
        }
        return OK;
    };

    virtual ~MultiFrameInterface() {};
};

REGISTER_PLUGIN_INTERFACE(MultiFrame, MultiFrameInterface);

/******************************************************************************
*
******************************************************************************/
class MultiFrameCallback : public MultiFramePlugin::RequestCallback
{
public:
    MultiFrameCallback(MultiFrameNode* pNode)
        : mpNode(pNode)
    {
    }

    virtual void onAborted(MultiFramePlugin::Request::Ptr pPlgRequest)
    {
        MY_LOGD("onAborted request: %p", pPlgRequest.get());
        onCompleted(pPlgRequest, UNKNOWN_ERROR);
    }

    virtual void onCompleted(MultiFramePlugin::Request::Ptr pPlgRequest, MERROR result)
    {
        RequestPtr pRequest = mpNode->findRequest(pPlgRequest);

        if (pRequest == NULL) {
            MY_LOGE("unknown request happened: %p, result %d", pPlgRequest.get(), result);
            return;
        }

        *pPlgRequest = MultiFramePlugin::Request();
        MY_LOGD("onCompleted request: %p, result %d", pPlgRequest.get(), result);
        mpNode->onRequestFinish(pRequest);
    }

    virtual ~MultiFrameCallback() { };
private:
    MultiFrameNode* mpNode;
};


/******************************************************************************
*
******************************************************************************/
MultiFrameNode::MultiFrameNode(NodeID_T nid, const char* name, MINT32 policy, MINT32 priority)
    : CaptureFeatureNode(nid, name, 0, policy, priority)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);
    TRACE_FUNC_EXIT();
}

MultiFrameNode::~MultiFrameNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL MultiFrameNode::onData(DataID id, RequestPtr &pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));

    const NodeID_T* nodeId = GetPath(id);
    NodeID_T sink = nodeId[1];

    if (sink == NID_MULTIRAW)
        pRequest->addParameter(PID_MULTIFRAME_TYPE, 0);
    else if (sink == NID_MULTIYUV)
        pRequest->addParameter(PID_MULTIFRAME_TYPE, 1);

    MBOOL ret = MTRUE;

    if (pRequest->isSatisfied(sink))
        mRequests.enque(pRequest);

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL MultiFrameNode::onAbort(RequestPtr &pRequest)
{
    Mutex::Autolock _l(mOperLock);
    MBOOL found = MFALSE;

    RequestPair p;
    {
        Mutex::Autolock _l(mPairLock);
        auto it = mRequestPairs.begin();
        for (; it != mRequestPairs.end(); it++) {
            RequestPair& rPair = *it;
            if (rPair.mpCapRequest == pRequest) {
                p = rPair;
                found = MTRUE;
                break;
            }
        }
    }

    auto pProvider = p.mpPlgProvider;
    if (!found) {
        // pick a pProvider
        for (size_t i = 0 ; i < mProviderMap.size() ; i++) {
            FeatureID_T featId = mProviderMap.keyAt(i);
            if (pRequest->hasFeature(featId)) {
                pProvider =  mProviderMap.valueFor(featId);
                break;
            }
        }
    }

    // Clear the selection repository
    do
    {
        // The rest requests will not queue to capture pipe, while abort() happend
        // Remove the rest selections.
        auto pFrontSel = mPlugin->frontSelection(pProvider);
        if (pFrontSel == NULL || pFrontSel->mRequestIndex == 0) {
            break;
        }
        auto pPopSel = mPlugin->popSelection(pProvider);
        MY_LOGW("Erase Selection C/I: %u/%u",
            pPopSel->mRequestCount, pPopSel->mRequestIndex);
    } while(true);

    if (found) {
        MY_LOGI("+, R/F Num: %d/%d",
            pRequest->getRequestNo(), pRequest->getFrameNo());
        std::vector<PluginRequestPtr> vpPlgRequests;
        vpPlgRequests.push_back(p.mpPlgRequest);

        pProvider->abort(vpPlgRequests);

         MY_LOGI("-, R/F Num: %d/%d",
            pRequest->getRequestNo(), pRequest->getFrameNo());
    }

    return MTRUE;
}

MBOOL MultiFrameNode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();

    mPlugin = MultiFramePlugin::getInstance(mSensorIndex);

    FeatureID_T featureId;
    auto& providers = mPlugin->getProviders();
    auto interface = mPlugin->getInterface();

    for (auto& pProvider : providers) {
        const MultiFramePlugin::Property& property =  pProvider->property();
        featureId = NULL_FEATURE;

        if (property.mFeatures & MTK_FEATURE_MFNR)
            featureId = FID_MFNR;
        else if (property.mFeatures & MTK_FEATURE_AINR)
            featureId = FID_AINR;
        else if (property.mFeatures & MTK_FEATURE_HDR)
            featureId = FID_HDR;
        else if (property.mFeatures & TP_FEATURE_MFNR)
            featureId = FID_MFNR_3RD_PARTY;
        else if (property.mFeatures & TP_FEATURE_HDR)
            featureId = FID_HDR_3RD_PARTY;
        else if (property.mFeatures & TP_FEATURE_HDR_DC)
            featureId = FID_HDR2_3RD_PARTY;

        if (featureId != NULL_FEATURE) {
            MY_LOGD_IF(mLogLevel, "find plugin:%s", FeatID2Name(featureId));
            mProviderMap.add(featureId, pProvider);

            if(property.mInitPhase==ePhase_OnPipeInit && mInitMapT.count(featureId) <= 0){
                auto job = std::async(std::launch::async,[&,pProvider](){
                    pProvider->init();
                });
                mInitMapT[featureId]=std::move(job);
                MY_LOGD("%s init on InitStage", FeatID2Name(featureId));
            }
        }
    }

    mpCallback = make_shared<MultiFrameCallback>(this);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MultiFrameNode::onUninit()
{
    TRACE_FUNC_ENTER();

    for (size_t i = 0 ; i < mProviderMap.size() ; i++) {
        ProviderPtr provider = mProviderMap.valueAt(i);
        FeatureID_T featureId = mProviderMap.keyAt(i);

        if ( mInitMapT.count(featureId) > 0 ) {
            if ( !mInitFeatures.hasBit(featureId) ) {
                MY_LOGD("get()+ featureName %s", FeatID2Name(featureId));
                mInitMapT[featureId].get();
                MY_LOGD("get()-");
                mInitFeatures.markBit(featureId);
            }
            provider->uninit();
        }
    }

    mProviderMap.clear();
    mInitMapT.clear();
    mInitFeatures.clear();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MultiFrameNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL MultiFrameNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MultiFrameNode::onThreadLoop()
{
    TRACE_FUNC_ENTER();
    if (!waitAllQueue())
    {
        TRACE_FUNC("Wait all queue exit");
        return MFALSE;
    }

    RequestPtr pRequest;
    if (!mRequests.deque(pRequest)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    pRequest->mTimer.startMF();

    onRequestProcess(pRequest);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID MultiFrameNode::onFlush()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onFlush();
    TRACE_FUNC_EXIT();
}

MBOOL MultiFrameNode::onRequestProcess(RequestPtr& pRequest)
{
    Mutex::Autolock _l(mOperLock);
    incExtThreadDependency();

    MINT32 frameType = pRequest->getParameter(PID_MULTIFRAME_TYPE);
    NodeID_T nodeId = frameType == 0 ? NID_MULTIRAW : NID_MULTIYUV;

    if (!pRequest->isValid() || pRequest->hasParameter(PID_FRAME_DROP)) {
        pRequest->decNodeReference(nodeId);
        onRequestFinish(pRequest);
        return MFALSE;
    }

    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    FeatureID_T featureId = NULL_FEATURE;
    CAM_TRACE_FMT_BEGIN("mf:process|r%df%d", requestNo, frameNo);
    MY_LOGI("+, R/F Num: %d/%d", requestNo, frameNo);


    sp<CaptureFeatureNodeRequest> pNodeReq =
        pRequest->getNodeRequest(nodeId);

    if (pNodeReq == NULL) {
        MY_LOGE("should not be here if no node request, type:%d", frameType);
        pRequest->addParameter(PID_FAILURE, 1);
        onRequestFinish(pRequest);
        return MFALSE;
    }

    TypeID_T uFullType = frameType == 0 ? TID_MAN_FULL_RAW : TID_MAN_FULL_YUV;

    auto pPlgRequest = mPlugin->createRequest();

    pPlgRequest->mIBufferFull       = PluginHelper::CreateBuffer(pNodeReq, uFullType, INPUT);
    pPlgRequest->mIBufferSpecified  = PluginHelper::CreateBuffer(pNodeReq, TID_MAN_SPEC_YUV, INPUT);
    pPlgRequest->mIBufferLCS        = PluginHelper::CreateBuffer(pNodeReq, TID_MAN_LCS, INPUT);
    pPlgRequest->mOBufferFull       = PluginHelper::CreateBuffer(pNodeReq, uFullType, OUTPUT);

    pPlgRequest->mIMetadataDynamic  = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_P1_DYNAMIC);
    pPlgRequest->mIMetadataApp      = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_APP);
    pPlgRequest->mIMetadataHal      = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_HAL);
    pPlgRequest->mOMetadataApp      = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_OUT_APP);
    pPlgRequest->mOMetadataHal      = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_OUT_HAL);

    // update frame info
    if (pRequest->hasParameter(PID_FRAME_INDEX))
        pPlgRequest->mRequestIndex = pRequest->getParameter(PID_FRAME_INDEX);
    if (pRequest->hasParameter(PID_FRAME_COUNT)) {
        pPlgRequest->mRequestCount = pRequest->getParameter(PID_FRAME_COUNT);
    }
    if (pRequest->hasParameter(PID_FRAME_DROP_COUNT)) {
        pPlgRequest->mRequestCount -= pRequest->getParameter(PID_FRAME_DROP_COUNT);
    }

    // pick a pProvider
    ProviderPtr pProvider = NULL;
    for (size_t i = 0 ; i < mProviderMap.size() ; i++) {
        FeatureID_T featId = mProviderMap.keyAt(i);
        if (pRequest->hasFeature(featId)) {
            pProvider =  mProviderMap.valueFor(featId);
            featureId = featId;
            break;
        }
    }

    MBOOL ret = MFALSE;
    if (pProvider != NULL)
    {
        if(!mInitFeatures.hasBit(featureId) && (mInitMapT.count(featureId)>0)){
            MY_LOGD("get()+ featureName %s", FeatID2Name(featureId));
            mInitMapT[featureId].get();
            MY_LOGD("get()-");
            mInitFeatures.markBit(featureId);
        }

        {
            Mutex::Autolock _l(mPairLock);
            auto& rPair = mRequestPairs.editItemAt(mRequestPairs.add());
            rPair.mpCapRequest = pRequest;
            rPair.mpPlgRequest = pPlgRequest;
            rPair.mpPlgProvider = pProvider;
        }

        pProvider->process(pPlgRequest, mpCallback);
        ret = MTRUE;
    }
    else
    {
        MY_LOGE("do not execute a plugin");
        pRequest->addParameter(PID_FAILURE, 1);
        onRequestFinish(pRequest);
    }

    MY_LOGI("-, R/F Num: %d/%d", requestNo, frameNo);
    CAM_TRACE_FMT_END();
    return MTRUE;
}

RequestPtr MultiFrameNode::findRequest(PluginRequestPtr& pPlgRequest)
{
    Mutex::Autolock _l(mPairLock);
    for (const auto& rPair : mRequestPairs) {
        if (pPlgRequest == rPair.mpPlgRequest) {
            return rPair.mpCapRequest;
        }
    }

    return NULL;
}

MBOOL MultiFrameNode::onRequestFinish(RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    CAM_TRACE_FMT_BEGIN("mf:finish|r%df%d", requestNo, frameNo);
    MY_LOGI("R/F Num: %d/%d", requestNo, frameNo);

    {
        Mutex::Autolock _l(mPairLock);
        auto it = mRequestPairs.begin();
        for (; it != mRequestPairs.end(); it++) {
            if ((*it).mpCapRequest == pRequest) {
                mRequestPairs.erase(it);
                break;
            }
        }
    }

    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if (pRequest->getParameter(PID_THUMBNAIL_TIMING) == NSPipelinePlugin::eTiming_Plugin)
        {
            MINT32 frameCount = pRequest->getParameter(PID_FRAME_COUNT);
            MINT32 frameIndex = pRequest->getParameter(PID_FRAME_INDEX);
            if (frameCount == frameIndex + 1)
            {
                if (pRequest->mpCallback != NULL) {
                    MY_LOGD("Nofity next capture at (%d/%d)", frameIndex, frameCount);
                    pRequest->mpCallback->onContinue(pRequest);
                } else {
                    MY_LOGW("have no request callback instance!");
                }
            }
        }
    }

    pRequest->mTimer.stopMF();
    MINT32 frameType = pRequest->getParameter(PID_MULTIFRAME_TYPE);
    dispatch(pRequest, frameType == 0 ? NID_MULTIRAW: NID_MULTIYUV);

    decExtThreadDependency();
    CAM_TRACE_FMT_END();
    return MTRUE;
}

MERROR MultiFrameNode::evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    auto& rSrcData = rInfer.getSharedSrcData();
    auto& rDstData = rInfer.getSharedDstData();
    auto& rFeatures = rInfer.getSharedFeatures();
    auto& rMetadatas = rInfer.getSharedMetadatas();

    MUINT8 uRequestCount = rInfer.getRequestCount();
    MUINT8 uRequestIndex = rInfer.getRequestIndex();

    MBOOL isEvaluated = MFALSE;
    MBOOL isValid;

    // Foreach all loaded plugin
    for (size_t i = 0 ; i < mProviderMap.size() ; i++) {

        FeatureID_T featId = mProviderMap.keyAt(i);
        if (!rInfer.hasFeature(featId)) {
            continue;
        } else if (isEvaluated) {
            MY_LOGE("has duplicated feature: %s", FeatID2Name(featId));
            continue;
        }

        isValid = MTRUE;

        ProviderPtr pProvider = mProviderMap.valueAt(i);

        if(pProvider->property().mInitPhase==ePhase_OnRequest && mInitMapT.count(featId) <= 0){
            auto job = std::async(std::launch::async,[&,pProvider](){
                pProvider->init();
            });
            mInitMapT[featId]=std::move(job);
            MY_LOGD("%s init on evaluate stage", FeatID2Name(featId));
        }

        // Gate the evaluate for RAW-Domain or YUV-Domain
        {
            auto pFrontSel = mPlugin->frontSelection(pProvider);
            if (pFrontSel == NULL) {
                MY_LOGE("have no a selection, feature:%s, count:%u, index:%u",
                        FeatID2Name(featId), uRequestCount ,uRequestIndex);
                // TODO: the selection will be popped if RAW-domain request
                //rInfer.clearFeature(featId);
                return BAD_VALUE;
            }
                const Selection& rFrontSel = *pFrontSel;

            if (rFrontSel.mIBufferFull.getRequired() && rFrontSel.mIBufferFull.isValid()) {
                MINT fmt = rFrontSel.mIBufferFull.getFormats()[0];
                if ((fmt & eImgFmt_RAW_START) == eImgFmt_RAW_START) {
                    if (nodeId != NID_MULTIRAW) {
                        MY_LOGD("ignore YUV-domain request in evaluation, nodeId:%d", nodeId);
                        return NAME_NOT_FOUND;
                    }
                } else {
                    if (nodeId != NID_MULTIYUV) {
                        MY_LOGD("ignore RAW-domain request in evaluation, nodeId:%d", nodeId);
                        return NAME_NOT_FOUND;
                    }
                }
            }
        }

        auto pPopSel = mPlugin->popSelection(pProvider);
        const Selection& rSel = *pPopSel;

        // should issue AEE if popped selection is not equal to request
        MY_LOGD("Pop Selection C/I: %u/%u; Request C/I :%u/%u",
                rSel.mRequestCount, rSel.mRequestIndex,
                uRequestCount ,uRequestIndex);

        auto ceil = [](MUINT32 val, MUINT32 align) {
            if (align) {
                if (align & (align -1))
                    MY_LOGE("not valid alignment(%d) to value(%d)", align, val);
                else
                    return (val + align - 1) & ~(align - 1);
            }
            return val;
        };

        // full size input
        if (rSel.mIBufferFull.getRequired()) {
            if (rSel.mIBufferFull.isValid()) {
                auto& src_0 = rSrcData.editItemAt(rSrcData.add());

                // Directly select the first format, using lazy strategy
                MINT fmt = rSel.mIBufferFull.getFormats()[0];
                // Check either RAW-Domain or YUV-Domain
                if ((fmt & eImgFmt_RAW_START) == eImgFmt_RAW_START) {
                    src_0.mTypeId = TID_MAN_FULL_RAW;
                   if (!rInfer.hasType(TID_MAN_FULL_RAW))
                        isValid = MFALSE;
                } else {
                    src_0.mTypeId = TID_MAN_FULL_YUV;
                    if (!rInfer.hasType(TID_MAN_FULL_YUV))
                        isValid = MFALSE;
                }

                if (isValid) {
                    src_0.mSizeId = rSel.mIBufferFull.getSizes()[0];
                    src_0.mFormat = fmt;

                    if (src_0.mSizeId == SID_SPECIFIED)
                        src_0.mSize = rSel.mIBufferFull.getSpecifiedSize();
                    else
                        src_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);

                    // alignment for MFNR
                    MUINT32 align_w;
                    MUINT32 align_h;
                    rSel.mIBufferFull.getAlignment(align_w, align_h);
                    src_0.mSize.w = ceil(src_0.mSize.w, align_w);
                    src_0.mSize.h = ceil(src_0.mSize.h, align_h);
                    if (align_w | align_h)
                        MY_LOGD("full buffer: align(%d,%d) size(%dx%d)",
                                align_w, align_h, src_0.mSize.w, src_0.mSize.h);
                }

            } else
                isValid = MFALSE;
        }

        // specified size input
        if (isValid && rSel.mIBufferSpecified.getRequired()) {
            if (!rInfer.hasType(TID_MAN_SPEC_YUV))
                isValid = MFALSE;

            if (rSel.mIBufferSpecified.isValid()) {
                auto& src_1 = rSrcData.editItemAt(rSrcData.add());
                src_1.mTypeId = TID_MAN_SPEC_YUV;
                src_1.mSizeId = rSel.mIBufferSpecified.getSizes()[0];
                src_1.mFormat = rSel.mIBufferSpecified.getFormats()[0];
                if (src_1.mSizeId == SID_SPECIFIED)
                    src_1.mSize = rSel.mIBufferSpecified.getSpecifiedSize();
                else if (src_1.mSizeId == SID_QUARTER) {
                    MSize full = rInfer.getSize(TID_MAN_FULL_RAW);
                    src_1.mSize = MSize(full.w / 2, full.h / 2);
                }

                // alignment for MFNR
                MUINT32 align_w;
                MUINT32 align_h;
                rSel.mIBufferSpecified.getAlignment(align_w, align_h);
                // allocate padding if not alignment
                src_1.mSize.w = ceil(src_1.mSize.w, align_w);
                src_1.mSize.h = ceil(src_1.mSize.h, align_h);
                if (align_w | align_h)
                    MY_LOGD("specified buffer: align(%d, %d) size(%d,%d)",
                            align_w, align_h, src_1.mSize.w, src_1.mSize.h);

            } else
                isValid = MFALSE;
        }

        // lcs
        if (isValid && rSel.mIBufferLCS.getRequired()) {
            if (!rInfer.hasType(TID_MAN_LCS))
                isValid = MFALSE;

            auto& src_2 = rSrcData.editItemAt(rSrcData.add());
            src_2.mTypeId = TID_MAN_LCS;
        }

        // face data
        const MultiFramePlugin::Property& rProperty = pProvider->property();
        if (rProperty.mFaceData == eFD_Current) {
            auto& src_1 = rSrcData.editItemAt(rSrcData.add());
            src_1.mTypeId = TID_MAN_FD;
            src_1.mSizeId = NULL_SIZE;
            rInfer.markFaceData(eFD_Current);
        }
        else if (rProperty.mFaceData == eFD_Cache) {
            rInfer.markFaceData(eFD_Cache);
        }
        else if (rProperty.mFaceData == eFD_None) {
            rInfer.markFaceData(eFD_None);
        }
        else {
            MY_LOGW("unknow faceDateType:%x", rInfer.mFaceDateType.value);
        }

        // full size output
        if (isValid && rSel.mOBufferFull.getRequired()) {
            if (rSel.mOBufferFull.isValid()) {
                auto& dst_0 = rDstData.editItemAt(rDstData.add());
                dst_0.mSizeId = rSel.mOBufferFull.getSizes()[0];
                dst_0.mFormat = rSel.mOBufferFull.getFormats()[0];
                dst_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);
                if ((dst_0.mFormat  & eImgFmt_RAW_START) == eImgFmt_RAW_START) {
                    dst_0.mTypeId = TID_MAN_FULL_RAW;
                } else {
                    dst_0.mTypeId = TID_MAN_FULL_YUV;
                }
            } else
                isValid = MFALSE;
        }

        if (isValid) {
            const MultiFramePlugin::Property& rProperty = pProvider->property();
            rInfer.markThumbnailTiming(rProperty.mThumbnailTiming);

            if (rSel.mIMetadataDynamic.getRequired())
                rMetadatas.push_back(MID_MAN_IN_P1_DYNAMIC);

            if (rSel.mIMetadataApp.getRequired())
                rMetadatas.push_back(MID_MAN_IN_APP);

            if (rSel.mIMetadataHal.getRequired())
                rMetadatas.push_back(MID_MAN_IN_HAL);

            if (uRequestIndex == 0 && rSel.mOMetadataApp.getRequired())
                rMetadatas.push_back(MID_MAN_OUT_APP);

            if (uRequestIndex == 0 && rSel.mOMetadataHal.getRequired())
                rMetadatas.push_back(MID_MAN_OUT_HAL);
        }

        if (isValid) {
            isEvaluated = MTRUE;
            rFeatures.push_back(featId);
            rInfer.addNodeIO(nodeId, rSrcData, rDstData, rMetadatas, rFeatures, MTRUE);
        } else
            MY_LOGW("has invalid evaluation:%s", FeatID2Name(featId));
    }

    return OK;
}
} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
