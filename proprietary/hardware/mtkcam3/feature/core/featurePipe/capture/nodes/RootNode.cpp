/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "RootNode.h"

#define PIPE_CLASS_TAG "RootNode"
#define PIPE_TRACE TRACE_ROOT_NODE
#include <featurePipe/core/include/PipeLog.h>

#include <mtkcam3/feature/lmv/lmv_ext.h>
#include <custom/feature/mfnr/camera_custom_mfll.h>
#include <custom/debug_exif/dbg_exif_param.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/hw/IFDContainer.h>
#include <mtkcam/utils/hw/HwTransform.h>

#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/INvBufUtil.h>
#include <mtkcam/utils/hw/IFDContainer.h>

#ifdef SUPPORT_MFNR
#include <mtkcam3/feature/mfnr/IMfllNvram.h>
#include <mtkcam3/feature/mfnr/MfllProperty.h>

#include <custom/feature/mfnr/camera_custom_mfll.h>
#include <custom/debug_exif/dbg_exif_param.h>
#if (MFLL_MF_TAG_VERSION > 0)
using namespace __namespace_mf(MFLL_MF_TAG_VERSION);
#include <tuple>
#include <string>
#endif
#include <fstream>
#include <sstream>

// CUSTOM (platform)
#if MTK_CAM_NEW_NVRAM_SUPPORT
#include <mtkcam/utils/mapping_mgr/cam_idx_mgr.h>
#endif
#include <camera_custom_nvram.h>
#include <featurePipe/capture/exif/ExifWriter.h>
//
#define MFLLBSS_DUMP_PATH       "/data/vendor/camera_dump/"
#define MFLLBSS_DUMP_FILENAME   "fd-data.txt"

// describes that how many frames we update to exif,
#define MFLLBSS_FD_RECT0_FRAME_COUNT    8
// describes that how many fd rect0 info we need at a frame,
#define MFLLBSS_FD_RECT0_PER_FRAME      4

#ifndef MFLL_ALGO_THREADS_PRIORITY
#define MFLL_ALGO_THREADS_PRIORITY 0
#endif

using std::vector;
using namespace mfll;

/*
 *  Tuning Param for BSS ALG.
 *  Should not be configure by customer
 */
static const int MF_BSS_ON = 1;
static const int MF_BSS_VER = 2;
static const int MF_BSS_ROI_PERCENTAGE = 95;

#endif //SUPPORT_MFNR

#define __DEBUG // enable debug

#ifdef __DEBUG
#include <memory>
#define FUNCTION_SCOPE \
auto __scope_logger__ = [](char const* f)->std::shared_ptr<const char>{ \
    CAM_LOGD("(%d)[%s] + ", ::gettid(), f); \
    return std::shared_ptr<const char>(f, [](char const* p){CAM_LOGD("(%d)[%s] -", ::gettid(), p);}); \
}(__FUNCTION__)
#else
#define FUNCTION_SCOPE
#endif //__DEBUG

#define MY_DBG_COND(level)          __builtin_expect( mDbgLevel >= level, false )
#define MY_LOGD3(...)               do { if ( MY_DBG_COND(3) ) MY_LOGD(__VA_ARGS__); } while(0)

#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>

#define CHECK_OBJECT(x)  do{                                            \
    if (x == nullptr) { MY_LOGE("Null %s Object", #x); return -MFALSE;} \
} while(0)


namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

class RootNode::FDContainerWraper final
{
public:
    using Ptr = UniquePtr<FDContainerWraper>;

public:
    static Ptr createInstancePtr(RequestPtr& pRequest);

public:
    MBOOL updateFaceData(RequestPtr& pRequest);

private:
    FDContainerWraper(const NSCamHW::HwMatrix& active2SensorMatrix, sp<IFDContainer>& fdReader);

    ~FDContainerWraper();

private:
    NSCamHW::HwMatrix   mActive2TGMatrix;
    sp<IFDContainer>    mFDReader;
};

auto
RootNode::FDContainerWraper::
createInstancePtr(RequestPtr& pRequest) -> Ptr
{
    MY_LOGD("create FDContainerWraper ptr, reqNo:%d", pRequest->getRequestNo());

    auto pInMetaHal = pRequest->getMetadata(MID_MAN_IN_HAL);
    IMetadata* pHalMeta = (pInMetaHal != nullptr) ? pInMetaHal->native() : nullptr;
    if(pHalMeta == nullptr) {
        MY_LOGW("failed to create instance, can not get inHalMetadata");
        return MFALSE;
    }
    //
    MINT32 sensorMode;
    if (!tryGetMetadata<MINT32>(pHalMeta, MTK_P1NODE_SENSOR_MODE, sensorMode)) {
        MY_LOGW("failed to create instance, can not get tag MTK_P1NODE_SENSOR_MODE from metadata, addr:%p", pHalMeta);
        return nullptr;
    }
    //
    NSCamHW::HwTransHelper hwTransHelper(sensorMode);
    NSCamHW::HwMatrix active2TGMatrix;
    if(!hwTransHelper.getMatrixFromActive(sensorMode, active2TGMatrix)) {
        MY_LOGW("failed to create instance, can not get active2TGMatrix, sensorMode:%d", sensorMode);
        return nullptr;
    }
    //
    sp<IFDContainer> fdReader = IFDContainer::createInstance(PIPE_CLASS_TAG, IFDContainer::eFDContainer_Opt_Read);
    if(fdReader == nullptr) {
        MY_LOGW("failed to create instance, can not get fdContainer");
        return MFALSE;
    }

    return Ptr(new FDContainerWraper(active2TGMatrix, fdReader), [](FDContainerWraper* p)
    {
        delete p;
    });
}

auto
RootNode::FDContainerWraper::
updateFaceData(RequestPtr& pRequest) -> MBOOL
{
    const MINT32 reqNo = pRequest->getRequestNo();
    MY_LOGD("update default fd info for request, reqNo:%d", reqNo);
    //
    auto pInMetaHal = pRequest->getMetadata(MID_MAN_IN_HAL);
    IMetadata* pHalMeta = (pInMetaHal != nullptr) ? pInMetaHal->native() : nullptr;
    if(pHalMeta == nullptr) {
        MY_LOGW("failed to get inHalMetadata, reqNo:%d", reqNo);
        return MFALSE;
    }
    //
    //
    MINT64 frameStartTimestamp = 0;
    if(!tryGetMetadata<MINT64>(pHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, frameStartTimestamp)) {
        MY_LOGW("failed to get p1 node frame start timestamp, reqNo:%d", reqNo);
        return MFALSE;
    }
    //
    auto createFaceDataPtr = [&fdReader=mFDReader](MINT64 tsStart, MINT64 tsEnd)
    {
        using FaceData = vector<FD_DATATYPE*>;
        FaceData* pFaceData = new FaceData();
        (*pFaceData) = fdReader->queryLock(tsStart, tsEnd);
        using FaceDataPtr = std::unique_ptr<FaceData, std::function<MVOID(FaceData*)>>;
        return FaceDataPtr(pFaceData, [&fdReader](FaceData* p)
        {
            fdReader->queryUnlock(*p);
            delete p;
        });

    };
    // query fd info by timestamps, fdData must be return after use
    static const MINT64 tolerence = 600000000; // 600ms
    const MINT64 tsStart = frameStartTimestamp - tolerence;
    const MINT64 tsEnd = frameStartTimestamp;
    auto faceDataPtr = createFaceDataPtr(tsStart, tsEnd);
    // back element is the closest to the timestamp we assigned
    auto foundItem = std::find_if(faceDataPtr->rbegin(), faceDataPtr->rend(), [] (const FD_DATATYPE* e)
    {
        return e != nullptr;
    });
    //
    if(foundItem == faceDataPtr->rend())
    {
        MY_LOGW("no faceData, reqNo:%d, timestampRange:(%ld, %ld)", reqNo, tsStart, tsEnd);
        return MFALSE;
    }
    //
    MtkCameraFaceMetadata faceData = (*foundItem)->facedata;
    const MINT32 faceCount = faceData.number_of_faces;
    if(faceCount == 0)
    {
        MY_LOGD("no faceData, reqNo:%d, count:%d", reqNo, faceCount);
        return MTRUE;
    }

    IMetadata::IEntry entryFaceRects(MTK_STATISTICS_FACE_RECTANGLES);
    IMetadata::IEntry entryPoseOriens(MTK_FACE_FEATURE_POSE_ORIENTATIONS);
    for(MINT32 index = 0; index < faceCount; ++index) {
        MtkCameraFace& faceInfo = faceData.faces[index];
        // Face Rectangle
        // the source face data use point left-top and right-bottom to show the fd rectangle
        // note: we use the MSize to save the right-bottom point of the face data
        const MPoint leftTop(faceInfo.rect[0], faceInfo.rect[1]);
        const MSize rightBottom(faceInfo.rect[2], faceInfo.rect[3]);
        const MRect activeDomainFaceRect(leftTop, rightBottom);
        MRect tgDomainFaceRect;
        mActive2TGMatrix.transform(activeDomainFaceRect, tgDomainFaceRect);
        MY_LOGD("detected face rectangle, reqNo:%d, faceNum:%d/%d, activeFaceRect:(%d, %d)x(%d, %d), tgFaceRect:(%d, %d)x(%d, %d)",
            reqNo, index, faceCount,
            activeDomainFaceRect.p.x, activeDomainFaceRect.p.y, activeDomainFaceRect.s.w, activeDomainFaceRect.s.h,
            tgDomainFaceRect.p.x, tgDomainFaceRect.p.y, tgDomainFaceRect.s.w, tgDomainFaceRect.s.h);
        entryFaceRects.push_back(tgDomainFaceRect, Type2Type<MRect>());
        // Face Pose
        const MINT32 poseX = 0;
        const MINT32 poseY = faceData.fld_rop[index];
        const MINT32 poseZ = faceData.fld_rip[index];
        MY_LOGD("detected face pose orientation, reqNo:%d, faceNum:%d/%d, (x, y, z):(%d, %d, %d)",
            reqNo, index, faceCount, poseX, poseY, poseZ);
        entryPoseOriens.push_back(poseX, Type2Type<MINT32>());    // pose X asix
        entryPoseOriens.push_back(poseY, Type2Type<MINT32>());    // pose Y asix
        entryPoseOriens.push_back(poseZ, Type2Type<MINT32>());    // pose Z asix
    }
    // update to appMeta
    pHalMeta->update(MTK_FEATURE_FACE_RECTANGLES, entryFaceRects);
    pHalMeta->update(MTK_FEATURE_FACE_POSE_ORIENTATIONS, entryPoseOriens);
    return MTRUE;
}

RootNode::FDContainerWraper::
FDContainerWraper(const NSCamHW::HwMatrix& active2SensorMatrix, sp<IFDContainer>& fdReader)
: mActive2TGMatrix(active2SensorMatrix)
, mFDReader(fdReader)
{
    MY_LOGD("ctor:%p", this);
}

RootNode::FDContainerWraper::
~FDContainerWraper()
{
    MY_LOGD("dtor:%p", this);
}

RootNode::RootNode(NodeID_T nid, const char* name, MINT32 policy, MINT32 priority)
    : CaptureFeatureNode(nid, name, 0, policy, priority)
    , mFDContainerWraperPtr(nullptr)
    , mDbgLevel(0)
    , mMfnrQueryIndex(-1)
    , mMfnrDecisionIso(-1)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);
    mDebugBSS = property_get_int32("vendor.debug.camera.bss.enable", 1);
    TRACE_FUNC_EXIT();
}

RootNode::~RootNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL RootNode::onData(DataID id, RequestPtr& pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));

    MBOOL ret = MFALSE;
    Mutex::Autolock _l(mLock);
    switch(id)
    {
        case PID_ENQUE:
        {
            const MINT32 frameIndex = pRequest->getParameter(PID_FRAME_INDEX);
            const MBOOL isFirstFrame = (frameIndex == 0);
            const MBOOL isNeededCachedFD = pRequest->getParameter(PID_FD_CACHED_DATA);
            MY_LOGD_IF(mLogLevel, "updateFaceData info., reqNo:%d, frameIndex:%d, isFirstFrame:%d, isNeededCachedFD:%d",
                pRequest->getRequestNo(), frameIndex, isFirstFrame, isNeededCachedFD);

            if (isFirstFrame && isNeededCachedFD) {
                // note: not tread-saft, in this function, we assert that is just only one thread call it
                if(mFDContainerWraperPtr == nullptr) {
                    mFDContainerWraperPtr = FDContainerWraper::createInstancePtr(pRequest);
                }
                mFDContainerWraperPtr->updateFaceData(pRequest);
            }

#ifdef SUPPORT_MFNR
            auto isApplyBss = [this](RequestPtr& pRequest) -> bool {
                if (pRequest->getParameter(PID_FRAME_COUNT) <= 1)
                    return false;

                MY_LOGD3("pRequest->getParameter(PID_FRAME_COUNT) = %d", pRequest->getParameter(PID_FRAME_COUNT));

                auto pInMetaHal = pRequest->getMetadata(MID_MAN_IN_HAL);
                IMetadata* pHalMeta = pInMetaHal->native();
                MINT32 selectedBssCount = 0;
                if (!IMetadata::getEntry<MINT32>(pHalMeta, MTK_FEATURE_BSS_SELECTED_FRAME_COUNT, selectedBssCount)) {
                    MY_LOGW("get MTK_FEATURE_BSS_SELECTED_FRAME_COUNT failed, set to 0");
                }

                if (selectedBssCount < 1)
                    return false;

                if (CC_UNLIKELY(mfll::MfllProperty::getBss() <= 0)) {
                    MY_LOGD("%s: Bypass bss due to force disable by property", __FUNCTION__);
                    return false;
                }

                return true;
            };

            if (isApplyBss(pRequest))
            {
                mRequests.enque(pRequest);
            } else
#endif

            {
                dispatch(pRequest);
            }
            ret = MTRUE;
            break;
        }
        default:
        {
            ret = MFALSE;
            MY_LOGE("unknown data id :%d", id);
            break;
        }
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL RootNode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();
#ifdef SUPPORT_MFNR
    mDbgLevel = mfll::MfllProperty::readProperty(mfll::Property_LogLevel);
    mDbgLevel = max(property_get_int32("vendor.bss.log_level", mDbgLevel), mDbgLevel);
#endif
    TRACE_FUNC_EXIT();
    return MTRUE;
}


MBOOL RootNode::onThreadStart()
{
    return MTRUE;
}

MBOOL RootNode::onThreadStop()
{
    return MTRUE;
}

MERROR RootNode::evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    (void) rInfer;
    return OK;
}

MBOOL RootNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr pRequest;

    CAM_TRACE_CALL();

    if (!waitAllQueue()) {
        return MFALSE;
    }

    if (!mRequests.deque(pRequest)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    onRequestProcess(pRequest);

    return MTRUE;
}

MBOOL RootNode::onAbort(RequestPtr& pRequest)
{
    Mutex::Autolock _l(mLock);
    auto it = mvPendingRequests.begin();
    for (; it != mvPendingRequests.end(); it++) {
        if ((*it) == pRequest) {
            mvPendingRequests.erase(it);
            MY_LOGI("R/F Num: %d/%d",
                    pRequest->getRequestNo(), pRequest->getFrameNo());
            onRequestFinish(pRequest);
            break;
        }
    }

    return MTRUE;
}

MBOOL RootNode::onRequestProcess(RequestPtr& pRequest)
{
    Mutex::Autolock _l(mLock);

    this->incExtThreadDependency();

    mvPendingRequests.push_back(pRequest);

    auto frameIndex = pRequest->getParameter(PID_FRAME_INDEX);
    auto frameCount = pRequest->getParameter(PID_FRAME_COUNT);
    if (mvPendingRequests.size() < (size_t)frameCount) {
        MY_LOGD("BSS: Keep waiting, Index:%d Pending/Expected: %zu/%d",
                frameIndex, mvPendingRequests.size(), frameCount);
    } else {
        MY_LOGD("BSS: Do re-order, Index:%d Pending/Expected: %zu/%d",
                frameIndex, mvPendingRequests.size(), frameCount);

        Vector<RequestPtr> vReadyRequests = mvPendingRequests;
        mvPendingRequests.clear();
#ifdef SUPPORT_MFNR
        doBss(vReadyRequests);
#else
        MY_LOGE("Not support BSS: dispatch");
        return MFALSE;
#endif
    }

    return MTRUE;
}

MVOID RootNode::onRequestFinish(RequestPtr& pRequest)
{
    dispatch(pRequest);
    this->decExtThreadDependency();
}

#ifdef SUPPORT_MFNR
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL RootNode::retrieveGmvInfo(IMetadata* pMetadata, int& x, int& y, MSize& size) const
{
    MBOOL  ret = MTRUE;
    MSize  rrzoSize;
    IMetadata::IEntry entry;
    struct __confidence{
        MINT32 x;
        MINT32 y;
        __confidence() : x(0), y(0) {}
    } confidence;

    /* get size first */
    ret = tryGetMetadata<MSize>(pMetadata, MTK_P1NODE_RESIZER_SIZE, rrzoSize);
    if (ret != MTRUE) {
        MY_LOGE("%s: cannot get rzo size", __FUNCTION__);
        goto lbExit;
    }

    entry = pMetadata->entryFor(MTK_EIS_REGION);

    /* check if a valid EIS_REGION */
    if (entry.count() < LMV_REGION_INDEX_SIZE) {
        MY_LOGE("%s: entry is not a valid LMV_REGION, size = %d",
                __FUNCTION__,
                entry.count());
        ret = MFALSE;
        goto lbExit;
    }

    /* read confidence */
    confidence.x = static_cast<MINT32>(entry.itemAt(LMV_REGION_INDEX_CONFX, Type2Type<MINT32>()));
    confidence.y = static_cast<MINT32>((MINT32)entry.itemAt(LMV_REGION_INDEX_CONFY, Type2Type<MINT32>()));

    /* to read GMV if confidence is enough */
    if (confidence.x > MFC_GMV_CONFX_TH) {
        x = entry.itemAt(LMV_REGION_INDEX_GMVX, Type2Type<MINT32>());
    }

    if (confidence.y > MFC_GMV_CONFY_TH) {
        y = entry.itemAt(LMV_REGION_INDEX_GMVY, Type2Type<MINT32>());
    }

    size = rrzoSize;

    MY_LOGD("LMV info conf(x,y) = (%d, %d), gmv(x, y) = (%d, %d)",
            confidence.x, confidence.y, x, y);

lbExit:
    return ret;
}
/*******************************************************************************
 *
 ********************************************************************************/
RootNode::GMV RootNode::calMotionVector(IMetadata* pMetadata, MBOOL isMain) const
{
    RootNode::GMV        mv;
    MSize               rrzoSize;
    MRect               p1ScalarRgn;
    MBOOL               ret = MTRUE;

    /* to get GMV info and the working resolution */
    ret = retrieveGmvInfo(pMetadata, mv.x, mv.y, rrzoSize);
    if (ret == MTRUE) {
        ret = tryGetMetadata<MRect>(
                pMetadata,
                MTK_P1NODE_SCALAR_CROP_REGION,
                p1ScalarRgn);
    }

    /* if works, mapping it from rzoDomain to MfllCore domain */
    if (ret == MTRUE) {
        /* the first frame, set GMV as zero */
        if (isMain) {
            mv.x = 0;
            mv.y = 0;
        }

        MY_LOGD("GMV(x,y)=(%d,%d), unit based on resized RAW",
                mv.x, mv.y);

        MY_LOGD("p1node scalar crop rgion (width): %d, gmv domain(width): %d",
                p1ScalarRgn.s.w, rrzoSize.w);
        /**
         *  the cropping crops height only, not for width. Hence, just
         *  simply uses width to calculate the ratio.
         */
        float ratio =
            static_cast<float>(p1ScalarRgn.s.w)
            /
            static_cast<float>(rrzoSize.w)
            ;
        MY_LOGD("%s: ratio = %f", __FUNCTION__, ratio);

        // we don't need floating computing because GMV is formated
        // with 8 bits floating point
        mv.x *= ratio;
        mv.y *= ratio;

        /* normalization */
        mv.x = mv.x >> 8;
        mv.y = mv.y >> 8;

        // assume the ability of EIS algo, which may seach near by
        // N pixels only, so if the GMV is more than N pixels,
        // we clip it

        auto CLIP = [](int x, const int n) -> int {
            if (x < -n)     return -n;
            else if(x > n)  return n;
            else            return x;
        };

        // Hence we've already known that search region is 32 by 32
        // pixel based on RRZO domain, we can map it to full size
        // domain and makes clip if it's out-of-boundary.
        int c = static_cast<int>(ratio * 32.0f);
        mv.x = CLIP(mv.x, c);
        mv.y = CLIP(mv.y, c);

        MY_LOGI("GMV'(x,y)=(%d,%d), unit: Mfll domain", mv.x, mv.y);
    }
    return mv;
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID RootNode::updateBssProcInfo(IImageBuffer* pBuf, BSS_PARAM_STRUCT& p, MINT32 frameNum) const
{
    FUNCTION_SCOPE;

    MSize srcSize(pBuf->getImgSize());

    MINT32 roiPercentage = MF_BSS_ROI_PERCENTAGE;
    MINT32 w = (srcSize.w * roiPercentage + 5) / 100;
    MINT32 h = (srcSize.h * roiPercentage + 5) / 100;
    MINT32 x = (srcSize.w - w) / 2;
    MINT32 y = (srcSize.h - h) / 2;

    #define MAKE_TAG(prefix, tag, id)   prefix##tag##id
    #define MAKE_TUPLE(tag, id)         std::make_tuple(#tag, id)
    #define DECLARE_BSS_ENUM_MAP()      std::map<std::tuple<const char*, int>, MUINT32> enumMap
    #define BUILD_BSS_ENUM_MAP(tag) \
            do { \
                enumMap[MAKE_TUPLE(tag, 0)] = (MUINT32)MAKE_TAG(CUST_MFLL_BSS_, tag, _00); \
                enumMap[MAKE_TUPLE(tag, 1)] = (MUINT32)MAKE_TAG(CUST_MFLL_BSS_, tag, _01); \
                enumMap[MAKE_TUPLE(tag, 2)] = (MUINT32)MAKE_TAG(CUST_MFLL_BSS_, tag, _02); \
                enumMap[MAKE_TUPLE(tag, 3)] = (MUINT32)MAKE_TAG(CUST_MFLL_BSS_, tag, _03); \
            } while (0)
    #define SET_CUST_MFLL_BSS(tag, idx, value) \
                        do { \
                            BUILD_BSS_ENUM_MAP(tag); \
                            enumMap[MAKE_TUPLE(tag, idx)] = (MUINT32)value; \
                        } while (0)
    #define GET_CUST_MFLL_BSS(tag) \
            [&, this]() { \
                BUILD_BSS_ENUM_MAP(tag); \
                return enumMap[MAKE_TUPLE(tag, mSensorIndex)]; \
            }();

    DECLARE_BSS_ENUM_MAP();

#if MTK_CAM_NEW_NVRAM_SUPPORT
    //replace by NVRAM
    if (CC_LIKELY( mMfnrQueryIndex >= 0 && mMfnrDecisionIso >= 0 )) {

        MY_LOGD3("replace BSS parameter by NVRAM data");

        /* read NVRAM for tuning data */
        size_t chunkSize = 0;
        std::shared_ptr<char> pChunk = getNvramChunk(&chunkSize);
        if (CC_UNLIKELY(pChunk == NULL)) {
            MY_LOGE("%s: read NVRAM failed, use default", __FUNCTION__);
        }
        else {
            char *pMutableChunk = const_cast<char*>(pChunk.get());
            NVRAM_CAMERA_FEATURE_MFLL_STRUCT* pNvram = reinterpret_cast<NVRAM_CAMERA_FEATURE_MFLL_STRUCT*>(pMutableChunk);

            if (CC_LIKELY( pNvram->bss_iso_th0 != 0 )) {
                /* update bad range and bad threshold */
                // do nothing?
                // get memc_noise_level by the current ISO
                if (mMfnrDecisionIso < static_cast<MINT32>(pNvram->bss_iso_th0)) {
                    SET_CUST_MFLL_BSS(CLIP_TH0, mSensorIndex, pNvram->bss_iso0_clip_th0);
                    SET_CUST_MFLL_BSS(CLIP_TH1, mSensorIndex, pNvram->bss_iso0_clip_th1);
                    SET_CUST_MFLL_BSS(CLIP_TH2, mSensorIndex, pNvram->bss_iso0_clip_th2);
                    SET_CUST_MFLL_BSS(CLIP_TH3, mSensorIndex, pNvram->bss_iso0_clip_th3);
                    SET_CUST_MFLL_BSS(ADF_TH,   mSensorIndex, pNvram->bss_iso0_adf_th);
                    SET_CUST_MFLL_BSS(SDF_TH,   mSensorIndex, pNvram->bss_iso0_sdf_th);
                } else if (mMfnrDecisionIso < static_cast<MINT32>(pNvram->bss_iso_th1)) {
                    SET_CUST_MFLL_BSS(CLIP_TH0, mSensorIndex, pNvram->bss_iso1_clip_th0);
                    SET_CUST_MFLL_BSS(CLIP_TH1, mSensorIndex, pNvram->bss_iso1_clip_th1);
                    SET_CUST_MFLL_BSS(CLIP_TH2, mSensorIndex, pNvram->bss_iso1_clip_th2);
                    SET_CUST_MFLL_BSS(CLIP_TH3, mSensorIndex, pNvram->bss_iso1_clip_th3);
                    SET_CUST_MFLL_BSS(ADF_TH,   mSensorIndex, pNvram->bss_iso1_adf_th);
                    SET_CUST_MFLL_BSS(SDF_TH,   mSensorIndex, pNvram->bss_iso1_sdf_th);
                } else if (mMfnrDecisionIso < static_cast<MINT32>(pNvram->bss_iso_th2)) {
                    SET_CUST_MFLL_BSS(CLIP_TH0, mSensorIndex, pNvram->bss_iso2_clip_th0);
                    SET_CUST_MFLL_BSS(CLIP_TH1, mSensorIndex, pNvram->bss_iso2_clip_th1);
                    SET_CUST_MFLL_BSS(CLIP_TH2, mSensorIndex, pNvram->bss_iso2_clip_th2);
                    SET_CUST_MFLL_BSS(CLIP_TH3, mSensorIndex, pNvram->bss_iso2_clip_th3);
                    SET_CUST_MFLL_BSS(ADF_TH,   mSensorIndex, pNvram->bss_iso2_adf_th);
                    SET_CUST_MFLL_BSS(SDF_TH,   mSensorIndex, pNvram->bss_iso2_sdf_th);
                } else if (mMfnrDecisionIso < static_cast<MINT32>(pNvram->bss_iso_th3)) {
                    SET_CUST_MFLL_BSS(CLIP_TH0, mSensorIndex, pNvram->bss_iso3_clip_th0);
                    SET_CUST_MFLL_BSS(CLIP_TH1, mSensorIndex, pNvram->bss_iso3_clip_th1);
                    SET_CUST_MFLL_BSS(CLIP_TH2, mSensorIndex, pNvram->bss_iso3_clip_th2);
                    SET_CUST_MFLL_BSS(CLIP_TH3, mSensorIndex, pNvram->bss_iso3_clip_th3);
                    SET_CUST_MFLL_BSS(ADF_TH,   mSensorIndex, pNvram->bss_iso3_adf_th);
                    SET_CUST_MFLL_BSS(SDF_TH,   mSensorIndex, pNvram->bss_iso3_sdf_th);
                } else {
                    SET_CUST_MFLL_BSS(CLIP_TH0, mSensorIndex, pNvram->bss_iso4_clip_th0);
                    SET_CUST_MFLL_BSS(CLIP_TH1, mSensorIndex, pNvram->bss_iso4_clip_th1);
                    SET_CUST_MFLL_BSS(CLIP_TH2, mSensorIndex, pNvram->bss_iso4_clip_th2);
                    SET_CUST_MFLL_BSS(CLIP_TH3, mSensorIndex, pNvram->bss_iso4_clip_th3);
                    SET_CUST_MFLL_BSS(ADF_TH,   mSensorIndex, pNvram->bss_iso4_adf_th);
                    SET_CUST_MFLL_BSS(SDF_TH,   mSensorIndex, pNvram->bss_iso4_sdf_th);
                }
                MY_LOGD3("%s: bss clip/adf/sdf apply nvram setting.", __FUNCTION__);
            }
        }
        pChunk = nullptr;
    } else {
        MY_LOGD3("use default values defined in custom/feature/mfnr/camera_custom_mfll.h");
    }
#endif

    ::memset(&p, 0x00, sizeof(BSS_PARAM_STRUCT));

    String8 str = String8::format("\n======= updateBssProcInfo start ======\n");
    p.BSS_ON            = MF_BSS_ON;
    p.BSS_VER           = MF_BSS_VER;
    p.BSS_ROI_WIDTH     = w;
    p.BSS_ROI_HEIGHT    = h;
    p.BSS_ROI_X0        = x;
    p.BSS_ROI_Y0        = y;
    p.BSS_SCALE_FACTOR  = GET_CUST_MFLL_BSS(SCALE_FACTOR);

    p.BSS_CLIP_TH0      = GET_CUST_MFLL_BSS(CLIP_TH0);
    p.BSS_CLIP_TH1      = GET_CUST_MFLL_BSS(CLIP_TH1);
    p.BSS_CLIP_TH2      = GET_CUST_MFLL_BSS(CLIP_TH2);
    p.BSS_CLIP_TH3      = GET_CUST_MFLL_BSS(CLIP_TH3);

    p.BSS_ZERO          = GET_CUST_MFLL_BSS(ZERO);
    p.BSS_FRAME_NUM     = frameNum;
    p.BSS_ADF_TH        = GET_CUST_MFLL_BSS(ADF_TH);
    p.BSS_SDF_TH        = GET_CUST_MFLL_BSS(SDF_TH);

    p.BSS_GAIN_TH0      = GET_CUST_MFLL_BSS(GAIN_TH0);
    p.BSS_GAIN_TH1      = GET_CUST_MFLL_BSS(GAIN_TH1);
    p.BSS_MIN_ISP_GAIN  = GET_CUST_MFLL_BSS(MIN_ISP_GAIN);
    p.BSS_LCSO_SIZE     = 0; // TODO: query lcso size for AE compensation

    p.BSS_YPF_EN        = GET_CUST_MFLL_BSS(YPF_EN);
    p.BSS_YPF_FAC       = GET_CUST_MFLL_BSS(YPF_FAC);
    p.BSS_YPF_ADJTH     = GET_CUST_MFLL_BSS(YPF_ADJTH);
    p.BSS_YPF_DFMED0    = GET_CUST_MFLL_BSS(YPF_DFMED0);
    p.BSS_YPF_DFMED1    = GET_CUST_MFLL_BSS(YPF_DFMED1);
    p.BSS_YPF_TH0       = GET_CUST_MFLL_BSS(YPF_TH0);
    p.BSS_YPF_TH1       = GET_CUST_MFLL_BSS(YPF_TH1);
    p.BSS_YPF_TH2       = GET_CUST_MFLL_BSS(YPF_TH2);
    p.BSS_YPF_TH3       = GET_CUST_MFLL_BSS(YPF_TH3);
    p.BSS_YPF_TH4       = GET_CUST_MFLL_BSS(YPF_TH4);
    p.BSS_YPF_TH5       = GET_CUST_MFLL_BSS(YPF_TH5);
    p.BSS_YPF_TH6       = GET_CUST_MFLL_BSS(YPF_TH6);
    p.BSS_YPF_TH7       = GET_CUST_MFLL_BSS(YPF_TH7);

    p.BSS_FD_EN         = GET_CUST_MFLL_BSS(FD_EN);
    p.BSS_FD_FAC        = GET_CUST_MFLL_BSS(FD_FAC);
    p.BSS_FD_FNUM       = GET_CUST_MFLL_BSS(FD_FNUM);

    p.BSS_EYE_EN        = GET_CUST_MFLL_BSS(EYE_EN);
    p.BSS_EYE_CFTH      = GET_CUST_MFLL_BSS(EYE_CFTH);
    p.BSS_EYE_RATIO0    = GET_CUST_MFLL_BSS(EYE_RATIO0);
    p.BSS_EYE_RATIO1    = GET_CUST_MFLL_BSS(EYE_RATIO1);
    p.BSS_EYE_FAC       = GET_CUST_MFLL_BSS(EYE_FAC);

    if (CC_UNLIKELY(getForceBss(reinterpret_cast<void*>(&p), sizeof(BSS_PARAM_STRUCT)))) {
        MY_LOGI("%s: force set BSS param as manual setting", __FUNCTION__);
    }

    str = str + String8::format("ON(%d) VER(%d) ROI(%d,%d, %dx%d) SCALE(%d)\n",
        p.BSS_ON, p.BSS_VER, p.BSS_ROI_X0, p.BSS_ROI_Y0, p.BSS_ROI_WIDTH, p.BSS_ROI_HEIGHT, p.BSS_SCALE_FACTOR
    );
    str = str + String8::format("CLIP(%d,%d,%d,%d)\n",
        p.BSS_CLIP_TH0, p.BSS_CLIP_TH1, p.BSS_CLIP_TH2, p.BSS_CLIP_TH3
    );
    str = str + String8::format("ZERO(%d) FRAME_NUM(%d) ADF_TH(%d) SDF_TH(%d)\n",
        p.BSS_ZERO, p.BSS_FRAME_NUM, p.BSS_ADF_TH, p.BSS_SDF_TH
    );
    str = str + String8::format("GAIN0(%d) GAIN1(%d) MIN_ISP_GAIN(%d) LCSO_SIZE(%d)\n",
        p.BSS_GAIN_TH0, p.BSS_GAIN_TH1, p.BSS_MIN_ISP_GAIN, p.BSS_LCSO_SIZE
    );
    str = str + String8::format("YPF: EN(%d) FAC(%d) ADJTH(%d) DFMED0(%d) DFMED1(%d)\n",
        p.BSS_YPF_EN, p.BSS_YPF_FAC, p.BSS_YPF_ADJTH, p.BSS_YPF_DFMED0, p.BSS_YPF_DFMED1
    );
    str = str + String8::format("YPF: TH(%d,%d,%d,%d,%d,%d,%d,%d)\n",
        p.BSS_YPF_TH0, p.BSS_YPF_TH1, p.BSS_YPF_TH2, p.BSS_YPF_TH3,
        p.BSS_YPF_TH4, p.BSS_YPF_TH5, p.BSS_YPF_TH6, p.BSS_YPF_TH7
    );
    str = str + String8::format("FD_EN(%d) BSS_FD_FAC(%d) BSS_FD_FNUM(%d)\n",
        p.BSS_FD_EN, p.BSS_FD_FAC, p.BSS_FD_FNUM
    );
    str = str + String8::format("EYE: EN(%d) CFTH(%d) RATIO0(%d) RATIO1(%d) FAC(%d)\n",
        p.BSS_EYE_EN, p.BSS_EYE_CFTH, p.BSS_EYE_RATIO0, p.BSS_EYE_RATIO1, p.BSS_EYE_FAC
    );
    str = str + String8::format("======= updateBssProcInfo end ======\n");
    MY_LOGD3("%s", str.string());

}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID RootNode::updateBssIOInfo(IImageBuffer* pBuf, BSS_INPUT_DATA_G& bss_input) const
{
    FUNCTION_SCOPE;

    memset(&bss_input, 0, sizeof(bss_input));

    IHalSensorList* sensorList = MAKE_HalSensorList();
    if(sensorList == NULL){
        MY_LOGE("get sensor Vector failed");
        return;
    }else{
        int sensorDev = sensorList->querySensorDevIdx(mSensorIndex);

        SensorStaticInfo sensorStaticInfo;
        sensorList->querySensorStaticInfo(sensorDev, &sensorStaticInfo);

        bss_input.BayerOrder = sensorStaticInfo.sensorFormatOrder;
        bss_input.Bitnum = [&]() -> MUINT32 {
            switch (sensorStaticInfo.rawSensorBit) {
                case RAW_SENSOR_8BIT:   return 8;
                case RAW_SENSOR_10BIT:  return 10;
                case RAW_SENSOR_12BIT:  return 12;
                case RAW_SENSOR_14BIT:  return 14;
                default:
                    MY_LOGE("get sensor raw bitnum failed");
                    return 0xFF;
            }
        }();
    }

    bss_input.Stride = pBuf->getBufStridesInBytes(0);
    bss_input.inWidth = pBuf->getImgSize().w;
    bss_input.inHeight = pBuf->getImgSize().h;

    String8 str = String8::format("\n======= updateBssIOInfo start ======\n");
    str = str + String8::format("BayerOrder(%d) Bitnum(%d) Stride(%d) Size(%dx%d)\n",
        bss_input.BayerOrder, bss_input.Bitnum, bss_input.Stride,
        bss_input.inWidth, bss_input.inHeight
    );

    str = str + String8::format("======= updateBssIOInfo end ======\n");
    MY_LOGD3("%s", str.string());
}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
RootNode::
appendBSSInput(Vector<RequestPtr>& rvRequests, BSS_INPUT_DATA_G& bss_input) const
{
    FUNCTION_SCOPE;

    MINT32 idx = 0;
    vector<int64_t> timestamps(rvRequests.size(), 0);

    MY_LOGD("appendBSSInput for (%zu) frames", rvRequests.size());
    for (size_t idx = 0; idx < rvRequests.size(); idx++) {
        RequestPtr& request = rvRequests.editItemAt(idx);

        MY_LOGD("idx(%d) reqNo(%d)", idx, request->getRequestNo());

        auto pInMetaHal = request->getMetadata(MID_MAN_IN_HAL);
        auto pInMetaApp = request->getMetadata(MID_MAN_IN_APP);
        auto pInBufRsz = request->getBuffer(BID_MAN_IN_RSZ);

        IMetadata* pHalMeta = pInMetaHal->native();
        IMetadata* pAppMeta = pInMetaApp->native();
        RootNode::GMV mv = calMotionVector(
            pHalMeta,
            (idx == 0) ? MTRUE : MFALSE
        );
        bss_input.gmv[idx].x = mv.x;
        bss_input.gmv[idx].y = mv.y;

        {
            struct T {
                int64_t val;
                MBOOL result;
                T() : val(-1), result(MFALSE) {};
            } timestamp;

#if MTK_CAM_DISPAY_FRAME_CONTROL_ON
            timestamp.result = NSCam::IMetadata::getEntry<int64_t>(pHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, timestamp.val);
#else
            timestamp.result = NSCam::IMetadata::getEntry<int64_t>(pAppMeta, MTK_SENSOR_TIMESTAMP, timestamp.val);
#endif

            MY_LOGD3("%s:=========================", __FUNCTION__);
            MY_LOGD3("%s: Get timestamp -> %d, timestamp->: %" PRIi64, __FUNCTION__, timestamp.result, timestamp.val);
            MY_LOGD3("%s:=========================", __FUNCTION__);

            timestamps[idx] = timestamp.val;
        }

        sp<IImageBuffer> pBuf_rs_raw = pInBufRsz->native();
        CHECK_OBJECT(pBuf_rs_raw);

        bss_input.apbyBssInImg[idx] = (MUINT8*)pBuf_rs_raw->getBufVA(0);

        MY_LOGD("gmv(%d,%d) pBuf(%p) ts(%zu)",
            bss_input.gmv[idx].x, bss_input.gmv[idx].y,
            bss_input.apbyBssInImg[idx],
            timestamps[idx]
        );
    }

    /* set fd info */
    {
        // 1. create IFDContainer instance
        auto fdReader = IFDContainer::createInstance(LOG_TAG,  IFDContainer::eFDContainer_Opt_Read);

        // 2. query fd info by timestamps, fdData must be return after use
        auto fdData = fdReader->queryLock(timestamps);

        if (mfll::MfllProperty::readProperty(mfll::Property_BssFdDump) == 1)
            fdReader->dumpInfo();

        // 3. fill fd info to bss
        {
            if (CC_LIKELY( fdData.size() == rvRequests.size() )) {
                for (size_t idx = 0 ; idx < fdData.size() ; idx++) {
                    if (fdData[idx] != nullptr)
                        bss_input.Face[idx] = &fdData[idx]->facedata;
                    else
                        bss_input.Face[idx] = nullptr;
                }
            }
            else {
                MY_LOGE("%s: query fdData size is not sync. input_ts(%zu), query(%zu), expect(%zu)", __FUNCTION__, timestamps.size(), fdData.size(), rvRequests.size());
            }
        }
#if (MFLL_MF_TAG_VERSION == 9)
        // 4. fill fd rect0 to exif
        {
            MINT32 reqId = rvRequests[0]->getRequestNo();

            ExifWriter writer(PIPE_CLASS_TAG);

            for (size_t i = 0; i < fdData.size(); i++) {
                if (fdData[i] != nullptr) {
                    writer.sendData(reqId, MF_TAG_FD_RECT0_X0_00 + i*MFLLBSS_FD_RECT0_PER_FRAME, (uint32_t)fdData[i]->faces[0].rect[0]);
                    writer.sendData(reqId, MF_TAG_FD_RECT0_Y0_00 + i*MFLLBSS_FD_RECT0_PER_FRAME, (uint32_t)fdData[i]->faces[0].rect[1]);
                    writer.sendData(reqId, MF_TAG_FD_RECT0_X1_00 + i*MFLLBSS_FD_RECT0_PER_FRAME, (uint32_t)fdData[i]->faces[0].rect[2]);
                    writer.sendData(reqId, MF_TAG_FD_RECT0_Y1_00 + i*MFLLBSS_FD_RECT0_PER_FRAME, (uint32_t)fdData[i]->faces[0].rect[3]);
                }
            }
        }
#endif
        // 5. fdData must be return after use
        fdReader->queryUnlock(fdData);
    }

    return MTRUE;
}
/*******************************************************************************
 *
 ********************************************************************************/
std::shared_ptr<char>
RootNode::getNvramChunk(size_t *bufferSize) const
{
    FUNCTION_SCOPE;

    Mutex::Autolock _l(mNvramChunkLock);
#ifdef MTK_CAM_NEW_NVRAM_SUPPORT
    size_t chunkSize = sizeof(NVRAM_CAMERA_FEATURE_MFLL_STRUCT);
    std::shared_ptr<char> chunk(new char[chunkSize]);
    NVRAM_CAMERA_FEATURE_STRUCT *pNvram;
    MUINT sensorDev = SENSOR_DEV_NONE;

    if (bufferSize)
        *bufferSize = 0;

    {
        IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
        if (pHalSensorList == NULL) {
            MY_LOGE("get IHalSensorList instance failed");
            return nullptr;
        }
        sensorDev = pHalSensorList->querySensorDevIdx(mSensorIndex);
    }

    auto pNvBufUtil = MAKE_NvBufUtil();
    if (pNvBufUtil == NULL) {
        MY_LOGE("pNvBufUtil==0");
        return nullptr;
    }
    auto result = pNvBufUtil->getBufAndRead(
            CAMERA_NVRAM_DATA_FEATURE,
            sensorDev, (void*&)pNvram);
    if (result != 0) {
        MY_LOGE("read buffer chunk fail");
        return nullptr;
    }

    memcpy((void*)chunk.get(), (void*)&pNvram->MFNR[mMfnrQueryIndex], chunkSize);
    MY_LOGD3("%s: read NVRAM_CAMERA_FEATURE_MFLL_STRUCT, size=%zu(byte)",
            __FUNCTION__, chunkSize);

    if (bufferSize)
        *bufferSize = chunkSize;

    return chunk;
#else
    MY_LOGD3("Not support for NVRAM 2.0");
    return nullptr;
#endif
}
/*******************************************************************************
 *
 ********************************************************************************/

MBOOL
RootNode::getForceBss(void* param_addr, size_t param_size) const
{
    FUNCTION_SCOPE;

    if ( param_size != sizeof(BSS_PARAM_STRUCT)) {
        MY_LOGE("%s: invalid sizeof param, param_size:%d, sizeof(BSS_PARAM_STRUCT):%d",
                 __FUNCTION__, param_size, sizeof(BSS_PARAM_STRUCT));
        return false;
    }

    int r = 0;
    bool isForceBssSetting = false;
    BSS_PARAM_STRUCT* param = reinterpret_cast<BSS_PARAM_STRUCT*>(param_addr);

    r = mfll::MfllProperty::readProperty(mfll::Property_BssOn);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ON = %d (original:%d)", __FUNCTION__, r, param->BSS_ON);
        param->BSS_ON = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssRoiWidth);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ROI_WIDTH = %d (original:%d)", __FUNCTION__, r, param->BSS_ROI_WIDTH);
        param->BSS_ROI_WIDTH = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssRoiHeight);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ROI_HEIGHT = %d (original:%d)", __FUNCTION__, r, param->BSS_ROI_HEIGHT);
        param->BSS_ROI_HEIGHT = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssRoiX0);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ROI_X0 = %d (original:%d)", __FUNCTION__, r, param->BSS_ROI_X0);
        param->BSS_ROI_X0 = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssRoiY0);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ROI_Y0 = %d (original:%d)", __FUNCTION__, r, param->BSS_ROI_Y0);
        param->BSS_ROI_Y0 = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssScaleFactor);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_SCALE_FACTOR = %d (original:%d)", __FUNCTION__, r, param->BSS_SCALE_FACTOR);
        param->BSS_SCALE_FACTOR = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssClipTh0);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_CLIP_TH0 = %d (original:%d)", __FUNCTION__, r, param->BSS_CLIP_TH0);
        param->BSS_CLIP_TH0 = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssClipTh1);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_CLIP_TH1 = %d (original:%d)", __FUNCTION__, r, param->BSS_CLIP_TH1);
        param->BSS_CLIP_TH1 = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssZero);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ZERO = %d (original:%d)", __FUNCTION__, r, param->BSS_ZERO);
        param->BSS_ZERO = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssAdfTh);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_ADF_TH = %d (original:%d)", __FUNCTION__, r, param->BSS_ADF_TH);
        param->BSS_ADF_TH = r;
        isForceBssSetting = true;
    }

    r = mfll::MfllProperty::readProperty(mfll::Property_BssSdfTh);
    if (r != -1) {
        MY_LOGI("%s: Force BSS_SDF_TH = %d (original:%d)", __FUNCTION__, r, param->BSS_SDF_TH);
        param->BSS_SDF_TH = r;
        isForceBssSetting = true;
    }

    return isForceBssSetting;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
RootNode::
dumpBssInputData2File(RequestPtr& firstRequest, BSS_INPUT_DATA_G& bss_input) const
{
    FUNCTION_SCOPE;

    auto pInMetaHal = firstRequest->getMetadata(MID_MAN_IN_HAL);
    IMetadata* pHalMeta = pInMetaHal->native();

    // dump info
    MINT32 uniqueKey = 0;
    MINT32 frameNum  = 0;
    MINT32 requestNo = 0;

    if (!IMetadata::getEntry<MINT32>(pHalMeta, MTK_PIPELINE_UNIQUE_KEY, uniqueKey)) {
        MY_LOGW("get MTK_PIPELINE_UNIQUE_KEY failed, set to 0");
    }
    if (!IMetadata::getEntry<MINT32>(pHalMeta, MTK_PIPELINE_FRAME_NUMBER, frameNum)) {
        MY_LOGW("get MTK_PIPELINE_FRAME_NUMBER failed, set to 0");
    }
    if (!IMetadata::getEntry<MINT32>(pHalMeta, MTK_PIPELINE_REQUEST_NUMBER, requestNo)) {
        MY_LOGW("get MTK_PIPELINE_REQUEST_NUMBER failed, set to 0");
    }

    char filepath[256] = {0};
    snprintf(filepath, sizeof(filepath)-1, "%s/%09d-%04d-%04d-""%s", MFLLBSS_DUMP_PATH, uniqueKey, requestNo, frameNum, MFLLBSS_DUMP_FILENAME);

#if 1
        MY_LOGE("TODO: please fix it. <BSS dump inputdata 2 File %s>", filepath);
#endif

}
/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
RootNode::
doBss(Vector<RequestPtr>& rvReadyRequests)
{
    FUNCTION_SCOPE;

    MBOOL ret = MFALSE;
    MINT32 const mainReqId = rvReadyRequests[0]->getRequestNo();
    size_t frameNumToSkip = 0;

    CAM_TRACE_FMT_BEGIN("doBss req(%d)", mainReqId);

    MINT32 frameNum = rvReadyRequests.size();
    MTKBss *mtkBss = NULL;
    BSS_PARAM_STRUCT bss_param;
    BSS_WB_STRUCT workingBufferInfo;
    std::unique_ptr<MUINT8[]> bss_working_buffer;
    Vector<RequestPtr> vOrderedRequests;


    // TODO: check it for HALv3
    //handleData(SHUTTER, rvReadyRequests.front());


    auto pInBufRsz =rvReadyRequests[0]->getBuffer(BID_MAN_IN_RSZ);

    // main frame's input, rrzo
    sp<IImageBuffer> pBuf_rs_raw_1 = pInBufRsz->native();
    CHECK_OBJECT(pBuf_rs_raw_1);
#if MTK_CAM_NEW_NVRAM_SUPPORT
    // 0. Prepare NVRAM
    {
        // Need to use same index as MFNR if MTK_FEATURE_MFNR_QUERY_INDEX is set
        auto pInMetaHal = rvReadyRequests.editItemAt(0)->getMetadata(MID_MAN_IN_HAL);
        IMetadata* pHalMeta = pInMetaHal->native();

        mMfnrQueryIndex = -1;
        mMfnrDecisionIso = -1;

        IMetadata::getEntry<MINT32>(pHalMeta, MTK_FEATURE_MFNR_NVRAM_QUERY_INDEX, mMfnrQueryIndex);
        IMetadata::getEntry<MINT32>(pHalMeta, MTK_FEATURE_MFNR_NVRAM_DECISION_ISO, mMfnrDecisionIso);

        MY_LOGD("get MTK_FEATURE_MFNR_NVRAM_QUERY_INDEX = %d and MTK_FEATURE_MFNR_NVRAM_DECISION_ISO = %d", mMfnrQueryIndex, mMfnrDecisionIso);
    }
#else
    MY_LOGD("use default values defined in custom/feature/mfnr/camera_custom_mfll.h");
#endif

    // 1. Create MTK BSS Algo
    mtkBss = MTKBss::createInstance(DRV_BSS_OBJ_SW);
    if (mtkBss == NULL) {
        MY_LOGE("%s: create MTKBss instance failed", __FUNCTION__);
        goto lbExit;
    }

    // 2. mtkBss->Init
    {
        // thread priority usage
        int _priority = 0;
        int _oripriority = 0;
        int _result = 0;

        // change the current thread's priority, the algorithm threads will inherits
        // this value.
        _priority = mfll::MfllProperty::readProperty(mfll::Property_AlgoThreadsPriority, MFLL_ALGO_THREADS_PRIORITY);
        _oripriority = 0;
        _result = Utils::setThreadPriority(_priority, _oripriority);
        if (CC_UNLIKELY( _result != true )) {
            MY_LOGW("set algo threads priority failed(err=%d)", _result);
        }
        else {
            MY_LOGD3("set algo threads priority to %d", _priority);
        }

        if (mtkBss->BssInit(NULL, NULL) != S_BSS_OK) {
            MY_LOGE("%s: init MTKBss failed", __FUNCTION__);
            goto lbExit;
        }

        // algorithm threads have been forked,
        // if priority set OK, reset it back to the original one
        if (CC_LIKELY( _result == true )) {
            _result = Utils::setThreadPriority( _oripriority, _oripriority );
            if (CC_UNLIKELY( _result != true )) {
                MY_LOGE("set priority back failed, weird!");
            }
        }
    }

    // 3. mtkBss->BssFeatureCtrl Setup working buffer
    {
        workingBufferInfo.rProcId    = BSS_PROC2;
        workingBufferInfo.u4Width    = pBuf_rs_raw_1->getImgSize().w;
        workingBufferInfo.u4Height   = pBuf_rs_raw_1->getImgSize().h;
        workingBufferInfo.u4FrameNum = frameNum;
        workingBufferInfo.u4WKSize   = 0; //it will return working buffer require size
        workingBufferInfo.pu1BW      = nullptr; // assign working buffer latrer.

        auto b = mtkBss->BssFeatureCtrl(BSS_FTCTRL_GET_WB_SIZE, (void*)&workingBufferInfo, NULL);
        if (b != S_BSS_OK) {
            MY_LOGE("get working buffer size from MTKBss failed (%d)", (int)b);
            goto lbExit;
        }
        if (workingBufferInfo.u4WKSize <= 0) {
            MY_LOGE("unexpected bss working buffer size: %u", workingBufferInfo.u4WKSize);
            goto lbExit;
        }

        bss_working_buffer = std::unique_ptr<MUINT8[]>(new MUINT8[workingBufferInfo.u4WKSize]{0});
        workingBufferInfo.pu1BW = bss_working_buffer.get(); // assign working buffer for bss algo.

        /* print out bss working buffer information */
        MY_LOGD3("%s: rProcId    = %d", __FUNCTION__, workingBufferInfo.rProcId);
        MY_LOGD3("%s: u4Width    = %u", __FUNCTION__, workingBufferInfo.u4Width);
        MY_LOGD3("%s: u4Height   = %u", __FUNCTION__, workingBufferInfo.u4Height);
        MY_LOGD3("%s: u4FrameNum = %u", __FUNCTION__, workingBufferInfo.u4FrameNum);
        MY_LOGD3("%s: u4WKSize   = %u", __FUNCTION__, workingBufferInfo.u4WKSize);
        MY_LOGD3("%s: pu1BW      = %p", __FUNCTION__, workingBufferInfo.pu1BW);


        b = mtkBss->BssFeatureCtrl(BSS_FTCTRL_SET_WB_SIZE, (void*)&workingBufferInfo, NULL);
        if (b != S_BSS_OK) {
            MY_LOGE("set working buffer to MTKBss failed, size=%d (%u)",(int)b, workingBufferInfo.u4WKSize);
            goto lbExit;
        }
    }

    {
        updateBssProcInfo(pBuf_rs_raw_1.get(), bss_param, frameNum);

        auto b = mtkBss->BssFeatureCtrl(BSS_FTCTRL_SET_PROC_INFO, (void*)&bss_param, NULL);
        if (b != S_BSS_OK) {
            MY_LOGE("et info to MTKBss failed (%d)", (int)b);
            goto lbExit;
        }
    }

    {
        BSS_INPUT_DATA_G bssInData;
        BSS_OUTPUT_DATA bssOutData;

        memset(&bssInData, 0, sizeof(bssInData));
        memset(&bssOutData, 0, sizeof(bssOutData));

        updateBssIOInfo(pBuf_rs_raw_1.get(), bssInData);
        appendBSSInput(rvReadyRequests, bssInData);

        collectPreBSSExifData(rvReadyRequests, bss_param, bssInData);

        // dump bss input info to text file for bss simulation
        if (mfll::MfllProperty::readProperty(mfll::Property_DumpRaw) == 1)
            dumpBssInputData2File(rvReadyRequests.editItemAt(0), bssInData);

        auto b = mtkBss->BssMain(BSS_PROC2, &bssInData, &bssOutData);
        if (b != S_BSS_OK) {
            MY_LOGE("MTKBss::Main returns failed (%d)", (int)b);
            goto lbExit;
        }

        {
            // check frame to skip
            frameNumToSkip = bssOutData.i4SkipFrmCnt;

            // due to adb force drop
            int forceDropNum = mfll::MfllProperty::getDropNum();
            if (CC_UNLIKELY(forceDropNum > -1)) {
                MY_LOGD("%s: force drop frame count = %d, original bss drop frame = %zu",
                        __FUNCTION__, forceDropNum, frameNumToSkip);
                frameNumToSkip = static_cast<size_t>(forceDropNum);
            }

            // due to capture M and blend N
            auto pInMetaHal = rvReadyRequests.editItemAt(0)->getMetadata(MID_MAN_IN_HAL);
            IMetadata* pHalMeta = pInMetaHal->native();
            MINT32 selectedBssCount = rvReadyRequests.size();
            if (CC_LIKELY( IMetadata::getEntry<MINT32>(pHalMeta, MTK_FEATURE_BSS_SELECTED_FRAME_COUNT, selectedBssCount) )) {
                int frameNumToSkipBase = static_cast<int>(rvReadyRequests.size()) - static_cast<int>(selectedBssCount);
                if (CC_UNLIKELY(frameNumToSkip < frameNumToSkipBase)) {
                    MY_LOGD("%s: update drop frame count = %d, original drop frame = %zu",
                            __FUNCTION__, frameNumToSkipBase, frameNumToSkip);

                    frameNumToSkip = static_cast<size_t>(frameNumToSkipBase);
                }
            }
        }

        Vector<MINT32> vNewOrdering;
        MUINT32 order;
//TODO: For Testing
#if 1
        for (size_t i = 0; i < rvReadyRequests.size(); i++) {
            order = bssOutData.originalOrder[i];
            MY_LOGD("BSS output - order(%d)", order);
            vOrderedRequests.push_back(rvReadyRequests[order]);
            vNewOrdering.push_back(order);
        }
#else
        frameNumToSkip = 0;
        for (size_t i = 0; i < rvReadyRequests.size(); i++) {
            order = (i+1)%rvReadyRequests.size();
            MY_LOGD("Fake BSS output - order(%d)", order);
            vOrderedRequests.push_back(rvReadyRequests[order]);
            vNewOrdering.push_back(order);
        }
#endif

        collectPostBSSExifData(rvReadyRequests, vNewOrdering, bssOutData);

        ExifWriter writer(PIPE_CLASS_TAG);
        writer.makeExifFromCollectedData(rvReadyRequests[bssOutData.originalOrder[0]]);

        // push Bss result into hal meta, all request have same information:
        // 1. MTK_STEREO_FRAME_PER_CAPTURE
        // 2. MTK_STEREO_BSS_RESULT: the result info set containing 3 integer:
        //                           1.original order
        //                           2.gmv.x
        //                           3.gmv.y
        for(auto &e : rvReadyRequests){
            auto pInMetaHal = e->getMetadata(MID_MAN_IN_HAL);
            IMetadata* pHalMeta = pInMetaHal->native();
            CHECK_OBJECT(pHalMeta);

            // frame per capture
//TODO: check
#if 0
            IMetadata::IEntry entry(MTK_STEREO_FRAME_PER_CAPTURE);
            entry.push_back(vOrderedRequests.size(), Type2Type<MINT32>());
            pHalMeta->update(entry.tag(), entry);

            // bss result
            IMetadata::IEntry entry2(MTK_STEREO_BSS_RESULT);
            for(int bssIdx=0 ; bssIdx<vOrderedRequests.size() ; bssIdx++){
                entry2.push_back(bssOutData.originalOrder[bssIdx], Type2Type<MINT32>());
                entry2.push_back(bssOutData.gmv[bssIdx].x, Type2Type<MINT32>());
                entry2.push_back(bssOutData.gmv[bssIdx].y, Type2Type<MINT32>());
            }
            pHalMeta->update(entry2.tag(), entry2);

            // enable mfb, must be 1
            IMetadata::IEntry entry3(MTK_STEREO_ENABLE_MFB);
            entry3.push_back(1, Type2Type<MINT32>());
            pHalMeta->update(entry3.tag(), entry3);
            // update feature mode to mfnr+bokeh
            IMetadata::IEntry entry4(MTK_STEREO_DCMF_FEATURE_MODE);
            entry4.push_back(MTK_DCMF_FEATURE_MFNR_BOKEH, Type2Type<MINT32>());
            pHalMeta->update(entry4.tag(), entry4);
#endif
        }
    }


    ret = MTRUE;

lbExit:
    if (mtkBss)
        mtkBss->destroyInstance();

    if (mDebugBSS == 0)
        reorder(rvReadyRequests, rvReadyRequests, frameNumToSkip);
    else if (!ret)
        reorder(rvReadyRequests, rvReadyRequests, frameNumToSkip);
    else
        reorder(rvReadyRequests, vOrderedRequests, frameNumToSkip);
    CAM_TRACE_FMT_END();

    return ret;
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID
RootNode::
collectPreBSSExifData(
    Vector<RequestPtr>& rvReadyRequests,
    BSS_PARAM_STRUCT& p,
    BSS_INPUT_DATA_G& bss_input) const
{
    FUNCTION_SCOPE;

    (void)rvReadyRequests;
    (void)p;
    (void)bss_input;

    MINT32 reqId = rvReadyRequests[0]->getRequestNo();

    ExifWriter writer(PIPE_CLASS_TAG);


        /* update debug info */
#if (MFLL_MF_TAG_VERSION > 0)
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ENABLE             ,(uint32_t)mfll::MfllProperty::getBss());
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ROI_WIDTH          ,(uint32_t)p.BSS_ROI_WIDTH      );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ROI_HEIGHT         ,(uint32_t)p.BSS_ROI_HEIGHT     );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_SCALE_FACTOR       ,(uint32_t)p.BSS_SCALE_FACTOR   );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_CLIP_TH0           ,(uint32_t)p.BSS_CLIP_TH0       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_CLIP_TH1           ,(uint32_t)p.BSS_CLIP_TH1       );

#   if (MFLL_MF_TAG_VERSION == 9)
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_CLIP_TH2           ,(uint32_t)p.BSS_CLIP_TH2       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_CLIP_TH3           ,(uint32_t)p.BSS_CLIP_TH3       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_FRAME_NUM          ,(uint32_t)p.BSS_FRAME_NUM      );
#   endif

    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ZERO               ,(uint32_t)p.BSS_ZERO           );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ROI_X0             ,(uint32_t)p.BSS_ROI_X0         );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ROI_Y0             ,(uint32_t)p.BSS_ROI_Y0         );

#   if (MFLL_MF_TAG_VERSION >= 3)
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ADF_TH             ,(uint32_t)p.BSS_ADF_TH         );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_SDF_TH             ,(uint32_t)p.BSS_SDF_TH         );
#   endif

#   if (MFLL_MF_TAG_VERSION == 9)
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ON                 ,(uint32_t)p.BSS_ON             );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_VER                ,(uint32_t)p.BSS_VER            );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_GAIN_TH0           ,(uint32_t)p.BSS_GAIN_TH0       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_GAIN_TH1           ,(uint32_t)p.BSS_GAIN_TH1       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_MIN_ISP_GAIN       ,(uint32_t)p.BSS_MIN_ISP_GAIN   );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_LCSO_SIZE          ,(uint32_t)p.BSS_LCSO_SIZE      );
    /* YPF info */
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_EN             ,(uint32_t)p.BSS_YPF_EN         );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_FAC            ,(uint32_t)p.BSS_YPF_FAC        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_ADJTH          ,(uint32_t)p.BSS_YPF_ADJTH      );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_DFMED0         ,(uint32_t)p.BSS_YPF_DFMED0     );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_DFMED1         ,(uint32_t)p.BSS_YPF_DFMED1     );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH0            ,(uint32_t)p.BSS_YPF_TH0        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH1            ,(uint32_t)p.BSS_YPF_TH1        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH2            ,(uint32_t)p.BSS_YPF_TH2        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH3            ,(uint32_t)p.BSS_YPF_TH3        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH4            ,(uint32_t)p.BSS_YPF_TH4        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH5            ,(uint32_t)p.BSS_YPF_TH5        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH6            ,(uint32_t)p.BSS_YPF_TH6        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_YPF_TH7            ,(uint32_t)p.BSS_YPF_TH7        );
    /* FD & eye info*/
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_FD_EN              ,(uint32_t)p.BSS_FD_EN          );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_FD_FAC             ,(uint32_t)p.BSS_FD_FAC         );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_FD_FNUM            ,(uint32_t)p.BSS_FD_FNUM        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_EYE_EN             ,(uint32_t)p.BSS_EYE_EN         );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_EYE_CFTH           ,(uint32_t)p.BSS_EYE_CFTH       );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_EYE_RATIO0         ,(uint32_t)p.BSS_EYE_RATIO0     );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_EYE_RATIO1         ,(uint32_t)p.BSS_EYE_RATIO1     );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_EYE_FAC            ,(uint32_t)p.BSS_EYE_FAC        );
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_AEVC_EN            ,(uint32_t)p.BSS_AEVC_EN        );
#   endif
#endif

    auto makeGmv32bits = [](short x, short y){
        return (uint32_t) y << 16 | (x & 0x0000FFFF);
    };
    for(auto i=0 ; i<rvReadyRequests.size() ; i++){
        if(i >= MAX_GMV_CNT){
            MY_LOGE("gmv count exceeds limitatin(%d)!", MAX_GMV_CNT);
            break;
        }
#if (MFLL_MF_TAG_VERSION > 0)
        writer.sendData(reqId,
            (unsigned int)MF_TAG_GMV_00 + i,
            (uint32_t)makeGmv32bits((short)bss_input.gmv[i].x, (short)bss_input.gmv[i].y)
        );
#endif
    }

    // set all sub requests belong to main request
    set<MINT32> mappingSet;
    for(auto &e : rvReadyRequests){
        mappingSet.insert(e->getRequestNo());
    }
    writer.addReqMapping(reqId, mappingSet);
}
/*******************************************************************************
 *
 ********************************************************************************/
MVOID RootNode::collectPostBSSExifData(
    Vector<RequestPtr>& rvReadyRequests,
    Vector<MINT32>& vNewIndex,
    BSS_OUTPUT_DATA& bss_output)
{
    FUNCTION_SCOPE;

    (void)rvReadyRequests;
    (void)vNewIndex;
    (void)bss_output;

    MINT32 reqId = rvReadyRequests[0]->getRequestNo();
    ExifWriter writer(PIPE_CLASS_TAG);

#if (MFLL_MF_TAG_VERSION == 9)
    /* bss result score */
    const size_t dbgIdxBssScoreCount = 8; // only 8 scores
    size_t dbgIdxBssScoreMSB = static_cast<size_t>(MF_TAG_BSS_FINAL_SCORE_00_MSB);
    size_t dbgIdxBssScoreLSB = static_cast<size_t>(MF_TAG_BSS_FINAL_SCORE_00_LSB);
    size_t dbgIdxBssSharpScoreMSB = static_cast<size_t>(MF_TAG_BSS_SHARP_SCORE_00_MSB);
    size_t dbgIdxBssSharpScoreLSB = static_cast<size_t>(MF_TAG_BSS_SHARP_SCORE_00_LSB);
#endif

    for (size_t i = 0; i < rvReadyRequests.size(); i++) {
        MY_LOGD("%s: SharpScore[%d]  = %lld", __FUNCTION__, i, bss_output.SharpScore[i]);
        MY_LOGD("%s: adj1_score[%d]  = %lld", __FUNCTION__, i, bss_output.adj1_score[i]);
        MY_LOGD("%s: adj2_score[%d]  = %lld", __FUNCTION__, i, bss_output.adj2_score[i]);
        MY_LOGD("%s: adj3_score[%d]  = %lld", __FUNCTION__, i, bss_output.adj3_score[i]);
        MY_LOGD("%s: final_score[%d] = %lld", __FUNCTION__, i, bss_output.final_score[i]);

#if (MFLL_MF_TAG_VERSION == 9)
        /* update final scores */
        if (__builtin_expect( i < dbgIdxBssScoreCount, true )) {
            const long long mask32bits = 0x00000000FFFFFFFF;
            writer.sendData(reqId, dbgIdxBssScoreMSB, (bss_output.final_score[i] >> 32) & mask32bits);
            writer.sendData(reqId, dbgIdxBssScoreLSB, bss_output.final_score[i] & mask32bits);
            writer.sendData(reqId, dbgIdxBssSharpScoreMSB, (bss_output.SharpScore[i] >> 32) & mask32bits);
            writer.sendData(reqId, dbgIdxBssSharpScoreLSB, bss_output.SharpScore[i] & mask32bits);
        }
        dbgIdxBssScoreMSB++;
        dbgIdxBssScoreLSB++;
        dbgIdxBssSharpScoreMSB++;
        dbgIdxBssSharpScoreLSB++;
#endif
    }


#if (MFLL_MF_TAG_VERSION > 0)
#   if (MFLL_MF_TAG_VERSION == 9)
    // bss order
    {
        // encoding for bss order
        /** MF_TAG_BSS_ORDER_IDX
         *
         *  BSS order for top 8 frames (MSB -> LSB)
         *
         *  |     4       |     4       |     4       |     4       |     4       |     4       |     4       |     4       |
         *  | bssOrder[0] | bssOrder[1] | bssOrder[2] | bssOrder[3] | bssOrder[4] | bssOrder[5] | bssOrder[6] | bssOrder[7] |
         */
        uint32_t bssOrder = 0;
        size_t i = 0;

        for ( ; i < vNewIndex.size() && i < 8 ; i++)
            bssOrder = (bssOrder << 4) | ((uint32_t)vNewIndex[i]<0xf?(uint32_t)vNewIndex[i]:0xf);
        for ( ; i < 8 ; i++)
            bssOrder = (bssOrder << 4) | 0xf;

        writer.sendData(reqId, (unsigned int)MF_TAG_BSS_ORDER_IDX           ,bssOrder                     );
    }
#   endif
    writer.sendData(reqId, (unsigned int)MF_TAG_BSS_BEST_IDX            ,(uint32_t)vNewIndex[0]        );
#endif

}
#endif //SUPPORT_MFNR
/*******************************************************************************
 *
 ********************************************************************************/
MVOID RootNode::reorder(
    Vector<RequestPtr>& rvRequests, Vector<RequestPtr>& rvOrderedRequests, size_t i4SkipFrmCnt)
{
    FUNCTION_SCOPE;

    if (rvRequests.size() != rvOrderedRequests.size()) {
        MY_LOGE("input(%zu) != result(%zu)", rvRequests.size(), rvOrderedRequests.size());
        return;
    }

    MY_LOGE("TODO: please fix it. <i4SkipFrmCnt = %zu>", i4SkipFrmCnt);


    // Switch input buffers with each other. To keep the first request's data path
    // Bind the life cycle of the request with bss input buffers to main request
    if (rvOrderedRequests[0] != rvRequests[0])
    {
        rvRequests[0]->setCrossRequest(rvOrderedRequests[0]);
        rvOrderedRequests[0]->setCrossRequest(rvRequests[0]);
    }

    size_t frameCount = rvOrderedRequests.size();
    MBOOL  bDrop;

    //
    if (i4SkipFrmCnt == frameCount) {
        i4SkipFrmCnt = frameCount - 1;
    }

#if 1 // REMOVE ME!!
    if (i4SkipFrmCnt + 1 >= frameCount) {
        MY_LOGW("frameCount %d, i4SkipFrmCnt %d", frameCount, i4SkipFrmCnt);
        i4SkipFrmCnt = frameCount - 2;
    }
#endif

    for (size_t i = 0; i < rvOrderedRequests.size(); i++) {

        rvOrderedRequests.editItemAt(i)->addParameter(PID_FRAME_DROP_COUNT, i4SkipFrmCnt);

        bDrop = (i + i4SkipFrmCnt >= frameCount);
        if (i == 0) {
            //PID_FRAME_INDEX = 0, no need to update
            onRequestFinish(rvRequests.editItemAt(0));

        } else if (rvOrderedRequests[i] == rvRequests[0]) {
            rvOrderedRequests.editItemAt(0)->addParameter(PID_FRAME_INDEX, i);
            if (bDrop) {
                rvOrderedRequests.editItemAt(0)->addParameter(PID_FRAME_DROP, 1);
            }
            onRequestFinish(rvOrderedRequests.editItemAt(0));
        } else {
            rvOrderedRequests.editItemAt(i)->addParameter(PID_FRAME_INDEX, i);
            if (bDrop) {
                rvOrderedRequests.editItemAt(i)->addParameter(PID_FRAME_DROP, 1);
            }
            onRequestFinish(rvOrderedRequests.editItemAt(i));
        }
    }
}

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
