/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

// Standard C header file
#include <stdarg.h>
#include <chrono>
#include <future>
#include <functional>
// Android system/core header file
#include <cutils/properties.h>
#include <utils/String8.h>
// mtkcam custom header file

// mtkcam global header file

// Module header file
#include <mtkcam/include/mtkcam/utils/metadata/IMetadata.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/drv/iopipe/SImager/IImageTransform.h>
#include <mtkcam/aaa/ICaptureNR.h>
// Local header file
#include "../CaptureFeaturePlugin.h"
#include "DepthNode.h"

// Logging
#define PIPE_CLASS_TAG "DepthNode"
#define PIPE_TRACE TRACE_DEPTH_NODE
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>


/*******************************************************************************
* MACRO Utilities Define.
********************************************************************************/
namespace { // anonymous namespace for MACRO function
using CamThread      = NSCam::NSCamFeature::NSFeaturePipe::CamThread;
using AutoObject     = std::unique_ptr<const char, std::function<void(const char*)>>;
using AutoDependency = std::unique_ptr<CamThread, std::function<MVOID(CamThread*)>>;
//
auto
createAutoScoper(const char* funcName) -> AutoObject
{
    CAM_LOGD("[%s] +", funcName);
    return AutoObject(funcName, [](const char* p)
    {
        CAM_LOGD("[%s] -", p);
    });
}
#define SCOPED_TRACER() auto scoped_tracer = ::createAutoScoper(__FUNCTION__)
//
auto
createAutoProfiler(const char* funcName, const char* fmt, ...) -> AutoObject
{
    static const MINT32 LENGTH = 512;

    char* pBuf = new char[LENGTH];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(pBuf, LENGTH, fmt, ap);
    va_end(ap);

    CAM_TRACE_BEGIN(pBuf);
    CAM_LOGD("[%s]%s +", funcName, pBuf);

    return AutoObject(pBuf, [funcName](const char* p)
    {
        CAM_LOGD("[%s]%s -", funcName, p);
        CAM_TRACE_END();

        delete[] p;
    });
}
#define AUTO_PROFILER(FMT, arg...) auto auto_profiler = ::createAutoProfiler(__FUNCTION__, FMT, ##arg);
//
auto
createAutoTimer(const char* funcName, const char* fmt, ...) -> AutoObject
{
    static const MINT32 LENGTH = 512;
    using Timing = std::chrono::time_point<std::chrono::high_resolution_clock>;
    using DuationTime = std::chrono::duration<float, std::milli>;
    //
    char* pBuf = new char[LENGTH];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(pBuf, LENGTH, fmt, ap);
    va_end(ap);
    //
    Timing startTime = std::chrono::high_resolution_clock::now();
    return AutoObject(pBuf, [funcName, startTime](const char* p)
    {
        Timing endTime = std::chrono::high_resolution_clock::now();
        DuationTime duationTime = endTime - startTime;
        CAM_LOGD("[%s]%s, elapsed(ms):%.4f", funcName, p, duationTime.count());

        delete[] p;
    });
}
#define AUTO_TIMER(FMT, arg...) auto auto_timer = ::createAutoTimer(__FUNCTION__, FMT, ##arg);
//
#define UNREFERENCED_PARAMETER(param) (param)
#define TO_STRING(val) (#val)
//
} // end anonymous namespace for MACRO function


/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {


/*******************************************************************************
* Used namespace.
********************************************************************************/
using namespace NSCam::NSPipelinePlugin;
using namespace NSIoPipe::NSSImager;


/*******************************************************************************
* Type Alias.
********************************************************************************/
//
using NodeRequestPtr    = sp<CaptureFeatureNodeRequest>;
//
using PluginPtr         = DepthNode::PluginPtr;
using PipeRequestPtr    = DepthNode::PipeRequestPtr;
using PluginRequestPtr  = DepthNode::PluginRequestPtr;
using BufferHandlePtr   = DepthNode::BufferHandlePtr;
using MetadataHandlePtr = DepthNode::MetadataHandlePtr;
//
using String8 = android::String8;


/*******************************************************************************
* Global Function.
*******************************************************************************/
static auto toString(IImageBuffer* pImgBuf) -> std::string;


/*******************************************************************************
* Class Define.
*******************************************************************************/
/**
 * @brief the depth offer interface
 */
class DepthInterface final: public DepthPlugin::IInterface
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthPlugin::IInterface Public Operators.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
public:
    auto offer(DepthPlugin::Selection& sel) -> MERROR override;

};
REGISTER_PLUGIN_INTERFACE(Depth, DepthInterface);
/**
 * @brief the depth offer interface
 */
class SwnrProcessor final
{
public:
    using Ptr = UniquePtr<SwnrProcessor>;

public:
    static inline auto createInstance(const std::string& processName, MINT32 openId) -> Ptr;

public:
    auto execute(IImageBuffer* pImgBuf, IMetadata* pIMetadata, sp<CaptureBufferPool> poolPtr) -> MERROR;

    auto waitExecuteDone() -> MERROR;

    ~SwnrProcessor();

private:
    SwnrProcessor(const std::string& name, MINT32 openId);

    SwnrProcessor(const SwnrProcessor& other) = delete;

private:
    auto onExecute(IImageBuffer* pImgBuf, IMetadata* pIMetadata, sp<CaptureBufferPool> poolPtr) -> MERROR;

private:
    using ImageTransformPtr  = UniquePtr<IImageTransform>;
    using ISwNRPtr           = UniquePtr<ISwNR>;

private:
    static inline auto getIsoValue(IMetadata* pIMetadata) -> MINT32;

    static inline auto createImageISwNRPtr(MINT32 openId) -> ISwNRPtr;

    static inline auto createImageTransformPtr() -> ImageTransformPtr;

    static inline auto imgCopy(IImageBuffer* pSrcImg, IImageBuffer* pDstImg) -> MBOOL;

private:
    const std::string       mProcessName;
    const MINT32            mOpenId;
    //
    std::future<MERROR>     mFuture;
    MBOOL                   mEnableDump;

private:
    // SWNR need this type of image format
    static const MUINT32 sSupportedImgFmt = eImgFmt_YV12;
};
/**
 * @brief utility class of the depth node
 */
class DepthNodeUtility final
{
public:
    DepthNodeUtility() = delete;

public:
    static auto createPluginRequestPtr(NodeID_T nodeId, PluginPtr& pluginPtr, PipeRequestPtr& pipeRequestPtr) -> PluginRequestPtr;

    static auto dump(IImageBuffer* pImgBuf, const std::string& fileName, MINT64 uniqueKey) -> MBOOL;

    static auto tryGetFeatureWarning(RequestPtr& pipeRequestPtr, MINT32& featureWarning) -> MBOOL;

    static auto getIsForceByPassFlow() -> MBOOL;

private:
    static auto createPluginRequestPtr(PluginPtr& pluginPtr, PipeRequestPtr& pipeRequestPtr) -> PluginRequestPtr;
};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Global Function Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
std::string
toString(IImageBuffer* pImgBuf)
{
    String8 ret = String8::format("[a:%p, si:%dx%d, st:%zu, f:%#09x, va:%p]",
        pImgBuf,
        pImgBuf->getImgSize().w, pImgBuf->getImgSize().h,
        pImgBuf->getBufStridesInBytes(0),
        pImgBuf->getImgFormat(),
        reinterpret_cast<void*>(pImgBuf->getBufVA(0)));
    return std::string(ret.string());
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthInterface Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
auto
DepthInterface::
offer(DepthPlugin::Selection& sel) -> MERROR
{
    sel.mIBufferFull
        .addSupportFormat(eImgFmt_NV21)
        .addSupportSize(eImgSize_Full);

    sel.mIBufferFull2
        .addSupportFormat(eImgFmt_NV21)
        .addSupportSize(eImgSize_Full);

    sel.mOBufferDepth
        .addSupportFormat(eImgFmt_Y8)
        .addSupportSize(eImgSize_Specified);

    return OK;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  SwnrProcessor Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
auto
SwnrProcessor::
createInstance(const std::string& processName, MINT32 openId) -> Ptr
{
    return Ptr(new SwnrProcessor(processName, openId), [](SwnrProcessor *p) -> MVOID
    {
        delete p;
    });
}

SwnrProcessor::
SwnrProcessor(const std::string& processName, MINT32 openId)
: mProcessName(processName)
, mOpenId(openId)
, mEnableDump(0)
{
    mEnableDump = ::property_get_int32("vendor.debug.camera.depthnode.preprocess.dump", mEnableDump);
    MY_LOGD("ctor, processName:%s, openId:%d, enableDump:%d, addr:%p",
        mProcessName.c_str(), mOpenId, mEnableDump, this);
}

SwnrProcessor::
~SwnrProcessor()
{
    MY_LOGD("dtor, processName:%s, openId:%d, addr:%p", mProcessName.c_str(), mOpenId, this);
    if ( mFuture.valid() )
    {
        MY_LOGW("some process is executing, wait done, processName:%s", mProcessName.c_str());
        MERROR ret = mFuture.get();
        MY_LOGW("legacy executed process wait done, processName:%s, ret:%d", mProcessName.c_str(), ret);
    }
}

auto
SwnrProcessor::
execute(IImageBuffer* pImgBuf, IMetadata* pIMetadata, sp<CaptureBufferPool> poolPtr) -> MERROR
{
    if ( mFuture.valid() )
    {
        MY_LOGW("please check, some process is executing, processName:%s", mProcessName.c_str());
        return INVALID_OPERATION;
    }
    //
    static MBOOL isSyncMode = MTRUE;
    //
    auto task = std::bind(&SwnrProcessor::onExecute, this, pImgBuf, pIMetadata, poolPtr);
    const std::launch lunchPolicy = (isSyncMode ? std::launch::async : std::launch::deferred);
    mFuture = std::async(lunchPolicy, task);
    return OK;
}

auto
SwnrProcessor::
waitExecuteDone() -> MERROR
{
    if ( !mFuture.valid() )
    {
        MY_LOGW("please check, there is no executed process to be waited, processName:%s", mProcessName.c_str());
        return INVALID_OPERATION;
    }
    return mFuture.get();
}

auto
SwnrProcessor::
onExecute(IImageBuffer* pImgBuf, IMetadata* pIMetadata, sp<CaptureBufferPool> poolPtr) -> MERROR
{
    SCOPED_TRACER();

    AUTO_PROFILER("preprocess swnr, processName:%s", mProcessName.c_str());
    AUTO_TIMER("profile swnr, processName:%s", mProcessName.c_str());

    const MSize imgSize = pImgBuf->getImgSize();
    const MINT inputImgFmt = pImgBuf->getImgFormat();
    const MBOOL isUsingWorkingBuf = (inputImgFmt != sSupportedImgFmt);
    MY_LOGD("decise buffer use, processName:%s, isUsingWorkingBuf:%d, inputImgFmt:%#09x, desirdImgFmt:%#09x",
        mProcessName.c_str(), isUsingWorkingBuf, inputImgFmt, sSupportedImgFmt);
    //
    const MINT64 uniqueKey = reinterpret_cast<MINT64>(pImgBuf);
    if ( mEnableDump )
    {
        const String8 dumpName = String8::format("before_swnr_%s", mProcessName.c_str());
        DepthNodeUtility::dump(pImgBuf, dumpName.string(), uniqueKey);
    }
    //
    SmartImageBuffer workingImgBufPtr = nullptr;
    if ( isUsingWorkingBuf )
    {
        workingImgBufPtr = poolPtr->getImageBuffer(imgSize.w, imgSize.h, sSupportedImgFmt);
        if ( workingImgBufPtr == nullptr )
        {
            MY_LOGW("failed to get workingBuf, processName:%s, fmt:%#09x", mProcessName.c_str(), inputImgFmt);
            return BAD_VALUE;
        }

        if ( !imgCopy(pImgBuf, workingImgBufPtr->mImageBuffer.get()) )
        {
            MY_LOGW("failed to copy image(inputBuf to workingBuf), processName:%s", mProcessName.c_str());
            return UNKNOWN_ERROR;
        }
    }
    // execute swnr
    {
        const MINT32 isoValue = getIsoValue(pIMetadata);
        //
        ISwNRPtr swnrPtr = createImageISwNRPtr(mOpenId);
        // TODO: let the swnrParam is correct
        ISwNR::SWNRParam swnrParam;
        swnrParam.isDualCam = MTRUE,
        swnrParam.iso = isoValue,
        swnrParam.isMfll = MFALSE;
        //
        IImageBuffer* pTmpImgBuf = (isUsingWorkingBuf ? workingImgBufPtr->mImageBuffer.get() : pImgBuf);
        MY_LOGW("execute swnr, processName:%s, iso:%d", mProcessName.c_str(), isoValue);
        if ( !swnrPtr->doSwNR(swnrParam, pTmpImgBuf) )
        {
             MY_LOGW("failed to execute swnr, processName:%s", mProcessName.c_str());
             return UNKNOWN_ERROR;
        }
    }
    //
    if ( isUsingWorkingBuf && !imgCopy(workingImgBufPtr->mImageBuffer.get(), pImgBuf) )
    {
        MY_LOGW("failed to copy image(workingBuf to inputBuf), processName:%s", mProcessName.c_str());
        return UNKNOWN_ERROR;
    }
    //
    if ( mEnableDump )
    {
        const String8 dumpName = String8::format("after_swnr_%s", mProcessName.c_str());
        DepthNodeUtility::dump(pImgBuf, dumpName.string(), uniqueKey);
    }
    return OK;
}

auto
SwnrProcessor::
getIsoValue(IMetadata* pMetaData) -> MINT32
{
    static const MINT32 defaultIsoValue = 100;
    IMetadata exifMeta;
    if ( !IMetadata::getEntry<IMetadata>(pMetaData, MTK_3A_EXIF_METADATA, exifMeta) )
    {
        MY_LOGW("failed to get 3A exif, tag:%d(%s)", MTK_3A_EXIF_METADATA, TO_STRING(MTK_3A_EXIF_METADATA));
        return defaultIsoValue;
    }
    //
    MINT32 ret = defaultIsoValue;
    if ( !IMetadata::getEntry<MINT32>(&exifMeta, MTK_3A_EXIF_AE_ISO_SPEED, ret) )
    {
        MY_LOGW("failed to get iso, tag:%d(%s)", MTK_3A_EXIF_AE_ISO_SPEED, TO_STRING(MTK_3A_EXIF_AE_ISO_SPEED));
        return defaultIsoValue;
    }
    return ret;
}

auto
SwnrProcessor::
createImageISwNRPtr(MINT32 openId) -> ISwNRPtr
{
    return ISwNRPtr(MAKE_SwNR(openId), [](ISwNR *p)
    {
        delete p;
    });
}

auto
DepthNodeUtility::
tryGetFeatureWarning(RequestPtr& pipeRequestPtr, MINT32& featureWarning) -> MBOOL
{
    const static MINT32 defaultValue = 0;
    featureWarning = defaultValue;
    //
    MINT32 featureWarningFromP1 = 0;
    {
        sp<MetadataHandle> inDynamicMetaPtr = pipeRequestPtr->getMetadata(MID_MAN_IN_P1_DYNAMIC);
        IMetadata* pInDynamicMeta = (inDynamicMetaPtr != nullptr) ? inDynamicMetaPtr->native() : nullptr;
        if( pInDynamicMeta == nullptr )
        {
            MY_LOGW("failed to get featureWarning, in-dynamic metadata is not existing");
            return MFALSE;
        }

        IMetadata::IEntry entry = pInDynamicMeta->entryFor(MTK_STEREO_FEATURE_WARNING);
        if( entry.isEmpty() )
        {
            MY_LOGW("failed to get metadata from in-dynamic, tag:%#09x(%s), and set as default value:%d",
                MTK_STEREO_FEATURE_WARNING, TO_STRING(MTK_STEREO_FEATURE_WARNING), defaultValue);
            return MFALSE;
        }
        featureWarningFromP1 = entry.itemAt(0, Type2Type<MINT32>());
    }
    featureWarning |= featureWarningFromP1;
    //
    MINT32 featureWarningFromApp = 0;
    {
        sp<MetadataHandle> inAppMetaPtr = pipeRequestPtr->getMetadata(MID_MAN_IN_APP);
        IMetadata* pInAppMeta = (inAppMetaPtr != nullptr) ? inAppMetaPtr->native() : nullptr;
        if( pInAppMeta == nullptr )
        {
            MY_LOGW("failed to get featureWarning, in-app metadata is not existing");
            return MFALSE;
        }

        IMetadata::IEntry entry = pInAppMeta->entryFor(MTK_VSDOF_FEATURE_CAPTURE_WARNING_MSG);
        if( entry.isEmpty() )
        {
            MY_LOGW("failed to get metadata from in-app, tag:%#09x(%s), and set as default value:%d",
                MTK_VSDOF_FEATURE_CAPTURE_WARNING_MSG, TO_STRING(MTK_VSDOF_FEATURE_CAPTURE_WARNING_MSG), defaultValue);
            return MFALSE;
        }
        featureWarningFromApp = entry.itemAt(0, Type2Type<MINT32>());
    }
    MY_LOGD("get featureWarning, featureWarningFromP1:%#09x, featureWarningFromApp:%#09x", featureWarningFromP1, featureWarningFromApp);
    featureWarning |= featureWarningFromApp;
    return MTRUE;
}

auto
SwnrProcessor::
createImageTransformPtr() -> ImageTransformPtr
{
    return ImageTransformPtr(IImageTransform::createInstance(), [](IImageTransform *p) -> MVOID
    {
        p->destroyInstance();
    });
}

auto
SwnrProcessor::
imgCopy(IImageBuffer* pSrcImg, IImageBuffer* pDstImg) -> MBOOL
{
    MY_LOGD("image copy, srcImgInfo:%s, dstImgInfo:%s", toString(pSrcImg).c_str(), toString(pDstImg).c_str());
    ImageTransformPtr imageTransformPtr = createImageTransformPtr();
    if ( imageTransformPtr == nullptr )
    {
        MY_LOGW("failed to get imageTransform");
        return MFALSE;
    }
    //
    const MSize imgSize = pSrcImg->getImgSize();
    const MRect cropRect = MRect(imgSize.w, imgSize.h);
    if ( !imageTransformPtr->execute(pSrcImg, pDstImg, nullptr, cropRect, 0, 3000) )
    {
        MY_LOGW("failed to execute image transform, srcImg:%p, dstImg:%p", pSrcImg, pDstImg);
        return MFALSE;
    }
    return MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthNodeUtility Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
auto
DepthNodeUtility::
createPluginRequestPtr(NodeID_T nodeId, PluginPtr& pluginPtr, PipeRequestPtr& pipeRequestPtr) -> PluginRequestPtr
{
    NodeRequestPtr nodeRequestPtr = pipeRequestPtr->getNodeRequest(nodeId);
    //
    PluginRequestPtr ret = pluginPtr->createRequest();
    {
        // in/out buffer
        ret->mIBufferFull       = PluginHelper::CreateBuffer(nodeRequestPtr, TID_MAN_FULL_YUV, INPUT);
        ret->mIBufferFull2      = PluginHelper::CreateBuffer(nodeRequestPtr, TID_SUB_FULL_YUV, INPUT);
        ret->mOBufferDepth      = PluginHelper::CreateBuffer(nodeRequestPtr, TID_MAN_DEPTH, OUTPUT);
        // in/out metadata
        ret->mIMetadataHal      = PluginHelper::CreateMetadata(nodeRequestPtr, MID_MAN_IN_HAL);
        ret->mIMetadataHal2     = PluginHelper::CreateMetadata(nodeRequestPtr, MID_SUB_IN_HAL);
        ret->mIMetadataApp      = PluginHelper::CreateMetadata(nodeRequestPtr, MID_MAN_IN_APP);
        ret->mIMetadataDynamic  = PluginHelper::CreateMetadata(nodeRequestPtr, MID_MAN_IN_P1_DYNAMIC);
        ret->mIMetadataDynamic2 = PluginHelper::CreateMetadata(nodeRequestPtr, MID_SUB_IN_P1_DYNAMIC);
        ret->mOMetadataApp      = PluginHelper::CreateMetadata(nodeRequestPtr, MID_MAN_OUT_APP);
        ret->mOMetadataHal      = PluginHelper::CreateMetadata(nodeRequestPtr, MID_MAN_OUT_HAL);
    }
    return ret;
};

auto
DepthNodeUtility::
dump(IImageBuffer* pImgBuf, const std::string& fileName, MINT64 uniqueKey) -> MERROR
{
    const MSize imgSize = pImgBuf->getImgSize();
    const size_t imgStrides = pImgBuf->getBufStridesInBytes(0);
    const MINT imgFmt = pImgBuf->getImgFormat();
    MY_LOGD("dmum image info, fileName:%s, addr:%p, size:(%dx%d), fmt:%#09x, stride:%zu",
        fileName.c_str(), pImgBuf, imgSize.w, imgSize.h, imgFmt, imgStrides);

    {
        // TODO: support image format for file extension
    }

    static const MINT32 writePermission = 0660;
    static const std::string directoryName = "/sdcard/vsdof/depthNode";

    const String8 directoryPath = String8::format("%s/%" PRId64, directoryName.c_str(), uniqueKey);
    const MBOOL isSuccessCreateDirectory = NSCam::Utils::makePath(directoryPath.string(), writePermission);
    if ( !isSuccessCreateDirectory )
    {
        MY_LOGW("failed to create directory, fullFilePath:%s", directoryPath.string());
        return MFALSE;
    }
    //
    const String8 fullFilePath = String8::format("%s/%s_%dx%d_%zu_%d.yuv",
        directoryPath.string(), fileName.c_str(), imgSize.w, imgSize.h, imgStrides, imgFmt);
    pImgBuf->saveToFile(fullFilePath.string());
    MY_LOGD("save image file, fullFilePath:%s", fullFilePath.string());
    return MTRUE;
}

auto
DepthNodeUtility::
getIsForceByPassFlow() -> MBOOL
{
    MUINT8 flowByPass = 0;
    flowByPass = ::property_get_int32("vendor.debug.camera.depthnode.force.flowbypass", flowByPass);
    if( flowByPass != 0 )
    {
        MY_LOGD("force flow bypass, val:%d", flowByPass);
        return MTRUE;
    }
    return MFALSE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthNode Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
DepthNode::
DepthNode(NodeID_T nodeId, const char* name, MINT32 policy, MINT32 priority)
: CaptureFeatureNode(nodeId, name, 0, policy, priority)
// note: we have to use the only one callback object to advoid leak
, mRequestCallbackPtr(this, [](RequestCallback* ptr){ UNREFERENCED_PARAMETER(ptr); })
, mForcePreprocess(0)
{
    mForcePreprocess = ::property_get_int32("vendor.debug.camera.depthnode.force.preprocess", mForcePreprocess);
    this->addWaitQueue(&mRequests);
    MY_LOGD("ctor:%p, forcePreprocess:%d", this, mForcePreprocess);
}

DepthNode::
~DepthNode()
{
    MY_LOGD("dtor:%p", this);
}

MVOID
DepthNode::
setBufferPool(const android::sp<CaptureBufferPool> &pool)
{
    mpBufferPool = pool;
    MY_LOGD("set buffer pool, addr:%p", pool.get());
}

MBOOL
DepthNode::
onData(DataID dataId, RequestPtr& data)
{
    SCOPED_TRACER();

    MY_LOGD("request arrived, req#:%d, dataId:%d(%s)",
        data->getRequestNo(), dataId, ID2Name(dataId));

    if ( data->isSatisfied(mNodeId) )
    {
        mRequests.enque(data);
    }
    else
    {
        MY_LOGD("not ready to enque, request is not satisfied, req#:%d, dataId:%d(%s)",
            data->getRequestNo(), dataId, ID2Name(dataId), mNodeId);
    }
    return MTRUE;
}

MVOID
DepthNode::
onFlush()
{
    SCOPED_TRACER();
    CaptureFeatureNode::onFlush();

    MY_LOGD("extThreadDependency:%d, requestSize:%zu", this->getExtThreadDependency(), mRequests.size());
    RequestPtr pipeRequestPtr;
    while ( mRequests.deque(pipeRequestPtr) )
    {
        // TODO: should we need dispatch the request?
        // dispatch(pipeRequestPtr);
    }
}

MBOOL
DepthNode::
onInit()
{
    SCOPED_TRACER();
    CaptureFeatureNode::onInit();

    MY_LOGD("get plugin instance, sensorIndex:%d, sensorIndex2:%d", mSensorIndex, mSensorIndex2);
    mPluginPtr = Plugin::getInstance(mSensorIndex, mSensorIndex2);
    auto& providers = mPluginPtr->getProviders();
    auto interface = mPluginPtr->getInterface();

    const MUINT64 supportedScenarioFeatures = mUsageHint.mSupportedScenarioFeatures;
    for( auto& provider : providers )
    {
        const Property& rProperty =  provider->property();
        if( !(supportedScenarioFeatures & rProperty.mFeatures) )
        {
            MY_LOGD("not in supportedScenarioFeatures(%#012" PRIx64 "), skip the provider, name:%s, feature:%#012" PRIx64,
                supportedScenarioFeatures, rProperty.mName, rProperty.mFeatures);
            continue;
        }

        FeatureID_T featureId = NULL_FEATURE;
        if ( rProperty.mFeatures & MTK_FEATURE_DEPTH )
        {
            featureId = FID_DEPTH;
        }
        else if ( rProperty.mFeatures & TP_FEATURE_DEPTH )
        {
            featureId = FID_DEPTH_3RD_PARTY;
        }
        else
        {
            MY_LOGW("can't match the supported features, skip the provider, name:%s, feature:%#012" PRIx64,
                rProperty.mName, rProperty.mFeatures);
            continue;
        }

        SelectionPtr selectionPtr = mPluginPtr->createSelection();
        Selection& rSelection = *selectionPtr.get();
        interface->offer(rSelection);
        provider->negotiate(rSelection);
        //
        std::launch initPolicy = std::launch::deferred;
        if(rProperty.mInitPhase == ePhase_OnPipeInit)
        {
            initPolicy = std::launch::async;
        }
        //
        ProviderInfo providerInfo =
        {
            .mProviderPtr  = provider,
            .mSelectionPtr = selectionPtr,
            .mInitJob = std::async(initPolicy, [provider, initPolicy]()
            {
                const Property& rProperty = provider->property();
                AUTO_TIMER("init provider, name:%s, features:%#012" PRIx64, rProperty.mName, rProperty.mFeatures);
                provider->init();
            })
        };
        mProviderInfoTable.insert(FeaturePairItem(featureId, std::move(providerInfo)));
        MY_LOGD("insert item table, featureId:%d(%s), name:%s, features:%#012" PRIx64 ", initPhase:%d, initPolicy:%d",
                featureId, FeatID2Name(featureId),
                rProperty.mName, rProperty.mFeatures,
                rProperty.mInitPhase, static_cast<MINT32>(initPolicy));
    }
    return MTRUE;
}

MBOOL
DepthNode::
onUninit()
{
    SCOPED_TRACER();

    for ( auto& pair : mProviderInfoTable )
    {
        const FeatureID_T featureId = pair.first;
        ProviderInfo& item = pair.second;
        // TODO: we need the supportScenarioFeatures to advoid init non-used provider
        if ( item.mInitJob.valid() )
        {
            const Property& rProperty = item.mProviderPtr->property();
            MY_LOGD("wait for init job completion, featureId:%d(%s), name:%s, features:%#012" PRIx64 ", initPhase:%d",
                featureId, FeatID2Name(featureId), rProperty.mName, rProperty.mFeatures, rProperty.mInitPhase);
            item.mInitJob.get();
        }
        //
        auto& providerPtr = item.mProviderPtr;
        providerPtr->uninit();
        providerPtr = nullptr;
    }
    mProviderInfoTable.clear();

    return MTRUE;
}

MBOOL
DepthNode::
onThreadStart()
{
    return MTRUE;
}

MBOOL
DepthNode::
onThreadStop()
{
    return MTRUE;
}

MBOOL
DepthNode::
onThreadLoop()
{
    SCOPED_TRACER();

    if ( !waitAllQueue() )
    {
        return MFALSE;
    }

    RequestPtr pipeRequestPtr;
    if ( !mRequests.deque(pipeRequestPtr) )
    {
        MY_LOGE("failed to deque");
        return MFALSE;
    }

    if ( pipeRequestPtr == nullptr )
    {
        MY_LOGE("pipeRequestPtr is nullptr");
        return MFALSE;
    }
    //
    const MINT32 reqNum = pipeRequestPtr->getRequestNo();
    //
    auto foundItem = mProviderInfoTable.end();
    MBOOL isFlowByPass = DepthNodeUtility::getIsForceByPassFlow();
    {
        if ( !isFlowByPass )
        {
            MINT32 featureWarning = 0;
            DepthNodeUtility::tryGetFeatureWarning(pipeRequestPtr, featureWarning);
            if ( featureWarning != 0 )
            {
                MY_LOGD("feature warning occur, req#:%d, featureWarning:%d", reqNum, featureWarning);
                isFlowByPass = MTRUE;
            }
        }
        //
        if ( !isFlowByPass )
        {
            auto item = mProviderInfoTable.begin();
            while( item != mProviderInfoTable.end() )
            {
                if( pipeRequestPtr->hasFeature(item->first) )
                {
                    break;
                }
                ++item;
            }
            //
            if ( item != mProviderInfoTable.end() )
            {
                foundItem = item;
            }
            else
            {
                MY_LOGD("failed to find provider, req#:%d", reqNum);
                isFlowByPass = MTRUE;
            }
        }
    }
    //
    if ( isFlowByPass )
    {
        MY_LOGD("something happen and by pass flow, req#:%d", reqNum);
        pipeRequestPtr->mTimer.startDEPTH();
        dispatch(pipeRequestPtr);
        pipeRequestPtr->mTimer.stopDEPTH();
    }
    else
    {
        const FeatureID_T featureId = foundItem->first;
        ProviderPtr& providerPtr = foundItem->second.mProviderPtr;
        const Property& rProperty = providerPtr->property();
        //
        InitJob& initJob = foundItem->second.mInitJob;
        if ( initJob.valid() )
        {
            MY_LOGD("wait for init job completion, req#:%d, featureId:%d(%s), name:%s, features:%#012" PRIx64 ", initPhase:%d",
                reqNum, featureId, FeatID2Name(featureId), rProperty.mName, rProperty.mFeatures, rProperty.mInitPhase);
            initJob.get();
        }
        //
        MY_LOGD("execute provider, req#:%d, featureId:%d(%s), name:%s, features:%#012" PRIx64,
            reqNum, featureId, FeatID2Name(featureId), rProperty.mName, rProperty.mFeatures);
        //
        pipeRequestPtr->mTimer.startDEPTH();
        preprocess(providerPtr, pipeRequestPtr);
        process(providerPtr, pipeRequestPtr);
    }
    return MTRUE;
}

MVOID
DepthNode::
process(ProviderPtr& providerPtr, RequestPtr& pipeRequestPtr)
{
    const MINT32 reqNum = pipeRequestPtr->getRequestNo();
    AUTO_PROFILER("process depth request, req#:%d", reqNum);
    //
    auto pluginRequestPtr = DepthNodeUtility::createPluginRequestPtr(getNodeID(), mPluginPtr, pipeRequestPtr);
    {
        std::lock_guard<std::mutex> guard(mRequestPairLock);
        mRequestPairTable.insert(RequestPairItem(pluginRequestPtr.get(),
        {
            .mPipeRequest   = pipeRequestPtr,
            .mPluginRequest = pluginRequestPtr
        }));
        // mark on-going-request start
        this->incExtThreadDependency();
        MY_LOGD("insert requestPair into table, req#:%d, pluginRequest:%p, dependencyCount:%d",
            reqNum, pluginRequestPtr.get(), this->getExtThreadDependency());
    }
    providerPtr->process(pluginRequestPtr, mRequestCallbackPtr);
}

MVOID
DepthNode::
preprocess(ProviderPtr& providerPtr, RequestPtr& pipeRequestPtr)
{
    const MINT32 reqNum = pipeRequestPtr->getRequestNo();
    AUTO_PROFILER("preprocess depthRequest, req#:%d", reqNum);
    AUTO_TIMER("profile preprocess, req#:%d", reqNum);

    const Property& property = providerPtr->property();
    const MBOOL isExecutSWNR = ((property.mPreprocess == ePreProcess_NR) || (mForcePreprocess & ePreProcess_NR));
    MY_LOGD("swnr preprocess, req#:%d, isExecutSWNR:%d", reqNum, isExecutSWNR);
    if ( isExecutSWNR )
    {
        AUTO_PROFILER("swnr preprocess for depth input, req#:%d", reqNum);
        NodeRequestPtr nodeRequestPtr = pipeRequestPtr->getNodeRequest(getNodeID());
        //
        const BufferID_T mainInputBufId = nodeRequestPtr->mapBufferID(TID_MAN_FULL_YUV, INPUT);
        const BufferID_T subInputBufId = nodeRequestPtr->mapBufferID(TID_SUB_FULL_YUV, INPUT);
        sp<BufferHandle> mianFSYuvImgBufPtr = pipeRequestPtr->getBuffer(mainInputBufId);
        sp<BufferHandle> subFSYuvImgBufPtr = pipeRequestPtr->getBuffer(subInputBufId);
        //
        sp<MetadataHandle> mainHalMetadataPtr = pipeRequestPtr->getMetadata(MID_MAN_IN_HAL);
        sp<MetadataHandle> subHalMetadataPtr = pipeRequestPtr->getMetadata(MID_SUB_IN_HAL);
        const MBOOL readyToPrepareImgBuf = (mianFSYuvImgBufPtr != nullptr) && (subFSYuvImgBufPtr != nullptr);
        const MBOOL readyToPrepareMetaData = (mainHalMetadataPtr != nullptr) && (subHalMetadataPtr != nullptr);
        if ( (!readyToPrepareImgBuf) || (!readyToPrepareMetaData) )
        {
            MY_LOGW("invalid parameters for NR process, req#:%d, mianFSYuvImgBufPtr:%p, subFSYuvImgBufPtr:%p. mainHalMetadataPtr:%p, subHalMetadataPtr,%p",
                reqNum,
                mianFSYuvImgBufPtr.get(), subFSYuvImgBufPtr.get(),
                mainHalMetadataPtr.get(), subHalMetadataPtr.get());
        }
        else
        {
            SwnrProcessor::Ptr mainSwnrProcessorPtr = SwnrProcessor::createInstance("mainFullYuv", mSensorIndex);
            SwnrProcessor::Ptr subSwnrProcessorPtr = SwnrProcessor::createInstance("subFullYuv", mSensorIndex2);
            //
            IImageBuffer* pMinFSYuvImgBuf = mianFSYuvImgBufPtr->native();
            IImageBuffer* pSubFSYuvImgBuf = subFSYuvImgBufPtr->native();
            IMetadata* pMainHalMetadata = mainHalMetadataPtr->native();
            IMetadata* pSubHalMetadata = subHalMetadataPtr->native();
            if ( (mainSwnrProcessorPtr->execute(pMinFSYuvImgBuf, pMainHalMetadata, mpBufferPool) != OK)
                || (subSwnrProcessorPtr->execute(pSubFSYuvImgBuf, pSubHalMetadata, mpBufferPool) != OK) )
            {
                 MY_LOGW("failed to execute swnr preprocess, req#:%d", reqNum);
            }
            else
            {
                MERROR mainSwnrRet = mainSwnrProcessorPtr->waitExecuteDone();
                MERROR subSwnrRet = subSwnrProcessorPtr->waitExecuteDone();
                MY_LOGD("swnr preprocess result, req#:%d, mainRet:%d, subRet:%d", reqNum, mainSwnrRet, subSwnrRet);
            }
        }
    }
}

MERROR
DepthNode::
evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    SCOPED_TRACER();

    UNREFERENCED_PARAMETER(nodeId);

    const MUINT8 reqtIndex = rInfer.getRequestIndex();
    const MUINT8 reqCount = rInfer.getRequestCount();
    const MBOOL isMainFrame = (reqtIndex == 0);

    MY_LOGD("evaluate info, reqIndex:%u, reqCount:%u, isMainFrame:%d",
        reqtIndex, reqCount, isMainFrame);

    if ( !isMainFrame )
        return OK;

    auto& rSrcData = rInfer.getSharedSrcData();
    auto& rDstData = rInfer.getSharedDstData();
    auto& rFeatures = rInfer.getSharedFeatures();
    auto& rMetadatas = rInfer.getSharedMetadatas();

    MBOOL isEvaluated = MFALSE;
    for (auto& item : mProviderInfoTable)
    {
        const FeatureID_T featureId = item.first;
        if ( !rInfer.hasFeature(featureId) )
        {
            continue;
        }
        else if ( isEvaluated )
        {
            MY_LOGE("duplicated feature:%d(%s)", featureId, FeatID2Name(featureId));
            continue;
        }

        const Selection& rSel = *(item.second.mSelectionPtr);
        MBOOL isOK = MTRUE;

        if ( isOK && rSel.mIBufferFull.getRequired() )
        {
            const TypeID_T typeId = TID_MAN_FULL_YUV;
            const MBOOL hasType = rInfer.hasType(typeId);
            const MBOOL isValid = rSel.mIBufferFull.isValid();

            isOK = (isValid && hasType);
            if ( isOK )
            {
                auto& srcTemp = rSrcData.editItemAt(rSrcData.add());
                srcTemp.mTypeId = typeId;
                srcTemp.mSizeId = rSel.mIBufferFull.getSizes()[0];
                srcTemp.mFormat = rSel.mIBufferFull.getFormats()[0];
            }
            else
            {
                MY_LOGW("failed to add src, hasType:%d, typeId:%d(%s), featureId:%d(%s)",
                    hasType, typeId, TypeID2Name(typeId), featureId, FeatID2Name(featureId));
            }
        }

        if ( isOK && rSel.mIBufferFull2.getRequired() )
        {
            const TypeID_T typeId = TID_SUB_FULL_YUV;
            const MBOOL hasType = rInfer.hasType(typeId);
            const MBOOL isValid = rSel.mIBufferFull2.isValid();

            isOK = ( isValid && hasType );
            if ( isOK )
            {
                auto& srcTemp = rSrcData.editItemAt(rSrcData.add());
                srcTemp.mTypeId = typeId;
                srcTemp.mSizeId = rSel.mIBufferFull2.getSizes()[0];
                srcTemp.mFormat = rSel.mIBufferFull2.getFormats()[0];
            }
            else
            {
                MY_LOGW("failed to add src, hasType:%d, typeId:%d(%s), featureId:%d(%s)",
                    hasType, typeId, TypeID2Name(typeId), featureId, FeatID2Name(featureId));
            }
        }

        const Property& rProperty =  item.second.mProviderPtr->property();
        if ( rProperty.mFaceData == eFD_Current )
        {
            auto& srcTemp = rSrcData.editItemAt(rSrcData.add());
            srcTemp.mTypeId = TID_MAN_FD;
            srcTemp.mSizeId = NULL_SIZE;
            rInfer.markFaceData(eFD_Current);
        }
        else if ( rProperty.mFaceData == eFD_Cache )
        {
            rInfer.markFaceData(eFD_Cache);
        }
        else if ( rProperty.mFaceData == eFD_None )
        {
            rInfer.markFaceData(eFD_None);
        }
        else
        {
            MY_LOGW("unknow faceDateType:%#09x", rInfer.mFaceDateType.value);
        }
        //
        if( rProperty.mBoost != 0 )
        {
            MY_LOGD("append boostType, value:%#09x", rProperty.mBoost);
            rInfer.appendBootType(rProperty.mBoost);
        }
        //
        if ( isOK && rSel.mOBufferDepth.getRequired() )
        {
            const TypeID_T typeId = TID_MAN_DEPTH;
            isOK = rSel.mOBufferDepth.isValid();

            if ( isOK )
            {
                auto& dstTemp = rDstData.editItemAt(rDstData.add());
                dstTemp.mTypeId = typeId;
                dstTemp.mSizeId = rSel.mOBufferDepth.getSizes()[0];
                dstTemp.mFormat = rSel.mOBufferDepth.getFormats()[0];
                dstTemp.mSize = rSel.mOBufferDepth.getSpecifiedSize();
            }
            else
            {
                MY_LOGW("failed to add dst, typeId:%d(%s), featureId:%d(%s)",
                    typeId, TypeID2Name(typeId), featureId, FeatID2Name(featureId));
            }
        }

        if ( isOK )
        {
            if (rSel.mIMetadataDynamic.getRequired())
                rMetadatas.push_back(MID_MAN_IN_P1_DYNAMIC);
            if (rSel.mIMetadataApp.getRequired())
                rMetadatas.push_back(MID_MAN_IN_APP);
            if (rSel.mIMetadataHal.getRequired())
                rMetadatas.push_back(MID_MAN_IN_HAL);
            if (rSel.mOMetadataApp.getRequired())
                rMetadatas.push_back(MID_MAN_OUT_APP);
            if (rSel.mOMetadataHal.getRequired())
                rMetadatas.push_back(MID_MAN_OUT_HAL);
            if (rSel.mIMetadataDynamic2.getRequired())
                rMetadatas.push_back(MID_SUB_IN_P1_DYNAMIC);
            if (rSel.mIMetadataHal2.getRequired())
                rMetadatas.push_back(MID_SUB_IN_HAL);
        }

        if ( isOK )
        {
            isEvaluated = MTRUE;
            rFeatures.push_back(featureId);
            rInfer.addNodeIO(NID_DEPTH, rSrcData, rDstData, rMetadatas, rFeatures);
            MY_LOGD("add feature, featureId:%d(%s)", featureId, FeatID2Name(featureId));
        }
    }
    return OK;
}

void
DepthNode::
onAborted(PluginRequestPtr pluginRequestPtr)
{
    MY_LOGD("aborted requestAddr:%p", pluginRequestPtr.get());
}

void
DepthNode::
onCompleted(PluginRequestPtr pluginRequestPtr, MERROR status)
{
    MY_LOGD("completed pluginRequest, addr:%p, status:%d", pluginRequestPtr.get(), status);

    const auto key = pluginRequestPtr.get();
    RequestPtr pipeRequestPtr = nullptr;
    {
        std::lock_guard<std::mutex> guard(mRequestPairLock);

        auto foundItem = mRequestPairTable.find(key);
        if ( foundItem != mRequestPairTable.end() )
        {
            pipeRequestPtr = foundItem->second.mPipeRequest;
            mRequestPairTable.erase(foundItem);
            MY_LOGD("remove requestPair from table, req#:%d, pluginRequest:%p, RequestPairTableCount:%zu",
                pipeRequestPtr->getRequestNo(), pluginRequestPtr.get(), mRequestPairTable.size());
        }
        *pluginRequestPtr = Plugin::Request();
    }

    if ( pipeRequestPtr == nullptr )
    {
        MY_LOGE("failed to find pipeRequest from pluginRequest, addr:%p", key);
        return;
    }

    pipeRequestPtr->mTimer.stopDEPTH();
    dispatch(pipeRequestPtr);
    //
    this->decExtThreadDependency();
    MY_LOGD("finish request, req#:%d, dependence:%d",
        pipeRequestPtr->getRequestNo(), this->getExtThreadDependency());
}

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
