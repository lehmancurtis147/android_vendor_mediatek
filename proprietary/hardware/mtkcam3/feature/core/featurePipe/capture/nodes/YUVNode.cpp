/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "YUVNode.h"

#define PIPE_CLASS_TAG "YUVNode"
#define PIPE_TRACE TRACE_YUV_NODE
#include <featurePipe/core/include/PipeLog.h>
#include <sstream>
#include "../CaptureFeaturePlugin.h"
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

using namespace NSCam::NSPipelinePlugin;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

/******************************************************************************
*
******************************************************************************/
class YuvInterface : public YuvPlugin::IInterface
{
public:
    virtual MERROR offer(YuvPlugin::Selection& sel)
    {
        sel.mIBufferFull
            .addSupportFormat(eImgFmt_NV12)
            .addSupportFormat(eImgFmt_YV12)
            .addSupportFormat(eImgFmt_YUY2)
            .addSupportFormat(eImgFmt_NV21)
            .addSupportFormat(eImgFmt_I420)
            .addSupportSize(eImgSize_Full);

        sel.mOBufferFull
            .addSupportFormat(eImgFmt_NV12)
            .addSupportFormat(eImgFmt_YV12)
            .addSupportFormat(eImgFmt_YUY2)
            .addSupportFormat(eImgFmt_NV21)
            .addSupportFormat(eImgFmt_I420)
            .addSupportSize(eImgSize_Full);

        return OK;
    };

    virtual ~YuvInterface() {};
};

REGISTER_PLUGIN_INTERFACE(Yuv, YuvInterface);

/******************************************************************************
*
******************************************************************************/
class YuvCallback : public YuvPlugin::RequestCallback
{
public:
    YuvCallback(YUVNode* pNode)
        : mpNode(pNode)
    {
    }

    virtual void onAborted(YuvPlugin::Request::Ptr pPlgRequest)
    {
        MY_LOGD("onAborted request: %p", pPlgRequest.get());
        onCompleted(pPlgRequest, UNKNOWN_ERROR);
    }

    virtual void onCompleted(YuvPlugin::Request::Ptr pPlgRequest, MERROR result)
    {
        RequestPtr pCapRequest = mpNode->findRequest(pPlgRequest);

        if (pCapRequest == NULL) {
            MY_LOGE("unknown request happened: %p, result %d", pPlgRequest.get(), result);
            return;
        }

        *pPlgRequest = YuvPlugin::Request();
        MY_LOGD("onCompleted request:%p, result:%d", pPlgRequest.get(), result);

        if (result != OK) {
            pCapRequest->addParameter(PID_FAILURE, 1);
        }

        MBOOL bRepeat = mpNode->onRequestRepeat(pCapRequest);
        // no more repeating
        if (bRepeat) {
            mpNode->onRequestProcess(pCapRequest);
        } else {
            mpNode->onRequestFinish(pCapRequest);
        }
    }

    virtual ~YuvCallback() { };
private:
    YUVNode* mpNode;
};


YUVNode::YUVNode(NodeID_T nid, const char *name, MINT32 policy, MINT32 priority, MBOOL hasTwinNodes)
    : CaptureFeatureNode(nid, name, policy, priority)
    , mHasTwinNodes(hasTwinNodes)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);
    TRACE_FUNC_EXIT();
}

YUVNode::~YUVNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID YUVNode::setBufferPool(const android::sp<CaptureBufferPool> &pool)
{
    TRACE_FUNC_ENTER();
    mpBufferPool = pool;
    TRACE_FUNC_EXIT();
}

MBOOL YUVNode::onData(DataID id, RequestPtr& pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));
    MBOOL ret = MTRUE;

    if (pRequest->isSatisfied(mNodeId)) {
        pRequest->addParameter(PID_REQUEST_REPEAT, 0);
        mRequests.enque(pRequest);
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL YUVNode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();


    mPlugin = YuvPlugin::getInstance(mSensorIndex);

    FeatureID_T featureId;
    auto& vpProviders = mPlugin->getProviders();
    mpInterface = mPlugin->getInterface();

    std::vector<ProviderPtr> vSortedProvider = vpProviders;

    std::sort(vSortedProvider.begin(), vSortedProvider.end(),
            [] (const ProviderPtr& p1, const ProviderPtr& p2) {
                return p1->property().mPosition < p2->property().mPosition ||
                       p1->property().mPriority > p2->property().mPriority;
            });

    for (auto& pProvider : vSortedProvider) {
        const YuvPlugin::Property& rProperty =  pProvider->property();
        featureId = NULL_FEATURE;

        if (mHasTwinNodes) {
            if (mNodeId == NID_YUV && rProperty.mPosition != 0)
                continue;
            if (mNodeId == NID_YUV2 && rProperty.mPosition != 1)
                continue;
        }

        if (rProperty.mFeatures & MTK_FEATURE_NR)
            featureId = FID_NR;
        else if (rProperty.mFeatures & MTK_FEATURE_FB)
            featureId = FID_FB;
        else if (rProperty.mFeatures & MTK_FEATURE_ABF)
            featureId = FID_ABF;
        else if (rProperty.mFeatures & TP_FEATURE_FB)
            featureId = FID_FB_3RD_PARTY;


        if (featureId != NULL_FEATURE) {
            MY_LOGD_IF(mLogLevel, "%s finds plugin:%s, priority:%d",
                    NodeID2Name(mNodeId), FeatID2Name(featureId), rProperty.mPriority);
            auto& item = mProviderPairs.editItemAt(mProviderPairs.add());
            item.mFeatureId = featureId;
            item.mpProvider = pProvider;

            if(rProperty.mInitPhase==ePhase_OnPipeInit && mInitMapT.count(featureId) <= 0){
                auto job = std::async(std::launch::async,[&,pProvider](){
                    pProvider->init();
                });
                mInitMapT[featureId]=std::move(job);
                MY_LOGD("%s init on InitStage", FeatID2Name(featureId));
            }
        }

    }

    mpCallback = make_shared<YuvCallback>(this);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL YUVNode::onUninit()
{
    TRACE_FUNC_ENTER();

    for (ProviderPair& p : mProviderPairs) {
        ProviderPtr pProvider = p.mpProvider;
        FeatureID_T featureId = p.mFeatureId;

        if ( mInitMapT.count(featureId) > 0 ) {
            if ( !mInitFeatures.hasBit(featureId) ) {
                MY_LOGD("get()+ featureName %s", FeatID2Name(featureId));
                mInitMapT[featureId].get();
                MY_LOGD("get()-");
                mInitFeatures.markBit(featureId);
            }
            pProvider->uninit();
        }
    }

    mProviderPairs.clear();
    mInitMapT.clear();
    mInitFeatures.clear();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL YUVNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL YUVNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}


MBOOL YUVNode::onThreadLoop()
{
    TRACE_FUNC_ENTER();
    if (!waitAllQueue()) {
        TRACE_FUNC("Wait all queue exit");
        return MFALSE;
    }

    RequestPtr pRequest;
    if (!mRequests.deque(pRequest)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    pRequest->mTimer.startYUV();
    onRequestProcess(pRequest);

    TRACE_FUNC_EXIT();
    return MTRUE;

}

MBOOL YUVNode::onRequestRepeat(RequestPtr& pRequest)
{
    MINT32 repeat = pRequest->getParameter(PID_REQUEST_REPEAT);
    repeat++;
    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(mNodeId + repeat);

    // no more repeating
    if (pNodeReq == NULL)
        return MFALSE;

    MY_LOGD("onRequestRepeat request:%d, repeat:%d",
            pRequest->getRequestNo(), repeat);

    {
        Mutex::Autolock _l(mPairLock);
        auto it = mRequestPairs.begin();
        for (; it != mRequestPairs.end(); it++) {
            if ((*it).mPipe == pRequest) {
                mRequestPairs.erase(it);
                break;
            }
        }
    }

    pRequest->addParameter(PID_REQUEST_REPEAT, repeat);

    return MTRUE;
}

MBOOL YUVNode::onRequestProcess(RequestPtr& pRequest)
{
    Mutex::Autolock _l(mOperLock);

    MINT32 repeat = pRequest->getParameter(PID_REQUEST_REPEAT);

    if (repeat == 0)
        incExtThreadDependency();

    NodeID_T nodeId = mNodeId + repeat;

    if (!pRequest->isValid()) {
        pRequest->decNodeReference(nodeId);
        onRequestFinish(pRequest);
        return MFALSE;
    }

    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    FeatureID_T featureId = NULL_FEATURE;
    CAM_TRACE_FMT_BEGIN("yuv(%d):process|r%df%d",mNodeId, requestNo, frameNo);
    MY_LOGI("(%d) +, R/F Num: %d/%d", mNodeId, requestNo, frameNo);

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(nodeId);
    if (pNodeReq == NULL) {
        MY_LOGE("should not be here if no node request");
        pRequest->addParameter(PID_FAILURE, 1);
        onRequestFinish(pRequest);
        return MFALSE;
    }

    // pick a provider
    ProviderPtr pProvider = NULL;
    for (ProviderPair& p : mProviderPairs) {
        FeatureID_T featId = p.mFeatureId;
        if (pRequest->hasFeature(featId)) {
            if (repeat > 0) {
                repeat--;
                continue;
            }
            pProvider = p.mpProvider;
            featureId = featId;
            break;
        }
    }


    if (pProvider == NULL) {
        MY_LOGE("do not execute a plugin");
        dispatch(pRequest);
        return MFALSE;
    }

    if(!mInitFeatures.hasBit(featureId) && (mInitMapT.count(featureId)>0)){
        MY_LOGD("get()+ featureName %s", FeatID2Name(featureId));
        mInitMapT[featureId].get();
        MY_LOGD("get()-");
        mInitFeatures.markBit(featureId);
    }

    auto pPlgRequest = mPlugin->createRequest();

    pPlgRequest->mIBufferFull = PluginHelper::CreateBuffer(pNodeReq, TID_MAN_FULL_YUV, INPUT);
    pPlgRequest->mOBufferFull = PluginHelper::CreateBuffer(pNodeReq, TID_MAN_FULL_YUV, OUTPUT);

    pPlgRequest->mIMetadataDynamic = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_P1_DYNAMIC);
    pPlgRequest->mIMetadataApp = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_APP);
    pPlgRequest->mIMetadataHal = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_IN_HAL);
    pPlgRequest->mOMetadataApp = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_OUT_APP);
    pPlgRequest->mOMetadataHal = PluginHelper::CreateMetadata(pNodeReq, MID_MAN_OUT_HAL);

    MBOOL ret = MFALSE;
    {
        Mutex::Autolock _l(mPairLock);
        auto& rPair = mRequestPairs.editItemAt(mRequestPairs.add());
        rPair.mPipe = pRequest;
        rPair.mPlugin = pPlgRequest;
    }

    pProvider->process(pPlgRequest, mpCallback);
    ret = MTRUE;

    MY_LOGI("(%d) -, R/F Num: %d/%d", mNodeId, requestNo, frameNo);
    CAM_TRACE_FMT_END();
    return ret;
}

RequestPtr YUVNode::findRequest(PluginRequestPtr& pPlgRequest)
{
    Mutex::Autolock _l(mPairLock);
    for (const auto& rPair : mRequestPairs) {
        if (pPlgRequest == rPair.mPlugin) {
            return rPair.mPipe;
        }
    }

    return NULL;
}

MBOOL YUVNode::onRequestFinish(RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    CAM_TRACE_FMT_BEGIN("yuv(%d):finish|r%df%d",mNodeId, requestNo, frameNo);
    MY_LOGI("(%d), R/F Num: %d/%d", mNodeId, requestNo, frameNo);

    {
        Mutex::Autolock _l(mPairLock);
        auto it = mRequestPairs.begin();
        for (; it != mRequestPairs.end(); it++) {
            if ((*it).mPipe == pRequest) {
                mRequestPairs.erase(it);
                break;
            }
        }
    }

    pRequest->mTimer.stopYUV();
    dispatch(pRequest);

    decExtThreadDependency();
    CAM_TRACE_FMT_END();
    return MTRUE;
}

MERROR YUVNode::evaluate(NodeID_T nodeId __unused, CaptureFeatureInferenceData& rInfer)
{
    (void) nodeId;

    if (rInfer.getRequestIndex() > 0) {
        // should not be involved to infer while blending frame
        return OK;
    }

    MBOOL isValid;
    MUINT8 uRepeatCount = 0;

    // Foreach all loaded plugin
    for (ProviderPair& p : mProviderPairs) {
        FeatureID_T featId = p.mFeatureId;

        if (!rInfer.hasFeature(featId)) {
            continue;
        } else if (uRepeatCount >= 3) {
            MY_LOGE("over max repeating count(3), ignore feature: %s", FeatID2Name(featId));
            continue;
        }

        auto& rSrcData = rInfer.getSharedSrcData();
        auto& rDstData = rInfer.getSharedDstData();
        auto& rFeatures = rInfer.getSharedFeatures();
        auto& rMetadatas = rInfer.getSharedMetadatas();

        isValid = MTRUE;

        ProviderPtr pProvider = p.mpProvider;
        const YuvPlugin::Property& rProperty =  pProvider->property();

        Selection sel;
        mpInterface->offer(sel);
        sel.mIMetadataHal.setControl(rInfer.mpIMetadataHal);
        sel.mIMetadataApp.setControl(rInfer.mpIMetadataApp);
        sel.mIMetadataDynamic.setControl(rInfer.mpIMetadataDynamic);
        if (pProvider->negotiate(sel) != OK) {
            MY_LOGD("bypass %s after negotiation", FeatID2Name(featId));
            rInfer.clearFeature(featId);
            continue;
        }

        if(rProperty.mInitPhase==ePhase_OnRequest && mInitMapT.count(featId) <= 0){
            auto job = std::async(std::launch::async,[&,pProvider](){
                pProvider->init();
            });
            mInitMapT[featId]=std::move(job);
            MY_LOGD("%s init on evaluate stage", FeatID2Name(featId));
        }

        // full size input
        if (sel.mIBufferFull.getRequired()) {
            if (sel.mIBufferFull.isValid()) {
                auto& src_0 = rSrcData.editItemAt(rSrcData.add());

                src_0.mTypeId = TID_MAN_FULL_YUV;
                if (!rInfer.hasType(TID_MAN_FULL_YUV))
                    isValid = MFALSE;

                src_0.mSizeId = sel.mIBufferFull.getSizes()[0];
                // Directly select the first format, using lazy strategy
                src_0.mFormat = sel.mIBufferFull.getFormats()[0];

                // In-place processing must add a output
                if (rProperty.mInPlace) {
                    auto& dst_0 = rDstData.editItemAt(rDstData.add());
                    dst_0.mTypeId = TID_MAN_FULL_YUV;
                    dst_0.mSizeId = src_0.mSizeId;
                    dst_0.mFormat = src_0.mFormat;
                    dst_0.mSize = rInfer.getSize(TID_MAN_FULL_YUV);
                    dst_0.mInPlace = true;
                }
            } else
                isValid = MFALSE;
        }

        // face data
        if (rProperty.mFaceData == eFD_Current) {
            auto& src_1 = rSrcData.editItemAt(rSrcData.add());
            src_1.mTypeId = TID_MAN_FD;
            src_1.mSizeId = NULL_SIZE;
            rInfer.markFaceData(eFD_Current);
        }
        else if (rProperty.mFaceData == eFD_Cache) {
            rInfer.markFaceData(eFD_Cache);
        }
        else if (rProperty.mFaceData == eFD_None) {
            rInfer.markFaceData(eFD_None);
        }
        else {
            MY_LOGW("unknow faceDateType:%x", rInfer.mFaceDateType.value);
        }

        // full size output
        if (!rProperty.mInPlace && sel.mOBufferFull.getRequired()) {
            if (sel.mOBufferFull.isValid()) {
                auto& dst_0 = rDstData.editItemAt(rDstData.add());
                dst_0.mTypeId = TID_MAN_FULL_YUV;
                dst_0.mSizeId = sel.mOBufferFull.getSizes()[0];
                dst_0.mFormat = sel.mOBufferFull.getFormats()[0];
                dst_0.mSize = rInfer.getSize(TID_MAN_FULL_YUV);
            } else
                isValid = MFALSE;
        }

        if (sel.mIMetadataDynamic.getRequired())
            rMetadatas.push_back(MID_MAN_IN_P1_DYNAMIC);

        if (sel.mIMetadataApp.getRequired())
            rMetadatas.push_back(MID_MAN_IN_APP);

        if (sel.mIMetadataHal.getRequired())
            rMetadatas.push_back(MID_MAN_IN_HAL);

        if (sel.mOMetadataApp.getRequired())
            rMetadatas.push_back(MID_MAN_OUT_APP);

        if (sel.mOMetadataHal.getRequired())
            rMetadatas.push_back(MID_MAN_OUT_HAL);

        if (isValid) {
            rFeatures.push_back(featId);
            rInfer.addNodeIO(mNodeId + uRepeatCount, rSrcData, rDstData, rMetadatas, rFeatures);
            uRepeatCount++;
        } else
            MY_LOGW("%s has invalid evaluation:%s",NodeID2Name(mNodeId), FeatID2Name(featId));
    }

    return OK;
}
} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
