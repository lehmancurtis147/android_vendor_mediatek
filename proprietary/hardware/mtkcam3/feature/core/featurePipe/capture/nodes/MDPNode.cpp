/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "MDPNode.h"

#define PIPE_CLASS_TAG "MDPNode"
#define PIPE_TRACE TRACE_MDP_NODE
#include <featurePipe/core/include/PipeLog.h>

#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/exif/DebugExifUtils.h>
#include <mtkcam/utils/TuningUtils/FileReadRule.h>

#if MTK_CAM_NEW_NVRAM_SUPPORT
//#include <mtkcam/utils/mapping_mgr/cam_idx_mgr.h>
#endif

using namespace NSCam::Utils;
using namespace NSCam::NSIoPipe;
using namespace NSCam::TuningUtils;

#define FD_TOLERENCE        (600000000)
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

MDPNode::MDPNode(NodeID_T nid, const char* name, MINT32 policy, MINT32 priority)
    : CaptureFeatureNode(nid, name, 0, policy, priority)
    , mpTransformer(NULL)
    , mpMdpSetting(NULL)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);
    mDebugDump = property_get_int32("vendor.debug.camera.p2.dump", 0) > 0;
    TRACE_FUNC_EXIT();
}

MDPNode::~MDPNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL MDPNode::onData(DataID id, RequestPtr &pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));

    mRequests.enque(pRequest);

    TRACE_FUNC_EXIT();
    return MTRUE;
}


MBOOL MDPNode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();

    mpTransformer = IImageTransform::createInstance();
    if (!mpTransformer) {
        MY_LOGE("can not get instance of ImageTransform");
        return MFALSE;
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MDPNode::onUninit()
{
    TRACE_FUNC_ENTER();
    if (mpTransformer) {
        mpTransformer->destroyInstance();
        mpTransformer = NULL;
    }

    if (mpMdpSetting != NULL) {
        void* pBuffer = mpMdpSetting->buffer;
        if (pBuffer)
            free(pBuffer);

        delete mpMdpSetting;
        mpMdpSetting = NULL;
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MDPNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL MDPNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MDPNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr pRequest;

    if (!waitAllQueue()) {
        return MFALSE;
    }

    if (!mRequests.deque(pRequest)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }
    TRACE_FUNC_ENTER();

    incExtThreadDependency();
    TRACE_FUNC("Frame %d in MDP", pRequest->getRequestNo());

    if (pRequest->isValid()) {
        pRequest->mTimer.startMDP();
        onRequestProcess(pRequest);
        pRequest->mTimer.stopMDP();
    }

    onRequestFinish(pRequest);
    decExtThreadDependency();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL MDPNode::onRequestProcess(RequestPtr &pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    CAM_TRACE_FMT_BEGIN("mdp:process|r%df%d", requestNo, frameNo);
    MY_LOGI("+, R/F Num: %d/%d", requestNo, frameNo);

    MBOOL ret = MTRUE;
    MINT32 iMagicNo = 0;

    mBufferItems.clear();

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(NID_MDP);

    if (pNodeReq == NULL)
        return MFALSE;

    IMetadata* pInHalMeta = pNodeReq->acquireMetadata(MID_MAN_IN_HAL);
    IMetadata* pInAppMeta = pNodeReq->acquireMetadata(MID_MAN_IN_APP);
    IMetadata* pInDynamicMeta = pNodeReq->acquireMetadata(MID_MAN_IN_P1_DYNAMIC);

    if (pInHalMeta != NULL) {
        tryGetMetadata<MINT32>(pInHalMeta, MTK_P1NODE_PROCESSOR_MAGICNUM, iMagicNo);
    }

    // Input
    BufferID_T uIBufFull = pNodeReq->mapBufferID(TID_MAN_FULL_YUV, INPUT);
    IImageBuffer* pSrcBuffer = pNodeReq->acquireBuffer(uIBufFull);
    if (pSrcBuffer == NULL) {
        MY_LOGE("no source image!");
        return BAD_VALUE;
    }

    // Output
    String8 str = String8::format("Resized(%d) R/F/M:%d/%d/%d",
                            0,
                            pRequest->getRequestNo(),
                            pRequest->getFrameNo(),
                            iMagicNo);

    sp<CropCalculator::Factor> pFactor = mpCropCalculator->getFactor(pInAppMeta, pInHalMeta);

    for (TypeID_T typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
        BufferID_T bufId = pNodeReq->mapBufferID(typeId, OUTPUT);
        if (bufId == NULL_BUFFER)
            continue;

        IImageBuffer* pDstBuffer = pNodeReq->acquireBuffer(bufId);
        if (pDstBuffer == NULL)
            continue;

        BufferItem& item = mBufferItems.editItemAt(mBufferItems.add());

        item.mIsCapture = (bufId == BID_MAN_OUT_JPEG);
        item.mpImageBuffer = pDstBuffer;
        item.mTransform = pNodeReq->getImageTransform(bufId);

        MSize dstSize = pDstBuffer->getImgSize();
        if (item.mTransform & eTransform_ROT_90)
            dstSize = MSize(dstSize.h, dstSize.w);

        MRect& crop = item.mCrop;
        mpCropCalculator->evaluate(pFactor, dstSize, crop);

        str += String8::format(", Type(%s) Rot(%d) Crop(%d,%d)(%dx%d) Size(%dx%d) Cap(%d)",
                    TypeID2Name(typeId),
                    item.mTransform,
                    crop.p.x, crop.p.y, crop.s.w, crop.s.h,
                    pDstBuffer->getImgSize().w, pDstBuffer->getImgSize().h,
                    item.mIsCapture);
    }

    MY_LOGD("%s", str.string());

    if (pSrcBuffer == NULL || mBufferItems.size() == 0) {
        MY_LOGE("wrong mdp in/out: src %p, dst count %zu", pSrcBuffer, mBufferItems.size());
        return BAD_VALUE;
    }

    if (mpFdReader == NULL)
        mpFdReader = IFDContainer::createInstance(LOG_TAG,  IFDContainer::eFDContainer_Opt_Read);

    MINT64 p1timestamp = 0;
    if (!tryGetMetadata<MINT64>(pInHalMeta, MTK_P1NODE_FRAME_START_TIMESTAMP, p1timestamp))
        MY_LOGD("cant get p1timestamp meta");

    MBOOL hasMdpSetting = MFALSE;
    // Lock FD Data
    vector<FD_DATATYPE*> vFdData = mpFdReader->queryLock(p1timestamp - FD_TOLERENCE, p1timestamp);
    {
        FD_DATATYPE* pFdData = NULL;
        if (vFdData.empty()) {
            MY_LOGW("can't query facedata ts:%" PRId64 "", p1timestamp);
            if (mLogLevel)
                mpFdReader->dumpInfo();
        } else {
            pFdData = vFdData.back();
        }

        MINT32 iTimestamp = 0;
        MINT32 iRealLv  = 0;
        MINT32 iIsoValue = 0;
        if (pInHalMeta)
        {
            tryGetMetadata<MINT32>(pInHalMeta, MTK_PIPELINE_UNIQUE_KEY, iTimestamp);
            tryGetMetadata<MINT32>(pInHalMeta, MTK_REAL_LV, iRealLv);
            tryGetMetadata<MINT32>(pInDynamicMeta, MTK_SENSOR_SENSITIVITY, iIsoValue);
        }

        for (size_t i = 0; i < mBufferItems.size(); i++) {
            str.clear();
            const BufferItem& item = mBufferItems[i];

            // 0:DST_BUF_0 1:DST_BUF_1
            size_t port = i % 2;

            IImageTransform::PQParam pqParam;
            pqParam.frameNo = pRequest->getFrameNo();
            pqParam.requestNo = pRequest->getRequestNo();
            pqParam.timestamp = iTimestamp;
            pqParam.lv_value  = iRealLv;
            pqParam.iso = iIsoValue;
            pqParam.sensorId = mSensorIndex;
            pqParam.mode = IImageTransform::Mode::Capture_Single;
            pqParam.enable = MFALSE;
            // just one mdp port can support clearzoom in the same time
            MBOOL enableCZ = pRequest->hasFeature(FID_CZ) && item.mIsCapture;
            if (enableCZ) {
                pqParam.type = IImageTransform::PQType::ClearZoom;
                pqParam.enable = MTRUE;
                pqParam.portIdx = port;
                // set CZConfig
                MUINT32 idx = 0;
                pqParam.cz.p_customSetting = (void*)getTuningFromNvram(mSensorIndex, idx, iMagicNo, NVRAM_TYPE_CZ, mLogLevel);
                str += String8::format("CZ mode:%d nvram:%p idx:%d ",
                                        pqParam.mode, pqParam.cz.p_customSetting, idx);
            }

            // all mdp port can enable dre in the same time
            // only capture stream applies DRE
            MBOOL enableDRE = pRequest->hasFeature(FID_DRE) && item.mIsCapture;
            if (enableDRE) {
                pqParam.type |= IImageTransform::PQType::DRE;
                pqParam.enable = MTRUE;
                pqParam.portIdx = port;
                pqParam.dre.cmd  = IImageTransform::DREParam::CMD::DRE_Apply;
                pqParam.dre.type = (i == mBufferItems.size() - 1)
                            ? IImageTransform::DREParam::HisType::His_One_Time
                            : IImageTransform::DREParam::HisType::His_Conti;
                pqParam.dre.userId = pqParam.frameNo;
                pqParam.dre.pBuffer = NULL;
                MUINT32 idx = 0;
                pqParam.dre.p_customSetting = (void*)getTuningFromNvram(mSensorIndex, idx, iMagicNo, NVRAM_TYPE_DRE, mLogLevel);
                pqParam.dre.customIdx = idx;
                str += String8::format("DRE cmd:0x%x type:0x%x userId:%" PRIu64 " buffer:0x%p nvram:0x%p idx:%d ",
                                        pqParam.dre.cmd, pqParam.dre.type,
                                        pqParam.dre.userId,
                                        pqParam.dre.pBuffer,
                                        pqParam.dre.p_customSetting,
                                        idx);
            }

            pqParam.p_mdpSetting = NULL;
            // should apply once in capture buffer
            if (enableCZ || enableDRE) {
                if (hasMdpSetting)
                    MY_LOGE("already existed mdp setting!");

                hasMdpSetting = MTRUE;
                if (mpMdpSetting == NULL) {
                    mpMdpSetting = new MDPSetting();
                    mpMdpSetting->size = MDPSETTING_MAX_SIZE;
                    mpMdpSetting->buffer = ::malloc(MDPSETTING_MAX_SIZE);
                }

                if (mpMdpSetting->buffer == NULL) {
                    MY_LOGE("fail to allocate mdp dbg buffer!");
                } else {
                    memset(mpMdpSetting->buffer, 0xFF, MDPSETTING_MAX_SIZE);
                    pqParam.p_mdpSetting = (void*) mpMdpSetting;
                    str += String8::format("setting:0x%p ", pqParam.p_mdpSetting);
                }

                // Face Data
                pqParam.p_faceInfor = pFdData;

                str += String8::format("PQtype:0x%x enable:%d portIdx:%d/%zu iso:%d sensorId:%d Timestamp:%d",
                        pqParam.type,
                        pqParam.enable,
                        pqParam.portIdx, mBufferItems.size(),
                        pqParam.iso,
                        pqParam.sensorId,
                        pqParam.timestamp);

                mpTransformer->setPQParameter(pqParam);
            }

            if (str.length() > 0)
                MY_LOGD("%s", str.string());

            if (i % 2 == 1 || i + 1 == mBufferItems.size()) {
                CAM_TRACE_BEGIN("mdp:execute");
                MBOOL hasSecond = i % 2 == 1;
                size_t base = hasSecond ? i - 1 : i;
                ret = mpTransformer->execute(
                                pSrcBuffer,
                                mBufferItems[base].mpImageBuffer,
                                (hasSecond) ? mBufferItems[base + 1].mpImageBuffer : 0L,
                                mBufferItems[base].mCrop,
                                (hasSecond) ? mBufferItems[base + 1].mCrop : 0L,
                                mBufferItems[base].mTransform,
                                (hasSecond) ? mBufferItems[base + 1].mTransform : 0L,
                                0xFFFFFFFF);
                CAM_TRACE_END();
            }
        }
    }
    mpFdReader->queryUnlock(vFdData);

    // Clear MDP setting's memory and update metadata
    if (hasMdpSetting) {
        unsigned char* pBuffer = static_cast<unsigned char*>(mpMdpSetting->buffer);
        MUINT32 size = mpMdpSetting->size;
        if (pBuffer) {
            MY_LOGD("Update Mdp debug info: addr %p, size %u %c %c", pBuffer, size, *pBuffer, *(pBuffer+1));
            if (mLogLevel > 2) {
                char filename[256] = {0};
                if (!makePath("data/vendor/camera_dump", 0660))
                    MY_LOGD("makePath[%s] fails", "data/vendor/camera_dump");
                sprintf(filename, "data/vendor/camera_dump/mdp_dump");
                saveBufToFile(filename, pBuffer, size);
            }

            IMetadata* pOutHalMeta = pNodeReq->acquireMetadata(MID_MAN_OUT_HAL);
            if (pOutHalMeta) {
                MY_LOGD_IF(mLogLevel, "set debug exif of mdp +");
                IMetadata exifMeta;
                tryGetMetadata<IMetadata>(pOutHalMeta, MTK_3A_EXIF_METADATA, exifMeta);
                if (DebugExifUtils::setDebugExif(
                        DebugExifUtils::DebugExifType::DEBUG_EXIF_RESERVE3,
                        static_cast<MUINT32>(MTK_RESVC_EXIF_DBGINFO_KEY),
                        static_cast<MUINT32>(MTK_RESVC_EXIF_DBGINFO_DATA),
                        size, pBuffer, &exifMeta) == nullptr)
                {
                    MY_LOGW("fail to set debug exif to metadata");
                }
                else
                {
                    trySetMetadata<IMetadata>(pOutHalMeta, MTK_3A_EXIF_METADATA, exifMeta);
                }
                MY_LOGD_IF(mLogLevel, "set debug exif of mdp -");
            }
        }
    }

    MY_LOGI("-, R/F Num: %d/%d [Finished]", requestNo, frameNo);
    CAM_TRACE_FMT_END();
    return ret;
}

MVOID MDPNode::onRequestFinish(RequestPtr& pRequest)
{
    pRequest->decNodeReference(NID_MDP);

    if (mDebugDump) {
        sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(NID_MDP);

        // Get Unique Key
        MINT32 iUniqueKey = 0;
        IMetadata* pInHalMeta = pNodeReq->acquireMetadata(MID_MAN_IN_HAL);
        tryGetMetadata<MINT32>(pInHalMeta, MTK_PIPELINE_UNIQUE_KEY, iUniqueKey);

        char filename[256] = {0};
        FILE_DUMP_NAMING_HINT hint;
        hint.UniqueKey          = iUniqueKey;
        hint.RequestNo          = pRequest->getRequestNo();
        hint.FrameNo            = pRequest->getFrameNo();
        extract_by_SensorOpenId(&hint, mSensorIndex);

        for (TypeID_T typeId = 0; typeId < NUM_OF_TYPE; typeId++) {
            BufferID_T bufId = pNodeReq->mapBufferID(typeId, OUTPUT);
            if (bufId == NULL_BUFFER)
                continue;

            IImageBuffer* pImgBuf = pNodeReq->acquireBuffer(bufId);
            if (pImgBuf == NULL)
                continue;

            extract(&hint, pImgBuf);
            genFileName_YUV(filename, sizeof(filename), &hint, YUV_PORT_UNDEFINED);

            pImgBuf->saveToFile(filename);
            MY_LOGD("dump output:%s", filename);
        }
    }

    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if (pRequest->getParameter(PID_THUMBNAIL_TIMING) == NSPipelinePlugin::eTiming_MDP)
        {
            if (pRequest->mpCallback != NULL) {
                MY_LOGD("Nofity next capture");
                pRequest->mpCallback->onContinue(pRequest);
            } else {
                MY_LOGW("have no request callback instance!");
            }
        }
    }

    dispatch(pRequest);
}

MERROR MDPNode::evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    auto& srcData = rInfer.getSharedSrcData();
    auto& dstData = rInfer.getSharedDstData();
    auto& features = rInfer.getSharedFeatures();
    auto& metadatas = rInfer.getSharedMetadatas();

    auto& src_0 = srcData.editItemAt(srcData.add());
    src_0.mTypeId = TID_MAN_FULL_YUV;
    src_0.mSizeId = SID_FULL;

    auto& dst_1 = dstData.editItemAt(dstData.add());
    dst_1.mTypeId = TID_MAN_CROP1_YUV;

    auto& dst_2 = dstData.editItemAt(dstData.add());
    dst_2.mTypeId = TID_MAN_CROP2_YUV;

    auto& dst_3 = dstData.editItemAt(dstData.add());
    dst_3.mTypeId = TID_THUMBNAIL;

    auto& dst_4 = dstData.editItemAt(dstData.add());
    dst_4.mTypeId = TID_JPEG;

    if (rInfer.mThumbnailTiming.hasBit(NSPipelinePlugin::eTiming_MDP)) {
        auto& dst_5 = dstData.editItemAt(dstData.add());
        dst_5.mTypeId = TID_POSTVIEW;
    }

    metadatas.push_back(MID_MAN_IN_P1_DYNAMIC);
    metadatas.push_back(MID_MAN_IN_APP);
    metadatas.push_back(MID_MAN_IN_HAL);

    rInfer.addNodeIO(nodeId, srcData, dstData, metadatas, features);

    return OK;
}



} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
