/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "P2ANode.h"

#define PIPE_CLASS_TAG "P2ANode"
#define PIPE_TRACE TRACE_P2A_NODE
#include <featurePipe/core/include/PipeLog.h>

#include <isp_tuning/isp_tuning.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include <mtkcam/drv/iopipe/SImager/IImageTransform.h>
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/drv/def/Dip_Notify_datatype.h>
#include <mtkcam/utils/TuningUtils/FileReadRule.h>

#include <cmath>
#include <camera_custom_nvram.h>

#include "../thread/CaptureTaskQueue.h"
#include <MTKDngOp.h>

using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace NSCam::NSIoPipe;
using namespace NSCam::TuningUtils;
using namespace NSCam::NSIoPipe::NSSImager;

#define ISP30_RULE01_CROP_OFFSET         (196)
#define ISP30_RULE02_CROP_OFFSET         (128)
#define ISP30_RULE02_RESIZE_RATIO        (8)

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

enum DeubgMode {
    AUTO    = -1,
    OFF     = 0,
    ON      = 1,
};

static MBOOL getHasVSDoFFeature(const RequestPtr& pRequest)
{
    const MBOOL hasDepthFeature = pRequest->hasFeature(FID_DEPTH) || pRequest->hasFeature(FID_DEPTH_3RD_PARTY);
    const MBOOL hasBokehFeature = pRequest->hasFeature(FID_BOKEH) || pRequest->hasFeature(FID_BOKEH_3RD_PARTY);
    return ((hasDepthFeature && hasBokehFeature) || pRequest->hasFeature(FID_PUREBOKEH_3RD_PARTY));
}

MVOID P2ANode::unpack(IImageBuffer* pPkInBuf, IImageBuffer* pUpkOutBuf )
{
    void *pUpkOutVa = (void *) (pUpkOutBuf->getBufVA(0));
    void *pPkInVa    = (void *) (pPkInBuf->getBufVA(0));
    MTKDngOp *MyDngop;
    DngOpResultInfo MyDngopResultInfo;
    DngOpImageInfo MyDngopImgInfo;
    int nImgWidth  = pPkInBuf->getImgSize().w;
    int nImgHeight = pPkInBuf->getImgSize().h;
    int nBufSize   = pPkInBuf->getBufSizeInBytes(0);
    int nImgStride = pPkInBuf->getBufStridesInBytes(0);
    // unpack algorithm
    MY_LOGD("Unpack +");
    MyDngop = MTKDngOp::createInstance(DRV_DNGOP_UNPACK_OBJ_SW);
    MyDngopImgInfo.Width = nImgWidth;
    MyDngopImgInfo.Height = nImgHeight;
    MyDngopImgInfo.Stride_src = nImgStride;
    MyDngopImgInfo.Stride_dst = pUpkOutBuf->getBufStridesInBytes(0);
    MyDngopImgInfo.BIT_NUM = 10;
    MyDngopImgInfo.BIT_NUM_DST = 10;
    MUINT32 buf_size = DNGOP_BUFFER_SIZE(nImgWidth * 2, nImgHeight);
    MyDngopImgInfo.Buff_size = buf_size;
    MyDngopImgInfo.srcAddr = pPkInVa;
    MyDngopResultInfo.ResultAddr = pUpkOutVa;
    MyDngop->DngOpMain((void*)&MyDngopImgInfo, (void*)&MyDngopResultInfo);
    MyDngop->destroyInstance(MyDngop);
    MY_LOGD("Unpack -");
    MY_LOGD("unpack processing. va[in]:%p, va[out]:%p", MyDngopImgInfo.srcAddr, MyDngopResultInfo.ResultAddr);
    MY_LOGD("img size(%dx%d) src stride(%d) bufSize(%d) -> dst stride(%d) bufSize(%zu)", nImgWidth, nImgHeight,
             MyDngopImgInfo.Stride_src,nBufSize, MyDngopImgInfo.Stride_dst , pUpkOutBuf->getBufSizeInBytes(0));
}

P2ANode::P2ANode(NodeID_T nid, const char* name, MINT32 policy, MINT32 priority)
    : CaptureFeatureNode(nid, name, 0, policy, priority)
    , mpIspHal(NULL)
    , mpIspHal2(NULL)
    , mpP2Opt(NULL)
    , mpP2Opt2(NULL)
    , mDipVer(0)
    , mSupportDRE(MFALSE)
    , mSupportCZ(MFALSE)
    , mISP3_0(MFALSE)
    , mTaskQueue(NULL)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);

    mDebugDS        = property_get_int32("vendor.debug.camera.ds.enable", -1);
    mDebugDSRatio   = property_get_int32("vendor.debug.camera.ds.ratio", 2);
    mDebugDRE       = property_get_int32("vendor.debug.camera.dre.enable", -1);
    mDebugCZ        = property_get_int32("vendor.debug.camera.cz.enable", -1);

    mDebugLoadIn    = (property_get_int32("vendor.debug.camera.dumpin.en", 0) == 2);

    mDebugDump      = property_get_int32("vendor.debug.camera.p2.dump", 0) > 0;
    mDebugUpkRaw    = property_get_int32("vendor.debug.camera.upkraw.dump", 0) > 0;

    mDebugImg2o     = property_get_int32("vendor.debug.camera.img2o.dump", 0) > 0;
    mDebugImg3o     = property_get_int32("vendor.debug.camera.img3o.dump", 0) > 0;


    TRACE_FUNC_EXIT();
}

P2ANode::~P2ANode()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::setSensorIndex(MINT32 sensorIndex, MINT32 sensorIndex2)
{
    CaptureFeatureNode::setSensorIndex(sensorIndex, sensorIndex2);
    mSensorFmt = (sensorIndex >= 0) ? getSensorRawFmt(mSensorIndex) : SENSOR_RAW_FMT_NONE;
    mSensorFmt2 = (sensorIndex2 >= 0) ? getSensorRawFmt(mSensorIndex2) : SENSOR_RAW_FMT_NONE;
    MY_LOGD("sensorIndex:%d -> sensorFmt:%#09x, sensorIndex2:%d -> sensorFmt2:%#09x",
        mSensorIndex, mSensorFmt, mSensorIndex2, mSensorFmt2);
}

MVOID P2ANode::setBufferPool(const android::sp<CaptureBufferPool> &pool)
{
    TRACE_FUNC_ENTER();
    mpBufferPool = pool;
    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::onInit()
{
    TRACE_FUNC_ENTER();
    CaptureFeatureNode::onInit();

    mTaskQueue = new CaptureTaskQueue();

    mpIspHal = MAKE_HalISP(mSensorIndex, "CFP");
    mpP2Opt = new P2Operator(LOG_TAG, mSensorIndex);

    if (mSensorIndex2 >= 0) {
        mpIspHal2 = MAKE_HalISP(mSensorIndex2, "CFP2");
        mpP2Opt2 = new P2Operator(LOG_TAG, mSensorIndex);
    }

    if (!NSIoPipe::NSPostProc::INormalStream::queryDIPInfo(mDipInfo)) {
        MY_LOGE("queryDIPInfo Error! Please check the error msg above!");
    }

    std::map<DP_ISP_FEATURE_ENUM, bool> mdpFeatures;
    DpIspStream::queryISPFeatureSupport(mdpFeatures);
    mSupportCZ = mdpFeatures[ISP_FEATURE_CLEARZOOM];
    mSupportDRE = mdpFeatures[ISP_FEATURE_DRE];
    mDipVer = mDipInfo[EDIPINFO_DIPVERSION];
    mISP3_0 = (mDipVer == EDIPHWVersion_30);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onUninit()
{
    TRACE_FUNC_ENTER();

    if (mpIspHal) {
        mpIspHal->destroyInstance("CFP");
        mpIspHal = NULL;
    }

    if (mpIspHal2) {
        mpIspHal2->destroyInstance("CFP2");
        mpIspHal2 = NULL;
    }

    if (mTaskQueue != NULL)
        delete mTaskQueue;

    TRACE_FUNC_EXIT();
    return MTRUE;
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
P2ANode::
enqueISP(RequestPtr& pRequest, shared_ptr<P2EnqueData>& pEnqueData)
{
    TRACE_FUNC_ENTER();

    MERROR ret = OK;
    P2EnqueData& enqueData = *pEnqueData.get();

    // Trigger Dump
    enqueData.mDebugDump = mDebugDump;
    enqueData.mDebugUpkRaw = mDebugUpkRaw;
    MINT32& frameNo = enqueData.mFrameNo;
    MINT32& requestNo = enqueData.mRequestNo;

    MBOOL master = enqueData.mSensorId == mSensorIndex;

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(NID_P2A);

    auto GetBuffer = [&](IImageBuffer*& rpBuf, BufferID_T bufId) -> IImageBuffer* {
        if (rpBuf)
            return rpBuf;
        if (bufId != NULL_BUFFER) {
            rpBuf = pNodeReq->acquireBuffer(bufId);
        }
        return rpBuf;
    };

    EnquePackage* pPackage = NULL;

#define ASSERT(predict, mesg)     \
    do {                          \
        if (!(predict)) {         \
            if (pPackage != NULL) \
                delete pPackage;  \
            MY_LOGE(mesg);        \
            return MFALSE;        \
        }                         \
    } while(0)

    // 1.input & output data
    IImageBuffer* pIMG2O = GetBuffer(enqueData.mIMG2O.mpBuf, enqueData.mIMG2O.mBufId);
    IImageBuffer* pIMG3O = GetBuffer(enqueData.mIMG3O.mpBuf, enqueData.mIMG3O.mBufId);
    IImageBuffer* pWROTO = GetBuffer(enqueData.mWROTO.mpBuf, enqueData.mWROTO.mBufId);
    IImageBuffer* pWDMAO = GetBuffer(enqueData.mWDMAO.mpBuf, enqueData.mWDMAO.mBufId);
    IImageBuffer* pCopy1 = GetBuffer(enqueData.mCopy1.mpBuf, enqueData.mCopy1.mBufId);
    IImageBuffer* pCopy2 = GetBuffer(enqueData.mCopy2.mpBuf, enqueData.mCopy2.mBufId);

    ASSERT(!!pIMG2O || !!pIMG3O || !!pWROTO || !!pWDMAO, "do not acquire a output buffer");

    IMetadata* pIMetaDynamic    = enqueData.mpIMetaDynamic;
    IMetadata* pIMetaApp        = enqueData.mpIMetaApp;
    IMetadata* pIMetaHal        = enqueData.mpIMetaHal;
    IMetadata* pOMetaApp        = enqueData.mpOMetaApp;
    IMetadata* pOMetaHal        = enqueData.mpOMetaHal;

    IImageBuffer* pIMGI = GetBuffer(enqueData.mIMGI.mpBuf, enqueData.mIMGI.mBufId);
    IImageBuffer* pLCEI = GetBuffer(enqueData.mLCEI.mpBuf, enqueData.mLCEI.mBufId);

    ASSERT(!!pIMGI, "do not acquire a input buffer");

    // 2. Prepare enque package
    SmartTuningBuffer pBufTuning = mpBufferPool->getTuningBuffer();
    PQParam* pPQParam = new PQParam();
    pPackage = new EnquePackage();
    pPackage->mpEnqueData = pEnqueData;
    pPackage->mTuningData = pBufTuning;
    pPackage->mpPQParam = pPQParam;
    pPackage->mpNode = this;
    if (mDebugUpkRaw) {
        SmartImageBuffer pUpkWorkingBuf = mpBufferPool->getImageBuffer(enqueData.mIMGI.mpBuf->getImgSize().w,
                                                                       enqueData.mIMGI.mpBuf->getImgSize().h,
                                                                       eImgFmt_BAYER10_UNPAK);
        pPackage->mUpkWorkingBuf = pUpkWorkingBuf;
    }
    // 3. Crop Calculation & add log
    const MSize& rImgiSize = pIMGI->getImgSize();
    String8 strEnqueLog;
    strEnqueLog += String8::format("Sensor(%d) Resized(%d) R/F Num: %d/%d, EnQ: IMGI Fmt(0x%x) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
               enqueData.mSensorId,
               enqueData.mResized,
               pRequest->getRequestNo(),
               pRequest->getFrameNo(),
               pIMGI->getImgFormat(),
               rImgiSize.w, rImgiSize.h,
               pIMGI->getBufVA(0),
               pIMGI->getBufPA(0));

    sp<CropCalculator::Factor> pFactor;
    if (enqueData.mWDMAO.mHasCrop ||
        enqueData.mWROTO.mHasCrop ||
        enqueData.mIMG2O.mHasCrop ||
        enqueData.mCopy1.mHasCrop ||
        enqueData.mCopy2.mHasCrop)
    {
        pFactor = mpCropCalculator->getFactor(pIMetaApp, pIMetaHal);
        ASSERT(pFactor != NULL, "can not get crop factor!");

        if (pOMetaApp != NULL) {
            MRect cropRegion = pFactor->mActiveCrop;
            if (pFactor->mEnableEis) {
                cropRegion.p.x += pFactor->mActiveEisMv.p.x;
                cropRegion.p.y += pFactor->mActiveEisMv.p.y;
            }
            // Update crop region to output app metadata
            trySetMetadata<MRect>(pOMetaApp, MTK_SCALER_CROP_REGION, cropRegion);
        }
    }

    auto GetCropRegion = [&](const char* sPort, P2Output& rOut, IImageBuffer* pImg) mutable {
        if (pImg == NULL)
            return;

        if (rOut.mHasCrop) {
            MSize cropSize = pImg->getImgSize();
            if (rOut.mTrans & eTransform_ROT_90)
                swap(cropSize.w, cropSize.h);

            mpCropCalculator->evaluate(pFactor, cropSize, rOut.mCropRegion, enqueData.mResized);
            if (enqueData.mScaleUp) {
                simpleTransform tranTG2DS(MPoint(0,0), enqueData.mScaleUpSize, rImgiSize);
                rOut.mCropRegion = transform(tranTG2DS, rOut.mCropRegion);
            }

        } else
            rOut.mCropRegion = MRect(rImgiSize.w, rImgiSize.h);

        strEnqueLog += String8::format(", %s Rot(%d) Crop(%d,%d)(%dx%d) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            sPort, rOut.mTrans,
            rOut.mCropRegion.p.x, rOut.mCropRegion.p.y,
            rOut.mCropRegion.s.w, rOut.mCropRegion.s.h,
            pImg->getImgSize().w, pImg->getImgSize().h,
            pImg->getBufVA(0), pImg->getBufPA(0));
    };

    GetCropRegion("WDMAO", enqueData.mWDMAO, pWDMAO);
    GetCropRegion("WROTO", enqueData.mWROTO, pWROTO);
    GetCropRegion("IMG2O", enqueData.mIMG2O, pIMG2O);
    GetCropRegion("IMG3O", enqueData.mIMG3O, pIMG3O);
    GetCropRegion("COPY1", enqueData.mCopy1, pCopy1);
    GetCropRegion("COPY2", enqueData.mCopy2, pCopy2);

    MY_LOGD("%s", strEnqueLog.string());

    // 3.1 ISP tuning
    TuningParam tuningParam = {NULL, NULL};
    {
        // For NDD
        trySetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_FRAME_NUMBER, frameNo);
        trySetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_REQUEST_NUMBER, requestNo);

        {
            const MBOOL isImgo = !enqueData.mResized;
            const MBOOL isMasterImgo = (master && isImgo);
            const MBOOL isBayerBayer = (mSensorFmt == SENSOR_RAW_Bayer) && (mSensorFmt2 == SENSOR_RAW_Bayer);
            const MBOOL isBayerMono = (mSensorFmt == SENSOR_RAW_Bayer) && (mSensorFmt2 == SENSOR_RAW_MONO);
            if (!isMasterImgo && (enqueData.mYuvRep || enqueData.mScaleUp || enqueData.mEnableMFB)) {
                MY_LOGW("no combined of non-MasterImgo with yuvRep(%d) or scaleUp(%d) or enableMFB(%d)",
                    enqueData.mYuvRep, enqueData.mScaleUp, enqueData.mEnableMFB);
            }

            IspProfileInfo profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture);
            // For Down Sacle
            if (enqueData.mYuvRep) {
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_YUV_Reprocess);
            } else if (enqueData.mScaleUp) {
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_Capture_MultiPass_HWNR);
                MINT32 resolution = enqueData.mScaleUpSize.w | enqueData.mScaleUpSize.h << 16;
                trySetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_RES_REVISED, resolution);
                // 0 or not exist: RAW->YUV, 1: YUV->YUV
                trySetMetadata<MINT32>(pIMetaHal, MTK_ISP_P2_IN_IMG_FMT, 1);
                MY_LOGD("apply profile(MultiPass_HWNR) revised resolution: 0x%x", resolution);
            } else if (enqueData.mEnableMFB) {
                profileInfo = MAKE_ISP_PROFILE_INFO(EIspProfile_MFNR_Before_Blend);
            // dualcam vsdof part
            } else if (enqueData.mEnableVSDoF) {
                if( !isBayerBayer && !isBayerMono ) {
                    MY_LOGW("unknown seneorFmt, sensorIndex:%d -> sensorFmt:%u, sensorIndex2:%d -> sensorFmt2:%u",
                            mSensorIndex, mSensorIndex2, mSensorFmt, mSensorFmt2);
                } else {
                    IspProfileHint hint;
                    hint.mSensorAliasName = master ? eSAN_Master : eSAN_Sub_01;
                    hint.mSensorConfigType = isBayerBayer ? eSCT_BayerBayer : eSCT_BayerMono;
                    hint.mRawImageType = isImgo ? eRIT_Imgo : eRIT_Rrzo;
                    profileInfo = IspProfileManager::get(hint);
                }
            }
            trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_ISP_PROFILE, profileInfo.mValue);
            MY_LOGD("set ispProfile, frame#:%d, req#:%d, profile:%d(%s), master:%d, imgo:%d, yuvRep:%d, scaleUp:%d, mfb:%d, vsdof:%d, bb:%d, bm:%d",
                frameNo, requestNo,
                profileInfo.mValue, profileInfo.mName.c_str(),
                master, isImgo,
                enqueData.mYuvRep, enqueData.mScaleUp, enqueData.mEnableMFB, enqueData.mEnableVSDoF,
                isBayerBayer, isBayerMono);
        }
        // Consturct tuning parameter
        {
            tuningParam.pRegBuf = reinterpret_cast<void*>(pBufTuning->mpVA);

            // LCEI
            if (pLCEI != NULL) {
                ASSERT(!enqueData.mResized, "use LCSO for RRZO buffer, should not happended!");
                tuningParam.pLcsBuf = reinterpret_cast<void*>(pLCEI);
            }

            // USE resize raw-->set PGN 0
            if (enqueData.mResized) {
                trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_PGN_ENABLE, 0);
            } else {
                trySetMetadata<MUINT8>(pIMetaHal, MTK_3A_PGN_ENABLE, 1);
            }
            MetaSet_T inMetaSet;
            inMetaSet.appMeta = *pIMetaApp;
            inMetaSet.halMeta = *pIMetaHal;

            MetaSet_T outMetaSet;
            if (master)
                ret = mpIspHal->setP2Isp(0, inMetaSet, &tuningParam, &outMetaSet);
            else
                ret = mpIspHal2->setP2Isp(0, inMetaSet, &tuningParam, &outMetaSet);

            ASSERT(ret == OK, "fail to set ISP");

            if (pOMetaHal != NULL) {
                (*pOMetaHal) = inMetaSet.halMeta + outMetaSet.halMeta;
#if MTK_ISP_SUPPORT_COLOR_SPACE
                // If flag on, the NVRAM will always prepare tuning data for P3 color space
                trySetMetadata<MINT32>(pOMetaHal, MTK_ISP_COLOR_SPACE, MTK_ISP_COLOR_SPACE_DISPLAY_P3);
#endif
            }
            if (pOMetaApp != NULL)
                (*pOMetaApp) += outMetaSet.appMeta;
        }
    }

    // 3.2 Fill PQ Param
    {
        MINT32 iIsoValue = 0;
        if (!tryGetMetadata<MINT32>(pIMetaDynamic, MTK_SENSOR_SENSITIVITY, iIsoValue))
            MY_LOGW("can not get iso value");

        MINT32 iMagicNum = 0;
        if (!tryGetMetadata<MINT32>(pIMetaHal, MTK_P1NODE_PROCESSOR_MAGICNUM, iMagicNum))
            MY_LOGW("can not get magic number");

        MINT32 iRealLv = 0;
        if (!tryGetMetadata<MINT32>(pIMetaHal, MTK_REAL_LV, iRealLv))
            MY_LOGW("can not get read lv");

        auto fillPQ = [&](DpPqParam& rPqParam, PortID port, MBOOL enableCZ) {
            String8 strEnqueLog;
            strEnqueLog += String8::format("Port(%d) Timestamp:%d",(int) port ,enqueData.mUniqueKey);

            rPqParam.enable           = false;
            rPqParam.scenario         = MEDIA_ISP_CAPTURE;
            rPqParam.u.isp.iso        = iIsoValue;
            rPqParam.u.isp.lensId     = enqueData.mSensorId;
            rPqParam.u.isp.LV         = iRealLv;
            rPqParam.u.isp.timestamp  = enqueData.mUniqueKey;
            rPqParam.u.isp.frameNo    = enqueData.mFrameNo;
            rPqParam.u.isp.requestNo  = enqueData.mRequestNo;

            if (mSupportCZ && enableCZ) {
                rPqParam.enable = (PQ_ULTRARES_EN);
                ClearZoomParam& rCzParam = rPqParam.u.isp.clearZoomParam;

                rCzParam.captureShot = CAPTURE_SINGLE;

                MUINT32 idx = 0;
                rCzParam.p_customSetting = (void*)getTuningFromNvram(enqueData.mSensorId, idx, iMagicNum, NVRAM_TYPE_CZ, false);
                strEnqueLog += String8::format(" CZ Capture:%d idx:0x%x",
                    rCzParam.captureShot,
                    idx);
            }

            if (mSupportDRE && enqueData.mEnableDRE) {
                rPqParam.enable |= (PQ_DRE_EN);
                rPqParam.scenario = MEDIA_ISP_CAPTURE;

                DpDREParam& rDreParam = rPqParam.u.isp.dpDREParam;
                rDreParam.cmd         = DpDREParam::Cmd::Initialize | DpDREParam::Cmd::Generate;
                rDreParam.userId      = rPqParam.u.isp.frameNo;
                rDreParam.buffer      = nullptr;
                MUINT32 idx = 0;
                rDreParam.p_customSetting = (void*)getTuningFromNvram(enqueData.mSensorId, idx, iMagicNum, NVRAM_TYPE_DRE, false);
                rDreParam.customIndex     = idx;
                strEnqueLog += String8::format(" DRE cmd:0x%x buffer:%p p_customSetting:%p idx:%d",
                                rDreParam.cmd, rDreParam.buffer, rDreParam.p_customSetting, idx);

            }

            strEnqueLog += String8::format(" PQ Enable:0x%x scenario:%d iso:%d",
                    rPqParam.enable,
                    rPqParam.scenario,
                    iIsoValue);

            MY_LOGD("%s", strEnqueLog.string());
        };

        // WROT
        if (pWROTO != NULL) {
            DpPqParam* pIspParam_WROT = new DpPqParam();
            fillPQ(*pIspParam_WROT, PORT_WROTO, enqueData.mWROTO.mClearZoom);
            pPQParam->WROTPQParam = static_cast<void*>(pIspParam_WROT);
        }

        // WDMA
        if (pWDMAO != NULL) {
            DpPqParam* pIspParam_WDMA = new DpPqParam();
            fillPQ(*pIspParam_WDMA, PORT_WDMAO, enqueData.mWDMAO.mClearZoom);
            pPQParam->WDMAPQParam = static_cast<void*>(pIspParam_WDMA);
        }

    }

    // 3.3 Srz tuning for Imgo (LCE not applied to rrzo)
    if (!enqueData.mScaleUp && !enqueData.mResized) {
        auto fillSRZ = [&]() -> ModuleInfo* {
            if (mDipVer < EDIPHWVersion_50) {
                MY_LOGD_IF(0, "isp version(0x%x) < 5.0, dont need SrzInfo", mDipVer);
                return NULL;
            }
            // srz4 config
            // ModuleInfo srz4_module;
            ModuleInfo* p = new ModuleInfo();
            p->moduleTag = EDipModule_SRZ4;
            p->frameGroup=0;

            _SRZ_SIZE_INFO_* pSrzParam = new _SRZ_SIZE_INFO_;
            if (pLCEI) {
                pSrzParam->in_w = pLCEI->getImgSize().w;
                pSrzParam->in_h = pLCEI->getImgSize().h;
                pSrzParam->crop_w = pLCEI->getImgSize().w;
                pSrzParam->crop_h = pLCEI->getImgSize().h;
            }
            if (pIMGI) {
                pSrzParam->out_w = pIMGI->getImgSize().w;
                pSrzParam->out_h = pIMGI->getImgSize().h;
            }

            p->moduleStruct = reinterpret_cast<MVOID*> (pSrzParam);

            return p;
        };
        pPackage->mpModuleInfo = fillSRZ();
    }

    // 4.create enque param
    NSIoPipe::QParams qParam;

    // 4.1 QParam template
    MINT32 iFrameNum = 0;
    QParamTemplateGenerator qPTempGen = QParamTemplateGenerator(
            iFrameNum, enqueData.mSensorId,
            enqueData.mTimeSharing ? ENormalStreamTag_Vss : ENormalStreamTag_Cap);

    qPTempGen.addInput(PORT_IMGI);
    if (!enqueData.mScaleUp && !enqueData.mResized && tuningParam.pLsc2Buf != NULL) {
        if (mDipVer == EDIPHWVersion_50) {
            qPTempGen.addInput(PORT_IMGCI);
        } else if (mDipVer == EDIPHWVersion_40) {
            qPTempGen.addInput(PORT_DEPI);
        } else {
            MY_LOGE("should not have LSC buffer in ISP 3.0");
        }
    }

    if (!enqueData.mScaleUp && !enqueData.mResized && pLCEI != NULL) {
        qPTempGen.addInput(PORT_LCEI);
        if (pPackage->mpModuleInfo != NULL) {
            qPTempGen.addModuleInfo(EDipModule_SRZ4,  pPackage->mpModuleInfo->moduleStruct);
            qPTempGen.addInput(PORT_DEPI);
        }
    }

    if (!enqueData.mScaleUp && tuningParam.pBpc2Buf != NULL) {
        qPTempGen.addInput(PORT_IMGBI);
    }

    if (pIMG2O != NULL) {
        qPTempGen.addOutput(PORT_IMG2O, 0);
        qPTempGen.addCrop(eCROP_CRZ, enqueData.mIMG2O.mCropRegion.p, enqueData.mIMG2O.mCropRegion.s, pIMG2O->getImgSize());
    }

    if (pIMG3O != NULL) {
        qPTempGen.addOutput(PORT_IMG3O, 0);
    }

    if (pWROTO != NULL) {
        qPTempGen.addOutput(PORT_WROTO, enqueData.mWROTO.mTrans);
        qPTempGen.addCrop(eCROP_WROT, enqueData.mWROTO.mCropRegion.p, enqueData.mWROTO.mCropRegion.s, pWROTO->getImgSize());
    }

    if (pWDMAO != NULL) {
        qPTempGen.addOutput(PORT_WDMAO, 0);
        qPTempGen.addCrop(eCROP_WDMA, enqueData.mWDMAO.mCropRegion.p, enqueData.mWDMAO.mCropRegion.s, pWDMAO->getImgSize());
    }

    qPTempGen.addExtraParam(EPIPE_MDP_PQPARAM_CMD, (MVOID*)pPQParam);

    ret = qPTempGen.generate(qParam) ? OK : BAD_VALUE;
    ASSERT(ret == OK, "fail to create QParams");

    // 4.2 QParam filler
    QParamTemplateFiller qParamFiller(qParam);
    qParamFiller.insertInputBuf(iFrameNum, PORT_IMGI, pIMGI);
    qParamFiller.insertTuningBuf(iFrameNum, pBufTuning->mpVA);

    if (!enqueData.mScaleUp && !enqueData.mResized && tuningParam.pLsc2Buf != NULL) {
        if (mDipVer == EDIPHWVersion_50) {
            qParamFiller.insertInputBuf(iFrameNum,  PORT_IMGCI,  static_cast<IImageBuffer*>(tuningParam.pLsc2Buf));
        } else if (mDipVer == EDIPHWVersion_40) {
            qParamFiller.insertInputBuf(iFrameNum,  PORT_DEPI,  static_cast<IImageBuffer*>(tuningParam.pLsc2Buf));
        }
    }

    if (!enqueData.mScaleUp && !enqueData.mResized && pLCEI != NULL) {
        qParamFiller.insertInputBuf(iFrameNum, PORT_LCEI, pLCEI);
        if(pPackage->mpModuleInfo != NULL){
            qParamFiller.insertInputBuf(iFrameNum, PORT_DEPI, pLCEI);
        }
    }

    if (!enqueData.mScaleUp && tuningParam.pBpc2Buf != NULL) {
        qParamFiller.insertInputBuf(iFrameNum,  PORT_IMGBI,  static_cast<IImageBuffer*>(tuningParam.pBpc2Buf));
    }

    if (pIMG2O != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_IMG2O, pIMG2O);
    }

    if (pIMG3O != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_IMG3O, pIMG3O);
    }

    if (pWROTO != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_WROTO, pWROTO);
    }

    if (pWDMAO != NULL) {
        qParamFiller.insertOutputBuf(iFrameNum, PORT_WDMAO, pWDMAO);
    }

    qParamFiller.setInfo(iFrameNum, requestNo, requestNo, enqueData.mTaskId);

    ret = qParamFiller.validate() ? OK : BAD_VALUE;
    ASSERT(ret == OK, "fail to create QParamFiller");

    // 5. prepare rest buffers using mdp copy
    auto IsCovered = [](P2Output& rSrc, MDPOutput& rDst) {
        if (rSrc.mpBuf == NULL || rDst.mpBuf == NULL)
            return MFALSE;

        // Ignore gary image
        MINT srcFmt = rSrc.mpBuf->getImgFormat();
        if (srcFmt == eImgFmt_Y8)
            return MFALSE;

        MSize& srcCrop = rSrc.mCropRegion.s;
        MSize& dstCrop = rDst.mCropRegion.s;
        // Make sure that the source FOV covers the destination FOV
#define FOV_THRES (1)
        if (srcCrop.w * (100 + FOV_THRES) < dstCrop.w * 100 ||
            srcCrop.h * (100 + FOV_THRES) < dstCrop.h * 100)
            return MFALSE;
#undef FOV_THRES

        MSize srcSize = rSrc.mpBuf->getImgSize();
        MSize dstSize = rDst.mpBuf->getImgSize();
        if (rSrc.mTrans & eTransform_ROT_90)
            swap(srcSize.w, srcSize.h);

        if (rDst.mTrans & eTransform_ROT_90)
            swap(dstSize.w, dstSize.h);

        // Make sure that the source image is bigger than destination image
#define SIZE_THRES (5)
        if (srcSize.w * (100 + SIZE_THRES) < dstSize.w * 100 ||
            srcSize.h * (100 + SIZE_THRES) < dstSize.h * 100)
            return MFALSE;
#undef SIZE_THRES

        simpleTransform tranCrop2SrcBuf(rSrc.mCropRegion.p, rSrc.mCropRegion.s, srcSize);
        MRect& rCropRegion = rDst.mSourceCrop;
        rCropRegion = transform(tranCrop2SrcBuf, rDst.mCropRegion);

        // Boundary Check for FOV tolerance
        if (rCropRegion.p.x < 0)
            rCropRegion.p.x = 0;
        if (rCropRegion.p.y < 0)
            rCropRegion.p.y = 0;

        if ((rCropRegion.p.x + rCropRegion.s.w) > srcSize.w)
            rCropRegion.s.w = srcSize.w - rCropRegion.p.x;
        if ((rCropRegion.p.y + rCropRegion.s.h) > srcSize.h)
            rCropRegion.s.h = srcSize.h - rCropRegion.p.y;

        // Make 2 bytes alignment
        rCropRegion.s.w &= ~(0x1);
        rCropRegion.s.h &= ~(0x1);

        if (rSrc.mTrans & eTransform_ROT_90) {
            swap(rCropRegion.p.x, rCropRegion.p.y);
            swap(rCropRegion.s.w, rCropRegion.s.h);
        }

        rDst.mpSource = rSrc.mpBuf;

        MUINT32& rSrcTrans = rSrc.mTrans;
        MUINT32& rDstTrans = rDst.mTrans;
        // Use XOR to calucate the transform, where the source image has a rotation or flip
        rDst.mSourceTrans = rSrcTrans ^ rDstTrans;
        // MDP does image rotation after flip.
        // If the source has a rotation and the destinatio doesn't, do the exceptional rule
        if (rSrcTrans & eTransform_ROT_90 && ~rDstTrans & eTransform_ROT_90) {
            if ((rSrcTrans & eTransform_ROT_180) == (rDstTrans & eTransform_ROT_180))
                rDst.mSourceTrans = eTransform_ROT_90 | eTransform_FLIP_V | eTransform_FLIP_H;
            else if ((rSrcTrans & eTransform_ROT_180) == (~rDstTrans & eTransform_ROT_180))
                rDst.mSourceTrans = eTransform_ROT_90;
        }

        return MTRUE;
    };

    // select a buffer source for MDP copy
    P2Output* pFirstHit = NULL;
    if (pCopy1 != NULL) {
        if (IsCovered(enqueData.mIMG2O, enqueData.mCopy1))
            pFirstHit = &enqueData.mIMG2O;
        else if (IsCovered(enqueData.mWROTO, enqueData.mCopy1))
            pFirstHit = &enqueData.mWROTO;
        else if (IsCovered(enqueData.mWDMAO, enqueData.mCopy1))
            pFirstHit = &enqueData.mWDMAO;
        else
            MY_LOGE("Copy1's FOV is not covered by P2 first-run output");
    }

    if (pCopy2 != NULL) {
        if (pFirstHit != NULL && IsCovered(*pFirstHit, enqueData.mCopy2)) {
            MY_LOGD("Use the same output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mIMG2O && IsCovered(enqueData.mIMG2O, enqueData.mCopy2)) {
            if (!IsCovered(enqueData.mIMG2O, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mWROTO && IsCovered(enqueData.mWROTO, enqueData.mCopy2)) {
            if (IsCovered(enqueData.mWROTO, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else if (pFirstHit != &enqueData.mWDMAO && IsCovered(enqueData.mWDMAO, enqueData.mCopy2)) {
            if (!IsCovered(enqueData.mWDMAO, enqueData.mCopy1))
                MY_LOGD("Use different output to serve two MDP outputs");
        } else {
            MY_LOGE("Copy2's FOV is not covered by P2 first-run output");
        }
    }



    // 6.enque
    pPackage->start();

    // callbacks
    qParam.mpfnCallback = onP2SuccessCallback;
    qParam.mpfnEnQFailCallback = onP2FailedCallback;
    qParam.mpCookie = (MVOID*) pPackage;

    CAM_TRACE_ASYNC_BEGIN("P2_Enque", (enqueData.mFrameNo << 3) + enqueData.mTaskId);

    // p2 enque
    if (master)
        ret = mpP2Opt->enque(qParam, LOG_TAG);
    else
        ret = mpP2Opt2->enque(qParam, LOG_TAG);

    ASSERT(ret == OK, "fail to enque P2");

    TRACE_FUNC_EXIT();
    return MTRUE;

#undef ASSERT

}

MBOOL P2ANode::onThreadStart()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onThreadStop()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onData(DataID id, RequestPtr &pRequest)
{
    TRACE_FUNC_ENTER();
    MY_LOGD_IF(mLogLevel, "Frame %d: %s arrived", pRequest->getRequestNo(), PathID2Name(id));
    MBOOL ret = MTRUE;

    if (pRequest->isSatisfied(mNodeId)) {
        mRequests.enque(pRequest);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr pRequest;

    CAM_TRACE_CALL();

    if (!waitAllQueue()) {
        return MFALSE;
    }

    if (!mRequests.deque(pRequest)) {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    } else if (pRequest == NULL) {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    TRACE_FUNC_ENTER();

    pRequest->mTimer.startP2A();
    onRequestProcess(pRequest);

    // Default timing of next capture is P2 start
    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if (!pRequest->hasParameter(PID_THUMBNAIL_TIMING))
        {
            MINT32 frameCount = pRequest->getParameter(PID_FRAME_COUNT);
            MINT32 frameIndex = pRequest->getParameter(PID_FRAME_INDEX);

            if ((frameCount < 2 && frameIndex < 1) || frameCount == frameIndex + 1)
            {
                if (pRequest->mpCallback != NULL) {
                    MY_LOGI("Nofity next capture at P2 beginning(%d/%d)", frameIndex, frameCount);
                    pRequest->mpCallback->onContinue(pRequest);
                } else {
                    MY_LOGW("have no request callback instance!");
                }
            }
        }
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID
P2ANode::
onP2SuccessCallback(QParams& rParams)
{
    EnquePackage* pPackage = (EnquePackage*) (rParams.mpCookie);
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    P2ANode* pNode = pPackage->mpNode;

    CAM_TRACE_ASYNC_END("P2_Enque", (rData.mFrameNo << 3) + rData.mTaskId);
    pPackage->stop();

    MY_LOGI("R/F Num: %d/%d, task:%d, timeconsuming: %dms",
            rData.mRequestNo,
            rData.mFrameNo,
            rData.mTaskId,
            pPackage->getElapsed());

    if (rData.mDebugDump) {
        char filename[256] = {0};
        FILE_DUMP_NAMING_HINT hint;
        hint.UniqueKey          = rData.mUniqueKey;
        hint.RequestNo          = rData.mRequestNo;
        hint.FrameNo            = rData.mFrameNo;

        extract_by_SensorOpenId(&hint, rData.mSensorId);

        auto DumpYuvBuffer = [&](IImageBuffer* pImgBuf, YUV_PORT port, const char* pStr) -> MVOID {
            if (pImgBuf == NULL)
                return;

            extract(&hint, pImgBuf);
            if (pStr == NULL) {
                genFileName_YUV(filename, sizeof(filename), &hint, port);
            } else {
                genFileName_YUV(filename, sizeof(filename), &hint, port, pStr);
            }

            pImgBuf->saveToFile(filename);
            MY_LOGD("Dump YUV: %s", filename);
        };

        auto DumpLcsBuffer = [&](IImageBuffer* pImgBuf, const char* pStr) -> MVOID {
            if (pImgBuf == NULL)
                return;

            extract(&hint, pImgBuf);
            genFileName_LCSO(filename, sizeof(filename), &hint, pStr);
            pImgBuf->saveToFile(filename);
            MY_LOGD("Dump LCEI: %s", filename);
        };

        auto DumpRawBuffer = [&](IImageBuffer* pImgBuf, RAW_PORT port, const char* pStr) -> MVOID {
            if (pImgBuf == NULL)
                return;

            extract(&hint, pImgBuf);
            genFileName_RAW(filename, sizeof(filename), &hint, port, pStr);
            pImgBuf->saveToFile(filename);
            MY_LOGD("Dump RAW: %s", filename);
        };

        String8 str;
        if (rData.mEnableMFB) {
            DumpYuvBuffer(rData.mIMG3O.mpBuf, YUV_PORT_IMG3O, NULL);
            DumpYuvBuffer(rData.mIMG2O.mpBuf, YUV_PORT_IMG2O, NULL);

            // do NOT show sensor name for MFNR naming
            hint.SensorDev = -1;

            MINT32 iso = 0;
            MINT64 exp = 0;
            tryGetMetadata<MINT32>(rData.mpIMetaDynamic, MTK_SENSOR_SENSITIVITY, iso);
            tryGetMetadata<MINT64>(rData.mpIMetaDynamic, MTK_SENSOR_EXPOSURE_TIME, exp);

            // convert ns into us
            exp /= 1000;
            str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-yuv", iso, exp);
            DumpYuvBuffer(rData.mWDMAO.mpBuf, YUV_PORT_NULL, str.string());
            str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-qyuv", iso, exp);
            DumpYuvBuffer(rData.mWROTO.mpBuf, YUV_PORT_NULL, str.string());

            IImageBuffer* pLCEI = rData.mLCEI.mpBuf;
            if (pLCEI != NULL) {
                str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-lcso__%dx%d",
                        iso, exp,
                        pLCEI->getImgSize().w,
                        pLCEI->getImgSize().h);
                DumpLcsBuffer(pLCEI, str.string());
            }

            str = String8::format("mfll-iso-%d-exp-%" PRId64 "-bfbld-raw", iso, exp);
            if(rData.mDebugUpkRaw){
               SmartImageBuffer pUpkWorkingBuf = pPackage->mUpkWorkingBuf;
               unpack(rData.mIMGI.mpBuf,pUpkWorkingBuf->mImageBuffer.get());
               DumpRawBuffer(pUpkWorkingBuf->mImageBuffer.get(), RAW_PORT_NULL, str.string());
            }else{
               DumpRawBuffer(rData.mIMGI.mpBuf, RAW_PORT_NULL, str.string());
            }
        } else {

            MUINT32 run = rData.mScaleUp ? 2 : 1;

            const char* pStr = NULL;
            if (run > 1) {
                str = String8::format("run%dout", run);
                pStr = str.string();
            }

            DumpYuvBuffer(rData.mIMG3O.mpBuf, YUV_PORT_IMG3O, pStr);
            DumpYuvBuffer(rData.mIMG2O.mpBuf, YUV_PORT_IMG2O, pStr);
            DumpYuvBuffer(rData.mWDMAO.mpBuf, YUV_PORT_WDMAO, pStr);
            DumpYuvBuffer(rData.mWROTO.mpBuf, YUV_PORT_WROTO, pStr);

            if (run < 2) {
                DumpLcsBuffer(rData.mLCEI.mpBuf, NULL);
                if (!rData.mYuvRep) {
                    if(rData.mDebugUpkRaw){
                       SmartImageBuffer pUpkWorkingBuf = pPackage->mUpkWorkingBuf;
                       unpack(rData.mIMGI.mpBuf,pUpkWorkingBuf->mImageBuffer.get());
                       DumpRawBuffer(
                               pUpkWorkingBuf->mImageBuffer.get(),
                               rData.mResized ? RAW_PORT_RRZO : RAW_PORT_IMGO,
                               NULL);
                    }else{
                       DumpRawBuffer(
                               rData.mIMGI.mpBuf,
                               rData.mResized ? RAW_PORT_RRZO : RAW_PORT_IMGO,
                               NULL);
                   }
                }
            }
        }
    }

    MBOOL hasCopyTask = MFALSE;
    if (rData.mCopy1.mpBuf != NULL || rData.mCopy2.mpBuf != NULL) {
        CaptureTaskQueue* pTaskQueue = pNode->mTaskQueue;
        if (pTaskQueue != NULL) {

            std::function<void()> task =
                [pPackage]()
                {
                    copyBuffers(pPackage);
                    delete pPackage;
                };

            pTaskQueue->addTask(task);
            hasCopyTask = MTRUE;
        }
    }

    if (!hasCopyTask) {
        // Could early callback only if there is no copy task
        if (rData.mWROTO.mEarlyRelease || rData.mWDMAO.mEarlyRelease) {

            RequestPtr pRequest = rData.mpHolder->mpRequest;
            sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(NID_P2A);

            if (rData.mWROTO.mBufId != NULL_BUFFER)
                pNodeReq->releaseBuffer(rData.mWROTO.mBufId);
            if (rData.mWDMAO.mBufId != NULL_BUFFER)
                pNodeReq->releaseBuffer(rData.mWDMAO.mBufId);
        }

        delete pPackage;
    }
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID
P2ANode::
onP2FailedCallback(QParams& rParams)
{
    EnquePackage* pPackage = (EnquePackage*) (rParams.mpCookie);
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    rData.mpHolder->mStatus = UNKNOWN_ERROR;

    CAM_TRACE_ASYNC_END("P2_Enque", (rData.mFrameNo << 3) + rData.mTaskId);
    pPackage->stop();

    MY_LOGE("R/F Num: %d/%d, task:%d, timeconsuming: %dms",
            rData.mRequestNo,
            rData.mFrameNo,
            rData.mTaskId,
            pPackage->getElapsed());

    delete pPackage;
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL
P2ANode::
onRequestProcess(RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();

#ifdef GTEST
    MY_LOGD("run GTEST, return directly, request:%d", requestNo);
    dispatch(pRequest);
    return MTRUE;
#endif

    CAM_TRACE_FMT_BEGIN("p2a:process|r%df%d", requestNo, frameNo);
    MY_LOGI("+, R/F Num: %d/%d", requestNo, frameNo);

    sp<CaptureFeatureNodeRequest> pNodeReq = pRequest->getNodeRequest(NID_P2A);
    MBOOL ret;

    // 0. Create request holder
    incExtThreadDependency();
    shared_ptr<RequestHolder> pHolder(new RequestHolder(pRequest),
            [=](RequestHolder* p) mutable
            {
                if (p->mStatus != OK)
                    p->mpRequest->addParameter(PID_FAILURE, 1);

                onRequestFinish(p->mpRequest);
                decExtThreadDependency();
                delete p;
            }
    );

    // Make sure that requests are finished in order
    {
        shared_ptr<RequestHolder> pLastHolder = mpLastHolder.lock();
        if (pLastHolder != NULL)
            pLastHolder->mpPrecedeOver = pHolder;

        mpLastHolder = pHolder;
    }

    // check drop frame
    if (pRequest->hasParameter(PID_FRAME_DROP))
    {
        MY_LOGI("drop Frame, R/F Num: %d/%d", requestNo, frameNo);
        return MTRUE;
    }

    // 0-1. Acquire Metadata
    IMetadata* pIMetaDynamic    = pNodeReq->acquireMetadata(MID_MAN_IN_P1_DYNAMIC);
    IMetadata* pIMetaApp        = pNodeReq->acquireMetadata(MID_MAN_IN_APP);
    IMetadata* pIMetaHal        = pNodeReq->acquireMetadata(MID_MAN_IN_HAL);
    IMetadata* pOMetaApp        = pNodeReq->acquireMetadata(MID_MAN_OUT_APP);
    IMetadata* pOMetaHal        = pNodeReq->acquireMetadata(MID_MAN_OUT_HAL);

    IMetadata* pIMetaHal2       = NULL;
    IMetadata* pIMetaDynamic2   = NULL;
    if (hasSubSensor()) {
        pIMetaHal2 = pNodeReq->acquireMetadata(MID_SUB_IN_HAL);
        pIMetaDynamic2 = pNodeReq->acquireMetadata(MID_SUB_IN_P1_DYNAMIC);
    }

    // 0-2. Get Data
    MINT32 uniqueKey = 0;
    tryGetMetadata<MINT32>(pIMetaHal, MTK_PIPELINE_UNIQUE_KEY, uniqueKey);

    MINT32 iIsoValue = 0;
    tryGetMetadata<MINT32>(pIMetaDynamic, MTK_SENSOR_SENSITIVITY, iIsoValue);

    // 1. Resized RAW of main sensor
    {
        BufferID_T uOResizedYuv = pNodeReq->mapBufferID(TID_MAN_RSZ_YUV, OUTPUT);
        BufferID_T uOPostview = mISP3_0 ? pNodeReq->mapBufferID(TID_POSTVIEW, OUTPUT) : NULL_BUFFER;
        BufferID_T uOThumbnail = mISP3_0 ? pNodeReq->mapBufferID(TID_THUMBNAIL, OUTPUT) : NULL_BUFFER;
        if ((uOResizedYuv & uOPostview & uOThumbnail) != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder             = pHolder;
            rEnqueData.mIMGI.mBufId         = pNodeReq->mapBufferID(TID_MAN_RSZ_RAW, INPUT);
            rEnqueData.mWDMAO.mBufId        = uOResizedYuv;
            rEnqueData.mWROTO.mBufId        = uOPostview;
            rEnqueData.mWROTO.mHasCrop      = (uOPostview != NULL_BUFFER);
            rEnqueData.mWROTO.mEarlyRelease = (uOPostview != NULL_BUFFER);
            rEnqueData.mIMG2O.mBufId        = uOThumbnail;
            rEnqueData.mIMG2O.mHasCrop      = (uOThumbnail != NULL_BUFFER);
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal;
            rEnqueData.mSensorId  = mSensorIndex;
            rEnqueData.mResized   = MTRUE;
            rEnqueData.mEnableVSDoF = getHasVSDoFFeature(pRequest);
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = 0;
            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("main sensor: resized yuv failed!");
                return MFALSE;
            }
        }
    }

    // 2. Full RAW of main sensor
    // YUV Reprocessing
    MBOOL isYuvRep = pNodeReq->mapBufferID(TID_MAN_FULL_YUV, INPUT) != NULL_BUFFER;
    // Down Scale: Only for IMGO
    MINT32 iDSRatio = 1;
    IImageBuffer* pDownScaleBuffer = NULL;
    MSize fullSize;
    MSize downSize;

    MBOOL isRunDS = MFALSE;

    if (mISP3_0 || isYuvRep || pRequest->getParameter(PID_FRAME_COUNT) > 1)
    {
        // do NOT execute down-scale if multi-frame blending or YUV reprocessing
    } else if (mDebugDS == OFF) {
        // do NOT execute down-scale if force DS off
    } else if (mDebugDS == ON) {
        iDSRatio = mDebugDSRatio;
        isRunDS  = MTRUE;
    } else {
#if MTK_CAM_NEW_NVRAM_SUPPORT
        MINT32 iMagicNo = 0;
        MUINT32 idx = 1;
        tryGetMetadata<MINT32>(pIMetaHal, MTK_P1NODE_PROCESSOR_MAGICNUM, iMagicNo);

        NVRAM_CAMERA_FEATURE_SWNR_THRES_STRUCT* t = (NVRAM_CAMERA_FEATURE_SWNR_THRES_STRUCT*)
            getTuningFromNvram(mSensorIndex, idx, iMagicNo, NVRAM_TYPE_SWNR_THRES, mLogLevel > 0);
        if (t != NULL) {
            iDSRatio = t->downscale_ratio;
            isRunDS = t->downscale_thres <= iIsoValue;

            MY_LOGD("Decide downscale iso:%d thres:%d ratio: %d",iIsoValue, t->downscale_thres, iDSRatio);
            if (iDSRatio < 1) {
                MY_LOGE("Has wrong downscale ratio: %d", iDSRatio);
                iDSRatio = 1;
            }
        } else
            MY_LOGE("fail to query nvram!");
#endif
    }
    // 2-1. Downscale
    if (isRunDS) {
        shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
        P2EnqueData& rEnqueData = *pEnqueData.get();
        rEnqueData.mpHolder  = pHolder;
        rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_RAW, INPUT);
        rEnqueData.mIMGI.mpBuf  = pNodeReq->acquireBuffer(rEnqueData.mIMGI.mBufId);
        rEnqueData.mLCEI.mBufId = pNodeReq->mapBufferID(TID_MAN_LCS, INPUT);
        rEnqueData.mpIMetaDynamic = pIMetaDynamic;
        rEnqueData.mpIMetaApp = pIMetaApp;
        rEnqueData.mpIMetaHal = pIMetaHal;
        rEnqueData.mEnableDRE = pRequest->hasFeature(FID_DRE);
        rEnqueData.mEnableMFB = pRequest->hasFeature(FID_MFNR);

        fullSize = rEnqueData.mIMGI.mpBuf->getImgSize();
        downSize = MSize(fullSize.w / iDSRatio, fullSize.h / iDSRatio);
        MY_LOGD("apply down-scale denoise: (%dx%d) -> (%dx%d)",
                fullSize.w, fullSize.h, downSize.w, downSize.h);

        // Get working buffer
        SmartImageBuffer pWorkingBuffer = mpBufferPool->getImageBuffer(downSize.w, downSize.h, eImgFmt_YUY2);
        // Push to resource holder
        pHolder->mpBuffers.push_back(pWorkingBuffer);
        pDownScaleBuffer = pWorkingBuffer->mImageBuffer.get();
        rEnqueData.mWDMAO.mpBuf = pDownScaleBuffer;

        if (!mISP3_0 && mDebugImg3o) {
            SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(fullSize.w, fullSize.h, eImgFmt_YUY2);
            // Push to resource holder
            pHolder->mpBuffers.push_back(pDebugBuffer);
            rEnqueData.mIMG3O.mpBuf = pDebugBuffer->mImageBuffer.get();
        }

        rEnqueData.mSensorId    = mSensorIndex;
        rEnqueData.mUniqueKey   = uniqueKey;
        rEnqueData.mRequestNo   = requestNo;
        rEnqueData.mFrameNo     = frameNo;
        rEnqueData.mTaskId      = 1;
        rEnqueData.mTimeSharing = MTRUE;
        ret = enqueISP(
            pRequest,
            pEnqueData);

        if (!ret) {
            MY_LOGE("main sensor: downsize failed!");
            return MFALSE;
        }
    }

    // 2-2. Upscale or Full-size enque
    {
        shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
        P2EnqueData& rEnqueData = *pEnqueData.get();
        rEnqueData.mpHolder  = pHolder;
        MBOOL isPureRaw = MFALSE;
        MSize srcSize;
        if (isRunDS) {
            rEnqueData.mIMGI.mpBuf = pDownScaleBuffer;
            rEnqueData.mScaleUp = MTRUE;
            rEnqueData.mScaleUpSize = fullSize;
            srcSize = downSize;
        } else {
            if (isYuvRep) {
                rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_YUV, INPUT);
                rEnqueData.mYuvRep = MTRUE;
            } else {
                rEnqueData.mIMGI.mBufId = pNodeReq->mapBufferID(TID_MAN_FULL_RAW, INPUT);
            }
            // Check Raw type
            auto IsPureRaw = [](IImageBuffer *pBuf) -> MBOOL {
                MINT64 rawType;
                if (pBuf->getImgDesc(eIMAGE_DESC_ID_RAW_TYPE, rawType))
                    return rawType == eIMAGE_DESC_RAW_TYPE_PURE;
                return MFALSE;
            };
            rEnqueData.mIMGI.mpBuf     = pNodeReq->acquireBuffer(rEnqueData.mIMGI.mBufId);
            isPureRaw = IsPureRaw(rEnqueData.mIMGI.mpBuf);

            rEnqueData.mIMGI.mPureRaw  =  isPureRaw;
            rEnqueData.mLCEI.mBufId    = pNodeReq->mapBufferID(TID_MAN_LCS, INPUT);

            if (mDebugLoadIn) {
                FileReadRule rule;
                MBOOL bMFNR = pRequest->getParameter(PID_FRAME_COUNT) > 1 ;
                rule.getFile_RAW(requestNo, bMFNR ? "MFNR" : "single_capture", rEnqueData.mIMGI.mpBuf, "P2Node", mSensorIndex);
                rEnqueData.mLCEI.mpBuf = pNodeReq->acquireBuffer(rEnqueData.mLCEI.mBufId);
                rule.getFile_LCSO(requestNo, bMFNR ? "MFNR" : "single_capture", rEnqueData.mLCEI.mpBuf, "P2Node", mSensorIndex);
            }

            srcSize = rEnqueData.mIMGI.mpBuf->getImgSize();
        }

        // the larger size has higher priority, the smaller size could using larger image to copy via MDP
        const TypeID_T typeIds[] = {
                TID_MAN_FULL_YUV,
                TID_JPEG,
                TID_MAN_CROP1_YUV,
                TID_MAN_CROP2_YUV,
                TID_MAN_SPEC_YUV,
                TID_MAN_FD_YUV,
                TID_POSTVIEW,
                TID_THUMBNAIL
        };

        MBOOL hasP2Resizer = !mISP3_0 || !isPureRaw;
        MBOOL hasCopyBuffer = MFALSE;
        auto UsedOutput = [&](P2Output& o) -> MBOOL {
            return o.mBufId != NULL_BUFFER || o.mpBuf != NULL;
        };

        // ISP 3.0: Calculate cropping & scaling for limitation of different view angle
        sp<CropCalculator::Factor> pFactor = mpCropCalculator->getFactor(pIMetaApp, pIMetaHal);
        for (TypeID_T typeId : typeIds) {
            if (mISP3_0) {
                if (typeId == TID_POSTVIEW || typeId == TID_THUMBNAIL)
                    continue;
            }

            BufferID_T bufId = pNodeReq->mapBufferID(typeId, OUTPUT);
            if (bufId == NULL_BUFFER)
                continue;

            IImageBuffer* pBuf = pNodeReq->acquireBuffer(bufId);
            if (pBuf == NULL) {
                MY_LOGE("should not have null buffer. type:%d, buf:%d",typeId, bufId);
                continue;
            }

            MUINT32 trans   = pNodeReq->getImageTransform(bufId);
            MBOOL applyCZ   = bufId == BID_MAN_OUT_JPEG && pRequest->hasFeature(FID_CZ);
            MBOOL needCrop  = typeId == TID_JPEG ||
                              typeId == TID_MAN_CROP1_YUV ||
                              typeId == TID_MAN_CROP2_YUV ||
                              typeId == TID_POSTVIEW ||
                              typeId == TID_THUMBNAIL;

            auto GetScaleCrop = [&](MRect& rCrop, MUINT32& rScaleRatio) mutable {
                if (needCrop) {
                    MSize imgSize = pBuf->getImgSize();
                    if (trans & eTransform_ROT_90)
                        swap(imgSize.w, imgSize.h);

                    mpCropCalculator->evaluate(pFactor, imgSize, rCrop, MFALSE);
                    if (rEnqueData.mScaleUp) {
                        simpleTransform tranTG2DS(MPoint(0,0), rEnqueData.mScaleUpSize, srcSize);
                        rCrop = transform(tranTG2DS, rCrop);
                    }
                    rScaleRatio = imgSize.w * 100 / rCrop.s.w;
                } else {
                    rCrop = MRect(srcSize.w, srcSize.h);
                    rScaleRatio = 1;
                }
            };


            MRect cropRegion;
            MUINT32 scaleRatio;
            GetScaleCrop(cropRegion, scaleRatio);

            auto SetOutput = [&](P2Output& o) {
                o.mpBuf = pBuf;
                o.mBufId = bufId;
                o.mHasCrop = needCrop;
                o.mClearZoom = applyCZ;
                o.mTrans = trans;
                o.mCropRegion = cropRegion;
                o.mScaleRatio = scaleRatio;
            };

            auto BeyondCapability = [&]() -> MBOOL {
                if (!mISP3_0)
                    return MFALSE;

                MUINT32 maxRatio= scaleRatio;
                MUINT32 minRatio = maxRatio;
                MINT32 maxOffset = cropRegion.p.x;
                MINT32 minOffset = maxOffset;

                MY_LOGD("ratio:%d offset:%d", scaleRatio, cropRegion.p.x);
                auto UpdateStatistics = [&](P2Output &o) {
                    if (o.mpBuf == NULL)
                        return;

                    if (o.mCropRegion.p.x > maxOffset)
                        maxOffset = o.mCropRegion.p.x;
                    if (o.mCropRegion.p.x < minOffset)
                        minOffset = o.mCropRegion.p.x;

                    MUINT32 oRatio = o.mScaleRatio;
                    if (oRatio > maxRatio)
                        maxRatio = oRatio;
                    if (oRatio < minRatio)
                        minRatio = oRatio;
                };

                UpdateStatistics(rEnqueData.mWDMAO);
                UpdateStatistics(rEnqueData.mWROTO);
                UpdateStatistics(rEnqueData.mIMG2O);

                // It's not acceptible if the offset difference is over 196
                if (maxOffset - minOffset > ISP30_RULE01_CROP_OFFSET)
                    return MTRUE;

                // To use HW resizer has the following limitation:
                // - Don't allow that downscale ratio is over 8 and offset is over 128.
                MBOOL hitRatio = maxRatio > minRatio * ISP30_RULE02_RESIZE_RATIO;
                MBOOL hitOffset = maxOffset - minOffset > ISP30_RULE02_CROP_OFFSET;
                if (hitRatio && hitOffset)
                    return MTRUE;

                // - The offset difference multiplied by the ratio differnce must be smaller than 1024
                if (hitRatio || hitOffset) {
                    if ((maxOffset - minOffset) * maxRatio >
                        minRatio * ISP30_RULE02_CROP_OFFSET * ISP30_RULE02_RESIZE_RATIO) {
                        return MTRUE;
                    }
                }

                return MFALSE;
            };

            MBOOL limited  = BeyondCapability();
            // Use P2 resizer to serve FD or thumbnail buffer,
            // but do NOT use IMG2O to crop or resize in ISP 3.0 while enque pure raw
            if (!limited && !UsedOutput(rEnqueData.mIMG2O) && (typeId == TID_MAN_FD_YUV || typeId == TID_THUMBNAIL) && hasP2Resizer)
            {
                SetOutput(rEnqueData.mIMG2O);
            } else if (!limited && !UsedOutput(rEnqueData.mWDMAO) && trans == 0) {
                SetOutput(rEnqueData.mWDMAO);
            } else if (!limited && !UsedOutput(rEnqueData.mWROTO)) {
                SetOutput(rEnqueData.mWROTO);
            } else if (!UsedOutput(rEnqueData.mCopy1)) {
                SetOutput(rEnqueData.mCopy1);
                hasCopyBuffer = MTRUE;
            } else if (!UsedOutput(rEnqueData.mCopy2)) {
                SetOutput(rEnqueData.mCopy2);
                hasCopyBuffer = MTRUE;
            } else {
                MY_LOGE("the buffer is not served, type:%s", TypeID2Name(typeId));
            }

            // [Specialize] MFNR's requirement
            if (pRequest->hasFeature(FID_MFNR)) {
                if (typeId == TID_MAN_SPEC_YUV) {
                    auto& dstSize = pBuf->getImgSize();
                    MSize newSize = MSize(srcSize.w / 2, srcSize.h / 2);
                    MY_LOGD("spec yuv: original size(%dx%d) new size(%dx%d), stride(%zu)",
                            dstSize.h, dstSize.h,
                            newSize.w, newSize.h,
                            pBuf->getBufStridesInBytes(0));
                    pBuf->setExtParam(newSize);
                } else if (typeId == TID_MAN_FULL_YUV) {
                    auto& dstSize = pBuf->getImgSize();
                    MSize newSize = srcSize;
                    MY_LOGD("full yuv: original size(%dx%d) new size(%dx%d), stride(%zu)",
                            dstSize.w, dstSize.h,
                            newSize.w, newSize.h,
                            pBuf->getBufStridesInBytes(0));
                    pBuf->setExtParam(newSize);
                }
            }
        }

        // Create a smaller intermediate buffer for memory copying
        if (mISP3_0 && hasCopyBuffer) {
            if (!UsedOutput(rEnqueData.mWDMAO) || !UsedOutput(rEnqueData.mWROTO)) {
                P2Output& rUnusedOutput = UsedOutput(rEnqueData.mWDMAO) ? rEnqueData.mWROTO : rEnqueData.mWDMAO;

                MSize tempSize = MSize(srcSize.w / ISP30_RULE02_RESIZE_RATIO,
                                       srcSize.h / ISP30_RULE02_RESIZE_RATIO);
                tempSize.w &= ~(0x1);
                tempSize.h &= ~(0x1);

                MY_LOGW("Create an intermediate buffer(%d,%d), resized ratio:%d",
                    tempSize.w, tempSize.h, ISP30_RULE02_RESIZE_RATIO);

                // Should hit the hw limitation here. Create a temp buffer to avoid full size processing
                SmartImageBuffer pTempBuffer = mpBufferPool->getImageBuffer(
                                    tempSize.w, tempSize.h, eImgFmt_YUY2);

                pHolder->mpBuffers.push_back(pTempBuffer);
                rUnusedOutput.mpBuf = pTempBuffer->mImageBuffer.get();
                rUnusedOutput.mBufId = 200; // Magic No for Temp Buffer
                rUnusedOutput.mHasCrop = MTRUE;
            }
        }

        if ((rEnqueData.mIMG2O.mBufId & rEnqueData.mWROTO.mBufId & rEnqueData.mWDMAO.mBufId) != NULL_BUFFER) {
            rEnqueData.mEnableMFB = !rEnqueData.mScaleUp && pRequest->hasFeature(FID_MFNR);
            rEnqueData.mEnableDRE = !rEnqueData.mScaleUp && pRequest->hasFeature(FID_DRE);
            rEnqueData.mpIMetaDynamic = pIMetaDynamic;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal;
            rEnqueData.mpOMetaApp = pOMetaApp;
            rEnqueData.mpOMetaHal = pOMetaHal;
            rEnqueData.mSensorId  = mSensorIndex;
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = 2;
            rEnqueData.mTimeSharing = MTRUE;

            // Deubg: Dump IMG3O only for ISP 4.0+
            if (!mISP3_0 && mDebugImg3o) {
                SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(srcSize.w, srcSize.h, eImgFmt_YUY2);
                pHolder->mpBuffers.push_back(pDebugBuffer);
                rEnqueData.mIMG3O.mpBuf = pDebugBuffer->mImageBuffer.get();
            }

            // Deubg: Dump IMG2O only for ISP 3.0
            if (mISP3_0 && mDebugImg2o) {
                SmartImageBuffer pDebugBuffer = mpBufferPool->getImageBuffer(srcSize.w, srcSize.h, eImgFmt_YUY2);
                pHolder->mpBuffers.push_back(pDebugBuffer);
                rEnqueData.mIMG2O.mpBuf = pDebugBuffer->mImageBuffer.get();
            }

            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("enqueISP failed!");
                return MFALSE;
            }
        }
    }

    // 3. Full RAW of sub sensor
    if (hasSubSensor()) {
        BufferID_T uOBufId = pNodeReq->mapBufferID(TID_SUB_FULL_YUV, OUTPUT);

        if (uOBufId != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder  = pHolder;
            rEnqueData.mIMGI.mBufId  = pNodeReq->mapBufferID(TID_SUB_FULL_RAW, INPUT);
            rEnqueData.mLCEI.mBufId  = pNodeReq->mapBufferID(TID_SUB_LCS, INPUT);
            rEnqueData.mWDMAO.mBufId = uOBufId;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal2;
            rEnqueData.mSensorId  = mSensorIndex2;
            rEnqueData.mEnableVSDoF = getHasVSDoFFeature(pRequest);
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = 3;
            rEnqueData.mTimeSharing = MTRUE;
            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("sub sensor: full yuv failed!");
                return MFALSE;
            }
        }
    }

    // 4. Resized RAW of sub sensor
    if (hasSubSensor()) {
        BufferID_T uOBufId = pNodeReq->mapBufferID(TID_SUB_RSZ_YUV, OUTPUT);

        if (uOBufId != NULL_BUFFER) {
            shared_ptr<P2EnqueData> pEnqueData = make_shared<P2EnqueData>();
            P2EnqueData& rEnqueData = *pEnqueData.get();

            rEnqueData.mpHolder  = pHolder;
            rEnqueData.mIMGI.mBufId  = pNodeReq->mapBufferID(TID_SUB_RSZ_RAW, INPUT);
            rEnqueData.mWDMAO.mBufId = uOBufId;
            rEnqueData.mpIMetaApp = pIMetaApp;
            rEnqueData.mpIMetaHal = pIMetaHal2;
            rEnqueData.mSensorId  = mSensorIndex2;
            rEnqueData.mResized   = MTRUE;
            rEnqueData.mEnableVSDoF = getHasVSDoFFeature(pRequest);
            rEnqueData.mUniqueKey = uniqueKey;
            rEnqueData.mRequestNo = requestNo;
            rEnqueData.mFrameNo   = frameNo;
            rEnqueData.mTaskId    = 4;
            ret = enqueISP(
                pRequest,
                pEnqueData);

            if (!ret) {
                MY_LOGE("sub sensor: resized yuv failed!");
                return MFALSE;
            }
        }
    }

    MY_LOGI("-, R/F Num: %d/%d", requestNo, frameNo);
    CAM_TRACE_FMT_END();
    return MTRUE;
}


/*******************************************************************************
 *
 ********************************************************************************/
MBOOL P2ANode::copyBuffers(EnquePackage* pPackage)
{
    P2EnqueData& rData = *pPackage->mpEnqueData.get();
    MINT32 requestNo = rData.mRequestNo;
    MINT32 frameNo = rData.mFrameNo;
    CAM_TRACE_FMT_BEGIN("p2a:copy|r%df%d", requestNo, frameNo);
    MY_LOGD("+, R/F Num: %d/%d", requestNo, frameNo);


    IImageBuffer* pSource1  = rData.mCopy1.mpSource;
    IImageBuffer* pSource2  = rData.mCopy2.mpSource;
    IImageBuffer* pCopy1    = rData.mCopy1.mpBuf;
    IImageBuffer* pCopy2    = rData.mCopy2.mpBuf;
    MUINT32 uTrans1         = rData.mCopy1.mSourceTrans;
    MUINT32 uTrans2         = rData.mCopy2.mSourceTrans;
    MRect& rCrop1           = rData.mCopy1.mSourceCrop;
    MRect& rCrop2           = rData.mCopy2.mSourceCrop;
    MBOOL hasCopy2          = pCopy2 != NULL;
    MBOOL hasSameSrc        = hasCopy2 ? pSource1 == pSource2 : MFALSE;

    if (pSource1 == NULL || pCopy1 == NULL) {
        MY_LOGE("should have source1 & copy1 buffer here. src:%p, dst:%p", pSource1, pCopy1);
        return MFALSE;
    }

    if (hasCopy2 && pSource2 == NULL) {
        MY_LOGE("should have source2 buffer here. src:%p", pSource1);
        return MFALSE;
    }

    String8 strCopyLog;

    auto InputLog = [&](const char* sPort, IImageBuffer* pBuf) mutable {
        strCopyLog += String8::format("Sensor(%d) Resized(%d) R/F Num: %d/%d, Copy: %s Fmt(0x%x) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            rData.mSensorId,
            rData.mResized,
            requestNo,
            frameNo,
            sPort,
            pBuf->getImgFormat(),
            pBuf->getImgSize().w, pBuf->getImgSize().h,
            pBuf->getBufVA(0),
            pBuf->getBufPA(0));
    };

    auto OutputLog = [&](const char* sPort, MDPOutput& rOut) mutable {
        strCopyLog += String8::format(", %s Rot(%d) Crop(%d,%d)(%dx%d) Size(%dx%d) VA/PA(%#" PRIxPTR "/%#" PRIxPTR ")",
            sPort, rOut.mSourceTrans,
            rOut.mSourceCrop.p.x, rOut.mSourceCrop.p.y,
            rOut.mSourceCrop.s.w, rOut.mSourceCrop.s.h,
            rOut.mpBuf->getImgSize().w, rOut.mpBuf->getImgSize().h,
            rOut.mpBuf->getBufVA(0), rOut.mpBuf->getBufPA(0));
    };

    InputLog("SRC1", pSource1);
    OutputLog("COPY1", rData.mCopy1);

    if (hasCopy2) {
        if (!hasSameSrc) {
            MY_LOGD("%s", strCopyLog.string());
            strCopyLog.clear();
            InputLog("SRC2", pSource2);
        }
        OutputLog("COPY2", rData.mCopy2);
    }
    MY_LOGD("%s", strCopyLog.string());

    // use IImageTransform to handle image cropping
    std::unique_ptr<IImageTransform, std::function<void(IImageTransform*)>> transform(
            IImageTransform::createInstance(),
            [](IImageTransform *p)
            {
                if (p)
                    p->destroyInstance();
            }
        );

    if (transform.get() == NULL) {
        MY_LOGE("IImageTransform is NULL, cannot generate output");
        return MFALSE;
    }

    MBOOL ret = MTRUE;
    ret = transform->execute(
            pSource1,
            pCopy1,
            (hasSameSrc) ? pCopy2 : 0,
            rCrop1,
            (hasSameSrc) ? rCrop2 : 0,
            uTrans1,
            (hasSameSrc) ? uTrans2 : 0,
            3000
        );

    if (!ret) {
        MY_LOGE("fail to do image transform: first run");
        return MFALSE;
    }

    if (hasCopy2 && !hasSameSrc) {
        ret = transform->execute(
                pSource2,
                pCopy2,
                0,
                rCrop2,
                0,
                uTrans2,
                0,
                3000
            );

        if (!ret) {
            MY_LOGE("fail to do image transform: second run");
            return MFALSE;
        }
    }

    CAM_TRACE_FMT_END();
    MY_LOGD("-, R/F Num: %d/%d", requestNo, frameNo);
    return MTRUE;
}

MVOID P2ANode::onRequestFinish(RequestPtr& pRequest)
{
    MINT32 requestNo = pRequest->getRequestNo();
    MINT32 frameNo = pRequest->getFrameNo();
    CAM_TRACE_FMT_BEGIN("p2a::finish|r%df%d)", requestNo, frameNo);
    MY_LOGI("R/F Num: %d/%d", requestNo, frameNo);

    pRequest->decNodeReference(NID_P2A);

    if (pRequest->getParameter(PID_ENABLE_NEXT_CAPTURE) > 0)
    {
        if (pRequest->hasParameter(PID_THUMBNAIL_TIMING) &&
            pRequest->getParameter(PID_THUMBNAIL_TIMING) == NSPipelinePlugin::eTiming_P2)
        {
            MINT32 frameCount = pRequest->getParameter(PID_FRAME_COUNT);
            MINT32 frameIndex = pRequest->getParameter(PID_FRAME_INDEX);

            if ((frameCount < 2 && frameIndex < 1) || frameCount == frameIndex + 1)
            {
                if (pRequest->mpCallback != NULL) {
                    MY_LOGI("Nofity next capture at P2 ending(%d/%d)", frameIndex, frameCount);
                    pRequest->mpCallback->onContinue(pRequest);
                } else {
                    MY_LOGW("have no request callback instance!");
                }
            }
        }
    }

    pRequest->mTimer.stopP2A();
    dispatch(pRequest);

    CAM_TRACE_FMT_END();
}

P2ANode::EnquePackage::~EnquePackage()
{
    if (mpPQParam != nullptr) {
        if (mpPQParam->WDMAPQParam != nullptr)
            delete static_cast<DpPqParam*>(mpPQParam->WDMAPQParam);
        if (mpPQParam->WROTPQParam != nullptr)
            delete static_cast<DpPqParam*>(mpPQParam->WROTPQParam);
        delete mpPQParam;
    }

    if (mpModuleInfo != nullptr) {
        if (mpModuleInfo->moduleStruct != nullptr)
            delete static_cast<_SRZ_SIZE_INFO_*>(mpModuleInfo->moduleStruct);
        delete mpModuleInfo;
    }
}

MERROR P2ANode::evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInfer)
{
    auto& rSrcData = rInfer.getSharedSrcData();
    auto& rDstData = rInfer.getSharedDstData();
    auto& rFeatures = rInfer.getSharedFeatures();
    auto& rMetadatas = rInfer.getSharedMetadatas();

    // Debug
    if (mDebugDRE == OFF)
        rInfer.clearFeature(FID_DRE);
    else if (mDebugDRE == ON)
        rInfer.markFeature(FID_DRE);

    if (mDebugCZ == OFF)
        rInfer.clearFeature(FID_CZ);
    else if (mDebugCZ == ON)
        rInfer.markFeature(FID_CZ);

    MBOOL hasMain = MFALSE;
    MBOOL hasSub = MFALSE;

    // Reprocessing
    if (rInfer.hasType(TID_MAN_FULL_YUV))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_FULL_YUV;
        src_0.mSizeId = SID_FULL;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_FULL_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mSize = rInfer.getSize(TID_MAN_FULL_YUV);
        dst_0.mFormat = eImgFmt_YV12;

        auto& dst_1 = rDstData.editItemAt(rDstData.add());
        dst_1.mTypeId = TID_MAN_CROP1_YUV;

        auto& dst_2 = rDstData.editItemAt(rDstData.add());
        dst_2.mTypeId = TID_MAN_CROP2_YUV;

        auto& dst_3 = rDstData.editItemAt(rDstData.add());
        dst_3.mTypeId = TID_JPEG;

        auto& dst_4 = rDstData.editItemAt(rDstData.add());
        dst_4.mTypeId = TID_THUMBNAIL;

        auto& dst_5 = rDstData.editItemAt(rDstData.add());
        dst_5.mTypeId = TID_MAN_FD_YUV;

        auto& dst_6 = rDstData.editItemAt(rDstData.add());
        dst_6.mTypeId = TID_POSTVIEW;

        hasMain = MTRUE;
    }
    else if (rInfer.hasType(TID_MAN_FULL_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_FULL_RAW;
        src_0.mSizeId = SID_FULL;

        auto& src_1 = rSrcData.editItemAt(rSrcData.add());
        src_1.mTypeId = TID_MAN_LCS;
        src_1.mSizeId = SID_ARBITRARY;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_FULL_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mSize = rInfer.getSize(TID_MAN_FULL_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        // should only output working buffer if run DRE
        if (!(mSupportDRE && rInfer.hasFeature(FID_DRE))) {
            auto& dst_1 = rDstData.editItemAt(rDstData.add());
            dst_1.mTypeId = TID_MAN_CROP1_YUV;

            auto& dst_2 = rDstData.editItemAt(rDstData.add());
            dst_2.mTypeId = TID_MAN_CROP2_YUV;
        }

        auto& dst_3 = rDstData.editItemAt(rDstData.add());
        dst_3.mTypeId = TID_MAN_SPEC_YUV;

        auto& dst_4 = rDstData.editItemAt(rDstData.add());
        dst_4.mTypeId = TID_JPEG;

        auto& dst_5 = rDstData.editItemAt(rDstData.add());
        dst_5.mTypeId = TID_THUMBNAIL;

        auto& dst_6 = rDstData.editItemAt(rDstData.add());
        dst_6.mTypeId = TID_MAN_FD_YUV;

        auto& dst_7 = rDstData.editItemAt(rDstData.add());
        dst_7.mTypeId = TID_POSTVIEW;

        hasMain = MTRUE;
    }

    if (rInfer.hasType(TID_SUB_FULL_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_SUB_FULL_RAW;
        src_0.mSizeId = SID_FULL;

        auto& src_1 = rSrcData.editItemAt(rSrcData.add());
        src_1.mTypeId = TID_SUB_LCS;
        src_1.mSizeId = SID_ARBITRARY;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_SUB_FULL_YUV;
        dst_0.mSizeId = SID_FULL;
        dst_0.mSize = rInfer.getSize(TID_SUB_FULL_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        hasSub = MTRUE;
    }

    if (rInfer.hasType(TID_MAN_RSZ_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_MAN_RSZ_RAW;
        src_0.mSizeId = SID_RESIZED;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_MAN_RSZ_YUV;
        dst_0.mSizeId = SID_RESIZED;
        dst_0.mSize = rInfer.getSize(TID_MAN_RSZ_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        hasMain = MTRUE;
    }

    if (rInfer.hasType(TID_SUB_RSZ_RAW))
    {
        auto& src_0 = rSrcData.editItemAt(rSrcData.add());
        src_0.mTypeId = TID_SUB_RSZ_RAW;
        src_0.mSizeId = SID_RESIZED;

        auto& dst_0 = rDstData.editItemAt(rDstData.add());
        dst_0.mTypeId = TID_SUB_RSZ_YUV;
        dst_0.mSizeId = SID_RESIZED;
        dst_0.mSize = rInfer.getSize(TID_SUB_RSZ_RAW);
        dst_0.mFormat = eImgFmt_YV12;

        hasSub = MTRUE;
    }

    if (hasMain) {
        rMetadatas.push_back(MID_MAN_IN_P1_DYNAMIC);
        rMetadatas.push_back(MID_MAN_IN_APP);
        rMetadatas.push_back(MID_MAN_IN_HAL);
        rMetadatas.push_back(MID_MAN_OUT_APP);
        rMetadatas.push_back(MID_MAN_OUT_HAL);
    }

    if (hasSub) {
        rMetadatas.push_back(MID_SUB_IN_P1_DYNAMIC);
        rMetadatas.push_back(MID_SUB_IN_HAL);
    }

    rInfer.addNodeIO(nodeId, rSrcData, rDstData, rMetadatas, rFeatures);

    return OK;
}

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
