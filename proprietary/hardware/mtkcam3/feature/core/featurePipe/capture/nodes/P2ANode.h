/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_CAPTURE_FEATURE_PIPE_P2A_NODE_H_
#define _MTK_CAMERA_CAPTURE_FEATURE_PIPE_P2A_NODE_H_

#include "CaptureFeatureNode.h"

#include <featurePipe/vsdof/util/P2Operator.h>
//#include <mtkcam/drv/iopipe/PostProc/IHalPostProcPipe.h>
#include <featurePipe/core/include/CamThreadNode.h>

#include <mtkcam/aaa/IHalISP.h>
#include <thread>
#include <future>

using namespace NS3Av3;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

class CaptureTaskQueue;

class P2ANode : public CaptureFeatureNode {
public:
    P2ANode(NodeID_T nid, const char *name, MINT32 policy = SCHED_NORMAL, MINT32 priority = DEFAULT_CAMTHREAD_PRIORITY);
    virtual ~P2ANode();
    virtual MVOID setSensorIndex(MINT32 sensorIndex, MINT32 sensorIndex2 = -1);
    MVOID setBufferPool(const android::sp<CaptureBufferPool> &pool);

public:
    virtual MBOOL onData(DataID id, RequestPtr &data);

protected:
    virtual MBOOL onInit();
    virtual MBOOL onUninit();
    virtual MBOOL onThreadStart();
    virtual MBOOL onThreadStop();
    virtual MBOOL onThreadLoop();
    virtual MERROR evaluate(NodeID_T nodeId, CaptureFeatureInferenceData& rInference);


public:

    // P2 callbacks
    static MVOID onP2SuccessCallback(QParams& rParams);
    static MVOID onP2FailedCallback(QParams& rParams);

    struct RequestHolder
    {
        RequestHolder(RequestPtr pRequest)
            : mpRequest(pRequest)
            , mStatus(OK)
        {};

        ~RequestHolder() {};

        Vector<SmartImageBuffer>        mpBuffers;
        RequestPtr                      mpRequest;
        std::shared_ptr<RequestHolder>  mpPrecedeOver;
        MERROR                          mStatus;
    };

    struct P2Input
    {
        IImageBuffer* mpBuf         = NULL;
        BufferID_T    mBufId        = NULL_BUFFER;
        MBOOL         mPureRaw      = MFALSE;
    };

    struct P2Output
    {
        IImageBuffer* mpBuf         = NULL;
        BufferID_T    mBufId        = NULL_BUFFER;
        MBOOL         mHasCrop      = MFALSE;
        MRect         mCropRegion   = MRect(0,0);
        MBOOL         mClearZoom    = MFALSE;
        MUINT32       mTrans        = 0;
        MUINT32       mScaleRatio   = 0;
        MBOOL         mEarlyRelease = MFALSE;
    };

    struct MDPOutput : P2Output
    {
        MDPOutput() : P2Output() { }

        IImageBuffer* mpSource      = NULL;
        MRect         mSourceCrop   = MRect(0,0);
        MUINT32       mSourceTrans  = 0;
    };

    struct P2EnqueData
    {
        P2Input     mIMGI           = P2Input();
        P2Input     mLCEI           = P2Input();
        P2Output    mIMG2O          = P2Output();
        P2Output    mWDMAO          = P2Output();
        P2Output    mWROTO          = P2Output();
        P2Output    mIMG3O          = P2Output();
        // Using MDP Copy
        MDPOutput   mCopy1          = MDPOutput();
        MDPOutput   mCopy2          = MDPOutput();

        IMetadata*  mpIMetaApp      = NULL;
        IMetadata*  mpIMetaHal      = NULL;
        IMetadata*  mpIMetaDynamic  = NULL;
        IMetadata*  mpOMetaHal      = NULL;
        IMetadata*  mpOMetaApp      = NULL;

        MINT32      mSensorId       = 0;
        MBOOL       mResized        = MFALSE;
        MBOOL       mYuvRep         = MFALSE;
        MBOOL       mScaleUp        = MFALSE;
        MSize       mScaleUpSize    = MSize(0,0);
        MBOOL       mEnableMFB      = MFALSE;
        MBOOL       mEnableDRE      = MFALSE;
        MBOOL       mEnableVSDoF    = MFALSE;
        MBOOL       mDebugDump      = MFALSE;
        MBOOL       mDebugUpkRaw    = MFALSE;
        MBOOL       mTimeSharing    = MFALSE;
        MINT32      mUniqueKey      = 0;
        MINT32      mRequestNo      = 0;
        MINT32      mFrameNo        = 0;
        MINT32      mTaskId         = 0;
        std::shared_ptr<RequestHolder>
                    mpHolder;
    };



    MBOOL enqueISP(
        RequestPtr& request,
        std::shared_ptr<P2EnqueData>& pEnqueData
    );

    // image processes
    MBOOL onRequestProcess(RequestPtr&);


    // routines
    MVOID onRequestFinish(RequestPtr&);


private:
    inline MBOOL hasSubSensor() {
        return mSensorIndex2 >= 0;
    }

    WaitQueue<RequestPtr>                   mRequests;

    IHalISP*                                mpIspHal;
    IHalISP*                                mpIspHal2;
    sp<P2Operator>                          mpP2Opt;
    sp<P2Operator>                          mpP2Opt2;
    MUINT                                   mSensorFmt;
    MUINT                                   mSensorFmt2;

    android::sp<CaptureBufferPool>          mpBufferPool;
    std::map<EDIPInfoEnum, MUINT32>         mDipInfo;
    MUINT32                                 mDipVer;
    MBOOL                                   mSupportDRE;
    MBOOL                                   mSupportCZ;
    MBOOL                                   mISP3_0;
    MINT32                                  mDebugDS;
    MINT32                                  mDebugDSRatio;
    MINT32                                  mDebugCZ;
    MINT32                                  mDebugDRE;
    MBOOL                                   mDebugLoadIn;
    MBOOL                                   mDebugDump;
    MBOOL                                   mDebugImg2o;
    MBOOL                                   mDebugImg3o;
    MBOOL                                   mDebugUpkRaw;
    CaptureTaskQueue*                       mTaskQueue;

    std::weak_ptr<RequestHolder>            mpLastHolder;

private:

    struct EnquePackage :public Timer
    {
    public:
        std::shared_ptr<P2EnqueData>        mpEnqueData;
        PQParam*                            mpPQParam;
        ModuleInfo*                         mpModuleInfo;
        SmartTuningBuffer                   mTuningData;
        SmartImageBuffer                    mUpkWorkingBuf;
        P2ANode*                            mpNode;

        EnquePackage()
            : mpPQParam(NULL)
            , mpModuleInfo(NULL)
            , mTuningData(NULL)
            , mpNode(NULL)
        {
        }

        ~EnquePackage();

    };

public:
    static MBOOL copyBuffers(EnquePackage*);
    static MVOID unpack(IImageBuffer* pPkInBuf, IImageBuffer* pUpkOutBuf );

};

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_CAPTURE_FEATURE_PIPE_P2A_NODE_H_
