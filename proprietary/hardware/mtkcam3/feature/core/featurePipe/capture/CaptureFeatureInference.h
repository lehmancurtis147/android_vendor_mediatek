/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_INFERENCE_H_
#define _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_INFERENCE_H_

#include <utils/RefBase.h>
#include <utils/Vector.h>
#include <utils/BitSet.h>

#include <mtkcam3/3rdparty/plugin/PipelinePlugin.h>
#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>

#include "CaptureFeatureRequest.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

class CaptureFeatureNode;

enum eCaptureFeatureInferenceStrategy {
    STG_LONGEST_PATH,
    STG_PREDEFINED
};

typedef MUINT Format_T;

struct DataItem
{
    DataItem()
        : mNodeId(0)
        , mTypeId(NULL_TYPE)
        , mFormat(0)
        , mSizeId(NULL_SIZE)
        , mSize(0, 0)
        , mRedirect(-1)
        , mBufferId(NULL_BUFFER)
    {
    }
    inline MVOID markReference(NodeID_T nodeId) {
        mReferences.markBit(nodeId);
    }

    inline MVOID markFeature(FeatureID_T featureId) {
        mFeatures.markBit(featureId);
    }

    NodeID_T    mNodeId;        // buffer owner
    TypeID_T    mTypeId;
    BitSet32    mReferences;    // node referred
    BitSet64    mFeatures;      // Feature applied
    Format_T    mFormat;        // Image Format
    SizeID_T    mSizeId;        // Image Size
    MSize       mSize;          // Specified Size
    MINT        mRedirect;
    BufferID_T  mBufferId;
};


/*
 * 1. Use source data to infer the maximum of data flow
 * 2. Map the targets into the flow
 * 3. Remove the data without any reference.
 *
 */
class CaptureFeatureInferenceData
{
public:

    struct SrcData
    {
        SrcData()
            : mTypeId(0)
            , mFormat(0)
            , mSizeId(0)
            , mSize(MSize(0, 0))
        {
        }

        TypeID_T    mTypeId;
        Format_T    mFormat;
        SizeID_T    mSizeId;
        MSize       mSize;
    };

    struct DstData
    {
        DstData()
            : mTypeId(0)
            , mFormat(0)
            , mSizeId(0)
            , mSize(MSize(0, 0))
            , mInPlace(MFALSE)
        {
        }

        TypeID_T    mTypeId;
        Format_T    mFormat;
        SizeID_T    mSizeId;
        MSize       mSize;
        MBOOL       mInPlace;
    };

    CaptureFeatureInferenceData();

    /*
     * Step 1:
     */
    MVOID addSource(TypeID_T tid, BufferID_T bid, Format_T fmt, MSize size);
    /*
     * Step 2:
     * a node could have multiple IO, but the IO should have no duplicated type
     *
     */
    MVOID addNodeIO(NodeID_T nid,
                    Vector<SrcData>& vSrcData,
                    Vector<DstData>& vDstData,
                    Vector<MetadataID_T>& vMetadata,
                    Vector<FeatureID_T>& vFeature,
                    MBOOL forced = MFALSE);

    /*
     * Step 3:
     *
     */
    MVOID addTarget(TypeID_T tid, BufferID_T bid);

    /*
     * Step 4:
     *
     */
    MVOID determine(sp<CaptureFeatureRequest> pRequest);

    MVOID dump();

    inline MBOOL hasType(TypeID_T tid) {
        return mInferredType.hasBit(tid);
    }

    inline MBOOL hasFeature(FeatureID_T fid) {
        return mFeatures.hasBit(fid);
    }

    inline MVOID markFeature(FeatureID_T fid) {
        mFeatures.markBit(fid);
    }

    inline MVOID clearFeature(FeatureID_T fid) {
        mFeatures.clearBit(fid);
    }

    inline MVOID markFaceData(NSPipelinePlugin::FaceData fdData) {
        mFaceDateType.markBit(fdData);
    }

    inline MVOID appendBootType(MUINT8 boostType) {
        mBoostType |= boostType;
    }

    inline MVOID markThumbnailTiming(MUINT32 timing) {
        mThumbnailTiming.markBit(timing);
    }

    MSize getSize(TypeID_T typeId);

    inline Vector<SrcData>& getSharedSrcData() {
        mTempSrcData.clear();
        return mTempSrcData;
    }

    inline Vector<DstData>& getSharedDstData() {
        mTempDstData.clear();
        return mTempDstData;
    }

    inline Vector<FeatureID_T>& getSharedFeatures() {
        mTempFeatures.clear();
        return mTempFeatures;
    }

    inline Vector<MetadataID_T>& getSharedMetadatas() {
        mTempMetadatas.clear();
        return mTempMetadatas;
    }
    inline MUINT8 getRequestCount() {
        return mRequestCount;
    }

    inline MUINT8 getRequestIndex() {
        return mRequestIndex;
    }


private:
    MUINT8 addDataItem(NodeID_T nid, TypeID_T tid,
            BufferID_T bid = NULL_BUFFER,
            BitSet64 features = BitSet64());


public:

    // for reuse
    Vector<SrcData>         mTempSrcData;
    Vector<DstData>         mTempDstData;
    Vector<FeatureID_T>     mTempFeatures;
    Vector<MetadataID_T>    mTempMetadatas;


    shared_ptr<IMetadata>   mpIMetadataHal;
    shared_ptr<IMetadata>   mpIMetadataApp;
    shared_ptr<IMetadata>   mpIMetadataDynamic;

    DataItem                mDataItems[32];
    MUINT8                  mDataCount;

    MUINT8                  mRequestIndex;
    MUINT8                  mRequestCount;
    android::BitSet32       mInferredType;
    // map type id into data item
    MUINT8                  mInferredItems[NUM_OF_TYPE];

    android::BitSet64       mFeatures;

    android::BitSet32       mFaceDateType;
    android::BitSet32       mThumbnailTiming;
    MUINT8                  mBoostType;
    // the value is index of data item
    MINT8                   mNodeInput[NUM_OF_NODE][NUM_OF_TYPE];
    MINT8                   mNodeOutput[NUM_OF_NODE][NUM_OF_TYPE];
    Vector<MetadataID_T>    mNodeMeta[NUM_OF_NODE];

    android::BitSet32       mPathUsed;
    android::BitSet32       mNodeUsed;
    MUINT8                  mPipeBufferCounter;

    // feature related
    MBOOL                   mRequiredFD;
    MBOOL                   mPerFrameFD;
};


class CaptureFeatureInference
{

public:
    CaptureFeatureInference();

    virtual MVOID addNode(NodeID_T nid, CaptureFeatureNode* pNode);

    virtual MERROR evaluate(sp<CaptureFeatureRequest> pRequest);

    virtual ~CaptureFeatureInference() {};

private:
    KeyedVector<NodeID_T, CaptureFeatureNode*> mNodeMap;

};
} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_INFERENCE_H_
