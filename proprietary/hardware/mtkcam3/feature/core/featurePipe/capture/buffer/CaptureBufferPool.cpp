/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#include "CaptureBufferPool.h"

#define PIPE_CLASS_TAG "Pool"
#define PIPE_TRACE TRACE_CAPTURE_FEATURE_NODE

#include <featurePipe/core/include/PipeLog.h>

using namespace NSCam::NSCamFeature::NSFeaturePipe;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

TuningBufferHandle::TuningBufferHandle(const sp<BufferPool<TuningBufferHandle> > &pool)
    : BufferHandle(pool)
    , mpVA(0)
{
}

sp<TuningBufferPool> TuningBufferPool::create(const char *name, MUINT32 size)
{
    TRACE_FUNC_ENTER();
    sp<TuningBufferPool> pool = new TuningBufferPool(name);
    if (pool == NULL) {
        MY_LOGE("OOM: Cannot create TuningBufferPool!");
    } else if (!pool->init(size)) {
        MY_LOGE("Pool initialization failed!");
        pool = NULL;
    }

    TRACE_FUNC_EXIT();
    return pool;
}

MVOID TuningBufferPool::destroy(android::sp<TuningBufferPool> &pool)
{
    TRACE_FUNC_ENTER();
    if (pool != NULL) {
        pool->releaseAll();
        pool = NULL;
    }
    TRACE_FUNC_EXIT();
}

TuningBufferPool::TuningBufferPool(const char* name)
    : BufferPool<TuningBufferHandle>(name)
    , muBufSize(0)
{
}

TuningBufferPool::~TuningBufferPool()
{
    uninit();
}

MBOOL TuningBufferPool::init(MUINT32 size)
{
    TRACE_FUNC_ENTER();

    android::Mutex::Autolock lock(mMutex);

    if (size <= 0)
        return MFALSE;

    muBufSize = size;

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID TuningBufferPool::uninit()
{
    TRACE_FUNC_ENTER();
    android::Mutex::Autolock lock(mMutex);
    this->releaseAll();
    TRACE_FUNC_EXIT();
}

sp<TuningBufferHandle> TuningBufferPool::doAllocate()
{
    TRACE_FUNC_ENTER();

    android::Mutex::Autolock lock(mMutex);
    sp<TuningBufferHandle> handle = new TuningBufferHandle(this);

    if (handle == NULL) {
        MY_LOGE("TuningBufferHandle create failed!");
        return NULL;
    }

    handle->mpVA = (MVOID*) malloc(muBufSize);

    if (handle->mpVA == NULL) {
        MY_LOGE("Out of memory!!");
        return NULL;
    }
    // clear memory
    memset((void*)handle->mpVA, 0, muBufSize);

    TRACE_FUNC_EXIT();

    return handle;
}

MBOOL TuningBufferPool::doRelease(TuningBufferHandle* handle)
{
    TRACE_FUNC_ENTER();

    if (handle->mpVA != NULL) {
        free(handle->mpVA);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

CaptureBufferPool::CaptureBufferPool(const char *name)
    : mName(name)
    , mbInit(MFALSE)
    , muTuningBufSize(0)

{
}

CaptureBufferPool::~CaptureBufferPool()
{
    uninit();
}

MBOOL CaptureBufferPool::init(Vector<BufferConfig> vBufConfig)
{
    Mutex::Autolock _l(mPoolLock);

    if (mbInit) {
        MY_LOGE("do init when it's already init!");
        return MFALSE;
    }

    mvImagePools.clear();

    for (auto& bufConf : vBufConfig) {

        MY_LOGD("[%s] s:%dx%d f:%d min:%d max:%d",
            bufConf.name,
            bufConf.width, bufConf.height, bufConf.format,
            bufConf.minCount,
            bufConf.maxCount
        );

        // SmartBuffer
        sp<ImageBufferPool> pImagePool = ImageBufferPool::create(
            bufConf.name,
            bufConf.width, bufConf.height,
            (EImageFormat)bufConf.format,
            ImageBufferPool::USAGE_HW_AND_SW, MFALSE);

        if (pImagePool == nullptr) {
            MY_LOGE("create [%s] failed!", bufConf.name);
            return MFALSE;
        }
        mvImagePools.add(
            make_tuple(bufConf.width, bufConf.height, bufConf.format),
            pImagePool);
    }

    // TODO: check the register table's size
    auto pNormalStream = NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0);
    if (pNormalStream == NULL)
    {
        MY_LOGE("pNormalStream create instance for DC BufferPool failed!");
        return MFALSE;
    }
    pNormalStream->init(LOG_TAG);
    muTuningBufSize = pNormalStream->getRegTableSize();
    mpTuningBufferPool = TuningBufferPool::create(mName, muTuningBufSize);
    mpTuningBufferPool->setAutoAllocate(8);

    pNormalStream->uninit(LOG_TAG);
    pNormalStream->destroyInstance();
    pNormalStream = NULL;

    mbInit = MTRUE;
    return MTRUE;
}


MBOOL CaptureBufferPool::uninit()
{
    Mutex::Autolock _l(mPoolLock);

    if (!mbInit) {
        MY_LOGE("do uninit when it's not init yet!");
        return MFALSE;
    }

    // image buffers release
    for (size_t i = 0; i < mvImagePools.size(); i++) {
        ImageBufferPool::destroy(mvImagePools.editValueAt(i));
    }
    mvImagePools.clear();

    TuningBufferPool::destroy(mpTuningBufferPool);
    mpTuningBufferPool = NULL;

    mbInit = MFALSE;
    return MTRUE;
}

MBOOL CaptureBufferPool::allocate()
{

    MBOOL ret = MTRUE;
    MY_LOGD("ret:%d", ret);
    return ret;
}

SmartTuningBuffer CaptureBufferPool::getTuningBuffer()
{
    SmartTuningBuffer pBuf = mpTuningBufferPool->request();
    memset((void*)pBuf->mpVA, 0, muTuningBufSize);
    return pBuf;
}

SmartImageBuffer CaptureBufferPool::getImageBuffer(MUINT32 width, MUINT32 height, MUINT32 format)
{
    Mutex::Autolock _l(mPoolLock);

    PoolKey_T poolKey =  make_tuple(width, height, format);

    if (mvImagePools.indexOfKey(poolKey) < 0) {
        MBOOL bUseSingleBuffer = (format == eImgFmt_I422 || format == eImgFmt_NV12);
        android::sp<ImageBufferPool> pImagePool = ImageBufferPool::create(
            "CapturePipe", width, height, (EImageFormat)format,
            ImageBufferPool::USAGE_HW_AND_SW, bUseSingleBuffer);

        if (pImagePool == nullptr) {
            MY_LOGE("create buffer pool failed!");
            return MFALSE;
        }

        pImagePool->setAutoAllocate(8);
        mvImagePools.add(poolKey, pImagePool);
    }

    sp<ImageBufferPool> pImagePool = mvImagePools.valueFor(poolKey);

    return pImagePool->request();
}


} // NSCapture
} // NSFeaturePipe
} // NSCamFeature
} // NSCam
