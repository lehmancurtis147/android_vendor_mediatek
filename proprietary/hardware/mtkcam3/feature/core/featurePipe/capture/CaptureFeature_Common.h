/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_COMMON_H_
#define _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_COMMON_H_

#include <memory>
#include <featurePipe/core/include/MtkHeader.h>

#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/metadata/IMetadata.h>
#include <featurePipe/core/include/ImageBufferPool.h>
#include <featurePipe/core/include/FatImageBufferPool.h>
#include <featurePipe/core/include/GraphicBufferPool.h>


#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>

#include <DpIspStream.h>
#include "DebugControl.h"

#include <mtkcam/utils/hw/HwTransform.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {

template<typename T>
using UniquePtr = std::unique_ptr<T, std::function<MVOID(T*)>>;

MBOOL copyImageBuffer(IImageBuffer *src, IImageBuffer *dst);

MBOOL dumpToFile(IImageBuffer *buffer, const char *fmt, ...);

MUINT32 align(MUINT32 val, MUINT32 bits);

MUINT getSensorRawFmt(MINT32 sensorId);

typedef MUINT8 PathID_T;
typedef MUINT8 NodeID_T;
const char* NodeID2Name(MUINT8 nodeId);
const char* PathID2Name(MUINT8 pathId);
const char* TypeID2Name(MUINT8 typeId);
const char* FeatID2Name(MUINT8 featId);
const char* SizeID2Name(MUINT8 sizeId);
PathID_T FindPath(NodeID_T src, NodeID_T dst);
const NodeID_T* GetPath(PathID_T pid);

/******************************************************************************
 *  Metadata Access
 ******************************************************************************/
/**
 * Try to get metadata value
 *
 * @param[in]  pMetadata: IMetadata instance
 * @param[in]  tag: the metadata tag to retrieve
 * @param[out]  rVal: the metadata value
 *
 *
 * @return
 *  -  true if successful; otherwise false.
 */
template <typename T>
inline MBOOL
tryGetMetadata(
    const IMetadata* pMetadata,
    MUINT32 const tag,
    T & rVal
)
{
    if( pMetadata == NULL ) {
        CAM_LOGW("pMetadata == NULL");
        return MFALSE;
    }
    //
    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}

/**
 * Try to set metadata value
 *
 * @param[in]  pMetadata: IMetadata instance
 * @param[in]  tag: the metadata tag to set
 * @param[in]  val: the metadata value to be configured
 *
 *
 * @return
 *  -  true if successful; otherwise false.
 */
template <typename T>
inline MVOID
trySetMetadata(
    IMetadata* pMetadata,
    MUINT32 const tag,
    T const& val
)
{
    if( pMetadata == NULL ) {
        CAM_LOGW("pMetadata == NULL");
        return;
    }
    //
    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    pMetadata->update(tag, entry);
}


// utilities for crop

/******************************************************************************
 *  Metadata Access
 ******************************************************************************/
inline MINT32 div_round(MINT32 const numerator, MINT32 const denominator) {
    return (( numerator < 0 ) ^ (denominator < 0 )) ?
        (( numerator - denominator/2)/denominator) : (( numerator + denominator/2)/denominator);
}

struct vector_f //vector with floating point
{
    MPoint  p;
    MPoint  pf;

                                vector_f(
                                        MPoint const& rP = MPoint(),
                                        MPoint const& rPf = MPoint()
                                        )
                                    : p(rP)
                                    , pf(rPf)
                                {}
};

struct simpleTransform
{
    // just support translation than scale, not a general formulation
    // translation
    MPoint    tarOrigin;
    // scale
    MSize     oldScale;
    MSize     newScale;

                                simpleTransform(
                                        MPoint rOrigin = MPoint(),
                                        MSize  rOldScale = MSize(),
                                        MSize  rNewScale = MSize()
                                        )
                                    : tarOrigin(rOrigin)
                                    , oldScale(rOldScale)
                                    , newScale(rNewScale)
                                {}
};

// transform MPoint
inline MPoint transform(simpleTransform const& trans, MPoint const& p) {
    return MPoint(
            div_round( (p.x - trans.tarOrigin.x) * trans.newScale.w, trans.oldScale.w),
            div_round( (p.y - trans.tarOrigin.y) * trans.newScale.h, trans.oldScale.h)
            );
};

inline MPoint inv_transform(simpleTransform const& trans, MPoint const& p) {
    return MPoint(
            div_round( p.x * trans.oldScale.w, trans.newScale.w) + trans.tarOrigin.x,
            div_round( p.y * trans.oldScale.h, trans.newScale.h) + trans.tarOrigin.y
            );
};

inline int int_floor(float x) {
    int i = (int)x;
    return i - (i > x);
}

// transform vector_f
inline vector_f transform(simpleTransform const& trans, vector_f const& p) {
    MFLOAT const x = (p.p.x + (p.pf.x/(MFLOAT)(1u<<31))) * trans.newScale.w / trans.oldScale.w;
    MFLOAT const y = (p.p.y + (p.pf.y/(MFLOAT)(1u<<31))) * trans.newScale.h / trans.oldScale.h;
    int const x_int = int_floor(x);
    int const y_int = int_floor(y);
    return vector_f(
            MPoint(x_int, y_int),
            MPoint((x - x_int) * (1u<<31), (y - y_int) * (1u<<31))
            );
};

inline vector_f inv_transform(simpleTransform const& trans, vector_f const& p) {
    MFLOAT const x = (p.p.x + (p.pf.x/(MFLOAT)(1u<<31))) * trans.oldScale.w / trans.newScale.w;
    MFLOAT const y = (p.p.y + (p.pf.y/(MFLOAT)(1u<<31))) * trans.oldScale.h / trans.newScale.h;
    int const x_int = int_floor(x);
    int const y_int = int_floor(y);
    return vector_f(
            MPoint(x_int, y_int),
            MPoint((x - x_int) * (1u<<31), (y - y_int) * (1u<<31))
            );
};

// transform MSize
inline MSize transform(simpleTransform const& trans, MSize const& s) {
    return MSize(
            div_round( s.w * trans.newScale.w, trans.oldScale.w),
            div_round( s.h * trans.newScale.h, trans.oldScale.h)
            );
};

inline MSize inv_transform(simpleTransform const& trans, MSize const& s) {
    return MSize(
            div_round( s.w * trans.oldScale.w, trans.newScale.w),
            div_round( s.h * trans.oldScale.h, trans.newScale.h)
            );
};

// transform MRect
inline MRect transform(simpleTransform const& trans, MRect const& r) {
    return MRect(transform(trans, r.p), transform(trans, r.s));
};

inline MRect inv_transform(simpleTransform const& trans, MRect const& r) {
    return MRect(inv_transform(trans, r.p), inv_transform(trans, r.s));
};

class CropCalculator : public RefBase {
public:

    CropCalculator(MUINT uSensorIndex, MUINT32 uLogLevel);
public:
    struct Factor : public RefBase {
        MSize               mSensorSize;
        // P1 Crop Info
        MRect               mP1SensorCrop;
        MRect               mP1DmaCrop;
        MSize               mP1ResizerSize;
        // Transform Matrix
        NSCamHW::HwMatrix   mActive2Sensor;
        NSCamHW::HwMatrix   mSensor2Active;
        simpleTransform     mSensor2Resizer;
        MINT32              mSensorMode;
        // Target crop: cropRegion (active array coorinate)
        // not applied eis's mv yet, but the crop area is already reduced by EIS ratio.
        MRect               mActiveCrop;
        MRect               mSensorCrop;
        float               mFovDiff_X;
        float               mFovDiff_Y;
        // EIS
        MBOOL               mEnableEis;
        vector_f            mActiveEisMv;   //active array coor.
        vector_f            mSensorEisMv;   //sensor coor.
        vector_f            mResizerEisMv;  //resized coor.

        MBOOL               isEqual(
                                sp<Factor> pFactor
                            ) const;

        MVOID               dump() const;

    };

    sp<Factor>              getFactor(
                                const IMetadata* inApp,
                                const IMetadata* inHal
                            );

    static MVOID            evaluate(
                                MSize const &srcSize,
                                MSize const &dstSize,
                                MRect &srcCrop
                            );

    MVOID                   evaluate(
                                sp<Factor> pFactor,
                                MSize const &dstSize,
                                MRect &srcCrop,
                                MBOOL const bResized = MFALSE
                            ) const;

    MBOOL                   refineBoundary(
                                MSize const &bufSize,
                                MRect &crop
                            ) const;

    const MRect&            getActiveArray() {
                                return mActiveArray;
                            };

    struct eis_region {
        MUINT32 x_int;
        MUINT32 x_float;
        MUINT32 y_int;
        MUINT32 y_float;
        MSize s;
#if SUPPORT_EIS_MV
        MUINT32 x_mv_int;
        MUINT32 x_mv_float;
        MUINT32 y_mv_int;
        MUINT32 y_mv_float;
        MUINT32 is_from_zzr;
#endif
    };

    MBOOL                   queryEisRegion(
                                const IMetadata*, eis_region&
                            ) const;

private:

    MUINT32                 mLogLevel;
    MRect                   mActiveArray;
    mutable NSCamHW::HwTransHelper
                            mHwTransHelper;
    sp<Factor>              mpLastFactor;

};

class IBooster
{
public:
    using Ptr = UniquePtr<IBooster>;

public:
    static Ptr createInstance(const std::string& name);

public:
    virtual ~IBooster() = 0;

public:
    virtual const std::string& getName() = 0;

    virtual MVOID enable() = 0;

    virtual MVOID disable() = 0;
};
using IBoosterPtr = IBooster::Ptr;

enum SensorAliasName
{
    eSAN_None,
    eSAN_Master,
    eSAN_Sub_01
};

enum SensorConfigType
{
    eSCT_None,
    eSCT_BayerBayer,
    eSCT_BayerMono
};

enum RawImageType
{
    eRIT_None,
    eRIT_Imgo,
    eRIT_Rrzo
};

union IspProfileHint
{
    const uint64_t mValue;
    struct
    {
        uint8_t mSensorAliasName    :8;
        uint8_t mSensorConfigType   :8;
        uint8_t mRawImageType       :8;
    };

    IspProfileHint()
    : IspProfileHint(eSAN_None, eSCT_None, eRIT_None)
    {

    }

    IspProfileHint(uint8_t sensorAliasName, uint8_t sensorConfigType, uint8_t rawImageType)
    : mValue(0)
    {
        mSensorAliasName = sensorAliasName;
        mSensorConfigType = sensorConfigType;
        mRawImageType = rawImageType;
    }

    struct Compare
    {
       bool inline operator() (const IspProfileHint& lhs, const IspProfileHint& rhs) const
       {
           return lhs.mValue < rhs.mValue;
       }
    };
};

struct IspProfileInfo
{
    std::string mName;
    MUINT8      mValue;

    IspProfileInfo(const char* name, MUINT8 value)
    : mName(name)
    , mValue(value)
    {

    }
};
#define MAKE_ISP_PROFILE_INFO(PROFILE_NAME) IspProfileInfo(#PROFILE_NAME, PROFILE_NAME)


class IspProfileManager final
{
public:
    IspProfileManager() = delete;

public:
    static const IspProfileInfo& get(const IspProfileHint& hint);
};

enum NVRAM_TYPE
{
    NVRAM_TYPE_DRE,
    NVRAM_TYPE_CZ,
    NVRAM_TYPE_SWNR_THRES
};

const void *getTuningFromNvram(MUINT32 openId, MUINT32& idx, MINT32 magicNo, MINT32 type, MBOOL enableLog);

} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_CAPTURE_FEATURE_PIPE_CAPTURE_FEATURE_COMMON_H_
