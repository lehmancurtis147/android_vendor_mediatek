/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

// Standard C header file
#include <mutex>
#include <algorithm>
#include <sstream>
#include <chrono>
#include <iomanip>
// Android system/core header file

// mtkcam custom header file

// mtkcam global header file
#include <vendor/mediatek/hardware/power/2.0/IPower.h>
#include <vendor/mediatek/hardware/power/2.0/types.h>

// Module header file
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
// eis
#include <camera_custom_eis.h>
#include <mtkcam3/feature/eis/EisInfo.h>
// nvram tuning
#include <camera_custom_nvram.h>
#include <mtkcam/aaa/INvBufUtil.h>
#if MTK_CAM_NEW_NVRAM_SUPPORT
#   include <mtkcam/utils/mapping_mgr/cam_idx_mgr.h>
#endif
//
#include <mtkcam/drv/IHalSensor.h>
//
#include <featurePipe/core/include/DebugUtil.h>
//
// isp tuning
#include <isp_tuning/isp_tuning.h>
// Local header file
#include "CaptureFeatureRequest.h"
#include "CaptureFeature_Common.h"

// Logging
#include "DebugControl.h"
#define PIPE_CLASS_TAG "Util"
#define PIPE_TRACE TRACE_CAPTURE_FEATURE_COMMON
#include <featurePipe/core/include/PipeLog.h>

/*******************************************************************************
* MACRO Utilities Define.
********************************************************************************/
namespace { // anonymous namespace for MACRO function
using AutoObject = NSCam::NSCamFeature::NSFeaturePipe::NSCapture::UniquePtr<const char>;
//
auto
createAutoScoper(const char* funcName) -> AutoObject
{
    CAM_LOGD("[%s] +", funcName);
    return AutoObject(funcName, [](const char* p)
    {
        CAM_LOGD("[%s] -", p);
    });
}
#define SCOPED_TRACER() auto scoped_tracer = ::createAutoScoper(__FUNCTION__)
//
auto
createAutoTimer(const char* funcName, const char* fmt, ...) -> AutoObject
{
    using Timing = std::chrono::time_point<std::chrono::high_resolution_clock>;
    using DuationTime = std::chrono::duration<float, std::milli>;
    //
    static const MINT32 LENGTH = 512;

    char* pBuf = new char[LENGTH];
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(pBuf, LENGTH, fmt, ap);
    va_end(ap);

    Timing startTime = std::chrono::high_resolution_clock::now();
    return AutoObject(pBuf, [funcName, startTime](const char* p)
    {
        Timing endTime = std::chrono::high_resolution_clock::now();
        DuationTime duationTime = endTime - startTime;
        CAM_LOGD("[%s] %s, elapsed(ms):%.4f", funcName, p, duationTime.count());
        delete[] p;
    });
}
#define AUTO_TIMER(FMT, arg...) auto auto_timer = ::createAutoTimer(__FUNCTION__, FMT, ##arg);
//
#define UNREFERENCED_PARAMETER(param) (param)
//
} // end anonymous namespace for MACRO function


/*******************************************************************************
* Namespace start.
********************************************************************************/
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
namespace NSCapture {


/*******************************************************************************
* Alias.
********************************************************************************/
using namespace vendor::mediatek::hardware::power::V2_0;


/*******************************************************************************
* Global Function.
*******************************************************************************/

MBOOL   copyImageBuffer(IImageBuffer *src, IImageBuffer *dst);

MBOOL   dumpToFile(IImageBuffer *buffer, const char *fmt, ...);

MUINT32 align(MUINT32 val, MUINT32 bits);

MUINT   getSensorRawFmt(MINT32 sensorId);


/*******************************************************************************
* Class Define
*******************************************************************************/
/**
 * @brief slim wrapper of Powerhal, and it is a singleton
 */
class PowerHalWrapper final
{
public:
    using Handle      = MINT32;

    static inline PowerHalWrapper* getInstance();

public:
    ~PowerHalWrapper();

public:
    Handle createHandle(const std::string& userName);

    MVOID destroyhandle(const std::string& userName, Handle handle);

    MVOID enableBoost(const std::string& userName, Handle handle);

    MVOID disableBoost(const std::string& userName, Handle handle);

private:
    PowerHalWrapper();

private:
    using Handles     = std::vector<Handle>;
    using HandlesItor = Handles::iterator;

private:
    MVOID initLock();

    MVOID uninitLock();

    MVOID dumpLock();

    inline MBOOL getIsValidLock(Handle handle);

    inline MBOOL getIsValidLock(Handle handle, HandlesItor& itor);

private:
    std::mutex           sLocker;
    sp<IPower>           sPowerPtr;
    std::vector<Handle>  sHandles;
    //
    static constexpr MINT32      INVALID_HANDLE = -1;
    static constexpr MINT        TIMEOUT        = (10*1000);
};
/**
 * @brief empty boost for dufaule use
 */
class EmptyBoost : public IBooster
{
public:
    EmptyBoost(const std::string& name);

    ~EmptyBoost();

public:
    const std::string& getName() override;

public:
    MVOID enable() override;

    MVOID disable() override;

private:
    const std::string       mName;
};
/**
 * @brief boost implementation of CUP
 */
class CPUBooster : public IBooster
{
public:
    CPUBooster(const std::string& name);

    ~CPUBooster();

public:
    const std::string& getName() override;

public:
    MVOID enable() override;

    MVOID disable() override;

private:
    const std::string       mName;
    PowerHalWrapper::Handle mHandle;
};


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Global Function Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
copyImageBuffer(IImageBuffer *src, IImageBuffer *dst)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;

    if( !src || !dst )
    {
        MY_LOGE("Invalid buffers src=%p dst=%p", src, dst);
        ret = MFALSE;
    }
    else if( src->getImgSize() != dst->getImgSize() )
    {
        MY_LOGE("Mismatch buffer size src(%dx%d) dst(%dx%d)",
                src->getImgSize().w, src->getImgSize().h,
                dst->getImgSize().w, dst->getImgSize().h);
        ret = MFALSE;
    }
    else
    {
        unsigned srcPlane = src->getPlaneCount();
        unsigned dstPlane = dst->getPlaneCount();

        if( !srcPlane || !dstPlane ||
            (srcPlane != dstPlane && srcPlane != 1 && dstPlane != 1) )
        {
            MY_LOGE("Mismatch buffer plane src(%d) dst(%d)", srcPlane, dstPlane);
            ret = MFALSE;
        }
        for( unsigned i = 0; i < srcPlane; ++i )
        {
            if( !src->getBufVA(i) )
            {
                MY_LOGE("Invalid src plane[%d] VA", i);
                ret = MFALSE;
            }
        }
        for( unsigned i = 0; i < dstPlane; ++i )
        {
            if( !dst->getBufVA(i) )
            {
                MY_LOGE("Invalid dst plane[%d] VA", i);
                ret = MFALSE;
            }
        }

        if( srcPlane == 1 )
        {
            MY_LOGD("src: plane=1 size=%zu stride=%zu",
                    src->getBufSizeInBytes(0), src->getBufStridesInBytes(0));
            ret = MFALSE;
        }
        if( dstPlane == 1 )
        {
            MY_LOGD("dst: plane=1 size=%zu stride=%zu",
                    dst->getBufSizeInBytes(0), dst->getBufStridesInBytes(0));
            ret = MFALSE;
        }

        if( ret )
        {
            char *srcVA = NULL, *dstVA = NULL;
            size_t srcSize = 0;
            size_t dstSize = 0;
            size_t srcStride = 0;
            size_t dstStride = 0;

            for( unsigned i = 0; i < srcPlane && i < dstPlane; ++i )
            {
                if( i < srcPlane )
                {
                    srcVA = (char*)src->getBufVA(i);
                }
                if( i < dstPlane )
                {
                    dstVA = (char*)dst->getBufVA(i);
                }

                srcSize = src->getBufSizeInBytes(i);
                dstSize = dst->getBufSizeInBytes(i);
                srcStride = src->getBufStridesInBytes(i);
                dstStride = dst->getBufStridesInBytes(i);
                MY_LOGD("plane[%d] memcpy %p(%zu)=>%p(%zu)",
                          i, srcVA, srcSize, dstVA, dstSize);
                if( srcStride == dstStride )
                {
                    memcpy((void*)dstVA, (void*)srcVA, (srcSize <= dstSize) ? srcSize : dstSize );
                }
                else
                {
                    MY_LOGD("Stride: src(%zu) dst(%zu)", srcStride, dstStride);
                    size_t stride = (srcStride < dstStride) ? srcStride : dstStride;
                    unsigned height = dstSize / dstStride;
                    for( unsigned j = 0; j < height; ++j )
                    {
                        memcpy((void*)dstVA, (void*)srcVA, stride);
                        srcVA += srcStride;
                        dstVA += dstStride;
                    }
                }
            }
        }
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL
dumpToFile(IImageBuffer *buffer, const char *fmt, ...)
{
    MBOOL ret = MFALSE;
    if( buffer && fmt )
    {
        char filename[256];
        va_list arg;
        va_start(arg, fmt);
        vsprintf(filename, fmt, arg);
        va_end(arg);
        buffer->saveToFile(filename);
        ret = MTRUE;
    }
    return ret;
}

MUINT32
align(MUINT32 val, MUINT32 bits)
{
    // example: align 5 bits => align 32
    MUINT32 mask = (0x01 << bits) - 1;
    return (val + mask) & (~mask);
}

MUINT
getSensorRawFmt(MINT32 sensorId)
{
    MUINT ret = SENSOR_RAW_FMT_NONE;
    IHalSensorList *sensorList = MAKE_HalSensorList();
    if (NULL == sensorList) {
        MY_LOGE("cannot get sensor list");
        return ret;
    }

    int32_t sensorCount = sensorList->queryNumberOfSensors();
    if(sensorId >= sensorCount) {
        MY_LOGW("sensor index should be lower than %d", sensorCount);
        return ret;
    }

    SensorStaticInfo sensorStaticInfo;
    memset(&sensorStaticInfo, 0, sizeof(SensorStaticInfo));
    MINT32 sendorDevIndex = sensorList->querySensorDevIdx(sensorId);
    sensorList->querySensorStaticInfo(sendorDevIndex, &sensorStaticInfo);

    ret = sensorStaticInfo.rawFmtType;
    MY_LOGD("get sensor raw Fmt, sensorId:%d, sendorIndex:%d, rawFmt:%u", sensorId, sendorDevIndex, ret);
    return ret;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Global Function and Variable Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static const NodeID_T sPathMap[NUM_OF_PATH][2] =
{
    {NID_ROOT,      NID_ROOT},          // PID_ENQUE
    {NID_ROOT,      NID_RAW},           // PID_ROOT_TO_RAW
    {NID_ROOT,      NID_P2A},           // PID_ROOT_TO_P2A
    {NID_ROOT,      NID_MULTIRAW},      // PID_ROOT_TO_MULTIRAW
    {NID_RAW,       NID_P2A},           // PID_RAW_TO_P2A
    {NID_MULTIRAW,  NID_P2A},           // PID_MULTIRAW_TO_P2A
    {NID_P2A,       NID_DEPTH},         // PID_P2A_TO_DEPTH
    {NID_P2A,       NID_FUSION},        // PID_P2A_TO_FUSION
    {NID_P2A,       NID_MULTIYUV},      // PID_P2A_TO_MULTIYUV
    {NID_P2A,       NID_YUV},           // PID_P2A_TO_YUV
    {NID_P2A,       NID_YUV2},          // PID_P2A_TO_YUV2
    {NID_P2A,       NID_MDP},           // PID_P2A_TO_MDP
    {NID_P2A,       NID_FD} ,           // PID_P2A_TO_FD
    {NID_FD,        NID_DEPTH},         // PID_FD_TO_DEPTH
    {NID_FD,        NID_FUSION},        // PID_FD_TO_FUSION
    {NID_FD,        NID_MULTIYUV},      // PID_FD_TO_MULTIFRAME
    {NID_FD,        NID_YUV},           // PID_FD_TO_YUV
    {NID_FD,        NID_YUV2},          // PID_FD_TO_YUV2
    {NID_MULTIYUV,  NID_YUV},           // PID_MULTIFRAME_TO_YUV
    {NID_MULTIYUV,  NID_YUV2},          // PID_MULTIFRAME_TO_YUV2
    {NID_MULTIYUV,  NID_BOKEH},         // PID_MULTIFRAME_TO_BOKEH
    {NID_MULTIYUV,  NID_MDP},           // PID_MULTIFRAME_TO_MDP
    {NID_FUSION,    NID_YUV},           // PID_FUSION_TO_YUV
    {NID_FUSION,    NID_MDP},           // PID_FUSION_TO_MDP
    {NID_DEPTH,     NID_BOKEH},         // PID_DEPTH_TO_BOKEH
    {NID_YUV,       NID_BOKEH},         // PID_YUV_TO_BOKEH
    {NID_YUV,       NID_YUV2},          // PID_YUV_TO_YUV2
    {NID_YUV,       NID_MDP},           // PID_YUV_TO_MDP
    {NID_BOKEH,     NID_YUV2},          // PID_BOKEH_TO_YUV2
    {NID_BOKEH,     NID_MDP},           // PID_BOKEH_TO_MDP
    {NID_YUV2,      NID_MDP},           // PID_YUV2_TO_MDP
};

const char*
PathID2Name(PathID_T pid)
{
    switch(pid)
    {
    case PID_ENQUE:                 return "enque";
    case PID_ROOT_TO_RAW:           return "root_to_raw";
    case PID_ROOT_TO_P2A:           return "root_to_p2a";
    case PID_ROOT_TO_MULTIRAW:      return "root_to_multiraw";
    case PID_RAW_TO_P2A:            return "raw_to_p2a";
    case PID_MULTIRAW_TO_P2A:       return "multiraw_to_p2a";
    case PID_P2A_TO_DEPTH:          return "p2a_to_depth";
    case PID_P2A_TO_FUSION:         return "p2a_to_fusion";
    case PID_P2A_TO_MULTIYUV:       return "p2a_to_multiyuv";
    case PID_P2A_TO_YUV:            return "p2a_to_yuv";
    case PID_P2A_TO_YUV2:           return "p2a_to_yuv2";
    case PID_P2A_TO_MDP:            return "p2a_to_mdp";
    case PID_P2A_TO_FD:             return "p2a_to_fd";
    case PID_FD_TO_DEPTH:           return "fd_to_depth";
    case PID_FD_TO_FUSION:          return "fd_to_fusion";
    case PID_FD_TO_MULTIFRAME:      return "fd_to_multiframe";
    case PID_FD_TO_YUV:             return "fd_to_yuv";
    case PID_FD_TO_YUV2:            return "fd_to_yuv2";
    case PID_MULTIFRAME_TO_YUV:     return "multiframe_to_yuv";
    case PID_MULTIFRAME_TO_YUV2:    return "multiframe_to_yuv2";
    case PID_MULTIFRAME_TO_BOKEH:   return "multiframe_to_bokeh";
    case PID_MULTIFRAME_TO_MDP:     return "multiframe_to_mdp";
    case PID_FUSION_TO_YUV:         return "fusion_to_yuv";
    case PID_FUSION_TO_MDP:         return "fusion_to_mdp";
    case PID_DEPTH_TO_BOKEH:        return "depth_to_bokeh";
    case PID_YUV_TO_BOKEH:          return "yuv_to_bokeh";
    case PID_YUV_TO_YUV2:           return "yuv_to_yuv2";
    case PID_YUV_TO_MDP:            return "yuv_to_mdp";
    case PID_BOKEH_TO_YUV2:         return "bokeh_to_yuv2";
    case PID_BOKEH_TO_MDP:          return "bokeh_to_mdp";
    case PID_YUV2_TO_MDP:           return "yuv2_to_mdp";
    case PID_DEQUE:                 return "deque";

    default:                        return "unknown";
    };
}

const char*
NodeID2Name(NodeID_T nid)
{
    switch(nid)
    {
    case NID_ROOT:                  return "root";
    case NID_RAW:                   return "raw";
    case NID_MULTIRAW:              return "multiraw";
    case NID_P2A:                   return "p2a";
    case NID_FD:                    return "fd";
    case NID_MULTIYUV:              return "multiyuv";
    case NID_FUSION:                return "fusion";
    case NID_DEPTH:                 return "depth";
    case NID_YUV:                   return "yuv";
    case NID_YUV_R1:                return "yuv_r1";
    case NID_YUV_R2:                return "yuv_r2";
    case NID_YUV2:                  return "yuv2";
    case NID_YUV2_R1:               return "yuv2_r1";
    case NID_YUV2_R2:               return "yuv2_r2";
    case NID_BOKEH:                 return "bokeh";
    case NID_MDP:                   return "mdp";

    default:                        return "unknown";
    };
}

const char*
TypeID2Name(TypeID_T tid)
{
    switch(tid)
    {
    case TID_MAN_FULL_RAW:          return "man_full_raw";
    case TID_MAN_FULL_YUV:          return "man_full_yuv";
    case TID_MAN_RSZ_RAW:           return "man_rsz_raw";
    case TID_MAN_RSZ_YUV:           return "man_rsz_yuv";
    case TID_MAN_CROP1_YUV:         return "man_crop1_yuv";
    case TID_MAN_CROP2_YUV:         return "man_crop2_yuv";
    case TID_MAN_SPEC_YUV:          return "man_spec_yuv";
    case TID_MAN_DEPTH:             return "man_depth";
    case TID_MAN_LCS:               return "man_lcs";
    case TID_MAN_FD_YUV:            return "man_fd_yuv";
    case TID_MAN_FD:                return "man_fd";
    case TID_SUB_FULL_RAW:          return "sub_full_raw";
    case TID_SUB_FULL_YUV:          return "sub_full_yuv";
    case TID_SUB_RSZ_RAW:           return "sub_rsz_raw";
    case TID_SUB_RSZ_YUV:           return "sub_rsz_yuv";
    case TID_SUB_LCS:               return "sub_lcs";
    case TID_POSTVIEW:              return "postview";
    case TID_JPEG:                  return "jpeg";
    case TID_THUMBNAIL:             return "thumbnail";
    case NULL_TYPE:                 return "";

    default:                        return "unknown";
    };
}

const char* FeatID2Name(FeatureID_T fid)
{
    switch(fid)
    {
    case FID_REMOSAIC:              return "remosaic";
    case FID_NR:                    return "nr";
    case FID_ABF:                   return "abf";
    case FID_HDR:                   return "hdr";
    case FID_MFNR:                  return "mfnr";
    case FID_FB:                    return "fb";
    case FID_BOKEH:                 return "bokeh";
    case FID_DEPTH:                 return "depth";
    case FID_FUSION:                return "fusion";
    case FID_CZ:                    return "cz";
    case FID_DRE:                   return "dre";
    case FID_FB_3RD_PARTY:          return "fb_3rd_party";
    case FID_HDR_3RD_PARTY:         return "hdr_3rd_party";
    case FID_HDR2_3RD_PARTY:        return "hdr2_3rd_party";
    case FID_MFNR_3RD_PARTY:        return "mfnr_3rd_party";
    case FID_BOKEH_3RD_PARTY:       return "bokeh_3rd_party";
    case FID_DEPTH_3RD_PARTY:       return "depth_3rd_party";
    case FID_FUSION_3RD_PARTY:      return "fusion_3rd_party";
    case FID_PUREBOKEH_3RD_PARTY:   return "purebokeh_3rd_party";

    default:                        return "unknown";
    };
}

const char*
SizeID2Name(FeatureID_T fid)
{
    switch(fid)
    {
    case SID_FULL:                  return "full";
    case SID_RESIZED:               return "resized";
    case SID_QUARTER:               return "quarter";
    case SID_ARBITRARY:             return "arbitrary";
    case SID_SPECIFIED:             return "specified";
    case NULL_SIZE:                 return "";

    default:                        return "unknown";
    };
}

PathID_T
FindPath(NodeID_T src, NodeID_T dst)
{
    for (PathID_T pid = PID_ENQUE + 1; pid < NUM_OF_PATH; pid++) {
        if (sPathMap[pid][0] == src && sPathMap[pid][1] == dst) {
            return pid;
        }
    }
    return NULL_PATH;
}

const NodeID_T* GetPath(PathID_T pid)
{
    if (pid < NUM_OF_PATH)
        return sPathMap[pid];
    return nullptr;
}

const void*
getTuningFromNvram(MUINT32 openId, MUINT32& idx, MINT32 magicNo, MINT32 type, MBOOL enableLog)
{
#if MTK_CAM_NEW_NVRAM_SUPPORT
    CAM_TRACE_BEGIN("getNvram");
    int err;
    NVRAM_CAMERA_FEATURE_STRUCT *pNvram;
    const void *pNRNvram = nullptr;

    if (idx >= EISO_NUM) {
        CAM_LOGE("wrong nvram idx %d", idx);
        return NULL;
    }

    // load some setting from nvram
    MUINT sensorDev = MAKE_HalSensorList()->querySensorDevIdx(openId);

    auto pNvBufUtil = MAKE_NvBufUtil();
    if (pNvBufUtil == NULL) {
        CAM_LOGE("pNvBufUtil==0");
        return NULL;
    }

    auto result = pNvBufUtil->getBufAndRead(
        CAMERA_NVRAM_DATA_FEATURE,
        sensorDev, (void*&)pNvram);
    if (result != 0) {
        CAM_LOGE("read buffer chunk fail");
        return NULL;
    }

    IdxMgr* pMgr = IdxMgr::createInstance(static_cast<ESensorDev_T>(sensorDev));
    CAM_IDX_QRY_COMB rMapping_Info;
    pMgr->getMappingInfo(static_cast<ESensorDev_T>(sensorDev), rMapping_Info, magicNo);

    switch (type)
    {
        case NVRAM_TYPE_DRE:
        {
            idx = pMgr->query(static_cast<ESensorDev_T>(sensorDev), NSIspTuning::EModule_CA_LTM, rMapping_Info, __FUNCTION__);
            pNRNvram = &(pNvram->CA_LTM[idx]);
            break;
        }
        case NVRAM_TYPE_CZ:
        {
            idx = pMgr->query(static_cast<ESensorDev_T>(sensorDev), NSIspTuning::EModule_ClearZoom, rMapping_Info, __FUNCTION__);
            pNRNvram = &(pNvram->ClearZoom[idx]);
            break;
        }
        case NVRAM_TYPE_SWNR_THRES:
        {
            idx = pMgr->query(static_cast<ESensorDev_T>(sensorDev), NSIspTuning::EModule_SWNR_THRES, rMapping_Info, __FUNCTION__);
            pNRNvram = &(pNvram->SWNR_THRES[idx]);
            break;
        }
        default:
        {
            CAM_LOGW("not support nvram type: %d", type);
            break;
        }
    }

    CAM_LOGD_IF(enableLog, "[%s] query nvram type(%d) magic(%d) mappingInfo index: %d",__FUNCTION__, type, magicNo, idx);
    CAM_TRACE_END();
    return pNRNvram;
#else
    UNREFERENCED_PARAMETER(openId);
    UNREFERENCED_PARAMETER(idx);
    UNREFERENCED_PARAMETER(magicNo);
    UNREFERENCED_PARAMETER(type);
    UNREFERENCED_PARAMETER(enableLog);
    return NULL;
#endif
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CropCalculator Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CropCalculator::
CropCalculator(MUINT uSensorIndex, MUINT32 uLogLevel)
    : mLogLevel(uLogLevel)
    , mHwTransHelper(uSensorIndex)
{
    sp<IMetadataProvider> pMetadataProvider = NSMetadataProviderManager::valueFor(uSensorIndex);
    if (pMetadataProvider != NULL) {
        const IMetadata& mrStaticMetadata = pMetadataProvider->getMtkStaticCharacteristics();

        if (tryGetMetadata<MRect>(&mrStaticMetadata, MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION, mActiveArray)) {
            MY_LOGD("Active Array(%d,%d)(%dx%d)",
                    mActiveArray.p.x, mActiveArray.p.y,
                    mActiveArray.s.w, mActiveArray.s.h);
        } else {
            MY_LOGE("no static info: MTK_SENSOR_INFO_ACTIVE_ARRAY_REGION");
        }
    } else {
        MY_LOGD("no metadata provider, senser:%d", uSensorIndex);
    }
}

sp<CropCalculator::Factor>
CropCalculator::
getFactor(const IMetadata* pInAppMeta, const IMetadata* pInHalMeta)
{
    sp<Factor> pFactor = new Factor();
    Factor& rFactor = *pFactor.get();

    if (!tryGetMetadata<MSize>(pInHalMeta, MTK_HAL_REQUEST_SENSOR_SIZE, rFactor.mSensorSize)) {
        MY_LOGE("cannot get MTK_HAL_REQUEST_SENSOR_SIZE");
        return NULL;
    }

    const MSize& sensor = rFactor.mSensorSize;

    // 1. Get current p1 buffer crop status
    if (!(tryGetMetadata<MRect>(pInHalMeta, MTK_P1NODE_SCALAR_CROP_REGION, rFactor.mP1SensorCrop) &&
          tryGetMetadata<MSize>(pInHalMeta, MTK_P1NODE_RESIZER_SIZE, rFactor.mP1ResizerSize) &&
          tryGetMetadata<MRect>(pInHalMeta, MTK_P1NODE_DMA_CROP_REGION, rFactor.mP1DmaCrop)))
    {
        MY_LOGW_IF(mLogLevel, "[FIXME] should sync with p1 for rFactor setting");
        rFactor.mP1SensorCrop = MRect(MPoint(0, 0), sensor);
        rFactor.mP1ResizerSize = sensor;
        rFactor.mP1DmaCrop = MRect(MPoint(0, 0), sensor);
    }

    // 2. Get sensor mode
    if (!tryGetMetadata<MINT32>(pInHalMeta, MTK_P1NODE_SENSOR_MODE, rFactor.mSensorMode)) {
        MY_LOGE("cannot get MTK_P1NODE_SENSOR_MODE");
        return NULL;
    }

    // 3. Query crop region (in active array domain)
    MRect& cropRegion = rFactor.mActiveCrop;
    if (!tryGetMetadata<MRect>(pInAppMeta, MTK_SCALER_CROP_REGION, cropRegion)) {
        cropRegion.p = MPoint(0, 0);
        cropRegion.s = mActiveArray.s;
        MY_LOGW("no MTK_SCALER_CROP_REGION: using full crop size %dx%d",
                   cropRegion.s.w, cropRegion.s.h);
    }

    // 4. Query EIS Mode
    MUINT8 eisMode = MTK_CONTROL_VIDEO_STABILIZATION_MODE_OFF;
    MINT32 advEisMode = MTK_EIS_FEATURE_EIS_MODE_OFF;
    if (!tryGetMetadata<MUINT8>(pInAppMeta, MTK_CONTROL_VIDEO_STABILIZATION_MODE, eisMode)) {
        MY_LOGW_IF(mLogLevel, "no MTK_CONTROL_VIDEO_STABILIZATION_MODE");
    }
    if (!tryGetMetadata<MINT32>(pInAppMeta, MTK_EIS_FEATURE_EIS_MODE, advEisMode)) {
        MY_LOGW_IF(mLogLevel, "no MTK_EIS_FEATURE_EIS_MODE");
    }

    rFactor.mEnableEis = (eisMode    == MTK_CONTROL_VIDEO_STABILIZATION_MODE_ON ||
                         advEisMode == MTK_EIS_FEATURE_EIS_MODE_ON);

    // 5. Use last rFactor if equal
    if (mpLastFactor != NULL && mpLastFactor->isEqual(pFactor)) {
        return mpLastFactor;
    } else {
        mpLastFactor = pFactor;
    }

    // 6. Transform Matrix
    if (!mHwTransHelper.getMatrixToActive(rFactor.mSensorMode, rFactor.mSensor2Active) ||
        !mHwTransHelper.getMatrixFromActive(rFactor.mSensorMode, rFactor.mActive2Sensor) ||
        !mHwTransHelper.calculateFovDifference(rFactor.mSensorMode, &rFactor.mFovDiff_X, &rFactor.mFovDiff_Y))
    {
        MY_LOGE("fail to get HW transform matrix or FOV difference!");
        return NULL;
    }

    rFactor.mSensor2Resizer = simpleTransform(
            rFactor.mP1SensorCrop.p,
            rFactor.mP1SensorCrop.s,
            rFactor.mP1ResizerSize);

    rFactor.mActive2Sensor.transform(rFactor.mActiveCrop, rFactor.mSensorCrop);

    MY_LOGD("Active:(%d,%d)(%dx%d) Sensor:(%d,%d)(%dx%d) Resizer:(%dx%d) DMA:(%d,%d)(%dx%d)",
           rFactor.mActiveCrop.p.x, rFactor.mActiveCrop.p.y,
           rFactor.mActiveCrop.s.w, rFactor.mActiveCrop.s.h,
           rFactor.mP1SensorCrop.p.x, rFactor.mP1SensorCrop.p.y,
           rFactor.mP1SensorCrop.s.w, rFactor.mP1SensorCrop.s.h,
           rFactor.mP1ResizerSize.w, rFactor.mP1ResizerSize.h,
           rFactor.mP1DmaCrop.p.x, rFactor.mP1DmaCrop.p.y,
           rFactor.mP1DmaCrop.s.w, rFactor.mP1DmaCrop.s.h);


    // 7. Query EIS
    {

        // EIS Info
        MINT64 iEisInfo = 0;
        MUINT32 uEisFactor = 0;
        MUINT32 uEisMode = 0;
        if (rFactor.mEnableEis) {
            if (!tryGetMetadata<MINT64>(pInHalMeta, MTK_EIS_INFO, iEisInfo)) {
                rFactor.mEnableEis = MFALSE;
                MY_LOGW("cannot get MTK_EIS_INFO, current EIS_INFO = %" PRIi64 " ", iEisInfo);
            } else {
                uEisFactor = EIS::EisInfo::getFactor(iEisInfo);
                uEisMode = EIS::EisInfo::getMode(iEisInfo);
                cropRegion.p.x += (cropRegion.s.w * (uEisFactor - 100) / 2 / uEisFactor);
                cropRegion.p.y += (cropRegion.s.h * (uEisFactor - 100) / 2 / uEisFactor);
                cropRegion.s = cropRegion.s * 100 / uEisFactor;
                MY_LOGD_IF(mLogLevel, "EIS: Factor %d, Mode 0x%x , Crop Region(%d, %d, %dx%d)",
                           uEisFactor, uEisMode,
                           cropRegion.p.x, cropRegion.p.y, cropRegion.s.w, cropRegion.s.h);
            }
        }

        // EIS Region
        eis_region eisRegion;
        if (rFactor.mEnableEis && queryEisRegion(pInHalMeta, eisRegion)) {
            // calculate mv
            vector_f& rSensorMv = rFactor.mSensorEisMv;
            vector_f& rResizerMv = rFactor.mResizerEisMv;
            MBOOL isResizedDomain = MTRUE;

#if SUPPORT_EIS_MV
            if (eisRegion.is_from_zzr)
            {
                rResizerMv.p.x  = eisRegion.x_mv_int;
                rResizerMv.pf.x = eisRegion.x_mv_float;
                rResizerMv.p.y  = eisRegion.y_mv_int;
                rResizerMv.pf.y = eisRegion.y_mv_float;
                rSensorMv = inv_transform(rFactor.mSensor2Resizer, rResizerMv);
            }
            else
            {
                isResizedDomain = MFALSE;
                rSensorMv.p.x  = eisRegion.x_mv_int;
                rSensorMv.pf.x = eisRegion.x_mv_float;
                rSensorMv.p.y  = eisRegion.y_mv_int;
                rSensorMv.pf.y = eisRegion.y_mv_float;
                rResizerMv = transform(rFactor.mSensor2Resizer, rSensorMv);
            }
#else
            //eis in resized domain
            if (EIS_MODE_IS_EIS_12_ENABLED(uEisMode))
            {
                const MSize& resizer = rFactor.mP1ResizerSize;
                rResizerMv.p.x = eisRegion.x_int - (resizer.w * (uEisFactor - 100) / 2 / uEisFactor);
                rResizerMv.pf.x = eisRegion.x_float;
                rResizerMv.p.y = eisRegion.y_int - (resizer.h * (uEisFactor - 100) / 2 / uEisFactor);
                rResizerMv.pf.y = eisRegion.y_float;
                rSensorMv = inv_transform(rFactor.mSensor2Resizer, rResizerMv);
            }
            else
            {
                rResizerMv.p.x = 0;
                rResizerMv.pf.x = 0.0f;
                rResizerMv.p.y = 0;
                rResizerMv.pf.y = 0.0f;
            }
#endif
            MY_LOGD_IF(mLogLevel > 1, "mv (%s): (%d, %d, %d, %d) -> (%d, %d, %d, %d)",
                       isResizedDomain ? "r->s" : "s->r",
                       rResizerMv.p.x,
                       rResizerMv.pf.x,
                       rResizerMv.p.y,
                       rResizerMv.pf.y,
                       rSensorMv.p.x,
                       rSensorMv.pf.x,
                       rSensorMv.p.y,
                       rSensorMv.pf.y
            );
            // rFactor.mActiveEisMv = inv_transform(rFactor.tranActive2Sensor, rFactor.mSensorEisMv);
            rFactor.mSensor2Active.transform(rFactor.mSensorEisMv.p, rFactor.mActiveEisMv.p);
            // FIXME: ignore float?
            // rFactor.mSensor2Active.transform(rFactor.mSensorEisMv.pf, rFactor.mActiveEisMv.pf);
            MY_LOGD_IF(mLogLevel > 1, "mv in active %d/%d, %d/%d",
                       rFactor.mActiveEisMv.p.x,
                       rFactor.mActiveEisMv.pf.x,
                       rFactor.mActiveEisMv.p.y,
                       rFactor.mActiveEisMv.pf.y
            );
        } else {
            rFactor.mEnableEis = MFALSE;
            memset(&rFactor.mActiveEisMv, 0, sizeof(vector_f));
            memset(&rFactor.mSensorEisMv, 0, sizeof(vector_f));
            memset(&rFactor.mResizerEisMv, 0, sizeof(vector_f));
        }
    }
    return pFactor;
}

MBOOL
CropCalculator::
queryEisRegion(const IMetadata* pInHalMeta, eis_region& region) const
{
    IMetadata::IEntry entry = pInHalMeta->entryFor(MTK_EIS_REGION);
#if SUPPORT_EIS_MV
    // get EIS's motion vector
    if (entry.count() > 8)
    {
        MINT32 x_mv         = entry.itemAt(6, Type2Type<MINT32>());
        MINT32 y_mv         = entry.itemAt(7, Type2Type<MINT32>());
        region.is_from_zzr  = entry.itemAt(8, Type2Type<MINT32>());
        MBOOL x_mv_negative = x_mv >> 31;
        MBOOL y_mv_negative = y_mv >> 31;
        // convert to positive for getting parts of int and float if negative
        if (x_mv_negative) x_mv = ~x_mv + 1;
        if (y_mv_negative) y_mv = ~y_mv + 1;
        region.x_mv_int   = (x_mv & (~0xFF)) >> 8;
        region.x_mv_float = (x_mv & (0xFF)) << 31;
        if(x_mv_negative){
            region.x_mv_int   = ~region.x_mv_int + 1;
            region.x_mv_float = ~region.x_mv_float + 1;
        }
        region.y_mv_int   = (y_mv& (~0xFF)) >> 8;
        region.y_mv_float = (y_mv& (0xFF)) << 31;
        if(y_mv_negative){
            region.y_mv_int   = ~region.y_mv_int + 1;
            region.y_mv_float = ~region.x_mv_float + 1;
        }
        MY_LOGD_IF(mLogLevel, "EIS MV:%d, %d, %d",
                        region.s.w,
                        region.s.h,
                        region.is_from_zzr);
     }
#endif
    // get EIS's region
    if (entry.count() > 5) {
        region.x_int = entry.itemAt(0, Type2Type<MINT32>());
        region.x_float = entry.itemAt(1, Type2Type<MINT32>());
        region.y_int = entry.itemAt(2, Type2Type<MINT32>());
        region.y_float = entry.itemAt(3, Type2Type<MINT32>());
        region.s.w = entry.itemAt(4, Type2Type<MINT32>());
        region.s.h = entry.itemAt(5, Type2Type<MINT32>());
        MY_LOGD_IF(mLogLevel, "EIS Region: %d, %d, %d, %d, %dx%d",
                   region.x_int,
                   region.x_float,
                   region.y_int,
                   region.y_float,
                   region.s.w,
                   region.s.h);
        return MTRUE;
    }
    MY_LOGW("wrong eis region count %d", entry.count());
    return MFALSE;
}

MVOID
CropCalculator::
evaluate(MSize const &srcSize, MSize const &dstSize, MRect &srcCrop)
{
    // pillarbox
    if (srcSize.w * dstSize.h > srcSize.h * dstSize.w) {
        srcCrop.s.w = div_round(srcSize.h * dstSize.w, dstSize.h);
        srcCrop.s.h = srcSize.h;
        srcCrop.p.x = ((srcSize.w - srcCrop.s.w) >> 1);
        srcCrop.p.y = 0;
    }
    // letterbox
    else {
        srcCrop.s.w = srcSize.w;
        srcCrop.s.h = div_round(srcSize.w * dstSize.h, dstSize.w);
        srcCrop.p.x = 0;
        srcCrop.p.y = ((srcSize.h - srcCrop.s.h) >> 1);
    }
}

MVOID
CropCalculator::
evaluate(sp<Factor> pFactor, MSize const &dstSize, MRect &srcCrop, MBOOL const bResized) const
{
    Factor& rFactor = *pFactor.get();
    MRect& rSensorCrop = rFactor.mSensorCrop;
#define abs(x,y) ((x)>(y)?(x)-(y):(y)-(x))
#define FOV_DIFF_TOLERANCE (3)
    MRect s_viewcrop;
    // pillarbox
    if (rSensorCrop.s.w * dstSize.h > rSensorCrop.s.h * dstSize.w) {
        s_viewcrop.s.w = div_round(rSensorCrop.s.h * dstSize.w, dstSize.h);
        s_viewcrop.s.h = rSensorCrop.s.h;
        s_viewcrop.p.x = rSensorCrop.p.x + ((rSensorCrop.s.w - s_viewcrop.s.w) >> 1);
        if( s_viewcrop.p.x < 0 && abs(s_viewcrop.p.x, 0) < FOV_DIFF_TOLERANCE )
           s_viewcrop.p.x = 0;
        s_viewcrop.p.y = rSensorCrop.p.y;
    }
    // letterbox
    else {
        s_viewcrop.s.w = rSensorCrop.s.w;
        s_viewcrop.s.h = div_round(rSensorCrop.s.w * dstSize.h, dstSize.w);
        s_viewcrop.p.x = rSensorCrop.p.x;
        s_viewcrop.p.y = rSensorCrop.p.y + ((rSensorCrop.s.h - s_viewcrop.s.h) >> 1);
        if( s_viewcrop.p.y < 0 && abs(s_viewcrop.p.y, 0) < FOV_DIFF_TOLERANCE ) {
           s_viewcrop.p.y = 0;
        }
    }
    MY_LOGD_IF(mLogLevel > 1, "s_cropRegion(%d, %d, %dx%d), dst %dx%d, view crop(%d, %d, %dx%d)",
               rSensorCrop.p.x, rSensorCrop.p.y,
               rSensorCrop.s.w, rSensorCrop.s.h,
               dstSize.w, dstSize.h,
               s_viewcrop.p.x, s_viewcrop.p.y,
               s_viewcrop.s.w, s_viewcrop.s.h
    );
#undef FOV_DIFF_TOLERANCE

    float ratio_s = (float)rFactor.mP1SensorCrop.s.w / (float)rFactor.mP1SensorCrop.s.h;
    float ratio_d = (float)s_viewcrop.s.w / (float)s_viewcrop.s.h;
    MY_LOGD_IF(mLogLevel > 1, "ratio_s:%f ratio_d:%f", ratio_s, ratio_d);

    // handle HAL3 sensor mode 16:9 FOV
    if ((s_viewcrop.p.x < 0 || s_viewcrop.p.y < 0) && abs(ratio_s, ratio_d) < 0.1f) {
        MRect refined = s_viewcrop;
        float ratio = (float)rFactor.mP1SensorCrop.s.h / (float)s_viewcrop.s.h;
        refined.s.w = s_viewcrop.s.w * ratio;
        refined.s.h = s_viewcrop.s.h * ratio;
        refined.p.x = s_viewcrop.p.x + ((s_viewcrop.s.w - refined.s.w) >> 1);
        refined.p.y = s_viewcrop.p.y + ((s_viewcrop.s.h - refined.s.h) >> 1);
        //
        s_viewcrop = refined;
        MY_LOGD_IF(mLogLevel > 1, "refine negative crop ratio %f r.s(%dx%d) r.p(%d, %d)",ratio, refined.s.w,refined.s.h, refined.p.x, refined.p.y );
    } else if (rFactor.mFovDiff_Y < 1 && rFactor.mFovDiff_Y > rFactor.mFovDiff_X && (s_viewcrop.s.w*rFactor.mP1SensorCrop.s.h < s_viewcrop.s.h*rFactor.mP1SensorCrop.s.w)) {
        MRect refined = s_viewcrop;
        float dY = 1.0f - rFactor.mFovDiff_Y;
        refined.s.w = s_viewcrop.s.w * dY;
        refined.s.h = s_viewcrop.s.h * dY;
        refined.p.x = s_viewcrop.p.x + ((s_viewcrop.s.w - refined.s.w) >> 1);
        refined.p.y = s_viewcrop.p.y + ((s_viewcrop.s.h - refined.s.h) >> 1);
        //
        s_viewcrop = refined;
        float dX = 1.0f - rFactor.mFovDiff_X;
        MY_LOGD_IF(mLogLevel > 1, "dX %f dY %f r.s(%dx%d) r.p(%d, %d)", dX, dY, refined.s.w,refined.s.h, refined.p.x, refined.p.y );
    }
    //
    MY_LOGD_IF(mLogLevel > 1, "p1 sensor crop(%d, %d,%dx%d), %d, %d",
                    rFactor.mP1SensorCrop.p.x, rFactor.mP1SensorCrop.p.y,
                    rFactor.mP1SensorCrop.s.w, rFactor.mP1SensorCrop.s.h,
                    s_viewcrop.s.w*rFactor.mP1SensorCrop.s.h,  s_viewcrop.s.h*rFactor.mP1SensorCrop.s.w);
#undef abs
    if (bResized) {
        MRect r_viewcrop = transform(rFactor.mSensor2Resizer, s_viewcrop);
        srcCrop.s = r_viewcrop.s;
        srcCrop.p = r_viewcrop.p + rFactor.mResizerEisMv.p;

        // make sure hw limitation
        srcCrop.s.w &= ~(0x1);
        srcCrop.s.h &= ~(0x1);

        // check boundary
        if (refineBoundary(rFactor.mP1ResizerSize, srcCrop)) {
            MY_LOGW_IF(mLogLevel, "[FIXME] need to check crop!");
            rFactor.dump();
        }

    } else {
        srcCrop.s = s_viewcrop.s;
        srcCrop.p = s_viewcrop.p + rFactor.mSensorEisMv.p;

        // make sure hw limitation
        srcCrop.s.w &= ~(0x1);
        srcCrop.s.h &= ~(0x1);

        // check boundary
        if (refineBoundary(rFactor.mSensorSize, srcCrop)) {
            MY_LOGW_IF(mLogLevel, "[FIXME] need to check crop!");
            rFactor.dump();
        }
    }

    MY_LOGD_IF(mLogLevel > 1, "resized %d, crop (%d.%d)(%dx%d)",
           bResized,
           srcCrop.p.x, srcCrop.p.y,
           srcCrop.s.w, srcCrop.s.h);
}

MBOOL
CropCalculator::
refineBoundary(MSize const &bufSize, MRect &crop) const
{
    // tolerance
    if (crop.p.x == -1)
        crop.p.x = 0;

    if (crop.p.y == -1)
        crop.p.y = 0;

    MBOOL isRefined = MFALSE;
    MRect refined = crop;
    if (crop.p.x < 0) {
        refined.p.x = 0;
        isRefined = MTRUE;
    }
    if (crop.p.y < 0) {
        refined.p.y = 0;
        isRefined = MTRUE;
    }

    if ((refined.p.x + crop.s.w) > bufSize.w) {
        refined.s.w = bufSize.w - refined.p.x;
        isRefined = MTRUE;
    }
    if ((refined.p.y + crop.s.h) > bufSize.h) {
        refined.s.h = bufSize.h - refined.p.y;
        isRefined = MTRUE;
    }

    if (isRefined) {
        // make sure hw limitation
        refined.s.w &= ~(0x1);
        refined.s.h &= ~(0x1);

        MY_LOGW_IF(mLogLevel, "buffer size:%dx%d, crop(%d,%d)(%dx%d) -> refined crop(%d,%d)(%dx%d)",
                bufSize.w, bufSize.h,
                crop.p.x,
                crop.p.y,
                crop.s.w,
                crop.s.h,
                refined.p.x,
                refined.p.y,
                refined.s.w,
                refined.s.h
        );
        crop = refined;
    }
    return isRefined;
}

MBOOL
CropCalculator::Factor::
isEqual(sp<Factor> pFactor) const
{
    if (pFactor->mEnableEis == MTRUE)
        return MFALSE;

    Factor& rFactor = *pFactor;
    if (mP1SensorCrop == rFactor.mP1SensorCrop &&
        mP1ResizerSize == rFactor.mP1ResizerSize &&
        mP1DmaCrop == rFactor.mP1DmaCrop &&
        mActiveCrop == rFactor.mActiveCrop &&
        mSensorMode == rFactor.mSensorMode)
    {
        return MTRUE;
    }

    return MFALSE;
}

MVOID
CropCalculator::Factor::
dump() const
{
    MY_LOGD("p1 sensro crop(%d,%d,%dx%d), resizer size(%dx%d), crop dma(%d,%d,%dx%d)",
            mP1SensorCrop.p.x,
            mP1SensorCrop.p.y,
            mP1SensorCrop.s.w,
            mP1SensorCrop.s.h,
            mP1ResizerSize.w,
            mP1ResizerSize.h,
            mP1DmaCrop.p.x,
            mP1DmaCrop.p.y,
            mP1DmaCrop.s.w,
            mP1DmaCrop.s.h
    );

    mActive2Sensor.dump("tran active to sensor");

    MY_LOGD("tran sensor to resized o %d, %d, s %dx%d -> %dx%d",
            mSensor2Resizer.tarOrigin.x,
            mSensor2Resizer.tarOrigin.y,
            mSensor2Resizer.oldScale.w,
            mSensor2Resizer.oldScale.h,
            mSensor2Resizer.newScale.w,
            mSensor2Resizer.newScale.h
    );
    MY_LOGD("modified active crop %d, %d, %dx%d",
            mActiveCrop.p.x,
            mActiveCrop.p.y,
            mActiveCrop.s.w,
            mActiveCrop.s.h
    );
    MY_LOGD("isEisOn %d", mEnableEis);
    MY_LOGD("mv in active %d/%d, %d/%d",
            mActiveEisMv.p.x, mActiveEisMv.pf.x,
            mActiveEisMv.p.y, mActiveEisMv.pf.y
    );
    MY_LOGD("mv in sensor %d/%d, %d/%d",
            mSensorEisMv.p.x, mSensorEisMv.pf.x,
            mSensorEisMv.p.y, mSensorEisMv.pf.y
    );
    MY_LOGD("mv in resized %d/%d, %d/%d",
            mResizerEisMv.p.x, mResizerEisMv.pf.x,
            mResizerEisMv.p.y, mResizerEisMv.pf.y
    );
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IBoosterPtr Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
IBoosterPtr
IBooster::createInstance(const std::string& name)
{
    auto getIsForceDisableBoost = []() -> MBOOL
    {
        MINT32 ret = ::property_get_int32("vendor.debug.camera.capture.boost.disable", 0);
        MY_LOGD("vendor.debug.camera.capture.boost.disable = %d", ret);
        return (ret > 0);
    };
    static MBOOL isForceDisable = getIsForceDisableBoost();
    //
    IBooster* pBoost = isForceDisable ? static_cast<IBooster*>(new EmptyBoost(name)) : static_cast<IBooster*>(new CPUBooster(name));
    return IBoosterPtr(pBoost, [](IBooster* p)
        {
            delete p;
        });
}

IBooster::~IBooster()
{

}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  PowerHalWrapper Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PowerHalWrapper*
PowerHalWrapper::
getInstance()
{
    static UniquePtr<PowerHalWrapper> singleton(new PowerHalWrapper(), [](PowerHalWrapper* p)
        {
            delete p;
        });
    return singleton.get();
}

PowerHalWrapper::
PowerHalWrapper()
{
    TRACE_FUNC("ctor:%p", this);
}

PowerHalWrapper::
~PowerHalWrapper()
{
    TRACE_FUNC("dtor:%p", this);
}

PowerHalWrapper::Handle
PowerHalWrapper::
createHandle(const std::string& userName)
{
    AUTO_TIMER("createPowerHalHandle, userName:%s", userName.c_str());

    Handle ret = INVALID_HANDLE;
    {
        std::lock_guard<std::mutex> guard(sLocker);
        //
        const size_t oldCount = sHandles.size();
        if(oldCount == 0) {
            initLock();
        }
        ret = sPowerPtr->scnReg();
        sHandles.push_back(ret);

        const size_t newCount = sHandles.size();
        MY_LOGD("create handle, powerPtr:%p, handle:%d, handleCount:%zu -> %zu",
            sPowerPtr.get(), ret, oldCount, newCount);
    }
    return ret;
}

MVOID
PowerHalWrapper::
destroyhandle(const std::string& userName, Handle handle)
{
    AUTO_TIMER("destroyPowerHalHandle, userName:%s", userName.c_str());

    std::lock_guard<std::mutex> guard(sLocker);
    //
    HandlesItor itor = sHandles.end();
    if(!getIsValidLock(handle, itor)) {
        MY_LOGE("invalid handle, powerPtr:%p, handle:%d", sPowerPtr.get(), handle);
        return;
    }
    //
    const size_t oldCount = sHandles.size();
    sHandles.erase(itor);
    sPowerPtr->scnDisable(handle);
    sPowerPtr->scnUnreg(handle);

    const size_t newCount = sHandles.size();
    MY_LOGD("destory handle, powerPtr:%p, handle:%d, handleCount:%zu -> %zu",
        sPowerPtr.get(), handle, oldCount, newCount);

    if(newCount == 0) {
        uninitLock();
    }
}

MVOID
PowerHalWrapper::
enableBoost(const std::string& userName, Handle handle)
{
    AUTO_TIMER("boostPowerHal, userName:%s", userName.c_str());

    std::lock_guard<std::mutex> guard(sLocker);
    //
    if(sPowerPtr == NULL) {
         MY_LOGW("failed to enable boost, invalid powerPtr");
         return;
    }

    if(!getIsValidLock(handle)) {
        MY_LOGW("invalid handle, powerPtr:%p, handle:%d", sPowerPtr.get(), handle);
        return;
    }
    // frenquency
    const MINT32 clusterCount = sPowerPtr->querySysInfo(MtkQueryCmd::CMD_GET_CLUSTER_NUM, 0);
    for(MINT32 i = 0; i < clusterCount; ++i) {
        // query the maximum frenquency of the current cluster
        MINT32 maxFreq = sPowerPtr->querySysInfo(MtkQueryCmd::CMD_GET_CLUSTER_CPU_FREQ_MAX, i);
        // set both the min and max frequency of current cluster to the maximum frequency
        sPowerPtr->scnConfig(handle, MtkPowerCmd::CMD_SET_CLUSTER_CPU_FREQ_MIN, i, maxFreq, 0, 0);
        sPowerPtr->scnConfig(handle, MtkPowerCmd::CMD_SET_CLUSTER_CPU_FREQ_MAX, i, maxFreq, 0, 0);
        TRACE_FUNC("modify CPU frequency, powerPtr:%p, handle:%d, clusterNum:%d/%d, maxFreq:%d",
            sPowerPtr.get(), handle, i, clusterCount, maxFreq);
    }
    // DRAM
    const MINT32 maxDramOPPLevel = 0;
    sPowerPtr->scnConfig(handle, MtkPowerCmd::CMD_SET_OPP_DDR, maxDramOPPLevel, 0, 0, 0);
    //
    sPowerPtr->scnEnable(handle, TIMEOUT);
    MY_LOGD("enable boost, powerPtr:%p, handle:%d", sPowerPtr.get(), handle);
}

MVOID
PowerHalWrapper::
disableBoost(const std::string& userName, Handle handle)
{
    AUTO_TIMER("disboostPowerHal, userName:%s", userName.c_str());

    std::lock_guard<std::mutex> guard(sLocker);
    //
    if(sPowerPtr == NULL) {
         MY_LOGW("failed to enable boost, invalid powerPtr");
         return;
    }

    if(!getIsValidLock(handle)) {
        MY_LOGW("invalid handle, powerPtr:%p, handle:%d", sPowerPtr.get(), handle);
        return;
    }
    sPowerPtr->scnDisable(handle);
    MY_LOGD("disable boost, powerPtr:%p, handle:%d", sPowerPtr.get(), handle);
}

MVOID
PowerHalWrapper::
initLock()
{
    if((sPowerPtr != NULL) || (sHandles.size() != 0)) {
        MY_LOGE("non-expected value, powerPtr:%p, handleCount:%d", sPowerPtr.get(), sHandles.size());
        dumpLock();
    }
    //
    sPowerPtr = IPower::getService();
    if(sPowerPtr != NULL) {
        MY_LOGD("get powerService, powerPtr:%p", sPowerPtr.get());
    }
    else {
        MY_LOGE("failed to get powerService");
    }
}

MVOID
PowerHalWrapper::
uninitLock()
{
    if((sPowerPtr == NULL) || (sHandles.size() != 0)) {
        MY_LOGE("non-expected value, powerPtr:%p, handleCount:%d", sPowerPtr.get(), sHandles.size());
        dumpLock();
    }
    //
    MY_LOGD("release powerService, powerPtr:%p", sPowerPtr.get());
    sPowerPtr = NULL;
    sHandles.clear();
}

MVOID
PowerHalWrapper::
dumpLock()
{
    const ssize_t handleCount = sHandles.size();
    std::stringstream ss;
    ss << "["
       << "pp: " << sPowerPtr.get() << ", "
       << "hs:[" ;
    const MINT32 lastIndex = sHandles.size() - 1;
    for(MINT32 i = 0; i < sHandles.size(); ++i) {
        ss << i << ": " << sHandles[i];

        if(i != lastIndex)
            ss << ", ";
    }
    ss << "]"
       << "]" << std::endl;
    MY_LOGD("dump info:%s", ss.str().c_str());
}

MBOOL
PowerHalWrapper::
getIsValidLock(Handle handle)
{
    return (std::find(sHandles.begin(), sHandles.end(), handle) != sHandles.end());
}

MBOOL
PowerHalWrapper::
getIsValidLock(Handle handle, HandlesItor& itor)
{
    itor = std::find(sHandles.begin(), sHandles.end(), handle);
    return (itor != sHandles.end());
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CPUBooster Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
EmptyBoost::
EmptyBoost(const std::string& name)
: mName(name)
{
    MY_LOGD("ctor:%p, name:%s", this, mName.c_str());
}

EmptyBoost::
~EmptyBoost()
{
    MY_LOGD("dtor:%p, name:%s", this, mName.c_str());
}

const std::string&
EmptyBoost::
getName()
{
    return mName;
}

MVOID
EmptyBoost::
enable()
{
    MY_LOGD("enable EmptyBoost, addr:%p, name:%s", this, mName.c_str());
}

MVOID
EmptyBoost::
disable()
{
    MY_LOGD("enable EmptyBoost, addr:%p, name:%s", this, mName.c_str());
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  CPUBooster Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
CPUBooster::
CPUBooster(const std::string& name)
: mName(name)
{
    mHandle = PowerHalWrapper::getInstance()->createHandle(getName());
    TRACE_FUNC("ctor:%p, name:%s, hande:%d", this, mName.c_str(), mHandle);
}

CPUBooster::
~CPUBooster()
{
    TRACE_FUNC("dtor:%p, name:%s, hande:%d", this, mName.c_str(), mHandle);
    PowerHalWrapper::getInstance()->destroyhandle(getName(), mHandle);
}

const std::string&
CPUBooster::
getName()
{
    return mName;
}

MVOID
CPUBooster::
enable()
{
    PowerHalWrapper::getInstance()->enableBoost(getName(), mHandle);
}

MVOID
CPUBooster::
disable()
{
    PowerHalWrapper::getInstance()->disableBoost(getName(), mHandle);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  IspProfileManager Implementation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

using IspProfileInfoTable = std::map<IspProfileHint, IspProfileInfo, IspProfileHint::Compare>;
inline static
const IspProfileInfoTable&
getIspProfileInfoTable()
{
    static const IspProfileInfoTable table = []()
    {
        // TODO: complie following code when more than ISP5.0
        #if (0)
            const IspProfileInfoTable ret =
            {
                // BayerBayer
                { {eSAN_Master, eSCT_BayerBayer, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Master, eSCT_BayerBayer, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture_Depth) },
                { {eSAN_Sub_01, eSCT_BayerBayer, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture_Full) },
                { {eSAN_Sub_01, eSCT_BayerBayer, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Sub_01, eSCT_BayerBayer, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture_Depth) },
                // BayerMono
                { {eSAN_Master, eSCT_BayerMono, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Master, eSCT_BayerMono, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture_Depth_toW) },
                { {eSAN_Sub_01, eSCT_BayerMono, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Sub_01, eSCT_BayerMono, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture_Depth) },
            };
        #else
            const IspProfileInfoTable ret =
            {
                // BayerBayer
                { {eSAN_Master, eSCT_BayerBayer, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Master, eSCT_BayerBayer, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Preview) },
                { {eSAN_Sub_01, eSCT_BayerBayer, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Sub_01, eSCT_BayerBayer, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Preview) },
                // BayerMono
                { {eSAN_Master, eSCT_BayerMono, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Master, eSCT_BayerMono, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Preview_toW) },
                { {eSAN_Sub_01, eSCT_BayerMono, eRIT_Imgo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture) },
                { {eSAN_Sub_01, eSCT_BayerMono, eRIT_Rrzo}, MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Preview) },
            };
        #endif
        //
        {
            std::stringstream ss;
            ss << "dualcam ispprofile table" << std::endl;
            for(const auto& item : ret)
            {
                ss << "key:0x" << std::hex << std::setw(10) << std::setfill('0') << item.first.mValue << ", "
                   << "profileName:" << item.second.mName << std::endl;
            }
            MY_LOGD("%s", ss.str().c_str());
        }
        return std::move(ret);
    }();
    return table;
}

const IspProfileInfo&
IspProfileManager::
get(const IspProfileHint& hint)
{
    const auto& table = getIspProfileInfoTable();
    auto found = table.find(hint);
    if(found != table.end())
    {
        MY_LOGD("found ispprofile, hint:%#012x, name:%s", hint.mValue, found->second.mName.c_str());
        return found->second;
    }
    else
    {
        static const IspProfileInfo ret = MAKE_ISP_PROFILE_INFO(EIspProfile_N3D_Capture);
        MY_LOGD("not existing hint and use the default profile. hint:%#012x, name:%s", hint.mValue, ret.mName.c_str());
        return ret;
    }
}


} // NSCapture
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
