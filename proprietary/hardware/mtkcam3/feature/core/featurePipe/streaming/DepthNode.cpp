/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "DepthNode.h"
#include <mtkcam/drv/iopipe/CamIO/IHalCamIO.h>
#include <mtkcam3/feature/eis/eis_hal.h>
#include <mtkcam3/feature/DualCam/FOVHal.h>

#include <mtkcam3/feature/stereo/pipe/IDepthMapPipe.h>
#include <mtkcam3/feature/stereo/pipe/IDepthMapEffectRequest.h>
#include <mtkcam3/feature/stereo/hal/stereo_size_provider.h>

#define PIPE_CLASS_TAG "DepthNode"
#define PIPE_TRACE TRACE_SFP_DEPTH_NODE
#include <featurePipe/core/include/PipeLog.h>

//=======================================================================================
#if SUPPORT_VSDOF
//=======================================================================================
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

// using namespace NSFeaturePipe_DepthMap::IDepthMapEffectRequest;
using NSFeaturePipe_DepthMap::IDepthMapEffectRequest;
using NSCam::NSCamFeature::NSFeaturePipe::FeaturePipeParam;

DepthNode::DepthNode(const char *name)
    : StreamingFeatureNode(name)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);
    TRACE_FUNC_EXIT();
}

DepthNode::~DepthNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL DepthNode::onData(DataID id, const RequestPtr &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;

    switch( id )
    {
    case ID_ROOT_TO_DEPTH:
        mRequests.enque(data);
        ret = MTRUE;
        break;
    default:
        ret = MFALSE;
        break;
    }

    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType DepthNode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo& /*reqInfo*/) const
{
    IOPolicyType policy = IOPOLICY_INOUT;
    return policy;
}

MBOOL DepthNode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    //depthmap pipe section
    std::vector<MUINT32> ids = mPipeUsage.getAllSensorIDs();
    sp<DepthMapPipeSetting> pPipeSetting = new DepthMapPipeSetting();
    pPipeSetting->miSensorIdx_Main1 = ids.at(0);
    pPipeSetting->miSensorIdx_Main2 = ids.at(1);
    // get main1 size by index.
    // main1 size usually put in first.
    pPipeSetting->mszRRZO_Main1 = mPipeUsage.getRrzoSizeByIndex(ids.at(0));
    MY_LOGI("SensorID:Main1(%d), Main2(%d)|"
            " RRZO_Main1 Create Size(%04dx%04d)",
            ids.at(0), ids.at(1),
            pPipeSetting->mszRRZO_Main1.w, pPipeSetting->mszRRZO_Main1.h);
    //pipe option
    sp<DepthMapPipeOption> pPipeOption = new DepthMapPipeOption();
    pPipeOption->mFlowType   = eDEPTH_FLOW_TYPE_QUEUED_DEPTH;
    pPipeOption->mSensorType = (SeneorModuleType)mPipeUsage.getSensorModule();
    if(mPipeUsage.supportDPE() && mPipeUsage.supportBokeh())
    {
        pPipeOption->mFeatureMode = eDEPTHNODE_MODE_VSDOF;
    }
    else if(mPipeUsage.supportDPE())
    {
        pPipeOption->mFeatureMode = eDEPTHNODE_MODE_MTK_UNPROCESS_DEPTH;
    }
    else
    {
        MY_LOGE("not support");
        return false;
    }
    MY_LOGI("RunningMode:%s", pPipeOption->mFeatureMode == eDEPTHNODE_MODE_VSDOF ?
            "ALL TK Flow" : "TK Depth+TP Bokeh");

    pPipeOption->setEnableDepthGenControl(MFALSE, 0);
    mpDepthMapPipe = IDepthMapPipe::createInstance(pPipeSetting, pPipeOption);
    MBOOL bRet = mpDepthMapPipe->init();
    if (!bRet) {
        MY_LOGE("onInit Failure");
        return false;
    }

    // Create Depth or DMBG buffer pool
    if(mPipeUsage.supportBokeh())
    {
        mDMBGImgPoolAllocateNeed = 3;
        MSize dmbgSize = StereoSizeProvider::getInstance()->getBufferSize(
                                                        E_DMBG, eSTEREO_SCENARIO_PREVIEW);
        mDMBGImgPool = ImageBufferPool::create("fpipe.DMBGImg", dmbgSize.w, dmbgSize.h,
                                        eImgFmt_STA_BYTE, ImageBufferPool::USAGE_HW_AND_SW);
    }
    else
    {
        mDepthMapImgPoolAllocateNeed = 3;
        MSize depthSize = StereoSizeProvider::getInstance()->getBufferSize(
                                                    E_DMH, eSTEREO_SCENARIO_PREVIEW);
        mDepthMapImgPool = ImageBufferPool::create("fpipe.DepthMapImg", depthSize.w, depthSize.h,
                                           eImgFmt_Y8, ImageBufferPool::USAGE_HW_AND_SW );
    }
    //
    miLogEnable = ::property_get_bool("vendor.debug.vsdof.tkflow.depthnode", 0);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DepthNode::onUninit()
{
    TRACE_FUNC_ENTER();
    //depthmap pipe section
    if (mpDepthMapPipe != nullptr) {
        if (0 != mvDepthNodePack.size()) {
            MY_LOGE("DepthNodePack should be release before uninit, size:%zu",
                    mvDepthNodePack.size());
            mvDepthNodePack.clear();
        }
        //
        mpDepthMapPipe->uninit();
        delete mpDepthMapPipe;
        mpDepthMapPipe = nullptr;
    }
    //
    IBufferPool::destroy(mDMBGImgPool);
    IBufferPool::destroy(mDepthMapImgPool);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID DepthNode::onFlush()
{
    TRACE_FUNC_ENTER();
    mpDepthMapPipe->flush();
    TRACE_FUNC_EXIT();
}

MBOOL DepthNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    if( mYuvImgPoolAllocateNeed && mYuvImgPool != NULL )
    {
        Timer timer;
        timer.start();
        mYuvImgPool->allocate(mYuvImgPoolAllocateNeed);
        timer.stop();
        MY_LOGD("mDepthYUVImg %s %d buf in %d ms", STR_ALLOCATE, mYuvImgPoolAllocateNeed,
                                                   timer.getElapsed());
    }

    if( mDMBGImgPoolAllocateNeed && mDMBGImgPool != NULL )
    {
        Timer timer;
        timer.start();
        mDMBGImgPool->allocate(mDMBGImgPoolAllocateNeed);
        timer.stop();
        MY_LOGD("mDMBGImg %s %d buf in %d ms", STR_ALLOCATE, mDMBGImgPoolAllocateNeed,
                                               timer.getElapsed());
    }

    if( mDepthMapImgPoolAllocateNeed && mDepthMapImgPool != NULL )
    {
        Timer timer;
        timer.start();
        mDepthMapImgPool->allocate(mDepthMapImgPoolAllocateNeed);
        timer.stop();
        MY_LOGD("mDepthMapImg %s %d buf in %d ms", STR_ALLOCATE, mDepthMapImgPoolAllocateNeed,
                                                   timer.getElapsed());
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DepthNode::onThreadStop()
{
    TRACE_FUNC_ENTER();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL DepthNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr request;

    P2_CAM_TRACE_CALL(TRACE_DEFAULT);

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mRequests.deque(request) )
    {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    }
    else if( request == NULL )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    TRACE_FUNC_ENTER();
    //
    MUINT16 decision = 0;
    request->mTimer.startDepth();
    DepthEnqueData enqueData;
    enqueData.mRequest = request;
    // create Request
    sp<IDepthMapEffectRequest> pDepMapReq = IDepthMapEffectRequest::createInstance(
                                                request->mRequestNo, onPipeReady, this);
    { // Input : RRZO, LCSO, HalIn, AppIn Meta
        const SFPSensorInput &masterSensorIn = request->getSensorInput(request->mMasterID);
        const SFPSensorInput &slaveSensorIn  = request->getSensorInput(request->mSlaveID);
        //
        std::vector<MUINT32> ids = mPipeUsage.getAllSensorIDs();
        //Gather Main1 necessary input data
        decision  = setInputData(ids.at(0), pDepMapReq, masterSensorIn);
        MY_LOGE_IF(decision & 0x1F, "reqID=%d:In Main1  with ERROR index(%#x),Abnormal",
                                    enqueData.mRequest->mRequestNo, decision);
        //Gather Main2 necessary input data
        decision |= setInputData(ids.at(1), pDepMapReq, slaveSensorIn);
        MY_LOGE_IF(decision & 0xE0, "reqID=%d:In Main2  with ERROR index(%#x),Abnormal",
                                    enqueData.mRequest->mRequestNo, decision);
    }

    // Output : CleanYUV, FD, DMBG or DepthMap, App/Hal OutMeta
    SFPIOManager &ioMgr       = request->mSFPIOManager;
    const SFPIOMap &generalIO = ioMgr.getFirstGeneralIO();
    // TODO: Physical YUV
    // const SFPIOMap &masterPhyIO = ioMgr.getPhysicalIO(request->mMasterID);
    // const SFPIOMap &slavePhyIO  = ioMgr.getPhysicalIO(request->mSlaveID);
    SFPOutput out;
    // FD
    if (request->getFDOutput(out)) {
        pDepMapReq->pushRequestImageBuffer(
                    {PBID_OUT_FDIMG , eBUFFER_IOTYPE_OUTPUT}, out.mBuffer);
    }
    if (request->needFullImg(this, request->mMasterID)) {
        enqueData.mOut.mCleanYuvImg.mBuffer = mYuvImgPool->requestIIBuffer(); //CleanYUV
    }
    else if (request->needNextFullImg(this, request->mMasterID))//customize input yuv for next node
    {
        MY_LOGD("Prepare customize input YUV");
        MSize resize(0,0), rrzo_size(0,0);
        enqueData.mOut.mCleanYuvImg.mBuffer =
                        request->requestNextFullImg(this, request->mMasterID, resize);//CleanYUV
        if (resize.w && resize.h) {// if assigned
            enqueData.mOut.mCleanYuvImg.mBuffer->getImageBuffer()->setExtParam(resize);
        } else {//not assign. set output size as RRZO size
            rrzo_size = request->getSensorInput(request->mMasterID).mRRZO->getImgSize();
            MY_LOGD("RRZO wxh(%04dx%04d)",rrzo_size.w, rrzo_size.h);
            if (rrzo_size.w && rrzo_size.h)
                enqueData.mOut.mCleanYuvImg.mBuffer->getImageBuffer()->setExtParam(rrzo_size);
        }
    }
    //
    if (request->hasGeneralOutput())//Gather Output
    {
        decision |= setOutputData(pDepMapReq, enqueData.mOut, generalIO);
        MY_LOGE_IF(decision & 0x1F00,  "reqID=%d:Output with ERROR index(%#x),Abnormal",
                                  enqueData.mRequest->mRequestNo, decision);
    } else
        MY_LOGE("NOT GeneralOut,Error State(%#x)",(decision |= (1<<13)) ); //0x2000, 8192
    //
    if (decision & 0x2F00)
        MY_LOGE("reqID=%d:Abnormal_Output Error state(%#x), !!NOT ENQUE!! ",
                enqueData.mRequest->mRequestNo, decision);
    //
    if (request->hasGeneralOutput() && decision == 0) {
        if (enqueData.mRequest->mRequestNo < 0) {
            MY_LOGE("[DepthNode]reqID=%d released, something wrong !!",
                                        enqueData.mRequest->mRequestNo);
            return MFALSE;
        } else {//hold enqueData members,
                //because its member hold strong pointer of CleanYUV / DMBG / Depth buffers.
            enqueData.mRequest->mTimer.startEnqueDepth();
            this->incExtThreadDependency(); // record one request enque to depth pipe.

            android::Mutex::Autolock lock(mLock);
            MY_LOGD("thread_loop reqID=%d ", enqueData.mRequest->mRequestNo);
            //For DepthNodePack, it consolidates DepthEnqueData & IDepthMapEffectRequest
            DepthNodePackage pack = {
                .depEnquePack  = enqueData,
                .depEffectPack = pDepMapReq,
            };
            mvDepthNodePack.add(enqueData.mRequest->mRequestNo, pack);
        }
        mpDepthMapPipe->enque(pDepMapReq);
    } else { //AbnormalCase
        MY_LOGE("To BokehNode, !!!Bypass DepthMapPipe!!! Something Wrong!");
        enqueData.mOut.mDepthSucess = MFALSE;
        handleData(ID_DEPTH_TO_BOKEH, DepthImgData(enqueData.mOut, request));
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MUINT16 DepthNode::setOutputData(sp<IDepthMapEffectRequest> pDepMapReq,
                                 DepthImg& out, const SFPIOMap &generalIO)
{
    MY_LOGD_IF(miLogEnable > 0, "+");
    MUINT16 decision = 0;

    if (out.mCleanYuvImg.mBuffer.get() != nullptr) {
        sp<IImageBuffer> spCleanYuvImg = out.mCleanYuvImg.mBuffer->getImageBuffer();
        if (!pDepMapReq->pushRequestImageBuffer(
                    {PBID_OUT_MV_F, eBUFFER_IOTYPE_OUTPUT}, spCleanYuvImg))
            decision |= (1<<8);//0x100, 256
    } else
        decision |= (1<<8);//0x100, 256

    if (mPipeUsage.supportBokeh())
    {
        out.mDMBGImg = mDMBGImgPool->requestIIBuffer();//DMBG

        sp<IImageBuffer> spDMBGImg = out.mDMBGImg->getImageBuffer();
        if (!pDepMapReq->pushRequestImageBuffer(
                    {PBID_OUT_DMBG , eBUFFER_IOTYPE_OUTPUT}, spDMBGImg)) {
            decision |= (1<<9);//0x200, 512
        }
    }
    else
    {
        out.mDepthMapImg = mDepthMapImgPool->requestIIBuffer();//DepthMap

        sp<IImageBuffer> spDepthImg = out.mDepthMapImg->getImageBuffer();
        if (!pDepMapReq->pushRequestImageBuffer(
                    {PBID_OUT_DEPTHMAP, eBUFFER_IOTYPE_OUTPUT}, spDepthImg)) {
            decision |= (1<<10);//0x400, 1024
        }
    }
    //MetaData Output
    if (!pDepMapReq->pushRequestMetadata(
                     {PBID_OUT_HAL_META, eBUFFER_IOTYPE_OUTPUT}, generalIO.mHalOut))
    {
        decision |= (1<<11);//0x800, 2048
    }
    //
    if (!pDepMapReq->pushRequestMetadata(
                     {PBID_OUT_APP_META, eBUFFER_IOTYPE_OUTPUT}, generalIO.mAppOut))
    {
        decision |= (1<<12);//0x1000, 4096
    }

    MY_LOGE_IF(decision != 0, "assign outout Data Error, index(%#x)", decision);

    MY_LOGD_IF(miLogEnable > 0, "-");
    return decision;
}

MUINT16 DepthNode::fillIntoDepthMapPipe(sp<IDepthMapEffectRequest> pDepMapReq,
                            vector<inputImgData>& vImgs, vector<inputMetaData>& vMetas)
{
    MUINT8  i;
    MUINT16 decision = 0;
    MY_LOGD_IF(miLogEnable > 0, "+");

    for (i = 0; i < std::min(vImgs.size(), vMetas.size()); i++)
    {
        if (!pDepMapReq->pushRequestImageBuffer(
                    {vImgs[i].param.bufferID, vImgs[i].param.ioType}, vImgs[i].buf))
            decision |= (1<<(2*i));
        else
            MY_LOGD_IF(miLogEnable > 0, "InSertBuf_OK_bID:%u,ioType:%u",
                       vImgs[i].param.bufferID, vImgs[i].param.ioType);
        if (!pDepMapReq->pushRequestMetadata(
                    {vMetas[i].param.bufferID, vMetas[i].param.ioType}, vMetas[i].meta))
            decision |= (1<<(2*i+1));
        else
            MY_LOGD_IF(miLogEnable > 0, "InSertMeta_OK_bID:%u,ioType:%u",
                       vMetas[i].param.bufferID, vMetas[i].param.ioType);
    }
    MY_LOGD_IF(miLogEnable > 0,"%d", i);
    //
    if (vImgs.size() < vMetas.size()) {//Main1
        if (!pDepMapReq->pushRequestMetadata(
                    {vMetas[i].param.bufferID, vMetas[i].param.ioType}, vMetas[i].meta))
            decision |= (1<<(2*i));
        else
            MY_LOGD_IF(miLogEnable > 0, "InSertMeta_OK_bID:%u, ioType:%u",
                       vMetas[i].param.bufferID, vMetas[i].param.ioType);
    } else {                               //Main2
        if (!pDepMapReq->pushRequestImageBuffer(
                    {vImgs[i].param.bufferID, vImgs[i].param.ioType}, vImgs[i].buf))
            decision |= (1<<(2*i));
        else
            MY_LOGD_IF(miLogEnable > 0, "InSertBuf bID_OK_%u, ioType:%u",
                       vImgs[i].param.bufferID, vImgs[i].param.ioType);
    }

    MY_LOGD_IF(miLogEnable > 0, "-");
    return decision;
}

MUINT16 DepthNode::setInputData(MUINT32 sensorID,
        sp<IDepthMapEffectRequest> pDepMapReq, const SFPSensorInput &data)
{
    MY_LOGD_IF(miLogEnable > 0, "+   ID:%d", sensorID);
    MUINT16 decision = 0, shift = 5;

    if (sensorID == 0) {        //Main1
        vector<inputImgData>  vImgs{
                {{PBID_IN_RSRAW1, eBUFFER_IOTYPE_INPUT}, data.mRRZO},//0x01
                {{PBID_IN_LCSO1 , eBUFFER_IOTYPE_INPUT}, data.mLCSO},//0x04
        };
        vector<inputMetaData> vMetas{
                {{PBID_IN_APP_META      , eBUFFER_IOTYPE_INPUT}, data.mAppIn}       , //0x02
                {{PBID_IN_HAL_META_MAIN1, eBUFFER_IOTYPE_INPUT}, data.mHalIn}       , //0x08
                {{PBID_IN_P1_RETURN_META, eBUFFER_IOTYPE_INPUT}, data.mAppDynamicIn}, //0x10
        };
        MY_LOGD_IF(miLogEnable > 0,"%s:v_Size:%zu, v_Size:%zu",
                   (sensorID == 0 ? "Main1" : "Main2"), vImgs.size(), vMetas.size());
        decision = fillIntoDepthMapPipe(pDepMapReq, vImgs, vMetas);

    } else {                    //Main2
        vector<inputImgData>  vImgs {
                {{PBID_IN_RSRAW2, eBUFFER_IOTYPE_INPUT}, data.mRRZO}, //0x20
                {{PBID_IN_LCSO2 , eBUFFER_IOTYPE_INPUT}, data.mLCSO}, //0x40
        };
        vector<inputMetaData> vMetas {
                {{PBID_IN_HAL_META_MAIN2, eBUFFER_IOTYPE_INPUT}, data.mHalIn},//0x80
        };
        MY_LOGD_IF(miLogEnable > 0,"%s:v_Size:%zu, v_Size:%zu ",
                   sensorID == 0 ? "Main1" : "Main2", vImgs.size(), vMetas.size());
        decision = fillIntoDepthMapPipe(pDepMapReq, vImgs, vMetas);
    }

    MY_LOGE_IF(decision != 0, "assign InMain(%d) Data Error, index(%#x)",
               (sensorID == 0 ? 1: 2), (sensorID != 0 ? (decision << shift): decision));

    MY_LOGD_IF(miLogEnable > 0, "-");
    return decision;
}

MVOID DepthNode::setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mYuvImgPool = pool;
    mYuvImgPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

void DepthNode::updateMetadata(const SFPSensorInput& input, const SFPIOMap& output)
{
    *(output.mHalOut)  += *(input.mHalIn);

    MY_LOGD("_inHal:%d, _outHal:%d", input.mHalIn->count(), output.mHalOut->count());
}

MVOID DepthNode::handleResultData(const RequestPtr &request, const DepthEnqueData &data)
{
    TRACE_FUNC_ENTER();

    if(mPipeUsage.supportBokeh())
    {
        handleData(ID_DEPTH_TO_BOKEH, DepthImgData(data.mOut, request));
    }
    else
    {
        updateMetadata(request->getSensorInput(request->mMasterID),
                        request->mSFPIOManager.getFirstGeneralIO());

        handleData(ID_DEPTH_TO_VENDOR, DepthImgData(data.mOut, request));
    }
    //
    if (request->needDump()) {
        if (data.mOut.mCleanYuvImg.mBuffer != nullptr) {
            data.mOut.mCleanYuvImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mOut.mCleanYuvImg.mBuffer->getImageBufferPtr(),
                    "DepthNode_yuv");
        }
        if (mPipeUsage.supportDPE()) {
            if (data.mOut.mDepthMapImg != nullptr) {
                data.mOut.mDepthMapImg->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
                dumpData(data.mRequest, data.mOut.mDepthMapImg->getImageBufferPtr(),
                        "DepthNode_depthmap");
            }
        } else if(mPipeUsage.supportBokeh()) {
            if (data.mOut.mDMBGImg != nullptr) {
                data.mOut.mDMBGImg->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
                dumpData(data.mRequest, data.mOut.mDMBGImg->getImageBufferPtr(),
                        "DepthNode_blurmap");
            }
        }
    }
    TRACE_FUNC_EXIT();
}

MVOID DepthNode::onPipeReady(MVOID* tag, NSDualFeature::ResultState state,
                                            sp<IDualFeatureRequest>& request)
{
    TRACE_FUNC_ENTER();
    DepthEnqueData   enqueData;
    DepthNodePackage depthNodePack;
    DepthNode*       pDepthNode = (DepthNode*)tag;
    sp<IDepthMapEffectRequest> pDepReq = (IDepthMapEffectRequest*) request.get();

    TRACE_FUNC("DepthPipeItem reqID=%d state=%d:%s",
               pDepReq->getRequestNo(), state, ResultState2Name(state));
    // if complete/not_ready -> enue to next item
    if (state == eRESULT_COMPLETE || state == eRESULT_DEPTH_NOT_READY || state == eRESULT_FLUSH)
    {
        ssize_t idx = -1;
        {
            idx = pDepthNode->mvDepthNodePack.indexOfKey(pDepReq->getRequestNo());
            if (idx < 0) {
                MY_LOGE("[DepthNode]reqID=%zu is missing, might released!!",idx);
                return;
            }
            android::Mutex::Autolock lock(pDepthNode->mLock);
            depthNodePack = pDepthNode->mvDepthNodePack.valueAt(idx);
            enqueData     = depthNodePack.depEnquePack;
            //
            enqueData.mOut.mDepthSucess = MTRUE;
            pDepthNode->handleResultData(enqueData.mRequest, enqueData);// send data to next nodes

            enqueData.mRequest->mTimer.stopEnqueDepth();
            pDepthNode->decExtThreadDependency(); // tell one request call back
        }

        {
            android::Mutex::Autolock lock(pDepthNode->mLock);
            MY_LOGD("Remove reqID=%d, mvDepEffReq size=%zu",
                    pDepReq->getRequestNo(), pDepthNode->mvDepthNodePack.size());
            pDepthNode->mvDepthNodePack.removeItem(pDepReq->getRequestNo());
        }
        //
        enqueData.mRequest->mTimer.stopDepth();
    }

    TRACE_FUNC_EXIT();
}
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

//=======================================================================================
#else //SUPPORT_VSDO
//=======================================================================================

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
//=======================================================================================
#endif //SUPPORT_VSDOF
//=======================================================================================

