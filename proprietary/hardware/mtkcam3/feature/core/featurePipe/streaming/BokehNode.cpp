
/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "BokehNode.h"

#include <mtkcam3/feature/stereo/hal/StereoArea.h>
#include <mtkcam3/feature/stereo/hal/stereo_size_provider.h>
#include <mtkcam/utils/metadata/IMetadata.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <isp_tuning.h>

#include <stereo_tuning_provider.h>

#define PIPE_CLASS_TAG "BokehNode"
#define PIPE_TRACE TRACE_SFP_BOKEH_NODE
#include <featurePipe/core/include/PipeLog.h>

#include <mtkcam3/feature/utils/p2/P2Util.h>
#include "P2CamContext.h"

#define BOKEH_NORMAL_STREAM_NAME "SFP_Bokeh"

#define FUNCTION_IN   MY_LOGD("%s +", __FUNCTION__)
#define FUNCTION_OUT  MY_LOGD("%s -", __FUNCTION__)
#define PIPE_LOG_TAG "BokehNode"

using namespace NSCam::NSIoPipe;
using namespace NSCam::NSIoPipe::NSPostProc;
using namespace NSIspTuning;
using VSDOF::util::sMDP_Config;

class DpBlitStream;
//=======================================================================================
#if SUPPORT_VSDOF
//=======================================================================================
namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

BokehNode::BokehNode(const char *name)
    : StreamingFeatureNode(name)
    , mNormalStream(NULL)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mDepthDatas);
    TRACE_FUNC_EXIT();
}

BokehNode::~BokehNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MBOOL BokehNode::onData(DataID id, const DepthImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;

    switch( id )
    {
    case ID_DEPTH_TO_BOKEH:
        mDepthDatas.enque(data);
        ret = MTRUE;
        break;
    default:
        ret = MFALSE;
        break;
    }

    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType BokehNode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    return reqInfo.isMaster() ? IOPOLICY_INOUT : IOPOLICY_BYPASS;
}


MBOOL BokehNode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();

    mNormalStream =  NSIoPipe::NSPostProc::INormalStream::createInstance(mSensorIndex);
    if( mNormalStream == NULL || !mNormalStream->init(BOKEH_NORMAL_STREAM_NAME))
    {
        MY_LOGE("BokehNode Init NormalStream Failed ! NoramlStream(%p)", mNormalStream);
    }

    mDynamicTuningPool = TuningBufferPool::create("bokeh.p2btuningBuf",
                                NSIoPipe::NSPostProc::INormalStream::getRegTableSize(),
                                MTRUE/*Use Buf Protect*/);

    mbShowDepthMap  = ::property_get_bool("vendor.debug.tkflow.bokeh.showdepth", 0);
    mbDumpImgeBuf   = ::property_get_bool("vendor.debug.tkflow.bokeh.dump_img", 0);
    mbPipeLogEnable = ::property_get_bool("vendor.debug.tkflow.bokeh.log", 0);

    extract_by_SensorOpenId(&mDumpHint, mSensorIndex);//Need to confirm with Jou-Feng or Ray

    MY_LOGD("openid(%#x), ShowDepthMap(%d), DumpImgBuf(%d)",
            mSensorIndex, mbShowDepthMap, mbDumpImgeBuf);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::onUninit()
{
    TRACE_FUNC_ENTER();
    if( mNormalStream )
    {
        mNormalStream->uninit(BOKEH_NORMAL_STREAM_NAME);
        mNormalStream->destroyInstance();
        mNormalStream = NULL;
    }
    TuningBufferPool::destroy(mDynamicTuningPool);

    if (mpDpStream != nullptr)
        delete mpDpStream;
    // 3A
#ifdef SUPPORT_SET_ISPPROFILE
    if (mp3AHal_Main1) {
        mp3AHal_Main1->destroyInstance("BOKEH_3A_MAIN1");
        mp3AHal_Main1 = NULL;
    }
#endif
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    if( mFullImgPoolAllocateNeed && mFullImgPool != NULL )
    {
        Timer timer;
        timer.start();
        mFullImgPool->allocate(mFullImgPoolAllocateNeed);
        timer.stop();
        MY_LOGD("Bokeh FullImg %s %d buf in %d ms", STR_ALLOCATE, mFullImgPoolAllocateNeed,
                timer.getElapsed());
    }

    //Set depi size
    miDmgi = StereoSizeProvider::getInstance()->getBufferSize(
                                                E_DMG, eSTEREO_SCENARIO_PREVIEW).size;
    MY_LOGD("DMGI size(%dx%d) ", miDmgi.w, miDmgi.h);
    //
    if (mDynamicTuningPool != NULL )
    {
        Timer timer(MTRUE);
        mDynamicTuningPool->allocate(mPipeUsage.getNumP2ATuning());
        timer.stop();
        MY_LOGD("Dynamic Tuning %s %d bufs in %d ms",
                STR_ALLOCATE, mPipeUsage.getNumP2ATuning(), timer.getElapsed());
    }
    //
    if (mbShowDepthMap) {
        const MINT32 usage = ImageBufferPool::USAGE_HW;
        createBufferPool(mpDepthMapBufPool, 720, 408, eImgFmt_Y8, 2,
                        "BOKEH_P2B_DEPTHMAP_BUF", usage, MTRUE);
        mpDepthMapBufPool->allocate(2);
        mpDpStream = new DpBlitStream();
    }
    //
#ifdef SUPPORT_SET_ISPPROFILE
        mp3AHal_Main1 = MAKE_Hal3A(mSensorIndex, "BOKEH_3A_MAIN1");
        MY_LOGD("3A crate Instance, Main1:%p", mp3AHal_Main1);
#endif

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    this->waitNormalStreamBaseDone();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    DepthImgData depthData;

    P2_CAM_TRACE_CALL(TRACE_DEFAULT);

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mDepthDatas.deque(depthData) )
    {
        MY_LOGE("DepthImgData deque out of sync");
        return MFALSE;
    }
    if( depthData.mRequest == NULL )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    TRACE_FUNC_ENTER();

    depthData.mRequest->mTimer.startBokeh();
    BokehEnqueData enqueData;
    QParams enqueParams;
    // Check Bokeh Input & hold strong reference
    MY_LOGD("BokehNode: reqID=%d ", depthData.mRequest->mRequestNo);
    //
    enqueData.mRequest  = depthData.mRequest;
    enqueData.mInYuvImg = depthData.mData.mCleanYuvImg;
    enqueData.mDMBG     = depthData.mData.mDMBGImg;
    //
    if (enqueData.mRequest->hasGeneralOutput() &&
        depthData.mData.mDepthSucess == MTRUE)//NonmalCase
    {
        //Gather Input Buffer
        if (depthData.mData.mCleanYuvImg.mBuffer == NULL) {
            MY_LOGE("Bokeh input Master YUV is NULL!  Can not enque P2!");
            return MFALSE;
        }
        if(depthData.mData.mDMBGImg == NULL) {
            MY_LOGE("Bokeh input DMBG is NULL!  Can not enque P2!");
            return MFALSE;
        }
        // Prepare QParam for P2
        enqueParams.mvFrameParams.push_back(FrameParams());
        enqueParams.mpCookie = (void*)&enqueData;

        FrameParams& mainFrame = enqueParams.mvFrameParams.editItemAt(0);
        prepareBokehInput(mainFrame, enqueData.mRequest, enqueData);
        prepareBokehOutputs(mainFrame, enqueData.mRequest, enqueData);

        //debugQParams(enqueParams);

        enqueFeatureStream(enqueParams, enqueData);
    }
    else
    { // AbnormalCase
        enqueParams.mDequeSuccess = MFALSE;
        depthData.mRequest->updateResult(enqueParams.mDequeSuccess);
        //
        MY_LOGE("reqID=%d:Abnormal Return Buffer Result:%d",
                enqueData.mRequest->mRequestNo, enqueParams.mDequeSuccess);
        handleResultData(enqueData.mRequest, enqueData);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID BokehNode::setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mFullImgPool = pool;
    mFullImgPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MBOOL BokehNode::prepareBokehInput(FrameParams &framePa, const RequestPtr &request,
                                                               BokehEnqueData &data)
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    TRACE_FUNC_ENTER();
    Feature::P2Util::push_in(framePa, NSCam::NSIoPipe::PORT_IMGI,
                                data.mInYuvImg.mBuffer->getImageBufferPtr());

    Feature::P2Util::push_in(framePa, NSCam::NSIoPipe::PORT_DMGI,
                                data.mDMBG->getImageBufferPtr());
    MBOOL bRet = true;

    MSize imgInSize = data.mInYuvImg.mBuffer->getImageBufferPtr()->getImgSize();

    bRet &= setSRZInfo(framePa, EDipModule_SRZ3, miDmgi, imgInSize);
    if (!bRet)
        MY_LOGE("setSRZInfo SRZ3 fail");

    SmartTuningBuffer tuningBuf = mDynamicTuningPool->request();
    memset(tuningBuf->mpVA, 0, mDynamicTuningPool->getBufSize());
    if (!StereoTuningProvider::getBokehTuningInfo(tuningBuf->mpVA)) {
        MY_LOGE("set Tuning Parameter Fail!");
        return MFALSE;
    }

#ifdef SUPPORT_SET_ISPPROFILE
        MY_LOGD("applyIspTuning section");
        const SFPSensorInput &masterIn = request->getSensorInput(request->mMasterID);
        _bokehNode_ispTuningConfig_ ispConfig = {
            masterIn.mAppIn, masterIn.mHalIn, mp3AHal_Main1, MFALSE,
            static_cast<MINT32>(request->mRequestNo)
        };
        trySetMetadata<MUINT8>(masterIn.mHalIn, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_Bokeh);
        trySetMetadata<MINT32>(masterIn.mHalIn, MTK_3A_ISP_BYPASS_LCE, true);
        TuningParam rTuningParam = applyISPTuning(PIPE_LOG_TAG, tuningBuf, ispConfig, MTRUE);
#endif

    framePa.mStreamTag  = ENormalStreamTag_Bokeh;
    framePa.mTuningData = tuningBuf->mpVA;

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::prepareBokehOutputs(FrameParams &framePa, const RequestPtr &request,
                                     BokehEnqueData &bokeh_data)
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    TRACE_FUNC_ENTER();
    //
    MUINT32 sID = request->mMasterID;
    NSCam::NSIoPipe::Output pipeOutput = {0};
    FrameInInfo inInfo;
    getFrameInInfo(inInfo, framePa);
    MSize imgiSize = inInfo.inSize;

    MCrpRsInfo mdp_crop = {}, crz_crop = {};

    SFPOutput output;
    std::vector<SFPOutput> outs;
    outs.reserve(5);
    // Internal Buffer has highest prority, need to push back first.
    if( request->needNextFullImg(this, sID) )
    {
        SFPOutput out;
        prepareNextFullSFPOut(out, request, bokeh_data, inInfo);
        outs.push_back(out);
    }

    if (request->needDisplayOutput(this) && request->getDisplayOutput(output))
    {
        pipeOutput.mPortID = PORT_WDMAO;
        pipeOutput.mBuffer = output.mBuffer;

        // MDP
        mdp_crop.mGroupID   = 2;
        mdp_crop.mCropRect  = MCropRect(imgiSize.w, imgiSize.h);
        mdp_crop.mResizeDst = imgiSize;
        // CRZ
        crz_crop.mGroupID   = 1;
        crz_crop.mCropRect  = MCropRect(imgiSize.w, imgiSize.h);
        crz_crop.mResizeDst = imgiSize;

        framePa.mvCropRsInfo.push_back(mdp_crop);
        framePa.mvCropRsInfo.push_back(crz_crop);
        framePa.mvOut.push_back(pipeOutput);

        outs.push_back(output);
    }

    if (request->needRecordOutput(this) && request->getRecordOutput(output))
    {
        pipeOutput.mPortID = PORT_WROTO;
        pipeOutput.mBuffer = output.mBuffer;

        // MDP
        mdp_crop.mGroupID   = 3,
        mdp_crop.mCropRect  = MCropRect(imgiSize.w, imgiSize.h),
        mdp_crop.mResizeDst = imgiSize,
        // CRZ
        crz_crop.mGroupID   = 1,
        crz_crop.mCropRect  = MCropRect(imgiSize.w, imgiSize.h),
        crz_crop.mResizeDst = imgiSize,

        framePa.mvCropRsInfo.push_back(mdp_crop);
        framePa.mvCropRsInfo.push_back(crz_crop);
        framePa.mvOut.push_back(pipeOutput);

        outs.push_back(output);
    }
    if (mbPipeLogEnable &&
        (request->needDisplayOutput(this) || request->needRecordOutput(this))) {
        MY_LOGD("IMGI size(%dx%d)"
                "P2B MDP setting: GroupId(%d) CropRect(%dx%d)"
                "P2B CRZ setting: GroupId(%d) CropRect(%dx%d)",
                imgiSize.w, imgiSize.h,
                mdp_crop.mGroupID, mdp_crop.mCropRect.s.w, mdp_crop.mCropRect.s.h,
                crz_crop.mGroupID, crz_crop.mCropRect.s.w, crz_crop.mCropRect.s.h);
    }

    if (request->needExtraOutput(this))
    {
        MY_LOGE("Not Support");
    }

    if (!bokeh_data.mRemainingOutputs.empty())
        MY_LOGE("Not Support Multiple output : RemainSize:%zu",
                bokeh_data.mRemainingOutputs.size());

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL BokehNode::needFullForExtraOut(std::vector<SFPOutput> &outs)
{
    static const MUINT32 maxMDPOut = 2;
    if(outs.size() > maxMDPOut)
        return MTRUE;
    MUINT32 rotCnt = 0;
    for (auto&& out : outs)
    {
        if (out.mTransform != 0)
        {
            ++rotCnt;
        }
    }
    return (rotCnt > 1);
}

MVOID BokehNode::prepareNextFullSFPOut(SFPOutput &output, const RequestPtr &request,
                                        BokehEnqueData &data, const FrameInInfo &inInfo)
{
    TRACE_FUNC_ENTER();
    MSize outSize, resize;

    sp<IImageBuffer> pInBuffer = data.mInYuvImg.mBuffer->getImageBuffer();
    outSize = pInBuffer->getImgSize();

    data.mOutNextFullImg.mBuffer = request->requestNextFullImg(this, request->mMasterID, resize);
    if( resize.w && resize.h )
    {
        outSize = resize;
    }
    TRACE_FUNC(" Frame %d QFullImg %s", request->mRequestNo,
                data.mOutNextFullImg.mBuffer == NULL ? "is null" : "" );

    data.mOutNextFullImg.mDomainOffset = data.mInYuvImg.mDomainOffset;
    data.mOutNextFullImg.mBuffer->getImageBuffer()->setTimestamp(inInfo.timestamp);
    data.mOutNextFullImg.mBuffer->getImageBuffer()->setExtParam(outSize);

    output.mBuffer      = data.mOutNextFullImg.mBuffer->getImageBufferPtr();
    output.mTransform   = 0;
    output.mCropRect    = MRect(MPoint(0,0), pInBuffer->getImgSize());
    output.mCropDstSize = outSize;

    TRACE_FUNC_EXIT();
}

MVOID BokehNode::prepareFullImg(FrameParams &frame, const RequestPtr &request,
                                BokehEnqueData &data, const FrameInInfo &inInfo)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d FullImgPool=(%d/%d)", request->mRequestNo,
                    mFullImgPool->peakAvailableSize(), mFullImgPool->peakPoolSize());

    data.mOutFullImg.mBuffer    = mFullImgPool->requestIIBuffer();
    sp<IImageBuffer> pOutBuffer = data.mOutFullImg.mBuffer->getImageBuffer();
    sp<IImageBuffer> pInBuffer  = data.mInYuvImg.mBuffer->getImageBuffer();

    data.mOutFullImg.mDomainOffset = data.mInYuvImg.mDomainOffset;
    pOutBuffer->setTimestamp(inInfo.timestamp);
    if (!pOutBuffer->setExtParam(pInBuffer->getImgSize()))
    {
        MY_LOGE("Full Img setExtParm Fail!, target size(%04dx%04d)",
                pInBuffer->getImgSize().w, pInBuffer->getImgSize().h);
    }

    Output output;
    output.mPortID = PortID(EPortType_Memory,  NSImageio::NSIspio::EPortIndex_IMG3O, PORTID_OUT);
    output.mBuffer = pOutBuffer.get();
    frame.mvOut.push_back(output);
    TRACE_FUNC_EXIT();
}

MVOID BokehNode::enqueFeatureStream(NSCam::NSIoPipe::QParams &params, BokehEnqueData &data)
{
    TRACE_FUNC_ENTER();
    MY_LOGD("sensor(%d) Frame %d enque start", mSensorIndex, data.mRequest->mRequestNo);
    data.mRequest->mTimer.startEnqueBokeh();
    this->incExtThreadDependency();
    this->enqueNormalStreamBase(mNormalStream, params, data);
    TRACE_FUNC_EXIT();
}

MVOID BokehNode::onNormalStreamBaseCB(const QParams &params, const BokehEnqueData &data)
{
    // This function is not thread safe,
    // avoid accessing BokehNode class members
    TRACE_FUNC_ENTER();

    RequestPtr request = data.mRequest;
    if ( request == NULL )
    {
        MY_LOGE("Missing request");
    }
    else
    {
        this->decExtThreadDependency();

        if ( !params.mDequeSuccess )
            MY_LOGW("Frame %d enque result failed", request->mRequestNo);

        request->updateResult(params.mDequeSuccess);
        handleResultData(request, data);

        request->mTimer.stopEnqueBokeh();
        MY_LOGD("sensor(%d) Frame %d enque done in %d ms, result = %d",
                mSensorIndex, request->mRequestNo, request->mTimer.getElapsedEnqueBokeh(),
                params.mDequeSuccess);

        if (mbDumpImgeBuf || mbShowDepthMap)
            dumpBuff(data.mRequest, params);

        // release malloc buffer
        for (const auto& frameParam : params.mvFrameParams) {
            for (const auto& moduleInfo : frameParam.mvModuleData) {
                if( moduleInfo.moduleTag == EDipModule_SRZ4) {
                    if (moduleInfo.moduleStruct != nullptr)
                        free(moduleInfo.moduleStruct);
                }
            }
        }
        request->mTimer.stopBokeh();
    }

    TRACE_FUNC_EXIT();
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID BokehNode::handleResultData(const RequestPtr &request, const BokehEnqueData &data)
{
    // This function is not thread safe,
    // because it is called by onQParamsCB,
    // avoid accessing BokehNode class members
    TRACE_FUNC_ENTER();
    BasicImg full;
    if ( data.mOutNextFullImg.mBuffer != NULL )
    {
        full = data.mOutNextFullImg;
    }
    else
    {
        full = data.mOutFullImg;
    }

    if ( mPipeUsage.supportDummy() )
    {
        handleData(ID_PREV_TO_DUMMY_FULLIMG, BasicImgData(full, request));
    }
    else if( mPipeUsage.supportVendor() && request->needVendor()) //ex:Face Beauty
    {
        handleData(ID_BOKEH_TO_VENDOR_FULLIMG, BasicImgData(full, request));
    }
#if 0
    else if( mPipeUsage.supportWarpNode() )
    {
        handleData(ID_P2A_TO_WARP_FULLIMG, BasicImgData(full, request));
    }
#endif
    else //To DISPLAY Preview
    {
        handleData(ID_BOKEH_TO_HELPER, HelperData(FeaturePipeParam::MSG_FRAME_DONE, request));
    }

    if( request->needP2AEarlyDisplay() ) //For eis 3.0
    {
        handleData(ID_BOKEH_TO_HELPER, HelperData(FeaturePipeParam::MSG_DISPLAY_DONE, request));
    }

    if( request->needDump() )
    {
        if( data.mOutFullImg.mBuffer != NULL )
        {
            data.mOutFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mOutFullImg.mBuffer->getImageBufferPtr(),
                     "BokehNode_full");
        }
        if( data.mOutNextFullImg.mBuffer != NULL )
        {
            data.mOutNextFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mOutNextFullImg.mBuffer->getImageBufferPtr(),
                     "BokehNode_nextfull");
        }
    }
    TRACE_FUNC_EXIT();
}

/*******************************************************************************
 *
 ********************************************************************************/
MUINT32 BokehNode:: mapToBufferID( NSCam::NSIoPipe::PortID const portId,
                                    const MUINT32 scenarioID)
{
    if (portId == PORT_IMGI)
        return BOKEH_ER_BUF_MAIN1;
    if (portId == PORT_DMGI)
        return BOKEH_ER_BUF_DMG;
    if (portId == PORT_DEPI)
        return BOKEH_ER_BUF_DMBG;
    if (portId == PORT_IMG3O)
        return BOKEH_ER_BUF_3DNR_OUTPUT;

    if (portId == PORT_WROTO) {
        if(scenarioID == eSTEREO_SCENARIO_CAPTURE)
            return BOKEH_ER_BUF_VSDOF_IMG;
        else if(scenarioID == eSTEREO_SCENARIO_RECORD)
            return BOKEH_ER_BUF_RECORD;
    }
    if (portId == PORT_WDMAO) {
        if (scenarioID == eSTEREO_SCENARIO_CAPTURE)
            return BOKEH_ER_BUF_WDMAIMG;
        if (scenarioID == eSTEREO_SCENARIO_RECORD ||
            scenarioID == eSTEREO_SCENARIO_PREVIEW)
            return BOKEH_ER_BUF_DISPLAY;
    }
    return BOKEH_ER_BUF_START;
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL BokehNode::setSRZInfo(FrameParams& frameParam, MINT32 modulTag,
                            MSize inputSize, MSize outputSize)
{
    CAM_TRACE_NAME("BokehNode::setSRZInfo");
    // set SRZ 3
    _SRZ_SIZE_INFO_ *srzInfo = (_SRZ_SIZE_INFO_ *)malloc(sizeof(_SRZ_SIZE_INFO_));
    srzInfo->in_w        = inputSize.w;
    srzInfo->in_h        = inputSize.h;
    srzInfo->crop_w      = inputSize.w;
    srzInfo->crop_h      = inputSize.h;
    srzInfo->crop_x      = 0;
    srzInfo->crop_y      = 0;
    srzInfo->out_w       = outputSize.w;
    srzInfo->out_h       = outputSize.h;
    srzInfo->crop_floatX = 0;
    srzInfo->crop_floatY = 0;
    //
    ModuleInfo moduleInfo;
    moduleInfo.moduleTag    = modulTag;
    moduleInfo.frameGroup   = 0;
    moduleInfo.moduleStruct = srzInfo;
    //
    MY_LOGD_IF(mbPipeLogEnable, "srz moduleTag (%d) in(%04dx%04d) out(%04dx%04d)",
               modulTag, srzInfo->in_w, srzInfo->in_h, srzInfo->out_w, srzInfo->out_h);

    frameParam.mvModuleData.push_back(moduleInfo);
    return MTRUE;
}

/*******************************************************************************
 *
 ********************************************************************************/
MVOID BokehNode::dumpBuff(RequestPtr reqPtr, const QParams& rParams)
{
    MINT32 reqID = reqPtr->mRequestNo;
    if (rParams.mvFrameParams.size()==0) {
        MY_LOGE("rParams.mvFrameParams is zero. should not happened");
        return;
    }
    //
    for (MUINT32 i = 0; i < rParams.mvFrameParams.size(); ++i) {
        MY_LOGD_IF(mbPipeLogEnable, "reqID(%d) in(%zu) out(%zu)", reqID,
                   rParams.mvFrameParams[i].mvIn.size(),
                   rParams.mvFrameParams[i].mvOut.size());
    }
    //
    if (mbShowDepthMap)
    {
        MY_LOGD("ShowDepthMap (%d) +", reqID);
        IImageBuffer *input = nullptr, *output = nullptr;

        for (const auto& frameParam : rParams.mvFrameParams)
        {
            for (const auto& inParam : frameParam.mvIn)
            {
                NSCam::NSIoPipe::PortID portId = inParam.mPortID;
                MUINT32 bufDataType = mapToBufferID(portId, eSTEREO_SCENARIO_PREVIEW);
                if (bufDataType == BOKEH_ER_BUF_DMG) {
                    input = inParam.mBuffer;
                    break;
                }
            }
        }

        for (const auto& frameParam : rParams.mvFrameParams)
        {
            for (const auto& outParam : frameParam.mvOut)
            {
                NSCam::NSIoPipe::PortID portId = outParam.mPortID;
                MUINT32 bufDataType = mapToBufferID(portId, eSTEREO_SCENARIO_PREVIEW);
                if (bufDataType == BOKEH_ER_BUF_DISPLAY) {
                    output = outParam.mBuffer;
                    break;
                }
            }
            // mark cache to invalid, ensure get buffer from memory.
            if (input != nullptr) {
                input->syncCache(eCACHECTRL_INVALID);
                shiftDepthMapValue(input, 2);
                input->syncCache(eCACHECTRL_FLUSH);
                outputDepthMap( input, output);
            } else {
                MY_LOGE("Passing null pointer input to syncCache");
            }
        }
        MY_LOGD("ShowDepthMap -");
    }

    if (mbDumpImgeBuf) // dump out port
    {
        MY_LOGD("Dump image(%d) +", reqID);
        const SFPSensorInput &masterSensorIn = reqPtr->getSensorInput(reqPtr->mMasterID);
        IMetadata* pMeta_InHal = masterSensorIn.mHalIn;
        //
        const size_t PATH_SIZE = 1024;
        char writepath[PATH_SIZE] = {0};
        extract(&mDumpHint, pMeta_InHal);

        for (const auto& frameParam : rParams.mvFrameParams)
        {
            for (const auto& outParam : frameParam.mvOut)
            {
                NSCam::NSIoPipe::PortID portId = outParam.mPortID;
                IImageBuffer* buf = outParam.mBuffer;
                if (buf != nullptr) {
                    extract(&mDumpHint, buf);
                    if (portId == PORT_WDMAO) {
                        genFileName_YUV(writepath, PATH_SIZE, &mDumpHint,
                                TuningUtils::YUV_PORT_WDMAO, "P2B_DISPLAY");
                    } else if (portId == PORT_WROTO) {
                        genFileName_YUV(writepath, PATH_SIZE, &mDumpHint,
                                TuningUtils::YUV_PORT_WROTO, "P2B_VSDOF");
                    }
                    buf->saveToFile(writepath);
                } else {
                    MY_LOGE("outParam.mBuffer is NULL, something wrong");
                }
            }

            for (const auto& inParam : frameParam.mvIn)
            {
                NSCam::NSIoPipe::PortID portId = inParam.mPortID;
                if (portId == PORT_DMGI) {
                    IImageBuffer* buf = inParam.mBuffer;
                    if (buf != nullptr) {
                        extract(&mDumpHint, buf);
                        genFileName_YUV(writepath, PATH_SIZE, &mDumpHint,
                                        TuningUtils::YUV_PORT_UNDEFINED, "P2B_DMBG");
                        buf->saveToFile(writepath);
                    } else {
                        MY_LOGE("inParam.mBuffer is NULL, something wrong");
                    }
                }
            }
        }
        MY_LOGD("Dump image -");
    }
}

//************************************************************************
//
//************************************************************************
MVOID BokehNode:: outputDepthMap( IImageBuffer* depthMap, IImageBuffer* displayResult)
{
    if (displayResult == nullptr || displayResult->getPlaneCount() != 3)
        return;
    if (mpDpStream == nullptr)
        return;
    //
    SmartImageBuffer mdpBuffer = mpDepthMapBufPool->request();
    //
    sMDP_Config config = {
        .pDpStream  = mpDpStream,
        .pSrcBuffer = depthMap,
        .pDstBuffer = mdpBuffer->mImageBuffer.get(),
        .rotAngle   = 0,
    };
    if(!excuteMDP(config)) {
        MY_LOGE("excuteMDP fail.");
        return;
    }
    //
    MSize outImgSize = displayResult->getImgSize();
    MSize inImgSize  = mdpBuffer->mImageBuffer->getImgSize();
    char* outAddr0   = (char*)displayResult->getBufVA(0);
    char* outAddr1   = (char*)displayResult->getBufVA(1);
    char* outAddr2   = (char*)displayResult->getBufVA(2);
    char* inAddr     = (char*)mdpBuffer->mImageBuffer->getBufVA(0);
    MINT32 halfInWidth  = inImgSize.w >> 1;
    MINT32 halfInHeight = inImgSize.h >> 1;
    MINT32 halfOutWidth = outImgSize.w  >> 1;
    //
    for (int i = 0; i < inImgSize.h; ++i) {
        memcpy(outAddr0, inAddr, inImgSize.w);
        outAddr0 += outImgSize.w;
        inAddr += inImgSize.w;
    }
    //
    for (int i = 0 ; i < halfInHeight; ++i) {
        memset(outAddr1, 128, halfInWidth);
        memset(outAddr2, 128, halfInWidth);
        outAddr1 += halfOutWidth;
        outAddr2 += halfOutWidth;
    }
    mdpBuffer = nullptr;
    return;
}

//************************************************************************
//
//************************************************************************
MVOID BokehNode:: shiftDepthMapValue(IImageBuffer* depthMap, MUINT8 shiftValue)
{
    // offset value
    MUINT8* data = (MUINT8*)depthMap->getBufVA(0);
    MSize const size = depthMap->getImgSize();
    for (int i = 0; i < size.w*size.h ; ++i) {
        *data = *data << shiftValue;
        *data = (MUINT8)std::max(0, std::min((int)*data, 255));
        data++;
    }
}

/*******************************************************************************
 *
 ********************************************************************************/
MBOOL BokehNode:: createBufferPool(android::sp<ImageBufferPool> &pPool,
    MUINT32 width, MUINT32 height, NSCam::EImageFormat format,
    MUINT32 bufCount, const char* caller, MUINT32 bufUsage, MBOOL continuesBuffer)
{
    FUNCTION_IN;
    MBOOL ret = MFALSE;
    pPool = ImageBufferPool::create(caller, width, height, format, bufUsage, continuesBuffer);
    if(pPool == nullptr)
    {
        ret = MFALSE;
        MY_LOGE("Create [%s] failed.", caller);
        goto lbExit;
    }
    for(MUINT32 i=0;i<bufCount;++i)
    {
        if(!pPool->allocate())
        {
            ret = MFALSE;
            MY_LOGE("Allocate [%s] working buffer failed.", caller);
            goto lbExit;
        }
    }
    ret = MTRUE;
    FUNCTION_OUT;
lbExit:
    return ret;
}

/*******************************************************************************
 *
 ********************************************************************************/
TuningParam BokehNode:: applyISPTuning(const char* name, SmartTuningBuffer& targetTuningBuf,
    const _bokehNode_ispTuningConfig_& ispConfig, MBOOL updateHalMeta)
{
    MY_LOGD("+, [%s] reqID=%d bIsResized=%d", name, ispConfig.reqNo, ispConfig.bInputResizeRaw);

    NS3Av3::TuningParam tuningParam = {NULL, NULL};
    tuningParam.pRegBuf = reinterpret_cast<void*>(targetTuningBuf->mpVA);

    MetaSet_T inMetaSet;
    IMetadata* pMeta_InApp  = ispConfig.pInAppMeta;
    IMetadata* pMeta_InHal  = ispConfig.pInHalMeta;

    inMetaSet.appMeta = *pMeta_InApp;
    inMetaSet.halMeta = *pMeta_InHal;

    MUINT8 profile = -1;
    if (tryGetMetadata<MUINT8>(&inMetaSet.halMeta, MTK_3A_ISP_PROFILE, profile))
        MY_LOGD("Profile:%d", profile);
    else
        MY_LOGW("Failed getting profile!");


    // USE resize raw-->set PGN 0
    if (ispConfig.bInputResizeRaw)
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 0);
    else
        updateEntry<MUINT8>(&(inMetaSet.halMeta), MTK_3A_PGN_ENABLE, 1);

    MetaSet_T resultMeta;
    ispConfig.p3AHAL->setIsp(0, inMetaSet, &tuningParam, &resultMeta);

    if (updateHalMeta) {
        // write ISP resultMeta to input hal Meta
        (*pMeta_InHal) += resultMeta.halMeta;
    }
    MY_LOGD("-, [%s] reqID=%d", name, ispConfig.reqNo);
    return tuningParam;
}
/*******************************************************************************
 *
 ********************************************************************************/
template <typename T>
inline MVOID trySetMetadata(IMetadata* pMetadata, MUINT32 const tag, T const& val)
{
    if( pMetadata == NULL ) {
        MY_LOGW("pMetadata == NULL");
        return;
    }
    //
    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    pMetadata->update(tag, entry);
}

template <typename T>
inline MBOOL tryGetMetadata(IMetadata* pMetadata, MUINT32 const tag, T & rVal)
{
    if( pMetadata == NULL ) {
        MY_LOGW("pMetadata == NULL");
        return MFALSE;
    }
    //
    IMetadata::IEntry entry = pMetadata->entryFor(tag);
    if( !entry.isEmpty() ) {
        rVal = entry.itemAt(0, Type2Type<T>());
        return MTRUE;
    }
    return MFALSE;
}

template <typename T>
inline MVOID updateEntry(IMetadata* pMetadata, MUINT32 const tag, T const& val)
{
    if( pMetadata == NULL ) {
        MY_LOGW("pMetadata == NULL");
        return;
    }

    IMetadata::IEntry entry(tag);
    entry.push_back(val, Type2Type<T>());
    pMetadata->update(tag, entry);
}

MVOID debugQParams(const QParams& rInputQParam)
{
    MY_LOGD("Frame size = %zu", rInputQParam.mvFrameParams.size());

    for (size_t index = 0; index < rInputQParam.mvFrameParams.size(); ++index)
    {
        auto& frameParam = rInputQParam.mvFrameParams.itemAt(index);
        MY_LOGD("=================================================");
        MY_LOGD("Frame index = %zu", index);
        MY_LOGD("mStreamTag=%d mSensorIdx=%d", frameParam.mStreamTag, frameParam.mSensorIdx);

        MY_LOGD("=== mvIn section ===");
        for (size_t index2=0;index2<frameParam.mvIn.size();++index2)
        {
            Input data = frameParam.mvIn[index2];
            MY_LOGD("Index = %zu"           , index2);
            MY_LOGD("mvIn.PortID.index = %d", data.mPortID.index);
            MY_LOGD("mvIn.PortID.type = %d" , data.mPortID.type);
            MY_LOGD("mvIn.PortID.inout = %d", data.mPortID.inout);
            MY_LOGD("mvIn.mBuffer=%p"       , data.mBuffer);
            if (data.mBuffer !=NULL)
            {
                MY_LOGD("mvIn.mBuffer->getImgSize = %dx%d", data.mBuffer->getImgSize().w,
                                                    data.mBuffer->getImgSize().h);

                MY_LOGD("mvIn.mBuffer->getImgFormat = %x", data.mBuffer->getImgFormat());
                MY_LOGD("mvIn.mBuffer->getPlaneCount = %zu", data.mBuffer->getPlaneCount());
                for (size_t j=0; j<data.mBuffer->getPlaneCount(); j++)
                {
                    MY_LOGD("mvIn.mBuffer->getBufVA(%zu) = %lX", j, data.mBuffer->getBufVA(j));
                    MY_LOGD("mvIn.mBuffer->getBufStridesInBytes(%zu) = %lX",
                            j, data.mBuffer->getBufStridesInBytes(j));
                }
            }
            else
            {
                MY_LOGD("mvIn.mBuffer is NULL!!");
            }
            MY_LOGD("mvIn.mTransform = %d", data.mTransform);
        }

        MY_LOGD("=== mvOut section ===");
        for (size_t index2=0;index2<frameParam.mvOut.size(); index2++)
        {
            Output data = frameParam.mvOut[index2];
            MY_LOGD("Index = %zu"            , index2);
            MY_LOGD("mvOut.PortID.index = %d", data.mPortID.index);
            MY_LOGD("mvOut.PortID.type = %d" , data.mPortID.type);
            MY_LOGD("mvOut.PortID.inout = %d", data.mPortID.inout);
            MY_LOGD("mvOut.mBuffer=%p"       , data.mBuffer);
            if (data.mBuffer != NULL)
            {
                MY_LOGD("mvOut.mBuffer->getImgSize = %dx%d", data.mBuffer->getImgSize().w,
                                                    data.mBuffer->getImgSize().h);

                MY_LOGD("mvOut.mBuffer->getImgFormat = %x", data.mBuffer->getImgFormat());
                MY_LOGD("mvOut.mBuffer->getPlaneCount = %zu", data.mBuffer->getPlaneCount());
                for (size_t j=0;j<data.mBuffer->getPlaneCount();j++)
                {
                    MY_LOGD("mvOut.mBuffer->getBufVA(%zu) = %lX", j, data.mBuffer->getBufVA(j));
                    MY_LOGD("mvOut.mBuffer->getBufStridesInBytes(%zu) = %zu",
                            j, data.mBuffer->getBufStridesInBytes(j));
                }
            }
            else
            {
                MY_LOGD("mvOut.mBuffer is NULL!!");
            }
            MY_LOGD("mvOut.mTransform = %d", data.mTransform);
        }

        MY_LOGD("=== mvCropRsInfo section ===");
        for (size_t i=0;i<frameParam.mvCropRsInfo.size(); i++)
        {
            MCrpRsInfo data = frameParam.mvCropRsInfo[i];
            MY_LOGD("Index = %zu", i);
            MY_LOGD("CropRsInfo.mGroupID=%d", data.mGroupID);
            MY_LOGD("CropRsInfo.mResizeDst=%dx%d", data.mResizeDst.w, data.mResizeDst.h);
            MY_LOGD("CropRsInfo.mCropRect.p_fractional=(%d,%d) ",
                    data.mCropRect.p_fractional.x, data.mCropRect.p_fractional.y);
            MY_LOGD("CropRsInfo.mCropRect.p_integral=(%d,%d) ",
                    data.mCropRect.p_integral.x, data.mCropRect.p_integral.y);
            MY_LOGD("CropRsInfo.mCropRect.s=%dx%d ", data.mCropRect.s.w, data.mCropRect.s.h);
        }

        MY_LOGD("=== mvModuleData section ===");
        for (size_t i=0;i<frameParam.mvModuleData.size(); i++)
        {
            ModuleInfo data = frameParam.mvModuleData[i];
            MY_LOGD("Index = %zu", i);
            MY_LOGD("ModuleData.moduleTag=%d", data.moduleTag);

            _SRZ_SIZE_INFO_ *SrzInfo = (_SRZ_SIZE_INFO_ *) data.moduleStruct;
            MY_LOGD("SrzInfo->in_w=%d", SrzInfo->in_w);
            MY_LOGD("SrzInfo->in_h=%d", SrzInfo->in_h);
            MY_LOGD("SrzInfo->crop_w=%lu", SrzInfo->crop_w);
            MY_LOGD("SrzInfo->crop_h=%lu", SrzInfo->crop_h);
            MY_LOGD("SrzInfo->crop_x=%d", SrzInfo->crop_x);
            MY_LOGD("SrzInfo->crop_y=%d", SrzInfo->crop_y);
            MY_LOGD("SrzInfo->crop_floatX=%d", SrzInfo->crop_floatX);
            MY_LOGD("SrzInfo->crop_floatY=%d", SrzInfo->crop_floatY);
            MY_LOGD("SrzInfo->out_w=%d", SrzInfo->out_w);
            MY_LOGD("SrzInfo->out_h=%d", SrzInfo->out_h);
        }
        MY_LOGD("TuningData=%p", frameParam.mTuningData);
        MY_LOGD("=== mvExtraData section ===");
        for (size_t i=0;i<frameParam.mvExtraParam.size(); i++)
        {
            auto extraParam = frameParam.mvExtraParam[i];
            if (extraParam.CmdIdx == EPIPE_FE_INFO_CMD)
            {
                FEInfo *feInfo = (FEInfo*) extraParam.moduleStruct;
                MY_LOGD("mFEDSCR_SBIT=%d  mFETH_C=%d  mFETH_G=%d",
                        feInfo->mFEDSCR_SBIT, feInfo->mFETH_C, feInfo->mFETH_G);
                MY_LOGD("mFEFLT_EN=%d  mFEPARAM=%d  mFEMODE=%d",
                        feInfo->mFEFLT_EN, feInfo->mFEPARAM, feInfo->mFEMODE);
                MY_LOGD("mFEYIDX=%d  mFEXIDX=%d  mFESTART_X=%d",
                        feInfo->mFEYIDX, feInfo->mFEXIDX, feInfo->mFESTART_X);
                MY_LOGD("mFESTART_Y=%d  mFEIN_HT=%d  mFEIN_WD=%d",
                        feInfo->mFESTART_Y, feInfo->mFEIN_HT, feInfo->mFEIN_WD);

            }
            else if (extraParam.CmdIdx == EPIPE_FM_INFO_CMD)
            {
                FMInfo *fmInfo = (FMInfo*) extraParam.moduleStruct;
                MY_LOGD("mFMHEIGHT=%d  mFMWIDTH=%d  mFMSR_TYPE=%d",
                        fmInfo->mFMHEIGHT, fmInfo->mFMWIDTH, fmInfo->mFMSR_TYPE);
                MY_LOGD("mFMOFFSET_X=%d  mFMOFFSET_Y=%d  mFMRES_TH=%d",
                        fmInfo->mFMOFFSET_X, fmInfo->mFMOFFSET_Y, fmInfo->mFMRES_TH);
                MY_LOGD("mFMSAD_TH=%d  mFMMIN_RATIO=%d",
                        fmInfo->mFMSAD_TH, fmInfo->mFMMIN_RATIO);
            }
            else if(extraParam.CmdIdx == EPIPE_MDP_PQPARAM_CMD)
            {
                PQParam* param = reinterpret_cast<PQParam*>(extraParam.moduleStruct);
                if (param->WDMAPQParam != nullptr)
                {
                    DpPqParam* dpPqParam = (DpPqParam*)param->WDMAPQParam;
                    DpIspParam& ispParam = dpPqParam->u.isp;
                    VSDOFParam& vsdofParam = dpPqParam->u.isp.vsdofParam;
                    MY_LOGD("WDMAPQParam %p enable = %d, scenario=%d",
                            dpPqParam, dpPqParam->enable, dpPqParam->scenario);
                    MY_LOGD("WDMAPQParam iso = %d, frameNo=%d requestNo=%d",
                            ispParam.iso , ispParam.frameNo, ispParam.requestNo);
                    MY_LOGD("WDMAPQParam lensId = %d, isRefocus=%d defaultUpTable=%d",
                            ispParam.lensId , vsdofParam.isRefocus, vsdofParam.defaultUpTable);
                    MY_LOGD("WDMAPQParam defaultDownTable = %d, IBSEGain=%d",
                            vsdofParam.defaultDownTable, vsdofParam.IBSEGain);
                }
                if (param->WROTPQParam != nullptr)
                {
                    DpPqParam* dpPqParam = (DpPqParam*)param->WROTPQParam;
                    DpIspParam&ispParam = dpPqParam->u.isp;
                    VSDOFParam& vsdofParam = dpPqParam->u.isp.vsdofParam;
                    MY_LOGD("WROTPQParam %p enable = %d, scenario=%d",
                            dpPqParam, dpPqParam->enable, dpPqParam->scenario);
                    MY_LOGD("WROTPQParam iso = %d, frameNo=%d requestNo=%d",
                            ispParam.iso , ispParam.frameNo, ispParam.requestNo);
                    MY_LOGD("WROTPQParam lensId = %d, isRefocus=%d defaultUpTable=%d",
                            ispParam.lensId , vsdofParam.isRefocus, vsdofParam.defaultUpTable);
                    MY_LOGD("WROTPQParam defaultDownTable = %d, IBSEGain=%d",
                            vsdofParam.defaultDownTable, vsdofParam.IBSEGain);
                }
            }
        }
    }
}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
//=======================================================================================
#else //SUPPORT_VSDOF
//=======================================================================================

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {
} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
//=======================================================================================
#endif //SUPPORT_VSDOF
//=======================================================================================
