/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "StreamingFeatureData.h"
#include "StreamingFeatureNode.h"
#include "StreamingFeature_Common.h"
#include <camera_custom_eis.h>
#include <camera_custom_dualzoom.h>

#include <utility>

#define PIPE_CLASS_TAG "Data"
#define PIPE_TRACE TRACE_STREAMING_FEATURE_DATA
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam3/feature/DualCam/DualCam.Common.h>

using NSCam::NSIoPipe::QParams;
using NSCam::NSIoPipe::MCropRect;
using NSCam::Feature::P2Util::P2Pack;
using NSCam::Feature::P2Util::P2SensorData;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

std::unordered_map<MUINT32, std::string> StreamingFeatureRequest::mFeatureMaskNameMap;

StreamingFeatureRequest::StreamingFeatureRequest(const StreamingFeaturePipeUsage &pipeUsage, const FeaturePipeParam &extParam, MUINT32 requestNo, MUINT32 recordNo, const EISQState &eisQ)
    : mExtParam(extParam)
    , mQParams(mExtParam.mQParams)
    , mvFrameParams(mQParams.mvFrameParams)
    , mPipeUsage(pipeUsage)
    , mVarMap(mExtParam.mVarMap)
    , mP2Pack(mExtParam.mP2Pack)
    , mLog(mP2Pack.mLog)
    , mSFPIOManager(mExtParam.mSFPIOManager)
    , mMasterID(mExtParam.getVar<MUINT32>(VAR_DUALCAM_FOV_MASTER_ID, pipeUsage.getSensorIndex()))
    , mIORequestMap({{mMasterID, IORequest<StreamingFeatureNode, StreamingReqInfo>()}})
    , mMasterIOReq(mIORequestMap[mMasterID])
    , mFeatureMask(extParam.mFeatureMask)
    , mRequestNo(requestNo)
    , mRecordNo(recordNo)
    , mMWFrameNo(extParam.mP2Pack.getFrameData().mMWFrameNo)
    , mAppMode(IStreamingFeaturePipe::APP_PHOTO_PREVIEW)
    , mDebugDump(0)
    , mP2DumpType(mExtParam.mDumpType)
    , mSlaveParamMap(mExtParam.mSlaveParamMap)
    , mDisplayFPSCounter(NULL)
    , mFrameFPSCounter(NULL)
    , mResult(MTRUE)
    , mNeedDump(MFALSE)
    , mForceIMG3O(MFALSE)
    , mForceWarpPass(MFALSE)
    , mForceGpuOut(NO_FORCE)
    , mForceGpuRGBA(MFALSE)
    , mForcePrintIO(MFALSE)
    , mIs4K2K(MFALSE)
    , mEISQState(eisQ)
{
    mQParams.mDequeSuccess = MFALSE;
    // 3DNR + EIS1.2 in 4K2K record mode use CRZ to reduce throughput
    mIsP2ACRZMode = getVar<MBOOL>(VAR_3DNR_EIS_IS_CRZ_MODE, MFALSE);
    mAppMode = getVar<IStreamingFeaturePipe::eAppMode>(VAR_APP_MODE, IStreamingFeaturePipe::APP_PHOTO_PREVIEW);

    for( unsigned i = 0, n = mvFrameParams.size(); i < n; ++i )
    {
        mvFrameParams.editItemAt(i).UniqueKey = mRequestNo;
    }

    mMasterID = getVar<MUINT32>(VAR_DUALCAM_FOV_MASTER_ID, pipeUsage.getSensorIndex());
    mSlaveID = getVar<MUINT32>(VAR_DUALCAM_FOV_SLAVE_ID, INVALID_SENSOR); // TODO use sub sensor list id to replace it
    if(mMasterID == mSlaveID || (mSlaveID != INVALID_SENSOR && !hasSlave(mSlaveID)))
    {
        MY_LOGE("Parse Request Sensor ID & slave FeatureParam FAIL! , master(%u), slave(%u), slaveParamExist(%d)",
                mMasterID, mSlaveID, hasSlave(mSlaveID));
    }

    mTPILog = mPipeUsage.supportVendorLog();
    if( mPipeUsage.supportVendorDebug() )
    {
        mTPILog = mTPILog || property_get_int32(KEY_DEBUG_TPI_LOG, mTPILog);
        mTPIDump = property_get_int32(KEY_DEBUG_TPI_DUMP, mTPIDump);
        mTPIScan = property_get_int32(KEY_DEBUG_TPI_SCAN, mTPIScan);
        mTPIBypass = property_get_int32(KEY_DEBUG_TPI_BYPASS, mTPIBypass);
    }

    mHasGeneralOutput = hasDisplayOutput() || hasRecordOutput() || hasExtraOutput();

    mTimer.start();
}

StreamingFeatureRequest::~StreamingFeatureRequest()
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);

    double frameFPS = 0, displayFPS = 0;
    if( mDisplayFPSCounter )
    {
        mDisplayFPSCounter->update(mTimer.getDisplayMark());
        displayFPS = mDisplayFPSCounter->getFPS();
    }
    if( mFrameFPSCounter )
    {
        mFrameFPSCounter->update(mTimer.getFrameMark());
        frameFPS = mFrameFPSCounter->getFPS();
    }
    mTimer.print(mRequestNo, mRecordNo, displayFPS, frameFPS);
}

MVOID StreamingFeatureRequest::setDisplayFPSCounter(FPSCounter *counter)
{
    mDisplayFPSCounter = counter;
}

MVOID StreamingFeatureRequest::setFrameFPSCounter(FPSCounter *counter)
{
    mFrameFPSCounter = counter;
}

MBOOL StreamingFeatureRequest::updateSFPIO()
{
    if( !mPipeUsage.isQParamIOValid() && mSFPIOManager.countAll() == 0)
    {
        MY_LOGE("QParamIO invalid with SFPIOMap size is 0 !!. Can not continue.");
        return MFALSE;
    }

    if( mPipeUsage.isQParamIOValid() ) // Hal1 Used
    {
        createIOMapByQParams();
    }
    return MTRUE;
}

MVOID StreamingFeatureRequest::calSizeInfo()
{
    SrcCropInfo cInfo;
    calNonLargeSrcCrop(cInfo, mMasterID);
    mNonLargeSrcCrops[mMasterID] = cInfo;

    mFullImgSize = cInfo.mSrcCrop.s;
    mIs4K2K = NSFeaturePipe::is4K2K(mFullImgSize);

    if(mSlaveID != INVALID_SENSOR)
    {
        calNonLargeSrcCrop(cInfo, mSlaveID);
        mNonLargeSrcCrops[mSlaveID] = cInfo;
    }
}

MVOID StreamingFeatureRequest::createIOMapByQParams()
{
    TRACE_FUNC_ENTER();
    if(mSFPIOManager.countNonLarge() != 0)
    {
        MY_LOGE("IOMap already exist before converting QParam to SFPIO!! nonLarge(%d)", mSFPIOManager.countNonLarge());
        return;
    }
    SFPIOMap ioMap;

    ioMap.mPathType = PATH_GENERAL;

    if(mvFrameParams.size() > 0)
    {
        parseIO(mMasterID, mvFrameParams[0], mVarMap, ioMap, mSFPIOManager);
    }
    // Add slave input
    if((mSlaveID != INVALID_SENSOR && hasSlave(mSlaveID)))
    {
        FeaturePipeParam &fparam_slave = getSlave(mSlaveID);
        if(fparam_slave.mQParams.mvFrameParams.size() > 0)
        {
            parseIO(mSlaveID, fparam_slave.mQParams.mvFrameParams[0], fparam_slave.mVarMap, ioMap, mSFPIOManager);
        }
    }
    ioMap.mHalOut = mVarMap.get<NSCam::IMetadata*>(VAR_HAL1_HAL_OUT_METADATA, NULL);
    ioMap.mAppOut = mVarMap.get<NSCam::IMetadata*>(VAR_HAL1_APP_OUT_METADATA, NULL);

    mSFPIOManager.addGeneral(ioMap);

    TRACE_FUNC_EXIT();
}

MBOOL StreamingFeatureRequest::updateResult(MBOOL result)
{
    mResult = (mResult && result);
    mQParams.mDequeSuccess = mResult;
    return mResult;
}

MBOOL StreamingFeatureRequest::doExtCallback(FeaturePipeParam::MSG_TYPE msg)
{
    MBOOL ret = MFALSE;
    if( msg == FeaturePipeParam::MSG_FRAME_DONE )
    {
        mTimer.stop();
    }
    if( mExtParam.mCallback )
    {
        ret = mExtParam.mCallback(msg, mExtParam);
    }
    return ret;
}

MVOID StreamingFeatureRequest::calNonLargeSrcCrop(SrcCropInfo &info, MUINT32 sensorID)
{
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    const SFPSensorInput &sensorIn = mSFPIOManager.getInput(sensorID);
    SFPSensorTuning tuning;

    if(generalIO.isValid() && generalIO.hasTuning(sensorID))
    {
        tuning = generalIO.getTuning(sensorID);
    }
    else if(mSFPIOManager.hasPhysicalIO(sensorID))
    {
        tuning = mSFPIOManager.getPhysicalIO(sensorID).getTuning(sensorID);
    }

    info.mIMGOSize = (sensorIn.mIMGO != NULL) ? sensorIn.mIMGO->getImgSize() : MSize(0,0);
    info.mRRZOSize = (sensorIn.mRRZO != NULL) ? sensorIn.mRRZO->getImgSize()
                      : mP2Pack.isValid() ? mP2Pack.getSensorData(sensorID).mP1OutSize
                      : getSensorVarMap(sensorID).get<MSize>(VAR_HAL1_P1_OUT_RRZ_SIZE, MSize(0,0));
    info.mIMGOin = (tuning.isIMGOin() && !tuning.isRRZOin());
    if(tuning.isRRZOin())
    {
        info.mSrcCrop = MRect(MPoint(0,0), info.mRRZOSize);
    }
    info.mIsSrcCrop = MFALSE;

    if(info.mIMGOin)
    {
        info.mSrcCrop = (mP2Pack.isValid()) ? mP2Pack.getSensorData(sensorID).mP1Crop
                    : getSensorVarMap(sensorID).get<MRect>(VAR_IMGO_2IMGI_P1CROP, MRect(0,0));
        info.mIsSrcCrop = MTRUE;
        // HW limitation
        info.mSrcCrop.p.x &= (~1);

    }
    // NOTE : currently not consider CRZ mode
    MY_LOGD("sID(%d), imgoIn(%d), srcCrop(%d,%d,%dx%d), isSrcCrop(%d), mP2Pack Valid(%d), imgo(%dx%d),rrz(%dx%d)",
            sensorID, info.mIMGOin, info.mSrcCrop.p.x, info.mSrcCrop.p.y, info.mSrcCrop.s.w, info.mSrcCrop.s.h,
            info.mIsSrcCrop, mP2Pack.isValid(), info.mIMGOSize.w, info.mIMGOSize.h, info.mRRZOSize.w, info.mRRZOSize.h);
}

IImageBuffer* StreamingFeatureRequest::getMasterInputBuffer()
{
    IImageBuffer *buffer = NULL;
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    const SFPSensorInput &masterIn = mSFPIOManager.getInput(mMasterID);
    SFPSensorTuning tuning;

    if(generalIO.isValid() && generalIO.hasTuning(mMasterID))
    {
        tuning = generalIO.getTuning(mMasterID);
    }
    else if(mSFPIOManager.hasPhysicalIO(mMasterID))
    {
        tuning = mSFPIOManager.getPhysicalIO(mMasterID).getTuning(mMasterID);
    }
    buffer =  tuning.isRRZOin() ? masterIn.mRRZO
        : tuning.isIMGOin() ? masterIn.mIMGO
        : NULL;
    return buffer;
}

MBOOL StreamingFeatureRequest::getDisplayOutput(SFPOutput &output)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    MBOOL ret = getOutBuffer(generalIO, IO_TYPE_DISPLAY, output);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No display buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getRecordOutput(SFPOutput &output)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    MBOOL ret = getOutBuffer(generalIO, IO_TYPE_RECORD, output);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No record buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getExtraOutput(SFPOutput &output)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    MBOOL ret = getOutBuffer(generalIO, IO_TYPE_EXTRA, output);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No extra buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getExtraOutputs(std::vector<SFPOutput> &outList)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    MBOOL ret = getOutBuffer(generalIO, IO_TYPE_EXTRA, outList);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No extra buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getPhysicalOutput(SFPOutput &output, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &phyIO = mSFPIOManager.getPhysicalIO(sensorID);
    MBOOL ret = getOutBuffer(phyIO, IO_TYPE_PHYSICAL, output);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No physical buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getLargeOutputs(std::vector<SFPOutput> &outList, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    const SFPIOMap &largeIO = mSFPIOManager.getLargeIO(sensorID);
    if(largeIO.isValid())
    {
        largeIO.getAllOutput(outList);
        ret = MTRUE;
    }
    if( !ret )
    {
        TRACE_FUNC("frame %d: No Large buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeatureRequest::getFDOutput(SFPOutput &output)
{
    TRACE_FUNC_ENTER();
    const SFPIOMap &generalIO = mSFPIOManager.getFirstGeneralIO();
    MBOOL ret = getOutBuffer(generalIO, IO_TYPE_FD, output);
    if( !ret )
    {
        TRACE_FUNC("frame %d: No FD buffer", mRequestNo);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

EISSourceDumpInfo StreamingFeatureRequest::getEISDumpInfo()
{
    return mEisDumpInfo;
}

IImageBuffer* StreamingFeatureRequest::getRecordOutputBuffer()
{
    SFPOutput output;
    IImageBuffer *outputBuffer = NULL;
    if( getRecordOutput(output) )
    {
        outputBuffer = output.mBuffer;
    }
    return outputBuffer;
}

android::sp<IIBuffer> StreamingFeatureRequest::requestNextFullImg(StreamingFeatureNode *node, MUINT32 sensorID, MSize &resize)
{
    TRACE_FUNC_ENTER();
    if(mIORequestMap.count(sensorID) > 0 && mIORequestMap.at(sensorID).needNextFull(node))
    {
        return mIORequestMap.at(sensorID).getNextFullImg(node, resize);
    }

    TRACE_FUNC_EXIT();
    return NULL;
}

MBOOL StreamingFeatureRequest::needDisplayOutput(StreamingFeatureNode *node)
{
    return mMasterIOReq.needPreview(node);
}

MBOOL StreamingFeatureRequest::needRecordOutput(StreamingFeatureNode *node)
{
    return mMasterIOReq.needRecord(node);
}

MBOOL StreamingFeatureRequest::needExtraOutput(StreamingFeatureNode *node)
{
    return mMasterIOReq.needPreviewCallback(node);
}

MBOOL StreamingFeatureRequest::needFullImg(StreamingFeatureNode *node, MUINT32 sensorID)
{
    return (mIORequestMap.count(sensorID) > 0)
            && (mIORequestMap.at(sensorID).needFull(node));
}

MBOOL StreamingFeatureRequest::needNextFullImg(StreamingFeatureNode *node, MUINT32 sensorID)
{
    return (mIORequestMap.count(sensorID) > 0)
            && (mIORequestMap.at(sensorID).needNextFull(node));
}

MBOOL StreamingFeatureRequest::needPhysicalOutput(StreamingFeatureNode *node, MUINT32 sensorID)
{
    return (mIORequestMap.count(sensorID) > 0)
            && (mIORequestMap.at(sensorID).needPhysicalOut(node));
}

MBOOL StreamingFeatureRequest::hasGeneralOutput() const
{
    return mHasGeneralOutput;
}

MBOOL StreamingFeatureRequest::hasDisplayOutput() const
{
    return existOutBuffer(mSFPIOManager.getFirstGeneralIO(), IO_TYPE_DISPLAY);
}

MBOOL StreamingFeatureRequest::hasRecordOutput() const
{
    return existOutBuffer(mSFPIOManager.getFirstGeneralIO(), IO_TYPE_RECORD);
}

MBOOL StreamingFeatureRequest::hasExtraOutput() const
{
    return existOutBuffer(mSFPIOManager.getFirstGeneralIO(), IO_TYPE_EXTRA);
}

MBOOL StreamingFeatureRequest::hasPhysicalOutput(MUINT32 sensorID) const
{
    return mSFPIOManager.getPhysicalIO(sensorID).isValid();
}

MBOOL StreamingFeatureRequest::hasLargeOutput(MUINT32 sensorID) const
{
    return mSFPIOManager.getLargeIO(sensorID).isValid();
}

MSize StreamingFeatureRequest::getEISInputSize() const
{
    MSize size = mFullImgSize;
    if( mPipeUsage.supportFOV() && !mPipeUsage.supportFOVCombineEIS() )
    {
        size = size - getFOVMarginPixel() * 2;
    }
    return size;
}

MSize StreamingFeatureRequest::getFOVAlignSize() const
{
    MSize size = mFullImgSize;
    if( mPipeUsage.supportFOV() )
    {
        size = size - getFOVMarginPixel() * 2;
    }
    return size;
}

SrcCropInfo StreamingFeatureRequest::getSrcCropInfo(MUINT32 sensorID)
{
    if(mNonLargeSrcCrops.count(sensorID) == 0)
    {
        MY_LOGW("sID(%d) srcCropInfo not exist!, create dummy.", sensorID);
        mNonLargeSrcCrops[sensorID] = SrcCropInfo();
    }
    return mNonLargeSrcCrops.at(sensorID);
}

MVOID StreamingFeatureRequest::setDumpProp(MINT32 start, MINT32 count, MBOOL byRecordNo)
{
    MINT32 debugDumpNo = byRecordNo ? mRecordNo : mRequestNo;
    mNeedDump = (start < 0) ||
                (((MINT32)debugDumpNo >= start) && (((MINT32)debugDumpNo - start) < count));
}

MVOID StreamingFeatureRequest::setForceIMG3O(MBOOL forceIMG3O)
{
    mForceIMG3O = forceIMG3O;
}

MVOID StreamingFeatureRequest::setForceWarpPass(MBOOL forceWarpPass)
{
    mForceWarpPass = forceWarpPass;
}

MVOID StreamingFeatureRequest::setForceGpuOut(MUINT32 forceGpuOut)
{
    mForceGpuOut = forceGpuOut;
}

MVOID StreamingFeatureRequest::setForceGpuRGBA(MBOOL forceGpuRGBA)
{
    mForceGpuRGBA = forceGpuRGBA;
}

MVOID StreamingFeatureRequest::setForcePrintIO(MBOOL forcePrintIO)
{
    mForcePrintIO = forcePrintIO;
}

MVOID StreamingFeatureRequest::setEISDumpInfo(const EISSourceDumpInfo& info)
{
    mEisDumpInfo = info;
}

MBOOL StreamingFeatureRequest::isForceIMG3O() const
{
    return mForceIMG3O;
}

MBOOL StreamingFeatureRequest::hasSlave(MUINT32 sensorID) const
{
    return (mSlaveParamMap.count(sensorID) != 0);
}

FeaturePipeParam& StreamingFeatureRequest::getSlave(MUINT32 sensorID)
{
    if(hasSlave(sensorID))
    {
        return mSlaveParamMap.at(sensorID);
    }
    else
    {
        MY_LOGE("sensor(%d) has no slave FeaturePipeParam !! create Dummy", sensorID);
        mSlaveParamMap[sensorID] = FeaturePipeParam();
        return mSlaveParamMap.at(sensorID);
    }
}

const SFPSensorInput& StreamingFeatureRequest::getSensorInput(MUINT32 sensorID) const
{
    return mSFPIOManager.getInput(sensorID);
}

VarMap& StreamingFeatureRequest::getSensorVarMap(MUINT32 sensorID)
{
    if(sensorID == mMasterID)
    {
        return mVarMap;
    }
    else
    {
        return mSlaveParamMap[sensorID].mVarMap;
    }
}

MBOOL StreamingFeatureRequest::getMasterFrameBasic(NSCam::NSIoPipe::FrameParams &output)
{
    output.UniqueKey = mRequestNo;
    output.mSensorIdx = getMasterID();
    output.mStreamTag = NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    if(mPipeUsage.isQParamIOValid())
    {
        if(mQParams.mvFrameParams.size())
        {
            output.FrameNo = mQParams.mvFrameParams[0].FrameNo;
            output.RequestNo = mQParams.mvFrameParams[0].RequestNo;
            output.Timestamp = mQParams.mvFrameParams[0].Timestamp;
            return MTRUE;
        }
        MY_LOGE("QParamValid = true but w/o any frame param exist!");
        return MFALSE;
    }
    else
    {
        if(mExtParam.mP2Pack.isValid())
        {
            output.FrameNo = mExtParam.mP2Pack.getFrameData().mMWFrameNo;
            output.RequestNo = mExtParam.mP2Pack.getFrameData().mMWFrameRequestNo;
            output.Timestamp = mExtParam.mP2Pack.getSensorData().mP1TS;
            return MTRUE;
        }
        MY_LOGE("QParamValid = false but w/o valid P2Pack!");
        return MFALSE;
    }
}

    // Legacy code for Hal1, w/o dynamic tuning & P2Pack
MBOOL StreamingFeatureRequest::getMasterFrameTuning(NSCam::NSIoPipe::FrameParams &output)
{
    if( mQParams.mvFrameParams.size() )
    {
        output.mTuningData = mQParams.mvFrameParams.editItemAt(0).mTuningData;
        output.mvModuleData = mQParams.mvFrameParams.editItemAt(0).mvModuleData;
        return MTRUE;
    }
    return MFALSE;
}

MBOOL StreamingFeatureRequest::getMasterFrameInput(NSCam::NSIoPipe::FrameParams &output)
{
    if( mQParams.mvFrameParams.size() )
    {
        output.mvIn = mQParams.mvFrameParams.editItemAt(0).mvIn;
        return MTRUE;
    }
    return MFALSE;
}



const char* StreamingFeatureRequest::getFeatureMaskName() const
{
    std::unordered_map<MUINT32, std::string>::const_iterator iter = mFeatureMaskNameMap.find(mFeatureMask);

    if( iter == mFeatureMaskNameMap.end() )
    {
        string str;

        appendVendorTag(str, mFeatureMask);
        append3DNRTag(str, mFeatureMask);
        appendFSCTag(str, mFeatureMask);
        appendEisTag(str, mFeatureMask);
        appendFOVTag(str, mFeatureMask);
        appendN3DTag(str, mFeatureMask);
        appendNoneTag(str, mFeatureMask);
        appendDefaultTag(str, mFeatureMask);

        iter = mFeatureMaskNameMap.insert(std::make_pair(mFeatureMask, str)).first;
    }

    return iter->second.c_str();
}

MBOOL StreamingFeatureRequest::need3DNR() const
{
    return HAS_3DNR(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needVHDR() const
{
    return HAS_VHDR(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needEIS() const
{
    return HAS_EIS(mFeatureMask) && hasRecordOutput();
}

MBOOL StreamingFeatureRequest::needEIS22() const
{
    return HAS_EIS(mFeatureMask) && mPipeUsage.supportEIS_22();
}

MBOOL StreamingFeatureRequest::needEIS25() const
{
    return HAS_EIS(mFeatureMask) && mPipeUsage.supportEIS_25();
}

MBOOL StreamingFeatureRequest::needEIS30() const
{
    return HAS_EIS(mFeatureMask) && mPipeUsage.supportEIS_30();
}

MBOOL StreamingFeatureRequest::needVendor() const
{
    return HAS_VENDOR_V1(mFeatureMask) || HAS_VENDOR_V2(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needVendorV1() const
{
    return HAS_VENDOR_V1(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needVendorV2() const
{
    return HAS_VENDOR_V2(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needVendorMDP() const
{
    return needVendor();
}

MBOOL StreamingFeatureRequest::needVendorFullImg() const
{
    return needVendor() && mPipeUsage.supportVendorFullImg();
}

MBOOL StreamingFeatureRequest::needEarlyFSCVendorFullImg() const
{
    // no MTK EIS and vendor node take action-->need early FSC in P2A
    return needVendor() && mPipeUsage.supportVendorFSCFullImg() && needFSC();
}

MBOOL StreamingFeatureRequest::needWarp() const
{
    return needEIS();
}

MBOOL StreamingFeatureRequest::needFullImg() const
{
    return mForceIMG3O || HAS_3DNR(mFeatureMask) || HAS_EIS(mFeatureMask)
        || (needVendorV1() && !needVendorFullImg()) || HAS_FOV(mFeatureMask)
        || HAS_N3D(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needFEFM() const
{
    return needEIS25() && mPipeUsage.supportFEFM();
}

MBOOL StreamingFeatureRequest::needEarlyDisplay() const
{
    // return needEIS25() || needEIS22();
    return HAS_EIS(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needP2AEarlyDisplay() const
{
    return needEarlyDisplay() && !needVendor() &&
           !needFOV();
}

MBOOL StreamingFeatureRequest::skipMDPDisplay() const
{
    return needEarlyDisplay();
}

MBOOL StreamingFeatureRequest::needRSC() const
{
    return mPipeUsage.supportRSCNode() &&
           (needEIS30() || (need3DNR() && HAS_3DNR_RSC(mFeatureMask)));
}

MBOOL StreamingFeatureRequest::needFSC() const
{
    return mPipeUsage.supportFSC() && HAS_FSC(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needDump() const
{
    return mNeedDump;
}

MBOOL StreamingFeatureRequest::needNddDump() const
{
    return mP2DumpType == Feature::P2Util::P2_DUMP_NDD && mP2Pack.isValid();
}

MBOOL StreamingFeatureRequest::isLastNodeP2A() const
{
    return !HAS_VENDOR_V1(mFeatureMask) &&
           !HAS_VENDOR_V2(mFeatureMask) &&
           !HAS_EIS(mFeatureMask) &&
           !HAS_FOV(mFeatureMask);
}

MBOOL StreamingFeatureRequest::is4K2K() const
{
    return mIs4K2K;
}

MUINT32 StreamingFeatureRequest::getMasterID() const
{
    return mMasterID;
}

MBOOL StreamingFeatureRequest::needFOV() const
{
    return HAS_FOV(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needN3D() const
{
    return HAS_N3D(mFeatureMask);
}

MBOOL StreamingFeatureRequest::needFOVFEFM() const
{
    MBOOL ret = MFALSE;
    if( mPipeUsage.supportFOV() )
    {
        ret = this->getVar<MINT32>(VAR_DUALCAM_DO_SYNC, 0);
    }
    return ret;
}

MBOOL StreamingFeatureRequest::isOnFOVSensor() const
{
    //TODO need change for fov on tele
    return mPipeUsage.supportFOV() && (getMasterID() == 2);
}

MBOOL StreamingFeatureRequest::needEISFullImg() const
{
    return (mPipeUsage.supportFOV() || mPipeUsage.supportVendor()) && needEIS();
}

MBOOL StreamingFeatureRequest::needP2AEarlyEISFullImg() const
{
    return needEISFullImg() && !needFOV() &&
           !needVendor();
}

MBOOL StreamingFeatureRequest::needHWFOVWarp() const
{
    return !needEIS() ||
           (mPipeUsage.supportWPE() && !mPipeUsage.support4K2K());
}

MBOOL StreamingFeatureRequest::needTOF() const
{
    return mPipeUsage.supportTOF();
}

MUINT32 StreamingFeatureRequest::needTPILog() const
{
    return mTPILog;
}

MUINT32 StreamingFeatureRequest::needTPIDump() const
{
    return mTPIDump;
}

MUINT32 StreamingFeatureRequest::needTPIScan() const
{
    return mTPIScan;
}

MUINT32 StreamingFeatureRequest::needTPIBypass() const
{
    return mTPIBypass;
}

MBOOL StreamingFeatureRequest::isSlaveParamValid() const
{
    return mSlaveID != INVALID_SENSOR && hasSlave(mSlaveID);
}

MSize StreamingFeatureRequest::getFOVMarginPixel() const
{
    return this->getVar<MSize>(VAR_DUALCAM_FOV_RRZO_MARGIN, MSize(0,0));
}

MSizeF StreamingFeatureRequest::getEISMarginPixel() const
{
    MRectF region = this->getVar<MRectF>(VAR_EIS_RRZO_CROP, MRectF(0, 0));
    TRACE_FUNC("FSC(%d). RRZO crop(%f,%f)(%fx%f)", needFSC(),
               region.p.x, region.p.y, region.s.w, region.s.h);
    return MSizeF(region.p.x, region.p.y);
}

MRectF StreamingFeatureRequest::getEISCropRegion() const
{
    return this->getVar<MRectF>(VAR_EIS_RRZO_CROP, MRectF(0, 0));
}

MSize StreamingFeatureRequest::getFSCMaxMarginPixel() const
{
    MSize maxFSCMargin(0, 0);
    FSC::FSC_WARPING_DATA_STRUCT fscData;
    if( this->tryGetVar<FSC::FSC_WARPING_DATA_STRUCT>(VAR_FSC_RRZO_WARP_DATA, fscData) )
    {
        maxFSCMargin = MSize(FSC_CROP_INT(fscData.maxRRZOCropRegion.p.x), FSC_CROP_INT(fscData.maxRRZOCropRegion.p.y));
        TRACE_FUNC("Max FSC RRZO crop(%d,%d)(%dx%d)",
               FSC_CROP_INT(fscData.maxRRZOCropRegion.p.x), FSC_CROP_INT(fscData.maxRRZOCropRegion.p.y),
               FSC_CROP_INT(fscData.maxRRZOCropRegion.s.w), FSC_CROP_INT(fscData.maxRRZOCropRegion.s.h));
    }
    else
    {
        MY_LOGW("Cannot get FSC_WARPING_DATA_STRUCT. Use default (%dx%d)", maxFSCMargin.w, maxFSCMargin.h);
    }
    return maxFSCMargin;
}

MBOOL StreamingFeatureRequest::isP2ACRZMode() const
{
    return mIsP2ACRZMode;
}

EISQ_ACTION StreamingFeatureRequest::getEISQAction() const
{
    return mEISQState.mAction;
}

MUINT32 StreamingFeatureRequest::getEISQCounter() const
{
    return mEISQState.mCounter;
}

MBOOL StreamingFeatureRequest::useWarpPassThrough() const
{
    return mForceWarpPass;
}

MBOOL StreamingFeatureRequest::useDirectGpuOut() const
{
    MBOOL val = MFALSE;
    if( mForceGpuRGBA == MFALSE )
    {
        if( mForceGpuOut )
        {
            val = (mForceGpuOut == FORCE_ON);
        }
        else
        {
            val = this->is4K2K() && !mPipeUsage.supportWPE() && !EISCustom::isEnabled4K2KMDP();
        }
    }
    return val;
}

MBOOL StreamingFeatureRequest::needPrintIO() const
{
    return mForcePrintIO;
}

MBOOL StreamingFeatureRequest::getCropInfo(NSCam::NSIoPipe::EPortCapbility cap, MUINT32 defCropGroup, NSCam::NSIoPipe::MCrpRsInfo &crop/*, MRectF *cropF*/)
{
    TRACE_FUNC_ENTER();
    unsigned count = 0;
    MUINT32 cropGroup = defCropGroup;

    if( mQParams.mvFrameParams.size() )
    {
        if( cropGroup != IMG2O_CROP_GROUP )
        {
            for( unsigned i = 0, size = mQParams.mvFrameParams[0].mvOut.size(); i < size; ++i )
            {
                if( mQParams.mvFrameParams[0].mvOut[i].mPortID.capbility == cap )
                {
                    switch( mQParams.mvFrameParams[0].mvOut[i].mPortID.index )
                    {
                    case NSImageio::NSIspio::EPortIndex_WDMAO:
                        cropGroup = WDMAO_CROP_GROUP;
                        break;
                    case NSImageio::NSIspio::EPortIndex_WROTO:
                        cropGroup = WROTO_CROP_GROUP;
                        break;
                    }
                }
            }
        }

        TRACE_FUNC("wanted crop group = %d, found group = %d", defCropGroup, cropGroup);

        for( unsigned i = 0, size = mQParams.mvFrameParams[0].mvCropRsInfo.size(); i < size; ++i )
        {
            if( mQParams.mvFrameParams[0].mvCropRsInfo[i].mGroupID == (MINT32)cropGroup )
            {
                if( ++count == 1 )
                {
                    crop = mQParams.mvFrameParams[0].mvCropRsInfo[i];
                    TRACE_FUNC("Found crop(%d): %dx%d", crop.mGroupID, crop.mCropRect.s.w, crop.mCropRect.s.h);
                }
            }
        }
    }

    if( count > 1 )
    {
        TRACE_FUNC("frame %d: suspicious crop(ask/found: %d/%d) number = %d", mRequestNo, defCropGroup, cropGroup, count);
    }
    TRACE_FUNC_EXIT();
    return count >= 1;
}

void StreamingFeatureRequest::appendEisTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_EIS(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }

        if( mPipeUsage.supportEIS_25() )
        {
            str += TAG_EIS_25();
        }
        else if( mPipeUsage.supportEIS_30() )
        {
            str += TAG_EIS_30();
        }
        else
        {
            str += TAG_EIS();
        }

        if( mPipeUsage.supportEIS_Q() )
        {
            str += TAG_EIS_QUEUE();
        }
    }
}

void StreamingFeatureRequest::append3DNRTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_3DNR(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }

        if( HAS_3DNR_RSC(mFeatureMask) )
        {
            str += TAG_3DNR_RSC();
        }
        else
        {
            str += TAG_3DNR();
        }
    }
}

void StreamingFeatureRequest::appendFSCTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_FSC(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }

        str += TAG_FSC();
    }
}

void StreamingFeatureRequest::appendVendorTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_VENDOR_V1(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }
        str += TAG_VENDOR_V1();
    }
    if( HAS_VENDOR_V2(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }
        str += TAG_VENDOR_V2();
    }
}

void StreamingFeatureRequest::appendFOVTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_FOV(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }

        str += TAG_FOV();
    }
}

void StreamingFeatureRequest::appendN3DTag(string& str, MUINT32 mFeatureMask) const
{
    if( HAS_N3D(mFeatureMask) )
    {
        if(!str.empty())
        {
            str += "+";
        }

        str += TAG_N3D();
    }
}

void StreamingFeatureRequest::appendNoneTag(string& str, MUINT32 mFeatureMask) const
{
    if( mFeatureMask == 0 )
    {
        str += "NONE";
    }
}

void StreamingFeatureRequest::appendDefaultTag(string& str, MUINT32 mFeatureMask) const
{
    (void)(mFeatureMask);
    if(str.empty())
    {
        str += "UNKNOWN";
    }
}

MVOID FEFMGroup::clear()
{
    this->High = NULL;
    this->Medium = NULL;
    this->Low = NULL;
}

MBOOL FEFMGroup::isValid() const
{
    return this->High != NULL;
}

RSCResult::RSCResult()
    : mMV(NULL)
    , mBV(NULL)
    , mIsValid(MFALSE)
{
}

RSCResult::RSCResult(const ImgBuffer &mv, const ImgBuffer &bv, const MSize& rssoSize, const RSC_STA_0& rscSta, MBOOL valid)
    : mMV(mv)
    , mBV(bv)
    , mRssoSize(rssoSize)
    , mRscSta(rscSta)
    , mIsValid(valid)
{
}

BasicImg::BasicImg()
    : mBuffer(NULL)
    , mDomainTransformScale(MSizeF(1.0f, 1.0f))
    , mIsReady(MTRUE)
{
}

BasicImg::BasicImg(const ImgBuffer &img)
    : mBuffer(img)
    , mDomainTransformScale(MSizeF(1.0f, 1.0f))
    , mIsReady(MTRUE)
{
}

BasicImg::BasicImg(const ImgBuffer &img, const MPointF &offset)
    : mBuffer(img)
    , mDomainOffset(offset)
    , mDomainTransformScale(MSizeF(1.0f, 1.0f))
    , mIsReady(MTRUE)
{
}

BasicImg::BasicImg(const ImgBuffer &img, const MPointF &offset, const MBOOL &isReady)
    : mBuffer(img)
    , mDomainOffset(offset)
    , mDomainTransformScale(MSizeF(1.0f, 1.0f))
    , mIsReady(isReady)
{
}

MVOID BasicImg::setSizeInfo(const BasicImg &img)
{
    if( mBuffer != NULL && img.mBuffer != NULL )
    {
        mBuffer->getImageBuffer()->setExtParam(img.mBuffer->getImageBuffer()->getImgSize());
    }
}

MVOID BasicImg::setDomainInfo(const BasicImg &img)
{
    mDomainOffset = img.mDomainOffset;
    mDomainTransformScale = img.mDomainTransformScale;
}

MBOOL BasicImg::syncCache(NSCam::eCacheCtrl ctrl)
{
    return (mBuffer != NULL) && mBuffer->syncCache(ctrl);
}

DualBasicImg::DualBasicImg()
{
}

DualBasicImg::DualBasicImg(const BasicImg &master)
    : mMaster(master)
{
}

DualBasicImg::DualBasicImg(const BasicImg &master, const BasicImg &slave)
    : mMaster(master)
    , mSlave(slave)
{
}

HelpReq::HelpReq()
{
}

HelpReq::HelpReq(FeaturePipeParam::MSG_TYPE msg)
    : mCBMsg(msg)
{
}

HelpReq::HelpReq(FeaturePipeParam::MSG_TYPE msg, INTERNAL_MSG_TYPE intMsg)
    : mCBMsg(msg)
    , mInternalMsg(intMsg)
{
}

TPIRes::TPIRes()
{
}

TPIRes::TPIRes(const BasicImg &yuv)
{
    mSFP[TPI_BUFFER_ID_MTK_YUV] = yuv;
}

TPIRes::TPIRes(const DualBasicImg &dual)
{
    mSFP[TPI_BUFFER_ID_MTK_YUV] = dual.mMaster;
    mSFP[TPI_BUFFER_ID_MTK_YUV_2] = dual.mSlave;
}

MVOID TPIRes::add(const DepthImg &depth)
{
    mSFP[TPI_BUFFER_ID_MTK_DEPTH] = depth.mDepthMapImg;
    mSFP[TPI_BUFFER_ID_MTK_DEPTH_INTENSITY] = depth.mDepthIntensity;
}

BasicImg TPIRes::getSFP(unsigned id) const
{
    BasicImg ret;
    auto it = mSFP.find(id);
    if( it != mSFP.end() )
    {
        ret = it->second;
    }
    return ret;
}

BasicImg TPIRes::getTP(unsigned id) const
{
    BasicImg ret;
    auto it = mTP.find(id);
    if( it != mTP.end() )
    {
        ret = it->second;
    }
    return ret;
}

IMetadata* TPIRes::getMeta(unsigned id) const
{
    IMetadata *ret = NULL;
    auto it = mMeta.find(id);
    if( it != mMeta.end() )
    {
        ret = it->second;
    }
    return ret;
}

MVOID TPIRes::setSFP(unsigned id, const BasicImg &img)
{
    mSFP[id] = img;
}

MVOID TPIRes::setTP(unsigned id, const BasicImg &img)
{
    mTP[id] = img;
}

MVOID TPIRes::setMeta(unsigned id, IMetadata *meta)
{
    mMeta[id] = meta;
}

MUINT32 TPIRes::getImgArray(TPI_Buffer imgs[], unsigned count) const
{
    MUINT32 index = 0;
    for( auto &img : mTP )
    {
        if( index < count && img.second.mBuffer != NULL )
        {
            imgs[index].mBufferID = img.first;
            imgs[index].mBufferPtr = img.second.mBuffer->getImageBufferPtr();
            ++index;
        }
    }
    return index;
}

MUINT32 TPIRes::getMetaArray(TPI_Meta metas[], unsigned count) const
{
    MUINT32 index = 0;
    for( auto &meta : mMeta )
    {
        if( index < count )
        {
            metas[index].mMetaID = meta.first;
            metas[index].mMetaPtr = meta.second;
            ++index;
        }
    }
    return index;
}

} // NSFeaturePipe
} // NSCamFeature
} // NSCam
