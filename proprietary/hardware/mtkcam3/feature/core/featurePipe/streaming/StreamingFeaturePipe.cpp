/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "StreamingFeaturePipe.h"

#define PIPE_CLASS_TAG "Pipe"
#define PIPE_TRACE TRACE_STREAMING_FEATURE_PIPE
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam/drv/def/Dip_Notify_datatype.h>

#define NORMAL_STREAM_NAME "StreamingFeature"

using namespace NSCam::NSIoPipe::NSPostProc;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

StreamingFeaturePipe::StreamingFeaturePipe(MUINT32 sensorIndex, const UsageHint &usageHint)
    : CamPipe<StreamingFeatureNode>("StreamingFeaturePipe")
    , mForceOnMask(0)
    , mForceOffMask(~0)
    , mSensorIndex(sensorIndex)
    , mPipeUsage(usageHint, sensorIndex)
    , mCounter(0)
    , mRecordCounter(0)
    , mDebugDump(0)
    , mDebugDumpCount(1)
    , mDebugDumpByRecordNo(MFALSE)
    , mForceIMG3O(MFALSE)
    , mForceWarpPass(MFALSE)
    , mForceGpuOut(NO_FORCE)
    , mForceGpuRGBA(MFALSE)
    , mUsePerFrameSetting(MFALSE)
    , mForcePrintIO(MFALSE)
    , mEarlyInited(MFALSE)
    , mRootNode("fpipe.root")
    , mP2A("fpipe.p2a")
    , mP2AMDP("fpipe.p2amdp")
    , mWarp("fpipe.warp")
    , mDepth("fpipe.depth")
    , mBokeh("fpipe.bokeh")
    , mFOV("fpipe.fov")
    , mFOVWarp("fpipe.fovwarp")
    , mN3D_P2Node("fpipe.n3dp2")
    , mN3D("fpipe.n3d")
    , mEIS("fpipe.eis")
    , mRSC("fpipe.rsc")
    , mHelper("fpipe.helper")
    , mVendor("fpipe.vendor")
    , mVendorMDP("fpipe.vmdp")
    , mDummy("fpipe.dummy")
{
    TRACE_FUNC_ENTER();

    MY_LOGI("create pipe(%p): SensorIndex=%d sensorNum(%d) dualMode(%d),UsageMode=%d VendorNode=%d EisMode=0x%x 3DNRMode=%d tsq=%d \
         StreamingSize=%dx%d usageHint.VendorNodeSize=%dx%d pipeUsage.VendorCusSize=%d dynTun=%d qParamVal=%d, Out(max/phy/large)=(%d/%d/%d), secType=%d", this, mSensorIndex, mPipeUsage.getNumSensor(),
        mPipeUsage.getDualMode(), mPipeUsage.getMode(), mPipeUsage.getVendorMode(), mPipeUsage.getEISMode(), mPipeUsage.get3DNRMode(), usageHint.mUseTSQ, usageHint.mStreamingSize.w, usageHint.mStreamingSize.h,
        usageHint.mVendorCusSize.w, usageHint.mVendorCusSize.h, mPipeUsage.supportVendorCusSize(), mPipeUsage.isDynamicTuning(), mPipeUsage.isQParamIOValid()
        , usageHint.mOutCfg.mMaxOutNum, usageHint.mOutCfg.mHasPhysical, usageHint.mOutCfg.mHasLarge, usageHint.mSecType);

    mAllSensorIDs = mPipeUsage.getAllSensorIDs();
    mNodeSignal = new NodeSignal();
    if( mNodeSignal == NULL )
    {
        MY_LOGE("OOM: cannot create NodeSignal");
    }

    for( int i = 0; i < P2CamContext::SENSOR_INDEX_MAX; i++ )
    {
        mContextCreated[i] = MFALSE;
    }

    mEarlyInited = earlyInit();
    TRACE_FUNC_EXIT();
}

StreamingFeaturePipe::~StreamingFeaturePipe()
{
    TRACE_FUNC_ENTER();
    MY_LOGD("destroy pipe(%p): SensorIndex=%d", this, mSensorIndex);
    lateUninit();
    // must call dispose to free CamGraph
    this->dispose();
    TRACE_FUNC_EXIT();
}

void StreamingFeaturePipe::setSensorIndex(MUINT32 sensorIndex)
{
    TRACE_FUNC_ENTER();
    this->mSensorIndex = sensorIndex;
    TRACE_FUNC_EXIT();
}

MBOOL StreamingFeaturePipe::init(const char *name)
{
    TRACE_FUNC_ENTER();
    (void)name;
    MBOOL ret = MFALSE;

    mTPIMgr = TPIMgr::createInstance();
    mTPIMgr->createSession(mSensorIndex, mPipeUsage.getTPMask(), mPipeUsage.getAppSessionMeta());
    mTPIMgr->initSession();
    mPipeUsage.updateTPIUsage(mTPIMgr);
    initNodes();
    mEISQControl.init(mPipeUsage);

    ret = PARENT_PIPE::init();
    mTPIMgr->start();
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::uninit(const char *name)
{
    TRACE_FUNC_ENTER();
    (void)name;
    MBOOL ret;
    mTPIMgr->stop();
    ret = PARENT_PIPE::uninit();

    uninitNodes();
    mTPIMgr->uninitSession();
    mTPIMgr->destroySession();
    TPIMgr::destroyInstance(mTPIMgr);

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::enque(const FeaturePipeParam &param)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if(mPipeUsage.isDynamicTuning() && !param.mP2Pack.isValid())
    {
        MY_LOGE("Dynamic Tuning w/o valid P2Pack!! Directly assert!");
        return MFALSE;
    }
    this->prepareFeatureRequest(param);
    RequestPtr request;
    request = new StreamingFeatureRequest(mPipeUsage, param, mCounter, mRecordCounter, mEISQControl.getCurrentState());
    if(request == NULL)
    {
        MY_LOGE("OOM: Cannot allocate StreamingFeatureRequest");
    }
    else
    {
        request->updateSFPIO();
        request->calSizeInfo();
        request->setDisplayFPSCounter(&mDisplayFPSCounter);
        request->setFrameFPSCounter(&mFrameFPSCounter);
        if( mPipeUsage.supportVendor(2) )
        {
            ENABLE_VENDOR_V2(request->mFeatureMask);
        }
        if( mUsePerFrameSetting )
        {
            this->prepareDebugSetting();
        }
        this->applyMaskOverride(request);
        this->applyVarMapOverride(request);
        mNodeSignal->clearStatus(NodeSignal::STATUS_IN_FLUSH);
        prepareIORequest(request);
        ret = CamPipe::enque(ID_ROOT_ENQUE, request);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::flush()
{
    TRACE_FUNC_ENTER();
    MY_LOGD("Trigger flush");
    mNodeSignal->setStatus(NodeSignal::STATUS_IN_FLUSH);
    if( mPipeUsage.supportEIS_Q() )
    {
        MY_LOGD("Notify EIS: flush begin");
        mEIS.triggerDryRun();
    }
    if (mPipeUsage.supportDPE())
    {
        MY_LOGD("Notify DepthNodeL: Flush");
        mDepth.onFlush();
    }
    CamPipe::sync();
    mEISQControl.reset();
    mWarp.clearTSQ();
    mNodeSignal->clearStatus(NodeSignal::STATUS_IN_FLUSH);
    if( mPipeUsage.supportEIS_Q() )
    {
        MY_LOGD("Notify EIS: flush end");
        mEIS.triggerDryRun();
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::setJpegParam(NSCam::NSIoPipe::NSPostProc::EJpgCmd cmd, int arg1, int arg2)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if( mNormalStream != NULL )
    {
        ret = mNormalStream->setJpegParam(cmd, arg1, arg2);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::setFps(MINT32 fps)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if( mNormalStream != NULL )
    {
        ret = mNormalStream->setFps(fps);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MUINT32 StreamingFeaturePipe::getRegTableSize()
{
    TRACE_FUNC_ENTER();
    MUINT32 ret = 0;
    if( mNormalStream != NULL )
    {
        ret = mNormalStream->getRegTableSize();
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::sendCommand(NSCam::NSIoPipe::NSPostProc::ESDCmd cmd, MINTPTR arg1, MINTPTR arg2, MINTPTR arg3)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if( mNormalStream != NULL )
    {
        ret = mNormalStream->sendCommand(cmd, arg1, arg2, arg3);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::addMultiSensorID(MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;

    if( sensorID < P2CamContext::SENSOR_INDEX_MAX )
    {
        android::Mutex::Autolock lock(mContextMutex);
        if( !mContextCreated[sensorID] )
        {
            P2CamContext::createInstance(sensorID, mPipeUsage);
            mContextCreated[sensorID] = MTRUE;
            ret = MTRUE;
        }
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MVOID StreamingFeaturePipe::sync()
{
    TRACE_FUNC_ENTER();
    MY_LOGD("Sync start");
    CamPipe::sync();
    MY_LOGD("Sync finish");
    TRACE_FUNC_EXIT();
}

IImageBuffer* StreamingFeaturePipe::requestBuffer()
{
    TRACE_FUNC_ENTER();
    IImageBuffer *buffer = NULL;
    buffer = mInputBufferStore.requestBuffer();
    TRACE_FUNC_EXIT();
    return buffer;
}

MBOOL StreamingFeaturePipe::returnBuffer(IImageBuffer *buffer)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    ret = mInputBufferStore.returnBuffer(buffer);
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::onInit()
{
    TRACE_FUNC_ENTER();
    MY_LOGI("+");
    MBOOL ret;
    ret = mEarlyInited &&
          this->prepareDebugSetting() &&
          this->prepareNodeSetting() &&
          this->prepareNodeConnection() &&
          this->prepareIOControl() &&
          this->prepareBuffer() &&
          this->prepareCamContext();

    MY_LOGI("-");
    TRACE_FUNC_EXIT();
    return ret;
}

MVOID StreamingFeaturePipe::onUninit()
{
    TRACE_FUNC_ENTER();
    MY_LOGI("+");
    this->releaseCamContext();
    this->releaseBuffer();
    this->releaseNodeSetting();
    MY_LOGI("-");
    TRACE_FUNC_EXIT();
}

MBOOL StreamingFeaturePipe::onData(DataID, const RequestPtr &)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::earlyInit()
{
    return this->prepareGeneralPipe();
}

MVOID StreamingFeaturePipe::lateUninit()
{
    this->releaseGeneralPipe();
}

MBOOL StreamingFeaturePipe::initNodes()
{
    TRACE_FUNC_ENTER();
    mNodes.push_back(&mRootNode);

    if( mPipeUsage.supportDepthP2() )
    {
        mNodes.push_back(&mDepth);
        if( mPipeUsage.supportBokeh() )     mNodes.push_back(&mBokeh);
    }
    else
    {
        mNodes.push_back(&mP2A);
        mNodes.push_back(&mP2AMDP);
    }

    if( mPipeUsage.supportDummy() )     mNodes.push_back(&mDummy);
    if( mPipeUsage.supportWarpNode() )  mNodes.push_back(&mWarp);
    if( mPipeUsage.supportRSCNode() )   mNodes.push_back(&mRSC);
    if( mPipeUsage.supportEISNode() )   mNodes.push_back(&mEIS);
    if( mPipeUsage.supportVendor(1) )   mNodes.push_back(&mVendor);
    if( mPipeUsage.supportFOV())
    {
        mNodes.push_back(&mFOV);
        mNodes.push_back(&mFOVWarp);
    }
    if( mPipeUsage.supportN3D())
    {
        mNodes.push_back(&mN3D_P2Node);
        mNodes.push_back(&mN3D);
    }
    if( mPipeUsage.supportVendor(2) )
    {
        MUINT32 count = mPipeUsage.getTPINodeCount();
        mTPIs.clear();
        mTPIs.reserve(count);
        char name[32];
        for( MUINT32 i = 0; i < count; ++i )
        {
            snprintf(name, sizeof(name), "fpipe.tpi.%d", i);
            mTPIs.push_back(new TPINode(name, i, mPipeUsage.getTPINodeIO(i), mTPIMgr));
            mNodes.push_back(mTPIs[i]);
        }
    }
    if( mPipeUsage.supportVendor() )    mNodes.push_back(&mVendorMDP);

    mNodes.push_back(&mHelper);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::uninitNodes()
{
    TRACE_FUNC_ENTER();
    for( unsigned i = 0, n = mTPIs.size(); i < n; ++i )
    {
        delete mTPIs[i];
        mTPIs[i] = NULL;
    }
    mTPIs.clear();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::prepareDebugSetting()
{
    TRACE_FUNC_ENTER();

    mForceOnMask = 0;
    mForceOffMask = ~0;

    #define CHECK_DEBUG_MASK(name)                                          \
    {                                                                       \
        MINT32 prop = getPropertyValue(KEY_FORCE_##name, VAL_FORCE_##name); \
        if( prop == FORCE_ON )    ENABLE_##name(mForceOnMask);              \
        if( prop == FORCE_OFF )   DISABLE_##name(mForceOffMask);            \
    }
    CHECK_DEBUG_MASK(EIS);
    CHECK_DEBUG_MASK(EIS_25);
    CHECK_DEBUG_MASK(EIS_30);
    CHECK_DEBUG_MASK(EIS_QUEUE);
    CHECK_DEBUG_MASK(3DNR);
    CHECK_DEBUG_MASK(VHDR);
    CHECK_DEBUG_MASK(VENDOR_V1);
    CHECK_DEBUG_MASK(VENDOR_V2);
    CHECK_DEBUG_MASK(DUMMY);
    #undef CHECK_DEBUG_SETTING

    mDebugDump = getPropertyValue(KEY_DEBUG_DUMP, VAL_DEBUG_DUMP);
    mDebugDumpCount = getPropertyValue(KEY_DEBUG_DUMP_COUNT, VAL_DEBUG_DUMP_COUNT);
    mDebugDumpByRecordNo = getPropertyValue(KEY_DEBUG_DUMP_BY_RECORDNO, VAL_DEBUG_DUMP_BY_RECORDNO);
    mForceIMG3O = getPropertyValue(KEY_FORCE_IMG3O, VAL_FORCE_IMG3O);
    mForceWarpPass = getPropertyValue(KEY_FORCE_WARP_PASS, VAL_FORCE_WARP_PASS);
    mForceGpuOut = getPropertyValue(KEY_FORCE_GPU_OUT, VAL_FORCE_GPU_OUT);
    mForceGpuRGBA = getPropertyValue(KEY_FORCE_GPU_RGBA, VAL_FORCE_GPU_RGBA);
    mUsePerFrameSetting = getPropertyValue(KEY_USE_PER_FRAME_SETTING, VAL_USE_PER_FRAME_SETTING);
    mForcePrintIO = getPropertyValue(KEY_FORCE_PRINT_IO, VAL_FORCE_PRINT_IO);

    if( !mPipeUsage.support3DNR() )
    {
        DISABLE_3DNR(mForceOffMask);
    }
    if( !mPipeUsage.supportEISNode() )
    {
        DISABLE_EIS(mForceOffMask);
    }
    if( !mPipeUsage.supportEIS_30() )
    {
        DISABLE_EIS_30(mForceOffMask);
    }
    if( !mPipeUsage.supportEIS_25() )
    {
        DISABLE_EIS_25(mForceOffMask);
    }
    if( !mPipeUsage.supportEISNode() || !mPipeUsage.supportEIS_Q() )
    {
        DISABLE_EIS_QUEUE(mForceOffMask);
    }
    if( !mPipeUsage.supportVendor(1) )
    {
        DISABLE_VENDOR_V1(mForceOffMask);
    }
    if( !mPipeUsage.supportVendor(2) )
    {
        DISABLE_VENDOR_V2(mForceOffMask);
    }

    MY_LOGD("forceOnMask=0x%04x, forceOffMask=0x%04x", mForceOnMask, ~mForceOffMask);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::prepareGeneralPipe()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    P2_CAM_TRACE_CALL(TRACE_DEFAULT);
    std::map<NSIoPipe::EDIPInfoEnum, MUINT32> dipInfo;
    if( !INormalStream::queryDIPInfo(dipInfo) )
    {
        MY_LOGE("queryDIPInfo fail!, assume it's dip 40");
        mDipVersion = NSIoPipe::EDIPHWVersion_40;
    }
    else
    {
        mDipVersion = dipInfo[NSIoPipe::EDIPINFO_DIPVERSION];
    }

    if( !mPipeUsage.supportBypassP2A() )
    {
        mNormalStream = INormalStream::createInstance(mSensorIndex);
        if( mNormalStream != NULL )
        {
            MBOOL secFlag = mPipeUsage.isSecureP2();
            ret = mNormalStream->init(NORMAL_STREAM_NAME, secFlag);
        }
        else
        {
            ret = MFALSE;
        }
        mP2A.setNormalStream(mNormalStream, mDipVersion);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL StreamingFeaturePipe::prepareNodeSetting()
{
    TRACE_FUNC_ENTER();
    NODE_LIST::iterator it, end;
    for( it = mNodes.begin(), end = mNodes.end(); it != end; ++it )
    {
        (*it)->setSensorIndex(mSensorIndex);
        (*it)->setPipeUsage(mPipeUsage);
        (*it)->setNodeSignal(mNodeSignal);
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::prepareNodeConnection()
{
    TRACE_FUNC_ENTER();
    if( mPipeUsage.supportDepthP2() )
    {
        this->connectData(ID_ROOT_TO_DEPTH, mRootNode, mDepth);
        if( mPipeUsage.supportBokeh() )
        {
            this->connectData(ID_DEPTH_TO_BOKEH, mDepth, mBokeh);
            this->connectData(ID_BOKEH_TO_HELPER, mBokeh, mHelper);
        }
    }
    else
    {
        this->connectData(ID_ROOT_TO_P2A, mRootNode, mP2A);
        this->connectData(ID_P2A_TO_HELPER, mP2A, mHelper);
        this->connectData(ID_P2A_TO_PMDP, mP2A, mP2AMDP);
        this->connectData(ID_PMDP_TO_HELPER, mP2AMDP, mHelper);
    }

    if( mPipeUsage.supportWarpNode() )
    {
        this->connectData(ID_P2A_TO_WARP_FULLIMG, mP2A, mWarp);
        this->connectData(ID_WARP_TO_HELPER, mWarp, mHelper, CONNECTION_SEQUENTIAL);
    }

    // EIS nodes
    if( mPipeUsage.supportEIS_22() )
    {
        this->connectData(ID_P2A_TO_EIS_P2DONE, mP2A, mEIS);
        this->connectData(ID_EIS_TO_WARP, mEIS, mWarp, CONNECTION_SEQUENTIAL);
    }
    else if( mPipeUsage.supportEIS_25() )
    {
        this->connectData(ID_P2A_TO_EIS_P2DONE, mP2A, mEIS);
        this->connectData(ID_EIS_TO_WARP, mEIS, mWarp, CONNECTION_SEQUENTIAL);
    }
    else if( mPipeUsage.supportEIS_30() )
    {
        this->connectData(ID_P2A_TO_EIS_P2DONE, mP2A, mEIS);
        this->connectData(ID_EIS_TO_WARP, mEIS, mWarp, CONNECTION_SEQUENTIAL);
        if( mPipeUsage.supportRSCNode() )
        {
            this->connectData(ID_RSC_TO_EIS, mRSC, mEIS, CONNECTION_SEQUENTIAL);
        }
    }

    if( mPipeUsage.supportRSCNode() )
    {
        this->connectData(ID_ROOT_TO_RSC, mRootNode, mRSC);
        this->connectData(ID_RSC_TO_HELPER, mRSC, mHelper, CONNECTION_SEQUENTIAL);
        if( mPipeUsage.support3DNRRSC() )
        {
            this->connectData(ID_RSC_TO_P2A, mRSC, mP2A, CONNECTION_SEQUENTIAL);
        }
    }

    if( mPipeUsage.supportVendor() )
    {
        StreamingFeatureNode *vNode = &mVendor;
        if( !mPipeUsage.supportVendor(1) && mPipeUsage.supportVendor(2) )
        {
            vNode = mTPIs[0];
        }

        if( mPipeUsage.supportDepthP2() )
        {
            if( mPipeUsage.supportBokeh() )
            {
                this->connectData(ID_BOKEH_TO_VENDOR_FULLIMG, mBokeh, *vNode);
            }
            else
            {
                this->connectData(ID_DEPTH_TO_VENDOR, mDepth, *vNode);
            }
        }
        else
        {
            this->connectData(ID_P2A_TO_VENDOR_FULLIMG, mP2A, *vNode);
        }

        if( mPipeUsage.supportVendor(1) && mPipeUsage.supportVendor(2) )
        {
            this->connectData(ID_VENDOR_TO_NEXT, mVendor, *mTPIs[0]);
        }
        else if( mPipeUsage.supportVendor(1) )
        {
            this->connectData(ID_VENDOR_TO_NEXT, mVendor, mVendorMDP);
        }
        else
        {
            this->connectData(ID_P2A_TO_VENDOR_FULLIMG, mP2A, *mTPIs[0]);
        }

        if( mPipeUsage.supportVendor(2) )
        {
            unsigned n = mPipeUsage.getTPINodeCount();
            for( unsigned i = 1; i < n; ++i )
            {
                this->connectData(ID_VENDOR_TO_NEXT, *mTPIs[i-1], *mTPIs[i]);
            }
            this->connectData(ID_VENDOR_TO_NEXT, *mTPIs[n-1], mVendorMDP);
        }

        if( mPipeUsage.supportWarpNode() )
        {
            this->connectData(ID_VMDP_TO_NEXT_FULLIMG, mVendorMDP, mWarp);
        }
        this->connectData(ID_VMDP_TO_HELPER, mVendorMDP, mHelper);
    }

    if(mPipeUsage.supportFOV())
    {
        this->connectData(ID_P2A_TO_FOV_FEFM, mP2A, mFOV, CONNECTION_SEQUENTIAL);
        this->connectData(ID_P2A_TO_FOV_FULLIMG, mP2A, mFOV, CONNECTION_SEQUENTIAL);
        this->connectData(ID_P2A_TO_FOV_WARP, mP2A, mFOVWarp, CONNECTION_SEQUENTIAL);
        //
        this->connectData(ID_FOV_TO_FOV_WARP, mFOV, mFOVWarp, CONNECTION_SEQUENTIAL);
        this->connectData(ID_FOV_WARP_TO_HELPER, mFOVWarp, mHelper, CONNECTION_SEQUENTIAL);
        if( mPipeUsage.supportVendor(1) )
        {
            this->connectData(ID_FOV_WARP_TO_VENDOR, mFOVWarp, mVendor, CONNECTION_SEQUENTIAL);
        }
        else if( mPipeUsage.supportVendor(2) )
        {
            this->connectData(ID_FOV_WARP_TO_VENDOR, mFOVWarp, *mTPIs[0], CONNECTION_SEQUENTIAL);
        }
        if( mPipeUsage.supportEIS_30() )
        {
            this->connectData(ID_FOV_TO_EIS_WARP, mFOV, mEIS, CONNECTION_SEQUENTIAL);
        }
        if( mPipeUsage.supportWarpNode() )
        {
            this->connectData(ID_FOV_TO_EIS_FULLIMG, mFOVWarp, mWarp, CONNECTION_SEQUENTIAL);
        }
    }
    if( mPipeUsage.supportN3D())
    {
        this->connectData(ID_P2A_TO_N3DP2, mP2A, mN3D_P2Node);
        this->connectData(ID_N3DP2_TO_N3D, mN3D_P2Node, mN3D);
        this->connectData(ID_N3D_TO_HELPER, mN3D, mHelper);
        if( mPipeUsage.supportVendor() )
        {
            this->connectData(ID_N3D_TO_VMDP, mN3D, mVendorMDP);
        }
    }

    this->setRootNode(&mRootNode);
    mRootNode.registerInputDataID(ID_ROOT_ENQUE);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::prepareIOControl()
{
    TRACE_FUNC_ENTER();

    StreamingFeatureNode *rootN = &mP2A;
    if( mPipeUsage.supportDepthP2() )
    {
        rootN = &mDepth;
    }

    mRecordPath.push_back(rootN);
    mDisplayPath.push_back(rootN);
    mPhysicalPath.push_back(rootN);

    if( mPipeUsage.supportBokeh() )
    {
        mRecordPath.push_back(&mBokeh);
        mDisplayPath.push_back(&mBokeh);
    }
    if( mPipeUsage.supportDummy() )
    {
        mRecordPath.push_back(&mDummy);
        mDisplayPath.push_back(&mDummy);
    }
    if( mPipeUsage.supportFOV() )
    {
        mRecordPath.push_back(&mFOVWarp);
        mDisplayPath.push_back(&mFOVWarp);
    }
    if( mPipeUsage.supportVendor(1) )
    {
        mRecordPath.push_back(&mVendor);
        mDisplayPath.push_back(&mVendor);
    }
    if( mPipeUsage.supportVendor(2) )
    {
        mRecordPath.push_back(mTPIs[0]);
        mDisplayPath.push_back(mTPIs[0]);
    }
    if( mPipeUsage.supportVendor() )
    {
        mRecordPath.push_back(&mVendorMDP);
        mDisplayPath.push_back(&mVendorMDP);
    }
    if( mPipeUsage.supportWarpNode() )
    {
        mRecordPath.push_back(&mWarp);
    }

    mIOControl.setRoot(rootN);
    mIOControl.addStream(STREAMTYPE_PREVIEW, mDisplayPath);
    mIOControl.addStream(STREAMTYPE_RECORD, mRecordPath);
    mIOControl.addStream(STREAMTYPE_PREVIEW_CALLBACK, mDisplayPath);
    mIOControl.addStream(STREAMTYPE_PHYSICAL, mPhysicalPath);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL StreamingFeaturePipe::prepareBuffer()
{
    TRACE_FUNC_ENTER();

    MSize fullSize(MAX_FULL_WIDTH, MAX_FULL_HEIGHT);
    MSize streamingSize = mPipeUsage.getStreamingSize();
    EImageFormat fullFormat = mPipeUsage.getFullImgFormat();

    if( streamingSize.w > 0 && streamingSize.h > 0 )
    {
        // align 64
        fullSize.w = align(streamingSize.w, 6);
        fullSize.h = align(streamingSize.h, 6);
    }

    MY_LOGD("sensor(%d) StreamingSize=(%dx%d) align64=(%dx%d)", mSensorIndex, streamingSize.w, streamingSize.h, fullSize.w, fullSize.h);

    if( mPipeUsage.supportP2AFeature() || mPipeUsage.supportDepthP2() ||
        mPipeUsage.supportYUVIn() || mPipeUsage.supportVendor() )
    {
        if( mPipeUsage.supportDepthP2() )
        {
            mDepthYuvOutPool = createFullImgPool("fpipe.depthOutImg", fullSize);
            if( mPipeUsage.supportBokeh() )
            {
                mBokehOutPool = createFullImgPool("fpipe.bokehOutImg", fullSize);
            }
        }
        else
        {
            mFullImgPool = createFullImgPool("fpipe.fullImg", fullSize);
        }
        if( mPipeUsage.supportPure() )
        {
            createPureImgPools("fpipe.p2aPureImg", fullSize);
        }
        if( mPipeUsage.supportDummy() )
        {
            mDummyImgPool = createFullImgPool("fpipe.dummyImg", fullSize);
        }
        if( mPipeUsage.supportVendor(1) )
        {
            mVendorInPool = createImgPool("fpipe.vendorIn", mPipeUsage.getVendorCusSize(fullSize), mPipeUsage.getVendorCusFormat(fullFormat));
            mVendorOutPool = createImgPool("fpipe.vendorOut", fullSize, mPipeUsage.getVendorCusFormat(fullFormat));
        }
        if( mPipeUsage.supportVendor(2) )
        {
            MSize tpiSize = mPipeUsage.getVendorCusSize(fullSize);
            EImageFormat tpiFmt = mPipeUsage.getVendorCusFormat(fullFormat);
            mTPIInPool = createImgPool("fpipe.tpiIn", tpiSize, tpiFmt);
            mTPIOutPool = createImgPool("fpipe.tpiOut", tpiSize, tpiFmt);
        }
    }

    if( mPipeUsage.supportWarpNode() )
    {
        MUINT32 eis_factor = mPipeUsage.getEISFactor();

        MUINT32 modifyW = fullSize.w;
        MUINT32 modifyH = fullSize.h;

        if( mPipeUsage.supportWarpCrop() )
        {
            modifyW = fullSize.w*100.0f/eis_factor;
            modifyH = fullSize.h*100.0f/eis_factor;
        }

        // align 32
        modifyW = align(modifyW, 5);
        modifyH = align(modifyH, 5);

        mWarpOutputPool = createWarpOutputPool("fpipe.warpOut", MSize(modifyW, modifyH));
        mEisFullImgPool = createFullImgPool("fpipe.eisFull", fullSize);
    }
    if( mPipeUsage.supportDepthP2() )
    {
        mDepth.setOutputBufferPool(mDepthYuvOutPool, mPipeUsage.getNumDepthImgBuffer());
        if(mPipeUsage.supportBokeh())
        {
            mBokeh.setOutputBufferPool(mBokehOutPool, mPipeUsage.getNumBokehOutBuffer());
        }
    }
    else
    {
        mP2A.setFullImgPool(mFullImgPool, mPipeUsage.getNumP2ABuffer());
        if(mPipeUsage.supportPure())
        {
            mP2A.setPureImgPool(mPureImgPoolMap);
        }
    }

    if( mPipeUsage.supportWarpNode() )
    {
        mWarp.setInputBufferPool(mEisFullImgPool);
        mWarp.setOutputBufferPool(mWarpOutputPool);
    }
    if( mPipeUsage.supportDummy() )
    {
        mDummy.setOutputBufferPool(mDummyImgPool, mPipeUsage.getNumVendorOutBuffer());
    }
    if( mPipeUsage.supportVendor(1) )
    {
        mVendor.setInputBufferPool(mVendorInPool, mPipeUsage.getNumVendorInBuffer());
        mVendor.setOutputBufferPool(mVendorOutPool, mPipeUsage.getNumVendorOutBuffer());
    }
    if( mPipeUsage.supportVendor(2) )
    {
        mTPIs[0]->setInputBufferPool(mTPIInPool, mPipeUsage.getNumTPIInBuffer());
        mTPIs[0]->setOutputBufferPool(mTPIOutPool, mPipeUsage.getNumTPIOutBuffer());
    }

    if( mPipeUsage.supportFOV() &&
        mPipeUsage.supportVendor() )
    {
        mFOVWarpOutputPool = createFullImgPool("fpipe.fovWarpOut", fullSize);
        mFOVWarp.setOutputBufferPool(mFOVWarpOutputPool);
    }
    mInputBufferStore.init(mFullImgPool);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

android::sp<IBufferPool> StreamingFeaturePipe::createFullImgPool(const char* name, MSize size)
{
    TRACE_FUNC_ENTER();

    android::sp<IBufferPool> fullImgPool;
    EImageFormat format = mPipeUsage.getFullImgFormat();
    if( mPipeUsage.supportGraphicBuffer() )
    {
        NativeBufferWrapper::ColorSpace color;
        color = mPipeUsage.supportEISNode() ?
                NativeBufferWrapper::YUV_BT601_FULL :
                NativeBufferWrapper::NOT_SET;

        android_pixel_format_t pixelFmt = toPixelFormat(format);
        fullImgPool = GraphicBufferPool::create(name, size.w, size.h, pixelFmt, GraphicBufferPool::USAGE_HW_TEXTURE, color);
    }
    else
    {
        fullImgPool = ImageBufferPool::create(name, size.w, size.h, format, ImageBufferPool::USAGE_HW );
    }

    TRACE_FUNC_EXIT();

    return fullImgPool;
}

MVOID StreamingFeaturePipe::createPureImgPools(const char* name, MSize size)
{
    TRACE_FUNC_ENTER();
    for(MUINT32 sId : mAllSensorIDs)
    {
        // TODO format need sync
        mPureImgPoolMap[sId] = createFullImgPool(name, size);
    }
    TRACE_FUNC_EXIT();
}

android::sp<IBufferPool> StreamingFeaturePipe::createImgPool(const char* name, MSize size, EImageFormat fmt)
{
    TRACE_FUNC_ENTER();

    android::sp<IBufferPool> pool;
    if( mPipeUsage.supportGraphicBuffer() )
    {
        android_pixel_format_t pixelFormat = toPixelFormat(fmt);
        pool = GraphicBufferPool::create(name, size.w, size.h, pixelFormat, GraphicBufferPool::USAGE_HW_TEXTURE);
    }
    else
    {
        pool = ImageBufferPool::create(name, size.w, size.h, fmt, ImageBufferPool::USAGE_HW );
    }

    TRACE_FUNC_EXIT();
    return pool;
}

android::sp<IBufferPool> StreamingFeaturePipe::createWarpOutputPool(const char* name, MSize size)
{
    TRACE_FUNC_ENTER();

    android::sp<IBufferPool> warpOutputPool;

    if( mPipeUsage.supportGraphicBuffer() )
    {
        android_pixel_format_t warpOutFmt = HAL_PIXEL_FORMAT_YV12;

        if( mPipeUsage.supportWPE() )
        {
            warpOutFmt = HAL_PIXEL_FORMAT_YCbCr_422_I;
        }
        else // GPU
        {
            warpOutFmt = mForceGpuRGBA ? HAL_PIXEL_FORMAT_RGBA_8888 : HAL_PIXEL_FORMAT_YV12;
        }

        MY_LOGD("sensor(%d) %s outsize=(%dx%d) format(%d) GraphicBuffer", mSensorIndex, mPipeUsage.supportWPE() ? "WPE" : "GPU", size.w, size.h, warpOutFmt);

        warpOutputPool = GraphicBufferPool::create(name, size.w, size.h, warpOutFmt, GraphicBufferPool::USAGE_HW_RENDER);
    }
    else
    {
        MY_LOGD("sensor(%d) WPE outsize=(%dx%d) format(%d) ImageBuffer", mSensorIndex, size.w, size.h, eImgFmt_YUY2);

        warpOutputPool = ImageBufferPool::create(name, size.w, size.h, eImgFmt_YUY2, ImageBufferPool::USAGE_HW);
    }

    TRACE_FUNC_EXIT();

    return warpOutputPool;
}

MVOID StreamingFeaturePipe::releaseNodeSetting()
{
    TRACE_FUNC_ENTER();
    this->disconnect();
    mDisplayPath.clear();
    mRecordPath.clear();
    mPhysicalPath.clear();
    TRACE_FUNC_EXIT();
}

MVOID StreamingFeaturePipe::releaseGeneralPipe()
{
    P2_CAM_TRACE_CALL(TRACE_DEFAULT);
    TRACE_FUNC_ENTER();
    mP2A.setNormalStream(NULL, mDipVersion);
    if( mNormalStream )
    {
        mNormalStream->uninit(NORMAL_STREAM_NAME);
        mNormalStream->destroyInstance();
        mNormalStream = NULL;
    }
    TRACE_FUNC_EXIT();
}

MVOID StreamingFeaturePipe::releaseBuffer()
{
    TRACE_FUNC_ENTER();

    mP2A.setFullImgPool(NULL);
    mDummy.setOutputBufferPool(NULL);
    mWarp.setInputBufferPool(NULL);
    mWarp.setOutputBufferPool(NULL);
    mVendor.setInputBufferPool(NULL);
    mVendor.setOutputBufferPool(NULL);
    if( mTPIs.size() && mTPIs[0] != NULL )
    {
        mTPIs[0]->setInputBufferPool(NULL);
        mTPIs[0]->setOutputBufferPool(NULL);
    }
    mFOVWarp.setOutputBufferPool(NULL);

    mInputBufferStore.uninit();

    IBufferPool::destroy(mFullImgPool);
    IBufferPool::destroy(mDepthYuvOutPool);
    IBufferPool::destroy(mBokehOutPool);
    IBufferPool::destroy(mDummyImgPool);
    IBufferPool::destroy(mVendorInPool);
    IBufferPool::destroy(mVendorOutPool);
    IBufferPool::destroy(mTPIInPool);
    IBufferPool::destroy(mTPIOutPool);
    IBufferPool::destroy(mEisFullImgPool);
    IBufferPool::destroy(mWarpOutputPool);
    IBufferPool::destroy(mFOVWarpOutputPool);

    TRACE_FUNC_EXIT();
}

MVOID StreamingFeaturePipe::applyMaskOverride(const RequestPtr &request)
{
    TRACE_FUNC_ENTER();
    request->mFeatureMask |= mForceOnMask;
    request->mFeatureMask &= mForceOffMask;
    request->setDumpProp(mDebugDump, mDebugDumpCount, mDebugDumpByRecordNo);
    request->setForceIMG3O(mForceIMG3O);
    request->setForceWarpPass(mForceWarpPass);
    request->setForceGpuOut(mForceGpuOut);
    request->setForceGpuRGBA(mForceGpuRGBA);
    request->setForcePrintIO(mForcePrintIO);
    TRACE_FUNC_EXIT();
}

MVOID StreamingFeaturePipe::applyVarMapOverride(const RequestPtr &request)
{
    TRACE_FUNC_ENTER();
    (void)(request);
    TRACE_FUNC_EXIT();
}

MBOOL StreamingFeaturePipe::prepareCamContext()
{
    TRACE_FUNC_ENTER();
    for(auto&& id : mAllSensorIDs)
    {
        addMultiSensorID(id);
    }

    TRACE_FUNC_EXIT();

    return MTRUE;
}

MVOID StreamingFeaturePipe::prepareFeatureRequest(const FeaturePipeParam &param)
{
    ++mCounter;
    eAppMode appMode = param.getVar<eAppMode>(VAR_APP_MODE, APP_PHOTO_PREVIEW);
    if( appMode == APP_VIDEO_RECORD ||
        appMode == APP_VIDEO_STOP )
    {
        ++mRecordCounter;
    }
    else if( mRecordCounter )
    {
        MY_LOGI("Set Record Counter %d=>0. AppMode=%d", mRecordCounter, appMode);
        mRecordCounter = 0;
    }
    this->prepareEISQControl(param);
    TRACE_FUNC("Request=%d, Record=%d, AppMode=%d", mCounter, mRecordCounter, appMode);
}

MVOID StreamingFeaturePipe::prepareEISQControl(const FeaturePipeParam &param)
{
    EISQActionInfo info;
    info.mAppMode = param.getVar<eAppMode>(VAR_APP_MODE, APP_PHOTO_PREVIEW);
    info.mRecordCount = mRecordCounter;
    info.mIsAppEIS = HAS_EIS(param.mFeatureMask);
    if(mPipeUsage.isQParamIOValid())
    {
        NSCam::NSIoPipe::Output record;
        info.mIsReady = getOutBuffer(param.getQParams(), IO_TYPE_RECORD, record);
    }
    else
    {
        info.mIsReady = existOutBuffer(param.mSFPIOManager.getGeneralIOs(), IO_TYPE_RECORD);
    }
    mEISQControl.update(info);

    TRACE_FUNC("AppMode=%d, Record=%d, AppEIS=%d, IsRecordBuffer=%d",
               info.mAppMode, info.mRecordCount, info.mIsAppEIS, info.mIsReady);
}

MVOID StreamingFeaturePipe::prepareIORequest(const RequestPtr &request)
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    TRACE_FUNC_ENTER();
    {
        android::String8 dumpStr;
        request->mSFPIOManager.appendDumpInfo(dumpStr);

        MY_S_LOGD(request->mLog, "master/slave(%d/%d) ReqNo(%d), feature=0x%04x(%s), SFPIOMgr:%s",
                request->getMasterID(), request->mSlaveID, request->mRequestNo,
                request->mFeatureMask, request->getFeatureMaskName(), dumpStr.c_str());
    }

    std::set<StreamType> generalStreams;
    if( request->hasDisplayOutput() )
    {
        generalStreams.insert(STREAMTYPE_PREVIEW);
    }
    if( request->hasRecordOutput() )
    {
        generalStreams.insert(STREAMTYPE_RECORD);
    }
    if( request->hasExtraOutput() )
    {
        generalStreams.insert(STREAMTYPE_PREVIEW_CALLBACK);
    }

    {// Master
        prepareIORequest(request, generalStreams, request->mMasterID);
    }
    if(request->hasSlave(request->mSlaveID))
    {
        prepareIORequest(request, generalStreams, request->mSlaveID);
    }

    TRACE_FUNC_EXIT();
}

MVOID StreamingFeaturePipe::prepareIORequest(const RequestPtr &request, std::set<StreamType> &generalStreams, MUINT32 sensorID)
{
    std::set<StreamType> streams = generalStreams;
    if( request->hasPhysicalOutput(sensorID) )
    {
        streams.insert(STREAMTYPE_PHYSICAL);
    }
    StreamingReqInfo reqInfo(request->mRequestNo, request->mFeatureMask, request->mMasterID, sensorID);
    IORequest<StreamingFeatureNode, StreamingReqInfo> &ioReq = request->mIORequestMap[sensorID];
    mIOControl.prepareMap(streams, reqInfo, ioReq.mOutMap, ioReq.mBufMap);

    if( request->needPrintIO() )
    {
        MY_LOGD("IOUtil ReqInfo : %s", reqInfo.dump());
        mIOControl.printMap(ioReq.mOutMap);
        mIOControl.dumpInfo(ioReq.mOutMap);
        mIOControl.dumpInfo(ioReq.mBufMap);
    }
}

MVOID StreamingFeaturePipe::releaseCamContext()
{
    TRACE_FUNC_ENTER();

    android::Mutex::Autolock lock(mContextMutex);
    for( int i = 0; i < P2CamContext::SENSOR_INDEX_MAX; i++ )
    {
        if( mContextCreated[i] )
        {
            P2CamContext::destroyInstance(i);
            mContextCreated[i] = MFALSE;
        }
    }

    TRACE_FUNC_EXIT();
}

} // NSFeaturePipe
} // NSCamFeature
} // NSCam
