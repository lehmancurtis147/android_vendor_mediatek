/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "TPIMgr_PluginWrapper.h"
#include "TPIMgr_Util.h"

#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>

#include "DebugControl.h"
#define PIPE_CLASS_TAG "TPIMGR_Plugin"
#define PIPE_TRACE TRACE_TPI_MGR_PLUGIN
#include <featurePipe/core/include/PipeLog.h>

using namespace NSCam::NSPipelinePlugin;

using NSCam::NSPipelinePlugin::MetadataHandle;
using NSCam::NSPipelinePlugin::BufferHandle;
typedef MetadataHandle::Ptr T_MetaHandlePtr;
typedef BufferHandle::Ptr T_BufferHandlePtr;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

void TPIMgr_PluginWrapper::initProperty(T_Property &prop)
{
    prop.mName = "unknown";
    prop.mFeatures = 0;
}

void initFormat(NSPipelinePlugin::BufferSelection &sel)
{
    sel.addSupportFormat(eImgFmt_YV12);
    sel.addSupportFormat(eImgFmt_NV12);
    sel.addSupportFormat(eImgFmt_NV21);
}

void TPIMgr_PluginWrapper::initSelection(T_Selection &sel, const std::shared_ptr<NSCam::IMetadata> &meta)
{
    if( sel != NULL )
    {
        sel->mSelStage = NSPipelinePlugin::eSelStage_CFG;
        sel->mCfgOrder = 0;
        sel->mCfgJoinEntry = NSPipelinePlugin::eJoinEntry_S_YUV;
        sel->mCfgInplace = false;
        sel->mCfgEnableFD = 0;
        sel->mCfgRun = false;
        sel->mP2Run = false;

        initFormat(sel->mIBufferMain1);
        initFormat(sel->mIBufferMain2);
        initFormat(sel->mOBufferMain1);
        initFormat(sel->mOBufferMain2);

        sel->mIMetadataApp.setControl(meta);
    }
}

void TPIMgr_PluginWrapper::print(const T_Property &prop)
{
    MY_LOGD("name=[%s] feature=[0x%016" PRIx64 "]", prop.mName, prop.mFeatures);
}

void TPIMgr_PluginWrapper::print(const T_Selection &sel)
{
    if( sel == NULL )
    {
        MY_LOGD("sel=NULL");
    }
    else
    {
        MY_LOGD("mSelStage=%d", sel->mSelStage);
        MY_LOGD("mCfgOrder=%d", sel->mCfgOrder);
        MY_LOGD("mCfgJoinEntry=%d", sel->mCfgJoinEntry);
        MY_LOGD("mCfgInplace=%d", sel->mCfgInplace);
        MY_LOGD("mCfgEnableFD=%d", sel->mCfgEnableFD);
        MY_LOGD("mCfgRun=%d", sel->mCfgRun);
        MY_LOGD("mP2Run=%d", sel->mP2Run);

        print(sel->mIBufferMain1, "in main1");
        print(sel->mIBufferMain2, "in main2");
        print(sel->mIBufferDepth, "in depth");
        print(sel->mOBufferMain1, "out main1");
        print(sel->mOBufferMain1, "out main2");
        print(sel->mOBufferDepth, "out depth");
    }
}

void TPIMgr_PluginWrapper::print(const NSPipelinePlugin::BufferSelection &sel, const char *name)
{
    if( sel.getRequired() )
    {
        std::vector<MINT> fmts = sel.getFormats();
        MINT fmt = fmts.size() ? fmts[0] : eImgFmt_UNKNOWN;
        MSize size = sel.getSpecifiedSize();
        MUINT32 alignW = 0, alignH = 0;
        sel.getAlignment(alignW, alignH);

        MY_LOGD("[%s] size(%d,%d) align(%d,%d) fmt(count=%zu, first=%d)", name, size.w, size.h, alignW, alignH, fmts.size(), fmt);
    }
}

bool TPIMgr_PluginWrapper::checkDup(const std::list<T_NodeID> &list, PluginMap &pluginMap)
{
    bool ret = true;
    if( list.size() )
    {
        unsigned last = pluginMap[list.front()].mSelection->mCfgOrder;
        for( auto id : list )
        {
            unsigned order = pluginMap[id].mSelection->mCfgOrder;
            if( last == order )
            {
                ret = false;
            }
            last = order;
        }
    }
    return ret;
}

void TPIMgr_PluginWrapper::print(const char *name, const std::list<T_NodeID> &list, PluginMap &pluginMap)
{
    MY_LOGD("path %s", name);
    for( auto id : list )
    {
        unsigned order = pluginMap[id].mSelection->mCfgOrder;
        MY_LOGD("id=%d order=%d", id, order);
    }
}

class MyBufferHandle : public BufferHandle
{
public:
    MyBufferHandle() : BufferHandle() {}
    MyBufferHandle(const TPI_Buffer &img) : BufferHandle(),
        mData(img.mBufferPtr) {}
    virtual ~MyBufferHandle() {}
    NSCam::IImageBuffer* acquire(MINT) { return mData; }
    MVOID release() {}
    MVOID dump(std::ostream&) const {}
private:
    IImageBuffer *mData = NULL;
};

class MyMetaHandle : public MetadataHandle
{
public:
    MyMetaHandle() : MetadataHandle() {}
    MyMetaHandle(const TPI_Meta &meta) : MetadataHandle(),
        mData(meta.mMetaPtr) {}
    virtual ~MyMetaHandle() {}
    NSCam::IMetadata* acquire() { return mData; }
    MVOID release() {}
    MVOID dump(std::ostream&) const {}
private:
    IMetadata *mData = NULL;
};

T_BufferHandlePtr makeBufferHandle(const TPI_Buffer &buffer)
{
    return std::make_shared<MyBufferHandle>(buffer);
}

T_MetaHandlePtr makeMetaHandle(const TPI_Meta &meta)
{
    return std::make_shared<MyMetaHandle>(meta);
}

TPIMgr_PluginWrapper::PluginInfo::PluginInfo()
{
    initProperty(mProperty);
    initSelection(mSelection, NULL);
}

TPIMgr_PluginWrapper::PluginInfo::PluginInfo(const T_Plugin &plugin, const T_Property &prop, const T_Selection &selection)
    : mPlugin(plugin)
    , mProperty(prop)
    , mSelection(selection)
{
}

TPIMgr_PluginWrapper::TPIMgr_PluginWrapper()
  : TPIMgr()
{
}

TPIMgr_PluginWrapper::~TPIMgr_PluginWrapper()
{
  if( mState == STATE_RUN )
  {
    MY_LOGD("User should call stop before exit");
    stop();
  }
  if( mState == STATE_STOP )
  {
    MY_LOGD("User should call uninit before exit");
    uninitSession();
  }
  if( mState == STATE_UNINIT )
  {
    MY_LOGD("User should call destroy before exit");
    destroySession();
  }
}

bool TPIMgr_PluginWrapper::createSession(unsigned sensorID, TPI_SCENARIO_TYPE scenario, const IMetadata &meta)
{
  bool ret = false;
  MY_LOGI("+ scenario=%d", scenario);
  if( mState == TPIMgr_PluginWrapper::STATE_IDLE )
  {
    mSession.mSessionID = generateSessionID();
    mSession.mLogicalSensorID = sensorID;
    mSession.mScenario = scenario;

    createPluginSession(sensorID, ~((TP_MASK_T)0), meta);
    convertPluginToSession();
    NSFeaturePipe::print(mSession);

    mState = TPIMgr_PluginWrapper::STATE_CREATE;
    ret = true;
  }
  MY_LOGI("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::createSession(unsigned sensorID, TP_MASK_T masks, const IMetadata &meta)
{
  bool ret = false;
  MY_LOGI("+ mask=0x%" PRIx64, masks);
  if( mState == TPIMgr_PluginWrapper::STATE_IDLE )
  {
    mSession.mSessionID = generateSessionID();
    mSession.mLogicalSensorID = sensorID;
    mSession.mScenario = TPI_SCENARIO_STREAMING_SINGLE;

    createPluginSession(sensorID, masks, meta);
    convertPluginToSession();
    NSFeaturePipe::print(mSession);

    mState = TPIMgr_PluginWrapper::STATE_CREATE;
    ret = true;
  }
  MY_LOGI("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::destroySession()
{
  bool ret = false;
  MY_LOGI("+");
  if( mState == TPIMgr_PluginWrapper::STATE_UNINIT )
  {
    destroyPluginSession();
    mSession.mSessionCookie = NULL;
    mState = TPIMgr_PluginWrapper::STATE_DESTROY;
    ret = true;
  }
  MY_LOGI("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::initSession()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_CREATE )
  {
    mState = TPIMgr_PluginWrapper::STATE_INIT;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::uninitSession()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_INIT ||
      mState == TPIMgr_PluginWrapper::STATE_STOP )
  {
    mState = TPIMgr_PluginWrapper::STATE_UNINIT;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::initNode(unsigned nodeID)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_INIT )
  {
    T_Plugin plugin = mPluginMap[nodeID].mPlugin;
    if( plugin == NULL )
    {
        MY_LOGE("Invalid node[0x%x]", nodeID);
        ret = false;
    }
    else
    {
        //plugin->init();
        ret = true;
    }
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::uninitNode(unsigned nodeID)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_STOP )
  {
    T_Plugin plugin = mPluginMap[nodeID].mPlugin;
    if( plugin != NULL )
    {
      // plugin->uninit();
      ret = true;
    }
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::start()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_INIT )
  {
    mState = TPIMgr_PluginWrapper::STATE_RUN;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::stop()
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_RUN )
  {
    mState = TPIMgr_PluginWrapper::STATE_STOP;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool TPIMgr_PluginWrapper::genFrame(unsigned &frame)
{
  bool ret = false;
  MY_LOGD("+");
  if( mState == TPIMgr_PluginWrapper::STATE_RUN )
  {
    frame = ++mFrameID;
    ret = true;
  }
  MY_LOGD("- ret=%d frame=%d", ret, frame);
  return ret;
}

bool TPIMgr_PluginWrapper::releaseFrame(unsigned frame)
{
  MY_LOGD("+");
  MY_LOGD("- frame=%d", frame);
  return true;
}

TPIMgr_PluginWrapper::T_Request TPIMgr_PluginWrapper::makeRequest(TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount)
{
    T_Request request = mPluginPool->createRequest();
    for( unsigned i = 0; i < metaCount; ++i )
    {
        fillMeta(request, meta[i]);
    }
    for( unsigned i = 0; i < imgCount; ++i )
    {
        fillBuffer(request, img[i]);
    }
    return request;
}

bool TPIMgr_PluginWrapper::enqueNode(unsigned nodeID, unsigned frame, TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount)
{
  (void)frame;
  bool ret = false;
  TRACE_FUNC("+ node:%d frame:%d", nodeID, frame);
  if( mState == TPIMgr_PluginWrapper::STATE_RUN )
  {
    T_Plugin plugin = mPluginMap[nodeID].mPlugin;
    if( plugin != NULL )
    {
      T_Request request;
      request = makeRequest(meta, metaCount, img, imgCount);
      ret = (plugin->process(request) == OK);
    }
  }
  TRACE_FUNC("- node:%d frame:%d ret:%d", nodeID, frame, ret);
  return ret;
}

bool TPIMgr_PluginWrapper::getSessionInfo(TPI_Session &session) const
{
  bool ret = false;
  if( mState != STATE_IDLE &&
      mState != STATE_DESTROY )
  {
    session = mSession;
    ret = true;
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

unsigned TPIMgr_PluginWrapper::generateSessionID()
{
  static std::atomic<unsigned> sSessionID(0);
  return sSessionID++;
}

bool TPIMgr_PluginWrapper::checkSession(const TPI_Session &session) const
{
  bool ret = true;
  MY_LOGD("+");
  if( session.mMgrVersion != TPI_VERSION ||
      session.mClientVersion != TPI_VERSION )
  {
    MY_LOGE("Invalid API VERSION");
    ret = false;
  }
  else if( session.mScenario == TPI_SCENARIO_UNKNOWN )
  {
    MY_LOGE("Invalid scenario");
    ret = false;
  }
  else if( session.mNodeInfoListCount > TPI_NODE_INFO_LIST_SIZE ||
      session.mPathInfoListCount > TPI_PATH_INFO_LIST_SIZE )
  {
    MY_LOGE("Invalid info list count");
    ret = false;
  }
  for( size_t i = 0; i < session.mNodeInfoListCount; ++i )
  {
    (void)session.mNodeInfoList[i];
  }
  for( size_t i = 0; i < session.mPathInfoListCount; ++i )
  {
    (void)session.mPathInfoList[i];
  }
  MY_LOGD("- ret=%d", ret);
  return ret;
}

bool isDualPlugin(MUINT64 plugin)
{
    return !!(plugin &
              ( MTK_FEATURE_BOKEH | TP_FEATURE_BOKEH |
                MTK_FEATURE_DUAL_YUV | TP_FEATURE_DUAL_YUV |
                MTK_FEATURE_DUAL_HWDEPTH | TP_FEATURE_DUAL_HWDEPTH ));
}

bool isEnableDual(TPI_SCENARIO_TYPE scenario)
{
    return !!(scenario == TPI_SCENARIO_STREAMING_DUAL_BASIC ||
              scenario == TPI_SCENARIO_STREAMING_DUAL_DEPTH );
}

void TPIMgr_PluginWrapper::createPluginSession(unsigned sensorID, TP_MASK_T masks, const IMetadata &meta)
{
    TRACE_FUNC_ENTER();
    mPluginPool = T_PluginType::getInstance(sensorID);
    if( mPluginPool != NULL )
    {
        unsigned id = 0; // must be between 0~1000
        bool enableDual = isEnableDual(mSession.mScenario);
        std::shared_ptr<NSCam::IMetadata> metaPtr = std::make_shared<NSCam::IMetadata>(meta);
        for( auto &plugin : mPluginPool->getProviders(masks) )
        {
            ++id;
            T_Property prop = plugin->property();
            print(prop);
            bool dualPlugin = isDualPlugin(prop.mFeatures);
            bool on = !!(prop.mFeatures&masks);
            MY_LOGI("id=%d on=%d feature=[0x%016" PRIx64 "] dual=%d(enable=%d) meta=%p", id, on, prop.mFeatures, dualPlugin, enableDual, metaPtr.get());
            if( on )
            {
                T_Selection sel = mPluginPool->createSelection();
                initSelection(sel, metaPtr);
                plugin->negotiate(*sel);
                print(sel);
                if( sel->mCfgRun )
                {
                    mPluginMap[id] = PluginInfo(plugin, prop, sel);
                    plugin->init();
                }
            }
        }
    }
    TRACE_FUNC_EXIT();
}

void TPIMgr_PluginWrapper::destroyPluginSession()
{
    for( auto &info : mPluginMap )
    {
        if( info.second.mPlugin != NULL )
        {
            info.second.mPlugin->uninit();
        }
    }
    mPluginMap.clear();
    mPluginPool = NULL;
}

void TPIMgr_PluginWrapper::convertPluginToSession()
{
    std::list<T_NodeID> yuvList;
    std::list<T_NodeID> dispList;

    for( auto &info : mPluginMap )
    {
        switch( info.second.mSelection->mCfgJoinEntry )
        {
        case NSPipelinePlugin::eJoinEntry_S_YUV:
            yuvList.push_back(info.first);
            break;
        case NSPipelinePlugin::eJoinEntry_S_DISP_ONLY:
            dispList.push_back(info.first);
            break;
        default:
            break;
        }
    }

    auto sortMethod = [&](unsigned lhs, unsigned rhs)
                      { return mPluginMap[lhs].mSelection->mCfgOrder <
                               mPluginMap[rhs].mSelection->mCfgOrder; };

    if( yuvList.size() )
    {
        yuvList.sort(sortMethod);
        bool dup = checkDup(yuvList, mPluginMap);
        if( dup )
        {
            MY_LOGW("duplicated order in yuv list, skip all");
            print("yuv", yuvList, mPluginMap);
        }
        else
        {
            addYuvPath(yuvList);
        }
    }
    if( dispList.size() )
    {
        dispList.sort(sortMethod);
        bool dup = checkDup(dispList, mPluginMap);
        if( dup )
        {
            MY_LOGW("duplicated order in disp list, skip all");
            print("disp only", dispList, mPluginMap);
        }
        else
        {
            addDispPath(dispList);
        }
    }
}

void TPIMgr_PluginWrapper::fillMeta(T_Request &request, const TPI_Meta &meta)
{
    switch( meta.mMetaID )
    {
    case TPI_META_ID_MTK_IN_APP:
        request->mIMetadataApp = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_IN_P1_APP:
        request->mIMetadataDynamic1 = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_IN_P1_HAL:
        request->mIMetadataHal1 = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_IN_P1_APP_2:
        request->mIMetadataDynamic2 = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_IN_P1_HAL_2:
        request->mIMetadataHal2 = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_OUT_P2_APP:
        request->mOMetadataApp = makeMetaHandle(meta);
        break;
    case TPI_META_ID_MTK_OUT_P2_HAL:
        request->mOMetadataHal = makeMetaHandle(meta);
        break;
    default:
        break;
    }
}

void TPIMgr_PluginWrapper::fillBuffer(T_Request &request, const TPI_Buffer &img)
{
    switch( img.mBufferID )
    {
    case TPI_BUFFER_ID_MTK_YUV:
        request->mIBufferMain1 = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_YUV_2:
        request->mIBufferMain2 = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_DEPTH:
        request->mIBufferDepth = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_DEPTH_INTENSITY:
        // request->mIBufferDepthIntensity = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_PURE:
        //request->mIBufferMain1 = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_PURE_2:
        //request->mIBufferMain1 = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_OUT_YUV:
        request->mOBufferMain1 = makeBufferHandle(img);
        break;
    case TPI_BUFFER_ID_MTK_OUT_YUV_2:
        request->mOBufferMain2 = makeBufferHandle(img);
        break;
    default:
        break;
    }
}

MBOOL TPIMgr_PluginWrapper::toBufferInfo(TPI_BufferInfo &bufferInfo, const NSPipelinePlugin::BufferSelection &sel)
{
    MBOOL ret = MFALSE;
    if( sel.getRequired() )
    {
        std::vector<MINT> fmts = sel.getFormats();
        bufferInfo.mFormat = fmts.size() ? (NSCam::EImageFormat)fmts[0] : eImgFmt_UNKNOWN;
        bufferInfo.mSize = sel.getSpecifiedSize();
        ret = MTRUE;
    }
    return ret;
}

void TPIMgr_PluginWrapper::addYuvPath(const std::list<unsigned> &genList)
{
    unsigned inID = TPI_NODE_ID_MTK_S_YUV;
    unsigned outID = TPI_NODE_ID_MTK_S_YUV_OUT;
    unsigned lastID = inID;

    TPI_NodeInfo inNode(inID);
    inNode.mCustomOption = TPI_MTK_OPT_STREAMING_NONE;

    TPI_NodeInfo outNode(outID);

    bool isFirst = true;
    bool needOutBuffer = false;
    bool needDepth = false;

    TPI_BufferInfo inYuv(TPI_BUFFER_ID_MTK_OUT_YUV);
    TPI_BufferInfo inDepth(TPI_BUFFER_ID_MTK_OUT_DEPTH);

    for( auto id : genList )
    {
        PluginInfo &info = mPluginMap[id];

        if( isFirst )
        {
            isFirst = false;
            toBufferInfo(inYuv, info.mSelection->mIBufferMain1);
            needDepth = toBufferInfo(inDepth, info.mSelection->mIBufferDepth);
        }

        TPI_NodeInfo tpNode(id);
        if( info.mSelection->mCfgInplace )
        {
            tpNode.mNodeOption |= TPI_NODE_OPT_INPLACE;
        }
        else
        {
            needOutBuffer = true;
        }

        {
            TPI_BufferInfo yuv(TPI_BUFFER_ID_MTK_OUT_YUV);
            yuv.mFormat = inYuv.mFormat;
            yuv.mSize = inYuv.mSize;
            addBufferInfo(tpNode, yuv);
        }

        addNodeInfo(mSession, tpNode);
        addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_YUV,
                                  id, TPI_BUFFER_ID_MTK_YUV);
        addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_YUV_2,
                                  id, TPI_BUFFER_ID_MTK_YUV_2);

        if( needDepth )
        {
            addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_DEPTH,
                                  id, TPI_BUFFER_ID_MTK_DEPTH);
        }

        lastID = id;
    }

    if( lastID != TPI_NODE_ID_MTK_S_YUV )
    {
        addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_YUV,
                               outID, TPI_BUFFER_ID_MTK_YUV);
    }

    {
        TPI_BufferInfo yuv;
        yuv.mFormat = inYuv.mFormat;
        yuv.mSize = inYuv.mSize;
        yuv.mBufferID = TPI_BUFFER_ID_MTK_OUT_YUV;
        addBufferInfo(inNode, yuv);
        yuv.mBufferID = TPI_BUFFER_ID_MTK_OUT_YUV_2;
        addBufferInfo(inNode, yuv);
        if( inYuv.mFormat != NSCam::eImgFmt_UNKNOWN )
        {
            inNode.mCustomOption |= TPI_MTK_OPT_STREAMING_CUSTOM_FORMAT;
        }
        if( inYuv.mSize.w && inYuv.mSize.h )
        {
            inNode.mCustomOption |= TPI_MTK_OPT_STREAMING_CUSTOM_SIZE;
        }
        if( !needOutBuffer )
        {
            inNode.mNodeOption |= TPI_NODE_OPT_INPLACE;
        }
        if( needDepth )
        {
            inNode.mCustomOption |= TPI_MTK_OPT_STREAMING_DEPTH;
            addBufferInfo(inNode, inDepth);
        }
    }

    addNodeInfo(mSession, inNode);
    addNodeInfo(mSession, outNode);
}

void TPIMgr_PluginWrapper::addDispPath(const std::list<unsigned> &genList)
{
    unsigned inID = TPI_NODE_ID_MTK_S_DISP_ONLY;
    unsigned outID = TPI_NODE_ID_MTK_S_DISP_ONLY_OUT;
    unsigned lastID = inID;

    TPI_NodeInfo inNode(inID);
    inNode.mCustomOption = TPI_MTK_OPT_STREAMING_NONE;

    TPI_NodeInfo outNode(outID);

    bool isFirst = true;

    for( auto id : genList )
    {
        PluginInfo &info = mPluginMap[id];

        if( isFirst )
        {
            isFirst = false;
            TPI_BufferInfo yuv(TPI_BUFFER_ID_MTK_OUT_YUV);
            toBufferInfo(yuv, info.mSelection->mIBufferMain1);
        }

        TPI_NodeInfo tpNode(id);
        tpNode.mNodeOption |= TPI_NODE_OPT_INPLACE;
        {
            TPI_BufferInfo yuv(TPI_BUFFER_ID_MTK_OUT_YUV);
            addBufferInfo(tpNode, yuv);
        }

        addNodeInfo(mSession, tpNode);
        addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_YUV,
                                  id, TPI_BUFFER_ID_MTK_YUV);

        lastID = id;
    }

    if( lastID != TPI_NODE_ID_MTK_S_YUV )
    {
        addPathInfo(mSession, lastID, TPI_BUFFER_ID_MTK_OUT_YUV,
                               outID, TPI_BUFFER_ID_MTK_YUV);
    }

    {
        TPI_BufferInfo yuv;
        yuv.mBufferID = TPI_BUFFER_ID_MTK_OUT_YUV;
        addBufferInfo(inNode, yuv);
    }

    addNodeInfo(mSession, inNode);
    addNodeInfo(mSession, outNode);
}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
