/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_MGR_PLUGIN_WRAPPER_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_MGR_PLUGIN_WRAPPER_H_

#include "TPIMgr.h"
#include <mtkcam3/3rdparty/plugin/PipelinePluginType.h>


namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

class TPIMgr_PluginWrapper : public TPIMgr
{
public:
  typedef NSCam::NSPipelinePlugin::JoinPlugin T_PluginType;
  typedef T_PluginType::Ptr                   T_PluginPool;
  typedef T_PluginType::Property              T_Property;
  typedef T_PluginType::IProvider::Ptr        T_Plugin;
  typedef T_PluginType::Selection::Ptr        T_Selection;
  typedef T_PluginType::Request::Ptr          T_Request;
  typedef unsigned                            T_NodeID;

private:
  class PluginInfo
  {
  public:
      PluginInfo();
      PluginInfo(const T_Plugin&, const T_Property&, const T_Selection&);
  public:
      T_Plugin mPlugin = NULL;
      T_Property mProperty;
      T_Selection mSelection = NULL;
  };
  typedef std::map<T_NodeID, PluginInfo> PluginMap;
  typedef std::list<T_NodeID> NodeIDList;

public:
  static void initProperty(T_Property &prop);
  static void initSelection(T_Selection &sel, const std::shared_ptr<NSCam::IMetadata> &meta);
  static void print(const T_Property &prop);
  static void print(const T_Selection &sel);
  static void print(const NSPipelinePlugin::BufferSelection &sel, const char *name);
  static void print(const char *name, const std::list<T_NodeID> &list, PluginMap &pluginMap);
  static bool checkDup(const std::list<T_NodeID> &list, PluginMap &pluginMap);
  static void fillMeta(T_Request &request, const TPI_Meta &meta);
  static void fillBuffer(T_Request &request, const TPI_Buffer &img);
  T_Request makeRequest(TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount);

public:
  TPIMgr_PluginWrapper();
  ~TPIMgr_PluginWrapper();
  bool createSession(unsigned logicalSensorID, TPI_SCENARIO_TYPE scenario, const IMetadata &meta);
  bool createSession(unsigned logicalSensorID, TP_MASK_T masks, const IMetadata &meta);
  bool destroySession();

  bool initSession();
  bool uninitSession();

  bool initNode(unsigned nodeID);
  bool uninitNode(unsigned nodeID);

  bool start();
  bool stop();

  bool genFrame(unsigned &frame);
  bool releaseFrame(unsigned frame);

  bool enqueNode(unsigned nodeID, unsigned frame, TPI_Meta meta[], size_t metaCount, TPI_Buffer img[], size_t imgCount);

  bool getSessionInfo(TPI_Session &session) const;

private:
  static unsigned generateSessionID();

private:
  bool checkSession(const TPI_Session &session) const;

private:
  void createPluginSession(unsigned sensorID, TP_MASK_T masks, const IMetadata &meta);
  void destroyPluginSession();
  void convertPluginToSession();
  MBOOL toBufferInfo(TPI_BufferInfo &bufferInfo, const NSPipelinePlugin::BufferSelection &sel);
  void addYuvPath(const std::list<T_NodeID> &list);
  void addDispPath(const std::list<T_NodeID> &list);

private:
  enum eState {
    STATE_IDLE,
    STATE_CREATE,
    STATE_INIT,
    STATE_RUN,
    STATE_STOP,
    STATE_UNINIT,
    STATE_DESTROY,
  };

private:
  eState mState = TPIMgr_PluginWrapper::STATE_IDLE;
  TPI_Session mSession;
  unsigned mFrameID = 0;

private:
  T_PluginPool mPluginPool = 0;
  PluginMap mPluginMap;
};

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_TPI_MGR_PLUGIN_WRAPPER_H_
