/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "P2ANode.h"
#include <mtkcam/drv/iopipe/CamIO/IHalCamIO.h>
#include <mtkcam3/feature/eis/eis_hal.h>
#include <mtkcam3/feature/DualCam/FOVHal.h>
#include <mtkcam3/feature/fsc/fsc_util.h>
//#include "FMHal.h"

#define PIPE_CLASS_TAG "P2ANode"
#define PIPE_TRACE TRACE_P2A_NODE
#include <featurePipe/core/include/PipeLog.h>
#include "P2CamContext.h"
#include "TuningHelper.h"

using NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
using NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;
using NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FM;
using NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;
using NSCam::NSIoPipe::Input;
using NSCam::NSIoPipe::Output;
using NSImageio::NSIspio::EPortIndex_LCEI;
using NSImageio::NSIspio::EPortIndex_IMG3O;
using NSImageio::NSIspio::EPortIndex_IMG2O;
using NSImageio::NSIspio::EPortIndex_WDMAO;
using NSImageio::NSIspio::EPortIndex_WROTO;
using NSImageio::NSIspio::EPortIndex_VIPI;
using NSImageio::NSIspio::EPortIndex_IMGI;
using NSImageio::NSIspio::EPortIndex_DEPI;//left
using NSImageio::NSIspio::EPortIndex_DMGI;//right
using NSImageio::NSIspio::EPortIndex_FEO;
using NSImageio::NSIspio::EPortIndex_MFBO;
using NSCam::NSIoPipe::ModuleInfo;

using namespace NSCam::NSIoPipe;

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

static MUINT32 calImgOffset(sp<IImageBuffer> pIMGBuffer, const MRect &tmpRect)
{
    MUINT32 u4PixelToBytes = 0;

    MINT imgFormat = pIMGBuffer->getImgFormat();

    if (imgFormat == eImgFmt_YV12)
    {
        u4PixelToBytes = 1;
    }
    else if (imgFormat == eImgFmt_YUY2)
    {
        u4PixelToBytes = 2;
    }
    else
    {
        MY_LOGW("unsupported image format %d", imgFormat);
    }

    return tmpRect.p.y * pIMGBuffer->getBufStridesInBytes(0) + tmpRect.p.x * u4PixelToBytes; //in byte
}

P2ANode::P2ANode(const char *name)
    : StreamingFeatureNode(name)
    , mp3A(NULL)
    , mpISP(NULL)
    , mNormalStream(NULL)
    , mFullImgPoolAllocateNeed(0)
    , mFEFMTuning{NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL}
    , mCropMode(CROP_MODE_NONE)
    , mEisMode(0)
    , mLastDualParamValid(MFALSE)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mRequests);

    m3dnrLogLevel = getPropertyValue("vendor.camera.3dnr.log.level", 0);

    TRACE_FUNC_EXIT();
}

P2ANode::~P2ANode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::setNormalStream(NSCam::NSIoPipe::NSPostProc::INormalStream *stream, MUINT32 version)
{
    TRACE_FUNC_ENTER();
    mNormalStream = stream;
    mDipVersion = version;
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::setFullImgPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mFullImgPool = pool;
    mFullImgPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::setPureImgPool(const std::unordered_map<MUINT32, android::sp<IBufferPool>> &poolMap)
{
    TRACE_FUNC_ENTER();
    mPureImgPoolMap = poolMap;
    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    if( mPipeUsage.supportFEFM() )
    {
        mEisMode = EIS_MODE_OFF;
        FMHal::getConfig(mPipeUsage.getStreamingSize(),mFM_FE_cfg);
        MY_LOGD("EIS2.5: FE0(%dx%d) FE1(%dx%d) FE2(%dx%d) feo_h(%dx%d) feo_m(%dx%d) feo_l(%dx%d) fmo_h(%dx%d) fmo_m(%dx%d) fmo_l(%dx%d)",
               mFM_FE_cfg[0].mImageSize.w,mFM_FE_cfg[0].mImageSize.h,
               mFM_FE_cfg[1].mImageSize.w,mFM_FE_cfg[1].mImageSize.h,
               mFM_FE_cfg[2].mImageSize.w,mFM_FE_cfg[2].mImageSize.h,
               mFM_FE_cfg[0].mFESize.w,mFM_FE_cfg[0].mFESize.h,
               mFM_FE_cfg[1].mFESize.w,mFM_FE_cfg[1].mFESize.h,
               mFM_FE_cfg[2].mFESize.w,mFM_FE_cfg[2].mFESize.h,
               mFM_FE_cfg[0].mFMSize.w,mFM_FE_cfg[0].mFMSize.h,
               mFM_FE_cfg[1].mFMSize.w,mFM_FE_cfg[1].mFMSize.h,
               mFM_FE_cfg[2].mFMSize.w,mFM_FE_cfg[2].mFMSize.h);
        mFE1ImgPool = ImageBufferPool::create("fpipe.fe1Img", mFM_FE_cfg[1].mImageSize, eImgFmt_YV12, ImageBufferPool::USAGE_HW);
        mFE2ImgPool = ImageBufferPool::create("fpipe.fe2Img", mFM_FE_cfg[2].mImageSize, eImgFmt_YV12, ImageBufferPool::USAGE_HW);
        mFE3ImgPool = ImageBufferPool::create("fpipe.fe3Img", mFM_FE_cfg[2].mImageSize, eImgFmt_YV12, ImageBufferPool::USAGE_HW);
        mFEOutputPool = FatImageBufferPool::create("fpipe.feo", mFM_FE_cfg[0].mFESize, eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW);
        mFEOutputPool_m = FatImageBufferPool::create("fpipe.feo_m", mFM_FE_cfg[1].mFESize, eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW);
        mFMOutputPool = FatImageBufferPool::create("fpipe.fmo", mFM_FE_cfg[0].mFMSize, eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW);
        mFMOutputPool_m = FatImageBufferPool::create("fpipe.fmo_m", mFM_FE_cfg[1].mFMSize, eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW);
        mFMRegisterPool = FatImageBufferPool::create("fpipe.fm_readregister", MSize(1,4), eImgFmt_STA_BYTE, FatImageBufferPool::USAGE_HW);

        for( unsigned i = 0; i < sizeof(mFEInfo)/sizeof(mFEInfo[0]); ++i )
        {
            memset(&mFEInfo[i], 0, sizeof(mFEInfo[0]));
        }
        for( unsigned i = 0; i < sizeof(mFMInfo)/sizeof(mFMInfo[0]); ++i )
        {
            memset(&mFMInfo[i], 0, sizeof(mFMInfo[0]));
        }
        for( unsigned i = 0; i < 9; ++i )
        {
            unsigned int tuningsize = NSIoPipe::NSPostProc::INormalStream::getRegTableSize();
            mFEFMTuning[i] = (char*)malloc(tuningsize);
            if( mFEFMTuning[i] )
            {
                memset(mFEFMTuning[i], 0, tuningsize);
            }
            else
            {
                MY_LOGE("Allocate FEFM tuning buffer failed. ID = %d, Size = %d", i, tuningsize);
            }
        }
    }
    if(mPipeUsage.supportFOV())
    {
#if SUPPORT_FOV
        MY_LOGD("Support Dual, create FOV buffer pools.");
        // query size from hal
        FOVHal::SizeConfig config = FOVHal::getSizeConfig(FOVHal::RATIO_4_3);
        // create pool with larger 4:3 buffer pool
        mFovFEOImgPool = ImageBufferPool::create("fpipe.fovfeoImg", config.mFEOSize, eImgFmt_STA_BYTE, ImageBufferPool::USAGE_HW);
        mFovFMOImgPool = ImageBufferPool::create("fpipe.fovfmoImg", config.mFMOSize, eImgFmt_STA_BYTE, ImageBufferPool::USAGE_HW);
        #if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
        mFovTuningBufferPool = TuningBufferPool::create("fpipe.fovtuningBuf", NSIoPipe::NSPostProc::INormalStream::getRegTableSize());
        #endif
#endif
    }

    if( mPipeUsage.support3DNRRSC() )
    {
        MY_LOGD("P2A add RSC waitQueue for 3DNR");
        this->addWaitQueue(&mRSCDatas);
    }

    if(mPipeUsage.isDynamicTuning())
    {
        mDynamicTuningPool = TuningBufferPool::create("fpipe.p2atuningBuf", NSIoPipe::NSPostProc::INormalStream::getRegTableSize(), MTRUE/*Use Buf Protect*/);
    }

    if(mPipeUsage.isSecureP2())
    {
        mSecBufCtrl.init(mPipeUsage.getSecureType());
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onUninit()
{
    TRACE_FUNC_ENTER();
    ImageBufferPool::destroy(mFE1ImgPool);
    ImageBufferPool::destroy(mFE2ImgPool);
    ImageBufferPool::destroy(mFE3ImgPool);
    FatImageBufferPool::destroy(mFEOutputPool);
    FatImageBufferPool::destroy(mFEOutputPool_m);
    FatImageBufferPool::destroy(mFMOutputPool);
    FatImageBufferPool::destroy(mFMOutputPool_m);
    FatImageBufferPool::destroy(mFMRegisterPool);
    ImageBufferPool::destroy(mFovFEOImgPool);
    ImageBufferPool::destroy(mFovFMOImgPool);
    for( unsigned i = 0; i < 9; ++i )
    {
        if( mFEFMTuning[i] )
        {
            ::free(mFEFMTuning[i]);
            mFEFMTuning[i] = NULL;
        }
    }
    #if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
    TuningBufferPool::destroy(mFovTuningBufferPool);
    #endif
    TuningBufferPool::destroy(mDynamicTuningPool);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onThreadStart()
{
    TRACE_FUNC_ENTER();

    if( mFullImgPoolAllocateNeed && mFullImgPool != NULL )
    {
        Timer timer(MTRUE);
        mFullImgPool->allocate(mFullImgPoolAllocateNeed);
        timer.stop();
        MY_LOGD("mFullImg %s %d buf in %d ms", STR_ALLOCATE, mFullImgPoolAllocateNeed, timer.getElapsed());
    }

    if( mPipeUsage.supportFEFM() &&
        mFE1ImgPool != NULL && mFE2ImgPool != NULL && mFE3ImgPool != NULL &&
        mFEOutputPool != NULL && mFEOutputPool_m != NULL &&
        mFMOutputPool != NULL && mFMOutputPool_m != NULL && mFMRegisterPool != NULL)
    {
        Timer timer(MTRUE);
        mFE1ImgPool->allocate(1*3);
        mFE2ImgPool->allocate(1*3);
        mFE3ImgPool->allocate(1*3);
        mFEOutputPool->allocate(3*4);
        mFEOutputPool_m->allocate(3*4);
        mFMOutputPool->allocate(6*3);
        mFMOutputPool_m->allocate(6*3);
        mFMRegisterPool->allocate(6*4);
        timer.stop();
        MY_LOGD("FE FM %s in %d ms", STR_ALLOCATE, timer.getElapsed());
    }

    if( mPipeUsage.isDynamicTuning() && mDynamicTuningPool != NULL )
    {
        Timer timer(MTRUE);
        mDynamicTuningPool->allocate(mPipeUsage.getNumP2ATuning());
        timer.stop();
        MY_LOGD("Dynamic Tuning %s %d bufs in %d ms", STR_ALLOCATE, mPipeUsage.getNumP2ATuning(), timer.getElapsed());
    }

    if( mPipeUsage.supportPure())
    {
        Timer timer(MTRUE);
        for(auto&& it : mPureImgPoolMap)
        {
            if(it.second != NULL)
            {
                it.second->allocate(mPipeUsage.getNumP2APureBuffer());
            }
        }
        timer.stop();
        MY_LOGD("Pure Buffer %s %d bufs with %zu maps in %d ms", STR_ALLOCATE, mPipeUsage.getNumP2APureBuffer(), mPureImgPoolMap.size(), timer.getElapsed());
    }

    if( mPipeUsage.supportFOV())
    {
        const int FOV_IMG_BUFSIZE = 3;
        Timer timer(MTRUE);
        // master + slave = 2 sets
        mFovFEOImgPool->allocate(FOV_IMG_BUFSIZE * 2);
        mFovFMOImgPool->allocate(FOV_IMG_BUFSIZE * 2);
        #if (MTKCAM_HAVE_DUAL_ZOOM_SUPPORT == 1)
        mFovTuningBufferPool->allocate(FOV_IMG_BUFSIZE * 2);
        #endif
        MY_LOGD("FOV buffers %s in %d ms", STR_ALLOCATE, timer.getElapsed());
    }
    init3A();
    initVHDR();
    initFEFM();
    initP2();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    this->waitNormalStreamBaseDone();
    uninitVHDR();
    uninitFEFM();
    uninitP2();
    uninit3A();

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::onData(DataID id, const RequestPtr &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;

    switch( id )
    {
    case ID_ROOT_TO_P2A:
        mRequests.enque(data);
        ret = MTRUE;
        break;
    default:
        ret = MFALSE;
        break;
    }

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::onData(DataID id, const RSCData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s  rsc_data arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;

    if( mPipeUsage.support3DNRRSC() )
    {
        if( id == ID_RSC_TO_P2A )
        {
            mRSCDatas.enque(data);
            ret = MTRUE;
        }
    }

    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType P2ANode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_INOUT;
    if( HAS_3DNR(reqInfo.mFeatureMask) && reqInfo.isMaster())
    {
        policy = IOPOLICY_LOOPBACK;
    }

    return policy;
}

MBOOL P2ANode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    RequestPtr request;
    RSCData rscData;

    P2_CAM_TRACE_CALL(TRACE_DEFAULT);

    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mRequests.deque(request) )
    {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    }
    else if( request == NULL )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }

    if( mPipeUsage.support3DNRRSC() )
    {
        if( !mRSCDatas.deque(rscData) )
        {
            MY_LOGE("RSCData deque out of sync");
            return MFALSE;
        }
        if( request != rscData.mRequest )
        {
            MY_LOGE("P2A_RSCData out of sync request(%d) rsc(%d)", request->mRequestNo, rscData.mRequest->mRequestNo);
            return MFALSE;
        }
    }

    TRACE_FUNC_ENTER();

    request->mTimer.startP2A();
    processP2A(request, rscData);
    request->mTimer.stopP2A();// When NormalStream callback, stopP2A will be called again to record this frame duration

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL isSameTuning(SFPIOMap& io1, SFPIOMap& io2, MUINT32 sensorID)
{
    return (io1.getTuning(sensorID).mFlag == io2.getTuning(sensorID).mFlag);
}

MBOOL P2ANode::processP2A(const RequestPtr &request, const RSCData &rscData)
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);

    TRACE_FUNC_ENTER();
    P2AEnqueData data;
    P2ATuningIndex tuningIndex;
    data.mRequest = request;

    if( mPipeUsage.supportBypassP2A() )
    {
        MY_LOGW("Bypass P2ANode not confirm supported now!!");
        prepareFullImgFromInput(request, data);
        handleResultData(request, data);
        return MTRUE;
    }

    SFPIOManager &ioMgr = request->mSFPIOManager;
    if(ioMgr.countAll() == 0)
    {
        MY_LOGW("No output frame exist in P2ANode, directly let SFP return.");
        handleData(ID_P2A_TO_HELPER, HelperData(FeaturePipeParam::MSG_FRAME_DONE, request));
        return MFALSE;
    }

    QParams param;
    // --- Start prepare General QParam
    request->mTimer.startP2ATuning();
    if( mPipeUsage.isDynamicTuning() )
    {
        if( request->need3DNR())
        {
            if (!update3DNRMvInfo(request, rscData))
            {
                MY_LOGW("update3DNRMvInfo failed!!");
            }
        }
        prepareRawTuning(param, request, data, tuningIndex);
    }
    else
    {
        prepareQParams(param, request, tuningIndex);
    }
    request->mTimer.stopP2ATuning();

    if( request->need3DNR())
    {
        if (!prepare3DNR(param, request, rscData, tuningIndex))
        {
            // set mPrevFullImg to NULL to disable VIPI port
            getP2CamContext(request->getMasterID())->setPrevFullImg(NULL);
        }
    }

    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "PrepareOutput");
    if(ioMgr.countNonLarge() != 0)
    {
        prepareNonMdpIO(param, request, data, tuningIndex);
        prepareMasterMdpOuts(param, request, data, tuningIndex);
        prepareSlaveOuts(param, request, data, tuningIndex);
    }

    if(ioMgr.countLarge() != 0)
    {
        if(tuningIndex.isLargeMasterValid())
        {
            prepareLargeMdpOuts(param, request, tuningIndex.mLargeMaster, request->mMasterID);
        }
        if(tuningIndex.isLargeSlaveValid())
        {
            prepareLargeMdpOuts(param, request, tuningIndex.mLargeSlave, request->mSlaveID);
        }
    }
    P2_CAM_TRACE_END(TRACE_ADVANCED);

    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "PrepareSecure");
    for( auto &frameParam : param.mvFrameParams )
    {
        if( !TuningHelper::processSecure(frameParam, mSecBufCtrl) )
        {
            MY_LOGE("Process Secure Flow Failed! SecureEnum(%d)", mSecBufCtrl.getSecureEnum());
            return MFALSE;
        }
    }
    P2_CAM_TRACE_END(TRACE_ADVANCED);

    // ---- Prepare QParam Done ----
    if( request->needPrintIO() )
    {
        printIO(request, param);
    }

    if( mPipeUsage.supportEISNode() && !mPipeUsage.supportFEFM() )
    {
        handleData(ID_P2A_TO_EIS_P2DONE, FMData(data.mFMResult, request));
    }

    enqueFeatureStream(param, data);


    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID P2ANode::onNormalStreamBaseCB(const QParams &params, const P2AEnqueData &data)
{
    // This function is not thread safe,
    // avoid accessing P2ANode class members
    TRACE_FUNC_ENTER();
    for(auto&& frame : params.mvFrameParams)
    {
        for( size_t i = 0; i < frame.mvExtraParam.size(); i++ )
        {
            MUINT cmdIdx = frame.mvExtraParam[i].CmdIdx;

            if( cmdIdx == EPIPE_IMG3O_CRSPINFO_CMD )
            {
                CrspInfo* extraParam = static_cast<CrspInfo*>(frame.mvExtraParam[i].moduleStruct);
                if( extraParam )
                {
                    delete extraParam;
                }
            }
        }
    }

    RequestPtr request = data.mRequest;
    if( request == NULL )
    {
        MY_LOGE("Missing request");
    }
    else
    {
        request->mTimer.stopEnqueP2A();
        MY_S_LOGD(data.mRequest->mLog, "sensor(%d) Frame %d enque done in %d ms, result = %d", mSensorIndex, request->mRequestNo, request->mTimer.getElapsedEnqueP2A(), params.mDequeSuccess);

        if( !params.mDequeSuccess )
        {
            MY_LOGW("Frame %d enque result failed", request->mRequestNo);
        }

        request->updateResult(params.mDequeSuccess);
        handleResultData(request, data);
        request->mTimer.stopP2A();
    }

    this->decExtThreadDependency();
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::handleResultData(const RequestPtr &request, const P2AEnqueData &data)
{
    // This function is not thread safe,
    // because it is called by onQParamsCB,
    // avoid accessing P2ANode class members
    TRACE_FUNC_ENTER();
    BasicImg full;
    if( data.mNextFullImg.mBuffer != NULL )
    {
        full = data.mNextFullImg;
    }
    else
    {
        full = data.mFullImg;
    }

    if( mPipeUsage.supportFOV())
    {
        TRACE_FUNC("to fov");
        handleData(ID_P2A_TO_FOV_FEFM, FOVP2AData(data.mFovP2AResult, request));
        handleData(ID_P2A_TO_FOV_FULLIMG, ImgBufferData(full.mBuffer, request));
        handleData(ID_P2A_TO_FOV_WARP, BasicImgData(full, request));
    }
    else if( mPipeUsage.supportDummy() )
    {
        handleData(ID_PREV_TO_DUMMY_FULLIMG, BasicImgData(full, request));
    }
    else if( mPipeUsage.supportVendor() )
    {
        TRACE_FUNC("to vendor");
        BasicImg slaveFull = data.mSlaveNextFullImg.mBuffer != NULL ? data.mSlaveNextFullImg : data.mSlaveFullImg;
        handleData(ID_P2A_TO_VENDOR_FULLIMG,
                DualBasicImgData(DualBasicImg(full, slaveFull), request));
        if( mPipeUsage.supportN3D() )
        {
            handleData(ID_P2A_TO_N3DP2, request);
        }
    }
    else if( mPipeUsage.supportWarpNode() )
    {
        handleData(ID_P2A_TO_WARP_FULLIMG, BasicImgData(full, request));
    }
    else if( mPipeUsage.supportN3D() )
    {
        handleData(ID_P2A_TO_N3DP2, request);
    }
    else
    {
        handleData(ID_P2A_TO_HELPER, HelperData(FeaturePipeParam::MSG_FRAME_DONE, request));
    }

    if( request->needP2AEarlyDisplay() )
    {
        handleData(ID_P2A_TO_HELPER, HelperData(FeaturePipeParam::MSG_DISPLAY_DONE, request));
    }

    if( mPipeUsage.supportEISNode() && mPipeUsage.supportFEFM() )
    {
        handleData(ID_P2A_TO_EIS_P2DONE, FMData(data.mFMResult, request));
    }

    P2AMDPReq mdpReq;
    if(!data.mRemainingOutputs.empty())
    {
        mdpReq.mMdpIn = data.mFullImg;
        mdpReq.mMdpOuts = std::move(data.mRemainingOutputs);
    }
    handleData(ID_P2A_TO_PMDP, P2AMDPReqData(mdpReq, request));

    if( request->needNddDump() )
    {
        if( data.mFullImg.mBuffer != NULL )
        {
            TuningUtils::FILE_DUMP_NAMING_HINT hint = request->mP2Pack.getSensorData(request->mMasterID).mNDDHint;
            data.mFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpNddData(&hint, data.mFullImg.mBuffer->getImageBufferPtr(), EPortIndex_IMG3O);
        }
    }

    if( request->needDump() )
    {
        if( data.mFullImg.mBuffer != NULL )
        {
            data.mFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mFullImg.mBuffer->getImageBufferPtr(), "full");
        }
        if( data.mNextFullImg.mBuffer != NULL )
        {
            data.mNextFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mNextFullImg.mBuffer->getImageBufferPtr(), "nextfull");
        }
        if( data.mSlaveFullImg.mBuffer != NULL )
        {
            data.mSlaveFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mSlaveFullImg.mBuffer->getImageBufferPtr(), "slaveFull");
        }
        if( data.mSlaveNextFullImg.mBuffer != NULL )
        {
            data.mSlaveNextFullImg.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mSlaveNextFullImg.mBuffer->getImageBufferPtr(), "slaveNextfull");
        }
        if( data.mPureImg != NULL )
        {
            data.mPureImg->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mPureImg->getImageBufferPtr(), "pure");
        }
        if( data.mSlavePureImg != NULL )
        {
            data.mSlavePureImg->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mSlavePureImg->getImageBufferPtr(), "slavePure");
        }
        if( data.mFMResult.FM_B.Register_Medium != NULL )
        {
            dumpData(data.mRequest, data.mFMResult.FM_B.Register_Medium->getImageBufferPtr(), "fm_reg_m");
        }
        if( data.mFovP2AResult.mFEO_Master != NULL )
        {
            dumpData(data.mRequest, data.mFovP2AResult.mFEO_Master->getImageBufferPtr(), "mFEO_Master");
        }
        if( data.mFovP2AResult.mFEO_Slave != NULL )
        {
            dumpData(data.mRequest, data.mFovP2AResult.mFEO_Slave->getImageBufferPtr(), "mFEO_Slave");
        }
        if( data.mFovP2AResult.mFMO_MtoS != NULL )
        {
            dumpData(data.mRequest, data.mFovP2AResult.mFMO_MtoS->getImageBufferPtr(), "mFMO_MtoS");
        }
        if( data.mFovP2AResult.mFMO_StoM != NULL )
        {
            dumpData(data.mRequest, data.mFovP2AResult.mFMO_StoM->getImageBufferPtr(), "mFMO_StoM");
        }
    }
    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::initP2()
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    if( mNormalStream != NULL )
    {
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MVOID P2ANode::uninitP2()
{
    TRACE_FUNC_ENTER();

    mNormalStream = NULL;
    mPrevFE.clear();

    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::prepareQParams(QParams &params, const RequestPtr &request, P2ATuningIndex &tuningIndex)
{
    TRACE_FUNC_ENTER();
    params.mvFrameParams.clear();
    params.mvFrameParams.push_back(FrameParams());
    FrameParams &master = params.mvFrameParams.editItemAt(0);
    request->getMasterFrameBasic(master);
    request->getMasterFrameInput(master);
    request->getMasterFrameTuning(master);
    prepareStreamTag(params, request);
    tuningIndex.mGenMaster = 0;

    MUINT32 slaveID = request->mSlaveID;
    if(request->needFullImg(this, slaveID) || request->needNextFullImg(this, slaveID) || request->needFOVFEFM())
    {
        if(!request->hasSlave(slaveID))
        {
            MY_LOGE("Failed to get slave feature params. Cannot copy slave FrameParams!");
            return MFALSE;
        }
        FeaturePipeParam &fparam_slave = request->getSlave(slaveID);
        if( ! fparam_slave.mQParams.mvFrameParams.size())
        {
            MY_LOGE("Slave QParam's FrameParam Size = 0. Cannot copy slave FrameParams!");
            return MFALSE;
        }
        params.mvFrameParams.push_back(fparam_slave.mQParams.mvFrameParams.itemAt(0));
        tuningIndex.mGenSlave = params.mvFrameParams.size() - 1;
        //
        FrameParams &f = params.mvFrameParams.editItemAt(tuningIndex.mGenSlave);
        f.mSensorIdx = slaveID;
        f.mStreamTag = ENormalStreamTag_Normal;
        f.mvOut.clear();
        f.mvCropRsInfo.clear();
        f.mvExtraParam.clear();
    }
    TRACE_FUNC_EXIT();
    return MFALSE;
}

MBOOL P2ANode::prepareStreamTag(QParams &params, const RequestPtr &request)
{
    TRACE_FUNC_ENTER();
    (void)(request);
    if( params.mvFrameParams.size() )
    {
        // 1. if non-TimeSharing --> use the original assgined stream tag, ex: ENormalStreamTag_Normal
        // 2. if TimeSharing -> use ENormalStreamTag_Vss
        if( mPipeUsage.supportTimeSharing() )
        {
            params.mvFrameParams.editItemAt(0).mStreamTag = ENormalStreamTag_Vss;
        }
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::prepareFullImgFromInput(const RequestPtr &request, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    IImageBuffer *input = NULL;
    if( (input = request->getMasterInputBuffer()) == NULL )
    {
        MY_LOGE("Cannot get input image buffer");
        ret = MFALSE;
    }
    else if( (data.mFullImg.mBuffer= new IIBuffer_IImageBuffer(input)) == NULL )
    {
        MY_LOGE("OOM: failed to allocate IIBuffer");
        ret = MFALSE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::prepareNonMdpIO(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;

    if(!tuningIndex.isMasterMainValid())
    {
        MY_LOGE("Both master General Normal/Pure & Physical tuning not exist! Can not prepare non mdp out img.");
        return MFALSE;
    }
    MUINT32 masterIndex = tuningIndex.getMasterMainIndex();
    MBOOL isGenNormalRun = (masterIndex == (MUINT32)tuningIndex.mGenMaster);
    FrameParams &frame = params.mvFrameParams.editItemAt(masterIndex);

    FrameInInfo inInfo;
    getFrameInInfo(inInfo, frame);
    const MUINT32 masterID = request->mMasterID;

    prepareFDImg(frame, request, data);
    prepareFDCrop(frame, request, data);

    if( request->isForceIMG3O() || (request->needFullImg(this, masterID) && isGenNormalRun ))
    {
        prepareFullImg(frame, request, data.mFullImg, inInfo, masterID);
        // Full Img no need crop
    }

    if( request->need3DNR() && isGenNormalRun &&
        getP2CamContext(request->getMasterID())->getPrevFullImg() != NULL)
    {
        prepareVIPI(frame, request, data);
        //handleVipiNr3dOffset(params, request, data);
    }

    if( request->needFEFM() )
    {
        if(request->isSlaveParamValid())
        {
            MY_LOGE("P2A FEFM not support multi sensor !!!");
        }
        else
        {
            prepareFEFM(params, request, data);
            prepareCropInfo_FE(params,request,data);
        }
    }
    if(request->needFOVFEFM())
    {
#if SUPPORT_FOV
        prepareFOVFEFM(params, request, data, tuningIndex);
        prepareCropInfo_FOV(params, request, data, tuningIndex);
#endif
    }
    mPrevFE = data.mFMResult.FE;

    getP2CamContext(request->getMasterID())->setPrevFullImg(
        (request->need3DNR() ? data.mFullImg.mBuffer: NULL));

    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::prepareMasterMdpOuts(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;

    if(!tuningIndex.isMasterMainValid())
    {
        MY_LOGE("Both master General & Physical tuning not exist! Can not prepare output img.");
        return MFALSE;
    }

    MUINT32 sID = request->mMasterID;
    // Handle Pure First
    if(tuningIndex.isPureMasterValid())
    {
        FrameParams &frame = params.mvFrameParams.editItemAt((MUINT32)tuningIndex.mPureMaster);
        preparePureImg(frame, request, data.mPureImg, sID);
    }

    // Handle Main Frame, it maybe normal or pure or physical frame
    MUINT32 masterIndex = tuningIndex.getMasterMainIndex();
    FrameParams &frame = params.mvFrameParams.editItemAt(masterIndex);
    FrameInInfo inInfo;
    getFrameInInfo(inInfo, frame);
    MBOOL needExtraPhyRun = (tuningIndex.isPhyMasterValid()) && (tuningIndex.mPhyMaster != MINT32(masterIndex));
    MBOOL isGenNormalRun = (masterIndex == (MUINT32)tuningIndex.mGenMaster);
    SFPOutput output;
    std::vector<SFPOutput> outList;
    outList.reserve(5);
    // Internal Buffer has highest prority, need to push back first.
    if( request->needNextFullImg(this, sID) && isGenNormalRun)
    {
        SFPOutput out;
        prepareNextFullSFPOut(out, request, data.mNextFullImg, inInfo, sID);
        outList.push_back(out);
    }

    if (request->needDisplayOutput(this) && request->getDisplayOutput(output))
    {
        outList.push_back(output);
    }

    if (request->needRecordOutput(this) && request->getRecordOutput(output))
    {
        outList.push_back(output);
    }

    if (request->needExtraOutput(this))
    {
        request->getExtraOutputs(outList); // extra output maybe more than 1.
    }

    if (!needExtraPhyRun && request->needPhysicalOutput(this, sID) && request->getPhysicalOutput(output, sID))
    {
        outList.push_back(output);
    }

    //prepareMdpFrameParam(params, masterIndex, outList);
    if(data.mFullImg.mBuffer == NULL && needFullForExtraOut(outList))
    {
        prepareFullImg(frame, request, data.mFullImg, inInfo, sID);
        if(!mPipeUsage.supportIMG3O())
        {
            MY_LOGD("Need Full img but different crop may not supportted! All output using p2amdp.");
            data.mRemainingOutputs = outList;
            outList.clear();
        }
    }
    prepareOneMdpFrameParam(frame, outList, data.mRemainingOutputs);
    if(data.mRemainingOutputs.size() > 0)
    {
        prepareExtraMdpCrop(data.mFullImg, data.mRemainingOutputs);
    }

    //--- Additional Run for physical output ---
    if (needExtraPhyRun && request->needPhysicalOutput(this, sID) && request->getPhysicalOutput(output, sID))
    {
        std::vector<SFPOutput> phyOutList;
        phyOutList.push_back(output);
        prepareMdpFrameParam(params, (MUINT32)tuningIndex.mPhyMaster, phyOutList);
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL P2ANode::prepareSlaveOuts(QParams &params, const RequestPtr &request, P2AEnqueData &data, const P2ATuningIndex &tuningIndex)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;
    if(!tuningIndex.isSlaveMainValid())
    {
        TRACE_FUNC("Both slave General Normal/Pure & Physical tuning not exist! Can not prepare output img.");
        return MFALSE;
    }
    const MUINT32 sID = request->mSlaveID;
    // Handle Pure First
    if(tuningIndex.isPureSlaveValid())
    {
        FrameParams &frame = params.mvFrameParams.editItemAt((MUINT32)tuningIndex.mPureSlave);
        preparePureImg(frame, request, data.mSlavePureImg, sID);
    }

    // Handle Main Frame, it maybe normal or pure or physical frame
    MUINT32 slaveIndex = tuningIndex.getSlaveMainIndex();
    MBOOL needExtraPhyRun = (tuningIndex.isPhySlaveValid()) && (tuningIndex.mPhySlave != MINT32(slaveIndex));
    MBOOL isGenNormalRun = (slaveIndex == (MUINT32)tuningIndex.mGenSlave);
    FrameParams &frame = params.mvFrameParams.editItemAt((MUINT32)slaveIndex);
    FrameInInfo inInfo;
    getFrameInInfo(inInfo, frame);

    std::vector<SFPOutput> outList;
    outList.reserve(2);

    if( request->needFullImg(this, sID) && isGenNormalRun)
    {
        prepareFullImg(frame, request, data.mSlaveFullImg, inInfo, sID);
        // Full Img no need crop
    }

    if( request->needNextFullImg(this, sID) && isGenNormalRun)
    {
        SFPOutput out;
        prepareNextFullSFPOut(out, request, data.mSlaveNextFullImg, inInfo, sID);
        outList.push_back(out);
    }
    SFPOutput output;
    if (!needExtraPhyRun && request->needPhysicalOutput(this, sID) && request->getPhysicalOutput(output, sID))
    {
        outList.push_back(output);
    }

    prepareMdpFrameParam(params, slaveIndex, outList);// no consider more MDP run for slave

    //--- Additional Run for physical output ---
    if (needExtraPhyRun && request->needPhysicalOutput(this, sID) && request->getPhysicalOutput(output, sID))
    {
        std::vector<SFPOutput> phyOutList;
        phyOutList.push_back(output);
        prepareMdpFrameParam(params, (MUINT32)tuningIndex.mPhySlave, outList);
    }
    TRACE_FUNC_EXIT();
    return ret;
}


MBOOL P2ANode::prepareLargeMdpOuts(QParams &params, const RequestPtr &request, MINT32 frameIndex, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MTRUE;

    std::vector<SFPOutput> outList;
    if( ! request->getLargeOutputs(outList, sensorID) )
    {
        MY_LOGE("Get Large Out List failed! sID(%d), QFrameInd(%d)", sensorID, frameIndex);
    }
    prepareMdpFrameParam(params, frameIndex, outList);// no consider more MDP run for slave
    TRACE_FUNC_EXIT();
    return ret;
}

MVOID P2ANode::prepareFullImg(FrameParams &frame, const RequestPtr &request, BasicImg &outImg, const FrameInInfo &inInfo, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    // TODO Pool maybe not the same in different sensors
    TRACE_FUNC("Frame %d FullImgPool=(%d/%d)", request->mRequestNo, mFullImgPool->peakAvailableSize(), mFullImgPool->peakPoolSize());
    outImg.mBuffer = mFullImgPool->requestIIBuffer();
    sp<IImageBuffer> pIMGBuffer = outImg.mBuffer->getImageBuffer();

    const SrcCropInfo srcCropInfo = request->getSrcCropInfo(sensorID);
    const MRect& srcCrop = srcCropInfo.mSrcCrop;
    outImg.mDomainOffset = srcCrop.p;
    pIMGBuffer->setTimestamp(inInfo.timestamp);
    if(!pIMGBuffer->setExtParam(srcCrop.s))
    {
        MY_LOGE("Full Img setExtParm Fail!, target size(%dx%d)", srcCrop.s.w, srcCrop.s.h);
    }

    if(mPipeUsage.supportIMG3O())
    {
        Output output;
        output.mPortID = PortID(EPortType_Memory, EPortIndex_IMG3O, PORTID_OUT);
        output.mBuffer = pIMGBuffer.get();
        if (srcCropInfo.mIsSrcCrop)
        {
            output.mOffsetInBytes = calImgOffset(pIMGBuffer, srcCrop); //in byte
            // new driver interface to overwrite mOffsetInBytes
            CrspInfo* crspParam = new CrspInfo();
            crspParam->m_CrspInfo.p_integral.x = srcCrop.p.x;
            crspParam->m_CrspInfo.p_integral.y = srcCrop.p.y;
            crspParam->m_CrspInfo.s.w = srcCrop.s.w;
            crspParam->m_CrspInfo.s.h = srcCrop.s.h;
            ExtraParam extraParam;
            extraParam.CmdIdx = EPIPE_IMG3O_CRSPINFO_CMD;
            extraParam.moduleStruct = static_cast<void*>(crspParam);
            frame.mvExtraParam.push_back(extraParam);
        }
        frame.mvOut.push_back(output);
    }
    else
    {
        SFPOutput sfpOut;
        sfpOut.mBuffer = pIMGBuffer.get();
        sfpOut.mTransform = 0;
        sfpOut.mCropRect = srcCrop;
        sfpOut.mCropDstSize = srcCrop.s;
        pushSFPOutToMdp(frame, NSCam::NSIoPipe::PORT_WDMAO, sfpOut);
    }

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::preparePureImg(FrameParams &frame, const RequestPtr &request, ImgBuffer &outImg, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();
    const sp<IBufferPool> &pool = mPureImgPoolMap[sensorID];
    if(pool == NULL)
    {
        MY_LOGE("Pure pool Null!!! sId(%d), can not generate pure", sensorID);
        return;
    }
    TRACE_FUNC("Frame %d PureImgPool=(%d/%d)", request->mRequestNo, pool->peakAvailableSize(), pool->peakPoolSize());
    outImg = pool->requestIIBuffer();
    sp<IImageBuffer> pIMGBuffer = outImg->getImageBuffer();

    const SrcCropInfo srcCropInfo = request->getSrcCropInfo(sensorID);
    const MRect& srcCrop = srcCropInfo.mSrcCrop;
    FrameInInfo inInfo;
    getFrameInInfo(inInfo, frame);
    pIMGBuffer->setTimestamp(inInfo.timestamp);
    // TODO custom size may be different from sensorID & pure YUV
    MSize vendorPureImgSize = mPipeUsage.supportVendorCusSize() ? mPipeUsage.getStreamingSize() : srcCropInfo.mSrcCrop.s;
    if(!pIMGBuffer->setExtParam(srcCrop.s))
    {
        MY_LOGE("sId(%d) Pure Img setExtParm Fail!, target size(%dx%d)",sensorID, vendorPureImgSize.w, vendorPureImgSize.h);
    }

    SFPOutput sfpOut;
    sfpOut.mBuffer = pIMGBuffer.get();
    sfpOut.mTransform = 0;
    sfpOut.mCropRect = srcCrop;
    sfpOut.mCropDstSize = vendorPureImgSize;
    pushSFPOutToMdp(frame, NSCam::NSIoPipe::PORT_WDMAO, sfpOut);

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::prepareVIPI(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();
    data.mPrevFullImg = getP2CamContext(request->getMasterID())->getPrevFullImg();
    Input input;
    input.mPortID = PortID(EPortType_Memory, EPortIndex_VIPI, PORTID_IN);
    input.mBuffer = data.mPrevFullImg->getImageBufferPtr();
    frame.mvIn.push_back(input);

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::prepareFEFM(QParams &params, const RequestPtr &request, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();

    data.mFE1Img = mFE1ImgPool->requestIIBuffer();
    data.mFE2Img = mFE2ImgPool->requestIIBuffer();
    data.mFE3Img = mFE3ImgPool->requestIIBuffer();
    data.mFMResult.FE.High = mFEOutputPool->requestIIBuffer();
    data.mFMResult.FE.Medium = mFEOutputPool_m->requestIIBuffer();
    data.mFMResult.FE.Low = mFEOutputPool_m->requestIIBuffer();//mFEOutputPool_l->requestIIBuffer();

    prepareFE(params, request, data);

    if( mPrevFE.isValid() )
    {
        data.mFMResult.PrevFE = mPrevFE;

        //Previous to Current
        data.mFMResult.FM_A.High = mFMOutputPool->requestIIBuffer();
        data.mFMResult.FM_A.Medium = mFMOutputPool_m->requestIIBuffer();
        data.mFMResult.FM_A.Low = mFMOutputPool_m->requestIIBuffer();//mFMOutputPool_l->requestIIBuffer();

        data.mFMResult.FM_A.Register_High = mFMRegisterPool->requestIIBuffer();
        data.mFMResult.FM_A.Register_Medium = mFMRegisterPool->requestIIBuffer();
        data.mFMResult.FM_A.Register_Low = mFMRegisterPool->requestIIBuffer();

        //Current to Previous
        data.mFMResult.FM_B.High = mFMOutputPool->requestIIBuffer();
        data.mFMResult.FM_B.Medium = mFMOutputPool_m->requestIIBuffer();
        data.mFMResult.FM_B.Low = mFMOutputPool_m->requestIIBuffer();//mFMOutputPool_l->requestIIBuffer();

        data.mFMResult.FM_B.Register_High = mFMRegisterPool->requestIIBuffer();
        data.mFMResult.FM_B.Register_Medium = mFMRegisterPool->requestIIBuffer();
        data.mFMResult.FM_B.Register_Low = mFMRegisterPool->requestIIBuffer();

        prepareFM(params, request, data);
    }
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::prepareNextFullSFPOut(SFPOutput &output, const RequestPtr &request, BasicImg &outImg, const FrameInInfo &inInfo, MUINT32 sensorID)
{
    TRACE_FUNC_ENTER();

    const SrcCropInfo srcCropInfo = request->getSrcCropInfo(sensorID);
    const MRect& srcCropRect = srcCropInfo.mSrcCrop;
    MSize resize = MSize(0,0);

    TRACE_FUNC(" Streaming %dx%d srcCropRect (%d,%d)(%dx%d), isSrcCrop %d",
        mPipeUsage.getStreamingSize().w, mPipeUsage.getStreamingSize().h,
        srcCropRect.p.x, srcCropRect.p.y, srcCropRect.s.w, srcCropRect.s.h, srcCropInfo.mIsSrcCrop);

    outImg.mBuffer = request->requestNextFullImg(this, sensorID, resize);
    TRACE_FUNC("sID(%d) Frame %d QFullImg %s", sensorID, request->mRequestNo, outImg.mBuffer == NULL ? "is null" : "" );
    MSize outSize = (resize.w && resize.h) ? resize : srcCropRect.s;
    MRectF domainSrcCrop = request->needEarlyFSCVendorFullImg() ?
        request->getVar<MRectF>(VAR_FSC_RRZO_CROP_REGION, MRectF(0, 0)) : MRectF(srcCropRect.p, srcCropRect.s);

    outImg.mBuffer->getImageBuffer()->setExtParam(outSize);
    outImg.mDomainOffset = domainSrcCrop.p;
    outImg.mDomainTransformScale.w = (float)outSize.w / domainSrcCrop.s.w;
    outImg.mDomainTransformScale.h = (float)outSize.h / domainSrcCrop.s.h;
    outImg.mBuffer->getImageBuffer()->setTimestamp(inInfo.timestamp);
    MY_LOGD_IF(request->needEarlyFSCVendorFullImg(), "mDomainOffset(%f,%f) mDomainTransformScale(%f,%f) from(%f,%f) to(%d,%d) earlyFSC(%d) flag(0x%x)",
        outImg.mDomainOffset.x, outImg.mDomainOffset.y, outImg.mDomainTransformScale.w, outImg.mDomainTransformScale.h,
        domainSrcCrop.s.w, domainSrcCrop.s.h, outSize.w, outSize.h, request->needEarlyFSCVendorFullImg(),
        FSCUtil::getConstrainFlag(mPipeUsage.getFSCMode()));

    output.mBuffer = outImg.mBuffer->getImageBufferPtr();
    output.mTransform = 0;
    output.mCropRect = domainSrcCrop;
    output.mCropDstSize = outSize;
    output.mDMAConstrainFlag = FSCUtil::getConstrainFlag(mPipeUsage.getFSCMode());

    TRACE_FUNC_EXIT();
}

MVOID P2ANode::prepareFDImg(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();
    (void)(data);
    SFPOutput sfpOut;
    if ( request->getFDOutput(sfpOut) )
    {
        Output out;
        sfpOut.convertTo(out);
        out.mPortID.index = EPortIndex_IMG2O;
        frame.mvOut.push_back(out);
    }
    TRACE_FUNC_EXIT();
}

MVOID P2ANode::prepareFDCrop(FrameParams &frame, const RequestPtr &request, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();
    (void)(data);
    SFPOutput output;
    if ( request->getFDOutput(output) )
    {
        if( !output.isCropValid())
        {
            MY_LOGD("default fd crop");
            output.mCropDstSize = output.mBuffer->getImgSize();
            output.mCropRect = MRect(MPoint(0, 0), output.mCropDstSize);
        }
        Feature::P2Util::push_crop(frame, IMG2O_CROP_GROUP, output.mCropRect, output.mCropDstSize);
    }

    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::prepareExtraMdpCrop(const BasicImg &fullImg, std::vector<SFPOutput> &leftOutList)
{
    TRACE_FUNC_ENTER();
    if(fullImg.mBuffer == NULL)
    {
        MY_LOGE("Need Extra MDP but Full Image is NULL !!");
    }
    for(auto&& sfpOut : leftOutList)
    {
        sfpOut.mCropRect.p.x = max(sfpOut.mCropRect.p.x - fullImg.mDomainOffset.x, 0.0f);
        sfpOut.mCropRect.p.y = max(sfpOut.mCropRect.p.y - fullImg.mDomainOffset.y, 0.0f);
    }
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::needFullForExtraOut(std::vector<SFPOutput> &outList)
{
    static const MUINT32 maxMDPOut = 2;
    if(outList.size() > maxMDPOut)
        return MTRUE;
    MUINT32 rotCnt = 0;
    for(auto&& out : outList)
    {
        if(out.mTransform != 0)
        {
            rotCnt += 1;
        }
    }
    return (rotCnt > 1);
}


MVOID P2ANode::enqueFeatureStream(NSCam::NSIoPipe::QParams &params, P2AEnqueData &data)
{
    TRACE_FUNC_ENTER();
    MY_S_LOGD(data.mRequest->mLog, "sensor(%d) Frame %d enque start", mSensorIndex, data.mRequest->mRequestNo);
    data.mRequest->mTimer.startEnqueP2A();
    this->incExtThreadDependency();
    this->enqueNormalStreamBase(mNormalStream, params, data);
    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::init3A()
{
    TRACE_FUNC_ENTER();

    if( mp3A == NULL && SUPPORT_3A_HAL )
    {
        mp3A = MAKE_Hal3A(mSensorIndex, PIPE_CLASS_TAG);
    }
    if( mpISP == NULL && SUPPORT_ISP_HAL )
    {
        mpISP = MAKE_HalISP(mSensorIndex, PIPE_CLASS_TAG);
    }

    MUINT eisMode = mPipeUsage.getEISMode();
    if( EIS_MODE_IS_EIS_22_ENABLED(eisMode) ||
        EIS_MODE_IS_EIS_25_ENABLED(eisMode) ||
        EIS_MODE_IS_EIS_30_ENABLED(eisMode) )
    {
        //Disable OIS
        MY_LOGD("mEisMode: 0x%x => Disable OIS \n", eisMode);
        if( mp3A )
        {
            mp3A->send3ACtrl(E3ACtrl_SetEnableOIS, 0, 0);
        }
        else
        {
            MY_LOGE("mp3A is NULL\n");
        }
        mEisMode = mPipeUsage.getEISMode();
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MVOID P2ANode::uninit3A()
{
    TRACE_FUNC_ENTER();

    if( mp3A )
    {
        //Enable OIS
        if( EIS_MODE_IS_EIS_22_ENABLED(mEisMode) ||
            EIS_MODE_IS_EIS_25_ENABLED(mEisMode) ||
            EIS_MODE_IS_EIS_30_ENABLED(mEisMode) )
        {
            MY_LOGD("mEisMode: 0x%x => Enable OIS \n", mEisMode);
            mp3A->send3ACtrl(E3ACtrl_SetEnableOIS, 1, 0);
            mEisMode = EIS_MODE_OFF;
        }

        // turn OFF 'pull up ISO value to gain FPS'
        AE_Pline_Limitation_T params;
        params. bEnable = MFALSE; // disable
        params. bEquivalent= MTRUE;
        params. u4IncreaseISO_x100= 100;
        params. u4IncreaseShutter_x100= 100;
        mp3A->send3ACtrl(E3ACtrl_SetAEPlineLimitation, (MINTPTR)&params, 0);

        // destroy the instance
        mp3A->destroyInstance(PIPE_CLASS_TAG);
        mp3A = NULL;

    }

    if( mpISP )
    {
        // destroy the instance
        mpISP->destroyInstance(PIPE_CLASS_TAG);
        mpISP = NULL;
    }

    TRACE_FUNC_EXIT();
}

MBOOL P2ANode::prepare3A(NSCam::NSIoPipe::QParams &params, const RequestPtr &request)
{
    TRACE_FUNC_ENTER();
    (void)params;
    (void)request;
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MINT32 P2ANode::prepareOneRawTuning(      MUINT32           sensorID,
                                    const SFPIOMap          &ioMap,
                                          QParams           &params,
                                    const RequestPtr        &request,
                                          P2AEnqueData      &data,
                                          P2ATuningExtra    &extra)
{
    TRACE_FUNC_ENTER();
    MINT32 retIndex = -1;

    Feature::P2Util::P2Pack newPack = Feature::P2Util::P2Pack(request->mP2Pack, request->mP2Pack.mLog, sensorID);
    SmartTuningBuffer tuningBuf = mDynamicTuningPool->request();
    memset(tuningBuf->mpVA, 0, mDynamicTuningPool->getBufSize());
    sp<P2ASrzRecord> srzRec = new P2ASrzRecord();
    data.tuningBufs.push_back(tuningBuf);
    data.tuningSrzs.push_back(srzRec);

    TuningHelper::Input tuningIn(newPack, tuningBuf);
    tuningIn.mpISP = getP2CamContext(sensorID)->getISP();
    tuningIn.mTag = NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    tuningIn.mP2ObjPtr.srz4 = &(srzRec->srz4);
    tuningIn.mP2ObjPtr.hasPQ = MFALSE;

    // --- prepare P2IO ---
    const SFPSensorTuning &tuning = ioMap.getTuning(sensorID);
    const SFPSensorInput &sensorIn = request->getSensorInput(sensorID);
    Feature::P2Util::P2IOPack &io = tuningIn.mIOPack;
    if(tuning.isRRZOin())
    {
        io.mIMGI.mBuffer = sensorIn.mRRZO;
        io.mFlag |= Feature::P2Util::P2Flag::FLAG_RESIZED;
    }
    else
    {
        io.mIMGI.mBuffer = sensorIn.mIMGO;
    }

    if(tuning.isLCSOin())
        io.mLCSO.mBuffer = sensorIn.mLCSO;

    if (io.mIMGI.mBuffer == NULL)
    {
        MY_LOGE("Invalid input buffer");
        TRACE_FUNC_EXIT();
        return retIndex;
    }

    TuningHelper::MetaParam metaParam;
    metaParam.mHalIn = sensorIn.mHalIn;
    metaParam.mAppIn = tuning.isAppPhyMetaIn() ? sensorIn.mAppInOverride : sensorIn.mAppIn;
    metaParam.mExtraAppOut = extra.mExtraAppOut;
    metaParam.mScene = extra.mScene;
    metaParam.mSensorID = sensorID;
    MBOOL needMetaOut = ((ioMap.isGenPath() && sensorID != request->getMasterID()) || (extra.mScene != TuningHelper::Tuning_Normal))
                    ? MFALSE : MTRUE;
    VarMap &varMap = request->getSensorVarMap(sensorID);
    if(needMetaOut)
    {
        metaParam.mHalOut = ioMap.mHalOut;
        metaParam.mAppOut = ioMap.mAppOut;
        if(varMap.tryGet<MRect>(VAR_FD_CROP_ACTIVE_REGION, metaParam.mFdCrop))
        {
            metaParam.mIsFDCropValid = MTRUE;
        }
    }

    // 3DNR
    NR3D::NR3DTuningInfo nr3dTuning;
    NR3DParam Nr3dParam;//default off
    if( !tuning.isDisable3DNR() && metaParam.mScene == TuningHelper::Tuning_Normal)
    {
        NR3D::NR3DMVInfo dftMvInfo;
        nr3dTuning.canEnable3dnrOnFrame = varMap.get<MBOOL>(VAR_3DNR_CAN_ENABLE_ON_FRAME, MFALSE);
        nr3dTuning.isoThreshold = varMap.get<MUINT32>(VAR_3DNR_ISO_THRESHOLD, 100);
        nr3dTuning.mvInfo = varMap.get<NR3D::NR3DMVInfo>(VAR_3DNR_MV_INFO, dftMvInfo);
        nr3dTuning.inputSize = io.mIMGI.mBuffer->getImgSize();
        // If CRZ is used, we must correct following fields and review ISP code
        const SrcCropInfo srcCropInfo = request->getSrcCropInfo(sensorID);
        nr3dTuning.inputCrop = srcCropInfo.mSrcCrop;
        // gyro
        nr3dTuning.gyroData = varMap.get<NR3D::GyroData>(VAR_3DNR_GYRO, NR3D::GyroData());
        //prepare 3DNR35 before setp2isp
        if( !prepare3DNR35(params, request, sensorID, Nr3dParam))
        {
            // set mPrevFullImg to NULL to disable VIPI port
            getP2CamContext(sensorID)->setPrevFullImg(NULL);
        }
        else
        {
            nr3dTuning.pNr3dParam = &Nr3dParam;
        }
        metaParam.mpNr3dTuningInfo = &nr3dTuning;
    }

    FrameParams   frameParam;
    if( !TuningHelper::processIsp_P2A_Raw2Yuv(tuningIn,
                                                frameParam,
                                                metaParam))
    {
        MY_LOGE("Prepare Raw Tuning Failed! Path(%s), sensor(%d),frameNo(%d),mvFrameSize(%zu)",
                ioMap.pathName(), sensorID, request->mRequestNo, params.mvFrameParams.size());
        return retIndex;
    }
    params.mvFrameParams.push_back(frameParam);
    retIndex = params.mvFrameParams.size() - 1;

    TRACE_FUNC_EXIT();

    return retIndex;
}

MBOOL P2ANode::prepareRawTuning(QParams &params, const RequestPtr &request, P2AEnqueData &data, P2ATuningIndex &tuningIndex)
{
    TRACE_FUNC_ENTER();
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    MBOOL dualSlaveValid = request->isSlaveParamValid();
    SFPIOManager &ioMgr = request->mSFPIOManager;
    const SFPIOMap &generalIO = ioMgr.getFirstGeneralIO();
    const SFPIOMap &masterPhyIO = ioMgr.getPhysicalIO(request->mMasterID);
    const SFPIOMap &slavePhyIO = ioMgr.getPhysicalIO(request->mSlaveID);
    const SFPIOMap &masterLargeIO = ioMgr.getLargeIO(request->mMasterID);
    const SFPIOMap &slaveLargeIO = ioMgr.getLargeIO(request->mSlaveID);
    const MBOOL isMergePath_Master = SFPIOMap::isSameTuning(masterPhyIO, generalIO, request->mMasterID);
    const MBOOL isMergePath_Slave = SFPIOMap::isSameTuning(slavePhyIO, generalIO, request->mSlaveID);

    if(generalIO.isValid())
    {
        // -- Master ---
        if(needNormalYuv(request->mMasterID, request))
        {
            P2ATuningExtra extra(isMergePath_Master ? masterPhyIO.mAppOut : NULL);
            tuningIndex.mGenMaster = prepareOneRawTuning(request->mMasterID, generalIO, params, request, data, extra);
        }
        if(needPureYuv(request->mMasterID, request))
        {
            P2ATuningExtra extra(NULL, TuningHelper::Tuning_Pure);
            tuningIndex.mPureMaster = prepareOneRawTuning(request->mMasterID, generalIO, params, request, data, extra);
        }

        // -- Slave ---
        if(dualSlaveValid && needNormalYuv(request->mSlaveID, request))
        {
            P2ATuningExtra extra(isMergePath_Slave ? slavePhyIO.mAppOut : NULL);
            tuningIndex.mGenSlave = prepareOneRawTuning(request->mSlaveID, generalIO, params, request, data, extra);
        }

        if(dualSlaveValid && needPureYuv(request->mSlaveID, request))
        {
            P2ATuningExtra extra(NULL, TuningHelper::Tuning_Pure);
            tuningIndex.mPureSlave = prepareOneRawTuning(request->mSlaveID, generalIO, params, request, data, extra);
        }

        MY_LOGE_IF(!(tuningIndex.isGenMasterValid() || tuningIndex.isPureMasterValid()),
                    "GeneralIO valid but General tuning master inValid !!");
    }

    if(masterPhyIO.isValid())
    {
        MBOOL masterFrameValid = tuningIndex.isGenMasterValid() || tuningIndex.isPureMasterValid();
        if( ! isMergePath_Master || !masterFrameValid)
        {
            P2ATuningExtra extra;
            tuningIndex.mPhyMaster = prepareOneRawTuning(request->mMasterID, masterPhyIO, params, request, data, extra);
        }
        else
        {
            tuningIndex.mPhyMaster = tuningIndex.isPureMasterValid() ? tuningIndex.mPureMaster
                                    : tuningIndex.mGenMaster;
        }
    }

    if(slavePhyIO.isValid() && dualSlaveValid)
    {
        MBOOL slaveFrameValid = tuningIndex.isGenSlaveValid() || tuningIndex.isPureSlaveValid();
        if( ! isMergePath_Slave || !slaveFrameValid)
        {
            P2ATuningExtra extra;
            tuningIndex.mPhySlave = prepareOneRawTuning(request->mSlaveID, slavePhyIO, params, request, data, extra);
        }
        else
        {
            tuningIndex.mPhySlave = tuningIndex.isPureSlaveValid() ? tuningIndex.mPureSlave
                                    : tuningIndex.mGenSlave;
        }
    }

    if(masterLargeIO.isValid())
    {
        P2ATuningExtra extra;
        tuningIndex.mLargeMaster = prepareOneRawTuning(request->mMasterID, masterLargeIO, params, request, data, extra);
    }

    if(slaveLargeIO.isValid())
    {
        P2ATuningExtra extra;
        tuningIndex.mLargeSlave = prepareOneRawTuning(request->mSlaveID, slaveLargeIO, params, request, data, extra);
    }

    MY_LOGI_IF((mLastDualParamValid != dualSlaveValid), "Dual Slave valid (%d)->(%d). slaveID(%d)",
            mLastDualParamValid, dualSlaveValid, request->mSlaveID);
    MY_LOGD("req(%d), TuningIndex, (GN/GP/Ph/L),master(%d/%d/%d/%d), slave(%d/%d/%d/%d)", request->mRequestNo,
            tuningIndex.mGenMaster, tuningIndex.mPureMaster, tuningIndex.mPhyMaster, tuningIndex.mLargeMaster,
            tuningIndex.mGenSlave, tuningIndex.mPureSlave, tuningIndex.mPhySlave, tuningIndex.mLargeSlave);

    mLastDualParamValid = dualSlaveValid;

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL P2ANode::needPureYuv(MUINT32 /*sensorID*/, const RequestPtr &/*request*/)
{
    // TODO need check sensor  & request
    return mPipeUsage.supportPure();
}

MBOOL P2ANode::needNormalYuv(MUINT32 sensorID, const RequestPtr &request)
{
    if(sensorID == request->mMasterID)
    {
        return request->needDisplayOutput(this)
                || request->needRecordOutput(this)
                || request->needExtraOutput(this)
                || request->needFullImg(this, sensorID)
                || request->needNextFullImg(this, sensorID)
                || request->needFOVFEFM()
                || !needPureYuv(sensorID, request);// in master general, normal or pure must exist one.
    }
    else
    {
        return  request->needFullImg(this, sensorID)
                || request->needNextFullImg(this, sensorID)
                || request->needFOVFEFM();
    }
}


} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
