/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "TPINode.h"
#include "MDPWrapper.h"

#define PIPE_CLASS_TAG "TPINode"
#define PIPE_TRACE TRACE_TPI_NODE
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

TPINode::TPINode(const char* name, MUINT32 tpiIndex, const TPI_IO &nodeIO, TPIMgr *mgr)
    : StreamingFeatureNode(name)
    , mTPINodeIndex(tpiIndex)
    , mTPINodeIO(nodeIO)
    , mTPIMgr(mgr)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mData);
    TRACE_FUNC_EXIT();
}

TPINode::~TPINode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID TPINode::setInputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mInputBufferPool = pool;
    mInputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MVOID TPINode::setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mOutputBufferPool = pool;
    mOutputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MBOOL TPINode::onData(DataID id, const BasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived TP[%d]", data.mRequest->mRequestNo, ID2Name(id), mTPINodeIndex);
    MBOOL ret = MFALSE;
    if( id == ID_P2A_TO_VENDOR_FULLIMG ||
        id == ID_BOKEH_TO_VENDOR_FULLIMG ||
        id == ID_VENDOR_TO_NEXT ||
        id == ID_FOV_WARP_TO_VENDOR ||
        id == ID_DUMMY_TO_NEXT_FULLIMG )
    {
        mData.enque(TPIData(data.mData, data.mRequest));
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL TPINode::onData(DataID id, const DualBasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived TP[%d]", data.mRequest->mRequestNo, ID2Name(id), mTPINodeIndex);
    MBOOL ret = MFALSE;
    if( id == ID_P2A_TO_VENDOR_FULLIMG )
    {
        mData.enque(TPIData(data.mData, data.mRequest));
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL TPINode::onData(DataID id, const TPIData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived TP[%d]", data.mRequest->mRequestNo, ID2Name(id), mTPINodeIndex);
    MBOOL ret = MFALSE;
    if( id == ID_VENDOR_TO_NEXT )
    {
        mData.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL TPINode::onData(DataID id, const DepthImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived TP[%d]", data.mRequest->mRequestNo, ID2Name(id), mTPINodeIndex);
    MBOOL ret = MFALSE;
    if( id == ID_DEPTH_TO_VENDOR )
    {
        mData.enque(TPIData(data.mData.mCleanYuvImg, data.mRequest));
        mDepthDatas.enque(data);
        ret = MTRUE;
    }
    else if( id == ID_TOF_TO_NEXT )
    {
        mDepthDatas.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType TPINode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_BYPASS;
    if( HAS_VENDOR_V2(reqInfo.mFeatureMask) )
    {
        policy = IOPOLICY_INOUT_EXCLUSIVE;
    }
    return policy;
}

MBOOL TPINode::getInputBufferPool(const StreamingReqInfo &/*reqInfo*/, android::sp<IBufferPool> &pool, MSize &resize)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    resize = MSize(0, 0);
    if( mInputBufferPool != NULL )
    {
        pool = mInputBufferPool;
        resize = mPipeUsage.getVendorCusSize(MSize(0,0));
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL TPINode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    MBOOL ret = mTPIMgr->initNode(mTPINodeIO.mNodeInfo.mNodeID);
    MY_LOGI("TP[%d]:initNode ret=%d", mTPINodeIndex, ret);
    mIsFirstTPI = (mTPINodeIndex == 0);
    mIsLastTPI = (mTPINodeIndex+1) == mPipeUsage.getTPINodeCount();
    mIsInplace = !!(mTPINodeIO.mNodeInfo.mNodeOption & TPI_NODE_OPT_INPLACE);
    mWaitDepthData = mIsFirstTPI &&
                   (mPipeUsage.supportTOF() ||
                   (mPipeUsage.supportDepthP2() && !mPipeUsage.supportBokeh()));
    if( mWaitDepthData )
    {
        this->addWaitQueue(&mDepthDatas);
    }
    TRACE_FUNC_EXIT();
    return mTPINodeIndex < MAX_TPI_COUNT;
}

MBOOL TPINode::onUninit()
{
    TRACE_FUNC_ENTER();
    mInputBufferPool = NULL;
    mOutputBufferPool = NULL;
    MBOOL ret = mTPIMgr->uninitNode(mTPINodeIO.mNodeID);
    MY_LOGI("TP[%d]:uninitNode ret=%d", mTPINodeIndex, ret);
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL TPINode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    if( mInputBufferPoolAllocateNeed && mInputBufferPool != NULL )
    {
        Timer timer(true);
        EImageFormat fmt = mInputBufferPool->getImageFormat();
        MSize size = mInputBufferPool->getImageSize();
        mInputBufferPool->allocate(mInputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGI("TP[%d] fpipe.tpi.in %s %d buf (0x%x/%s)(%dx%d) in %d ms", mTPINodeIndex, STR_ALLOCATE, mInputBufferPoolAllocateNeed, fmt, toName(fmt), size.w, size.h, timer.getElapsed());
    }
    if( mOutputBufferPoolAllocateNeed && mOutputBufferPool != NULL )
    {
        Timer timer(true);
        EImageFormat fmt = mOutputBufferPool->getImageFormat();
        MSize size = mOutputBufferPool->getImageSize();
        mOutputBufferPool->allocate(mOutputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGI("TP[%d] fpipe.tpi.out %s %d buf (0x%x/%s)(%dx%d) in %d ms", mTPINodeIndex, STR_ALLOCATE, mOutputBufferPoolAllocateNeed, fmt, toName(fmt), size.w, size.h, timer.getElapsed());
    }

    if( !mIsLastTPI && !mIsInplace )
    {
        MSize streamingSize = mPipeUsage.getStreamingSize();
        EImageFormat fullFormat = mPipeUsage.getVendorCusFormat(mPipeUsage.getFullImgFormat());
        for( unsigned i = 0; i < mTPINodeIO.mNodeInfo.mBufferInfoListCount; ++i )
        {
            TPI_BufferInfo info = mTPINodeIO.mNodeInfo.mBufferInfoList[i];
            EImageFormat fmt = toValid(info.mFormat, fullFormat);
            MSize size = toValid(info.mSize, streamingSize);
            unsigned id = info.mBufferID;
            if( mBufferPools[id] == NULL )
            {
                android::sp<IBufferPool> pool;
                pool = ImageBufferPool::create("tpi", size, fmt, ImageBufferPool::USAGE_HW);
                mBufferPools[id] = pool;
                MUINT32 need = 3;
                Timer timer(true);
                pool->allocate(need);
                timer.stop();
                MY_LOGI("TP[%d] fpipe.tpi.%d=0x%x %s %d buf (0x%x/%s)(%dx%d) in %d ms", mTPINodeIndex, i, id, STR_ALLOCATE, need, fmt, toName(fmt), size.w, size.h, timer.getElapsed());
            }
        }
    }

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL TPINode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    for( auto &&it : mBufferPools )
    {
        IBufferPool::destroy(it.second);
    }
    mBufferPools.clear();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL TPINode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    TPIData data;
    DepthImgData depthData;
    RequestPtr request;
    TPIRes in, out;
    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mData.deque(data) )
    {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    }
    if( data.mRequest == NULL )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }
    if( mWaitDepthData )
    {
        if( !mDepthDatas.deque(depthData) )
        {
            MY_LOGE("DepthData deque out of sync");
            return MFALSE;
        }
        if( data.mRequest != depthData.mRequest )
        {
            MY_LOGE("DepthData out of sync");
            return MFALSE;
        }
        data.mData.add(depthData.mData);
    }

    TRACE_FUNC_ENTER();
    request = data.mRequest;
    in = data.mData;
    request->mTimer.startTPI(mTPINodeIndex);
    TRACE_FUNC("Frame %d in Vendor", request->mRequestNo);
    MY_LOGD("TP[%d] sensor(%d) Frame %d process start needTPI:%d inplace:%d bypass:%d", mTPINodeIndex, mSensorIndex, request->mRequestNo, !!request->needVendorV2(), mIsInplace, request->needTPIBypass());

    MBOOL result = MFALSE;
    if( request->needVendorV2() && request->hasGeneralOutput() )
    {
        checkFirstTPI(request, in);
        result = process(request, in, out);
    }
    MY_LOGD("TP[%d] sensor(%d) Frame %d process done in %d ms, result:%d inplace:%d bypass:%d", mTPINodeIndex, mSensorIndex, request->mRequestNo, request->mTimer.getElapsedEnqueTPI(mTPINodeIndex), result, mIsInplace, request->needTPIBypass());

    if( !result )
    {
        handleBypass(request, in, out);
    }
    dumpBuffer(request, in, out);
    handleResultData(request, out);
    request->mTimer.stopTPI(mTPINodeIndex);

    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL TPINode::checkFirstTPI(const RequestPtr &request, TPIRes &in)
{
    MBOOL ret = MTRUE;
    if( mIsFirstTPI )
    {
        BasicImg inYuv, inYuv2, inDepth, inDepthIntensity, outYuv;
        inYuv = in.getSFP(TPI_BUFFER_ID_MTK_YUV);
        inYuv2 = in.getSFP(TPI_BUFFER_ID_MTK_YUV_2);
        inDepth = in.getSFP(TPI_BUFFER_ID_MTK_DEPTH);
        inDepthIntensity = in.getSFP(TPI_BUFFER_ID_MTK_DEPTH_INTENSITY);
        in.setTP(TPI_BUFFER_ID_MTK_OUT_YUV, inYuv);
        in.setTP(TPI_BUFFER_ID_MTK_OUT_YUV_2, inYuv2);
        in.setTP(TPI_BUFFER_ID_MTK_OUT_DEPTH, inDepth);
        in.setTP(TPI_BUFFER_ID_MTK_OUT_DEPTH_INTENSITY, inDepthIntensity);
        inYuv.syncCache(eCACHECTRL_INVALID);
        inYuv2.syncCache(eCACHECTRL_INVALID);
        if( mPipeUsage.supportVendorInplace() )
        {
            outYuv = inYuv;
        }
        else
        {
            outYuv = mOutputBufferPool->requestIIBuffer();
            outYuv.mBuffer->getImageBuffer()->setExtParam(inYuv.mBuffer->getImageBuffer()->getImgSize());
            outYuv.setDomainInfo(inYuv);
        }
        in.setSFP(TPI_BUFFER_ID_MTK_OUT_YUV, outYuv);


        const SFPSensorInput &master = request->getSensorInput(request->mMasterID);
        TRACE_FUNC("TP[%d] appIn=%p, halIn=%p, appDynamicIn=%p", mTPINodeIndex, master.mAppIn, master.mHalIn, master.mAppDynamicIn);
        in.setMeta(TPI_META_ID_MTK_IN_APP, master.mAppIn);
        in.setMeta(TPI_META_ID_MTK_IN_P1_HAL, master.mHalIn);
        in.setMeta(TPI_META_ID_MTK_IN_P1_APP, master.mAppDynamicIn);

        const SFPIOMap &generalIO = request->mSFPIOManager.getFirstGeneralIO();
        in.setMeta(TPI_META_ID_MTK_OUT_P2_APP, generalIO.mAppOut);
        in.setMeta(TPI_META_ID_MTK_OUT_P2_HAL, generalIO.mHalOut);

        if( mPipeUsage.supportDual() )
        {
            const SFPSensorInput &slave = request->getSensorInput(request->mSlaveID);
            in.setMeta(TPI_META_ID_MTK_IN_P1_HAL_2, slave.mHalIn);
            in.setMeta(TPI_META_ID_MTK_IN_P1_APP_2, slave.mAppDynamicIn);
        }

    }
    return ret;
}

unsigned TPINode::findOutID()
{
    MBOOL found = MFALSE;
    unsigned id = TPI_BUFFER_ID_MTK_OUT_YUV;
    for( auto &next : mTPINodeIO.mNextPortMap )
    {
        for( auto &in : next.second )
        {
            if( in.mNode == TPI_NODE_ID_MTK_S_YUV_OUT &&
                in.mPort == TPI_BUFFER_ID_MTK_YUV )
            {
                id = next.first;
                found = MTRUE;
                break;
            }
        }
        if( found )
        {
            break;
        }
    }
    return id;
}

MBOOL TPINode::process(const RequestPtr &request, const TPIRes &in, TPIRes &out)
{
    (void)request;
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    out.mMeta = in.mMeta;
    out.mSFP = in.mSFP;
    for( auto &prev : mTPINodeIO.mPrevPortMap )
    {
        unsigned inID = prev.first;
        auto first = prev.second.begin();
        if( first != prev.second.end() )
        {
            unsigned outID = first->mPort;
            out.setTP(inID, in.getTP(outID));
        }
    }
    // allocate out working buffer
    if( mIsInplace )
    {
        out.setTP(TPI_BUFFER_ID_MTK_OUT_YUV, in.getTP(TPI_BUFFER_ID_MTK_OUT_YUV));
    }
    else if( mIsLastTPI )
    {
        unsigned outID = findOutID();
        out.setTP(outID, in.getSFP(TPI_BUFFER_ID_MTK_OUT_YUV));
    }
    else
    {
        BasicImg main = in.getSFP(TPI_BUFFER_ID_MTK_YUV);
        for( unsigned i = 0; i < mTPINodeIO.mNodeInfo.mBufferInfoListCount; ++i )
        {
            BasicImg buffer;
            unsigned id = mTPINodeIO.mNodeInfo.mBufferInfoList[i].mBufferID;
            if( mBufferPools[id] != NULL )
            {
                buffer.mBuffer = mBufferPools[id]->requestIIBuffer();
                buffer.setDomainInfo(main);
                out.setTP(id, buffer);
            }
        }
    }

    TPI_Meta meta[32];
    TPI_Buffer img[32];
    unsigned imgCount = out.getImgArray(img, 32);
    unsigned metaCount = out.getMetaArray(meta, 32);
    dumpLog(request, in, out, meta, metaCount, img, imgCount);

    request->mTimer.startEnqueTPI(mTPINodeIndex);
    P2_CAM_TRACE_BEGIN(TRACE_DEFAULT, "3rdParty plugin");
    if( !request->needTPIBypass() )
    {
        ret = mTPIMgr->enqueNode(mTPINodeIO.mNodeInfo.mNodeID, request->mRequestNo, meta, metaCount, img, imgCount);
    }
    P2_CAM_TRACE_END(TRACE_DEFAULT);
    request->mTimer.stopEnqueTPI(mTPINodeIndex);

    TRACE_FUNC_EXIT();
    return ret;
}

MVOID TPINode::handleBypass(const RequestPtr &request, const TPIRes &in, TPIRes &out)
{
    TRACE_FUNC_ENTER();
    (void)request;
    //unsigned inID = TPI_BUFFER_ID_MTK_YUV;
    //unsigned outID = TPI_BUFFER_ID_MTK_OUT_YUV;
    //out.setTP(outID, out.getTP(inID));
    out = in;
    TRACE_FUNC_EXIT();
}

MVOID TPINode::dumpLog(const RequestPtr &request, const TPIRes &in, const TPIRes &out, TPI_Meta meta[], unsigned metaCount, TPI_Buffer img[], unsigned imgCount)
{
    (void)in;
    (void)out;

    if( request->needTPILog() )
    {
        MY_LOGD("TP[%d]=0x%x isFirst=%d isLast=%d prev=%zu next=%zu enque imgCount = %d, metaCount = %d",
            mTPINodeIndex, mTPINodeIO.mNodeInfo.mNodeID,
            mIsFirstTPI, mIsLastTPI,
            mTPINodeIO.mPrevPortMap.size(), mTPINodeIO.mNextPortMap.size(),
            imgCount, metaCount);
        for( unsigned i = 0; i < imgCount; ++i )
        {
            unsigned stride = 0, bytes = 0;
            MSize size(0,0);
            IImageBuffer *ptr = img[i].mBufferPtr;
            EImageFormat fmt = eImgFmt_UNKNOWN;
            if( ptr )
            {
                stride = ptr->getBufStridesInBytes(0);
                bytes = ptr->getBufSizeInBytes(0);
                size = ptr->getImgSize();
                fmt = (EImageFormat)ptr->getImgFormat();
            }
            MY_LOGD("TP[%d] img[%d] id:0x%x buffer:%p (%dx%d) fmt=%d(%s) stride=%d bytes=%d",
                mTPINodeIndex, i, img[i].mBufferID, ptr, size.w, size.h, fmt, toName(fmt), stride, bytes);
        }
        for( unsigned i = 0; i < metaCount; ++i )
        {
            MY_LOGD("TP[%d] meta[%d] id:0x%x meta:%p",
                mTPINodeIndex, i, meta[i].mMetaID, meta[i].mMetaPtr);
        }
    }
}

MVOID TPINode::dumpBuffer(const RequestPtr &request, const TPIRes &in, const TPIRes &out)
{
    TRACE_FUNC_ENTER();
    if( request->needTPIDump() || request->needDump() )
    {
        if( mIsFirstTPI )
        {
            dumpData(request, in.getSFP(TPI_BUFFER_ID_MTK_YUV), "tpi.in");
            dumpData(request, in.getSFP(TPI_BUFFER_ID_MTK_YUV_2), "tpi.in2");
            dumpData(request, in.getSFP(TPI_BUFFER_ID_MTK_DEPTH), "tpi.inDepth");
            dumpData(request, in.getSFP(TPI_BUFFER_ID_MTK_DEPTH_INTENSITY), "tpi.inDepthInt");
        }
        if( mIsLastTPI )
        {
            dumpData(request, out.getTP(TPI_BUFFER_ID_MTK_OUT_YUV), "tpi.out");
        }
        TPI_Buffer buffer[32];
        unsigned bufferCount = out.getImgArray(buffer, 32);
        for( unsigned i = 0; i < bufferCount; ++i )
        {
            dumpData(request, buffer[i].mBufferPtr, "tpi_%d_id_0x%x", mTPINodeIndex, buffer[i].mBufferID);
        }
    }

    TRACE_FUNC_EXIT();
}

MVOID TPINode::handleResultData(const RequestPtr &request, const TPIRes &out)
{
    TRACE_FUNC_ENTER();
    if( mIsLastTPI )
    {
        BasicImg outYuv = out.getTP(TPI_BUFFER_ID_MTK_OUT_YUV);
        outYuv.syncCache(eCACHECTRL_FLUSH);
        handleData(ID_VENDOR_TO_NEXT, BasicImgData(outYuv, request));
    }
    else
    {
        handleData(ID_VENDOR_TO_NEXT, TPIData(out, request));
    }
    TRACE_FUNC_EXIT();
}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
