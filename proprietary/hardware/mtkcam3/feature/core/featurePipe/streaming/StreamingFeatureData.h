/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_FEATURE_DATA_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_FEATURE_DATA_H_

#include <unordered_map>
#include "MtkHeader.h"
//#include <mtkcam3/feature/featurePipe/IStreamingFeaturePipe.h>
//#include <mtkcam/common/faces.h>
//#include <mtkcam3/featureio/eis_type.h>
#include <camera_custom_eis.h>

#include <utils/RefBase.h>
#include <utils/Vector.h>

#include <featurePipe/core/include/WaitQueue.h>
#include <featurePipe/core/include/IIBuffer.h>
#include <featurePipe/core/include/IOUtil.h>
#include <featurePipe/core/include/TuningBufferPool.h>

#include "StreamingFeatureTimer.h"
#include "StreamingFeaturePipeUsage.h"

#include "EISQControl.h"
#include "MDPWrapper.h"

#include "tpi/TPIMgr.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

enum Domain { RRZO_DOMAIN, WARP_DOMAIN };

class StreamingFeatureNode;
class StreamingReqInfo;
class SrcCropInfo // This class will do copy from user, please keep size as small as possible
{
public:
    MRect mSrcCrop = MRect(0, 0);
    MBOOL mIMGOin = MFALSE;
    MBOOL mIsSrcCrop = MFALSE;
    MSize mRRZOSize = MSize(0, 0);
    MSize mIMGOSize = MSize(0, 0);
};

class HelperRWData
{
public:
    // Should match to FeaturePipeParam MSG
    // Now only display done & frame done need wait P2AMDP done.
    enum Msg
    {
        Msg_PMDP_Done = 1,
        Msg_Display_Done = 1 << 1,
        Msg_Frame_Done = 1 << 2
    };

    HelperRWData(){}
    ~HelperRWData(){}
    MBOOL isMsgReceived(Msg msg) {return mMsgBits & msg;}
    MVOID markMsgReceived(Msg msg) {mMsgBits |= msg;}

private:
    MUINT32 mMsgBits = 0;
};

struct EISSourceDumpInfo
{
    MBOOL needDump = false;
    MBOOL markFrame = false;
    std::string filePath;
};

class StreamingFeatureRequest : public virtual android::RefBase
{
private:
    // must allocate extParam before everything else
    FeaturePipeParam mExtParam;
    // alias members, do not change initialize order
    NSCam::NSIoPipe::QParams &mQParams;
    android::Vector<NSCam::NSIoPipe::FrameParams> &mvFrameParams;
public:
    // alias members, do not change initialize order
    const StreamingFeaturePipeUsage &mPipeUsage;
    VarMap &mVarMap;
    const Feature::P2Util::P2Pack &mP2Pack;
    const Feature::ILog mLog;
    SFPIOManager &mSFPIOManager;
    MUINT32 mSlaveID = INVALID_SENSOR;
    MUINT32 mMasterID = INVALID_SENSOR;
    std::unordered_map<MUINT32, IORequest<StreamingFeatureNode, StreamingReqInfo>> mIORequestMap;
    IORequest<StreamingFeatureNode, StreamingReqInfo> &mMasterIOReq;

    MUINT32 mFeatureMask;
    const MUINT32 mRequestNo;
    const MUINT32 mRecordNo;
    const MUINT32 mMWFrameNo;
    IStreamingFeaturePipe::eAppMode mAppMode;
    StreamingFeatureTimer mTimer;
    //NSCam::NSIoPipe::QParams mP2A_QParams;
    //NSCam::NSIoPipe::QParams mN3D_P2Node_QParams;
    MINT32 mDebugDump;
    Feature::P2Util::P2DumpType mP2DumpType = Feature::P2Util::P2_DUMP_NONE;;
    HelperRWData mHelperNodeData; // NOTE can only access by helper node thread
private:
    std::unordered_map<MUINT32, FeaturePipeParam> &mSlaveParamMap;
    std::unordered_map<MUINT32, SrcCropInfo> mNonLargeSrcCrops;
    MSize mFullImgSize;
    MBOOL mHasGeneralOutput = MFALSE;

public:
    StreamingFeatureRequest(const StreamingFeaturePipeUsage &pipeUsage, const FeaturePipeParam &extParam, MUINT32 requestNo, MUINT32 recordNo, const EISQState &eisQ);
    ~StreamingFeatureRequest();

    MVOID setDisplayFPSCounter(FPSCounter *counter);
    MVOID setFrameFPSCounter(FPSCounter *counter);
    MBOOL updateSFPIO();
    MVOID calSizeInfo();

    MBOOL updateResult(MBOOL result);
    MBOOL doExtCallback(FeaturePipeParam::MSG_TYPE msg);

    IImageBuffer* getMasterInputBuffer();
    IImageBuffer* getRecordOutputBuffer();

    MBOOL getDisplayOutput(SFPOutput &output);
    MBOOL getRecordOutput(SFPOutput &output);
    MBOOL getExtraOutput(SFPOutput &output);
    MBOOL getExtraOutputs(std::vector<SFPOutput> &outList);
    MBOOL getPhysicalOutput(SFPOutput &output, MUINT32 sensorID);
    MBOOL getLargeOutputs(std::vector<SFPOutput> &outList, MUINT32 sensorID);
    MBOOL getFDOutput(SFPOutput &output);
    EISSourceDumpInfo getEISDumpInfo();

    android::sp<IIBuffer> requestNextFullImg(StreamingFeatureNode *node, MUINT32 sensorID, MSize &resize);

    MBOOL needDisplayOutput(StreamingFeatureNode *node);
    MBOOL needRecordOutput(StreamingFeatureNode *node);
    MBOOL needExtraOutput(StreamingFeatureNode *node);
    MBOOL needFullImg(StreamingFeatureNode *node, MUINT32 sensorID);
    MBOOL needNextFullImg(StreamingFeatureNode *node, MUINT32 sensorID);
    MBOOL needPhysicalOutput(StreamingFeatureNode *node, MUINT32 sensorID);

    MBOOL hasGeneralOutput() const;
    MBOOL hasDisplayOutput() const;
    MBOOL hasRecordOutput() const;
    MBOOL hasExtraOutput() const;
    MBOOL hasPhysicalOutput(MUINT32 sensorID) const;
    MBOOL hasLargeOutput(MUINT32 sensorID) const;

    MSize getEISInputSize() const;
    MSize getFOVAlignSize() const;
    SrcCropInfo getSrcCropInfo(MUINT32 sensorID);

    DECLARE_VAR_MAP_INTERFACE(mVarMap, setVar, getVar, tryGetVar, clearVar);

    MVOID setDumpProp(MINT32 start, MINT32 count, MBOOL byRecordNo);
    MVOID setForceIMG3O(MBOOL forceIMG3O);
    MVOID setForceWarpPass(MBOOL forceWarpPass);
    MVOID setForceGpuOut(MUINT32 forceGpuOut);
    MVOID setForceGpuRGBA(MBOOL forceGpuRGBA);
    MVOID setForcePrintIO(MBOOL forcePrintIO);
    MVOID setEISDumpInfo(const EISSourceDumpInfo& info);

    MBOOL isForceIMG3O() const;
    MBOOL hasSlave(MUINT32 sensorID) const;
    MBOOL isSlaveParamValid() const;
    FeaturePipeParam& getSlave(MUINT32 sensorID);
    const SFPSensorInput& getSensorInput(MUINT32 sensorID) const ;
    VarMap& getSensorVarMap(MUINT32 sensorID);

    MBOOL getMasterFrameBasic(NSCam::NSIoPipe::FrameParams &output);

    // Legacy code for Hal1, w/o dynamic tuning & P2Pack
    MBOOL getMasterFrameTuning(NSCam::NSIoPipe::FrameParams &output);
    MBOOL getMasterFrameInput(NSCam::NSIoPipe::FrameParams &output);
    // -----

    const char* getFeatureMaskName() const;
    MBOOL need3DNR() const;
    MBOOL needVHDR() const;
    MBOOL needEIS() const;
    MBOOL needEIS22() const;
    MBOOL needEIS25() const;
    MBOOL needEIS30() const;
    MBOOL needVendor() const;
    MBOOL needVendorV1() const;
    MBOOL needVendorV2() const;
    MBOOL needVendorMDP() const;
    MBOOL needVendorFullImg() const;
    MBOOL needEarlyFSCVendorFullImg() const;
    MBOOL needWarp() const;
    MBOOL needFullImg() const;
    MBOOL needFEFM() const;
    MBOOL needEarlyDisplay() const;
    MBOOL needP2AEarlyDisplay() const;
    MBOOL skipMDPDisplay() const;
    MBOOL needRSC() const;
    MBOOL needFSC() const;
    MBOOL needDump() const;
    MBOOL needNddDump() const;
    MBOOL isLastNodeP2A() const;
    MBOOL is4K2K() const;
    MBOOL isP2ACRZMode() const;
    EISQ_ACTION getEISQAction() const;
    MUINT32 getEISQCounter() const;
    MBOOL useWarpPassThrough() const;
    MBOOL useDirectGpuOut() const;
    MBOOL needPrintIO() const;
    MUINT32 getMasterID() const;
    MBOOL needFOV() const;
    MBOOL needFOVFEFM() const;
    MBOOL isOnFOVSensor() const;
    MBOOL needEISFullImg() const;
    MBOOL needP2AEarlyEISFullImg() const;
    MBOOL needVMDPEISFullImg() const;
    MBOOL needHWFOVWarp() const;
    MBOOL needTOF() const;

    MUINT32 needTPILog() const;
    MUINT32 needTPIDump() const;
    MUINT32 needTPIScan() const;
    MUINT32 needTPIBypass() const;

    MSize getFOVMarginPixel() const;
    MSize getFSCMaxMarginPixel() const;
    MSizeF getEISMarginPixel() const;
    MRectF getEISCropRegion() const;

    MBOOL needN3D() const;

private:
    MVOID createIOMapByQParams();

    MVOID checkEISQControl();

    MBOOL getCropInfo(NSCam::NSIoPipe::EPortCapbility cap, MUINT32 defCropGroup, NSCam::NSIoPipe::MCrpRsInfo &crop);
    MVOID calNonLargeSrcCrop(SrcCropInfo &info, MUINT32 sensorID);

    void appendEisTag(string& str, MUINT32 mFeatureMask) const;
    void append3DNRTag(string& str, MUINT32 mFeatureMask) const;
    void appendFSCTag(string& str, MUINT32 mFeatureMask) const;
    void appendVendorTag(string& str, MUINT32 mFeatureMask) const;
    void appendFOVTag(string& str, MUINT32 mFeatureMask) const;
    void appendN3DTag(string& str, MUINT32 mFeatureMask) const;
    void appendNoneTag(string& str, MUINT32 mFeatureMask) const;
    void appendDefaultTag(string& str, MUINT32 mFeatureMask) const;

private:
    FPSCounter *mDisplayFPSCounter;
    FPSCounter *mFrameFPSCounter;

    static std::unordered_map<MUINT32, std::string> mFeatureMaskNameMap;

private:
    MBOOL mResult;
    MBOOL mNeedDump;
    MBOOL mForceIMG3O;
    MBOOL mForceWarpPass;
    MUINT32 mForceGpuOut;
    MBOOL mForceGpuRGBA;
    MBOOL mForcePrintIO;
    MBOOL mIs4K2K;
    MBOOL mIsP2ACRZMode;
    EISQState mEISQState;
    EISSourceDumpInfo mEisDumpInfo;

    MUINT32 mTPILog = 0;
    MUINT32 mTPIDump = 0;
    MUINT32 mTPIScan = 0;
    MUINT32 mTPIBypass = 0;
};
typedef android::sp<StreamingFeatureRequest> RequestPtr;
typedef android::sp<IIBuffer> ImgBuffer;
typedef std::unordered_map<MUINT32, android::sp<IBufferPool>> PoolMap;

template <typename T>
class Data
{
public:
    T mData;
    RequestPtr mRequest;

    // lower value will be process first
    MUINT32 mPriority;

    Data()
        : mPriority(0)
    {}

    virtual ~Data() {}

    Data(const T &data, const RequestPtr &request, MINT32 nice = 0)
        : mData(data)
        , mRequest(request)
      , mPriority(request->mRequestNo)
    {
        if( nice > 0 )
        {
            // TODO: watch out for overflow
            mPriority += nice;
        }
    }

    T& operator->()
    {
        return mData;
    }

    const T& operator->() const
    {
        return mData;
    }

    class IndexConverter
    {
    public:
        IWaitQueue::Index operator()(const Data &data) const
        {
            return IWaitQueue::Index(data.mRequest->mRequestNo,
                                     data.mPriority);
        }
        static unsigned getID(const Data &data)
        {
            return data.mRequest->mRequestNo;
        }
        static unsigned getPriority(const Data &data)
        {
            return data.mPriority;
        }
    };
};


class MyFace
{
public:
    MtkCameraFaceMetadata mMeta;
    MtkCameraFace mFaceBuffer[15];
    MtkFaceInfo mPosBuffer[15];
    MyFace()
    {
        mMeta.faces = mFaceBuffer;
        mMeta.posInfo = mPosBuffer;
        mMeta.number_of_faces = 0;
        mMeta.ImgWidth = 0;
        mMeta.ImgHeight = 0;
    }

    MyFace(const MyFace &src)
    {
        mMeta = src.mMeta;
        mMeta.faces = mFaceBuffer;
        mMeta.posInfo = mPosBuffer;
        for( int i = 0; i < 15; ++i )
        {
            mFaceBuffer[i] = src.mFaceBuffer[i];
            mPosBuffer[i] = src.mPosBuffer[i];
        }
    }

    MyFace operator=(const MyFace &src)
    {
        mMeta = src.mMeta;
        mMeta.faces = mFaceBuffer;
        mMeta.posInfo = mPosBuffer;
        for( int i = 0; i < 15; ++i )
        {
            mFaceBuffer[i] = src.mFaceBuffer[i];
            mPosBuffer[i] = src.mPosBuffer[i];
        }
        return *this;
    }
};

class FEFMGroup
{
public:
    ImgBuffer High;
    ImgBuffer Medium;
    ImgBuffer Low;

    ImgBuffer Register_High;
    ImgBuffer Register_Medium;
    ImgBuffer Register_Low;

    MVOID clear();
    MBOOL isValid() const;
};

class FMResult
{
public:
    FEFMGroup FM_A;
    FEFMGroup FM_B;
    FEFMGroup FE;
    FEFMGroup PrevFE;
};

class RSCResult
{
public:
    union RSC_STA_0
    {
        MUINT32 value;
        struct
        {
            MUINT16 sta_gmv_x; // regard RSC_STA as composition of gmv_x and gmv_y
            MUINT16 sta_gmv_y;
        };
        RSC_STA_0()
            : value(0)
        {
        }
    };

    ImgBuffer mMV;
    ImgBuffer mBV;
    MSize     mRssoSize;
    RSC_STA_0 mRscSta; // gmv value of RSC
    MBOOL     mIsValid;

    RSCResult();
    RSCResult(const ImgBuffer &mv, const ImgBuffer &bv, const MSize& rssoSize, const RSC_STA_0& rscSta, MBOOL valid);
};

class FovP2AResult
{
public:
    ImgBuffer mFEO_Master;
    ImgBuffer mFEO_Slave;
    ImgBuffer mFMO_MtoS;
    ImgBuffer mFMO_StoM;
    SmartTuningBuffer mFMTuningBuf0;
    SmartTuningBuffer mFMTuningBuf1;
    MSize mFEInSize_Master;
    MSize mFEInSize_Slave;
};

class FOVResult
{
public:
    ImgBuffer mWarpMap;
    MSize     mWarpMapSize;
    MSize     mWPESize;
    MRect     mDisplayCrop;
    MRect     mRecordCrop;
    MRect     mExtraCrop;
    MSize     mSensorBaseMargin;
    MSize     mRRZOBaseMargin;
    MPoint    mFOVShift;
    float     mFOVScale;
};

class BasicImg
{
public:
    ImgBuffer mBuffer;
    MPointF   mDomainOffset;
    MSizeF    mDomainTransformScale;
    MBOOL     mIsReady;

    BasicImg();
    BasicImg(const ImgBuffer &img);
    BasicImg(const ImgBuffer &img, const MPointF &offset);
    BasicImg(const ImgBuffer &img, const MPointF &offset, const MBOOL &isReady);

    MVOID setSizeInfo(const BasicImg &img);
    MVOID setDomainInfo(const BasicImg &img);
    MBOOL syncCache(NSCam::eCacheCtrl ctrl);
};

class N3DResult
{
public:
    ImgBuffer mFEBInputImg_Master;
    ImgBuffer mFEBInputImg_Slave;
    ImgBuffer mFECInputImg_Master;
    ImgBuffer mFECInputImg_Slave;
    ImgBuffer mFEBO_Master;
    ImgBuffer mFEBO_Slave;
    ImgBuffer mFECO_Master;
    ImgBuffer mFECO_Slave;
    ImgBuffer mFMBO_MtoS;
    ImgBuffer mFMBO_StoM;
    ImgBuffer mFMCO_MtoS;
    ImgBuffer mFMCO_StoM;
    ImgBuffer mCCin_Master;
    ImgBuffer mCCin_Slave;
    ImgBuffer mRectin_Master;
    ImgBuffer mRectin_Slave;
    SmartTuningBuffer tuningBuf1;
    SmartTuningBuffer tuningBuf2;
    SmartTuningBuffer tuningBuf3;
    SmartTuningBuffer tuningBuf4;
    SmartTuningBuffer tuningBuf5;
    SmartTuningBuffer tuningBuf6;
    SmartTuningBuffer tuningBuf7;
    SmartTuningBuffer tuningBuf8;
};

class DualBasicImg
{
public:
    BasicImg mMaster;
    BasicImg mSlave;
    DualBasicImg();
    DualBasicImg(const BasicImg &master);
    DualBasicImg(const BasicImg &master, const BasicImg &slave);
};

class P2AMDPReq
{
public:
    BasicImg                    mMdpIn;
    std::vector<SFPOutput>      mMdpOuts;
};

class HelpReq
{
public:
    enum INTERNAL_MSG_TYPE {MSG_UNKNOWN, MSG_PMDP_DONE};
    FeaturePipeParam::MSG_TYPE  mCBMsg = FeaturePipeParam::MSG_INVALID;
    INTERNAL_MSG_TYPE           mInternalMsg = MSG_UNKNOWN;
    HelpReq();
    HelpReq(FeaturePipeParam::MSG_TYPE msg);
    HelpReq(FeaturePipeParam::MSG_TYPE msg, INTERNAL_MSG_TYPE intMsg);
};

class DepthImg
{
public:
    BasicImg  mCleanYuvImg;
    ImgBuffer mDMBGImg;
    ImgBuffer mDepthMapImg;
    ImgBuffer mDepthIntensity;
    MBOOL     mDepthSucess;
};

class TPIRes
{
public:
    std::map<unsigned, BasicImg> mSFP;
    std::map<unsigned, BasicImg> mTP;
    std::map<unsigned, IMetadata*> mMeta;

public:
    TPIRes();
    TPIRes(const BasicImg &yuv);
    TPIRes(const DualBasicImg &dual);
    MVOID add(const DepthImg &depth);
    BasicImg getSFP(unsigned id) const;
    BasicImg getTP(unsigned id) const;
    IMetadata* getMeta(unsigned id) const;
    MVOID setSFP(unsigned id, const BasicImg &img);
    MVOID setTP(unsigned id, const BasicImg &img);
    MVOID setMeta(unsigned id, IMetadata *meta);
    MUINT32 getImgArray(TPI_Buffer imgs[], unsigned count) const;
    MUINT32 getMetaArray(TPI_Meta metas[], unsigned count) const;
};

typedef Data<ImgBuffer> ImgBufferData;
typedef Data<EIS_HAL_CONFIG_DATA> EisConfigData;
typedef Data<MyFace> FaceData;
typedef Data<FMResult> FMData;
typedef Data<FeaturePipeParam::MSG_TYPE> CBMsgData;
typedef Data<HelpReq> HelperData;
typedef Data<RSCResult> RSCData;
typedef Data<FovP2AResult> FOVP2AData;
typedef Data<FOVResult> FOVData;
typedef Data<BasicImg> BasicImgData;
typedef Data<N3DResult> N3DData;
typedef Data<DualBasicImg> DualBasicImgData;
typedef Data<DepthImg> DepthImgData;
typedef Data<P2AMDPReq> P2AMDPReqData;
typedef Data<TPIRes> TPIData;

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_FEATURE_DATA_H_
