/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "StreamingFeaturePipeUsage.h"
#include "StreamingFeaturePipe.h"
#include "TuningHelper.h"
#include <mtkcam3/feature/eis/eis_ext.h>
#include <mtkcam3/feature/stereo/StereoCamEnum.h>
#include <mtkcam3/feature/3dnr/3dnr_defs.h>
#include <mtkcam3/feature/fsc/fsc_defs.h>
#include <camera_custom_eis.h>
#include <camera_custom_dualzoom.h>

// draft, just for offline fov debug
#include <cutils/properties.h>

#define PIPE_CLASS_TAG "PipeUsage"
#define PIPE_TRACE TRACE_STREAMING_FEATURE_USAGE
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

typedef IStreamingFeaturePipe::UsageHint UsageHint;
#define ADD_3DNR_RSC_SUPPORT 1

StreamingFeaturePipeUsage::StreamingFeaturePipeUsage()
{

}


StreamingFeaturePipeUsage::StreamingFeaturePipeUsage(UsageHint hint, MUINT32 sensorIndex)
    : mUsageHint(hint)
    , mStreamingSize(mUsageHint.mStreamingSize)
    , mVendorMode(mUsageHint.mVendorMode)
    , mVendorCusSize(mUsageHint.mVendorCusSize)
    , m3DNRMode(mUsageHint.m3DNRMode)
    , mFSCMode(mUsageHint.mFSCMode)
    , mDualMode(mUsageHint.mDualMode)
    , mSensorIndex(sensorIndex)
    , mOutCfg(mUsageHint.mOutCfg)
    , mNumSensor(mUsageHint.mAllSensorIDs.size())
    , mSecType(hint.mSecType)
    , mResizedRawSizeMap(mUsageHint.mResizedRawMap)
{
    if (mUsageHint.mMode == IStreamingFeaturePipe::USAGE_DEFAULT)
    {
        mUsageHint.mMode = IStreamingFeaturePipe::USAGE_FULL;
    }

    //remove future
    mUsageHint.mVendorMode = (getPropertyValue(KEY_ENABLE_VENDOR_V1, SUPPORT_VENDOR_NODE) == 1 );
    mVendorMode = mUsageHint.mVendorMode;
    mVendorDebug = getPropertyValue(KEY_DEBUG_TPI, 0);
    mVendorLog = getPropertyValue(KEY_DEBUG_TPI_LOG, 0);
    mEnableVendorCusSize = (getPropertyValue(KEY_ENABLE_VENDOR_V1_SIZE, SUPPORT_VENDOR_SIZE) == 1 );
    mEnableVendorCusFormat = (getPropertyValue(KEY_ENABLE_VENDOR_V1_FORMAT, SUPPORT_VENDOR_FORMAT) == 1 );
    mEnableDummy = (getPropertyValue(KEY_ENABLE_DUMMY, SUPPORT_DUMMY_NODE) == 1 );
    mSupportPure = (getPropertyValue(KEY_ENABLE_PURE_YUV, SUPPORT_PURE_YUV) == 1 ); // TODO test, remove later

    switch (mUsageHint.mMode)
    {
    case IStreamingFeaturePipe::USAGE_P2A_PASS_THROUGH_TIME_SHARING:
        mPipeFunc = 0;
        mP2AMode = P2A_MODE_TIME_SHARING;
        break;
    case IStreamingFeaturePipe::USAGE_P2A_FEATURE:
        mPipeFunc = IStreamingFeaturePipe::PIPE_USAGE_3DNR;
        mP2AMode = P2A_MODE_FEATURE;
        break;
    case IStreamingFeaturePipe::USAGE_FULL:
        mPipeFunc = IStreamingFeaturePipe::PIPE_USAGE_EIS | IStreamingFeaturePipe::PIPE_USAGE_3DNR | IStreamingFeaturePipe::PIPE_USAGE_EARLY_DISPLAY;
        mP2AMode = P2A_MODE_FEATURE;
        break;
    case IStreamingFeaturePipe::USAGE_STEREO_EIS:
        mPipeFunc = IStreamingFeaturePipe::PIPE_USAGE_EIS;
        mP2AMode = P2A_MODE_BYPASS;
        break;
    case IStreamingFeaturePipe::USAGE_P2A_PASS_THROUGH:
    default:
        mPipeFunc = 0;
        mP2AMode = P2A_MODE_NORMAL;
        break;
    }

    if( mEnableVendorCusSize && supportEISNode() )
    {
        MY_LOGW("Force disable VendorCusSize because of EIS constrain");
        mEnableVendorCusSize = MFALSE;
    }

    mDualWideSensorIndex = supportFOV() ? DUALZOOM_WIDE_CAM_ID : 0;
    mDualTeleSensorIndex = supportFOV() ? DUALZOOM_TELE_CAM_ID : 0;

    if(m3DNRMode && !support3DNR())
    {
        MY_LOGE("3DNR not supportted! But UsageHint 3DNR Mode(%d) enabled!!", m3DNRMode);
    }
    MY_LOGI("create usage : sPure(%d)", mSupportPure);
}

MVOID StreamingFeaturePipeUsage::updateTPIUsage(const TPIMgr *mgr)
{
    mTPIUsage.updateUsage(mgr);
}

MBOOL StreamingFeaturePipeUsage::supportP2AP2() const
{
    return !supportDPE();
}

MBOOL StreamingFeaturePipeUsage::supportDepthP2() const
{
    return supportDPE();
}

MBOOL StreamingFeaturePipeUsage::supportLargeOut() const
{
    return mOutCfg.mHasLarge;
}
MBOOL StreamingFeaturePipeUsage::supportPhysicalOut() const
{
    return mOutCfg.mHasPhysical;
}

MBOOL StreamingFeaturePipeUsage::supportIMG3O() const
{
#ifdef SUPPORT_IMG3O
    return MTRUE;
#else
    return MFALSE;
#endif
}

MBOOL StreamingFeaturePipeUsage::supportEISNode() const
{
    return (mPipeFunc & IStreamingFeaturePipe::PIPE_USAGE_EIS) &&
           ( EIS_MODE_IS_EIS_30_ENABLED(mUsageHint.mEISInfo.mode) ||
             EIS_MODE_IS_EIS_25_ENABLED(mUsageHint.mEISInfo.mode) ||
             EIS_MODE_IS_EIS_22_ENABLED(mUsageHint.mEISInfo.mode) );
}

MBOOL StreamingFeaturePipeUsage::supportWarpNode() const
{
    return supportEISNode();
}

MBOOL StreamingFeaturePipeUsage::supportRSCNode() const
{
    return ((mPipeFunc & IStreamingFeaturePipe::PIPE_USAGE_EIS) &&
           supportEIS_30() && EIS_MODE_IS_EIS_IMAGE_ENABLED(mUsageHint.mEISInfo.mode)) ||
           support3DNRRSC();
}

MBOOL StreamingFeaturePipeUsage::supportTOFNode() const
{
    return supportTOF();
}

MBOOL StreamingFeaturePipeUsage::supportP2ALarge() const
{
    return mOutCfg.mHasLarge;
}

MBOOL StreamingFeaturePipeUsage::support4K2K() const
{
    return is4K2K(mStreamingSize);
}

MBOOL StreamingFeaturePipeUsage::supportFOVCombineEIS() const
{
    MUINT32 videoType = mUsageHint.mEISInfo.videoConfig;
    return supportFOV() && EISCustom::isEnabledFOVWarpCombine(videoType) && !supportVendor();
}

MBOOL StreamingFeaturePipeUsage::supportTimeSharing() const
{
    return mP2AMode == P2A_MODE_TIME_SHARING;
}

MBOOL StreamingFeaturePipeUsage::supportP2AFeature() const
{
    return mP2AMode == P2A_MODE_FEATURE;
}

MBOOL StreamingFeaturePipeUsage::supportBypassP2A() const
{
    return mP2AMode == P2A_MODE_BYPASS;
}

MBOOL StreamingFeaturePipeUsage::supportYUVIn() const
{
    return mP2AMode == P2A_MODE_BYPASS;
}

MBOOL StreamingFeaturePipeUsage::supportPure() const
{
    return mSupportPure;
}

MBOOL StreamingFeaturePipeUsage::supportEIS_22() const
{
    return EIS_MODE_IS_EIS_22_ENABLED(mUsageHint.mEISInfo.mode) && supportEISNode();
}

MBOOL StreamingFeaturePipeUsage::supportEIS_25() const
{
    return EIS_MODE_IS_EIS_25_ENABLED(mUsageHint.mEISInfo.mode) && supportEISNode();
}

MBOOL StreamingFeaturePipeUsage::supportEIS_30() const
{
    return EIS_MODE_IS_EIS_30_ENABLED(mUsageHint.mEISInfo.mode) && supportEISNode();
}

MBOOL StreamingFeaturePipeUsage::supportEIS_Q() const
{
    return ( supportEIS_25() || supportEIS_30() ) && EIS_MODE_IS_EIS_QUEUE_ENABLED(mUsageHint.mEISInfo.mode);
}

MBOOL StreamingFeaturePipeUsage::supportEIS_TSQ() const
{
    return mUsageHint.mUseTSQ && supportEIS_Q();
}

MBOOL StreamingFeaturePipeUsage::supportFEFM() const
{
    return supportEIS_25() && EIS_MODE_IS_EIS_IMAGE_ENABLED(mUsageHint.mEISInfo.mode);
}

MBOOL StreamingFeaturePipeUsage::supportRSC() const
{
    return supportEIS_30() && EIS_MODE_IS_EIS_IMAGE_ENABLED(mUsageHint.mEISInfo.mode);
}

MBOOL StreamingFeaturePipeUsage::supportWPE() const
{
    return ( supportEIS_30() || supportFOV() ) && NSCam::NSIoPipe::WPEQuerySupport();
}

MBOOL StreamingFeaturePipeUsage::supportWarpCrop() const
{
    if( supportWPE() )
    {
        return !USE_WPE_STAND_ALONE;
    }
    return SUPPORT_GPU_CROP;
}

MBOOL StreamingFeaturePipeUsage::supportDual() const
{
    return  getNumSensor() >= 2;
}

MBOOL StreamingFeaturePipeUsage::supportN3D() const
{
    return MTKCAM_HAVE_DUALCAM_DENOISE_SUPPORT &&
           (mDualMode & v1::Stereo::E_STEREO_FEATURE_DENOISE);
}

MBOOL StreamingFeaturePipeUsage::supportFOV() const
{
    return SUPPORT_FOV &&
           (mDualMode == v1::Stereo::E_DUALCAM_FEATURE_ZOOM);
}

MBOOL StreamingFeaturePipeUsage::supportDepth() const
{
    return supportDPE() || supportTOF();
}

MBOOL StreamingFeaturePipeUsage::supportDPE() const
{
    return SUPPORT_VSDOF &&
            ( (mDualMode & v1::Stereo::E_STEREO_FEATURE_VSDOF) ||
              (mDualMode & v1::Stereo::E_STEREO_FEATURE_MTK_DEPTHMAP) );
}

MBOOL StreamingFeaturePipeUsage::supportBokeh() const
{
    return SUPPORT_VSDOF &&
            (mDualMode & v1::Stereo::E_STEREO_FEATURE_VSDOF);
}

MBOOL StreamingFeaturePipeUsage::supportTOF() const
{
    //return mDualMode == v1::Stereo::E_STEREO_FEATURE_TOF;
    return MFALSE;
}

MBOOL StreamingFeaturePipeUsage::support3DNR() const
{
    MBOOL ret = MFALSE;
    if(!supportIMG3O())
    {
        return MFALSE;
    }
    // could use property to enalbe 3DNR
    // assign value at getPipeUsageHint
    if (this->is3DNRModeMaskEnable(NR3D::E3DNR_MODE_MASK_HAL_FORCE_SUPPORT) ||
        this->is3DNRModeMaskEnable(NR3D::E3DNR_MODE_MASK_UI_SUPPORT))
    {
        ret = this->supportP2AFeature();
    }
    return ret;
}

MBOOL StreamingFeaturePipeUsage::support3DNRRSC() const
{
    MBOOL ret = MFALSE;
#if ADD_3DNR_RSC_SUPPORT
    if (support3DNR() && is3DNRModeMaskEnable(NR3D::E3DNR_MODE_MASK_RSC_EN))
    {
        ret = MTRUE;
    }
#endif // ADD_3DNR_RSC_SUPPORT
    return ret;
}

MBOOL StreamingFeaturePipeUsage::is3DNRModeMaskEnable(NR3D::E3DNR_MODE_MASK mask) const
{
    return (m3DNRMode & mask);
}

MBOOL StreamingFeaturePipeUsage::supportFSC() const
{
    return (mUsageHint.mFSCMode & NSCam::FSC::EFSC_MODE_MASK_FSC_EN);
}

MBOOL StreamingFeaturePipeUsage::supportVendorFSCFullImg() const
{
    return supportVendor() && supportFSC() && !supportEISNode();
}

MBOOL StreamingFeaturePipeUsage::supportFull_YUY2() const
{
    return USE_YUY2_FULL_IMG && supportWPE() ;
}

MBOOL StreamingFeaturePipeUsage::supportGraphicBuffer() const
{
    return !NSCam::NSIoPipe::WPEQuerySupport();
}

EImageFormat StreamingFeaturePipeUsage::getFullImgFormat() const
{
    return supportFull_YUY2() ? eImgFmt_YUY2 : eImgFmt_YV12;
}

MBOOL StreamingFeaturePipeUsage::supportDummy() const
{
    return mEnableDummy;
}

MBOOL StreamingFeaturePipeUsage::isDynamicTuning() const
{
    return mUsageHint.mDynamicTuning;
}

MBOOL StreamingFeaturePipeUsage::isQParamIOValid() const
{
    return mUsageHint.mQParamIOValid;
}

std::vector<MUINT32> StreamingFeaturePipeUsage::getAllSensorIDs() const
{
    return mUsageHint.mAllSensorIDs;
}

MUINT32 StreamingFeaturePipeUsage::getMode() const
{
    return mUsageHint.mMode;
}

MUINT32 StreamingFeaturePipeUsage::getEISMode() const
{
    return mUsageHint.mEISInfo.mode;
}

MUINT32 StreamingFeaturePipeUsage::getEISFactor() const
{
    return mUsageHint.mEISInfo.factor ? mUsageHint.mEISInfo.factor : 100;
}

MUINT32 StreamingFeaturePipeUsage::getSensorModule() const
{
    return mUsageHint.mSensorModule;
}

MUINT32 StreamingFeaturePipeUsage::getVendorMode() const
{
    return mVendorMode;
}

MUINT32 StreamingFeaturePipeUsage::getDualMode() const
{
    return mDualMode;
}

MUINT32 StreamingFeaturePipeUsage::get3DNRMode() const
{
    return m3DNRMode;
}

MUINT32 StreamingFeaturePipeUsage::getFSCMode() const
{
    return mFSCMode;
}

TP_MASK_T StreamingFeaturePipeUsage::getTPMask() const
{
    return mUsageHint.mTP;
}

IMetadata StreamingFeaturePipeUsage::getAppSessionMeta() const
{
    return mUsageHint.mAppSessionMeta;
}

MSize StreamingFeaturePipeUsage::getStreamingSize() const
{
    return mStreamingSize;
}

MSize StreamingFeaturePipeUsage::getRrzoSizeByIndex(MUINT32 sID)
{
    MSize result= {0, 0};
    if (sID >= mResizedRawSizeMap.size()) {
        MY_LOGE("sensor ID(%d) >= resized raw size list(%zu)",
                sID, mResizedRawSizeMap.size());
        return result;
    }
    result = mResizedRawSizeMap[sID];
    return result;
}

MUINT32 StreamingFeaturePipeUsage::getNumSensor() const
{
    return mNumSensor;
}

MUINT32 StreamingFeaturePipeUsage::getNumP2ABuffer() const
{
    MUINT32 num = (mOutCfg.mMaxOutNum > 2) ? 3 : 0; // need full for additional MDP run ( no need consider physical & large)
    num = max(num, get3DNRBufferNum().mBasic);
    num = max(num, getVendorBufferNum().mBasic);

    if( supportFOV() )
    {
        num = max(num, get3DNRBufferNum().mBasic);
        num = max(num, getDualFOVBufferNum().mBasic);
    }
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumP2APureBuffer() const
{
    MUINT32 num = max((MUINT32)3, getVendorBufferNum().mBasic);
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumDepthImgBuffer() const
{
    // TODO consider DepthPipe buffer depth
    return 3;
}

MUINT32 StreamingFeaturePipeUsage::getNumBokehOutBuffer() const
{
    // TODO need confirm by VSDOF owner
    return 3;
}

MUINT32 StreamingFeaturePipeUsage::getNumP2ATuning() const
{
    MUINT32 num = MIN_P2A_TUNING_BUF_NUM;
    if(mOutCfg.mHasPhysical)
    {
        num *= 2;
    }
    if(supportP2ALarge())
    {
        num *= 2;
    }
    num = max(num, getNumP2ABuffer());
    if( supportDual() )
    {
        num *= 2;
    }

    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumWarpInBuffer() const
{
    MUINT32 num = 0;
    num = max(num, getEISBufferNum().mBasic);
    num += getVendorBufferNum().mBasic;
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumExtraWarpInBuffer() const
{
    MUINT32 num = 0;
    num = max(num, getEISBufferNum().mExtra);
    num += getVendorBufferNum().mExtra;
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumWarpOutBuffer() const
{
    return supportWarpNode() ? 3 : 0;
}

MUINT32 StreamingFeaturePipeUsage::getNumFOVWarpOutBuffer() const
{
    MUINT32 num = 0;
    if( supportFOV() && supportVendor() )
    {
        num = max(getDualFOVBufferNum().mBasic, getVendorBufferNum().mBasic);
    }
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumVendorInBuffer() const
{
    MUINT32 num = 0;
    num = max(num,  getVendorBufferNum().mBasic);
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumVendorOutBuffer() const
{
    MUINT32 num = 0;
    num = max(num,  getVendorBufferNum().mBasic);
    return num;
}

MUINT32 StreamingFeaturePipeUsage::getNumTPIInBuffer() const
{
    return mTPIUsage.getNumInBuffer() * getNumSensor();
}

MUINT32 StreamingFeaturePipeUsage::getNumTPIOutBuffer() const
{
    return mTPIUsage.getNumOutBuffer();
}

StreamingFeaturePipeUsage::BufferNumInfo StreamingFeaturePipeUsage::get3DNRBufferNum() const
{
    MUINT32 num = 0;
    if( support3DNR() )
    {
        num = 3;
        if( supportFOV() )
        {
            num += 1;
        }
    }

    return BufferNumInfo(num);
}

StreamingFeaturePipeUsage::BufferNumInfo StreamingFeaturePipeUsage::getEISBufferNum() const
{
    MUINT32 basic = 0, extra = 0;
    if( supportEISNode() )
    {
        basic = 5;
        if( supportEIS_Q() )
        {
            extra = getEISQueueSize();
        }
    }
    return BufferNumInfo(basic, extra);
}

StreamingFeaturePipeUsage::BufferNumInfo StreamingFeaturePipeUsage::getVendorBufferNum() const
{
    return supportVendor() ? BufferNumInfo(3+1) : BufferNumInfo(0);
}

StreamingFeaturePipeUsage::BufferNumInfo StreamingFeaturePipeUsage::getDualFOVBufferNum() const
{
    return supportFOV() ? BufferNumInfo(3) : BufferNumInfo(0);
}

MUINT32 StreamingFeaturePipeUsage::getSensorIndex() const
{
    return mSensorIndex;
}

MUINT32 StreamingFeaturePipeUsage::getDualSensorIndex_Wide() const
{
    return mDualWideSensorIndex;
}

MUINT32 StreamingFeaturePipeUsage::getDualSensorIndex_Tele() const
{
    return mDualTeleSensorIndex;
}

MUINT32 StreamingFeaturePipeUsage::getEISQueueSize() const
{
    return mUsageHint.mEISInfo.queueSize;
}

MUINT32 StreamingFeaturePipeUsage::getEISStartFrame() const
{
    return mUsageHint.mEISInfo.startFrame;
}

MUINT32 StreamingFeaturePipeUsage::getEISVideoConfig() const
{
    return mUsageHint.mEISInfo.videoConfig;
}

MUINT32 StreamingFeaturePipeUsage::getWarpPrecision() const
{
    return supportWPE() ? 5 : WARP_MAP_PRECISION_BIT;
}

MUINT32 StreamingFeaturePipeUsage::supportVendor(MUINT32 ver) const
{
    MUINT32 v1 = mVendorMode ? 1 : 0;
    MUINT32 v2 = mTPIUsage.supportTPI() ? 2 : 0;
    if( mP2AMode == P2A_MODE_FEATURE )
    {
        ver = ver == 1 ? v1 : ver == 2 ? v2 : v1 ? v1 : v2;
    }
    return ver;
}

MBOOL StreamingFeaturePipeUsage::supportVendorDebug() const
{
    return mVendorDebug;
}

MBOOL StreamingFeaturePipeUsage::supportVendorLog() const
{
    return mVendorLog;
}

MBOOL StreamingFeaturePipeUsage::supportVendorInplace() const
{
    return supportVendor(2) && mTPIUsage.supportInplace();
}

MBOOL StreamingFeaturePipeUsage::supportVendorCusSize() const
{
    //return supportVendor(1) && mEnableVendorCusSize;
    return supportVendor(2) && mTPIUsage.supportCustomSize();
}


MBOOL StreamingFeaturePipeUsage::supportVendorCusFormat() const
{
    //return supportVendor(1) && mEnableVendorCusFormat;
    return supportVendor(2) && mTPIUsage.supportCustomFormat();
}

MBOOL StreamingFeaturePipeUsage::supportVendorFullImg() const
{
    // always use separate vendor fullimg
    return MTRUE;
}

MSize StreamingFeaturePipeUsage::getVendorCusSize(const MSize &original) const
{
    //return supportVendorCusSize() ? mVendorCusSize : original;
    return supportVendorCusSize() ? mTPIUsage.getCustomSize(original) : original;
}

EImageFormat StreamingFeaturePipeUsage::getVendorCusFormat(const EImageFormat &original) const
{
    //return supportVendorCusFormat() ? SUPPORT_VENDOR_FULL_FORMAT : original;
    EImageFormat fmt = supportVendorCusFormat() ? mTPIUsage.getCustomFormat(original) : original;
    return toValid(fmt, original);
}

TPI_SCENARIO_TYPE StreamingFeaturePipeUsage::toTPIScenario() const
{
    TPI_SCENARIO_TYPE type = TPI_SCENARIO_UNKNOWN;
    switch( mDualMode )
    {
    case v1::Stereo::E_STEREO_FEATURE_THIRD_PARTY:
        type = TPI_SCENARIO_STREAMING_DUAL_BASIC;
        break;
    case v1::Stereo::E_STEREO_FEATURE_MTK_DEPTHMAP:
        type = TPI_SCENARIO_STREAMING_DUAL_DEPTH;
        break;
    case v1::Stereo::E_STEREO_FEATURE_VSDOF:
        type = TPI_SCENARIO_STREAMING_DUAL_VSDOF;
        break;
    default:
    //case v1::Stereo::E_STREO_FEATURE_DENOISE:
    //case v1::Stereo::E_DUALCAM_FEATURE_ZOOM:
        type = TPI_SCENARIO_STREAMING_SINGLE;
        break;
    }
    return type;
}

MUINT32 StreamingFeaturePipeUsage::getTPINodeCount() const
{
    return mTPIUsage.getNodeCount();
}

TPI_IO StreamingFeaturePipeUsage::getTPINodeIO(MUINT32 index) const
{
    return mTPIUsage.getNodeIO(index);
}

MBOOL StreamingFeaturePipeUsage::isSecureP2() const
{
    return (mSecType != NSCam::SecType::mem_normal);
}

NSCam::SecType StreamingFeaturePipeUsage::getSecureType() const
{
    return mSecType;
}

} // NSFeaturePipe
} // NSCamFeature
} // NSCam
