/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_TPI_USAGE_H_
#define _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_TPI_USAGE_H_

#include <mtkcam/def/common.h>
#include <map>
#include <vector>
#include "tpi/TPIMgr.h"
#include "DebugControl.h"

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

class TPIUsage
{
public:
    TPIUsage();

    MVOID updateUsage(const TPIMgr *mgr);

    MBOOL supportTPI() const;
    MBOOL isValidIndex(MUINT32 index) const;
    MUINT32 getNodeCount() const;
    TPI_IO getNodeIO(MUINT32 index) const;

    MBOOL supportInplace() const;
    MBOOL supportYUV() const;
    MBOOL supportDepth() const;
    MBOOL supportPure() const;

    MBOOL supportCustomFullImg() const;
    MBOOL supportCustomFormat() const;
    MBOOL supportCustomSize() const;
    MSize getCustomSize() const;
    MSize getCustomSize(const MSize &original) const;
    EImageFormat getCustomFormat() const;
    EImageFormat getCustomFormat(EImageFormat orginal) const;
    MBOOL supportReadWriteBuffer() const;

    MBOOL getNumInBuffer() const;
    MBOOL getNumOutBuffer() const;

private:
    MVOID findCustomSetting(const TPI_NodeInfo &node, EImageFormat &fmt, MSize &size);
    MBOOL supportNodeOption(MUINT32 option) const;
    MBOOL supportCustomOption(MUINT32 option) const;

private:
    MBOOL mEnable = MFALSE;
    MUINT32 mNodeCount = 0;
    TPI_Session mSession;

    EImageFormat mCustomFormat = eImgFmt_YV12;
    MSize mCustomSize = MSize(0,0);

    TPI_IOMap mNodeMap;
    TPI_IO mInNode;
    TPI_IO mOutNode;
    std::vector<TPI_IO> mNodes;
};

} // NSFeaturePipe
} // NSCamFeature
} // NSCam

#endif // _MTK_CAMERA_STREAMING_FEATURE_PIPE_STREAMING_TPI_USAGE_H_
