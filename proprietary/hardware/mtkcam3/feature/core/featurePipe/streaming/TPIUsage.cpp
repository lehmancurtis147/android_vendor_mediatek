/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "TPIUsage.h"
#include "StreamingFeature_Common.h"

#include "DebugControl.h"
#define PIPE_CLASS_TAG "TPIUsage"
#define PIPE_TRACE TRACE_TPI_USAGE
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

TPIUsage::TPIUsage()
{
}

MVOID TPIUsage::updateUsage(const TPIMgr *mgr)
{
    mEnable = MFALSE;
    mNodeCount = 0;
    mNodeMap.clear();
    mNodes.clear();
    mCustomFormat = eImgFmt_UNKNOWN;
    mCustomSize = MSize(0,0);

    if( mgr && mgr->getSessionInfo(mSession) && mSession.mNodeInfoListCount )
    {
        mNodeMap = mgr->toIOMap(mSession);

        if( mNodeMap.count(TPI_NODE_ID_MTK_S_YUV) &&
            mNodeMap.count(TPI_NODE_ID_MTK_S_YUV_OUT) )
        {
            mInNode = mNodeMap[TPI_NODE_ID_MTK_S_YUV];
            mOutNode = mNodeMap[TPI_NODE_ID_MTK_S_YUV_OUT];
            mNodes.reserve(mSession.mNodeInfoListCount);

            unsigned index = TPI_NODE_ID_MTK_S_YUV;
            while(index != TPI_NODE_ID_MTK_S_YUV_OUT)
            {
                auto it = mNodeMap.find(index);
                MY_LOGD("target=0x%x find=%d", index, it != mNodeMap.end());
                if( it != mNodeMap.end() )
                {
                    MY_LOGD("travelCount=%d prev=%zu next=%zu", it->second.mTravelCount, it->second.mPrevPortMap.size(), it->second.mNextPortMap.size());
                }
                if( it == mNodeMap.end() ||
                    it->second.mTravelCount ||
                    !it->second.mNextPortMap.size() )
                {
                    break;
                }
                it->second.mTravelCount += 1;
                if( it->first != TPI_NODE_ID_MTK_S_YUV )
                {
                    MY_LOGD("push_back 0x%x", it->first);
                    mNodes.push_back(it->second);
                }
                if( !it->second.mNextPortMap.begin()->second.size() )
                {
                    break;
                }
                index = it->second.mNextPortMap.begin()->second.begin()->mNode;
            }
            if( mNodes.size() > MAX_TPI_COUNT )
            {
                MY_LOGW("number of TPI(%zu) exceeds limit(%d), skip all",
                        mNodes.size(), MAX_TPI_COUNT);
                mNodes.clear();
            }
            if( index == TPI_NODE_ID_MTK_S_YUV_OUT &&
                mNodes.size() )
            {
                mEnable = MTRUE;
                mNodeCount = mNodes.size();

                findCustomSetting(mInNode.mNodeInfo, mCustomFormat, mCustomSize);
                MY_LOGI("option(%x) fmt(%d/%s) size(%dx%d)", mInNode.mNodeInfo.mCustomOption, mCustomFormat, toName(mCustomFormat), mCustomSize.w, mCustomSize.h);
            }
        }
    }

    MY_LOGD("enable:%d count:%d/%zu/%zu", mEnable, mNodeCount, mNodes.size(), mNodeMap.size());
}

MBOOL TPIUsage::supportTPI() const
{
    return mEnable;
}

MUINT32 TPIUsage::getNodeCount() const
{
    return mNodeCount;
}

MBOOL TPIUsage::isValidIndex(MUINT32 index) const
{
    return mEnable && index >= 0 && index < mNodeCount;
}

TPI_IO TPIUsage::getNodeIO(MUINT32 index) const
{
    TPI_IO io;
    if( isValidIndex(index) )
    {
        io = mNodes[index];
    }
    return io;
}

MBOOL TPIUsage::supportInplace() const
{
    return supportNodeOption(TPI_NODE_OPT_INPLACE);
}

MBOOL TPIUsage::supportYUV() const
{
    return mEnable;
}

MBOOL TPIUsage::supportDepth() const
{
    return supportCustomOption(TPI_MTK_OPT_STREAMING_DEPTH);
}

MBOOL TPIUsage::supportPure() const
{
    return supportCustomOption(TPI_MTK_OPT_STREAMING_PURE);
}

MBOOL TPIUsage::supportCustomFullImg() const
{
    return supportCustomFormat() || supportCustomSize();
}

MBOOL TPIUsage::supportCustomFormat() const
{
    return supportCustomOption(TPI_MTK_OPT_STREAMING_CUSTOM_FORMAT);
}

MBOOL TPIUsage::supportCustomSize() const
{
    return supportCustomOption(TPI_MTK_OPT_STREAMING_CUSTOM_SIZE);
}

MSize TPIUsage::getCustomSize() const
{
    return mCustomSize;
}

MSize TPIUsage::getCustomSize(const MSize &original) const
{
    return supportCustomSize() ? mCustomSize : original;
}

EImageFormat TPIUsage::getCustomFormat() const
{
    return mCustomFormat;
}

EImageFormat TPIUsage::getCustomFormat(EImageFormat original) const
{
    return supportCustomFormat() ? mCustomFormat : original;
}

MBOOL TPIUsage::supportReadWriteBuffer() const
{
    return supportCustomOption(TPI_MTK_OPT_STREAMING_READ_WRITE);
}

MBOOL TPIUsage::getNumInBuffer() const
{
    return mEnable ? mNodeCount + 3 : 0;
}

MBOOL TPIUsage::getNumOutBuffer() const
{
    return (mEnable && !supportInplace()) ? mNodeCount + 3 : 0;
}

MVOID TPIUsage::findCustomSetting(const TPI_NodeInfo &node, EImageFormat &fmt, MSize &size)
{
    fmt = eImgFmt_UNKNOWN;
    size = MSize(0,0);
    for( unsigned i = 0; i < node.mBufferInfoListCount; ++i )
    {
        if( node.mBufferInfoList[i].mBufferID == TPI_BUFFER_ID_MTK_OUT_YUV )
        {
            fmt = node.mBufferInfoList[i].mFormat;
            size = node.mBufferInfoList[i].mSize;
            break;
        }
    }
}

MBOOL TPIUsage::supportNodeOption(MUINT32 option) const
{
    return mEnable &&
           (mInNode.mNodeInfo.mNodeOption & option);
}

MBOOL TPIUsage::supportCustomOption(MUINT32 option) const
{
    return mEnable &&
           (mInNode.mNodeInfo.mCustomOption & option);
}

} // NSFeaturePipe
} // NSCamFeature
} // NSCam
