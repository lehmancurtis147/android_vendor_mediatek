/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define PIPE_CLASS_TAG "TuningHelper"
#define PIPE_MODULE_TAG "TuningHelper"
//#define PIPE_TRACE TRACE_TUNING_HELPER
#include <featurePipe/core/include/PipeLog.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/utils/metadata/IMetadata.h>
#include <mtkcam3/feature/3dnr/util_3dnr.h>
#include <mtkcam3/feature/utils/p2/P2Util.h>
#include <mtkcam3/feature/utils/p2/P2Trace.h>

#include "P2CamContext.h"
#include "TuningHelper.h"
#include <isp_tuning/isp_tuning.h>


namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

using namespace Feature;
using namespace Feature::P2Util;

MVOID
updateInputMeta_Raw2Yuv(
                      TuningHelper::MetaParam&  metaParam,
                      NS3Av3::MetaSet_T&        inMetaSet)
{
    if(metaParam.mScene == TuningHelper::Tuning_Normal)
    {
        sp<P2CamContext> p2CamContext = getP2CamContext(metaParam.mSensorID);
        if (p2CamContext != NULL && p2CamContext->get3dnr() != NULL && metaParam.mpNr3dTuningInfo != NULL) // Run only if 3DNR is enabled
            p2CamContext->get3dnr()->updateISPMetadata(&inMetaSet.halMeta, *(metaParam.mpNr3dTuningInfo));
    }
    else if(metaParam.mScene == TuningHelper::Tuning_Pure)
    {
        // TODO get profile maybe from meta or custom?   Now just use N3D Preview
        IMetadata::setEntry<MUINT8>(&inMetaSet.halMeta, MTK_3A_ISP_PROFILE, NSIspTuning::EIspProfile_N3D_Preview);
    }
}

MVOID
updateOutputMeta_Raw2Yuv( TuningHelper::MetaParam& metaParam)
{
    if(metaParam.mHalOut != NULL)
    {
        if(metaParam.mIsFDCropValid)
        {
            IMetadata::setEntry<MRect>(metaParam.mHalOut, MTK_P2NODE_FD_CROP_REGION, metaParam.mFdCrop);
        }
    }

}

MBOOL
TuningHelper::
processIsp_P2A_Raw2Yuv(
                const Input&                   in,
                      NSIoPipe::FrameParams&   frameParam,
                      MetaParam&               metaParam)
{
    if( (in.mpISP == NULL && SUPPORT_ISP_HAL) || in.mTuningBuf == NULL)
    {
        MY_LOGE("Process Tuning fail! pISP(%p), tuningBuf(%p)",
                in.mpISP, in.mTuningBuf.get());
        return MFALSE;
    }

    if( in.mIOPack.mIMGI.mBuffer == NULL )
    {
        MY_LOGE("Process Tuning fail! Not support both IMGO/RRZO exist or non-exist. imgi is NULL");
        return MFALSE;
    }

    if(metaParam.mHalIn == NULL || metaParam.mAppIn == NULL )
    {
        MY_LOGE("Can not get Input Metadata from metaParam! halI/O(%p/%p), appI/O/ExtraO(%p/%p/%p)",
                metaParam.mHalIn, metaParam.mHalOut, metaParam.mAppIn, metaParam.mAppOut, metaParam.mExtraAppOut);
        return MFALSE;
    }

    {
        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.r2y)::copyInMeta");
        NS3Av3::MetaSet_T inMetaSet;
        NS3Av3::MetaSet_T outMetaSet;

        inMetaSet.appMeta = *(metaParam.mAppIn); // copy
        inMetaSet.halMeta = *(metaParam.mHalIn); // copy
        P2_CAM_TRACE_END(TRACE_ADVANCED);

        updateInputMeta_Raw2Yuv(metaParam, inMetaSet);

        // ---  set Isp ---
        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.r2y)::makeFrameParam");
        NS3Av3::TuningParam tuningParam =
            P2Util::makeTuningParam(in.mP2Pack.mLog, in.mP2Pack, in.mpISP, inMetaSet, &outMetaSet,
                    in.mIOPack.isResized(), in.mTuningBuf->mpVA, in.mIOPack.mLCSO.mBuffer);
        frameParam = P2Util::makeFrameParams(in.mP2Pack, in.mTag, in.mIOPack, in.mP2ObjPtr, tuningParam);
        P2_CAM_TRACE_END(TRACE_ADVANCED);

        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.r2y)::copyOutMeta");
        if(metaParam.mAppOut != NULL)
            *(metaParam.mAppOut) = outMetaSet.appMeta; //copy
        if(metaParam.mExtraAppOut != NULL)
            *(metaParam.mExtraAppOut) = outMetaSet.appMeta; //copy
        if(metaParam.mHalOut != NULL)
        {
            *(metaParam.mHalOut) = inMetaSet.halMeta; //copy
            *(metaParam.mHalOut) += outMetaSet.halMeta; //copy
        }
        P2_CAM_TRACE_END(TRACE_ADVANCED);
        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.r2y)::updateOutputMeta");
        updateOutputMeta_Raw2Yuv(metaParam);
        P2_CAM_TRACE_END(TRACE_ADVANCED);
        P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.r2y)::freeMetaSet");
    }
    P2_CAM_TRACE_END(TRACE_ADVANCED);
    return MTRUE;
}

MBOOL
TuningHelper::
processSecure(
                      NSIoPipe::FrameParams&   frameParam,
               Feature::SecureBufferControl&   secBufCtrl)
{
    P2_CAM_TRACE_BEGIN(TRACE_ADVANCED, "TH(p2a.ps)::processSecure");
    frameParam.mSecureFra = MFALSE;
    NSIoPipe::EDIPSecureEnum secureEnum = secBufCtrl.getSecureEnum();
    if (secureEnum != NSIoPipe::EDIPSecureEnum::EDIPSecure_NONE)
    {
        frameParam.mSecureFra = MTRUE;
        for( auto &in : frameParam.mvIn )
        {
            in.mSecureTag = secureEnum;
            if( needSecureBuffer(in.mPortID) && in.mBuffer != NULL )
            {
                MVOID *secHandle = secBufCtrl.registerAndGetSecHandle(in.mBuffer);
                if ( secHandle != NULL )
                {
                    MY_LOGD_IF(secBufCtrl.mLogLevel, "Alloc secure buffer success, PortID(%u) secHandle(%p)", in.mPortID.index, secHandle);
                    in.mSecHandle = (MUINTPTR)secHandle;
                }
                else{
                    MY_LOGE("Alloc secure buffer fail, PortID(%u)", in.mPortID.index);
                    return MFALSE;
                }
            }
        }
        for( auto &out : frameParam.mvOut )
        {
            out.mSecureTag = secureEnum;
        }
    }

    P2_CAM_TRACE_END(TRACE_ADVANCED);
    return MTRUE;
}

} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
