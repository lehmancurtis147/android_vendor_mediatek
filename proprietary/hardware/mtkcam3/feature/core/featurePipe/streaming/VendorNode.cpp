/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include "VendorNode.h"
#include "MDPWrapper.h"

#define PIPE_CLASS_TAG "VendorNode"
#define PIPE_TRACE TRACE_VENDOR_NODE
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe {

VendorNode::VendorNode(const char *name)
    : StreamingFeatureNode(name)
    , mInputBufferPoolAllocateNeed(0)
    , mOutputBufferPoolAllocateNeed(0)
{
    TRACE_FUNC_ENTER();
    this->addWaitQueue(&mData);
    TRACE_FUNC_EXIT();
}

VendorNode::~VendorNode()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
}

MVOID VendorNode::setInputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mInputBufferPool = pool;
    mInputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MVOID VendorNode::setOutputBufferPool(const android::sp<IBufferPool> &pool, MUINT32 allocate)
{
    TRACE_FUNC_ENTER();
    mOutputBufferPool = pool;
    mOutputBufferPoolAllocateNeed = allocate;
    TRACE_FUNC_EXIT();
}

MBOOL VendorNode::onData(DataID id, const BasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;
    if( id == ID_P2A_TO_VENDOR_FULLIMG ||
        id == ID_BOKEH_TO_VENDOR_FULLIMG ||
        id == ID_FOV_WARP_TO_VENDOR ||
        id == ID_DUMMY_TO_NEXT_FULLIMG )
    {
        mData.enque(DualBasicImgData(data.mData, data.mRequest));
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL VendorNode::onData(DataID id, const DualBasicImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;
    if( id == ID_P2A_TO_VENDOR_FULLIMG ||
        id == ID_BOKEH_TO_VENDOR_FULLIMG )
    {
        mData.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL VendorNode::onData(DataID id, const DepthImgData &data)
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC("Frame %d: %s arrived", data.mRequest->mRequestNo, ID2Name(id));
    MBOOL ret = MFALSE;
    if( id == ID_DEPTH_TO_VENDOR )
    {
        mDepthDatas.enque(data);
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

IOPolicyType VendorNode::getIOPolicy(StreamType /*stream*/, const StreamingReqInfo &reqInfo) const
{
    IOPolicyType policy = IOPOLICY_BYPASS;

    if( HAS_VENDOR_V1(reqInfo.mFeatureMask) )
    {
        if( mPipeUsage.supportVendorFullImg() )
        {
            policy = IOPOLICY_INOUT_EXCLUSIVE;
        }
        else if( mPipeUsage.supportVendorInplace() )
        {
            policy = IOPOLICY_INPLACE;
        }
        else
        {
            policy = IOPOLICY_INOUT;
        }
    }

    return policy;
}

MBOOL VendorNode::getInputBufferPool(const StreamingReqInfo &/*reqInfo*/, android::sp<IBufferPool> &pool, MSize &resize)
{
    TRACE_FUNC_ENTER();
    MBOOL ret = MFALSE;
    resize = MSize(0,0);
    if( mInputBufferPool != NULL )
    {
        pool = mInputBufferPool;
        resize = mPipeUsage.getVendorCusSize(MSize(0,0));
        ret = MTRUE;
    }
    TRACE_FUNC_EXIT();
    return ret;
}

MBOOL VendorNode::onInit()
{
    TRACE_FUNC_ENTER();
    StreamingFeatureNode::onInit();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorNode::onUninit()
{
    TRACE_FUNC_ENTER();
    mInputBufferPool = NULL;
    mOutputBufferPool = NULL;
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorNode::onThreadStart()
{
    TRACE_FUNC_ENTER();
    if( mInputBufferPoolAllocateNeed && mInputBufferPool != NULL )
    {
        Timer timer(MTRUE);
        mInputBufferPool->allocate(mInputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGD("fpipe.vendor.in %s %d buf in %d ms", STR_ALLOCATE, mInputBufferPoolAllocateNeed, timer.getElapsed());
    }
    if( mOutputBufferPoolAllocateNeed && mOutputBufferPool != NULL )
    {
        Timer timer(MTRUE);
        mOutputBufferPool->allocate(mOutputBufferPoolAllocateNeed);
        timer.stop();
        MY_LOGD("fpipe.vendor.out %s %d buf in %d ms", STR_ALLOCATE, mOutputBufferPoolAllocateNeed, timer.getElapsed());
    }
    return MTRUE;
}

MBOOL VendorNode::onThreadStop()
{
    TRACE_FUNC_ENTER();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorNode::onThreadLoop()
{
    TRACE_FUNC("Waitloop");
    DualBasicImgData data;
    RequestPtr request;
    if( !waitAllQueue() )
    {
        return MFALSE;
    }
    if( !mData.deque(data) )
    {
        MY_LOGE("Request deque out of sync");
        return MFALSE;
    }
    if( data.mRequest == NULL )
    {
        MY_LOGE("Request out of sync");
        return MFALSE;
    }
    TRACE_FUNC_ENTER();
    request = data.mRequest;
    request->mTimer.startVendor();
    TRACE_FUNC("Frame %d in Vendor", request->mRequestNo);

    BasicImg out;
    MY_LOGD("sensor(%d) Frame %d process start needVendor %d", mSensorIndex, request->mRequestNo, request->needVendor());
    request->mTimer.startEnqueVendor();
    MBOOL result = MTRUE;
    if( request->needVendorV1() && request->hasGeneralOutput() )
    {
        BasicImg master = data.mData.mMaster;
        BasicImg slave = data.mData.mSlave;
        if( master.mBuffer != NULL )
        {
            if( mPipeUsage.supportVendorInplace() )
            {
                out = master;
            }
            else
            {
                // full image
                if( request->needFullImg(this, request->mMasterID) )
                {
                    out.mBuffer = mOutputBufferPool->requestIIBuffer();
                }
                else if( request->needNextFullImg(this, request->mMasterID) )
                {
                    MSize resize;
                    out.mBuffer = request->requestNextFullImg(this, request->mMasterID, resize);
                }
            }

            if(out.mBuffer != NULL)
            {
                out.mBuffer->getImageBuffer()->setExtParam(master.mBuffer->getImageBuffer()->getImgSize());
            }

            result = processVendor(request, master.mBuffer, slave.mBuffer,  out.mBuffer);

            if( result )
            {
                out.setDomainInfo(master);
            }
            else
            {
                MY_LOGW("sensor(%d) Frame %d processVendor fail", mSensorIndex, request->mRequestNo);
                out = master.mBuffer;
            }
        }
        else
        {
            MY_LOGW("sensor(%d) Frame %d mIn is NULL", mSensorIndex, request->mRequestNo);
        }
    }
    else
    {
        //return input as output
        out = data.mData.mMaster;
    }
    request->mTimer.stopEnqueVendor();
    request->updateResult(result);

    if( request->needDump() )
    {
        if( data.mData.mMaster.mBuffer != NULL )
        {
            data.mData.mMaster.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mData.mMaster.mBuffer->getImageBufferPtr(), "vendor.in");
        }
        if( data.mData.mSlave.mBuffer != NULL )
        {
            data.mData.mSlave.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, data.mData.mSlave.mBuffer->getImageBufferPtr(), "vendor.in2");
        }
        if( out.mBuffer!= NULL )
        {
            out.mBuffer->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
            dumpData(data.mRequest, out.mBuffer->getImageBufferPtr(), "vendor.out");
        }
    }

    MY_LOGD("sensor(%d) Frame %d process done in %d ms, result = %d", mSensorIndex, request->mRequestNo, request->mTimer.getElapsedEnqueVendor(), result);
    handleData(ID_VENDOR_TO_NEXT, BasicImgData(out, request));

    request->mTimer.stopVendor();
    TRACE_FUNC_EXIT();
    return MTRUE;
}

MBOOL VendorNode::processVendor(const RequestPtr &request, const ImgBuffer &master, const ImgBuffer &slave, const ImgBuffer &out)
{
    TRACE_FUNC_ENTER();
    MBOOL result = MTRUE;

    master->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
    if( slave != NULL )
    {
        slave->getImageBuffer()->syncCache(eCACHECTRL_INVALID);
    }

    if( mPipeUsage.supportVendorInplace() )
    {
        result = processVendorInplace(request, master, slave);
    }
    else
    {
        result = processVendorInOut(request, master, slave, out);
    }

    out->getImageBuffer()->syncCache(eCACHECTRL_FLUSH);

    TRACE_FUNC_EXIT();
    return result;
}

MBOOL VendorNode::processVendorInOut(const RequestPtr &request, const ImgBuffer &master, const ImgBuffer &slave, const ImgBuffer &out)
{
    TRACE_FUNC_ENTER();
    (void)slave;

    MBOOL result = MTRUE;

    // Add IN AND OUT vendor code here
    // and return process result
    result = copy(request, master, out);//this is example PLEASE replace it
    drawScanLine(out->getImageBufferPtr());

    TRACE_FUNC_EXIT();
    return  result;
}

MBOOL VendorNode::processVendorInplace(const RequestPtr &request, const ImgBuffer &img, const ImgBuffer &slave)
{
    TRACE_FUNC_ENTER();

    (void)(request);
    (void)(img);
    (void)(slave);

    MBOOL result = MTRUE;

    // Add INPLACE vendor code here
    // and return process result
    drawScanLine(img->getImageBufferPtr());

    TRACE_FUNC_EXIT();
    return  result;
}

MBOOL VendorNode::copy(const RequestPtr &request, const ImgBuffer &in, const ImgBuffer &out)
{
    TRACE_FUNC_ENTER();

    Timer timer(MTRUE);

    MDPWrapper mdp(PIPE_CLASS_TAG);

    MDPWrapper::Output output;
    output.mPortID = PortID(EPortType_Memory, NSImageio::NSIspio::EPortIndex_WDMAO, PORTID_OUT);
    output.mBuffer = out->getImageBufferPtr();

    MDPWrapper::MCropRect crop;
    crop.s = in->getImageBuffer()->getImgSize();
    crop.p_integral.x = 0;
    crop.p_integral.y = 0;

    MDPWrapper::OUTPUT_ARRAY outputs;
    outputs.push_back(MDPWrapper::MDPOutput(output, crop));

    MBOOL result = mdp.process(in->getImageBufferPtr(), outputs, request->needPrintIO());
    MY_LOGD("Frame %d MDP process result %d in %d ms", request->mRequestNo, result, timer.getNow());

    TRACE_FUNC_EXIT();
    return result;
}


} // namespace NSFeaturePipe
} // namespace NSCamFeature
} // namespace NSCam
