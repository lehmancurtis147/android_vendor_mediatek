/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
//
#include "../bokeh_common.h"
#include <mtkcam3/feature/stereo/pipe/vsdof_common.h>
#include <mtkcam3/feature/stereo/pipe/vsdof_data_define.h>
#include <mtkcam3/feature/stereo/hal/stereo_size_provider.h>
//
#define PIPE_MODULE_TAG "BokehPipe"
#define PIPE_CLASS_TAG "VendorFilterNode"
//
#include "VendorFilterNode.h"
//
#include <PipeLog.h>
#include <string>
//
#include <chrono>
//
#include <DpBlitStream.h>
#include <vsdof/util/vsdof_util.h>

#define FUNC_START  MY_LOGD("+")
#define FUNC_END    MY_LOGD("-")
using namespace std;
using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace VSDOF::util;

//************************************************************************
//
//************************************************************************
VendorFilterNode::
VendorFilterNode(
    const char *name,
    Graph_T *graph,
    MINT8 mode  __attribute__((UNUSED)))
    : BokehPipeNode(name, graph)
{
    MY_LOGD("ctor(0x%x)", this);
    this->addWaitQueue(&mRequests);
}
//************************************************************************
//
//************************************************************************
VendorFilterNode::
~VendorFilterNode()
{
    MY_LOGD("dctor(%p)", this);
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onInit()
{
    FUNC_START;
    MBOOL ret = MTRUE;
    BokehPipeNode::onInit();
    //
    mpDpStream = new DpBlitStream();
    ret = MTRUE;
    FUNC_END;
    return ret;
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onUninit()
{
    FUNC_START;
    cleanUp();
    FUNC_END;
    return MTRUE;
}
//************************************************************************
//
//************************************************************************
MVOID
VendorFilterNode::
cleanUp()
{
    FUNC_START;
    mRequests.clear();
    // dump all queue size
    MY_LOGD("mRequests.size(%d)", mRequests.size());
    // delete mdp instance
    if(mpDpStream!= nullptr)
    delete mpDpStream;
    FUNC_END;
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onThreadStart()
{
    return MTRUE;
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onThreadStop()
{
    return MTRUE;
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onData(
    DataID data,
    EffectRequestPtr &request)
{
    TRACE_FUNC_ENTER();
    VSDOF_PRFLOG("reqID=%d +", request->getRequestNo());
    MBOOL ret = MFALSE;
    //
    if(request->vInputFrameInfo.size() == 0)
    {
        MY_LOGE("vInputFrameInfo.size() is 0");
        return ret;
    }
    //
    if(request->vOutputFrameInfo.size() == 0)
    {
        MY_LOGE("vOutputFrameInfo.size() is 0");
        return ret;
    }
    //
    switch(data)
    {
        case ID_ROOT_ENQUE:
            mRequests.enque(request);
            ret = MTRUE;
            break;
        default:
            ret = MFALSE;
            break;
    }
    //
    VSDOF_PRFLOG("-");
    TRACE_FUNC_EXIT();
    return ret;
}
//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
onThreadLoop()
{
    EffectRequestPtr request;
    //
    if( !waitAllQueue() )
    {
        // mJobs.abort() called
        return MFALSE;
    }
    //
    if( !mRequests.deque(request) )
    {
        return MFALSE;
    }
    CAM_TRACE_NAME("VendorFilterNode::onThreadLoop");
    //
    VendorInputBuffer inputBuffers;
    VendorOutputBuffer outputBuffers;
    VendorInputParameter inputParameters;
    prepareVendorInOutData(request, &inputBuffers, &outputBuffers, &inputParameters);
    //
    if(!processVendor(inputParameters, inputBuffers, outputBuffers))
    {
        return MFALSE;
    }
    //
    handleData(VENDOR_OUT, request);
    //
    return MTRUE;
}

MBOOL
VendorFilterNode::
prepareVendorInOutData(
    const EffectRequestPtr& request,
    VendorInputBuffer* inputBuffers,
    VendorOutputBuffer* outputBuffers,
    VendorInputParameter* inputParameters
)
{
    MINT32 bufferCheck = 1;
    auto getImageBuffer = [&request, &bufferCheck](
        BokehEffectRequestBufferType type,
        VendorFilterNode::IN_OUT_TYPE in_out_type) -> IImageBuffer*
    {
        auto bufferTypeToString = [&type]()
        {
            if(BOKEH_ER_BUF_MAIN1 == type)
            {
                return "BOKEH_ER_BUF_MAIN1";
            }
            else if(BOKEH_ER_OUTPUT_DEPTHMAP == type)
            {
                return "BOKEH_ER_OUTPUT_DEPTHMAP";
            }
            else if(BOKEH_ER_BUF_DISPLAY == type)
            {
                return "BOKEH_ER_BUF_DISPLAY";
            }
            return "";
        };
        ssize_t keyIndex = -1;
        sp<EffectFrameInfo> frameInfo = nullptr;
        sp<IImageBuffer> frame = nullptr;
        if(VendorFilterNode::IN_OUT_TYPE::IN == in_out_type)
        {
            keyIndex = request->vInputFrameInfo.indexOfKey(type);
            if(keyIndex>=0)
            {
                frameInfo = request->vInputFrameInfo.valueAt(keyIndex);
                frameInfo->getFrameBuffer(frame);
            }
            else
            {
                bufferCheck &= 0;
                MY_LOGE("Get buffer fail. bufferType(%s)", bufferTypeToString());
                return nullptr;
            }
        }
        else
        {
            keyIndex = request->vOutputFrameInfo.indexOfKey(type);
            if(keyIndex>=0)
            {
                frameInfo = request->vOutputFrameInfo.valueAt(keyIndex);
                frameInfo->getFrameBuffer(frame);
            }
            else
            {
                bufferCheck &= 0;
                MY_LOGE("Get buffer fail. bufferType(%s)", bufferTypeToString());
                return nullptr;
            }
        }
        //
        if(frame.get() == nullptr)
        {
            bufferCheck &= 0;
            MY_LOGE("Buffer is invalid. bufferType(%s)", bufferTypeToString());
            return nullptr;
        }
        MY_LOGD("%s: %dx%d", bufferTypeToString(), frame->getImgSize().w, frame->getImgSize().h);
        bufferCheck &= 1;
        return frame.get();
    };
    inputBuffers->pInClearImage = getImageBuffer(BOKEH_ER_BUF_MAIN1, VendorFilterNode::IN_OUT_TYPE::IN);
    inputBuffers->pInDepthmap = getImageBuffer(BOKEH_ER_OUTPUT_DEPTHMAP, VendorFilterNode::IN_OUT_TYPE::IN);
    outputBuffers->pOutBokehImage = getImageBuffer(BOKEH_ER_BUF_DISPLAY, VendorFilterNode::IN_OUT_TYPE::OUT);

    // get EffectRequest parameter
    const sp<EffectParameter> pEffectParameter = request->getRequestParameter();
    inputParameters->bokehLevel = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_LEVEL);
    inputParameters->afState = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_AF_STATE);
    // get af roi
    MINT32 afTopLeftX     = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_AF_ROI_T_L_X);
    MINT32 afTopLeftY     = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_AF_ROI_T_L_Y);
    MINT32 afBottomRightX = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_AF_ROI_B_R_X);
    MINT32 afBottomRightY = pEffectParameter->getInt(VSDOF_FRAME_BOKEH_AF_ROI_B_R_Y);
    MY_LOGD_IF(miPipeLogEnable,"bokehLevel(%d), afState(%d), afTopLeftX(%d), afTopLeftY(%d), afBottomRightX(%d), afBottomRightY(%d)",
                inputParameters->bokehLevel, inputParameters->afState, afTopLeftX,
                afTopLeftY, afBottomRightX, afBottomRightY);
    return MTRUE;
}

//************************************************************************
//
//************************************************************************
MBOOL
VendorFilterNode::
processVendor(
    const VendorInputParameter& inputParameters,
    VendorInputBuffer inputBuffers,
    VendorOutputBuffer outputBuffers
)
{
    CAM_TRACE_BEGIN("VendorFilterNode::processVendor");
    MY_LOGD_IF(miPipeLogEnable, "+");

    MBOOL ret = MTRUE;
    // ---------------------------
    // Get input parameter example
    // ---------------------------
    // Get Bokeh level
    MINT32 bokehLevel = inputParameters.bokehLevel;
    // Get AF State
    MINT32 afState = inputParameters.afState;
    // Get AF ROI
    MRect afRoiTopLeft = inputParameters.afRoiTopLeft;
    MRect afRoiBottomRight = inputParameters.afRoiBottomRight;

    // --------------------------------
    // Add IN AND OUT vendor code here
    // and return process result
    // --------------------------------
    if(!copy(inputBuffers.pInClearImage, outputBuffers.pOutBokehImage)) //this is example PLEASE replace it
    {
        ret = MFALSE;
    }
    //
    MY_LOGD_IF(miPipeLogEnable, "-");
    CAM_TRACE_END();
    return ret;
}

MBOOL
VendorFilterNode::
copy(
    sp<IImageBuffer> source,
    sp<IImageBuffer> target
)
{
    if(source != nullptr)
    {
        if(mpDpStream == nullptr)
        {
            MY_LOGE("mpDpStream is null.");
            return MFALSE;
        }
        sMDP_Config config;
        config.pDpStream = mpDpStream;
        config.pSrcBuffer = source.get();
        config.pDstBuffer = target.get();
        if(!excuteMDP(config))
        {
            MY_LOGE("excuteMDP fail.");
            return MFALSE;
        }
    }
    return MTRUE;
}
