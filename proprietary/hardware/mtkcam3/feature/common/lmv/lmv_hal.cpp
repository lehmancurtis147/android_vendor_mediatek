/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
*      TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

/**
* @file lmv_hal.cpp
*
* LMV Hal Source File
*
*/

#include <cstdio>
#include <queue>
#include <cutils/atomic.h>
#include <utils/threads.h>
#include <utils/SystemClock.h>
#include <utils/Trace.h>
#include <utils/String8.h>
#include <cutils/properties.h>
#include <cutils/atomic.h>

#include "lmv_drv.h"

#include <android/sensor.h>
#include <mtkcam/utils/sys/SensorProvider.h>
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
#include <mtkcam/utils/imgbuf/IDummyImageBufferHeap.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/drv/iopipe/CamIO/IHalCamIO.h>
#include <mtkcam3/feature/eis/eis_type.h>
#include <camera_custom_nvram.h>
#include <camera_custom_eis.h>

using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSCam::NSIoPipe;
using namespace NSCam::NSIoPipe::NSCamIOPipe;
using namespace NSCam::Utils;

#include "lmv_hal_imp.h"

#if 1
#undef  ATRACE_TAG
#define ATRACE_TAG ATRACE_TAG_ALWAYS
#define DP_TRACE_CALL()                 ATRACE_CALL()
#define DP_TRACE_BEGIN(name)            ATRACE_BEGIN(name)
#define DP_TRACE_END()                  ATRACE_END()

#else

#define DP_TRACE_CALL()
#define DP_TRACE_BEGIN(name)
#define DP_TRACE_END()

#endif  // CONFIG_FOR_SYSTRACE


/*******************************************************************************
*
********************************************************************************/
#define LMV_HAL_DEBUG

#ifdef LMV_HAL_DEBUG

#undef __func__
#define __func__ __FUNCTION__

#undef  LOG_TAG
#define LOG_TAG "LMVHal"
#include <mtkcam/utils/std/Log.h>

#define LMV_LOG(fmt, arg...)    CAM_LOGD("[%s]" fmt, __func__, ##arg)
#define LMV_INF(fmt, arg...)    CAM_LOGI("[%s]" fmt, __func__, ##arg)
#define LMV_WRN(fmt, arg...)    CAM_LOGW("[%s] WRN(%5d):" fmt, __func__, __LINE__, ##arg)
#define LMV_ERR(fmt, arg...)    CAM_LOGE("[%s] %s ERROR(%5d):" fmt, __func__,__FILE__, __LINE__, ##arg)
#define LMV_ENTER()             LMV_LOG("+")
#define LMV_EXIT()              LMV_LOG("-")

#else
#define LMV_LOG(a,...)
#define LMV_INF(a,...)
#define LMV_WRN(a,...)
#define LMV_ERR(a,...)
#define LMV_ENTER()             LMV_LOG("+")
#define LMV_EXIT()              LMV_LOG("-")

#endif

#define LMV_HAL_NAME            "LMVHal"
#define LMV_HAL_DUMP            "vendor.debug.lmv.dump"
#define LMV_HAL_GYRO_INTERVAL   "vendor.debug.lmv.setinterval"

#define LMV_DUMP_PATH           "/data/vendor/dump"

#define intPartShift            (8)
#define floatPartShift          (31 - intPartShift)
#define DEBUG_DUMP_FRAMW_NUM    (10)

template <> LMVHalObj<0>* LMVHalObj<0>::spInstance = 0;
template <> LMVHalObj<1>* LMVHalObj<1>::spInstance = 0;
template <> LMVHalObj<2>* LMVHalObj<2>::spInstance = 0;
template <> LMVHalObj<3>* LMVHalObj<3>::spInstance = 0;

template <> Mutex LMVHalObj<0>::s_instMutex(::Mutex::PRIVATE);
template <> Mutex LMVHalObj<1>::s_instMutex(::Mutex::PRIVATE);
template <> Mutex LMVHalObj<2>::s_instMutex(::Mutex::PRIVATE);
template <> Mutex LMVHalObj<3>::s_instMutex(::Mutex::PRIVATE);


const MUINT32 GyroInterval_ms = 20;

MINT32 LMVHalImp::mDebugDump = 0;


#define LMVO_BUFFER_NUM (30)

LMVHal *LMVHal::CreateInstance(char const *userName, const MUINT32 &aSensorIdx)
{
    LMV_LOG("user(%s)", userName);
    return LMVHalImp::GetInstance(aSensorIdx);
}

LMVHal *LMVHalImp::GetInstance(const MUINT32 &aSensorIdx)
{
    LMV_LOG("sensorIdx(%u)", aSensorIdx);

    switch( aSensorIdx )
    {
        case 0 : return LMVHalObj<0>::GetInstance();
        case 1 : return LMVHalObj<1>::GetInstance();
        case 2 : return LMVHalObj<2>::GetInstance();
        case 3 : return LMVHalObj<3>::GetInstance();
        default :
            LMV_WRN("Current limit is 4 sensors, use 0");
            return LMVHalObj<0>::GetInstance();
    }
}

MVOID LMVHalImp::DestroyInstance(char const *userName)
{
    LMV_LOG("user(%s)", userName);
}

LMVHalImp::LMVHalImp(const MUINT32 &aSensorIdx)
    : LMVHal()
    , mSensorIdx(aSensorIdx)
{
    memset(&mSensorStaticInfo, 0, sizeof(mSensorStaticInfo));
    memset(&mSensorDynamicInfo, 0, sizeof(mSensorDynamicInfo));
    memset(&mLmvAlgoProcData, 0, sizeof(mLmvAlgoProcData));

    while( !mLMVOBufferList.empty() )
    {
        mLMVOBufferList.pop();
    }
}

MINT32 LMVHalImp::Init(const MUINT32 eisFactor)
{
    DP_TRACE_CALL();

    Mutex::Autolock lock(mLock);

    if( mUsers > 0 )
    {
        android_atomic_inc(&mUsers);
        LMV_LOG("sensorIdx(%u) has %d users", mSensorIdx, mUsers);
        return LMV_RETURN_NO_ERROR;
    }
    LMV_LOG("(%p) mSensorIdx(%u) init", this, mSensorIdx);

    mDebugDump = ::property_get_int32(LMV_HAL_DUMP, mDebugDump);
    mEisPlusCropRatio = eisFactor > 100 ? eisFactor : EISCustom::getEIS12Factor();

    m_pHalSensorList = MAKE_HalSensorList();
    if( m_pHalSensorList == NULL )
    {
        LMV_ERR("IHalSensorList::get fail");
        goto create_fail_exit;
    }
    if( GetSensorInfo() != LMV_RETURN_NO_ERROR )
    {
        LMV_ERR("GetSensorInfo fail");
        goto create_fail_exit;
    }

    m_pLMVDrv = LMVDrv::CreateInstance(mSensorIdx);
    if( m_pLMVDrv == NULL )
    {
        LMV_ERR("LMVDrv::createInstance fail");
        goto create_fail_exit;
    }
    if( m_pLMVDrv->Init() != LMV_RETURN_NO_ERROR )
    {
        LMV_ERR("LMVDrv::Init fail");
        goto create_fail_exit;
    }

    LMV_LOG("TG(%d), mEisPlusCropRatio(%u)", mSensorDynamicInfo.TgInfo, mEisPlusCropRatio);

    DP_TRACE_BEGIN("CreateMultiMemBuf");
    CreateMultiMemBuf(LMVO_MEMORY_SIZE, (LMVO_BUFFER_NUM+1), m_pLMVOMainBuffer, m_pLMVOSliceBuffer);
    if( !m_pLMVOSliceBuffer[0]->getBufVA(0) )
    {
        LMV_ERR("LMVO slice buf create ImageBuffer fail!");
        LMV_EXIT();
        return LMV_RETURN_MEMORY_ERROR;
    }
    {
        Mutex::Autolock lock(mLMVOBufferListLock);
        for( int index = 0; index < LMVO_BUFFER_NUM; index++ )
        {
            mLMVOBufferList.push(m_pLMVOSliceBuffer[index]);
        }
    }
    DP_TRACE_END();

    android_atomic_inc(&mUsers);

    if( UNLIKELY(mDebugDump >= 3) )
    {
        mDumpFile = fopen(LMV_DUMP_PATH"/lmv.log", "w+");

        if( mDumpFile == NULL )
        {
            LMV_WRN("Open file fail! (errno=%d)", errno);
        }
    }

    LMV_EXIT();
    return LMV_RETURN_NO_ERROR;

create_fail_exit:

    if( m_pLMVDrv != NULL )
    {
        m_pLMVDrv->Uninit();
        m_pLMVDrv->DestroyInstance();
        m_pLMVDrv = NULL;
    }

    if( m_pHalSensorList != NULL )
    {
        m_pHalSensorList = NULL;
    }
    LMV_EXIT();
    return LMV_RETURN_NULL_OBJ;
}

MINT32 LMVHalImp::Uninit()
{
    Mutex::Autolock lock(mLock);

    if( mUsers <= 0 )
    {
        LMV_LOG("mSensorIdx(%u) has 0 user", mSensorIdx);
        return LMV_RETURN_NO_ERROR;
    }

    android_atomic_dec(&mUsers);

    if( mUsers == 0 )
    {
        LMV_LOG("mSensorIdx(%u) uninit, TG(%d)", mSensorIdx, mSensorDynamicInfo.TgInfo);

        if( m_pLMVDrv != NULL )
        {
            LMV_LOG("m_pLMVDrv uninit");

            m_pLMVDrv->Uninit();
            m_pLMVDrv->DestroyInstance();
            m_pLMVDrv = NULL;
        }

        if( UNLIKELY(mDebugDump >= 2) )
        {
            if( mSensorDynamicInfo.TgInfo != CAM_TG_NONE )
            {
                MINT32 err = m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_SAVE_LOG, NULL, NULL);
                if( err != S_EIS_OK )
                {
                    LMV_ERR("EisFeatureCtrl(EIS_FEATURE_SAVE_LOG) fail(0x%x)", err);
                }
            }
        }

        if( m_pEisAlg != NULL )
        {
            LMV_LOG("m_pEisAlg uninit");
            m_pEisAlg->EisReset();
            m_pEisAlg->destroyInstance();
            m_pEisAlg = NULL;
        }
        if( mpSensorProvider != NULL )
        {
            mpSensorProvider->disableSensor(SENSOR_TYPE_GYRO);
            mpSensorProvider = NULL;
        }
        if( m_pHalSensorList != NULL )
        {
            m_pHalSensorList = NULL;
        }

        if( UNLIKELY(mDebugDump >= 2) )
        {
            m_pLmvDbgBuf->unlockBuf("LMVDbgBuf");
            DestroyMemBuf(m_pLmvDbgBuf);
        }
        DestroyMultiMemBuf((LMVO_BUFFER_NUM+1), m_pLMVOMainBuffer, m_pLMVOSliceBuffer);

        mLmvInput_W = 0;
        mLmvInput_H = 0;
        mP1Target_W = 0;
        mP1Target_H = 0;
        mFrameCnt = 0;
        mEisPass1Enabled = 0;
        mIsLmvConfig = 0;
        mCmvX_Int = 0;
        mDoLmvCount = 0;
        mCmvX_Flt = 0;
        mCmvY_Int = 0;
        mMVtoCenterX = 0;
        mMVtoCenterY = 0;
        mCmvY_Flt = 0;
        mGMV_X = 0;
        mGMV_Y = 0;
        mMAX_GMV = LMV_MAX_GMV_DEFAULT;
        mVideoW = 0;
        mVideoH = 0;
        mMemAlignment = 0;
        mBufIndex = 0;
        mDebugDump = 0;

        {
            Mutex::Autolock lock(mLMVOBufferListLock);
            while( !mLMVOBufferList.empty() )
            {
                mLMVOBufferList.pop();
            }
        }

        if( mDumpFile != NULL )
        {
            fflush(mDumpFile);
            fclose(mDumpFile);
            LMV_LOG("Close file successfully");
            mDumpFile = NULL;
        }

    }
    else
    {
        LMV_LOG("mSensorIdx(%u) has %d users", mSensorIdx, mUsers);
    }
    return LMV_RETURN_NO_ERROR;
}

MINT32 LMVHalImp::CreateMultiMemBuf(MUINT32 memSize, MUINT32 num, android::sp<IImageBuffer>& spMainImageBuf, android::sp<IImageBuffer> spImageBuf[MAX_LMV_MEMORY_SIZE])
{
    MINT32 err = LMV_RETURN_NO_ERROR;
    MUINT32 totalSize = memSize*num;

    if( num >= MAX_LMV_MEMORY_SIZE )
    {
        LMV_ERR("num of image buffer is larger than MAX_LMV_MEMORY_SIZE(%d)", MAX_LMV_MEMORY_SIZE);
        return LMV_RETURN_MEMORY_ERROR;
    }

    IImageBufferAllocator::ImgParam imgParam(totalSize, 0);

    sp<IIonImageBufferHeap> pHeap = IIonImageBufferHeap::create(LMV_HAL_NAME, imgParam);
    if( pHeap == NULL )
    {
        LMV_ERR("image buffer heap create fail");
        return LMV_RETURN_MEMORY_ERROR;
    }

    MUINT const usage = (GRALLOC_USAGE_SW_READ_OFTEN |
                        GRALLOC_USAGE_SW_WRITE_OFTEN | // ISP3 is software-write
                        GRALLOC_USAGE_HW_CAMERA_READ |
                        GRALLOC_USAGE_HW_CAMERA_WRITE);
    spMainImageBuf = pHeap->createImageBuffer();
    if( spMainImageBuf == NULL )
    {
        LMV_ERR("mainImage buffer create fail");
        return LMV_RETURN_MEMORY_ERROR;
    }
    if( !(spMainImageBuf->lockBuf(LMV_HAL_NAME, usage)) )
    {
        LMV_ERR("image buffer lock fail");
        return LMV_RETURN_MEMORY_ERROR;
    }
    MUINTPTR const iVAddr = pHeap->getBufVA(0);
    MUINTPTR const iPAddr = pHeap->getBufPA(0);
    MINT32 const iHeapId = pHeap->getHeapID();

    LMV_LOG("IIonImageBufferHeap iVAddr:%p, iPAddr:%p, iHeapId:%d. spMainImageBuf iVAddr:%p, iPAddr:%p",
            (void*)iVAddr, (void*)iPAddr, iHeapId,
            (void*)spMainImageBuf->getBufVA(0), (void*)spMainImageBuf->getBufPA(0));

    for( MUINT32 index = 0; index < num; index++)
    {
        MUINTPTR const cVAddr = iVAddr + ((index)*(memSize));
        MUINTPTR const virtAddr[] = {cVAddr, 0, 0};
        MUINTPTR const cPAddr = iPAddr + ((index)*(memSize));
        MUINTPTR const phyAddr[] = {cPAddr, 0, 0};
        IImageBufferAllocator::ImgParam imgParam_t = IImageBufferAllocator::ImgParam(memSize, 0);
        PortBufInfo_dummy portBufInfo = PortBufInfo_dummy(iHeapId, virtAddr, phyAddr, 1);
        sp<IImageBufferHeap> imgBufHeap = IDummyImageBufferHeap::create(LMV_HAL_NAME, imgParam_t, portBufInfo, false);
        if( imgBufHeap == NULL )
        {
            LMV_ERR("acquire LMV_HAL - image buffer heap create fail");
            return LMV_RETURN_MEMORY_ERROR;
        }
        sp<IImageBuffer> imgBuf = imgBufHeap->createImageBuffer();
        if( imgBuf == NULL )
        {
            LMV_ERR("acquire LMV_HAL - image buffer create fail");
            return LMV_RETURN_MEMORY_ERROR;
        }
        if( !(imgBuf->lockBuf(LMV_HAL_NAME, usage)) )
        {
            LMV_ERR("acquire LMV_HAL - image buffer lock fail");
            return LMV_RETURN_MEMORY_ERROR;
        }

        spImageBuf[index] = imgBuf;
    }
    LMV_EXIT();
    return err;
}

MINT32 LMVHalImp::CreateMemBuf(MUINT32 memSize, android::sp<IImageBuffer>& spImageBuf)
{
    MINT32 err = LMV_RETURN_NO_ERROR;
    IImageBufferAllocator* pImageBufferAlloc = IImageBufferAllocator::getInstance();

    LMV_LOG("Size(%u)", memSize);
    IImageBufferAllocator::ImgParam bufParam((size_t)memSize, 0);
    spImageBuf = pImageBufferAlloc->alloc(LMV_HAL_NAME, bufParam);
    LMV_EXIT();
    return err;
}

MINT32 LMVHalImp::DestroyMultiMemBuf(MUINT32 num, android::sp<IImageBuffer>& spMainImageBuf, android::sp<IImageBuffer> spImageBuf[MAX_LMV_MEMORY_SIZE])
{
    MINT32 err = LMV_RETURN_NO_ERROR;
    for( MUINT32 index = 0; index < num; index++)
    {
        spImageBuf[index]->unlockBuf(LMV_HAL_NAME);
        spImageBuf[index] = NULL;
    }

    spMainImageBuf->unlockBuf(LMV_HAL_NAME);
    spMainImageBuf = NULL;

    LMV_EXIT();
    return err;
}

MINT32 LMVHalImp::DestroyMemBuf(android::sp<IImageBuffer>& spImageBuf)
{
    MINT32 err = LMV_RETURN_NO_ERROR;
    IImageBufferAllocator* pImageBufferAlloc = IImageBufferAllocator::getInstance();
    LMV_ENTER();

    if( spImageBuf != NULL )
    {
        pImageBufferAlloc->free(spImageBuf.get());
        spImageBuf = NULL;
    }

    LMV_EXIT();
    return err;
}

MINT32 LMVHalImp::GetSensorInfo()
{
    LMV_LOG("mSensorIdx(%u)", mSensorIdx);

    mSensorDev = m_pHalSensorList->querySensorDevIdx(mSensorIdx);
    m_pHalSensorList->querySensorStaticInfo(mSensorDev, &mSensorStaticInfo);
    m_pHalSensor = m_pHalSensorList->createSensor(LMV_HAL_NAME, 1, &mSensorIdx);
    if( m_pHalSensor == NULL )
    {
        LMV_ERR("m_pHalSensorList->createSensor fail");
        return LMV_RETURN_API_FAIL;
    }
    if( m_pHalSensor->querySensorDynamicInfo(mSensorDev, &mSensorDynamicInfo) == MFALSE )
    {
        LMV_ERR("querySensorDynamicInfo fail");
        return LMV_RETURN_API_FAIL;
    }

    m_pHalSensor->destroyInstance(LMV_HAL_NAME);
    m_pHalSensor = NULL;

    return LMV_RETURN_NO_ERROR;
}

MVOID LMVHalImp::EnableGyroSensor()
{
    DP_TRACE_BEGIN("SensorProvider");
    if( mpSensorProvider == NULL )
    {
        mpSensorProvider = SensorProvider::createInstance(LMV_HAL_NAME);
        MUINT32 interval = ::property_get_int32(LMV_HAL_GYRO_INTERVAL, GyroInterval_ms);

        if( mpSensorProvider->enableSensor(SENSOR_TYPE_GYRO, interval) )
        {
            LMV_LOG("Enable SensorProvider success");
        }
        else
        {
            LMV_ERR("Enable SensorProvider fail");
        }
    }

    DP_TRACE_END();
}

MINT32 LMVHalImp::ConfigLMV(const LMV_HAL_CONFIG_DATA &aLmvConfig)
{
    if( mLmvSupport == MFALSE )
    {
        LMV_LOG("mSensorIdx(%u) not support LMV", mSensorIdx);
        return LMV_RETURN_NO_ERROR;
    }

    MINT32 err = LMV_RETURN_NO_ERROR;

    static EIS_SET_ENV_INFO_STRUCT eisAlgoInitData;

    LMV_SENSOR_ENUM sensorType;
    switch( aLmvConfig.sensorType )
    {
        case NSCam::NSSensorType::eRAW:
            sensorType = LMV_RAW_SENSOR;
            break;
        case NSCam::NSSensorType::eYUV:
            sensorType = LMV_YUV_SENSOR;
            break;
        default:
            LMV_ERR("not support sensor type(%u), use RAW setting", aLmvConfig.sensorType);
            sensorType = LMV_RAW_SENSOR;
            break;
    }

    if( UNLIKELY(mDebugDump >= 1) )
    {
        LMV_LOG("mIsLmvConfig(%u)", mIsLmvConfig);
    }

    if( mIsLmvConfig == 0 )
    {
        GetEisCustomize(&eisAlgoInitData.eis_tuning_data);
        eisAlgoInitData.Eis_Input_Path = EIS_PATH_RAW_DOMAIN;   // RAW domain

        if( mSensorDynamicInfo.TgInfo == CAM_TG_NONE )
        {
            // Reget sensor information
            if( GetSensorInfo() != LMV_RETURN_NO_ERROR )
            {
                LMV_ERR("GetSensorInfo fail");
            }
            LMV_LOG("TG(%d)",mSensorDynamicInfo.TgInfo);
        }

        if( m_pEisAlg == NULL )
        {
            m_pEisAlg = MTKEis::createInstance();
            if( m_pEisAlg == NULL )
            {
                LMV_ERR("MTKEis::createInstance fail");
                return LMV_RETURN_UNKNOWN_ERROR;
            }
        }
        if( m_pEisAlg->EisInit(&eisAlgoInitData) != S_EIS_OK )
        {
            LMV_ERR("EisInit fail(0x%x)",err);
            return LMV_RETURN_API_FAIL;
        }

        if( UNLIKELY(mDebugDump >= 2) )
        {
            MUINT32 lmvMemSize = 0;
            EIS_SET_LOG_BUFFER_STRUCT eisAlgoLogInfo;

            if( m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_GET_DEBUG_INFO, NULL, &lmvMemSize) != S_EIS_OK ||
                lmvMemSize == 0 )
            {
                LMV_ERR("EisFeatureCtrl(EIS_FEATURE_GET_DEBUG_INFO) fail(0x%x)",err);
                eisAlgoLogInfo.Eis_Log_Buf_Addr = NULL;
                eisAlgoLogInfo.Eis_Log_Buf_Size = 0;
            }
            else
            {
                CreateMemBuf(lmvMemSize, m_pLmvDbgBuf);
                m_pLmvDbgBuf->lockBuf("LMVDbgBuf", eBUFFER_USAGE_SW_MASK);
                if( !m_pLmvDbgBuf->getBufVA(0) )
                {
                    LMV_ERR("mLmvDbgBuf create ImageBuffer fail");
                    return LMV_RETURN_MEMORY_ERROR;
                }

                LMV_LOG("mLmvDbgBuf : size(%u),virAdd(0x%p)", lmvMemSize, (void*)m_pLmvDbgBuf->getBufVA(0));
                eisAlgoLogInfo.Eis_Log_Buf_Addr = (MVOID *)m_pLmvDbgBuf->getBufVA(0);
                eisAlgoLogInfo.Eis_Log_Buf_Size = lmvMemSize;
            }
            if( m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_SET_DEBUG_INFO, &eisAlgoLogInfo, NULL) != S_EIS_OK )
            {
                LMV_ERR("EisFeatureCtrl(EIS_FEATURE_SET_DEBUG_INFO) fail(0x%x)",err);
            }
        }

        mTsForAlgoDebug = 0;

        if( UNLIKELY(mDebugDump >= 1) )
        {
            String8 str;
            str += String8::format("EIS tuning_data:sensitivity(%d),filter_small_motion(%u),adv_shake_ext(%u),"
                                   "stabilization_strength(%f),new_tru_th(%u),vot_th(%u),votb_enlarge_size(%u),"
                                   "min_s_th(%u),vec_th(%u),spr_offset(%u),spr_gain1(%u),spr_gain2(%u),"
                                   "vot_his_method(%d),smooth_his_step(%u),eis_debug(%u)",
                                    eisAlgoInitData.eis_tuning_data.sensitivity,
                                    eisAlgoInitData.eis_tuning_data.filter_small_motion,
                                    eisAlgoInitData.eis_tuning_data.adv_shake_ext,
                                    eisAlgoInitData.eis_tuning_data.stabilization_strength,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.new_tru_th,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.vot_th,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.votb_enlarge_size,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.min_s_th,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.vec_th,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.spr_offset,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.spr_gain1,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.spr_gain2,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.vot_his_method,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.smooth_his_step,
                                    eisAlgoInitData.eis_tuning_data.advtuning_data.eis_debug );
            String8 str_gmv_pan(",gmv_pan");
            String8 str_gmv_sm(",gmv_sm");
            String8 str_cmv_pan(",cmv_pan");
            String8 str_cmv_sm(",cmv_sm");
            const int MV_ARRAY_NUM = 4;
            for( int i = 0; i < MV_ARRAY_NUM; ++i)
            {
                str_gmv_pan += String8::format("(%u)", eisAlgoInitData.eis_tuning_data.advtuning_data.gmv_pan_array[i]);
                str_gmv_sm  += String8::format("(%u)", eisAlgoInitData.eis_tuning_data.advtuning_data.gmv_sm_array[i]);
                str_cmv_pan += String8::format("(%u)", eisAlgoInitData.eis_tuning_data.advtuning_data.cmv_pan_array[i]);
                str_cmv_sm  += String8::format("(%u)", eisAlgoInitData.eis_tuning_data.advtuning_data.cmv_sm_array[i]);
            }
            str += str_gmv_pan + str_gmv_sm + str_cmv_pan + str_cmv_sm;
            LMV_LOG("%s", str.string());
        }

        if( m_pLMVDrv->ConfigLMVReg(mSensorDynamicInfo.TgInfo) != LMV_RETURN_NO_ERROR )
        {
            LMV_ERR("ConfigLMVReg fail(0x%x)", err);
            return LMV_RETURN_API_FAIL;
        }

        mIsLmvConfig = 1;
        {
            Mutex::Autolock lock(mLock);
            mEisPass1Enabled = 1;
        }
    }
    return LMV_RETURN_NO_ERROR;
}

MINT32 LMVHalImp::DoLMVCalc(QBufInfo const &pBufInfo)
{
    MINT32 err = LMV_RETURN_NO_ERROR;
    const MUINT64 aTimeStamp = pBufInfo.mvOut[0].mMetaData.mTimeStamp; //Maybe framedone

    if( mLmvSupport == MFALSE )
    {
        LMV_LOG("mSensorIdx(%u) not support LMV", mSensorIdx);
        return LMV_RETURN_EISO_MISS;
    }

    if( UNLIKELY(mTsForAlgoDebug == 0) )
    {
        mTsForAlgoDebug = aTimeStamp;
    }

    if( UNLIKELY(mDebugDump >= 1) )
    {
        LMV_LOG("mSensorIdx=%u,mEisPass1Enabled(%u)", mSensorIdx, mEisPass1Enabled);
    }

    if( aTimeStamp <= 0 )
    {
        LMV_LOG("DoP1Eis aTimeStamp is not reasonable(%" PRIi64 ")",aTimeStamp);
    }
    else
    {
        EIS_RESULT_INFO_STRUCT eisCMVResult;
        MINTPTR lmvoBufferVA = 0;
        for( size_t i = 0; i < pBufInfo.mvOut.size(); i++ )
        {
            if( pBufInfo.mvOut[i].mPortID.index == PORT_RRZO.index )
            {
                //crop region
                mP1ResizeIn_W = mLmvInput_W = pBufInfo.mvOut.at(i).mMetaData.mDstSize.w;
                mP1ResizeIn_H = mLmvInput_H = pBufInfo.mvOut.at(i).mMetaData.mDstSize.h;
            }
            if( pBufInfo.mvOut[i].mPortID.index == PORT_EISO.index )
            {
                lmvoBufferVA = pBufInfo.mvOut.at(i).mBuffer->getBufVA(0);
            }
        }

        {
            Mutex::Autolock lock(mP1Lock);

            if( m_pLMVDrv->Get2PixelMode() == 1 )
            {
                mLmvInput_W >>= 1;
            }
            else if( m_pLMVDrv->Get2PixelMode() == 2 )
            {
                mLmvInput_W >>= 2;
            }

            mLmvInput_W -= 4;   //ryan wang request to -4
            mLmvInput_H -= 4;   //ryan wang request to -4

            mP1Target_W = (mLmvInput_W / (mEisPlusCropRatio / 100.0));
            mP1Target_H = (mLmvInput_H / (mEisPlusCropRatio / 100.0));

            mP1ResizeOut_W = (mP1ResizeIn_W / (mEisPlusCropRatio / 100.0));
            mP1ResizeOut_H = (mP1ResizeIn_H / (mEisPlusCropRatio / 100.0));

            mLmvAlgoProcData.eis_image_size_config.InputWidth   = mLmvInput_W;
            mLmvAlgoProcData.eis_image_size_config.InputHeight  = mLmvInput_H;
            mLmvAlgoProcData.eis_image_size_config.TargetWidth  = mP1Target_W;
            mLmvAlgoProcData.eis_image_size_config.TargetHeight = mP1Target_H;
        }

        //> get LMV HW statistic
        if( m_pLMVDrv->GetLmvHwStatistic(lmvoBufferVA, &mLmvAlgoProcData.eis_state) == LMV_RETURN_EISO_MISS )
        {
            LMV_WRN("EISO data miss");
            return LMV_RETURN_NO_ERROR;
        }

        mLmvAlgoProcData.DivH      = m_pLMVDrv->GetLMVDivH();
        mLmvAlgoProcData.DivV      = m_pLMVDrv->GetLMVDivV();
        mLmvAlgoProcData.EisWinNum = m_pLMVDrv->GetLMVMbNum();

        // get gyro/acceleration data
        SensorData gyroData;
        SensorData accData;
        MBOOL gyroValid = mpSensorProvider->getLatestSensorData(SENSOR_TYPE_GYRO, gyroData);
        //MBOOL accValid = mpSensorProvider->getLatestSensorData(SENSOR_TYPE_ACCELERATION, accData);
        MBOOL accValid = false;

        {
            Mutex::Autolock lock(mP1Lock);

            mLmvAlgoProcData.sensor_info.GyroValid = gyroValid;
            mLmvAlgoProcData.sensor_info.Gvalid = accValid;

            if( gyroValid )
            {
                mLmvAlgoProcData.sensor_info.GyroInfo[0] = gyroData.gyro[0];
                mLmvAlgoProcData.sensor_info.GyroInfo[1] = gyroData.gyro[1];
                mLmvAlgoProcData.sensor_info.GyroInfo[2] = gyroData.gyro[2];
            }
        }

        if( UNLIKELY(mDebugDump >= 1) )
        {
            LMV_LOG("EN:(Acc,Gyro)=(%d,%d)/Acc(%f,%f,%f)/Gyro(%f,%f,%f)", accValid, gyroValid,
                    mLmvAlgoProcData.sensor_info.AcceInfo[0], mLmvAlgoProcData.sensor_info.AcceInfo[1], mLmvAlgoProcData.sensor_info.AcceInfo[2],
                    mLmvAlgoProcData.sensor_info.GyroInfo[0],mLmvAlgoProcData.sensor_info.GyroInfo[1],mLmvAlgoProcData.sensor_info.GyroInfo[2]);
        }

        if( m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_SET_PROC_INFO, &mLmvAlgoProcData, NULL) != S_EIS_OK )
        {
            LMV_ERR("EisAlg:LMV_FEATURE_SET_PROC_INFO fail(0x%x)",err);

            return LMV_RETURN_API_FAIL;
        }
        if( m_pEisAlg->EisMain(&eisCMVResult) != S_EIS_OK )
        {
            LMV_ERR("EisAlg:EisMain fail(0x%x), mSensorIdx=%u",err, mSensorIdx);
            return LMV_RETURN_API_FAIL;
        }

        EIS_GET_PLUS_INFO_STRUCT eisData2EisPlus;
        if( m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_GET_EIS_PLUS_DATA, NULL, &eisData2EisPlus) != S_EIS_OK )
        {
            LMV_ERR("EisAlg:LMV_FEATURE_GET_LMV_PLUS_DATA fail(0x%x)",err);
            return LMV_RETURN_API_FAIL;
        }

        {
            Mutex::Autolock lock(mP2Lock);

            if( m_pLMVDrv->Get2PixelMode() == 1 )
            {
                if( mDebugDump > 0 )
                {
                    LMV_LOG("eisData2EisPlus.GMVx *= 2");
                }
                eisData2EisPlus.GMVx *= 2.0f;
            }
            else if( m_pLMVDrv->Get2PixelMode() == 2 )
            {
                if( mDebugDump > 0 )
                {
                    LMV_LOG("eisData2EisPlus.GMVx *= 4");
                }
                eisData2EisPlus.GMVx *= 4.0f;
            }

            mLmvLastData2EisPlus.GMVx  = eisData2EisPlus.GMVx;
            mLmvLastData2EisPlus.GMVy  = eisData2EisPlus.GMVy;
            mLmvLastData2EisPlus.ConfX = eisData2EisPlus.ConfX;
            mLmvLastData2EisPlus.ConfY = eisData2EisPlus.ConfY;
        }

        EIS_GMV_INFO_STRUCT lmvGMVResult;

        if( m_pEisAlg->EisFeatureCtrl(EIS_FEATURE_GET_ORI_GMV, NULL, &lmvGMVResult) != S_EIS_OK )
        {
            LMV_ERR("EisAlg:LMV_FEATURE_GET_ORI_GMV fail(0x%x)", err);
            return LMV_RETURN_API_FAIL;
        }

        if( m_pLMVDrv->Get2PixelMode() == 1 )
        {
            if( mDebugDump > 0 )
            {
                LMV_LOG("eisGMVResult.LMV_GMVx *= 2");
            }

            lmvGMVResult.EIS_GMVx *= 2.0f;
        }
        else if( m_pLMVDrv->Get2PixelMode() == 2 )
        {
            if( mDebugDump > 0 )
            {
                LMV_LOG("eisGMVResult.LMV_GMVx *= 4");
            }

            lmvGMVResult.EIS_GMVx *= 4.0f;
        }

        mGMV_X = lmvGMVResult.EIS_GMVx;
        mGMV_Y = lmvGMVResult.EIS_GMVy;
        mMAX_GMV = m_pLMVDrv->GetLMVMaxGmv();

        PrepareLmvResult(eisCMVResult.CMV_X,eisCMVResult.CMV_Y);

        mFrameCnt = m_pLMVDrv->GetFirstFrameInfo();

        LMV_LOG("Crop(%u),Sensor(%u),LMVIn(%u,%u),P1Target(%u,%u)=>GMV(%d,%d),CMV(%d,%d),CMVCenter(%d,%d)",
                mEisPlusCropRatio, mSensorIdx, mLmvInput_W,mLmvInput_H,mP1Target_W,mP1Target_H,
                mGMV_X, mGMV_Y, mCmvX_Int, mCmvY_Int, mMVtoCenterX, mMVtoCenterY);

        if( UNLIKELY(mDebugDump >= 2) )
        {
            DumpStatistic(mLmvAlgoProcData.eis_state);
        }

        if( UNLIKELY(mDebugDump >= 1) )
        {
            LMV_LOG("mFrameCnt(%u)",mFrameCnt);
        }
        if( mFrameCnt == 0 )
        {
            LMV_LOG("not first frame");
            mFrameCnt = 1;
        }

    }

    mDoLmvCount++;

    return LMV_RETURN_NO_ERROR;
}

MVOID LMVHalImp::PrepareLmvResult(const MINT32 &cmvX, const MINT32 &cmvY)
{
    if( UNLIKELY(mDebugDump >= 1) )
    {
        LMV_LOG("cmvX(%d),cmvY(%d)", cmvX, cmvY);
    }

    Mutex::Autolock lock(mP1Lock);

    //====== Boundary Checking ======
    if( cmvX < 0 )
    {
        LMV_ERR("cmvX should not be negative(%u), fix to 0",cmvX);
        mCmvX_Int = mCmvX_Flt = 0;
    }
    else
    {
        MFLOAT tempCMV_X = cmvX / 256.0;
        MINT32 tempFinalCmvX = cmvX;
        mMVtoCenterX = cmvX;

        if( (tempCMV_X + (MFLOAT)mP1ResizeOut_W) > (MFLOAT)mP1ResizeIn_W )
        {
            LMV_LOG("cmvX too large(%u), fix to %u",cmvX,(mP1ResizeIn_W - mP1ResizeOut_W));
            tempFinalCmvX = (mP1ResizeIn_W - mP1ResizeOut_W);
        }

        mMVtoCenterX -=  ((mLmvInput_W-mP1Target_W)<<(intPartShift-1)); //Make mv for the top-left of center

        if( m_pLMVDrv->Get2PixelMode() == 1 )
        {
            if( mDebugDump > 0 )
            {
                LMV_LOG("tempFinalCmvX *= 2");
            }

            tempFinalCmvX *= 2;
            mMVtoCenterX *= 2;
        }
        else if( m_pLMVDrv->Get2PixelMode() == 2 )
        {
            if( mDebugDump > 0 )
            {
                LMV_LOG("tempFinalCmvX *= 4");
            }

            tempFinalCmvX *= 4;
            mMVtoCenterX *= 4;
        }

        mCmvX_Int = (tempFinalCmvX & (~0xFF)) >> intPartShift;
        mCmvX_Flt = (tempFinalCmvX & (0xFF));
    }

    if( cmvY < 0 )
    {
        LMV_ERR("cmvY should not be negative(%u), fix to 0",cmvY);

        mCmvY_Int = mCmvY_Flt = 0;
    }
    else
    {
        MFLOAT tempCMV_Y = cmvY / 256.0;
        MINT32 tempFinalCmvY = cmvY;
        mMVtoCenterY = cmvY;

        if( (tempCMV_Y + (MFLOAT)mP1ResizeOut_H) > (MFLOAT)mP1ResizeIn_H )
        {
            LMV_LOG("cmvY too large(%u), fix to %u",cmvY,(mP1ResizeIn_H - mP1ResizeOut_H));

            tempFinalCmvY = (mP1ResizeIn_H - mP1ResizeOut_H);
        }
        mMVtoCenterY -=  ((mP1ResizeIn_H-mP1ResizeOut_H)<<(intPartShift-1)); //Make mv for the top-left of center

        mCmvY_Int = (tempFinalCmvY & (~0xFF)) >> intPartShift;
        mCmvY_Flt = (tempFinalCmvY & (0xFF));
    }

    if( mDebugDump > 0 )
    {
        LMV_LOG("X(%u,%u),Y(%u,%u),MVtoCenter (%d,%d)",mCmvX_Int, mCmvX_Flt,
                                                       mCmvY_Int, mCmvY_Flt, mMVtoCenterX, mMVtoCenterY);
    }
}

MVOID LMVHalImp::GetLMVResult(MUINT32 &a_CMV_X_Int,
                                 MUINT32 &a_CMV_X_Flt,
                                 MUINT32 &a_CMV_Y_Int,
                                 MUINT32 &a_CMV_Y_Flt,
                                 MUINT32 &a_TarWidth,
                                 MUINT32 &a_TarHeight,
                                 MINT32  &a_MVtoCenterX,
                                 MINT32  &a_MVtoCenterY,
                                 MUINT32 &a_isFromRRZ)
{
    if( mLmvSupport == MFALSE )
    {
        LMV_LOG("mSensorIdx(%u) not support LMV",mSensorIdx);
        a_CMV_X_Int = 0;
        a_CMV_X_Flt = 0;
        a_CMV_Y_Int = 0;
        a_CMV_Y_Flt = 0;
        a_TarWidth  = 0;
        a_TarHeight = 0;
        a_MVtoCenterX = 0;
        a_MVtoCenterY = 0;
        a_isFromRRZ = 0;
        return;
    }

    {
        Mutex::Autolock lock(mP1Lock);

        a_CMV_X_Int = mCmvX_Int;
        a_CMV_X_Flt = mCmvX_Flt;
        a_CMV_Y_Int = mCmvY_Int;
        a_CMV_Y_Flt = mCmvY_Flt;
        a_TarWidth  = mP1ResizeOut_W;
        a_TarHeight = mP1ResizeOut_H;
        a_MVtoCenterX = mMVtoCenterX;
        a_MVtoCenterY = mMVtoCenterY;
        a_isFromRRZ = 1; //Hardcode MUST be fix later!!!!
    }

    if( mDebugDump >= 1 )
    {
        LMV_LOG("X(%u,%u),Y(%u,%u)", a_CMV_X_Int, a_CMV_X_Flt, a_CMV_Y_Int, a_CMV_Y_Flt);
    }
}

MVOID LMVHalImp::GetGmv(MINT32 &aGMV_X, MINT32 &aGMV_Y, MUINT32 *confX, MUINT32 *confY, MUINT32 *MAX_GMV)
{
    if( mLmvSupport == MFALSE )
    {
        LMV_LOG("mSensorIdx(%u) not support LMV",mSensorIdx);
        return;
    }

    aGMV_X = mGMV_X;
    aGMV_Y = mGMV_Y;

    if( MAX_GMV != NULL )
    {
        *MAX_GMV = mMAX_GMV;
    }

    {
        Mutex::Autolock lock(mP1Lock);

        if( confX != NULL )
        {
            *confX = mLmvLastData2EisPlus.ConfX;
        }

        if( confY != NULL )
        {
            *confY = mLmvLastData2EisPlus.ConfY;
        }
    }

    if( UNLIKELY(mDebugDump >= 1) )
    {
        if( confX && confY )
        {
            LMV_LOG("GMV(%d,%d),Conf(%d,%d)", aGMV_X, aGMV_Y, *confX, *confY);
        }
        else
        {
            LMV_LOG("GMV(%d,%d)", aGMV_X, aGMV_Y);
        }
    }
}

MBOOL LMVHalImp::GetLMVSupportInfo(const MUINT32 &aSensorIdx)
{
    mLmvSupport = m_pLMVDrv->GetLMVSupportInfo(aSensorIdx);
    return mLmvSupport;
}

MVOID LMVHalImp::GetLMVInputSize(MUINT32 *aWidth, MUINT32 *aHeight)
{
    if( NULL != m_pLMVDrv )
    {
        m_pLMVDrv->GetLMVInputSize(aWidth, aHeight);
    }
    else
    {
        LMV_ERR("m_pLMVDrv is NULL");
    }
}

MVOID LMVHalImp::GetRegSetting(void *data)
{
    if( NULL != m_pLMVDrv )
    {
        m_pLMVDrv->GetRegSetting(data);
    }
    else
    {
        LMV_ERR("m_pLMVDrv is NULL");
    }
}

MUINT32 LMVHalImp::GetLMVStatus()
{
    Mutex::Autolock lock(mLock);

    MUINT32 retVal = mEisPass1Enabled;
    return retVal;
}

MINT32 LMVHalImp::GetBufLMV(android::sp<IImageBuffer>& spBuf)
{
    Mutex::Autolock lock(mLMVOBufferListLock);

    if( !mLMVOBufferList.empty() )
    {
        spBuf = mLMVOBufferList.front();
        mLMVOBufferList.pop();
        if( UNLIKELY(mDebugDump >= 1) )
        {
            LMV_LOG("GetBufLMV : %zu", mLMVOBufferList.size());
        }
    }
    else
    {
        spBuf = m_pLMVOSliceBuffer[LMVO_BUFFER_NUM];

        LMV_WRN("GetBufEis empty!!");
    }
    return LMV_RETURN_NO_ERROR;
}

MSize LMVHalImp::QueryMinSize(MBOOL isEISOn, MSize sensorSize, NSCam::MSize outputSize, MSize requestSize, MSize FovMargin)
{
    MSize retSize;
    MINT32 out_width = 0;
    MINT32 out_height = 0;
    if( isEISOn == MFALSE )
    {
        out_width  = (requestSize.w <= 160) ? 160 : requestSize.w;
        out_height = (requestSize.h <= 160) ? 160 : requestSize.h;
    }
    else
    {
        if( mVideoW == 0 && outputSize.w != 0 )
        {
            mVideoW = outputSize.w;
        }

        if( mVideoH == 0 && outputSize.h != 0 )
        {
            mVideoH = outputSize.h;
        }

        if( (mVideoW  < VR_UHD_W) && (mVideoH  < VR_UHD_H) )
        {
            if( EISCustom::isEnabledLosslessMode() )
            {
                out_width  = (requestSize.w <= (EIS_FE_MAX_INPUT_W + FovMargin.w))? (EIS_FE_MAX_INPUT_W + FovMargin.w) : requestSize.w;
                out_height = (requestSize.h <= (EIS_FE_MAX_INPUT_H + FovMargin.h))? (EIS_FE_MAX_INPUT_H + FovMargin.h) : requestSize.h;
            }
            else
            {
                out_width  = (requestSize.w <= (VR_1080P_W + FovMargin.w))? (VR_1080P_W + FovMargin.w) : requestSize.w;
                out_height = (requestSize.h <= (VR_1080P_H + FovMargin.h))? (VR_1080P_H + FovMargin.h) : requestSize.h;
            }
        }
        else
        {
            MSize EISPlusFOV;
            if( EISCustom::isEnabledLosslessMode() )
            {
                EISPlusFOV.w = (VR_UHD_W*mEisPlusCropRatio/100.0f) + FovMargin.w;
                EISPlusFOV.h = (VR_UHD_H*mEisPlusCropRatio/100.0f) + FovMargin.h;
            }
            else
            {
                EISPlusFOV.w = (VR_UHD_W) + FovMargin.w;
                EISPlusFOV.h = (VR_UHD_H) + FovMargin.h;
            }
            out_width  = (requestSize.w <= EISPlusFOV.w) ?
                         EISPlusFOV.w : requestSize.w;
            out_height = (requestSize.h <= EISPlusFOV.h) ?
                         EISPlusFOV.h : requestSize.h;
            out_width = (out_width <= sensorSize.w)? out_width : sensorSize.w;
            out_height = (out_height <= sensorSize.h)? out_height : sensorSize.h;
            if( ((out_width*9)>>4) < out_height )
            {
                //Align video view angle
                out_height = (out_width*9)>>4;
            }
        }

        if( UNLIKELY(mDebugDump >= 1) )
        {
            LMV_LOG("eis(%d), sensor: %d/%d, outputSize: %d/%d, videoSize: %d/%d, ret: %d/%d, crop %d",
                    isEISOn, sensorSize.w, sensorSize.h, outputSize.w, outputSize.h, mVideoW, mVideoH , out_width, out_height, mEisPlusCropRatio);
        }
    }
    retSize = MSize(out_width, out_height);

    return retSize;
}


MINT32 LMVHalImp::NotifyLMV(QBufInfo& pBufInfo)
{
    android::sp<IImageBuffer> retBuf;
    for( size_t i = 0; i < pBufInfo.mvOut.size(); i++ )
    {
        if( pBufInfo.mvOut[i].mPortID.index == PORT_EISO.index )
        {

            Mutex::Autolock lock(mLMVOBufferListLock);

            retBuf = (pBufInfo.mvOut.at(i).mBuffer);
            mLMVOBufferList.push(retBuf);
            if( UNLIKELY(mDebugDump >= 1) )
            {
                LMV_LOG("NotifyLMV : %zu", mLMVOBufferList.size());
            }
        }
    }

    return LMV_RETURN_NO_ERROR;
}

MINT32 LMVHalImp::NotifyLMV(android::sp<NSCam::IImageBuffer>& spBuf)
{
    Mutex::Autolock lock(mLMVOBufferListLock);

    if( spBuf != 0 )
    {
        mLMVOBufferList.push(spBuf);
        if( UNLIKELY(mDebugDump >= 1) )
        {
            LMV_LOG("NotifyLMV : %zu - Drop", mLMVOBufferList.size());
        }
    }

    return LMV_RETURN_NO_ERROR;
}

MVOID LMVHalImp::GetLMVStatistic(EIS_STATISTIC_STRUCT *a_pLMV_Stat)
{
    for( MINT32 i = 0; i < LMV_MAX_WIN_NUM; ++i )
    {
        a_pLMV_Stat->i4LMV_X[i]    = mLmvAlgoProcData.eis_state.i4LMV_X[i];
        a_pLMV_Stat->i4LMV_Y[i]    = mLmvAlgoProcData.eis_state.i4LMV_Y[i];
        a_pLMV_Stat->i4LMV_X2[i]   = mLmvAlgoProcData.eis_state.i4LMV_X2[i];
        a_pLMV_Stat->i4LMV_Y2[i]   = mLmvAlgoProcData.eis_state.i4LMV_Y2[i];
        a_pLMV_Stat->NewTrust_X[i] = mLmvAlgoProcData.eis_state.NewTrust_X[i];
        a_pLMV_Stat->NewTrust_Y[i] = mLmvAlgoProcData.eis_state.NewTrust_Y[i];
        a_pLMV_Stat->SAD[i]        = mLmvAlgoProcData.eis_state.SAD[i];
        a_pLMV_Stat->SAD2[i]       = mLmvAlgoProcData.eis_state.SAD2[i];
        a_pLMV_Stat->AVG_SAD[i]    = mLmvAlgoProcData.eis_state.AVG_SAD[i];
    }
}

MVOID LMVHalImp::GetEisCustomize(EIS_TUNING_PARA_STRUCT *a_pDataOut)
{
    if( mDebugDump >= 1 )
    {
        LMV_ENTER();
    }

    EIS_Customize_Para_t customSetting;

    EISCustom::getEISData(&customSetting);

    a_pDataOut->sensitivity            = (EIS_SENSITIVITY_ENUM)customSetting.sensitivity;
    a_pDataOut->filter_small_motion    = customSetting.filter_small_motion;
    a_pDataOut->adv_shake_ext          = customSetting.adv_shake_ext;  // 0 or 1
    a_pDataOut->stabilization_strength = customSetting.stabilization_strength;  // 0.5~0.95

    a_pDataOut->advtuning_data.new_tru_th        = customSetting.new_tru_th;         // 0~100
    a_pDataOut->advtuning_data.vot_th            = customSetting.vot_th;             // 1~16
    a_pDataOut->advtuning_data.votb_enlarge_size = customSetting.votb_enlarge_size;  // 0~1280
    a_pDataOut->advtuning_data.min_s_th          = customSetting.min_s_th;           // 10~100
    a_pDataOut->advtuning_data.vec_th            = customSetting.vec_th;             // 0~11   should be even
    a_pDataOut->advtuning_data.spr_offset        = customSetting.spr_offset;         //0 ~ MarginX/2
    a_pDataOut->advtuning_data.spr_gain1         = customSetting.spr_gain1;          // 0~127
    a_pDataOut->advtuning_data.spr_gain2         = customSetting.spr_gain2;          // 0~127

    a_pDataOut->advtuning_data.gmv_pan_array[0] = customSetting.gmv_pan_array[0];   //0~5
    a_pDataOut->advtuning_data.gmv_pan_array[1] = customSetting.gmv_pan_array[1];   //0~5
    a_pDataOut->advtuning_data.gmv_pan_array[2] = customSetting.gmv_pan_array[2];   //0~5
    a_pDataOut->advtuning_data.gmv_pan_array[3] = customSetting.gmv_pan_array[3];   //0~5

    a_pDataOut->advtuning_data.gmv_sm_array[0] = customSetting.gmv_sm_array[0];    //0~5
    a_pDataOut->advtuning_data.gmv_sm_array[1] = customSetting.gmv_sm_array[1];    //0~5
    a_pDataOut->advtuning_data.gmv_sm_array[2] = customSetting.gmv_sm_array[2];    //0~5
    a_pDataOut->advtuning_data.gmv_sm_array[3] = customSetting.gmv_sm_array[3];    //0~5

    a_pDataOut->advtuning_data.cmv_pan_array[0] = customSetting.cmv_pan_array[0];   //0~5
    a_pDataOut->advtuning_data.cmv_pan_array[1] = customSetting.cmv_pan_array[1];   //0~5
    a_pDataOut->advtuning_data.cmv_pan_array[2] = customSetting.cmv_pan_array[2];   //0~5
    a_pDataOut->advtuning_data.cmv_pan_array[3] = customSetting.cmv_pan_array[3];   //0~5

    a_pDataOut->advtuning_data.cmv_sm_array[0] = customSetting.cmv_sm_array[0];    //0~5
    a_pDataOut->advtuning_data.cmv_sm_array[1] = customSetting.cmv_sm_array[1];    //0~5
    a_pDataOut->advtuning_data.cmv_sm_array[2] = customSetting.cmv_sm_array[2];    //0~5
    a_pDataOut->advtuning_data.cmv_sm_array[3] = customSetting.cmv_sm_array[3];    //0~5

    a_pDataOut->advtuning_data.vot_his_method  = (EIS_VOTE_METHOD_ENUM)customSetting.vot_his_method; //0 or 1
    a_pDataOut->advtuning_data.smooth_his_step = customSetting.smooth_his_step; // 2~6

    a_pDataOut->advtuning_data.eis_debug = customSetting.eis_debug;

    if( mDebugDump >= 1 )
    {
        LMV_EXIT();
    }
}

MVOID LMVHalImp::DumpStatistic(const EIS_STATISTIC_STRUCT &aLmvStat)
{
    LMV_ENTER();

    MINT32 totalMVX = 0;
    MINT32 totalMVY = 0;

    for( int i = 0; i < LMV_MAX_WIN_NUM; ++i )
    {
        totalMVX += aLmvStat.i4LMV_X[i];
        totalMVY += aLmvStat.i4LMV_Y[i];
    }

    MPoint rawLMV(totalMVX/LMV_MAX_WIN_NUM, totalMVY/LMV_MAX_WIN_NUM);

    LMV_LOG("GMV(%d,%d),CMV(%d,%d):Raw(%d,%d),Center(%d,%d)",
            mGMV_X, mGMV_Y, mCmvX_Int, mCmvY_Int, rawLMV.x, rawLMV.y, mMVtoCenterX, mMVtoCenterY);

    if( mDebugDump >= 3 )
    {
        for( int i = 0; i < LMV_MAX_WIN_NUM; ++i )
        {
            LMV_LOG("MB%d%d,(LMV_X,LMV_Y)=(%d,%d),(LMV_X2,LMV_Y2)=(%d,%d),MinSAD(%u),(NewTrust_X,NewTrust_Y)=(%u,%u),MinSAD2(%u),AvgSAD(%u)",
                    (i/4), (i%4),
                    aLmvStat.i4LMV_X[i], aLmvStat.i4LMV_Y[i],
                    aLmvStat.i4LMV_X2[i], aLmvStat.i4LMV_Y2[i],
                    aLmvStat.SAD[i],
                    aLmvStat.NewTrust_X[i], aLmvStat.NewTrust_Y[i],
                    aLmvStat.SAD2[i],
                    aLmvStat.AVG_SAD[i]);
        }

        if( mDumpFile != NULL )
        {
            fprintf(mDumpFile, "GMV(%d,%d),CMV(%d,%d):Raw(%d,%d),Center(%d,%d)\n",
                    mGMV_X, mGMV_Y, mCmvX_Int, mCmvY_Int, rawLMV.x, rawLMV.y, mMVtoCenterX, mMVtoCenterY);
        }

    }

    LMV_EXIT();
}

