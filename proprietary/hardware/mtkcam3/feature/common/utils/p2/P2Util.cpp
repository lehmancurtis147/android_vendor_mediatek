/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#include <mtkcam/custom/ExifFactory.h>
#include <mtkcam/def/common.h>
#include <mtkcam/drv/def/Dip_Notify_datatype.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/utils/exif/DebugExifUtils.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam3/feature/lcenr/lcenr.h>
#include <DpDataType.h>

#include <mtkcam3/feature/utils/p2/P2Util.h>
#include <mtkcam3/feature/utils/p2/P2Trace.h>
#define ILOG_MODULE_TAG P2Util
#include <mtkcam3/feature/utils/log/ILogHeader.h>

using android::sp;
using NS3Av3::TuningParam;
using NS3Av3::IHal3A;
using NS3Av3::IHalISP;
using NS3Av3::MetaSet_T;
using NSCam::NSIoPipe::NSPostProc::INormalStream;
using NSCam::NSIoPipe::EPortCapbility;
using NSCam::NSIoPipe::EPortCapbility_None;
using NSCam::NSIoPipe::EPortCapbility_Disp;
using NSCam::NSIoPipe::EPortCapbility_Rcrd;
using NSCam::NSIoPipe::ModuleInfo;
using NSCam::NSIoPipe::EDIPInfoEnum;
using NSCam::NSIoPipe::EDIPINFO_NEED_SECURE_BUFFER_LCEI;
using NSCam::NSIoPipe::EDIPINFO_NEED_SECURE_BUFFER_DEPI;
using NSCam::NSIoPipe::EDIPINFO_NEED_SECURE_BUFFER_DMGI;
using NSCam::NSIoPipe::EDIPINFO_NEED_SECURE_BUFFER_IMGBI;
using NSCam::NSIoPipe::EDIPINFO_NEED_SECURE_BUFFER_IMGCI;

using NSImageio::NSIspio::EPortIndex;
using NSImageio::NSIspio::EPortIndex_IMGI;
using NSImageio::NSIspio::EPortIndex_IMGBI;
using NSImageio::NSIspio::EPortIndex_IMGCI;
using NSImageio::NSIspio::EPortIndex_VIPI;
using NSImageio::NSIspio::EPortIndex_DEPI;
using NSImageio::NSIspio::EPortIndex_LCEI;
using NSImageio::NSIspio::EPortIndex_DMGI;
using NSImageio::NSIspio::EPortIndex_BPCI;
using NSImageio::NSIspio::EPortIndex_LSCI;
using NSImageio::NSIspio::VirDIPPortIdx_YNR_FACEI;
using NSImageio::NSIspio::VirDIPPortIdx_YNR_LCEI;
using NSImageio::NSIspio::EPortIndex_IMG2O;
using NSImageio::NSIspio::EPortIndex_IMG3O;
using NSImageio::NSIspio::EPortIndex_WDMAO;
using NSImageio::NSIspio::EPortIndex_WROTO;

using NSCam::NSIoPipe::PORT_IMGI;
using NSCam::NSIoPipe::PORT_IMGBI;
using NSCam::NSIoPipe::PORT_IMGCI;
using NSCam::NSIoPipe::PORT_VIPI;
using NSCam::NSIoPipe::PORT_DEPI;
using NSCam::NSIoPipe::PORT_LCEI;
using NSCam::NSIoPipe::PORT_DMGI;
using NSCam::NSIoPipe::PORT_BPCI;
using NSCam::NSIoPipe::PORT_LSCI;
using NSCam::NSIoPipe::PORT_YNR_FACEI;
using NSCam::NSIoPipe::PORT_YNR_LCEI;
using NSCam::NSIoPipe::PORT_IMG2O;
using NSCam::NSIoPipe::PORT_IMG3O;
using NSCam::NSIoPipe::PORT_WDMAO;
using NSCam::NSIoPipe::PORT_WROTO;


namespace NSCam {
namespace Feature {
namespace P2Util {

/*******************************************
Common function
*******************************************/

MBOOL is4K2K(const MSize &size)
{
    const MINT32 UHD_VR_WIDTH = 3840;
    const MINT32 UHD_VR_HEIGHT = 2160;
    return (size.w >= UHD_VR_WIDTH && size.h >= UHD_VR_HEIGHT);
}

MCropRect getCropRect(const MRectF &rectF) {
    #define MAX_MDP_FRACTION_BIT (20) // MDP use 20bits
    MCropRect cropRect(rectF.p.toMPoint(), rectF.s.toMSize());
    cropRect.p_fractional.x = (rectF.p.x-cropRect.p_integral.x) * (1<<MAX_MDP_FRACTION_BIT);
    cropRect.p_fractional.y = (rectF.p.y-cropRect.p_integral.y) * (1<<MAX_MDP_FRACTION_BIT);
    cropRect.w_fractional = (rectF.s.w-cropRect.s.w) * (1<<MAX_MDP_FRACTION_BIT);
    cropRect.h_fractional = (rectF.s.h-cropRect.s.h) * (1<<MAX_MDP_FRACTION_BIT);
    return cropRect;
}

auto getDebugExif()
{
    static auto const sInst = MAKE_DebugExif();
    return sInst;
}

/*******************************************
Tuning function
*******************************************/

void* allocateRegBuffer(MBOOL zeroInit)
{
    void *buffer = ::malloc(INormalStream::getRegTableSize());
    if( buffer && zeroInit )
    {
        memset(buffer, 0, INormalStream::getRegTableSize());
    }
    return buffer;
}

MVOID releaseRegBuffer(void* &buffer)
{
    ::free(buffer);
    buffer = NULL;
}

TuningParam makeTuningParam(const ILog &log, const P2Pack &p2Pack, IHalISP *halISP, MetaSet_T &inMetaSet, MetaSet_T *pOutMetaSet, MBOOL resized, void *regBuffer, IImageBuffer *lcso)
{
    (void)log;
    TRACE_S_FUNC_ENTER(log);
    TuningParam tuning;
    tuning.pRegBuf = regBuffer;
    tuning.pLcsBuf = lcso;

    trySet<MUINT8>(inMetaSet.halMeta, MTK_3A_PGN_ENABLE,
                   resized ? 0 : 1);
    P2_CAM_TRACE_BEGIN(TRACE_DEFAULT, "P2Util:Tuning");
    if( halISP && regBuffer )
    {
        MINT32 ret3A = halISP->setP2Isp(0, inMetaSet, &tuning, pOutMetaSet);
        if( ret3A < 0 )
        {
            MY_S_LOGW(log, "hal3A->setIsp failed, memset regBuffer to 0");
            if( tuning.pRegBuf )
            {
                memset(tuning.pRegBuf, 0, INormalStream::getRegTableSize());
            }
        }
        if( pOutMetaSet )
        {
            updateExtraMeta(p2Pack, pOutMetaSet->halMeta);
            updateDebugExif(p2Pack, inMetaSet.halMeta, pOutMetaSet->halMeta);
        }
    }
    else
    {
        MY_S_LOGE(log, "cannot run setIsp: hal3A=%p reg=%p", halISP, regBuffer);
    }
    P2_CAM_TRACE_END(TRACE_DEFAULT);

    TRACE_S_FUNC_EXIT(log);
    return tuning;
}

MBOOL needSecureBuffer(const NSIoPipe::PortID &port)
{
    static std::map<EDIPInfoEnum, MUINT32> dipInfoMap;
    if (CC_UNLIKELY(dipInfoMap.empty()))
    {
        MBOOL isOK = INormalStream::queryDIPInfo(dipInfoMap);
        MY_LOGE_IF(isOK != MTRUE, "query DIP information failed");
    }

    switch(port.index) {
        case NSImageio::NSIspio::EPortIndex_IMGCI :
            return dipInfoMap[EDIPINFO_NEED_SECURE_BUFFER_IMGCI];
        case NSImageio::NSIspio::EPortIndex_IMGBI :
            return dipInfoMap[EDIPINFO_NEED_SECURE_BUFFER_IMGBI];
        case NSImageio::NSIspio::EPortIndex_LCEI :
            return dipInfoMap[EDIPINFO_NEED_SECURE_BUFFER_LCEI];
        case NSImageio::NSIspio::EPortIndex_DEPI :
            return dipInfoMap[EDIPINFO_NEED_SECURE_BUFFER_DEPI];
        case NSImageio::NSIspio::EPortIndex_DMGI :
            return dipInfoMap[EDIPINFO_NEED_SECURE_BUFFER_DMGI];
        default:
            return MFALSE;
    }
}

/*******************************************
Metadata function
*******************************************/

MVOID updateExtraMeta(const P2Pack &p2Pack, IMetadata &outHal)
{
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    TRACE_S_FUNC_ENTER(p2Pack.mLog);
    trySet<MINT32>(outHal, MTK_PIPELINE_FRAME_NUMBER, p2Pack.getFrameData().mMWFrameNo);
    trySet<MINT32>(outHal, MTK_PIPELINE_REQUEST_NUMBER, p2Pack.getFrameData().mMWFrameRequestNo);
    TRACE_S_FUNC_EXIT(p2Pack.mLog);
}

MVOID updateDebugExif(const P2Pack &p2Pack, const IMetadata &inHal, IMetadata &outHal)
{
    (void)p2Pack;
    P2_CAM_TRACE_CALL(TRACE_ADVANCED);
    TRACE_S_FUNC_ENTER(p2Pack.mLog);
    MUINT8 needExif = 0;
    if( tryGet<MUINT8>(inHal, MTK_HAL_REQUEST_REQUIRE_EXIF, needExif) &&
        needExif )
    {
        MINT32 vhdrMode = SENSOR_VHDR_MODE_NONE;
        if( tryGet<MINT32>(inHal, MTK_P1NODE_SENSOR_VHDR_MODE, vhdrMode) &&
            vhdrMode != SENSOR_VHDR_MODE_NONE )
        {
            std::map<MUINT32, MUINT32> debugInfoList;
            debugInfoList[getDebugExif()->getTagId_MF_TAG_IMAGE_HDR()] = 1;

            IMetadata exifMeta;
            tryGet<IMetadata>(outHal, MTK_3A_EXIF_METADATA, exifMeta);
            if( DebugExifUtils::setDebugExif(
                    DebugExifUtils::DebugExifType::DEBUG_EXIF_MF,
                    static_cast<MUINT32>(MTK_MF_EXIF_DBGINFO_MF_KEY),
                    static_cast<MUINT32>(MTK_MF_EXIF_DBGINFO_MF_DATA),
                    debugInfoList, &exifMeta) != NULL )
            {
                trySet<IMetadata>(outHal, MTK_3A_EXIF_METADATA, exifMeta);
            }
        }
    }
    TRACE_S_FUNC_EXIT(p2Pack.mLog);
}

MBOOL updateCropRegion(IMetadata &outHal, const MRect &rect)
{
    // TODO: set output crop w/ margin setting
    MBOOL ret = MFALSE;
    ret = trySet<MRect>(outHal, MTK_SCALER_CROP_REGION, rect);
    return ret;
}

/*******************************************
QParams util function
*******************************************/

EPortCapbility toCapability(MUINT32 usage)
{
    EPortCapbility cap = EPortCapbility_None;
    if( usage & (GRALLOC_USAGE_HW_COMPOSER|GRALLOC_USAGE_HW_TEXTURE) )
    {
        cap = EPortCapbility_Disp;
    }
    else if( usage & GRALLOC_USAGE_HW_VIDEO_ENCODER )
    {
        cap = EPortCapbility_Rcrd;
    }
    return cap;
}

const char* toName(EPortIndex index)
{
    switch( index )
    {
    case EPortIndex_IMGI:           return "imgi";
    case EPortIndex_IMGBI:          return "imgbi";
    case EPortIndex_IMGCI:          return "imgci";
    case EPortIndex_VIPI:           return "vipi";
    case EPortIndex_DEPI:           return "depi";
    case EPortIndex_LCEI:           return "lcei";
    case EPortIndex_DMGI:           return "dmgi";
    case EPortIndex_BPCI:           return "bpci";
    case EPortIndex_LSCI:           return "lsci";
    case VirDIPPortIdx_YNR_FACEI:   return "ynr_facei";
    case VirDIPPortIdx_YNR_LCEI:    return "ynr_lcei";
    case EPortIndex_IMG2O:          return "img2o";
    case EPortIndex_IMG3O:          return "img3o";
    case EPortIndex_WDMAO:          return "wdmao";
    case EPortIndex_WROTO:          return "wroto";
    default:                        return "unknown";
    };
    return NULL;
}

const char* toName(MUINT32 index)
{
    return toName((EPortIndex)index);
}

const char* toName(const NSCam::NSIoPipe::PortID &port)
{
    return toName((EPortIndex)port.index);
}

const char* toName(const Input &input)
{
    return toName((EPortIndex)input.mPortID.index);
}

const char* toName(const Output &output)
{
    return toName((EPortIndex)output.mPortID.index);
}

MBOOL is(const PortID &port, EPortIndex index)
{
    return port.index == index;
}

MBOOL is(const Input &input, EPortIndex index)
{
    return input.mPortID.index == index;
}

MBOOL is(const Output &output, EPortIndex index)
{
    return output.mPortID.index == index;
}

MBOOL is(const PortID &port, const PortID &rhs)
{
    return port.index == rhs.index;
}

MBOOL is(const Input &input, const PortID &rhs)
{
    return input.mPortID.index == rhs.index;
}

MBOOL is(const Output &output, const PortID &rhs)
{
    return output.mPortID.index == rhs.index;
}

MVOID printQParams(const ILog &log, unsigned i, const Input &input)
{
    (void)log;
    (void)i;
    const unsigned index = input.mPortID.index;
    MSize size;
    MINT fmt = 0;
    if( input.mBuffer )
    {
        size = input.mBuffer->getImgSize();
        fmt = input.mBuffer->getImgFormat();
    }
    MY_S_LOGD(log, "mvIn[%d] idx=%d size=(%d,%d) fmt=0x%08x", i, index, size.w, size.h, fmt);
}

MVOID printQParams(const ILog &log, unsigned i, const Output &output)
{
    (void)log;
    (void)i;
    const unsigned index = output.mPortID.index;
    const MUINT32 cap = output.mPortID.capbility;
    const MINT32 transform = output.mTransform;
    MSize size;
    MINT fmt = 0;
    if( output.mBuffer )
    {
        size = output.mBuffer->getImgSize();
        fmt = output.mBuffer->getImgFormat();
    }
    MY_S_LOGD(log, "mvOut[%d] idx=%s size=(%d,%d) fmt=0x%08x, cap=0x%02x, transform=%d", i, toName(index), size.w, size.h, fmt, cap, transform);
}

MVOID printQParams(const ILog &log, unsigned i, const MCrpRsInfo &crop)
{
    (void)log;
    (void)i;
    (void)crop;
    MY_S_LOGD(log, "mvCropRsInfo[%d] groupID=%d frameGroup=%d i(%d,%d) f(%d,%d) s(%dx%d) r(%dx%d)",
              i, crop.mGroupID, crop.mFrameGroup,
              crop.mCropRect.p_integral.x, crop.mCropRect.p_integral.y,
              crop.mCropRect.p_fractional.x, crop.mCropRect.p_fractional.y,
              crop.mCropRect.s.w, crop.mCropRect.s.h,
              crop.mResizeDst.w, crop.mResizeDst.h);
}

MVOID printQParams(const ILog &log, unsigned i, const ModuleInfo &info)
{
    (void)log;
    (void)i;
    switch(info.moduleTag)
    {
    case EDipModule_SRZ1:
        MY_S_LOGD(log, "mvModuleData[%d] tag=SRZ1(%d)", i, info.moduleTag);
        break;
    case EDipModule_SRZ4:
        MY_S_LOGD(log, "mvModuleData[%d] tag=SRZ4(%d) ", i, info.moduleTag);
        break;
    default:
        MY_S_LOGD(log, "mvModuleData[%d] tag=UNKNWON(%d)", i, info.moduleTag);
        break;
    };
}

MVOID printQParams(const ILog &log, unsigned i, const ExtraParam &extra)
{
    (void)log;
    (void)i;
    switch(extra.CmdIdx)
    {
    case NSIoPipe::EPIPE_FE_INFO_CMD:
        MY_S_LOGD(log, "mvExtraParam[%d] cmd=FE(%d)", i, extra.CmdIdx);
        break;
    case NSIoPipe::EPIPE_FM_INFO_CMD:
        MY_S_LOGD(log, "mvExtraParam[%d] cmd=FM(%d)", i, extra.CmdIdx);
        break;
    case NSIoPipe::EPIPE_MDP_PQPARAM_CMD:
        MY_S_LOGD(log, "mvExtraParam[%d] cmd=PQParam(%d)", i, extra.CmdIdx);
        break;
    default:
        MY_S_LOGD(log, "mvExtraParam[%d] cmd=UNKOWN(%d)", i, extra.CmdIdx);
        break;
    };
}

MVOID printQParams(const ILog &log, const QParams &params)
{
    for( unsigned f = 0, fCount = params.mvFrameParams.size(); f < fCount; ++f )
    {
        const FrameParams &frame = params.mvFrameParams[f];
        MY_S_LOGD(log, "QParams %d/%d", f, fCount);

        for( unsigned i = 0, n = frame.mvIn.size(); i < n; ++i )
        {
            printQParams(log, i, frame.mvIn[i]);
        }
        for( unsigned i = 0, n = frame.mvOut.size(); i < n; ++i )
        {
            printQParams(log, i, frame.mvOut[i]);
        }
        for( unsigned i = 0, n = frame.mvCropRsInfo.size(); i < n; ++i )
        {
            printQParams(log, i, frame.mvCropRsInfo[i]);
        }
        for( unsigned i = 0, n = frame.mvModuleData.size(); i < n; ++i )
        {
            printQParams(log, i, frame.mvModuleData[i]);
        }
        for( unsigned i = 0, n = frame.mvExtraParam.size(); i < n; ++i )
        {
            printQParams(log, i, frame.mvExtraParam[i]);
        }
    }
}

MVOID printTuningParam(const ILog &log, const TuningParam &tuning)
{
    MY_S_LOGD(log, "reg=%p lcs=%p", tuning.pRegBuf, tuning.pLcsBuf);
}

MVOID push_in(FrameParams &frame, const PortID &portID, IImageBuffer *buffer)
{
    Input input;
    input.mPortID = portID,
    input.mPortID.group = 0;
    input.mBuffer = buffer;
    frame.mvIn.push_back(input);
}

MVOID push_in(FrameParams &frame, const PortID &portID, const P2IO &in)
{
    push_in(frame, portID, in.mBuffer);
}

MVOID push_out(FrameParams &frame, const PortID &portID, IImageBuffer *buffer)
{
    push_out(frame, portID, buffer, EPortCapbility_None, 0);
}

MVOID push_out(FrameParams &frame, const PortID &portID, IImageBuffer *buffer, EPortCapbility cap, MINT32 transform)
{
    Output output;
    output.mPortID = portID;
    output.mPortID.group = 0;
    output.mPortID.capbility = cap;
    output.mTransform = transform;
    output.mBuffer = buffer;
    frame.mvOut.push_back(output);
}

MVOID push_out(FrameParams &frame, const PortID &portID, const P2IO &out)
{
    push_out(frame, portID, out.mBuffer, out.mCapability, out.mTransform);
}

MVOID push_crop(FrameParams &frame, MUINT32 cropID, const MCropRect &crop, const MSize &size)
{
    MCrpRsInfo cropInfo;
    cropInfo.mGroupID = cropID;
    cropInfo.mCropRect = crop;
    cropInfo.mResizeDst = size;
    frame.mvCropRsInfo.push_back(cropInfo);
}

MVOID push_crop(FrameParams &frame, MUINT32 cropID, const MRectF &crop, const MSize &size, const MINT32 dmaConstrainFlag)
{
    MCrpRsInfo cropInfo;
    cropInfo.mGroupID = cropID;
    cropInfo.mCropRect = getCropRect(crop);

    if ((dmaConstrainFlag & DMACONSTRAIN_NOSUBPIXEL) || (dmaConstrainFlag & DMACONSTRAIN_2BYTEALIGN))
    {
        cropInfo.mCropRect.p_fractional.x = 0;
        cropInfo.mCropRect.p_fractional.y = 0;
        cropInfo.mCropRect.w_fractional = 0;
        cropInfo.mCropRect.h_fractional = 0;
        if (dmaConstrainFlag & DMACONSTRAIN_2BYTEALIGN)
        {
            cropInfo.mCropRect.p_integral.x &= (~1);
            cropInfo.mCropRect.p_integral.y &= (~1);
        }
    }
    cropInfo.mResizeDst = size;
    frame.mvCropRsInfo.push_back(cropInfo);
}
/*******************************************
QParams function
*******************************************/

MBOOL push_srz4_LCENR(FrameParams &frame, _SRZ_SIZE_INFO_ *srz4, const P2Pack &p2Pack, MBOOL resized, const MSize &imgiSize, const MSize &lcsoSize)
{
    if( srz4 )
    {
        LCENR_IN_PARAMS in;
        LCENR_OUT_PARAMS out;
        const P2SensorData &data = p2Pack.getSensorData();

        in.resized = resized;
        in.p2_in = imgiSize;
        in.rrz_in = data.mP1BinSize;
        in.rrz_crop_in = data.mP1BinCrop;
        in.rrz_out = data.mP1OutSize;
        in.lce_full = lcsoSize;
        calculateLCENRConfig(in, out);

        *srz4 = out.srz4Param;

        ModuleInfo info;
        info.moduleTag = EDipModule_SRZ4;
        info.frameGroup = 0;
        info.moduleStruct = reinterpret_cast<MVOID*>(srz4);
        frame.mvModuleData.push_back(info);
    }
    return (srz4 != NULL);
}

DpPqParam* makeDpPqParam(DpPqParam *param, const P2Pack &p2Pack, const Output &out)
{
    if( !param )
    {
        MY_S_LOGE(p2Pack.mLog, "Invalid DpPqParam buffer = nullptr, port:%s(%d)",
                  toName(out.mPortID), out.mPortID.index);
        return nullptr;
    }

    return makeDpPqParam(param, p2Pack, out.mPortID.capbility);
}

DpPqParam* makeDpPqParam(DpPqParam *param, const P2Pack &p2Pack, const MUINT32 portCapabitity)
{
    if( !param )
    {
        MY_S_LOGE(p2Pack.mLog, "Invalid DpPqParam buffer = nullptr, portCapabitity:(%d)",
                  portCapabitity);
        return nullptr;
    }

    DpIspParam &ispParam = param->u.isp;

    param->scenario = MEDIA_ISP_PREVIEW;
    param->enable = false;

    ispParam.iso = p2Pack.getSensorData().mISO;
    ispParam.timestamp = p2Pack.getSensorData().mMWUniqueKey;
    ispParam.frameNo = p2Pack.getFrameData().mMWFrameNo;
    ispParam.requestNo = p2Pack.getFrameData().mMWFrameRequestNo;
    ispParam.lensId = p2Pack.getSensorData().mSensorID;
    ispParam.userString[0] = '\0';

    const P2PlatInfo *plat = p2Pack.getPlatInfo();

    if( p2Pack.getConfigInfo().mSupportClearZoom &&
        plat->supportClearZoom() &&
        portCapabitity == EPortCapbility_Disp )
    {
        param->enable |= (PQ_COLOR_EN|PQ_ULTRARES_EN);
        ClearZoomParam &cz = ispParam.clearZoomParam;
        cz.captureShot = CAPTURE_SINGLE;

        uint32_t customIndex = 0;
        cz.p_customSetting = plat->queryNVRamData(P2PlatInfo::FID_CLEARZOOM, p2Pack.getSensorData().mMagic3A, customIndex);
    }

    if( p2Pack.getConfigInfo().mSupportDRE &&
        plat->supportDRE() )
    {
        param->enable |= (PQ_DRE_EN);
        DpDREParam &dre = ispParam.dpDREParam;
        dre.cmd = DpDREParam::Cmd::Initialize | DpDREParam::Cmd::Default;
        dre.userId = ((unsigned long long)MEDIA_ISP_PREVIEW)<<32;
        dre.buffer = nullptr;
        dre.p_customSetting = plat->queryNVRamData(P2PlatInfo::FID_DRE, p2Pack.getSensorData().mMagic3A, dre.customIndex);
    }

    return param;
}

MVOID push_PQParam(FrameParams &frame, const P2Pack &p2Pack, const P2ObjPtr &obj)
{
    if( !obj.pqParam )
    {
        MY_S_LOGE(p2Pack.mLog, "Invalid pqParam buffer = NULL");
        return;
    }

    obj.pqParam->WDMAPQParam = NULL;
    obj.pqParam->WROTPQParam = NULL;
    for( const Output &out : frame.mvOut )
    {
        if( out.mPortID.index == EPortIndex_WDMAO &&
            obj.pqParam->WDMAPQParam == NULL )
        {
            obj.pqParam->WDMAPQParam = makeDpPqParam(obj.pqWDMA, p2Pack, out);
        }
        else if( out.mPortID.index == EPortIndex_WROTO &&
                 obj.pqParam->WROTPQParam == NULL )
        {
            obj.pqParam->WROTPQParam = makeDpPqParam(obj.pqWROT, p2Pack, out);
        }
    }

    ExtraParam extra;
    extra.CmdIdx = NSIoPipe::EPIPE_MDP_PQPARAM_CMD;
    extra.moduleStruct = (void*)obj.pqParam;
    frame.mvExtraParam.push_back(extra);
}

MVOID updateQParams(QParams &qparams, const P2Pack &p2Pack, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning)
{
    updateFrameParams(qparams.mvFrameParams.editItemAt(0), p2Pack, io, obj, tuning);
}

QParams makeQParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning)
{
    QParams qparams;
    qparams.mvFrameParams.push_back(makeFrameParams(p2Pack, tag, io, obj, tuning));
    return qparams;
}

QParams makeQParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj)
{
    QParams qparams;
    qparams.mvFrameParams.push_back(makeFrameParams(p2Pack, tag, io, obj));
    return qparams;
}


MVOID updateFrameParams(FrameParams &frame, const P2Pack &p2Pack, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning)
{
    TRACE_S_FUNC_ENTER(p2Pack.mLog);

    const P2PlatInfo* platInfo = p2Pack.getPlatInfo();

    MSize inSize = io.mIMGI.getImgSize();
    for( auto &in : frame.mvIn )
    {
        if( is(in, PORT_IMGI) && in.mBuffer )
        {
            inSize = in.mBuffer->getImgSize();
            break;
        }
    }

    if( tuning.pRegBuf )
    {
        frame.mTuningData = tuning.pRegBuf;
    }
    if( tuning.pLsc2Buf )
    {
        push_in(frame, platInfo->getLsc2Port(), (IImageBuffer*)tuning.pLsc2Buf);
    }
    if( tuning.pBpc2Buf )
    {
        push_in(frame, platInfo->getBpc2Port(), (IImageBuffer*)tuning.pBpc2Buf);
    }

    if( tuning.pLcsBuf )
    {
        IImageBuffer *lcso = (IImageBuffer*)tuning.pLcsBuf;
        push_in(frame, PORT_LCEI, lcso);
        if( platInfo->hasYnvLceiPort() )
        {
            push_in(frame, platInfo->getYnrLceiPort(), lcso);
            push_srz4_LCENR(frame, obj.srz4, p2Pack, io.isResized(), inSize, lcso->getImgSize());
        }
    }

    TRACE_S_FUNC_EXIT(p2Pack.mLog);
}

FrameParams makeFrameParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj, const TuningParam &tuning)
{
    TRACE_S_FUNC_ENTER(p2Pack.mLog);
    FrameParams fparam = makeFrameParams(p2Pack, tag, io, obj);
    updateFrameParams(fparam, p2Pack, io, obj, tuning);
    TRACE_S_FUNC_EXIT(p2Pack.mLog);
    return fparam;
}

FrameParams makeFrameParams(const P2Pack &p2Pack, ENormalStreamTag tag, const P2IOPack &io, const P2ObjPtr &obj)
{
    const ILog &log = p2Pack.mLog;
    TRACE_S_FUNC_ENTER(log);

    const sp<Cropper> cropper = p2Pack.getSensorData().mCropper;
    MUINT32 cropFlag = 0;
    cropFlag |= io.isResized() ? Cropper::USE_RESIZED : 0;
    cropFlag |= io.useLMV() ? Cropper::USE_EIS_12 : 0;

    FrameParams frame;
    frame.mStreamTag = tag;
    frame.UniqueKey = p2Pack.getSensorData().mMWUniqueKey;

    if( io.mIMGI.isValid() )
    {
        push_in(frame, PORT_IMGI, io.mIMGI);
    }

    if( io.mIMG2O.isValid() )
    {
        MCropRect crop = cropper->calcViewAngle(log, io.mIMG2O.getTransformSize(), cropFlag);
        push_out(frame, PORT_IMG2O, io.mIMG2O);
        push_crop(frame, CROP_IMG2O, crop, io.mIMG2O.getImgSize());
    }
    if( io.mWDMAO.isValid() )
    {
        MCropRect crop = cropper->calcViewAngle(log, io.mWDMAO.getTransformSize(), cropFlag);
        push_out(frame, PORT_WDMAO, io.mWDMAO);
        push_crop(frame, CROP_WDMAO, crop, io.mWDMAO.getImgSize());
    }
    if( io.mWROTO.isValid() )
    {
        MCropRect crop = cropper->calcViewAngle(log, io.mWROTO.getTransformSize(), cropFlag);
        push_out(frame, PORT_WROTO, io.mWROTO);
        push_crop(frame, CROP_WROTO, crop, io.mWROTO.getImgSize());
    }

    if(obj.hasPQ)
    {
        push_PQParam(frame, p2Pack, obj);
    }

    TRACE_S_FUNC_EXIT(log);
    return frame;
}

} // namespace P2Util
} // namespace Feature
} // namespace NSCam
