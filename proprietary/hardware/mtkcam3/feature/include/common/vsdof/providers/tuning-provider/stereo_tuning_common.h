/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef STEREO_TUNING_COMMON_H_
#define STEREO_TUNING_COMMON_H_

#include <vector>   //for std::vector
#include <utility>  //for std::pair

#include <camera_custom_stereo.h>
#include <mtkcam3/feature/stereo/hal/stereo_common.h>
#include <mtkcam3/feature/stereo/hal/stereo_setting_provider.h>
#include <mtkcam3/feature/stereo/hal/stereo_size_provider.h>

#define PERPERTY_TUNING_PROVIDER_LOG    "vendor.STEREO.log.tuning"

// Common & SW part
#define QUERY_KEY_SCENARIO              "Scenario"
#define QUERY_KEY_TUNING_PARAMS         "TuningParams"

// OCC
#define QUERY_KEY_CORE_NUMBER           "CoreNumber"
#define QUERY_KEY_OCC_CORE_NUMBER       "OCC_CoreNumber"

// SW Bokeh
#define QUERY_KEY_APP                   "APP"
#define QUERY_KEY_CAMERA                "Camera"
#define QUERY_KEY_GALLERY               "Gallery"

// GF&SW Bokeh
#define QUERY_KEY_DOF_LEVEL             "DoFLevel"  //input
#define QUERY_KEY_DOF_VALUE             "DoFValue"  //output
#define QUERY_KEY_DOF_TABLE_PREFIX      "DoFTable"
#define QUERY_KEY_CLEAR_RANGE_TABLE     "ClearRangeTable"
#define QUERY_KEY_CLEAR_TABLE_PREFIX    "ClearTable"

// GF
#define QUERY_KEY_GF_CORE_NUMBER        "GF_CoreNumber"
#define QUERY_KEY_DISPCTRLPOINTS        "dispCtrlPoints"
#define QUERY_KEY_BLURGAINTABLE         "blurGainTable"

// OCC&WMF
#define DEPTH_MASK_VALUE (255)

enum ENUM_CLEAR_REGION  //for GF
{
    E_CLEAR_REGION_SMALL  = 0,
    E_CLEAR_REGION_MEDIUM = 1,
    E_CLEAR_REGION_LARGE  = 2,
};

typedef std::map<std::string, int> QUERY_INT_T;
typedef std::map<std::string, const char *> QUERY_STR_T;
typedef std::map<std::string, void *> QUERY_BUFFER_T;
typedef std::vector<std::pair<std::string, int>> TUNING_PAIR_LIST_T;
typedef std::vector<int>    QUERY_INT_LIST_T;

struct TuningQuery_T
{
    QUERY_INT_T intParams;
    QUERY_STR_T strParams;
    QUERY_BUFFER_T  results;
};

#endif