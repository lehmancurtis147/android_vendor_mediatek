#define LOG_TAG "utilSystrace"
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>
#include <android/log.h>

#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)

#define ATRACE_MESSAGE_LEN (256)

int util_atrace_marker_fd = -1;

void utilTraceInitOnce()
{
    util_atrace_marker_fd = open("/sys/kernel/debug/tracing/trace_marker", O_WRONLY | O_CLOEXEC);
    if (util_atrace_marker_fd == -1)
    {
        LOGD("Error opening trace file: %s (%d)", strerror(errno), errno);
        LOGD("utilTraceInit fail");
    }
    else
    {
        LOGD("utilTraceInit ok");
    }
}

void utilTraceTerminate()
{
    LOGD("[%s] fd is %d", __FUNCTION__, util_atrace_marker_fd);
    if (util_atrace_marker_fd != -1)
    {
        close(util_atrace_marker_fd);
    }
    LOGD("utilTraceTerminate done");
}

void utilTraceBegin(const char *name)
{
    char buf[ATRACE_MESSAGE_LEN];
    int len = snprintf(buf, ATRACE_MESSAGE_LEN, "B|%d|%s", getpid(), name);
    if (len >= (int) sizeof(buf))
    {
        LOGD("Truncated name in %s: %s", __FUNCTION__, name);
        len = sizeof(buf) - 1;
        write(util_atrace_marker_fd, buf, len);
    }
}

void utilTraceEnd()
{
    char c = 'E';
    if (1 != write(util_atrace_marker_fd, &c, 1))
    {
        LOGD("utilTraceEnd write error: %s", strerror(errno));
    }
}

void utilTraceAsyncBegin(const char *name, const int32_t cookie)
{
    char buf[ATRACE_MESSAGE_LEN];
    int len = snprintf(buf, ATRACE_MESSAGE_LEN, "S|%d|%s|%i", getpid(), name, cookie);
    write(util_atrace_marker_fd, buf, len);
}

void utilTraceAsyncEnd(const char *name, const int32_t cookie)
{
    char buf[ATRACE_MESSAGE_LEN];
    int len = snprintf(buf, ATRACE_MESSAGE_LEN, "F|%d|%s|%i", getpid(), name, cookie);
    write(util_atrace_marker_fd, buf, len);
}

