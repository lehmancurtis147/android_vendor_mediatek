#include <cutils/sockets.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <unistd.h>
#include <errno.h>
#include <pthread.h>

#include "Context.h"
#include "LogDefine.h"
#include "MSocket.h"

#define DATA_LEN	256

MSocket::MSocket()
{
	m_socketID = -1;
	m_threadID = -1;
	m_stop = 0;
	memset(&m_thread, 0, sizeof(pthread_t));
	m_Mutex = PTHREAD_MUTEX_INITIALIZER;
	m_type = SOCKET_END;
	
	signal(SIGPIPE,SIG_IGN);
	
}

MSocket::~MSocket(void)
{
	deinit();
}

int MSocket::initServer(const char * socket_name, int namespaceId, int bListen)
{
	META_LOG("[META][Socket] To Create Socket Server:(%s)", socket_name);
	
	m_socketID = socket_local_server(socket_name, namespaceId, SOCK_STREAM);
	
	META_LOG("[Meta][Socket] m_socketID = %d errno = %d", m_socketID,errno);	

	listen(m_socketID,4);

	while((m_socketID = accept(m_socketID,NULL,NULL))>0)
	{
		break;
		META_LOG("Meta][Socket]Accept Connection");
	}

	META_LOG("Meta][Socket]Meta Should Block Here!");

	if(bListen)
	{
		m_threadID = pthread_create(&m_thread, NULL, ThreadFunc,  this);
		if(m_threadID)
		{
			META_LOG("[Meta][Socket] Failed to create socket thread!");
			return 0;
		}
	}
	
	return 1;	
		
}

int MSocket::initClient(const char * socket_name, int namespaceId, int bListen)
{
	int count = 0;
	int val = 0;
	
	META_LOG("[Meta][Socket] To connect server:(%s)", socket_name);
	while(m_socketID < 0) 
	{
		count++;
		m_socketID = socket_local_client(socket_name, namespaceId, SOCK_STREAM);
		META_LOG("[Meta][Socket] init client m_socketID = %d", m_socketID);
        META_LOG("[Meta][Socket] errno = %d, string = %s", errno, strerror(errno));
		usleep(200*1000);
		if(count == 5)
			return 0;		
	}

	META_LOG("[Meta][Socket] connect successful");
	//if bListen is true, we will create thread to read socket data.
	if(bListen)
	{
		m_threadID = pthread_create(&m_thread, NULL, ThreadFunc,  this);
		if(m_threadID)
		{
			META_LOG("[Meta][Socket] Failed to create socket thread!");
			return 0;
		}
	}
	
	if(0 == setsockopt(m_socketID, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)))
	{
		META_LOG("[Meta][Socket] set socket to option to TCP_NODELAY!");
	}
	
	return 1;
}

void MSocket::deinit()
{
	if(m_threadID == 0)
	{
		m_stop = 1;
		pthread_join(m_thread, NULL);
	}

    if (m_socketID > 0)
    {
       	close (m_socketID);
        m_socketID = -1;
    }
}

void MSocket::send_msg(const char *msg)
{
	int nWritten = 0;

	META_LOG("[Meta][Socket] send mssage (%s) - socket id = %d", msg,  m_socketID);

	if((nWritten = write(m_socketID, msg, strlen(msg))) < 0)
	{
		META_LOG("[Meta][Socket] socket write error: %s", strerror(errno));
	}
	else
	{
		META_LOG("[Meta][Socket] write %d Bytes, total = %zd", nWritten, strlen(msg));
	}
}

void *MSocket::ThreadFunc(void *p)
{
	MSocket *pSocket = (MSocket *)p;
	if(pSocket != NULL)
		pSocket->wait_msg();
	
	return NULL;
}

//////////////////////////////////////////////MATCISocket////////////////////////////////////////////////////

MATCISocket::MATCISocket()
{

}

MATCISocket::MATCISocket(SOCKET_TYPE type)
{
	m_type = type;
}

MATCISocket::~MATCISocket()
{

}

void MATCISocket::wait_msg()
{
	const char *msg = "calibration";
	const char *msg_ok = "OK";
	char data[DATA_LEN] = {0};
	int  len = 0;
	
	META_LOG("[Meta][MATCISocket] wait_msg m_socketID = %d", m_socketID);

	while(m_stop == 0)
	{
		len = read(m_socketID, data, DATA_LEN);
		if(len >0)
		{
			if(len == DATA_LEN)
				data[len-1] = 0;
			else
				data[len] = 0;
			
			META_LOG("[Meta][MATCISocket] m_socketID = %d, data len = %d, rawdata = (%s)", m_socketID, len, data);
			char *pos = strstr(data, msg);
			if(pos != NULL)
			{
				createSerPortThread();
				createAllModemThread();
				continue;
			}
			pos = strstr(data, msg_ok);
			if(pos != NULL)
			{
				META_LOG("[Meta][MATCISocket][DEBUG] got OK from modem");
				if(getATRespFlag()==1)
				{
					META_LOG("[Meta][MATCISocket][DEBUG] setATRespFlag to 0");
					setATRespFlag(0);
				}
				continue;
			}
			
			setATRespFlag(-1);
		}
		else
		{		
			usleep(100000); // wake up every 0.1sec   
		}		
	}
	return;
}


//////////////////////////////////////////////MLogSocket////////////////////////////////////////////////////

MLogSocket::MLogSocket()
{
	m_mdlogpulling = -1;
	m_mblogpulling = -1;
}

MLogSocket::MLogSocket(SOCKET_TYPE type)
{
	m_type = type;
	m_mdlogpulling = -1;
	m_mblogpulling = -1;
}

MLogSocket::~MLogSocket()
{

}

void MLogSocket::wait_msg()
{
	char data[DATA_LEN] = {0};
	int  len = 0;

	META_LOG("[Meta][MLogSocket] wait_msg m_socketID = %d", m_socketID);

	while(m_stop == 0)
	{
		memset(data, 0, DATA_LEN);				
		len = read(m_socketID, data, DATA_LEN);

		if(len > 0)
		{
			if(len == DATA_LEN)
				data[len-1] = 0;
			else
				data[len] = 0;
			
			META_LOG("[Meta][MLogSocket] m_socketID = %d, data len = %d, rawdata = (%s), cmd = (%s)", m_socketID, len, data, m_strCmd.c_str());
			
			pthread_mutex_lock(&m_Mutex);
			if(strcmp(data, "pull_mdlog_start,1") == 0)
				m_mdlogpulling = 1;
			else if(strcmp(data, "ext_autopull") == 0)
				m_mblogpulling = 1;
			pthread_mutex_unlock(&m_Mutex);
			
			if(m_strCmd.size() > 1)
			{
				if(strstr(data, m_strCmd.c_str()) != NULL)
				{
					pthread_mutex_lock(&m_Mutex);
					m_strRsp = string(data);
					m_strCmd = "";
					META_LOG("[Meta][MLogSocket] wait_msg response = (%s)", m_strRsp.c_str());
					pthread_mutex_unlock(&m_Mutex);
				}
			}
		}
		else
		{		
			usleep(100000); // wake up every 0.1sec   
		}		
	}
	return;
}

int MLogSocket::recv_rsp(char *buf)
{
	META_LOG("[Meta][MLogSocket] recv_rsp begin");
	int count = 0;
	while(1)
	{
		pthread_mutex_lock(&m_Mutex);
		if(m_strRsp.length() > 0)
		{
			string strRsp = m_strRsp.erase(0, m_strRsp.find_last_of(',')+1);
			if(strRsp.length() > 0)
				strncpy(buf, strRsp.c_str(), strRsp.length());
			META_LOG("[Meta][MLogSocket] recv_rsp response = (%s)", buf);
			m_strRsp = "";
			pthread_mutex_unlock(&m_Mutex);
			return true;
		}
		pthread_mutex_unlock(&m_Mutex);
		usleep(100000);
		if (++count == 45)
		{
			pthread_mutex_lock(&m_Mutex);
			META_LOG("[Meta][MLogSocket] recv_rsp end-false");
			m_strCmd = "";
			pthread_mutex_unlock(&m_Mutex);
			return false;
		}
	}

	META_LOG("[Meta][MLogSocket] recv_rsp end-true");
	return true;
}


void MLogSocket::send_msg(const char *msg, bool ignore)
{
	int nWritten = 0;

	META_LOG("[Meta][MLogSocket] send mssage (%s) - socket id = %d", msg,  m_socketID);

	if(ignore == false)
	{
		pthread_mutex_lock(&m_Mutex);
		m_strCmd = string(msg);
		pthread_mutex_unlock(&m_Mutex);
	}
	
	if((nWritten = write(m_socketID, msg, strlen(msg))) < 0)
	{
		m_strCmd = "";
		META_LOG("[Meta][MLogSocket] socket write error: %s", strerror(errno));
	}
	else
	{
		META_LOG("[Meta][MLogSocket] write %d Bytes, total = %zd", nWritten, strlen(msg));
	}
}

int MLogSocket::getLogPullingStatus(int type)
{
	int status = -1;
	switch(type)
	{
		case SOCKET_MDLOGGER:
			status = m_mdlogpulling;
			break;
		case SOCKET_MOBILELOG:
			status = m_mblogpulling;
			break;
	}

	return status;
}

void MLogSocket::setLogPullingStatus(int type, int value)
{	
	pthread_mutex_lock(&m_Mutex);

	switch(type)
	{
		case SOCKET_MDLOGGER:
			m_mdlogpulling = value;
			break;
		case SOCKET_MOBILELOG:
			m_mblogpulling = value;
			break;
	}

	pthread_mutex_unlock(&m_Mutex);
}




