#ifndef _METASOCKET_H_
#define _METASOCKET_H_

#include <string>
using namespace std;

typedef enum
{
	SOCKET_MDLOGGER  = 0,
	SOCKET_MOBILELOG = 1,
	SOCKET_ATCI = 2,
	SOCKET_AT_ATCI = 3,
	SOCKET_END       = 4
}SOCKET_TYPE;


class MSocket
{

public:
	MSocket();
	virtual ~MSocket(void);
	int initClient(const char * socket_name, int namespaceId, int bListen=1);
	int initServer(const char * socket_name, int namespaceId, int bListen=1);
	void deinit();
	int getSocketID() const
	{
	   return m_socketID;
	}
	virtual void send_msg(const char *msg);

private:
	static void* ThreadFunc(void*);
	virtual void wait_msg() = 0;

public:
	SOCKET_TYPE m_type;

protected:	
	int m_socketID;
	int m_threadID;
	int m_stop;
	pthread_t  m_thread;
	pthread_mutex_t m_Mutex;
};

//////////////////////////////////////////////MATCISocket////////////////////////////////////////////////////

class MATCISocket : public MSocket
{
public:
	MATCISocket();
	MATCISocket(SOCKET_TYPE type);
	virtual ~MATCISocket();

private:	
	virtual void wait_msg();
};

//////////////////////////////////////////////MLogSocket////////////////////////////////////////////////////

class MLogSocket : public MSocket
{
public:
	MLogSocket();
	MLogSocket(SOCKET_TYPE type);
	virtual ~MLogSocket();
	int recv_rsp(char *buf);
	void send_msg(const char *msg, bool ignore);
	int getLogPullingStatus(int type);
	void setLogPullingStatus(int type, int value);

private:
	string m_strCmd;
	string m_strRsp;
	int    m_mdlogpulling;
	int    m_mblogpulling;
	
private:	
	virtual void wait_msg();
};


#endif


