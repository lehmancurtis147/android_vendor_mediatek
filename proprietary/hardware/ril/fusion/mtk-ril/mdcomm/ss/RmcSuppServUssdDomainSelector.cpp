/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#include "RmcSuppServUssdDomainSelector.h"
#include "RfxIntsData.h"
#include "RfxVoidData.h"
#include "RfxStringsData.h"
#include "RfxStringData.h"
#include "RfxMessageId.h"
#include "RfxRilUtils.h"
#include "GsmUtil.h"
#include "SSUtil.h"
#include "SSConfig.h"
#include "SuppServDef.h"
#include "rfx_properties.h"

#include <telephony/mtk_ril.h>
#include <mtkconfigutils.h>
#include <libmtkrilutils.h>
#include <mtk_properties.h>
#include <string.h>
#include <unistd.h>

RFX_REGISTER_DATA_TO_EVENT_ID(RfxStringsData, RFX_MSG_EVENT_UNSOL_ON_USSD);
RFX_REGISTER_DATA_TO_EVENT_ID(RfxStringsData, RFX_MSG_EVENT_UNSOL_ON_USSI);

static const int event[] = {
    RFX_MSG_EVENT_UNSOL_ON_USSD,
    RFX_MSG_EVENT_UNSOL_ON_USSI
};

RmcSuppServUssdDomainSelector::RmcSuppServUssdDomainSelector(int slot_id, int channel_id) :
    RfxBaseHandler(slot_id, channel_id) {
    registerToHandleEvent(event, sizeof(event)/sizeof(int));
    setUssiAction(USSI_REQUEST);
    mUssiSnapshot = NULL;
}

RmcSuppServUssdDomainSelector::~RmcSuppServUssdDomainSelector() {
    if (mUssiSnapshot != NULL) {
        free(mUssiSnapshot);
    }
}

void RmcSuppServUssdDomainSelector::onHandleEvent(const sp<RfxMclMessage>& msg) {
    int id = msg->getId();
    switch(id) {
        case RFX_MSG_EVENT_UNSOL_ON_USSD:
            handleOnUssd(msg);
            break;
        case RFX_MSG_EVENT_UNSOL_ON_USSI:
            handleOnUssi(msg);
            break;
        default:
            logE(TAG, "should not be here");
            break;
    }
}

void RmcSuppServUssdDomainSelector::handleOnUssd(const sp<RfxMclMessage>& msg) {
    logD(TAG, "handleOnUssd, from UrcHandler");
    sp<RfxMclMessage> urc;
    urc = RfxMclMessage::obtainUrc(RFX_MSG_UNSOL_ON_USSD, m_slot_id,
            RfxStringsData(msg->getData()->getData(), msg->getData()->getDataLength()));
    responseToTelCore(urc);
}

void RmcSuppServUssdDomainSelector::handleOnUssi(const sp<RfxMclMessage>& msg) {
    logD(TAG, "handleOnUssi, from UrcHandler");

    if (isWithoutUssiFramework()) {
        sp<RfxMclMessage> newUssdUrc = convertUssiToUssdUrc(msg);
        int ussdMode = atoi(((char **) newUssdUrc->getData()->getData())[0]);

        if ((ussdMode == 0) || (ussdMode == 1) || (ussdMode == 5)) {
            logD(TAG, "handleOnUssi, no need to do CSFB, ussdMode = %d", ussdMode);
            responseToTelCore(newUssdUrc);
        } else {
            logD(TAG, "handleOnUssi, need to do CSFB, ussdMode = %d", ussdMode);
            handleUssiCSFB(newUssdUrc);
        }
    } else {
        sp<RfxMclMessage> urc = RfxMclMessage::obtainUrc(RFX_MSG_UNSOL_ON_USSI, m_slot_id,
                RfxStringsData(msg->getData()->getData(), msg->getData()->getDataLength()));
        responseToTelCore(urc);
    }
}

/**
 * Handle USSI CSFB, param "ussi" is the string need to do CSFB
 */
void RmcSuppServUssdDomainSelector::handleUssiCSFB(const sp<RfxMclMessage>& msg) {
    if (mUssiSnapshot != NULL) {
        logD(TAG, "handleUssiCSFB, mUssiSnapshot = %s", mUssiSnapshot);

        sp<RfxMclMessage> tmpMsg = RfxMclMessage::obtainRequest(
                RFX_MSG_REQUEST_SEND_USSD,
                new RfxStringData((void *) mUssiSnapshot, strlen(mUssiSnapshot)),
                msg->getSlotId(),
                msg->getToken(),
                msg->getSendToMainProtocol(),
                msg->getRilToken(),
                msg->getTimeStamp(),
                msg->getAddAtFront());
        requestSendUSSD(tmpMsg, SEND_URC_BACK);

        free(mUssiSnapshot);
    } else {
        logE(TAG, "handleUssiCSFB fails, mUssiSnapshot is NULL");
    }
}

void RmcSuppServUssdDomainSelector::requestSendUSSD(const sp<RfxMclMessage>& msg,
        UssdReportCase reportCase) {
    logD(TAG, "requestSendUSSD, reportCase = %d", reportCase);

    sp<RfxAtResponse> p_response;
    int err;
    char* cmd = NULL;
    RIL_Errno ret = RIL_E_INTERNAL_ERR;
    int strLen = 0;
    char* pTmpStr = NULL;
    char* p_ussdRequest = NULL;
    char* p_input_ussdRequest = (char*) msg->getData()->getData();

    if (p_input_ussdRequest == NULL || strlen(p_input_ussdRequest) == 0) {
        logE(TAG, "requestSendUSSD:p_ussdRequest null or empty.");
        goto error;
    }
    p_ussdRequest = convertToUCS2(p_input_ussdRequest);

    if (p_ussdRequest == NULL) {
        logE(TAG, "requestSendUSSD:p_ussdRequest malloc fail");
        goto error;
    }

    /**
     * AT+ECUSD=<m>,<n>,<str>,<dcs>
     * <m>: 1 for SS, 2 for USSD
     * <n>: 1 for execute SS or USSD, 2 for cancel USSD session
     * <str>: string type parameter, the SS or USSD string
     */

    /**
     * 01xx    General Data Coding indication
     *
     * Bits 5..0 indicate the following:
     *   Bit 5, if set to 0, indicates the text is uncompressed
     *   Bit 5, if set to 1, indicates the text is compressed using the compression algorithm defined in 3GPP TS 23.042 [13]
     *
     *   Bit 4, if set to 0, indicates that bits 1 to 0 are reserved and have no message class meaning
     *   Bit 4, if set to 1, indicates that bits 1 to 0 have a message class meaning:
     *
     *     Bit 1   Bit 0       Message Class:
     *       0       0           Class 0
     *       0       1           Class 1 Default meaning: ME-specific.
     *       1       0           Class 2 (U)SIM specific message.
     *       1       1           Class 3 Default meaning: TE-specific (see 3GPP TS 27.005 [8])
     *
     *   Bits 3 and 2 indicate the character set being used, as follows:
     *
     *     Bit 3   Bit 2       Character set:
     *       0       0           GSM 7 bit default alphabet
     *       0       1           8 bit data
     *       1       0           UCS2 (16 bit) [10]
     *       1       1           Reserved
     */
    //BEGIN mtk08470 [20130109][ALPS00436983]
    // USSD string cannot more than MAX_RIL_USSD_NUMBER_LENGTH digits
    // We convert input char to unicode hex string and store it to p_ussdRequest.
    // For example, convert input "1" to "3100"; So len of p_ussdRequest is 4 times of input
    strLen = strlen(p_ussdRequest)/4;
    if (strLen > MAX_RIL_USSD_NUMBER_LENGTH) {
        logW(TAG, "USSD stringlen = %d, max = %d", strLen, MAX_RIL_USSD_NUMBER_LENGTH);
        strLen = MAX_RIL_USSD_NUMBER_LENGTH;
    }
    pTmpStr = (char*) calloc(1, (4*strLen+1));
    if(pTmpStr == NULL) {
        logE(TAG, "Malloc fail");
        free((char *)p_ussdRequest);
        goto error;
    }
    memcpy(pTmpStr, p_ussdRequest, 4*strLen);
    //END mtk08470 [20130109][ALPS00436983]
    asprintf(&cmd, "AT+ECUSD=2,1,\"%s\",72", pTmpStr); /* <dcs> = 0x48 */

    p_response = atSendCommand(cmd);

    free(cmd);
    free(pTmpStr);
    free((char *)p_ussdRequest);

    err = p_response->getError();
    if (err < 0 || p_response == NULL) {
        logE(TAG, "requestSendUSSD Fail");
        goto error;
    }

    switch (p_response->atGetCmeError()) {
        case CME_SUCCESS:
            ret = RIL_E_SUCCESS;
            break;
        case CME_CALL_BARRED:
        case CME_OPR_DTR_BARRING:
            ret = RIL_E_CALL_BARRED;
            break;
        case CME_PHB_FDN_BLOCKED:
            ret = RIL_E_FDN_CHECK_FAILURE;
            break;
        default:
            break;
    }

error:
    if (reportCase == SEND_RESPONSE_BACK) {
        logD(TAG, "requestSendUSSD: send response back to framework");
        sp<RfxMclMessage> response = RfxMclMessage::obtainResponse(msg->getId(), ret,
                RfxVoidData(), msg, false);

        // response to TeleCore
        responseToTelCore(response);
    } else if (reportCase == SEND_URC_BACK) {
        if (ret != RIL_E_SUCCESS) {
            logD(TAG, "requestSendUSSD: send urc back to framework");
            sp<RfxMclMessage> ussdUrcMsg;
            char *ussdUrcStrings[2];

            asprintf(&ussdUrcStrings[0], "%d", 4);
            asprintf(&ussdUrcStrings[1], "");
            ussdUrcMsg = RfxMclMessage::obtainUrc(RFX_MSG_UNSOL_ON_USSD,
                    m_slot_id, RfxStringsData(ussdUrcStrings, 2));
            responseToTelCore(ussdUrcMsg);

            free(ussdUrcStrings[0]);
            free(ussdUrcStrings[1]);
        } else {
            logD(TAG, "requestSendUSSD: no need to send anything to framework");
        }
    } else {
        logD(TAG, "requestSendUSSD: no need to send anything to framework");
    }

}

void RmcSuppServUssdDomainSelector::requestCancelUssd(const sp<RfxMclMessage>& msg) {
    sp<RfxAtResponse> p_response;
    int err;
    RIL_Errno ret = RIL_E_INTERNAL_ERR;

    /**
     * AT+ECUSD=<m>,<n>,<str>
     * <m>: 1 for SS, 2 for USSD
     * <n>: 1 for execute SS or USSD, 2 for cancel USSD session
     * <str>: string type parameter, the SS or USSD string
     */

    p_response = atSendCommand("AT+ECUSD=2,2");
    err = p_response->getError();

    if (err < 0 || p_response->getSuccess() == 0) {
        logE(TAG, "Cancel USSD failed.");
    } else {
        ret = RIL_E_SUCCESS;
    }

    sp<RfxMclMessage> response = RfxMclMessage::obtainResponse(msg->getId(), ret,
            RfxVoidData(), msg, false);

    // response to TeleCore
    responseToTelCore(response);
}

void RmcSuppServUssdDomainSelector::requestSendUSSI(const sp<RfxMclMessage>& msg) {
    const char** strings = (const char**) (msg->getData()->getData());
    sp<RfxAtResponse> p_response;
    int action = atoi(strings[0]);
    const char* ussi = strings[1];
    String8 currentMccmnc = getMclStatusManager()->
            getString8Value(RFX_STATUS_KEY_UICC_GSM_NUMERIC, String8("0"));
    int currRadioState = getMclStatusManager()->getIntValue(RFX_STATUS_KEY_RADIO_STATE);
    logD(TAG, "requestSendUSSI: action = %d, ussi = %s", action, ussi);

    if (isWithoutUssiFramework()) {
        // Snapshot the USSI string in USSD domain selector
        if (mUssiSnapshot != NULL) {
            free(mUssiSnapshot);
        }

        mUssiSnapshot = strdup(ussi);
        if (mUssiSnapshot == NULL) {
            logE(TAG, "requestSendUSSI: mUssiSnapshot strdup fail");
        }
    }

    const char* p_ussdRequest = convertToUCS2((char*)ussi);

    int err;
    RIL_Errno ret = RIL_E_GENERIC_FAILURE;
    int strLen = 0;
    char* pTmpStr = NULL;

    if (p_ussdRequest == NULL || strlen(p_ussdRequest) == 0) {
        logE(TAG, "requestSendUSSI:p_ussdRequest null or empty.");
        goto error;
    }

    /**
     * The framework's IMS registration status may not be updated immediately if UE under a
     * severe condition while running MTBF test. It causes the USSI request may be sent from
     * framework when IMS is not registered. Therefore, we should check IMS registration status
     * again by sending AT+CIREG? in order to avoid unnecessary AT+CMD request in following steps
     * (e.g. AT+EAPPROVE).
     */
    if(!isWithoutUssiFramework() && !isImsRegOn()) {
        logE(TAG, "requestSendUSSI: it's not allowed by IMS registration check, return RIL_E_GENERIC_FAILURE");
        ret = RIL_E_GENERIC_FAILURE;
        sleep(1);  // Let the UI have time to show up the dialog
        goto error;
    }

    if (!isFdnAllowed(ussi)) {
        logE(TAG, "requestSendUSSI: it's not allowed by FDN check, return RIL_E_FDN_CHECK_FAILURE");
        ret = RIL_E_FDN_CHECK_FAILURE;
        goto error;
    }

    /**
     * VoPS works only when UE camps on cellular netowrk. If the flight mode
     * is on, then VoPS = 0. We should skip VoPS check when flight mode on and
     * WFC is registered. We check radio state is because flight mode on causes
     * radio is powered off.
     */
    if (currRadioState == RADIO_STATE_OFF) {
        logD(TAG, "Radio explictly powered off, skip VoPS check");
    } else {
        if (!isVopsOn()) {
            logE(TAG, "requestSendUSSI: it's not allowed by VoPS check, return RIL_E_GENERIC_FAILURE");
            ret = RIL_E_GENERIC_FAILURE;
            goto error;
        }
    }

    strLen = strlen(p_ussdRequest)/4;
    if (strLen > MAX_RIL_USSD_NUMBER_LENGTH) {
        logW(TAG, "USSI stringlen = %d, max = %d", strLen, MAX_RIL_USSD_NUMBER_LENGTH);
        strLen = MAX_RIL_USSD_NUMBER_LENGTH;
    }
    pTmpStr = (char*) calloc(1, (4*strLen+1));
    if(pTmpStr == NULL) {
        logE(TAG, "Malloc fail");
        free((char *)p_ussdRequest);
        goto error;
    }
    memcpy(pTmpStr, p_ussdRequest, 4*strLen);

    if (isSimulateUSSI()) {
        logD(TAG, "Simulate USSI by CS");
        p_response = atSendCommand(String8::format("AT+ECUSD=2,1,\"%s\",72", pTmpStr));
    } else {
        if (SSConfig::ussiWithNoLang(currentMccmnc.string())) {
            p_response = atSendCommand(String8::format("AT+EIUSD=2,1,%d,\"%s\",\"\",0",
                    action, ussi));
        } else {
            p_response = atSendCommand(String8::format("AT+EIUSD=2,1,%d,\"%s\",\"en\",0",
                    action, ussi));
        }
    }

    free(pTmpStr);
    free((char *)p_ussdRequest);

    err = p_response->getError();
    if (err < 0 || p_response == NULL) {
        logE(TAG, "requestSendUSSI Fail");
        goto error;
    }

    switch (p_response->atGetCmeError()) {
        case CME_SUCCESS:
            ret = RIL_E_SUCCESS;
            break;
        case CME_CALL_BARRED:
        case CME_OPR_DTR_BARRING:
            ret = RIL_E_CALL_BARRED;
            break;
        case CME_PHB_FDN_BLOCKED:
            ret = RIL_E_FDN_CHECK_FAILURE;
            break;
        case CME_OPERATION_NOT_SUPPORTED:
            ret = RIL_E_REQUEST_NOT_SUPPORTED;
            break;
         case CME_USSI_NOT_SUPPORTED:
            sleep(1); // Let the UI have time to show up the dialog
            ret = RIL_E_GENERIC_FAILURE;
            break;
        default:
            break;
    }

error:
    if (isWithoutUssiFramework()) {
        logD(TAG, "requestSendUSSI: isWithoutUssiFramework ret = %d", ret);
        if (ret == RIL_E_GENERIC_FAILURE) {
            logW(TAG, "requestSendUSSI: generic fail, do CSFB directly in UssdDomainSelector");
            sp<RfxMclMessage> newMsg = convertUssiToUssdReq(msg);
            requestSendUSSD(newMsg, SEND_RESPONSE_BACK);
        } else {
            // For other USSI error cause, no need to do CSFB
            sp<RfxMclMessage> response = RfxMclMessage::obtainResponse(RFX_MSG_REQUEST_SEND_USSD,
                    ret, RfxVoidData(), msg, false);

            // response to TeleCore
            responseToTelCore(response);
        }
    } else {
        logD(TAG, "requestSendUSSI: isUssiFramework ret = %d", ret);
        // MTK framework will handle the CSFB logic
        sp<RfxMclMessage> response = RfxMclMessage::obtainResponse(msg->getId(), ret,
                RfxVoidData(), msg, false);

        // response to TeleCore
        responseToTelCore(response);
    }
}

void RmcSuppServUssdDomainSelector::requestCancelUssi(const sp<RfxMclMessage>& msg) {
    sp<RfxAtResponse> p_response;
    int err;
    RIL_Errno ret = RIL_E_GENERIC_FAILURE;

    if (isSimulateUSSI()) {
        p_response = atSendCommand("AT+ECUSD=2,2");
    } else {
        p_response = atSendCommand("AT+EIUSD=2,2,2,\"\",\"en\",0");
    }

    err = p_response->getError();

    if (err < 0 || p_response->getSuccess() == 0) {
        logD(TAG, "Cancel USSD failed.");
    } else {
        ret = RIL_E_SUCCESS;
    }

    sp<RfxMclMessage> response = RfxMclMessage::obtainResponse(msg->getId(), ret,
            RfxVoidData(), msg, false);

    // response to TeleCore
    responseToTelCore(response);
}

/**
 * If using AOSP framework, need to convert "send" USSD reuqest to USSI request
 * when IMS is registered on.
 */
void RmcSuppServUssdDomainSelector::requestSendUssdDomainSelect(const sp<RfxMclMessage>& msg) {
    if (DBG) {
        logD(TAG, "requestSendUssdDomainSelect: isWithoutUssiFramework() = %d",
                isWithoutUssiFramework());
    }

    if (isWithoutUssiFramework() && isImsRegOn()) {
        sp<RfxMclMessage> newMsg = convertUssdToUssiReq(msg);
        requestSendUSSI(newMsg);
    } else {
        requestSendUSSD(msg, SEND_RESPONSE_BACK);
    }
}

/**
 * If using AOSP framework, need to convert "cancel" USSD reuqest to USSI request
 * when IMS is registered on.
 */
void RmcSuppServUssdDomainSelector::requestCancelUssdDomainSelect(const sp<RfxMclMessage>& msg) {
    if (DBG) {
        logD(TAG, "requestCancelUssdDomainSelect: isWithoutUssiFramework() = %d",
                isWithoutUssiFramework());
    }

    if (isWithoutUssiFramework() && isImsRegOn()) {
        sp<RfxMclMessage> newMsg = RfxMclMessage::obtainRequest(
                RFX_MSG_REQUEST_CANCEL_USSI,
                new RfxVoidData(),
                msg->getSlotId(),
                msg->getToken(),
                msg->getSendToMainProtocol(),
                msg->getRilToken(),
                msg->getTimeStamp(),
                msg->getAddAtFront());
        requestCancelUssi(newMsg);

        // Reset mUssiAction to USSI_REQUEST
        setUssiAction(USSI_REQUEST);
    } else {
        requestCancelUssd(msg);
    }
}

/**
 * Convert USSD request to USSI request
 */
sp<RfxMclMessage> RmcSuppServUssdDomainSelector::convertUssdToUssiReq(
        const sp<RfxMclMessage>& msg) {
    sp<RfxMclMessage> newUssiReqMsg;
    char *newUssiReqStrings[2];

    // Action
    asprintf(&newUssiReqStrings[0], (getUssiAction() == USSI_REQUEST) ? "1" : "0");

    // USSI String
    asprintf(&newUssiReqStrings[1], "%s", (char*) msg->getData()->getData());

    if (DBG) {
        logD(TAG, "convertUssdToUssiReq: action = %s, ussi = %s",
                newUssiReqStrings[0], newUssiReqStrings[1]);
    }

    // Change the message id from USSD request to USSI reuqest
    newUssiReqMsg = RfxMclMessage::obtainRequest(
            RFX_MSG_REQUEST_SEND_USSI,
            new RfxStringsData(newUssiReqStrings, 2),
            msg->getSlotId(),
            msg->getToken(),
            msg->getSendToMainProtocol(),
            msg->getRilToken(),
            msg->getTimeStamp(),
            msg->getAddAtFront());

    free(newUssiReqStrings[0]);
    free(newUssiReqStrings[1]);

    return newUssiReqMsg;
}

/**
 * Convert USSI request to USSD request back becasue something wrong during the USSI attempt
 */
sp<RfxMclMessage> RmcSuppServUssdDomainSelector::convertUssiToUssdReq(
        const sp<RfxMclMessage>& msg) {
    sp<RfxMclMessage> newUssdReqMsg;
    char *newUssdReqString;

    // USSD String
    asprintf(&newUssdReqString, "%s", ((char**) msg->getData()->getData())[1]);

    if (DBG) {
        logD(TAG, "convertUssiToUssdReq: ussd = %s", newUssdReqString);
    }

    // Change the message id from USSI request to USSD reuqest
    newUssdReqMsg = RfxMclMessage::obtainRequest(
            RFX_MSG_REQUEST_SEND_USSD,
            new RfxStringData((void *) newUssdReqString, strlen(newUssdReqString)),
            msg->getSlotId(),
            msg->getToken(),
            msg->getSendToMainProtocol(),
            msg->getRilToken(),
            msg->getTimeStamp(),
            msg->getAddAtFront());

    free(newUssdReqString);

    return newUssdReqMsg;
}

/**
 * The API is used for FDN check and emergnecy number approved.
 */
sp<RfxMclMessage> RmcSuppServUssdDomainSelector::convertUssiToUssdUrc(
        const sp<RfxMclMessage>& msg) {
    sp<RfxMclMessage> newUssdUrcMsg;
    char *newUssdUrcStrings[2];
    const char **ussiUrcStrings = (const char**) (msg->getData()->getData());

    /**
     * USSI response from the network, or network initiated operation
     * +EIUSD: <class>,<status>,<str>,<lang>,<error_code>,<alertingpattern>,<sip_cause>
     * <m>:
     *    1   USSD notify
     *    2   SS notify
     *    3   MD execute result
     * <n>:  if m=1
     *    0   no further user action required
     *    1   further user action required
     *    2   USSD terminated by network
     *    3   other local client has responded
     *    4   operation not supported
     *    5    network time out
     * <n>:if m=3, value is return value of MD
     *    0   execute success
     *    1   common error
     *    2   IMS unregistered
     *    3   IMS busy
     *    4   NW error response
     *    5   session not exist
     *    6   NW not support(404)
     * <str>: USSD/SS string
     * <lang>: USSD language
     * <error_code> USSD error code in xml
     * <alertingpattern> alerting pattern of NW initiated INVITE
     * <sip_cause> sip error code
     */
    const char *m = ussiUrcStrings[0];
    const char *n = ussiUrcStrings[1];

    if (DBG) {
        logD(TAG, "convertUssiToUssdUrc: m = %s, n = %s", m, n);
    }

    // Initialization
    setUssiAction(USSI_REQUEST);  // Every time UE receives an USSI URC from modem,
                                  // we recover the state of mUssiAction to USSI_REQUEST.
                                  // If we found the EIUSD URC is "further user action required",
                                  // then change it to USSI_RESPONSE.
    asprintf(&newUssdUrcStrings[0], "%d", 4);  // Set USSD mode as general USSD error (4) first,
                                               // e.g., not supported, time out, network error

    // USSD mode
    if (strcmp(m, "1") == 0) {
        if (strcmp(n, "0") == 0 || strcmp(n, "2") == 0) {
            asprintf(&newUssdUrcStrings[0], "%d", 0);  // USSD_MODE_NOTIFY (0)
                                                       // <m> = 1, <n> = 0, no further action
                                                       // <m> = 1, <n> = 2, terminated by network
        } else if (strcmp(n, "1") == 0) {
            asprintf(&newUssdUrcStrings[0], "%d", 1);  // USSD_MODE_REQUEST (1)
                                                       // <m> = 1, <n> = 1, further action required
            setUssiAction(USSI_RESPONSE);  // Pull up the flag to let USSD domain selector
                                           // know the action of "next" received USSD
                                           // is a response, not a request
        } else if (strcmp(n, "5") == 0) {
            asprintf(&newUssdUrcStrings[0], "%d", 5);  // USSD_MODE_NW_TIMEOUT (5)
                                                       // <m> = 1, <n> = 5, netowrk time out
            setUssiAction(USSI_REQUEST);   // Reset the flag to let USSD domain selector
                                           // know the action of "next" received USSD
                                           // is a request, not a response. Because the current
                                           // USSI session is over
        }
    } else if (strcmp(m, "3") == 0) {
        if (strcmp(n, "0") == 0) {
            asprintf(&newUssdUrcStrings[0], "%d", 0);  // USSD_MODE_NOTIFY (0)
                                                       // <m> = 3, <n> = 5, execute success
        }
    }

    // USSD string
    asprintf(&newUssdUrcStrings[1], "%s", ussiUrcStrings[2]);

    if (DBG) {
        logD(TAG, "convertUssiToUssdUrc: return ussdMode = %s", newUssdUrcStrings[0]);
    }

    // Change the message id from USSI URC to USSD URC
    newUssdUrcMsg = RfxMclMessage::obtainUrc(RFX_MSG_UNSOL_ON_USSD, m_slot_id,
            RfxStringsData(newUssdUrcStrings, 2));

    free(newUssdUrcStrings[0]);
    free(newUssdUrcStrings[1]);

    return newUssdUrcMsg;
}

/**
 * The API is used for FDN check and emergnecy number approved.
 */
bool RmcSuppServUssdDomainSelector::isFdnAllowed(const char* ussi) {
    sp<RfxAtResponse> p_response;
    int err;
    char* cmd = NULL;
    RfxAtLine *line;
    RIL_Errno ret = RIL_E_GENERIC_FAILURE;
    int responses[2] = {0};

    if (ussi == NULL || strlen(ussi) == 0) {
        logE(TAG, "isFdnAllowed: Null parameters.");
        goto error;
    }

    /**
     * AT+EAPPROVE=<dial_number>
     * <dial_number>: string, dialing number.
     */
    asprintf(&cmd, "AT+EAPPROVE=\"%s\"", ussi);
    p_response = atSendCommandSingleline(cmd, "+EAPPROVE:");
    free(cmd);

    err = p_response->getError();
    if (err < 0 || p_response == NULL) {
        logE(TAG, "isFdnAllowed Fail");
        goto error;
    }

    switch (p_response->atGetCmeError()) {
        case CME_SUCCESS:
            break;
        default:     // AT CMD format error, should not be here
            goto error;
    }

    if (p_response->getIntermediates() != NULL) {
        line = p_response->getIntermediates();
        line->atTokStart(&err);
        if (err < 0) {
            goto error;
        }

        /**
         * <is_allowed> : integer
         * 0   The number is not allowed
         * 1   The number is allowed
         */
        responses[0] = line->atTokNextint(&err);
        if (err < 0) {
            goto error;
        }

        /**
         * <is_emergency> : integer
         * 0   The number is not emergnecy number
         * 1   The number is emergency number
         */
        responses[1] = line->atTokNextint(&err);
        if (err < 0) {
            goto error;
        }
    }

    ret = RIL_E_SUCCESS;

error:
    if (ret == RIL_E_SUCCESS) {
        if (responses[1] == 0) {       // not an ECC number, determined by MD PHB
            return (responses[0] == 1);
        } else if (responses[1] == 1){ // Approve if it is an ECC number
            return true;
        }
    }
    return true;                       // Approve if we get any kind of CME error
}

/**
 * The API is used for IMS VoPS check.
 */
bool RmcSuppServUssdDomainSelector::isVopsOn() {
    sp<RfxAtResponse> p_response;
    int err;
    RfxAtLine *line;
    RIL_Errno ret = RIL_E_GENERIC_FAILURE;
    int responses[2] = {0};

    /**
     * Query IMS network reporting
     * AT+CIREP?
     * +CIREP: <reporting>,<nwimsvops>
     */
    p_response = atSendCommandSingleline("AT+CIREP?", "+CIREP:");

    err = p_response->getError();
    if (err < 0 || p_response == NULL) {
        logE(TAG, "isVopsOn Fail");
        goto error;
    }

    switch (p_response->atGetCmeError()) {
        case CME_SUCCESS:
            break;
        default:     // AT CMD format error, should not be here
            goto error;
    }

    if (p_response->getIntermediates() != NULL) {
        line = p_response->getIntermediates();
        line->atTokStart(&err);
        if (err < 0) {
            goto error;
        }

        /**
         * <reporting> : integer
         * Enables or disables reporting of changes in the IMSVOPS supported
         * indication received from the network and reporting of PS to CS SRVCC,
         * PS to CS vSRVCC and CS to PS SRVCC handover information.
         *
         * 0   Disable reporting
         * 1   Enable reporting
         */
        responses[0] = line->atTokNextint(&err);
        if (err < 0) {
            goto error;
        }

        /**
         * <nwimsvops> : integer
         * Gives the last IMS Voice Over PS sessions (IMSVOPS) supported
         * indication received from network.
         *
         * 0   IMSVOPS support indication is not received from network, or is negative
         * 1   IMSVOPS support indication as received from network is positive
         */
        responses[1] = line->atTokNextint(&err);
        if (err < 0) {
            goto error;
        }
    }

    ret = RIL_E_SUCCESS;

error:
    if (ret == RIL_E_SUCCESS) {
        return (responses[1] == 1);
    }
    return true;    // Assume VoPS is on by default
}

/**
 * The API is used for IMS registration status check.
 */
bool RmcSuppServUssdDomainSelector::isImsRegOn() {
    sp<RfxAtResponse> p_response;
    RfxAtLine *line;
    RIL_Errno ret = RIL_E_GENERIC_FAILURE;
    int err;
    int skip;
    int response[2] = {0};


    /**
     * Query IMS registration information
     * AT+CIREG?
     * +CIREG: <n>,<reg_info>[,<ext_info>]
     */
    p_response = atSendCommandSingleline("AT+CIREG?", "+CIREG:");

    err = p_response->getError();
    if (err != 0 ||
            p_response == NULL ||
            p_response->getSuccess() == 0 ||
            p_response->getIntermediates() == NULL) {
        logE(TAG, "isImsRegOn reg_info Fail");
        goto error;
    }

    // handle intermediate
    line = p_response->getIntermediates();

    // go to start position
    line->atTokStart(&err);
    if (err < 0) {
        goto error;
    }

    // go to start position
    line->atTokStart(&err);
    if (err < 0) goto error;

    // <mode>
    skip = line->atTokNextint(&err);
    if (err < 0 || skip < 0 ) {
        logE(TAG, "The <mode> is an invalid value!!!");
        goto error;
    } else {
        /**
         * <reg_info> : integer
         * Indicates the IMS registration status. The UE is seen as registered as long as
         * one or more of its public user identities are registered with any of its
         * contact addresses
         *
         * 0   not registered
         * 1   registered
         */
        response[0] = line->atTokNextint(&err);
        if (err < 0 ) goto error;

        response[1] = 1; // RADIO_TECH_3GPP
    }

    return (response[0] == 1);

error:
    logE(TAG, "There is something wrong with the AT+CIREG?, return false for isImsRegOn()");
    return false;   // Assume no IMS registration by default
}

UssiAction RmcSuppServUssdDomainSelector::getUssiAction() {
    logD(TAG, "getUssiAction(): mUssiAction = %s", ussiActionToString(mUssiAction));
    return mUssiAction;
}

void RmcSuppServUssdDomainSelector::setUssiAction(UssiAction action) {
    logD(TAG, "setUssiAction(): %s -> %s", ussiActionToString(mUssiAction),
            ussiActionToString(action));
    if (mUssiAction == action) {
        return;
    }
    mUssiAction = action;
}

const char *RmcSuppServUssdDomainSelector::ussiActionToString(UssiAction action) {
    switch (action) {
        case USSI_REQUEST:
            return "USSI_REQUEST";
        case USSI_RESPONSE:
            return "USSI_RESPONSE";
        default:
            // not possible here!
            return NULL;
    }
}

bool RmcSuppServUssdDomainSelector::isWithoutUssiFramework() {
    char r[MTK_PROPERTY_VALUE_MAX];

    // If the current project does not have USSI Java Framework, then need to set
    // default value to "1"
    mtk_property_get(PROPERTY_WITHOUT_USSI_FRAMEWORK, r, "0");

    return (atoi(r) == 1);
}
