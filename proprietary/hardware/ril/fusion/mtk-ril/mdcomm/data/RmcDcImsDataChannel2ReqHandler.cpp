/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include <memory>
#include <regex>
#include <string>
#include "RfxVoidData.h"
#include "RfxStringData.h"
#include "RfxIntsData.h"
#include "RfxMessageId.h"
#include "RfxImsBearerNotifyData.h"
#include "RfxImsDataInfoNotifyData.h"
#include "RmcDcUtility.h"
#include "RmcDcImsDataChannel2ReqHandler.h"

#define RFX_LOG_TAG "RmcDcImsDc2ReqHandler"

/*****************************************************************************
 * Class RmcDcImsDataChannel2ReqHandler
 * Handle RIL request on DATA2 (RIL_CMD_PROXY_10) channel
 *****************************************************************************/
RFX_IMPLEMENT_HANDLER_CLASS(RmcDcImsDataChannel2ReqHandler, RIL_CMD_PROXY_10);

RFX_REGISTER_DATA_TO_URC_ID(RfxImsBearerNotifyData, RFX_MSG_URC_IMS_BEARER_ACTIVATION);
RFX_REGISTER_DATA_TO_URC_ID(RfxImsBearerNotifyData, RFX_MSG_URC_IMS_BEARER_DEACTIVATION);
RFX_REGISTER_DATA_TO_URC_ID(RfxIntsData, RFX_MSG_URC_IMS_BEARER_INIT);
RFX_REGISTER_DATA_TO_URC_ID(RfxImsDataInfoNotifyData, RFX_MSG_URC_IMS_DATA_INFO_NOTIFY);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_IMS_BEARER_ACTIVATION_DONE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_IMS_BEARER_DEACTIVATION_DONE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_SET_IMS_BEARER_NOTIFICATION);
RFX_REGISTER_DATA_TO_EVENT_ID(RfxVoidData, RFX_MSG_EVENT_IMS_REQUEST_HANDLER_REGISTER_DONE);


RmcDcImsDataChannel2ReqHandler::RmcDcImsDataChannel2ReqHandler(int slot_id, int channel_id) :
    RfxBaseHandler(slot_id, channel_id) {

    RFX_LOG_D(RFX_LOG_TAG, "RmcDcImsDataChannel2ReqHandler Ctor, slot_id=%d, channel_id=%d", slot_id, channel_id);

    const int requestList[] = {
        RFX_MSG_REQUEST_IMS_BEARER_ACTIVATION_DONE,
        RFX_MSG_REQUEST_IMS_BEARER_DEACTIVATION_DONE,
        RFX_MSG_REQUEST_SET_IMS_BEARER_NOTIFICATION,
    };

    const int eventList[] = {
        RFX_MSG_EVENT_URC_HANDLER_REGISTER_DONE,
        RFX_MSG_EVENT_DATA_IMS_PDN_NOTIFY,
        RFX_MSG_EVENT_DATA_PCSCF_ADDRESS_DISCOVERY,
    };

    registerToHandleRequest(requestList, sizeof(requestList) / sizeof(int));
    registerToHandleEvent(eventList, sizeof(eventList) / sizeof(int));
    sendEvent(RFX_MSG_EVENT_IMS_REQUEST_HANDLER_REGISTER_DONE, RfxVoidData(),
            RIL_CMD_PROXY_URC, m_slot_id);
}

RmcDcImsDataChannel2ReqHandler::~RmcDcImsDataChannel2ReqHandler() {
}

void RmcDcImsDataChannel2ReqHandler::onHandleRequest(const sp<RfxMclMessage>& msg) {
    switch (msg->getId()) {
        case RFX_MSG_REQUEST_IMS_BEARER_ACTIVATION_DONE:
            handleImsBearerActivationDone(msg);
            break;
        case RFX_MSG_REQUEST_IMS_BEARER_DEACTIVATION_DONE:
            handleImsBearerDeactivationDone(msg);
            break;
         case RFX_MSG_REQUEST_SET_IMS_BEARER_NOTIFICATION:
            handleImsBearerNotificationSet(msg);
            break;
        default:
            RFX_LOG_D(RFX_LOG_TAG, "unknown request, ignore!");
            break;
    }
}

void RmcDcImsDataChannel2ReqHandler::onHandleEvent(const sp<RfxMclMessage>& msg) {
    switch (msg->getId()) {
        case RFX_MSG_EVENT_URC_HANDLER_REGISTER_DONE:
            handleRegisterUrcDone();
            break;
        case RFX_MSG_EVENT_DATA_IMS_PDN_NOTIFY:
            handleImsBearerNotify(msg);
            break;
        case RFX_MSG_EVENT_DATA_PCSCF_ADDRESS_DISCOVERY:
            handlePcscfAddressDiscovery(msg);
            break;
        default:
            RFX_LOG_E(RFX_LOG_TAG, "should not be here");
            break;
    }
}


/*****************************************************************************
 * REQUEST HANDLER
 *****************************************************************************/

void RmcDcImsDataChannel2ReqHandler::notifyImsBearerRequest(int requestId, RIL_IMS_BearerNotification* notification) {
    //RFX_LOG_D(RFX_LOG_TAG, "%s", __FUNCTION__);
    if(isEnableBearerNotification) {
        sp<RfxMclMessage> urc_to_tel_core = RfxMclMessage::obtainUrc(requestId,
            m_slot_id, RfxImsBearerNotifyData((void*)notification, sizeof(RIL_IMS_BearerNotification)));
        responseToTelCore(urc_to_tel_core);
        if(requestId == RFX_MSG_URC_IMS_BEARER_ACTIVATION){
            getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_ACT_AID,notification->aid);
        }else if(requestId == RFX_MSG_URC_IMS_BEARER_DEACTIVATION){
            getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_DEACT_AID,notification->aid);
        }
    } else {
        int aid = notification->aid;
        int err = RIL_E_SUCCESS;
        int rid = m_slot_id;
        sp<RfxAtResponse> p_response;
        String8 cmd = String8::format("AT+EIMSPDN= \"confirm\", %d, %d", aid, err);
        RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] send %s", rid, __FUNCTION__, cmd.string());
        p_response = atSendCommand(cmd);
        if (p_response->isAtResponseFail()) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s], %s returns ERROR: %d", rid, __FUNCTION__,
                cmd.string(), p_response->getError());
        }
    }
}

void RmcDcImsDataChannel2ReqHandler::notifyImsDataInfo(RIL_IMS_DataInfoNotify* notification) {
    RFX_LOG_D(RFX_LOG_TAG, "%s", __FUNCTION__);

    sp<RfxMclMessage> urc_to_tel_core = RfxMclMessage::obtainUrc(RFX_MSG_URC_IMS_DATA_INFO_NOTIFY,
        m_slot_id, RfxImsDataInfoNotifyData((void*)notification, sizeof(RIL_IMS_DataInfoNotify)));
    responseToTelCore(urc_to_tel_core);
}

// AT+EIMSPDN activation confirm to DDM
void RmcDcImsDataChannel2ReqHandler::handleImsBearerActivationDone(const sp<RfxMclMessage>& msg) {
    //RFX_LOG_D(RFX_LOG_TAG, "handleImsBearerActivationDone");

    const int *pReqData = (const int*)msg->getData()->getData();
    int aid = pReqData[0];
    int err = pReqData[1];
    int rid = m_slot_id;
    sp<RfxAtResponse> p_response;
    sp<RfxMclMessage> responseMsg;

    String8 cmd = String8::format("AT+EIMSPDN= \"confirm\", %d, %d", aid, err);
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] send %s", rid, __FUNCTION__, cmd.string());
    p_response = atSendCommand(cmd);
    if (p_response->isAtResponseFail()) {
        RFX_LOG_E(RFX_LOG_TAG, "[%d][%s], %s returns ERROR: %d", rid, __FUNCTION__,
            cmd.string(), p_response->getError());
    }
    responseMsg = RfxMclMessage::obtainResponse(RIL_E_SUCCESS, RfxVoidData(), msg);
    responseToTelCore(responseMsg);
    getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_ACT_AID,RFX_NO_ONGOING_IMS_PDN_AID);
}

// AT+EIMSPDN deactivation confirm to DDM
void RmcDcImsDataChannel2ReqHandler::handleImsBearerDeactivationDone(const sp<RfxMclMessage>& msg) {
    //RFX_LOG_D(RFX_LOG_TAG, "handleImsBearerDeactivationDone");

    const int *pReqData = (const int*)msg->getData()->getData();
    int aid = pReqData[0];
    int err = pReqData[1];
    int rid = m_slot_id;
    sp<RfxAtResponse> p_response;
    sp<RfxMclMessage> responseMsg;

    String8 cmd = String8::format("AT+EIMSPDN= \"confirm\", %d, %d", aid, err);
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] send %s", rid, __FUNCTION__, cmd.string());
    p_response = atSendCommand(cmd);
    if (p_response->isAtResponseFail()) {
        RFX_LOG_E(RFX_LOG_TAG, "[%d][%s], %s returns ERROR: %d", rid, __FUNCTION__,
            cmd.string(), p_response->getError());
    }
    responseMsg = RfxMclMessage::obtainResponse(RIL_E_SUCCESS, RfxVoidData(), msg);
    responseToTelCore(responseMsg);
    getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_DEACT_AID,RFX_NO_ONGOING_IMS_PDN_AID);
}

/*****************************************************************************
 * EVENT HANDLER
 *****************************************************************************/

// Callback function when RmcDcUrcHandler URC registration ready
void RmcDcImsDataChannel2ReqHandler::handleRegisterUrcDone() {
    //RFX_LOG_D(RFX_LOG_TAG, "handleRegisterUrcDone");

    sp<RfxAtResponse> p_response;
    String8 cmd("");
    if (RmcDcUtility::isImsSupport()) {
        cmd.append(String8::format("AT+EIMSPDN= \"onoff\", 1"));
    } else {
        cmd.append(String8::format("AT+EIMSPDN= \"onoff\", 0"));
    }
    RFX_LOG_D(RFX_LOG_TAG, "[%d][onRegisterUrcDone], send %s", m_slot_id, cmd.string());
    p_response = atSendCommand(cmd);
    if (p_response->isAtResponseFail()) {
        RFX_LOG_E(RFX_LOG_TAG, "%s returns ERROR", cmd.string());
    }

}

// URC +EIMSPDN "notify" URC, msg is RfxStringData format
void RmcDcImsDataChannel2ReqHandler::handleImsBearerNotify(const sp<RfxMclMessage>& msg) {
        //RFX_LOG_D(RFX_LOG_TAG, "handleImsBearerNotify");

        //+EIMSPDN: <cmd>, <aid>, <state>, <type>
        //<cmd> :
        //       "notify" -> MD notify AP to construct IMS PDN
        static int ACTION_IMS_BEARER_DEACTIVATION = 0;
        static int ACTION_IMS_BEARER_ACTIVATION = 1;
        int action = -1;

        char *urc = (char*)msg->getData()->getData();
        int rid = m_slot_id;
        int err = 0;
        char *cmdFormat = NULL;
        std::unique_ptr<RIL_IMS_BearerNotification> notification(new RIL_IMS_BearerNotification());
        std::unique_ptr<RfxAtLine> pLine(new RfxAtLine(urc, NULL));
        RFX_LOG_I(RFX_LOG_TAG, "[%d][%s] urc=%s", rid, __FUNCTION__, urc);
        pLine->atTokStart(&err);
        if (err < 0) return;

        cmdFormat = pLine->atTokNextstr(&err);
        if (err < 0) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing cmd",
                    rid, __FUNCTION__);
            return;
        }
        if (strncmp("notify", cmdFormat, strlen("notify")) == 0) {
            int aid = -1;
            int state = -1;
            char *type = NULL;

            aid = pLine->atTokNextint(&err);
            if (err < 0) {
                RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing aid",
                        rid, __FUNCTION__);
                return;
            }

            action = pLine->atTokNextint(&err);
            if (err < 0) {
                RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing state",
                        rid, __FUNCTION__);
                return;
            }

            type = pLine->atTokNextstr(&err);
            if (err < 0) {
                RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing type",
                        rid, __FUNCTION__);
                return;
            }

            notification->phone = m_slot_id;
            notification->aid = aid;
            notification->type = type;
            RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] IMS notification phone=%d, aid=%d, type=%s, action = %d",
                        rid, __FUNCTION__, notification->phone, notification->aid, notification->type, action);

        // Emergency PDN shall be always sync to FWK to meet AGPS IR.92 requirement.
#if 0
            if (needIgnoreImsBearerNotify(action, type)) {
                confirmImsBearerNotify(aid, err);
                return;
            }
#endif

            if (action == ACTION_IMS_BEARER_ACTIVATION) {
                notifyImsBearerRequest(RFX_MSG_URC_IMS_BEARER_ACTIVATION, notification.get());
            } else if (action == ACTION_IMS_BEARER_DEACTIVATION) {
                notifyImsBearerRequest(RFX_MSG_URC_IMS_BEARER_DEACTIVATION, notification.get());
            }
        } else if(strncmp("init", cmdFormat, strlen("init")) == 0) {

            RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] IMS notification phone=%d bearer initial...",
                    rid, __FUNCTION__, m_slot_id);
            sp<RfxMclMessage> urc_to_tel_core = RfxMclMessage::obtainUrc(
                    RFX_MSG_URC_IMS_BEARER_INIT, m_slot_id,
                    RfxIntsData((void*)&m_slot_id, sizeof(int)));
            responseToTelCore(urc_to_tel_core);
            getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_ACT_AID,RFX_NO_ONGOING_IMS_PDN_AID);
            getMclStatusManager()->setIntValue(RFX_STATUS_KEY_IMS_PDN_DEACT_AID,RFX_NO_ONGOING_IMS_PDN_AID);
        }else if(strncmp("reject", cmdFormat, strlen("reject")) == 0){
            int aid = -1;
            int cause = -1;
            char *type = NULL;
            char *strEvent = NULL;
            char *strCause = NULL;

            std::unique_ptr<RIL_IMS_DataInfoNotify> notification(new RIL_IMS_DataInfoNotify());

            // Notify Clear Codes 33/29 event for IMS PDN activation
            //+EIMSPDN: "reject", <cause> , <type>
            //<cause> : 29 or 33
            //<type> : "ims" or "emergency"

            cause = pLine->atTokNextint(&err);
            if (err < 0) {
                RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing cause",
                        rid, __FUNCTION__);
                return;
            }

            type = pLine->atTokNextstr(&err);
            if (err < 0) {
                RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing type",
                        rid, __FUNCTION__);
                return;
            }

            asprintf(&strEvent, "ClearCodes");
            asprintf(&strCause, "%d", cause);
            notification->phone = m_slot_id;
            notification->type = type;
            notification->event = strEvent;
            notification->extra = strCause;
            notifyImsDataInfo(notification.get());
            if(strEvent != NULL) free(strEvent);
            if(strCause != NULL) free(strCause);
        }
}

void RmcDcImsDataChannel2ReqHandler::handleImsBearerNotificationSet(const sp<RfxMclMessage>& msg) {
    const int *pReqData = (const int*)msg->getData()->getData();
    int enable = pReqData[0];
    int rid = m_slot_id;
    sp<RfxAtResponse> p_response;
    sp<RfxMclMessage> responseMsg;

    isEnableBearerNotification = enable;
    RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] isEnableBearerNotification: %d", rid, __FUNCTION__, isEnableBearerNotification);
    responseMsg = RfxMclMessage::obtainResponse(RIL_E_SUCCESS, RfxVoidData(), msg);
    responseToTelCore(responseMsg);
}


// P-CSCF discovery, not support now, msg is RfxStringData format
void RmcDcImsDataChannel2ReqHandler::handlePcscfAddressDiscovery(const sp<RfxMclMessage>& msg) {
        //RFX_LOG_D(RFX_LOG_TAG, "handlePcscfAddressDiscovery");

        //+EIMSPDIS:  <transaction_id>,<em_ind>,<method>, <nw_if_name[]>
        //AT+EIMSPCSCF= <transaction_id>,<method>, <protocol_type>, <port_num>, <addr>
        //AT+EIMSPDIS= <transaction_id>, <method>, <is_success>
        char *urc = (char*)msg->getData()->getData();
        int rid = m_slot_id;
        int err = 0;
        int tranid = -1;
        int em_ind = -1;
        int method = -1;
        char *interfaceId = NULL;
        sp<RfxAtResponse> p_response;
        String8 pcscfCmd;

        RfxAtLine *pLine = new RfxAtLine(urc, NULL);
        RFX_LOG_I(RFX_LOG_TAG, "[%d][%s] urc=%s", rid, __FUNCTION__, urc);

        pLine->atTokStart(&err);
        if (err < 0) goto error;

        tranid = pLine->atTokNextint(&err);
        if (err < 0) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing tranid",
                    rid, __FUNCTION__);
            goto error;
        }

        em_ind = pLine->atTokNextint(&err);
        if (err < 0) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing em_ind",
                    rid, __FUNCTION__);
            goto error;
        }

        method = pLine->atTokNextint(&err);
        if (err < 0) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing method",
                    rid, __FUNCTION__);
            goto error;
        }

        interfaceId = pLine->atTokNextstr(&err);
        if (err < 0) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] ERROR occurs when parsing interfaceId",
                    rid, __FUNCTION__);
            goto error;
        }

        RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] pcscf discovery tranid=%d, em_ind=%d, method = %d,ifaceId=%s",
                rid, __FUNCTION__, tranid, em_ind, method, interfaceId);

        //Current not support pcscf discovery, so always response fail to MD.
        pcscfCmd = String8::format("AT+EIMSPDIS= %d, %d, 0", tranid, method);
        //RFX_LOG_D(RFX_LOG_TAG, "[%d][%s], rid, __FUNCTION__, send %s", rid, __FUNCTION__, pcscfCmd.string());
        p_response = atSendCommand(pcscfCmd);
        if (p_response->isAtResponseFail()) {
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s], %s returns ERROR", rid, __FUNCTION__, pcscfCmd.string());
        }

    error:
        AT_LINE_FREE(pLine);
        return;

}
