/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include "RtcDataController.h"
#include "RtcDataUtils.h"
#include "ims/RtcImsController.h"
#include "RfxRilUtils.h"
#include <string>
#include <stdlib.h>


#define RFX_LOG_TAG "RtcDC"

/// Temp data setting/Data switch setting from OEM @{
#define RTC_TEMP_DATA_SETTING_STATUS  "TEMP_DATA_SETTING_SYNC"
#define RTC_DATA_SETTING_STATUS  "DATA_STATUS_SYNC"
#define RTC_DATA_SETTING_NUM     3
/// @}
/// FastDormancy status that sync from EM. @{
#define RTC_EM_FASTDORMANCY_SYNC  "EM_FASTDORMANCY_SYNC"
#define RTC_EM_FASTDORMANCY_TIMER_LENGTH 3
#define RTC_EM_FASTDORMANCY_TIMER_ARGUMENT_LENGTH 4
/// @}

/*****************************************************************************
 * Class RtcDataController
 *****************************************************************************/
RFX_IMPLEMENT_CLASS("RtcDataController", RtcDataController, RfxController);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxStringsData, RfxDataCallResponseData, RFX_MSG_REQUEST_SETUP_DATA_CALL);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxStringsData, RfxVoidData, RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxIntsData, RFX_MSG_REQUEST_LAST_DATA_CALL_FAIL_CAUSE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxDataCallResponseData, RFX_MSG_REQUEST_DATA_CALL_LIST);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxSetDataProfileData, RfxVoidData, RFX_MSG_REQUEST_SET_DATA_PROFILE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxStringData, RfxVoidData, RFX_MSG_REQUEST_RESET_MD_DATA_RETRY_COUNT);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIaApnData, RfxVoidData, RFX_MSG_REQUEST_SET_INITIAL_ATTACH_APN);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxLceStatusResponseData, RFX_MSG_REQUEST_START_LCE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxLceStatusResponseData, RFX_MSG_REQUEST_STOP_LCE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxLceDataResponseData, RFX_MSG_REQUEST_PULL_LCEDATA);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_SET_LTE_ACCESS_STRATUM_REPORT);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxIntsData, RfxVoidData, RFX_MSG_REQUEST_SET_LTE_UPLINK_DATA_TRANSFER);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxFdModeData, RfxVoidData, RFX_MSG_REQUEST_SET_FD_MODE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxLinkCapacityReportingCriteriaData, RfxVoidData, RFX_MSG_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA);
RFX_REGISTER_DATA_TO_URC_ID(RfxDataCallResponseData, RFX_MSG_URC_DATA_CALL_LIST_CHANGED);
RFX_REGISTER_DATA_TO_URC_ID(RfxLceDataResponseData, RFX_MSG_URC_LCEDATA_RECV);
RFX_REGISTER_DATA_TO_URC_ID(RfxIntsData, RFX_MSG_URC_LTE_ACCESS_STRATUM_STATE_CHANGE);
RFX_REGISTER_DATA_TO_URC_ID(RfxVoidData, RFX_MSG_URC_MD_DATA_RETRY_COUNT_RESET);
RFX_REGISTER_DATA_TO_URC_ID(RfxLinkCapacityEstimateData, RFX_MSG_URC_LINK_CAPACITY_ESTIMATE);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxVoidData, RFX_MSG_REQUEST_RESET_ALL_CONNECTIONS);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxSetDataProfileData, RfxVoidData, RFX_MSG_REQUEST_SET_DATA_PROFILE_EX);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxVoidData, RfxVoidData, RFX_MSG_REQUEST_RESET_APN_TABLE_EX);

RtcDataController::RtcDataController() :
    isUnderCapabilitySwitch(false),
    requestTokenIdForDisableIms(INVALID_VALUE),
    transIdForDisableIms(INVALID_VALUE),
    mSimIccid(String8("")) {
}

RtcDataController::~RtcDataController() {
}

void RtcDataController::onInit() {
    RfxController::onInit();  // Required: invoke super class implementation
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] enter", m_slot_id, __FUNCTION__);

    int modemOffState = getNonSlotScopeStatusManager()->getIntValue(
        RFX_STATUS_KEY_MODEM_OFF_STATE, MODEM_OFF_IN_IDLE);
    isUnderCapabilitySwitch = (modemOffState == MODEM_OFF_BY_SIM_SWITCH) ? true : false;

    const int request_id_list[] = {
        RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD,
        RFX_MSG_REQUEST_RESET_MD_DATA_RETRY_COUNT,
        RFX_MSG_REQUEST_START_LCE,
        RFX_MSG_REQUEST_STOP_LCE,
        RFX_MSG_REQUEST_PULL_LCEDATA,
        RFX_MSG_REQUEST_SETUP_DATA_CALL,
        RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL,
        RFX_MSG_REQUEST_DATA_CALL_LIST,
        RFX_MSG_REQUEST_LAST_DATA_CALL_FAIL_CAUSE,
        RFX_MSG_REQUEST_SET_DATA_PROFILE,
        RFX_MSG_REQUEST_SET_INITIAL_ATTACH_APN,
        RFX_MSG_REQUEST_SET_LTE_ACCESS_STRATUM_REPORT,
        RFX_MSG_REQUEST_SET_LTE_UPLINK_DATA_TRANSFER,
        RFX_MSG_REQUEST_SET_FD_MODE,
        RFX_MSG_REQUEST_RESET_ALL_CONNECTIONS,
        RFX_MSG_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA,
        RFX_MSG_REQUEST_SET_DATA_PROFILE_EX,
        RFX_MSG_REQUEST_RESET_APN_TABLE_EX,
    };

    registerToHandleRequest(request_id_list,
            sizeof(request_id_list) / sizeof(const int));

    registerForStatusChange();
}

void RtcDataController::registerForStatusChange() {
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] enter", m_slot_id, __FUNCTION__);
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_WORLD_MODE_STATE,
        RfxStatusChangeCallback(this, &RtcDataController::onWorldModeStateChanged));

    getNonSlotScopeStatusManager()->registerStatusChanged(RFX_STATUS_KEY_MODEM_OFF_STATE,
        RfxStatusChangeCallback(this, &RtcDataController::onModemOffStateChanged));

    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_UICC_GSM_NUMERIC,
            RfxStatusChangeCallback(this, &RtcDataController::onUiccMccMncChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_UICC_CDMA_NUMERIC,
            RfxStatusChangeCallback(this, &RtcDataController::onUiccMccMncChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_GSM_SPN,
            RfxStatusChangeCallback(this, &RtcDataController::onSpnChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_CDMA_SPN,
            RfxStatusChangeCallback(this, &RtcDataController::onSpnChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_GSM_IMSI,
            RfxStatusChangeCallback(this, &RtcDataController::onImsiChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_C2K_IMSI,
            RfxStatusChangeCallback(this, &RtcDataController::onImsiChanged));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_GSM_GID1,
            RfxStatusChangeCallback(this, &RtcDataController::onGid1Changed));
    getStatusManager()->registerStatusChanged(RFX_STATUS_KEY_GSM_PNN,
            RfxStatusChangeCallback(this, &RtcDataController::onPnnChanged));

    /// Sync data setting from OEM @{
    // Register data setting status change from OEM hook string and handle sync
    // data setting to modem
    getStatusManager()->registerStatusChanged(
                RFX_STATUS_KEY_TELEPHONY_ASSISTANT_STATUS,
                RfxStatusChangeCallback(this, &RtcDataController::onDataSettingStatusChanged));
    /// @}

    getStatusManager(m_slot_id)->registerStatusChangedEx(RFX_STATUS_KEY_SIM_ICCID,
            RfxStatusChangeCallbackEx(this, &RtcDataController::onSimIccidChanged));
}

void RtcDataController::onWorldModeStateChanged(RfxStatusKeyEnum key,
    RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    int newValue = value.asInt();
    int oldValue = old_value.asInt();
    RFX_LOG_I(RFX_LOG_TAG, "[%d][%s] old = %d, new = %d",
            m_slot_id, __FUNCTION__, oldValue, newValue);
    if (newValue == WORLD_MODE_SWITCHING) {
        sp<RfxMessage> reqToRild = RfxMessage::obtainRequest(m_slot_id,
                RFX_MSG_REQUEST_CLEAR_ALL_PDN_INFO, RfxVoidData());
        requestToMcl(reqToRild);
    }
}

void RtcDataController::onModemOffStateChanged(RfxStatusKeyEnum key,
    RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    int newValue = value.asInt();
    int oldValue = old_value.asInt();
    if (newValue == MODEM_OFF_BY_SIM_SWITCH) {
        RFX_LOG_I(RFX_LOG_TAG, "[%d][%s] Enter Sim switch state", m_slot_id, __FUNCTION__);
        isUnderCapabilitySwitch = true;
    } else {
        if (isUnderCapabilitySwitch) {
            RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] Leave Sim switch state", m_slot_id, __FUNCTION__);
            char no_reset_support[RFX_PROPERTY_VALUE_MAX] = { 0 };
            rfx_property_get("vendor.ril.simswitch.no_reset_support", no_reset_support, "0");
            if (strcmp(no_reset_support, "1")==0) {
                sp<RfxMessage> reqToRild = RfxMessage::obtainRequest(m_slot_id,
                    RFX_MSG_REQUEST_RESEND_SYNC_DATA_SETTINGS_TO_MD, RfxVoidData());
                reqToRild->setAddAtFront(true);
                requestToMcl(reqToRild);
            }
        }
        isUnderCapabilitySwitch = false;
    }
}

void RtcDataController::onUiccMccMncChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strMccMnc = value.asString8();
    String8 mccMncKey("");

    if (!strMccMnc.isEmpty() && 0 == atoi(value.asString8().string())) {
        strMccMnc = String8::format("%d", atoi(value.asString8().string()));
    }

    RFX_LOG_D(RFX_LOG_TAG, "[%d]onUiccMccMncChanged: strMccMnc = %s",
            m_slot_id, strMccMnc.string());

    if (RFX_STATUS_KEY_UICC_GSM_NUMERIC == key) {
        mccMncKey.append("vendor.ril.data.gsm_mcc_mnc");
    } else {
        mccMncKey.append("vendor.ril.data.cdma_mcc_mnc");
    }
    mccMncKey.append(String8::format("%d", m_slot_id));
    rfx_property_set(mccMncKey, strMccMnc.string());
}

void RtcDataController::onSpnChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strSpn = value.asString8();
    String8 keySpn("");

    RFX_LOG_D(RFX_LOG_TAG, "[%d]onSpnChanged: strSpn = %s", m_slot_id, strSpn.string());

    if (RFX_STATUS_KEY_GSM_SPN == key) {
        keySpn.append("vendor.ril.data.gsm_spn");
    } else {
        keySpn.append("vendor.ril.data.cdma_spn");
    }
    keySpn.append(String8::format("%d", m_slot_id));
    rfx_property_set(keySpn, strSpn.string());
}

void RtcDataController::onImsiChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strImsi = value.asString8();
    String8 keyImsi("");

    RFX_LOG_D(RFX_LOG_TAG, "[%d]onImsiChanged: strImsi = %s", m_slot_id, strImsi.string());

    if (RFX_STATUS_KEY_GSM_IMSI == key) {
        keyImsi.append("vendor.ril.data.gsm_imsi");
    } else {
        keyImsi.append("vendor.ril.data.cdma_imsi");
    }
    keyImsi.append(String8::format("%d", m_slot_id));
    rfx_property_set(keyImsi, strImsi.string());
}

void RtcDataController::onGid1Changed(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strGid1 = value.asString8();

    RFX_LOG_D(RFX_LOG_TAG, "[%d]onGid1Changed: strGid1 = %s", m_slot_id, strGid1.string());

    String8 keyGid1("vendor.ril.data.gid1-");
    keyGid1.append(String8::format("%d", m_slot_id));
    rfx_property_set(keyGid1, strGid1.string());
}

void RtcDataController::onPnnChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strPnn = value.asString8();

    RFX_LOG_D(RFX_LOG_TAG, "[%d]onPnnChanged: strPnn = %s", m_slot_id, strPnn.string());

    String8 keyPnn("vendor.ril.data.pnn");
    keyPnn.append(String8::format("%d", m_slot_id));
    rfx_property_set(keyPnn, strPnn.string());
}

/// Sync data setting from OEM @{
void RtcDataController::onDataSettingStatusChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 strStatus = value.asString8();
    RFX_LOG_D(RFX_LOG_TAG, "[%d] onDataSettingStatusChanged: value = %s",
            m_slot_id, strStatus.string());
    if (strStatus.find(String8(RTC_DATA_SETTING_STATUS)) != -1) {
        if (RtcDataUtils::isSupportSyncDataSettingFromOem()) {
            // parse settings (ex: DATA_STATUS_SYNC:1,-2,0)
            int settings[RTC_DATA_SETTING_NUM] = {0};

            // pattern string DATA_STATUS_SYNC
            char *tempSetting = strtok((char *)strStatus.string(), ":,");
            int i = 0;
            while (tempSetting != NULL && i < RTC_DATA_SETTING_NUM) {
                tempSetting = strtok(NULL, ":,");
                if (tempSetting != NULL) {
                    settings[i++] = atoi(tempSetting);
                }
            }

            RFX_LOG_D(RFX_LOG_TAG, "[%d] onDataSettingStatusChanged: settings = %d,%d,%d",
                    m_slot_id, settings[0], settings[1], settings[2]);

            sp<RfxMessage> reqToRild = RfxMessage::obtainRequest(m_slot_id,
                    RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD,
                    RfxIntsData(settings, RTC_DATA_SETTING_NUM));
            handleSyncDataSettingsToMD(reqToRild);
        }
    } else if (strStatus.find(String8(RTC_TEMP_DATA_SETTING_STATUS)) != -1) {
        if (RtcDataUtils::isSupportTempDataSwitchFromOem()) {
            int tempDataSetting = atoi(strStatus.string() + strlen(strStatus.string()) - 1);
            RFX_LOG_D(RFX_LOG_TAG, "onDataSettingStatusChanged: tempDataSetting = %d", tempDataSetting);
            getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_TEMP_DATA_SETTING,
                    tempDataSetting);
        }
    } else if (strStatus.find(String8(RTC_EM_FASTDORMANCY_SYNC)) != -1) {
        int timer[RTC_EM_FASTDORMANCY_TIMER_LENGTH] = {0};
        int status[RTC_EM_FASTDORMANCY_TIMER_ARGUMENT_LENGTH] = {0};

        char *tempFdSetting = strtok((char *)strStatus.string(), ":,");
        int i = 0;
        while (tempFdSetting != NULL && i < RTC_EM_FASTDORMANCY_TIMER_LENGTH) {
            tempFdSetting = strtok(NULL, ":,");
            if (tempFdSetting != NULL) {
                timer[i++] = atoi(tempFdSetting);
            }
        }

        RFX_LOG_D(RFX_LOG_TAG, "[%d] onDataSettingStatusChanged: fastdormancy = %d,%d,%d",
                m_slot_id, timer[0], timer[1], timer[2]);

        // mode 2 is for fastdormancy timer
        if (timer[0] == 2) {
            status[0] = 3; // args num
            status[1] = timer[0]; // mode
            status[2] = timer[1]; // timer type
            status[3] = timer[2]; // timer value

            sp<RfxMessage> request = RfxMessage::obtainRequest(m_slot_id,
                RFX_MSG_REQUEST_SET_FD_MODE,
                RfxIntsData(status, RTC_EM_FASTDORMANCY_TIMER_ARGUMENT_LENGTH));
            requestToMcl(request);
        }
    }
}
/// @}

void RtcDataController::onDeinit() {
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] enter", m_slot_id, __FUNCTION__);
    getStatusManager(m_slot_id)->unRegisterStatusChangedEx(RFX_STATUS_KEY_SIM_STATE,
            RfxStatusChangeCallbackEx(this, &RtcDataController::onSimIccidChanged));
    RfxController::onDeinit();
}

bool RtcDataController::onHandleRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] requestId: %s",
            m_slot_id, __FUNCTION__, idToString(msg_id));

    switch (msg_id) {
        case RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD:
            /// Sync data setting from OEM @{
            if (RtcDataUtils::isSupportSyncDataSettingFromOem()) {
                // Not support if sync data setting from OEM hook string
                RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] Don't sync setting if sync from OEM",
                        m_slot_id, __FUNCTION__);
                sp<RfxMessage> responseMsg =
                        RfxMessage::obtainResponse(RIL_E_REQUEST_NOT_SUPPORTED, message, true);
                responseToRilj(responseMsg);
            } else {
            /// @}
                handleSyncDataSettingsToMD(message);
            }
            break;
        case RFX_MSG_REQUEST_SETUP_DATA_CALL:
            handleSetupDataRequest(message);
            break;
        case RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL:
            handleDeactivateDataRequest(message);
            break;
        case RFX_MSG_REQUEST_SET_FD_MODE:
        case RFX_MSG_REQUEST_RESET_MD_DATA_RETRY_COUNT:
        case RFX_MSG_REQUEST_START_LCE:
        case RFX_MSG_REQUEST_STOP_LCE:
        case RFX_MSG_REQUEST_PULL_LCEDATA:
        case RFX_MSG_REQUEST_DATA_CALL_LIST:
        case RFX_MSG_REQUEST_LAST_DATA_CALL_FAIL_CAUSE:
        case RFX_MSG_REQUEST_SET_DATA_PROFILE:
        case RFX_MSG_REQUEST_SET_INITIAL_ATTACH_APN:
        case RFX_MSG_REQUEST_SET_LTE_ACCESS_STRATUM_REPORT:
        case RFX_MSG_REQUEST_SET_LTE_UPLINK_DATA_TRANSFER:
        case RFX_MSG_REQUEST_RESET_ALL_CONNECTIONS:
        case RFX_MSG_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA:
        case RFX_MSG_REQUEST_SET_DATA_PROFILE_EX:
            requestToMcl(message);
            break;
        default:
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] unknown request, ignore!", m_slot_id, __FUNCTION__);
            break;
    }
    return true;
}

bool RtcDataController::onHandleResponse(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] responseId: %s",
            m_slot_id, __FUNCTION__ , idToString(msg_id));

    switch (msg_id) {
        case RFX_MSG_REQUEST_SETUP_DATA_CALL:
            handleSetupDataResponse(message);
            break;
        case RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD:
            /// Sync data setting from OEM @{
            if (RtcDataUtils::isSupportSyncDataSettingFromOem()) {
                // Not support if sync data setting from OEM hook string
                // Don't response to RILJ
                break;
            }
            /// @}
            /* falls through */
        case RFX_MSG_REQUEST_SET_FD_MODE:
        case RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL:
        case RFX_MSG_REQUEST_RESET_MD_DATA_RETRY_COUNT:
        case RFX_MSG_REQUEST_START_LCE:
        case RFX_MSG_REQUEST_STOP_LCE:
        case RFX_MSG_REQUEST_PULL_LCEDATA:
        case RFX_MSG_REQUEST_DATA_CALL_LIST:
        case RFX_MSG_REQUEST_LAST_DATA_CALL_FAIL_CAUSE:
        case RFX_MSG_REQUEST_SET_DATA_PROFILE:
        case RFX_MSG_REQUEST_SET_INITIAL_ATTACH_APN:
        case RFX_MSG_REQUEST_SET_LTE_ACCESS_STRATUM_REPORT:
        case RFX_MSG_REQUEST_SET_LTE_UPLINK_DATA_TRANSFER:
        case RFX_MSG_REQUEST_RESET_ALL_CONNECTIONS:
        case RFX_MSG_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA:
            responseToRilj(message);
            break;
        case RFX_MSG_REQUEST_SET_DATA_PROFILE_EX:
        case RFX_MSG_REQUEST_RESET_APN_TABLE_EX:
            RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] not handle %s", m_slot_id, __FUNCTION__,
                    idToString(msg_id));
            break;
        default:
            RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] unknown response, ignore!", m_slot_id, __FUNCTION__);
            break;
    }
    return true;
}

bool RtcDataController::onHandleUrc(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] urcId: %s", m_slot_id, __FUNCTION__, idToString(msg_id));
    return true;
}

void RtcDataController::handleSyncDataSettingsToMD(const sp<RfxMessage>& message) {
    // For sync the data settings.
    int *pReqData = (int *) message->getData()->getData();
    int reqDataNum = message->getData()->getDataLength() / sizeof(int);

    int defaultDataSelected = SKIP_DATA_SETTINGS; // default data Sim

    if (reqDataNum >= DEFAULT_DATA_SIM + 1) {  // For telephony framework backward comparable.
        defaultDataSelected = pReqData[DEFAULT_DATA_SIM];
    }

    if (defaultDataSelected != SKIP_DATA_SETTINGS) {
        getNonSlotScopeStatusManager()->setIntValue(RFX_STATUS_KEY_DEFAULT_DATA_SIM,
            defaultDataSelected);
    }

    requestToMcl(message);
}

void RtcDataController::preCheckIfNeedDisableIms(const sp<RfxMessage>& message) {
    const char **pReqData = (const char **) message->getData()->getData();

    int slot_id = m_slot_id;
    RfxNwServiceState defaultServiceState (0, 0, 0 ,0);
    RfxNwServiceState serviceState = getStatusManager()
            ->getServiceStateValue(RFX_STATUS_KEY_SERVICE_STATE, defaultServiceState);
    int dataRadioTech = serviceState.getRilDataRadioTech();

    RFX_LOG_D(RFX_LOG_TAG, "preCheckIfNeedDisableIms: apntype=%s, slot id=%d, datastate=%d ",
            pReqData[1], slot_id, dataRadioTech);

    //Check if apn type is MMS
    if (atoi(pReqData[1]) != MTK_RIL_DATA_PROFILE_MMS) {
        return;
    }
    //RFX_LOG_D(RFX_LOG_TAG, "preCheckIfNeedDisableIms: apn type is mms");

    //Check if MMS is sent by secondary SIM, get slot id
    if (slot_id == INVALID_VALUE) {
        return;
    }
    slot_id = ((m_slot_id == 0) ? 1 : 0);
    //RFX_LOG_D(RFX_LOG_TAG, "preCheckIfNeedDisableIms: slot is secondary");

    //Check if he RAT is under 2G/3G/C2K
    switch (dataRadioTech) {
        case RADIO_TECH_LTE:
            return;
        case RADIO_TECH_LTE_CA:
            return;
        default:
            break;
    }
    requestTokenIdForDisableIms = message->getToken();

    //ImsPreCheck
    RtcImsController *imsController;
    sp<RfxAction> action;
    logD(RFX_LOG_TAG, "Disable IMS , slotId=%d", slot_id);
    imsController = (RtcImsController *) findController(slot_id,
            RFX_OBJ_CLASS_INFO(RtcImsController));
    action = new RfxAction1<const sp<RfxMessage>>(this,
            &RtcDataController::onImsConfirmed, message);
    imsController->requestImsDisable(slot_id, action);
    //RFX_LOG_D(RFX_LOG_TAG, "requestImsDisable finished");
}

void RtcDataController::handleSetupDataRequest(const sp<RfxMessage>& message) {

    if(RtcDataUtils::isSupportTemporaryDisableIms() && (RfxRilUtils::getSimCount() == 2)) {
        preCheckIfNeedDisableIms(message);
    }
    requestToMcl(message);
}

void RtcDataController::handleSetupDataResponse(const sp<RfxMessage>& response) {

    //Remember the transferID of MMS pdn
    if (response->getToken() == requestTokenIdForDisableIms) {
        if (response->getError() != RIL_E_SUCCESS) {
            logD(RFX_LOG_TAG, "setupdata response fail!");
            requestResumeIms(response);
            responseToRilj(response);
            return;
        }
        const int *pRspData = (int *) response->getData()->getData();
        RFX_LOG_D(RFX_LOG_TAG, "handleSetupDataResponse: cid=%d", pRspData[2]);
        transIdForDisableIms = pRspData[2];
        requestTokenIdForDisableIms = INVALID_VALUE;
    }
    responseToRilj(response);
}

void RtcDataController::handleDeactivateDataRequest(const sp<RfxMessage>& message) {

    const char **pReqData = (const char **) message->getData()->getData();

    RFX_LOG_D(RFX_LOG_TAG, "handleDeactivateDataRequest: cid=%s", pReqData[0]);
    //If the cid is same, resume ims
    if (transIdForDisableIms == atoi(pReqData[0])) {
        requestResumeIms(message);
    }
    requestToMcl(message);
}

bool RtcDataController::onCheckIfRejectMessage(const sp<RfxMessage>& message,
        bool isModemPowerOff, int radioState) {
    int msgId = message->getId();
    if((radioState == (int)RADIO_STATE_OFF) &&
            (msgId == RFX_MSG_REQUEST_START_LCE ||
             msgId == RFX_MSG_REQUEST_STOP_LCE ||
             msgId == RFX_MSG_REQUEST_PULL_LCEDATA ||
             msgId == RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD ||
             msgId == RFX_MSG_REQUEST_SET_DATA_PROFILE ||
             msgId == RFX_MSG_REQUEST_SET_INITIAL_ATTACH_APN ||
            (RfxRilUtils::isWfcSupport() &&
             msgId == RFX_MSG_REQUEST_SETUP_DATA_CALL) ||
            (RfxRilUtils::isWfcSupport() &&
             msgId == RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL) ||
             msgId == RFX_MSG_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA ||
             msgId == RFX_MSG_REQUEST_SET_DATA_PROFILE_EX)) {
        return false;
    } else if ((radioState == (int)RADIO_STATE_UNAVAILABLE) &&
            (msgId == RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD ||
             msgId == RFX_MSG_REQUEST_SET_DATA_PROFILE ||
            (RfxRilUtils::isWfcSupport() &&
             msgId == RFX_MSG_REQUEST_SETUP_DATA_CALL) ||
            (RfxRilUtils::isWfcSupport() &&
             msgId == RFX_MSG_REQUEST_DEACTIVATE_DATA_CALL) ||
             msgId == RFX_MSG_REQUEST_SET_DATA_PROFILE_EX)) {
        return false;
    }
    return RfxController::onCheckIfRejectMessage(message, isModemPowerOff, radioState);
}

bool RtcDataController::onPreviewMessage(const sp<RfxMessage>& message) {
    if (canHandleRequest(message)) {
        // RFX_LOG_D(RFX_LOG_TAG, "onPreviewMessage: true");
        return true;
    }
    // RFX_LOG_D(RFX_LOG_TAG, "onPreviewMessage: false");
    return false;
}

bool RtcDataController::onCheckIfResumeMessage(const sp<RfxMessage>& message) {
    if (canHandleRequest(message)) {
        // RFX_LOG_D(RFX_LOG_TAG, "onCheckIfResumeMessage: true");
        return true;
    }
    // RFX_LOG_D(RFX_LOG_TAG, "onCheckIfResumeMessage: false");
    return false;
}

bool RtcDataController::canHandleRequest(const sp<RfxMessage>& message) {
    int msgId = message->getId();

    if (msgId == RFX_MSG_REQUEST_SYNC_DATA_SETTINGS_TO_MD) {
        //check sim switch
        if (isUnderCapabilitySwitch == true) {
            // RFX_LOG_D(RFX_LOG_TAG, "[%s] Is under sim switch, don't process DDS sync to MD.",
                // idToString(msgId));
            return false;
        }
    }
    // RFX_LOG_D(RFX_LOG_TAG, "canHandleRequest [%s] true.", idToString(msgId));
    return true;
}

void RtcDataController::onImsConfirmed(const sp<RfxMessage> message) {
    int slotId = message->getSlotId(); // get sim slot id.
    int defaultDataSim = getNonSlotScopeStatusManager()
            ->getIntValue(RFX_STATUS_KEY_DEFAULT_DATA_SIM); // get default data sim slot id.
    logD(RFX_LOG_TAG, "onImsConfirmed Slot: %d, ims preCheck Done", defaultDataSim);
}

void RtcDataController::requestResumeIms(const sp<RfxMessage> message) {
    RtcImsController *imsController;
    sp<RfxAction> action;
    int slot_id = m_slot_id;
    slot_id = ((m_slot_id == 0) ? 1 : 0);
    logD(RFX_LOG_TAG, "Resume IMS precheck, slotId=%d", slot_id);
    imsController = (RtcImsController *) findController(slot_id,
            RFX_OBJ_CLASS_INFO(RtcImsController));
    action = new RfxAction1<const sp<RfxMessage>>(this,
            &RtcDataController::onImsConfirmed, message);
    imsController->requestImsResume(slot_id, action);
    //RFX_LOG_D(RFX_LOG_TAG, "requestImsResume finished");
    requestTokenIdForDisableIms = INVALID_VALUE;
}

void RtcDataController::onSimIccidChanged(int slotId, RfxStatusKeyEnum key,
    RfxVariant old_value, RfxVariant value) {
    RFX_UNUSED(key);
    RFX_UNUSED(old_value);
    String8 newIccid = value.asString8();
    char oldIccid[MTK_PROPERTY_VALUE_MAX] = {0};

    if (slotId > 3) {
        RFX_LOG_E(RFX_LOG_TAG, "[%d][%s] current max slot count support is 4, not %d",
                slotId, __FUNCTION__, slotId+1);
        return;
    }

    if (newIccid.string() == NULL) {
        RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] newIccid = null", slotId, __FUNCTION__);
        return;
    }

    if (0 == strcmp(newIccid.string(), "N/A")) {
        RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] newIccid = N/A", slotId, __FUNCTION__);
        return;
    }

    RFX_LOG_D(RFX_LOG_TAG, "[%d][%s] mSimIccid = %s, newIccid = %s",
            slotId, __FUNCTION__, mSimIccid.string(), newIccid.string());

    if (0 != newIccid.compare(mSimIccid)) {
        RFX_LOG_I(RFX_LOG_TAG, "[%d][%s] reset ApnTableEx due to iccid changed",
                slotId, __FUNCTION__);
        mSimIccid.setTo(newIccid);
        sp<RfxMessage> reqToRild = RfxMessage::obtainRequest(slotId,
                RFX_MSG_REQUEST_RESET_APN_TABLE_EX, RfxVoidData());
        requestToMcl(reqToRild);
    }
}
