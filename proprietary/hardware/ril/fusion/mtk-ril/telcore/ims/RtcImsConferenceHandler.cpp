/*
 * RtcImsConferenceHandler.cpp
 *
 */
#include <stdlib.h>
#include <string>
#include <iostream>
#include <sstream>

#include "RfxRootController.h"
#include "RfxLog.h"
#include "RfxRilUtils.h"
#include "RfxStringsData.h"
#include "RfxXmlParser.h"
#include "utils/Timers.h"

#include "MtkPhoneNumberUtils.h"
#include "RtcImsConferenceController.h"
#include "RtcImsConferenceHandler.h"

#define RFX_LOG_TAG "RtcImsConference"

const string RtcImsConferenceHandler::TAG_NEXT_LINE("<ascii_10>");
const string RtcImsConferenceHandler::TAG_RETURN("<ascii_13>");
const string RtcImsConferenceHandler::TAG_DOUBLE_QUOTE("<ascii_34>");

#define ANONYMOUS_URI  "sip:anonymous@anonymous.invalid"

RtcImsConferenceHandler::RtcImsConferenceHandler(int slot) :
        mIsCepNotified(false),
        mConfCallId(-1),
        mCepVersion(-1),
        mHostAddr(""),
        mAddingParticipant("null"),
        mRemovingParticipant("null")
{
    m_slot_id = slot;
    RFX_LOG_D(RFX_LOG_TAG, "RtcImsConferenceHandler()");
}

RtcImsConferenceHandler::~RtcImsConferenceHandler() {
    RFX_LOG_D(RFX_LOG_TAG, "~RtcImsConferenceHandler()");
}

void RtcImsConferenceHandler::startConference() {
    mIsConferenceActive = true;
}

void RtcImsConferenceHandler::closeConference() {
    RFX_LOG_D(RFX_LOG_TAG, "closeConference()");
    mConfCallId = -1;
    mCepVersion = -1;
    mIsCepNotified = false;
    mRemovingParticipant = "null";
    mAddingParticipant = "null";
    mHostAddr = "";
    //clean the number variable
    mLocalParticipants.clear();
    mConfParticipants.clear();
    mParticipants.clear();
    mParticipantsAddrMap.clear();
    mRestoreImsConferenceParticipant = false;
    mIsConferenceActive = false;
}

void RtcImsConferenceHandler::firstMerge(string num_1, string num_2) {
    RFX_LOG_D(RFX_LOG_TAG,
            "firstMerge, num_1: %s, num_2: %s", num_1.data(), num_2.data());
    mLocalParticipants.clear();
    mLocalParticipants.push_back(normalizeNumberFromCLIR(num_1));
    mLocalParticipants.push_back(normalizeNumberFromCLIR(num_2));
}

void RtcImsConferenceHandler::tryAddParticipant(string addr) {
    string number = getUserNameFromSipTelUriString(addr);
    number = normalizeNumberFromCLIR(number);
    mAddingParticipant = number;
}

void RtcImsConferenceHandler::tryRemoveParticipant(string addr) {
    string number = getUserNameFromSipTelUriString(addr);
    number = normalizeNumberFromCLIR(number);
    mRemovingParticipant = number;
}

bool RtcImsConferenceHandler::modifyParticipantComplete() {
    RFX_LOG_D(RFX_LOG_TAG, "modifyParticipantComplete");
    bool isContain = false;
    isContain = isContainParticipant(mLocalParticipants, mAddingParticipant);
    bool isFirstMerge = (mAddingParticipant == "null" && mRemovingParticipant == "null");
    if (mAddingParticipant != "null" && !isContain) {
        mLocalParticipants.push_back(mAddingParticipant);
    }

    if (mRemovingParticipant != "null") {
        removeParticipant(mLocalParticipants, mRemovingParticipant);
    }
    mRemovingParticipant = "null";
    mAddingParticipant = "null";
    for (int i = 0; i < (int)mLocalParticipants.size(); i++) {
        RFX_LOG_D(RFX_LOG_TAG, "modifyParticipantComplete: "
                "mLocalParticipants:%s", mLocalParticipants[i].data());
    }
    return mIsCepNotified && isFirstMerge;
}

void RtcImsConferenceHandler::modifyParticipantFailed() {
    RFX_LOG_D(RFX_LOG_TAG, "modifyParticipantFailed");
    mRemovingParticipant = "null";
    mAddingParticipant = "null";
}

string RtcImsConferenceHandler::getUserNameFromSipTelUriString(string uriString) {
    RFX_LOG_D(RFX_LOG_TAG,
        "getUserNameFromSipTelUriString uriString: %s", uriString.data());
    if (uriString.empty()) {
        return "";
    }
    //Uri uri = Uri.parse(uriString);
    // Get the address part, i.e. everything between 'sip:' and the fragment separator '#'.
    // ex: '+8618407404132@10.185.184.137:5087;transport=UDP'
    // or '1234;phone-context=munich.example.com;isub=@1234'
    vector<string> hostParts = splitString(uriString, ";");
    string headParts = hostParts[0];
    vector<string> addressParts = splitString(headParts, ":");
    string addressPart = addressParts[0];
    if(addressParts.size() > 1) {
        addressPart = addressParts[1];
    }
    vector<string> umberParts = splitString(addressPart, "@");
    string address = umberParts[0];//uri.getSchemeSpecificPart();
    if (address.empty()) {
        return "";
    }
    string userName = MtkPhoneNumberUtils::getUsernameFromUriNumber(address);
    if (userName.empty()) {
        return "";
    }

    int pIndex = userName.find(";");
    int wIndex = userName.find(",");
    if (pIndex >= 0 && wIndex >= 0) {
        userName = userName.substr(0, std::min(pIndex,wIndex));
    } else if (pIndex >=0) {
        userName = userName.substr(0, pIndex);
    } else if (wIndex >=0) {
        userName = userName.substr(0, wIndex);
    }
    return userName;
}

RtcImsConferenceCallMessageHandler* RtcImsConferenceHandler::parseXmlPackage(string data) {
    RtcImsConferenceCallMessageHandler* parsedData = new RtcImsConferenceCallMessageHandler();
    sp<RfxXmlParser> parser = new RfxXmlParser();
    RFX_LOG_D(RFX_LOG_TAG, "parseXmlPackage data: %s", data.data());
    parser->parse(parsedData, data);
    //Read conference data and parse it
    return parsedData;
}

void RtcImsConferenceHandler::updateConferenceStateWithLocalCache() {
    RFX_LOG_D(RFX_LOG_TAG, "updateConferenceStateWithLocalCache");
    if (mIsCepNotified) {
        return;
    }
    RfxRootController *root = RFX_OBJ_GET_INSTANCE(RfxRootController);
    RtcImsConferenceController *ctrl =
            (RtcImsConferenceController *)root->findController(m_slot_id,
                    RFX_OBJ_CLASS_INFO(RtcImsConferenceController));
    mParticipants.clear();
    int count = (int)mLocalParticipants.size();
    for (int i = 0; i < count; i++) {
        string addr = mLocalParticipants[i];
        sp<ConferenceCallUser> user = new ConferenceCallUser();
        user->mUserAddr = addr;
        user->mDisplayText = addr;
        user->mEndPoint = addr;
        user->mStatus = RtcImsConferenceCallMessageHandler::STATUS_CONNECTED;
        user->mEntity = addr;
        mParticipants.push_back(user);
    }

    if (ctrl != NULL && count > 0) {
        ctrl->onParticipantsUpdate();
    }
}

void RtcImsConferenceHandler::setupHost(RtcImsConferenceCallMessageHandler* xmlData){
    //get host address from the optional xml element <host-info>
    RFX_LOG_D(RFX_LOG_TAG, "setupHost");
    string hostAddr = getUserNameFromSipTelUriString(xmlData->getHostInfo());

    bool isContain = hostAddr.find(' ') != string::npos;
    while (!hostAddr.empty() && isContain) {
        hostAddr.erase(hostAddr.find(' '));
    }
    if (!hostAddr.empty()) {
        mHostAddr = hostAddr;
        return;
    }

    vector<sp<ConferenceCallUser>> users = xmlData->getUsers();
    sp<ConferenceCallUser> user = users[0];
    mHostAddr = getUserNameFromSipTelUriString(user->mEntity);
    RFX_LOG_D(RFX_LOG_TAG, "consider the first user as host");
}

bool RtcImsConferenceHandler::isSelfAddress(string address) {
    if (address.empty()) return false;
    RFX_LOG_D(RFX_LOG_TAG,
        "isSelfAddress(): address: %s, mHostAddr: %s", address.data(), mHostAddr.data());
    if (mHostAddr.compare(address) == 0 || MtkPhoneNumberUtils::compareLoosely(mHostAddr, address)) {
        RFX_LOG_D(RFX_LOG_TAG, "isSelfAddress(): true, meet host info in xml");
        return true;
    }

    RfxRootController* root = RFX_OBJ_GET_INSTANCE(RfxRootController);
    Vector<String8> xuis = root->getStatusManager(m_slot_id)->getString8VectorValue(
            RFX_STATUS_KEY_XUI_INFO);
    for (int i = 0; i < (int)xuis.size(); ++i) {
        string xui = getUserNameFromSipTelUriString(xuis[i].string());
        if (xui.empty()) continue;
        if (MtkPhoneNumberUtils::compareLoosely(address, xui)) {
            return true;
        }
    }
    RFX_LOG_D(RFX_LOG_TAG, "isSelfAddress(): false");
    return false;
}

bool RtcImsConferenceHandler::updateParticipants(sp<ConferenceCallUser> oldParticipant, string to) {

    bool updated = false;
    string from = oldParticipant->mUserAddr;

    RFX_LOG_D(
            RFX_LOG_TAG,
            "updateParticipants() : from %s to %s", from.data(), to.data());

    for(int i = 0; i < (int)mConfParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = mConfParticipants[i];

        if (participant->mUserAddr == from) {
            participant->mUserAddr = to;
            updated = true;
        }
    }

    if (!updated) {

        RFX_LOG_D(RFX_LOG_TAG, "updateParticipants() : add new item");

        oldParticipant->mUserAddr = to;
        mConfParticipants.push_back(oldParticipant);
    }

    return updated;

}

void RtcImsConferenceHandler::showCacheAndXmlData(string callerName) {

    RFX_LOG_D(
        RFX_LOG_TAG,
        "showCacheAndXmlData()  : call by %s ", callerName.data());

    RFX_LOG_D(RFX_LOG_TAG, " - mLocalParticipants =>");

    for(int i = 0; i < (int)mLocalParticipants.size(); i++) {
        RFX_LOG_D(RFX_LOG_TAG, "    + element : %s", mLocalParticipants[i].data());
    }

    RFX_LOG_D(RFX_LOG_TAG, " - mConfParticipants =>");

    for(int i = 0; i < (int)mConfParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = mConfParticipants[i];

        RFX_LOG_D(RFX_LOG_TAG, "    + element : %s", participant->mUserAddr.data());
    }

    RFX_LOG_D(RFX_LOG_TAG, " - mUnkownParticipants =>");

    for(int i = 0; i < (int)mUnkownParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = mUnkownParticipants[i];

        RFX_LOG_D(RFX_LOG_TAG, "    + element : %s", participant->mUserAddr.data());
    }
}

void RtcImsConferenceHandler::restoreParticipantsAddressByLocalCache() {
    RFX_LOG_D(
            RFX_LOG_TAG,
            "restoreParticipantsAddressByLocalCache() :  Local() = %d , Xml.C = %d, , Xml.U = %d",
            (int)mLocalParticipants.size(),
            (int)mConfParticipants.size(),
            (int)mUnkownParticipants.size());

    showCacheAndXmlData("restoreParticipantsAddressByLocalCache (before)");

    if (mRemovingParticipant != "null") {
        removeParticipant(mLocalParticipants, mRemovingParticipant);
    }
    mRemovingParticipant = "null";

    //copy mLocalParticipants
    vector<string> LocalUnMatchParticipants(mLocalParticipants);
    //copy mConfParticipants
    vector<sp<ConferenceCallUser>> XmlParticipants(mConfParticipants);

    vector<sp<ConferenceCallUser>> XmlUnMatchParticipants;

    for(int i = 0; i < (int)XmlParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = XmlParticipants[i];
        string userHandle = getPairedAddress(participant->mUserAddr);

        if (participant->mStatus == RtcImsConferenceCallMessageHandler::STATUS_DISCONNECTED) {
            // do not restore the disconnected user, the disconnected user does not contain
            // in the cached
            continue;
        }

        if (isSelfAddress(userHandle) == false
                && isContainParticipant(LocalUnMatchParticipants, userHandle) == false) {
            // Not self and match failed, keep addr and wait for restore.
            XmlUnMatchParticipants.push_back(participant);

        } else {
            // update paired data
            updateParticipants(participant, userHandle);

            // remove paired address from restoreCandidate
            removeParticipant(LocalUnMatchParticipants, userHandle);
        }
    }

    // use the "not paired" local address to restored the special user entity
    for(int i = 0; i < (int)XmlUnMatchParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = XmlUnMatchParticipants[i];
        string userHandle = participant->mUserAddr;

        if ((int)LocalUnMatchParticipants.size() == 0) {
            // No more candidates, just ignore the data.
            break;
        }

        // Always get first candidate to restore.
        string restoreHandle = LocalUnMatchParticipants[0];
        LocalUnMatchParticipants.erase(LocalUnMatchParticipants.begin());

        RFX_LOG_D(
            RFX_LOG_TAG,
            "restoreParticipantsAddressByLocalCache() : restore participants %s to: %s",
            userHandle.data(),
            restoreHandle.data());

        mParticipantsAddrMap.insert(pair<string, string>(restoreHandle , userHandle));

        // update restore data
        updateParticipants(participant, restoreHandle);

    }

    restoreUnknowParticipants(LocalUnMatchParticipants);

    // show restore result
    //
    showCacheAndXmlData("restoreParticipantsAddressByLocalCache (after)");

}

void RtcImsConferenceHandler::restoreUnknowParticipants(vector<string> LocalUnMatchParticipants) {

    //copy mConfParticipants
    vector<sp<ConferenceCallUser>> unkownXmlParticipants(mUnkownParticipants);

    int restoreIndex = 0;

    for(int i = 0; i < (int)mUnkownParticipants.size(); i++) {

        sp<ConferenceCallUser> participant = mUnkownParticipants[i];

        if ((int)LocalUnMatchParticipants.size() <= restoreIndex) {
            unkownXmlParticipants.push_back(participant);
            continue;
        }

        string userHandle = LocalUnMatchParticipants[restoreIndex];

        // update data
        updateParticipants(participant, userHandle);

        if(unkownXmlParticipants.size() > 0) {
            unkownXmlParticipants.erase(unkownXmlParticipants.begin());
        }

        RFX_LOG_D(
            RFX_LOG_TAG,
            "restoreUnknowParticipants() : restore unknown participants( %d ) to: %s",
            restoreIndex,
            userHandle.data());

        restoreIndex++;
    }

    mUnkownParticipants = unkownXmlParticipants;

}

void RtcImsConferenceHandler::fullUpdateParticipants(vector<sp<ConferenceCallUser>> users) {
    RFX_LOG_D(RFX_LOG_TAG, "fullUpdateParticipants");
    mConfParticipants.clear();
    mUnkownParticipants.clear();

    for (int i = 0; i < (int)users.size(); i++) {
        sp<ConferenceCallUser> user = users[i];
        string userAddr = getUserNameFromSipTelUriString(user->mEntity);

        while (!userAddr.empty() && userAddr.find(' ') != string::npos) {
            userAddr.erase(userAddr.find(' '));
        }
        if(userAddr.empty()) {
            user->mUserAddr = "";
            mUnkownParticipants.push_back(user);
            continue;
        }
        user->mUserAddr = userAddr;
        mConfParticipants.push_back(user);
    }
}

void RtcImsConferenceHandler::partialUpdateParticipants(vector<sp<ConferenceCallUser>> users) {
    RFX_LOG_D(RFX_LOG_TAG, "partialUpdateParticipants");
    for (int i = 0; i < (int)users.size(); i++) {
        sp<ConferenceCallUser> user = users[i];
        string userAddr = getUserNameFromSipTelUriString(user->mEntity);
        user->mUserAddr = userAddr;
        string status = user->mStatus;

        while (!userAddr.empty() && userAddr.find(' ') != string::npos) {
            userAddr.erase(userAddr.find(' '));
        }
        // update participants
        if (userAddr.empty()) {
            if (!status.empty() && status ==
                    RtcImsConferenceCallMessageHandler::STATUS_CONNECTED) {
                if(!user->mUserAddr.empty()) {
                    user->mUserAddr = "";
                }
                mUnkownParticipants.push_back(user);
                RFX_LOG_D(RFX_LOG_TAG, "add participants");
            } else if (!status.empty() && status ==
                    RtcImsConferenceCallMessageHandler::STATUS_DISCONNECTED) {
                // remove last unknown participants
                for(int j = 0; j < (int)mUnkownParticipants.size(); j++) {
                    mUnkownParticipants.erase(mConfParticipants.begin() + j);
                }
                RFX_LOG_D(RFX_LOG_TAG, "remove  participants");
            }
        } else {
            if(!(status == RtcImsConferenceCallMessageHandler::STATUS_DIALING_OUT)) {
                for(int j = 0; j < (int)mConfParticipants.size(); j++) {
                    if(MtkPhoneNumberUtils::compareLoosely(
                            mConfParticipants[j]->mUserAddr, userAddr)) {
                        mConfParticipants.erase(mConfParticipants.begin() + j);
                        break;
                    }
                }
                mConfParticipants.push_back(user);
            }
        }
    }
}

void RtcImsConferenceHandler::addLocalCache(vector<string> users) {
    for(int i = 0; i < (int)users.size(); i++) {
        mLocalParticipants.push_back(users[i]);
    }
}

void RtcImsConferenceHandler::updateLocalCache() {
    for(int i = 0; i < (int)mConfParticipants.size(); i++) {
        if(RtcImsConferenceCallMessageHandler::STATUS_DISCONNECTED.compare(
                mConfParticipants[i]->mStatus) == 0) {
            removeParticipant(mLocalParticipants, mConfParticipants[i]->mUserAddr);
        }
    }
}

void RtcImsConferenceHandler::notifyConfStateUpdate() {
    RFX_LOG_D(RFX_LOG_TAG, "notifyConfStateUpdate()");
    mParticipants.clear();
    for(int i = 0; i < (int)mConfParticipants.size(); i++) {
        mParticipants.push_back(mConfParticipants[i]);
    }

    for(int i = 0; i < (int)mUnkownParticipants.size(); i++) {
        mParticipants.push_back(mUnkownParticipants[i]);
    }

    RfxRootController *root = RFX_OBJ_GET_INSTANCE(RfxRootController);
    RtcImsConferenceController *ctrl =
            (RtcImsConferenceController *)root->findController(m_slot_id,
                    RFX_OBJ_CLASS_INFO(RtcImsConferenceController));
    if (ctrl != NULL) {
        ctrl->onParticipantsUpdate();
    }
}

vector<sp<ConferenceCallUser>> RtcImsConferenceHandler::getConfParticipantsInfo() {
    return mParticipants;
}

string RtcImsConferenceHandler::getConfParticipantUri(string addr) {
    if (addr.empty()) {
        return ANONYMOUS_URI;
    }
    if (!mRestoreImsConferenceParticipant) {
        return addr;
    } else {
        if (checkCarrierConfig(RFX_STATUS_KEY_OPERATE_IMS_CONFERENCE_PARTICIPANTS_BY_USER_ENTITY)) {
            map<string, string>::iterator iter = mParticipantsAddrMap.find(addr);
            if (iter != mParticipantsAddrMap.end()) {
                return iter->second;
            }
        }
    }
    return addr;
}
//bool RtcImsConferenceHandler::isEmptyConference() {
//    RFX_LOG_D(RFX_LOG_TAG, "isEmptyConference");
//    int userCount = mUnknowParticipants.size();
//
//    unordered_map<string, Passel>::iterator ite;
//    for (ite = mConfParticipants.begin(); ite != mConfParticipants.end(); ite++) {
//        std::map<string, string> confInfo = ite->second;
//        string status = confInfo.getString(ConferenceCallMessageHandler::STATUS);
//        if (status != ConferenceCallMessageHandler::STATUS_DISCONNECTED) {
//            ++ userCount;
//        }
//    }
//    RFX_LOG_D(RFX_LOG_TAG, "isEmptyConference userCount: %d", userCount);
//    if (userCount <= 1) {
//        return true;
//    }
//    return false;
//}

/**
 * To handle IMS conference call message
 *
 * @param len The length of data
 * @param data Conference call message
 */
void RtcImsConferenceHandler::handleImsConfCallMessage(const sp<RfxMessage>& message) {
    RFX_LOG_D(RFX_LOG_TAG, "handleVolteConfCallMessage");
    /*
     * +EIMSEVTPKG: <call_id>,<type>,<urc_index>,<total_urc_count>,<data>
     * <call_id>:  0~255
     * <type>: 1 = Conference Event Package; 2 = Dialog Event Package; 3 = Message Waiting Event Package
     * <urc_index>: 1~255, the index of URC part
     * <total_urc_count>: 1~255
     * <data>: xml raw data, max length = 1950
     */

    RfxStringsData* data = (RfxStringsData*)message->getData();
    char** params = (char**)data->getData();
    int callId = atoi(params[0]);
    int msgType = atoi(params[1]);
    int index = atoi(params[2]);
    int count = atoi(params[3]);
    char* rawData = params[4];

    // ALPS04037793: Don't handle CEP if not conference host.
    if (mIsConferenceActive == false) {
        return;
    }

    if (callId == 255) {
        return;
    }
    int cepState;
    bool isPartialCEP;
    int version;
    int userCount;

    bool isFirstPkt = (index == 1);
    mCepData = concatData(isFirstPkt, mCepData, rawData);
    if (index != count) {
        //do nothing
        return;

    }
    mCepData = recoverDataFromAsciiTag(mCepData);
    if (mCepData.empty()) {
        RFX_LOG_D(RFX_LOG_TAG, "Failed to handleImsConfCallMessage due to data is empty");
        return;
    }

    RtcImsConferenceCallMessageHandler* xmlData = parseXmlPackage(mCepData);
    if (xmlData == NULL) {
        RFX_LOG_D(RFX_LOG_TAG, "can't create xmlData object, update conf state with local cache");
        updateConferenceStateWithLocalCache();
        return;
    }


    //get user data from xml and fill them into ImsConferenceState data structure.
    vector<sp<ConferenceCallUser>> users = xmlData->getUsers();
    if (users.size() == 0 && mConfCallId == -1) {
        goto back;
    }
    //get CPE state
    cepState = xmlData->getCEPState();
    RFX_LOG_D(RFX_LOG_TAG, "cepState: %d", cepState);
    isPartialCEP = (cepState == RtcImsConferenceCallMessageHandler::CEP_STATE_PARTIAL);

    version = xmlData->getVersion();
    //Full CEP should process by the serial number.
    //Partial CEP is independent, no need to process sequentially.
    if (isPartialCEP == false) {
        if (mCepVersion >= version && mCepVersion != -1) {
            RFX_LOG_D(RFX_LOG_TAG, "version is equal or less than local version: %d, version: %d",
                    mCepVersion, version);
            goto back;
        }
    }
    mIsCepNotified = true;
    mCepVersion = version;
    //setup host
    if (mHostAddr.empty()) {
        setupHost(xmlData);
        RFX_LOG_D(RFX_LOG_TAG, "setupHost: mHostAddr: %s",mHostAddr.data());
    }
    mConfCallId = callId;

    //get optional xml element:user count
    userCount = xmlData->getUserCount();

    //no optional user count element,
    //remove the participants who is not included in the xml.
    switch (cepState) {
        case RtcImsConferenceCallMessageHandler::CEP_STATE_FULL:
            fullUpdateParticipants(users);
            break;
        case RtcImsConferenceCallMessageHandler::CEP_STATE_PARTIAL:
            partialUpdateParticipants(users);
            break;
        default:
            if ((userCount == -1) || (userCount == (int)users.size())) {
                fullUpdateParticipants(users);
            } else {
                partialUpdateParticipants(users);
                isPartialCEP = true;
            }
            break;
    }
    RFX_LOG_D(RFX_LOG_TAG, "isPartialCEP: %d", isPartialCEP);

    if (checkCarrierConfig(RFX_STATUS_KEY_RESTORE_IMS_CONFERENCE_PARTICIPANT)) {
        mRestoreImsConferenceParticipant = true;
        restoreParticipantsAddressByLocalCache();
    }
    notifyConfStateUpdate();
    updateLocalCache();
back:
    delete xmlData;
    xmlData = NULL;
}

bool RtcImsConferenceHandler::checkCarrierConfig(const RfxStatusKeyEnum key) {
    RfxRootController* root = RFX_OBJ_GET_INSTANCE(RfxRootController);
    if (root != NULL && root->getStatusManager(m_slot_id)->getString8Value(key) == "true") {
        return true;
    }

    return false;
}

bool RtcImsConferenceHandler::isContainParticipant(vector<string> participants, string participant) {
    for (int i = 0; i < (int)participants.size(); i++) {
        if (MtkPhoneNumberUtils::compareLoosely(participants[i], participant)) {
            RFX_LOG_D(RFX_LOG_TAG, "isContainParticipant: true, meet host info in xml");
            return true;
        }
    }
    return false;
}

void RtcImsConferenceHandler::removeParticipant(vector<string>& participants, string participant) {
    for (int i = 0; i < (int)participants.size(); i++) {
        if (MtkPhoneNumberUtils::compareLoosely(participants[i], participant)) {
            participants.erase(participants.begin() + i);
            break;
        }
    }
}

string RtcImsConferenceHandler::getPairedAddress(const string &addr) {
    for (int i = 0; i < (int)mLocalParticipants.size(); i++) {
        if (MtkPhoneNumberUtils::compareLoosely(mLocalParticipants[i], addr)) {
            RFX_LOG_D(RFX_LOG_TAG, "isContainParticipant: true, meet host info in xml");
            return mLocalParticipants[i];
        }
    }
    return addr;
}

string RtcImsConferenceHandler::concatData(int isFirst, string origData, string appendData) {
    if (isFirst) {
        return appendData;
    }
    return origData + appendData;
}

vector<string> RtcImsConferenceHandler::splitString(string str, string c) {
    string::size_type pos;
    vector<string> result;
    str += c;
    string::size_type len = str.size();
    for (string::size_type i = 0; i < len; i ++) {
        pos = str.find(c, i);
        if (pos < len) {
            string temp = str.substr(i, pos-i);
            result.push_back(temp);
            i = pos + c.size() -1;
        }
    }
    return result;
}

string RtcImsConferenceHandler::recoverDataFromAsciiTag(string data) {
    data = replaceAll(data, TAG_RETURN, "\r");
    data = replaceAll(data, TAG_DOUBLE_QUOTE, "\"");
    data = replaceAll(data, TAG_NEXT_LINE, "\n");
    return data;
}

string RtcImsConferenceHandler::normalizeNumberFromCLIR(string number) {
    number = replaceAll(number, "*31#", "");
    number = replaceAll(number, "#31#", "");
    return number;
}

string RtcImsConferenceHandler::replaceAll(string &str,
        const string &old_value, const string &new_value) {
    while(true) {
        string::size_type pos(0);
        if((pos=str.find(old_value)) != string::npos) {
            str.replace(pos, old_value.length(), new_value);
        } else {
            break;
        }
    }
    return str;
}
