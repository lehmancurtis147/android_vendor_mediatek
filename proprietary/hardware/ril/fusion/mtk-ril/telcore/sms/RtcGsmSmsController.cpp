/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/*****************************************************************************
 * Include
 *****************************************************************************/
#include "RtcGsmSmsController.h"
#include <telephony/mtk_ril.h>
#include "RfxMessageId.h"
#include "RfxStringsData.h"
#include "RfxStringData.h"
#include "RfxSmsRspData.h"
#include "rfx_properties.h"
#include "RfxRilUtils.h"
#include "RtcImsSmsController.h"

using ::android::String8;

RFX_IMPLEMENT_CLASS("RtcGsmSmsController", RtcGsmSmsController, RfxController);

// Register dispatch and response class
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxStringsData, RfxSmsRspData, \
        RFX_MSG_REQUEST_IMS_SEND_GSM_SMS);
RFX_REGISTER_DATA_TO_REQUEST_ID(RfxStringsData, RfxSmsRspData, \
        RFX_MSG_REQUEST_IMS_SEND_GSM_SMS_EX);

RFX_REGISTER_DATA_TO_URC_ID(RfxStringData, \
        RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT_EX);




/*****************************************************************************
 * Class RtcGsmSmsController
 *****************************************************************************/
RtcGsmSmsController::RtcGsmSmsController() {
    setTag(String8("RtcGsmSmsCtrl"));
    mSmsFwkReady = false;
    mSmsTimerHandle = NULL;
    mSmsSending = false;
    mNeedStatusReport = false;
}

RtcGsmSmsController::~RtcGsmSmsController() {
}

void RtcGsmSmsController::onInit() {
    // Required: invoke super class implementation
    RfxController::onInit();

    const int request_id_list[] = {
        RFX_MSG_REQUEST_IMS_SEND_GSM_SMS,
        RFX_MSG_REQUEST_IMS_SEND_GSM_SMS_EX,
        RFX_MSG_REQUEST_SEND_SMS,
        RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE,
        RFX_MSG_REQUEST_WRITE_SMS_TO_SIM,
        RFX_MSG_REQUEST_DELETE_SMS_ON_SIM,
        RFX_MSG_REQUEST_REPORT_SMS_MEMORY_STATUS,
        RFX_MSG_REQUEST_GET_SMS_SIM_MEM_STATUS,
        RFX_MSG_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG,
        RFX_MSG_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG,
        RFX_MSG_REQUEST_GSM_GET_BROADCAST_LANGUAGE,
        RFX_MSG_REQUEST_GSM_SET_BROADCAST_LANGUAGE,
        RFX_MSG_REQUEST_GSM_SMS_BROADCAST_ACTIVATION,
        RFX_MSG_REQUEST_GET_GSM_SMS_BROADCAST_ACTIVATION,
        RFX_MSG_REQUEST_SMS_ACKNOWLEDGE,
        RFX_MSG_REQUEST_SMS_ACKNOWLEDGE_EX,
    };

    const int urc_id_list[] = {
        RFX_MSG_URC_RESPONSE_NEW_SMS,
        RFX_MSG_URC_RESPONSE_NEW_BROADCAST_SMS,
        RFX_MSG_URC_RESPONSE_ETWS_NOTIFICATION,
        RFX_MSG_URC_RESPONSE_NEW_SMS_ON_SIM,
        RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT,
        RFX_MSG_URC_SIM_SMS_STORAGE_FULL,
        RFX_MSG_URC_CDMA_RUIM_SMS_STORAGE_FULL
    };

    // register request & URC id list
    // NOTE. one id can only be registered by one controller
    if (RfxRilUtils::isSmsSupport()) {
        registerToHandleRequest(request_id_list, sizeof(request_id_list)/sizeof(const int));

        registerToHandleUrc(urc_id_list, sizeof(urc_id_list)/sizeof(const int));

        // register callbacks to get required information
        getStatusManager()->registerStatusChanged(RFX_STATUS_CONNECTION_STATE,
                RfxStatusChangeCallback(this, &RtcGsmSmsController::onHidlStateChanged));
    }
}

bool RtcGsmSmsController::onCheckIfRejectMessage(
        const sp<RfxMessage>& message, bool isModemPowerOff, int radioState) {
    int msgId = message->getId();
    bool isWfcSupport = RfxRilUtils::isWfcSupport();

    if (!isModemPowerOff && (radioState == (int)RADIO_STATE_OFF) &&
            (msgId == RFX_MSG_REQUEST_SEND_SMS ||
            msgId == RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE) &&
            (isWfcSupport)) {
        logD(mTag, "onCheckIfRejectMessage, isModemPowerOff %d, isWfcSupport %d",
                (isModemPowerOff == false) ? 0 : 1, isWfcSupport);
        return false;
    } else if (!isModemPowerOff && (radioState == (int)RADIO_STATE_OFF) &&
            (msgId == RFX_MSG_REQUEST_WRITE_SMS_TO_SIM ||
            msgId == RFX_MSG_REQUEST_DELETE_SMS_ON_SIM ||
            msgId == RFX_MSG_REQUEST_REPORT_SMS_MEMORY_STATUS ||
            msgId == RFX_MSG_REQUEST_GET_SMS_SIM_MEM_STATUS ||
            msgId == RFX_MSG_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG ||
            msgId == RFX_MSG_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG ||
            msgId == RFX_MSG_REQUEST_GSM_GET_BROADCAST_LANGUAGE ||
            msgId == RFX_MSG_REQUEST_GSM_SET_BROADCAST_LANGUAGE ||
            msgId == RFX_MSG_REQUEST_GSM_SMS_BROADCAST_ACTIVATION ||
            msgId == RFX_MSG_REQUEST_GET_GSM_SMS_BROADCAST_ACTIVATION ||
            msgId == RFX_MSG_REQUEST_SMS_ACKNOWLEDGE ||
            msgId == RFX_MSG_REQUEST_SMS_ACKNOWLEDGE_EX)) {
        logD(mTag, "onCheckIfRejectMessage, isModemPowerOff %d, radioState %d",
                (isModemPowerOff == false) ? 0 : 1, radioState);
        return false;
    }

    return RfxController::onCheckIfRejectMessage(message, isModemPowerOff, radioState);
}

void RtcGsmSmsController::handleRequest(const sp<RfxMessage>& msg) {
    int msg_id = msg->getId();
    switch (msg_id) {
        case RFX_MSG_REQUEST_SET_SMS_FWK_READY: {
                mSmsFwkReady = true;
            }
            break;
        case RFX_MSG_REQUEST_IMS_SEND_SMS:
        case RFX_MSG_REQUEST_IMS_SEND_SMS_EX: {
                RIL_IMS_SMS_Message *pIms = (RIL_IMS_SMS_Message*)msg->getData()->getData();
                char** pStrs = pIms->message.gsmMessage;
                char* pdu = pStrs[1];
                int countStr = GSM_SMS_MESSAGE_STRS_COUNT;
                int type = smsHexCharToDecInt(pdu, 2);
                sp<RfxMessage> req;

                logD(mTag, "smsc=%s, type=%02X, pdu=%s", ((pStrs[0] != NULL)? pStrs[0] : "null"),
                        type, ((pStrs[1] != NULL)? pStrs[1] : "null"));

                if (msg_id == RFX_MSG_REQUEST_IMS_SEND_SMS_EX) {
                    if ((type & 0x20) == 0x20) {
                        logD(mTag, "Status report is needed");
                        mNeedStatusReport = true;
                    } else {
                        logD(mTag, "Don't need status report");
                    }
                }

                int newId = (msg_id == RFX_MSG_REQUEST_IMS_SEND_SMS)?
                        RFX_MSG_REQUEST_IMS_SEND_GSM_SMS : RFX_MSG_REQUEST_IMS_SEND_GSM_SMS_EX;
                req = RfxMessage::obtainRequest(newId,
                        RfxStringsData(pStrs, countStr), msg, false);

                mSmsSending = true;
                requestToMcl(req);
            }
            break;
    }
}

bool RtcGsmSmsController::onHandleResponse(const sp<RfxMessage>& msg) {
    int msg_id = msg->getId();
    switch (msg_id) {
        case RFX_MSG_REQUEST_SEND_SMS:
        case RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE: {
            responseToRilj(msg);
            mSmsSending = false;
            break;
        }
        case RFX_MSG_REQUEST_WRITE_SMS_TO_SIM:
        case RFX_MSG_REQUEST_DELETE_SMS_ON_SIM:
        case RFX_MSG_REQUEST_REPORT_SMS_MEMORY_STATUS:
        case RFX_MSG_REQUEST_GET_SMS_SIM_MEM_STATUS:
        case RFX_MSG_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG:
        case RFX_MSG_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG:
        case RFX_MSG_REQUEST_GSM_GET_BROADCAST_LANGUAGE:
        case RFX_MSG_REQUEST_GSM_SET_BROADCAST_LANGUAGE:
        case RFX_MSG_REQUEST_GSM_SMS_BROADCAST_ACTIVATION:
        case RFX_MSG_REQUEST_GET_GSM_SMS_BROADCAST_ACTIVATION:
        case RFX_MSG_REQUEST_SMS_ACKNOWLEDGE:
        case RFX_MSG_REQUEST_SMS_ACKNOWLEDGE_EX: {
                // Send RILJ directly
                responseToRilj(msg);
            }
            break;
        case RFX_MSG_REQUEST_IMS_SEND_GSM_SMS:
        case RFX_MSG_REQUEST_IMS_SEND_GSM_SMS_EX: {
                if (msg_id == RFX_MSG_REQUEST_IMS_SEND_GSM_SMS_EX) {
                    // Update vector
                    if (msg->getError() == RIL_E_SUCCESS && mNeedStatusReport) {
                        RIL_SMS_Response* data = (RIL_SMS_Response*)msg->getData()->getData();
                        logD(mTag, "Ref %d is waitting for status report", data->messageRef);
                        ((RtcImsSmsController *)getParent())->addReferenceId(data->messageRef);
                    }
                    mNeedStatusReport = false;
                }
                sp<RfxMessage> rsp;
                int newId = (msg_id == RFX_MSG_REQUEST_IMS_SEND_GSM_SMS)?
                        RFX_MSG_REQUEST_IMS_SEND_SMS : RFX_MSG_REQUEST_IMS_SEND_SMS_EX;
                rsp = RfxMessage::obtainResponse(newId, msg);

                responseToRilj(rsp);
                mSmsSending = false;
            }
            break;
        default: {
            logD(mTag, "Not Support the req %d", msg_id);
            break;
        }
    }

    return true;
}

bool RtcGsmSmsController::previewMessage(const sp<RfxMessage>& message) {
    return onPreviewMessage(message);
}

bool RtcGsmSmsController::onPreviewMessage(const sp<RfxMessage>& msg) {
    int msgId = msg->getId();
    switch (msgId) {
        case RFX_MSG_REQUEST_IMS_SEND_SMS:
        case RFX_MSG_REQUEST_IMS_SEND_SMS_EX:
        case RFX_MSG_REQUEST_SEND_SMS:
        case RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE: {
            if (mSmsSending && (msg->getType() == REQUEST)) {
                //We only send one request to rmc, the following request should queue in rtc
                logD(mTag, "the previous request is sending, queue %s", idToString(msgId));
                return false;
            }
            break;
        }

        case RFX_MSG_URC_RESPONSE_NEW_SMS:
        case RFX_MSG_URC_RESPONSE_NEW_BROADCAST_SMS:
        case RFX_MSG_URC_RESPONSE_ETWS_NOTIFICATION:
        case RFX_MSG_URC_RESPONSE_NEW_SMS_ON_SIM:
        case RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT:
        case RFX_MSG_URC_SIM_SMS_STORAGE_FULL:
        case RFX_MSG_URC_CDMA_RUIM_SMS_STORAGE_FULL: {
            // If SMS framework is not ready, we should not allow NEW_SMS, NEW_BROADCAST,
            // NEW_SMS_ON_SIM and STATUS_REPORT
            if (!mSmsFwkReady) {
                logD(mTag, "SMS framework isn't ready yet. queue %s", idToString(msgId));
                return false;
            }
            break;
        }
    }
    return true;
}

bool RtcGsmSmsController::checkIfResumeMessage(const sp<RfxMessage>& message) {
    return onCheckIfResumeMessage(message);
}

bool RtcGsmSmsController::onCheckIfResumeMessage(const sp<RfxMessage>& msg) {
    int msgId = msg->getId();
    switch (msgId) {
        case RFX_MSG_REQUEST_IMS_SEND_SMS:
        case RFX_MSG_REQUEST_IMS_SEND_SMS_EX:
        case RFX_MSG_REQUEST_SEND_SMS:
        case RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE: {
            if (!mSmsSending && (msg->getType() == REQUEST)) {
                //We only send one request to rmc, the following request should queue in rtc
                logD(mTag, "the previous request is done, resume %s", idToString(msgId));
                return true;
            }
            break;
        }

        case RFX_MSG_URC_RESPONSE_NEW_SMS:
        case RFX_MSG_URC_RESPONSE_NEW_BROADCAST_SMS:
        case RFX_MSG_URC_RESPONSE_ETWS_NOTIFICATION:
        case RFX_MSG_URC_RESPONSE_NEW_SMS_ON_SIM:
        case RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT:
        case RFX_MSG_URC_SIM_SMS_STORAGE_FULL:
        case RFX_MSG_URC_CDMA_RUIM_SMS_STORAGE_FULL: {
            // If SMS framework is not ready, we should not allow NEW_SMS, NEW_BROADCAST,
            // NEW_SMS_ON_SIM and STATUS_REPORT
            if (mSmsFwkReady) {
                logD(mTag, "SMS framework is ready. Let's resume %s!", idToString(msgId));
                return true;
            }
            break;
        }
    }
    return false;
}

bool RtcGsmSmsController::onHandleRequest(const sp<RfxMessage>& message) {
    int msg_id = message->getId();
    switch (msg_id) {
        case RFX_MSG_REQUEST_SEND_SMS:
        case RFX_MSG_REQUEST_SEND_SMS_EXPECT_MORE: {
            mSmsSending = true;
            requestToMcl(message);
            break;
        }
        case RFX_MSG_REQUEST_WRITE_SMS_TO_SIM:
        case RFX_MSG_REQUEST_DELETE_SMS_ON_SIM:
        case RFX_MSG_REQUEST_REPORT_SMS_MEMORY_STATUS:
        case RFX_MSG_REQUEST_GET_SMS_SIM_MEM_STATUS:
        case RFX_MSG_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG:
        case RFX_MSG_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG:
        case RFX_MSG_REQUEST_GSM_GET_BROADCAST_LANGUAGE:
        case RFX_MSG_REQUEST_GSM_SET_BROADCAST_LANGUAGE:
        case RFX_MSG_REQUEST_GSM_SMS_BROADCAST_ACTIVATION:
        case RFX_MSG_REQUEST_GET_GSM_SMS_BROADCAST_ACTIVATION:
        case RFX_MSG_REQUEST_SMS_ACKNOWLEDGE:
        case RFX_MSG_REQUEST_SMS_ACKNOWLEDGE_EX: {
            // Send RMC directly
            requestToMcl(message);
            break;
        }
        default: {
            logD(mTag, "onHandleRequest, not Support the req %s", idToString(msg_id));
            break;
        }
    }

    return true;
}

bool RtcGsmSmsController::onHandleUrc(const sp<RfxMessage>& msg) {
    int msg_id = msg->getId();
    switch (msg_id) {
        case RFX_MSG_URC_RESPONSE_NEW_SMS:
        case RFX_MSG_URC_RESPONSE_NEW_BROADCAST_SMS:
        case RFX_MSG_URC_RESPONSE_ETWS_NOTIFICATION:
        case RFX_MSG_URC_RESPONSE_NEW_SMS_ON_SIM:
        case RFX_MSG_URC_SIM_SMS_STORAGE_FULL:
        case RFX_MSG_URC_CDMA_RUIM_SMS_STORAGE_FULL: {
                // Send RILJ directly
                responseToRilj(msg);
            }
            break;
        case RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT: {
                char* pdu = (char*)msg->getData()->getData();
                int ref = getReferenceIdFromCDS(pdu);
                logD(mTag, "Ref %d is coming, cachedSize= %d, pdu %s", ref,
                                ((RtcImsSmsController *)getParent())->getCacheSize(), pdu);
                if (((RtcImsSmsController *)getParent())->removeReferenceIdCached(ref)) {
                    // This request comes from Ims, so we send it to Ims RIL indication
                    sp<RfxMessage> unsol = RfxMessage::obtainUrc(
                            m_slot_id, RFX_MSG_URC_RESPONSE_NEW_SMS_STATUS_REPORT_EX, msg);
                    responseToRilj(unsol);
                } else {
                    // Send RILJ directly
                    responseToRilj(msg);
                }
            }
            break;
        default:
            logD(mTag, "Not Support the urc %s", idToString(msg_id));
            break;
    }

    return true;
}

void RtcGsmSmsController::onHidlStateChanged(RfxStatusKeyEnum key,
        RfxVariant old_value, RfxVariant value) {
    bool oldState = false, newState = false;

    RFX_UNUSED(key);
    oldState = old_value.asBool();
    newState = value.asBool();

    logD(mTag, "onHidlStateChanged (%s, %s, %s) (slot %d)", boolToString(oldState),
            boolToString(newState), boolToString(mSmsFwkReady), getSlotId());

    if (mSmsTimerHandle != NULL) {
        RfxTimer::stop(mSmsTimerHandle);
    }
    mSmsTimerHandle = NULL;
    if (!newState) {
        mSmsFwkReady = false;
    } else if (newState && !mSmsFwkReady){
        //When RILD and RILJ Hidl connected, maybe InboundSmsHandler is initializing
        //So we start timer to wait InboundSmsHandler finish init and mCi.setOnNewGsmSms
        mSmsTimerHandle = RfxTimer::start(RfxCallback0(this,
                &RtcGsmSmsController::delaySetSmsFwkReady), ms2ns(DELAY_SET_SMS_FWK_READY_TIMER));
    }
}

void RtcGsmSmsController::delaySetSmsFwkReady() {
    logD(mTag, "delaySetSmsFwkReady(%s to true)(slot%d)", boolToString(mSmsFwkReady), getSlotId());
    mSmsFwkReady = true;
}


/*****************************************************************************
 * Utility function
 *****************************************************************************/
const char* RtcGsmSmsController::boolToString(bool value) {
    return value ? "true" : "false";
}

int RtcGsmSmsController::getReferenceIdFromCDS(char *hex) {
    int smscLength = smsHexCharToDecInt(hex, 2);
    //CDS format: smscLength(2) - smsc(smscLength * 2) - type(2) - reference id - ...
    return smsHexCharToDecInt(hex + 2 + smscLength * 2 + 2, 2);
}

int RtcGsmSmsController::smsHexCharToDecInt(char *hex, int length) {
    int i = 0;
    int value, digit;

    for (i = 0, value = 0; i < length && hex[i] != '\0'; i++) {
        if (hex[i]>='0' && hex[i]<='9') {
            digit = hex[i] - '0';
        } else if ( hex[i]>='A' && hex[i] <= 'F') {
            digit = hex[i] - 'A' + 10;
        } else if ( hex[i]>='a' && hex[i] <= 'f') {
            digit = hex[i] - 'a' + 10;
        } else {
            return -1;
        }
        value = value*16 + digit;
    }

    return value;
}



