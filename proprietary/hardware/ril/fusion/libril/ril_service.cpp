/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "RILC"

#include <vendor/mediatek/hardware/radio/3.0/IRadio.h>
#include <vendor/mediatek/hardware/radio/3.6/IRadio.h>
#include <vendor/mediatek/hardware/radio/3.0/IRadioResponse.h>
#include <vendor/mediatek/hardware/radio/3.0/IRadioIndication.h>
#include <vendor/mediatek/hardware/radio/3.1/IImsRadioIndication.h>
#include <vendor/mediatek/hardware/radio/3.6/IImsRadioIndication.h>
#include <vendor/mediatek/hardware/radio/3.0/IImsRadioResponse.h>
#include <vendor/mediatek/hardware/radio/3.0/IMwiRadioIndication.h>
#include <vendor/mediatek/hardware/radio/3.0/IMwiRadioResponse.h>
#include <vendor/mediatek/hardware/radio/3.5/ISubsidyLockIndication.h>
#include <vendor/mediatek/hardware/radio/3.5/ISubsidyLockResponse.h>

#include <hwbinder/IPCThreadState.h>
#include <hwbinder/ProcessState.h>
#include <ril_service.h>
#include <hidl/HidlTransportSupport.h>
#include <utils/SystemClock.h>
#include <inttypes.h>
#include <libmtkrilutils.h>
#include <mtk_log.h>
#include <mtk_properties.h>
#include <mtkconfigutils.h>

#include <fcntl.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#define INVALID_HEX_CHAR 16

using namespace android::hardware::radio;
using namespace android::hardware::radio::V1_0;
using namespace vendor::mediatek::hardware::radio;
using namespace vendor::mediatek::hardware::radio::V3_0;
using namespace vendor::mediatek::hardware::radio::V3_1;
using namespace vendor::mediatek::hardware::radio::V3_5;
using namespace vendor::mediatek::hardware::radio::V3_6;

namespace AOSP_V1_0 = android::hardware::radio::V1_0;
namespace AOSP_V1_1 = android::hardware::radio::V1_1;
namespace AOSP_V1_2 = android::hardware::radio::V1_2;
namespace VENDOR_V3_0 = vendor::mediatek::hardware::radio::V3_0;
namespace VENDOR_V3_1 = vendor::mediatek::hardware::radio::V3_1;
namespace VENDOR_V3_5 = vendor::mediatek::hardware::radio::V3_5;
namespace VENDOR_V3_6 = vendor::mediatek::hardware::radio::V3_6;

using ::android::hardware::configureRpcThreadpool;
using ::android::hardware::joinRpcThreadpool;
using ::android::hardware::Return;
using ::android::hardware::hidl_string;
using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_array;
using ::android::hardware::Void;
using ::android::hardware::radio::V1_2::AudioQuality;
using android::CommandInfo;
using android::RequestInfo;
using android::requestToString;
using android::sp;

#define BOOL_TO_INT(x) (x ? 1 : 0)
#define ATOI_NULL_HANDLED(x) (x ? atoi(x) : -1)
#define ATOI_NULL_HANDLED_DEF(x, defaultVal) (x ? atoi(x) : defaultVal)

#if defined(ANDROID_MULTI_SIM)
#define CALL_ONREQUEST(a, b, c, d, e) \
        s_vendorFunctions->onRequest((a), (b), (c), (d), ((RIL_SOCKET_ID)(e)))
#define CALL_ONSTATEREQUEST(a) s_vendorFunctions->onStateRequest((RIL_SOCKET_ID)(a))
#else
#define CALL_ONREQUEST(a, b, c, d, e) s_vendorFunctions->onRequest((a), (b), (c), (d))
#define CALL_ONSTATEREQUEST(a) s_vendorFunctions->onStateRequest()
#endif

RIL_RadioFunctions *s_vendorFunctions = NULL;
static CommandInfo *s_commands;

// M @{
static CardState s_cardState[MAX_SIM_COUNT * DIVISION_COUNT];
// M @}

struct RadioImpl;

//#if (SIM_COUNT >= 2)
sp<RadioImpl> radioService[MAX_SIM_COUNT * DIVISION_COUNT];
int64_t nitzTimeReceived[MAX_SIM_COUNT * DIVISION_COUNT];
// counter used for synchronization. It is incremented every time response callbacks are updated.
volatile int32_t mCounterRadio[MAX_SIM_COUNT * DIVISION_COUNT];

// To Compute IMS Slot Id
extern "C" int toRealSlot(int slotId);
extern "C" int isImsSlot(int slotId);
extern "C" int toImsSlot(int slotId);
extern "C" int isSESlot(int slotId);

static pthread_rwlock_t radioServiceRwlocks[] = { PTHREAD_RWLOCK_INITIALIZER,
                                                  PTHREAD_RWLOCK_INITIALIZER,
                                                  PTHREAD_RWLOCK_INITIALIZER,
                                                  PTHREAD_RWLOCK_INITIALIZER};

void convertRilHardwareConfigListToHal(void *response, size_t responseLen,
        hidl_vec<HardwareConfig>& records);

void convertRilRadioCapabilityToHal(void *response, size_t responseLen, RadioCapability& rc);

void convertRilLceDataInfoToHal(void *response, size_t responseLen, LceDataInfo& lce);

void convertRilSignalStrengthToHal(void *response, size_t responseLen,
        SignalStrength& signalStrength);

void convertRilSignalStrengthToHal_1_2(void *response, size_t responseLen,
        AOSP_V1_2::SignalStrength& signalStrength);

void convertRilDataCallToHal(RIL_Data_Call_Response_v11 *dcResponse,
        SetupDataCallResult& dcResult);

void convertRilDataCallListToHal(void *response, size_t responseLen,
        hidl_vec<SetupDataCallResult>& dcResultList);

char* convertRilCellInfoListToHal(void *response, size_t responseLen, hidl_vec<CellInfo>& records);

char* convertRilCellInfoListToHal_1_2(void *response, size_t responseLen,
        hidl_vec<AOSP_V1_2::CellInfo>& records);

RadioIndicationType convertIntToRadioIndicationType(int indicationType);

// M: [OD over ePDG] start
int encodeRat(int active, int rat, int slotId);

void convertRilDataCallToHalEx(MTK_RIL_Data_Call_Response_v11 *dcResponse,
        SetupDataCallResult& dcResult, int slotId);

void convertRilDataCallListToHalEx(void *response, size_t responseLen,
        hidl_vec<SetupDataCallResult>& dcResultList, int slotId);
// M: [OD over ePDG] end

// M: [Inactive Timer] start
int decodeInactiveTimer(unsigned int authType);
// M: [Inactive Timer] end

bool isMtkFwkAddonNotExisted(int slotId);
void setDataProfileEx(int32_t serial, const DataProfileInfo& profiles, bool isRoaming, int slotId);

struct RadioImpl : public VENDOR_V3_6::IRadio {
    int32_t mSlotId;
    sp<AOSP_V1_0::IRadioResponse> mRadioResponse;
    sp<AOSP_V1_0::IRadioIndication> mRadioIndication;
    sp<AOSP_V1_1::IRadioResponse> mRadioResponseV1_1;
    sp<AOSP_V1_1::IRadioIndication> mRadioIndicationV1_1;
    sp<AOSP_V1_2::IRadioResponse> mRadioResponseV1_2;
    sp<AOSP_V1_2::IRadioIndication> mRadioIndicationV1_2;
    sp<VENDOR_V3_0::IRadioResponse> mRadioResponseMtk;
    sp<VENDOR_V3_0::IRadioIndication> mRadioIndicationMtk;
    sp<IImsRadioResponse> mRadioResponseIms;
    sp<VENDOR_V3_0::IImsRadioIndication> mRadioIndicationIms;
    sp<VENDOR_V3_1::IImsRadioIndication> mRadioIndicationImsV3_1;
    sp<VENDOR_V3_6::IImsRadioIndication> mRadioIndicationImsV3_6;
    sp<IMwiRadioResponse> mRadioResponseMwi;
    sp<IMwiRadioIndication> mRadioIndicationMwi;
    sp<ISERadioResponse> mRadioResponseSE;
    sp<ISERadioIndication> mRadioIndicationSE;

    Return<void> setResponseFunctions(
            const ::android::sp<AOSP_V1_0::IRadioResponse>& radioResponse,
            const ::android::sp<AOSP_V1_0::IRadioIndication>& radioIndication);

    Return<void> setResponseFunctionsMtk(
            const ::android::sp<VENDOR_V3_0::IRadioResponse>& radioResponse,
            const ::android::sp<VENDOR_V3_0::IRadioIndication>& radioIndication);

    Return<void> setResponseFunctionsIms(
            const ::android::sp<IImsRadioResponse>& radioResponse,
            const ::android::sp<VENDOR_V3_0::IImsRadioIndication>& radioIndication);

    Return<void> setResponseFunctionsMwi(
            const ::android::sp<IMwiRadioResponse>& radioResponse,
            const ::android::sp<IMwiRadioIndication>& radioIndication);

    Return<void> setResponseFunctionsSubsidyLock(
            const ::android::sp<VENDOR_V3_5::ISubsidyLockResponse>& sublockResponse,
            const ::android::sp<VENDOR_V3_5::ISubsidyLockIndication>& sublockIndication);

    Return<void> sendSubsidyLockRequest(int32_t serial, int32_t reqType,
            const ::android::hardware::hidl_vec<uint8_t>& data);

    /// MTK: ForSE @{
    Return<void> setResponseFunctionsSE(
            const ::android::sp<ISERadioResponse>& radioResponse,
            const ::android::sp<ISERadioIndication>& radioIndication);
    /// MTK: ForSE @}

    Return<void> getIccCardStatus(int32_t serial);

    Return<void> supplyIccPinForApp(int32_t serial, const hidl_string& pin,
            const hidl_string& aid);

    Return<void> supplyIccPukForApp(int32_t serial, const hidl_string& puk,
            const hidl_string& pin, const hidl_string& aid);

    Return<void> supplyIccPin2ForApp(int32_t serial,
            const hidl_string& pin2,
            const hidl_string& aid);

    Return<void> supplyIccPuk2ForApp(int32_t serial, const hidl_string& puk2,
            const hidl_string& pin2, const hidl_string& aid);

    Return<void> changeIccPinForApp(int32_t serial, const hidl_string& oldPin,
            const hidl_string& newPin, const hidl_string& aid);

    Return<void> changeIccPin2ForApp(int32_t serial, const hidl_string& oldPin2,
            const hidl_string& newPin2, const hidl_string& aid);

    Return<void> supplyNetworkDepersonalization(int32_t serial, const hidl_string& netPin);

    Return<void> getCurrentCalls(int32_t serial);

    Return<void> dial(int32_t serial, const Dial& dialInfo);

    Return<void> getImsiForApp(int32_t serial,
            const ::android::hardware::hidl_string& aid);

    Return<void> hangup(int32_t serial, int32_t gsmIndex);

    Return<void> hangupWaitingOrBackground(int32_t serial);

    Return<void> hangupForegroundResumeBackground(int32_t serial);

    Return<void> switchWaitingOrHoldingAndActive(int32_t serial);

    Return<void> conference(int32_t serial);

    Return<void> rejectCall(int32_t serial);

    Return<void> getLastCallFailCause(int32_t serial);

    Return<void> getSignalStrength(int32_t serial);

    Return<void> getVoiceRegistrationState(int32_t serial);

    Return<void> getDataRegistrationState(int32_t serial);

    Return<void> getOperator(int32_t serial);

    Return<void> setRadioPower(int32_t serial, bool on);

    Return<void> sendDtmf(int32_t serial,
            const ::android::hardware::hidl_string& s);

    Return<void> sendSms(int32_t serial, const GsmSmsMessage& message);

    Return<void> sendSMSExpectMore(int32_t serial, const GsmSmsMessage& message);

    Return<void> setupDataCall(int32_t serial,
            RadioTechnology radioTechnology,
            const DataProfileInfo& profileInfo,
            bool modemCognitive,
            bool roamingAllowed,
            bool isRoaming);

    Return<void> iccIOForApp(int32_t serial,
            const IccIo& iccIo);

    Return<void> sendUssd(int32_t serial,
            const ::android::hardware::hidl_string& ussd);

    Return<void> cancelPendingUssd(int32_t serial);

    Return<void> getClir(int32_t serial);

    Return<void> setClir(int32_t serial, int32_t status);

    Return<void> getCallForwardStatus(int32_t serial,
            const CallForwardInfo& callInfo);

    Return<void> setCallForward(int32_t serial,
            const CallForwardInfo& callInfo);

    Return<void> setColp(int32_t serial, int32_t status);

    Return<void> setColr(int32_t serial, int32_t status);

    Return<void> queryCallForwardInTimeSlotStatus(int32_t serial,
            const CallForwardInfoEx& callInfoEx);

    Return<void> setCallForwardInTimeSlot(int32_t serial,
            const CallForwardInfoEx& callInfoEx);

    Return<void> runGbaAuthentication(int32_t serial,
            const hidl_string& nafFqdn, const hidl_string& nafSecureProtocolId,
            bool forceRun, int32_t netId);

    Return<void> getCallWaiting(int32_t serial, int32_t serviceClass);

    Return<void> setCallWaiting(int32_t serial, bool enable, int32_t serviceClass);

    Return<void> acknowledgeLastIncomingGsmSms(int32_t serial,
            bool success, SmsAcknowledgeFailCause cause);

    Return<void> acceptCall(int32_t serial);

    Return<void> deactivateDataCall(int32_t serial,
            int32_t cid, bool reasonRadioShutDown);

    Return<void> getFacilityLockForApp(int32_t serial,
            const ::android::hardware::hidl_string& facility,
            const ::android::hardware::hidl_string& password,
            int32_t serviceClass,
            const ::android::hardware::hidl_string& appId);

    Return<void> setFacilityLockForApp(int32_t serial,
            const ::android::hardware::hidl_string& facility,
            bool lockState,
            const ::android::hardware::hidl_string& password,
            int32_t serviceClass,
            const ::android::hardware::hidl_string& appId);

    Return<void> setBarringPassword(int32_t serial,
            const ::android::hardware::hidl_string& facility,
            const ::android::hardware::hidl_string& oldPassword,
            const ::android::hardware::hidl_string& newPassword);

    Return<void> getNetworkSelectionMode(int32_t serial);

    Return<void> setNetworkSelectionModeAutomatic(int32_t serial);

    Return<void> setNetworkSelectionModeManual(int32_t serial,
            const ::android::hardware::hidl_string& operatorNumeric);

    Return<void> setNetworkSelectionModeManualWithAct(int32_t serial,
            const ::android::hardware::hidl_string& operatorNumeric,
            const ::android::hardware::hidl_string& act,
            const ::android::hardware::hidl_string& mode);

    Return<void> getAvailableNetworks(int32_t serial);

    Return<void> getAvailableNetworksWithAct(int32_t serial);

    Return<void> getSignalStrengthWithWcdmaEcio(int32_t serial);

    Return<void> startNetworkScan(int32_t serial, const AOSP_V1_1::NetworkScanRequest& request);

    Return<void> cancelAvailableNetworks(int32_t serial);
    Return<void> stopNetworkScan(int32_t serial);

    Return<void> startDtmf(int32_t serial,
            const ::android::hardware::hidl_string& s);

    Return<void> stopDtmf(int32_t serial);

    Return<void> getBasebandVersion(int32_t serial);

    Return<void> separateConnection(int32_t serial, int32_t gsmIndex);

    Return<void> setMute(int32_t serial, bool enable);

    Return<void> getMute(int32_t serial);

    Return<void> setBarringPasswordCheckedByNW(int32_t serial,
            const ::android::hardware::hidl_string& facility,
            const ::android::hardware::hidl_string& oldPassword,
            const ::android::hardware::hidl_string& newPassword,
            const ::android::hardware::hidl_string& cfmPassword);

    Return<void> getClip(int32_t serial);

    Return<void> setClip(int32_t serial, int32_t clipEnable);

    Return<void> getColp(int32_t serial);

    Return<void> getColr(int32_t serial);

    Return<void> sendCnap(int32_t serial, const ::android::hardware::hidl_string& cnapMessage);

    Return<void> getDataCallList(int32_t serial);

    Return<void> setSuppServiceNotifications(int32_t serial, bool enable);

    Return<void> writeSmsToSim(int32_t serial,
            const SmsWriteArgs& smsWriteArgs);

    Return<void> deleteSmsOnSim(int32_t serial, int32_t index);

    Return<void> setBandMode(int32_t serial, RadioBandMode mode);

    Return<void> getAvailableBandModes(int32_t serial);

    Return<void> sendEnvelope(int32_t serial,
            const ::android::hardware::hidl_string& command);

    Return<void> sendTerminalResponseToSim(int32_t serial,
            const ::android::hardware::hidl_string& commandResponse);

    Return<void> handleStkCallSetupRequestFromSim(int32_t serial, bool accept);

    Return<void> handleStkCallSetupRequestFromSimWithResCode(int32_t serial, int32_t resultCodet);

    Return<void> setPdnReuse(int32_t serial,
            const ::android::hardware::hidl_string& pdnReuse);

    Return<void> setOverrideApn(int32_t serial,
            const ::android::hardware::hidl_string& overrideApn);

    Return<void> setPdnNameReuse(int32_t serial,
            const ::android::hardware::hidl_string& apnName);

    Return<void> explicitCallTransfer(int32_t serial);

    Return<void> setPreferredNetworkType(int32_t serial, PreferredNetworkType nwType);

    Return<void> getPreferredNetworkType(int32_t serial);

    Return<void> getNeighboringCids(int32_t serial);

    Return<void> setLocationUpdates(int32_t serial, bool enable);

    Return<void> setCdmaSubscriptionSource(int32_t serial,
            CdmaSubscriptionSource cdmaSub);

    Return<void> setCdmaRoamingPreference(int32_t serial, CdmaRoamingType type);

    Return<void> getCdmaRoamingPreference(int32_t serial);

    Return<void> setTTYMode(int32_t serial, TtyMode mode);

    Return<void> getTTYMode(int32_t serial);

    Return<void> setPreferredVoicePrivacy(int32_t serial, bool enable);

    Return<void> getPreferredVoicePrivacy(int32_t serial);

    Return<void> sendCDMAFeatureCode(int32_t serial,
            const ::android::hardware::hidl_string& featureCode);

    Return<void> sendBurstDtmf(int32_t serial,
            const ::android::hardware::hidl_string& dtmf,
            int32_t on,
            int32_t off);

    Return<void> sendCdmaSms(int32_t serial, const CdmaSmsMessage& sms);

    Return<void> acknowledgeLastIncomingCdmaSms(int32_t serial,
            const CdmaSmsAck& smsAck);

    Return<void> getGsmBroadcastConfig(int32_t serial);

    Return<void> setGsmBroadcastConfig(int32_t serial,
            const hidl_vec<GsmBroadcastSmsConfigInfo>& configInfo);

    Return<void> setGsmBroadcastActivation(int32_t serial, bool activate);

    Return<void> getCdmaBroadcastConfig(int32_t serial);

    Return<void> setCdmaBroadcastConfig(int32_t serial,
            const hidl_vec<CdmaBroadcastSmsConfigInfo>& configInfo);

    Return<void> setCdmaBroadcastActivation(int32_t serial, bool activate);

    Return<void> getCDMASubscription(int32_t serial);

    Return<void> writeSmsToRuim(int32_t serial, const CdmaSmsWriteArgs& cdmaSms);

    Return<void> deleteSmsOnRuim(int32_t serial, int32_t index);

    Return<void> getDeviceIdentity(int32_t serial);

    Return<void> exitEmergencyCallbackMode(int32_t serial);

    Return<void> getSmscAddress(int32_t serial);

    Return<void> setSmscAddress(int32_t serial,
            const ::android::hardware::hidl_string& smsc);

    Return<void> reportSmsMemoryStatus(int32_t serial, bool available);

    Return<void> reportStkServiceIsRunning(int32_t serial);

    Return<void> getCdmaSubscriptionSource(int32_t serial);

    Return<void> requestIsimAuthentication(int32_t serial,
            const ::android::hardware::hidl_string& challenge);

    Return<void> acknowledgeIncomingGsmSmsWithPdu(int32_t serial,
            bool success,
            const ::android::hardware::hidl_string& ackPdu);

    Return<void> sendEnvelopeWithStatus(int32_t serial,
            const ::android::hardware::hidl_string& contents);

    Return<void> getVoiceRadioTechnology(int32_t serial);

    Return<void> getCellInfoList(int32_t serial);

    Return<void> setCellInfoListRate(int32_t serial, int32_t rate);

    Return<void> setInitialAttachApn(int32_t serial, const DataProfileInfo& dataProfileInfo,
            bool modemCognitive, bool isRoaming);
    Return<void> getImsRegistrationState(int32_t serial);

    Return<void> sendImsSms(int32_t serial, const ImsSmsMessage& message);

    Return<void> iccTransmitApduBasicChannel(int32_t serial, const SimApdu& message);

    Return<void> iccOpenLogicalChannel(int32_t serial,
            const ::android::hardware::hidl_string& aid, int32_t p2);

    Return<void> iccCloseLogicalChannel(int32_t serial, int32_t channelId);

    Return<void> iccTransmitApduLogicalChannel(int32_t serial, const SimApdu& message);

    Return<void> nvReadItem(int32_t serial, NvItem itemId);

    Return<void> nvWriteItem(int32_t serial, const NvWriteItem& item);

    Return<void> nvWriteCdmaPrl(int32_t serial,
            const ::android::hardware::hidl_vec<uint8_t>& prl);

    Return<void> nvResetConfig(int32_t serial, ResetNvType resetType);

    Return<void> setUiccSubscription(int32_t serial, const SelectUiccSub& uiccSub);

    Return<void> setDataAllowed(int32_t serial, bool allow);

    Return<void> getHardwareConfig(int32_t serial);

    Return<void> requestIccSimAuthentication(int32_t serial,
            int32_t authContext,
            const ::android::hardware::hidl_string& authData,
            const ::android::hardware::hidl_string& aid);

    Return<void> setDataProfile(int32_t serial,
            const ::android::hardware::hidl_vec<DataProfileInfo>& profiles, bool isRoaming);

    Return<void> requestShutdown(int32_t serial);

    Return<void> getRadioCapability(int32_t serial);

    Return<void> setRadioCapability(int32_t serial, const RadioCapability& rc);

    Return<void> startLceService(int32_t serial, int32_t reportInterval, bool pullMode);

    Return<void> stopLceService(int32_t serial);

    Return<void> pullLceData(int32_t serial);

    Return<void> getModemActivityInfo(int32_t serial);

    Return<void> setAllowedCarriers(int32_t serial,
            bool allAllowed,
            const CarrierRestrictions& carriers);

    Return<void> getAllowedCarriers(int32_t serial);

    Return<void> sendDeviceState(int32_t serial, DeviceStateType deviceStateType, bool state);

    Return<void> setIndicationFilter(int32_t serial, int32_t indicationFilter);

    Return<void> startKeepalive(int32_t serial, const AOSP_V1_1::KeepaliveRequest& keepalive);

    Return<void> stopKeepalive(int32_t serial, int32_t sessionHandle);

    Return<void> setSimCardPower(int32_t serial, bool powerUp);
    Return<void> setSimCardPower_1_1(int32_t serial,
            const AOSP_V1_1::CardPowerState state);

    Return<void> responseAcknowledgement();

    Return<void> setCarrierInfoForImsiEncryption(int32_t serial,
            const AOSP_V1_1::ImsiEncryptionInfo& message);

    void checkReturnStatus(Return<void>& ret);
    /// M: CC: call control @{
    Return<void> hangupAll(int32_t serial);
    Return<void> hangupWithReason(int32_t serial, int32_t callId, int32_t reason);
    Return<void> setCallIndication(int32_t serial,
            int32_t mode, int32_t callId, int32_t seqNumber);
    Return<void> emergencyDial(int32_t serial, const Dial& dialInfo);
    Return<void> setEccServiceCategory(int32_t serial, int32_t serviceCategory);
    // M: Set ECC ist to MD
    Return<void> setEccList(int32_t serial, const hidl_string& list1,
            const hidl_string& list2);
    /// @}

    /// M: CC: E911 request current status.
    Return<void> currentStatus(int32_t serial, int32_t airplaneMode,
            int32_t imsReg);

    /// M: CC: E911 request set ECC preferred RAT.
    Return<void> eccPreferredRat(int32_t serial, int32_t phoneType);

    Return<void> setVoicePreferStatus(int32_t serial, int32_t status);

    Return<void> setEccNum(int32_t serial, const hidl_string& eccListWithCard,
            const hidl_string& eccListNoCard);
    Return<void> getEccNum(int32_t serial);

    Return<void> triggerModeSwitchByEcc(int32_t serial, int32_t mode);
    Return<void> setTrm(int32_t serial, int32_t mode);
    Return<void> videoCallAccept(int32_t serial, int32_t videoMode, int32_t callId);

    Return<void> imsEctCommand(int32_t serial, const hidl_string& number,
            int32_t type);

    Return<void> holdCall(int32_t serial, int32_t callId);

    Return<void> resumeCall(int32_t serial, int32_t callId);

    Return<void> imsDeregNotification(int32_t serial,int32_t cause);

    Return<void> setImsEnable(int32_t serial, bool isOn);

    Return<void> setVolteEnable(int32_t serial, bool isOn);

    Return<void> setWfcEnable(int32_t serial, bool isOn);

    Return<void> setVilteEnable(int32_t serial, bool isOn);

    Return<void> setViWifiEnable(int32_t serial, bool isOn);

    Return<void> setRcsUaEnable(int32_t serial, bool isOn);

    Return<void> setImsVoiceEnable(int32_t serial, bool isOn);

    Return<void> setImsVideoEnable(int32_t serial, bool isOn);

    Return<void> setImscfg(int32_t serial, bool volteEnable,
                 bool vilteEnable, bool vowifiEnable, bool viwifiEnable,
                 bool smsEnable, bool eimsEnable);

    Return<void> setModemImsCfg(int32_t serial, const hidl_string& keys,
            const hidl_string& values, int32_t type);

    Return<void> getProvisionValue(int32_t serial,
            const hidl_string& provisionstring);

    // Ims config telephonyware START
    Return<void> setImsCfgProvisionValue(int32_t serial, int32_t configId,
                                         const hidl_string& value);

    Return<void> getImsCfgProvisionValue(int32_t serial, int32_t configId);

    Return<void>
    setImsCfgFeatureValue(int32_t serial, int32_t featureId, int32_t network, int32_t value,
                          int32_t isLast);

    Return<void> getImsCfgFeatureValue(int32_t serial, int32_t featureId, int32_t network);

    Return<void> setImsCfgResourceCapValue(int32_t serial, int32_t featureId, int32_t value);

    Return<void> getImsCfgResourceCapValue(int32_t serial, int32_t featureId);
    // Ims config telephonyware END

    // Femtocell feature
    Return<void> getFemtocellList(int32_t serial);
    Return<void> abortFemtocellList(int32_t serial);
    Return<void> selectFemtocell(int32_t serial,
        const ::android::hardware::hidl_string& operatorNumeric,
        const ::android::hardware::hidl_string& act,
        const ::android::hardware::hidl_string& csgId);
    Return<void> queryFemtoCellSystemSelectionMode(int32_t serial);
    Return<void> setFemtoCellSystemSelectionMode(int32_t serial, int32_t mode);

    Return<void> setProvisionValue(int32_t serial,
            const hidl_string& provisionstring, const hidl_string& provisionValue);

    Return<void> addImsConferenceCallMember(int32_t serial, int32_t confCallId,
            const hidl_string& address, int32_t callToAdd);

    Return<void> removeImsConferenceCallMember(int32_t serial, int32_t confCallId,
            const hidl_string& address, int32_t callToRemove);

    Return<void> setWfcProfile(int32_t serial, int32_t wfcPreference);


    Return<void> conferenceDial(int32_t serial, const ConferenceDial& dailInfo);

    Return<void> vtDial(int32_t serial, const Dial& dialInfo);

    Return<void> vtDialWithSipUri(int32_t serial, const hidl_string& address);

    Return<void> dialWithSipUri(int32_t serial, const hidl_string& address);

    Return<void> sendUssi(int32_t serial, int32_t action, const hidl_string& ussiString);

    Return<void> cancelUssi(int32_t serial);

    Return<void> getXcapStatus(int32_t serial);

    Return<void> resetSuppServ(int32_t serial);

    Return<void> setupXcapUserAgentString(int32_t serial, const hidl_string& userAgent);

    Return<void> forceReleaseCall(int32_t serial, int32_t callId);

    Return<void> setImsRtpReport(int32_t serial, int32_t pdnId,
            int32_t networkId, int32_t timer);

    Return<void> imsBearerActivationDone(int32_t serial, int32_t aid, int32_t status);

    Return<void> imsBearerDeactivationDone(int32_t serial, int32_t aid, int32_t status);

    Return<void> setImsBearerNotification(int32_t serial, int32_t enable);

    Return<void> pullCall(int32_t serial, const hidl_string& target, bool isVideoCall);

    Return<void> setImsRegistrationReport(int32_t serial);

    // MTK-START: SIM
    Return<void> getATR(int32_t serial);
    Return<void> getIccid(int32_t serial);
    Return<void> setSimPower(int32_t serial, int32_t mode);
    // MTK-END

    // MTK-START: SIM GBA
    Return<void> doGeneralSimAuthentication(int32_t serial, const SimAuthStructure& simAuth);
    // MTK-END

    // FastDormancy
    Return<void> setFdMode(int32_t serial, int mode, int param1, int param2);

    // ATCI Start
    sp<IAtciResponse> mAtciResponse;
    sp<IAtciIndication> mAtciIndication;

    Return<void> setResponseFunctionsForAtci(
            const ::android::sp<IAtciResponse>& atciResponse,
            const ::android::sp<IAtciIndication>& atciIndication);

    Return<void> sendAtciRequest(int32_t serial,
            const ::android::hardware::hidl_vec<uint8_t>& data);
    // ATCI End
    /// M: eMBMS feature
    Return<void> sendEmbmsAtCommand(int32_t serial, const ::android::hardware::hidl_string& data);
    /// M: eMBMS end
    Return<void> getSmsRuimMemoryStatus(int32_t serial);

    // worldmode {
    Return<void> setResumeRegistration(int32_t serial, int32_t sessionId);

    Return<void> storeModemType(int32_t serial, int32_t modemType);

    Return<void> reloadModemType(int32_t serial, int32_t modemType);
    // worldmode }

    Return<void> resetRadio(int32_t serial);
    Return<void> restartRILD(int32_t serial);

    // M: Data Framework - common part enhancement
    Return<void> syncDataSettingsToMd(int32_t serial,
            const hidl_vec<int32_t>& settings);
    Return<void> resetMdDataRetryCount(int32_t serial,
            const hidl_string& apn);
    // M: Data Framework - CC 33
    Return<void> setRemoveRestrictEutranMode(int32_t serial, int32_t type);
    // M: [LTE][Low Power][UL traffic shaping] @{
    Return<void> setLteAccessStratumReport(int32_t serial, int32_t enable);
    Return<void> setLteUplinkDataTransfer(int32_t serial, int32_t state, int32_t interfaceId);
    // M: [LTE][Low Power][UL traffic shaping] @}
    // SMS-START
    Return<void> getSmsParameters(int32_t serial);
    Return<void> setSmsParameters(int32_t serial, const SmsParams& message);
    Return<void> getSmsMemStatus(int32_t serial);
    Return<void> setEtws(int32_t serial, int32_t mode);
    Return<void> removeCbMsg(int32_t serial, int32_t channelId, int32_t serialId);
    Return<void> setGsmBroadcastLangs(int32_t serial,
            const ::android::hardware::hidl_string& langs);
    Return<void> getGsmBroadcastLangs(int32_t serial);
    Return<void> getGsmBroadcastActivation(int32_t serial);
    Return<void> sendImsSmsEx(int32_t serial, const ImsSmsMessage& message);
    Return<void> setSmsFwkReady(int32_t serial);
    Return<void> acknowledgeLastIncomingGsmSmsEx(int32_t serial,
            bool success, SmsAcknowledgeFailCause cause);
    Return<void> acknowledgeLastIncomingCdmaSmsEx(int32_t serial, const CdmaSmsAck& smsAck);
    // SMS-END
    // MTK-START: SIM ME LOCK
    Return<void> queryNetworkLock(int32_t serial, int32_t category);
    Return<void> setNetworkLock(int32_t serial, int32_t category, int32_t lockop,
            const hidl_string& password, const hidl_string& data_imsi,
            const hidl_string& gid1, const hidl_string& gid2);
    Return<void> supplyDepersonalization(int32_t serial, const hidl_string& netPin, int32_t type);
    // MTK-END
    Return<void> setRxTestConfig(int32_t serial, int32_t antType);
    Return<void> getRxTestResult(int32_t serial, int32_t mode);

    Return<void> getPOLCapability(int32_t serial);
    Return<void> getCurrentPOLList(int32_t serial);
    Return<void> setPOLEntry(int32_t serial, int32_t index,
            const ::android::hardware::hidl_string& numeric,
            int32_t nAct);
    // PHB start
    Return<void> queryPhbStorageInfo(int32_t serial, int32_t type);
    Return<void> writePhbEntry(int32_t serial, const PhbEntryStructure& phbEntry);
    Return<void> readPhbEntry(int32_t serial, int32_t type, int32_t bIndex, int32_t eIndex);
    Return<void> queryUPBCapability(int32_t serial);
    Return<void> editUPBEntry(int32_t serial, const hidl_vec<hidl_string>& data);
    Return<void> deleteUPBEntry(int32_t serial, int32_t entryType, int32_t adnIndex, int32_t entryIndex);
    Return<void> readUPBGasList(int32_t serial, int32_t startIndex, int32_t endIndex);
    Return<void> readUPBGrpEntry(int32_t serial, int32_t adnIndex);
    Return<void> writeUPBGrpEntry(int32_t serial, int32_t adnIndex, const hidl_vec<int32_t>& grpIds);
    Return<void> getPhoneBookStringsLength(int32_t serial);
    Return<void> getPhoneBookMemStorage(int32_t serial);
    Return<void> setPhoneBookMemStorage(int32_t serial, const hidl_string& storage, const hidl_string& password);
    Return<void> readPhoneBookEntryExt(int32_t serial, int32_t index1, int32_t index2);
    Return<void> writePhoneBookEntryExt(int32_t serial, const PhbEntryExt& phbEntryExt);
    Return<void> queryUPBAvailable(int32_t serial, int32_t eftype, int32_t fileIndex);
    Return<void> readUPBEmailEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex);
    Return<void> readUPBSneEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex);
    Return<void> readUPBAnrEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex);
    Return<void> readUPBAasList(int32_t serial, int32_t startIndex, int32_t endIndex);
    Return<void> setPhonebookReady(int32_t serial, int32_t ready);
    // PHB End
    Return<void> setApcMode(int32_t serial, int32_t mode, int32_t reportMode, int32_t interval);
    Return<void> getApcInfo(int32_t serial);
    /// M: [Network][C2K] Sprint roaming control @{
    Return<void> setRoamingEnable(int32_t serial, const hidl_vec<int32_t>& config);
    Return<void> getRoamingEnable(int32_t serial, int32_t phoneId);
    /// @}
    Return<void> setLteReleaseVersion(int32_t serial, int32_t mode);
    Return<void> getLteReleaseVersion(int32_t serial);

    Return<void> setModemPower(int32_t serial, bool isOn);
    // External SIM [Start]
    Return<void> sendVsimNotification(int32_t serial, uint32_t transactionId,
            uint32_t eventId, uint32_t simType);

    Return<void> sendVsimOperation(int32_t serial, uint32_t transactionId,
            uint32_t eventId, int32_t result, int32_t dataLength, const hidl_vec<uint8_t>& data);
    // External SIM [End]

    Return<void> setVoiceDomainPreference(int32_t serial, int32_t vdp);
    Return<void> setWifiEnabled(int32_t serial, const hidl_string& ifName,
        int32_t isWifiEnabled, int32_t isFlightModeOn);
    Return<void> setWifiAssociated(int32_t serial, const hidl_string& ifName,
        int32_t associated, const hidl_string& ssid, const hidl_string& apMac);
    Return<void> setWifiSignalLevel(int32_t serial,
            int32_t rssi, int32_t snr);
    Return<void> setWifiIpAddress(int32_t serial,
            const hidl_string& ifName, const hidl_string& ipv4Addr,
            const hidl_string& ipv6Addr);
    Return<void> setLocationInfo(int32_t serial,
            const hidl_string& accountId,
            const hidl_string& broadcastFlag, const hidl_string& latitude,
            const hidl_string& longitude, const hidl_string& accuracy,
            const hidl_string& method, const hidl_string& city, const hidl_string& state,
            const hidl_string& zip, const hidl_string& countryCode, const hidl_string& ueWlanMac);
    Return<void> setEmergencyAddressId(int32_t serial, const hidl_string& aid);
    Return<void> setNattKeepAliveStatus(int32_t serial,
            const hidl_string& ifName, bool enable,
            const hidl_string& srcIp, int32_t srcPort,
            const hidl_string& dstIp, int32_t dstPort);
    Return<void> setWifiPingResult(int32_t serial, int32_t rat, int32_t latency,
            int32_t pktloss);
    Return<void> setE911State(int32_t serial, int32_t state);
    Return<void> setServiceStateToModem(int32_t serial, int32_t voiceRegState,
            int32_t dataRegState, int32_t voiceRoamingType, int32_t dataRoamingType,
            int32_t rilVoiceRegState, int32_t rilDataRegState);
    Return<void> sendRequestRaw(int32_t serial, const hidl_vec<uint8_t>& data);
    Return<void> sendRequestStrings(int32_t serial, const hidl_vec<hidl_string>& data);
    Return<void> startNetworkScan_1_2(int32_t serial, const AOSP_V1_2::NetworkScanRequest& request);
    Return<void> setIndicationFilter_1_2(int32_t serial,
            int32_t indicationFilter);
    Return<void> setSignalStrengthReportingCriteria(int32_t serial, int32_t hysteresisMs,
            int32_t hysteresisDb, const hidl_vec<int32_t>& thresholdsDbm,
            AOSP_V1_2::AccessNetwork accessNetwork);
    Return<void> setLinkCapacityReportingCriteria(int32_t serial, int32_t hysteresisMs,
            int32_t hysteresisDlKbps, int32_t hysteresisUlKbps,
            const hidl_vec<int32_t>& thresholdsDownlinkKbps, const hidl_vec<int32_t>& thresholdsUplinkKbps,
            AOSP_V1_2::AccessNetwork accessNetwork);
    Return<void> setupDataCall_1_2(int32_t serial,
            AOSP_V1_2::AccessNetwork accessNetwork,
            const DataProfileInfo& dataProfileInfo, bool modemCognitive, bool roamingAllowed,
            bool isRoaming, AOSP_V1_2::DataRequestReason reason,
            const hidl_vec<hidl_string>& addresses, const hidl_vec<hidl_string>& dnses);
    Return<void> deactivateDataCall_1_2(int32_t serial, int32_t cid,
            AOSP_V1_2::DataRequestReason reason);
    Return<void> dataConnectionAttach(int32_t serial, int32_t type);
    Return<void> dataConnectionDetach(int32_t serial, int32_t type);
    Return<void> resetAllConnections(int32_t serial);
    Return<void> reportAirplaneMode(int32_t serial, int32_t enable);
    Return<void> reportSimMode(int32_t serial, int32_t simMode);
    Return<void> setSilentReboot(int32_t serial, int32_t enable);
    Return<void> setTxPowerStatus(int32_t serial, int32_t mode);
    Return<void> setPropImsHandover(int32_t serial, const hidl_string& value);
    Return<void> setOperatorConfiguration(int32_t serial, int32_t type, const hidl_string& data);
    Return<void> setSuppServProperty(int32_t serial, const hidl_string& name, const hidl_string& value);
    Return<void> getSuppServProperty(int32_t serial, const hidl_string& name);
    // MTK-START: SIM SLOT LOCK
    Return<void> supplyDeviceNetworkDepersonalization(int32_t serial, const hidl_string& netPin);
    // MTK-END
    Return<void> notifyEPDGScreenState(int32_t serial, int32_t state);
};

void memsetAndFreeStrings(int numPointers, ...) {
    va_list ap;
    va_start(ap, numPointers);
    for (int i = 0; i < numPointers; i++) {
        char *ptr = va_arg(ap, char *);
        if (ptr) {
#ifdef MEMSET_FREED
#define MAX_STRING_LENGTH 4096
            memset(ptr, 0, strnlen(ptr, MAX_STRING_LENGTH));
#endif
            free(ptr);
        }
    }
    va_end(ap);
}

void sendErrorResponse(RequestInfo *pRI, RIL_Errno err) {
    pRI->pCI->responseFunction((int) pRI->socket_id,
            (int) RadioResponseType::SOLICITED, pRI->token, err, NULL, 0);
}

/**
 * Copies over src to dest. If memory allocation fails, responseFunction() is called for the
 * request with error RIL_E_NO_MEMORY.
 * Returns true on success, and false on failure.
 */
bool copyHidlStringToRil(char **dest, const hidl_string &src, RequestInfo *pRI, bool allowEmpty) {
    size_t len = src.size();
    if (len == 0 && !allowEmpty) {
        *dest = NULL;
        return true;
    }
    *dest = (char *) calloc(len + 1, sizeof(char));
    if (*dest == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }
    strncpy(*dest, src.c_str(), len + 1);
    return true;
}

bool copyHidlStringToRil(char **dest, const hidl_string &src, RequestInfo *pRI) {
    return copyHidlStringToRil(dest, src, pRI, false);
}

AOSP_V1_2::AudioQuality convertCallsSpeechCodecToHidlAudioQuality(int speechCodec) {
    /**
     *   MtkSpeechCodecTypes
     *     NONE(0),
     *     QCELP13K(0x0001),
     *     EVRC(0x0002),
     *     EVRC_B(0x0003),
     *     EVRC_WB(0x0004),
     *     EVRC_NW(0x0005),
     *     AMR_NB(0x0006),
     *     AMR_WB(0x0007),
     *     GSM_EFR(0x0008),
     *     GSM_FR(0x0009),
     *     GSM_HR(0x000A);
     */
    switch (speechCodec) {
    case 0x0006:
        return AOSP_V1_2::AudioQuality::AMR;
    case 0x0007:
        return AOSP_V1_2::AudioQuality::AMR_WB;
    case 0x0008:
        return AOSP_V1_2::AudioQuality::GSM_EFR;
    case 0x0009:
        return AOSP_V1_2::AudioQuality::GSM_FR;
    case 0x000A:
        return AOSP_V1_2::AudioQuality::GSM_HR;
    case 0x0002:
        return AOSP_V1_2::AudioQuality::EVRC;
    case 0x0003:
        return AOSP_V1_2::AudioQuality::EVRC_B;
    case 0x0004:
        return AOSP_V1_2::AudioQuality::EVRC_WB;
    case 0x0005:
        return AOSP_V1_2::AudioQuality::EVRC_NW;
    default:
        return AOSP_V1_2::AudioQuality::UNSPECIFIED;
    }
}

hidl_string convertCharPtrToHidlString(const char *ptr) {
    hidl_string ret;
    if (ptr != NULL) {
        // TODO: replace this with strnlen
        ret.setToExternal(ptr, strlen(ptr));
    }
    return ret;
}

bool dispatchVoid(int serial, int slotId, int request) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }
    CALL_ONREQUEST(request, NULL, 0, pRI, slotId);
    return true;
}

bool dispatchString(int serial, int slotId, int request, const char * str) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    char *pString;
    if (!copyHidlStringToRil(&pString, str, pRI)) {
        return false;
    }

    CALL_ONREQUEST(request, pString, sizeof(char *), pRI, slotId);

    memsetAndFreeStrings(1, pString);
    return true;
}

bool dispatchStrings(int serial, int slotId, int request, bool allowEmpty, int countStrings, ...) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    char **pStrings;
    pStrings = (char **)calloc(countStrings, sizeof(char *));
    if (pStrings == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(request));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }
    va_list ap;
    va_start(ap, countStrings);
    for (int i = 0; i < countStrings; i++) {
        const char* str = va_arg(ap, const char *);
        if (!copyHidlStringToRil(&pStrings[i], hidl_string(str), pRI, allowEmpty)) {
            va_end(ap);
            for (int j = 0; j < i; j++) {
                memsetAndFreeStrings(1, pStrings[j]);
            }
            free(pStrings);
            return false;
        }
    }
    va_end(ap);

    CALL_ONREQUEST(request, pStrings, countStrings * sizeof(char *), pRI, slotId);

    if (pStrings != NULL) {
        for (int i = 0 ; i < countStrings ; i++) {
            memsetAndFreeStrings(1, pStrings[i]);
        }

#ifdef MEMSET_FREED
        memset(pStrings, 0, countStrings * sizeof(char *));
#endif
        free(pStrings);
    }
    return true;
}

bool dispatchStrings(int serial, int slotId, int request, const hidl_vec<hidl_string>& data) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    int countStrings = data.size();
    char **pStrings;
    pStrings = (char **)calloc(countStrings, sizeof(char *));
    if (pStrings == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(request));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }

    for (int i = 0; i < countStrings; i++) {
        if (!copyHidlStringToRil(&pStrings[i], data[i], pRI)) {
            for (int j = 0; j < i; j++) {
                memsetAndFreeStrings(1, pStrings[j]);
            }
            free(pStrings);
            return false;
        }
    }

    CALL_ONREQUEST(request, pStrings, countStrings * sizeof(char *), pRI, slotId);

    if (pStrings != NULL) {
        for (int i = 0 ; i < countStrings ; i++) {
            memsetAndFreeStrings(1, pStrings[i]);
        }

#ifdef MEMSET_FREED
        memset(pStrings, 0, countStrings * sizeof(char *));
#endif
        free(pStrings);
    }
    return true;
}

bool dispatchInts(int serial, int slotId, int request, int countInts, ...) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    int *pInts = (int *)calloc(countInts, sizeof(int));

    if (pInts == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(request));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }
    va_list ap;
    va_start(ap, countInts);
    for (int i = 0; i < countInts; i++) {
        pInts[i] = va_arg(ap, int);
    }
    va_end(ap);

    CALL_ONREQUEST(request, pInts, countInts * sizeof(int), pRI, slotId);

    if (pInts != NULL) {
#ifdef MEMSET_FREED
        memset(pInts, 0, countInts * sizeof(int));
#endif
        free(pInts);
    }
    return true;
}

bool dispatchCallForwardStatus(int serial, int slotId, int request,
                              const CallForwardInfo& callInfo) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_CallForwardInfo cf;
    cf.status = (int) callInfo.status;
    cf.reason = callInfo.reason;
    cf.serviceClass = callInfo.serviceClass;
    cf.toa = callInfo.toa;
    cf.timeSeconds = callInfo.timeSeconds;

    if (!copyHidlStringToRil(&cf.number, callInfo.number, pRI)) {
        return false;
    }

    CALL_ONREQUEST(request, &cf, sizeof(cf), pRI, slotId);

    memsetAndFreeStrings(1, cf.number);

    return true;
}

bool dispatchCallForwardInTimeSlotStatus(int serial, int slotId, int request,
                              const CallForwardInfoEx& callInfoEx) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_CallForwardInfoEx cfEx;
    cfEx.status = (int) callInfoEx.status;
    cfEx.reason = callInfoEx.reason;
    cfEx.serviceClass = callInfoEx.serviceClass;
    cfEx.toa = callInfoEx.toa;
    cfEx.timeSeconds = callInfoEx.timeSeconds;

    if (!copyHidlStringToRil(&cfEx.number, callInfoEx.number, pRI)) {
        return false;
    }

    if (!copyHidlStringToRil(&cfEx.timeSlotBegin, callInfoEx.timeSlotBegin, pRI)) {
        memsetAndFreeStrings(1, cfEx.number);
        return false;
    }

    if (!copyHidlStringToRil(&cfEx.timeSlotEnd, callInfoEx.timeSlotEnd, pRI)) {
        memsetAndFreeStrings(2, cfEx.number, cfEx.timeSlotBegin);
        return false;
    }

    s_vendorFunctions->onRequest(request, &cfEx, sizeof(cfEx), pRI,
            pRI->socket_id);

    memsetAndFreeStrings(3, cfEx.number, cfEx.timeSlotBegin, cfEx.timeSlotEnd);

    return true;
}

bool dispatchRaw(int serial, int slotId, int request, const hidl_vec<uint8_t>& rawBytes) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    const uint8_t *uData = rawBytes.data();

    CALL_ONREQUEST(request, (void *) uData, rawBytes.size(), pRI, slotId);

    return true;
}

bool dispatchIccApdu(int serial, int slotId, int request, const SimApdu& message) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_SIM_APDU apdu = {};

    apdu.sessionid = message.sessionId;
    apdu.cla = message.cla;
    apdu.instruction = message.instruction;
    apdu.p1 = message.p1;
    apdu.p2 = message.p2;
    apdu.p3 = message.p3;

    if (!copyHidlStringToRil(&apdu.data, message.data, pRI)) {
        return false;
    }

    CALL_ONREQUEST(request, &apdu, sizeof(apdu), pRI, slotId);

    memsetAndFreeStrings(1, apdu.data);

    return true;
}

void checkReturnStatus(int32_t slotId, Return<void>& ret, bool isRadioService) {
    if (ret.isOk() == false) {
        mtkLogE(LOG_TAG, "checkReturnStatus: unable to call response/indication callback");
        // Remote process hosting the callbacks must be dead. Reset the callback objects;
        // there's no other recovery to be done here. When the client process is back up, it will
        // call setResponseFunctions()

        // Caller should already hold rdlock, release that first
        // note the current counter to avoid overwriting updates made by another thread before
        // write lock is acquired.
        int counter = mCounterRadio[slotId];
        pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(slotId);
        int ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // acquire wrlock
        ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // make sure the counter value has not changed
        if (counter == mCounterRadio[slotId]) {
            if(isImsSlot(slotId)) {
                radioService[slotId]->mRadioResponseIms = NULL;
                radioService[slotId]->mRadioIndicationIms = NULL;
            }
            else {
                radioService[slotId]->mRadioResponse = NULL;
                radioService[slotId]->mRadioIndication = NULL;
                radioService[slotId]->mRadioResponseV1_1 = NULL;
                radioService[slotId]->mRadioIndicationV1_1 = NULL;
                radioService[slotId]->mRadioResponseV1_2 = NULL;
                radioService[slotId]->mRadioIndicationV1_2 = NULL;
            }
            mCounterRadio[slotId]++;
            android::onCommandDisconnect((RIL_SOCKET_ID) slotId);
        } else {
            mtkLogE(LOG_TAG, "checkReturnStatus: not resetting responseFunctions as they likely "
                    "got updated on another thread");
        }

        // release wrlock
        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);

        // Reacquire rdlock
        ret = pthread_rwlock_rdlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }
}

void RadioImpl::checkReturnStatus(Return<void>& ret) {
    ::checkReturnStatus(mSlotId, ret, true);
}

Return<void> RadioImpl::setResponseFunctions(
        const ::android::sp<AOSP_V1_0::IRadioResponse>& radioResponseParam,
        const ::android::sp<AOSP_V1_0::IRadioIndication>& radioIndicationParam) {
    mtkLogI(LOG_TAG, "setResponseFunctions, slotId = %d", mSlotId);

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponse = radioResponseParam;
    mRadioIndication = radioIndicationParam;
    mRadioResponseV1_1 = AOSP_V1_1::IRadioResponse::castFrom(mRadioResponse).withDefault(nullptr);
    mRadioIndicationV1_1 = AOSP_V1_1::IRadioIndication::castFrom(mRadioIndication).withDefault(nullptr);
    if (mRadioResponseV1_1 == nullptr || mRadioIndicationV1_1 == nullptr) {
        mRadioResponseV1_1 = nullptr;
        mRadioIndicationV1_1 = nullptr;
    }
    mRadioResponseV1_2 = AOSP_V1_2::IRadioResponse::castFrom(mRadioResponse).withDefault(nullptr);
    mRadioIndicationV1_2 = AOSP_V1_2::IRadioIndication::castFrom(mRadioIndication).withDefault(nullptr);
    if (mRadioResponseV1_2 == nullptr || mRadioIndicationV1_2 == nullptr) {
        mRadioResponseV1_2 = nullptr;
        mRadioIndicationV1_2 = nullptr;
    }

    mCounterRadio[mSlotId]++;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    // client is connected. Send initial indications.
    android::onNewCommandConnect((RIL_SOCKET_ID) mSlotId);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsMtk(
        const ::android::sp<VENDOR_V3_0::IRadioResponse>& radioResponseParam,
        const ::android::sp<VENDOR_V3_0::IRadioIndication>& radioIndicationParam) {
    mtkLogI(LOG_TAG, "setResponseFunctionsMtk, slotId = %d", mSlotId);

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseMtk = radioResponseParam;
    mRadioIndicationMtk = radioIndicationParam;

    mRadioResponseMtk = VENDOR_V3_0::IRadioResponse::castFrom(mRadioResponseMtk).withDefault(nullptr);
    mRadioIndicationMtk = VENDOR_V3_0::IRadioIndication::castFrom(mRadioIndicationMtk).withDefault(nullptr);
    if (mRadioResponseMtk == nullptr || mRadioIndicationMtk == nullptr) {
        mRadioResponseMtk = nullptr;
        mRadioIndicationMtk = nullptr;
    }

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsIms(
        const ::android::sp<IImsRadioResponse>& radioResponseParam,
        const ::android::sp<VENDOR_V3_0::IImsRadioIndication>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctionsIms");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseIms = radioResponseParam;
    mRadioIndicationIms = radioIndicationParam;

    mRadioIndicationImsV3_1 =
            VENDOR_V3_1::IImsRadioIndication::castFrom(mRadioIndicationIms).withDefault(nullptr);

    mRadioIndicationImsV3_6 =
            VENDOR_V3_6::IImsRadioIndication::castFrom(mRadioIndicationIms).withDefault(nullptr);

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

Return<void> RadioImpl::setResponseFunctionsMwi(
        const ::android::sp<IMwiRadioResponse>& radioResponseParam,
        const ::android::sp<IMwiRadioIndication>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctionsMwi");

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseMwi = radioResponseParam;
    mRadioIndicationMwi = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}

/// MTK: ForSE @{
Return<void> RadioImpl::setResponseFunctionsSE(
        const ::android::sp<ISERadioResponse>& radioResponseParam,
        const ::android::sp<ISERadioIndication>& radioIndicationParam) {
    mtkLogD(LOG_TAG, "[%d] setResponseFunctionsSE", mSlotId);

    pthread_rwlock_t *radioServiceRwlockPtr = radio::getRadioServiceRwlock(mSlotId);
    int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
    assert(ret == 0);

    mRadioResponseSE = radioResponseParam;
    mRadioIndicationSE = radioIndicationParam;

    ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
    assert(ret == 0);

    return Void();
}
/// MTK: ForSE @}

Return<void> RadioImpl::getIccCardStatus(int32_t serial) {
    mtkLogD(LOG_TAG, "getIccCardStatus: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_SIM_STATUS);
    return Void();
}

Return<void> RadioImpl::supplyIccPinForApp(int32_t serial, const hidl_string& pin,
        const hidl_string& aid) {
    mtkLogD(LOG_TAG, "supplyIccPinForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_SIM_PIN, true,
            2, pin.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::supplyIccPukForApp(int32_t serial, const hidl_string& puk,
                                           const hidl_string& pin, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "supplyIccPukForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_SIM_PUK, true,
            3, puk.c_str(), pin.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::supplyIccPin2ForApp(int32_t serial, const hidl_string& pin2,
                                            const hidl_string& aid) {
    mtkLogD(LOG_TAG, "supplyIccPin2ForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_SIM_PIN2, true,
            2, pin2.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::supplyIccPuk2ForApp(int32_t serial, const hidl_string& puk2,
                                            const hidl_string& pin2, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "supplyIccPuk2ForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_SIM_PUK2, true,
            3, puk2.c_str(), pin2.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::changeIccPinForApp(int32_t serial, const hidl_string& oldPin,
                                           const hidl_string& newPin, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "changeIccPinForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_CHANGE_SIM_PIN, true,
            3, oldPin.c_str(), newPin.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::changeIccPin2ForApp(int32_t serial, const hidl_string& oldPin2,
                                            const hidl_string& newPin2, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "changeIccPin2ForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_CHANGE_SIM_PIN2, true,
            3, oldPin2.c_str(), newPin2.c_str(), aid.c_str());
    return Void();
}

Return<void> RadioImpl::supplyNetworkDepersonalization(int32_t serial,
                                                       const hidl_string& netPin) {
    mtkLogD(LOG_TAG, "supplyNetworkDepersonalization: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_NETWORK_DEPERSONALIZATION, true,
            1, netPin.c_str());
    return Void();
}

Return<void> RadioImpl::getCurrentCalls(int32_t serial) {
    mtkLogD(LOG_TAG, "getCurrentCalls: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_CURRENT_CALLS);
    return Void();
}

Return<void> RadioImpl::dial(int32_t serial, const Dial& dialInfo) {
    mtkLogD(LOG_TAG, "dial: serial %d", serial);

    int requestId = RIL_REQUEST_DIAL;
    if(isImsSlot(mSlotId)) {
        requestId = RIL_REQUEST_IMS_DIAL;
    }

    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, requestId);
    if (pRI == NULL) {
        return Void();
    }
    RIL_Dial dial = {};
    RIL_UUS_Info uusInfo = {};
    int32_t sizeOfDial = sizeof(dial);

    if (!copyHidlStringToRil(&dial.address, dialInfo.address, pRI)) {
        return Void();
    }
    dial.clir = (int) dialInfo.clir;

    if (dialInfo.uusInfo.size() != 0) {
        uusInfo.uusType = (RIL_UUS_Type) dialInfo.uusInfo[0].uusType;
        uusInfo.uusDcs = (RIL_UUS_DCS) dialInfo.uusInfo[0].uusDcs;

        if (dialInfo.uusInfo[0].uusData.size() == 0) {
            uusInfo.uusData = NULL;
            uusInfo.uusLength = 0;
        } else {
            if (!copyHidlStringToRil(&uusInfo.uusData, dialInfo.uusInfo[0].uusData, pRI)) {
                memsetAndFreeStrings(1, dial.address);
                return Void();
            }
            uusInfo.uusLength = dialInfo.uusInfo[0].uusData.size();
        }

        dial.uusInfo = &uusInfo;
    }

    CALL_ONREQUEST(requestId, &dial, sizeOfDial, pRI, mSlotId);

    memsetAndFreeStrings(2, dial.address, uusInfo.uusData);

    return Void();
}

Return<void> RadioImpl::getImsiForApp(int32_t serial, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "getImsiForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_GET_IMSI, false,
            1, aid.c_str());
    return Void();
}

Return<void> RadioImpl::hangup(int32_t serial, int32_t gsmIndex) {
    mtkLogD(LOG_TAG, "hangup: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_HANGUP, 1, gsmIndex);
    return Void();
}

Return<void> RadioImpl::hangupWaitingOrBackground(int32_t serial) {
    mtkLogD(LOG_TAG, "hangupWaitingOrBackground: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_HANGUP_WAITING_OR_BACKGROUND);
    return Void();
}

Return<void> RadioImpl::hangupForegroundResumeBackground(int32_t serial) {
    mtkLogD(LOG_TAG, "hangupForegroundResumeBackground: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_HANGUP_FOREGROUND_RESUME_BACKGROUND);
    return Void();
}

Return<void> RadioImpl::switchWaitingOrHoldingAndActive(int32_t serial) {
    mtkLogD(LOG_TAG, "switchWaitingOrHoldingAndActive: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SWITCH_WAITING_OR_HOLDING_AND_ACTIVE);
    return Void();
}

Return<void> RadioImpl::conference(int32_t serial) {
    mtkLogD(LOG_TAG, "conference: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CONFERENCE);
    return Void();
}

Return<void> RadioImpl::rejectCall(int32_t serial) {
    mtkLogD(LOG_TAG, "rejectCall: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_UDUB);
    return Void();
}

Return<void> RadioImpl::getLastCallFailCause(int32_t serial) {
    mtkLogD(LOG_TAG, "getLastCallFailCause: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_LAST_CALL_FAIL_CAUSE);
    return Void();
}

Return<void> RadioImpl::getSignalStrength(int32_t serial) {
    mtkLogD(LOG_TAG, "getSignalStrength: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SIGNAL_STRENGTH);
    return Void();
}

Return<void> RadioImpl::getSignalStrengthWithWcdmaEcio(int32_t serial) {
    mtkLogD(LOG_TAG, "getSignalStrengthWithWcdmaEcio: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SIGNAL_STRENGTH_WITH_WCDMA_ECIO);
    return Void();
}

Return<void> RadioImpl::getVoiceRegistrationState(int32_t serial) {
    mtkLogD(LOG_TAG, "getVoiceRegistrationState: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_VOICE_REGISTRATION_STATE);
    return Void();
}

Return<void> RadioImpl::getDataRegistrationState(int32_t serial) {
    mtkLogD(LOG_TAG, "getDataRegistrationState: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_DATA_REGISTRATION_STATE);
    return Void();
}

Return<void> RadioImpl::getOperator(int32_t serial) {
    mtkLogD(LOG_TAG, "getOperator: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_OPERATOR);
    return Void();
}

Return<void> RadioImpl::setRadioPower(int32_t serial, bool on) {
    mtkLogD(LOG_TAG, "setRadioPower: serial %d on %d", serial, on);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RADIO_POWER, 1, BOOL_TO_INT(on));
    return Void();
}

Return<void> RadioImpl::sendDtmf(int32_t serial, const hidl_string& s) {
    mtkLogD(LOG_TAG, "sendDtmf: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_DTMF, s.c_str());
    return Void();
}

Return<void> RadioImpl::sendSms(int32_t serial, const GsmSmsMessage& message) {
    mtkLogD(LOG_TAG, "sendSms: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_SMS, false,
            2, message.smscPdu.c_str(), message.pdu.c_str());
    return Void();
}

Return<void> RadioImpl::sendSMSExpectMore(int32_t serial, const GsmSmsMessage& message) {
    mtkLogD(LOG_TAG, "sendSMSExpectMore: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_SMS_EXPECT_MORE, false,
            2, message.smscPdu.c_str(), message.pdu.c_str());
    return Void();
}

static bool convertMvnoTypeToString(MvnoType type, char *&str) {
    switch (type) {
        case MvnoType::IMSI:
            str = (char *)"imsi";
            return true;
        case MvnoType::GID:
            str = (char *)"gid";
            return true;
        case MvnoType::SPN:
            str = (char *)"spn";
            return true;
        case MvnoType::NONE:
            str = (char *)"";
            return true;
    }
    return false;
}

Return<void> RadioImpl::setupDataCall(int32_t serial, RadioTechnology radioTechnology,
                                      const DataProfileInfo& dataProfileInfo, bool modemCognitive,
                                      bool roamingAllowed, bool isRoaming) {

    mtkLogD(LOG_TAG, "setupDataCall: serial %d", serial);

    if (s_vendorFunctions->version >= 4 && s_vendorFunctions->version <= 14) {
        const hidl_string &protocol =
                (isRoaming ? dataProfileInfo.roamingProtocol : dataProfileInfo.protocol);
        dispatchStrings(serial, mSlotId, RIL_REQUEST_SETUP_DATA_CALL, false, 7,
            std::to_string((int) radioTechnology + 2).c_str(),
            std::to_string((int) dataProfileInfo.profileId).c_str(),
            dataProfileInfo.apn.c_str(),
            dataProfileInfo.user.c_str(),
            dataProfileInfo.password.c_str(),
            std::to_string((int) dataProfileInfo.authType).c_str(),
            protocol.c_str());
    } else if (s_vendorFunctions->version >= 15) {
        char *mvnoTypeStr = NULL;
        if (!convertMvnoTypeToString(dataProfileInfo.mvnoType, mvnoTypeStr)) {
            RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                    RIL_REQUEST_SETUP_DATA_CALL);
            if (pRI != NULL) {
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            }
            return Void();
        }
        dispatchStrings(serial, mSlotId, RIL_REQUEST_SETUP_DATA_CALL, false, 15,
            std::to_string((int) radioTechnology + 2).c_str(),
            std::to_string((int) dataProfileInfo.profileId).c_str(),
            dataProfileInfo.apn.c_str(),
            dataProfileInfo.user.c_str(),
            dataProfileInfo.password.c_str(),
            std::to_string((int) dataProfileInfo.authType).c_str(),
            dataProfileInfo.protocol.c_str(),
            dataProfileInfo.roamingProtocol.c_str(),
            std::to_string(dataProfileInfo.supportedApnTypesBitmap).c_str(),
            std::to_string(dataProfileInfo.bearerBitmap).c_str(),
            modemCognitive ? "1" : "0",
            std::to_string(dataProfileInfo.mtu).c_str(),
            mvnoTypeStr,
            dataProfileInfo.mvnoMatchData.c_str(),
            roamingAllowed ? "1" : "0");
    } else {
        mtkLogE(LOG_TAG, "Unsupported RIL version %d, min version expected 4", s_vendorFunctions->version);
        RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                RIL_REQUEST_SETUP_DATA_CALL);
        if (pRI != NULL) {
            sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
        }
    }
    return Void();
}

Return<void> RadioImpl::iccIOForApp(int32_t serial, const IccIo& iccIo) {
    mtkLogD(LOG_TAG, "iccIOForApp: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SIM_IO);
    if (pRI == NULL) {
        return Void();
    }

    RIL_SIM_IO_v6 rilIccIo = {};
    rilIccIo.command = iccIo.command;
    rilIccIo.fileid = iccIo.fileId;
    if (!copyHidlStringToRil(&rilIccIo.path, iccIo.path, pRI)) {
        return Void();
    }

    rilIccIo.p1 = iccIo.p1;
    rilIccIo.p2 = iccIo.p2;
    rilIccIo.p3 = iccIo.p3;

    if (!copyHidlStringToRil(&rilIccIo.data, iccIo.data, pRI)) {
        memsetAndFreeStrings(1, rilIccIo.path);
        return Void();
    }

    if (!copyHidlStringToRil(&rilIccIo.pin2, iccIo.pin2, pRI)) {
        memsetAndFreeStrings(2, rilIccIo.path, rilIccIo.data);
        return Void();
    }

    if (!copyHidlStringToRil(&rilIccIo.aidPtr, iccIo.aid, pRI)) {
        memsetAndFreeStrings(3, rilIccIo.path, rilIccIo.data, rilIccIo.pin2);
        return Void();
    }

    CALL_ONREQUEST(RIL_REQUEST_SIM_IO, &rilIccIo, sizeof(rilIccIo), pRI, mSlotId);

    memsetAndFreeStrings(4, rilIccIo.path, rilIccIo.data, rilIccIo.pin2, rilIccIo.aidPtr);

    return Void();
}

Return<void> RadioImpl::sendUssd(int32_t serial, const hidl_string& ussd) {
    mtkLogD(LOG_TAG, "sendUssd: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_SEND_USSD, ussd.c_str());
    return Void();
}

Return<void> RadioImpl::cancelPendingUssd(int32_t serial) {
    mtkLogD(LOG_TAG, "cancelPendingUssd: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CANCEL_USSD);
    return Void();
}

Return<void> RadioImpl::getClir(int32_t serial) {
    mtkLogD(LOG_TAG, "getClir: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_CLIR);
    return Void();
}

Return<void> RadioImpl::setClir(int32_t serial, int32_t status) {
    mtkLogD(LOG_TAG, "setClir: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_CLIR, 1, status);
    return Void();
}

Return<void> RadioImpl::getCallForwardStatus(int32_t serial, const CallForwardInfo& callInfo) {
    mtkLogD(LOG_TAG, "getCallForwardStatus: serial %d", serial);
    dispatchCallForwardStatus(serial, mSlotId, RIL_REQUEST_QUERY_CALL_FORWARD_STATUS,
            callInfo);
    return Void();
}

Return<void> RadioImpl::setCallForward(int32_t serial, const CallForwardInfo& callInfo) {
    mtkLogD(LOG_TAG, "setCallForward: serial %d", serial);
    dispatchCallForwardStatus(serial, mSlotId, RIL_REQUEST_SET_CALL_FORWARD,
            callInfo);
    return Void();
}

Return<void> RadioImpl::setColp(int32_t serial, int32_t status) {
    mtkLogD(LOG_TAG, "setColp: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_COLP, 1, status);
    return Void();
}

Return<void> RadioImpl::setColr(int32_t serial, int32_t status) {
    mtkLogD(LOG_TAG, "setColr: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_COLR, 1, status);
    return Void();
}

Return<void> RadioImpl::queryCallForwardInTimeSlotStatus(int32_t serial,
                                                         const CallForwardInfoEx& callInfoEx) {
    mtkLogD(LOG_TAG, "queryCallForwardInTimeSlotStatus: serial %d", serial);
    dispatchCallForwardInTimeSlotStatus(serial, mSlotId, RIL_REQUEST_QUERY_CALL_FORWARD_IN_TIME_SLOT,
            callInfoEx);
    return Void();
}

Return<void> RadioImpl::setCallForwardInTimeSlot(int32_t serial,
                                                 const CallForwardInfoEx& callInfoEx) {
    mtkLogD(LOG_TAG, "setCallForwardInTimeSlot: serial %d", serial);
    dispatchCallForwardInTimeSlotStatus(serial, mSlotId, RIL_REQUEST_SET_CALL_FORWARD_IN_TIME_SLOT,
            callInfoEx);
    return Void();
}

Return<void> RadioImpl::runGbaAuthentication(int32_t serial,
            const hidl_string& nafFqdn, const hidl_string& nafSecureProtocolId,
            bool forceRun, int32_t netId) {
    mtkLogD(LOG_TAG, "runGbaAuthentication: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_RUN_GBA, true, 4,
                    nafFqdn.c_str(), nafSecureProtocolId.c_str(),
                    forceRun ? "1" : "0", (std::to_string(netId)).c_str());
    return Void();
}

Return<void> RadioImpl::getCallWaiting(int32_t serial, int32_t serviceClass) {
    mtkLogD(LOG_TAG, "getCallWaiting: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_QUERY_CALL_WAITING, 1, serviceClass);
    return Void();
}

Return<void> RadioImpl::setCallWaiting(int32_t serial, bool enable, int32_t serviceClass) {
    mtkLogD(LOG_TAG, "setCallWaiting: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_CALL_WAITING, 2, BOOL_TO_INT(enable),
            serviceClass);
    return Void();
}

Return<void> RadioImpl::acknowledgeLastIncomingGsmSms(int32_t serial,
                                                      bool success, SmsAcknowledgeFailCause cause) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingGsmSms: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SMS_ACKNOWLEDGE, 2, BOOL_TO_INT(success),
            cause);
    return Void();
}

Return<void> RadioImpl::acceptCall(int32_t serial) {
    mtkLogD(LOG_TAG, "acceptCall: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_ANSWER);
    return Void();
}

Return<void> RadioImpl::deactivateDataCall(int32_t serial,
                                           int32_t cid, bool reasonRadioShutDown) {
    mtkLogD(LOG_TAG, "deactivateDataCall: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_DEACTIVATE_DATA_CALL, false,
            2, (const char *) (std::to_string(cid)).c_str(), reasonRadioShutDown ? "2" : "1");
    return Void();
}

Return<void> RadioImpl::getFacilityLockForApp(int32_t serial, const hidl_string& facility,
                                              const hidl_string& password, int32_t serviceClass,
                                              const hidl_string& appId) {
    mtkLogD(LOG_TAG, "getFacilityLockForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_QUERY_FACILITY_LOCK, true,
            4, facility.c_str(), password.c_str(),
            (std::to_string(serviceClass)).c_str(), appId.c_str());
    return Void();
}

Return<void> RadioImpl::setFacilityLockForApp(int32_t serial, const hidl_string& facility,
                                              bool lockState, const hidl_string& password,
                                              int32_t serviceClass, const hidl_string& appId) {
    mtkLogD(LOG_TAG, "setFacilityLockForApp: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_FACILITY_LOCK, true,
            5, facility.c_str(), lockState ? "1" : "0", password.c_str(),
            (std::to_string(serviceClass)).c_str(), appId.c_str() );
    return Void();
}

Return<void> RadioImpl::setBarringPassword(int32_t serial, const hidl_string& facility,
                                           const hidl_string& oldPassword,
                                           const hidl_string& newPassword) {
    mtkLogD(LOG_TAG, "setBarringPassword: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_CHANGE_BARRING_PASSWORD, true,
            3, facility.c_str(), oldPassword.c_str(), newPassword.c_str());
    return Void();
}

Return<void> RadioImpl::getNetworkSelectionMode(int32_t serial) {
    mtkLogD(LOG_TAG, "getNetworkSelectionMode: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_NETWORK_SELECTION_MODE);
    return Void();
}

Return<void> RadioImpl::setNetworkSelectionModeAutomatic(int32_t serial) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeAutomatic: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SET_NETWORK_SELECTION_AUTOMATIC);
    return Void();
}

Return<void> RadioImpl::setNetworkSelectionModeManual(int32_t serial,
                                                      const hidl_string& operatorNumeric) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeManual: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_SET_NETWORK_SELECTION_MANUAL,
            operatorNumeric.c_str());
    return Void();
}

Return<void> RadioImpl::setNetworkSelectionModeManualWithAct(int32_t serial,
                                                      const hidl_string& operatorNumeric,
                                                      const hidl_string& act,
                                                      const hidl_string& mode) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeManualWithAct: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_NETWORK_SELECTION_MANUAL_WITH_ACT, true,
            3, operatorNumeric.c_str(), act.c_str(), mode.c_str());
    return Void();
}

Return<void> RadioImpl::getAvailableNetworks(int32_t serial) {
    mtkLogD(LOG_TAG, "getAvailableNetworks: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_AVAILABLE_NETWORKS);
    return Void();
}

Return<void> RadioImpl::getAvailableNetworksWithAct(int32_t serial) {
    mtkLogD(LOG_TAG, "getAvailableNetworksWithAct: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_AVAILABLE_NETWORKS_WITH_ACT);
    return Void();
}

Return<void> RadioImpl::cancelAvailableNetworks(int32_t serial) {
    mtkLogD(LOG_TAG, "cancelAvailableNetworks: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_ABORT_QUERY_AVAILABLE_NETWORKS);
    return Void();
}

Return<void> RadioImpl::startNetworkScan(int32_t serial, const AOSP_V1_1::NetworkScanRequest& request) {
    mtkLogD(LOG_TAG, "startNetworkScan: serial %d", serial);

    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_START_NETWORK_SCAN);
    if (pRI == NULL) {
        return Void();
    }

    if (request.specifiers.size() > MAX_RADIO_ACCESS_NETWORKS) {
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return Void();
    }

    RIL_NetworkScanRequest scan_request = {};

    scan_request.type = (RIL_ScanType) request.type;
    scan_request.interval = request.interval;
    scan_request.specifiers_length = request.specifiers.size();
    for (size_t i = 0; i < request.specifiers.size(); ++i) {
        if (request.specifiers[i].geranBands.size() > MAX_BANDS ||
            request.specifiers[i].utranBands.size() > MAX_BANDS ||
            request.specifiers[i].eutranBands.size() > MAX_BANDS ||
            request.specifiers[i].channels.size() > MAX_CHANNELS) {
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            return Void();
        }
        const AOSP_V1_1::RadioAccessSpecifier& ras_from =
                request.specifiers[i];
        RIL_RadioAccessSpecifier& ras_to = scan_request.specifiers[i];

        ras_to.radio_access_network = (RIL_RadioAccessNetworks) ras_from.radioAccessNetwork;
        ras_to.channels_length = ras_from.channels.size();

        std::copy(ras_from.channels.begin(), ras_from.channels.end(), ras_to.channels);
        const std::vector<uint32_t> * bands = nullptr;
        switch (request.specifiers[i].radioAccessNetwork) {
            case AOSP_V1_1::RadioAccessNetworks::GERAN:
                ras_to.bands_length = ras_from.geranBands.size();
                bands = (std::vector<uint32_t> *) &ras_from.geranBands;
                break;
            case AOSP_V1_1::RadioAccessNetworks::UTRAN:
                ras_to.bands_length = ras_from.utranBands.size();
                bands = (std::vector<uint32_t> *) &ras_from.utranBands;
                break;
            case AOSP_V1_1::RadioAccessNetworks::EUTRAN:
                ras_to.bands_length = ras_from.eutranBands.size();
                bands = (std::vector<uint32_t> *) &ras_from.eutranBands;
                break;
            default:
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
        }
        // safe to copy to geran_bands because it's a union member
        for (size_t idx = 0; idx < ras_to.bands_length; ++idx) {
            ras_to.bands.geran_bands[idx] = (RIL_GeranBands) (*bands)[idx];
        }
    }
    scan_request.maxSearchTime = 0;
    scan_request.incrementalResults = 0;
    scan_request.incrementalResultsPeriodicity = 0;
    scan_request.mccMncs = NULL;
    scan_request.mccMncs_length = 0;

    CALL_ONREQUEST(RIL_REQUEST_START_NETWORK_SCAN, &scan_request, sizeof(scan_request), pRI,
            mSlotId);

    return Void();
}

Return<void> RadioImpl::stopNetworkScan(int32_t serial) {
    mtkLogD(LOG_TAG, "stopNetworkScan: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_STOP_NETWORK_SCAN);
    return Void();
}

Return<void> RadioImpl::startDtmf(int32_t serial, const hidl_string& s) {
    mtkLogD(LOG_TAG, "startDtmf: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_DTMF_START,
            s.c_str());
    return Void();
}

Return<void> RadioImpl::stopDtmf(int32_t serial) {
    mtkLogD(LOG_TAG, "stopDtmf: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_DTMF_STOP);
    return Void();
}

Return<void> RadioImpl::getBasebandVersion(int32_t serial) {
    mtkLogD(LOG_TAG, "getBasebandVersion: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_BASEBAND_VERSION);
    return Void();
}

Return<void> RadioImpl::separateConnection(int32_t serial, int32_t gsmIndex) {
    mtkLogD(LOG_TAG, "separateConnection: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SEPARATE_CONNECTION, 1, gsmIndex);
    return Void();
}

Return<void> RadioImpl::setMute(int32_t serial, bool enable) {
    mtkLogD(LOG_TAG, "setMute: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_MUTE, 1, BOOL_TO_INT(enable));
    return Void();
}

Return<void> RadioImpl::getMute(int32_t serial) {
    mtkLogD(LOG_TAG, "getMute: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_MUTE);
    return Void();
}

Return<void> RadioImpl::setBarringPasswordCheckedByNW(int32_t serial, const hidl_string& facility,
                                           const hidl_string& oldPassword,
                                           const hidl_string& newPassword,
                                           const hidl_string& cfmPassword) {
    mtkLogD(LOG_TAG, "setBarringPasswordCheckedByNW: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_CHANGE_BARRING_PASSWORD, true,
            4, facility.c_str(), oldPassword.c_str(), newPassword.c_str(), cfmPassword.c_str());
    return Void();
}

Return<void> RadioImpl::getClip(int32_t serial) {
    mtkLogD(LOG_TAG, "getClip: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_CLIP);
    return Void();
}

Return<void> RadioImpl::setClip(int32_t serial, int32_t clipEnable) {
    mtkLogD(LOG_TAG, "setClip: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_CLIP, 1, clipEnable);
    return Void();
}

Return<void> RadioImpl::getColp(int32_t serial) {
    mtkLogD(LOG_TAG, "getColp: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_COLP);
    return Void();
}

Return<void> RadioImpl::getColr(int32_t serial) {
    mtkLogD(LOG_TAG, "getColr: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_COLR);
    return Void();
}

Return<void> RadioImpl::sendCnap(int32_t serial, const hidl_string& cnapMessage) {
    mtkLogD(LOG_TAG, "sendCnap: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_SEND_CNAP, cnapMessage.c_str());
    return Void();
}

Return<void> RadioImpl::getDataCallList(int32_t serial) {
    mtkLogD(LOG_TAG, "getDataCallList: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_DATA_CALL_LIST);
    return Void();
}

Return<void> RadioImpl::setSuppServiceNotifications(int32_t serial, bool enable) {
    mtkLogD(LOG_TAG, "setSuppServiceNotifications: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SUPP_SVC_NOTIFICATION, 1,
            BOOL_TO_INT(enable));
    return Void();
}

Return<void> RadioImpl::writeSmsToSim(int32_t serial, const SmsWriteArgs& smsWriteArgs) {
    mtkLogD(LOG_TAG, "writeSmsToSim: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_WRITE_SMS_TO_SIM);
    if (pRI == NULL) {
        return Void();
    }

    RIL_SMS_WriteArgs args;
    args.status = (int) smsWriteArgs.status;

    if (!copyHidlStringToRil(&args.pdu, smsWriteArgs.pdu, pRI)) {
        return Void();
    }

    if (!copyHidlStringToRil(&args.smsc, smsWriteArgs.smsc, pRI)) {
        memsetAndFreeStrings(1, args.pdu);
        return Void();
    }

    CALL_ONREQUEST(RIL_REQUEST_WRITE_SMS_TO_SIM, &args, sizeof(args), pRI, mSlotId);

    memsetAndFreeStrings(2, args.smsc, args.pdu);

    return Void();
}

Return<void> RadioImpl::deleteSmsOnSim(int32_t serial, int32_t index) {
    mtkLogD(LOG_TAG, "deleteSmsOnSim: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_DELETE_SMS_ON_SIM, 1, index);
    return Void();
}

Return<void> RadioImpl::setBandMode(int32_t serial, RadioBandMode mode) {
    mtkLogD(LOG_TAG, "setBandMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_BAND_MODE, 1, mode);
    return Void();
}

Return<void> RadioImpl::getAvailableBandModes(int32_t serial) {
    mtkLogD(LOG_TAG, "getAvailableBandModes: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_AVAILABLE_BAND_MODE);
    return Void();
}

Return<void> RadioImpl::sendEnvelope(int32_t serial, const hidl_string& command) {
    mtkLogD(LOG_TAG, "sendEnvelope: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_STK_SEND_ENVELOPE_COMMAND,
            command.c_str());
    return Void();
}

Return<void> RadioImpl::sendTerminalResponseToSim(int32_t serial,
                                                  const hidl_string& commandResponse) {
    mtkLogD(LOG_TAG, "sendTerminalResponseToSim: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_STK_SEND_TERMINAL_RESPONSE,
            commandResponse.c_str());
    return Void();
}

Return<void> RadioImpl::handleStkCallSetupRequestFromSim(int32_t serial, bool accept) {
    mtkLogD(LOG_TAG, "handleStkCallSetupRequestFromSim: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM,
            1, BOOL_TO_INT(accept));
    return Void();
}

Return<void> RadioImpl::explicitCallTransfer(int32_t serial) {
    mtkLogD(LOG_TAG, "explicitCallTransfer: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_EXPLICIT_CALL_TRANSFER);
    return Void();
}

Return<void> RadioImpl::setPreferredNetworkType(int32_t serial, PreferredNetworkType nwType) {
    mtkLogD(LOG_TAG, "setPreferredNetworkType: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_PREFERRED_NETWORK_TYPE, 1, nwType);
    return Void();
}

Return<void> RadioImpl::getPreferredNetworkType(int32_t serial) {
    mtkLogD(LOG_TAG, "getPreferredNetworkType: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_PREFERRED_NETWORK_TYPE);
    return Void();
}

Return<void> RadioImpl::getNeighboringCids(int32_t serial) {
    mtkLogD(LOG_TAG, "getNeighboringCids: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_NEIGHBORING_CELL_IDS);
    return Void();
}

Return<void> RadioImpl::setLocationUpdates(int32_t serial, bool enable) {
    mtkLogD(LOG_TAG, "setLocationUpdates: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_LOCATION_UPDATES, 1, BOOL_TO_INT(enable));
    return Void();
}

Return<void> RadioImpl::setCdmaSubscriptionSource(int32_t serial, CdmaSubscriptionSource cdmaSub) {
    mtkLogD(LOG_TAG, "setCdmaSubscriptionSource: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CDMA_SET_SUBSCRIPTION_SOURCE, 1, cdmaSub);
    return Void();
}

Return<void> RadioImpl::setCdmaRoamingPreference(int32_t serial, CdmaRoamingType type) {
    mtkLogD(LOG_TAG, "setCdmaRoamingPreference: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CDMA_SET_ROAMING_PREFERENCE, 1, type);
    return Void();
}

Return<void> RadioImpl::getCdmaRoamingPreference(int32_t serial) {
    mtkLogD(LOG_TAG, "getCdmaRoamingPreference: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CDMA_QUERY_ROAMING_PREFERENCE);
    return Void();
}

Return<void> RadioImpl::setTTYMode(int32_t serial, TtyMode mode) {
    mtkLogD(LOG_TAG, "setTTYMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_TTY_MODE, 1, mode);
    return Void();
}

Return<void> RadioImpl::getTTYMode(int32_t serial) {
    mtkLogD(LOG_TAG, "getTTYMode: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_TTY_MODE);
    return Void();
}

Return<void> RadioImpl::setPreferredVoicePrivacy(int32_t serial, bool enable) {
    mtkLogD(LOG_TAG, "setPreferredVoicePrivacy: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CDMA_SET_PREFERRED_VOICE_PRIVACY_MODE,
            1, BOOL_TO_INT(enable));
    return Void();
}

Return<void> RadioImpl::getPreferredVoicePrivacy(int32_t serial) {
    mtkLogD(LOG_TAG, "getPreferredVoicePrivacy: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CDMA_QUERY_PREFERRED_VOICE_PRIVACY_MODE);
    return Void();
}

Return<void> RadioImpl::sendCDMAFeatureCode(int32_t serial, const hidl_string& featureCode) {
    mtkLogD(LOG_TAG, "sendCDMAFeatureCode: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_CDMA_FLASH,
            featureCode.c_str());
    return Void();
}

Return<void> RadioImpl::sendBurstDtmf(int32_t serial, const hidl_string& dtmf, int32_t on,
                                      int32_t off) {
    mtkLogD(LOG_TAG, "sendBurstDtmf: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_CDMA_BURST_DTMF, false,
            3, dtmf.c_str(), (std::to_string(on)).c_str(),
            (std::to_string(off)).c_str());
    return Void();
}

void constructCdmaSms(RIL_CDMA_SMS_Message &rcsm, const CdmaSmsMessage& sms) {
    rcsm.uTeleserviceID = sms.teleserviceId;
    rcsm.bIsServicePresent = BOOL_TO_INT(sms.isServicePresent);
    rcsm.uServicecategory = sms.serviceCategory;
    rcsm.sAddress.digit_mode = (RIL_CDMA_SMS_DigitMode) sms.address.digitMode;
    rcsm.sAddress.number_mode = (RIL_CDMA_SMS_NumberMode) sms.address.numberMode;
    rcsm.sAddress.number_type = (RIL_CDMA_SMS_NumberType) sms.address.numberType;
    rcsm.sAddress.number_plan = (RIL_CDMA_SMS_NumberPlan) sms.address.numberPlan;

    rcsm.sAddress.number_of_digits = sms.address.digits.size();
    int digitLimit= MIN((rcsm.sAddress.number_of_digits), RIL_CDMA_SMS_ADDRESS_MAX);
    for (int i = 0; i < digitLimit; i++) {
        rcsm.sAddress.digits[i] = sms.address.digits[i];
    }

    rcsm.sSubAddress.subaddressType = (RIL_CDMA_SMS_SubaddressType) sms.subAddress.subaddressType;
    rcsm.sSubAddress.odd = BOOL_TO_INT(sms.subAddress.odd);

    rcsm.sSubAddress.number_of_digits = sms.subAddress.digits.size();
    digitLimit= MIN((rcsm.sSubAddress.number_of_digits), RIL_CDMA_SMS_SUBADDRESS_MAX);
    for (int i = 0; i < digitLimit; i++) {
        rcsm.sSubAddress.digits[i] = sms.subAddress.digits[i];
    }

    rcsm.uBearerDataLen = sms.bearerData.size();
    digitLimit= MIN((rcsm.uBearerDataLen), RIL_CDMA_SMS_BEARER_DATA_MAX);
    for (int i = 0; i < digitLimit; i++) {
        rcsm.aBearerData[i] = sms.bearerData[i];
    }
}

Return<void> RadioImpl::sendCdmaSms(int32_t serial, const CdmaSmsMessage& sms) {
    mtkLogD(LOG_TAG, "sendCdmaSms: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_CDMA_SEND_SMS);
    if (pRI == NULL) {
        return Void();
    }

    RIL_CDMA_SMS_Message rcsm = {};
    constructCdmaSms(rcsm, sms);

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rcsm, sizeof(rcsm), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::acknowledgeLastIncomingCdmaSms(int32_t serial, const CdmaSmsAck& smsAck) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingCdmaSms: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_CDMA_SMS_ACKNOWLEDGE);
    if (pRI == NULL) {
        return Void();
    }

    RIL_CDMA_SMS_Ack rcsa = {};

    rcsa.uErrorClass = (RIL_CDMA_SMS_ErrorClass) smsAck.errorClass;
    rcsa.uSMSCauseCode = smsAck.smsCauseCode;

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rcsa, sizeof(rcsa), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::acknowledgeLastIncomingCdmaSmsEx(int32_t serial, const CdmaSmsAck& smsAck) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingCdmaSmsEx: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(
            serial, mSlotId, RIL_REQUEST_CDMA_SMS_ACKNOWLEDGE_EX);
    if (pRI == NULL) {
        return Void();
    }

    RIL_CDMA_SMS_Ack rcsa = {};

    rcsa.uErrorClass = (RIL_CDMA_SMS_ErrorClass) smsAck.errorClass;
    rcsa.uSMSCauseCode = smsAck.smsCauseCode;

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rcsa, sizeof(rcsa), pRI, mSlotId);
    return Void();
}


Return<void> RadioImpl::getGsmBroadcastConfig(int32_t serial) {
    mtkLogD(LOG_TAG, "getGsmBroadcastConfig: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GSM_GET_BROADCAST_SMS_CONFIG);
    return Void();
}

Return<void> RadioImpl::setGsmBroadcastConfig(int32_t serial,
                                              const hidl_vec<GsmBroadcastSmsConfigInfo>&
                                              configInfo) {
    mtkLogD(LOG_TAG, "setGsmBroadcastConfig: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_GSM_SET_BROADCAST_SMS_CONFIG);
    if (pRI == NULL) {
        return Void();
    }

    int num = configInfo.size();
    RIL_GSM_BroadcastSmsConfigInfo gsmBci[num];
    RIL_GSM_BroadcastSmsConfigInfo *gsmBciPtrs[num];

    for (int i = 0 ; i < num ; i++ ) {
        gsmBciPtrs[i] = &gsmBci[i];
        gsmBci[i].fromServiceId = configInfo[i].fromServiceId;
        gsmBci[i].toServiceId = configInfo[i].toServiceId;
        gsmBci[i].fromCodeScheme = configInfo[i].fromCodeScheme;
        gsmBci[i].toCodeScheme = configInfo[i].toCodeScheme;
        gsmBci[i].selected = BOOL_TO_INT(configInfo[i].selected);
    }

    CALL_ONREQUEST(pRI->pCI->requestNumber, gsmBciPtrs,
            num * sizeof(RIL_GSM_BroadcastSmsConfigInfo *), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::setGsmBroadcastActivation(int32_t serial, bool activate) {
    mtkLogD(LOG_TAG, "setGsmBroadcastActivation: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_GSM_SMS_BROADCAST_ACTIVATION,
            1, BOOL_TO_INT(!activate));
    return Void();
}

Return<void> RadioImpl::getCdmaBroadcastConfig(int32_t serial) {
    mtkLogD(LOG_TAG, "getCdmaBroadcastConfig: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CDMA_GET_BROADCAST_SMS_CONFIG);
    return Void();
}

Return<void> RadioImpl::setCdmaBroadcastConfig(int32_t serial,
                                               const hidl_vec<CdmaBroadcastSmsConfigInfo>&
                                               configInfo) {
    mtkLogD(LOG_TAG, "setCdmaBroadcastConfig: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_CDMA_SET_BROADCAST_SMS_CONFIG);
    if (pRI == NULL) {
        return Void();
    }

    int num = configInfo.size();
    RIL_CDMA_BroadcastSmsConfigInfo cdmaBci[num];
    RIL_CDMA_BroadcastSmsConfigInfo *cdmaBciPtrs[num];

    for (int i = 0 ; i < num ; i++ ) {
        cdmaBciPtrs[i] = &cdmaBci[i];
        cdmaBci[i].service_category = configInfo[i].serviceCategory;
        cdmaBci[i].language = configInfo[i].language;
        cdmaBci[i].selected = BOOL_TO_INT(configInfo[i].selected);
    }

    CALL_ONREQUEST(pRI->pCI->requestNumber, cdmaBciPtrs,
            num * sizeof(RIL_CDMA_BroadcastSmsConfigInfo *), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::setCdmaBroadcastActivation(int32_t serial, bool activate) {
    mtkLogD(LOG_TAG, "setCdmaBroadcastActivation: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CDMA_SMS_BROADCAST_ACTIVATION,
            1, BOOL_TO_INT(!activate));
    return Void();
}

Return<void> RadioImpl::getCDMASubscription(int32_t serial) {
    mtkLogD(LOG_TAG, "getCDMASubscription: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CDMA_SUBSCRIPTION);
    return Void();
}

Return<void> RadioImpl::writeSmsToRuim(int32_t serial, const CdmaSmsWriteArgs& cdmaSms) {
    mtkLogD(LOG_TAG, "writeSmsToRuim: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_CDMA_WRITE_SMS_TO_RUIM);
    if (pRI == NULL) {
        return Void();
    }

    RIL_CDMA_SMS_WriteArgs rcsw = {};
    rcsw.status = (int) cdmaSms.status;
    constructCdmaSms(rcsw.message, cdmaSms.message);

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rcsw, sizeof(rcsw), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::deleteSmsOnRuim(int32_t serial, int32_t index) {
    mtkLogD(LOG_TAG, "deleteSmsOnRuim: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CDMA_DELETE_SMS_ON_RUIM, 1, index);
    return Void();
}

Return<void> RadioImpl::getDeviceIdentity(int32_t serial) {
    mtkLogD(LOG_TAG, "getDeviceIdentity: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_DEVICE_IDENTITY);
    return Void();
}

Return<void> RadioImpl::exitEmergencyCallbackMode(int32_t serial) {
    mtkLogD(LOG_TAG, "exitEmergencyCallbackMode: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_EXIT_EMERGENCY_CALLBACK_MODE);
    return Void();
}

Return<void> RadioImpl::getSmscAddress(int32_t serial) {
    mtkLogD(LOG_TAG, "getSmscAddress: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_SMSC_ADDRESS);
    return Void();
}

Return<void> RadioImpl::setSmscAddress(int32_t serial, const hidl_string& smsc) {
    mtkLogD(LOG_TAG, "setSmscAddress: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_SET_SMSC_ADDRESS,
            smsc.c_str());
    return Void();
}

Return<void> RadioImpl::reportSmsMemoryStatus(int32_t serial, bool available) {
    mtkLogD(LOG_TAG, "reportSmsMemoryStatus: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_REPORT_SMS_MEMORY_STATUS, 1,
            BOOL_TO_INT(available));
    return Void();
}

Return<void> RadioImpl::reportStkServiceIsRunning(int32_t serial) {
    mtkLogD(LOG_TAG, "reportStkServiceIsRunning: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_REPORT_STK_SERVICE_IS_RUNNING);
    return Void();
}

Return<void> RadioImpl::getCdmaSubscriptionSource(int32_t serial) {
    mtkLogD(LOG_TAG, "getCdmaSubscriptionSource: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CDMA_GET_SUBSCRIPTION_SOURCE);
    return Void();
}

Return<void> RadioImpl::requestIsimAuthentication(int32_t serial, const hidl_string& challenge) {
    mtkLogD(LOG_TAG, "requestIsimAuthentication: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_ISIM_AUTHENTICATION,
            challenge.c_str());
    return Void();
}

Return<void> RadioImpl::acknowledgeIncomingGsmSmsWithPdu(int32_t serial, bool success,
                                                         const hidl_string& ackPdu) {
    mtkLogD(LOG_TAG, "acknowledgeIncomingGsmSmsWithPdu: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ACKNOWLEDGE_INCOMING_GSM_SMS_WITH_PDU, false,
            2, success ? "1" : "0", ackPdu.c_str());
    return Void();
}

Return<void> RadioImpl::sendEnvelopeWithStatus(int32_t serial, const hidl_string& contents) {
    mtkLogD(LOG_TAG, "sendEnvelopeWithStatus: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_STK_SEND_ENVELOPE_WITH_STATUS,
            contents.c_str());
    return Void();
}

Return<void> RadioImpl::getVoiceRadioTechnology(int32_t serial) {
    mtkLogD(LOG_TAG, "getVoiceRadioTechnology: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_VOICE_RADIO_TECH);
    return Void();
}

Return<void> RadioImpl::getCellInfoList(int32_t serial) {
    mtkLogD(LOG_TAG, "getCellInfoList: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_CELL_INFO_LIST);
    return Void();
}

Return<void> RadioImpl::setCellInfoListRate(int32_t serial, int32_t rate) {
    mtkLogD(LOG_TAG, "setCellInfoListRate: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_UNSOL_CELL_INFO_LIST_RATE, 1, rate);
    return Void();
}

Return<void> RadioImpl::setInitialAttachApn(int32_t serial, const DataProfileInfo& dataProfileInfo,
                                            bool modemCognitive, bool isRoaming) {
    mtkLogD(LOG_TAG, "setInitialAttachApn: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_SET_INITIAL_ATTACH_APN);
    if (pRI == NULL) {
        return Void();
    }

    if (s_vendorFunctions->version <= 14) {
        RIL_InitialAttachApn iaa = {};

        if (!copyHidlStringToRil(&iaa.apn, dataProfileInfo.apn, pRI, true)) {
            return Void();
        }

        const hidl_string &protocol =
                (isRoaming ? dataProfileInfo.roamingProtocol : dataProfileInfo.protocol);

        if (!copyHidlStringToRil(&iaa.protocol, protocol, pRI)) {
            memsetAndFreeStrings(1, iaa.apn);
            return Void();
        }
        iaa.authtype = (int) dataProfileInfo.authType;
        if (!copyHidlStringToRil(&iaa.username, dataProfileInfo.user, pRI)) {
            memsetAndFreeStrings(2, iaa.apn, iaa.protocol);
            return Void();
        }
        if (!copyHidlStringToRil(&iaa.password, dataProfileInfo.password, pRI)) {
            memsetAndFreeStrings(3, iaa.apn, iaa.protocol, iaa.username);
            return Void();
        }

        CALL_ONREQUEST(RIL_REQUEST_SET_INITIAL_ATTACH_APN, &iaa, sizeof(iaa), pRI, mSlotId);

        memsetAndFreeStrings(4, iaa.apn, iaa.protocol, iaa.username, iaa.password);
    } else {
        RIL_InitialAttachApn_v15 iaa = {};

        if (!copyHidlStringToRil(&iaa.apn, dataProfileInfo.apn, pRI, true)) {
            return Void();
        }

        if (!copyHidlStringToRil(&iaa.protocol, dataProfileInfo.protocol, pRI)) {
            memsetAndFreeStrings(1, iaa.apn);
            return Void();
        }
        if (!copyHidlStringToRil(&iaa.roamingProtocol, dataProfileInfo.roamingProtocol, pRI)) {
            memsetAndFreeStrings(2, iaa.apn, iaa.protocol);
            return Void();
        }
        iaa.authtype = (int) dataProfileInfo.authType;
        if (!copyHidlStringToRil(&iaa.username, dataProfileInfo.user, pRI)) {
            memsetAndFreeStrings(3, iaa.apn, iaa.protocol, iaa.roamingProtocol);
            return Void();
        }
        if (!copyHidlStringToRil(&iaa.password, dataProfileInfo.password, pRI)) {
            memsetAndFreeStrings(4, iaa.apn, iaa.protocol, iaa.roamingProtocol, iaa.username);
            return Void();
        }
        iaa.supportedTypesBitmask = dataProfileInfo.supportedApnTypesBitmap;
        iaa.bearerBitmask = dataProfileInfo.bearerBitmap;
        iaa.modemCognitive = BOOL_TO_INT(modemCognitive);
        iaa.mtu = dataProfileInfo.mtu;

        if (!convertMvnoTypeToString(dataProfileInfo.mvnoType, iaa.mvnoType)) {
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            memsetAndFreeStrings(5, iaa.apn, iaa.protocol, iaa.roamingProtocol, iaa.username,
                    iaa.password);
            return Void();
        }

        if (!copyHidlStringToRil(&iaa.mvnoMatchData, dataProfileInfo.mvnoMatchData, pRI)) {
            memsetAndFreeStrings(5, iaa.apn, iaa.protocol, iaa.roamingProtocol, iaa.username,
                    iaa.password);
            return Void();
        }

        if (isMtkFwkAddonNotExisted(mSlotId)) {
            setDataProfileEx(serial, dataProfileInfo, isRoaming, mSlotId);
        }

        // M: added canHandleIms for legacy modem
        iaa.canHandleIms = BOOL_TO_INT((((iaa.supportedTypesBitmask & ApnTypes::IMS) != 0) &&
                ((iaa.supportedTypesBitmask ^ (int) MtkApnTypes::MTKALL) != 0)));

        CALL_ONREQUEST(RIL_REQUEST_SET_INITIAL_ATTACH_APN, &iaa, sizeof(iaa), pRI, mSlotId);

        memsetAndFreeStrings(6, iaa.apn, iaa.protocol, iaa.roamingProtocol, iaa.username,
                iaa.password, iaa.mvnoMatchData);
    }

    return Void();
}

Return<void> RadioImpl::getImsRegistrationState(int32_t serial) {
    mtkLogD(LOG_TAG, "getImsRegistrationState: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_IMS_REGISTRATION_STATE);
    return Void();
}

bool dispatchImsGsmSms(const ImsSmsMessage& message, RequestInfo *pRI) {
    RIL_IMS_SMS_Message rism = {};
    char **pStrings;
    int countStrings = 2;
    int dataLen = sizeof(char *) * countStrings;

    rism.tech = RADIO_TECH_3GPP;
    rism.retry = BOOL_TO_INT(message.retry);
    rism.messageRef = message.messageRef;

    if (message.gsmMessage.size() != 1) {
        mtkLogE(LOG_TAG, "dispatchImsGsmSms: Invalid len %s", requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return false;
    }

    pStrings = (char **)calloc(countStrings, sizeof(char *));
    if (pStrings == NULL) {
        mtkLogE(LOG_TAG, "dispatchImsGsmSms: Memory allocation failed for request %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }

    if (!copyHidlStringToRil(&pStrings[0], message.gsmMessage[0].smscPdu, pRI)) {
#ifdef MEMSET_FREED
        memset(pStrings, 0, dataLen);
#endif
        free(pStrings);
        return false;
    }

    if (!copyHidlStringToRil(&pStrings[1], message.gsmMessage[0].pdu, pRI)) {
        memsetAndFreeStrings(1, pStrings[0]);
#ifdef MEMSET_FREED
        memset(pStrings, 0, dataLen);
#endif
        free(pStrings);
        return false;
    }

    rism.message.gsmMessage = pStrings;
    CALL_ONREQUEST(pRI->pCI->requestNumber, &rism, sizeof(RIL_RadioTechnologyFamily) +
            sizeof(uint8_t) + sizeof(int32_t) + dataLen, pRI, pRI->socket_id);

    for (int i = 0 ; i < countStrings ; i++) {
        memsetAndFreeStrings(1, pStrings[i]);
    }

#ifdef MEMSET_FREED
    memset(pStrings, 0, dataLen);
#endif
    free(pStrings);

    return true;
}

struct ImsCdmaSms {
    RIL_IMS_SMS_Message imsSms;
    RIL_CDMA_SMS_Message cdmaSms;
};

bool dispatchImsCdmaSms(const ImsSmsMessage& message, RequestInfo *pRI) {
    ImsCdmaSms temp = {};

    if (message.cdmaMessage.size() != 1) {
        mtkLogE(LOG_TAG, "dispatchImsCdmaSms: Invalid len %s", requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return false;
    }

    temp.imsSms.tech = RADIO_TECH_3GPP2;
    temp.imsSms.retry = BOOL_TO_INT(message.retry);
    temp.imsSms.messageRef = message.messageRef;
    temp.imsSms.message.cdmaMessage = &temp.cdmaSms;

    constructCdmaSms(temp.cdmaSms, message.cdmaMessage[0]);

    // Vendor code expects payload length to include actual msg payload
    // (sizeof(RIL_CDMA_SMS_Message)) instead of (RIL_CDMA_SMS_Message *) + size of other fields in
    // RIL_IMS_SMS_Message
    int payloadLen = sizeof(RIL_RadioTechnologyFamily) + sizeof(uint8_t) + sizeof(int32_t)
            + sizeof(RIL_CDMA_SMS_Message);

    CALL_ONREQUEST(pRI->pCI->requestNumber, &temp.imsSms, payloadLen, pRI, pRI->socket_id);

    return true;
}

Return<void> RadioImpl::sendImsSms(int32_t serial, const ImsSmsMessage& message) {
    mtkLogD(LOG_TAG, "sendImsSms: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_IMS_SEND_SMS);
    if (pRI == NULL) {
        return Void();
    }

    RIL_RadioTechnologyFamily format = (RIL_RadioTechnologyFamily) message.tech;

    if (RADIO_TECH_3GPP == format) {
        dispatchImsGsmSms(message, pRI);
    } else if (RADIO_TECH_3GPP2 == format) {
        dispatchImsCdmaSms(message, pRI);
    } else {
        mtkLogE(LOG_TAG, "sendImsSms: Invalid radio tech %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
    }
    return Void();
}

Return<void> RadioImpl::iccTransmitApduBasicChannel(int32_t serial, const SimApdu& message) {
    mtkLogD(LOG_TAG, "[%d] iccTransmitApduBasicChannel: serial %d", mSlotId, serial);
    dispatchIccApdu(serial, mSlotId, RIL_REQUEST_SIM_TRANSMIT_APDU_BASIC, message);
    return Void();
}

Return<void> RadioImpl::iccOpenLogicalChannel(int32_t serial, const hidl_string& aid, int32_t p2) {
    mtkLogD(LOG_TAG, "[%d] iccOpenLogicalChannel: serial %d", mSlotId, serial);
    if (s_vendorFunctions->version < 15) {
        dispatchString(serial, mSlotId, RIL_REQUEST_SIM_OPEN_CHANNEL, aid.c_str());
    } else {
        RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SIM_OPEN_CHANNEL);
        if (pRI == NULL) {
            return Void();
        }

        RIL_OpenChannelParams params = {};

        params.p2 = p2;

        if (!copyHidlStringToRil(&params.aidPtr, aid, pRI)) {
            return Void();
        }

        CALL_ONREQUEST(pRI->pCI->requestNumber, &params, sizeof(params), pRI, mSlotId);

        memsetAndFreeStrings(1, params.aidPtr);
    }
    return Void();
}

Return<void> RadioImpl::iccCloseLogicalChannel(int32_t serial, int32_t channelId) {
    mtkLogD(LOG_TAG, "[%d] iccCloseLogicalChannel: serial %d", mSlotId, serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SIM_CLOSE_CHANNEL, 1, channelId);
    return Void();
}

Return<void> RadioImpl::iccTransmitApduLogicalChannel(int32_t serial, const SimApdu& message) {
    mtkLogD(LOG_TAG, "[%d] iccTransmitApduLogicalChannel: serial %d", mSlotId, serial);
    dispatchIccApdu(serial, mSlotId, RIL_REQUEST_SIM_TRANSMIT_APDU_CHANNEL, message);
    return Void();
}

Return<void> RadioImpl::nvReadItem(int32_t serial, NvItem itemId) {
    mtkLogD(LOG_TAG, "nvReadItem: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_NV_READ_ITEM);
    if (pRI == NULL) {
        return Void();
    }

    RIL_NV_ReadItem nvri = {};
    nvri.itemID = (RIL_NV_Item) itemId;

    CALL_ONREQUEST(pRI->pCI->requestNumber, &nvri, sizeof(nvri), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::nvWriteItem(int32_t serial, const NvWriteItem& item) {
    mtkLogD(LOG_TAG, "nvWriteItem: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_NV_WRITE_ITEM);
    if (pRI == NULL) {
        return Void();
    }

    RIL_NV_WriteItem nvwi = {};

    nvwi.itemID = (RIL_NV_Item) item.itemId;

    if (!copyHidlStringToRil(&nvwi.value, item.value, pRI)) {
        return Void();
    }

    CALL_ONREQUEST(pRI->pCI->requestNumber, &nvwi, sizeof(nvwi), pRI, mSlotId);

    memsetAndFreeStrings(1, nvwi.value);
    return Void();
}

Return<void> RadioImpl::nvWriteCdmaPrl(int32_t serial, const hidl_vec<uint8_t>& prl) {
    mtkLogD(LOG_TAG, "nvWriteCdmaPrl: serial %d", serial);
    dispatchRaw(serial, mSlotId, RIL_REQUEST_NV_WRITE_CDMA_PRL, prl);
    return Void();
}

Return<void> RadioImpl::nvResetConfig(int32_t serial, ResetNvType resetType) {
    int rilResetType = -1;
    mtkLogD(LOG_TAG, "nvResetConfig: serial %d", serial);
    /* Convert ResetNvType to RIL.h values
     * RIL_REQUEST_NV_RESET_CONFIG
     * 1 - reload all NV items
     * 2 - erase NV reset (SCRTN)
     * 3 - factory reset (RTN)
     */
    switch(resetType) {
      case ResetNvType::RELOAD:
        rilResetType = 1;
        break;
      case ResetNvType::ERASE:
        rilResetType = 2;
        break;
      case ResetNvType::FACTORY_RESET:
        rilResetType = 3;
        break;
    }
    dispatchInts(serial, mSlotId, RIL_REQUEST_NV_RESET_CONFIG, 1, rilResetType);
    return Void();
}

Return<void> RadioImpl::setUiccSubscription(int32_t serial, const SelectUiccSub& uiccSub) {
    mtkLogD(LOG_TAG, "setUiccSubscription: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_SET_UICC_SUBSCRIPTION);
    if (pRI == NULL) {
        return Void();
    }

    RIL_SelectUiccSub rilUiccSub = {};

    rilUiccSub.slot = uiccSub.slot;
    rilUiccSub.app_index = uiccSub.appIndex;
    rilUiccSub.sub_type = (RIL_SubscriptionType) uiccSub.subType;
    rilUiccSub.act_status = (RIL_UiccSubActStatus) uiccSub.actStatus;

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rilUiccSub, sizeof(rilUiccSub), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::setDataAllowed(int32_t serial, bool allow) {
    mtkLogD(LOG_TAG, "setDataAllowed: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_ALLOW_DATA, 1, BOOL_TO_INT(allow));
    return Void();
}

Return<void> RadioImpl::getHardwareConfig(int32_t serial) {
    mtkLogD(LOG_TAG, "getHardwareConfig: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_HARDWARE_CONFIG);
    return Void();
}

Return<void> RadioImpl::requestIccSimAuthentication(int32_t serial, int32_t authContext,
        const hidl_string& authData, const hidl_string& aid) {
    mtkLogD(LOG_TAG, "requestIccSimAuthentication: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SIM_AUTHENTICATION);
    if (pRI == NULL) {
        return Void();
    }

    RIL_SimAuthentication pf = {};

    pf.authContext = authContext;

    if (!copyHidlStringToRil(&pf.authData, authData, pRI)) {
        return Void();
    }

    if (!copyHidlStringToRil(&pf.aid, aid, pRI)) {
        memsetAndFreeStrings(1, pf.authData);
        return Void();
    }

    CALL_ONREQUEST(pRI->pCI->requestNumber, &pf, sizeof(pf), pRI, mSlotId);

    memsetAndFreeStrings(2, pf.authData, pf.aid);
    return Void();
}

/**
 * @param numProfiles number of data profile
 * @param dataProfiles the pointer to the actual data profiles. The acceptable type is
          RIL_DataProfileInfo or RIL_DataProfileInfo_v15.
 * @param dataProfilePtrs the pointer to the pointers that point to each data profile structure
 * @param numfields number of string-type member in the data profile structure
 * @param ... the variadic parameters are pointers to each string-type member
 **/
template <typename T>
void freeSetDataProfileData(int numProfiles, T *dataProfiles, T **dataProfilePtrs,
                            int numfields, ...) {
    va_list args;
    va_start(args, numfields);

    // Iterate through each string-type field that need to be free.
    for (int i = 0; i < numfields; i++) {
        // Iterate through each data profile and free that specific string-type field.
        // The type 'char *T::*' is a type of pointer to a 'char *' member inside T structure.
        char *T::*ptr = va_arg(args, char *T::*);
        for (int j = 0; j < numProfiles; j++) {
            memsetAndFreeStrings(1, dataProfiles[j].*ptr);
        }
    }

    va_end(args);

#ifdef MEMSET_FREED
    memset(dataProfiles, 0, numProfiles * sizeof(T));
    memset(dataProfilePtrs, 0, numProfiles * sizeof(T *));
#endif
    free(dataProfiles);
    free(dataProfilePtrs);
}

// M: [Inactive Timer] start
int decodeInactiveTimer(unsigned int authType) {
    return authType >> APN_INACTIVE_TIMER_KEY;
}
// M: [Inactive Timer] end

Return<void> RadioImpl::setDataProfile(int32_t serial, const hidl_vec<DataProfileInfo>& profiles,
                                       bool isRoaming) {
    mtkLogD(LOG_TAG, "setDataProfile: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SET_DATA_PROFILE);
    if (pRI == NULL) {
        return Void();
    }

    size_t num = profiles.size();
    bool success = false;

    if (s_vendorFunctions->version <= 14) {

        RIL_DataProfileInfo *dataProfiles =
            (RIL_DataProfileInfo *) calloc(num, sizeof(RIL_DataProfileInfo));

        if (dataProfiles == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return Void();
        }

        RIL_DataProfileInfo **dataProfilePtrs =
            (RIL_DataProfileInfo **) calloc(num, sizeof(RIL_DataProfileInfo *));
        if (dataProfilePtrs == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            free(dataProfiles);
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return Void();
        }

        for (size_t i = 0; i < num; i++) {
            dataProfilePtrs[i] = &dataProfiles[i];

            success = copyHidlStringToRil(&dataProfiles[i].apn, profiles[i].apn, pRI, true);

            const hidl_string &protocol =
                    (isRoaming ? profiles[i].roamingProtocol : profiles[i].protocol);

            if (success && !copyHidlStringToRil(&dataProfiles[i].protocol, protocol, pRI, true)) {
                success = false;
            }

            if (success && !copyHidlStringToRil(&dataProfiles[i].user, profiles[i].user, pRI,
                    true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].password, profiles[i].password,
                    pRI, true)) {
                success = false;
            }

            if (!success) {
                freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 4,
                    &RIL_DataProfileInfo::apn, &RIL_DataProfileInfo::protocol,
                    &RIL_DataProfileInfo::user, &RIL_DataProfileInfo::password);
                return Void();
            }

            dataProfiles[i].profileId = (RIL_DataProfile) profiles[i].profileId;
            dataProfiles[i].authType = ((unsigned int) profiles[i].authType) & APN_AUTH_TYPE_MAX_NUM;
            dataProfiles[i].type = (int) profiles[i].type;
            dataProfiles[i].maxConnsTime = profiles[i].maxConnsTime;
            dataProfiles[i].maxConns = profiles[i].maxConns;
            dataProfiles[i].waitTime = profiles[i].waitTime;
            dataProfiles[i].enabled = BOOL_TO_INT(profiles[i].enabled);
        }

        CALL_ONREQUEST(RIL_REQUEST_SET_DATA_PROFILE, dataProfilePtrs,
                num * sizeof(RIL_DataProfileInfo *), pRI, mSlotId);

        freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 4,
                &RIL_DataProfileInfo::apn, &RIL_DataProfileInfo::protocol,
                &RIL_DataProfileInfo::user, &RIL_DataProfileInfo::password);
    } else {
        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //RIL_DataProfileInfo_v15 *dataProfiles =
        //    (RIL_DataProfileInfo_v15 *) calloc(num, sizeof(RIL_DataProfileInfo_v15));
        RIL_MtkDataProfileInfo *dataProfiles =
            (RIL_MtkDataProfileInfo *) calloc(num, sizeof(RIL_MtkDataProfileInfo));

        if (dataProfiles == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return Void();
        }

        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //RIL_DataProfileInfo_v15 **dataProfilePtrs =
        //    (RIL_DataProfileInfo_v15 **) calloc(num, sizeof(RIL_DataProfileInfo_v15 *));
        RIL_MtkDataProfileInfo **dataProfilePtrs =
            (RIL_MtkDataProfileInfo **) calloc(num, sizeof(RIL_MtkDataProfileInfo *));

        if (dataProfilePtrs == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            free(dataProfiles);
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return Void();
        }

        for (size_t i = 0; i < num; i++) {
            dataProfilePtrs[i] = &dataProfiles[i];

            success = copyHidlStringToRil(&dataProfiles[i].apn, profiles[i].apn, pRI, true);
            if (success && !copyHidlStringToRil(&dataProfiles[i].protocol, profiles[i].protocol,
                    pRI)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].roamingProtocol,
                    profiles[i].roamingProtocol, pRI, true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].user, profiles[i].user, pRI,
                    true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].password, profiles[i].password,
                    pRI, true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].mvnoMatchData,
                    profiles[i].mvnoMatchData, pRI, true)) {
                success = false;
            }

            if (success && !convertMvnoTypeToString(profiles[i].mvnoType,
                    dataProfiles[i].mvnoType)) {
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                success = false;
            }

            if (!success) {
                // M: use data profile to sync apn tables to modem
                // remark AOSP
                //freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                //    &RIL_DataProfileInfo_v15::apn, &RIL_DataProfileInfo_v15::protocol,
                //    &RIL_DataProfileInfo_v15::roamingProtocol, &RIL_DataProfileInfo_v15::user,
                //    &RIL_DataProfileInfo_v15::password, &RIL_DataProfileInfo_v15::mvnoMatchData);
                freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                    &RIL_MtkDataProfileInfo::apn, &RIL_MtkDataProfileInfo::protocol,
                    &RIL_MtkDataProfileInfo::roamingProtocol, &RIL_MtkDataProfileInfo::user,
                    &RIL_MtkDataProfileInfo::password, &RIL_MtkDataProfileInfo::mvnoMatchData);
                return Void();
            }

            dataProfiles[i].profileId = (RIL_DataProfile) profiles[i].profileId;
            dataProfiles[i].authType = ((unsigned int) profiles[i].authType) & APN_AUTH_TYPE_MAX_NUM;
            dataProfiles[i].type = (int) profiles[i].type;
            dataProfiles[i].maxConnsTime = profiles[i].maxConnsTime;
            dataProfiles[i].maxConns = profiles[i].maxConns;
            dataProfiles[i].waitTime = profiles[i].waitTime;
            dataProfiles[i].enabled = BOOL_TO_INT(profiles[i].enabled);
            dataProfiles[i].supportedTypesBitmask = profiles[i].supportedApnTypesBitmap;
            dataProfiles[i].bearerBitmask = profiles[i].bearerBitmap;
            dataProfiles[i].mtu = profiles[i].mtu;

            // M: use data profile to sync apn tables to modem
            // set default value for inactiveTimer
            dataProfiles[i].inactiveTimer = decodeInactiveTimer((unsigned int) profiles[i].authType);

        }

        CALL_ONREQUEST(RIL_REQUEST_SET_DATA_PROFILE, dataProfilePtrs,
                num * sizeof(RIL_MtkDataProfileInfo *), pRI, mSlotId);

        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
        //        &RIL_DataProfileInfo_v15::apn, &RIL_DataProfileInfo_v15::protocol,
        //        &RIL_DataProfileInfo_v15::roamingProtocol, &RIL_DataProfileInfo_v15::user,
        //        &RIL_DataProfileInfo_v15::password, &RIL_DataProfileInfo_v15::mvnoMatchData);
        freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                &RIL_MtkDataProfileInfo::apn, &RIL_MtkDataProfileInfo::protocol,
                &RIL_MtkDataProfileInfo::roamingProtocol, &RIL_MtkDataProfileInfo::user,
                &RIL_MtkDataProfileInfo::password, &RIL_MtkDataProfileInfo::mvnoMatchData);
    }

    return Void();
}

Return<void> RadioImpl::requestShutdown(int32_t serial) {
    mtkLogD(LOG_TAG, "requestShutdown: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SHUTDOWN);
    return Void();
}

Return<void> RadioImpl::getRadioCapability(int32_t serial) {
    mtkLogD(LOG_TAG, "getRadioCapability: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_RADIO_CAPABILITY);
    return Void();
}

Return<void> RadioImpl::setRadioCapability(int32_t serial, const RadioCapability& rc) {
    mtkLogD(LOG_TAG, "setRadioCapability: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SET_RADIO_CAPABILITY);
    if (pRI == NULL) {
        return Void();
    }

    RIL_RadioCapability rilRc = {};

    // TODO : set rilRc.version using HIDL version ?
    rilRc.session = rc.session;
    rilRc.phase = (int) rc.phase;
    rilRc.rat = (int) rc.raf;
    rilRc.status = (int) rc.status;
    strncpy(rilRc.logicalModemUuid, rc.logicalModemUuid.c_str(), MAX_UUID_LENGTH);

    CALL_ONREQUEST(pRI->pCI->requestNumber, &rilRc, sizeof(rilRc), pRI, mSlotId);

    return Void();
}

Return<void> RadioImpl::startLceService(int32_t serial, int32_t reportInterval, bool pullMode) {
    mtkLogD(LOG_TAG, "startLceService: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_START_LCE, 2, reportInterval,
            BOOL_TO_INT(pullMode));
    return Void();
}

Return<void> RadioImpl::stopLceService(int32_t serial) {
    mtkLogD(LOG_TAG, "stopLceService: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_STOP_LCE);
    return Void();
}

Return<void> RadioImpl::pullLceData(int32_t serial) {
    mtkLogD(LOG_TAG, "pullLceData: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_PULL_LCEDATA);
    return Void();
}

Return<void> RadioImpl::getModemActivityInfo(int32_t serial) {
    mtkLogD(LOG_TAG, "getModemActivityInfo: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_ACTIVITY_INFO);
    return Void();
}

Return<void> RadioImpl::setAllowedCarriers(int32_t serial, bool allAllowed,
                                           const CarrierRestrictions& carriers) {
    mtkLogD(LOG_TAG, "setAllowedCarriers: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_SET_CARRIER_RESTRICTIONS);
    if (pRI == NULL) {
        return Void();
    }

    RIL_CarrierRestrictions cr = {};
    RIL_Carrier *allowedCarriers = NULL;
    RIL_Carrier *excludedCarriers = NULL;

    cr.len_allowed_carriers = carriers.allowedCarriers.size();
    allowedCarriers = (RIL_Carrier *)calloc(cr.len_allowed_carriers, sizeof(RIL_Carrier));
    if (allowedCarriers == NULL) {
        mtkLogE(LOG_TAG, "setAllowedCarriers: Memory allocation failed for request %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return Void();
    }
    cr.allowed_carriers = allowedCarriers;

    cr.len_excluded_carriers = carriers.excludedCarriers.size();
    excludedCarriers = (RIL_Carrier *)calloc(cr.len_excluded_carriers, sizeof(RIL_Carrier));
    if (excludedCarriers == NULL) {
        mtkLogE(LOG_TAG, "setAllowedCarriers: Memory allocation failed for request %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
#ifdef MEMSET_FREED
        memset(allowedCarriers, 0, cr.len_allowed_carriers * sizeof(RIL_Carrier));
#endif
        free(allowedCarriers);
        return Void();
    }
    cr.excluded_carriers = excludedCarriers;

    for (int i = 0; i < cr.len_allowed_carriers; i++) {
        allowedCarriers[i].mcc = carriers.allowedCarriers[i].mcc.c_str();
        allowedCarriers[i].mnc = carriers.allowedCarriers[i].mnc.c_str();
        allowedCarriers[i].match_type = (RIL_CarrierMatchType) carriers.allowedCarriers[i].matchType;
        allowedCarriers[i].match_data = carriers.allowedCarriers[i].matchData.c_str();
    }

    for (int i = 0; i < cr.len_excluded_carriers; i++) {
        excludedCarriers[i].mcc = carriers.excludedCarriers[i].mcc.c_str();
        excludedCarriers[i].mnc = carriers.excludedCarriers[i].mnc.c_str();
        excludedCarriers[i].match_type =
                (RIL_CarrierMatchType) carriers.excludedCarriers[i].matchType;
        excludedCarriers[i].match_data = carriers.excludedCarriers[i].matchData.c_str();
    }

    CALL_ONREQUEST(pRI->pCI->requestNumber, &cr, sizeof(RIL_CarrierRestrictions), pRI, mSlotId);

#ifdef MEMSET_FREED
    memset(allowedCarriers, 0, cr.len_allowed_carriers * sizeof(RIL_Carrier));
    memset(excludedCarriers, 0, cr.len_excluded_carriers * sizeof(RIL_Carrier));
#endif
    free(allowedCarriers);
    free(excludedCarriers);
    return Void();
}

Return<void> RadioImpl::getAllowedCarriers(int32_t serial) {
    mtkLogD(LOG_TAG, "getAllowedCarriers: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_GET_CARRIER_RESTRICTIONS);
    sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
    //dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_CARRIER_RESTRICTIONS);
    return Void();
}

Return<void> RadioImpl::sendDeviceState(int32_t serial, DeviceStateType deviceStateType,
                                        bool state) {
    mtkLogD(LOG_TAG, "sendDeviceState: serial %d", serial);
    if (s_vendorFunctions->version < 15) {
        if (deviceStateType ==  DeviceStateType::LOW_DATA_EXPECTED) {
            mtkLogD(LOG_TAG, "sendDeviceState: calling screen state %d", BOOL_TO_INT(!state));
            dispatchInts(serial, mSlotId, RIL_REQUEST_SCREEN_STATE, 1, BOOL_TO_INT(!state));
        } else {
            RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                    RIL_REQUEST_SEND_DEVICE_STATE);
            sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
        }
        return Void();
    }
    dispatchInts(serial, mSlotId, RIL_REQUEST_SEND_DEVICE_STATE, 2, (int) deviceStateType,
            BOOL_TO_INT(state));
    return Void();
}

Return<void> RadioImpl::setIndicationFilter(int32_t serial, int32_t indicationFilter) {
    mtkLogD(LOG_TAG, "setIndicationFilter: serial %d", serial);
    if (s_vendorFunctions->version < 15) {
        RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                RIL_REQUEST_SET_UNSOLICITED_RESPONSE_FILTER);
        sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
        return Void();
    }
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_UNSOLICITED_RESPONSE_FILTER, 1, indicationFilter);
    return Void();
}

Return<void> RadioImpl::setSimCardPower(int32_t serial, bool powerUp) {
    mtkLogD(LOG_TAG, "setSimCardPower: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SIM_CARD_POWER, 1, BOOL_TO_INT(powerUp));
    return Void();
}

Return<void> RadioImpl::setSimCardPower_1_1(int32_t serial, const AOSP_V1_1::CardPowerState state) {
#if VDBG
    mtkLogD(LOG_TAG, "setSimCardPower_1_1: serial %d state %d", serial, state);
#endif
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SIM_CARD_POWER, 1, state);
    return Void();
}

Return<void> RadioImpl::setCarrierInfoForImsiEncryption(int32_t serial,
        const AOSP_V1_1::ImsiEncryptionInfo& data) {
    mtkLogD(LOG_TAG, "setCarrierInfoForImsiEncryption: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(
            serial, mSlotId, RIL_REQUEST_SET_CARRIER_INFO_IMSI_ENCRYPTION);
    if (pRI == NULL) {
        mtkLogE(LOG_TAG, "setCarrierInfoForImsiEncryption: pRI == NULL");
        return Void();
    }

    RIL_CarrierInfoForImsiEncryption imsiEncryption = {};

    if (!copyHidlStringToRil(&imsiEncryption.mnc, data.mnc, pRI)) {
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return Void();
    }
    if (!copyHidlStringToRil(&imsiEncryption.mcc, data.mcc, pRI)) {
        memsetAndFreeStrings(1, imsiEncryption.mnc);
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return Void();
    }
    if (!copyHidlStringToRil(&imsiEncryption.keyIdentifier, data.keyIdentifier, pRI)) {
        memsetAndFreeStrings(2, imsiEncryption.mnc, imsiEncryption.mcc);
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return Void();
    }
    imsiEncryption.carrierKeyLength = data.carrierKey.size();
    imsiEncryption.carrierKey = new uint8_t[imsiEncryption.carrierKeyLength];
    memcpy(imsiEncryption.carrierKey, data.carrierKey.data(), imsiEncryption.carrierKeyLength);
    imsiEncryption.expirationTime = data.expirationTime;
    CALL_ONREQUEST(pRI->pCI->requestNumber, &imsiEncryption,
            sizeof(RIL_CarrierInfoForImsiEncryption), pRI, mSlotId);
    delete(imsiEncryption.carrierKey);
    return Void();
}

Return<void> RadioImpl::startKeepalive(int32_t serial, const AOSP_V1_1::KeepaliveRequest& keepalive) {
#if VDBG
    mtkLogD(LOG_TAG, "%s(): %d", __FUNCTION__, serial);
#endif
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_START_KEEPALIVE);

    // M @{ we don't support
    if (pRI != NULL) {
        sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
    }
    return Void();
    // M @}

    RIL_KeepaliveRequest kaReq = {};

    kaReq.type = static_cast<RIL_KeepaliveType>(keepalive.type);
    switch(kaReq.type) {
        case NATT_IPV4:
            if (keepalive.sourceAddress.size() != 4 ||
                    keepalive.destinationAddress.size() != 4) {
                mtkLogE(LOG_TAG, "Invalid address for keepalive!");
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
            }
            break;
        case NATT_IPV6:
            if (keepalive.sourceAddress.size() != 16 ||
                    keepalive.destinationAddress.size() != 16) {
                mtkLogE(LOG_TAG, "Invalid address for keepalive!");
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
            }
            break;
        default:
            mtkLogE(LOG_TAG, "Unknown packet keepalive type!");
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            return Void();
    }

    ::memcpy(kaReq.sourceAddress, keepalive.sourceAddress.data(), keepalive.sourceAddress.size());
    kaReq.sourcePort = keepalive.sourcePort;

    ::memcpy(kaReq.destinationAddress,
            keepalive.destinationAddress.data(), keepalive.destinationAddress.size());
    kaReq.destinationPort = keepalive.destinationPort;

    kaReq.maxKeepaliveIntervalMillis = keepalive.maxKeepaliveIntervalMillis;
    kaReq.cid = keepalive.cid; // This is the context ID of the data call

    CALL_ONREQUEST(pRI->pCI->requestNumber, &kaReq, sizeof(RIL_KeepaliveRequest), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::stopKeepalive(int32_t serial, int32_t sessionHandle) {
#if VDBG
    mtkLogD(LOG_TAG, "%s(): %d", __FUNCTION__, serial);
#endif
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_STOP_KEEPALIVE);

    // M @{ we don't support
    if (pRI != NULL) {
        sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
    }
    return Void();
    // M @}

    CALL_ONREQUEST(pRI->pCI->requestNumber, &sessionHandle, sizeof(uint32_t), pRI, mSlotId);
    return Void();
}

Return<void> RadioImpl::responseAcknowledgement() {
    android::releaseWakeLock();
    return Void();
}

Return<void> RadioImpl::triggerModeSwitchByEcc(int32_t serial, int32_t mode) {
    dispatchInts(serial, mSlotId, RIL_REQUEST_SWITCH_MODE_FOR_ECC, 1, mode);
    return Void();
}

Return<void> RadioImpl::setTrm(int32_t serial, int32_t mode) {
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_TRM, 1, mode);
    return Void();
}

// MTK-START: SIM
Return<void> RadioImpl::getATR(int32_t serial) {
    mtkLogD(LOG_TAG, "[%d] getATR: serial %d", mSlotId, serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SIM_GET_ATR);
    return Void();
}

Return<void> RadioImpl::getIccid(int32_t serial) {
    mtkLogD(LOG_TAG, "getIccid: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SIM_GET_ICCID);
    return Void();
}

Return<void> RadioImpl::setSimPower(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "setSimPower: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SIM_POWER, 1, mode);
    return Void();
}

int radio::onCardDetectedInd(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    return 0;
}
// MTK-END

// MTK-START: SIM GBA
bool dispatchSimGeneralAuth(int serial, int slotId, int request, const SimAuthStructure& simAuth) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_SimAuthStructure rilSimAuth;
    rilSimAuth.sessionId= simAuth.sessionId;
    rilSimAuth.mode= simAuth.mode;

    if (!copyHidlStringToRil(&rilSimAuth.param1, simAuth.param1, pRI)) {
        return false;
    }

    if (!copyHidlStringToRil(&rilSimAuth.param2, simAuth.param2, pRI)) {
        return false;
    }

    rilSimAuth.tag = simAuth.tag;

    s_vendorFunctions->onRequest(request, &rilSimAuth, sizeof(rilSimAuth), pRI,
            pRI->socket_id);

    memsetAndFreeStrings(1, rilSimAuth.param1);
    memsetAndFreeStrings(1, rilSimAuth.param2);

    return true;
}

Return<void> RadioImpl::doGeneralSimAuthentication(int32_t serial,
        const SimAuthStructure& simAuth) {
    mtkLogD(LOG_TAG, "doGeneralSimAuthentication: serial %d", serial);
    dispatchSimGeneralAuth(serial, mSlotId, RIL_REQUEST_GENERAL_SIM_AUTH, simAuth);
    return Void();
}
// MTK-END

/// M: CC: MTK proprietary call control ([IMS] common flow) @{
Return<void> RadioImpl::hangupAll(int32_t serial) {
    mtkLogD(LOG_TAG, "hangupAll: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_HANGUP_ALL);
    return Void();
}

Return<void> RadioImpl::hangupWithReason(int32_t serial, int32_t callId, int32_t reason) {
    return Void();
}

Return<void> RadioImpl::setCallIndication(int32_t serial, int32_t mode, int32_t callId, int32_t seqNumber) {
    mtkLogD(LOG_TAG, "setCallIndication: mode %d, callId %d, seqNumber %d", mode, callId, seqNumber);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_CALL_INDICATION, 3, mode, callId, seqNumber);
    return Void();
}

Return<void> RadioImpl::emergencyDial(int32_t serial, const Dial& dialInfo) {
    mtkLogD(LOG_TAG, "emergencyDial: serial %d", serial);

    int requestId = RIL_REQUEST_EMERGENCY_DIAL;
    if(isImsSlot(mSlotId)) {
        requestId = RIL_REQUEST_IMS_EMERGENCY_DIAL;
    }

    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, requestId);
    if (pRI == NULL) {
        return Void();
    }
    RIL_Dial dial = {};
    RIL_UUS_Info uusInfo = {};
    int32_t sizeOfDial = sizeof(dial);

    if (!copyHidlStringToRil(&dial.address, dialInfo.address, pRI)) {
        return Void();
    }
    dial.clir = (int) dialInfo.clir;

    if (dialInfo.uusInfo.size() != 0) {
        uusInfo.uusType = (RIL_UUS_Type) dialInfo.uusInfo[0].uusType;
        uusInfo.uusDcs = (RIL_UUS_DCS) dialInfo.uusInfo[0].uusDcs;

        if (dialInfo.uusInfo[0].uusData.size() == 0) {
            uusInfo.uusData = NULL;
            uusInfo.uusLength = 0;
        } else {
            if (!copyHidlStringToRil(&uusInfo.uusData, dialInfo.uusInfo[0].uusData, pRI)) {
                memsetAndFreeStrings(1, dial.address);
                return Void();
            }
            uusInfo.uusLength = dialInfo.uusInfo[0].uusData.size();
        }

        dial.uusInfo = &uusInfo;
    }

    s_vendorFunctions->onRequest(requestId,
                                 &dial, sizeOfDial, pRI,pRI->socket_id);

    memsetAndFreeStrings(2, dial.address, uusInfo.uusData);
    return Void();
}

Return<void> RadioImpl::setEccServiceCategory(int32_t serial, int32_t serviceCategory) {
    mtkLogD(LOG_TAG, "setEccServiceCategory: serial %d, serviceCategory %d", serial, serviceCategory);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_ECC_SERVICE_CATEGORY, 1, serviceCategory);
    return Void();
}

/// M: CC: @}
// M: [LTE][Low Power][UL traffic shaping] @{
Return<void> RadioImpl::setLteAccessStratumReport(int32_t serial, int32_t enable) {
    mtkLogD(LOG_TAG, "setLteAccessStratumReport: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_LTE_ACCESS_STRATUM_REPORT, 1, enable);
    return Void();
}

Return<void> RadioImpl::setLteUplinkDataTransfer(int32_t serial, int32_t state, int32_t interfaceId) {
    mtkLogD(LOG_TAG, "setLteUplinkDataTransfer: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_LTE_UPLINK_DATA_TRANSFER, 2, state, interfaceId);
    return Void();
}
// M: [LTE][Low Power][UL traffic shaping] @}

/// M: CC: E911 request current status.
Return<void> RadioImpl::currentStatus(int32_t serial, int32_t airplaneMode,
        int32_t imsReg) {
    mtkLogD(LOG_TAG, "currentStatus: serial %d, airplaneMode %d, imsReg %d", serial,
            airplaneMode, imsReg);
    dispatchInts(serial, mSlotId, RIL_REQUEST_CURRENT_STATUS, 2, airplaneMode, imsReg);

    return Void();
}

/// M: CC: E911 request set ECC preferred RAT.
Return<void> RadioImpl::eccPreferredRat(int32_t serial, int32_t phoneType) {
    mtkLogD(LOG_TAG, "eccPreferredRat: serial %d, phoneType %d", serial, phoneType);
    dispatchInts(serial, mSlotId, RIL_REQUEST_ECC_PREFERRED_RAT, 1, phoneType);

    return Void();
}

Return<void> RadioImpl::sendRequestRaw(int32_t serial, const hidl_vec<uint8_t>& data) {
    mtkLogD(LOG_TAG, "RadioImpl::sendRequestRaw: serial %d", serial);
    dispatchRaw(serial, mSlotId, RIL_REQUEST_OEM_HOOK_RAW, data);
    return Void();
}

Return<void> RadioImpl::videoCallAccept(int32_t serial,
                                        int32_t videoMode, int32_t callId) {
    mtkLogD(LOG_TAG, "videoCallAccept: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_VIDEO_CALL_ACCEPT, 2, videoMode, callId);
    return Void();
}

Return<void> RadioImpl::imsEctCommand(int32_t serial, const hidl_string& number,
        int32_t type) {
    mtkLogD(LOG_TAG, "imsEctCommand: serial %d", serial);
    std::string strType = std::to_string(type);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_IMS_ECT, false,
            2, (const char *)number.c_str(), (const char *)strType.c_str());
    return Void();
}

Return<void> RadioImpl::holdCall(int32_t serial, int32_t callId) {
    mtkLogD(LOG_TAG, "holdCall: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_HOLD_CALL, 1, callId);
    return Void();
}

Return<void> RadioImpl::resumeCall(int32_t serial, int32_t callId) {
    mtkLogD(LOG_TAG, "resumeCall: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RESUME_CALL, 1, callId);
    return Void();
}

Return<void> RadioImpl::imsDeregNotification(int32_t serial, int32_t cause) {
    mtkLogD(LOG_TAG, "imsDeregNotification: serial %d, cause %d", serial, cause);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_DEREG_NOTIFICATION, 1, cause);
    return Void();
}

// Femtocell feature
Return<void> RadioImpl::getFemtocellList(int32_t serial) {
    mtkLogD(LOG_TAG, "getFemtocellList: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_FEMTOCELL_LIST);
    return Void();
}

Return<void> RadioImpl::abortFemtocellList(int32_t serial) {
    mtkLogD(LOG_TAG, "abortFemtocellList: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_ABORT_FEMTOCELL_LIST);
    return Void();
}

Return<void> RadioImpl::selectFemtocell(int32_t serial,
                                                const hidl_string& operatorNumeric,
                                                const hidl_string& act,
                                                const hidl_string& csgId) {
    mtkLogD(LOG_TAG, "selectFemtocell: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SELECT_FEMTOCELL, true, 3,
            operatorNumeric.c_str(), act.c_str(), csgId.c_str());
    return Void();
}

Return<void> RadioImpl::queryFemtoCellSystemSelectionMode(int32_t serial) {
    mtkLogD(LOG_TAG, "queryFemtoCellSystemSelectionMode: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_FEMTOCELL_SYSTEM_SELECTION_MODE);
    return Void();
}

Return<void> RadioImpl::setFemtoCellSystemSelectionMode(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "setFemtoCellSystemSelectionMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_FEMTOCELL_SYSTEM_SELECTION_MODE, 1, mode);
    return Void();
}

Return<void> RadioImpl::setImsEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setImsEnable: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMS_ENABLE, 1, isOn ? 1:0);
    return Void();
}

Return<void> RadioImpl::setVolteEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setVolteEnable: serial %d", serial);

    FeatureValue feature;
    memset(feature.value, isOn ? 1 : 0, sizeof(feature.value));
    mtkSetFeature(CONFIG_VOLTE, &feature);

    radio::setVolteEnableResponse(mSlotId, RESPONSE_SOLICITED,
                                  serial, RIL_E_SUCCESS, NULL, 0);

    return Void();
}

Return<void> RadioImpl::setWfcEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setWfcEnable: serial %d", serial);

    FeatureValue feature;
    memset(feature.value, isOn ? 1 : 0, sizeof(feature.value));
    mtkSetFeature(CONFIG_WFC, &feature);

    radio::setWfcEnableResponse(mSlotId, RESPONSE_SOLICITED,
                                serial, RIL_E_SUCCESS, NULL, 0);

    return Void();
}

Return<void> RadioImpl::setVilteEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setVilteEnable: serial %d", serial);

    FeatureValue feature;
    memset(feature.value, isOn ? 1 : 0, sizeof(feature.value));
    mtkSetFeature(CONFIG_VILTE, &feature);

    radio::setVilteEnableResponse(mSlotId, RESPONSE_SOLICITED,
                                  serial, RIL_E_SUCCESS, NULL, 0);

    return Void();
}

Return<void> RadioImpl::setViWifiEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setViWifiEnable: serial %d", serial);

    FeatureValue feature;
    memset(feature.value, isOn ? 1 : 0, sizeof(feature.value));
    mtkSetFeature(CONFIG_VIWIFI, &feature);

    radio::setViWifiEnableResponse(mSlotId, RESPONSE_SOLICITED,
                                   serial, RIL_E_SUCCESS, NULL, 0);

    return Void();
}

Return<void> RadioImpl::setRcsUaEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setRcsUaEnable: serial %d", serial);

    FeatureValue feature;
    memset(feature.value, isOn ? 1 : 0, sizeof(feature.value));
    mtkSetFeature(CONFIG_RCSUA, &feature);

    radio::setRcsUaEnableResponse(mSlotId, RESPONSE_SOLICITED,
                                   serial, RIL_E_SUCCESS, NULL, 0);

    return Void();
}

Return<void> RadioImpl::setImsVoiceEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setImsVoiceEnable: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMS_VOICE_ENABLE, 1, isOn ? 1:0);
    return Void();
}

Return<void> RadioImpl::setImsVideoEnable(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setImsVideoEnable: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMS_VIDEO_ENABLE, 1, isOn ? 1:0);
    return Void();
}

Return<void> RadioImpl::setImscfg(int32_t serial, bool volteEnable,
        bool vilteEnable, bool vowifiEnable, bool viwifiEnable,
        bool smsEnable, bool eimsEnable) {

    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMSCFG, 6,
                 BOOL_TO_INT(volteEnable),
                 BOOL_TO_INT(vilteEnable),
                 BOOL_TO_INT(vowifiEnable),
                 BOOL_TO_INT(viwifiEnable),
                 BOOL_TO_INT(smsEnable),
                 BOOL_TO_INT(eimsEnable));
    return Void();
}

Return<void> RadioImpl::setModemImsCfg(int32_t serial, const hidl_string& keys,
        const hidl_string& values, int32_t type) {
   mtkLogD(LOG_TAG, "setModemImsCfg: serial %d", serial);
   dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_MD_IMSCFG, false, 3, keys.c_str(), values.c_str(),
           std::to_string(type).c_str());
   return Void();
}

Return<void> RadioImpl::getProvisionValue(int32_t serial,
        const hidl_string& provisionstring) {
    mtkLogD(LOG_TAG, "getProvisionValue: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_GET_PROVISION_VALUE, provisionstring.c_str());
    return Void();
}

Return<void> RadioImpl::setProvisionValue(int32_t serial,
        const hidl_string& provisionstring, const hidl_string& provisionValue) {
    mtkLogD(LOG_TAG, "setProvisionValue: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_PROVISION_VALUE, false,
            2, provisionstring.c_str(), provisionValue.c_str());
    return Void();
}

// Ims Config telephonyware START

Return<void>
RadioImpl::setImsCfgFeatureValue(int32_t serial, int32_t featureId, int32_t network, int32_t value,
                                 int32_t isLast) {
    mtkLogD(LOG_TAG, "setImsCfgFeatureValue: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_SET_FEATURE,
                 4, featureId, network, value, isLast);
    return Void();
}

Return<void> RadioImpl::getImsCfgFeatureValue(int32_t serial, int32_t featureId, int32_t network) {
    mtkLogD(LOG_TAG, "getImsCfgFeatureValue: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_GET_FEATURE,
                 2, featureId, network);
    return Void();
}


Return<void> RadioImpl::setImsCfgProvisionValue(int32_t serial, int32_t configId,
                                                const hidl_string& value) {
    mtkLogD(LOG_TAG, "setImsCfgProvisionValue: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_SET_PROVISION,false,
                    2, std::to_string(configId).c_str(), value.c_str());
    return Void();
}

Return<void> RadioImpl::getImsCfgProvisionValue(int32_t serial, int32_t configId) {
    mtkLogD(LOG_TAG, "setImsCfgProvisionValue: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_GET_PROVISION,
                 1, configId);
    return Void();
}

Return<void> RadioImpl::setImsCfgResourceCapValue(int32_t serial, int32_t featureId, int32_t value) {
    mtkLogD(LOG_TAG, "setImsCfgResourceCapValue: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_SET_RESOURCE_CAP,
                 2, featureId, value);
    return Void();
}

Return<void> RadioImpl::getImsCfgResourceCapValue(int32_t serial, int32_t featureId) {
    mtkLogD(LOG_TAG, "getImsCfgResourceCapValue: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_CONFIG_GET_RESOURCE_CAP,
                 1, featureId);
    return Void();
}
// Ims Config telephonyware END

Return<void> RadioImpl::addImsConferenceCallMember(int32_t serial, int32_t confCallId,
        const hidl_string& address, int32_t callToAdd) {
    mtkLogD(LOG_TAG, "addImsConferenceCallMember: serial %d", serial);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_ADD_IMS_CONFERENCE_CALL_MEMBER, false, 3,
            std::to_string(confCallId).c_str(), address.c_str(),
            std::to_string(callToAdd).c_str());

    return Void();
}

Return<void> RadioImpl::removeImsConferenceCallMember(int32_t serial, int32_t confCallId,
        const hidl_string& address, int32_t callToRemove) {
    mtkLogD(LOG_TAG, "removeImsConferenceCallMember: serial %d", serial);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_REMOVE_IMS_CONFERENCE_CALL_MEMBER, false, 3,
            std::to_string(confCallId).c_str(), address.c_str(),
            std::to_string(callToRemove).c_str());

    return Void();
}


Return<void> RadioImpl::setWfcProfile(int32_t serial, int32_t wfcPreference) {
    mtkLogD(LOG_TAG, "setWfcProfile: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_WFC_PROFILE, 1, wfcPreference);
    return Void();
}

Return<void> RadioImpl::conferenceDial(int32_t serial, const ConferenceDial& dialInfo) {
    mtkLogD(LOG_TAG, "conferenceDial: serial %d", serial);

    int request = RIL_REQUEST_CONFERENCE_DIAL;
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, request);
    if (pRI == NULL) {
        return Void();
    }

    int countStrings = dialInfo.dialNumbers.size() + 3;
    char **pStrings;
    pStrings = (char **)calloc(countStrings, sizeof(char *));
    if (pStrings == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(request));

        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return Void();
    }

    if(!copyHidlStringToRil(&pStrings[0], dialInfo.isVideoCall ? "1":"0", pRI)) {
        free(pStrings);
        return Void();
    }

    if(!copyHidlStringToRil(&pStrings[1],
                            std::to_string(dialInfo.dialNumbers.size()).c_str(), pRI)) {
        memsetAndFreeStrings(1, pStrings[0]);
        free(pStrings);
        return Void();
    }

    int i = 0;
    for (i = 0; i < (int) dialInfo.dialNumbers.size(); i++) {
        if (!copyHidlStringToRil(&pStrings[i + 2], dialInfo.dialNumbers[i], pRI)) {
            for (int j = 0; j < i + 2; j++) {
                memsetAndFreeStrings(1, pStrings[j]);
            }

            free(pStrings);
            return Void();
        }
    }

    if(!copyHidlStringToRil(&pStrings[i + 2],
        std::to_string((int)dialInfo.clir).c_str(), pRI)) {
        for (int j = 0; j < (int) dialInfo.dialNumbers.size() + 2; j++) {
            memsetAndFreeStrings(1, pStrings[j]);
        }

        free(pStrings);
        return Void();
    }

    s_vendorFunctions->onRequest(request, pStrings, countStrings * sizeof(char *), pRI,
            pRI->socket_id);

    if (pStrings != NULL) {
        for (int j = 0 ; j < countStrings ; j++) {
            memsetAndFreeStrings(1, pStrings[j]);
        }

#ifdef MEMSET_FREED
        memset(pStrings, 0, countStrings * sizeof(char *));
#endif
        free(pStrings);
    }

    return Void();
}
// [IMS] ViLTE Dial
Return<void> RadioImpl::vtDial(int32_t serial, const Dial& dialInfo) {
    mtkLogD(LOG_TAG, "vtDial: serial %d", serial);
    int requestId = RIL_REQUEST_VT_DIAL;
    if(isImsSlot(mSlotId)) {
        requestId = RIL_REQUEST_IMS_VT_DIAL;
    }
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, requestId);
    if (pRI == NULL) {
        return Void();
    }
    RIL_Dial dial = {};
    RIL_UUS_Info uusInfo = {};
    int32_t sizeOfDial = sizeof(dial);
    if (!copyHidlStringToRil(&dial.address, dialInfo.address, pRI)) {
        return Void();
    }
    dial.clir = (int) dialInfo.clir;
    if (dialInfo.uusInfo.size() != 0) {
        uusInfo.uusType = (RIL_UUS_Type) dialInfo.uusInfo[0].uusType;
        uusInfo.uusDcs = (RIL_UUS_DCS) dialInfo.uusInfo[0].uusDcs;
        if (dialInfo.uusInfo[0].uusData.size() == 0) {
            uusInfo.uusData = NULL;
            uusInfo.uusLength = 0;
        } else {
            if (!copyHidlStringToRil(&uusInfo.uusData,
                                     dialInfo.uusInfo[0].uusData, pRI)) {
                memsetAndFreeStrings(1, dial.address);
                return Void();
            }
            uusInfo.uusLength = dialInfo.uusInfo[0].uusData.size();
        }
        dial.uusInfo = &uusInfo;
    }
    s_vendorFunctions->onRequest(requestId,
                                 &dial, sizeOfDial, pRI, pRI->socket_id);
    memsetAndFreeStrings(2, dial.address, uusInfo.uusData);
    return Void();
}
Return<void> RadioImpl::vtDialWithSipUri(int32_t serial, const hidl_string& address) {
    mtkLogD(LOG_TAG, "vtDialWithSipUri: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_VT_DIAL_WITH_SIP_URI, address.c_str());

    return Void();
}

Return<void> RadioImpl::dialWithSipUri(int32_t serial, const hidl_string& address) {
    mtkLogD(LOG_TAG, "dialWithSipUri: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_DIAL_WITH_SIP_URI, address.c_str());

    return Void();
}

Return<void> RadioImpl::sendUssi(int32_t serial, int32_t action,
        const hidl_string& ussiString) {
    mtkLogD(LOG_TAG, "sendUssi: serial %d", serial);

    hidl_string strAction = std::to_string(action);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SEND_USSI, true, 2,
                    strAction.c_str(), ussiString.c_str());

    return Void();
}

Return<void> RadioImpl::cancelUssi(int32_t serial) {
    mtkLogD(LOG_TAG, "cancelPendingUssi: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_CANCEL_USSI);

    return Void();
}

Return<void> RadioImpl::getXcapStatus(int32_t serial) {
    mtkLogD(LOG_TAG, "getXcapStatus: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_XCAP_STATUS);

    return Void();
}

Return<void> RadioImpl::resetSuppServ(int32_t serial) {
    mtkLogD(LOG_TAG, "resetSuppServ: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_RESET_SUPP_SERV);

    return Void();
}

Return<void> RadioImpl::setupXcapUserAgentString(int32_t serial, const hidl_string& userAgent) {
    mtkLogD(LOG_TAG, "setupXcapUserAgentString: serial %d, userAgent %s",
            serial, userAgent.c_str());

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SETUP_XCAP_USER_AGENT_STRING, true, 1,
                    userAgent.c_str());

    return Void();
}

Return<void> RadioImpl::forceReleaseCall(int32_t serial, int32_t callId) {
    mtkLogD(LOG_TAG, "forceHangup: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_FORCE_RELEASE_CALL, 1,
                 callId);

    return Void();
}

Return<void> RadioImpl::imsBearerActivationDone(int32_t serial, int32_t aid,
       int32_t status) {

    mtkLogD(LOG_TAG, "responseBearerActivationDone: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_BEARER_ACTIVATION_DONE, 2,
                 aid, status);

    return Void();
}

Return<void> RadioImpl::imsBearerDeactivationDone(int32_t serial, int32_t aid,
       int32_t status) {

    mtkLogD(LOG_TAG, "responseBearerDeactivationDone: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_IMS_BEARER_DEACTIVATION_DONE, 2,
                 aid, status);

    return Void();
}

Return<void> RadioImpl::setImsBearerNotification(int32_t serial, int32_t enable) {

    mtkLogD(LOG_TAG, "setImsBearerNotification: serial:  %d, enable: %d", serial, enable);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMS_BEARER_NOTIFICATION, 1, enable);

    return Void();
}

Return<void> RadioImpl::setImsRtpReport(int32_t serial, int32_t pdnId,
       int32_t networkId, int32_t timer) {
    mtkLogD(LOG_TAG, "setImsRtpReport: serial %d", serial);

    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_IMS_RTP_REPORT, 3,
                 pdnId, networkId, timer);

    return Void();
}
Return<void> RadioImpl::pullCall(int32_t serial,
                                 const hidl_string& target,
                                 bool isVideoCall) {
    mtkLogD(LOG_TAG, "pullCall: serial %d", serial);

    dispatchStrings(serial, mSlotId, RIL_REQUEST_PULL_CALL, false, 2, target.c_str(),
                 isVideoCall ? "1":"0");

    return Void();
}
Return<void> RadioImpl::setImsRegistrationReport(int32_t serial) {
    mtkLogD(LOG_TAG, "setImsRegistrationReport: serial %d", serial);

    dispatchVoid(serial, mSlotId, RIL_REQUEST_SET_IMS_REGISTRATION_REPORT);
    return Void();
}

Return<void> RadioImpl::sendRequestStrings(int32_t serial,
        const hidl_vec<hidl_string>& data) {
    mtkLogD(LOG_TAG, "RadioImpl::sendRequestStrings: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_OEM_HOOK_STRINGS, data);
    return Void();
}

// ATCI Start
Return<void> RadioImpl::setResponseFunctionsForAtci(
        const ::android::sp<IAtciResponse>& atciResponseParam,
        const ::android::sp<IAtciIndication>& atciIndicationParam) {
    mtkLogD(LOG_TAG, "setResponseFunctionsForAtci");
    mAtciResponse = atciResponseParam;
    mAtciIndication = atciIndicationParam;
    return Void();
}

Return<void> RadioImpl::sendAtciRequest(int32_t serial, const hidl_vec<uint8_t>& data) {
    mtkLogD(LOG_TAG, "sendAtciRequest: serial %d", serial);
    dispatchRaw(serial, mSlotId, RIL_REQUEST_OEM_HOOK_ATCI_INTERNAL, data);
    return Void();
}
// ATCI End

// SUBSIDYLOCK Start
Return<void> RadioImpl::setResponseFunctionsSubsidyLock(
        const ::android::sp<VENDOR_V3_5::ISubsidyLockResponse>& sublockResponseParam,
        const ::android::sp<VENDOR_V3_5::ISubsidyLockIndication>& sublockIndicationParam) {
    return Void();
}

Return<void> RadioImpl::sendSubsidyLockRequest(int32_t serial, int32_t reqType,
        const hidl_vec<uint8_t>& data) {
    return Void();
}
// SUBSIDYLOCK End

/// M: eMBMS feature
Return<void> RadioImpl::sendEmbmsAtCommand(int32_t serial, const hidl_string& s) {
    mtkLogD(LOG_TAG, "sendEmbmsAtCommand: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_EMBMS_AT_CMD, s.c_str());
    return Void();
}
/// M: eMBMS end

// worldmode {
Return<void> RadioImpl::setResumeRegistration(int32_t serial, int32_t sessionId) {
    mtkLogD(LOG_TAG, "RadioImpl::setResumeRegistration: serial %d sessionId %d", serial, sessionId);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RESUME_REGISTRATION, 1, sessionId);

    return Void();
}

// MTK-START: SIM ME LOCK
Return<void> RadioImpl::queryNetworkLock(int32_t serial, int32_t category) {
    mtkLogD(LOG_TAG, "queryNetworkLock: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_QUERY_SIM_NETWORK_LOCK, 1, category);
    return Void();
}

Return<void> RadioImpl::setNetworkLock(int32_t serial, int32_t category,
        int32_t lockop, const ::android::hardware::hidl_string& password,
        const ::android::hardware::hidl_string& data_imsi,
        const ::android::hardware::hidl_string& gid1,
        const ::android::hardware::hidl_string& gid2) {
    mtkLogD(LOG_TAG, "setNetworkLock: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_SIM_NETWORK_LOCK,
            true,
            6,
            std::to_string((int) category).c_str(),
            std::to_string((int) lockop).c_str(),
            password.c_str(),
            data_imsi.c_str(),
            gid1.c_str(),
            gid2.c_str());
    return Void();
}

Return<void> RadioImpl::supplyDepersonalization(int32_t serial, const hidl_string& netPin,
        int32_t type) {
    mtkLogD(LOG_TAG, "supplyDepersonalization: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_DEPERSONALIZATION, true, 2,
            netPin.c_str(), std::to_string((int) type).c_str());
    return Void();
}
// MTK-END

Return<void> RadioImpl::storeModemType(int32_t serial, int32_t modemType) {
    mtkLogD(LOG_TAG, "RadioImpl::storeModemType: serial %d modemType %d", serial, modemType);
    dispatchInts(serial, mSlotId, RIL_REQUEST_STORE_MODEM_TYPE, 1, modemType);
    return Void();
}

Return<void> RadioImpl::reloadModemType(int32_t serial, int32_t modemType) {
    mtkLogD(LOG_TAG, "RadioImpl::reloadModemType: serial %d modemType %d", serial, modemType);
    dispatchInts(serial, mSlotId, RIL_REQUEST_RELOAD_MODEM_TYPE, 1, modemType);
    return Void();
}
// worldmode }
// PHB START
Return<void> RadioImpl::queryPhbStorageInfo(int32_t serial, int32_t type) {
    mtkLogD(LOG_TAG, "queryPhbStorageInfo: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_QUERY_PHB_STORAGE_INFO,
            1, type);
    return Void();
}

bool dispatchPhbEntry(int serial, int slotId, int request,
                              const PhbEntryStructure& phbEntry) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_PhbEntryStructure pbe;
    pbe.type = phbEntry.type;
    pbe.index = phbEntry.index;

    if (!copyHidlStringToRil(&pbe.number, phbEntry.number, pRI)) {
        return false;
    }
    pbe.ton = phbEntry.ton;

    if (!copyHidlStringToRil(&pbe.alphaId, phbEntry.alphaId, pRI)) {
        return false;
    }

    s_vendorFunctions->onRequest(request, &pbe, sizeof(pbe), pRI,
            pRI->socket_id);

    memsetAndFreeStrings(1, pbe.number);
    memsetAndFreeStrings(1, pbe.alphaId);

    return true;
}

Return<void> RadioImpl::writePhbEntry(int32_t serial, const PhbEntryStructure& phbEntry) {
    mtkLogD(LOG_TAG, "writePhbEntry: serial %d", serial);
    dispatchPhbEntry(serial, mSlotId, RIL_REQUEST_WRITE_PHB_ENTRY, phbEntry);
    return Void();
}

Return<void> RadioImpl::readPhbEntry(int32_t serial, int32_t type, int32_t bIndex, int32_t eIndex) {
    mtkLogD(LOG_TAG, "readPhbEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_PHB_ENTRY,
            3, type, bIndex, eIndex);
    return Void();
}

Return<void> RadioImpl::queryUPBCapability(int32_t serial) {
    mtkLogD(LOG_TAG, "queryUPBCapability: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_QUERY_UPB_CAPABILITY);
    return Void();
}

Return<void> RadioImpl::editUPBEntry(int32_t serial, const hidl_vec<hidl_string>& data) {
    mtkLogD(LOG_TAG, "editUPBEntry: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_EDIT_UPB_ENTRY, data);
    return Void();
}

Return<void> RadioImpl::deleteUPBEntry(int32_t serial, int32_t entryType, int32_t adnIndex, int32_t entryIndex) {
    mtkLogD(LOG_TAG, "deleteUPBEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_DELETE_UPB_ENTRY,
           3, entryType, adnIndex, entryIndex);
    return Void();
}

Return<void> RadioImpl::readUPBGasList(int32_t serial, int32_t startIndex, int32_t endIndex) {
    mtkLogD(LOG_TAG, "readUPBGasList: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_UPB_GAS_LIST,
            2, startIndex, endIndex);
    return Void();
}

Return<void> RadioImpl::readUPBGrpEntry(int32_t serial, int32_t adnIndex) {
    mtkLogD(LOG_TAG, "readUPBGrpEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_UPB_GRP,
            1, adnIndex);
    return Void();
}

bool dispatchGrpEntry(int serial, int slotId, int request, int adnIndex, const hidl_vec<int32_t>& grpId) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    int countInts = grpId.size() + 1;
    int *pInts = (int *)calloc(countInts, sizeof(int));

    if (pInts == NULL) {
        mtkLogE(LOG_TAG, "Memory allocation failed for request %s", requestToString(request));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return false;
    }
    pInts[0] = adnIndex;
    for (int i = 1; i < countInts; i++) {
        pInts[i] = grpId[i-1];
    }

    s_vendorFunctions->onRequest(request, pInts, countInts * sizeof(int), pRI,
            pRI->socket_id);

    if (pInts != NULL) {
#ifdef MEMSET_FREED
        memset(pInts, 0, countInts * sizeof(int));
#endif
        free(pInts);
    }
    return true;
}

Return<void> RadioImpl::writeUPBGrpEntry(int32_t serial, int32_t adnIndex, const hidl_vec<int32_t>& grpIds) {
    mtkLogD(LOG_TAG, "writeUPBGrpEntry: serial %d", serial);
    dispatchGrpEntry(serial, mSlotId, RIL_REQUEST_WRITE_UPB_GRP, adnIndex, grpIds);
    return Void();
}

Return<void> RadioImpl::getPhoneBookStringsLength(int32_t serial) {
    mtkLogD(LOG_TAG, "getPhoneBookStringsLength: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_PHB_STRING_LENGTH);
    return Void();
}

Return<void> RadioImpl::getPhoneBookMemStorage(int32_t serial) {
    mtkLogD(LOG_TAG, "getPhoneBookMemStorage: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_PHB_MEM_STORAGE);
    return Void();
}

Return<void> RadioImpl::setPhoneBookMemStorage(int32_t serial,
        const hidl_string& storage, const hidl_string& password) {
    mtkLogD(LOG_TAG, "setPhoneBookMemStorage: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_PHB_MEM_STORAGE, true,
            2, storage.c_str(), password.c_str());
    return Void();
}

Return<void> RadioImpl::readPhoneBookEntryExt(int32_t serial, int32_t index1, int32_t index2) {
    mtkLogD(LOG_TAG, "readPhoneBookEntryExt: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_PHB_ENTRY_EXT,
            2, index1, index2);
    return Void();
}

bool dispatchPhbEntryExt(int serial, int slotId, int request,
                              const PhbEntryExt& phbEntryExt) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_PHB_ENTRY pbe;
    pbe.index = phbEntryExt.index;

    if (!copyHidlStringToRil(&pbe.number, phbEntryExt.number, pRI)) {
        return false;
    }
    pbe.type = phbEntryExt.type;

    if (!copyHidlStringToRil(&pbe.text, phbEntryExt.text, pRI)) {
        return false;
    }

    pbe.hidden = phbEntryExt.hidden;

    if (!copyHidlStringToRil(&pbe.group, phbEntryExt.group, pRI)) {
        return false;
    }

    if (!copyHidlStringToRil(&pbe.adnumber, phbEntryExt.adnumber, pRI)) {
        return false;
    }
    pbe.adtype = phbEntryExt.adtype;

    if (!copyHidlStringToRil(&pbe.secondtext, phbEntryExt.secondtext, pRI)) {
        return false;
    }

    if (!copyHidlStringToRil(&pbe.email, phbEntryExt.email, pRI)) {
        return false;
    }

    s_vendorFunctions->onRequest(request, &pbe, sizeof(pbe), pRI,
            pRI->socket_id);

    memsetAndFreeStrings(1, pbe.number);
    memsetAndFreeStrings(1, pbe.text);
    memsetAndFreeStrings(1, pbe.group);
    memsetAndFreeStrings(1, pbe.adnumber);
    memsetAndFreeStrings(1, pbe.secondtext);
    memsetAndFreeStrings(1, pbe.email);

    return true;
}

Return<void> RadioImpl::writePhoneBookEntryExt(int32_t serial, const PhbEntryExt& phbEntryExt) {
    mtkLogD(LOG_TAG, "writePhoneBookEntryExt: serial %d", serial);
    dispatchPhbEntryExt(serial, mSlotId, RIL_REQUEST_WRITE_PHB_ENTRY_EXT,
            phbEntryExt);
    return Void();
}

Return<void> RadioImpl::queryUPBAvailable(int32_t serial, int32_t eftype, int32_t fileIndex) {
    mtkLogD(LOG_TAG, "queryUPBAvailable: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_QUERY_UPB_AVAILABLE,
            2, eftype, fileIndex);
    return Void();
}

Return<void> RadioImpl::readUPBEmailEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex) {
    mtkLogD(LOG_TAG, "readUPBEmailEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_EMAIL_ENTRY,
            2, adnIndex, fileIndex);
    return Void();
}

Return<void> RadioImpl::readUPBSneEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex) {
    mtkLogD(LOG_TAG, "readUPBSneEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_SNE_ENTRY,
            2, adnIndex, fileIndex);
    return Void();
}

Return<void> RadioImpl::readUPBAnrEntry(int32_t serial, int32_t adnIndex, int32_t fileIndex) {
    mtkLogD(LOG_TAG, "readUPBAnrEntry: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_ANR_ENTRY,
            2, adnIndex, fileIndex);
    return Void();
}

Return<void> RadioImpl::readUPBAasList(int32_t serial, int32_t startIndex, int32_t endIndex) {
    mtkLogD(LOG_TAG, "readUPBAasList: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_READ_UPB_AAS_LIST,
            2, startIndex, endIndex);
    return Void();
}

Return<void> RadioImpl::setPhonebookReady(int32_t serial, int32_t ready) {
    mtkLogD(LOG_TAG, "RadioImpl::setPhonebookReady: serial %d ready %d", serial, ready);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_PHONEBOOK_READY, 1, ready);
    return Void();
}

// PHB END

Return<void> RadioImpl::setRxTestConfig(int32_t serial, int32_t antType) {
    mtkLogD(LOG_TAG, "setRxTestConfig: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_VSS_ANTENNA_CONF, 1, antType);
    return Void();
}
Return<void> RadioImpl::getRxTestResult(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "getRxTestResult: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_VSS_ANTENNA_INFO, 1, mode);
    return Void();
}


Return<void> RadioImpl::getPOLCapability(int32_t serial) {
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_POL_CAPABILITY);
    return Void();
}

Return<void> RadioImpl::getCurrentPOLList(int32_t serial) {
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_POL_LIST);
    return Void();
}

Return<void> RadioImpl::setPOLEntry(int32_t serial, int32_t index,
        const ::android::hardware::hidl_string& numeric,
        int32_t nAct) {
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_POL_ENTRY,
            true,
            3,
            std::to_string((int) index).c_str(),
            numeric.c_str(),
            std::to_string((int) nAct).c_str());
    return Void();
}

/// M: [Network][C2K] Sprint roaming control @{
Return<void> RadioImpl::setRoamingEnable(int32_t serial, const hidl_vec<int32_t>& config) {
    mtkLogD(LOG_TAG, "setRoamingEnable: serial: %d", serial);
    if (config.size() == 6) {
        dispatchInts(serial, mSlotId, RIL_REQUEST_SET_ROAMING_ENABLE, 6,
                config[0], config[1], config[2], config[3], config[4], config[5]);
    } else {
        mtkLogE(LOG_TAG, "setRoamingEnable: param error, num: %d (should be 6)", (int) config.size());
    }
    return Void();
}

Return<void> RadioImpl::getRoamingEnable(int32_t serial, int32_t phoneId) {
    dispatchInts(serial, mSlotId, RIL_REQUEST_GET_ROAMING_ENABLE, 1, phoneId);
    return Void();
}
/// @}

Return<void> RadioImpl::setLteReleaseVersion(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "setLteReleaseVersion: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_LTE_RELEASE_VERSION, 1, mode);
    return Void();
}

Return<void> RadioImpl::getLteReleaseVersion(int32_t serial) {
    mtkLogD(LOG_TAG, "getLteReleaseVersion: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_LTE_RELEASE_VERSION);
    return Void();
}

Return<void> RadioImpl::setPropImsHandover(int32_t serial, const hidl_string& value) {
    mtkLogI(LOG_TAG, "setPropImsHandover: serial %d, value %s", serial, value.c_str());
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_SETPROP_IMS_HANDOVER);
    if (pRI == NULL) {
        return Void();
    } else {
        sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
    }
    return Void();
}

Return<void> RadioImpl::setOperatorConfiguration(int32_t serial, int32_t type, const hidl_string& data) {
    mtkLogD(LOG_TAG, "setOperatorConfiguration: serial %d", serial);
    mtkLogD(LOG_TAG, "setOperatorConfiguration: type %d", type);
    mtkLogD(LOG_TAG, "setOperatorConfiguration: data %s", data.c_str());

    FeatureValue feature;
    // Write MD SBP ID for CCCI
    int fd;
    char *value = NULL;
    int size;
    char buf[8];
    int ret = -1;

    memcpy(feature.value, data.c_str(), strlen(data.c_str())+1);
    mtkLogD(LOG_TAG, "setOperatorConfiguration: feature.value %s", feature.value);

    if (type == CXP_CONFIG_TYPE_OPTR) {
        mtkSetFeature(CONFIG_CXP_OPTR, &feature);
    } else if (type == CXP_CONFIG_TYPE_SPEC) {
        mtkSetFeature(CONFIG_CXP_SPEC, &feature);
    } else if (type == CXP_CONFIG_TYPE_SEG) {
        mtkSetFeature(CONFIG_CXP_SEG, &feature);
    } else if (type == CXP_CONFIG_TYPE_SBP) {
        mtkSetFeature(CONFIG_CXP_SBP, &feature);
        // Write MD SBP ID for CCCI
        // set umask for necessary file permission
        umask(S_IWGRP | S_IXGRP | S_IWOTH | S_IXOTH);
        fd = open("/mnt/vendor/nvdata/APCFG/APRDCL/CXP_SBP",O_WRONLY | O_CREAT | O_TRUNC, 0644);
        // Recovery umask to RILD default setting
        umask(S_IRGRP | S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH);
        if (fd >= 0) {
            value = (char*) malloc(20);
            memset(value,0x00,20);
            memcpy(value,data.c_str(),strlen(data.c_str())+1);

            size = snprintf(buf, 8, "%s", value);
            ret = write(fd, buf, size);
            mtkLogE(LOG_TAG, "[CXP][%s]fd:%d, write ret value:%d, error=%d\n",
                    __func__, fd, ret, errno);
            free(value);
            close(fd);
        } else {
            mtkLogE(LOG_TAG, "[CXP][%s]Open NVDATA CXP_SBP fail, fd:%d, error=%d\n",
                    __func__, fd, errno);
        }
    } else if (type == CXP_CONFIG_TYPE_SUBID) {
        mtkSetFeature(CONFIG_CXP_SUBID, &feature);
    } else {
        mtkLogD(LOG_TAG,"setOperatorConfiguration: invalid configuration type");
    }
    return Void();
}

Return<void> RadioImpl::setSuppServProperty(int32_t serial, const hidl_string& name, const hidl_string& value) {
    mtkLogD(LOG_TAG, "setSuppServProperty: serial %d, name %s, value %s ", serial, name.c_str(), value.c_str());

    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_SS_PROPERTY, true,
            2, name.c_str(), value.c_str());
    return Void();
}

Return<void> RadioImpl::getSuppServProperty(int32_t serial, const hidl_string& name) {
    mtkLogD(LOG_TAG, "getSuppServProperty: serial %d, name %s", serial, name.c_str());

    dispatchString(serial, mSlotId, RIL_REQUEST_GET_SS_PROPERTY, name.c_str());
    return Void();
}

/***************************************************************************************************
 * RESPONSE FUNCTIONS
 * Functions above are used for requests going from framework to vendor code. The ones below are
 * responses for those requests coming back from the vendor code.
 **************************************************************************************************/

void radio::acknowledgeRequest(int slotId, int serial) {
    if (radioService[slotId]->mRadioResponse != NULL) {
        Return<void> retStatus = radioService[slotId]->mRadioResponse->acknowledgeRequest(serial);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acknowledgeRequest: radioService[%d]->mRadioResponse == NULL", slotId);
    }
}

void populateResponseInfo(RadioResponseInfo& responseInfo, int serial, int responseType,
                         RIL_Errno e) {
    responseInfo.serial = serial;
    switch (responseType) {
        case RESPONSE_SOLICITED:
            responseInfo.type = RadioResponseType::SOLICITED;
            break;
        case RESPONSE_SOLICITED_ACK_EXP:
            responseInfo.type = RadioResponseType::SOLICITED_ACK_EXP;
            break;
    }
    responseInfo.error = (RadioError) e;
}

int responseIntOrEmpty(RadioResponseInfo& responseInfo, int serial, int responseType, RIL_Errno e,
               void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    int ret = -1;

    if (response == NULL && responseLen == 0) {
        // Earlier RILs did not send a response for some cases although the interface
        // expected an integer as response. Do not return error if response is empty. Instead
        // Return -1 in those cases to maintain backward compatibility.
    } else if (response == NULL || responseLen != sizeof(int)) {
        mtkLogE(LOG_TAG, "responseIntOrEmpty: Invalid response");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
    } else {
        int *p_int = (int *) response;
        ret = p_int[0];
    }
    return ret;
}

int responseInt(RadioResponseInfo& responseInfo, int serial, int responseType, RIL_Errno e,
               void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    int ret = -1;

    if (response == NULL || responseLen != sizeof(int)) {
        mtkLogE(LOG_TAG, "responseInt: Invalid response");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
    } else {
        int *p_int = (int *) response;
        ret = p_int[0];
    }
    return ret;
}

int radio::getIccCardStatusResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponse != NULL
            || radioService[slotId]->mRadioResponseV1_2 != NULL
            || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        AOSP_V1_2::CardStatus cardStatus = {};
        RIL_CardStatus_v7 *p_cur = ((RIL_CardStatus_v7 *) response);
        if (response == NULL || responseLen != sizeof(RIL_CardStatus_v7)
                || p_cur->gsm_umts_subscription_app_index >= p_cur->num_applications
                || p_cur->cdma_subscription_app_index >= p_cur->num_applications
                || p_cur->ims_subscription_app_index >= p_cur->num_applications) {
            mtkLogE(LOG_TAG, "getIccCardStatusResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            cardStatus.base.cardState = (CardState) p_cur->card_state;
            cardStatus.base.universalPinState = (PinState) p_cur->universal_pin_state;
            cardStatus.base.gsmUmtsSubscriptionAppIndex = p_cur->gsm_umts_subscription_app_index;
            cardStatus.base.cdmaSubscriptionAppIndex = p_cur->cdma_subscription_app_index;
            cardStatus.base.imsSubscriptionAppIndex = p_cur->ims_subscription_app_index;

            RIL_AppStatus *rilAppStatus = p_cur->applications;
            cardStatus.base.applications.resize(p_cur->num_applications);
            AppStatus *appStatus = cardStatus.base.applications.data();
            mtkLogD(LOG_TAG, "getIccCardStatusResponse: num_applications %d", p_cur->num_applications);
            for (int i = 0; i < p_cur->num_applications; i++) {
                appStatus[i].appType = (AppType) rilAppStatus[i].app_type;
                appStatus[i].appState = (AppState) rilAppStatus[i].app_state;
                appStatus[i].persoSubstate = (PersoSubstate) rilAppStatus[i].perso_substate;
                appStatus[i].aidPtr = convertCharPtrToHidlString(rilAppStatus[i].aid_ptr);
                appStatus[i].appLabelPtr = convertCharPtrToHidlString(
                        rilAppStatus[i].app_label_ptr);
                appStatus[i].pin1Replaced = rilAppStatus[i].pin1_replaced;
                appStatus[i].pin1 = (PinState) rilAppStatus[i].pin1;
                appStatus[i].pin2 = (PinState) rilAppStatus[i].pin2;
            }

            // Parameters add from radio hidl v1.2
            if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
                cardStatus.physicalSlotId = p_cur->physicalSlotId;
                cardStatus.atr = convertCharPtrToHidlString(p_cur->atr);
                cardStatus.iccid = convertCharPtrToHidlString(p_cur->iccId);
            }
        }

        // M @{
        s_cardState[slotId] = cardStatus.base.cardState;
        // M @}

        if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
            Return<void> retStatus = radioService[slotId]->mRadioResponseV1_2->
                    getIccCardStatusResponse_1_2(responseInfo, cardStatus);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) {
            /// MTK: ForSE @{
            Return<void> retStatus = radioService[slotId]->mRadioResponseSE->
                getIccCardStatusResponse(responseInfo, cardStatus);
            radioService[slotId]->checkReturnStatus(retStatus);
            /// MTK: ForSE @}
        } else {
            Return<void> retStatus = radioService[slotId]->mRadioResponse->
                    getIccCardStatusResponse(responseInfo, cardStatus.base);
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    } else {
        mtkLogE(LOG_TAG, "getIccCardStatusResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::supplyIccPinForAppResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyIccPinForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                supplyIccPinForAppResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyIccPinForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::supplyIccPukForAppResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyIccPukForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->supplyIccPukForAppResponse(
                responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyIccPukForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::supplyIccPin2ForAppResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyIccPin2ForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                supplyIccPin2ForAppResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyIccPin2ForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::supplyIccPuk2ForAppResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyIccPuk2ForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                supplyIccPuk2ForAppResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyIccPuk2ForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::changeIccPinForAppResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "changeIccPinForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                changeIccPinForAppResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "changeIccPinForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::changeIccPin2ForAppResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "changeIccPin2ForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                changeIccPin2ForAppResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "changeIccPin2ForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::supplyNetworkDepersonalizationResponse(int slotId,
                                                 int responseType, int serial, RIL_Errno e,
                                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyNetworkDepersonalizationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                supplyNetworkDepersonalizationResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyNetworkDepersonalizationResponse: radioService[%d]->mRadioResponse == "
                "NULL", slotId);
    }

    return 0;
}

int radio::getCurrentCallsResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCurrentCallsResponse: serial %d", serial);

    if (radioService[slotId] != NULL && (radioService[slotId]->mRadioResponse != NULL
            || radioService[slotId]->mRadioResponseV1_2 != NULL)) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<AOSP_V1_2::Call> callsV1_2;
        hidl_vec<AOSP_V1_0::Call> calls;
        if (response == NULL || (responseLen % sizeof(RIL_Call *)) != 0) {
            mtkLogE(LOG_TAG, "getCurrentCallsResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int num = responseLen / sizeof(RIL_Call *);
            if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
                callsV1_2.resize(num);

                for (int i = 0 ; i < num ; i++) {
                    RIL_Call *p_cur = ((RIL_Call **) response)[i];
                    /* each call info */
                    callsV1_2[i].base.state = (CallState) p_cur->state;
                    callsV1_2[i].base.index = p_cur->index;
                    callsV1_2[i].base.toa = p_cur->toa;
                    callsV1_2[i].base.isMpty = p_cur->isMpty;
                    callsV1_2[i].base.isMT = p_cur->isMT;
                    callsV1_2[i].base.als = p_cur->als;
                    callsV1_2[i].base.isVoice = p_cur->isVoice;
                    callsV1_2[i].base.isVoicePrivacy = p_cur->isVoicePrivacy;
                    callsV1_2[i].base.number = convertCharPtrToHidlString(p_cur->number);
                    callsV1_2[i].base.numberPresentation = (CallPresentation) p_cur->numberPresentation;
                    callsV1_2[i].base.name = convertCharPtrToHidlString(p_cur->name);
                    callsV1_2[i].base.namePresentation = (CallPresentation) p_cur->namePresentation;
                    if (p_cur->uusInfo != NULL && p_cur->uusInfo->uusData != NULL) {
                        RIL_UUS_Info *uusInfo = p_cur->uusInfo;
                        callsV1_2[i].base.uusInfo[0].uusType = (UusType) uusInfo->uusType;
                        callsV1_2[i].base.uusInfo[0].uusDcs = (UusDcs) uusInfo->uusDcs;
                        // convert uusInfo->uusData to a null-terminated string
                        char *nullTermStr = strndup(uusInfo->uusData, uusInfo->uusLength);
                        callsV1_2[i].base.uusInfo[0].uusData = nullTermStr;
                        free(nullTermStr);
                    }
                    callsV1_2[i].audioQuality = convertCallsSpeechCodecToHidlAudioQuality(p_cur->speechCodec);
                }
            } else {
                calls.resize(num);

                for (int i = 0 ; i < num ; i++) {
                    RIL_Call *p_cur = ((RIL_Call **) response)[i];
                    /* each call info */
                    calls[i].state = (CallState) p_cur->state;
                    calls[i].index = p_cur->index;
                    calls[i].toa = p_cur->toa;
                    calls[i].isMpty = p_cur->isMpty;
                    calls[i].isMT = p_cur->isMT;
                    calls[i].als = p_cur->als;
                    calls[i].isVoice = p_cur->isVoice;
                    calls[i].isVoicePrivacy = p_cur->isVoicePrivacy;
                    calls[i].number = convertCharPtrToHidlString(p_cur->number);
                    calls[i].numberPresentation = (CallPresentation) p_cur->numberPresentation;
                    calls[i].name = convertCharPtrToHidlString(p_cur->name);
                    calls[i].namePresentation = (CallPresentation) p_cur->namePresentation;
                    if (p_cur->uusInfo != NULL && p_cur->uusInfo->uusData != NULL) {
                        RIL_UUS_Info *uusInfo = p_cur->uusInfo;
                        calls[i].uusInfo[0].uusType = (UusType) uusInfo->uusType;
                        calls[i].uusInfo[0].uusDcs = (UusDcs) uusInfo->uusDcs;
                        // convert uusInfo->uusData to a null-terminated string
                        char *nullTermStr = strndup(uusInfo->uusData, uusInfo->uusLength);
                        calls[i].uusInfo[0].uusData = nullTermStr;
                        free(nullTermStr);
                    }
                }
            }
        }

        Return<void> retStatus = (radioService[slotId]->mRadioResponseV1_2 != NULL ?
                radioService[slotId]->mRadioResponseV1_2->getCurrentCallsResponse_1_2(
                        responseInfo, callsV1_2) :
                radioService[slotId]->mRadioResponse->getCurrentCallsResponse(
                        responseInfo, calls));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCurrentCallsResponse: radioService[%d] or mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::dialResponse(int slotId,
                       int responseType, int serial, RIL_Errno e, void *response,
                       size_t responseLen) {
    mtkLogD(LOG_TAG, "dialResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->dialResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "dialResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

/// M: CC: E911 request current status response
int radio::currentStatusResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "currentStatusResponse: serial %d", serial);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseMtk->
                                 currentStatusResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "currentStatusResponse: radioService[%d]->mRadioResponseMtk == NULL",
                                                                           slotId);
    }

    return 0;
}

/// M: CC: E911 request set ECC preferred RAT.
int radio::eccPreferredRatResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "eccPreferredRatResponse: serial %d", serial);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseMtk->
                                 eccPreferredRatResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "eccPreferredRatResponse: radioService[%d]->mRadioResponseMtk == NULL",
                                                                           slotId);
    }

    return 0;
}

int radio::getIMSIForAppResponse(int slotId,
                                int responseType, int serial, RIL_Errno e, void *response,
                                size_t responseLen) {
    mtkLogD(LOG_TAG, "getIMSIForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getIMSIForAppResponse(
                responseInfo, convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getIMSIForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::hangupConnectionResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "hangupConnectionResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->hangupConnectionResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "hangupConnectionResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::hangupWaitingOrBackgroundResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "hangupWaitingOrBackgroundResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->hangupWaitingOrBackgroundResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "hangupWaitingOrBackgroundResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::hangupForegroundResumeBackgroundResponse(int slotId, int responseType, int serial,
                                                    RIL_Errno e, void *response,
                                                    size_t responseLen) {
    mtkLogD(LOG_TAG, "hangupWaitingOrBackgroundResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->hangupWaitingOrBackgroundResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "hangupWaitingOrBackgroundResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::switchWaitingOrHoldingAndActiveResponse(int slotId, int responseType, int serial,
                                                   RIL_Errno e, void *response,
                                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "switchWaitingOrHoldingAndActiveResponse: serial %d", serial);
    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                    mRadioResponseIms->switchWaitingOrHoldingAndActiveResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if (radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->
                    switchWaitingOrHoldingAndActiveResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "switchWaitingOrHoldingAndActiveResponse: radioService[%d]->mRadioResponseIms"
                    " == NULL", slotId);
        }
    } else if (radioService[slotId]->mRadioResponseMtk != NULL
            || radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = (radioService[slotId]->mRadioResponseMtk != NULL ?
                radioService[slotId]->mRadioResponseMtk->switchWaitingOrHoldingAndActiveResponse(
                        responseInfo) :
                radioService[slotId]->mRadioResponse->switchWaitingOrHoldingAndActiveResponse(
                        responseInfo));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "switchWaitingOrHoldingAndActiveResponse: radioService[%d] or mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::conferenceResponse(int slotId, int responseType,
                             int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "conferenceResponse: serial %d", serial);
    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                    mRadioResponseIms->conferenceResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if (radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->conferenceResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "conferenceResponse: radioService[%d]->mRadioResponseIms"
                    " == NULL", slotId);
        }
    } else if (radioService[slotId]->mRadioResponseMtk != NULL
            || radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = (radioService[slotId]->mRadioResponseMtk != NULL ?
                radioService[slotId]->mRadioResponseMtk->conferenceResponse(responseInfo) :
                radioService[slotId]->mRadioResponse->conferenceResponse(responseInfo));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "conferenceResponse: radioService[%d] or mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::rejectCallResponse(int slotId, int responseType,
                             int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "rejectCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->rejectCallResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "rejectCallResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getLastCallFailCauseResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e, void *response,
                                       size_t responseLen) {
    mtkLogD(LOG_TAG, "getLastCallFailCauseResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        LastCallFailCauseInfo info = {};
        info.vendorCause = hidl_string();
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getCurrentCallsResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else if (responseLen == sizeof(int)) {
            int *pInt = (int *) response;
            info.causeCode = (LastCallFailCause) pInt[0];
        } else if (responseLen == sizeof(RIL_LastCallFailCauseInfo))  {
            RIL_LastCallFailCauseInfo *pFailCauseInfo = (RIL_LastCallFailCauseInfo *) response;
            info.causeCode = (LastCallFailCause) pFailCauseInfo->cause_code;
            info.vendorCause = convertCharPtrToHidlString(pFailCauseInfo->vendor_cause);
        } else {
            mtkLogE(LOG_TAG, "getCurrentCallsResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponse->getLastCallFailCauseResponse(
                responseInfo, info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getLastCallFailCauseResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}


int radio::getSignalStrengthResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getSignalStrengthResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseV1_2!= NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        AOSP_V1_2::SignalStrength signalStrength = {};
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "getSignalStrengthResponse_1_2: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilSignalStrengthToHal_1_2(response, responseLen, signalStrength);
        }

        Return<void> retStatus =
                radioService[slotId]->mRadioResponseV1_2->getSignalStrengthResponse_1_2(
                responseInfo, signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        SignalStrength signalStrength = {};
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "getSignalStrengthResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilSignalStrengthToHal(response, responseLen, signalStrength);
        }

        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->getSignalStrengthResponse(
                responseInfo, signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getSignalStrengthResponse_1_2: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getSignalStrengthWithWcdmaEcioResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getSignalStrengthWithWcdmaEcioResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        SignalStrengthWithWcdmaEcio signalStrength = {};
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "getSignalStrengthWithWcdmaEcioResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int32_t *rilSignalStrength = (int32_t *) response;
            signalStrength.gsm_signalStrength = rilSignalStrength[0];
            signalStrength.gsm_bitErrorRate = rilSignalStrength[1];
            signalStrength.wcdma_rscp = rilSignalStrength[19];
            signalStrength.wcdma_ecio = rilSignalStrength[20];
            signalStrength.cdma_dbm = rilSignalStrength[3];
            signalStrength.cdma_ecio = rilSignalStrength[4];
            signalStrength.evdo_dbm = rilSignalStrength[5];
            signalStrength.evdo_ecio = rilSignalStrength[6];
            signalStrength.evdo_signalNoiseRatio = rilSignalStrength[7];
            signalStrength.lte_signalStrength = rilSignalStrength[8];
            signalStrength.lte_rsrp = rilSignalStrength[9];
            signalStrength.lte_rsrq = rilSignalStrength[10];
            signalStrength.lte_rssnr = rilSignalStrength[11];
            signalStrength.lte_cqi = rilSignalStrength[12];
            signalStrength.tdscdma_rscp = rilSignalStrength[16];
        }

        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->getSignalStrengthWithWcdmaEcioResponse(
                responseInfo, signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getSignalStrengthWithWcdmaEcioResponse:mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

RIL_CellInfoType getCellInfoTypeRadioTechnology(char *rat) {
    if (rat == NULL) {
        return RIL_CELL_INFO_TYPE_NONE;
    }

    int radioTech = atoi(rat);

    switch(radioTech) {

        case RADIO_TECH_GPRS:
        case RADIO_TECH_EDGE:
        case RADIO_TECH_GSM: {
            return RIL_CELL_INFO_TYPE_GSM;
        }

        case RADIO_TECH_UMTS:
        case RADIO_TECH_HSDPA:
        case RADIO_TECH_HSUPA:
        case RADIO_TECH_HSPA:
        case RADIO_TECH_HSPAP: {
            return RIL_CELL_INFO_TYPE_WCDMA;
        }

        case RADIO_TECH_IS95A:
        case RADIO_TECH_IS95B:
        case RADIO_TECH_1xRTT:
        case RADIO_TECH_EVDO_0:
        case RADIO_TECH_EVDO_A:
        case RADIO_TECH_EVDO_B:
        case RADIO_TECH_EHRPD: {
            return RIL_CELL_INFO_TYPE_CDMA;
        }

        case RADIO_TECH_LTE:
        case RADIO_TECH_LTE_CA: {
            return RIL_CELL_INFO_TYPE_LTE;
        }

        case RADIO_TECH_TD_SCDMA: {
            return RIL_CELL_INFO_TYPE_TD_SCDMA;
        }

        default: {
            break;
        }
    }

    return RIL_CELL_INFO_TYPE_NONE;

}

void fillCellIdentityResponse(CellIdentity &cellIdentity, RIL_CellIdentity_v16 &rilCellIdentity) {

    cellIdentity.cellIdentityGsm.resize(0);
    cellIdentity.cellIdentityWcdma.resize(0);
    cellIdentity.cellIdentityCdma.resize(0);
    cellIdentity.cellIdentityTdscdma.resize(0);
    cellIdentity.cellIdentityLte.resize(0);
    cellIdentity.cellInfoType = (CellInfoType)rilCellIdentity.cellInfoType;
    switch(rilCellIdentity.cellInfoType) {

        case RIL_CELL_INFO_TYPE_GSM: {
            cellIdentity.cellIdentityGsm.resize(1);
            cellIdentity.cellIdentityGsm[0].mcc =
                    std::to_string(rilCellIdentity.cellIdentityGsm.mcc);
            cellIdentity.cellIdentityGsm[0].mnc =
                    std::to_string(rilCellIdentity.cellIdentityGsm.mnc);
            cellIdentity.cellIdentityGsm[0].lac = rilCellIdentity.cellIdentityGsm.lac;
            cellIdentity.cellIdentityGsm[0].cid = rilCellIdentity.cellIdentityGsm.cid;
            cellIdentity.cellIdentityGsm[0].arfcn = rilCellIdentity.cellIdentityGsm.arfcn;
            cellIdentity.cellIdentityGsm[0].bsic = rilCellIdentity.cellIdentityGsm.bsic;
            break;
        }

        case RIL_CELL_INFO_TYPE_WCDMA: {
            cellIdentity.cellIdentityWcdma.resize(1);
            cellIdentity.cellIdentityWcdma[0].mcc =
                    std::to_string(rilCellIdentity.cellIdentityWcdma.mcc);
            cellIdentity.cellIdentityWcdma[0].mnc =
                    std::to_string(rilCellIdentity.cellIdentityWcdma.mnc);
            cellIdentity.cellIdentityWcdma[0].lac = rilCellIdentity.cellIdentityWcdma.lac;
            cellIdentity.cellIdentityWcdma[0].cid = rilCellIdentity.cellIdentityWcdma.cid;
            cellIdentity.cellIdentityWcdma[0].psc = rilCellIdentity.cellIdentityWcdma.psc;
            cellIdentity.cellIdentityWcdma[0].uarfcn = rilCellIdentity.cellIdentityWcdma.uarfcn;
            break;
        }

        case RIL_CELL_INFO_TYPE_CDMA: {
            cellIdentity.cellIdentityCdma.resize(1);
            cellIdentity.cellIdentityCdma[0].networkId = rilCellIdentity.cellIdentityCdma.networkId;
            cellIdentity.cellIdentityCdma[0].systemId = rilCellIdentity.cellIdentityCdma.systemId;
            cellIdentity.cellIdentityCdma[0].baseStationId =
                    rilCellIdentity.cellIdentityCdma.basestationId;
            cellIdentity.cellIdentityCdma[0].longitude = rilCellIdentity.cellIdentityCdma.longitude;
            cellIdentity.cellIdentityCdma[0].latitude = rilCellIdentity.cellIdentityCdma.latitude;
            break;
        }

        case RIL_CELL_INFO_TYPE_LTE: {
            cellIdentity.cellIdentityLte.resize(1);
            cellIdentity.cellIdentityLte[0].mcc =
                    std::to_string(rilCellIdentity.cellIdentityLte.mcc);
            cellIdentity.cellIdentityLte[0].mnc =
                    std::to_string(rilCellIdentity.cellIdentityLte.mnc);
            cellIdentity.cellIdentityLte[0].ci = rilCellIdentity.cellIdentityLte.ci;
            cellIdentity.cellIdentityLte[0].pci = rilCellIdentity.cellIdentityLte.pci;
            cellIdentity.cellIdentityLte[0].tac = rilCellIdentity.cellIdentityLte.tac;
            cellIdentity.cellIdentityLte[0].earfcn = rilCellIdentity.cellIdentityLte.earfcn;
            break;
        }

        case RIL_CELL_INFO_TYPE_TD_SCDMA: {
            cellIdentity.cellIdentityTdscdma.resize(1);
            cellIdentity.cellIdentityTdscdma[0].mcc =
                    std::to_string(rilCellIdentity.cellIdentityTdscdma.mcc);
            cellIdentity.cellIdentityTdscdma[0].mnc =
                    std::to_string(rilCellIdentity.cellIdentityTdscdma.mnc);
            cellIdentity.cellIdentityTdscdma[0].lac = rilCellIdentity.cellIdentityTdscdma.lac;
            cellIdentity.cellIdentityTdscdma[0].cid = rilCellIdentity.cellIdentityTdscdma.cid;
            cellIdentity.cellIdentityTdscdma[0].cpid = rilCellIdentity.cellIdentityTdscdma.cpid;
            break;
        }

        default: {
            break;
        }
    }
}

// It's for V1_2 CellIdentity
void fillCellIdentityResponse_1_2(AOSP_V1_2::CellIdentity &cellIdentity, const RIL_CellIdentity_v16 &rilCellIdentity) {
    char buff[6];
    memset(buff, 0, 6 * sizeof(char));
    hidl_string mnc_string;
    cellIdentity.cellIdentityGsm.resize(0);
    cellIdentity.cellIdentityWcdma.resize(0);
    cellIdentity.cellIdentityCdma.resize(0);
    cellIdentity.cellIdentityTdscdma.resize(0);
    cellIdentity.cellIdentityLte.resize(0);
    cellIdentity.cellInfoType = (CellInfoType)rilCellIdentity.cellInfoType;
    switch (rilCellIdentity.cellInfoType) {
        case RIL_CELL_INFO_TYPE_GSM: {
            cellIdentity.cellIdentityGsm.resize(1);
            cellIdentity.cellIdentityGsm[0].base.mcc =
                    std::to_string(rilCellIdentity.cellIdentityGsm.mcc);
            if (rilCellIdentity.cellIdentityGsm.mnc_len == 3) {
                snprintf(buff, 6, "%03d", rilCellIdentity.cellIdentityGsm.mnc);
            } else {
                snprintf(buff, 6, "%02d", rilCellIdentity.cellIdentityGsm.mnc);
            }
            mnc_string = buff;
            cellIdentity.cellIdentityGsm[0].base.mnc = mnc_string;
            cellIdentity.cellIdentityGsm[0].base.lac = rilCellIdentity.cellIdentityGsm.lac;
            cellIdentity.cellIdentityGsm[0].base.cid = rilCellIdentity.cellIdentityGsm.cid;
            cellIdentity.cellIdentityGsm[0].base.arfcn = rilCellIdentity.cellIdentityGsm.arfcn;
            cellIdentity.cellIdentityGsm[0].base.bsic = rilCellIdentity.cellIdentityGsm.bsic;
            cellIdentity.cellIdentityGsm[0].operatorNames.alphaLong =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityGsm.operName.long_name);
            cellIdentity.cellIdentityGsm[0].operatorNames.alphaShort =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityGsm.operName.short_name);
            break;
        }

        case RIL_CELL_INFO_TYPE_WCDMA: {
            cellIdentity.cellIdentityWcdma.resize(1);
            cellIdentity.cellIdentityWcdma[0].base.mcc =
                    std::to_string(rilCellIdentity.cellIdentityWcdma.mcc);
            if (rilCellIdentity.cellIdentityWcdma.mnc_len == 3) {
                snprintf(buff, 6, "%03d", rilCellIdentity.cellIdentityWcdma.mnc);
            } else {
                snprintf(buff, 6, "%02d", rilCellIdentity.cellIdentityWcdma.mnc);
            }
            mnc_string = buff;
            cellIdentity.cellIdentityWcdma[0].base.mnc = mnc_string;
            cellIdentity.cellIdentityWcdma[0].base.lac = rilCellIdentity.cellIdentityWcdma.lac;
            cellIdentity.cellIdentityWcdma[0].base.cid = rilCellIdentity.cellIdentityWcdma.cid;
            cellIdentity.cellIdentityWcdma[0].base.psc = rilCellIdentity.cellIdentityWcdma.psc;
            cellIdentity.cellIdentityWcdma[0].base.uarfcn = rilCellIdentity.cellIdentityWcdma.uarfcn;
            cellIdentity.cellIdentityWcdma[0].operatorNames.alphaLong =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityWcdma.operName.long_name);
            cellIdentity.cellIdentityWcdma[0].operatorNames.alphaShort =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityWcdma.operName.short_name);
            break;
        }

        case RIL_CELL_INFO_TYPE_CDMA: {
            cellIdentity.cellIdentityCdma.resize(1);
            cellIdentity.cellIdentityCdma[0].base.networkId = rilCellIdentity.cellIdentityCdma.networkId;
            cellIdentity.cellIdentityCdma[0].base.systemId = rilCellIdentity.cellIdentityCdma.systemId;
            cellIdentity.cellIdentityCdma[0].base.baseStationId =
                    rilCellIdentity.cellIdentityCdma.basestationId;
            cellIdentity.cellIdentityCdma[0].base.longitude = rilCellIdentity.cellIdentityCdma.longitude;
            cellIdentity.cellIdentityCdma[0].base.latitude = rilCellIdentity.cellIdentityCdma.latitude;
            cellIdentity.cellIdentityCdma[0].operatorNames.alphaLong =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityCdma.operName.long_name);
            cellIdentity.cellIdentityCdma[0].operatorNames.alphaShort =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityCdma.operName.short_name);
            break;
        }

        case RIL_CELL_INFO_TYPE_LTE: {
            cellIdentity.cellIdentityLte.resize(1);
            cellIdentity.cellIdentityLte[0].base.mcc =
                    std::to_string(rilCellIdentity.cellIdentityLte.mcc);
            if (rilCellIdentity.cellIdentityLte.mnc_len == 3) {
                snprintf(buff, 6, "%03d", rilCellIdentity.cellIdentityLte.mnc);
            } else {
                snprintf(buff, 6, "%02d", rilCellIdentity.cellIdentityLte.mnc);
            }
            mnc_string = buff;
            cellIdentity.cellIdentityLte[0].base.mnc = mnc_string;
            cellIdentity.cellIdentityLte[0].base.ci = rilCellIdentity.cellIdentityLte.ci;
            cellIdentity.cellIdentityLte[0].base.pci = rilCellIdentity.cellIdentityLte.pci;
            cellIdentity.cellIdentityLte[0].base.tac = rilCellIdentity.cellIdentityLte.tac;
            cellIdentity.cellIdentityLte[0].base.earfcn = rilCellIdentity.cellIdentityLte.earfcn;
            cellIdentity.cellIdentityLte[0].operatorNames.alphaLong =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityLte.operName.long_name);
            cellIdentity.cellIdentityLte[0].operatorNames.alphaShort =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityLte.operName.short_name);
            cellIdentity.cellIdentityLte[0].bandwidth = rilCellIdentity.cellIdentityLte.bandwidth;
            break;
        }

        case RIL_CELL_INFO_TYPE_TD_SCDMA: {
            cellIdentity.cellIdentityTdscdma.resize(1);
            cellIdentity.cellIdentityTdscdma[0].base.mcc =
                    std::to_string(rilCellIdentity.cellIdentityTdscdma.mcc);
            if (rilCellIdentity.cellIdentityTdscdma.mnc_len == 3) {
                snprintf(buff, 6, "%03d", rilCellIdentity.cellIdentityTdscdma.mnc);
            } else {
                snprintf(buff, 6, "%02d", rilCellIdentity.cellIdentityTdscdma.mnc);
            }
            mnc_string = buff;
            cellIdentity.cellIdentityTdscdma[0].base.mnc = mnc_string;
            cellIdentity.cellIdentityTdscdma[0].base.lac = rilCellIdentity.cellIdentityTdscdma.lac;
            cellIdentity.cellIdentityTdscdma[0].base.cid = rilCellIdentity.cellIdentityTdscdma.cid;
            cellIdentity.cellIdentityTdscdma[0].base.cpid = rilCellIdentity.cellIdentityTdscdma.cpid;
            cellIdentity.cellIdentityTdscdma[0].operatorNames.alphaLong =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityTdscdma.operName.long_name);
            cellIdentity.cellIdentityTdscdma[0].operatorNames.alphaShort =
                    convertCharPtrToHidlString(rilCellIdentity.cellIdentityTdscdma.operName.short_name);
            break;
        }

        default: {
            break;
        }
    }
}

int convertResponseStringEntryToInt(char **response, int index, int numStrings) {
    if ((response != NULL) &&  (numStrings > index) && (response[index] != NULL)) {
        return atoi(response[index]);
    }

    return -1;
}

void fillCellIdentityFromVoiceRegStateResponseString(CellIdentity &cellIdentity,
        int numStrings, char** response) {

    RIL_CellIdentity_v16 rilCellIdentity;
    memset(&rilCellIdentity, -1, sizeof(RIL_CellIdentity_v16));

    rilCellIdentity.cellInfoType = getCellInfoTypeRadioTechnology(response[3]);
    switch(rilCellIdentity.cellInfoType) {

        case RIL_CELL_INFO_TYPE_GSM: {
            rilCellIdentity.cellIdentityGsm.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityGsm.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }

        case RIL_CELL_INFO_TYPE_WCDMA: {
            rilCellIdentity.cellIdentityWcdma.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityWcdma.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            rilCellIdentity.cellIdentityWcdma.psc =
                    convertResponseStringEntryToInt(response, 14, numStrings);
            break;
        }

        case RIL_CELL_INFO_TYPE_TD_SCDMA:{
            rilCellIdentity.cellIdentityTdscdma.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityTdscdma.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }

        case RIL_CELL_INFO_TYPE_CDMA:{
            rilCellIdentity.cellIdentityCdma.basestationId =
                    convertResponseStringEntryToInt(response, 4, numStrings);
            rilCellIdentity.cellIdentityCdma.longitude =
                    convertResponseStringEntryToInt(response, 5, numStrings);
            rilCellIdentity.cellIdentityCdma.latitude =
                    convertResponseStringEntryToInt(response, 6, numStrings);
            rilCellIdentity.cellIdentityCdma.systemId =
                    convertResponseStringEntryToInt(response, 8, numStrings);
            rilCellIdentity.cellIdentityCdma.networkId =
                    convertResponseStringEntryToInt(response, 9, numStrings);
            break;
        }

        case RIL_CELL_INFO_TYPE_LTE:{
            rilCellIdentity.cellIdentityLte.tac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityLte.ci =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }

        default: {
            break;
        }
    }

    fillCellIdentityResponse(cellIdentity, rilCellIdentity);
}

/* Fill Cell Identity info from Data Registration State Response.
 * This fucntion is applicable only for RIL Version < 15.
 * Response is a  "char **".
 * First and Second entries are in hex string format
 * and rest are integers represented in ascii format. */
void fillCellIdentityFromDataRegStateResponseString(CellIdentity &cellIdentity,
        int numStrings, char** response) {

    RIL_CellIdentity_v16 rilCellIdentity;
    memset(&rilCellIdentity, -1, sizeof(RIL_CellIdentity_v16));

    rilCellIdentity.cellInfoType = getCellInfoTypeRadioTechnology(response[3]);
    switch(rilCellIdentity.cellInfoType) {
        case RIL_CELL_INFO_TYPE_GSM: {
            rilCellIdentity.cellIdentityGsm.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityGsm.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }
        case RIL_CELL_INFO_TYPE_WCDMA: {
            rilCellIdentity.cellIdentityWcdma.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityWcdma.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }
        case RIL_CELL_INFO_TYPE_TD_SCDMA:{
            rilCellIdentity.cellIdentityTdscdma.lac =
                    convertResponseStringEntryToInt(response, 1, numStrings);
            rilCellIdentity.cellIdentityTdscdma.cid =
                    convertResponseStringEntryToInt(response, 2, numStrings);
            break;
        }
        case RIL_CELL_INFO_TYPE_LTE: {
            rilCellIdentity.cellIdentityLte.tac =
                    convertResponseStringEntryToInt(response, 6, numStrings);
            rilCellIdentity.cellIdentityLte.pci =
                    convertResponseStringEntryToInt(response, 7, numStrings);
            rilCellIdentity.cellIdentityLte.ci =
                    convertResponseStringEntryToInt(response, 8, numStrings);
            break;
        }
        default: {
            break;
        }
    }

    fillCellIdentityResponse(cellIdentity, rilCellIdentity);
}

// It's also getVoiceRegistrationStateResponse_1_2
int radio::getVoiceRegistrationStateResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getVoiceRegistrationStateResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        // use V1_2
        AOSP_V1_2::VoiceRegStateResult voiceRegResponse = {};
        if (response == NULL) {
               mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse Invalid response: NULL");
               if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_VoiceRegistrationStateResponse *voiceRegState =
                    (RIL_VoiceRegistrationStateResponse *)response;

            if (responseLen != sizeof(RIL_VoiceRegistrationStateResponse)) {
                mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                voiceRegResponse.regState = (RegState) voiceRegState->regState;
                voiceRegResponse.rat = voiceRegState->rat;;
                voiceRegResponse.cssSupported = voiceRegState->cssSupported;
                voiceRegResponse.roamingIndicator = voiceRegState->roamingIndicator;
                voiceRegResponse.systemIsInPrl = voiceRegState->systemIsInPrl;
                voiceRegResponse.defaultRoamingIndicator = voiceRegState->defaultRoamingIndicator;
                voiceRegResponse.reasonForDenial = voiceRegState->reasonForDenial;
                fillCellIdentityResponse_1_2(voiceRegResponse.cellIdentity,
                        voiceRegState->cellIdentity);
            }
        }
        // response V1_2
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseV1_2->getVoiceRegistrationStateResponse_1_2(
                responseInfo, voiceRegResponse);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        VoiceRegStateResult voiceRegResponse = {};
        int numStrings = responseLen / sizeof(char *);
        if (response == NULL) {
               mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse Invalid response: NULL");
               if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else if (s_vendorFunctions->version <= 14) {
            if (numStrings != 15) {
                mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                char **resp = (char **) response;
                voiceRegResponse.regState = (RegState) ATOI_NULL_HANDLED_DEF(resp[0], 4);
                voiceRegResponse.rat = ATOI_NULL_HANDLED(resp[3]);
                voiceRegResponse.cssSupported = ATOI_NULL_HANDLED_DEF(resp[7], 0);
                voiceRegResponse.roamingIndicator = ATOI_NULL_HANDLED(resp[10]);
                voiceRegResponse.systemIsInPrl = ATOI_NULL_HANDLED_DEF(resp[11], 0);
                voiceRegResponse.defaultRoamingIndicator = ATOI_NULL_HANDLED_DEF(resp[12], 0);
                voiceRegResponse.reasonForDenial = ATOI_NULL_HANDLED_DEF(resp[13], 0);
                fillCellIdentityFromVoiceRegStateResponseString(voiceRegResponse.cellIdentity,
                         numStrings, resp);
            }
        } else {
            RIL_VoiceRegistrationStateResponse *voiceRegState =
                    (RIL_VoiceRegistrationStateResponse *)response;

            if (responseLen != sizeof(RIL_VoiceRegistrationStateResponse)) {
                mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                voiceRegResponse.regState = (RegState) voiceRegState->regState;
                voiceRegResponse.rat = voiceRegState->rat;;
                voiceRegResponse.cssSupported = voiceRegState->cssSupported;
                voiceRegResponse.roamingIndicator = voiceRegState->roamingIndicator;
                voiceRegResponse.systemIsInPrl = voiceRegState->systemIsInPrl;
                voiceRegResponse.defaultRoamingIndicator = voiceRegState->defaultRoamingIndicator;
                voiceRegResponse.reasonForDenial = voiceRegState->reasonForDenial;
                fillCellIdentityResponse(voiceRegResponse.cellIdentity,
                        voiceRegState->cellIdentity);
            }
        }
        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->getVoiceRegistrationStateResponse(
                responseInfo, voiceRegResponse);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getVoiceRegistrationStateResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

// It's also getDataRegistrationStateResponse_1_2
int radio::getDataRegistrationStateResponse(int slotId,
                                           int responseType, int serial, RIL_Errno e,
                                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getDataRegistrationStateResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseV1_2!= NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        // use V1_2
        AOSP_V1_2::DataRegStateResult dataRegResponse = {};
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getDataRegistrationStateResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_DataRegistrationStateResponse *dataRegState =
                    (RIL_DataRegistrationStateResponse *)response;

            if (responseLen != sizeof(RIL_DataRegistrationStateResponse)) {
                mtkLogE(LOG_TAG, "getDataRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                dataRegResponse.regState = (RegState) dataRegState->regState;
                dataRegResponse.rat = dataRegState->rat;;
                dataRegResponse.reasonDataDenied = dataRegState->reasonDataDenied;
                dataRegResponse.maxDataCalls = dataRegState->maxDataCalls;
                fillCellIdentityResponse_1_2(dataRegResponse.cellIdentity, dataRegState->cellIdentity);
            }
        }
        // response V1_2
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseV1_2->getDataRegistrationStateResponse_1_2(responseInfo,
                dataRegResponse);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        DataRegStateResult dataRegResponse = {};
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getDataRegistrationStateResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else if (s_vendorFunctions->version <= 14) {
            int numStrings = responseLen / sizeof(char *);
            if ((numStrings != 6) && (numStrings != 11)) {
                mtkLogE(LOG_TAG, "getDataRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                char **resp = (char **) response;
                dataRegResponse.regState = (RegState) ATOI_NULL_HANDLED_DEF(resp[0], 4);
                dataRegResponse.rat =  ATOI_NULL_HANDLED_DEF(resp[3], 0);
                dataRegResponse.reasonDataDenied =  ATOI_NULL_HANDLED(resp[4]);
                dataRegResponse.maxDataCalls =  ATOI_NULL_HANDLED_DEF(resp[5], 1);
                fillCellIdentityFromDataRegStateResponseString(dataRegResponse.cellIdentity,
                         numStrings, resp);
            }
        } else {
            RIL_DataRegistrationStateResponse *dataRegState =
                    (RIL_DataRegistrationStateResponse *)response;

            if (responseLen != sizeof(RIL_DataRegistrationStateResponse)) {
                mtkLogE(LOG_TAG, "getDataRegistrationStateResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                dataRegResponse.regState = (RegState) dataRegState->regState;
                dataRegResponse.rat = dataRegState->rat;;
                dataRegResponse.reasonDataDenied = dataRegState->reasonDataDenied;
                dataRegResponse.maxDataCalls = dataRegState->maxDataCalls;
                fillCellIdentityResponse(dataRegResponse.cellIdentity, dataRegState->cellIdentity);
            }
        }

        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->getDataRegistrationStateResponse(responseInfo,
                dataRegResponse);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getDataRegistrationStateResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getOperatorResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "getOperatorResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_string longName;
        hidl_string shortName;
        hidl_string numeric;
        int numStrings = responseLen / sizeof(char *);
        if (response == NULL || numStrings != 3) {
            mtkLogE(LOG_TAG, "getOperatorResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;

        } else {
            char **resp = (char **) response;
            longName = convertCharPtrToHidlString(resp[0]);
            shortName = convertCharPtrToHidlString(resp[1]);
            numeric = convertCharPtrToHidlString(resp[2]);
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getOperatorResponse(
                responseInfo, longName, shortName, numeric);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getOperatorResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setRadioPowerResponse(int slotId,
                                int responseType, int serial, RIL_Errno e, void *response,
                                size_t responseLen) {
    mtkLogD(LOG_TAG, "setRadioPowerResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->setRadioPowerResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRadioPowerResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendDtmfResponse(int slotId,
                           int responseType, int serial, RIL_Errno e, void *response,
                           size_t responseLen) {
    mtkLogD(LOG_TAG, "sendDtmfResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->sendDtmfResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendDtmfResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

SendSmsResult makeSendSmsResult(RadioResponseInfo& responseInfo, int serial, int responseType,
                                RIL_Errno e, void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    SendSmsResult result = {};

    if (response == NULL || responseLen != sizeof(RIL_SMS_Response)) {
        mtkLogE(LOG_TAG, "Invalid response: NULL");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        result.ackPDU = hidl_string();
    } else {
        RIL_SMS_Response *resp = (RIL_SMS_Response *) response;
        result.messageRef = resp->messageRef;
        result.ackPDU = convertCharPtrToHidlString(resp->ackPDU);
        result.errorCode = resp->errorCode;
    }
    return result;
}

int radio::sendSmsResponse(int slotId,
                          int responseType, int serial, RIL_Errno e, void *response,
                          size_t responseLen) {
    mtkLogD(LOG_TAG, "sendSmsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        SendSmsResult result = makeSendSmsResult(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus = radioService[slotId]->mRadioResponse->sendSmsResponse(responseInfo,
                result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendSmsResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::sendSMSExpectMoreResponse(int slotId,
                                    int responseType, int serial, RIL_Errno e, void *response,
                                    size_t responseLen) {
    mtkLogD(LOG_TAG, "sendSMSExpectMoreResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        SendSmsResult result = makeSendSmsResult(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus = radioService[slotId]->mRadioResponse->sendSMSExpectMoreResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendSMSExpectMoreResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setupDataCallResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e, void *response,
                                 size_t responseLen) {
    mtkLogD(LOG_TAG, "setupDataCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        SetupDataCallResult result = {};
        // M: [OD over ePDG]
        // remark AOSP
        //if (response == NULL || responseLen != sizeof(RIL_Data_Call_Response_v11)) {
        if (response == NULL || responseLen != sizeof(MTK_RIL_Data_Call_Response_v11)) {
            mtkLogE(LOG_TAG, "setupDataCallResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            result.status = DataCallFailCause::ERROR_UNSPECIFIED;
            result.type = hidl_string();
            result.ifname = hidl_string();
            result.addresses = hidl_string();
            result.dnses = hidl_string();
            result.gateways = hidl_string();
            result.pcscf = hidl_string();
        } else {
            // M: [OD over ePDG]
            // remark AOSP
            //convertRilDataCallToHal((RIL_Data_Call_Response_v11 *) response, result);
            convertRilDataCallToHalEx((MTK_RIL_Data_Call_Response_v11 *) response, result, slotId);
        }

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus = radioService[slotId]->mRadioResponse->setupDataCallResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setupDataCallResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

IccIoResult responseIccIo(RadioResponseInfo& responseInfo, int serial, int responseType,
                           RIL_Errno e, void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    IccIoResult result = {};

    if (response == NULL || responseLen != sizeof(RIL_SIM_IO_Response)) {
        mtkLogE(LOG_TAG, "Invalid response: NULL");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        result.simResponse = hidl_string();
    } else {
        RIL_SIM_IO_Response *resp = (RIL_SIM_IO_Response *) response;
        result.sw1 = resp->sw1;
        result.sw2 = resp->sw2;
        result.simResponse = convertCharPtrToHidlString(resp->simResponse);
    }
    return result;
}

int radio::iccIOForAppResponse(int slotId,
                      int responseType, int serial, RIL_Errno e, void *response,
                      size_t responseLen) {
    mtkLogD(LOG_TAG, "iccIOForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        IccIoResult result = responseIccIo(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus = radioService[slotId]->mRadioResponse->iccIOForAppResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "iccIOForAppResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::sendUssdResponse(int slotId,
                           int responseType, int serial, RIL_Errno e, void *response,
                           size_t responseLen) {
    mtkLogD(LOG_TAG, "sendUssdResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->sendUssdResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendUssdResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::cancelPendingUssdResponse(int slotId,
                                    int responseType, int serial, RIL_Errno e, void *response,
                                    size_t responseLen) {
    mtkLogD(LOG_TAG, "cancelPendingUssdResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->cancelPendingUssdResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cancelPendingUssdResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getClirResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "getClirResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int n = -1, m = -1;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || numInts != 2) {
            mtkLogE(LOG_TAG, "getClirResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            n = pInt[0];
            m = pInt[1];
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getClirResponse(responseInfo,
                n, m);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getClirResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setClirResponse(int slotId,
                          int responseType, int serial, RIL_Errno e, void *response,
                          size_t responseLen) {
    mtkLogD(LOG_TAG, "setClirResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->setClirResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setClirResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getCallForwardStatusResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCallForwardStatusResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<CallForwardInfo> callForwardInfos;

        if (response == NULL || responseLen % sizeof(RIL_CallForwardInfo *) != 0) {
            mtkLogE(LOG_TAG, "getCallForwardStatusResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int num = responseLen / sizeof(RIL_CallForwardInfo *);
            callForwardInfos.resize(num);
            for (int i = 0 ; i < num; i++) {
                RIL_CallForwardInfo *resp = ((RIL_CallForwardInfo **) response)[i];
                callForwardInfos[i].status = (CallForwardInfoStatus) resp->status;
                callForwardInfos[i].reason = resp->reason;
                callForwardInfos[i].serviceClass = resp->serviceClass;
                callForwardInfos[i].toa = resp->toa;
                callForwardInfos[i].number = convertCharPtrToHidlString(resp->number);
                callForwardInfos[i].timeSeconds = resp->timeSeconds;
            }
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponse->getCallForwardStatusResponse(
                responseInfo, callForwardInfos);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCallForwardStatusResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setCallForwardResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e, void *response,
                                 size_t responseLen) {
    mtkLogD(LOG_TAG, "setCallForwardResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->setCallForwardResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCallForwardResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getCallWaitingResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e, void *response,
                                 size_t responseLen) {
    mtkLogD(LOG_TAG, "getCallWaitingResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        bool enable = false;
        int serviceClass = -1;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || numInts != 2) {
            mtkLogE(LOG_TAG, "getCallWaitingResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            enable = pInt[0] == 1 ? true : false;
            serviceClass = pInt[1];
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getClirResponse(responseInfo,
                enable, serviceClass);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCallWaitingResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setCallWaitingResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e, void *response,
                                 size_t responseLen) {
    mtkLogD(LOG_TAG, "setCallWaitingResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->setCallWaitingResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCallWaitingResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::acknowledgeLastIncomingGsmSmsResponse(int slotId,
                                                int responseType, int serial, RIL_Errno e,
                                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingGsmSmsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponse->acknowledgeLastIncomingGsmSmsResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acknowledgeLastIncomingGsmSmsResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::acceptCallResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "acceptCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->acceptCallResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acceptCallResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::deactivateDataCallResponse(int slotId,
                                                int responseType, int serial, RIL_Errno e,
                                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "deactivateDataCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus = radioService[slotId]->mRadioResponse->deactivateDataCallResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "deactivateDataCallResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getFacilityLockForAppResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getFacilityLockForAppResponse: serial %d", serial);

    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->
                    getFacilityLockForAppResponse(responseInfo, ret);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getFacilityLockForAppResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
            // If Ims response function doesn't exist, call AOSP response function
            if (radioService[slotId]->mRadioResponse != NULL) {
                RadioResponseInfo responseInfo = {};
                int ret = responseInt(responseInfo, serial, responseType, e, response,
                        responseLen);
                Return<void> retStatus = radioService[slotId]->mRadioResponse->
                        getFacilityLockForAppResponse(responseInfo, ret);
                radioService[slotId]->checkReturnStatus(retStatus);
            } else {
                mtkLogE(LOG_TAG,
                        "getFacilityLockForAppResponse: radioService[%d]->mRadioResponse == NULL",
                        slotId);
            }
        }
    } else {
        if (radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
            Return<void> retStatus = radioService[slotId]->mRadioResponse->
                    getFacilityLockForAppResponse(responseInfo, ret);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getFacilityLockForAppResponse: radioService[%d]->mRadioResponse == NULL",
                    slotId);
        }
    }
    return 0;
}

int radio::setFacilityLockForAppResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setFacilityLockForAppResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setFacilityLockForAppResponse(responseInfo,
                ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setFacilityLockForAppResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setBarringPasswordResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setBarringPasswordResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setBarringPasswordResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setBarringPasswordResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getNetworkSelectionModeResponse(int slotId,
                                          int responseType, int serial, RIL_Errno e, void *response,
                                          size_t responseLen) {
    mtkLogD(LOG_TAG, "getNetworkSelectionModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        bool manual = false;
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "getNetworkSelectionModeResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            manual = pInt[0] == 1 ? true : false;
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getNetworkSelectionModeResponse(
                responseInfo,
                manual);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getNetworkSelectionModeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setNetworkSelectionModeAutomaticResponse(int slotId, int responseType, int serial,
                                                    RIL_Errno e, void *response,
                                                    size_t responseLen) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeAutomaticResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setNetworkSelectionModeAutomaticResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setNetworkSelectionModeAutomaticResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::setNetworkSelectionModeManualResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeManualResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setNetworkSelectionModeManualResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acceptCallResponse: radioService[%d]->setNetworkSelectionModeManualResponse "
                "== NULL", slotId);
    }

    return 0;
}

// MTK-START: SIM
int radio::getATRResponse(int slotId,
        int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "[%d] getATRResponse: serial %d", slotId, serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus;
        if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) { /// MTK: ForSE
            retStatus = radioService[slotId]->mRadioResponseSE->getATRResponse(
                responseInfo,
                convertCharPtrToHidlString((char *) response));
        } else {
            retStatus = radioService[slotId]->mRadioResponseMtk->getATRResponse(
                responseInfo,
                convertCharPtrToHidlString((char *) response));
        }
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvReadItemResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getIccidResponse(int slotId, int responseType, int serial, RIL_Errno e, void *response,
        size_t responseLen) {
    mtkLogD(LOG_TAG, "getIccidResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->getIccidResponse(
                responseInfo, convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvReadItemResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setSimPowerResponse(int slotId,
        int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSimPowerResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setSimPowerResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvReadItemResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}
// MTK-END

// MTK-START: SIM ME LOCK
int radio::queryNetworkLockResponse(int slotId,
        int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryNetworkLockResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        RIL_SimMeLockCatInfo *pLockCatInfo = NULL;
        int category = -1, state = -1, retry_cnt = -1, autolock_cnt = -1,
                num_set = -1, total_set = -1, key_state = -1;

        if (response == NULL || responseLen != sizeof(RIL_SimMeLockCatInfo)) {
            mtkLogE(LOG_TAG, "queryNetworkLockResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            pLockCatInfo = (RIL_SimMeLockCatInfo *) response;

            category = pLockCatInfo->catagory;
            state = pLockCatInfo->state;
            retry_cnt = pLockCatInfo->retry_cnt;
            autolock_cnt = pLockCatInfo->autolock_cnt;
            num_set = pLockCatInfo->num_set;
            total_set = pLockCatInfo->total_set;
            key_state = pLockCatInfo->key_state;
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->queryNetworkLockResponse(
                responseInfo, category, state, retry_cnt, autolock_cnt, num_set, total_set, key_state);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "queryNetworkLockResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setNetworkLockResponse(int slotId,
        int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setNetworkLockResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setNetworkLockResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setNetworkLockResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::supplyDepersonalizationResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyDepersonalizationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                supplyDepersonalizationResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyDepersonalizationResponse: radioService[%d]->mRadioResponseMtk == "
                "NULL", slotId);
    }

    return 0;
}
// MTK-END

int convertOperatorStatusToInt(const char *str) {
    if (strncmp("unknown", str, 9) == 0) {
        return (int) OperatorStatus::UNKNOWN;
    } else if (strncmp("available", str, 9) == 0) {
        return (int) OperatorStatus::AVAILABLE;
    } else if (strncmp("current", str, 9) == 0) {
        return (int) OperatorStatus::CURRENT;
    } else if (strncmp("forbidden", str, 9) == 0) {
        return (int) OperatorStatus::FORBIDDEN;
    } else {
        return -1;
    }
}

int radio::getAvailableNetworksResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "getAvailableNetworksResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<OperatorInfo> networks;
        if (response == NULL || responseLen % (4 * sizeof(char *))!= 0) {
            mtkLogE(LOG_TAG, "getAvailableNetworksResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            networks.resize(numStrings/4);
            for (int i = 0, j = 0; i < numStrings; i = i + 4, j++) {
                networks[j].alphaLong = convertCharPtrToHidlString(resp[i]);
                networks[j].alphaShort = convertCharPtrToHidlString(resp[i + 1]);
                networks[j].operatorNumeric = convertCharPtrToHidlString(resp[i + 2]);
                int status = convertOperatorStatusToInt(resp[i + 3]);
                if (status == -1) {
                    if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
                } else {
                    networks[j].status = (OperatorStatus) status;
                }
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getAvailableNetworksResponse(responseInfo,
                networks);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getAvailableNetworksResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::startDtmfResponse(int slotId,
                            int responseType, int serial, RIL_Errno e,
                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "startDtmfResponse: serial %d", serial);

    if(radioService[slotId] == NULL) {
        mtkLogE(LOG_TAG, "startDtmfResponse: radioService[%d] == NULL", slotId);
        return 0;
    }

    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseIms->startDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if(radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->startDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "startDtmfResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    }
    else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseMtk->startDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if(radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->startDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    }

    return 0;
}

int radio::stopDtmfResponse(int slotId,
                           int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "stopDtmfResponse: serial %d", serial);

    if(radioService[slotId] == NULL) {
        mtkLogE(LOG_TAG, "stopDtmfResponse: radioService[%d] == NULL", slotId);
        return 0;
    }

    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseIms->stopDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if(radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->stopDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "stopDtmfResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    }
    else {

        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseMtk->stopDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if(radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->stopDtmfResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    }

    return 0;
}

int radio::getBasebandVersionResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getBasebandVersionResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getBasebandVersionResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getBasebandVersionResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::separateConnectionResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "separateConnectionResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL
            || radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = (radioService[slotId]->mRadioResponseMtk != NULL ?
                radioService[slotId]->mRadioResponseMtk->separateConnectionResponse(responseInfo) :
                radioService[slotId]->mRadioResponse->separateConnectionResponse(responseInfo));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "separateConnectionResponse: radioService[%d] or mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setMuteResponse(int slotId,
                          int responseType, int serial, RIL_Errno e,
                          void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setMuteResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setMuteResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setMuteResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getMuteResponse(int slotId,
                          int responseType, int serial, RIL_Errno e, void *response,
                          size_t responseLen) {
    mtkLogD(LOG_TAG, "getMuteResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        bool enable = false;
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "getMuteResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            enable = pInt[0] == 1 ? true : false;
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getMuteResponse(responseInfo,
                enable);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getMuteResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getClipResponse(int slotId,
                          int responseType, int serial, RIL_Errno e,
                          void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getClipResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getClipResponse(responseInfo,
                (ClipStatus) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getClipResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setClipResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setClipResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->setClipResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setClipResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setClipResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setClipResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }

    return 0;
}

int radio::getColpResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getColpResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            int n = -1, m = -1;
            int numInts = responseLen / sizeof(int);
            if (response == NULL || numInts != 2) {
                mtkLogE(LOG_TAG, "getColpResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                int *pInt = (int *) response;
                n = pInt[0];
                m = pInt[1];
            }
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->
                    getColpResponse(responseInfo, n, m);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getColpResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            int n = -1, m = -1;
            int numInts = responseLen / sizeof(int);
            if (response == NULL || numInts != 2) {
                mtkLogE(LOG_TAG, "getColpResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                int *pInt = (int *) response;
                n = pInt[0];
                m = pInt[1];
            }
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                    getColpResponse(responseInfo, n, m);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getColpResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }
    return 0;
}

int radio::getColrResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getColrResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            int n = responseInt(responseInfo, serial, responseType, e, response, responseLen);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->
                    getColrResponse(responseInfo, n);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getColrResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            int n = responseInt(responseInfo, serial, responseType, e, response, responseLen);
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                    getColrResponse(responseInfo, n);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getColrResponse: radioService[%d]->mRadioResponseMtk == NULL",
                    slotId);
        }
    }

    return 0;
}

int radio::sendCnapResponse(int slotId, int responseType, int serial, RIL_Errno e,
                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendCnapResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int n = -1, m = -1;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || numInts != 2) {
            mtkLogE(LOG_TAG, "sendCnapResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            n = pInt[0];
            m = pInt[1];
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                sendCnapResponse(responseInfo, n, m);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendCnapResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setColpResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setColpResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->setColpResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setColpResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setColpResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setColpResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }

    return 0;
}

int radio::setColrResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setColrResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->setColrResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setColrResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setColrResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setColrResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }

    return 0;
}

int radio::queryCallForwardInTimeSlotStatusResponse(int slotId, int responseType, int serial,
                                                    RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryCallForwardInTimeSlotStatusResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            hidl_vec<CallForwardInfoEx> callForwardInfoExs;

            if ((response == NULL && responseLen != 0)
                    || responseLen % sizeof(RIL_CallForwardInfoEx *) != 0) {
                mtkLogE(LOG_TAG, "queryCallForwardInTimeSlotStatusResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                int num = responseLen / sizeof(RIL_CallForwardInfoEx *);
                callForwardInfoExs.resize(num);
                for (int i = 0 ; i < num; i++) {
                    RIL_CallForwardInfoEx *resp = ((RIL_CallForwardInfoEx **) response)[i];
                    callForwardInfoExs[i].status = (CallForwardInfoStatus) resp->status;
                    callForwardInfoExs[i].reason = resp->reason;
                    callForwardInfoExs[i].serviceClass = resp->serviceClass;
                    callForwardInfoExs[i].toa = resp->toa;
                    callForwardInfoExs[i].number = convertCharPtrToHidlString(resp->number);
                    callForwardInfoExs[i].timeSeconds = resp->timeSeconds;
                    callForwardInfoExs[i].timeSlotBegin = convertCharPtrToHidlString(resp->timeSlotBegin);
                    callForwardInfoExs[i].timeSlotEnd = convertCharPtrToHidlString(resp->timeSlotEnd);
                }
            }

            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->
                    queryCallForwardInTimeSlotStatusResponse(responseInfo, callForwardInfoExs);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "queryCallForwardInTimeSlotStatusResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            hidl_vec<CallForwardInfoEx> callForwardInfoExs;

            if ((response == NULL && responseLen != 0)
                    || responseLen % sizeof(RIL_CallForwardInfoEx *) != 0) {
                mtkLogE(LOG_TAG, "queryCallForwardInTimeSlotStatusResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                int num = responseLen / sizeof(RIL_CallForwardInfoEx *);
                callForwardInfoExs.resize(num);
                for (int i = 0 ; i < num; i++) {
                    RIL_CallForwardInfoEx *resp = ((RIL_CallForwardInfoEx **) response)[i];
                    callForwardInfoExs[i].status = (CallForwardInfoStatus) resp->status;
                    callForwardInfoExs[i].reason = resp->reason;
                    callForwardInfoExs[i].serviceClass = resp->serviceClass;
                    callForwardInfoExs[i].toa = resp->toa;
                    callForwardInfoExs[i].number = convertCharPtrToHidlString(resp->number);
                    callForwardInfoExs[i].timeSeconds = resp->timeSeconds;
                    callForwardInfoExs[i].timeSlotBegin = convertCharPtrToHidlString(resp->timeSlotBegin);
                    callForwardInfoExs[i].timeSlotEnd = convertCharPtrToHidlString(resp->timeSlotEnd);
                }
            }

            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                    queryCallForwardInTimeSlotStatusResponse(responseInfo, callForwardInfoExs);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "queryCallForwardInTimeSlotStatusResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }

    return 0;
}

int radio::setCallForwardInTimeSlotResponse(int slotId, int responseType, int serial, RIL_Errno e,
                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCallForwardInTimeSlotResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->
                    setCallForwardInTimeSlotResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setCallForwardInTimeSlotResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                    setCallForwardInTimeSlotResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setCallForwardInTimeSlotResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
        }
    }

    return 0;
}

int radio::runGbaAuthenticationResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "runGbaAuthenticationResponse: serial %d", serial);

    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            hidl_vec<hidl_string> resList;
            int numStrings = responseLen / sizeof(char*);
            if (response == NULL || responseLen % sizeof(char *) != 0) {
                mtkLogE(LOG_TAG, "runGbaAuthenticationResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                char **pString = (char **) response;
                resList.resize(numStrings);
                for (int i = 0; i < numStrings; i++) {
                    resList[i] = convertCharPtrToHidlString(pString[i]);
                }
            }
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseIms->runGbaAuthenticationResponse(responseInfo,
                    resList);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "runGbaAuthenticationResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
        }
    } else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            hidl_vec<hidl_string> resList;
            int numStrings = responseLen / sizeof(char*);
            if (response == NULL || responseLen % sizeof(char *) != 0) {
                mtkLogE(LOG_TAG, "runGbaAuthenticationResponse Invalid response: NULL");
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            } else {
                char **pString = (char **) response;
                resList.resize(numStrings);
                for (int i = 0; i < numStrings; i++) {
                    resList[i] = convertCharPtrToHidlString(pString[i]);
                }
            }
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseMtk->runGbaAuthenticationResponse(responseInfo,
                    resList);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "runGbaAuthenticationResponse: radioService[%d]->mRadioResponseMtk == NULL",
                    slotId);
        }
    }

    return 0;
}

int radio::getDataCallListResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getDataCallListResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<SetupDataCallResult> ret;
        // M: [OD over ePDG]
        // remark AOSP
        //if (response == NULL || responseLen % sizeof(RIL_Data_Call_Response_v11) != 0) {
        if (response == NULL || responseLen % sizeof(MTK_RIL_Data_Call_Response_v11) != 0) {
            mtkLogE(LOG_TAG, "getDataCallListResponse: invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            // M: [OD over ePDG]
            // remark AOSP
            //convertRilDataCallListToHal(response, responseLen, ret);
            convertRilDataCallListToHalEx(response, responseLen, ret, slotId);
        }

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus = radioService[slotId]->mRadioResponse->getDataCallListResponse(
                responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getDataCallListResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setSuppServiceNotificationsResponse(int slotId,
                                              int responseType, int serial, RIL_Errno e,
                                              void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSuppServiceNotificationsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setSuppServiceNotificationsResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSuppServiceNotificationsResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::deleteSmsOnSimResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "deleteSmsOnSimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->deleteSmsOnSimResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "deleteSmsOnSimResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setBandModeResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setBandModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setBandModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setBandModeResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::writeSmsToSimResponse(int slotId,
                                int responseType, int serial, RIL_Errno e,
                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "writeSmsToSimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->writeSmsToSimResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "writeSmsToSimResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getAvailableBandModesResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e, void *response,
                                        size_t responseLen) {
    mtkLogD(LOG_TAG, "getAvailableBandModesResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<RadioBandMode> modes;
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "getAvailableBandModesResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            int numInts = responseLen / sizeof(int);
            modes.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                modes[i] = (RadioBandMode) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getAvailableBandModesResponse(responseInfo,
                modes);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getAvailableBandModesResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendEnvelopeResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendEnvelopeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendEnvelopeResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendEnvelopeResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::sendTerminalResponseToSimResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendTerminalResponseToSimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendTerminalResponseToSimResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendTerminalResponseToSimResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::handleStkCallSetupRequestFromSimResponse(int slotId,
                                                   int responseType, int serial,
                                                   RIL_Errno e, void *response,
                                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "handleStkCallSetupRequestFromSimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->handleStkCallSetupRequestFromSimResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "handleStkCallSetupRequestFromSimResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::explicitCallTransferResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "explicitCallTransferResponse: serial %d", serial);

    if(isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponseIms
                                          ->explicitCallTransferResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else if (radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->explicitCallTransferResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "explicitCallTransferResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                      slotId);
        }
    }
    else {
        if (radioService[slotId]->mRadioResponseMtk != NULL
                || radioService[slotId]->mRadioResponse != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus
                    (radioService[slotId]->mRadioResponseMtk != NULL ?
                            radioService[slotId]->mRadioResponseMtk->explicitCallTransferResponse(
                                    responseInfo) :
                            radioService[slotId]->mRadioResponse->explicitCallTransferResponse(
                                    responseInfo));
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "explicitCallTransferResponse: radioService[%d] or mRadioResponse == NULL",
                                                                                      slotId);
        }
    }

    return 0;
}

int radio::setPreferredNetworkTypeResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPreferredNetworkTypeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setPreferredNetworkTypeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPreferredNetworkTypeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}


int radio::getPreferredNetworkTypeResponse(int slotId,
                                          int responseType, int serial, RIL_Errno e,
                                          void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getPreferredNetworkTypeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getPreferredNetworkTypeResponse(
                responseInfo, (PreferredNetworkType) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPreferredNetworkTypeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getNeighboringCidsResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getNeighboringCidsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<NeighboringCell> cells;

        if (response == NULL || responseLen % sizeof(RIL_NeighboringCell *) != 0) {
            mtkLogE(LOG_TAG, "getNeighboringCidsResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int num = responseLen / sizeof(RIL_NeighboringCell *);
            cells.resize(num);
            for (int i = 0 ; i < num; i++) {
                RIL_NeighboringCell *resp = ((RIL_NeighboringCell **) response)[i];
                cells[i].cid = convertCharPtrToHidlString(resp->cid);
                cells[i].rssi = resp->rssi;
            }
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getNeighboringCidsResponse(responseInfo,
                cells);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getNeighboringCidsResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setLocationUpdatesResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setLocationUpdatesResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setLocationUpdatesResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setLocationUpdatesResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setCdmaSubscriptionSourceResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCdmaSubscriptionSourceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setCdmaSubscriptionSourceResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCdmaSubscriptionSourceResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setCdmaRoamingPreferenceResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCdmaRoamingPreferenceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setCdmaRoamingPreferenceResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCdmaRoamingPreferenceResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getCdmaRoamingPreferenceResponse(int slotId,
                                           int responseType, int serial, RIL_Errno e,
                                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCdmaRoamingPreferenceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getCdmaRoamingPreferenceResponse(
                responseInfo, (CdmaRoamingType) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCdmaRoamingPreferenceResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setTTYModeResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setTTYModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setTTYModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setTTYModeResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getTTYModeResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getTTYModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getTTYModeResponse(responseInfo,
                (TtyMode) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getTTYModeResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setPreferredVoicePrivacyResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPreferredVoicePrivacyResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setPreferredVoicePrivacyResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPreferredVoicePrivacyResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getPreferredVoicePrivacyResponse(int slotId,
                                           int responseType, int serial, RIL_Errno e,
                                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getPreferredVoicePrivacyResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        bool enable = false;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || numInts != 1) {
            mtkLogE(LOG_TAG, "getPreferredVoicePrivacyResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            enable = pInt[0] == 1 ? true : false;
        }
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getPreferredVoicePrivacyResponse(
                responseInfo, enable);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPreferredVoicePrivacyResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendCDMAFeatureCodeResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendCDMAFeatureCodeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendCDMAFeatureCodeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendCDMAFeatureCodeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendBurstDtmfResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendBurstDtmfResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendBurstDtmfResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendBurstDtmfResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::sendCdmaSmsResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "sendCdmaSmsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        SendSmsResult result = makeSendSmsResult(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendCdmaSmsResponse(responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendCdmaSmsResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::acknowledgeLastIncomingCdmaSmsResponse(int slotId,
                                                 int responseType, int serial, RIL_Errno e,
                                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingCdmaSmsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->acknowledgeLastIncomingCdmaSmsResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acknowledgeLastIncomingCdmaSmsResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::acknowledgeLastIncomingCdmaSmsExResponse(int slotId,
                                                 int responseType, int serial, RIL_Errno e,
                                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingCdmaSmsExResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseIms->acknowledgeLastIncomingCdmaSmsExResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "acknowledgeLastIncomingCdmaSmsExResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}


int radio::getGsmBroadcastConfigResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getGsmBroadcastConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<GsmBroadcastSmsConfigInfo> configs;

        if (response == NULL || responseLen % sizeof(RIL_GSM_BroadcastSmsConfigInfo *) != 0) {
            mtkLogE(LOG_TAG, "getGsmBroadcastConfigResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int num = responseLen / sizeof(RIL_GSM_BroadcastSmsConfigInfo *);
            configs.resize(num);
            for (int i = 0 ; i < num; i++) {
                RIL_GSM_BroadcastSmsConfigInfo *resp =
                        ((RIL_GSM_BroadcastSmsConfigInfo **) response)[i];
                configs[i].fromServiceId = resp->fromServiceId;
                configs[i].toServiceId = resp->toServiceId;
                configs[i].fromCodeScheme = resp->fromCodeScheme;
                configs[i].toCodeScheme = resp->toCodeScheme;
                configs[i].selected = resp->selected == 1 ? true : false;
            }
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getGsmBroadcastConfigResponse(responseInfo,
                configs);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getGsmBroadcastConfigResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setGsmBroadcastConfigResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setGsmBroadcastConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setGsmBroadcastConfigResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setGsmBroadcastConfigResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setGsmBroadcastActivationResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setGsmBroadcastActivationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setGsmBroadcastActivationResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setGsmBroadcastActivationResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getCdmaBroadcastConfigResponse(int slotId,
                                         int responseType, int serial, RIL_Errno e,
                                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCdmaBroadcastConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<CdmaBroadcastSmsConfigInfo> configs;

        if (response == NULL || responseLen % sizeof(RIL_CDMA_BroadcastSmsConfigInfo *) != 0) {
            mtkLogE(LOG_TAG, "getCdmaBroadcastConfigResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int num = responseLen / sizeof(RIL_CDMA_BroadcastSmsConfigInfo *);
            configs.resize(num);
            for (int i = 0 ; i < num; i++) {
                RIL_CDMA_BroadcastSmsConfigInfo *resp =
                        ((RIL_CDMA_BroadcastSmsConfigInfo **) response)[i];
                configs[i].serviceCategory = resp->service_category;
                configs[i].language = resp->language;
                configs[i].selected = resp->selected == 1 ? true : false;
            }
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getCdmaBroadcastConfigResponse(responseInfo,
                configs);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCdmaBroadcastConfigResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setCdmaBroadcastConfigResponse(int slotId,
                                         int responseType, int serial, RIL_Errno e,
                                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCdmaBroadcastConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setCdmaBroadcastConfigResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCdmaBroadcastConfigResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setCdmaBroadcastActivationResponse(int slotId,
                                             int responseType, int serial, RIL_Errno e,
                                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCdmaBroadcastActivationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setCdmaBroadcastActivationResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCdmaBroadcastActivationResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getCDMASubscriptionResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e, void *response,
                                      size_t responseLen) {
    mtkLogD(LOG_TAG, "getCDMASubscriptionResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        int numStrings = responseLen / sizeof(char *);
        hidl_string emptyString;
        if (response == NULL || numStrings != 5) {
            mtkLogE(LOG_TAG, "getOperatorResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->getCDMASubscriptionResponse(
                    responseInfo, emptyString, emptyString, emptyString, emptyString, emptyString);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            char **resp = (char **) response;
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->getCDMASubscriptionResponse(
                    responseInfo,
                    convertCharPtrToHidlString(resp[0]),
                    convertCharPtrToHidlString(resp[1]),
                    convertCharPtrToHidlString(resp[2]),
                    convertCharPtrToHidlString(resp[3]),
                    convertCharPtrToHidlString(resp[4]));
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    } else {
        mtkLogE(LOG_TAG, "getCDMASubscriptionResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::writeSmsToRuimResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "writeSmsToRuimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->writeSmsToRuimResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "writeSmsToRuimResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::deleteSmsOnRuimResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "deleteSmsOnRuimResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->deleteSmsOnRuimResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "deleteSmsOnRuimResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getDeviceIdentityResponse(int slotId,
                                    int responseType, int serial, RIL_Errno e, void *response,
                                    size_t responseLen) {
    mtkLogD(LOG_TAG, "getDeviceIdentityResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        int numStrings = responseLen / sizeof(char *);
        hidl_string emptyString;
        if (response == NULL || numStrings != 4) {
            mtkLogE(LOG_TAG, "getDeviceIdentityResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->getDeviceIdentityResponse(responseInfo,
                    emptyString, emptyString, emptyString, emptyString);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            char **resp = (char **) response;
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->getDeviceIdentityResponse(responseInfo,
                    convertCharPtrToHidlString(resp[0]),
                    convertCharPtrToHidlString(resp[1]),
                    convertCharPtrToHidlString(resp[2]),
                    convertCharPtrToHidlString(resp[3]));
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    } else {
        mtkLogE(LOG_TAG, "getDeviceIdentityResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::exitEmergencyCallbackModeResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "exitEmergencyCallbackModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->exitEmergencyCallbackModeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "exitEmergencyCallbackModeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getSmscAddressResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getSmscAddressResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getSmscAddressResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getSmscAddressResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

/// M: CC: @}

int radio::setSmscAddressResponse(int slotId,
                                             int responseType, int serial, RIL_Errno e,
                                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSmscAddressResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setSmscAddressResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSmscAddressResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::reportSmsMemoryStatusResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reportSmsMemoryStatusResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->reportSmsMemoryStatusResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "reportSmsMemoryStatusResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::reportStkServiceIsRunningResponse(int slotId,
                                             int responseType, int serial, RIL_Errno e,
                                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reportStkServiceIsRunningResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->
                reportStkServiceIsRunningResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "reportStkServiceIsRunningResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getCdmaSubscriptionSourceResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCdmaSubscriptionSourceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getCdmaSubscriptionSourceResponse(
                responseInfo, (CdmaSubscriptionSource) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getCdmaSubscriptionSourceResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::requestIsimAuthenticationResponse(int slotId,
                                            int responseType, int serial, RIL_Errno e,
                                            void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "requestIsimAuthenticationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->requestIsimAuthenticationResponse(
                responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "requestIsimAuthenticationResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::acknowledgeIncomingGsmSmsWithPduResponse(int slotId,
                                                   int responseType,
                                                   int serial, RIL_Errno e, void *response,
                                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "acknowledgeIncomingGsmSmsWithPduResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->acknowledgeIncomingGsmSmsWithPduResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acknowledgeIncomingGsmSmsWithPduResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::sendEnvelopeWithStatusResponse(int slotId,
                                         int responseType, int serial, RIL_Errno e, void *response,
                                         size_t responseLen) {
    mtkLogD(LOG_TAG, "sendEnvelopeWithStatusResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        IccIoResult result = responseIccIo(responseInfo, serial, responseType, e,
                response, responseLen);

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendEnvelopeWithStatusResponse(responseInfo,
                result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendEnvelopeWithStatusResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getVoiceRadioTechnologyResponse(int slotId,
                                          int responseType, int serial, RIL_Errno e,
                                          void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getVoiceRadioTechnologyResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getVoiceRadioTechnologyResponse(
                responseInfo, (RadioTechnology) ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getVoiceRadioTechnologyResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

// It's also getCellInfoListResponse_1_2
int radio::getCellInfoListResponse(int slotId,
                                   int responseType,
                                   int serial, RIL_Errno e, void *response,
                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "getCellInfoListResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseV1_2!= NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        // use AOSP V1.2
        char* used_memory = NULL;
        hidl_vec<AOSP_V1_2::CellInfo> ret;
        if ((response == NULL && responseLen != 0)
                || responseLen % sizeof(RIL_CellInfo_v12) != 0) {
            mtkLogE(LOG_TAG, "getCellInfoListResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            used_memory = convertRilCellInfoListToHal_1_2(response, responseLen, ret);
        }

        Return<void> retStatus =
            radioService[slotId]->mRadioResponseV1_2->getCellInfoListResponse_1_2(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        char* used_memory = NULL;
        hidl_vec<CellInfo> ret;
        if ((response == NULL && responseLen != 0)
                || responseLen % sizeof(RIL_CellInfo_v12) != 0) {
            mtkLogE(LOG_TAG, "getCellInfoListResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            used_memory = convertRilCellInfoListToHal(response, responseLen, ret);
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponse->getCellInfoListResponse(
                responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else {
        mtkLogE(LOG_TAG, "getCellInfoListResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setCellInfoListRateResponse(int slotId,
                                       int responseType,
                                       int serial, RIL_Errno e, void *response,
                                       size_t responseLen) {
    mtkLogD(LOG_TAG, "setCellInfoListRateResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setCellInfoListRateResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCellInfoListRateResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setInitialAttachApnResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setInitialAttachApnResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setInitialAttachApnResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setInitialAttachApnResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getImsRegistrationStateResponse(int slotId,
                                           int responseType, int serial, RIL_Errno e,
                                           void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getImsRegistrationStateResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        bool isRegistered = false;
        int ratFamily = 0;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || numInts != 2) {
            mtkLogE(LOG_TAG, "getImsRegistrationStateResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            isRegistered = pInt[0] == 1 ? true : false;
            ratFamily = pInt[1];
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getImsRegistrationStateResponse(
                responseInfo, isRegistered, (RadioTechnologyFamily) ratFamily);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getImsRegistrationStateResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendImsSmsResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "sendImsSmsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        SendSmsResult result = makeSendSmsResult(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendImsSmsResponse(responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendSmsResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::iccTransmitApduBasicChannelResponse(int slotId,
                                               int responseType, int serial, RIL_Errno e,
                                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "[%d] iccTransmitApduBasicChannelResponse: serial %d", slotId, serial);

    if (radioService[slotId]->mRadioResponse != NULL || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        IccIoResult result = responseIccIo(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus;
        if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) { /// MTK: ForSE
            retStatus = radioService[slotId]->mRadioResponseSE->iccTransmitApduBasicChannelResponse(
                responseInfo, result);
        } else {
            retStatus = radioService[slotId]->mRadioResponse->iccTransmitApduBasicChannelResponse(
                responseInfo, result);
        }
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "iccTransmitApduBasicChannelResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::iccOpenLogicalChannelResponse(int slotId,
                                         int responseType, int serial, RIL_Errno e, void *response,
                                         size_t responseLen) {
    mtkLogD(LOG_TAG, "[%d] iccOpenLogicalChannelResponse: serial %d", slotId, serial);

    if (radioService[slotId]->mRadioResponse != NULL || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int channelId = -1;
        hidl_vec<int8_t> selectResponse;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "iccOpenLogicalChannelResponse Invalid response: NULL");
            if (response != NULL) {
                if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
            }
        } else {
            int *pInt = (int *) response;
            channelId = pInt[0];
            selectResponse.resize(numInts - 1);
            for (int i = 1; i < numInts; i++) {
                selectResponse[i - 1] = (int8_t) pInt[i];
            }
        }

        Return<void> retStatus;
        if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) { /// MTK: ForSE
            retStatus = radioService[slotId]->mRadioResponseSE->iccOpenLogicalChannelResponse(responseInfo,
                channelId, selectResponse);
        } else {
            retStatus = radioService[slotId]->mRadioResponse->iccOpenLogicalChannelResponse(responseInfo,
                channelId, selectResponse);
        }
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "iccOpenLogicalChannelResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::iccCloseLogicalChannelResponse(int slotId,
                                          int responseType, int serial, RIL_Errno e,
                                          void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "[%d] iccCloseLogicalChannelResponse: serial %d", slotId, serial);

    if (radioService[slotId]->mRadioResponse != NULL || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus;
        if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) { /// MTK: ForSE
            retStatus = radioService[slotId]->mRadioResponseSE->iccCloseLogicalChannelResponse(
                responseInfo);
        } else {
            retStatus = radioService[slotId]->mRadioResponse->iccCloseLogicalChannelResponse(
                responseInfo);
        }
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "iccCloseLogicalChannelResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::iccTransmitApduLogicalChannelResponse(int slotId,
                                                 int responseType, int serial, RIL_Errno e,
                                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "[%d] iccTransmitApduLogicalChannelResponse: serial %d", slotId, serial);

    if (radioService[slotId]->mRadioResponse != NULL || radioService[slotId]->mRadioResponseSE != NULL) {
        RadioResponseInfo responseInfo = {};
        IccIoResult result = responseIccIo(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus;
        if (isSESlot(slotId) && radioService[slotId]->mRadioResponseSE != NULL) { /// MTK: ForSE
            retStatus = radioService[slotId]->mRadioResponseSE->iccTransmitApduLogicalChannelResponse(
                responseInfo, result);
        } else {
            retStatus = radioService[slotId]->mRadioResponse->iccTransmitApduLogicalChannelResponse(
                responseInfo, result);
        }
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "iccTransmitApduLogicalChannelResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::nvReadItemResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "nvReadItemResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->nvReadItemResponse(
                responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvReadItemResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::nvWriteItemResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "nvWriteItemResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->nvWriteItemResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvWriteItemResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::nvWriteCdmaPrlResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "nvWriteCdmaPrlResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->nvWriteCdmaPrlResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvWriteCdmaPrlResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::nvResetConfigResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "nvResetConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->nvResetConfigResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nvResetConfigResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setUiccSubscriptionResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setUiccSubscriptionResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setUiccSubscriptionResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setUiccSubscriptionResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setDataAllowedResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setDataAllowedResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setDataAllowedResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setDataAllowedResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getHardwareConfigResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getHardwareConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<HardwareConfig> result;
        if ((response == NULL && responseLen != 0)
                || responseLen % sizeof(RIL_HardwareConfig) != 0) {
            mtkLogE(LOG_TAG, "hardwareConfigChangedInd: invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilHardwareConfigListToHal(response, responseLen, result);
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponse->getHardwareConfigResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getHardwareConfigResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::requestIccSimAuthenticationResponse(int slotId,
                                               int responseType, int serial, RIL_Errno e,
                                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "requestIccSimAuthenticationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        IccIoResult result = responseIccIo(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->requestIccSimAuthenticationResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "requestIccSimAuthenticationResponse: radioService[%d]->mRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::setDataProfileResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setDataProfileResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setDataProfileResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setDataProfileResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::requestShutdownResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "requestShutdownResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->requestShutdownResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "requestShutdownResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

void responseRadioCapability(RadioResponseInfo& responseInfo, int serial,
        int responseType, RIL_Errno e, void *response, size_t responseLen, RadioCapability& rc) {
    populateResponseInfo(responseInfo, serial, responseType, e);

    if (response == NULL || responseLen != sizeof(RIL_RadioCapability)) {
        mtkLogE(LOG_TAG, "responseRadioCapability: Invalid response");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        rc.logicalModemUuid = hidl_string();
    } else {
        convertRilRadioCapabilityToHal(response, responseLen, rc);
    }
}

int radio::getRadioCapabilityResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getRadioCapabilityResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        RadioCapability result = {};
        responseRadioCapability(responseInfo, serial, responseType, e, response, responseLen,
                result);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->getRadioCapabilityResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getRadioCapabilityResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setRadioCapabilityResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setRadioCapabilityResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        RadioCapability result = {};
        responseRadioCapability(responseInfo, serial, responseType, e, response, responseLen,
                result);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->setRadioCapabilityResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRadioCapabilityResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

LceStatusInfo responseLceStatusInfo(RadioResponseInfo& responseInfo, int serial, int responseType,
                                    RIL_Errno e, void *response, size_t responseLen) {
    populateResponseInfo(responseInfo, serial, responseType, e);
    LceStatusInfo result = {};

    if (response == NULL || responseLen != sizeof(RIL_LceStatusInfo)) {
        mtkLogE(LOG_TAG, "Invalid response: NULL");
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
    } else {
        RIL_LceStatusInfo *resp = (RIL_LceStatusInfo *) response;
        result.lceStatus = (LceStatus) resp->lce_status;
        result.actualIntervalMs = (uint8_t) resp->actual_interval_ms;
    }

    return result;
}

int radio::startLceServiceResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "startLceServiceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        LceStatusInfo result = responseLceStatusInfo(responseInfo, serial, responseType, e,
                response, responseLen);

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::SIM_ABSENT;
        }
        // M @}

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->startLceServiceResponse(responseInfo,
                result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "startLceServiceResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::stopLceServiceResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "stopLceServiceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        LceStatusInfo result = responseLceStatusInfo(responseInfo, serial, responseType, e,
                response, responseLen);

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->stopLceServiceResponse(responseInfo,
                result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stopLceServiceResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::pullLceDataResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "pullLceDataResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        LceDataInfo result = {};
        if (response == NULL || responseLen != sizeof(RIL_LceDataInfo)) {
            mtkLogE(LOG_TAG, "pullLceDataResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilLceDataInfoToHal(response, responseLen, result);
        }

        // M @{
        if (s_cardState[slotId] == CardState::ABSENT) {
            responseInfo.error = RadioError::NONE;
        }
        // M @}

        Return<void> retStatus = radioService[slotId]->mRadioResponse->pullLceDataResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "pullLceDataResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::getModemActivityInfoResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getModemActivityInfoResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        ActivityStatsInfo info;
        if (response == NULL || responseLen != sizeof(RIL_ActivityStatsInfo)) {
            mtkLogE(LOG_TAG, "getModemActivityInfoResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_ActivityStatsInfo *resp = (RIL_ActivityStatsInfo *)response;
            info.sleepModeTimeMs = resp->sleep_mode_time_ms;
            info.idleModeTimeMs = resp->idle_mode_time_ms;
            for(int i = 0; i < RIL_NUM_TX_POWER_LEVELS; i++) {
                info.txmModetimeMs[i] = resp->tx_mode_time_ms[i];
            }
            info.rxModeTimeMs = resp->rx_mode_time_ms;
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getModemActivityInfoResponse(responseInfo,
                info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getModemActivityInfoResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setAllowedCarriersResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setAllowedCarriersResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setAllowedCarriersResponse(responseInfo,
                ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setAllowedCarriersResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getAllowedCarriersResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getAllowedCarriersResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        CarrierRestrictions carrierInfo = {};
        bool allAllowed = true;
        if (response == NULL) {
#if VDBG
            mtkLogD(LOG_TAG, "getAllowedCarriersResponse response is NULL: all allowed");
#endif
            carrierInfo.allowedCarriers.resize(0);
            carrierInfo.excludedCarriers.resize(0);
        } else if (responseLen != sizeof(RIL_CarrierRestrictions)) {
            mtkLogE(LOG_TAG, "getAllowedCarriersResponse Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_CarrierRestrictions *pCr = (RIL_CarrierRestrictions *)response;
            if (pCr->len_allowed_carriers > 0 || pCr->len_excluded_carriers > 0) {
                allAllowed = false;
            }

            carrierInfo.allowedCarriers.resize(pCr->len_allowed_carriers);
            for(int i = 0; i < pCr->len_allowed_carriers; i++) {
                RIL_Carrier *carrier = pCr->allowed_carriers + i;
                carrierInfo.allowedCarriers[i].mcc = convertCharPtrToHidlString(carrier->mcc);
                carrierInfo.allowedCarriers[i].mnc = convertCharPtrToHidlString(carrier->mnc);
                carrierInfo.allowedCarriers[i].matchType = (CarrierMatchType) carrier->match_type;
                carrierInfo.allowedCarriers[i].matchData =
                        convertCharPtrToHidlString(carrier->match_data);
            }

            carrierInfo.excludedCarriers.resize(pCr->len_excluded_carriers);
            for(int i = 0; i < pCr->len_excluded_carriers; i++) {
                RIL_Carrier *carrier = pCr->excluded_carriers + i;
                carrierInfo.excludedCarriers[i].mcc = convertCharPtrToHidlString(carrier->mcc);
                carrierInfo.excludedCarriers[i].mnc = convertCharPtrToHidlString(carrier->mnc);
                carrierInfo.excludedCarriers[i].matchType = (CarrierMatchType) carrier->match_type;
                carrierInfo.excludedCarriers[i].matchData =
                        convertCharPtrToHidlString(carrier->match_data);
            }
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->getAllowedCarriersResponse(responseInfo,
                allAllowed, carrierInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getAllowedCarriersResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::sendDeviceStateResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "sendDeviceStateResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->sendDeviceStateResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendDeviceStateResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::setCarrierInfoForImsiEncryptionResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setCarrierInfoForImsiEncryptionResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseV1_1 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseV1_1->
                setCarrierInfoForImsiEncryptionResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setCarrierInfoForImsiEncryptionResponse: radioService[%d]->mRadioResponseV1_1 == "
                "NULL", slotId);
    }
    return 0;
}

int radio::setIndicationFilterResponse(int slotId,
                              int responseType, int serial, RIL_Errno e,
                              void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setIndicationFilterResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponse->setIndicationFilterResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setIndicationFilterResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::setSimCardPowerResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
#if VDBG
    mtkLogD(LOG_TAG, "setSimCardPowerResponse: serial %d", serial);
#endif

    if (radioService[slotId]->mRadioResponse != NULL
            || radioService[slotId]->mRadioResponseV1_1 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        if (radioService[slotId]->mRadioResponseV1_1 != NULL) {
            Return<void> retStatus = radioService[slotId]->mRadioResponseV1_1->
                    setSimCardPowerResponse_1_1(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogD(LOG_TAG, "setSimCardPowerResponse: radioService[%d]->mRadioResponseV1_1 == NULL",
                    slotId);
            Return<void> retStatus
                    = radioService[slotId]->mRadioResponse->setSimCardPowerResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        }
    } else {
        mtkLogE(LOG_TAG, "setSimCardPowerResponse: radioService[%d]->mRadioResponse == NULL && "
                "radioService[%d]->mRadioResponseV1_1 == NULL", slotId, slotId);
    }
    return 0;
}

int radio::startNetworkScanResponse(int slotId, int responseType, int serial, RIL_Errno e,
                                    void *response, size_t responseLen) {
#if VDBG
    mtkLogD(LOG_TAG, "startNetworkScanResponse: serial %d", serial);
#endif
    if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseV1_2->startNetworkScanResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponseV1_1 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseV1_1->startNetworkScanResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "startNetworkScanResponse: radioService[%d]->mRadioResponseV1_1 == NULL", slotId);
    }

    return 0;
}

int radio::stopNetworkScanResponse(int slotId, int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
#if VDBG
    mtkLogD(LOG_TAG, "stopNetworkScanResponse: serial %d", serial);
#endif

    if (radioService[slotId]->mRadioResponseV1_2 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseV1_2->stopNetworkScanResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponseV1_1 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseV1_1->stopNetworkScanResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stopNetworkScanResponse: radioService[%d]->mRadioResponseV1_1 == NULL", slotId);
    }

    return 0;
}

void convertRilKeepaliveStatusToHal(const RIL_KeepaliveStatus *rilStatus,
        AOSP_V1_1::KeepaliveStatus& halStatus) {
    halStatus.sessionHandle = rilStatus->sessionHandle;
    halStatus.code = static_cast<AOSP_V1_1::KeepaliveStatusCode>(rilStatus->code);
}

int radio::startKeepaliveResponse(int slotId, int responseType, int serial, RIL_Errno e,
                                    void *response, size_t responseLen) {
#if VDBG
    mtkLogD(LOG_TAG, "%s(): %d", __FUNCTION__, serial);
#endif
    RadioResponseInfo responseInfo = {};
    populateResponseInfo(responseInfo, serial, responseType, e);

    // If we don't have a radio service, there's nothing we can do
    if (radioService[slotId]->mRadioResponseV1_1 == NULL) {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseV1_1 == NULL", __FUNCTION__, slotId);
        return 0;
    }

    AOSP_V1_1::KeepaliveStatus ks = {};
    if (response == NULL || responseLen != sizeof(AOSP_V1_1::KeepaliveStatus)) {
        mtkLogE(LOG_TAG, "%s: invalid response - %d", __FUNCTION__, static_cast<int>(e));
        if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
    } else {
        convertRilKeepaliveStatusToHal(static_cast<RIL_KeepaliveStatus*>(response), ks);
    }

    Return<void> retStatus =
            radioService[slotId]->mRadioResponseV1_1->startKeepaliveResponse(responseInfo, ks);
    radioService[slotId]->checkReturnStatus(retStatus);
    return 0;
}

int radio::stopKeepaliveResponse(int slotId, int responseType, int serial, RIL_Errno e,
                                    void *response, size_t responseLen) {
#if VDBG
    mtkLogD(LOG_TAG, "%s(): %d", __FUNCTION__, serial);
#endif
    RadioResponseInfo responseInfo = {};
    populateResponseInfo(responseInfo, serial, responseType, e);

    // If we don't have a radio service, there's nothing we can do
    if (radioService[slotId]->mRadioResponseV1_1 == NULL) {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseV1_1 == NULL", __FUNCTION__, slotId);
        return 0;
    }

    Return<void> retStatus =
            radioService[slotId]->mRadioResponseV1_1->stopKeepaliveResponse(responseInfo);
    radioService[slotId]->checkReturnStatus(retStatus);
    return 0;
}

int radio::sendRequestRawResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
   mtkLogD(LOG_TAG, "sendRequestRawResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<uint8_t> data;

        if (response == NULL) {
            mtkLogE(LOG_TAG, "sendRequestRawResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            data.setToExternal((uint8_t *) response, responseLen);
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                sendRequestRawResponse(responseInfo, data);
        checkReturnStatus(slotId, retStatus, false);
    } else {
        mtkLogE(LOG_TAG, "sendRequestRawResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::sendRequestStringsResponse(int slotId,
                                      int responseType, int serial, RIL_Errno e,
                                      void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendRequestStringsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<hidl_string> data;

        if ((response == NULL && responseLen != 0) || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "sendRequestStringsResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            data.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                data[i] = convertCharPtrToHidlString(resp[i]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->sendRequestStringsResponse(
                responseInfo, data);
        checkReturnStatus(slotId, retStatus, false);
    } else {
        mtkLogE(LOG_TAG, "sendRequestStringsResponse: radioService[%d]->mRadioResponseMtk == "
                "NULL", slotId);
    }

    return 0;
}

// ATCI Start
int radio::sendAtciResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendAtciResponse: serial %d", serial);
    if (radioService[slotId] != NULL && radioService[slotId]->mAtciResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<uint8_t> data;

        if (response == NULL) {
            mtkLogE(LOG_TAG, "sendAtciResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            data.setToExternal((uint8_t *) response, responseLen);
        }
        Return<void> retStatus = radioService[slotId]->mAtciResponse->
                sendAtciResponse(responseInfo, data);
        if (!retStatus.isOk()) {
            mtkLogE(LOG_TAG, "sendAtciResponse: unable to call response callback");
            radioService[slotId]->mAtciResponse = NULL;
            radioService[slotId]->mAtciIndication = NULL;
        }
    } else {
        mtkLogE(LOG_TAG, "sendAtciResponse: radioService[%d]->mAtciResponse == NULL", slotId);
    }

    return 0;
}
// ATCI End

// SUBSIDYLOCK Start
int radio::sendSubsidyLockResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    return 0;
}
// SUBSIDYLOCK End

/// M: CC: call control @{
int radio::hangupAllResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "hangupAllResponse: serial %d", serial);
    if(isImsSlot(slotId)) {

        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseIms->hangupAllResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "hangupAllResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
        }
    }
    else {

        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseMtk->hangupAllResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "hangupAllResponse: radioService[%d]->mRadioResponseMtk == NULL",
                    slotId);
        }
    }

    return 0;
}

int radio::setCallIndicationResponse(int slotId,
                                     int responseType, int serial, RIL_Errno e,
                                     void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setCallIndicationResponse: serial %d", serial);
    if(isImsSlot(slotId)) {

        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseIms->
                                     setCallIndicationResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setCallIndicationResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
        }
    }
    else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseMtk->
                                     setCallIndicationResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setCallIndicationResponse: radioService[%d]->mRadioResponseMtk == NULL",
                    slotId);
        }
    }

    return 0;
}

int radio::emergencyDialResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "emergencyDialResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->emergencyDialResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "emergencyDialResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::setEccServiceCategoryResponse(int slotId,
                                         int responseType, int serial, RIL_Errno e,
                                         void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setEccServiceCategoryResponse: serial %d", serial);
    if(isImsSlot(slotId)) {

        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseIms->
                                     setEccServiceCategoryResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setEccServiceCategoryResponse: radioService[%d]->mRadioResponseIms == NULL",
                    slotId);
        }
    }
    else {
        if (radioService[slotId]->mRadioResponseMtk != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->
                                     mRadioResponseMtk->
                                     setEccServiceCategoryResponse(responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "setEccServiceCategoryResponse: radioService[%d]->mRadioResponseMtk == NULL",
                    slotId);
        }
    }

    return 0;
}
/// M: CC: @}

// PHB START
int radio::queryPhbStorageInfoResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryPhbStorageInfoResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> storageInfo;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "queryPhbStorageInfoResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            storageInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                storageInfo[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->queryPhbStorageInfoResponse(responseInfo,
                storageInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "queryPhbStorageInfoResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::writePhbEntryResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "writePhbEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->writePhbEntryResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "writePhbEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

void convertRilPhbEntryStructureToHal(void *response, size_t responseLen,
        hidl_vec<PhbEntryStructure>& resultList) {
    int num = responseLen / sizeof(RIL_PhbEntryStructure*);
    RIL_PhbEntryStructure **phbEntryResponse = (RIL_PhbEntryStructure **) response;
    resultList.resize(num);
    for (int i = 0; i < num; i++) {
        resultList[i].type = phbEntryResponse[i]->type;
        resultList[i].index = phbEntryResponse[i]->index;
        resultList[i].number = convertCharPtrToHidlString(phbEntryResponse[i]->number);
        resultList[i].ton = phbEntryResponse[i]->ton;
        resultList[i].alphaId = convertCharPtrToHidlString(phbEntryResponse[i]->alphaId);
    }
}

int radio::readPhbEntryResponse(int slotId,
                                int responseType, int serial, RIL_Errno e,
                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readPhbEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<PhbEntryStructure> result;
        if (response == NULL || responseLen % sizeof(RIL_PhbEntryStructure*) != 0) {
            mtkLogE(LOG_TAG, "readPhbEntryResponse: invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilPhbEntryStructureToHal(response, responseLen, result);
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->readPhbEntryResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readPhbEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::queryUPBCapabilityResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryUPBCapabilityResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> upbCapability;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "queryUPBCapabilityResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            upbCapability.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                upbCapability[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->queryUPBCapabilityResponse(responseInfo,
                upbCapability);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "queryUPBCapabilityResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::editUPBEntryResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "editUPBEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->editUPBEntryResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "editUPBEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::deleteUPBEntryResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "deleteUPBEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->deleteUPBEntryResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "deleteUPBEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::readUPBGasListResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBGasListResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<hidl_string> gasList;
        int numStrings = responseLen / sizeof(char*);
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "readUPBGasListResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **pString = (char **) response;
            gasList.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                gasList[i] = convertCharPtrToHidlString(pString[i]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBGasListResponse(responseInfo,
                gasList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBGasListResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::readUPBGrpEntryResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBGrpEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> grpEntries;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "readUPBGrpEntryResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            grpEntries.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                grpEntries[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBGrpEntryResponse(responseInfo,
                grpEntries);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBGrpEntryResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::writeUPBGrpEntryResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "writeUPBGrpEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->writeUPBGrpEntryResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "writeUPBGrpEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::getPhoneBookStringsLengthResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getPhoneBookStringsLengthResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> stringLengthInfo;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "getPhoneBookStringsLengthResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            stringLengthInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                stringLengthInfo[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBGrpEntryResponse(responseInfo,
                stringLengthInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPhoneBookStringsLengthResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::getPhoneBookMemStorageResponse(int slotId,
                                        int responseType, int serial, RIL_Errno e,
                                        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getPhoneBookMemStorageResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        PhbMemStorageResponse phbMemStorage;
        if (response == NULL || responseLen != sizeof(RIL_PHB_MEM_STORAGE_RESPONSE)) {
            mtkLogE(LOG_TAG, "getPhoneBookMemStorageResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_PHB_MEM_STORAGE_RESPONSE *resp = (RIL_PHB_MEM_STORAGE_RESPONSE *)response;
            phbMemStorage.storage = convertCharPtrToHidlString(resp->storage);
            phbMemStorage.used = resp->used;
            phbMemStorage.total = resp->total;
        }

        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getPhoneBookMemStorageResponse(responseInfo,
                phbMemStorage);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPhoneBookMemStorageResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::setPhoneBookMemStorageResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPhoneBookMemStorageResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setPhoneBookMemStorageResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPhoneBookMemStorageResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

void convertRilPhbEntryExtStrucutreToHal(void *response, size_t responseLen,
        hidl_vec<PhbEntryExt>& resultList) {
    int num = responseLen / sizeof(RIL_PHB_ENTRY *);

    RIL_PHB_ENTRY **phbEntryExtResponse = (RIL_PHB_ENTRY **) response;
    resultList.resize(num);
    for (int i = 0; i < num; i++) {
        resultList[i].index = phbEntryExtResponse[i]->index;
        resultList[i].number = convertCharPtrToHidlString(phbEntryExtResponse[i]->number);
        resultList[i].type = phbEntryExtResponse[i]->type;
        resultList[i].text = convertCharPtrToHidlString(phbEntryExtResponse[i]->text);
        resultList[i].hidden = phbEntryExtResponse[i]->hidden;
        resultList[i].group = convertCharPtrToHidlString(phbEntryExtResponse[i]->group);
        resultList[i].adnumber = convertCharPtrToHidlString(phbEntryExtResponse[i]->adnumber);
        resultList[i].adtype = phbEntryExtResponse[i]->adtype;
        resultList[i].secondtext = convertCharPtrToHidlString(phbEntryExtResponse[i]->secondtext);
        resultList[i].email = convertCharPtrToHidlString(phbEntryExtResponse[i]->email);
    }
}

int radio::setNetworkSelectionModeManualWithActResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setNetworkSelectionModeManualWithActResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setNetworkSelectionModeManualWithActResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acceptCallResponse: radioService[%d]->setNetworkSelectionModeManualWithActResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::readPhoneBookEntryExtResponse(int slotId,
                                int responseType, int serial, RIL_Errno e,
                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readPhoneBookEntryExtResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<PhbEntryExt> result;
        if (response == NULL || responseLen % sizeof(RIL_PHB_ENTRY *) != 0) {
            mtkLogE(LOG_TAG, "readPhoneBookEntryExtResponse: invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilPhbEntryExtStrucutreToHal(response, responseLen, result);
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->readPhoneBookEntryExtResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readPhoneBookEntryExtResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::writePhoneBookEntryExtResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                                   void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "writePhoneBookEntryExtResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->writePhoneBookEntryExtResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "writePhoneBookEntryExtResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::getAvailableNetworksWithActResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "getAvailableNetworksWithActResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<OperatorInfoWithAct> networks;
        if (response == NULL || responseLen % (6 * sizeof(char *))!= 0) {
            mtkLogE(LOG_TAG, "getAvailableNetworksWithActResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            networks.resize(numStrings/6);
            for (int i = 0, j = 0; i < numStrings; i = i + 6, j++) {
                networks[j].base.alphaLong = convertCharPtrToHidlString(resp[i]);
                networks[j].base.alphaShort = convertCharPtrToHidlString(resp[i + 1]);
                networks[j].base.operatorNumeric = convertCharPtrToHidlString(resp[i + 2]);
                int status = convertOperatorStatusToInt(resp[i + 3]);
                if (status == -1) {
                    if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
                } else {
                    networks[j].base.status = (OperatorStatus) status;
                }
                networks[j].lac = convertCharPtrToHidlString(resp[i + 4]);
                networks[j].act = convertCharPtrToHidlString(resp[i + 5]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getAvailableNetworksWithActResponse(responseInfo,
                networks);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getAvailableNetworksWithActResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::cancelAvailableNetworksResponse(int slotId,
                             int responseType, int serial, RIL_Errno e,
                             void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "cancelAvailableNetworksResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->cancelAvailableNetworksResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "acceptCallResponse: radioService[%d]->cancelAvailableNetworksResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::queryUPBAvailableResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryUPBAvailableResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> upbAvailable;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "queryUPBAvailableResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            upbAvailable.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                upbAvailable[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->queryUPBAvailableResponse(responseInfo,
                upbAvailable);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "queryUPBAvailableResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::readUPBEmailEntryResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBEmailEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBEmailEntryResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBEmailEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::readUPBSneEntryResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBSneEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBSneEntryResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBSneEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::readUPBAnrEntryResponse(int slotId,
                                int responseType, int serial, RIL_Errno e,
                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBAnrEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        hidl_vec<PhbEntryStructure> result;
        if (response == NULL || responseLen % sizeof(RIL_PhbEntryStructure*) != 0) {
            mtkLogD(LOG_TAG, "readUPBAnrEntryResponse: invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            convertRilPhbEntryStructureToHal(response, responseLen, result);
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->readUPBAnrEntryResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBAnrEntryResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::readUPBAasListResponse(int slotId,
                                       int responseType, int serial, RIL_Errno e,
                                       void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "readUPBAasListResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<hidl_string> aasList;
        int numStrings = responseLen / sizeof(char *);
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "readUPBAasListResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **pString = (char **) response;
            aasList.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                aasList[i] = convertCharPtrToHidlString(pString[i]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->readUPBAasListResponse(responseInfo,
                aasList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "readUPBAasListResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::setPhonebookReadyResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPhonebookReadyResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setPhonebookReadyResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPhonebookReadyResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}
// PHB END

/// [IMS] IMS Response Start
int radio::imsEmergencyDialResponse(int slotId,
                                    int responseType, int serial, RIL_Errno e,
                                    void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "imsEmergencyDialResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 emergencyDialResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEmergencyDialResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                              slotId);
    }

    return 0;
}

int radio::imsDialResponse(int slotId,
                           int responseType, int serial, RIL_Errno e, void *response,
                           size_t responseLen) {
    mtkLogD(LOG_TAG, "imsDialResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 dialResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponse->dialResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDialResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
    }

    return 0;
}

int radio::imsVtDialResponse(int slotId, int responseType,
                             int serial, RIL_Errno e, void *response,
                             size_t responselen) {
    mtkLogD(LOG_TAG, "imsVtDialResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 vtDialResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsVtDialResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
    }

    return 0;
}

int radio::videoCallAcceptResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "acceptVideoCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 videoCallAcceptResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEctCommandResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                           slotId);
    }
    return 0;
}

int radio::imsEctCommandResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "imsEctCommandResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 imsEctCommandResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEctCommandResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                           slotId);
    }
    return 0;
}
int radio::holdCallResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "holdResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 holdCallResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "holdCallResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                      slotId);
    }
    return 0;
}
int radio::resumeCallResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "resumeResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 resumeCallResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resumeCallResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }
    return 0;
}
int radio::imsDeregNotificationResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "imsDeregNotificationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 imsDeregNotificationResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDeregNotificationResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                                  slotId);
    }

    return 0;
}

int radio::setImsEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setImsEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImsEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }
    return 0;
}

int radio::setVolteEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setVolteEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setVolteEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setVolteEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }
    return 0;
}

int radio::setWfcEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setWfcEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setWfcEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setWfcEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                          slotId);
    }
    return 0;
}
int radio::setVilteEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setVilteEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setVilteEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setVilteEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                            slotId);
    }

    return 0;
}
int radio::setViWifiEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setViWifiEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setViWifiEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setViWifiEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                             slotId);
    }

    return 0;
}

int radio::setRcsUaEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setRcsUaEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setRcsUaEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRcsUaEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                             slotId);
    }

    return 0;
}

int radio::setImsVoiceEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "setImsVoiceEnableResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImsVoiceEnableResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsVoiceEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                               slotId);
    }

    return 0;
}

int radio::setImsVideoEnableResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setImsVideoEnableResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImsVideoEnableResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsVideoEnableResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                               slotId);
    }

    return 0;
}

int radio::setImscfgResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setImscfgResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImscfgResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImscfgResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                       slotId);
    }

    return 0;
}

int radio::setModemImsCfgResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setModemImsCfgResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setModemImsCfgResponse(responseInfo,
                                 convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setModemImsCfgResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::getProvisionValueResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "getProvisionValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 getProvisionValueResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getProvisionValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                               slotId);
    }

    return 0;
}

int radio::setProvisionValueResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setProvisionValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setProvisionValueResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setProvisionValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                               slotId);
    }

    return 0;
}

//IMS Config TelephonyWare START

int radio::setImsCfgFeatureValueResponse(int slotId, int responseType,
                                         int serial, RIL_Errno e, void *response,
                                         size_t responselen) {

    mtkLogD(LOG_TAG, "setImsCfgFeatureValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                setImsCfgFeatureValueResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "setImsCfgFeatureValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::getImsCfgFeatureValueResponse(int slotId, int responseType,
                                         int serial, RIL_Errno e, void *response,
                                         size_t responselen) {

    mtkLogD(LOG_TAG, "getImsCfgFeatureValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        int value = responseIntOrEmpty(responseInfo, serial, responseType, e, response,
                                       responselen);
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                getImsCfgFeatureValueResponse(responseInfo, value);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "getImsCfgFeatureValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::setImsCfgProvisionValueResponse(int slotId, int responseType,
                                           int serial, RIL_Errno e, void *response,
                                           size_t responselen) {

    mtkLogD(LOG_TAG, "setImsCfgProvisionValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                setImsCfgProvisionValueResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "setImsCfgProvisionValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::getImsCfgProvisionValueResponse(int slotId, int responseType,
                                         int serial, RIL_Errno e, void *response,
                                         size_t responselen) {

    mtkLogD(LOG_TAG, "getImsCfgProvisionValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                getImsCfgProvisionValueResponse(responseInfo, convertCharPtrToHidlString(
                (char *) response));

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "getImsCfgProvisionValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::setImsCfgResourceCapValueResponse(int slotId, int responseType,
                                             int serial, RIL_Errno e, void *response,
                                             size_t responselen) {

    mtkLogD(LOG_TAG, "setImsCfgResourceCapValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                setImsCfgResourceCapValueResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "setImsCfgResourceCapValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::getImsCfgResourceCapValueResponse(int slotId, int responseType,
                                         int serial, RIL_Errno e, void *response,
                                         size_t responselen) {

    mtkLogD(LOG_TAG, "getImsCfgResourceCapValueResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        int value = responseIntOrEmpty(responseInfo, serial, responseType, e, response,
                                       responselen);
        Return<void> retStatus = radioService[slotId]->
                mRadioResponseIms->
                getImsCfgResourceCapValueResponse(responseInfo, value);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "getImsCfgResourceCapValueResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}
//IMS Config TelephonyWare END



int radio::addImsConferenceCallMemberResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "addImsConfCallMemberRsp: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 addImsConferenceCallMemberResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "addImsConfCallMemberRsp: radioService[%d]->mRadioResponseIms == NULL",
                                                                             slotId);
    }

    return 0;
}

int radio::removeImsConferenceCallMemberResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "removeImsConfCallMemberRsp: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);

        Return<void> retStatus = radioService[slotId]->
                               mRadioResponseIms->
                               removeImsConferenceCallMemberResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "removeImsConfCallMemberRsp: radioService[%d]->mRadioResponseIms == NULL",
                                                                                slotId);
    }

    return 0;
}
int radio::setWfcProfileResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setWfcProfileResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setWfcProfileResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setWfcProfileResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                           slotId);
    }

    return 0;
}
int radio::conferenceDialResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "conferenceDialResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 conferenceDialResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "conferenceDialResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                            slotId);
    }

    return 0;
}
int radio::vtDialWithSipUriResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "vtDialWithSipUriResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 vtDialWithSipUriResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "vtDialWithSipUriResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                              slotId);
    }

    return 0;
}
int radio::dialWithSipUriResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "dialWithSipUriResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 dialWithSipUriResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "dialWithSipUriResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}
int radio::sendUssiResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "sendUssiResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]
                                 ->mRadioResponseIms
                                 ->sendUssiResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendUssiResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                      slotId);
    }

    return 0;
}

int radio::cancelUssiResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "cancelUssiResponse: serial %d", serial);
    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]
                                 ->mRadioResponseIms
                                 ->cancelUssiResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cancelUssiResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                        slotId);
    }

    return 0;
}

int radio::getXcapStatusResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->getXcapStatusResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG, "getXcapStatusResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        mtkLogE(LOG_TAG, "getXcapStatusResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::resetSuppServResponse(int slotId, int responseType, int serial, RIL_Errno e,
                           void *response, size_t responseLen) {
    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]->mRadioResponseIms->resetSuppServResponse(
                    responseInfo);
            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG,
                    "resetSuppServResponse: radioService[%d]->mRadioResponseIms == NULL", slotId);
        }
    } else {
        mtkLogE(LOG_TAG,
                "resetSuppServResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setupXcapUserAgentStringResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "setupXcapUserAgentStringResponse: serial %d", serial);
    if (isImsSlot(slotId)) {
        if (radioService[slotId]->mRadioResponseIms != NULL) {
            RadioResponseInfo responseInfo = {};
            populateResponseInfo(responseInfo, serial, responseType, e);
            Return<void> retStatus = radioService[slotId]
                                    ->mRadioResponseIms
                                    ->setupXcapUserAgentStringResponse(responseInfo);

            radioService[slotId]->checkReturnStatus(retStatus);
        } else {
            mtkLogE(LOG_TAG,
                    "setupXcapUserAgentStringResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                  slotId);
        }
    } else {
        mtkLogE(LOG_TAG,
                "setupXcapUserAgentStringResponse: radioService[%d]->mRadioResponseMtk == NULL",
                                                              slotId);
    }

    return 0;
}

int radio::forceReleaseCallResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "forceReleaseCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 forceReleaseCallResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "forceReleaseCallResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                              slotId);
    }
    return 0;
}

int radio::setImsRtpReportResponse(int slotId,
                            int responseType, int serial, RIL_Errno e,
                            void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setImsRtpReportResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImsRtpReportResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsRtpReportResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                             slotId);
    }

    return 0;
}

int radio::imsBearerActivationDoneResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "imsBearerActiveResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 imsBearerActivationDoneResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsBearerActiveResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                             slotId);
    }

    return 0;
}

int radio::imsBearerDeactivationDoneResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "imsBearerDeactiveResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 imsBearerDeactivationDoneResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsBearerDeactiveResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                               slotId);
    }

    return 0;
}

int radio::setImsBearerNotificationResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setImsBearerNotificationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setImsBearerNotificationResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsBearerNotificationResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::pullCallResponse(int slotId,
                            int responseType, int serial, RIL_Errno e,
                            void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "pullCallResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseIms
                                                     ->pullCallResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "pullCallResponse: radioService[%d]->mRadioResponseIms == NULL",
                                                                      slotId);
    }
    return 0;
}

int radio::setImsRegistrationReportResponse(int slotId,
                            int responseType, int serial, RIL_Errno e,
                            void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setImsRegistrationReportRsp: serial %d", serial);


    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseIms
                                                     ->setImsRegistrationReportResponse(
                                                       responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setImsRegistrationReportRsp: radioService[%d]->mRadioResponseIms == NULL",
                                                                                 slotId);
    }
    return 0;
}

RadioIndicationType convertIntToRadioIndicationType(int indicationType) {
    return indicationType == RESPONSE_UNSOLICITED ? (RadioIndicationType::UNSOLICITED) :
            (RadioIndicationType::UNSOLICITED_ACK_EXP);
}

int radio::resetAttachApnInd(int slotId, int indicationType, int token, RIL_Errno e,
                             void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        Return<void> retStatus
                = radioService[slotId]->mRadioIndicationMtk->resetAttachApnInd(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resetAttachApnInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

int radio::mdChangeApnInd(int slotId, int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_vec<int32_t> data;

        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "mdChangeApnInd Invalid response: NULL");
            return 0;
        } else {
            int *pInt = (int *) response;
            data.resize(numInts);
            for (int i=0; i<numInts; i++) {
                data[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioIndicationMtk->mdChangedApnInd(
                convertIntToRadioIndicationType(indicationType), data[0]);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "mdChangeApnInd: radioService[%d]->mRadioIndicationMtk == NULL",
                slotId);
    }
    return 0;
}

// World Phone {
int radio::setResumeRegistrationResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setResumeRegistrationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setResumeRegistrationResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setResumeRegistrationResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::storeModemTypeResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "storeModemTypeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->storeModemTypeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "storeModemTypeResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::reloadModemTypeResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reloadModemTypeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->reloadModemTypeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "reloadModemTypeResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

// World Phone }

// Radio Indication functions

int radio::radioStateChangedInd(int slotId,
                                 int indicationType, int token, RIL_Errno e, void *response,
                                 size_t responseLen) {

    if(s_vendorFunctions == NULL) {
        mtkLogE(LOG_TAG, "radioStateChangedInd: service is not ready");
        return 0;
    }

    // Retrive Radio State
    RadioState radioState = (RadioState) s_vendorFunctions->
                                         onStateRequest((RIL_SOCKET_ID)slotId);
    mtkLogD(LOG_TAG, "radioStateChangedInd: radioState %d, slot = %d", radioState, slotId);

    // Send to RILJ
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->radioStateChanged(
                convertIntToRadioIndicationType(indicationType), radioState);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        Return<void> retStatus = radioService[slotId]->mRadioIndication->radioStateChanged(
                convertIntToRadioIndicationType(indicationType), radioState);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "radioStateChangedInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    // Send to IMS
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndication != NULL) {

        Return<void> retStatus = radioService[imsSlotId]->mRadioIndication->radioStateChanged(
                convertIntToRadioIndicationType(indicationType), radioState);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "radioStateChangedInd: radioService[%d]->mRadioIndication == NULL", imsSlotId);
    }

    return 0;
}


/// M: eMBMS feature
int radio::sendEmbmsAtCommandResponse(int slotId,
                                  int responseType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "sendEmbmsAtCommandResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->sendEmbmsAtCommandResponse(responseInfo,
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendEmbmsAtCommandResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::embmsAtInfoInd(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    //dbg
    mtkLogD(LOG_TAG, "embmsAtInfoInd: slotId:%d", slotId);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_string info;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "embmsAtInfoInd: invalid response");
            return 0;
        } else {
            mtkLogD(LOG_TAG, "embmsAtInfoInd[%d]: %s", slotId, (char*)response);
            info = convertCharPtrToHidlString((char *)response);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->eMBMSAtInfoIndication(
                convertIntToRadioIndicationType(indicationType), info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "embmsAtInfoInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::embmsSessionStatusInd(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen == 0) {
            mtkLogE(LOG_TAG, "embmsSessionStatusInd: invalid response");
            return 0;
        }

        int32_t status = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "embmsSessionStatusInd[%d]: %d", slotId, status);
        Return<void> retStatus =
            radioService[slotId]->mRadioIndicationMtk->eMBMSSessionStatusIndication(
                convertIntToRadioIndicationType(indicationType), status);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "embmsSessionStatusInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
/// M: eMBMS end

int radio::callStateChangedInd(int slotId,
                               int indicationType, int token, RIL_Errno e, void *response,
                               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "callStateChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->callStateChanged(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "callStateChangedInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::networkStateChangedInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "networkStateChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->networkStateChanged(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "networkStateChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

uint8_t hexCharToInt(uint8_t c) {
    if (c >= '0' && c <= '9') return (c - '0');
    if (c >= 'A' && c <= 'F') return (c - 'A' + 10);
    if (c >= 'a' && c <= 'f') return (c - 'a' + 10);

    return INVALID_HEX_CHAR;
}

uint8_t * convertHexStringToBytes(void *response, size_t responseLen) {
    if (responseLen % 2 != 0) {
        return NULL;
    }

    uint8_t *bytes = (uint8_t *)calloc(responseLen/2, sizeof(uint8_t));
    if (bytes == NULL) {
        mtkLogE(LOG_TAG, "convertHexStringToBytes: cannot allocate memory for bytes string");
        return NULL;
    }
    uint8_t *hexString = (uint8_t *)response;

    for (size_t i = 0; i < responseLen; i += 2) {
        uint8_t hexChar1 = hexCharToInt(hexString[i]);
        uint8_t hexChar2 = hexCharToInt(hexString[i + 1]);

        if (hexChar1 == INVALID_HEX_CHAR || hexChar2 == INVALID_HEX_CHAR) {
            mtkLogE(LOG_TAG, "convertHexStringToBytes: invalid hex char %d %d",
                    hexString[i], hexString[i + 1]);
            free(bytes);
            return NULL;
        }
        bytes[i/2] = ((hexChar1 << 4) | hexChar2);
    }

    return bytes;
}

int radio::newSmsInd(int slotId, int indicationType,
                     int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "newSmsInd: invalid response");
            return 0;
        }

        uint8_t *bytes = convertHexStringToBytes(response, responseLen);
        if (bytes == NULL) {
            mtkLogE(LOG_TAG, "newSmsInd: convertHexStringToBytes failed");
            return 0;
        }

        hidl_vec<uint8_t> pdu;
        pdu.setToExternal(bytes, responseLen/2);
        mtkLogD(LOG_TAG, "newSmsInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->newSms(
                convertIntToRadioIndicationType(indicationType), pdu);
        radioService[slotId]->checkReturnStatus(retStatus);
        free(bytes);
    } else {
        mtkLogE(LOG_TAG, "newSmsInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::newSmsStatusReportInd(int slotId,
                                 int indicationType, int token, RIL_Errno e, void *response,
                                 size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "newSmsStatusReportInd: invalid response");
            return 0;
        }

        uint8_t *bytes = convertHexStringToBytes(response, responseLen);
        if (bytes == NULL) {
            mtkLogE(LOG_TAG, "newSmsStatusReportInd: convertHexStringToBytes failed");
            return 0;
        }

        hidl_vec<uint8_t> pdu;
        pdu.setToExternal(bytes, responseLen/2);
        mtkLogD(LOG_TAG, "newSmsStatusReportInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->newSmsStatusReport(
                convertIntToRadioIndicationType(indicationType), pdu);
        radioService[slotId]->checkReturnStatus(retStatus);
        free(bytes);
    } else {
        mtkLogE(LOG_TAG, "newSmsStatusReportInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::newSmsOnSimInd(int slotId, int indicationType,
                          int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "newSmsOnSimInd: invalid response");
            return 0;
        }
        int32_t recordNumber = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "newSmsOnSimInd: slotIndex %d", recordNumber);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->newSmsOnSim(
                convertIntToRadioIndicationType(indicationType), recordNumber);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "newSmsOnSimInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::onUssdInd(int slotId, int indicationType,
                     int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != 2 * sizeof(char *)) {
            mtkLogE(LOG_TAG, "onUssdInd: invalid response");
            return 0;
        }
        char **strings = (char **) response;
        char *mode = strings[0];
        hidl_string msg = convertCharPtrToHidlString(strings[1]);
        UssdModeType modeType = (UssdModeType) atoi(mode);
        mtkLogD(LOG_TAG, "onUssdInd: mode %s", mode);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->onUssd(
                convertIntToRadioIndicationType(indicationType), modeType, msg);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onUssdInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::nitzTimeReceivedInd(int slotId,
                               int indicationType, int token, RIL_Errno e, void *response,
                               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "nitzTimeReceivedInd: invalid response");
            return 0;
        }
        hidl_string nitzTime = convertCharPtrToHidlString((char *) response);
#if VDBG
        mtkLogD(LOG_TAG, "nitzTimeReceivedInd: nitzTime %s receivedTime %" PRId64, nitzTime.c_str(),
                nitzTimeReceived[slotId]);
#endif
        Return<void> retStatus = radioService[slotId]->mRadioIndication->nitzTimeReceived(
                convertIntToRadioIndicationType(indicationType), nitzTime,
                nitzTimeReceived[slotId]);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "nitzTimeReceivedInd: radioService[%d]->mRadioIndication == NULL", slotId);
        return -1;
    }

    return 0;
}

void convertRilSignalStrengthToHal(void *response, size_t responseLen,
        SignalStrength& signalStrength) {
    int32_t *rilSignalStrength = (int32_t *) response;

    signalStrength.gw.signalStrength = rilSignalStrength[0];
    signalStrength.gw.bitErrorRate = rilSignalStrength[1];
    signalStrength.cdma.dbm = rilSignalStrength[3];
    signalStrength.cdma.ecio = rilSignalStrength[4];
    signalStrength.evdo.dbm = rilSignalStrength[5];
    signalStrength.evdo.ecio = rilSignalStrength[6];
    signalStrength.evdo.signalNoiseRatio = rilSignalStrength[7];
    signalStrength.lte.signalStrength = rilSignalStrength[8];
    signalStrength.lte.rsrp = rilSignalStrength[9];
    signalStrength.lte.rsrq = rilSignalStrength[10];
    signalStrength.lte.rssnr = rilSignalStrength[11];
    signalStrength.lte.cqi = rilSignalStrength[12];
    signalStrength.lte.timingAdvance = rilSignalStrength[13];
    signalStrength.tdScdma.rscp = rilSignalStrength[16];
}

void convertRilSignalStrengthToHal_1_2(void *response, size_t responseLen,
        AOSP_V1_2::SignalStrength& signalStrength) {
    int32_t *rilSignalStrength = (int32_t *) response;

    signalStrength.gsm.signalStrength = rilSignalStrength[0];
    signalStrength.gsm.bitErrorRate = rilSignalStrength[1];
    signalStrength.gsm.timingAdvance = rilSignalStrength[2];
    signalStrength.cdma.dbm = rilSignalStrength[3];
    signalStrength.cdma.ecio = rilSignalStrength[4];
    signalStrength.evdo.dbm = rilSignalStrength[5];
    signalStrength.evdo.ecio = rilSignalStrength[6];
    signalStrength.evdo.signalNoiseRatio = rilSignalStrength[7];
    signalStrength.lte.signalStrength = rilSignalStrength[8];
    signalStrength.lte.rsrp = rilSignalStrength[9];
    signalStrength.lte.rsrq = rilSignalStrength[10];
    signalStrength.lte.rssnr = rilSignalStrength[11];
    signalStrength.lte.cqi = rilSignalStrength[12];
    signalStrength.lte.timingAdvance = rilSignalStrength[13];
    // AOSP still use 1.0's TdScdma, not 1.2's Tdscdma. It's "S", not "s"
    // signalStrength.tdScdma.signalStrength = rilSignalStrength[14];
    // signalStrength.tdScdma.bitErrorRate = rilSignalStrength[15];
    signalStrength.tdScdma.rscp = rilSignalStrength[16];
    signalStrength.wcdma.base.signalStrength = rilSignalStrength[17];
    signalStrength.wcdma.base.bitErrorRate = rilSignalStrength[18];
    signalStrength.wcdma.rscp = rilSignalStrength[19];
    signalStrength.wcdma.ecno = rilSignalStrength[20];
}


int radio::currentSignalStrengthInd(int slotId,
                                    int indicationType, int token, RIL_Errno e,
                                    void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_2!= NULL) {
        // wait for AOSP define RIL_SignalStrength_v1X for signal 1.2
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "currentSignalStrength_1_2: invalid response");
            return 0;
        }

        AOSP_V1_2::SignalStrength signalStrength = {};

        convertRilSignalStrengthToHal_1_2(response, responseLen, signalStrength);

        Return<void> retStatus = radioService[slotId]->mRadioIndicationV1_2->currentSignalStrength_1_2(
                convertIntToRadioIndicationType(indicationType), signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "currentSignalStrength: invalid response");
            return 0;
        }

        SignalStrength signalStrength = {};

        convertRilSignalStrengthToHal(response, responseLen, signalStrength);

        Return<void> retStatus = radioService[slotId]->mRadioIndication->currentSignalStrength(
                convertIntToRadioIndicationType(indicationType), signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "currentSignalStrength: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::currentSignalStrengthWithWcdmaEcioInd(int slotId,
                                    int indicationType, int token, RIL_Errno e,
                                    void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk!= NULL) {
        if (response == NULL || responseLen != 21*sizeof(int)) {
            mtkLogE(LOG_TAG, "currentSignalStrengthWithWcdmaEcioInd: invalid response");
            return 0;
        }
        SignalStrengthWithWcdmaEcio signalStrength = {};
        int32_t *rilSignalStrength = (int32_t *) response;
        signalStrength.gsm_signalStrength = rilSignalStrength[0];
        signalStrength.gsm_bitErrorRate = rilSignalStrength[1];
        signalStrength.wcdma_rscp = rilSignalStrength[19];
        signalStrength.wcdma_ecio = rilSignalStrength[20];
        signalStrength.cdma_dbm = rilSignalStrength[3];
        signalStrength.cdma_ecio = rilSignalStrength[4];
        signalStrength.evdo_dbm = rilSignalStrength[5];
        signalStrength.evdo_ecio = rilSignalStrength[6];
        signalStrength.evdo_signalNoiseRatio = rilSignalStrength[7];
        signalStrength.lte_signalStrength = rilSignalStrength[8];
        signalStrength.lte_rsrp = rilSignalStrength[9];
        signalStrength.lte_rsrq = rilSignalStrength[10];
        signalStrength.lte_rssnr = rilSignalStrength[11];
        signalStrength.lte_cqi = rilSignalStrength[12];
        signalStrength.tdscdma_rscp = rilSignalStrength[16];
        Return<void> retStatus =
                radioService[slotId]->mRadioIndicationMtk->currentSignalStrengthWithWcdmaEcioInd(
                convertIntToRadioIndicationType(indicationType), signalStrength);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "currentSignalStrength: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

void convertRilDataCallToHal(RIL_Data_Call_Response_v11 *dcResponse,
        SetupDataCallResult& dcResult) {
    dcResult.status = (DataCallFailCause) dcResponse->status;
    dcResult.suggestedRetryTime = dcResponse->suggestedRetryTime;
    dcResult.cid = dcResponse->cid;
    dcResult.active = dcResponse->active;
    dcResult.type = convertCharPtrToHidlString(dcResponse->type);
    dcResult.ifname = convertCharPtrToHidlString(dcResponse->ifname);
    dcResult.addresses = convertCharPtrToHidlString(dcResponse->addresses);
    dcResult.dnses = convertCharPtrToHidlString(dcResponse->dnses);
    dcResult.gateways = convertCharPtrToHidlString(dcResponse->gateways);
    dcResult.pcscf = convertCharPtrToHidlString(dcResponse->pcscf);
    dcResult.mtu = dcResponse->mtu;
}

void convertRilDataCallListToHal(void *response, size_t responseLen,
        hidl_vec<SetupDataCallResult>& dcResultList) {
    int num = responseLen / sizeof(RIL_Data_Call_Response_v11);

    RIL_Data_Call_Response_v11 *dcResponse = (RIL_Data_Call_Response_v11 *) response;
    dcResultList.resize(num);
    for (int i = 0; i < num; i++) {
        convertRilDataCallToHal(&dcResponse[i], dcResultList[i]);
    }
}

// M: [OD over ePDG] start
static int s_isWfcSupport = -1;
int encodeRat(int active, int rat, int slotId) {
    char wifiSupportApn[MTK_PROPERTY_VALUE_MAX] = {0};
    mtk_property_get("persist.vendor.radio.data.oem_wifi_support_apn", wifiSupportApn, "0");

    if (s_isWfcSupport < 0) {
        FeatureValue feature;
        memset(feature.value, 0, sizeof(feature.value));
        mtkGetFeature(CONFIG_WFC, &feature);
        s_isWfcSupport = strcmp(feature.value, "1") == 0 ? 1 : 0;
    }

    if ((atoi(wifiSupportApn) == 1) || isMtkFwkAddonNotExisted(slotId) || (s_isWfcSupport == 0) ||
            (active >= RAT_TYPE_KEY) || (active < 0) ||
            ((rat + 1) > RAT_TYPE_MAX) || (rat < 0)) {
        mtkLogI(LOG_TAG, "encodeRat: cannot encode rat type into active [wifiSupportApn=%d, "
                "isMtkFwkAddonNotExisted=%d, s_isWfcSupport=%d, rat=%d, active=%d]",
                atoi(wifiSupportApn), BOOL_TO_INT(isMtkFwkAddonNotExisted(slotId)),
                s_isWfcSupport, rat, active);
        return active;
    }
    return ((rat - 1) * RAT_TYPE_KEY) + active;
}

void convertRilDataCallToHalEx(MTK_RIL_Data_Call_Response_v11 *dcResponse,
        SetupDataCallResult& dcResult, int slotId) {
    dcResult.status = (DataCallFailCause) dcResponse->status;
    dcResult.suggestedRetryTime = dcResponse->suggestedRetryTime;
    dcResult.cid = dcResponse->cid;
    dcResult.active = encodeRat(dcResponse->active, dcResponse->rat, slotId);
    dcResult.type = convertCharPtrToHidlString(dcResponse->type);
    dcResult.ifname = convertCharPtrToHidlString(dcResponse->ifname);
    dcResult.addresses = convertCharPtrToHidlString(dcResponse->addresses);
    dcResult.dnses = convertCharPtrToHidlString(dcResponse->dnses);
    dcResult.gateways = convertCharPtrToHidlString(dcResponse->gateways);
    dcResult.pcscf = convertCharPtrToHidlString(dcResponse->pcscf);
    dcResult.mtu = dcResponse->mtu;
}

void convertRilDataCallListToHalEx(void *response, size_t responseLen,
        hidl_vec<SetupDataCallResult>& dcResultList, int slotId) {
    int num = responseLen / sizeof(MTK_RIL_Data_Call_Response_v11);

    MTK_RIL_Data_Call_Response_v11 *dcResponse = (MTK_RIL_Data_Call_Response_v11 *) response;
    dcResultList.resize(num);
    for (int i = 0; i < num; i++) {
        convertRilDataCallToHalEx(&dcResponse[i], dcResultList[i], slotId);
    }
}
// M: [OD over ePDG] end

int radio::dataCallListChangedInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if ((response == NULL && responseLen != 0) ||
                // M: [OD over ePDG]
                // remark AOSP
                //responseLen % sizeof(RIL_Data_Call_Response_v11) != 0) {
                responseLen % sizeof(MTK_RIL_Data_Call_Response_v11) != 0) {
            mtkLogE(LOG_TAG, "dataCallListChangedInd: invalid response");
            return 0;
        }
        hidl_vec<SetupDataCallResult> dcList;
        // M: [OD over ePDG]
        // remark AOSP
        //convertRilDataCallListToHal(response, responseLen, dcList);
        convertRilDataCallListToHalEx(response, responseLen, dcList, slotId);
        mtkLogD(LOG_TAG, "dataCallListChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->dataCallListChanged(
                convertIntToRadioIndicationType(indicationType), dcList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "dataCallListChangedInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::suppSvcNotifyInd(int slotId, int indicationType,
                            int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_SuppSvcNotification)) {
            mtkLogE(LOG_TAG, "suppSvcNotifyInd: invalid response");
            return 0;
        }

        SuppSvcNotification suppSvc = {};
        RIL_SuppSvcNotification *ssn = (RIL_SuppSvcNotification *) response;
        suppSvc.isMT = ssn->notificationType;
        suppSvc.code = ssn->code;
        suppSvc.index = ssn->index;
        suppSvc.type = ssn->type;
        suppSvc.number = convertCharPtrToHidlString(ssn->number);

        mtkLogD(LOG_TAG, "suppSvcNotifyInd: isMT %d code %d index %d type %d",
                suppSvc.isMT, suppSvc.code, suppSvc.index, suppSvc.type);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->suppSvcNotify(
                convertIntToRadioIndicationType(indicationType), suppSvc);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "suppSvcNotifyInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::cfuStatusNotifyInd(int slotId, int indicationType,
                            int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != 2 * sizeof(int)) {
            mtkLogE(LOG_TAG, "cfuStatusNotifyInd: invalid response");
            return 0;
        }

        CfuStatusNotification cfuStatus = {};
        int *csn = (int *) response;
        cfuStatus.status = csn[0];
        cfuStatus.lineId = csn[1];

        mtkLogD(LOG_TAG, "cfuStatusNotifyInd: status = %d, line = %d", cfuStatus.status, cfuStatus.lineId);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->cfuStatusNotify(
                convertIntToRadioIndicationType(indicationType), cfuStatus);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cfuStatusNotifyInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

/// M: CC: call control ([IMS] common flow) @{
int radio::incomingCallIndicationInd(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {

    if(response == NULL) {
        mtkLogE(LOG_TAG, "incomingCallIndicationInd: response is NULL");
        return 0;
    }

    char **resp = (char **) response;
    int numStrings = responseLen / sizeof(char *);
    if(numStrings < 7) {
        mtkLogE(LOG_TAG, "incomingCallIndicationInd: items length is invalid, slot = %d", slotId);
        return 0;
    }

    int mode = atoi(resp[3]);
    if(mode >= 20) {   // Code Mode >= 20, IMS's Call Info Indication, otherwise CS's
        int imsSlot = toImsSlot(slotId);
        if (radioService[imsSlot] != NULL &&
            radioService[imsSlot]->mRadioIndicationIms != NULL) {

            IncomingCallNotification inCallNotify = {};
            // EAIC: <callId>, <number>, <type>, <call mode>, <seq no>
            inCallNotify.callId = convertCharPtrToHidlString(resp[0]);
            inCallNotify.number = convertCharPtrToHidlString(resp[1]);
            inCallNotify.type = convertCharPtrToHidlString(resp[2]);
            inCallNotify.callMode = convertCharPtrToHidlString(resp[3]);
            inCallNotify.seqNo = convertCharPtrToHidlString(resp[4]);
            inCallNotify.redirectNumber = convertCharPtrToHidlString(resp[5]);
            inCallNotify.toNumber = convertCharPtrToHidlString(resp[6]);

            mtkLogD(LOG_TAG, "incomingCallIndicationInd: %s, %s, %s, %s, %s, %s, %s",
                    resp[0], resp[1], resp[2], resp[3], resp[4], resp[5], resp[6]);

            Return<void> retStatus = radioService[imsSlot]->
                                     mRadioIndicationIms->incomingCallIndication(
                                     convertIntToRadioIndicationType(indicationType),
                                     inCallNotify);
            radioService[imsSlot]->checkReturnStatus(retStatus);
        }
        else {
            mtkLogE(LOG_TAG, "incomingCallIndicationInd: service[%d]->mRadioIndicationIms == NULL",
                                                                                imsSlot);
        }

        return 0;
    }

    //      case RIL_UNSOL_INCOMING_CALL_INDICATION: ret = responseStrings(p); break;
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != 7 * sizeof(char *)) {
            mtkLogE(LOG_TAG, "incomingCallIndicationInd: invalid response");
            return 0;
        }
        IncomingCallNotification inCallNotify = {};
        char **strings = (char **) response;
        // EAIC: <callId>, <number>, <type>, <call mode>, <seq no>
        inCallNotify.callId = convertCharPtrToHidlString(strings[0]);
        inCallNotify.number = convertCharPtrToHidlString(strings[1]);
        inCallNotify.type = convertCharPtrToHidlString(strings[2]);
        inCallNotify.callMode = convertCharPtrToHidlString(strings[3]);
        inCallNotify.seqNo = convertCharPtrToHidlString(strings[4]);
        inCallNotify.redirectNumber = convertCharPtrToHidlString(strings[5]);
        // string[6] is used by ims. no need here.
        mtkLogD(LOG_TAG, "incomingCallIndicationInd: %s, %s, %s, %s, %s, %s",
                strings[0], strings[1], strings[2],
                strings[3], strings[4], strings[5]);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->incomingCallIndication(
                convertIntToRadioIndicationType(indicationType), inCallNotify);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "incomingCallIndicationInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }


    return 0;
}

/// M: CC: GSM 02.07 B.1.26 Ciphering Indicator support
int radio::cipherIndicationInd(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != 4 * sizeof(char *)) {
            mtkLogE(LOG_TAG, "cipherInd: invalid response");
            return 0;
        }
        CipherNotification cipherNotify = {};
        char **strings = (char **) response;
        //+ECIPH:  <sim_cipher_ind>,<mm_connection>,<cs_cipher_on>,<ps_cipher_on>
        cipherNotify.simCipherStatus = convertCharPtrToHidlString(strings[0]);
        cipherNotify.sessionStatus = convertCharPtrToHidlString(strings[1]);
        cipherNotify.csStatus = convertCharPtrToHidlString(strings[2]);
        cipherNotify.psStatus = convertCharPtrToHidlString(strings[3]);
        mtkLogD(LOG_TAG, "cipherInd: %s, %s, %s, %s", strings[0], strings[1], strings[2], strings[3]);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->cipherIndication(
                convertIntToRadioIndicationType(indicationType), cipherNotify);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cipherInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

/// M: CC: call control
int radio::crssNotifyInd(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_CrssNotification)) {
            mtkLogE(LOG_TAG, "crssNotifyInd: invalid response");
            return 0;
        }
        CrssNotification crssNotify = {};
        RIL_CrssNotification *crss = (RIL_CrssNotification *) response;
        crssNotify.code = crss->code;
        crssNotify.type = crss->type;
        crssNotify.number = convertCharPtrToHidlString(crss->number);
        crssNotify.alphaid = convertCharPtrToHidlString(crss->alphaid);
        crssNotify.cli_validity = crss->cli_validity;

        mtkLogD(LOG_TAG, "crssNotifyInd: code %d type %d cli_validity %d",
                crssNotify.code, crssNotify.type, crssNotify.cli_validity);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->crssIndication(
                convertIntToRadioIndicationType(indicationType), crssNotify);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "crssNotifyInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

/// M: CC: GSA HD Voice for 2/3G network support
int radio::speechCodecInfoInd(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "ims speechCodecInfoInd: invalid response");
            return 0;
        }
        int32_t info = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "ims speechCodecInfoInd: %d", info);
        Return<void> retStatus =
                radioService[imsSlotId]->mRadioIndicationIms->speechCodecInfoIndication(
                        convertIntToRadioIndicationType(indicationType), info);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "ims speechCodecInfoInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "speechCodecInfoInd: invalid response");
            return 0;
        }
        int32_t info = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "speechCodecInfoInd: %d", info);
        Return<void> retStatus =
                radioService[slotId]->mRadioIndicationMtk->speechCodecInfoIndication(
                convertIntToRadioIndicationType(indicationType), info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "speechCodecInfoInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

/// M: CC: CDMA call accepted indication @{
int radio::cdmaCallAcceptedInd(int slotId, int indicationType,
                               int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "cdmaCallAcceptedInd: slotId=%d", slotId);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->cdmaCallAccepted(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaCallAcceptedInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
/// @}

int radio::stkSessionEndInd(int slotId, int indicationType,
                            int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "stkSessionEndInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->stkSessionEnd(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stkSessionEndInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::stkProactiveCommandInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "stkProactiveCommandInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "stkProactiveCommandInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->stkProactiveCommand(
                convertIntToRadioIndicationType(indicationType),
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stkProactiveCommandInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::stkEventNotifyInd(int slotId, int indicationType,
                             int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "stkEventNotifyInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "stkEventNotifyInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->stkEventNotify(
                convertIntToRadioIndicationType(indicationType),
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stkEventNotifyInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::stkCallSetupInd(int slotId, int indicationType,
                           int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "stkCallSetupInd: invalid response");
            return 0;
        }
        int32_t timeout = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "stkCallSetupInd: timeout %d", timeout);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->stkCallSetup(
                convertIntToRadioIndicationType(indicationType), timeout);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stkCallSetupInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::simSmsStorageFullInd(int slotId,
                                int indicationType, int token, RIL_Errno e, void *response,
                                size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "simSmsStorageFullInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->simSmsStorageFull(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "simSmsStorageFullInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::simRefreshInd(int slotId, int indicationType,
                         int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_SimRefreshResponse_v7)) {
            mtkLogE(LOG_TAG, "simRefreshInd: invalid response");
            return 0;
        }

        SimRefreshResult refreshResult = {};
        RIL_SimRefreshResponse_v7 *simRefreshResponse = ((RIL_SimRefreshResponse_v7 *) response);
        refreshResult.type =
                (android::hardware::radio::V1_0::SimRefreshType) simRefreshResponse->result;
        refreshResult.efId = simRefreshResponse->ef_id;
        refreshResult.aid = convertCharPtrToHidlString(simRefreshResponse->aid);

        mtkLogD(LOG_TAG, "simRefreshInd: type %d efId %d", refreshResult.type, refreshResult.efId);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->simRefresh(
                convertIntToRadioIndicationType(indicationType), refreshResult);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "simRefreshInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

void convertRilCdmaSignalInfoRecordToHal(RIL_CDMA_SignalInfoRecord *signalInfoRecord,
        CdmaSignalInfoRecord& record) {
    record.isPresent = signalInfoRecord->isPresent;
    record.signalType = signalInfoRecord->signalType;
    record.alertPitch = signalInfoRecord->alertPitch;
    record.signal = signalInfoRecord->signal;
}

int radio::callRingInd(int slotId, int indicationType,
                       int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        bool isGsm;
        CdmaSignalInfoRecord record = {};
        if (response == NULL || responseLen == 0) {
            isGsm = true;
        } else {
            isGsm = false;
            if (responseLen != sizeof (RIL_CDMA_SignalInfoRecord)) {
                mtkLogE(LOG_TAG, "callRingInd: invalid response");
                return 0;
            }
            convertRilCdmaSignalInfoRecordToHal((RIL_CDMA_SignalInfoRecord *) response, record);
        }

        mtkLogD(LOG_TAG, "callRingInd: isGsm %d", isGsm);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->callRing(
                convertIntToRadioIndicationType(indicationType), isGsm, record);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "callRingInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::simStatusChangedInd(int slotId,
                               int indicationType, int token, RIL_Errno e, void *response,
                               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "simStatusChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->simStatusChanged(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "simStatusChangedInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

bool makeCdmaSmsMessage(CdmaSmsMessage &msg, void *response, size_t responseLen) {
    if (response == NULL || responseLen != sizeof(RIL_CDMA_SMS_Message)) {
        return false;
    }

    RIL_CDMA_SMS_Message *rilMsg = (RIL_CDMA_SMS_Message *) response;
    msg.teleserviceId = rilMsg->uTeleserviceID;
    msg.isServicePresent = rilMsg->bIsServicePresent;
    msg.serviceCategory = rilMsg->uServicecategory;
    msg.address.digitMode =
            (android::hardware::radio::V1_0::CdmaSmsDigitMode) rilMsg->sAddress.digit_mode;
    msg.address.numberMode =
            (android::hardware::radio::V1_0::CdmaSmsNumberMode) rilMsg->sAddress.number_mode;
    msg.address.numberType =
            (android::hardware::radio::V1_0::CdmaSmsNumberType) rilMsg->sAddress.number_type;
    msg.address.numberPlan =
            (android::hardware::radio::V1_0::CdmaSmsNumberPlan) rilMsg->sAddress.number_plan;

    int digitLimit = MIN((rilMsg->sAddress.number_of_digits), RIL_CDMA_SMS_ADDRESS_MAX);
    msg.address.digits.setToExternal(rilMsg->sAddress.digits, digitLimit);

    msg.subAddress.subaddressType = (android::hardware::radio::V1_0::CdmaSmsSubaddressType)
            rilMsg->sSubAddress.subaddressType;
    msg.subAddress.odd = rilMsg->sSubAddress.odd;

    digitLimit= MIN((rilMsg->sSubAddress.number_of_digits), RIL_CDMA_SMS_SUBADDRESS_MAX);
    msg.subAddress.digits.setToExternal(rilMsg->sSubAddress.digits, digitLimit);

    digitLimit = MIN((rilMsg->uBearerDataLen), RIL_CDMA_SMS_BEARER_DATA_MAX);
    msg.bearerData.setToExternal(rilMsg->aBearerData, digitLimit);
    return true;
}

int radio::cdmaNewSmsInd(int slotId, int indicationType,
                         int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        CdmaSmsMessage msg = {};
        if (!makeCdmaSmsMessage(msg, response, responseLen)) {
            mtkLogE(LOG_TAG, "cdmaNewSmsInd: invalid response");
        }

        mtkLogD(LOG_TAG, "cdmaNewSmsInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaNewSms(
                convertIntToRadioIndicationType(indicationType), msg);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaNewSmsInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::cdmaNewSmsIndEx(int slotId, int indicationType,
                         int token, RIL_Errno e, void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        CdmaSmsMessage msg = {};
        if (!makeCdmaSmsMessage(msg, response, responseLen)) {
            mtkLogE(LOG_TAG, "cdmaNewSmsIndEx: invalid response");
        }

        mtkLogD(LOG_TAG, "cdmaNewSmsIndEx");
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationIms->cdmaNewSmsEx(
                convertIntToRadioIndicationType(indicationType), msg);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "cdmaNewSmsInd:Ex radioService[%d]->mRadioIndicationIms == NULL", imsSlotId);
    }

    return 0;
}


int radio::newBroadcastSmsInd(int slotId,
                              int indicationType, int token, RIL_Errno e, void *response,
                              size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "newBroadcastSmsInd: invalid response");
            return 0;
        }

        hidl_vec<uint8_t> data;
        data.setToExternal((uint8_t *) response, responseLen);
        mtkLogD(LOG_TAG, "newBroadcastSmsInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->newBroadcastSms(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "newBroadcastSmsInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::cdmaRuimSmsStorageFullInd(int slotId,
                                     int indicationType, int token, RIL_Errno e, void *response,
                                     size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "cdmaRuimSmsStorageFullInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaRuimSmsStorageFull(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaRuimSmsStorageFullInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::restrictedStateChangedInd(int slotId,
                                     int indicationType, int token, RIL_Errno e, void *response,
                                     size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "restrictedStateChangedInd: invalid response");
            return 0;
        }
        int32_t state = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "restrictedStateChangedInd: state %d", state);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->restrictedStateChanged(
                convertIntToRadioIndicationType(indicationType), (PhoneRestrictedState) state);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "restrictedStateChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::enterEmergencyCallbackModeInd(int slotId,
                                         int indicationType, int token, RIL_Errno e, void *response,
                                         size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "enterEmergencyCallbackModeInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->enterEmergencyCallbackMode(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "enterEmergencyCallbackModeInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

// M: [VzW] Data Framework @{
int radio::pcoDataAfterAttachedInd(int slotId,
               int indicationType, int token, RIL_Errno e, void *response,
               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_PCO_Data_attached)) {
            mtkLogE(LOG_TAG, "pcoDataAfterAttachedInd: invalid response");
            return 0;
        }

        PcoDataAttachedInfo pco = {};
        RIL_PCO_Data_attached *rilPcoData = (RIL_PCO_Data_attached *)response;
        pco.cid = rilPcoData->cid;
        pco.apnName = convertCharPtrToHidlString(rilPcoData->apn_name);
        pco.bearerProto = convertCharPtrToHidlString(rilPcoData->bearer_proto);
        pco.pcoId = rilPcoData->pco_id;
        pco.contents.setToExternal((uint8_t *) rilPcoData->contents, rilPcoData->contents_length);

        mtkLogD(LOG_TAG, "pcoDataAfterAttachedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->pcoDataAfterAttached(
                convertIntToRadioIndicationType(indicationType), pco);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "pcoDataAfterAttachedInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::networkRejectCauseInd(int slotId,
               int indicationType, int token, RIL_Errno e, void *response,
               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);

        if (response == NULL || numInts < 3) {
            mtkLogE(LOG_TAG, "networkRejectCauseInd: invalid response");
            return 0;
        }

        hidl_vec<int32_t> data;
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }

        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->networkRejectCauseInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "networkRejectCauseInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// M: [VzW] Data Framework @}

// M: [VzW] Data Framework @{
int radio::volteLteConnectionStatusInd(int slotId,
               int indicationType, int token, RIL_Errno e, void *response,
               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "volteLteConnectionStatusInd Invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "volteLteConnectionStatusInd");
        hidl_vec<int32_t> data;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->volteLteConnectionStatus(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "volteLteConnectionStatusInd: radioService[%d]->volteLteConnectionStatusInd == NULL", slotId);
    }
    return 0;
}
// M: [VzW] Data Framework @}

int radio::cdmaCallWaitingInd(int slotId,
                              int indicationType, int token, RIL_Errno e, void *response,
                              size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_CDMA_CallWaiting_v6)) {
            mtkLogE(LOG_TAG, "cdmaCallWaitingInd: invalid response");
            return 0;
        }

        CdmaCallWaiting callWaitingRecord = {};
        RIL_CDMA_CallWaiting_v6 *callWaitingRil = ((RIL_CDMA_CallWaiting_v6 *) response);
        callWaitingRecord.number = convertCharPtrToHidlString(callWaitingRil->number);
        callWaitingRecord.numberPresentation =
                (CdmaCallWaitingNumberPresentation) callWaitingRil->numberPresentation;
        callWaitingRecord.name = convertCharPtrToHidlString(callWaitingRil->name);
        convertRilCdmaSignalInfoRecordToHal(&callWaitingRil->signalInfoRecord,
                callWaitingRecord.signalInfoRecord);
        callWaitingRecord.numberType = (CdmaCallWaitingNumberType) callWaitingRil->number_type;
        callWaitingRecord.numberPlan = (CdmaCallWaitingNumberPlan) callWaitingRil->number_plan;

        mtkLogD(LOG_TAG, "cdmaCallWaitingInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaCallWaiting(
                convertIntToRadioIndicationType(indicationType), callWaitingRecord);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaCallWaitingInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::cdmaOtaProvisionStatusInd(int slotId,
                                     int indicationType, int token, RIL_Errno e, void *response,
                                     size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "cdmaOtaProvisionStatusInd: invalid response");
            return 0;
        }
        int32_t status = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "cdmaOtaProvisionStatusInd: status %d", status);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaOtaProvisionStatus(
                convertIntToRadioIndicationType(indicationType), (CdmaOtaProvisionStatus) status);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaOtaProvisionStatusInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::cdmaInfoRecInd(int slotId,
                          int indicationType, int token, RIL_Errno e, void *response,
                          size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_CDMA_InformationRecords)) {
            mtkLogE(LOG_TAG, "cdmaInfoRecInd: invalid response");
            return 0;
        }

        CdmaInformationRecords records = {};
        RIL_CDMA_InformationRecords *recordsRil = (RIL_CDMA_InformationRecords *) response;

        char* string8 = NULL;
        int num = MIN(recordsRil->numberOfInfoRecs, RIL_CDMA_MAX_NUMBER_OF_INFO_RECS);
        if (recordsRil->numberOfInfoRecs > RIL_CDMA_MAX_NUMBER_OF_INFO_RECS) {
            mtkLogE(LOG_TAG, "cdmaInfoRecInd: received %d recs which is more than %d, dropping "
                    "additional ones", recordsRil->numberOfInfoRecs,
                    RIL_CDMA_MAX_NUMBER_OF_INFO_RECS);
        }
        records.infoRec.resize(num);
        for (int i = 0 ; i < num ; i++) {
            CdmaInformationRecord *record = &records.infoRec[i];
            RIL_CDMA_InformationRecord *infoRec = &recordsRil->infoRec[i];
            record->name = (CdmaInfoRecName) infoRec->name;
            // All vectors should be size 0 except one which will be size 1. Set everything to
            // size 0 initially.
            record->display.resize(0);
            record->number.resize(0);
            record->signal.resize(0);
            record->redir.resize(0);
            record->lineCtrl.resize(0);
            record->clir.resize(0);
            record->audioCtrl.resize(0);
            switch (infoRec->name) {
                case RIL_CDMA_DISPLAY_INFO_REC:
                case RIL_CDMA_EXTENDED_DISPLAY_INFO_REC: {
                    if (infoRec->rec.display.alpha_len > CDMA_ALPHA_INFO_BUFFER_LENGTH) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: invalid display info response length %d "
                                "expected not more than %d", (int) infoRec->rec.display.alpha_len,
                                CDMA_ALPHA_INFO_BUFFER_LENGTH);
                        return 0;
                    }
                    string8 = (char*) malloc((infoRec->rec.display.alpha_len + 1) * sizeof(char));
                    if (string8 == NULL) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: Memory allocation failed for "
                                "responseCdmaInformationRecords");
                        return 0;
                    }
                    memcpy(string8, infoRec->rec.display.alpha_buf, infoRec->rec.display.alpha_len);
                    string8[(int)infoRec->rec.display.alpha_len] = '\0';

                    record->display.resize(1);
                    record->display[0].alphaBuf = string8;
                    free(string8);
                    string8 = NULL;
                    break;
                }

                case RIL_CDMA_CALLED_PARTY_NUMBER_INFO_REC:
                case RIL_CDMA_CALLING_PARTY_NUMBER_INFO_REC:
                case RIL_CDMA_CONNECTED_NUMBER_INFO_REC: {
                    if (infoRec->rec.number.len > CDMA_NUMBER_INFO_BUFFER_LENGTH) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: invalid display info response length %d "
                                "expected not more than %d", (int) infoRec->rec.number.len,
                                CDMA_NUMBER_INFO_BUFFER_LENGTH);
                        return 0;
                    }
                    string8 = (char*) malloc((infoRec->rec.number.len + 1) * sizeof(char));
                    if (string8 == NULL) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: Memory allocation failed for "
                                "responseCdmaInformationRecords");
                        return 0;
                    }
                    memcpy(string8, infoRec->rec.number.buf, infoRec->rec.number.len);
                    string8[(int)infoRec->rec.number.len] = '\0';

                    record->number.resize(1);
                    record->number[0].number = string8;
                    free(string8);
                    string8 = NULL;
                    record->number[0].numberType = infoRec->rec.number.number_type;
                    record->number[0].numberPlan = infoRec->rec.number.number_plan;
                    record->number[0].pi = infoRec->rec.number.pi;
                    record->number[0].si = infoRec->rec.number.si;
                    break;
                }

                case RIL_CDMA_SIGNAL_INFO_REC: {
                    record->signal.resize(1);
                    record->signal[0].isPresent = infoRec->rec.signal.isPresent;
                    record->signal[0].signalType = infoRec->rec.signal.signalType;
                    record->signal[0].alertPitch = infoRec->rec.signal.alertPitch;
                    record->signal[0].signal = infoRec->rec.signal.signal;
                    break;
                }

                case RIL_CDMA_REDIRECTING_NUMBER_INFO_REC: {
                    if (infoRec->rec.redir.redirectingNumber.len >
                                                  CDMA_NUMBER_INFO_BUFFER_LENGTH) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: invalid display info response length %d "
                                "expected not more than %d\n",
                                (int)infoRec->rec.redir.redirectingNumber.len,
                                CDMA_NUMBER_INFO_BUFFER_LENGTH);
                        return 0;
                    }
                    string8 = (char*) malloc((infoRec->rec.redir.redirectingNumber.len + 1) *
                            sizeof(char));
                    if (string8 == NULL) {
                        mtkLogE(LOG_TAG, "cdmaInfoRecInd: Memory allocation failed for "
                                "responseCdmaInformationRecords");
                        return 0;
                    }
                    memcpy(string8, infoRec->rec.redir.redirectingNumber.buf,
                            infoRec->rec.redir.redirectingNumber.len);
                    string8[(int)infoRec->rec.redir.redirectingNumber.len] = '\0';

                    record->redir.resize(1);
                    record->redir[0].redirectingNumber.number = string8;
                    free(string8);
                    string8 = NULL;
                    record->redir[0].redirectingNumber.numberType =
                            infoRec->rec.redir.redirectingNumber.number_type;
                    record->redir[0].redirectingNumber.numberPlan =
                            infoRec->rec.redir.redirectingNumber.number_plan;
                    record->redir[0].redirectingNumber.pi = infoRec->rec.redir.redirectingNumber.pi;
                    record->redir[0].redirectingNumber.si = infoRec->rec.redir.redirectingNumber.si;
                    record->redir[0].redirectingReason =
                            (CdmaRedirectingReason) infoRec->rec.redir.redirectingReason;
                    break;
                }

                case RIL_CDMA_LINE_CONTROL_INFO_REC: {
                    record->lineCtrl.resize(1);
                    record->lineCtrl[0].lineCtrlPolarityIncluded =
                            infoRec->rec.lineCtrl.lineCtrlPolarityIncluded;
                    record->lineCtrl[0].lineCtrlToggle = infoRec->rec.lineCtrl.lineCtrlToggle;
                    record->lineCtrl[0].lineCtrlReverse = infoRec->rec.lineCtrl.lineCtrlReverse;
                    record->lineCtrl[0].lineCtrlPowerDenial =
                            infoRec->rec.lineCtrl.lineCtrlPowerDenial;
                    break;
                }

                case RIL_CDMA_T53_CLIR_INFO_REC: {
                    record->clir.resize(1);
                    record->clir[0].cause = infoRec->rec.clir.cause;
                    break;
                }

                case RIL_CDMA_T53_AUDIO_CONTROL_INFO_REC: {
                    record->audioCtrl.resize(1);
                    record->audioCtrl[0].upLink = infoRec->rec.audioCtrl.upLink;
                    record->audioCtrl[0].downLink = infoRec->rec.audioCtrl.downLink;
                    break;
                }

                case RIL_CDMA_T53_RELEASE_INFO_REC:
                    mtkLogE(LOG_TAG, "cdmaInfoRecInd: RIL_CDMA_T53_RELEASE_INFO_REC: INVALID");
                    return 0;

                default:
                    mtkLogE(LOG_TAG, "cdmaInfoRecInd: Incorrect name value");
                    return 0;
            }
        }

        mtkLogD(LOG_TAG, "cdmaInfoRecInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaInfoRec(
                convertIntToRadioIndicationType(indicationType), records);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaInfoRecInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::indicateRingbackToneInd(int slotId,
                                   int indicationType, int token, RIL_Errno e, void *response,
                                   size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "indicateRingbackToneInd: invalid response");
            return 0;
        }
        bool start = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "indicateRingbackToneInd: start %d", start);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->indicateRingbackTone(
                convertIntToRadioIndicationType(indicationType), start);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "indicateRingbackToneInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::resendIncallMuteInd(int slotId,
                               int indicationType, int token, RIL_Errno e, void *response,
                               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "resendIncallMuteInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->resendIncallMute(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resendIncallMuteInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::cdmaSubscriptionSourceChangedInd(int slotId,
                                            int indicationType, int token, RIL_Errno e,
                                            void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "cdmaSubscriptionSourceChangedInd: invalid response");
            return 0;
        }
        int32_t cdmaSource = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "cdmaSubscriptionSourceChangedInd: cdmaSource %d", cdmaSource);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->
                cdmaSubscriptionSourceChanged(convertIntToRadioIndicationType(indicationType),
                (CdmaSubscriptionSource) cdmaSource);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaSubscriptionSourceChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::cdmaPrlChangedInd(int slotId,
                             int indicationType, int token, RIL_Errno e, void *response,
                             size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "cdmaPrlChangedInd: invalid response");
            return 0;
        }
        int32_t version = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "cdmaPrlChangedInd: version %d", version);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cdmaPrlChanged(
                convertIntToRadioIndicationType(indicationType), version);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "cdmaPrlChangedInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::exitEmergencyCallbackModeInd(int slotId,
                                        int indicationType, int token, RIL_Errno e, void *response,
                                        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "exitEmergencyCallbackModeInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->exitEmergencyCallbackMode(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "exitEmergencyCallbackModeInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }
    // sync the exit EmergencyCallbackModeInd to IMS
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationIms->
                                 exitEmergencyCallbackMode(
                                 convertIntToRadioIndicationType(indicationType));
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "exitEmergencyCallbackModeInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::noEmergencyCallbackModeInd(int slotId,
                                        int indicationType, int token, RIL_Errno e, void *response,
                                        size_t responseLen) {

    // Currently only sync noEmergencyCallbackModeInd to IMS
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationImsV3_1 != NULL) {
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationImsV3_1->
                                 noEmergencyCallbackMode(
                                 convertIntToRadioIndicationType(indicationType));
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "noEmergencyCallbackModeInd: radioService[%d]->mRadioIndicationImsV3_1 == NULL",
                                                                            imsSlotId);
    }

    return 0;
}


int radio::rilConnectedInd(int slotId,
                           int indicationType, int token, RIL_Errno e, void *response,
                           size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "rilConnectedIndMtk");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->rilConnected(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "rilConnectedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->rilConnected(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "rilConnectedInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    // hangup all if the Phone process disconnected to sync the AP / MD state
    if (radioService[slotId] != NULL) {
        radioService[slotId]->hangupAll(0);
    }
    return 0;
}

int radio::voiceRadioTechChangedInd(int slotId,
                                    int indicationType, int token, RIL_Errno e, void *response,
                                    size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "voiceRadioTechChangedInd: invalid response");
            return 0;
        }
        int32_t rat = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "voiceRadioTechChangedInd: rat %d", rat);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->voiceRadioTechChanged(
                convertIntToRadioIndicationType(indicationType), (RadioTechnology) rat);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "voiceRadioTechChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

char* convertRilCellInfoListToHal(void *response, size_t responseLen, hidl_vec<CellInfo>& records) {
    int num = responseLen / sizeof(RIL_CellInfo_v12);
    records.resize(num);

    char* mMncs = (char*) calloc(num * 6 , sizeof(char));
    memset(mMncs, 0, num * sizeof(char) * 6);
    RIL_CellInfo_v12 *rillCellInfo = (RIL_CellInfo_v12 *) response;
    for (int i = 0; i < num; i++) {
        records[i].cellInfoType = (CellInfoType) rillCellInfo->cellInfoType;
        records[i].registered = rillCellInfo->registered;
        records[i].timeStampType = (TimeStampType) rillCellInfo->timeStampType;
        records[i].timeStamp = rillCellInfo->timeStamp;
        // All vectors should be size 0 except one which will be size 1. Set everything to
        // size 0 initially.
        records[i].gsm.resize(0);
        records[i].wcdma.resize(0);
        records[i].cdma.resize(0);
        records[i].lte.resize(0);
        records[i].tdscdma.resize(0);
        switch(rillCellInfo->cellInfoType) {
            case RIL_CELL_INFO_TYPE_GSM: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].gsm.resize(1);
                CellInfoGsm *cellInfoGsm = &records[i].gsm[0];
                cellInfoGsm->cellIdentityGsm.mcc =
                        std::to_string(rillCellInfo->CellInfo.gsm.cellIdentityGsm.mcc);
                if (rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc_len == 2) {
                    snprintf(mMnc, 4, "%02d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                } else if (rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc_len == 3) {
                    snprintf(mMnc, 4, "%03d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                } else {
                    snprintf(mMnc, 4, "%d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                }
                cellInfoGsm->cellIdentityGsm.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoGsm->cellIdentityGsm.lac =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.lac;
                cellInfoGsm->cellIdentityGsm.cid =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.cid;
                cellInfoGsm->cellIdentityGsm.arfcn =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.arfcn;
                cellInfoGsm->cellIdentityGsm.bsic =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.bsic;
                cellInfoGsm->signalStrengthGsm.signalStrength =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.signalStrength;
                cellInfoGsm->signalStrengthGsm.bitErrorRate =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.bitErrorRate;
                cellInfoGsm->signalStrengthGsm.timingAdvance =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.timingAdvance;
                break;
            }

            case RIL_CELL_INFO_TYPE_WCDMA: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].wcdma.resize(1);
                CellInfoWcdma *cellInfoWcdma = &records[i].wcdma[0];
                cellInfoWcdma->cellIdentityWcdma.mcc =
                        std::to_string(rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mcc);
                if (rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc_len == 2) {
                    snprintf(mMnc, 4, "%02d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                } else if (rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc_len == 3) {
                    snprintf(mMnc, 4, "%03d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                } else {
                    snprintf(mMnc, 4, "%d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                }
                cellInfoWcdma->cellIdentityWcdma.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoWcdma->cellIdentityWcdma.lac =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.lac;
                cellInfoWcdma->cellIdentityWcdma.cid =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.cid;
                cellInfoWcdma->cellIdentityWcdma.psc =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.psc;
                cellInfoWcdma->cellIdentityWcdma.uarfcn =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.uarfcn;
                cellInfoWcdma->signalStrengthWcdma.signalStrength =
                        rillCellInfo->CellInfo.wcdma.signalStrengthWcdma.signalStrength;
                cellInfoWcdma->signalStrengthWcdma.bitErrorRate =
                        rillCellInfo->CellInfo.wcdma.signalStrengthWcdma.bitErrorRate;
                break;
            }

            case RIL_CELL_INFO_TYPE_CDMA: {
                records[i].cdma.resize(1);
                CellInfoCdma *cellInfoCdma = &records[i].cdma[0];
                cellInfoCdma->cellIdentityCdma.networkId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.networkId;
                cellInfoCdma->cellIdentityCdma.systemId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.systemId;
                cellInfoCdma->cellIdentityCdma.baseStationId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.basestationId;
                cellInfoCdma->cellIdentityCdma.longitude =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.longitude;
                cellInfoCdma->cellIdentityCdma.latitude =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.latitude;
                cellInfoCdma->signalStrengthCdma.dbm =
                        rillCellInfo->CellInfo.cdma.signalStrengthCdma.dbm;
                cellInfoCdma->signalStrengthCdma.ecio =
                        rillCellInfo->CellInfo.cdma.signalStrengthCdma.ecio;
                cellInfoCdma->signalStrengthEvdo.dbm =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.dbm;
                cellInfoCdma->signalStrengthEvdo.ecio =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.ecio;
                cellInfoCdma->signalStrengthEvdo.signalNoiseRatio =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.signalNoiseRatio;
                break;
            }

            case RIL_CELL_INFO_TYPE_LTE: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].lte.resize(1);
                CellInfoLte *cellInfoLte = &records[i].lte[0];
                cellInfoLte->cellIdentityLte.mcc =
                        std::to_string(rillCellInfo->CellInfo.lte.cellIdentityLte.mcc);
                if (rillCellInfo->CellInfo.lte.cellIdentityLte.mnc_len == 2) {
                    snprintf(mMnc, 4, "%02d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                } else if (rillCellInfo->CellInfo.lte.cellIdentityLte.mnc_len == 3) {
                    snprintf(mMnc, 4, "%03d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                } else {
                    snprintf(mMnc, 4, "%d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                }
                cellInfoLte->cellIdentityLte.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoLte->cellIdentityLte.ci =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.ci;
                cellInfoLte->cellIdentityLte.pci =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.pci;
                cellInfoLte->cellIdentityLte.tac =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.tac;
                cellInfoLte->cellIdentityLte.earfcn =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.earfcn;
                cellInfoLte->signalStrengthLte.signalStrength =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.signalStrength;
                cellInfoLte->signalStrengthLte.rsrp =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rsrp;
                cellInfoLte->signalStrengthLte.rsrq =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rsrq;
                cellInfoLte->signalStrengthLte.rssnr =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rssnr;
                cellInfoLte->signalStrengthLte.cqi =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.cqi;
                cellInfoLte->signalStrengthLte.timingAdvance =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.timingAdvance;
                break;
            }

            case RIL_CELL_INFO_TYPE_TD_SCDMA: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].tdscdma.resize(1);
                CellInfoTdscdma *cellInfoTdscdma = &records[i].tdscdma[0];
                cellInfoTdscdma->cellIdentityTdscdma.mcc =
                        std::to_string(rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mcc);
                if (rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc_len == 2) {
                    snprintf(mMnc, 4, "%02d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                } else if (rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc_len == 3) {
                    snprintf(mMnc, 4, "%03d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                } else {
                    snprintf(mMnc, 4, "%d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                }
                cellInfoTdscdma->cellIdentityTdscdma.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoTdscdma->cellIdentityTdscdma.lac =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.lac;
                cellInfoTdscdma->cellIdentityTdscdma.cid =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.cid;
                cellInfoTdscdma->cellIdentityTdscdma.cpid =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.cpid;
                cellInfoTdscdma->signalStrengthTdscdma.rscp =
                        rillCellInfo->CellInfo.tdscdma.signalStrengthTdscdma.rscp;
                break;
            }
            default: {
                break;
            }
        }
        rillCellInfo += 1;
    }
    return mMncs;
}

char* convertRilCellInfoListToHal_1_2(void *response, size_t responseLen,
            hidl_vec<AOSP_V1_2::CellInfo>& records) {
    int num = responseLen / sizeof(RIL_CellInfo_v12);
    records.resize(num);

    char* mMncs = (char*) calloc(num * 6 , sizeof(char));
    memset(mMncs, 0, num * sizeof(char) * 6);
    RIL_CellInfo_v12 *rillCellInfo = (RIL_CellInfo_v12 *) response;
    for (int i = 0; i < num; i++) {
        records[i].cellInfoType = (CellInfoType) rillCellInfo->cellInfoType;
        records[i].registered = rillCellInfo->registered;
        records[i].timeStampType = (TimeStampType) rillCellInfo->timeStampType;
        records[i].timeStamp = rillCellInfo->timeStamp;
        records[i].connectionStatus =
                (AOSP_V1_2::CellConnectionStatus) rillCellInfo->connectionStatus;
        // All vectors should be size 0 except one which will be size 1. Set everything to
        // size 0 initially.
        records[i].gsm.resize(0);
        records[i].wcdma.resize(0);
        records[i].cdma.resize(0);
        records[i].lte.resize(0);
        records[i].tdscdma.resize(0);
        switch (rillCellInfo->cellInfoType) {
            case RIL_CELL_INFO_TYPE_GSM: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].gsm.resize(1);
                AOSP_V1_2::CellInfoGsm *cellInfoGsm = &records[i].gsm[0];
                cellInfoGsm->cellIdentityGsm.base.mcc =
                        std::to_string(rillCellInfo->CellInfo.gsm.cellIdentityGsm.mcc);
                if (rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc_len == 2) {
                    snprintf(mMnc, 5, "%02d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                } else if (rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc_len == 3) {
                    snprintf(mMnc, 5, "%03d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                } else {
                    snprintf(mMnc, 5, "%d", rillCellInfo->CellInfo.gsm.cellIdentityGsm.mnc);
                }
                cellInfoGsm->cellIdentityGsm.base.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoGsm->cellIdentityGsm.base.lac =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.lac;
                cellInfoGsm->cellIdentityGsm.base.cid =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.cid;
                cellInfoGsm->cellIdentityGsm.base.arfcn =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.arfcn;
                cellInfoGsm->cellIdentityGsm.base.bsic =
                        rillCellInfo->CellInfo.gsm.cellIdentityGsm.bsic;
                cellInfoGsm->cellIdentityGsm.operatorNames.alphaLong =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.gsm.cellIdentityGsm.operName.long_name);
                cellInfoGsm->cellIdentityGsm.operatorNames.alphaShort =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.gsm.cellIdentityGsm.operName.short_name);

                cellInfoGsm->signalStrengthGsm.signalStrength =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.signalStrength;
                cellInfoGsm->signalStrengthGsm.bitErrorRate =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.bitErrorRate;
                cellInfoGsm->signalStrengthGsm.timingAdvance =
                        rillCellInfo->CellInfo.gsm.signalStrengthGsm.timingAdvance;
                break;
            }

            case RIL_CELL_INFO_TYPE_WCDMA: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].wcdma.resize(1);
                AOSP_V1_2::CellInfoWcdma *cellInfoWcdma = &records[i].wcdma[0];
                cellInfoWcdma->cellIdentityWcdma.base.mcc =
                        std::to_string(rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mcc);
                if (rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc_len == 2) {
                    snprintf(mMnc, 5, "%02d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                } else if (rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc_len == 3) {
                    snprintf(mMnc, 5, "%03d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                } else {
                    snprintf(mMnc, 5, "%d", rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.mnc);
                }
                cellInfoWcdma->cellIdentityWcdma.base.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoWcdma->cellIdentityWcdma.base.lac =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.lac;
                cellInfoWcdma->cellIdentityWcdma.base.cid =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.cid;
                cellInfoWcdma->cellIdentityWcdma.base.psc =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.psc;
                cellInfoWcdma->cellIdentityWcdma.base.uarfcn =
                        rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.uarfcn;
                cellInfoWcdma->cellIdentityWcdma.operatorNames.alphaLong =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.operName.long_name);
                cellInfoWcdma->cellIdentityWcdma.operatorNames.alphaShort =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.wcdma.cellIdentityWcdma.operName.short_name);

                cellInfoWcdma->signalStrengthWcdma.base.signalStrength =
                        rillCellInfo->CellInfo.wcdma.signalStrengthWcdma.signalStrength;
                cellInfoWcdma->signalStrengthWcdma.base.bitErrorRate =
                        rillCellInfo->CellInfo.wcdma.signalStrengthWcdma.bitErrorRate;
                break;
            }

            case RIL_CELL_INFO_TYPE_CDMA: {
                records[i].cdma.resize(1);
                AOSP_V1_2::CellInfoCdma *cellInfoCdma = &records[i].cdma[0];
                cellInfoCdma->cellIdentityCdma.base.networkId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.networkId;
                cellInfoCdma->cellIdentityCdma.base.systemId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.systemId;
                cellInfoCdma->cellIdentityCdma.base.baseStationId =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.basestationId;
                cellInfoCdma->cellIdentityCdma.base.longitude =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.longitude;
                cellInfoCdma->cellIdentityCdma.base.latitude =
                        rillCellInfo->CellInfo.cdma.cellIdentityCdma.latitude;
                /* long_name and short_name are NULL
                cellInfoGsm->cellIdentityGsm.operNames.alphaLong =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.gsm.cellIdentityGsm.operName.long_name);
                cellInfoGsm->cellIdentityGsm.operNames.alphaShort =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.gsm.cellIdentityGsm.operName.short_name);
                */
                cellInfoCdma->signalStrengthCdma.dbm =
                        rillCellInfo->CellInfo.cdma.signalStrengthCdma.dbm;
                cellInfoCdma->signalStrengthCdma.ecio =
                        rillCellInfo->CellInfo.cdma.signalStrengthCdma.ecio;
                cellInfoCdma->signalStrengthEvdo.dbm =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.dbm;
                cellInfoCdma->signalStrengthEvdo.ecio =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.ecio;
                cellInfoCdma->signalStrengthEvdo.signalNoiseRatio =
                        rillCellInfo->CellInfo.cdma.signalStrengthEvdo.signalNoiseRatio;
                break;
            }

            case RIL_CELL_INFO_TYPE_LTE: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].lte.resize(1);
                AOSP_V1_2::CellInfoLte *cellInfoLte = &records[i].lte[0];
                cellInfoLte->cellIdentityLte.base.mcc =
                        std::to_string(rillCellInfo->CellInfo.lte.cellIdentityLte.mcc);
                if (rillCellInfo->CellInfo.lte.cellIdentityLte.mnc_len == 2) {
                    snprintf(mMnc, 5, "%02d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                } else if (rillCellInfo->CellInfo.lte.cellIdentityLte.mnc_len == 3) {
                    snprintf(mMnc, 5, "%03d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                } else {
                    snprintf(mMnc, 5, "%d", rillCellInfo->CellInfo.lte.cellIdentityLte.mnc);
                }
                cellInfoLte->cellIdentityLte.base.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoLte->cellIdentityLte.base.ci =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.ci;
                cellInfoLte->cellIdentityLte.base.pci =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.pci;
                cellInfoLte->cellIdentityLte.base.tac =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.tac;
                cellInfoLte->cellIdentityLte.base.earfcn =
                        rillCellInfo->CellInfo.lte.cellIdentityLte.earfcn;
                cellInfoLte->cellIdentityLte.operatorNames.alphaLong =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.lte.cellIdentityLte.operName.long_name);
                cellInfoLte->cellIdentityLte.operatorNames.alphaShort =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.lte.cellIdentityLte.operName.short_name);
                cellInfoLte->cellIdentityLte.bandwidth = 0x7FFFFFFF;
                cellInfoLte->signalStrengthLte.signalStrength =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.signalStrength;
                cellInfoLte->signalStrengthLte.rsrp =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rsrp;
                cellInfoLte->signalStrengthLte.rsrq =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rsrq;
                cellInfoLte->signalStrengthLte.rssnr =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.rssnr;
                cellInfoLte->signalStrengthLte.cqi =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.cqi;
                cellInfoLte->signalStrengthLte.timingAdvance =
                        rillCellInfo->CellInfo.lte.signalStrengthLte.timingAdvance;
                break;
            }

            case RIL_CELL_INFO_TYPE_TD_SCDMA: {
                char* mMnc = &(mMncs[i * 6]);
                records[i].tdscdma.resize(1);
                AOSP_V1_2::CellInfoTdscdma *cellInfoTdscdma = &records[i].tdscdma[0];
                cellInfoTdscdma->cellIdentityTdscdma.base.mcc =
                        std::to_string(rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mcc);
                if (rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc_len == 2) {
                    snprintf(mMnc, 5, "%02d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                } else if (rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc_len == 3) {
                    snprintf(mMnc, 5, "%03d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                } else {
                    snprintf(mMnc, 5, "%d", rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.mnc);
                }
                cellInfoTdscdma->cellIdentityTdscdma.base.mnc = convertCharPtrToHidlString(mMnc);
                cellInfoTdscdma->cellIdentityTdscdma.base.lac =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.lac;
                cellInfoTdscdma->cellIdentityTdscdma.base.cid =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.cid;
                cellInfoTdscdma->cellIdentityTdscdma.base.cpid =
                        rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.cpid;
                cellInfoTdscdma->cellIdentityTdscdma.operatorNames.alphaLong =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.operName.long_name);
                cellInfoTdscdma->cellIdentityTdscdma.operatorNames.alphaShort =
                        convertCharPtrToHidlString(
                            rillCellInfo->CellInfo.tdscdma.cellIdentityTdscdma.operName.short_name);

                cellInfoTdscdma->signalStrengthTdscdma.rscp =
                        rillCellInfo->CellInfo.tdscdma.signalStrengthTdscdma.rscp;
                break;
            }
            default: {
                break;
            }
        }
        rillCellInfo += 1;
    }
    return mMncs;
}


int radio::cellInfoListInd(int slotId,
                           int indicationType, int token, RIL_Errno e, void *response,
                           size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_2!= NULL) {
        if (response == NULL || responseLen % sizeof(RIL_CellInfo_v12) != 0) {
            mtkLogE(LOG_TAG, "cellInfoListInd: invalid response");
            return 0;
        }

        hidl_vec<AOSP_V1_2::CellInfo> records;
        char* used_memory = NULL;
        used_memory = convertRilCellInfoListToHal_1_2(response, responseLen, records);

        mtkLogD(LOG_TAG, "cellInfoListInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationV1_2->cellInfoList_1_2(
                convertIntToRadioIndicationType(indicationType), records);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen % sizeof(RIL_CellInfo_v12) != 0) {
            mtkLogE(LOG_TAG, "cellInfoListInd: invalid response");
            return 0;
        }

        hidl_vec<CellInfo> records;
        char* used_memory = NULL;
        used_memory = convertRilCellInfoListToHal(response, responseLen, records);

        mtkLogD(LOG_TAG, "cellInfoListInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->cellInfoList(
                convertIntToRadioIndicationType(indicationType), records);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else {
        mtkLogE(LOG_TAG, "cellInfoListInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::imsNetworkStateChangedInd(int slotId,
                                     int indicationType, int token, RIL_Errno e, void *response,
                                     size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        mtkLogD(LOG_TAG, "imsNetworkStateChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->imsNetworkStateChanged(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsNetworkStateChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::subscriptionStatusChangedInd(int slotId,
                                        int indicationType, int token, RIL_Errno e, void *response,
                                        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "subscriptionStatusChangedInd: invalid response");
            return 0;
        }
        bool activate = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "subscriptionStatusChangedInd: activate %d", activate);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->subscriptionStatusChanged(
                convertIntToRadioIndicationType(indicationType), activate);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "subscriptionStatusChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::srvccStateNotifyInd(int slotId,
                               int indicationType, int token, RIL_Errno e, void *response,
                               size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "srvccStateNotifyInd: invalid response");
            return 0;
        }
        int32_t state = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "srvccStateNotifyInd: rat %d", state);
        Return<void> retStatus = radioService[slotId]->mRadioIndication->srvccStateNotify(
                convertIntToRadioIndicationType(indicationType), (SrvccState) state);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "srvccStateNotifyInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

void convertRilHardwareConfigListToHal(void *response, size_t responseLen,
        hidl_vec<HardwareConfig>& records) {
    int num = responseLen / sizeof(RIL_HardwareConfig);
    records.resize(num);

    RIL_HardwareConfig *rilHardwareConfig = (RIL_HardwareConfig *) response;
    for (int i = 0; i < num; i++) {
        records[i].type = (HardwareConfigType) rilHardwareConfig[i].type;
        records[i].uuid = convertCharPtrToHidlString(rilHardwareConfig[i].uuid);
        records[i].state = (HardwareConfigState) rilHardwareConfig[i].state;
        switch (rilHardwareConfig[i].type) {
            case RIL_HARDWARE_CONFIG_MODEM: {
                records[i].modem.resize(1);
                records[i].sim.resize(0);
                HardwareConfigModem *hwConfigModem = &records[i].modem[0];
                hwConfigModem->rat = rilHardwareConfig[i].cfg.modem.rat;
                hwConfigModem->maxVoice = rilHardwareConfig[i].cfg.modem.maxVoice;
                hwConfigModem->maxData = rilHardwareConfig[i].cfg.modem.maxData;
                hwConfigModem->maxStandby = rilHardwareConfig[i].cfg.modem.maxStandby;
                break;
            }

            case RIL_HARDWARE_CONFIG_SIM: {
                records[i].sim.resize(1);
                records[i].modem.resize(0);
                records[i].sim[0].modemUuid =
                        convertCharPtrToHidlString(rilHardwareConfig[i].cfg.sim.modemUuid);
                break;
            }
        }
    }
}

int radio::hardwareConfigChangedInd(int slotId,
                                    int indicationType, int token, RIL_Errno e, void *response,
                                    size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen % sizeof(RIL_HardwareConfig) != 0) {
            mtkLogE(LOG_TAG, "hardwareConfigChangedInd: invalid response");
            return 0;
        }

        hidl_vec<HardwareConfig> configs;
        convertRilHardwareConfigListToHal(response, responseLen, configs);

        mtkLogD(LOG_TAG, "hardwareConfigChangedInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->hardwareConfigChanged(
                convertIntToRadioIndicationType(indicationType), configs);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "hardwareConfigChangedInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

void convertRilRadioCapabilityToHal(void *response, size_t responseLen, RadioCapability& rc) {
    RIL_RadioCapability *rilRadioCapability = (RIL_RadioCapability *) response;
    rc.session = rilRadioCapability->session;
    rc.phase = (android::hardware::radio::V1_0::RadioCapabilityPhase) rilRadioCapability->phase;
    rc.raf = rilRadioCapability->rat;
    rc.logicalModemUuid = convertCharPtrToHidlString(rilRadioCapability->logicalModemUuid);
    rc.status = (android::hardware::radio::V1_0::RadioCapabilityStatus) rilRadioCapability->status;
}

int radio::radioCapabilityIndicationInd(int slotId,
                                        int indicationType, int token, RIL_Errno e, void *response,
                                        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_RadioCapability)) {
            mtkLogE(LOG_TAG, "radioCapabilityIndicationInd: invalid response");
            return 0;
        }

        RadioCapability rc = {};
        convertRilRadioCapabilityToHal(response, responseLen, rc);

        mtkLogD(LOG_TAG, "radioCapabilityIndicationInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->radioCapabilityIndication(
                convertIntToRadioIndicationType(indicationType), rc);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "radioCapabilityIndicationInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

bool isServiceTypeCfQuery(RIL_SsServiceType serType, RIL_SsRequestType reqType) {
    if ((reqType == SS_INTERROGATION) &&
        (serType == SS_CFU ||
         serType == SS_CF_BUSY ||
         serType == SS_CF_NO_REPLY ||
         serType == SS_CF_NOT_REACHABLE ||
         serType == SS_CF_ALL ||
         serType == SS_CF_ALL_CONDITIONAL)) {
        return true;
    }
    return false;
}

int radio::onSupplementaryServiceIndicationInd(int slotId,
                                               int indicationType, int token, RIL_Errno e,
                                               void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_StkCcUnsolSsResponse)) {
            mtkLogE(LOG_TAG, "onSupplementaryServiceIndicationInd: invalid response");
            return 0;
        }

        RIL_StkCcUnsolSsResponse *rilSsResponse = (RIL_StkCcUnsolSsResponse *) response;
        StkCcUnsolSsResult ss = {};
        ss.serviceType = (SsServiceType) rilSsResponse->serviceType;
        ss.requestType = (SsRequestType) rilSsResponse->requestType;
        ss.teleserviceType = (SsTeleserviceType) rilSsResponse->teleserviceType;
        ss.serviceClass = rilSsResponse->serviceClass;
        ss.result = (RadioError) rilSsResponse->result;

        if (isServiceTypeCfQuery(rilSsResponse->serviceType, rilSsResponse->requestType)) {
            mtkLogD(LOG_TAG, "onSupplementaryServiceIndicationInd CF type, num of Cf elements %d",
                    rilSsResponse->cfData.numValidIndexes);
            if (rilSsResponse->cfData.numValidIndexes > NUM_SERVICE_CLASSES) {
                mtkLogE(LOG_TAG, "onSupplementaryServiceIndicationInd numValidIndexes is greater than "
                        "max value %d, truncating it to max value", NUM_SERVICE_CLASSES);
                rilSsResponse->cfData.numValidIndexes = NUM_SERVICE_CLASSES;
            }

            ss.cfData.resize(1);
            ss.ssInfo.resize(0);

            /* number of call info's */
            ss.cfData[0].cfInfo.resize(rilSsResponse->cfData.numValidIndexes);

            for (int i = 0; i < rilSsResponse->cfData.numValidIndexes; i++) {
                 RIL_CallForwardInfo cf = rilSsResponse->cfData.cfInfo[i];
                 CallForwardInfo *cfInfo = &ss.cfData[0].cfInfo[i];

                 cfInfo->status = (CallForwardInfoStatus) cf.status;
                 cfInfo->reason = cf.reason;
                 cfInfo->serviceClass = cf.serviceClass;
                 cfInfo->toa = cf.toa;
                 cfInfo->number = convertCharPtrToHidlString(cf.number);
                 cfInfo->timeSeconds = cf.timeSeconds;
                 mtkLogD(LOG_TAG, "onSupplementaryServiceIndicationInd: "
                        "Data: %d,reason=%d,cls=%d,toa=%d,num=%s,tout=%d],", cf.status,
                        cf.reason, cf.serviceClass, cf.toa, (char*)cf.number, cf.timeSeconds);
            }
        } else {
            ss.ssInfo.resize(1);
            ss.cfData.resize(0);

            /* each int */
            ss.ssInfo[0].ssInfo.resize(SS_INFO_MAX);
            for (int i = 0; i < SS_INFO_MAX; i++) {
                 mtkLogD(LOG_TAG, "onSupplementaryServiceIndicationInd: Data: %d",
                        rilSsResponse->ssInfo[i]);
                 ss.ssInfo[0].ssInfo[i] = rilSsResponse->ssInfo[i];
            }
        }

        mtkLogD(LOG_TAG, "onSupplementaryServiceIndicationInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->
                onSupplementaryServiceIndication(convertIntToRadioIndicationType(indicationType),
                ss);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSupplementaryServiceIndicationInd: "
                "radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::stkCallControlAlphaNotifyInd(int slotId,
                                        int indicationType, int token, RIL_Errno e, void *response,
                                        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "stkCallControlAlphaNotifyInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "stkCallControlAlphaNotifyInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->stkCallControlAlphaNotify(
                convertIntToRadioIndicationType(indicationType),
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "stkCallControlAlphaNotifyInd: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

void convertRilLceDataInfoToHal(void *response, size_t responseLen, LceDataInfo& lce) {
    RIL_LceDataInfo *rilLceDataInfo = (RIL_LceDataInfo *)response;
    lce.lastHopCapacityKbps = rilLceDataInfo->last_hop_capacity_kbps;
    lce.confidenceLevel = rilLceDataInfo->confidence_level;
    lce.lceSuspended = rilLceDataInfo->lce_suspended;
}

int radio::lceDataInd(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_LceDataInfo)) {
            mtkLogE(LOG_TAG, "lceDataInd: invalid response");
            return 0;
        }

        LceDataInfo lce = {};
        convertRilLceDataInfoToHal(response, responseLen, lce);
        mtkLogD(LOG_TAG, "lceDataInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->lceData(
                convertIntToRadioIndicationType(indicationType), lce);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "lceDataInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::pcoDataInd(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_PCO_Data)) {
            mtkLogE(LOG_TAG, "pcoDataInd: invalid response");
            return 0;
        }

        PcoDataInfo pco = {};
        RIL_PCO_Data *rilPcoData = (RIL_PCO_Data *)response;
        pco.cid = rilPcoData->cid;
        pco.bearerProto = convertCharPtrToHidlString(rilPcoData->bearer_proto);
        pco.pcoId = rilPcoData->pco_id;
        pco.contents.setToExternal((uint8_t *) rilPcoData->contents, rilPcoData->contents_length);

        mtkLogD(LOG_TAG, "pcoDataInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->pcoData(
                convertIntToRadioIndicationType(indicationType), pco);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "pcoDataInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

int radio::modemResetInd(int slotId,
                         int indicationType, int token, RIL_Errno e, void *response,
                         size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "modemResetInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "modemResetInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndication->modemReset(
                convertIntToRadioIndicationType(indicationType),
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "modemResetInd: radioService[%d]->mRadioIndication == NULL", slotId);
    }

    return 0;
}

// It's also networkScanResultInd_1_2
int radio::networkScanResultInd(int slotId,
                         int indicationType, int token, RIL_Errno e, void *response,
                         size_t responseLen) {
    // networkScanResultInd_1_2
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_2!= NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "networkScanResultInd_1_2: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "networkScanResultInd_1_2");
        RIL_NetworkScanResult *networkScanResult = (RIL_NetworkScanResult *) response;

        AOSP_V1_2::NetworkScanResult result;
        // No AOSP_V1_2::ScanStatus
        char* used_memory = NULL;
        result.status = (AOSP_V1_1::ScanStatus) networkScanResult->status;
        result.error = (RadioError) e;
        used_memory = convertRilCellInfoListToHal_1_2(
                networkScanResult->network_infos,
                networkScanResult->network_infos_length * sizeof(RIL_CellInfo_v12),
                result.networkInfos);
        // Use mRadioIndicationMtk to update operator name for mvno/eons
        Return<void> retStatus;
        if (radioService[slotId]->mRadioIndicationMtk != NULL)
            retStatus = radioService[slotId]->mRadioIndicationMtk->networkScanResult_1_2(
                    convertIntToRadioIndicationType(indicationType), result);
        else
            retStatus = radioService[slotId]->mRadioIndicationV1_2->networkScanResult_1_2(
                    convertIntToRadioIndicationType(indicationType), result);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_1 != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "networkScanResultInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "networkScanResultInd");
        RIL_NetworkScanResult *networkScanResult = (RIL_NetworkScanResult *) response;

        char* used_memory = NULL;
        AOSP_V1_1::NetworkScanResult result;
        result.status = (AOSP_V1_1::ScanStatus) networkScanResult->status;
        result.error = (RadioError) e;
        used_memory = convertRilCellInfoListToHal(
                networkScanResult->network_infos,
                networkScanResult->network_infos_length * sizeof(RIL_CellInfo_v12),
                result.networkInfos);
        // Use mRadioIndicationMtk to update operator name for mvno/eons
        Return<void> retStatus;
        if (radioService[slotId]->mRadioIndicationMtk != NULL)
            retStatus = radioService[slotId]->mRadioIndicationMtk->networkScanResult(
                    convertIntToRadioIndicationType(indicationType), result);
        else
            retStatus = radioService[slotId]->mRadioIndicationV1_1->networkScanResult(
                    convertIntToRadioIndicationType(indicationType), result);
        radioService[slotId]->checkReturnStatus(retStatus);
        if (used_memory) free(used_memory);
        used_memory = NULL;
    } else {
        mtkLogE(LOG_TAG, "networkScanResultInd: radioService[%d]->mRadioIndicationV1_1 == NULL", slotId);
    }
    return 0;
}

int radio::oemHookRawInd(int slotId,
                         int indicationType, int token, RIL_Errno e, void *response,
                         size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "oemHookRawInd: invalid response");
            return 0;
        }

        hidl_vec<uint8_t> data;
        data.setToExternal((uint8_t *) response, responseLen);
        mtkLogD(LOG_TAG, "oemHookRawInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->oemHookRaw(
                convertIntToRadioIndicationType(indicationType), data);
        checkReturnStatus(slotId, retStatus, false);
    } else {
        mtkLogE(LOG_TAG, "oemHookRawInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

// ATCI Start
int radio::atciInd(int slotId,
                   int indicationType, int token, RIL_Errno e, void *response,
                   size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mAtciIndication != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "atciInd: invalid response");
            return 0;
        }

        hidl_vec<uint8_t> data;
        data.setToExternal((uint8_t *) response, responseLen);
        mtkLogD(LOG_TAG, "atciInd");
        Return<void> retStatus = radioService[slotId]->mAtciIndication->atciInd(
                convertIntToRadioIndicationType(indicationType), data);
        if (!retStatus.isOk()) {
            mtkLogE(LOG_TAG, "sendAtciResponse: unable to call indication callback");
            radioService[slotId]->mAtciResponse = NULL;
            radioService[slotId]->mAtciIndication = NULL;
        }
    } else {
        mtkLogE(LOG_TAG, "atciInd: radioService[%d]->mAtciIndication == NULL", slotId);
    }

    return 0;
}
// ATCI End

/// [IMS] Indication ////////////////////////////////////////////////////////////////////
int radio::callInfoIndicationInd(int slotId,
                                 int indicationType, int token, RIL_Errno e,
                                 void *response, size_t responseLen) {

    char **resp = (char **) response;
    int numStrings = responseLen / sizeof(char *);
    if(numStrings < 5) {
        mtkLogE(LOG_TAG, "callInfoIndicationInd: items length is invalid, slot = %d", slotId);
        return 0;
    }

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_vec<hidl_string> data;
        data.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            data[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogD(LOG_TAG, "callInfoIndicationInd:: %d: %s", i, resp[i]);
        }

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->callInfoIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 data);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "callInfoIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                          imsSlotId);
    }

    return 0;
}

int radio::econfResultIndicationInd(int slotId,
                                    int indicationType, int token, RIL_Errno e,
                                    void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 5) {
            mtkLogE(LOG_TAG, "econfResultIndicationInd: items length invalid, slotId = %d",
                                                                     imsSlotId);
            return 0;
        }

        hidl_string confCallId = convertCharPtrToHidlString(resp[0]);
        hidl_string op = convertCharPtrToHidlString(resp[1]);
        hidl_string num = convertCharPtrToHidlString(resp[2]);
        hidl_string result = convertCharPtrToHidlString(resp[3]);
        hidl_string cause = convertCharPtrToHidlString(resp[4]);
        hidl_string joinedCallId;
        if(numStrings > 5) {
            joinedCallId = convertCharPtrToHidlString(resp[5]);
        }

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->econfResultIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 confCallId, op, num, result, cause, joinedCallId);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "econfResultIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                             imsSlotId);
    }

    return 0;
}

int radio::sipCallProgressIndicatorInd(int slotId,
                                       int indicationType, int token, RIL_Errno e,
                                       void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 5) {
            mtkLogE(LOG_TAG, "sipCallProgressIndicatorInd: items length invalid, slotId = %d",
                                                                        imsSlotId);
            return 0;
        }

        hidl_string callId = convertCharPtrToHidlString(resp[0]);
        hidl_string dir = convertCharPtrToHidlString(resp[1]);
        hidl_string sipMsgType = convertCharPtrToHidlString(resp[2]);
        hidl_string method = convertCharPtrToHidlString(resp[3]);
        hidl_string responseCode = convertCharPtrToHidlString(resp[4]);
        hidl_string reasonText;
        if(numStrings > 5) {
            reasonText = convertCharPtrToHidlString(resp[5]);
        }

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->sipCallProgressIndicator(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, dir, sipMsgType, method, responseCode, reasonText);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "sipCallProgressIndicatorInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                                imsSlotId);
    }

    return 0;
}

int radio::callmodChangeIndicatorInd(int slotId,
                                    int indicationType, int token, RIL_Errno e,
                                    void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 5) {
            mtkLogE(LOG_TAG, "callmodChangeIndicatorInd: items length invalid, slotId = %d",
                                                                       imsSlotId);
            return 0;
        }

        hidl_string callId = convertCharPtrToHidlString(resp[0]);
        hidl_string callMode = convertCharPtrToHidlString(resp[1]);
        hidl_string videoState = convertCharPtrToHidlString(resp[2]);
        hidl_string autoDirection = convertCharPtrToHidlString(resp[3]);
        hidl_string pau = convertCharPtrToHidlString(resp[4]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->callmodChangeIndicator(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, callMode, videoState, autoDirection, pau);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "callmodChangeIndicatorInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                              imsSlotId);
    }

    return 0;
}

int radio::videoCapabilityIndicatorInd(int slotId,
                                       int indicationType, int token, RIL_Errno e,
                                       void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 3) {
            mtkLogE(LOG_TAG, "videoCapabilityIndicatorInd: items length invalid, slotId = %d",
                                                                        imsSlotId);
            return 0;
        }

        hidl_string callId = convertCharPtrToHidlString(resp[0]);
        hidl_string localVideoCaoability = convertCharPtrToHidlString(resp[1]);
        hidl_string remoteVideoCaoability = convertCharPtrToHidlString(resp[2]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->videoCapabilityIndicator(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, localVideoCaoability, remoteVideoCaoability);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "videoCapabilityIndicatorInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                                imsSlotId);
    }

    return 0;
}

int radio::onUssiInd(int slotId,
                     int indicationType, int token, RIL_Errno e, void *response,
                     size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 7) {
            mtkLogE(LOG_TAG, "onUssiInd: items length invalid, slotId = %d",
                                                      imsSlotId);
            return 0;
        }

        hidl_string clazz = convertCharPtrToHidlString(resp[0]);
        hidl_string status = convertCharPtrToHidlString(resp[1]);
        hidl_string str = convertCharPtrToHidlString(resp[2]);
        hidl_string lang = convertCharPtrToHidlString(resp[3]);
        hidl_string errorCode = convertCharPtrToHidlString(resp[4]);
        hidl_string alertingPattern = convertCharPtrToHidlString(resp[5]);
        hidl_string sipCause = convertCharPtrToHidlString(resp[6]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->onUssi(
                                 convertIntToRadioIndicationType(indicationType),
                                 clazz, status, str, lang, errorCode,
                                 alertingPattern, sipCause);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    }
    else {
        mtkLogE(LOG_TAG, "onUssiInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                              imsSlotId);
    }

    return 0;
}

int radio::getProvisionDoneInd(int slotId,
                               int indicationType, int token, RIL_Errno e,
                               void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_string result1;
        hidl_string result2;
        int numStrings = responseLen / sizeof(char *);

        if (response == NULL || numStrings < 2) {
            mtkLogE(LOG_TAG, "getProvisionDone Invalid response: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            result1 = convertCharPtrToHidlString(resp[0]);
            result2 = convertCharPtrToHidlString(resp[1]);
        }

        mtkLogD(LOG_TAG, "getProvisionDoneInd");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->getProvisionDone(
                                 convertIntToRadioIndicationType(indicationType),
                                 result1, result2);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getProvisionDoneInd: radioService[%d]->mRadioIndicationMtk == NULL",
                                                                         imsSlotId);
    }

    return 0;
}

int radio::imsCfgDynamicImsSwitchCompleteInd(int slotId,
                                             int indicationType, int token, RIL_Errno e,
                                             void *response,
                                             size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
        && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        mtkLogD(LOG_TAG, "imsCfgDynamicImsSwitchCompleteInd");
        Return<void> retStatus = radioService[imsSlotId]->
                mRadioIndicationIms->imsCfgDynamicImsSwitchComplete(
                convertIntToRadioIndicationType(indicationType)
        );
    } else {
        mtkLogE(LOG_TAG,
                "imsCfgDynamicImsSwitchCompleteInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }
    return 0;
}

int radio::imsCfgFeatureChangedInd(int slotId,
                                   int indicationType, int token, RIL_Errno e, void *response,
                                   size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
        && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        int feature_id = ((int32_t *) response)[0];
        int value = ((int32_t *) response)[1];
        mtkLogD(LOG_TAG, "imsCfgFeatureChangedInd");
        Return<void> retStatus = radioService[imsSlotId]->
                mRadioIndicationIms->imsCfgFeatureChanged(
                convertIntToRadioIndicationType(indicationType), imsSlotId, feature_id, value
        );
    } else {
        mtkLogE(LOG_TAG,
                "imsCfgFeatureChangedInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }
    return 0;
}

int radio::imsCfgConfigChangedInd(int slotId,
                                   int indicationType, int token, RIL_Errno e, void *response,
                                   size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
        && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        char** data = (char**) response;
        hidl_string config_id;
        hidl_string value;
        config_id = convertCharPtrToHidlString(data[0]);
        value = convertCharPtrToHidlString(data[1]);
        mtkLogD(LOG_TAG, "imsCfgConfigChangedInd");
        Return<void> retStatus = radioService[imsSlotId]->
                mRadioIndicationIms->imsCfgConfigChanged(
                convertIntToRadioIndicationType(indicationType), imsSlotId, config_id, value
        );
    } else {
        mtkLogE(LOG_TAG,
                "imsCfgConfigChangedInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }
    return 0;
}

int radio::imsCfgConfigLoadedInd(int slotId,
                                 int indicationType, int token, RIL_Errno e,
                                 void *response,
                                 size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
        && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        mtkLogD(LOG_TAG, "imsCfgConfigLoadedInd");
        Return<void> retStatus = radioService[imsSlotId]->
                mRadioIndicationIms->imsCfgConfigLoaded(
                convertIntToRadioIndicationType(indicationType)
        );
    } else {
        mtkLogE(LOG_TAG,
                "imsCfgConfigLoadedInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }
    return 0;
}

int radio::imsRtpInfoInd(int slotId,
                         int indicationType, int token, RIL_Errno e, void *response,
                         size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_string pdnId;
        hidl_string networkId;
        hidl_string timer;
        hidl_string sendPktLost;
        hidl_string recvPktLost;
        int numStrings = responseLen / sizeof(char *);

        if (response == NULL || numStrings < 5) {
            mtkLogE(LOG_TAG, "imsRtpInfoInd Invalid response: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            pdnId = convertCharPtrToHidlString(resp[0]);
            networkId = convertCharPtrToHidlString(resp[1]);
            timer = convertCharPtrToHidlString(resp[2]);
            sendPktLost = convertCharPtrToHidlString(resp[3]);
            recvPktLost = convertCharPtrToHidlString(resp[4]);
        }

        mtkLogD(LOG_TAG, "imsRtpInfoInd");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->imsRtpInfo(
                                 convertIntToRadioIndicationType(indicationType),
                                 pdnId, networkId, timer, sendPktLost, recvPktLost);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsRtpInfoInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                   imsSlotId);
    }

    return 0;
}

int radio::onXuiInd(int slotId,
                    int indicationType, int token, RIL_Errno e, void *response,
                    size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_string accountId;
        hidl_string broadcastFlag;
        hidl_string xuiInfo;
        int numStrings = responseLen / sizeof(char *);

        if (response == NULL || numStrings < 3) {
            mtkLogE(LOG_TAG, "onXuiInd Invalid response: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            accountId = convertCharPtrToHidlString(resp[0]);
            broadcastFlag = convertCharPtrToHidlString(resp[1]);
            xuiInfo = convertCharPtrToHidlString(resp[2]);
        }

        mtkLogD(LOG_TAG, "onXuiInd");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->onXui(
                                 convertIntToRadioIndicationType(indicationType),
                                 accountId, broadcastFlag, xuiInfo);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onXuiInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                             imsSlotId);
    }

    return 0;
}

int radio::onVolteSubscription (int slotId, int indicationType, int token, RIL_Errno e,
                                void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen < sizeof(int)) {
            mtkLogE(LOG_TAG, "onVolteSubscription: invalid response");
            return 0;
        }

        int status = ((int32_t *) response)[0];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 onVolteSubscription(
                                 convertIntToRadioIndicationType(indicationType),
                                 status);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onVolteSubscription: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::imsEventPackageIndicationInd(int slotId,
                                        int indicationType, int token, RIL_Errno e,
                                        void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_string callId;
        hidl_string pType;
        hidl_string urcIdx;
        hidl_string totalUrcCount;
        hidl_string rawData;
        int numStrings = responseLen / sizeof(char *);

        if (response == NULL || numStrings < 5) {
            mtkLogE(LOG_TAG, "imsEventPackageIndication Invalid response: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            callId = convertCharPtrToHidlString(resp[0]);
            pType = convertCharPtrToHidlString(resp[1]);
            urcIdx = convertCharPtrToHidlString(resp[2]);
            totalUrcCount = convertCharPtrToHidlString(resp[3]);
            rawData = convertCharPtrToHidlString(resp[4]);
        }

        mtkLogD(LOG_TAG, "imsEventPackageIndication");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->imsEventPackageIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, pType, urcIdx, totalUrcCount, rawData);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEventPackageIndication: radioService[%d]->mRadioIndicationIms == NULL",
                                                                              imsSlotId);
    }

    return 0;
}

int radio::imsRegistrationInfoInd(int slotId,
                              int indicationType, int token, RIL_Errno e,
                              void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen < (sizeof(int) * 2)) {
            mtkLogE(LOG_TAG, "imsRegistrationInfoInd: invalid response");
            return 0;
        }

        int status = ((int32_t *) response)[0];
        int capacity = ((int32_t *) response)[1];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsRegistrationInfo(
                                 convertIntToRadioIndicationType(indicationType),
                                 status, capacity);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsRegistrationInfoInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::imsEnableDoneInd(int slotId,
                            int indicationType, int token, RIL_Errno e,
                            void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        mtkLogD(LOG_TAG, "imsEnableDoneInd");
        Return<void> retStatus =
                radioService[imsSlotId]->mRadioIndicationIms->imsEnableDone(
                convertIntToRadioIndicationType(indicationType));

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEnableDoneInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                     imsSlotId);
    }

    return 0;
}

int radio::imsDisableDoneInd(int slotId,
                             int indicationType, int token, RIL_Errno e,
                             void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        mtkLogD(LOG_TAG, "imsDisableDoneInd");
        Return<void> retStatus =
                radioService[imsSlotId]->mRadioIndicationIms->imsDisableDone(
                convertIntToRadioIndicationType(indicationType));

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDisableDoneInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                      imsSlotId);
    }

    return 0;
}

int radio::imsEnableStartInd(int slotId,
                             int type, int token, RIL_Errno e,
                             void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        mtkLogD(LOG_TAG, "imsEnableStartInd, slotId = %d, IMS slotId = %d", slotId, imsSlotId);
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsEnableStart(convertIntToRadioIndicationType(type));

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsEnableStartInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                      imsSlotId);
    }

    return 0;
}

int radio::imsDisableStartInd(int slotId,
                              int type, int token, RIL_Errno e,
                              void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        mtkLogD(LOG_TAG, "imsDisableStartInd, slotId = %d, IMS slotId = %d", slotId, imsSlotId);
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsDisableStart(convertIntToRadioIndicationType(type));

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDisableStartInd: radioService[%d]->mRadioIndicationMtk == NULL", imsSlotId);
    }

    return 0;
}

int radio::ectIndicationInd(int slotId,
                            int indicationType, int token, RIL_Errno e,
                            void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen < (sizeof(int) * 3)) {
            mtkLogE(LOG_TAG, "ectIndicationInd: invalid response");
            return 0;
        }

        int callId = ((int32_t *) response)[0];
        int ectResult = ((int32_t *) response)[1];
        int cause = ((int32_t *) response)[2];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 ectIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, ectResult, cause);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "ectIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                     imsSlotId);
    }

    return 0;
}

int radio::volteSettingInd(int slotId,
                           int indicationType, int token, RIL_Errno e,
                           void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen < (sizeof(int))) {
            mtkLogE(LOG_TAG, "volteSettingInd: invalid response");
            return 0;
        }

        int status = ((int32_t *) response)[0];
        bool isEnable = (status == 1) ? true : false;

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 volteSetting(
                                 convertIntToRadioIndicationType(indicationType),
                                 isEnable);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "volteSettingInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                    imsSlotId);
    }

    return 0;
}

int radio::imsBearerActivationInd(int slotId,
                                  int indicationType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {

    mtkLogD(LOG_TAG, "imsBearerActivationInd: serial %d", serial);

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen != sizeof(RIL_IMS_BearerNotification)) {
            mtkLogE(LOG_TAG, "imsBearerActivationInd: invalid response");
            return 0;
        }

        RIL_IMS_BearerNotification *p_cur = (RIL_IMS_BearerNotification *) response;
        int aid = p_cur->aid;
        hidl_string type = convertCharPtrToHidlString(p_cur->type);

        Return<void> retStatus =
                radioService[imsSlotId]->
                mRadioIndicationIms->
                imsBearerActivation(convertIntToRadioIndicationType(indicationType),
                                    aid, type);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsBearerActivationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::imsBearerDeactivationInd(int slotId,
                                    int indicationType, int serial, RIL_Errno e,
                                    void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    mtkLogD(LOG_TAG, "imsBearerDeactivationInd: serial %d", serial);

    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen != sizeof(RIL_IMS_BearerNotification)) {
            mtkLogE(LOG_TAG, "imsBearerDeactivationInd: invalid response");
            return 0;
        }

        RIL_IMS_BearerNotification *p_cur = (RIL_IMS_BearerNotification *) response;
        int aid = p_cur->aid;
        hidl_string type = convertCharPtrToHidlString(p_cur->type);

        Return<void> retStatus =
                radioService[imsSlotId]->
                mRadioIndicationIms->
                imsBearerDeactivation(convertIntToRadioIndicationType(indicationType),
                                      aid, type);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsBearerDeactivationInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                              imsSlotId);
    }

    return 0;
}

int radio::imsBearerInitInd(int slotId,
                            int indicationType, int serial, RIL_Errno e,
                            void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    mtkLogD(LOG_TAG, "imsBearerInitInd: serial %d", serial);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        Return<void> retStatus =
                radioService[imsSlotId]->
                mRadioIndicationIms->
                imsBearerInit(convertIntToRadioIndicationType(indicationType));

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsBearerInitInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                     imsSlotId);
    }

    return 0;
}

int radio::imsDataInfoNotifyInd(int slotId,
                                  int indicationType, int serial, RIL_Errno e,
                                  void *response, size_t responseLen) {

    mtkLogD(LOG_TAG, "imsDataInfoNotifyInd: serial %d", serial);

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen != sizeof(RIL_IMS_DataInfoNotify)) {
            mtkLogE(LOG_TAG, "imsDataInfoNotifyInd: invalid response");
            return 0;
        }

        RIL_IMS_DataInfoNotify *p_cur = (RIL_IMS_DataInfoNotify *) response;
        hidl_string type = convertCharPtrToHidlString(p_cur->type);
        hidl_string event = convertCharPtrToHidlString(p_cur->event);
        hidl_string extra = convertCharPtrToHidlString(p_cur->extra);

        Return<void> retStatus =
                radioService[imsSlotId]->
                mRadioIndicationIms->
                imsDataInfoNotify(convertIntToRadioIndicationType(indicationType),
                                    type, event, extra);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDataInfoNotifyInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::imsDeregDoneInd(int slotId,
                           int indicationType, int token, RIL_Errno e,
                           void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsDeregDone(
                                 convertIntToRadioIndicationType(indicationType));

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDeregDoneInd: radioService[%d]->mRadioIndicationIms = NULL",
                                                                   imsSlotId);
    }
    return 0;
}

int radio::confSRVCCInd(int slotId,
                        int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL &&
            radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "confSRVCCInd: invalid response");
            return 0;
        }
        hidl_vec<int32_t> data;
        int numInts = responseLen / sizeof(int);
        data.resize(numInts);
        int *pInt = (int *) response;

        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }

        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->confSRVCC(
                                 convertIntToRadioIndicationType(indicationType), data);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "confSRVCCInd: radioService[%d]->mRadioIndicationMtk = NULL", slotId);
    }

    return 0;
}

int radio::multiImsCountInd(int slotId,
          int indicationType, int token, RIL_Errno e,
          void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen < (sizeof(int))) {
            mtkLogE(LOG_TAG, "multiImsCountInd: invalid response");
            return 0;
        }

        int count = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 multiImsCount(
                                 convertIntToRadioIndicationType(indicationType),
                                 count);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDeregDoneInd: radioService[%d]->mRadioIndicationIms = NULL",
                                                                   imsSlotId);
    }
    return 0;
}

int radio::imsSupportEccInd(int slotId,
                     int indicationType, int token, RIL_Errno e,
                     void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL &&
            radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        if (response == NULL || responseLen < sizeof(int)) {
            mtkLogE(LOG_TAG, "imsSupportEccInd: invalid response");
            return 0;
        }

        int status = ((int32_t *) response)[0];

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsSupportEcc(
                                 convertIntToRadioIndicationType(indicationType),
                                 status);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsSupportEccInd: radioService[%d]->mRadioIndicationIms == NULL",
                                                                            imsSlotId);
    }

    return 0;
}

int radio::redialEmergencyIndication(int slotId, int indicationType, int token, RIL_Errno e,
        void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationImsV3_6 != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "redialEmergencyIndication: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "redialEmergencyIndication");

        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        if(numStrings < 1) {
            mtkLogE(LOG_TAG, "redialEmergencyIndication: items length invalid, slotId = %d", imsSlotId);
            return 0;
        }

        hidl_string callId = convertCharPtrToHidlString(resp[0]);

        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationImsV3_6->
                                 imsRedialEmergencyIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "redialEmergencyIndication: radioService[%d]->mRadioIndicationImsV3_6 == NULL", slotId);
    }

     return 0;
}


int radio::emergencyBearerInfoInd(int slotId, int indicationType, int token, RIL_Errno e,
        void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "emergencyBearerInfoInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "emergencyBearerInfoInd");
        int32_t s1Support = ((int32_t *) response)[0];
        // We use same interface to update ECC support or not.
        // So value will be transformed here.
        // 0 => 2
        // 1 => 3
        s1Support += 2;
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->
                                 imsSupportEcc(
                                 convertIntToRadioIndicationType(indicationType),
                                 s1Support);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "emergencyBearerInfoInd: radioService[%d]->mRadioIndicationIms == NULL", slotId);
    }

     return 0;
}

int radio::imsRadioInfoChangeInd(int slotId,
                     int indicationType, int token, RIL_Errno e,
                     void *response, size_t responseLen) {
    return 0;
}

void radio::registerService(RIL_RadioFunctions *callbacks, CommandInfo *commands) {
    using namespace android::hardware;
    int simCount = 1;
    const char *serviceNames[] = {
            android::RIL_getServiceName()
            , RIL2_SERVICE_NAME
            , RIL3_SERVICE_NAME
            , RIL4_SERVICE_NAME
            };

    const char *imsServiceNames[] = {
            IMS_RIL1_SERVICE_NAME
            , IMS_RIL2_SERVICE_NAME
            , IMS_RIL3_SERVICE_NAME
            , IMS_RIL4_SERVICE_NAME
            };

    const char *seServiceNames[] = {
            SE_RIL1_SERVICE_NAME
            , SE_RIL2_SERVICE_NAME
            , SE_RIL3_SERVICE_NAME
            , SE_RIL4_SERVICE_NAME
            };

    /* [ALPS03590595]Set s_vendorFunctions and s_commands before registering service to
        null exception timing issue. */
    s_vendorFunctions = callbacks;
    s_commands = commands;

    simCount = getSimCount();
    configureRpcThreadpool(1, true /* callerWillJoin */);
    char tempstr[MTK_PROPERTY_VALUE_MAX] = {0};
    mtk_property_get("persist.vendor.ril.test_mode", tempstr, "0");
    if (atoi(tempstr) != 0) {
        mtkLogW(LOG_TAG, "registerService: MTTS mode, don't register HIDL service!!");
        return;
    }
    for (int i = 0; i < simCount; i++) {
        pthread_rwlock_t *radioServiceRwlockPtr = getRadioServiceRwlock(i);
        int ret = pthread_rwlock_wrlock(radioServiceRwlockPtr);
        assert(ret == 0);

        radioService[i] = new RadioImpl;
        radioService[i]->mSlotId = i;
        mtkLogI(LOG_TAG, "registerService: starting IRadio %s", serviceNames[i]);
        android::status_t status = radioService[i]->registerAsService(serviceNames[i]);
        int imsSlot = (i % simCount) + (DIVISION_IMS * simCount);
        radioService[imsSlot] = new RadioImpl;
        radioService[imsSlot]->mSlotId = imsSlot;
        mtkLogD(LOG_TAG, "radio::registerService: starting IMS IRadio %s, slot = %d, realSlot = %d",
              imsServiceNames[i], radioService[imsSlot]->mSlotId, imsSlot);

        // Register IMS Radio Stub
        status = radioService[imsSlot]->registerAsService(imsServiceNames[i]);
        mtkLogD(LOG_TAG, "radio::registerService IRadio for IMS status:%d", status);

        /// MTK: ForSE @{
        int seSlot = i + (DIVISION_SE * simCount);
        radioService[seSlot] = new RadioImpl;
        radioService[seSlot]->mSlotId = seSlot;
        status = radioService[seSlot]->registerAsService(seServiceNames[i]);
        mtkLogD(LOG_TAG, "radio::registerService IRadio for SE status:%d", status);
        /// MTK: ForSE @}

        ret = pthread_rwlock_unlock(radioServiceRwlockPtr);
        assert(ret == 0);
    }
}

void rilc_thread_pool() {
    joinRpcThreadpool();
}

pthread_rwlock_t * radio::getRadioServiceRwlock(int slotId) {
    pthread_rwlock_t *radioServiceRwlockPtr = &(radioServiceRwlocks[toRealSlot(slotId)]);
    return radioServiceRwlockPtr;
}

// should acquire write lock for the corresponding service before calling this
void radio::setNitzTimeReceived(int slotId, long timeReceived) {
    nitzTimeReceived[slotId] = timeReceived;
}

// MTK-START: SIM
int radio::onVirtualSimOn(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onVirtualSimOn");
        int32_t simInserted = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onVirtualSimOn(
                convertIntToRadioIndicationType(indicationType), simInserted);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onVirtualSimOn: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onVirtualSimOff(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onVirtualSimOff");
        int32_t simInserted = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onVirtualSimOff(
                convertIntToRadioIndicationType(indicationType), simInserted);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onVirtualSimOff: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onImeiLock(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onImeiLock");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onImeiLock(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onImeiLock: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onImsiRefreshDone(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onImsiRefreshDone");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onImsiRefreshDone(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onImsiRefreshDone: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// MTK-END
Return<void> RadioImpl::getSmsRuimMemoryStatus(int32_t serial) {
    mtkLogD(LOG_TAG, "getSmsRuimMemoryStatus: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_SMS_RUIM_MEM_STATUS);
    return Void();
}

int radio::getSmsRuimMemoryStatusResponse(int slotId,
                                   int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getSmsRuimMemoryStatusResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        SmsMemStatus status = {};
        if (response == NULL || responseLen != sizeof (RIL_SMS_Memory_Status)) {
            mtkLogE(LOG_TAG, "getSmsRuimMemoryStatusResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_SMS_Memory_Status *mem_status = (RIL_SMS_Memory_Status*)response;
            status.used = mem_status->used;
            status.total = mem_status->total;
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getSmsRuimMemoryStatusResponse(
                responseInfo, status);
        radioService[slotId]->checkReturnStatus(retStatus);

    } else {
        mtkLogE(LOG_TAG, "getSmsRuimMemoryStatusResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }
    return 0;
}

Return<void> RadioImpl::setEccList(int32_t serial, const ::android::hardware::hidl_string& list1,
            const ::android::hardware::hidl_string& list2){
    mtkLogD(LOG_TAG, "setEccList: ecclist %s, %s", list1.c_str(), list2.c_str());
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_ECC_LIST, false, 2,
            list1.c_str(), list2.c_str());
    return Void();
}

int radio::setEccListResponse(int slotId,
        int responseType, int serial, RIL_Errno e,
        void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "setEccListResponse: slotId %d, serial %d, e %d", slotId, serial, e);
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setEccListResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setEccListResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

Return<void> RadioImpl::setVoicePreferStatus(int32_t serial, int32_t status) {
    return Void();
}

Return<void> RadioImpl::setEccNum(int32_t serial, const hidl_string& eccListWithCard,
        const hidl_string& eccListNoCard) {
    mtkLogI(LOG_TAG, "setEccNum: eccListWithCard %s, eccListNoCard %s",
            eccListWithCard.c_str(), eccListNoCard.c_str());
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_ECC_NUM, false, 2,
            eccListWithCard.c_str(), eccListNoCard.c_str());
    return Void();
}

Return<void> RadioImpl::getEccNum(int32_t serial) {
    mtkLogD(LOG_TAG, "getEccNum");
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_ECC_NUM);
    return Void();
}

int radio::setVoicePreferStatusResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    return 0;
}

int radio::setEccNumResponse(int slotId, int responseType,
        int serial, RIL_Errno e, void *response,
        size_t responselen) {
    mtkLogD(LOG_TAG, "setEccNumResponse: slotId %d, serial %d, e %d", slotId, serial, e);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setEccNumResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogD(LOG_TAG, "setEccNumResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }
    return 0;
}

int radio::getEccNumResponse(int slotId, int responseType,
                            int serial, RIL_Errno e, void *response,
                            size_t responselen) {
    mtkLogD(LOG_TAG, "getEccNumResponse: slotId %d, serial %d, e %d", slotId, serial, e);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getEccNumResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogD(LOG_TAG, "getEccNumResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }
    return 0;
}

int radio::eccNumIndication(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "eccNumIndication invalid response");
            return 0;
        }

        char *resp = (char *) response;
        hidl_string eccList = convertCharPtrToHidlString(resp);

        mtkLogD(LOG_TAG, "eccNumIndication: %s", resp);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->eccNumIndication(
                convertIntToRadioIndicationType(indicationType), eccList, eccList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "eccNumIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

// FastDormancy Begin
bool dispatchFdMode(int serial, int slotId, int request, int mode, int param1, int param2) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_FdModeStructure args;
    args.mode = mode;

    /* AT+EFD=<mode>[,<param1>[,<param2>]] */
    /* For all modes: but mode 0 & 1 only has one argument */
    if (mode == 0 || mode == 1) {
        args.paramNumber = 1;
    }

    if (mode == 2) {
        args.paramNumber = 3;
        args.parameter1 = param1;
        args.parameter2 = param2;
    }

    if (mode == 3) {
        args.paramNumber = 2;
        args.parameter1 = param1;
    }

    s_vendorFunctions->onRequest(request, &args, sizeof(RIL_FdModeStructure), pRI, pRI->socket_id);

    return true;
}

Return<void> RadioImpl::setFdMode(int32_t serial, int mode, int param1, int param2) {
    mtkLogD(LOG_TAG, "setFdMode: serial %d mode %d para1 %d para2 %d", serial, mode, param1, param2);
    dispatchFdMode(serial, mSlotId, RIL_REQUEST_SET_FD_MODE, mode, param1, param2);
    return Void();
}

int radio::setFdModeResponse(int slotId, int responseType,
                      int serial, RIL_Errno e, void *response, size_t responselen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->setFdModeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setFdModeResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}
// FastDormancy End


// World Phone {
int radio::plmnChangedIndication(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "plmnChangedIndication: invalid response");
            return 0;
        }
        hidl_vec<hidl_string> plmn;
        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        plmn.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            plmn[i] = convertCharPtrToHidlString(resp[i]);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->plmnChangedIndication(
                convertIntToRadioIndicationType(indicationType), plmn);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "plmnChangedIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::registrationSuspendedIndication(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "registrationSuspendedIndication: invalid response");
            return 0;
        }
        hidl_vec<int32_t> sessionId;
        int numInts = responselen / sizeof(int);
        int *pInt = (int *) response;
        sessionId.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            sessionId[i] = pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->registrationSuspendedIndication(
                convertIntToRadioIndicationType(indicationType), sessionId);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "registrationSuspendedIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::gmssRatChangedIndication(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "gmssRatChangedIndication: invalid response");
            return 0;
        }
        hidl_vec<int32_t> gmss;
        int numInts = responselen / sizeof(int);
        int *pInt = (int *) response;
        gmss.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            gmss[i] = pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->gmssRatChangedIndication(
                convertIntToRadioIndicationType(indicationType), gmss);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "gmssRatChangedIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::worldModeChangedIndication(int slotId,
                      int indicationType, int token, RIL_Errno e, void *response,
                      size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "worldModeChangedIndication: invalid response");
            return 0;
        }
        hidl_vec<int32_t> mode;
        int numInts = responselen / sizeof(int);
        int *pInt = (int *) response;
        mode.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            mode[i] = pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->worldModeChangedIndication(
                convertIntToRadioIndicationType(indicationType), mode);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "worldModeChangedIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// World Phone }

int radio::esnMeidChangeInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "esnMeidChangeInd: invalid response");
            return 0;
        }
        hidl_string esnMeid((const char*)response, responseLen);
        mtkLogD(LOG_TAG, "esnMeidChangeInd (0x%s - %d)", esnMeid.c_str(), responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->esnMeidChangeInd(
                convertIntToRadioIndicationType(indicationType),
                esnMeid);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "esnMeidChangeInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

Return<void> RadioImpl::resetRadio(int32_t serial) {
    mtkLogD(LOG_TAG, "resetRadio: serial: %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_RESET_RADIO);
    return Void();
}

int radio::resetRadioResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->resetRadioResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resetRadioResponse: radioService[%d]->resetRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

Return<void> RadioImpl::restartRILD(int32_t serial) {
    mtkLogD(LOG_TAG, "restartRILD: serial: %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_RESTART_RILD);
    return Void();
}

int radio::restartRILDResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->restartRILDResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "restartRILDResponse: radioService[%d]->restartRILDResponse "
                "== NULL", slotId);
    }

    return 0;
}

// / M: BIP, only to build pass {
int radio::bipProactiveCommandInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    return 0;
}
// / M: BIP }
// / M: OTASP, only to build pass {
int radio::triggerOtaSPInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    return 0;
}
// / M: OTASP }

// / M: STK, only to build pass {
int radio::onStkMenuResetInd(int slotId,
                                  int indicationType, int token, RIL_Errno e, void *response,
                                  size_t responseLen) {
    return 0;
}

Return<void> RadioImpl::handleStkCallSetupRequestFromSimWithResCode(int32_t serial,
            int32_t resultCode){
    mtkLogD(LOG_TAG, "handleStkCallSetupRequestFromSimwithResCode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_STK_HANDLE_CALL_SETUP_REQUESTED_FROM_SIM_WITH_RESULT_CODE,
            1, resultCode);
    return Void();
}

Return<void> RadioImpl::setPdnReuse(int32_t serial, const hidl_string& pdnReuse){
    return Void();
}

 Return<void> RadioImpl::setOverrideApn(int32_t serial, const hidl_string& overrideApn){
    return Void();
}

Return<void> RadioImpl::setPdnNameReuse(int32_t serial, const hidl_string& apnName){
    return Void();
}

int radio::setPdnReuseResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    return 0;
}

int radio::setOverrideApnResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    return 0;
}

int radio::setPdnNameReuseResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    return 0;
}

int radio::handleStkCallSetupRequestFromSimWithResCodeResponse(int slotId,
                                                   int responseType, int serial,
                                                   RIL_Errno e, void *response,
                                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "handleStkCallSetupRequestFromSimWithResCodeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->handleStkCallSetupRequestFromSimWithResCodeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "handleStkCallSetupRequestFromSimWithResCodeResponse: radioService[%d]->mRadioResponseMtk "
                "== NULL", slotId);
    }

    return 0;
}
// / M: STK }

Return<void> RadioImpl::syncDataSettingsToMd(int32_t serial, const hidl_vec<int32_t>& settings) {
    mtkLogD(LOG_TAG, "syncDataSettingsToMd: serial: %d", serial);
    if (settings.size() == 3) {
        dispatchInts(serial, mSlotId, RIL_REQUEST_SYNC_DATA_SETTINGS_TO_MD, 3,
                settings[0], settings[1], settings[2]);
    } else if (settings.size() == 5) {
        dispatchInts(serial, mSlotId, RIL_REQUEST_SYNC_DATA_SETTINGS_TO_MD, 5,
                settings[0], settings[1], settings[2], settings[3], settings[4]);
    } else {
        mtkLogE(LOG_TAG, "syncDataSettingsToMd: param error, num: %d (should be 3 or 5)", (int) settings.size());
    }
    return Void();
}

int radio::syncDataSettingsToMdResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->syncDataSettingsToMdResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "syncDataSettingsToMdResponse: radioService[%d]->resetRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

// M: Data Framework - Data Retry enhancement @{
Return<void> RadioImpl::resetMdDataRetryCount(int32_t serial, const hidl_string& apn) {
    mtkLogD(LOG_TAG, "resetMdDataRetryCount: serial: %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_RESET_MD_DATA_RETRY_COUNT, apn.c_str());
    return Void();
}

int radio::resetMdDataRetryCountResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->resetMdDataRetryCountResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resetMdDataRetryCountResponse: radioService[%d]->resetRadioResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::onMdDataRetryCountReset(int slotId, int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onMdDataRetryCountReset");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onMdDataRetryCountReset(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onMdDataRetryCountReset: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// M: Data Framework - Data Retry enhancement @}

// M: Data Framework - CC 33
Return<void> RadioImpl::setRemoveRestrictEutranMode(int32_t serial, int32_t type) {
    return Void();
}
int radio::setRemoveRestrictEutranModeResponse(int slotId, int responseType, int serial,
        RIL_Errno e, void *response, size_t responseLen){
    return 0;
}

int radio::onRemoveRestrictEutran(int slotId, int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onRemoveRestrictEutran");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onRemoveRestrictEutran(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onRemoveRestrictEutran: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onPcoStatus(int slotId, int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "onPcoStatus: invalid response");
            return 0;
        }
        hidl_vec<int32_t> pco;
        int numInts = responseLen / sizeof(int);
        int *pInt = (int *) response;
        pco.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            pco[i] = pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onPcoStatus(
                convertIntToRadioIndicationType(indicationType), pco);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onPcoStatus: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

// M: [LTE][Low Power][UL traffic shaping] @{
int radio::setLteAccessStratumReportResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setLteAccessStratumReportResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                setLteAccessStratumReportResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setLteAccessStratumReportResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::setLteUplinkDataTransferResponse(int slotId,
                               int responseType, int serial, RIL_Errno e,
                               void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setLteUplinkDataTransferResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                setLteUplinkDataTransferResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setLteUplinkDataTransferResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::onLteAccessStratumStateChanged(int slotId, int indicationType, int token, RIL_Errno e,
                        void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_vec<int32_t> data;

        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "onLteAccessStratumStateChanged Invalid response: NULL");
            return 0;
        } else {
            int *pInt = (int *) response;
            data.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                data[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioIndicationMtk->onLteAccessStratumStateChanged(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "mdChangeApnInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}
// M: [LTE][Low Power][UL traffic shaping] @}

// MTK-START: SIM HOT SWAP
int radio::onSimPlugIn(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimPlugIn");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onSimPlugIn(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimPlugIn: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onSimPlugOut(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimPlugOut");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onSimPlugOut(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimPlugOut: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// MTK-END

// MTK-START: SIM MISSING/RECOVERY
int radio::onSimMissing(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimMissing");
        int32_t simInserted = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onSimMissing(
                convertIntToRadioIndicationType(indicationType), simInserted);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimMissing: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onSimRecovery(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimRecovery");
        int32_t simInserted = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onSimRecovery(
                convertIntToRadioIndicationType(indicationType), simInserted);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimRecovery: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// MTK-END
// MTK-START: SIM POWER
int radio::onSimPowerChangedInd(int slotId, int indicationType, int token, RIL_Errno e,
        void *response, size_t responseLen) {
    return 0;
}
// MTK-END
// MTK-START: SIM COMMON SLOT
int radio::onSimTrayPlugIn(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimTrayPlugIn");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onSimTrayPlugIn(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimTrayPlugIn: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::onSimCommonSlotNoChanged(int slotId,
        int indicationType, int token, RIL_Errno e, void *response,
        size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "onSimCommonSlotNoChanged");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->
                onSimCommonSlotNoChanged(convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onSimCommonSlotNoChanged: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// MTK-END
// SMS-START
Return<void> RadioImpl::getSmsParameters(int32_t serial) {
    mtkLogD(LOG_TAG, "getSmsParameters: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_SMS_PARAMS);
    return Void();
}

bool dispatchSmsParametrs(int serial, int slotId, int request, const SmsParams& message) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_SmsParams params;
    memset (&params, 0, sizeof(RIL_SmsParams));

    params.dcs = message.dcs;
    params.format = message.format;
    params.pid = message.pid;
    params.vp = message.vp;

    s_vendorFunctions->onRequest(request, &params, sizeof(params), pRI, pRI->socket_id);

    return true;
}


Return<void> RadioImpl::setSmsParameters(int32_t serial, const SmsParams& message) {
    mtkLogD(LOG_TAG, "setSmsParameters: serial %d", serial);
    dispatchSmsParametrs(serial, mSlotId, RIL_REQUEST_SET_SMS_PARAMS, message);
    return Void();
}

Return<void> RadioImpl::setEtws(int32_t serial, int32_t mode) {
    mtkLogD(LOG_TAG, "setEtws: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_ETWS, 1, mode);
    return Void();
}

Return<void> RadioImpl::removeCbMsg(int32_t serial, int32_t channelId, int32_t serialId) {
    mtkLogD(LOG_TAG, "removeCbMsg: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_REMOVE_CB_MESSAGE, 2, channelId, serialId);
    return Void();
}

Return<void> RadioImpl::getSmsMemStatus(int32_t serial) {
    mtkLogD(LOG_TAG, "getSmsMemStatus: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_SMS_SIM_MEM_STATUS);
    return Void();
}

Return<void> RadioImpl::setGsmBroadcastLangs(int32_t serial, const hidl_string& langs) {
    mtkLogD(LOG_TAG, "setGsmBroadcastLangs: serial %d", serial);
    dispatchString(serial, mSlotId, RIL_REQUEST_GSM_SET_BROADCAST_LANGUAGE, langs.c_str());
    return Void();
}

Return<void> RadioImpl::getGsmBroadcastLangs(int32_t serial) {
    mtkLogD(LOG_TAG, "getGsmBroadcastLangs: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GSM_GET_BROADCAST_LANGUAGE);
    return Void();
}

Return<void> RadioImpl::getGsmBroadcastActivation(int32_t serial) {
    mtkLogD(LOG_TAG, "getGsmBroadcastActivation: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_GSM_SMS_BROADCAST_ACTIVATION);
    return Void();
}

Return<void> RadioImpl::sendImsSmsEx(int32_t serial, const ImsSmsMessage& message) {
    mtkLogD(LOG_TAG, "sendImsSmsEx: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_IMS_SEND_SMS_EX);
    if (pRI == NULL) {
        return Void();
    }

    RIL_RadioTechnologyFamily format = (RIL_RadioTechnologyFamily) message.tech;

    if (RADIO_TECH_3GPP == format) {
        dispatchImsGsmSms(message, pRI);
    } else if (RADIO_TECH_3GPP2 == format) {
        dispatchImsCdmaSms(message, pRI);
    } else {
        mtkLogE(LOG_TAG, "sendImsSms: Invalid radio tech %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
    }
    return Void();
}

Return<void> RadioImpl::acknowledgeLastIncomingGsmSmsEx(int32_t serial,
                                                      bool success, SmsAcknowledgeFailCause cause) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingGsmSmsEx: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SMS_ACKNOWLEDGE_EX, 2, BOOL_TO_INT(success),
            cause);
    return Void();
}

Return<void> RadioImpl::setSmsFwkReady(int32_t serial) {
    mtkLogD(LOG_TAG, "setSmsFwkReady: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_SET_SMS_FWK_READY);
    return Void();
}

int radio::setSmsFwkRsp(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSmsFwkRsp: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setSmsFwkReadyRsp(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSmsFwkRsp: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::sendImsSmsExResponse(int slotId,
                              int responseType, int serial, RIL_Errno e, void *response,
                              size_t responseLen) {
    mtkLogD(LOG_TAG, "sendImsSmsExResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        SendSmsResult result = makeSendSmsResult(responseInfo, serial, responseType, e, response,
                responseLen);

        Return<void> retStatus
                = radioService[slotId]->mRadioResponseIms->sendImsSmsExResponse(
                responseInfo, result);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "sendImsSmsExResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

int radio::acknowledgeLastIncomingGsmSmsExResponse(int slotId,
                                                int responseType, int serial, RIL_Errno e,
                                                void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "acknowledgeLastIncomingGsmSmsExResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseIms->acknowledgeLastIncomingGsmSmsExResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "acknowledgeLastIncomingGsmSmsExResponse: radioService[%d]->mRadioResponseIms "
                "== NULL", slotId);
    }

    return 0;
}

int radio::getSmsParametersResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        SmsParams params = {};
        if (response == NULL || responseLen != sizeof(RIL_SmsParams)) {
            mtkLogE(LOG_TAG, "getSmsParametersResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_SmsParams *p_cur = ((RIL_SmsParams *) response);
            params.format = p_cur->format;
            params.dcs = p_cur->dcs;
            params.vp = p_cur->vp;
            params.pid = p_cur->pid;
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                getSmsParametersResponse(responseInfo, params);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getSmsParametersResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setSmsParametersResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSmsParametersResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setSmsParametersResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSmsParametersResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setEtwsResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setEtwsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setEtwsResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setEtwsResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::removeCbMsgResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "removeCbMsgResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->removeCbMsgResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "removeCbMsgResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::newEtwsInd(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responselen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen == 0) {
            mtkLogE(LOG_TAG, "newEtwsInd: invalid response");
            return 0;
        }

        EtwsNotification etws = {};
        RIL_CBEtwsNotification *pEtws = (RIL_CBEtwsNotification *)response;
        etws.messageId = pEtws->messageId;
        etws.serialNumber = pEtws->serialNumber;
        etws.warningType = pEtws->warningType;
        etws.plmnId = convertCharPtrToHidlString(pEtws->plmnId);
        etws.securityInfo = convertCharPtrToHidlString(pEtws->securityInfo);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->newEtwsInd(
                convertIntToRadioIndicationType(indicationType), etws);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "newEtwsInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

int radio::newSmsIndEx(int slotId, int indicationType,
                     int token, RIL_Errno e, void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "newSmsIndEx: invalid response");
            return 0;
        }

        uint8_t *bytes = convertHexStringToBytes(response, responseLen);
        if (bytes == NULL) {
            mtkLogE(LOG_TAG, "newSmsIndEx: convertHexStringToBytes failed");
            return 0;
        }

        hidl_vec<uint8_t> pdu;
        pdu.setToExternal(bytes, responseLen/2);
        mtkLogD(LOG_TAG, "newSmsIndEx");
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationIms->newSmsEx(
                convertIntToRadioIndicationType(indicationType), pdu);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
        free(bytes);
    } else {
        mtkLogE(LOG_TAG, "newSmsIndEx: radioService[%d]->mRadioIndicationIms == NULL", imsSlotId);
    }

    return 0;
}

int radio::newSmsStatusReportIndEx(int slotId,
                                 int indicationType, int token, RIL_Errno e, void *response,
                                 size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "newSmsStatusReportIndEx: invalid response");
            return 0;
        }

        uint8_t *bytes = convertHexStringToBytes(response, responseLen);
        if (bytes == NULL) {
            mtkLogE(LOG_TAG, "newSmsStatusReportIndEx: convertHexStringToBytes failed");
            return 0;
        }

        hidl_vec<uint8_t> pdu;
        pdu.setToExternal(bytes, responseLen/2);
        mtkLogD(LOG_TAG, "newSmsStatusReportIndEx");
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationIms->newSmsStatusReportEx(
                convertIntToRadioIndicationType(indicationType), pdu);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
        free(bytes);
    } else {
        mtkLogE(LOG_TAG, "newSmsStatusReportIndEx: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }
    return 0;
}

int radio::getSmsMemStatusResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        SmsMemStatus params = {};
        if (response == NULL || responseLen != sizeof(RIL_SMS_Memory_Status)) {
            mtkLogE(LOG_TAG, "getSmsMemStatusResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_SMS_Memory_Status *p_cur = ((RIL_SMS_Memory_Status *) response);
            params.used = p_cur->used;
            params.total = p_cur->total;
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                getSmsMemStatusResponse(responseInfo, params);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getSmsMemStatusResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::setGsmBroadcastLangsResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setGsmBroadcastLangsResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setGsmBroadcastLangsResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setGsmBroadcastLangsResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::getGsmBroadcastLangsResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                getGsmBroadcastLangsResponse(
                responseInfo, convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getGsmBroadcastLangsResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::getGsmBroadcastActivationRsp(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int *pInt = (int *) response;
        int activation = pInt[0];
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                getGsmBroadcastActivationRsp(responseInfo, activation);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getGsmBroadcastActivationRsp: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::meSmsStorageFullInd(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "meSmsStorageFullInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->meSmsStorageFullInd(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "meSmsStorageFullInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::smsReadyInd(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        mtkLogD(LOG_TAG, "smsReadyInd");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->smsReadyInd(
                convertIntToRadioIndicationType(indicationType));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "smsReadyInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;

}
// SMS-END

int radio::responsePsNetworkStateChangeInd(int slotId,
                                           int indicationType, int token, RIL_Errno e,
                                           void *response, size_t responseLen) {

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_vec<int32_t> data;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "responsePsNetworkStateChangeInd Invalid response: NULL");
            return 0;
        } else {
            int *pInt = (int *) response;
            data.resize(numInts);
            for (int i=0; i<numInts; i++) {
                data[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioIndicationMtk->responsePsNetworkStateChangeInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responsePsNetworkStateChangeInd: radioService[%d]->responsePsNetworkStateChangeInd == NULL",
                slotId);
    }
    return 0;
}

int radio::responseCsNetworkStateChangeInd(int slotId,
                              int indicationType, int token, RIL_Errno e, void *response,
                              size_t responseLen) {

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "responseCsNetworkStateChangeInd Invalid response: NULL");
            return 0;
        }
        mtkLogD(LOG_TAG, "responseCsNetworkStateChangeInd");
        hidl_vec<hidl_string> data;
        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        data.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            data[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogD(LOG_TAG, "responseCsNetworkStateChangeInd:: %d: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseCsNetworkStateChangeInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseCsNetworkStateChangeInd: radioService[%d]->responseCsNetworkStateChangeInd == NULL", slotId);
    }
    return 0;
}

int radio::networkInfoInd(int slotId, int indicationType, int token, RIL_Errno e, void *response,
                                   size_t responseLen) {
    mtkLogD(LOG_TAG, "networkInfoInd");

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_vec<hidl_string> networkInfo;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "networkInfoInd Invalid networkInfo: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            networkInfo.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                networkInfo[i] = convertCharPtrToHidlString(resp[i]);
            }
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->networkInfoInd(
                 convertIntToRadioIndicationType(indicationType), networkInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "networkInfoInd: radioService[%d]->mRadioIndication "
                 "== NULL", slotId);
    }
    return 0;
}

int radio::setRxTestConfigResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setRxTestConfigResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> respAntConf;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "setRxTestConfigResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            respAntConf.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                respAntConf[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setRxTestConfigResponse(responseInfo,
                respAntConf);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRxTestConfigResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::getRxTestResultResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getRxTestResultResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> respAntInfo;
        int numInts = responseLen / sizeof(int);
        if (response == NULL || responseLen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "getRxTestResultResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            respAntInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                respAntInfo[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getRxTestResultResponse(responseInfo,
                respAntInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getRxTestResultResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::responseInvalidSimInd(int slotId,
                              int indicationType, int token, RIL_Errno e, void *response,
                              size_t responseLen) {


    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "responseInvalidSimInd Invalid response: NULL");
            return 0;
        }
        mtkLogD(LOG_TAG, "responseInvalidSimInd");
        hidl_vec<hidl_string> data;
        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        data.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            data[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogD(LOG_TAG, "responseCsNetworkStateChangeInd:: %d: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseInvalidSimInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseInvalidSimInd: radioService[%d]->responseInvalidSimInd == NULL", slotId);
    }
    return 0;
}

int radio::responseLteNetworkInfo(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen != sizeof(int)) {
            mtkLogE(LOG_TAG, "responseLteNetworkInfo: invalid response");
            return 0;
        }
        int32_t info = ((int32_t *) response)[0];
        mtkLogD(LOG_TAG, "responseLteNetworkInfo: %d", info);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseLteNetworkInfo(
                convertIntToRadioIndicationType(indicationType), info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "vtStatusInfoInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::getPOLCapabilityResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getPOLCapabilityResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> polCapability;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getPOLCapabilityResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            int numInts = responseLen / sizeof(int);
            polCapability.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                polCapability[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getPOLCapabilityResponse(
                responseInfo, polCapability);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPOLCapabilityResponse: radioService[%d]->getPOLCapabilityResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::getCurrentPOLListResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getCurrentPOLListResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<hidl_string> polList;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getPOLCapabilityResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            polList.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                polList[i] = convertCharPtrToHidlString(resp[i]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getCurrentPOLListResponse(
                responseInfo, polList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getPOLCapabilityResponse: radioService[%d]->getPOLCapabilityResponse "
                "== NULL", slotId);
    }

    return 0;
}

int radio::setPOLEntryResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPOLEntryResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setPOLEntryResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPOLEntryResponse: radioService[%d]->setPOLEntryResponse "
                "== NULL", slotId);
    }

    return 0;

}
// PHB START
int radio::phbReadyNotificationInd(int slotId,
                                   int indicationType, int token, RIL_Errno e, void *response,
                                   size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "phbReadyNotificationInd: invalid response");
            return 0;
        }
        mtkLogD(LOG_TAG, "phbReadyNotificationInd");
        int32_t isPhbReady = ((int32_t *) response)[0];
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->phbReadyNotification(
                convertIntToRadioIndicationType(indicationType), isPhbReady);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "phbReadyNotificationInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

     return 0;
}  // PHB END

int radio::setTrmResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setTrmResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->setTrmResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setTrmResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }
    return 0;
}

Return<void> RadioImpl::setApcMode(int32_t serial, int32_t mode,
        int32_t reportMode, int32_t interval) {
    mtkLogD(LOG_TAG, "setApcMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_PSEUDO_CELL_MODE, 3,
            mode, reportMode, interval);
    return Void();
}

Return<void> RadioImpl::getApcInfo(int32_t serial) {
    mtkLogD(LOG_TAG, "getApcInfo: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_GET_PSEUDO_CELL_INFO);
    return Void();
}

int radio::responseNetworkEventInd(int slotId,
                                           int indicationType, int token, RIL_Errno e,
                                           void *response, size_t responseLen) {

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "responseNetworkEventInd Invalid response: NULL");
            return 0;
        }
        mtkLogD(LOG_TAG, "responseNetworkEventInd");
        hidl_vec<int32_t> data;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseNetworkEventInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseNetworkEventInd: radioService[%d]->responseNetworkEventInd == NULL", slotId);
    }
    return 0;
}

int radio::responseModulationInfoInd(int slotId,
                                           int indicationType, int token, RIL_Errno e,
                                           void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "responseModulationInfoInd Invalid response: NULL");
            return 0;
        }
        mtkLogD(LOG_TAG, "responseModulationInfoInd");
        hidl_vec<int32_t> data;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseModulationInfoInd(
                convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseModulationInfoInd: radioService[%d]->responseModulationInfoInd == NULL", slotId);
    }
    return 0;
}

int radio::setApcModeResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setApcModeResponse: serial %d", serial);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
            = radioService[slotId]->mRadioResponseMtk->setApcModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setApcModeResponse: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

// Femtocell feature
int radio::getFemtocellListResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getFemtocellListResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<hidl_string> femtoList;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getFemtocellListResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            char **resp = (char **) response;
            int numStrings = responseLen / sizeof(char *);
            femtoList.resize(numStrings);
            for (int i = 0; i < numStrings; i++) {
                femtoList[i] = convertCharPtrToHidlString(resp[i]);
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getFemtocellListResponse(
                responseInfo, femtoList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getFemtocellListResponse: radioService[%d]->mRadioIndicationMtk "
                "== NULL", slotId);
    }

    return 0;
}

int radio::abortFemtocellListResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "abortFemtocellListResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->abortFemtocellListResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "abortFemtocellListResponse: radioService[%d]->mRadioResponseMtk "
                "== NULL", slotId);
    }

    return 0;
}

int radio::selectFemtocellResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "selectFemtocellResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->selectFemtocellResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "selectFemtocellResponse: radioService[%d]->mRadioResponseMtk "
                "== NULL", slotId);
    }

    return 0;
}

int radio::queryFemtoCellSystemSelectionModeResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "queryFemtoCellSystemSelectionModeResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        int mode = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->queryFemtoCellSystemSelectionModeResponse(
                responseInfo, mode);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "queryFemtoCellSystemSelectionModeResponse: radioService[%d]->mRadioResponseMtk "
                "== NULL", slotId);
    }

    return 0;
}

int radio::setFemtoCellSystemSelectionModeResponse(int slotId, int responseType, int serial, RIL_Errno e,
                         void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setFemtoCellSystemSelectionModeResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setFemtoCellSystemSelectionModeResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setFemtoCellSystemSelectionModeResponse: radioService[%d]->mRadioResponseMtkmRadioResponseMtk "
                "== NULL", slotId);
    }

    return 0;
}

int radio::responseFemtocellInfo(int slotId,
                         int indicationType, int token, RIL_Errno e, void *response,
                         size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG, "responseFemtocellInfo: invalid indication");
            return 0;
        }

        hidl_vec<hidl_string> info;
        char **resp = (char **) response;
        int numStrings = responseLen / sizeof(char *);
        info.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            info[i] = convertCharPtrToHidlString(resp[i]);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->responseFemtocellInfo(
                convertIntToRadioIndicationType(indicationType), info);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "responseFemtocellInfo: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

int radio::getApcInfoResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getApcInfoResponse: serial %d", serial);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> pseudoCellInfo;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getApcInfoResponse Invalid response: NULL");
            return 0;
        } else {
            int *pInt = (int *) response;
            int numInts = responseLen / sizeof(int);
            pseudoCellInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                pseudoCellInfo[i] = (int32_t)(pInt[i]);
            }
        }
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->getApcInfoResponse(
                responseInfo, pseudoCellInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getApcInfoResponse: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

int radio::onPseudoCellInfoInd(int slotId, int indicationType, int token, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "onPseudoCellInfoInd");
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        hidl_vec<int32_t> pseudoCellInfo;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "onPseudoCellInfoInd Invalid response: NULL");
            return 0;
        } else {
            int *pInt = (int *) response;
            int numInts = responseLen / sizeof(int);
            pseudoCellInfo.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                pseudoCellInfo[i] = (int32_t)(pInt[i]);
            }
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onPseudoCellInfoInd(
                convertIntToRadioIndicationType(indicationType), pseudoCellInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onPseudoCellInfoInd: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }
    return 0;
}

/// M: [Network][C2K] Sprint roaming control @{
int radio::setRoamingEnableResponse(int slotId, int responseType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setRoamingEnableResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                setRoamingEnableResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setRoamingEnableResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }
    return 0;
}

int radio::getRoamingEnableResponse(int slotId, int responseType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getRoamingEnableResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        hidl_vec<int32_t> config;
        if (response == NULL) {
            mtkLogE(LOG_TAG, "getRoamingEnableResponse Invalid response: NULL");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            int *pInt = (int *) response;
            int numInts = responseLen / sizeof(int);
            config.resize(numInts);
            for (int i = 0; i < numInts; i++) {
                config[i] = (int32_t) pInt[i];
            }
        }
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getRoamingEnableResponse(
                responseInfo, config);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getRoamingEnableResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }
    return 0;
}
/// @}

int radio::setLteReleaseVersionResponse(int slotId, int responseType, int serial, RIL_Errno e,
    void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setLteReleaseVersionResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setLteReleaseVersionResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setLteReleaseVersionResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::getLteReleaseVersionResponse(int slotId, int responseType, int serial, RIL_Errno e,
    void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "getLteReleaseVersionResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        int ret = responseInt(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getLteReleaseVersionResponse(
                responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "getLteReleaseVersionResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::onMccMncChangedInd(int slotId, int indicationType, int token, RIL_Errno e,
    void *response, size_t responselen) {
    mtkLogD(LOG_TAG, "onMccMncChangedInd: slotId:%d", slotId);
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "onMccMncChangedInd: Invalid response: NULL");
            return 0;
        } else {
            mtkLogD(LOG_TAG, "onMccMncChangedInd[%d]: %s", slotId, (char*) response);
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onMccMncChanged(
                convertIntToRadioIndicationType(indicationType),
                convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onMccMncChangedInd: radioService[%d]->mRadioIndicationMtk == NULL",
                slotId);
    }

    return 0;
}

int radio::dataAllowedNotificationInd(int slotId, int indicationType,
        int token, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogE(LOG_TAG, "Do NOT support dataAllowedNotificationInd!");
    return 0;
}

int radio::triggerModeSwitchByEccResponse(int slotId, int responseType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "triggerModeSwitchByEccResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                triggerModeSwitchByEccResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "triggerModeSwitchByEccResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }
    return 0;
}

Return<void> RadioImpl::setModemPower(int32_t serial, bool isOn) {
    mtkLogD(LOG_TAG, "setModemPower: serial: %d, isOn: %d", serial, isOn);
    if (isOn) {
        dispatchVoid(serial, mSlotId, RIL_REQUEST_MODEM_POWERON);
    } else {
        dispatchVoid(serial, mSlotId, RIL_REQUEST_MODEM_POWEROFF);
    }
    return Void();
}

int radio::setModemPowerResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setModemPowerResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setModemPowerResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setModemPowerResponse: radioService[%d]->setModemPowerResponse "
                "== NULL", slotId);
    }
    return 0;
}

// External SIM [START]
bool dispatchVsimEvent(int serial, int slotId, int request,
        uint32_t transactionId, uint32_t eventId, uint32_t simType) {
    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        return false;
    }

    RIL_VsimEvent args;
    args.transaction_id = transactionId;
    args.eventId = eventId;
    args.sim_type = simType;

    s_vendorFunctions->onRequest(request, &args, sizeof(args), pRI, pRI->socket_id);

    return true;
}

bool dispatchVsimOperationEvent(int serial, int slotId, int request,
        uint32_t transactionId, uint32_t eventId, int32_t result,
        int32_t dataLength, const hidl_vec<uint8_t>& data) {

    mtkLogD(LOG_TAG, "dispatchVsimOperationEvent: enter id=%d", eventId);

    RequestInfo *pRI = android::addRequestToList(serial, slotId, request);
    if (pRI == NULL) {
        mtkLogD(LOG_TAG, "dispatchVsimOperationEvent: pRI is NULL.");
        return false;
    }

    RIL_VsimOperationEvent args;

    memset (&args, 0, sizeof(args));

    // Transcation id
    args.transaction_id = transactionId;
    // Event id
    args.eventId = eventId;
    // Result
    args.result = result;
    // Data length
    args.data_length = dataLength;

    // Data array
    const uint8_t *uData = data.data();
    args.data= (char  *) calloc(1, (sizeof(char) * args.data_length * 2) + 1);
    memset(args.data, 0, ((sizeof(char) * args.data_length * 2) + 1));
    for (int i = 0; i < args.data_length; i++) {
        sprintf((args.data + (i*2)), "%02X", uData[i]);
    }

    //mtkLogD(LOG_TAG, "dispatchVsimOperationEvent: id=%d, data=%s", args.eventId, args.data);

    s_vendorFunctions->onRequest(request, &args, sizeof(args), pRI, pRI->socket_id);

    free(args.data);

    return true;
}

Return<void> RadioImpl::sendVsimNotification(int32_t serial, uint32_t transactionId,
        uint32_t eventId, uint32_t simType) {
    mtkLogD(LOG_TAG, "sendVsimNotification: serial %d", serial);
    dispatchVsimEvent(serial, mSlotId, RIL_REQUEST_VSIM_NOTIFICATION, transactionId, eventId, simType);
    return Void();
}

Return<void> RadioImpl::sendVsimOperation(int32_t serial, uint32_t transactionId,
        uint32_t eventId, int32_t result, int32_t dataLength, const hidl_vec<uint8_t>& data) {
    mtkLogD(LOG_TAG, "sendVsimOperation: serial %d", serial);
    dispatchVsimOperationEvent(serial, mSlotId, RIL_REQUEST_VSIM_OPERATION,
            transactionId, eventId, result, dataLength, data);
    return Void();
}

int radio::vsimNotificationResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {

    mtkLogD(LOG_TAG, "vsimNotificationResponse: serial %d, error: %d", serial, e);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        VsimEvent params = {};
        if (response == NULL || responseLen != sizeof(RIL_VsimEvent)) {
            mtkLogE(LOG_TAG, "vsimNotificationResponse: Invalid response");
            if (e == RIL_E_SUCCESS) responseInfo.error = RadioError::INVALID_RESPONSE;
        } else {
            RIL_VsimEvent *p_cur = ((RIL_VsimEvent *) response);
            params.transactionId = p_cur->transaction_id;
            params.eventId = p_cur->eventId;
            params.simType = p_cur->sim_type;
        }

        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                vsimNotificationResponse(responseInfo, params);
        radioService[slotId]->checkReturnStatus(retStatus);

    } else {
        mtkLogE(LOG_TAG, "vsimNotificationResponse: radioService[%d]->mRadioResponse == NULL", slotId);
    }

    return 0;
}

int radio::vsimOperationResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {

    mtkLogD(LOG_TAG, "vsimOperationResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->vsimOperationResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "vsimOperationResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }

    return 0;
}

int radio::onVsimEventIndication(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responselen) {

    mtkLogD(LOG_TAG, "onVsimEventIndication: indicationType %d", indicationType);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responselen == 0) {
            mtkLogE(LOG_TAG, "onVsimEventIndication: invalid response");
            return 0;
        }

        VsimOperationEvent event = {};
        RIL_VsimOperationEvent *response_data = (RIL_VsimOperationEvent *)response;
        event.transactionId = response_data->transaction_id;
        event.eventId = response_data->eventId;
        event.result = response_data->result;
        event.dataLength = response_data->data_length;
        event.data = convertCharPtrToHidlString(response_data->data);

        //mtkLogD(LOG_TAG, "onVsimEventIndication: id=%d, data_length=%d, data=%s", event.eventId, response_data->data_length, response_data->data);

        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->onVsimEventIndication(
                convertIntToRadioIndicationType(indicationType), event);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onVsimEventIndication: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}
// External SIM [END]

Return<void> RadioImpl::setVoiceDomainPreference(int32_t serial, int32_t vdp){
    mtkLogD(LOG_TAG, "setVoiceDomainPreference: %d", vdp);

    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_VOICE_DOMAIN_PREFERENCE, 1, vdp);

    return Void();
}

int radio::setVoiceDomainPreferenceResponse(int slotId,
                            int responseType, int serial, RIL_Errno e,
                            void *response,
                            size_t responselen) {

    mtkLogD(LOG_TAG, "setVoiceDomainPreferenceResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseIms != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus = radioService[slotId]->
                                 mRadioResponseIms->
                                 setVoiceDomainPreferenceResponse(responseInfo);

        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setVoiceDomainPreferenceResponse: radioService[%d]->mRadioResponseIms == NULL",
                slotId);
    }

    return 0;
}

/// Ims Data Framework @{
int radio::dedicatedBearerActivationInd(int slotId, int indicationType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "dedicatedBearerActivationInd: Current is not support");
    return 0;
}

int radio::dedicatedBearerModificationInd(int slotId, int indicationType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "dedicatedBearerModificationInd: Current is not support");
    return 0;
}

int radio::dedicatedBearerDeactivationInd(int slotId, int indicationType, int serial,
        RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "dedicatedBearerDeactivationInd: Current is not support");
    return 0;
}

#ifndef WIFI_SNR_UNKNOW
#define WIFI_SNR_UNKNOW 60
#endif

Return<void> RadioImpl::setWifiSignalLevel(int32_t serial,
            int32_t rssi, int32_t snr) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_WIFI_SIGNAL_LEVEL, true, 3,
            "wlan", std::to_string(rssi).c_str(),
            (snr == WIFI_SNR_UNKNOW) ? "unknow" : std::to_string(snr).c_str());
    return Void();
}

Return<void> RadioImpl::setWifiEnabled(int32_t serial,
    const hidl_string& ifName, int32_t isWifiEnabled, int32_t isFlightModeOn) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_WIFI_ENABLED, true, 3,
        ifName.c_str(), std::to_string(isWifiEnabled).c_str(), std::to_string(isFlightModeOn).c_str());
    return Void();
}

Return<void> RadioImpl::setWifiAssociated(int32_t serial,
        const hidl_string& ifName, int32_t associated, const hidl_string& ssid,
        const hidl_string& apMac) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_WIFI_ASSOCIATED, true, 4,
            ifName.c_str(), std::to_string(associated).c_str(), ssid.c_str(), apMac.c_str());
    return Void();
}

Return<void> RadioImpl::setWifiIpAddress(int32_t serial,
        const hidl_string& ifName, const hidl_string& ipv4Addr, const hidl_string& ipv6Addr) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_WIFI_IP_ADDRESS, true, 3,
            ifName.c_str(), ipv4Addr.c_str(), ipv6Addr.c_str());
    return Void();
}

Return<void> RadioImpl::setLocationInfo(int32_t serial,
        const hidl_string& accountId, const hidl_string& broadcastFlag, const hidl_string& latitude,
        const hidl_string& longitude, const hidl_string& accuracy, const hidl_string& method,
        const hidl_string& city, const hidl_string& state, const hidl_string& zip,
        const hidl_string& countryCode, const hidl_string& ueWlanMac) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_GEO_LOCATION, true, 11,
            accountId.c_str(),
            broadcastFlag.c_str(),
            latitude.c_str(),
            longitude.c_str(),
            accuracy.c_str(),
            method.c_str(),
            city.c_str(),
            state.c_str(),
            zip.c_str(),
            countryCode.c_str(),
            ueWlanMac.c_str());
    return Void();
}

Return<void> RadioImpl::setEmergencyAddressId(int32_t serial,
    const hidl_string& aid) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_EMERGENCY_ADDRESS_ID, true, 1, aid.c_str());
    return Void();
}

Return<void> RadioImpl::setNattKeepAliveStatus(int32_t serial,
        const hidl_string& ifName, bool enable,
        const hidl_string& srcIp, int32_t srcPort,
        const hidl_string& dstIp, int32_t dstPort) {
    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_SET_NATT_KEEP_ALIVE_STATUS, true, 6,
            ifName.c_str(),
            enable ? "1" : "0",
            srcIp.c_str(),
            std::to_string(srcPort).c_str(),
            dstIp.c_str(),
            std::to_string(dstPort).c_str());
    return Void();
}

Return<void> RadioImpl::setWifiPingResult(int32_t serial, int32_t rat,
        int32_t latency, int32_t pktloss) {

    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_WIFI_PING_RESULT, 3,
                 rat,
                 latency,
                 pktloss);

    return Void();
}

Return<void> RadioImpl::notifyEPDGScreenState(int32_t serial, int32_t state) {
    return Void();
}

int radio::setWifiEnabledResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setWifiEnabledResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setWifiAssociatedResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setWifiAssociatedResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setWifiSignalLevelResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setWifiSignalLevelResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setWifiIpAddressResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setWifiIpAddressResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setLocationInfoResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setLocationInfoResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setEmergencyAddressIdResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setEmergencyAddressIdResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setNattKeepAliveStatusResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setNattKeepAliveStatusResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::setWifiPingResultResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMwi != NULL) {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMtk NOT NULL", __func__, slotId);
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMwi
                ->setWifiPingResultResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMwi == NULL", __func__, slotId);
    }
    return 0;
}

int radio::onWifiMonitoringThreshouldChanged(int slotId, int indicationType,
        int token, RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indStgs;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        mtkLogE(LOG_TAG, "onWifiMonitoringThreshouldChanged responselen: %d, sizeof(int): %lu, numInts: %d",
                responselen, (unsigned long)sizeof(int), numInts);
        indStgs.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indStgs[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi
                ->onWifiMonitoringThreshouldChanged(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWifiPdnActivate(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indStgs;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        indStgs.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indStgs[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWifiPdnActivate(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWfcPdnError(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indStgs;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        mtkLogE(LOG_TAG, "onWfcPdnError responselen: %d, sizeof(int): %lu, numInts: %d",
                responselen, (unsigned long)sizeof(int), numInts);
        indStgs.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indStgs[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWfcPdnError(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onPdnHandover(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indStgs;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        mtkLogE(LOG_TAG, "onPdnHandover responselen: %d, sizeof(int): %lu, numInts: %d",
                responselen, (unsigned long)sizeof(int), numInts);
        indStgs.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indStgs[i] = (int32_t) pInt[i];
            mtkLogE(LOG_TAG, "onPdnHandover indStgs[%d]: %d", i, indStgs[i]);
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onPdnHandover(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWifiRoveout(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<hidl_string> indStgs;
        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        indStgs.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            indStgs[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogE(LOG_TAG, "onWifiRoveout indStgs[%d]: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWifiRoveout(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onLocationRequest(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<hidl_string> indStgs;
        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        indStgs.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            indStgs[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogE(LOG_TAG, "onLocationRequest indStgs[%d]: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onLocationRequest(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWfcPdnStateChanged(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(int) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indStgs;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        indStgs.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indStgs[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWfcPdnStateChanged(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onNattKeepAliveChanged(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<hidl_string> indStgs;
        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        indStgs.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            indStgs[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogE(LOG_TAG, "onNattKeepAliveChanged indStgs[%d]: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onNattKeepAliveChanged(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWifiPingRequest(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }

        hidl_vec<int32_t> indPing;
        int *pInt = (int *) response;
        int numInts = responselen / sizeof(int);
        mtkLogE(LOG_TAG, "onWifiPingRequest responselen: %d, sizeof(int): %lu, numInts: %d",
                responselen, (unsigned long)sizeof(int), numInts);
        indPing.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indPing[i] = (int32_t) pInt[i];
            mtkLogE(LOG_TAG, "onWifiPingRequest indPing[%d]: %d", i, indPing[i]);
        }

        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWifiPingRequest(
                convertIntToRadioIndicationType(indicationType), indPing);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

int radio::onWifiPdnOOS(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responselen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationMwi != NULL) {
        if (response == NULL || responselen % sizeof(char *) != 0) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<hidl_string> indStgs;
        char **resp = (char **) response;
        int numStrings = responselen / sizeof(char *);
        indStgs.resize(numStrings);
        for (int i = 0; i < numStrings; i++) {
            indStgs[i] = convertCharPtrToHidlString(resp[i]);
            mtkLogE(LOG_TAG, "onWifiPdnOOS indStgs[%d]: %s", i, resp[i]);
        }
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationMwi->onWifiPdnOOS(
                convertIntToRadioIndicationType(indicationType), indStgs);
        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMwi == NULL", __func__, imsSlotId);
    }

    return 0;
}

Return<void> RadioImpl::setE911State(int32_t serial, int32_t state) {
    mtkLogD(LOG_TAG, "setE911State: serial %d, state %d", serial, state);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_E911_STATE, 1, state);
    return Void();
}

int radio::setE911StateResponse(int slotId, int responseType, int serial, RIL_Errno e,
        void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setE911StateResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
            = radioService[slotId]->mRadioResponseMtk->setE911StateResponse(
                    responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setE911StateResponse: radioService[%d]->mRadioResponseMtk == NULL",
                slotId);
    }

    return 0;
}

/// @}

Return<void> RadioImpl::setServiceStateToModem(int32_t serial, int32_t voiceRegState,
            int32_t dataRegState, int32_t voiceRoamingType, int32_t dataRoamingType,
            int32_t rilVoiceRegState, int32_t rilDataRegState) {

    mtkLogD(LOG_TAG, "%s: serial %d", __func__, serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SERVICE_STATE, 6,
                 voiceRegState,
                 dataRegState,
                 voiceRoamingType,
                 dataRoamingType,
                 rilVoiceRegState,
                 rilDataRegState);
    return Void();
}

int radio::setServiceStateToModemResponse(int slotId, int responseType, int serial,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, err);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk
                ->setServiceStateToModemResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioResponseMtk == NULL", __func__, slotId);
    }
    return 0;
}

int radio::onTxPowerIndication(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indTxPower;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        mtkLogE(LOG_TAG, "onTxPowerIndication responselen: %d, sizeof(int): %lu, numInts: %d",
                responseLen, (unsigned long)sizeof(int), numInts);
        indTxPower.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indTxPower[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk
                ->onTxPowerIndication(
                convertIntToRadioIndicationType(indicationType), indTxPower);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMtk == NULL", __func__, slotId);
    }
    return 0;
}

int radio::onTxPowerStatusIndication(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "%s: invalid response", __func__);
            return 0;
        }
        hidl_vec<int32_t> indTxPower;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        mtkLogE(LOG_TAG, "onTxPowerIndication responselen: %d, sizeof(int): %lu, numInts: %d",
                responseLen, (unsigned long)sizeof(int), numInts);
        indTxPower.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            indTxPower[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk
                ->onTxPowerStatusIndication(
                convertIntToRadioIndicationType(indicationType), indTxPower);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "%s: radioService[%d]->mRadioIndicationMtk == NULL", __func__, slotId);
    }
    return 0;
}

Return<void> RadioImpl::startNetworkScan_1_2(int32_t serial,
        const AOSP_V1_2::NetworkScanRequest& request) {
    mtkLogD(LOG_TAG, "startNetworkScan: serial %d", serial);

    RequestInfo *pRI = android::addRequestToList(serial, mSlotId, RIL_REQUEST_START_NETWORK_SCAN);
    if (pRI == NULL) {
        return Void();
    }

    if (request.specifiers.size() > MAX_RADIO_ACCESS_NETWORKS) {
        sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
        return Void();
    }

    RIL_NetworkScanRequest scan_request = {};

    scan_request.type = (RIL_ScanType) request.type;
    mtkLogD(LOG_TAG, "startNetworkScan type: %d", scan_request.type);
    scan_request.interval = request.interval;
    mtkLogD(LOG_TAG, "startNetworkScan interval: %d", scan_request.interval);
    scan_request.specifiers_length = request.specifiers.size();
    mtkLogD(LOG_TAG, "startNetworkScan specifiers_length: %d", scan_request.specifiers_length);

    for (size_t i = 0; i < request.specifiers.size(); ++i) {
        if (request.specifiers[i].geranBands.size() > MAX_BANDS ||
            request.specifiers[i].utranBands.size() > MAX_BANDS ||
            request.specifiers[i].eutranBands.size() > MAX_BANDS ||
            request.specifiers[i].channels.size() > MAX_CHANNELS) {
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            return Void();
        }
        const AOSP_V1_1::RadioAccessSpecifier& ras_from =
                request.specifiers[i];
        RIL_RadioAccessSpecifier& ras_to = scan_request.specifiers[i];

        ras_to.radio_access_network = (RIL_RadioAccessNetworks) ras_from.radioAccessNetwork;
        mtkLogD(LOG_TAG, "startNetworkScan specifiers[%d].radio_access_network: %d", i, ras_to.radio_access_network);
        ras_to.channels_length = ras_from.channels.size();
        mtkLogD(LOG_TAG, "startNetworkScan specifiers[%d].channels_length: %d", i, ras_to.channels_length);
        std::copy(ras_from.channels.begin(), ras_from.channels.end(), ras_to.channels);

        const std::vector<uint32_t> * bands = nullptr;
        switch (request.specifiers[i].radioAccessNetwork) {
            case AOSP_V1_1::RadioAccessNetworks::GERAN:
                ras_to.bands_length = ras_from.geranBands.size();
                mtkLogD(LOG_TAG, "startNetworkScan specifiers[%d].bands_length: %d", i, ras_to.bands_length);
                bands = (std::vector<uint32_t> *) &ras_from.geranBands;
                break;
            case AOSP_V1_1::RadioAccessNetworks::UTRAN:
                ras_to.bands_length = ras_from.utranBands.size();
                bands = (std::vector<uint32_t> *) &ras_from.utranBands;
                break;
            case AOSP_V1_1::RadioAccessNetworks::EUTRAN:
                ras_to.bands_length = ras_from.eutranBands.size();
                bands = (std::vector<uint32_t> *) &ras_from.eutranBands;
                break;
            default:
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
        }
        // safe to copy to geran_bands because it's a union member
        for (size_t idx = 0; idx < ras_to.bands_length; ++idx) {
            ras_to.bands.geran_bands[idx] = (RIL_GeranBands) (*bands)[idx];
            mtkLogD(LOG_TAG, "startNetworkScan specifiers[%d].bands[%d]: %d", i, idx, ras_to.bands.geran_bands[idx]);
        }
    }
    scan_request.maxSearchTime = request.maxSearchTime;
    mtkLogD(LOG_TAG, "startNetworkScan maxSearchTime: %d", scan_request.maxSearchTime);
    scan_request.incrementalResults = request.incrementalResults ? 1 : 0;
    mtkLogD(LOG_TAG, "startNetworkScan incrementalResults: %d", scan_request.incrementalResults);
    scan_request.incrementalResultsPeriodicity = request.incrementalResultsPeriodicity;
    mtkLogD(LOG_TAG, "startNetworkScan incrementalResultsPeriodicity: %d", scan_request.incrementalResultsPeriodicity);
    scan_request.mccMncs_length = request.mccMncs.size();
    mtkLogD(LOG_TAG, "startNetworkScan mccMncs_length: %d", scan_request.mccMncs_length);
    scan_request.mccMncs = (char**) calloc(scan_request.mccMncs_length, sizeof(char*));
    if (scan_request.mccMncs != NULL) {
        for (size_t i = 0; i < scan_request.mccMncs_length; i++) {
            if (!copyHidlStringToRil(&(scan_request.mccMncs[i]), request.mccMncs[i], pRI)) {
                // clear the memory for previous results
                for (size_t j = i-1; j >= 0 ; j--) {
                    if (scan_request.mccMncs[j]) {
                        memsetAndFreeStrings(1, scan_request.mccMncs[j]);
                    }
                }
                if (scan_request.mccMncs) free(scan_request.mccMncs);
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
            }
            mtkLogD(LOG_TAG, "startNetworkScan mccMncs[%d]: %s", i, scan_request.mccMncs[i]);
        }
    } else {
        mtkLogD(LOG_TAG, "startNetworkScan mccMncs calloc fail");
    }

    CALL_ONREQUEST(RIL_REQUEST_START_NETWORK_SCAN, &scan_request, sizeof(scan_request), pRI,
            mSlotId);

    if (scan_request.mccMncs) {
        for (size_t i = 0; i < scan_request.mccMncs_length; i++) {
            if (scan_request.mccMncs[i]) {
                memsetAndFreeStrings(1, scan_request.mccMncs[i]);
                scan_request.mccMncs[i] = NULL;
            }
        }
        free(scan_request.mccMncs);
    }
    scan_request.mccMncs = NULL;
    return Void();
}

Return<void> RadioImpl::setIndicationFilter_1_2(int32_t serial, int32_t indicationFilter) {
    mtkLogD(LOG_TAG, "setIndicationFilter: serial %d", serial);
    if (s_vendorFunctions->version < 15) {
        RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                RIL_REQUEST_SET_UNSOLICITED_RESPONSE_FILTER);
        if (pRI != NULL) {
            mtkLogV(LOG_TAG, "setIndicationFilter_1_2: not supported.");
            sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
        }
        return Void();
    }
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_UNSOLICITED_RESPONSE_FILTER, 1, indicationFilter);
    return Void();
}

Return<void> RadioImpl::setSignalStrengthReportingCriteria(int32_t serial, int32_t hysteresisMs,
        int32_t hysteresisDb, const hidl_vec<int32_t>& thresholdsDbm,
        AOSP_V1_2::AccessNetwork accessNetwork) {
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_SET_SIGNAL_STRENGTH_REPORTING_CRITERIA);

    if (pRI == NULL) {
        mtkLogE(LOG_TAG, "setSignalStrengthReportingCriteria: pRI is NULL.");
        return Void();
    }

    //  M: For VTS error check.
    if (thresholdsDbm.size() > 1) {
        for (size_t i = 0; i < (thresholdsDbm.size()-1); i++) {
            if (hysteresisDb > std::abs((int)thresholdsDbm[i+1]-thresholdsDbm[i])) {
                mtkLogE(LOG_TAG, "incrementalResultsPeriodicity:%d", thresholdsDbm[i], thresholdsDbm[i+1]);
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                return Void();
            }
        }
    }

    //  M: For VTS request.
    if (hysteresisMs == 5000 ||
            (hysteresisMs == 0 && hysteresisDb == 0)) {
        sendErrorResponse(pRI, RIL_E_SUCCESS);
        return Void();
    }

    sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
    return Void();
}

Return<void> RadioImpl::setLinkCapacityReportingCriteria(int32_t serial, int32_t hysteresisMs,
        int32_t hysteresisDlKbps, int32_t hysteresisUlKbps,
        const hidl_vec<int32_t>& thresholdsDownlinkKbps, const hidl_vec<int32_t>& thresholdsUplinkKbps,
        AOSP_V1_2::AccessNetwork accessNetwork) {
    int numOfDlThreshold = thresholdsDownlinkKbps.size() > MAX_LCE_THRESHOLD_NUMBER ?
            MAX_LCE_THRESHOLD_NUMBER : thresholdsDownlinkKbps.size();
    int numOfUlThreshold = thresholdsUplinkKbps.size() > MAX_LCE_THRESHOLD_NUMBER ?
            MAX_LCE_THRESHOLD_NUMBER : thresholdsUplinkKbps.size();
    RIL_LinkCapacityReportingCriteria *data = NULL;
    RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
            RIL_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA);
    if (pRI == NULL) {
        mtkLogE(LOG_TAG, "setLinkCapacityReportingCriteria: pRI memory allocation failed for request %s",
                requestToString(RIL_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA));
        return Void();
    }

    // For VTS invalid arguments check. @{
    for (int i = 1; numOfDlThreshold > 1 && i < numOfDlThreshold; i++) {
        if (hysteresisDlKbps > std::abs(thresholdsDownlinkKbps[i]-thresholdsDownlinkKbps[i-1])) {
            mtkLogE(LOG_TAG, "setLinkCapacityReportingCriteria: hysteresisDlKbps(%d) too big"
                    " for downlink thresholds delta", hysteresisDlKbps);
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            return Void();
        }
    }
    for (int i = 1; numOfUlThreshold > 1 && i < numOfUlThreshold; i++) {
        if (hysteresisUlKbps > std::abs(thresholdsUplinkKbps[i]-thresholdsUplinkKbps[i-1])) {
            mtkLogE(LOG_TAG, "setLinkCapacityReportingCriteria: hysteresisUlKbps(%d) too big"
                    " for uplink thresholds delta", hysteresisUlKbps);
            sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            return Void();
        }
    }
    // @}

    // For VTS requirements. @{
    if (((hysteresisMs == 0 && hysteresisDlKbps == 0 && hysteresisUlKbps == 0) ||
            (hysteresisMs == 5000 && hysteresisDlKbps == 500 && hysteresisUlKbps == 100)) &&
            (accessNetwork == AOSP_V1_2::AccessNetwork::GERAN)) {
        mtkLogI(LOG_TAG, "setLinkCapacityReportingCriteria: pass vts verification test");
        sendErrorResponse(pRI, RIL_E_SUCCESS);
        return Void();
    }
    // @}

    data = (RIL_LinkCapacityReportingCriteria *) calloc(1, sizeof(RIL_LinkCapacityReportingCriteria));
    if (data == NULL) {
        mtkLogE(LOG_TAG, "setLinkCapacityReportingCriteria: data memory allocation failed for request %s",
                requestToString(pRI->pCI->requestNumber));
        sendErrorResponse(pRI, RIL_E_NO_MEMORY);
        return Void();
    }

    data->hysteresisMs = hysteresisMs;
    data->hysteresisDlKbps = hysteresisDlKbps;
    data->hysteresisUlKbps = hysteresisUlKbps;
    data->thresholdDlKbpsNumber = numOfDlThreshold;
    for (int i = 0; i < numOfDlThreshold; i++) {
        data->thresholdDlKbpsList[i] = thresholdsDownlinkKbps[i];
    }
    data->thresholdUlKbpsNumber = numOfUlThreshold;
    for (int i = 0; i < numOfUlThreshold; i++) {
        data->thresholdUlKbpsList[i] = thresholdsUplinkKbps[i];
    }
    data->accessNetwork = (int)accessNetwork;

    CALL_ONREQUEST(RIL_REQUEST_SET_LINK_CAPACITY_REPORTING_CRITERIA, data,
            sizeof(RIL_LinkCapacityReportingCriteria), pRI, mSlotId);

    free(data);
    return Void();
}

Return<void> RadioImpl::setupDataCall_1_2(int32_t serial,
        AOSP_V1_2::AccessNetwork accessNetwork,
        const DataProfileInfo& dataProfileInfo, bool modemCognitive, bool roamingAllowed,
        bool isRoaming, AOSP_V1_2::DataRequestReason reason,
        const hidl_vec<hidl_string>& addresses, const hidl_vec<hidl_string>& dnses) {
    // TODO: one day maybe we'll need to pass addresses and dnses to vendor ril.

    mtkLogD(LOG_TAG, "setupDataCall_1_2: serial %d", serial);

    if (s_vendorFunctions->version >= 4 && s_vendorFunctions->version <= 14) {
        const hidl_string &protocol =
                (isRoaming ? dataProfileInfo.roamingProtocol : dataProfileInfo.protocol);
        dispatchStrings(serial, mSlotId, RIL_REQUEST_SETUP_DATA_CALL, false, 7,
            std::to_string((int) accessNetwork).c_str(),
            std::to_string((int) dataProfileInfo.profileId).c_str(),
            dataProfileInfo.apn.c_str(),
            dataProfileInfo.user.c_str(),
            dataProfileInfo.password.c_str(),
            std::to_string((int) dataProfileInfo.authType).c_str(),
            protocol.c_str());
    } else if (s_vendorFunctions->version >= 15) {
        char *mvnoTypeStr = NULL;
        if (!convertMvnoTypeToString(dataProfileInfo.mvnoType, mvnoTypeStr)) {
            RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                    RIL_REQUEST_SETUP_DATA_CALL);
            if (pRI != NULL) {
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
            }
            return Void();
        }
        if (isMtkFwkAddonNotExisted(mSlotId)) {
            setDataProfileEx(serial, dataProfileInfo, isRoaming, mSlotId);
        }
        dispatchStrings(serial, mSlotId, RIL_REQUEST_SETUP_DATA_CALL, false, 16,
            std::to_string((int) accessNetwork).c_str(),
            std::to_string((int) dataProfileInfo.profileId).c_str(),
            dataProfileInfo.apn.c_str(),
            dataProfileInfo.user.c_str(),
            dataProfileInfo.password.c_str(),
            std::to_string((int) dataProfileInfo.authType).c_str(),
            dataProfileInfo.protocol.c_str(),
            dataProfileInfo.roamingProtocol.c_str(),
            std::to_string(dataProfileInfo.supportedApnTypesBitmap).c_str(),
            std::to_string(dataProfileInfo.bearerBitmap).c_str(),
            modemCognitive ? "1" : "0",
            std::to_string(dataProfileInfo.mtu).c_str(),
            mvnoTypeStr,
            dataProfileInfo.mvnoMatchData.c_str(),
            roamingAllowed ? "1" : "0",
            std::to_string((int) reason).c_str());
    } else {
        mtkLogE(LOG_TAG, "Unsupported RIL version %d, min version expected 4", s_vendorFunctions->version);
        RequestInfo *pRI = android::addRequestToList(serial, mSlotId,
                RIL_REQUEST_SETUP_DATA_CALL);
        if (pRI != NULL) {
            sendErrorResponse(pRI, RIL_E_REQUEST_NOT_SUPPORTED);
        }
    }
    return Void();
}

Return<void> RadioImpl::deactivateDataCall_1_2(int32_t serial, int32_t cid,
        AOSP_V1_2::DataRequestReason reason) {
    mtkLogD(LOG_TAG, "deactivateDataCall_1_2: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_DEACTIVATE_DATA_CALL, false,
            2, (std::to_string(cid)).c_str(), (std::to_string((int) reason)).c_str());
    return Void();
}

Return<void> RadioImpl::reportAirplaneMode(int32_t serial, int32_t on) {
    mtkLogI(LOG_TAG, "reportAirplaneMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_REPORT_AIRPLANE_MODE, 1, on);
    return Void();
}

Return<void> RadioImpl::reportSimMode(int32_t serial, int32_t simMode) {
    mtkLogI(LOG_TAG, "reportSimMode: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_REPORT_SIM_MODE, 1, simMode);
    return Void();
}

Return<void> RadioImpl::setSilentReboot (int32_t serial, int32_t enable) {
    mtkLogI(LOG_TAG, "setSilentReboot: serial %d", serial);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_SILENT_REBOOT, 1, enable);
    return Void();
}

Return<void> RadioImpl::setTxPowerStatus(int32_t serial, int32_t mode) {
    mtkLogI(LOG_TAG, "setTxPowerStatus: serial %d, enale:%d", serial, mode);
    dispatchInts(serial, mSlotId, RIL_REQUEST_SET_TX_POWER_STATUS, 1, mode);
    return Void();
}

int radio::setSignalStrengthReportingCriteriaResponse(int slotId,
                                 int responseType, int serial, RIL_Errno e,
                                 void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setSignalStrengthReportingCriteriaResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponse != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseV1_2->setSignalStrengthReportingCriteriaResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSignalStrengthReportingCriteriaResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::currentPhysicalChannelConfigs(int slotId,
                                    int indicationType, int token, RIL_Errno e,
                                    void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_2!= NULL) {
        if (response == NULL || responseLen != 4*sizeof(int)) {
            mtkLogE(LOG_TAG, "currentPhysicalChannelConfigs: invalid response");
            return 0;
        }

        hidl_vec<AOSP_V1_2::PhysicalChannelConfig> physicalChannelConfig = {};

        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        int numConfigs = numInts / 2;
        int num_valid_scell = 0;
        // valid scell should be in front of invalid scell.
        // ex. pscell, valid scell, invalid scell. OK
        // ex. pscell, invalid scell, valid scell. Wrong
        for (int i = 1; i < numConfigs; i++) {
            if (pInt[i*2] > 0) num_valid_scell++;
        }
        // prepare for one pcell and valid scells. report pcell always.
        physicalChannelConfig.resize(1 + num_valid_scell);
        for (int i = 0; i < (num_valid_scell+1); i++) {
            physicalChannelConfig[i].status = (AOSP_V1_2::CellConnectionStatus)(pInt[i*2]);
            physicalChannelConfig[i].cellBandwidthDownlink = (int32_t)(pInt[(i*2)+1]);
        }

        mtkLogD(LOG_TAG, "currentPhysicalChannelConfigs");
        Return<void> retStatus = radioService[slotId]->mRadioIndicationV1_2->currentPhysicalChannelConfigs(
                convertIntToRadioIndicationType(indicationType), physicalChannelConfig);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "currentPhysicalChannelConfigs: radioService[%d]->mRadioIndication == NULL",
                slotId);
    }

    return 0;
}

int radio::setLinkCapacityReportingCriteriaResponse(int slotId, int responseType,
                                  int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setLinkCapacityReportingCriteriaResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseV1_2 != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseV1_2->setLinkCapacityReportingCriteriaResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "setLinkCapacityReportingCriteriaResponse: radioService[%d] or mRadioIndicationV1_2 == NULL",
                slotId);
    }

    return 0;

}

int radio::currentLinkCapacityEstimate(int slotId, int indicationType, int token,
                                  RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationV1_2 != NULL) {
        if (response == NULL || responseLen % sizeof(RIL_LinkCapacityEstimate) != 0) {
            mtkLogE(LOG_TAG, "currentLinkCapacityEstimate: invalid response");
            return 0;
        }
        AOSP_V1_2::LinkCapacityEstimate lce;
        RIL_LinkCapacityEstimate *resp = (RIL_LinkCapacityEstimate *) response;
        lce.downlinkCapacityKbps = resp->downlinkCapacityKbps;
        lce.uplinkCapacityKbps = resp->uplinkCapacityKbps;
        Return<void> retStatus = radioService[slotId]->mRadioIndicationV1_2->
                currentLinkCapacityEstimate(convertIntToRadioIndicationType(indicationType), lce);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
                "currentLinkCapacityEstimate: radioService[%d] or mRadioIndicationV1_2 == NULL",
                slotId);
    }

    return 0;
}

int radio::onImsConferenceInfoIndication(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_vec<ImsConfParticipant> participants;
        if (response == NULL || responseLen % sizeof(RIL_Conference_Participants) != 0) {
            mtkLogE(LOG_TAG, "imsConferenceInfoIndication Invalid response: NULL");
            return 0;
        } else {
            int num = responseLen / sizeof(RIL_Conference_Participants);
            participants.resize(num);
            for (int i = 0; i< num; i++) {
                RIL_Conference_Participants p_cur = ((RIL_Conference_Participants *) response)[i];
                participants[i].user_addr = convertCharPtrToHidlString(p_cur.useraddr);
                participants[i].end_point = convertCharPtrToHidlString(p_cur.end_point);
                participants[i].entity = convertCharPtrToHidlString(p_cur.entity);
                participants[i].display_text = convertCharPtrToHidlString(p_cur.display_text);
                participants[i].status = convertCharPtrToHidlString(p_cur.status);
            }
        }

        mtkLogD(LOG_TAG, "imsConferenceInfoIndication");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->imsConferenceInfoIndication(
                                 convertIntToRadioIndicationType(indicationType), participants);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsConferenceInfoIndication: radioService[%d]->mRadioIndicationIms == NULL",
                                                                              imsSlotId);
    }

    return 0;
}

int radio::onLteMessageWaitingIndication(int slotId, int indicationType, int token,
        RIL_Errno err, void *response, size_t responseLen) {

    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL
            && radioService[imsSlotId]->mRadioIndicationIms != NULL) {

        hidl_string callId;
        hidl_string pType;
        hidl_string urcIdx;
        hidl_string totalUrcCount;
        hidl_string rawData;
        int numStrings = responseLen / sizeof(char *);

        if (response == NULL || numStrings < 5) {
            mtkLogE(LOG_TAG, "lteMessageWaitingIndication Invalid response: NULL");
            return 0;
        } else {
            char **resp = (char **) response;
            callId = convertCharPtrToHidlString(resp[0]);
            pType = convertCharPtrToHidlString(resp[1]);
            urcIdx = convertCharPtrToHidlString(resp[2]);
            totalUrcCount = convertCharPtrToHidlString(resp[3]);
            rawData = convertCharPtrToHidlString(resp[4]);
        }

        mtkLogD(LOG_TAG, "lteMessageWaitingIndication");
        Return<void> retStatus = radioService[imsSlotId]->
                                 mRadioIndicationIms->lteMessageWaitingIndication(
                                 convertIntToRadioIndicationType(indicationType),
                                 callId, pType, urcIdx, totalUrcCount, rawData);

        radioService[imsSlotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "lteMessageWaitingIndication: radioService[%d]->mRadioIndicationIms == NULL",
                                                                              imsSlotId);
    }

    return 0;
}

///M:Dialog Event Package Info @{
int radio::imsDialogIndicationInd(int slotId, int indicationType, int token, RIL_Errno e,
        void *response, size_t responseLen) {
    int imsSlotId = toImsSlot(slotId);
    if (radioService[imsSlotId] != NULL && radioService[imsSlotId]->mRadioIndicationIms != NULL) {
        mtkLogE(LOG_TAG, "imsDialogIndicationInd: response:%p, responseLen:%d",response,responseLen);
        if ((response == NULL && responseLen != 0) || responseLen % sizeof(RIL_DialogInfo*) != 0) {
            mtkLogE(LOG_TAG, "imsDialogIndicationInd: invalid response");
            return 0;
        }

        hidl_vec < Dialog > dialogList;
        int num = responseLen / sizeof(RIL_DialogInfo*);
        mtkLogE(LOG_TAG, "imsDialogIndicationInd num:%d", num);

        dialogList.resize(num);
        for (int i = 0; i < num; i++) {
            RIL_DialogInfo *dialog = ((RIL_DialogInfo **) response)[i];
            dialogList[i].dialogId = dialog->dialogId;
            dialogList[i].callState = dialog->callState;
            dialogList[i].callType = dialog->callType;
            dialogList[i].isCallHeld = dialog->isCallHeld;
            dialogList[i].isPullable = dialog->isPullable;
            dialogList[i].address = convertCharPtrToHidlString(dialog->address);
            mtkLogE(LOG_TAG,"imsDialogIndicationInd: dialog[%d] Id:%d", i, dialogList[i].dialogId);
        }

        mtkLogE(LOG_TAG, "imsDialogIndicationInd");
        Return<void> retStatus = radioService[imsSlotId]->mRadioIndicationIms->imsDialogIndication(
                convertIntToRadioIndicationType(indicationType), dialogList);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "imsDialogIndicationInd: radioService[%d]->mRadioIndicationIms == NULL",
                imsSlotId);
    }

    return 0;
}
///@}

// PS/CS attach request.
Return<void> RadioImpl::dataConnectionAttach(int32_t serial, int type) {
    mtkLogD(LOG_TAG, "dataConnectionAttach: serial %d type %d", serial, type);
    dispatchInts(serial, mSlotId, RIL_REQUEST_DATA_CONNECTION_ATTACH, 1, type);
    return Void();
}

// PS/CS attach response.
int radio::dataConnectionAttachResponse(int slotId, int responseType,
                      int serial, RIL_Errno e, void *response, size_t responselen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->dataConnectionAttachResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "dataConnectionAttachResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

// PS/CS detach request.
Return<void> RadioImpl::dataConnectionDetach(int32_t serial, int type) {
    mtkLogD(LOG_TAG, "dataConnectionDetach: serial %d type %d", serial, type);
    dispatchInts(serial, mSlotId, RIL_REQUEST_DATA_CONNECTION_DETACH, 1, type);
    return Void();
}

// PS/CS detach response.
int radio::dataConnectionDetachResponse(int slotId, int responseType,
                      int serial, RIL_Errno e, void *response, size_t responselen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->dataConnectionDetachResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "dataConnectionDetachResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

// Cleanup all connections request.
Return<void> RadioImpl::resetAllConnections(int32_t serial) {
    mtkLogD(LOG_TAG, "resetAllConnections: serial %d", serial);
    dispatchVoid(serial, mSlotId, RIL_REQUEST_RESET_ALL_CONNECTIONS);
    return Void();
}

// Cleanup all connections response.
int radio::resetAllConnectionsResponse(int slotId, int responseType,
                      int serial, RIL_Errno e, void *response, size_t responselen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->resetAllConnectionsResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "resetAllConnectionsResponse: radioService[%d]->mRadioResponse == NULL",
                slotId);
    }

    return 0;
}

int radio::reportAirplaneModeResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reportAirplaneModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->reportAirplaneModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "reportAirplaneModeResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }
    return 0;
}

int radio::reportSimModeResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reportSimModeResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->reportSimModeResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "reportSimModeResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }
    return 0;
}

int radio::setSilentRebootResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "reportSimModeRsp: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->setSilentRebootResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSilentRebootResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }
    return 0;
}

int radio::setTxPowerStatusResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setTxPowerStatusResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->setTxPowerStatusResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setSilentRebootResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }
    return 0;
}

int radio::setPropImsHandoverResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "setPropImsHandoverResponse: serial %d", serial);

    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus =
                radioService[slotId]->mRadioResponseMtk->setPropImsHandoverResponse(responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "setPropImsHandoverResponse: radioService[%d]->mRadioResponseMtk == NULL", slotId);
    }
    return 0;
}

int radio::setOperatorConfigurationResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setOperatorConfigurationResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
            "setOperatorConfigurationResponse: radioService[%d]->mRadioResponseMtk=NULL",slotId);
    }
    return 0;
}

int radio::setSuppServPropertyResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->setSuppServPropertyResponse(
                responseInfo);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
            "setSuppServPropertyResponse: radioService[%d]->mRadioResponseMtk=NULL",slotId);
    }
    return 0;
}

int radio::getSuppServPropertyResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId]->mRadioResponseMtk != NULL) {
        RadioResponseInfo responseInfo = {};
        populateResponseInfo(responseInfo, serial, responseType, e);
        Return<void> retStatus
                = radioService[slotId]->mRadioResponseMtk->getSuppServPropertyResponse(
                responseInfo, convertCharPtrToHidlString((char *) response));
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG,
            "getSuppServPropertyResponse: radioService[%d]->mRadioResponseMtk=NULL",slotId);
    }
    return 0;
}

bool isMtkFwkAddonNotExisted(int slotId) {
    return (radioService[slotId]->mRadioResponseMtk == NULL) &&
            (radioService[slotId]->mRadioIndicationMtk == NULL);
}

void setDataProfileEx(int32_t serial, const DataProfileInfo& profiles, bool isRoaming, int slotId) {
    mtkLogI(LOG_TAG, "setDataProfileEx: serial %d", serial);
    RequestInfo *pRI = android::addRequestToList(serial, slotId, RIL_REQUEST_SET_DATA_PROFILE_EX);
    if (pRI == NULL) {
        return;
    }

    size_t num = 1; // profiles.size();
    bool success = false;

    if (s_vendorFunctions->version <= 14) {

        RIL_DataProfileInfo *dataProfiles =
            (RIL_DataProfileInfo *) calloc(num, sizeof(RIL_DataProfileInfo));

        if (dataProfiles == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return;
        }

        RIL_DataProfileInfo **dataProfilePtrs =
            (RIL_DataProfileInfo **) calloc(num, sizeof(RIL_DataProfileInfo *));
        if (dataProfilePtrs == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            free(dataProfiles);
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return;
        }

        for (size_t i = 0; i < num; i++) {
            dataProfilePtrs[i] = &dataProfiles[i];

            success = copyHidlStringToRil(&dataProfiles[i].apn, profiles.apn, pRI, true);

            const hidl_string &protocol =
                    (isRoaming ? profiles.roamingProtocol : profiles.protocol);

            if (success && !copyHidlStringToRil(&dataProfiles[i].protocol, protocol, pRI, true)) {
                success = false;
            }

            if (success && !copyHidlStringToRil(&dataProfiles[i].user, profiles.user, pRI,
                    true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].password, profiles.password,
                    pRI, true)) {
                success = false;
            }

            if (!success) {
                freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 4,
                    &RIL_DataProfileInfo::apn, &RIL_DataProfileInfo::protocol,
                    &RIL_DataProfileInfo::user, &RIL_DataProfileInfo::password);
                return;
            }

            dataProfiles[i].profileId = (RIL_DataProfile) profiles.profileId;
            dataProfiles[i].authType = ((unsigned int) profiles.authType) & APN_AUTH_TYPE_MAX_NUM;
            dataProfiles[i].type = (int) profiles.type;
            dataProfiles[i].maxConnsTime = profiles.maxConnsTime;
            dataProfiles[i].maxConns = profiles.maxConns;
            dataProfiles[i].waitTime = profiles.waitTime;
            dataProfiles[i].enabled = BOOL_TO_INT(profiles.enabled);
        }

        CALL_ONREQUEST(RIL_REQUEST_SET_DATA_PROFILE_EX, dataProfilePtrs,
                num * sizeof(RIL_DataProfileInfo *), pRI, slotId);

        freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 4,
                &RIL_DataProfileInfo::apn, &RIL_DataProfileInfo::protocol,
                &RIL_DataProfileInfo::user, &RIL_DataProfileInfo::password);
    } else {
        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //RIL_DataProfileInfo_v15 *dataProfiles =
        //    (RIL_DataProfileInfo_v15 *) calloc(num, sizeof(RIL_DataProfileInfo_v15));
        RIL_MtkDataProfileInfo *dataProfiles =
            (RIL_MtkDataProfileInfo *) calloc(num, sizeof(RIL_MtkDataProfileInfo));

        if (dataProfiles == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return;
        }

        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //RIL_DataProfileInfo_v15 **dataProfilePtrs =
        //    (RIL_DataProfileInfo_v15 **) calloc(num, sizeof(RIL_DataProfileInfo_v15 *));
        RIL_MtkDataProfileInfo **dataProfilePtrs =
            (RIL_MtkDataProfileInfo **) calloc(num, sizeof(RIL_MtkDataProfileInfo *));

        if (dataProfilePtrs == NULL) {
            mtkLogE(LOG_TAG, "Memory allocation failed for request %s",
                    requestToString(pRI->pCI->requestNumber));
            free(dataProfiles);
            sendErrorResponse(pRI, RIL_E_NO_MEMORY);
            return;
        }

        for (size_t i = 0; i < num; i++) {
            dataProfilePtrs[i] = &dataProfiles[i];

            success = copyHidlStringToRil(&dataProfiles[i].apn, profiles.apn, pRI, true);
            if (success && !copyHidlStringToRil(&dataProfiles[i].protocol, profiles.protocol,
                    pRI)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].roamingProtocol,
                    profiles.roamingProtocol, pRI, true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].user, profiles.user, pRI,
                    true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].password, profiles.password,
                    pRI, true)) {
                success = false;
            }
            if (success && !copyHidlStringToRil(&dataProfiles[i].mvnoMatchData,
                    profiles.mvnoMatchData, pRI, true)) {
                success = false;
            }

            if (success && !convertMvnoTypeToString(profiles.mvnoType,
                    dataProfiles[i].mvnoType)) {
                sendErrorResponse(pRI, RIL_E_INVALID_ARGUMENTS);
                success = false;
            }

            if (!success) {
                // M: use data profile to sync apn tables to modem
                // remark AOSP
                //freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                //    &RIL_DataProfileInfo_v15::apn, &RIL_DataProfileInfo_v15::protocol,
                //    &RIL_DataProfileInfo_v15::roamingProtocol, &RIL_DataProfileInfo_v15::user,
                //    &RIL_DataProfileInfo_v15::password, &RIL_DataProfileInfo_v15::mvnoMatchData);
                freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                    &RIL_MtkDataProfileInfo::apn, &RIL_MtkDataProfileInfo::protocol,
                    &RIL_MtkDataProfileInfo::roamingProtocol, &RIL_MtkDataProfileInfo::user,
                    &RIL_MtkDataProfileInfo::password, &RIL_MtkDataProfileInfo::mvnoMatchData);
                return;
            }

            dataProfiles[i].profileId = (RIL_DataProfile) profiles.profileId;
            dataProfiles[i].authType = ((unsigned int) profiles.authType) & APN_AUTH_TYPE_MAX_NUM;
            dataProfiles[i].type = (int) profiles.type;
            dataProfiles[i].maxConnsTime = profiles.maxConnsTime;
            dataProfiles[i].maxConns = profiles.maxConns;
            dataProfiles[i].waitTime = profiles.waitTime;
            dataProfiles[i].enabled = BOOL_TO_INT(profiles.enabled);
            dataProfiles[i].supportedTypesBitmask = profiles.supportedApnTypesBitmap;
            dataProfiles[i].bearerBitmask = profiles.bearerBitmap;
            dataProfiles[i].mtu = profiles.mtu;

            // M: use data profile to sync apn tables to modem
            // set default value for inactiveTimer
            dataProfiles[i].inactiveTimer = decodeInactiveTimer((unsigned int) profiles.authType);

        }

        CALL_ONREQUEST(RIL_REQUEST_SET_DATA_PROFILE_EX, dataProfilePtrs,
                num * sizeof(RIL_MtkDataProfileInfo *), pRI, slotId);

        // M: use data profile to sync apn tables to modem
        // remark AOSP
        //freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
        //        &RIL_DataProfileInfo_v15::apn, &RIL_DataProfileInfo_v15::protocol,
        //        &RIL_DataProfileInfo_v15::roamingProtocol, &RIL_DataProfileInfo_v15::user,
        //        &RIL_DataProfileInfo_v15::password, &RIL_DataProfileInfo_v15::mvnoMatchData);
        freeSetDataProfileData(num, dataProfiles, dataProfilePtrs, 6,
                &RIL_MtkDataProfileInfo::apn, &RIL_MtkDataProfileInfo::protocol,
                &RIL_MtkDataProfileInfo::roamingProtocol, &RIL_MtkDataProfileInfo::user,
                &RIL_MtkDataProfileInfo::password, &RIL_MtkDataProfileInfo::mvnoMatchData);
    }

    return;
}

int radio::onDsbpStateChanged(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL || responseLen == 0) {
            mtkLogE(LOG_TAG,"onDsbpStateChanged: invalid indication");
            return 0;
        }

        int *state = (int *)response;
        DsbpState dsbpState = (DsbpState) state[0];
        mtkLogI(LOG_TAG, "onDsbpStateChanged slot: %d, state: %d", slotId, state[0]);
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk->dsbpStateChanged(
                convertIntToRadioIndicationType(indicationType), dsbpState);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "onDsbpStateChanged: radioService[%d]->mRadioIndicationMtk == NULL", slotId);
    }

    return 0;
}

// MTK-START: SIM SLOT LOCK
int radio::smlSlotLockInfoChangedInd(int slotId,
        int indicationType, int token, RIL_Errno e, void *response, size_t responseLen) {
    if (radioService[slotId] != NULL && radioService[slotId]->mRadioIndicationMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "smlSlotLockInfoChangedInd: invalid response!");
            return 0;
        }
        hidl_vec<int32_t> data;
        int *pInt = (int *) response;
        int numInts = responseLen / sizeof(int);
        mtkLogD(LOG_TAG, "smlSlotLockInfoChangedInd responselen: %d, sizeof(int): %lu, numInt: %d",
                responseLen, (unsigned long)sizeof(int), numInts);
        data.resize(numInts);
        for (int i = 0; i < numInts; i++) {
            data[i] = (int32_t) pInt[i];
        }
        Return<void> retStatus = radioService[slotId]->mRadioIndicationMtk
                ->smlSlotLockInfoChangedInd(convertIntToRadioIndicationType(indicationType), data);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "smlSlotLockInfoChangedInd: radioService[%d] or mRadioIndication is NULL",
                slotId);
    }
    return 0;
}

int radio::supplyDeviceNetworkDepersonalizationResponse(int slotId,
        int responseType, int serial, RIL_Errno e, void *response, size_t responseLen) {
    mtkLogD(LOG_TAG, "supplyDeviceNetworkDepersonalizationResponse: serial %d", serial);

    if (radioService[slotId] != NULL && radioService[slotId]->mRadioResponseMtk != NULL) {
        if (response == NULL) {
            mtkLogE(LOG_TAG, "supplyDeviceNetworkDepersonalizationResponse: invalid response!");
            return 0;
        }
        RadioResponseInfo responseInfo = {};
        int ret = responseIntOrEmpty(responseInfo, serial, responseType, e, response, responseLen);
        Return<void> retStatus = radioService[slotId]->mRadioResponseMtk->
                supplyDeviceNetworkDepersonalizationResponse(responseInfo, ret);
        radioService[slotId]->checkReturnStatus(retStatus);
    } else {
        mtkLogE(LOG_TAG, "supplyDeviceNetworkDepersonalizationResponse: "
                "radioService[%d]->mRadioResponse == NULL", slotId);
    }
    return 0;
}

Return<void> RadioImpl::supplyDeviceNetworkDepersonalization(int32_t serial,
        const hidl_string& netPin) {
    mtkLogD(LOG_TAG, "supplyDeviceNetworkDepersonalizationResponse: serial %d", serial);
    dispatchStrings(serial, mSlotId, RIL_REQUEST_ENTER_DEVICE_NETWORK_DEPERSONALIZATION, true,
            1, netPin.c_str());
    return Void();
}
// MTK-END
