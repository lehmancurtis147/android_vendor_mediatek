/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#ifndef _CCT_OP_DATA_H_
#define _CCT_OP_DATA_H_

typedef enum {
    CCTIF_NO_ERROR         = 0,            ///< The function work successfully
    CCTIF_UNKNOWN_ERROR    = 0x80000000,   ///< Unknown error
    CCTIF_INVALID_DRIVER   = 0x80000001,
    CCTIF_UNSUPPORT_SENSOR_TYPE = 0x80000002,
    CCTIF_BAD_CTRL_CODE,
    CCTIF_BAD_PARAM,
    CCTIF_NOT_INIT,
} CCTIF_ERROR_ENUM;



enum
{
    FT_CCT_OP_SENSOR_START  = 0x1000,
    FT_CCT_OP_3A_START      = 0x2000,
    FT_CCT_OP_ISP_START     = 0x3000,
    FT_CCT_OP_NVRAM_START   = 0x4000,
    FT_CCT_OP_CAPTURE_START = 0x5000,
    FT_CCT_OP_SHELL_START   = 0x6000,
    FT_CCT_OP_EMCAM_START   = 0x8000
};


#if 1
//updated
typedef enum
{
    FT_CCT_OP_GET_SENSOR = FT_CCT_OP_SENSOR_START                             //n             sensor
    ,FT_CCT_OP_SWITCH_SENSOR                                                //n             sensor
    ,FT_CCT_OP_SET_SENSOR_REG                                                //n                sensor
    ,FT_CCT_OP_GET_SENSOR_REG                                                //n                sensor
    ,FT_CCT_OP_LSC_GET_SENSOR_RESOLUTION                                    //n             sensor
    ,FT_CCT_OP_SENSOR_TYPE_MAX
    ,FT_CCT_OP_TEST_SET_PROP                                                //n             test only
    ,FT_CCT_OP_TEST_GET_PROP                                                //n             test only

    ,FT_CCT_OP_AE_GET_ON_OFF = FT_CCT_OP_3A_START                             //n                3a 0x2000
    ,FT_CCT_OP_AE_SET_ON_OFF                                                 //n                3a 0x2001
    ,FT_CCT_OP_AE_GET_BAND                                                    //n                3a 0x2002
    ,FT_CCT_OP_AE_SET_BAND                                                    //n                3a 0x2003
    ,FT_CCT_OP_AE_GET_METERING_MODE                                            //n                3a 0x2004
    ,FT_CCT_OP_AE_SET_METERING_MODE                                            //n                3a 0x2005
    ,FT_CCT_OP_AE_GET_SCENE_MODE                                            //n                3a 0x2006
    ,FT_CCT_OP_AE_SET_SCENE_MODE                                            //n                3a 0x2007
    ,FT_CCT_OP_AE_GET_AUTO_PARA                                                 //n                3a 0x2008
    ,FT_CCT_OP_AE_SET_AUTO_PARA                                                //n                3a 0x2009
    ,FT_CCT_OP_AE_GET_CAPTURE_PARA                                            //n                3a 0x200A
    ,FT_CCT_OP_AE_SET_CAPTURE_PARA                                            //n                3a 0x200B
    ,FT_CCT_OP_AF_GET_RANGE                                                     //n                3a 0x200C
    ,FT_CCT_OP_AF_GET_POS                                                    //n                3a 0x200D
    ,FT_CCT_OP_AF_SET_POS                                                    //n                3a 0x200E
    ,FT_CCT_OP_AWB_GET_ON_OFF                                                //n                3a 0x200F
    ,FT_CCT_OP_AWB_SET_ON_OFF                                                //n                3a 0x2010
    ,FT_CCT_OP_AWB_GET_LIGHT_PROB                                            //n                3a 0x2011
    ,FT_CCT_OP_AWB_GET_MODE                                                     //n                3a 0x2012
    ,FT_CCT_OP_AWB_SET_MODE                                                     //n                3a 0x2013
    ,FT_CCT_OP_AWB_GET_GAIN                                                    //n                3a 0x2014
    ,FT_CCT_OP_AWB_SET_GAIN                                                    //n                3a 0x2015
    ,FT_CCT_OP_FLASH_GET_MODE                                                //n                3a 0x2016
    ,FT_CCT_OP_FLASH_SET_MODE                                                //n                3a 0x2017
    ,FT_CCT_OP_3A_TYPE_MAX

    ,FT_CCT_OP_GET_ID    = FT_CCT_OP_ISP_START                                //n                isp
    ,FT_CCT_OP_ISP_GET_ON_OFF                                                 //n                isp
    ,FT_CCT_OP_ISP_SET_ON_OFF                                                 //n                isp
    ,FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF                                        //n                isp
    ,FT_CCT_OP_ISP_SET_CCM_FIXED_ON_OFF                                        //n                isp
    ,FT_CCT_OP_ISP_GET_CCM_MATRIX                                            //n                isp
    ,FT_CCT_OP_ISP_SET_CCM_MATRIX                                            //n                isp
    ,FT_CCT_OP_GET_SHADING_ON_OFF                                            //n                isp
    ,FT_CCT_OP_SET_SHADING_ON_OFF                                            //n                isp
    ,FT_CCT_OP_GET_SHADING_INDEX                                            //n                isp
    ,FT_CCT_OP_SET_SHADING_INDEX                                                //n                isp
    ,FT_CCT_OP_GET_SHADING_TSF_ON_OFF                                        //n                isp
    ,FT_CCT_OP_SET_SHADING_TSF_ON_OFF                                        //n                isp
    ,FT_CCT_OP_ISP_TYPE_MAX

    ,FT_CCT_OP_ISP_GET_NVRAM_DATA = FT_CCT_OP_NVRAM_START                    //n                isp
    ,FT_CCT_OP_ISP_SET_NVRAM_DATA                                            //n                isp
    ,FT_CCT_OP_ISP_SET_PARTIAL_NVRAM_DATA                                    //n             isp
    ,FT_CCT_OP_ISP_SAVE_NVRAM_DATA                                                //n                isp
    ,FT_CCT_OP_ISP_GET_NVRAM_STRUCT
    ,FT_CCT_OP_NVRAM_TYPE_MAX

    ,FT_CCT_OP_ISP_EXEC_SHELL_CMD = FT_CCT_OP_SHELL_START                       //n                adb shell

    ,FT_CCT_OP_AF_BRECKET_STEP = FT_CCT_OP_EMCAM_START
    ,FT_CCT_OP_AE_BRECKET_STEP
    ,FT_CCT_OP_EMCAM_TYPE_MAX

    ,FT_CCT_OP_END
} CCT_OP_ID;


#define FT_CCT_OP_SENSOR_TYPE_OP_NO            (FT_CCT_OP_SENSOR_TYPE_MAX - FT_CCT_OP_SENSOR_START)
#define FT_CCT_OP_3A_TYPE_OP_NO             (FT_CCT_OP_3A_TYPE_MAX - FT_CCT_OP_3A_START)
#define FT_CCT_OP_ISP_TYPE_OP_NO             (FT_CCT_OP_ISP_TYPE_MAX - FT_CCT_OP_ISP_START)
#define FT_CCT_OP_EMCAM_TYPE_OP_NO              (FT_CCT_OP_EMCAM_TYPE_MAX - FT_CCT_OP_EMCAM_START)

#endif


typedef enum  {
   CCT_FL_MODE_OFF = 0,
   CCT_FL_MODE_HI_TEMP,
   CCT_FL_MODE_LO_TEMP,
   CCT_FL_MODE_MIX_TEMP
}  CCT_FL_MODE_T;


typedef enum {
    CCT_NVRAM_DATA_LSC_PARA = 0
    ,CCT_NVRAM_DATA_LSC_TABLE
    ,CCT_NVRAM_DATA_LSC
    ,CCT_NVRAM_DATA_AE_PLINE
    ,CCT_NVRAM_DATA_AE
    ,CCT_NVRAM_DATA_AF
    ,CCT_NVRAM_DATA_AWB
    ,CCT_NVRAM_DATA_ISP
    ,CCT_NVRAM_DATA_FEATURE
    ,CCT_NVRAM_DATA_STROBE
    ,CCT_NVRAM_DATA_FLASH_AWB
    ,CCT_NVRAM_DATA_FLASH_CALIBRATION
    ,CCT_NVRAM_DATA_3A
    ,CCT_NVRAM_DATA_ENUM_MAX

} CCT_NVRAM_DATA_T;

typedef struct
{
    CCT_NVRAM_DATA_T    dataType;
    MINT8*                pDataBuf;
} CCT_NVRAM_DATA_SET_T;

typedef enum {
    ISP_CATEGORY_SL2F = 0,
    ISP_CATEGORY_DBS,
    ISP_CATEGORY_ADBS,
    ISP_CATEGORY_OBC,
    ISP_CATEGORY_BPC,
    ISP_CATEGORY_NR1,
    ISP_CATEGORY_PDC,
    ISP_CATEGORY_RMM,
    ISP_CATEGORY_RNR,
    ISP_CATEGORY_SL2,
    ISP_CATEGORY_UDM,
    ISP_CATEGORY_CCM,
    ISP_CATEGORY_GGM,
    ISP_CATEGORY_IHDR_GGM,
    ISP_CATEGORY_ANR,
    ISP_CATEGORY_ANR2,
    ISP_CATEGORY_CCR,
    //ISP_CATEGORY_BOK,
    ISP_CATEGORY_HFG,
    ISP_CATEGORY_EE,
    ISP_CATEGORY_NR3D,
    ISP_CATEGORY_MFB,
    ISP_CATEGORY_MIXER3,
    ISP_CATEGORY_PCA,
    ISP_CATEGORY_COLOR,
    //ISP_CATEGORY_HLR,
    ISP_CATEGORY_ABF,
    ISP_CATEGORY_MAX
}CCT_ISP_CATEGORY_T;

typedef enum {
    CCT_NVRAM_STRUCT_SIZE_SHADING = 0,
    CCT_NVRAM_STRUCT_SIZE_AE_PLINE,
    CCT_NVRAM_STRUCT_SIZE_LENS_PARA,
    CCT_NVRAM_STRUCT_SIZE_ISP_PARAM,
    CCT_NVRAM_STRUCT_SIZE_FEATURE,
    CCT_NVRAM_STRUCT_SIZE_STROBE,
    CCT_NVRAM_STRUCT_SIZE_FLASH_CALIBRATION,
    CCT_NVRAM_STRUCT_SIZE_3A,

    CCT_NVRAM_STRUCT_NUMBER_SL2F,
    CCT_NVRAM_STRUCT_NUMBER_DBS,
    CCT_NVRAM_STRUCT_NUMBER_ADBS,
    CCT_NVRAM_STRUCT_NUMBER_OBC,
    CCT_NVRAM_STRUCT_NUMBER_BNR_BPC,
    CCT_NVRAM_STRUCT_NUMBER_BNR_NR1,
    CCT_NVRAM_STRUCT_NUMBER_BNR_PDC,
    CCT_NVRAM_STRUCT_NUMBER_RMM,
    CCT_NVRAM_STRUCT_NUMBER_RNR,
    CCT_NVRAM_STRUCT_NUMBER_SL2,
    CCT_NVRAM_STRUCT_NUMBER_UDM,
    CCT_NVRAM_STRUCT_NUMBER_NBC_ANR,
    CCT_NVRAM_STRUCT_NUMBER_NBC_LCE_LINK,
    CCT_NVRAM_STRUCT_NUMBER_NBC2_ANR2,
    CCT_NVRAM_STRUCT_NUMBER_NBC2_CCR,
    CCT_NVRAM_STRUCT_NUMBER_NBC2_ABF,
    CCT_NVRAM_STRUCT_NUMBER_EE,
    CCT_NVRAM_STRUCT_NUMBER_HFG,
    CCT_NVRAM_STRUCT_NUMBER_NR3D,
    CCT_NVRAM_STRUCT_NUMBER_MFB,
    CCT_NVRAM_STRUCT_NUMBER_MIXER3,
    CCT_NVRAM_STRUCT_NUMBER_SWNR_THRES,
    CCT_NVRAM_STRUCT_NUMBER_SWNR,
    CCT_NVRAM_STRUCT_NUMBER_MFNR,
    CCT_NVRAM_STRUCT_NUMBER_NBC_TBL,
    CCT_NVRAM_STRUCT_NUMBER_FD_ANR,
    CCT_NVRAM_STRUCT_NUMBER_CLEARZOOM,
    CCT_NVRAM_STRUCT_NUMBER_CA_LTM,
    CCT_NVRAM_STRUCT_NUMBER_CCM,
    CCT_NVRAM_STRUCT_NUMBER_COLOR_PARAM,
    CCT_NVRAM_STRUCT_NUMBER_COLOR,
    CCT_NVRAM_STRUCT_NUMBER_PCA,
    CCT_NVRAM_STRUCT_NUMBER_AE,
    CCT_NVRAM_STRUCT_NUMBER_AF,
    CCT_NVRAM_STRUCT_NUMBER_AWB,
    CCT_NVRAM_STRUCT_NUMBER_FlASH_AE,
    CCT_NVRAM_STRUCT_NUMBER_FlASH_AWB,
    CCT_NVRAM_STRUCT_NUMBER_FlASH_CALIBRATION,
    CCT_NVRAM_STRUCT_NUMBER_GMA,
    CCT_NVRAM_STRUCT_NUMBER_LCE,
    CCT_NVRAM_STRUCT_NUMBER_DCE,

    CCT_NVRAM_STRUCT_MAXIMUM
}CCT_NVRAM_STRUCT_T;

typedef struct {
    MUINT32    mode;
    MUINT32    color_temperature;
    MUINT32    offset;
    MUINT32    length;
    MUINT32    table[MAX_SHADING_PvwFrm_SIZE];
} CCT_SHADING_TAB_STRUCT;

#endif
