/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
#ifndef _CCT_HANDLE_H_
#define _CCT_HANDLE_H_

#include "nvram_drv.h"

//#include "AcdkTypes.h"
#include "AcdkCctBase.h"

#include <mtkcam/drv/IHalSensor.h>


//#include "CctIF.h"   			// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk
//#include "AcdkCommon.h"		// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk

#include "cct_op_data.h"

#include "IHal3A.h"

using namespace NS3Av3;

class CctHandle
{
public:
    static CctHandle *createInstance(CAMERA_DUAL_CAMERA_SENSOR_ENUM eSensorEnum);
    void destroyInstance();

    MINT32 cct_OpDispatch(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );
	MINT32 cct_GetCctOutBufSize(CCT_OP_ID op, MINT32 dtype);
	MINT32 cct_GetCctOutBufSize(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn);
	void init();

protected:
    //CctHandle();
    CctHandle( NVRAM_CAMERA_ISP_PARAM_STRUCT* pbuf_isp, NVRAM_CAMERA_SHADING_STRUCT* pbuf_shd, NVRAM_CAMERA_3A_STRUCT* pbuf_3a, NVRAM_LENS_PARA_STRUCT* pbuf_ln, NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT* pbuf_flash_cal, NvramDrvBase*	pnvram_drv, MUINT32 isensor_dev);
	~CctHandle();

private:
    MINT32  cct_getSensorStaticInfo();
    MINT32  cct_QuerySensor(MVOID *a_pCCTSensorInfoOut, MUINT32 *pRealParaOutLen);
    MINT32  cct_GetSensorRes(MVOID *pCCTSensorResOut, MUINT32 *pRealParaOutLen);
    MINT32  cct_ReadSensorReg(MVOID *puParaIn, MVOID *puParaOut, MUINT32 *pu4RealParaOutLen);
    MINT32  cct_WriteSensorReg(MVOID *puParaIn);
    MINT32  setIspOnOff(MUINT32 const u4Category, MBOOL const fgOn);
    MINT32  getIspOnOff(MUINT32 const u4Category, MBOOL& rfgOn) const;

    MVOID   setIspOnOff_OBC(MBOOL const fgOn);
    MVOID   setIspOnOff_BPC(MBOOL const fgOn);
    MVOID   setIspOnOff_CT(MBOOL const fgOn);
    MVOID   setIspOnOff_PDC(MBOOL const fgOn);
    MVOID   setIspOnOff_SLK(MBOOL const fgOn);
    MVOID   setIspOnOff_DM(MBOOL const fgOn);
    MVOID   setIspOnOff_CCM(MBOOL const fgOn);
    MVOID   setIspOnOff_GGM(MBOOL const fgOn);
    MVOID   setIspOnOff_YNR(MBOOL const fgOn);
    MVOID   setIspOnOff_CNR(MBOOL const fgOn);
    MVOID   setIspOnOff_CCR(MBOOL const fgOn);
    MVOID   setIspOnOff_ABF(MBOOL const fgOn);
    MVOID   setIspOnOff_EE(MBOOL const fgOn);
    MVOID   setIspOnOff_COLOR(MBOOL const fgOn);

    MBOOL   getIspOnOff_OBC() const;
    MBOOL   getIspOnOff_BPC() const;
    MBOOL   getIspOnOff_CT() const;
    MBOOL   getIspOnOff_PDC() const;
    MBOOL   getIspOnOff_SLK() const;
    MBOOL   getIspOnOff_DM() const;
    MBOOL   getIspOnOff_CCM() const;
    MBOOL   getIspOnOff_GGM() const;
    MBOOL   getIspOnOff_YNR() const;
    MBOOL   getIspOnOff_CNR() const;
    MBOOL   getIspOnOff_CCR() const;
    MBOOL   getIspOnOff_ABF() const;
    MBOOL   getIspOnOff_EE() const;
    MBOOL   getIspOnOff_COLOR() const;

    MINT32 cct_HandleSensorOp(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );
    MINT32 cct_Handle3AOp(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );
    MINT32 cct_HandleIspOp(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );
    MINT32 cct_HandleNvramOp(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );
    MINT32 cct_HandleEmcamOp(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn, MUINT32 u4ParaOutLen, MUINT8 *puParaOut, MUINT32 *pu4RealParaOutLen );

    MINT32 cctNvram_GetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf, MINT32 outSize, void *pOutBuf, MINT32 *pRealOutSize);
    MINT32 cctNvram_SetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf);
    MINT32 cctNvram_SetPartialNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT32 bufOffset, MUINT8 *pInBuf);
    MINT32 cctNvram_SaveNvramData(CCT_NVRAM_DATA_T dataType);
    MINT32 cctNvram_SetIspNvramData(ISP_NVRAM_REGISTER_STRUCT *pIspRegs);
    MINT32 cctNvram_GetNvramStruct(MINT32 inSize, MINT32 *pInBuf, MINT32 outSize, MINT32 *pOutBuf, MINT32 *pRealOutSize);

private:
    CAMERA_DUAL_CAMERA_SENSOR_ENUM const m_eSensorEnum;

    MINT32  mSensorDev;
    NSCam::SensorStaticInfo m_SensorStaticInfo;
    MBOOL   m_bGetSensorStaticInfo;
    IHal3A *m_pIHal3A;

    NvramDrvBase*   m_pNvramDrv;

    NVRAM_CAMERA_ISP_PARAM_STRUCT&  m_rBuf_ISP;

    ISP_NVRAM_COMMON_STRUCT&        m_rISPComm;
    ISP_NVRAM_REGISTER_STRUCT&      m_rISPRegs;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Shading
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
private:    ////    NVRAM buffer.

    NVRAM_CAMERA_SHADING_STRUCT&    m_rBuf_SD;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  3A (AE,AWB)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
private:    ////    NVRAM buffer.

    NVRAM_CAMERA_3A_STRUCT& m_rBuf_3A;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Lens (AF)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
private:    ////    NVRAM buffer.

    NVRAM_LENS_PARA_STRUCT& m_rBuf_LN;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//  Flash Calibration
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
private:    ////    NVRAM buffer.

    NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT& m_rBuf_FC;
};


#endif

