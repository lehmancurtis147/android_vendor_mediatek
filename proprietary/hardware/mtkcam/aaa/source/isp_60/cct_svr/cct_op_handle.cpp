/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
#define LOG_TAG "cct_handle"

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/poll.h>
#include <unistd.h>

#include <pthread.h>

#include "kd_imgsensor_define.h"

#include "sensor_drv.h"
#include <mtkcam/drv/IHalSensor.h>

//#include "CctIF.h"   			// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk
//#include "AcdkCommon.h"		// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk

#include "cct_feature.h"

#include "cct_op_data.h"
#include "cct_op_handle.h"


#include <aaa_types.h>
#include <aaa_error_code.h>
#include <aaa_hal_if.h>
#include "awb_mgr_if.h"

#include <isp_reg.h>
#include <af_feature.h>
#include <af_algo_if.h>
#include "af_mgr_if.h"
#include <mtkcam/def/common.h>
using namespace NSCam;
#include "ae_mgr_if.h"

#include "awb_param.h"
#include "af_param.h"
#include "ae_param.h"
#include "flash_mgr.h"
#include "isp_tuning_mgr.h"
#include "isp_mgr.h"
#include <lsc/ILscMgr.h>
#include <ILscNvram.h>
#include <nvbuf_util.h>
//#include <mtkcam/aaa/aaa_hal_common.h>
//#include "IHal3A.h"

#include <cutils/properties.h>

using namespace NSIspTuningv3;
using namespace NS3Av3;
using namespace NSCam;


#define ISP_ID_TEXT_LENGTH          (8)

char gb_MT6757P_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6757p";
char gb_MT6799P_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6799p";
char gb_MT6763_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6763";
char gb_MT6771_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6771";
char gb_MT6775_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6775";


#define CCT_AWB_NVRAM_TBL_NO            (2)     //(AWB_NVRAM_IDX_NUM)

#define CCT_CMD_OUTBUF_SIZE_COUNT       (FT_CCT_OP_SENSOR_TYPE_OP_NO + FT_CCT_OP_3A_TYPE_OP_NO + FT_CCT_OP_ISP_TYPE_OP_NO + FT_CCT_OP_EMCAM_TYPE_OP_NO)

MINT32 giCctCmdOutBufSize[CCT_CMD_OUTBUF_SIZE_COUNT][2] = {
   {  FT_CCT_OP_GET_SENSOR,                     sizeof(ACDK_CCT_SENSOR_INFO_STRUCT) },
   {  FT_CCT_OP_SET_SENSOR_REG,                 0 },
   {  FT_CCT_OP_GET_SENSOR_REG,                 sizeof(ACDK_CCT_REG_RW_STRUCT) },
   {  FT_CCT_OP_LSC_GET_SENSOR_RESOLUTION,      sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT) },

   {  FT_CCT_OP_AE_GET_ON_OFF,                  sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_ON_OFF,                  0 },
   {  FT_CCT_OP_AE_GET_BAND,                    sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_BAND,                    0 },
   {  FT_CCT_OP_AE_GET_METERING_MODE,           sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_METERING_MODE,           0 },
   {  FT_CCT_OP_AE_GET_SCENE_MODE,              sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_SCENE_MODE,              0 },
   {  FT_CCT_OP_AE_GET_AUTO_PARA,               sizeof(ACDK_AE_MODE_CFG_T) },
   {  FT_CCT_OP_AE_SET_AUTO_PARA,               0 },
   {  FT_CCT_OP_AE_GET_CAPTURE_PARA,            sizeof(ACDK_AE_MODE_CFG_T) },
   {  FT_CCT_OP_AE_SET_CAPTURE_PARA,            0 },
   {  FT_CCT_OP_AF_GET_RANGE,                   sizeof(FOCUS_RANGE_T) },
   {  FT_CCT_OP_AF_GET_POS,                     sizeof(MINT32) },
   {  FT_CCT_OP_AF_SET_POS,                     0 },
   {  FT_CCT_OP_AWB_GET_ON_OFF,                 sizeof(MINT32) },
   {  FT_CCT_OP_AWB_SET_ON_OFF,                 0 },
   {  FT_CCT_OP_AWB_GET_LIGHT_PROB,             sizeof(AWB_LIGHT_PROBABILITY_T) },
   {  FT_CCT_OP_AWB_GET_MODE,                   sizeof(MINT32) },
   {  FT_CCT_OP_AWB_SET_MODE,                   0 },
   {  FT_CCT_OP_AWB_GET_GAIN,                   sizeof(AWB_GAIN_T) },
   {  FT_CCT_OP_AWB_SET_GAIN,                   0 },
   {  FT_CCT_OP_FLASH_GET_MODE,                 sizeof(MINT32) },
   {  FT_CCT_OP_FLASH_SET_MODE,                 0 },

   {  FT_CCT_OP_GET_ID,                         ISP_ID_TEXT_LENGTH },
   {  FT_CCT_OP_ISP_GET_ON_OFF,                 sizeof(MINT32) },
   {  FT_CCT_OP_ISP_SET_ON_OFF,                 0 },
   {  FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF,       sizeof(MINT32) },
   {  FT_CCT_OP_ISP_SET_CCM_FIXED_ON_OFF,       0 },
   {  FT_CCT_OP_ISP_GET_CCM_MATRIX,             sizeof(ACDK_CCT_CCM_STRUCT) },
   {  FT_CCT_OP_ISP_SET_CCM_MATRIX,             0 },
   {  FT_CCT_OP_GET_SHADING_ON_OFF,             sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) },
   {  FT_CCT_OP_SET_SHADING_ON_OFF,             0 },
   {  FT_CCT_OP_GET_SHADING_INDEX,              sizeof(MINT32) },
   {  FT_CCT_OP_SET_SHADING_INDEX,              0 },
   {  FT_CCT_OP_GET_SHADING_TSF_ON_OFF,         sizeof(MINT32) },
   {  FT_CCT_OP_SET_SHADING_TSF_ON_OFF,         0 },

   {  FT_CCT_OP_AF_BRECKET_STEP,                0 },
   {  FT_CCT_OP_AE_BRECKET_STEP,                0 }

//   {  FT_CCT_OP_ISP_GET_NVRAM_NUMBER,           CCT_NVRAM_STRUCT_MAXIMUM * sizeof(MINT32) },
//   {  FT_CCT_OP_ISP_GET_NVRAM_DATA,             0 },
//   {  FT_CCT_OP_ISP_SET_NVRAM_DATA,             0 },
//   {  FT_CCT_OP_ISP_SAVE_NVRAM_DATA,            0 },

};

MINT32 giCctNvramCmdOutBufSize[CCT_NVRAM_DATA_ENUM_MAX][2] = {
   {  CCT_NVRAM_DATA_LSC_PARA,          sizeof(winmo_cct_shading_comp_struct) },
   {  CCT_NVRAM_DATA_LSC_TABLE,         sizeof(CCT_SHADING_TAB_STRUCT) },
   {  CCT_NVRAM_DATA_LSC,               sizeof(NVRAM_CAMERA_SHADING_STRUCT) },
   {  CCT_NVRAM_DATA_AE_PLINE,          sizeof(AE_PLINETABLE_T) },
   {  CCT_NVRAM_DATA_AE,                sizeof(AE_NVRAM_T) * CAM_SCENARIO_NUM },                   // need to support 7 scenarios ... Done
   {  CCT_NVRAM_DATA_AF,                sizeof(NVRAM_LENS_PARA_STRUCT) },       // need to support 7 scenarios
   {  CCT_NVRAM_DATA_AWB,               sizeof(AWB_NVRAM_T) * CAM_SCENARIO_NUM },   // need to support 7 scenarios ... Done
   {  CCT_NVRAM_DATA_ISP,               sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) },
   {  CCT_NVRAM_DATA_FEATURE,           sizeof(NVRAM_CAMERA_FEATURE_STRUCT) },
   {  CCT_NVRAM_DATA_STROBE,            sizeof(NVRAM_CAMERA_STROBE_STRUCT) },
   {  CCT_NVRAM_DATA_FLASH_AWB,         sizeof(FLASH_AWB_NVRAM_T) },
   {  CCT_NVRAM_DATA_FLASH_CALIBRATION, sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT) },
   {  CCT_NVRAM_DATA_3A,                sizeof(NVRAM_CAMERA_3A_STRUCT) }
};

#ifdef mt6775
MINT32 giCctNvramNumber[0][0] = {
};
#else
MINT32 giCctNvramNumber[CCT_NVRAM_STRUCT_MAXIMUM][2] = {
   {  CCT_NVRAM_STRUCT_SIZE_SHADING,     sizeof(NVRAM_CAMERA_SHADING_STRUCT)     },
   {  CCT_NVRAM_STRUCT_SIZE_AE_PLINE,    sizeof(AE_PLINETABLE_T)                 },
   {  CCT_NVRAM_STRUCT_SIZE_LENS_PARA,   sizeof(NVRAM_LENS_PARA_STRUCT)          },
   {  CCT_NVRAM_STRUCT_SIZE_ISP_PARAM,   sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT)   },
   {  CCT_NVRAM_STRUCT_SIZE_FEATURE,     sizeof(NVRAM_CAMERA_FEATURE_STRUCT)     },
   {  CCT_NVRAM_STRUCT_SIZE_STROBE,      sizeof(NVRAM_CAMERA_STROBE_STRUCT)      },
   {  CCT_NVRAM_STRUCT_SIZE_FLASH_CALIBRATION, sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT) },
   {  CCT_NVRAM_STRUCT_SIZE_3A,          sizeof(NVRAM_CAMERA_3A_STRUCT)          },

   {  CCT_NVRAM_STRUCT_NUMBER_SL2F,              NVRAM_SL2F_TBL_NUM           },
   {  CCT_NVRAM_STRUCT_NUMBER_DBS,               NVRAM_DBS_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_ADBS,              NVRAM_ADBS_TBL_NUM           },
   {  CCT_NVRAM_STRUCT_NUMBER_OBC,               NVRAM_OBC_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_BNR_BPC,           NVRAM_BPC_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_BNR_NR1,           NVRAM_NR1_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_BNR_PDC,           NVRAM_PDC_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_RMM,               NVRAM_RMM_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_RNR,               NVRAM_RNR_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_SL2,               NVRAM_SL2_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_UDM,               NVRAM_UDM_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC_ANR,           NVRAM_ANR_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC_LCE_LINK,      NVRAM_LCE_LINK_NUM           },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC2_ANR2,         NVRAM_ANR2_TBL_NUM           },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC2_CCR,          NVRAM_CCR_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC2_ABF,          NVRAM_ABF_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_EE,                NVRAM_EE_TBL_NUM             },
   {  CCT_NVRAM_STRUCT_NUMBER_HFG,               NVRAM_HFG_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_NR3D,              NVRAM_NR3D_TBL_NUM           },
   {  CCT_NVRAM_STRUCT_NUMBER_MFB,               NVRAM_MFB_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_MIXER3,            NVRAM_MIXER3_TBL_NUM         },
   {  CCT_NVRAM_STRUCT_NUMBER_SWNR_THRES,        NVRAM_SWNR_THRES_NUM         },
   {  CCT_NVRAM_STRUCT_NUMBER_SWNR,              NVRAM_SWNR_TBL_NUM_2         },
   {  CCT_NVRAM_STRUCT_NUMBER_MFNR,              NVRAM_MFNR_TBL_NUM_2         },
   {  CCT_NVRAM_STRUCT_NUMBER_NBC_TBL,           NVRAM_NBC_TBL_NUM            },
   {  CCT_NVRAM_STRUCT_NUMBER_FD_ANR,            NVRAM_FD_ANR_NUM             },
   {  CCT_NVRAM_STRUCT_NUMBER_CLEARZOOM,         NVRAM_CLEARZOOM_NUM_2        },
   {  CCT_NVRAM_STRUCT_NUMBER_CA_LTM,            NVRAM_CA_LTM_NUM_2           },
   {  CCT_NVRAM_STRUCT_NUMBER_CCM,               ISP_NVRAM_CCM_CT_NUM_2       },
   {  CCT_NVRAM_STRUCT_NUMBER_COLOR_PARAM,       ISP_NVRAM_COLOR_PARAM_NUM_2  },
   {  CCT_NVRAM_STRUCT_NUMBER_COLOR,             ISP_NVRAM_COLOR_NUM_2        },
   {  CCT_NVRAM_STRUCT_NUMBER_PCA,               1                            },
   {  CCT_NVRAM_STRUCT_NUMBER_AE,                AE_CAM_SCENARIO_NUM          },
   {  CCT_NVRAM_STRUCT_NUMBER_AF,                AF_CAM_SCENARIO_NUM_2        },
   {  CCT_NVRAM_STRUCT_NUMBER_AWB,               AWB_CAM_SCENARIO_NUM         },
   {  CCT_NVRAM_STRUCT_NUMBER_FlASH_AE,          FlASH_AE_NUM_2               },
   {  CCT_NVRAM_STRUCT_NUMBER_FlASH_AWB,         FlASH_AWB_NUM_2              },
   {  CCT_NVRAM_STRUCT_NUMBER_FlASH_CALIBRATION, FlASH_CALIBRATION_NUM_2      },
   {  CCT_NVRAM_STRUCT_NUMBER_GMA,               ISP_NVRAM_GMA_NUM_2          },
   {  CCT_NVRAM_STRUCT_NUMBER_LCE,               ISP_NVRAM_LCE_NUM_2          },
   {  CCT_NVRAM_STRUCT_NUMBER_DCE,               ISP_NVRAM_DCE_NUM_2          }
};
#endif

CctHandle*
CctHandle::
createInstance(CAMERA_DUAL_CAMERA_SENSOR_ENUM eSensorEnum)
{
    NVRAM_CAMERA_ISP_PARAM_STRUCT*  pbuf_isp;
    NVRAM_CAMERA_SHADING_STRUCT*    pbuf_shd;
    NVRAM_CAMERA_3A_STRUCT*    pbuf_3a;
    NVRAM_LENS_PARA_STRUCT*    pbuf_ln;
    NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT*    pbuf_flash_cal;
    int err;
    NvramDrvBase*	pnvram_drv;

    ALOGD("CctHandle createInstance +");
    pnvram_drv = NvramDrvBase::createInstance();

//    ALOGD("Init sensor +");
//    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
//    pIHalSensorList->searchSensors();
//    int sensorCount = pIHalSensorList->queryNumberOfSensors();
//    ALOGD("Init sensor -, sensorCount(%d)", sensorCount);

    NvBufUtil::getInstance().setAndroidMode(0);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_ISP, eSensorEnum, (void*&)pbuf_isp);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_SHADING, eSensorEnum, (void*&)pbuf_shd);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_3A, eSensorEnum, (void*&)pbuf_3a);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_LENS, eSensorEnum, (void*&)pbuf_ln);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_FLASH_CALIBRATION, eSensorEnum, (void*&)pbuf_flash_cal);

    ALOGD("CctHandle createInstance -");
    return new CctHandle(pbuf_isp, pbuf_shd, pbuf_3a, pbuf_ln, pbuf_flash_cal, pnvram_drv, eSensorEnum);
}


void
CctHandle::
destroyInstance()
{
    delete this;
}

CctHandle::
CctHandle( NVRAM_CAMERA_ISP_PARAM_STRUCT* pbuf_isp, NVRAM_CAMERA_SHADING_STRUCT* pbuf_shd, NVRAM_CAMERA_3A_STRUCT* pbuf_3a, NVRAM_LENS_PARA_STRUCT* pbuf_ln, NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT* pbuf_flash_cal, NvramDrvBase*	pnvram_drv, MUINT32 isensor_dev)
    : m_eSensorEnum((CAMERA_DUAL_CAMERA_SENSOR_ENUM)isensor_dev)
    , m_bGetSensorStaticInfo(MFALSE)
    , m_pIHal3A(NULL)
    , m_pNvramDrv( pnvram_drv )
    , m_rBuf_ISP( *pbuf_isp )
    , m_rISPComm( m_rBuf_ISP.ISPComm )
    , m_rISPRegs( m_rBuf_ISP.ISPRegs )
    , m_rBuf_SD( *pbuf_shd )
    , m_rBuf_3A( *pbuf_3a )
    , m_rBuf_LN( *pbuf_ln )
    , m_rBuf_FC( *pbuf_flash_cal )
//    , m_rISPPca( m_rBuf_ISP.ISPPca )
{
    ALOGD("CctHandle construct +");
    ALOGD("Sensor Dev (%d) is used", m_eSensorEnum);
    mSensorDev = m_eSensorEnum;

    ALOGD("CctHandle construct -");
}


CctHandle::
~CctHandle()
{
    m_pIHal3A->send3ACtrl(E3ACtrl_Enable3ASetParams, MTRUE, 0);
    m_pIHal3A->send3ACtrl(E3ACtrl_SetOperMode, NSIspTuning::EOperMode_Normal, 0);
}


void
CctHandle::
init()
{
    ALOGD("CctHandle init +");
    if (m_eSensorEnum == DUAL_CAMERA_MAIN_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(0, "CctHandle");
    } else if (m_eSensorEnum == DUAL_CAMERA_SUB_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(1, "CctHandle");
    } else if (m_eSensorEnum == DUAL_CAMERA_MAIN_2_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(2, "CctHandle");
    } else {
        m_pIHal3A = MAKE_Hal3A(3, "CctHandle");
    }

    m_pIHal3A->send3ACtrl(E3ACtrl_Enable3ASetParams, MFALSE, 0);
    m_pIHal3A->send3ACtrl(E3ACtrl_SetOperMode, NSIspTuning::EOperMode_Meta, 0);

    ALOGD("CctHandle init -");
}



MINT32
CctHandle::
cct_getSensorStaticInfo()
{
    if (m_bGetSensorStaticInfo) return CCTIF_NO_ERROR;

    MINT32 err = SENSOR_NO_ERROR;
    IHalSensorList*const pHalSensorList = MAKE_HalSensorList();
    pHalSensorList->querySensorStaticInfo(mSensorDev,&m_SensorStaticInfo);
    m_bGetSensorStaticInfo = MTRUE;
    return SENSOR_NO_ERROR;
}


MINT32
CctHandle::
cct_QuerySensor(MVOID *a_pCCTSensorInfoOut, MUINT32 *pRealParaOutLen)
{

    MINT32 err = SENSOR_NO_ERROR;
#if 0
    char *str;
    str = (char *) a_pCCTSensorInfoOut;
    strcpy(str, "cct_QuerySensor executed");
    *pRealParaOutLen = strlen(str);
#else
    cct_getSensorStaticInfo();

    ACDK_CCT_SENSOR_INFO_STRUCT *pSensorEngInfoOut = (ACDK_CCT_SENSOR_INFO_STRUCT*)a_pCCTSensorInfoOut;
    pSensorEngInfoOut->DeviceId = m_SensorStaticInfo.sensorDevID;
    pSensorEngInfoOut->Type = static_cast<ACDK_CCT_REG_TYPE_ENUM>(m_SensorStaticInfo.sensorType);
    pSensorEngInfoOut->StartPixelBayerPtn = static_cast<ACDK_SENSOR_OUTPUT_DATA_FORMAT_ENUM>(m_SensorStaticInfo.sensorFormatOrder);
    pSensorEngInfoOut->GrabXOffset = 0;
    pSensorEngInfoOut->GrabYOffset = 0;

    printf("[CCTOPQuerySensor] Id = 0x%x\n", pSensorEngInfoOut->DeviceId);
    printf("[CCTOPQuerySensor] Type = %d\n", pSensorEngInfoOut->Type);
    printf("[CCTOPQuerySensor] StartPixelBayerPtn = %d\n", pSensorEngInfoOut->StartPixelBayerPtn);
    printf("[CCTOPQuerySensor] GrabXOffset = %d\n", pSensorEngInfoOut->GrabXOffset);
    printf("[CCTOPQuerySensor] GrabYOffset = %d\n", pSensorEngInfoOut->GrabYOffset);

    ALOGD("[CCTOPQuerySensor] Id = 0x%x\n", pSensorEngInfoOut->DeviceId);
    ALOGD("[CCTOPQuerySensor] Type = %d\n", pSensorEngInfoOut->Type);
    ALOGD("[CCTOPQuerySensor] StartPixelBayerPtn = %d\n", pSensorEngInfoOut->StartPixelBayerPtn);
    ALOGD("[CCTOPQuerySensor] GrabXOffset = %d\n", pSensorEngInfoOut->GrabXOffset);
    ALOGD("[CCTOPQuerySensor] GrabYOffset = %d\n", pSensorEngInfoOut->GrabYOffset);

    *pRealParaOutLen = sizeof(ACDK_CCT_SENSOR_INFO_STRUCT);
#endif
    return err;
}


MINT32
CctHandle::
cct_GetSensorRes(MVOID *pCCTSensorResOut, MUINT32 *pRealParaOutLen)
{
    MINT32 err = SENSOR_NO_ERROR;
#if 0
    char *str;
    str = (char *) pCCTSensorResOut;
    strcpy(str, "cct_GetSensorRes executed");
    *pRealParaOutLen = strlen(str);
#else
    cct_getSensorStaticInfo();

    ACDK_CCT_SENSOR_RESOLUTION_STRUCT *pSensorResolution = (ACDK_CCT_SENSOR_RESOLUTION_STRUCT *)pCCTSensorResOut;
    pSensorResolution->SensorPreviewWidth  = m_SensorStaticInfo.previewWidth;
    pSensorResolution->SensorPreviewHeight = m_SensorStaticInfo.previewHeight;
    pSensorResolution->SensorFullWidth     = m_SensorStaticInfo.captureWidth;
    pSensorResolution->SensorFullHeight    = m_SensorStaticInfo.captureHeight;
    pSensorResolution->SensorVideoWidth    = m_SensorStaticInfo.videoWidth;
    pSensorResolution->SensorVideoHeight   = m_SensorStaticInfo.videoHeight;
    pSensorResolution->SensorVideo1Width   = m_SensorStaticInfo.video1Width;
    pSensorResolution->SensorVideo1Height  = m_SensorStaticInfo.video1Height;
    pSensorResolution->SensorVideo2Width   = m_SensorStaticInfo.video2Width;
    pSensorResolution->SensorVideo2Height  = m_SensorStaticInfo.video2Height;
    pSensorResolution->SensorCustom1Width   = m_SensorStaticInfo.SensorCustom1Width;
    pSensorResolution->SensorCustom1Height  = m_SensorStaticInfo.SensorCustom1Height;
    pSensorResolution->SensorCustom2Width   = m_SensorStaticInfo.SensorCustom2Width;
    pSensorResolution->SensorCustom2Height  = m_SensorStaticInfo.SensorCustom2Height;
    pSensorResolution->SensorCustom3Width   = m_SensorStaticInfo.SensorCustom3Width;
    pSensorResolution->SensorCustom3Height  = m_SensorStaticInfo.SensorCustom3Height;
    pSensorResolution->SensorCustom4Width   = m_SensorStaticInfo.SensorCustom4Width;
    pSensorResolution->SensorCustom4Height  = m_SensorStaticInfo.SensorCustom4Height;
    pSensorResolution->SensorCustom5Width   = m_SensorStaticInfo.SensorCustom5Width;
    pSensorResolution->SensorCustom5Height  = m_SensorStaticInfo.SensorCustom5Height;

    printf("[CCTOPGetSensorRes] PreviewWidth = %d, PreviewHeight = %d\n", pSensorResolution->SensorPreviewWidth, pSensorResolution->SensorPreviewHeight);
    printf("[CCTOPGetSensorRes] SensorFullWidth = %d, SensorFullHeight = %d\n", pSensorResolution->SensorFullWidth, pSensorResolution->SensorFullHeight);
    printf("[CCTOPGetSensorRes] SensorVideoWidth = %d, SensorVideoHeight = %d\n", pSensorResolution->SensorVideoWidth, pSensorResolution->SensorVideoHeight);
    printf("[CCTOPGetSensorRes] SensorVideo1Width = %d, SensorVideo1Height = %d\n", pSensorResolution->SensorVideo1Width, pSensorResolution->SensorVideo1Height);
    printf("[CCTOPGetSensorRes] SensorVideo2Width = %d, SensorVideo2Height = %d\n", pSensorResolution->SensorVideo2Width, pSensorResolution->SensorVideo2Height);
    printf("[CCTOPGetSensorRes] SensorCustom1Width = %d, SensorCustom1Height = %d\n", pSensorResolution->SensorCustom1Width, pSensorResolution->SensorCustom1Height);
    printf("[CCTOPGetSensorRes] SensorCustom2Width = %d, SensorCustom2Height = %d\n", pSensorResolution->SensorCustom2Width, pSensorResolution->SensorCustom2Height);
    printf("[CCTOPGetSensorRes] SensorCustom3Width = %d, SensorCustom3Height = %d\n", pSensorResolution->SensorCustom3Width, pSensorResolution->SensorCustom3Height);
    printf("[CCTOPGetSensorRes] SensorCustom4Width = %d, SensorCustom4Height = %d\n", pSensorResolution->SensorCustom4Width, pSensorResolution->SensorCustom4Height);
    printf("[CCTOPGetSensorRes] SensorCustom5Width = %d, SensorCustom5Height = %d\n", pSensorResolution->SensorCustom5Width, pSensorResolution->SensorCustom5Height);

    ALOGD("[CCTOPGetSensorRes] PreviewWidth = %d, PreviewHeight = %d\n", pSensorResolution->SensorPreviewWidth, pSensorResolution->SensorPreviewHeight);
    ALOGD("[CCTOPGetSensorRes] SensorFullWidth = %d, SensorFullHeight = %d\n", pSensorResolution->SensorFullWidth, pSensorResolution->SensorFullHeight);
    ALOGD("[CCTOPGetSensorRes] SensorVideoWidth = %d, SensorVideoHeight = %d\n", pSensorResolution->SensorVideoWidth, pSensorResolution->SensorVideoHeight);
    ALOGD("[CCTOPGetSensorRes] SensorVideo1Width = %d, SensorVideo1Height = %d\n", pSensorResolution->SensorVideo1Width, pSensorResolution->SensorVideo1Height);
    ALOGD("[CCTOPGetSensorRes] SensorVideo2Width = %d, SensorVideo2Height = %d\n", pSensorResolution->SensorVideo2Width, pSensorResolution->SensorVideo2Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom1Width = %d, SensorCustom1Height = %d\n", pSensorResolution->SensorCustom1Width, pSensorResolution->SensorCustom1Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom2Width = %d, SensorCustom2Height = %d\n", pSensorResolution->SensorCustom2Width, pSensorResolution->SensorCustom2Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom3Width = %d, SensorCustom3Height = %d\n", pSensorResolution->SensorCustom3Width, pSensorResolution->SensorCustom3Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom4Width = %d, SensorCustom4Height = %d\n", pSensorResolution->SensorCustom4Width, pSensorResolution->SensorCustom4Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom5Width = %d, SensorCustom5Height = %d\n", pSensorResolution->SensorCustom5Width, pSensorResolution->SensorCustom5Height);

    *pRealParaOutLen = sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT);
#endif
    return err;
}


MINT32
CctHandle::
cct_ReadSensorReg(MVOID *puParaIn, MVOID *puParaOut, MUINT32 *pu4RealParaOutLen)
{
    MINT32 err = SENSOR_NO_ERROR;
    ACDK_SENSOR_FEATURE_ENUM eSensorFeature = SENSOR_FEATURE_GET_REGISTER;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoIn = (PACDK_CCT_REG_RW_STRUCT)puParaIn;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoOut = (PACDK_CCT_REG_RW_STRUCT)puParaOut;
    MUINT32 Data[2], sensorParaLen;
    IHalSensor *pHalSensorObj;

    ALOGD("[ACDK_CCT_OP_READ_SENSOR_REG]\n");

    Data[0] = pSensorRegInfoIn->RegAddr;
    Data[1] = 0;

    sensorParaLen = 2 * sizeof(MUINT32);

    // Sensor hal init
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    pHalSensorObj = pIHalSensorList->createSensor("cct_sensor_access", 0);

    if(pHalSensorObj == NULL) {
        ALOGE("[AAA Sensor Mgr] Can not create SensorHal obj\n");
        return SENSOR_INVALID_SENSOR;
    }

    err = pHalSensorObj->sendCommand(mSensorDev, SENSOR_CMD_SET_CCT_FEATURE_CONTROL, (MUINTPTR)&eSensorFeature, (MUINTPTR)&Data[0], (MUINTPTR)&sensorParaLen);

    pSensorRegInfoOut->RegAddr = pSensorRegInfoIn->RegAddr;
    pSensorRegInfoOut->RegData = Data[1];

    *pu4RealParaOutLen = sizeof(ACDK_CCT_REG_RW_STRUCT);

    if (err != SENSOR_NO_ERROR) {
        ALOGE("[CCTOReadSensorReg() error]\n");
        return err;
    }

    ALOGD("[CCTOReadSensorReg] regAddr = %x, regData = %x\n", Data[0], Data[1]);

    return err;
}

/*******************************************************************************
*
********************************************************************************/
MINT32
CctHandle::
cct_WriteSensorReg(MVOID *puParaIn)
{
    MINT32 err = SENSOR_NO_ERROR;
    ACDK_SENSOR_FEATURE_ENUM eSensorFeature = SENSOR_FEATURE_SET_REGISTER;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoIn = (PACDK_CCT_REG_RW_STRUCT)puParaIn;
    MUINT32 Data[2], sensorParaLen;
    IHalSensor *pHalSensorObj;

    ALOGD("[ACDK_CCT_OP_WRITE_SENSOR_REG]\n");

    Data[0] = pSensorRegInfoIn->RegAddr;
    Data[1] = pSensorRegInfoIn->RegData;

    sensorParaLen = 2 * sizeof(MUINT32);

    // Sensor hal init
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    pHalSensorObj = pIHalSensorList->createSensor("cct_sensor_access", 0);

    if(pHalSensorObj == NULL) {
        ALOGE("[AAA Sensor Mgr] Can not create SensorHal obj\n");
        return SENSOR_INVALID_SENSOR;
    }

    err = pHalSensorObj->sendCommand(mSensorDev, SENSOR_CMD_SET_CCT_FEATURE_CONTROL, (MUINTPTR)&eSensorFeature, (MUINTPTR)&Data[0], (MUINTPTR)&sensorParaLen);

    if (err != SENSOR_NO_ERROR) {
        ALOGE("[CCTOPWriteSensorReg() error]\n");
        return err;
    }

    ALOGD("[CCTOPWriteSensorReg] regAddr = %x, regData = %x\n", Data[0], Data[1]);

    return err;
}

MVOID
CctHandle::
setIspOnOff_OBC(MBOOL const fgOn)
{
#if 0
    ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}

MVOID
CctHandle::
setIspOnOff_BPC(MBOOL const fgOn)
{
#if 0
    //m_rISPRegs.BPC[u4Index].con.bits.BPC_ENABLE = fgOn;
    //m_rISPRegs.BNR_BPC[u4Index].con.bits.BPC_EN = fgOn; //definition change: CAM_BPC_CON CAM+0800H

    ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTBPCEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}


MVOID
CctHandle::
setIspOnOff_CT(MBOOL const fgOn)
{
#if 0
    //m_rISPRegs.BPC_NR1[u4Index].con.bits.NR1_CT_EN = fgOn;

    ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}

MVOID
CctHandle::
setIspOnOff_PDC(MBOOL const fgOn)
{
#if 0
    //m_rISPRegs.BNR_PDC[u4Index].con.bits.PDC_EN = fgOn;

    ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTPDCEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}


MVOID
CctHandle::
setIspOnOff_SLK(MBOOL const fgOn)
{
#if 0

    ISP_MGR_SLK_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}


MVOID
CctHandle::
setIspOnOff_DM(MBOOL const fgOn)
{
#if 0
    ISP_MGR_DM::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);

    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_CFA::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_CFA::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif

}

MVOID
CctHandle::
setIspOnOff_CCM(MBOOL const fgOn)
{
#if 0
    ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}

MVOID
CctHandle::
setIspOnOff_GGM(MBOOL const fgOn)
{
#if 0
    ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}

MVOID
CctHandle::
setIspOnOff_CNR(MBOOL const fgOn)
{
#if 0

    ISP_MGR_CNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif

}

MVOID
CctHandle::
setIspOnOff_YNR(MBOOL const fgOn)
{
#if 0

    ISP_MGR_YNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTYNREnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif

}

MVOID
CctHandle::
setIspOnOff_CCR(MBOOL const fgOn)
{
#if 0

    ISP_MGR_YNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTCCREnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif

}

MVOID
CctHandle::
setIspOnOff_ABF(MBOOL const fgOn)
{
#if 0
    ISP_MGR_YNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTABFEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}


MVOID
CctHandle::
setIspOnOff_EE(MBOOL const fgOn)
{
#if 0
// Choo
//    ISP_DIP_X_SEEE_SRK_CTRL_T &EECtrl_SRK = m_rISPRegs.EE[u4Index].srk_ctrl.bits;
//    ISP_DIP_X_SEEE_CLIP_CTRL_T &EECtrl_Clip = m_rISPRegs.EE[u4Index].clip_ctrl.bits;

//    EECtrl_Clip.SEEE_OVRSH_CLIP_EN = fgOn;//definition change: CAM_SEEE_CLIP_CTRL CAM+AA4H

    ISP_MGR_SEEE_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEEEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif

}


MVOID
CctHandle::
setIspOnOff_COLOR(MBOOL const fgOn)
{
#if 0
    ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTColorEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
#endif
}


MBOOL
CctHandle::
getIspOnOff_OBC() const
{
#if 0
    return ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_BPC() const
{
#if 0
    return  ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTBPCEnable();
#endif
return MTRUE;
}


MBOOL
CctHandle::
getIspOnOff_CT() const
{
#if 0
    return  ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTCTEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_PDC() const
{
#if 0
    return  ISP_MGR_BPC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTPDCEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_SLK() const
{
#if 0

    return  ISP_MGR_SLK_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;

}

MBOOL
CctHandle::
getIspOnOff_DM() const
{
#if 0

    return  ISP_MGR_DM::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;

}

MBOOL
CctHandle::
getIspOnOff_CCM() const
{
#if 0
    return ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_GGM() const
{
#if 0
    return  ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_YNR() const
{
#if 0

        return (ISP_MGR_YNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable());
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_CNR() const
{
#if 0

        return (ISP_MGR_CNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTCNREnable());
#endif
return MTRUE;
}


MBOOL
CctHandle::
getIspOnOff_CCR() const
{
#if 0

        return ISP_MGR_CNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTCCREnable();
#endif
return MTRUE;

}

MBOOL
CctHandle::
getIspOnOff_ABF() const
{
#if 0
    return  ISP_MGR_CNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTABFEnable();
#endif
return MTRUE;
}

//MBOOL
//CctHandle::
//getIspOnOff_BOK() const
//{
//
//        return ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTBOKEnable();
//
//}


MBOOL
CctHandle::
getIspOnOff_EE() const
{
#if 0
    return ISP_MGR_EE_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEEEnable();
#endif
return MTRUE;
}

MBOOL
CctHandle::
getIspOnOff_COLOR() const
{
#if 0
    return  ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
#endif
return MTRUE;
}

//MBOOL
//CctHandle::
//getIspOnOff_HLR() const
//{
//    return  MFALSE;
//    //return  ISP_MGR_HLR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
//}

MINT32
CctHandle::
setIspOnOff(MUINT32 const u4Category, MBOOL const fgOn)
{
#define SET_ISP_ON_OFF(_category)\
    case ISP_CATEGORY_##_category:\
        setIspOnOff_##_category(fgOn);\
        MY_LOG("[setIspOnOff] < %s >", #_category);\
        break

    switch  ( u4Category )
    {
#if 0
        SET_ISP_ON_OFF(OBC);
        SET_ISP_ON_OFF(BPC);    //BNR_BPC
        SET_ISP_ON_OFF(CT);    //BNR_NR1
        SET_ISP_ON_OFF(PDC);
        SET_ISP_ON_OFF(SLK);
        SET_ISP_ON_OFF(DM);
        SET_ISP_ON_OFF(CCM);
        SET_ISP_ON_OFF(GGM);
        SET_ISP_ON_OFF(YNR);
        SET_ISP_ON_OFF(CNR);
        SET_ISP_ON_OFF(CCR);
        SET_ISP_ON_OFF(ABF);
        SET_ISP_ON_OFF(EE);
        SET_ISP_ON_OFF(COLOR);
#endif
        default:
            MY_ERR("[setIspOnOff] Unsupported Category(%d)", u4Category);
            return  CCTIF_BAD_PARAM;
    }
    MY_LOG("[%s] (u4Category, fgOn) = (%d, %d)", __FUNCTION__, u4Category, fgOn);
    return  CCTIF_NO_ERROR;
}


MINT32
CctHandle::
getIspOnOff(MUINT32 const u4Category, MBOOL& rfgOn) const
{
#define GET_ISP_ON_OFF(_category)\
    case ISP_CATEGORY_##_category:\
        MY_LOG("[getIspOnOff] < %s >", #_category);\
        rfgOn = getIspOnOff_##_category();\
        break

    switch  ( u4Category )
    {
#if 0
        GET_ISP_ON_OFF(OBC);
        GET_ISP_ON_OFF(BPC);    //BNR_BPC
        GET_ISP_ON_OFF(CT);    //BNR_NR1
        GET_ISP_ON_OFF(PDC);    //BNR_PDC
        GET_ISP_ON_OFF(SLK);
        GET_ISP_ON_OFF(DM);
        GET_ISP_ON_OFF(CCM);
        GET_ISP_ON_OFF(GGM);
        GET_ISP_ON_OFF(YNR);
        GET_ISP_ON_OFF(CNR);
        GET_ISP_ON_OFF(CCR);
        GET_ISP_ON_OFF(ABF);
        GET_ISP_ON_OFF(EE);
        GET_ISP_ON_OFF(COLOR);
#endif
        default:
            MY_ERR("[getIspOnOff] Unsupported Category(%d)", u4Category);
            return  CCTIF_BAD_PARAM;
    }
    MY_LOG("[%s] (u4Category, rfgOn) = (%d, %d)", __FUNCTION__, u4Category, rfgOn);
    return  CCTIF_NO_ERROR;
}


MINT32
CctHandle::
cct_HandleSensorOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MINT32 err = CCTIF_NO_ERROR;
    ALOGD("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    switch(op) {
    case FT_CCT_OP_GET_SENSOR:
        if (puParaOut != NULL)
            err = cct_QuerySensor(puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_SWITCH_SENSOR:
        break;
    case FT_CCT_OP_SET_SENSOR_REG:
        if (u4ParaInLen == sizeof(ACDK_CCT_REG_RW_STRUCT) && puParaIn != NULL)
            err = cct_WriteSensorReg(puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_GET_SENSOR_REG:
        if (u4ParaInLen == sizeof(ACDK_CCT_REG_RW_STRUCT) && puParaIn != NULL && puParaOut != NULL)
            err = cct_ReadSensorReg(puParaIn, puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_LSC_GET_SENSOR_RESOLUTION:
        if (puParaOut != NULL)
            err = cct_GetSensorRes(puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_TEST_SET_PROP:       // cmd for test only
        if (puParaIn != NULL && u4ParaInLen > 0) {
            char cmd[1024];
            char val[1024];
            MINT32 cmdlen, vallen;

            cmdlen = strlen((char *)puParaIn);
            strncpy(cmd, (char *)puParaIn, cmdlen);

            vallen = strlen((char *)(puParaIn+cmdlen+1));
            strncpy(val, (char *)(puParaIn+cmdlen+1), vallen);

            ALOGD("Property %s set to %s",cmd,val);
            property_set( cmd, val );
        }
        else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_TEST_GET_PROP:       // cmd for test only
        if (puParaIn != NULL && u4ParaInLen > 0) {
            char cmd[1024];
            char val[1024];
            MINT32 cmdlen;

            cmdlen = strlen((char *)puParaIn);

            strncpy(cmd, (char *)puParaIn, cmdlen);
            property_get( cmd, val, NULL );
            ALOGD("Property %s is %s",cmd,val);
        }
        else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;
    }
    return err;
}


MINT32
CctHandle::
cct_Handle3AOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    ALOGD("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    int ret;
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 *i32In = NULL;
    MUINT32 mode;
    MBOOL enable;

    switch(op) {
    case FT_CCT_OP_AE_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEEnable(mSensorDev);
            else
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_BAND:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetFlickerMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_BAND:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetFlickerMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_METERING_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetMeteringMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_METERING_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetMeteringMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_SCENE_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetAEScene(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_SCENE_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetAEScene(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_AUTO_PARA:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetExpParam(mSensorDev, (VOID *)puParaIn, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_AUTO_PARA:
        if ( u4ParaInLen == sizeof(ACDK_AE_MODE_CFG_T))
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyExpParam(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_CAPTURE_PARA:
        if (puParaOut != NULL) {
            err = NS3Av3::IAeMgr::getInstance().CCTOGetCaptureParams(mSensorDev, (VOID *)puParaOut);
            *pu4RealParaOutLen = sizeof(ACDK_AE_MODE_CFG_T);
        } else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_CAPTURE_PARA:
        if ( u4ParaInLen == sizeof(ACDK_AE_MODE_CFG_T))
            err = NS3Av3::IAeMgr::getInstance().CCTOSetCaptureParams(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AF_GET_RANGE:
        if (puParaOut != NULL)
            err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFGetFocusRange( (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AF_GET_POS:
        if (puParaOut != NULL)
            err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFGetBestPos( (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AF_SET_POS:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if (*i32In < 0)
                err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFOpeartion();
            else
                err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPMFOpeartion( *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBEnable(mSensorDev);
            else
                err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_LIGHT_PROB:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetLightProb(mSensorDev, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_GET_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetAWBMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if(*i32In < (MINT32)LIB3A_AWB_MODE_NUM)
              err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSetAWBMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_GAIN:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetAWBGain(mSensorDev, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_GAIN:
        if (u4ParaInLen == sizeof(AWB_GAIN_T))
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSetAWBGain(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_FLASH_GET_MODE:
        if (puParaOut != NULL) {
            MY_LOG("ACDK_CCT_OP_FLASH_GET_INFO line=%d\n",__LINE__);
            ret = FlashMgr::getInstance(mSensorDev)->cctGetFlashInfo((int*)puParaOut);
            *pu4RealParaOutLen = sizeof(MINT32);
            if(ret!=1)
                err=CCTIF_BAD_CTRL_CODE;
        } else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_FLASH_SET_MODE:
        if (u4ParaInLen == sizeof(MINT32)) {
            MY_LOG("ACDK_CCT_OP_FLASH_ENABLE line=%d\n",__LINE__);
            CCT_FL_MODE_T mode = (CCT_FL_MODE_T) (*(MUINT32 *)puParaIn);
            if (mode == CCT_FL_MODE_OFF) {    //disable
                ret = FlashMgr::getInstance(mSensorDev)->cctFlashEnable(0);
            }
            else if (mode == CCT_FL_MODE_HI_TEMP) {   //enable ... high color temp
                ret = FlashMgr::getInstance(mSensorDev)->cctFlashEnable(1);
                ret = FlashMgr::getInstance(mSensorDev)->clearManualFlash();
                ret = FlashMgr::getInstance(mSensorDev)->setManualFlash(1, 0);
            }
            else if (mode == CCT_FL_MODE_LO_TEMP) {   //enable ... low color temp
                ret = FlashMgr::getInstance(mSensorDev)->cctFlashEnable(1);
                ret = FlashMgr::getInstance(mSensorDev)->clearManualFlash();
                ret = FlashMgr::getInstance(mSensorDev)->setManualFlash(0, 1);
            }
            else if (mode == CCT_FL_MODE_MIX_TEMP) {   //enable ... mix both color temp
                ret = FlashMgr::getInstance(mSensorDev)->cctFlashEnable(1);
                ret = FlashMgr::getInstance(mSensorDev)->clearManualFlash();
                ret = FlashMgr::getInstance(mSensorDev)->setManualFlash(1, 1);
            }
            else
                ret = 0;
            if(ret!=1)
                err=CCTIF_BAD_CTRL_CODE;
        } else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;

    }
    return err;
}


MINT32
CctHandle::
cct_HandleIspOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MBOOL ret;
    MINT32 err = CCTIF_NO_ERROR;

    switch(op) {
    case FT_CCT_OP_GET_ID:
        {
        if ( ISP_ID_TEXT_LENGTH != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
        }
        else {
            *pu4RealParaOutLen = 0;
            memset((void *)puParaOut, 0, ISP_ID_TEXT_LENGTH );
#ifdef mt6775
            strncpy((char *)puParaOut, gb_MT6775_ISP_ID_TEXT, ISP_ID_TEXT_LENGTH);
#else
            strncpy((char *)puParaOut, gb_MT6771_ISP_ID_TEXT, ISP_ID_TEXT_LENGTH);
#endif
            *pu4RealParaOutLen = ISP_ID_TEXT_LENGTH;
            MY_LOG("( FT_CCT_OP_GET_ID ) done, ID:%s\n", (char *)(puParaOut));
        }
        }
        break;
    case FT_CCT_OP_ISP_GET_ON_OFF:
        {
        if  ( sizeof(CCT_ISP_CATEGORY_T) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if  ( sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        CCT_ISP_CATEGORY_T const eCategory = *reinterpret_cast<CCT_ISP_CATEGORY_T*>(puParaIn);
        MBOOL&       rfgEnable = reinterpret_cast<ACDK_CCT_FUNCTION_ENABLE_STRUCT*>(puParaOut)->Enable;

        err = getIspOnOff(eCategory, rfgEnable);
        *pu4RealParaOutLen = sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT);
        MY_LOG("[-FT_CCT_OP_ISP_GET_ON_OFF] (eCategory, rfgEnable)=(%d, %d)", eCategory, rfgEnable);
        }
        break;
    case FT_CCT_OP_ISP_SET_ON_OFF:
        {
        if  ( (sizeof(CCT_ISP_CATEGORY_T) + sizeof(MBOOL)) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        CCT_ISP_CATEGORY_T const eCategory = *reinterpret_cast<CCT_ISP_CATEGORY_T const*>(puParaIn);
        MBOOL enable = (MBOOL) *(puParaIn + 4);

        if (enable)
            err = setIspOnOff(eCategory, 1);
        else
            err = setIspOnOff(eCategory, 0);

        MY_LOG("[-FT_CCT_OP_ISP_SET_ON_OFF] eCategory(%d), err(%x)", eCategory, err);
        }
        break;
    case FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF:
        {
        MINT32 en = 0, dyccm = 0;

        MY_LOGD("[FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF]\n");
        if  ( sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut )
            return  CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        
        dyccm = NSIspTuningv3::IspTuningMgr::getInstance().getDynamicCCM((MINT32)m_eSensorEnum);
        if(dyccm < 0) {
            MY_LOGD("( FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF ) fail, dyccm=%d\n", dyccm);
            err = CCTIF_UNKNOWN_ERROR;
            break;
        }
        
        en = (MFALSE == dyccm)? MTRUE:MFALSE; //switch true/false
        reinterpret_cast<ACDK_CCT_FUNCTION_ENABLE_STRUCT*>(puParaOut)->Enable = en;
        *pu4RealParaOutLen = sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT);
        err = CCTIF_NO_ERROR;
        
        MY_LOGD("( FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF ) done, en=%d\n", en);
        }
        break;
    case FT_CCT_OP_ISP_SET_CCM_FIXED_ON_OFF:
        {
        if(puParaIn == NULL)
        {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MY_LOG("Enable Dynamic CCM!!\n");
        MBOOL enable = (MBOOL) *(puParaIn);

        if (enable)
            ret = NSIspTuningv3::IspTuningMgr::getInstance().setDynamicCCM((MINT32)m_eSensorEnum, MFALSE);
        else
            ret = NSIspTuningv3::IspTuningMgr::getInstance().setDynamicCCM((MINT32)m_eSensorEnum, MTRUE);

        *pu4RealParaOutLen = 0;
        if (ret == MTRUE)
            err = CCTIF_NO_ERROR;
        else
            err = CCTIF_UNKNOWN_ERROR;
        }
        break;
    case FT_CCT_OP_ISP_GET_CCM_MATRIX:
        {
        if  ( sizeof(ACDK_CCT_CCM_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_CCM_STRUCT*const     pDst = reinterpret_cast<ACDK_CCT_CCM_STRUCT*>(puParaOut);
        ISP_NVRAM_CCM_T ccm;

//Chooo
//        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).get(ccm);

        pDst->M11 = ccm.cnv_1.bits.CCM_CNV_00;//ccm.conv0a.bits.G2G_CNV_00;
        pDst->M12 = ccm.cnv_1.bits.CCM_CNV_01;//ccm.conv0a.bits.G2G_CNV_01;
        pDst->M13 = ccm.cnv_2.bits.CCM_CNV_02;//ccm.conv0b.bits.G2G_CNV_02;
        pDst->M21 = ccm.cnv_3.bits.CCM_CNV_10;//ccm.conv1a.bits.G2G_CNV_10;
        pDst->M22 = ccm.cnv_3.bits.CCM_CNV_11;//ccm.conv1a.bits.G2G_CNV_11;
        pDst->M23 = ccm.cnv_4.bits.CCM_CNV_12;//ccm.conv1b.bits.G2G_CNV_12;
        pDst->M31 = ccm.cnv_5.bits.CCM_CNV_20;//ccm.conv2a.bits.G2G_CNV_20;
        pDst->M32 = ccm.cnv_5.bits.CCM_CNV_21;//ccm.conv2a.bits.G2G_CNV_21;
        pDst->M33 = ccm.cnv_6.bits.CCM_CNV_22;//ccm.conv2b.bits.G2G_CNV_22;

        *pu4RealParaOutLen = sizeof(ACDK_CCT_CCM_STRUCT);

        MY_LOG("[ACDK_CCT_V2_OP_AWB_GET_CURRENT_CCM]\n");
        MY_LOG("M11 0x%03X\n", pDst->M11);
        MY_LOG("M12 0x%03X\n", pDst->M12);
        MY_LOG("M13 0x%03X\n", pDst->M13);
        MY_LOG("M21 0x%03X\n", pDst->M21);
        MY_LOG("M22 0x%03X\n", pDst->M22);
        MY_LOG("M23 0x%03X\n", pDst->M23);
        MY_LOG("M31 0x%03X\n", pDst->M31);
        MY_LOG("M32 0x%03X\n", pDst->M32);
        MY_LOG("M33 0x%03X\n", pDst->M33);

        MY_LOG("( FT_CCT_OP_ISP_GET_CCM_MATRIX ) done\n");
        }
        break;
    case FT_CCT_OP_ISP_SET_CCM_MATRIX:
        {
        if  ( sizeof(ACDK_CCT_CCM_STRUCT) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MY_LOG("[ACDK_CCT_V2_OP_AWB_SET_CURRENT_CCM]\n");

        ISP_NVRAM_CCM_T rDst;
        ACDK_CCT_CCM_STRUCT*const       pSrc = reinterpret_cast<ACDK_CCT_CCM_STRUCT*>(puParaIn);
    /*
        rDst.conv0a.bits.G2G_CNV_00 = pSrc->M11;
        rDst.conv0a.bits.G2G_CNV_01 = pSrc->M12;
        rDst.conv0b.bits.G2G_CNV_02 = pSrc->M13;
        rDst.conv1a.bits.G2G_CNV_10 = pSrc->M21;
        rDst.conv1a.bits.G2G_CNV_11 = pSrc->M22;
        rDst.conv1b.bits.G2G_CNV_12 = pSrc->M23;
        rDst.conv2a.bits.G2G_CNV_20 = pSrc->M31;
        rDst.conv2a.bits.G2G_CNV_21 = pSrc->M32;
        rDst.conv2b.bits.G2G_CNV_22 = pSrc->M33;
    */
        rDst.cnv_1.bits.CCM_CNV_00 = pSrc->M11;
        rDst.cnv_1.bits.CCM_CNV_01 = pSrc->M12;
        rDst.cnv_2.bits.CCM_CNV_02 = pSrc->M13;
        rDst.cnv_3.bits.CCM_CNV_10 = pSrc->M21;
        rDst.cnv_3.bits.CCM_CNV_11 = pSrc->M22;
        rDst.cnv_4.bits.CCM_CNV_12 = pSrc->M23;
        rDst.cnv_5.bits.CCM_CNV_20 = pSrc->M31;
        rDst.cnv_5.bits.CCM_CNV_21 = pSrc->M32;
        rDst.cnv_6.bits.CCM_CNV_22 = pSrc->M33;
//Chooo
//        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).put(rDst);
        NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
        //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
        //NSIspTuningv3::ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
        usleep(200000);
        MY_LOG("M11 0x%03X", pSrc->M11);
        MY_LOG("M12 0x%03X", pSrc->M12);
        MY_LOG("M13 0x%03X", pSrc->M13);
        MY_LOG("M21 0x%03X", pSrc->M21);
        MY_LOG("M22 0x%03X", pSrc->M22);
        MY_LOG("M23 0x%03X", pSrc->M23);
        MY_LOG("M31 0x%03X", pSrc->M31);
        MY_LOG("M32 0x%03X", pSrc->M32);
        MY_LOG("M33 0x%03X", pSrc->M33);

        MY_LOG("( FT_CCT_OP_ISP_SET_CCM_MATRIX ) done\n");
        }
        break;
    case FT_CCT_OP_GET_SHADING_ON_OFF:
        {
        if ( sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_MODULE_CTRL_STRUCT*const pShadingPara = reinterpret_cast<ACDK_CCT_MODULE_CTRL_STRUCT*>(puParaOut);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pShadingPara->Enable = pLscMgr->getOnOff();
        *pu4RealParaOutLen = sizeof(ACDK_CCT_MODULE_CTRL_STRUCT);
        MY_LOG("[%s] GET_SHADING_ON_OFF(%s)", __FUNCTION__, (pShadingPara->Enable ? "On":"Off"));
        }
        break;
    case FT_CCT_OP_SET_SHADING_ON_OFF:
        {
        if ( sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_MODULE_CTRL_STRUCT*const pShadingPara = reinterpret_cast<ACDK_CCT_MODULE_CTRL_STRUCT*>(puParaIn);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pLscMgr->setOnOff(pShadingPara->Enable);
        pLscMgr->updateLsc();

        MY_LOG("[%s] SET_SHADING_ON_OFF(%s)", __FUNCTION__, (pShadingPara->Enable ? "On":"Off"));
        }
        break;
    case FT_CCT_OP_GET_SHADING_INDEX:
        {
        if  ( ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MUINT32 *pShadingIndex = reinterpret_cast<MUINT32*>(puParaOut);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        *pShadingIndex = pLscMgr->getCTIdx();

        MY_LOG("[%s] GET_SHADING_INDEX(%d)", __FUNCTION__, *pShadingIndex);

        }
        break;
    case FT_CCT_OP_SET_SHADING_INDEX:
        {
        if ( ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MUINT32 u4CCT = *reinterpret_cast<MUINT8*>(puParaIn);

        MY_LOG("[%s] SET_SHADING_INDEX(%d)", __FUNCTION__, u4CCT);

        IspTuningMgr::getInstance().enableDynamicShading(m_eSensorEnum, MFALSE);
        IspTuningMgr::getInstance().setIndex_Shading(m_eSensorEnum, u4CCT);
        }
        break;
    case FT_CCT_OP_GET_SHADING_TSF_ON_OFF:
        {
        if  ( ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        UINT32 u4OnOff;
        *pu4RealParaOutLen = 0;
        MY_LOG("[%s] + GET_SHADING_TSF_ONOFF", __FUNCTION__);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        u4OnOff = pLscMgr->getTsfOnOff();
        *(reinterpret_cast<UINT32*>(puParaOut)) = u4OnOff;

        MY_LOG("[%s] - GET_SHADING_TSF_ONOFF(%d)", __FUNCTION__, u4OnOff);
        }
        break;
    case FT_CCT_OP_SET_SHADING_TSF_ON_OFF:
        {
        if(puParaIn == NULL)
        {
            err = CCTIF_BAD_PARAM;
            break;
        }
        UINT32 u4OnOff = *(reinterpret_cast<UINT32*>(puParaIn));
        *pu4RealParaOutLen = 0;
        MY_LOG("[%s] + SET_SHADING_TSF_ONOFF(%d)", __FUNCTION__, u4OnOff);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pLscMgr->setTsfOnOff(u4OnOff);

        MY_LOG("[%s] - SET_SHADING_TSF_ONOFF", __FUNCTION__);
        }
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;
    }
    return err;
}

MINT32
CctHandle::
cctNvram_GetNvramStruct(MINT32 inSize, MINT32 *pInBuf, MINT32 outSize, MINT32 *pOutBuf, MINT32 *pRealOutSize)
{
    *pRealOutSize = 0;
    if(inSize == 0)
    {
        MY_LOG("[%s] get all NVRAM structure parameters", __FUNCTION__);
        int output_count = CCT_NVRAM_STRUCT_MAXIMUM;
        int output_size = output_count * sizeof(MINT32);
        if( outSize < output_size )
        {
            MY_LOG("[%s] outSize too small (%d, %d)", __FUNCTION__, outSize, output_size);
            return CCTIF_BAD_PARAM;
        }

        for(int i=0; i<output_count; i++)
        {
            pOutBuf[i] = giCctNvramNumber[i][1];
        }
        *pRealOutSize = output_size;
    }
    else
    {
        //partial modules
        MY_LOG("[%s] partial modules not support", __FUNCTION__);
        return CCTIF_BAD_PARAM;
    }
    return 0;
}
MINT32
CctHandle::
cct_HandleNvramOp(CCT_OP_ID op,
                    MUINT32 u4ParaInLen,
                    MUINT8 *puParaIn,
                    MUINT32 u4ParaOutLen,
                    MUINT8 *puParaOut,
                    MUINT32 *pu4RealParaOutLen )
{
    CCT_NVRAM_DATA_T dtype;
    MINT32 inSize, outSize;
    MINT32 *pRealOutSize = NULL;
    MUINT32 inOffset;
    MUINT8 *pInBuf, *pOutBuf;
    MINT32 status = 0;

    switch(op) {
    case FT_CCT_OP_ISP_GET_NVRAM_DATA:
        if  ( ! puParaOut ) {
            break;
        }
        dtype = *((CCT_NVRAM_DATA_T *) puParaIn);
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T));
        outSize = u4ParaOutLen;
        pOutBuf = puParaOut;
        pRealOutSize = (MINT32 *) pu4RealParaOutLen;
        status = cctNvram_GetNvramData(dtype, inSize, pInBuf, outSize, (void *)pOutBuf, pRealOutSize);
        break;
    case FT_CCT_OP_ISP_SET_NVRAM_DATA:
        dtype = *((CCT_NVRAM_DATA_T *) puParaIn);
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T));
        status = cctNvram_SetNvramData(dtype, inSize, pInBuf);
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_ISP_SET_PARTIAL_NVRAM_DATA:
        // For this command FT_CCT_OP_ISP_SET_PARTIAL_NVRAM_DATA, there are 2 parameters in the beginning of
        // puParaIn buffer. The 1st parameter is dtype (data type) and the 2nd is inOffset (in buffer offset)
        //
        dtype = *((CCT_NVRAM_DATA_T *) puParaIn);
        inOffset = *(((CCT_NVRAM_DATA_T *) puParaIn)+1);
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T) - sizeof(MUINT32);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T) + sizeof(MUINT32));
        status = cctNvram_SetPartialNvramData(dtype, inSize, inOffset, pInBuf);
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_ISP_SAVE_NVRAM_DATA:
        dtype = *((CCT_NVRAM_DATA_T *) puParaIn);
        status = cctNvram_SaveNvramData(dtype);
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_ISP_GET_NVRAM_STRUCT:
        if  ( ! puParaOut ) {
            break;
        }
        inSize = u4ParaInLen;
        outSize = u4ParaOutLen;
        pRealOutSize = (MINT32 *) pu4RealParaOutLen;
        status = cctNvram_GetNvramStruct(inSize, (MINT32*)puParaIn, outSize, (MINT32*)puParaOut, pRealOutSize);
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;

    }
    return status;
}


MINT32
CctHandle::
cct_HandleEmcamOp(CCT_OP_ID op,
                    MUINT32 u4ParaInLen,
                    MUINT8 *puParaIn,
                    MUINT32 u4ParaOutLen,
                    MUINT8 *puParaOut,
                    MUINT32 *pu4RealParaOutLen )
{
    MY_LOG("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    int ret;
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 *i32In = NULL;
    MUINT32 u4SubCmdRealOutLen = 0;
    MBOOL enable;

    switch(op) {

    case FT_CCT_OP_AF_BRECKET_STEP:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_BRECKET_STEP");
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            FOCUS_RANGE_T af_range;
            MINT32 currentPos = 0;
            err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFGetFocusRange((VOID *)&af_range, &u4SubCmdRealOutLen);
            if( err != S_AF_OK || u4SubCmdRealOutLen != sizeof(FOCUS_RANGE_T) ){
                MY_ERR("[%s] CCTOPAFGetFocusRange failed", __FUNCTION__);
                break;
            }

            ACDK_AF_INFO_T af_info;
            err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFGetAFInfo(&af_info, &u4SubCmdRealOutLen);
            if( err != S_AF_OK || u4SubCmdRealOutLen != sizeof(ACDK_AF_INFO_T) ){
                MY_ERR("[%s] CCTOPAFGetAFInfo failed", __FUNCTION__);
                break;
            }
            currentPos = af_info.i4CurrPos;
            //
            MINT32 manualPos;
            i32In = (MINT32 *)puParaIn;
            manualPos = currentPos + *i32In;
            MY_LOG("AF breacket set manual af POS: %d=%d+%d, manual range = (%d, %d)", manualPos, currentPos, *i32In, af_range.i4InfPos, af_range.i4MacroPos);
            if(manualPos > af_range.i4InfPos && manualPos < af_range.i4MacroPos){
                err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPMFOpeartion(manualPos);
                if( err != S_AF_OK ){
                    MY_ERR("[%s] CCTOPMFOpeartion failed", __FUNCTION__);
                    break;
                }
            } else {
                err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFOpeartion();
                if( err != S_AF_OK ){
                    MY_ERR("[%s] CCTOPMFOpeartion failed", __FUNCTION__);
                    break;
                }
            }
        }
        else
            err = CCTIF_BAD_PARAM;
        break;
    case FT_CCT_OP_AE_BRECKET_STEP:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AE_BRECKET_STEP");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            //get realISO
            MINT64 isoBase = 100;
            FrameOutputParam_T RTParams;
            err = IAeMgr::getInstance().getRTParams(mSensorDev, *reinterpret_cast<FrameOutputParam_T*>(&RTParams));
            if( err != S_AE_OK )
            {
                MY_ERR("[%s] getRTParams fail", __FUNCTION__);
            }
            else
            {
                isoBase = (MINT64)RTParams.u4RealISOValue* 1024 / (MINT64)RTParams.u4PreviewSensorGain_x1024* 1024 / (MINT64)RTParams.u4PreviewISPGain_x1024;
                MY_LOG("setManualAEControl: isoBase(%lld),u4RealISOValue(%ld),u4PreviewSensorGain_x1024(%ld),u4PreviewISPGain_x1024(%ld)",
                        isoBase, RTParams.u4RealISOValue, RTParams.u4PreviewSensorGain_x1024, RTParams.u4PreviewISPGain_x1024);
            }
            //get current EV value
            strAEOutput aeOutput;
            i32In = (MINT32 *) puParaIn;
            IAeMgr::getInstance().switchCapureDiffEVState(mSensorDev, (MINT8) (*i32In/10), aeOutput);
            MY_LOG("setAeDiffEvValue : %d ", ((*i32In)/10));
            //set EVValue
            //AE off
            {
                MUINT32 aeMode = MTK_CONTROL_AE_MODE_OFF;
                IAeMgr::getInstance().setAEMode(mSensorDev, aeMode);
                MY_LOG("set MTK_CONTROL_AE_MODE (OFF)");
            }
            AE_SENSOR_PARAM_T strSensorParams;
            strSensorParams.u4Sensitivity   = ( MUINT32 )( ( MINT64 ) aeOutput.EvSetting.u4AfeGain*aeOutput.EvSetting.u4IspGain*isoBase/1024/1024);
            strSensorParams.u8ExposureTime  = ((MUINT32)aeOutput.EvSetting.u4Eposuretime)*1000;
            //strSensorParams.u8FrameDuration = rNewParam.i8FrameDuration;
            MY_LOG("set MTK_SENSOR_SENSITIVITY (%d)", strSensorParams.u4Sensitivity);
            MY_LOG("set MTK_SENSOR_EXPOSURE_TIME (%d) ", strSensorParams.u8ExposureTime);
            IAeMgr::getInstance().UpdateSensorParams(mSensorDev, strSensorParams);
        }
        else
            err = CCTIF_BAD_PARAM;
        break;
    case FT_CCT_OP_AE_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEEnable(mSensorDev);
            else
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    default:
        MY_LOG("Not support cmd %d", (int)op);
        break;

    }
    return err;
}


MINT32
CctHandle::
cct_OpDispatch(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MINT32 status = 0;

    if (op >= FT_CCT_OP_SENSOR_START && op < FT_CCT_OP_3A_START ) {
        status = cct_HandleSensorOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_3A_START && op < FT_CCT_OP_ISP_START ) {
        status = cct_Handle3AOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_ISP_START && op < FT_CCT_OP_NVRAM_START ) {
        status = cct_HandleIspOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_SHELL_START ) {
        status = cct_HandleNvramOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_EMCAM_START && op < FT_CCT_OP_END ) {
        status = cct_HandleEmcamOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else {
        //???
    }

    return status;
}


MINT32
CctHandle::
cct_GetCctOutBufSize(CCT_OP_ID op, MINT32 dtype)
{
    MINT32 idx;

    if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_END ) {
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_STRUCT ) {
            return (CCT_NVRAM_STRUCT_MAXIMUM * sizeof(MINT32));
        }
        // determine the output data size by according the op and dtype
        if ( op == FT_CCT_OP_ISP_SAVE_NVRAM_DATA || op == FT_CCT_OP_ISP_SET_NVRAM_DATA )
            return 0;
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_DATA ) {
            if (dtype >= CCT_NVRAM_DATA_ENUM_MAX )
                return -2;      // data type not found
            else
                return giCctNvramCmdOutBufSize[dtype][1];
        }
        return -1;
    } else {
        // determine the output data size by according the op only
        idx=0;
        for(int i = 0; i < CCT_CMD_OUTBUF_SIZE_COUNT; i++)
        {
            if(giCctCmdOutBufSize[i][0] == op)
            {
                idx = i;
                break;
            }
        }

        if (giCctCmdOutBufSize[idx][0] == op)
            return giCctCmdOutBufSize[idx][1];
        else
            return -1;      // OP_NOT_FOUND
    }
}


MINT32
CctHandle::
cct_GetCctOutBufSize(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn)
{
    MINT32 idx;
    MINT32 dtype;

    if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_NVRAM_TYPE_MAX ) {
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_STRUCT ) {
            if (u4ParaInLen == 0) {
                return (CCT_NVRAM_STRUCT_MAXIMUM * sizeof(MINT32));
            } else
                return -2;
        }
        // determine the output data size by according the op and dtype
        if ( puParaIn == NULL || u4ParaInLen < 4 )
            return -2;
        if ( op == FT_CCT_OP_ISP_SAVE_NVRAM_DATA || op == FT_CCT_OP_ISP_SET_NVRAM_DATA )
            return 0;
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_DATA ) {
            if (u4ParaInLen >= sizeof(MINT32)) {
                dtype = *((MINT32 *)puParaIn);
                if (dtype >= CCT_NVRAM_DATA_ENUM_MAX )
                    return -2;      // data type not found
                else {
                    ALOGD("CCT NVRAM cmd enum[%d] Out Buffer size:%d",dtype,giCctNvramCmdOutBufSize[dtype][1]);
                    return giCctNvramCmdOutBufSize[dtype][1];
                }
            } else
                return -2;
        }
        return -1;
    } else {
        // determine the output data size by according the op only
        idx=0;
        for(int i = 0; i < CCT_CMD_OUTBUF_SIZE_COUNT; i++)
        {
            if(giCctCmdOutBufSize[i][0] == op)
            {
                idx = i;
                break;
            }
        }

        if (giCctCmdOutBufSize[idx][0] == op) {
            ALOGD("CCT cmd Out Buffer size:%d",giCctCmdOutBufSize[idx][1]);
            return giCctCmdOutBufSize[idx][1];
        } else
            return 0;      // for test only
            //return -1;      // OP_NOT_FOUND
    }
}


MINT32
CctHandle::
cctNvram_GetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf, MINT32 outSize, void *pOutBuf, MINT32 *pRealOutSize)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    MY_LOG("[%s] dataType=%d, inSize=%d, outSize=%d", __FUNCTION__, dataType, inSize, outSize);

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        {
        winmo_cct_shading_comp_struct*const pShadingPara = reinterpret_cast<winmo_cct_shading_comp_struct*>(pOutBuf);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        if (pLscMgr == NULL)
        {
            MY_ERR("GET_SHADING_PARA fail! NULL pLscMgr!");
            err = CCTIF_BAD_PARAM;
            break;
        }
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        if (pTbl == NULL)
        {
            MY_ERR("GET_SHADING_PARA fail to get table!");
            err = CCTIF_BAD_PARAM;
            break;
        }
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        pShadingPara->SHADING_EN          = pLscMgr->getOnOff();
        pShadingPara->SHADINGBLK_XNUM     = rCfg.rCfgBlk.i4BlkX+1;
        pShadingPara->SHADINGBLK_YNUM     = rCfg.rCfgBlk.i4BlkY+1;
        pShadingPara->SHADINGBLK_WIDTH    = rCfg.rCfgBlk.i4BlkW;
        pShadingPara->SHADINGBLK_HEIGHT   = rCfg.rCfgBlk.i4BlkH;
        pShadingPara->SHADING_RADDR       = 0;
        pShadingPara->SD_LWIDTH           = rCfg.rCfgBlk.i4BlkLastW;
        pShadingPara->SD_LHEIGHT          = rCfg.rCfgBlk.i4BlkLastH;
        pShadingPara->SDBLK_RATIO00       = 32;
        pShadingPara->SDBLK_RATIO01       = 32;
        pShadingPara->SDBLK_RATIO10       = 32;
        pShadingPara->SDBLK_RATIO11       = 32;
        *pRealOutSize = sizeof(winmo_cct_shading_comp_struct);
        }
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        CCT_SHADING_TAB_STRUCT *CctTabPtr;

        if  (sizeof(CCT_SHADING_TAB_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if  ((inSize < 4) || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        CctTabPtr = (CCT_SHADING_TAB_STRUCT *) pInBuf;

        CCT_SHADING_TAB_STRUCT*const pShadingtabledata  = reinterpret_cast<CCT_SHADING_TAB_STRUCT*> (pOutBuf);
        pShadingtabledata->length= MAX_SHADING_PvwFrm_SIZE;
        pShadingtabledata->color_temperature = CctTabPtr->color_temperature;
        pShadingtabledata->offset = CctTabPtr->offset;
        pShadingtabledata->mode= CctTabPtr->mode;

        NSIspTuning::ESensorMode_T eLscScn = (NSIspTuning::ESensorMode_T)pShadingtabledata->mode;
        MUINT8* pDst = (MUINT8*)(pShadingtabledata->table);
        MUINT32 u4CtIdx = pShadingtabledata->color_temperature;
        MUINT32 u4Size = pShadingtabledata->length;

        ALOGD("[%s +] GET_SHADING_TABLE: SensorMode(%d),CT(%d),Src(%p),Size(%d)", __FUNCTION__,
            eLscScn, u4CtIdx, pDst, u4Size);

        if (pDst == NULL)
        {
            MY_ERR("GET_SHADING_TABLE: NULL pDst");
            err = CCTIF_BAD_PARAM;
            break;
        }

        if (u4CtIdx >= 4)
        {
            MY_ERR("GET_SHADING_TABLE: Wrong CtIdx(%d)", u4CtIdx);
            err = CCTIF_BAD_PARAM;
            break;
        }

        //NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        //const ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();

        // read from nvram buffer
        MUINT32* pSrc = pLscNvram->getLut(ESensorMode_Capture, u4CtIdx);
        ::memcpy(pDst, pSrc, u4Size*sizeof(MUINT32));

        ALOGD("[%s -] GET_SHADING_TABLE", __FUNCTION__);
        *pRealOutSize = sizeof(CCT_SHADING_TAB_STRUCT);
        }
        break;
    case CCT_NVRAM_DATA_LSC:
        if  (sizeof(NVRAM_CAMERA_SHADING_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ::memcpy(pOutBuf, &m_rBuf_SD, sizeof(NVRAM_CAMERA_SHADING_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_SHADING_STRUCT);
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        if  (sizeof(AE_PLINETABLE_T) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetPlineNVRAM(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_AE:
        if  (sizeof(AE_NVRAM_T)*CAM_SCENARIO_NUM != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &(m_rBuf_3A.rAENVRAM[0]), sizeof(AE_NVRAM_T)*AE_NVRAM_IDX_NUM);
        //*pRealOutSize = sizeof(AE_NVRAM_T)*AE_NVRAM_IDX_NUM;

        //or
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetNVRAMParam(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
#endif
    case CCT_NVRAM_DATA_AF:
        if  (sizeof(NVRAM_LENS_PARA_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &m_rBuf_LN, sizeof(NVRAM_LENS_PARA_STRUCT));
        //*pRealOutSize = sizeof(NVRAM_LENS_PARA_STRUCT);

        //or
        err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFGetNVRAMParam( (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_AWB:
        if  (sizeof(AWB_NVRAM_T)*CAM_SCENARIO_NUM != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &(m_rBuf_3A.rAWBNVRAM[0]), sizeof(AWB_NVRAM_T)*CCT_AWB_NVRAM_TBL_NO);
        //*pRealOutSize = sizeof(AWB_NVRAM_T)*CCT_AWB_NVRAM_TBL_NO;

        //or
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetNVRAMParam(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
#endif
    case CCT_NVRAM_DATA_ISP:
        if  (sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ::memcpy(pOutBuf, &m_rBuf_ISP, sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT);
        break;
    case CCT_NVRAM_DATA_FEATURE:
        {
        if  (sizeof(NVRAM_CAMERA_FEATURE_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_FEATURE_STRUCT* pNvram;
        NVRAM_CAMERA_FEATURE_STRUCT* pCctNvram = reinterpret_cast<NVRAM_CAMERA_FEATURE_STRUCT*>(pOutBuf);
        err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pNvram, 1);
        *pCctNvram = *pNvram;
        *pRealOutSize = sizeof(NVRAM_CAMERA_FEATURE_STRUCT);
        }
        break;
    case CCT_NVRAM_DATA_STROBE:
        if  (sizeof(NVRAM_CAMERA_STROBE_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ret = FlashMgr::getInstance(mSensorDev)->cctReadNvramToPcMeta((VOID *)pOutBuf, (MUINT32 *) pRealOutSize);
        if(ret!=0)
            err=CCTIF_UNKNOWN_ERROR;
        *pRealOutSize = sizeof(NVRAM_CAMERA_STROBE_STRUCT);
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_FLASH_AWB:
        if  (sizeof(FLASH_AWB_NVRAM_T) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        /* TODO: modify for AWB nvram */
        //::memcpy(pOutBuf, &(m_rBuf_3A.Flash_AWB[0]), sizeof(FLASH_AWB_NVRAM_T));
        *pRealOutSize = sizeof(FLASH_AWB_NVRAM_T);
        break;
#else
    case CCT_NVRAM_DATA_FLASH_CALIBRATION:
        if  (sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        memcpy(pOutBuf, &m_rBuf_FC, sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT);
        break;
#endif
#ifndef mt6775
    case CCT_NVRAM_DATA_3A:
        if  (sizeof(NVRAM_CAMERA_3A_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        memcpy(pOutBuf, &m_rBuf_3A, sizeof(NVRAM_CAMERA_3A_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_3A_STRUCT);
        break;
#endif
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }
    if( pRealOutSize ) {
        MY_LOG("[%s] err=%d, pRealOutSize=%d", __FUNCTION__, err, *pRealOutSize);
    }
    return err;
}


MINT32
CctHandle::
cctNvram_SetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    MY_LOG("[%s] dataType=%d, inSize=%d", __FUNCTION__, dataType, inSize);

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        {
        if  ( sizeof(winmo_cct_shading_comp_struct) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        winmo_cct_shading_comp_struct*const pShadingPara = reinterpret_cast<winmo_cct_shading_comp_struct*>(pInBuf);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        // only change grid number to NVRAM, do not let HW take effect immediately.
        pLscData->GridXNum = pShadingPara->SHADINGBLK_XNUM + 1;
        pLscData->GridYNum = pShadingPara->SHADINGBLK_YNUM + 1;
        pLscData->Width    = rCfg.i4ImgWd;
        pLscData->Height   = rCfg.i4ImgHt;
        }
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        if  ( sizeof(CCT_SHADING_TAB_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        CCT_SHADING_TAB_STRUCT*const pShadingtabledata  = reinterpret_cast<CCT_SHADING_TAB_STRUCT*> (pInBuf);

        NSIspTuning::ESensorMode_T eLscScn = (NSIspTuning::ESensorMode_T)pShadingtabledata->mode;
        //MUINT8* pSrc = (MUINT8*)(pShadingtabledata->pBuffer);
        MUINT8* pSrc = (MUINT8*)(pShadingtabledata->table);
        MUINT32 u4CtIdx = pShadingtabledata->color_temperature;
        MUINT32 u4Size = pShadingtabledata->length;

        MY_LOG("[%s +] SET_SHADING_TABLE: SensorMode(%d),CT(%d),Src(%p),Size(%d)", __FUNCTION__,
            eLscScn, u4CtIdx, pSrc, u4Size);

        if (pSrc == NULL)
        {
            MY_ERR("SET_SHADING_TABLE: NULL pSrc");
            err = CCTIF_BAD_PARAM;
            break;
        }

        if (u4CtIdx >= 4)
        {
            MY_ERR("SET_SHADING_TABLE: Wrong CtIdx(%d)", u4CtIdx);
            err = CCTIF_BAD_PARAM;
            break;
        }

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        //const ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();

        // write to nvram buffer
        MUINT32* pDst = pLscNvram->getLut(ESensorMode_Capture, u4CtIdx);
        ::memcpy(pDst, pSrc, u4Size*sizeof(MUINT32));
        // reset flow to validate ?
        pLscMgr->CCTOPReset();
        }
        break;
    case CCT_NVRAM_DATA_LSC:
        // This case combined CCT_NVRAM_DATA_LSC_PARA and CCT_NVRAM_DATA_LSC_TABLE
        {
        if  ( sizeof(NVRAM_CAMERA_SHADING_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_SHADING_STRUCT* const pShadingNvram  = reinterpret_cast<NVRAM_CAMERA_SHADING_STRUCT*> (pInBuf);
        // get LSC nvram
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        ISP_SHADING_STRUCT* pDst = pLscNvram->getLscNvram();
        // need to overwrite sensor real witdth, height
        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        // write to nvram buffer
        ::memcpy(pDst, &(pShadingNvram->Shading), sizeof(ISP_SHADING_STRUCT));
        pDst->Width    = rCfg.i4ImgWd;
        pDst->Height   = rCfg.i4ImgHt;

        // reset flow to validate ?
        pLscMgr->CCTOPReset();
        }
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        if  ( sizeof(AE_PLINETABLE_T) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyPlineNVRAM(mSensorDev, (VOID *)pInBuf);
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_AE:
        {
        if  ( sizeof(AE_NVRAM_T) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyNVRAMParam(mSensorDev, (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
#endif
#ifdef mt6775
    case CCT_NVRAM_DATA_AF:
        {
        if  ( sizeof(NVRAM_LENS_DATA_PARA_STRUCT) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAfMgr::getInstance().CCTOPAFApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFApplyNVRAMParam( (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
#else
    case CCT_NVRAM_DATA_AF:
        {
        if ( sizeof(NVRAM_LENS_PARA_STRUCT) != inSize || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        memcpy((void *)&m_rBuf_LN, (void *)pInBuf, sizeof(NVRAM_LENS_PARA_STRUCT));
        break;
        }
#endif
    case CCT_NVRAM_DATA_AWB:
        {
        if  ( sizeof(AWB_NVRAM_T) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBApplyNVRAMParam(mSensorDev, (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
    case CCT_NVRAM_DATA_ISP:
        {
        // TO DO ......
        // There is no related implementation found in the previous CCT. Need to add the implementation here.
        if ( sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) != inSize || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //NVRAM_CAMERA_ISP_PARAM_STRUCT* pIspParam = reinterpret_cast<NVRAM_CAMERA_ISP_PARAM_STRUCT*>(pInBuf);
        //err = cctNvram_SetIspNvramData( &(pIspParam->ISPRegs) );
        memcpy((void *)&m_rBuf_ISP, (void *)pInBuf, sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        break;
        }
    case CCT_NVRAM_DATA_FEATURE:
        {
        if  ( sizeof(NVRAM_CAMERA_FEATURE_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_FEATURE_STRUCT* pNvram;
        NVRAM_CAMERA_FEATURE_STRUCT* pCctNvram = reinterpret_cast<NVRAM_CAMERA_FEATURE_STRUCT*>(pInBuf);

        //test only
        // MUINT32 th_old;
        // MUINT32 th_apply;
        // MUINT32 th_new;

        err = NvBufUtil::getInstance().getBuf(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pNvram);

        //test only
        // th_old = pNvram->mfll.mfll_iso_th;
        // th_apply = pCctNvram->mfll.mfll_iso_th;

        *pNvram = *pCctNvram;

        //test only
        // NVRAM_CAMERA_FEATURE_STRUCT* pReadNvram;
        // err = NvBufUtil::getInstance().getBuf(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pReadNvram);
        // th_new = pReadNvram->mfll.mfll_iso_th;
        // MY_LOG("apply mfll_iso_th old(%d) apply(%d) new(%d)", th_old, th_apply, th_new );
        }
        break;
    case CCT_NVRAM_DATA_STROBE:
        if  ( sizeof(NVRAM_CAMERA_STROBE_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ret = FlashMgr::getInstance(mSensorDev)->cctSetNvdataMeta(pInBuf, inSize);
        if(ret!=0)
            err=CCTIF_UNKNOWN_ERROR;
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_FLASH_AWB:
        if  ( sizeof(FLASH_AWB_NVRAM_T) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        err = NS3Av3::IAwbMgr::getInstance().CCTOPFlashAWBApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        break;
#else
    case CCT_NVRAM_DATA_FLASH_CALIBRATION:
        {
        if ( sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT) != inSize || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        memcpy((void *)&m_rBuf_FC, (void *)pInBuf, sizeof(NVRAM_CAMERA_FLASH_CALIBRATION_STRUCT));
        break;
        }
#endif
#ifndef mt6775
    case CCT_NVRAM_DATA_3A:
        if  ( sizeof(NVRAM_CAMERA_3A_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
//        memcpy(&m_rBuf_3A, pInBuf, sizeof(NVRAM_CAMERA_3A_STRUCT));
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBApplyK71NVRAMParam(mSensorDev,(MVOID*)pInBuf);
        break;
#endif
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }

    MY_LOG("[%s] err=%d", __FUNCTION__, err);
    return err;
}


MINT32
CctHandle::
cctNvram_SetPartialNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT32 bufOffset, MUINT8 *pInBuf)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    MY_LOG("[%s] dataType=%d, inSize=%d, bufOffset=%d", __FUNCTION__, dataType, inSize, bufOffset);

    switch (dataType) {
    case CCT_NVRAM_DATA_ISP:
        {
        MUINT8 *pIspParamBuf;
        if ( inSize <= 0 || ! pInBuf || bufOffset >= sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT)) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if ( (inSize + bufOffset) > sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT)) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        pIspParamBuf = (MUINT8 *)&m_rBuf_ISP;
        pIspParamBuf += bufOffset;
        memcpy((void *)pIspParamBuf, (void *)pInBuf, inSize);
        }
        break;
    default:
        err = CCTIF_BAD_PARAM;
        break;
    }
    MY_LOG("[%s] err=%d", __FUNCTION__, err);
    return err;
}



MINT32
CctHandle::
cctNvram_SaveNvramData(CCT_NVRAM_DATA_T dataType)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    MY_LOG("[%s] dataType=%d", __FUNCTION__, dataType);

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);

        if (pLscNvram->writeNvramTbl()) {
            MY_LOG("[%s] SDTBL_SAVE_TO_NVRAM OK", __FUNCTION__);
        }
        else {
            MY_ERR("SDTBL_SAVE_TO_NVRAM fail");
            err = CCTIF_UNKNOWN_ERROR;
        }
        }
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        err = NS3Av3::IAeMgr::getInstance().CCTOPAESavePlineNVRAM(mSensorDev);
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_AE:
        err = NS3Av3::IAeMgr::getInstance().CCTOPAESaveNVRAMParam(mSensorDev);
        break;
#endif
    case CCT_NVRAM_DATA_AF:
        err = NS3Av3::IAfMgr::getInstance(mSensorDev).CCTOPAFSaveNVRAMParam();
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_AWB:
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSaveNVRAMParam(mSensorDev);
        break;
#endif
    case CCT_NVRAM_DATA_ISP:
        MY_LOG("IMP_CCT_CTRL( ACDK_CCT_OP_ISP_SAVE_TO_NVRAM )");

        ret = NvBufUtil::getInstance().write(CAMERA_NVRAM_DATA_ISP, m_eSensorEnum);
        if  ( 0 != ret ) {
            MY_ERR("[ACDK_CCT_OP_ISP_SAVE_TO_NVRAM] write fail err=%d(0x%x)\n", ret , ret );
            err = CCTIF_UNKNOWN_ERROR;
        } else
            MY_LOG("IMP_CCT_CTRL( ACDK_CCT_OP_ISP_SAVE_TO_NVRAM ) done");
        break;
    case CCT_NVRAM_DATA_FEATURE:
        err = NvBufUtil::getInstance().write(CAMERA_NVRAM_DATA_FEATURE, mSensorDev);
        break;
    case CCT_NVRAM_DATA_STROBE:
        MY_LOG("ACDK_CCT_OP_STROBE_WRITE_NVRAM");
        ret = FlashMgr::getInstance(mSensorDev)->cctWriteNvram();
        if(ret!=0)
            err=CCTIF_UNKNOWN_ERROR;
        break;
#ifdef mt6775
    case CCT_NVRAM_DATA_FLASH_AWB:
        err = NS3Av3::IAwbMgr::getInstance().CCTOPFlashAWBSaveNVRAMParam(mSensorDev);
        break;
#else
    case CCT_NVRAM_DATA_FLASH_CALIBRATION:
        err = NS3Av3::IAwbMgr::getInstance().CCTOPFlashCalibrationSaveNVRAMParam(mSensorDev);
        break;
#endif
#ifndef mt6775
    case CCT_NVRAM_DATA_3A:
        err = NS3Av3::IAeMgr::getInstance().CCTOPAESaveNVRAMParam(mSensorDev);
        break;
#endif
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }
    MY_LOG("[%s] err=%d", __FUNCTION__, err);
    return err;
}


MINT32
CctHandle::
cctNvram_SetIspNvramData(ISP_NVRAM_REGISTER_STRUCT *pIspRegs)
{
    MINT32 index;

//Chooo
#if 0
    index = pIspRegs->Idx.SL2F;
    if (index < NVRAM_SL2F_TBL_NUM) {
        m_rISPRegs.SL2F[index] = (pIspRegs->SL2F[index]);
        m_rISPRegsIdx.SL2F = static_cast<MUINT8>(index);
        ISP_MGR_SL2F_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2F[index]));
        ISP_MGR_SL2G_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2F[index]));
    }

    index = pIspRegs->Idx.DBS;
    if (index < NVRAM_DBS_TBL_NUM) {
        m_rISPRegs.DBS[index] = (pIspRegs->DBS[index]);
        ISP_MGR_DBS_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->DBS[index]));
        ISP_MGR_DBS2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->DBS[index]));
    }

    index = pIspRegs->Idx.ADBS;
    if (index < NVRAM_ADBS_TBL_NUM) {
        m_rISPRegs.ADBS[index] = (pIspRegs->ADBS[index]);
        ISP_MGR_ADBS_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->ADBS[index]));
        ISP_MGR_ADBS2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->ADBS[index]));
    }

    index = pIspRegs->Idx.OBC;
    if (index < NVRAM_OBC_TBL_NUM) {
        m_rISPRegs.OBC[index] = (pIspRegs->OBC[index]);
        ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->OBC[index]));
        ISP_MGR_OBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->OBC[index]));
    }

    index = pIspRegs->Idx.BNR_BPC;
    if (index < NVRAM_BPC_TBL_NUM) {
        m_rISPRegs.BNR_BPC[index] = (pIspRegs->BNR_BPC[index]);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_BPC[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_BPC[index]));
    }

    index = pIspRegs->Idx.BNR_NR1;
    if (index < NVRAM_NR1_TBL_NUM) {
        m_rISPRegs.BNR_NR1[index] = (pIspRegs->BNR_NR1[index]);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_NR1[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_NR1[index]));
    }

    index = pIspRegs->Idx.BNR_PDC;
    if (index < NVRAM_PDC_TBL_NUM) {
        m_rISPRegs.BNR_PDC[index] = (pIspRegs->BNR_PDC[index]);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_PDC[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_PDC[index]));
    }
#if 0
    index = pIspRegs->Idx.RMM;
    if (index < NVRAM_RMM_TBL_NUM) {
        m_rISPRegs.RMM[index] = (pIspRegs->RMM[index]);
        ISP_MGR_RMM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RMM[index]));
        ISP_MGR_RMM2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RMM[index]));
    }
#endif
    index = pIspRegs->Idx.RNR;
    if (index < NVRAM_RNR_TBL_NUM) {
        m_rISPRegs.RNR[index] = (pIspRegs->RNR[index]);
        ISP_MGR_RNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RNR[index]));
    }

    index = pIspRegs->Idx.SL2;
    if (index < NVRAM_SL2_TBL_NUM) {
        m_rISPRegs.SL2[index] = (pIspRegs->SL2[index]);
        ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2[index]));
    }

    index = pIspRegs->Idx.UDM;
    if (index < NVRAM_UDM_TBL_NUM) {
        m_rISPRegs.UDM[index] = (pIspRegs->UDM[index]);
        ISP_MGR_UDM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->UDM[index]));
    }

#if 0
    index = pIspRegs->Idx.CCM;
    if (index < NVRAM_CCM_TBL_NUM) {
        m_rISPRegs.CCM[index] = (pIspRegs->CCM[index]);
        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->CCM[index]));
    }

    index = pIspRegs->Idx.GGM;
    if (index < NVRAM_GGM_TBL_NUM) {
        m_rISPRegs.GGM[index] = (pIspRegs->GGM[index]);
        ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->GGM[index]));
    }

    index = pIspRegs->Idx.IHDR_GGM;
    if (index < NVRAM_IHDR_GGM_TBL_NUM) {
        m_rISPRegs.IHDR_GGM[index] = (pIspRegs->IHDR_GGM[index]);
        //
        // Need to check how to apply the IHDR_GGM setting to the hardware. There is no ISP_MGR_IHDR_GGM_T existed.
        //ISP_MGR_IHDR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).put(&(pIspRegs->IHDR_GGM[index]));     //????????
    }
#endif

    index = pIspRegs->Idx.ANR;
    if (index < NVRAM_ANR_TBL_NUM) {
        m_rISPRegs.NBC_ANR[index] = (pIspRegs->NBC_ANR[index]);
        ISP_MGR_NBC_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->NBC_ANR[index]));
    }

    index = pIspRegs->Idx.ANR2;
    if (index < NVRAM_ANR2_TBL_NUM) {
        m_rISPRegs.NBC2_ANR2[index] = (pIspRegs->NBC2_ANR2[index]);
        ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->NBC2_ANR2[index]));
    }

    index = pIspRegs->Idx.CCR;
    if (index < NVRAM_CCR_TBL_NUM) {
        m_rISPRegs.NBC2_CCR[index] = (pIspRegs->NBC2_CCR[index]);
        ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->NBC2_CCR[index]));
    }

    index = pIspRegs->Idx.HFG;
    if (index < NVRAM_HFG_TBL_NUM) {
        m_rISPRegs.HFG[index] = (pIspRegs->HFG[index]);
        ISP_MGR_HFG_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->HFG[index]));
    }

    index = pIspRegs->Idx.EE;
    if (index < NVRAM_EE_TBL_NUM) {
        m_rISPRegs.EE[index] = (pIspRegs->EE[index]);
        ISP_MGR_SEEE_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->EE[index]));
    }

    index = pIspRegs->Idx.MFB;
    if (index < NVRAM_MFB_TBL_NUM) {
        m_rISPRegs.MFB[index] = (pIspRegs->MFB[index]);
        ISP_MGR_MFB_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->MFB[index]));
    }

    index = pIspRegs->Idx.MIXER3;
    if (index < NVRAM_MIXER3_TBL_NUM) {
        m_rISPRegs.MIXER3[index] = (pIspRegs->MIXER3[index]);
        ISP_MGR_MIXER3_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->MIXER3[index]));
    }

#endif
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);

    MY_LOG("[%s] done ", __FUNCTION__);
    return  CCTIF_NO_ERROR;
}

