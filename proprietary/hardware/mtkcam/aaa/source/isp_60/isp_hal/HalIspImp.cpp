/* Copyright Statement:
*
* This software/firmware and related documentation ("MediaTek Software") are
* protected under relevant copyright laws. The information contained herein is
* confidential and proprietary to MediaTek Inc. and/or its licensors. Without
* the prior written permission of MediaTek inc. and/or its licensors, any
* reproduction, modification, use or disclosure of MediaTek Software, and
* information contained herein, in whole or in part, shall be strictly
* prohibited.
*
* MediaTek Inc. (C) 2010. All rights reserved.
*
* BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
* THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
* RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
* ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
* WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
* WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
* NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
* RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
* INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
* TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
* RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
* OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
* SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
* RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
* STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
* ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
* RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
* MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
* CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
* The following software/firmware and/or related documentation ("MediaTek
* Software") have been modified by MediaTek Inc. All revisions are subject to
* any receiver's applicable license agreements with MediaTek Inc.
*/

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "HalIspImp"

#include <mtkcam/utils/std/Log.h>
#include <mtkcam/def/common.h>
#include <mtkcam/utils/std/common.h>

//For Metadata
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include "aaa_utils.h"


#include <mtkcam/utils/std/Trace.h>
#include <aaa_trace.h>

#include <mtkcam/drv/IHalSensor.h>

#include <mutex>
#include <faces.h>

//tuning utils
#include <mtkcam/utils/TuningUtils/FileReadRule.h>
#include <SttBufQ.h>
#include <mtkcam/utils/TuningUtils/FileDumpNamingRule.h>
#include <mtkcam/utils/sys/IFileCache.h>

#include <sys/stat.h>

#include <isp_tuning_buf.h>

#include <dip_reg.h>

#include <mtkcam/utils/exif/IBaseCamExif.h>
#include <isp_mgr.h>

// Index manager
#if MTK_CAM_NEW_NVRAM_SUPPORT
#include <mtkcam/utils/mapping_mgr/cam_idx_mgr.h>
#include <nvbuf_util.h>
#include <EModule_string.h>
#include <EApp_string.h>
#include <EIspProfile_string.h>
#include <ESensorMode_string.h>
#include "camera_custom_msdk.h"
#endif

#include "HalIspImp.h"

using namespace NS3Av3;
using namespace NSCam;
using namespace NSCam::TuningUtils;


#if defined(HAVE_AEE_FEATURE)
#include <aee.h>
#define AEE_ASSERT_ISP_HAL(String) \
          do { \
              aee_system_exception( \
                  "HalISP", \
                  NULL, \
                  DB_OPT_DEFAULT, \
                  String); \
          } while(0)
#else
#define AEE_ASSERT_ISP_HAL(String)
#endif

#define GET_PROP(prop, dft, val)\
{\
   val = property_get_int32(prop,dft);\
}

#define HALISP_LOG_SET_P1       (1<<0)
#define HALISP_LOG_SET_P2       (2<<0)
#define HALISP_LOG_SETPARAM_P2  (3<<0)
#define HALISP_LOG_GET_P1       (4<<0)
#define HALISP_LOG_GET_P2       (5<<0)
#define HALISP_LOG_CONVERT_P2   (6<<0)

MUINT32 HalIspImp::m_u4LogEn = 0;

static void
_dumpAAO(const char* filename, MUINT32 SensorDev, MINT32 MagicNumberRequest)
{
    android::sp<IFileCache> fptr;

    android::sp<ISttBufQ> pSttBufQ;
    android::sp<ISttBufQ::DATA> pData;
    int N;

    pSttBufQ = ISttBufQ::getInstance(SensorDev);
    if (!pSttBufQ.get()) {
        CAM_LOGW("ISttBufQ::getInstance(SensorDev=%d) error!!", SensorDev);
        goto lbExit;
    }

    pData = pSttBufQ->deque_byMagicNumberRequest(MagicNumberRequest);
    if (!pData.get()) {
        CAM_LOGW("pSttBufQ->GetByMagicNumberRequest(%d) error!!", MagicNumberRequest);
        goto lbExit;
    }

    fptr = IFileCache::open(filename);
    if (fptr.get()==NULL) {
        CAM_LOGW("open file(%s) error!!", filename);
        goto lbExit;
    }

    N = pData->AAO.size();
    if ((int)fptr->write(pData->AAO.editArray(), N) != N) {
        CAM_LOGW("file(%s) write error!!", filename);
        goto lbExit;
    }

lbExit:
    if(pData.get()) {
        pSttBufQ->enque_first(pData);
    }
}

static
inline MBOOL
_dumpDebugInfo(const char* filename, const IMetadata& metaExif)
{
    android::sp<IFileCache> fid = IFileCache::open(filename);
    if (fid.get())
    {
        IMetadata::Memory p3ADbg;
        if (IMetadata::getEntry<IMetadata::Memory>(&metaExif, MTK_3A_EXIF_DBGINFO_AAA_DATA, p3ADbg))
        {
            CAM_LOGD("[%s] %s, 3A(%p, %d)", __FUNCTION__, filename, p3ADbg.array(), (MINT32)p3ADbg.size());
            MUINT8 hdr[6] = {0, 0, 0xFF, 0xE6, 0, 0};
            MUINT16 size = (MUINT16)(p3ADbg.size()+2);
            hdr[4] = (size >> 8); // big endian
            hdr[5] = size & 0xFF;
            fid->write(hdr, 6);
            fid->write(p3ADbg.array(), p3ADbg.size());
        }
        IMetadata::Memory pIspDbg;
        if (IMetadata::getEntry<IMetadata::Memory>(&metaExif, MTK_3A_EXIF_DBGINFO_ISP_DATA, pIspDbg))
        {
            CAM_LOGD("[%s] %s, ISP(%p, %d)", __FUNCTION__, filename, pIspDbg.array(), (MINT32)pIspDbg.size());
            MUINT8 hdr[4] = {0xFF, 0xE7, 0, 0};
            MUINT16 size = (MUINT16)(pIspDbg.size()+2);
            hdr[2] = (size >> 8);
            hdr[3] = size & 0xFF;
            fid->write(hdr, 4);
            fid->write(pIspDbg.array(), pIspDbg.size());
        }
        return MTRUE;
    } else {
        CAM_LOGW("IFileCache open fail <%s>", filename);
        return MFALSE;
    }
}

static MBOOL _isFileExist(const char* file) {
  struct stat buffer;
  return (stat (file, &buffer) == 0);
}

/*******************************************************************************
* implementations
********************************************************************************/
HalIspImp*
HalIspImp::
createInstance(MINT32 const i4SensorIdx, const char* strUser)
{
    GET_PROP("vendor.debug.halisp.log", 0, m_u4LogEn);
    CAM_LOGD("[%s] sensorIdx(%d) %s", __FUNCTION__, i4SensorIdx, strUser);
    switch (i4SensorIdx)
    {
    case 0:
        {
            static HalIspImp _singleton(0);
            _singleton.init(strUser);
            return &_singleton;
        }
    case 1:
        {
            static HalIspImp _singleton(1);
            _singleton.init(strUser);
            return &_singleton;
        }
    case 2:
        {
            static HalIspImp _singleton(2);
            _singleton.init(strUser);
            return &_singleton;
        }
    case 3:
        {
            static HalIspImp _singleton(3);
            _singleton.init(strUser);
            return &_singleton;
        }
    default:
        CAM_LOGE("Unsupport sensor Index: %d\n", i4SensorIdx);
        return NULL;
    }
}

HalIspImp::
HalIspImp(MINT32 const i4SensorIdx)
    : m_Users(0)
    , m_Lock()
    , m_i4SensorIdx(i4SensorIdx)
    , m_i4SensorDev(0)
    , m_pTuning(NULL)
    , m_pCamIO(NULL)
    , m_pResultPoolObj(NULL)
    , m_i4SensorMode(0)
    , m_i4TgWidth(1000)
    , m_i4TgHeight(1000)
    , m_i4faceNum(0)
    , m_bFaceDetectEnable(0)
    , m_eIspProfile(NSIspTuning::EIspProfile_Preview)
    , m_i4CopyLscP1En(0)
    , m_rResultMtx()
    , m_u1ColorCorrectMode(MTK_COLOR_CORRECTION_MODE_FAST)
    , m_u1IsGetExif(0)
    , m_bIsCapEnd(0)
    , m_pLcsDrv(NULL)
    , m_i4SubsampleCount(1)
{
    // query SensorDev from HalSensorList
    IHalSensorList* const pHalSensorList = MAKE_HalSensorList();
    if (!pHalSensorList)
        CAM_LOGE("[%s] MAKE HalSensorList fail", __FUNCTION__);
    else
        m_i4SensorDev = pHalSensorList->querySensorDevIdx(i4SensorIdx);

    CAM_LOGD("[%s] sensorIdx(0x%04x) sensorDev(%d)", __FUNCTION__, i4SensorIdx, m_i4SensorDev);
}

MVOID
HalIspImp::
destroyInstance(const char* strUser)
{
    CAM_LOGD("[%s]+ sensorIdx(%d)  User(%s)", __FUNCTION__, m_i4SensorIdx, strUser);
    uninit(strUser);
    CAM_LOGD("[%s]- ", __FUNCTION__);
}

MINT32
HalIspImp::
config(const ConfigInfo_T& rConfigInfo)
{
    CAM_LOGD("[%s] config i4SubsampleCount(%d)", __FUNCTION__, rConfigInfo.i4SubsampleCount);

    MUINT32 u4AFWidth, u4AFHeight;

    if (m_i4SubsampleCount != rConfigInfo.i4SubsampleCount){
        CAM_LOGD("[%s] m_i4SubsampleCount(%d, %d)", __FUNCTION__, m_i4SubsampleCount, rConfigInfo.i4SubsampleCount);
        m_i4SubsampleCount = rConfigInfo.i4SubsampleCount;
        // TuningMgr uninit
        if (m_pTuning)
        {
            CAM_LOGD("[%s] m_pTuning uninit +", __FUNCTION__);
            m_pTuning->uninit(m_i4SensorDev);
            m_pTuning = NULL;
            CAM_LOGD("[%s] m_pTuning uninit -", __FUNCTION__);
        }
        //====== Destroy LCS Driver ======
        //if(m_pLcsDrv != NULL)
        //{

        //    m_pLcsDrv->Uninit();
        //    m_pLcsDrv->DestroyInstance();
        //    m_pLcsDrv = NULL;

        //}
       // TuningMgr init
        if (m_pTuning == NULL)
        {
            CAM_LOGD("[%s] m_pTuning init +", __FUNCTION__);
            AAA_TRACE_D("TUNING init");
            m_pTuning = &IspTuningMgr::getInstance();
            if (!m_pTuning->init(m_i4SensorDev, m_i4SensorIdx, rConfigInfo.i4SubsampleCount))
            {
                CAM_LOGE("Fail to init IspTuningMgr (%d,%d)", m_i4SensorDev, m_i4SensorIdx);
                AEE_ASSERT_ISP_HAL("Fail to init IspTuningMgr");
                AAA_TRACE_END_D;
                return MFALSE;
            }
            CAM_LOGD("[%s] m_pTuning init -", __FUNCTION__);
            AAA_TRACE_END_D;
        }
        //InitLCS();
        //querySensorStaticInfo(); //Add this, will NE
    }


    // NormalIOPipe create instance
    if (m_pCamIO == NULL)
    {
        m_pCamIO = (INormalPipe*)INormalPipeUtils::get()->createDefaultNormalPipe(m_i4SensorIdx, LOG_TAG);
        if (m_pCamIO == NULL)
        {
            CAM_LOGE("Fail to create NormalPipe");
            return MFALSE;
        }
    }

    updateTGInfo();

    // query input size info for AFO
    m_pCamIO->sendCommand(NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_BIN_INFO,
                        (MINTPTR)&u4AFWidth, (MINTPTR)&u4AFHeight, 0);
    MBOOL bFrontalBin = (m_i4TgWidth == (MINT32)u4AFWidth && m_i4TgHeight == (MINT32)u4AFHeight) ? MFALSE : MTRUE;

    m_pTuning->setSensorMode(m_i4SensorDev, m_i4SensorMode, bFrontalBin, u4AFWidth, u4AFHeight);
    //Need change
    m_pTuning->setIspProfile(m_i4SensorDev, m_eIspProfile);

    MBOOL bEnable_flk = MFALSE;
    m_pCamIO->sendCommand(NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_FLK_INFO,
                        (MINTPTR)&bEnable_flk, 0, 0);

    IspTuningMgr::getInstance().setFlkEnable(m_i4SensorDev, bEnable_flk);
    m_pTuning->notifyRPGEnable(m_i4SensorDev, MTRUE);   // apply awb gain for init stat

    //valid shading params
    const LSCConfigResult_T *m_pISPLscResult=(LSCConfigResult_T*)m_pResultPoolObj->getResult(ConfigMagic, E_LSC_CONFIGRESULTTOISP, __FUNCTION__); // request frame no 1 for config

    if(m_pISPLscResult != NULL) {
        ISP_MGR_LSC_T::getInstance(static_cast<ESensorDev_T>(m_i4SensorDev)).putBufAndRatio(*m_pISPLscResult->rResultBuf, m_pISPLscResult->ratio);
    } else {
        CAM_LOGE("[%s] no E_LSC_CONFIGRESULTTOISP in result pool", __FUNCTION__);
    }

    RequestSet_T rRequestSet;
    rRequestSet.vNumberSet.clear();
    for (MINT32 i4Num = 1; i4Num <= m_i4SubsampleCount; i4Num++)
        rRequestSet.vNumberSet.push_back(i4Num);

    //m_pTuning->validatePerFrameP1(m_i4SensorDev, rRequestSet, MTRUE);
    IspTuningMgr::getInstance().validate(m_i4SensorDev, rRequestSet, MTRUE);

    CAM_LOGD("[%s]- AFWH(%dx%d), bEnable_flk=%d", __FUNCTION__, u4AFWidth, u4AFHeight, (int)bEnable_flk);
    return MTRUE;
}

MVOID
HalIspImp::
setSensorMode(MINT32 i4SensorMode)
{
    CAM_LOGD("[%s] mode(%d)", __FUNCTION__, i4SensorMode);
    m_i4SensorMode = i4SensorMode;
}

MBOOL
HalIspImp::
init(const char* strUser)
{
    GET_PROP("vendor.debug.camera.copy.p1.lsc", 0, m_i4CopyLscP1En);

    CAM_LOGD("[%s] m_Users: %d, SensorDev %d, index %d \n", __FUNCTION__, std::atomic_load((&m_Users)), m_i4SensorDev, m_i4SensorIdx);

    // check user count
    std::lock_guard<std::mutex> lock(m_Lock);

    if (m_Users > 0)
    {
        CAM_LOGD("[%s] %d has created \n", __FUNCTION__, std::atomic_load((&m_Users)));
        MINT32 ret __unused = std::atomic_fetch_add((&m_Users), 1);
        return MTRUE;
    }


    // TuningMgr init
    if (m_pTuning == NULL)
    {
        AAA_TRACE_D("TUNING init");
        m_pTuning = &IspTuningMgr::getInstance();
        if (!m_pTuning->init(m_i4SensorDev, m_i4SensorIdx))
        {
            CAM_LOGE("Fail to init IspTuningMgr (%d,%d)", m_i4SensorDev, m_i4SensorIdx);
            AEE_ASSERT_ISP_HAL("Fail to init IspTuningMgr");
            AAA_TRACE_END_D;
            return MFALSE;
        }
        AAA_TRACE_END_D;
    }

    if(m_pResultPoolObj == NULL)
        m_pResultPoolObj = IResultPool::getInstance(m_i4SensorDev);
    if(m_pResultPoolObj == NULL)
        CAM_LOGE("ResultPool getInstance fail");

    querySensorStaticInfo();

    MINT32 ret __unused = std::atomic_fetch_add((&m_Users), 1);
    return MTRUE;
}

MBOOL
HalIspImp::
uninit(const char* strUser)
{
    std::lock_guard<std::mutex> lock(m_Lock);

    // If no more users, return directly and do nothing.
    if (m_Users <= 0)
    {
        return MTRUE;
    }
    CAM_LOGD("[%s] m_Users: %d \n", __FUNCTION__, std::atomic_load((&m_Users)));

    // More than one user, so decrease one User.
    MINT32 ret __unused = std::atomic_fetch_sub((&m_Users), 1);

    if (m_Users == 0) // There is no more User after decrease one User
    {
        // TuningMgr uninit
        if (m_pTuning)
        {
            m_pTuning->uninit(m_i4SensorDev);
            m_pTuning = NULL;
        }

        //====== Destroy LCS Driver ======

        if(m_pLcsDrv != NULL)
        {

            m_pLcsDrv->Uninit();
            m_pLcsDrv->DestroyInstance();
            m_pLcsDrv = NULL;

        }
        CAM_LOGD("[%s] done\n", __FUNCTION__);

    }
    else    // There are still some users.
    {
        CAM_LOGD("[%s] Still %d users \n", __FUNCTION__, std::atomic_load((&m_Users)));
    }
    return MTRUE;
}

MBOOL
HalIspImp::
start()
{
    CAM_LOGD("[%s] +", __FUNCTION__);
    //RequestSet_T rRequestSet;
    //rRequestSet.vNumberSet.clear();
    //rRequestSet.vNumberSet.push_back(1);
    //m_pTuning->validatePerFrameP1(m_i4SensorDev, rRequestSet, MTRUE);

    IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_SET_GMA_SCENARIO, IspTuningMgr::E_GMA_SCENARIO_PREVIEW, 0);
    IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_SET_LCE_SCENARIO, IspTuningMgr::E_LCE_SCENARIO_PREVIEW, 0);
    IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_NOTIFY_START, 0, 0);

    CAM_LOGD("[%s] -", __FUNCTION__);
    return MTRUE;
}

MBOOL
HalIspImp::
stop()
{
    CAM_LOGD("[%s] +", __FUNCTION__);

    IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_NOTIFY_STOP, 0, 0);
    // NormalIOPipe destroy instance
    if (m_pCamIO != NULL)
    {
        m_pCamIO->destroyInstance(LOG_TAG);
        m_pCamIO = NULL;
    }
#if 0
    // TuningMgr uninit
    if (m_pTuning)
    {
        m_pTuning->uninit(m_i4SensorDev);
        m_pTuning = NULL;
    }
#endif
    CAM_LOGD("[%s] -", __FUNCTION__);
    return MTRUE;
}

MBOOL
HalIspImp::
setP1Isp(const vector<MetaSet_T*>& requestQ, MBOOL const fgForce/*MINT32 const i4SensorDev, RequestSet_T const RequestSet, MetaSet_T& control, MBOOL const fgForce, MINT32 i4SubsampleIdex*/)
{
    MetaSet_T* it = requestQ[0];
    MINT32 i4FrmId = it->MagicNum;
    MINT32 i4FrmId4SMVR = 0;
    m_i4Magic = i4FrmId;

    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] + i4FrmId(%d) fgForce(%d)", __FUNCTION__, i4FrmId, fgForce);
    /*****************************
     *     Special flow - InitReq
     *     Warning : MW set one request only, unlike set three request to HAL3A
     *               So ISP_profile will wrong, this issue need to fix from MW
     *****************************/
    if(fgForce)
    {
        MetaSet_T* it = requestQ[0];
        RequestSet_T rRequestSet;
        rRequestSet.vNumberSet.clear();
        rRequestSet.vNumberSet.push_back(i4FrmId);
        NS3Av3::ParamIspProfile_T _3AProf(NSIspTuning::EIspProfile_Preview, i4FrmId, 1, MTRUE, NS3Av3::ParamIspProfile_T::EParamValidate_All, rRequestSet);
        _3AProf.i4MagicNum = i4FrmId;
        IspTuningMgr::getInstance().setIspProfile(m_i4SensorDev, NSIspTuning::EIspProfile_Preview);
        validateP1(_3AProf, MTRUE);
        return MTRUE;
    }
    /*****************************
     *     Parse ISP Param
     *****************************/

    RequestSet_T rRequestSet;
    rRequestSet.vNumberSet.clear();
    MUINT8 u1CapIntent = MTK_CONTROL_CAPTURE_INTENT_PREVIEW;
    MUINT8 u1ColorCorrectMode = MTK_COLOR_CORRECTION_MODE_FAST;
    const IMetadata& _appmeta = it->appMeta;
    const IMetadata& _halmeta = it->halMeta;
    MUINT8 u1IspProfile = 0xFF;
    MINT32 i4DisableP1=0;

    for (MINT32 i = 0; i < requestQ.size(); i++)
    {
        MetaSet_T* it = requestQ[i];
        i4FrmId = it->MagicNum;
        m_i4Magic = i4FrmId;
        u1CapIntent = MTK_CONTROL_CAPTURE_INTENT_PREVIEW;
        u1ColorCorrectMode = MTK_COLOR_CORRECTION_MODE_FAST;
        u1IspProfile = 0xFF;
        const IMetadata& _appmeta = it->appMeta;
        const IMetadata& _halmeta = it->halMeta;

        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] i4FrmId(%d)", __FUNCTION__, i4FrmId);
        rRequestSet.vNumberSet.push_back(std::max(0, i4FrmId));

        // Dual PDAF support for EngMode
        QUERY_ENTRY_SINGLE(_halmeta, MTK_HAL_REQUEST_PASS1_DISABLE, i4DisableP1);
        if (!QUERY_ENTRY_SINGLE(_halmeta, MTK_HAL_REQUEST_REQUIRE_EXIF, m_u1IsGetExif))
            m_u1IsGetExif = 0;

        for (MINT32 j = 0; j < _appmeta.count(); j++)
        {
            IMetadata::IEntry entry = _appmeta.entryAt(j);
            mtk_camera_metadata_tag_t tag = (mtk_camera_metadata_tag_t)entry.tag();
            // convert metadata tag into 3A settings.
            switch (tag)
            {
            case MTK_CONTROL_CAPTURE_INTENT:
                {
                    u1CapIntent = entry.itemAt(0, Type2Type< MUINT8 >());
                }
                break;
            // ISP
            case MTK_CONTROL_EFFECT_MODE:
                {
                    MUINT8 u1EffectMode = entry.itemAt(0, Type2Type< MUINT8 >());
                    IspTuningMgr::getInstance().setEffect(m_i4SensorDev,u1EffectMode);
                }
                break;
            case MTK_CONTROL_SCENE_MODE:
                {
                    MUINT32 u4SceneMode = entry.itemAt(0, Type2Type< MUINT8 >());
                    IspTuningMgr::getInstance().setSceneMode(m_i4SensorDev,u4SceneMode);
                }
                break;
            case MTK_EDGE_MODE:
                {
                    MUINT8 u1EdgeMode = entry.itemAt(0, Type2Type< MUINT8 >());
                    IspTuningMgr::getInstance().setEdgeMode(m_i4SensorDev,u1EdgeMode);
                }
                break;
            case MTK_NOISE_REDUCTION_MODE:
                {
                    MUINT8 u1NRMode = entry.itemAt(0, Type2Type< MUINT8 >());
                    IspTuningMgr::getInstance().setNoiseReductionMode(m_i4SensorDev,u1NRMode);
                }
                break;
            // Color correction
            case MTK_COLOR_CORRECTION_MODE:
                {
                    u1ColorCorrectMode = entry.itemAt(0, Type2Type<MUINT8>());
                    m_u1ColorCorrectMode = u1ColorCorrectMode;
                    IspTuningMgr::getInstance().setColorCorrectionMode(m_i4SensorDev,u1ColorCorrectMode);
                }
                break;
            case MTK_COLOR_CORRECTION_TRANSFORM:
                {
                    MFLOAT fColorCorrectMat[9];
                    for (MINT32 k = 0; k < 9; k++)
                    {
                        MRational rMat = entry.itemAt(k, Type2Type<MRational>());
                        fColorCorrectMat[k] = (0.0f != rMat.denominator) ? (MFLOAT)rMat.numerator / rMat.denominator : 0.0f;
                    }
                    IspTuningMgr::getInstance().setColorCorrectionTransform(m_i4SensorDev,
                    fColorCorrectMat[0], fColorCorrectMat[1], fColorCorrectMat[2],
                    fColorCorrectMat[3], fColorCorrectMat[4], fColorCorrectMat[5],
                    fColorCorrectMat[6], fColorCorrectMat[7], fColorCorrectMat[8]);
                }
                break;
            }
        }

    }


    // ISP profile
    if (u1IspProfile == 0xFF){
        switch (u1CapIntent)
        {
            case MTK_CONTROL_CAPTURE_INTENT_PREVIEW:
            case MTK_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG:
                m_eIspProfile = NSIspTuning::EIspProfile_Preview;
                break;
            case MTK_CONTROL_CAPTURE_INTENT_VIDEO_RECORD:
            case MTK_CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT:
                m_eIspProfile = NSIspTuning::EIspProfile_Video;
                break;
            default:
                m_eIspProfile = NSIspTuning::EIspProfile_Preview;
                break;
        }
    }
    else{

        NSIspTuning::EIspProfile_T eIspProfile = static_cast<NSIspTuning::EIspProfile_T>(u1IspProfile);
        CAM_LOGD_IF(m_eIspProfile != eIspProfile, "[%s] eIspProfile %d -> %d", __FUNCTION__, m_eIspProfile, eIspProfile);
        m_eIspProfile = eIspProfile;
    }
    IspTuningMgr::getInstance().setIspProfile(m_i4SensorDev,m_eIspProfile);

    //Need perframe call??
    IspTuningMgr::getInstance().notifyRPGEnable(m_i4SensorDev, MTRUE);

    IspTuningBufCtrl::getInstance(m_i4SensorDev)->clearP1Buffer();

    if(m_i4SubsampleCount>1)
    {
        AllResult_T *pAllResult = m_pResultPoolObj->getAllResult(i4FrmId);
        i4FrmId4SMVR = pAllResult->rOld3AInfo.i4ConvertMagic[0];
    }
    else
        i4FrmId4SMVR = i4FrmId;
    /*****************************
     *     Set AWBInfo to ISP
     *****************************/
    AWBResultInfo_T *m_pAWBResultInfo = (AWBResultInfo_T*)m_pResultPoolObj->getResult(i4FrmId4SMVR, E_AWB_RESULTINFO4ISP, __FUNCTION__);
    if(m_pAWBResultInfo)
        IspTuningMgr::getInstance().setAWBInfo2ISP(m_i4SensorDev, m_pAWBResultInfo->AWBInfo4ISP);
    /*****************************
     *     Set AEInfo to ISP
     *****************************/
    const AEResultInfo_T  *pAEResultInfo = (AEResultInfo_T*)m_pResultPoolObj->getResult(i4FrmId4SMVR, E_AE_RESULTINFO, __FUNCTION__);
    if(pAEResultInfo)
    {
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] u4DGNGain(%d)", __FUNCTION__, pAEResultInfo->AEPerframeInfo.rAEISPInfo.u4DGNGain);
        IspTuningMgr::getInstance().setAEInfo2ISP(m_i4SensorDev, pAEResultInfo->AEPerframeInfo.rAEISPInfo);
        IspTuningMgr::getInstance().setIspFlareGainOffset(m_i4SensorDev, pAEResultInfo->AEPerframeInfo.rAEISPInfo.i2FlareGain, pAEResultInfo->AEPerframeInfo.rAEISPInfo.i2FlareOffset);
        IspTuningMgr::getInstance().setISPAEGain(m_i4SensorDev, MFALSE, (pAEResultInfo->AEPerframeInfo.rAEISPInfo.u4DGNGain>>1));
    }else
        CAM_LOGD("[%s] pAEResultInfo(%p)", __FUNCTION__, pAEResultInfo);

    /*****************************
     *     ISP Validate
     *****************************/
    rRequestSet.fgDisableP1 = i4DisableP1;
    NS3Av3::ParamIspProfile_T _3AProf(m_eIspProfile, i4FrmId, 0, MTRUE, NS3Av3::ParamIspProfile_T::EParamValidate_All, rRequestSet);
    //Use ResultPool
    const HALResultToMeta_T* pHALResult = (HALResultToMeta_T*)m_pResultPoolObj->getResult(i4FrmId4SMVR, E_HAL_RESULTTOMETA, __FUNCTION__);
    if(pHALResult)
    {
        rRequestSet.fgKeep = pHALResult->fgKeep;
        m_bIsCapEnd = pHALResult->fgKeep;
    }
    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] rRequestSet.fgKeep(%d)", __FUNCTION__, rRequestSet.fgKeep);

    AAA_TRACE_D("P1_VLD");
    AAA_TRACE_ISP(P1_VLD);
    if (_3AProf.rRequestSet.vNumberSet[0] > 0)
    {
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] validate rRequestSet.vNumberSet[0](%d)", __FUNCTION__, _3AProf.rRequestSet.vNumberSet[0]);
        validateP1(_3AProf, MTRUE);
    }
    AAA_TRACE_END_ISP;
    AAA_TRACE_END_D;

    /*****************************
     *     Get ISP Result
     *****************************/
    std::vector<MINT32> rNumberSet = rRequestSet.vNumberSet;
    std::vector<MINT32>::iterator it4Magic;
    MINT32 i4MagicNum = 0;
    for (it4Magic = rNumberSet.begin(); it4Magic != rNumberSet.end(); it4Magic++)
    {
        i4MagicNum = (*it4Magic);
        getCurrResult(i4MagicNum);
    }

    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P1, "[%s] - i4FrmId(%d) i4FrmId4SMVR(%d)", __FUNCTION__, i4FrmId, i4FrmId4SMVR);
    return MTRUE;
}

MBOOL
HalIspImp::
setP2Isp(MINT32 flowType, const MetaSet_T& control, TuningParam* pTuningBuf, MetaSet_T* pResult)
{
    MINT32 i4P2En = 0, defaultValue = 1;
    const NSIspTuning::RAWIspCamInfo *pCaminfoBuf = NULL;
    MUINT32 u4readDump = 0;
    FileReadRule rule;

#if CAM3_DEFAULT_ISP
    defaultValue = 0;
#endif
    GET_PROP("vendor.debug.hal3av3.p2", defaultValue, i4P2En);

    if (i4P2En == 0 || pTuningBuf == NULL)
    {
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s] IT: flowType(%d), pTuningBuf(%p)", __FUNCTION__, flowType, pTuningBuf);
        return -1;
    }
    else
    {
        //MBOOL fgLog = mu4LogEn & HAL3AADAPTER3_LOG_PF;
        //MBOOL fgLogEn0 = (mu4LogEn & HAL3AADAPTER3_LOG_SET_0) ? MTRUE : MFALSE;
        //MBOOL fgLogEn1 = (mu4LogEn & HAL3AADAPTER3_LOG_SET_1) ? MTRUE : MFALSE;
        //MBOOL fgLogEn2 = (mu4LogEn & HAL3AADAPTER3_LOG_SET_2) ? MTRUE : MFALSE;

        MINT32 i4Ret = -1;

        AAA_TRACE_HAL(setIspPrepare);
        ResultP2_T rResultP2;
        P2Param_T rNewP2Param;
        NSIspTuning::ISP_INFO_T rIspInfo;
        IMetadata::Memory pCaminfoMeta;
        auto bCamInfoMeta = IMetadata::getEntry<IMetadata::Memory>(&control.halMeta, MTK_PROCESSOR_CAMINFO, pCaminfoMeta);
        if (bCamInfoMeta) {
            pCaminfoBuf = (const NSIspTuning::RAWIspCamInfo *)(pCaminfoMeta.array());
        }

        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_REQUIRE_EXIF,       rNewP2Param.u1Exif);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_DUMP_EXIF,          rNewP2Param.u1DumpExif);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_STEREO_FEATURE_DENOISE_MODE,    rNewP2Param.i4DenoiseMode);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_P1NODE_PROCESSOR_MAGICNUM,      rNewP2Param.i4MagicNum);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_PIPELINE_FRAME_NUMBER,          rNewP2Param.i4FrmNo);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_PIPELINE_REQUEST_NUMBER,        rNewP2Param.i4ReqNo);
        if (!control.halMeta.entryFor(MTK_3A_REPEAT_RESULT).isEmpty())
            QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_REPEAT_RESULT,           rNewP2Param.u1RepeatResult);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_ISP_P2_IN_IMG_FMT,              rNewP2Param.i4P2InImgFmt);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_ISP_P2_TUNING_UPDATE_MODE,      rNewP2Param.u1P2TuningUpdate);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_ISP_P2_IN_IMG_RES_REVISED,      rNewP2Param.ResizeYUV);

        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s] u1RepeatResult(%d): i4MagicNum(%d)", __FUNCTION__, rNewP2Param.u1RepeatResult, rNewP2Param.i4MagicNum);

        QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_PGN_ENABLE,                  rNewP2Param.u1PGN);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_ISP_PROFILE,                 rNewP2Param.u1IspProfile);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_P1NODE_RAW_TYPE,                rNewP2Param.i4RawType);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_BRIGHTNESS_MODE,    rNewP2Param.i4BrightnessMode);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_CONTRAST_MODE,      rNewP2Param.i4ContrastMode);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_HUE_MODE,           rNewP2Param.i4HueMode);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_SATURATION_MODE,    rNewP2Param.i4SaturationMode);
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_HAL_REQUEST_EDGE_MODE,          rNewP2Param.i4halEdgeMode);

        QUERY_ENTRY_SINGLE(control.appMeta, MTK_CONTROL_CAPTURE_INTENT,         rNewP2Param.u1CapIntent);
        QUERY_ENTRY_SINGLE(control.appMeta, MTK_TONEMAP_MODE,                   rNewP2Param.u1TonemapMode);
        QUERY_ENTRY_SINGLE(control.appMeta, MTK_EDGE_MODE,                      rNewP2Param.u1appEdgeMode);
        QUERY_ENTRY_SINGLE(control.appMeta, MTK_NOISE_REDUCTION_MODE,           rNewP2Param.u1NrMode);
        QUERY_ENTRY_SINGLE(control.appMeta, MTK_SENSOR_SENSITIVITY,             rNewP2Param.i4ISO);

        MBOOL HasExif = QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_EXIF_METADATA, rNewP2Param.rexifMeta);

        NSCam::IMetadata::Memory rpdbgIsp;
        if (HasExif) {
            QUERY_ENTRY_SINGLE<NSCam::IMetadata::Memory>(rNewP2Param.rexifMeta, MTK_3A_EXIF_DBGINFO_ISP_DATA, rpdbgIsp);
            rNewP2Param.rpdbgIsp = &rpdbgIsp;
        }

//------------------------------------Extract Pointer to Metadata---------------------------------------------

        #define FETCH_ENTRY_SINGLE(VAR, TYPE, TAG) \
            TYPE VAR;\
            QUERY_ENTRY_SINGLE<TYPE>(control.halMeta, TAG, VAR); \
            rNewP2Param.VAR = &VAR;

        FETCH_ENTRY_SINGLE(rpSclCropRect,   MRect,              MTK_3A_PRV_CROP_REGION);
        //FETCH_ENTRY_SINGLE(rpP1Crop,        MRect,             MTK_P1NODE_SCALAR_CROP_REGION);  //TG Domain
        FETCH_ENTRY_SINGLE(rpP1Crop,        MRect,              MTK_P1NODE_BIN_CROP_REGION);  //After FBin Domain
        FETCH_ENTRY_SINGLE(rpP2Crop,        MRect,              MTK_ISP_P2_CROP_REGION);
        FETCH_ENTRY_SINGLE(rpRzSize,        MSize,              MTK_P1NODE_RESIZER_SIZE);
        FETCH_ENTRY_SINGLE(rpP2OriginSize,  MSize,              MTK_ISP_P2_ORIGINAL_SIZE);
        FETCH_ENTRY_SINGLE(rpP2RzSize,      MSize,              MTK_ISP_P2_RESIZER_SIZE);
        FETCH_ENTRY_SINGLE(rpLscData,       IMetadata::Memory,  MTK_LSC_TBL_DATA);
        FETCH_ENTRY_SINGLE(rpRzInSize,      MSize,              MTK_P1NODE_BIN_SIZE);  //After FBin Domain

        #undef FETCH_ENTRY_SINGLE

#if 0 //Pass2 not use
        if (rNewP2Param.rpSclCropRect)
        {
            mPrvCropRegion.p.x = rNewP2Param.rpSclCropRect->p.x;
            mPrvCropRegion.p.y = rNewP2Param.rpSclCropRect->p.y;
            mPrvCropRegion.s.w = rNewP2Param.rpSclCropRect->s.w;
            mPrvCropRegion.s.h = rNewP2Param.rpSclCropRect->s.h;

            // crop info for AE
            rNewP2Param.rScaleCropRect.i4Xoffset = rNewP2Param.rpSclCropRect->p.x;
            rNewP2Param.rScaleCropRect.i4Yoffset = rNewP2Param.rpSclCropRect->p.y;
            rNewP2Param.rScaleCropRect.i4Xwidth  = rNewP2Param.rpSclCropRect->s.w;
            rNewP2Param.rScaleCropRect.i4Yheight = rNewP2Param.rpSclCropRect->s.h;

            // crop info for AF
            CameraArea_T& rArea = mAfParams.rScaleCropArea;
            MINT32 i4TgWidth = 0;
            MINT32 i4TgHeight = 0;
            mpHal3aObj->queryTgSize(i4TgWidth,i4TgHeight);

            rArea.i4Left   = mPrvCropRegion.p.x;
            rArea.i4Top    = mPrvCropRegion.p.y;
            rArea.i4Right  = mPrvCropRegion.p.x + mPrvCropRegion.s.w;
            rArea.i4Bottom = mPrvCropRegion.p.y + mPrvCropRegion.s.h;
            rArea = _transformArea(mi4SensorIdx, mi4SensorMode, rArea);
            rArea = _clipArea(i4TgWidth, i4TgHeight, rArea);

            CAM_LOGD_IF(1, "[%s] AE SCL CROP(%d,%d,%d,%d) AF SCL CROP(%d,%d,%d,%d)",
                    __FUNCTION__, rNewP2Param.rScaleCropRect.i4Xoffset, rNewP2Param.rScaleCropRect.i4Yoffset, rNewP2Param.rScaleCropRect.i4Xwidth, rNewP2Param.rScaleCropRect.i4Yheight,
                    rArea.i4Left, rArea.i4Top, rArea.i4Right, rArea.i4Bottom);
        }
#endif
        IMetadata::IEntry entryRed = control.appMeta.entryFor(MTK_TONEMAP_CURVE_RED);
        if (entryRed.tag() != IMetadata::IEntry::BAD_TAG)
        {
            rNewP2Param.u4TonemapCurveRedSize = entryRed.count();
            rNewP2Param.pTonemapCurveRed = (const MFLOAT*)entryRed.data();
            //rNewP2Param.pTonemapCurveRed = pTonemapRed;
        }

        IMetadata::IEntry entryGreen = control.appMeta.entryFor(MTK_TONEMAP_CURVE_GREEN);
        if (entryGreen.tag() != IMetadata::IEntry::BAD_TAG)
        {
            rNewP2Param.u4TonemapCurveGreenSize = entryGreen.count();
            rNewP2Param.pTonemapCurveGreen = (const MFLOAT*)entryGreen.data();
            //rNewP2Param.pTonemapCurveGreen = pTonemapGreen;
        }

        IMetadata::IEntry entryBlue = control.appMeta.entryFor(MTK_TONEMAP_CURVE_BLUE);
        if (entryBlue.tag() != IMetadata::IEntry::BAD_TAG)
        {
            rNewP2Param.u4TonemapCurveBlueSize = entryBlue.count();
            rNewP2Param.pTonemapCurveBlue = (const MFLOAT*)entryBlue.data();
            //rNewP2Param.pTonemapCurveBlue = pTonemapBlue;
        }
        AAA_TRACE_END_HAL;

        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2,"[%s] FrameId(%d), HueMode(%d),BrightnessMode(%d), ContrastMode(%d), SaturationMode(%d), i4EdgeMode(%d)"
                , __FUNCTION__, rNewP2Param.i4MagicNum, rNewP2Param.i4HueMode, rNewP2Param.i4BrightnessMode, rNewP2Param.i4ContrastMode, rNewP2Param.i4SaturationMode, rNewP2Param.i4halEdgeMode);

        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s] MTK_TONEMAP_MODE(%d), MagicNum(%d) Rsize(%d) Gsize(%d) Bsize(%d)",
                    __FUNCTION__, rNewP2Param.u1TonemapMode, rNewP2Param.i4MagicNum,
                    rNewP2Param.u4TonemapCurveRedSize, rNewP2Param.u4TonemapCurveGreenSize, rNewP2Param.u4TonemapCurveBlueSize);

//=========================================================================================================
//                                                                              Metadata extraction complete
//=========================================================================================================
        rIspInfo.isCapture = 0
                    || rNewP2Param.u1CapIntent == MTK_CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT
                    || rNewP2Param.u1CapIntent == MTK_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG
                    || rNewP2Param.u1CapIntent == MTK_CONTROL_CAPTURE_INTENT_STILL_CAPTURE
                    ;
        extract(&rIspInfo.hint, &control.halMeta);
        rIspInfo.hint.SensorDev = m_i4SensorDev;

        m_pResultPoolObj->lockLastInfo();
        LastInfo_T vLastInfo = m_pResultPoolObj->getLastInfo();
        // Restore caminfo
        if (pCaminfoBuf == NULL && vLastInfo.mBackupCamInfo_copied)
        {
            vLastInfo.mBackupCamInfo.rCropRzInfo.i4TGoutW = m_i4TgWidth;
            vLastInfo.mBackupCamInfo.rCropRzInfo.i4TGoutH = m_i4TgHeight;
            vLastInfo.mBackupCamInfo.rMapping_Info.eSensorMode = static_cast<ESensorMode_T>(m_i4SensorMode);
            vLastInfo.mBackupCamInfo.rMapping_Info.eIspProfile = static_cast<NSIspTuning::EIspProfile_T>(rNewP2Param.u1IspProfile);

            pCaminfoBuf = &vLastInfo.mBackupCamInfo;
            CAM_LOGD("[%s] Restore caminfo,copied(%d)/mode(%d)/profile(%d)/FrmId(%d)/TG(%d,%d)",__FUNCTION__, vLastInfo.mBackupCamInfo_copied,
                    vLastInfo.mBackupCamInfo.rMapping_Info.eSensorMode,
                    vLastInfo.mBackupCamInfo.rMapping_Info.eIspProfile,
                     rNewP2Param.i4MagicNum, m_i4TgWidth, m_i4TgHeight);
        }
        m_pResultPoolObj->unlockLastInfo();

        if (pCaminfoBuf)
        {
            AAA_TRACE_HAL(CopyCaminfo);
            ::memcpy(&rIspInfo.rCamInfo, pCaminfoBuf, sizeof(NSIspTuning::RAWIspCamInfo));

            QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_ISP_DISABLE_NR, rNewP2Param.bBypassNR);

            if(!GET_ENTRY_ARRAY(control.halMeta, MTK_3A_ISP_NR3D_SW_PARAMS, rNewP2Param.NR3D_Data, 14))
                CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s]+ NR3D params get fail",__FUNCTION__);

            //Feature Control Overwrite
            QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_ISP_BYPASS_LCE, rNewP2Param.bBypassLCE);

            AAA_TRACE_END_HAL;

            setISPInfo(rNewP2Param, rIspInfo, 0);

            if(!rIspInfo.rCamInfo.bBypassLCE){

                if (pTuningBuf->pLcsBuf == NULL)
                {
                     CAM_LOGE("[%s] [-No Lcso Buffer ]", __FUNCTION__);
                     rIspInfo.rCamInfo.bBypassLCE = MTRUE;
                }
                else if (!(rIspInfo.rCamInfo.rLCS_Info.rInSetting.fgOnOff))
                {
                     CAM_LOGE("[%s] [- LCS turn off ]", __FUNCTION__);
                     rIspInfo.rCamInfo.bBypassLCE = MTRUE;
                }
            }

            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s] bBypassLCE %d",__FUNCTION__, rIspInfo.rCamInfo.bBypassLCE);

            if (rule.isREADEnable("ISPHAL"))
                _readDump(pTuningBuf, control, pResult, &rIspInfo, (MINT32)E_Lsc_Output);

            MUINT32 u4ManualMode = 0;
#if HAL3A_TEST_OVERRIDE
            GET_PROP("vendor.debug.hal3av3.testp2", 0, u4ManualMode);
            _test_p2(u4ManualMode, mParams, rNewP2Param);
#endif

            setP2Params(rNewP2Param, &rResultP2);

            // set ISP Profile to file naming hint
            rIspInfo.hint.IspProfile = rIspInfo.rCamInfo.rMapping_Info.eIspProfile;

            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s]+ sensorDev(%d), key(%09d), #(%d), flow(%d), ispProfile(%d), rpg(%d), pTuningBuf(%p)",
                __FUNCTION__, m_i4SensorDev, rIspInfo.i4UniqueKey, rIspInfo.rCamInfo.u4Id, flowType, rIspInfo.rCamInfo.rMapping_Info.eIspProfile, rIspInfo.rCamInfo.fgRPGEnable, pTuningBuf);

            if (rNewP2Param.u1Exif)
            {
                if(/*rIspInfo.rCamInfo.rMapping_Info.eIspProfile != EIspProfile_Capture_MultiPass_HWNR && */rNewP2Param.rpdbgIsp)
                {
                    // after got p1 debug info
                    rResultP2.vecDbgIspP2.appendArray(rNewP2Param.rpdbgIsp->array(), rNewP2Param.rpdbgIsp->size());
                }


                // generate P2 tuning and get result including P2 debug info
                generateP2(flowType, rIspInfo, pTuningBuf, &rResultP2);

                if (pResult)
                {
                    // after got p1 debug info, append p2 debug info, and then put to result
                    if (rResultP2.vecDbgIspP2.size())
                    {
                        UPDATE_ENTRY_SINGLE<MINT32>(rNewP2Param.rexifMeta, MTK_3A_EXIF_DBGINFO_ISP_KEY, ISP_DEBUG_KEYID);
                        IMetadata::Memory dbgIsp;
                        dbgIsp.appendVector(rResultP2.vecDbgIspP2);
                        UPDATE_ENTRY_SINGLE(rNewP2Param.rexifMeta, MTK_3A_EXIF_DBGINFO_ISP_DATA, dbgIsp);
                    }

                    // multi-pass NR debug info
                    if (rResultP2.vecDbgIspP2_MultiP.size())
                    {
                        QUERY_ENTRY_SINGLE(pResult->halMeta, MTK_3A_EXIF_METADATA, rNewP2Param.rexifMeta);
                        UPDATE_ENTRY_SINGLE<MINT32>(rNewP2Param.rexifMeta, MTK_POSTNR_EXIF_DBGINFO_NR_KEY, DEBUG_EXIF_MID_CAM_RESERVE1);
                        IMetadata::Memory dbgIsp;
                        dbgIsp.appendVector(rResultP2.vecDbgIspP2_MultiP);
                        UPDATE_ENTRY_SINGLE(rNewP2Param.rexifMeta, MTK_POSTNR_EXIF_DBGINFO_NR_DATA, dbgIsp);
                    }

                    UPDATE_ENTRY_SINGLE(pResult->halMeta, MTK_3A_EXIF_METADATA, rNewP2Param.rexifMeta);

                    MINT32 CaptureDump = property_get_int32("vendor.debug.camera.dump.p2.debuginfo", 0);
                    MINT32 PreviewDump = property_get_int32("vendor.debug.camera.dump.isp.preview", 0);
                    if ((CaptureDump && rIspInfo.isCapture) || PreviewDump || rNewP2Param.u1DumpExif)
                    {
                        char filename[512];
                        genFileName_TUNING(filename, sizeof(filename), &rIspInfo.hint);
                        _dumpDebugInfo(filename, rNewP2Param.rexifMeta);
                    }

                    MINT32 dumpAAO        = property_get_int32("vendor.debug.camera.AAO.dump", 0);
                    MINT32 dumpAAOPreview = property_get_int32("vendor.debug.camera.AAO.dump.preview", 0);
                    if ((dumpAAO && rIspInfo.isCapture) || dumpAAOPreview)
                    {
                        char filename[512];
                        char temp[512];
                        genFileName_HW_AAO(filename, sizeof(filename), &rIspInfo.hint);

                        sprintf(temp, "/sdcard/camera_dump/captue_end_aao_%d.hw_aao", rNewP2Param.i4MagicNum);
                        if(_isFileExist(temp))
                        {
                            int result;
                            result= rename(temp, filename);
                            if ( result == 0 )
                                CAM_LOGD("Renamed success: %s", filename);
                            else
                                CAM_LOGD("Renamed fail: %s", temp);
                        }
                        else
                            _dumpAAO(filename, m_i4SensorDev, rNewP2Param.i4MagicNum);
                    }
                }
            }
            else
            {
                // generate P2 tuning only
                generateP2(flowType, rIspInfo, pTuningBuf, NULL);
            }

#if 0 // HAL3 ReprocessCaptureTest#testReprocessRequestKeys failed
            if (pResult)
            {
                UPDATE_ENTRY_SINGLE(pResult->appMeta, MTK_EDGE_MODE, static_cast<MUINT8>(rIspInfo.rCamInfo.eEdgeMode));
                UPDATE_ENTRY_SINGLE(pResult->appMeta, MTK_NOISE_REDUCTION_MODE, static_cast<MUINT8>(rIspInfo.rCamInfo.eNRMode));
            }
#endif
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SET_P2, "[%s]- OK(%p, %p)", __FUNCTION__, pTuningBuf->pRegBuf, pTuningBuf->pLsc2Buf);
            i4Ret = 0;
        }
        else if(rNewP2Param.u1IspProfile == EIspProfile_YUV_Reprocess){
            rIspInfo.rCamInfo.bBypassLCE = MTRUE;

            setISPInfo(rNewP2Param, rIspInfo, 1);

            if (pResult){
                UPDATE_ENTRY_SINGLE(pResult->appMeta, MTK_EDGE_MODE, static_cast<MUINT8>(rIspInfo.rCamInfo.eEdgeMode));
                UPDATE_ENTRY_SINGLE(pResult->appMeta, MTK_NOISE_REDUCTION_MODE, static_cast<MUINT8>(rIspInfo.rCamInfo.eNRMode));
                UPDATE_ENTRY_SINGLE(pResult->halMeta, MTK_3A_EXIF_METADATA, getReprocStdExif(control));
            }

            generateP2(flowType, rIspInfo, pTuningBuf, NULL);

            i4Ret = 0;
        }
        else
        {
            CAM_LOGE("[%s] NG (no caminfo)", __FUNCTION__);
            i4Ret = -1;
        }

        AAA_TRACE_HAL(getP2Result);
        getP2Result(rNewP2Param, &rResultP2);
        AAA_TRACE_END_HAL;

        AAA_TRACE_HAL(Convert2Meta);
        convertP2ResultToMeta(rResultP2, pResult);
        AAA_TRACE_END_HAL;

        AAA_TRACE_HAL(ReadDump);
        if (rule.isREADEnable("ISPHAL"))
            _readDump(pTuningBuf, control, pResult, &rIspInfo, (MINT32)E_Tuning_Output);
        if (rule.isREADEnable("ISPHAL"))
            _readDump(pTuningBuf, control, pResult, &rIspInfo, (MINT32)E_MFB_Output);
        AAA_TRACE_END_HAL;

        return i4Ret;
    }

    return MTRUE;
}

MINT32
HalIspImp::
sendIspCtrl(EISPCtrl_T eISPCtrl, MINTPTR iArg1, MINTPTR iArg2)
{
    MINT32 i4Ret = 0;
    switch (eISPCtrl)
    {
        // ----------------------------------ISP----------------------------------
        case EISPCtrl_GetIspGamma:
            IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_GET_ISP_GAMMA, iArg1, iArg2);
            break;
        case EISPCtrl_ValidatePass1:
            {
                MINT32 i4Magic = (MINT32)iArg1;
                NSIspTuning::EIspProfile_T prof = static_cast<NSIspTuning::EIspProfile_T>(iArg2);
                RequestSet_T rRequestSet;
                rRequestSet.vNumberSet.clear();
                rRequestSet.vNumberSet.push_back(i4Magic);
                NS3Av3::ParamIspProfile_T _3AProf(prof, i4Magic, 1, MTRUE, NS3Av3::ParamIspProfile_T::EParamValidate_All, rRequestSet);
                _3AProf.i4MagicNum = i4Magic;
                IspTuningMgr::getInstance().setIspProfile(m_i4SensorDev, prof);
                validateP1(_3AProf, MTRUE);
            }
            break;
        case EISPCtrl_SetIspProfile:
            IspTuningMgr::getInstance().setIspProfile(m_i4SensorDev, static_cast<NSIspTuning::EIspProfile_T>(iArg1));
            m_eIspProfile = static_cast<NSIspTuning::EIspProfile_T>(iArg1);
            break;
        case EISPCtrl_GetOBOffset:
            {
                const ISPResultToMeta_T *pISPResult = (const ISPResultToMeta_T*)m_pResultPoolObj->getResult(m_i4Magic, E_ISP_RESULTTOMETA, __FUNCTION__);
                CAM_LOGD("[%s], GetOBOffset ISPResult(%p) at MagicNum(%d)", __FUNCTION__, pISPResult, m_i4Magic);
                if (!pISPResult)
                {
                    MINT32 rHistoryReqMagic[HistorySize] = {0,0,0};
                    m_pResultPoolObj->getHistory(rHistoryReqMagic);
                    pISPResult = (const ISPResultToMeta_T*)m_pResultPoolObj->getResult(rHistoryReqMagic[1], E_ISP_RESULTTOMETA, __FUNCTION__);
                    CAM_LOGD("[%s], Get ISPResult(%p) at MagicNum(%d) Instead", __FUNCTION__, pISPResult, rHistoryReqMagic[1]);
                }
                MINT32 *OBOffset = reinterpret_cast<MINT32*>(iArg1);

                OBOffset[0] = pISPResult->rCamInfo.rOBC1.offset_r.val;
                OBOffset[1] = pISPResult->rCamInfo.rOBC1.offset_gr.val;
                OBOffset[2] = pISPResult->rCamInfo.rOBC1.offset_gb.val;
                OBOffset[3] = pISPResult->rCamInfo.rOBC1.offset_b.val;
            }
            break;
        case EISPCtrl_GetRwbInfo:
            IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_GET_RWB_INFO, iArg1, iArg2);
            break;
        case EISPCtrl_SetOperMode:
            MINT32 i4OperMode;
            i4OperMode = IspTuningMgr::getInstance().getOperMode(m_i4SensorDev);
            if(i4OperMode != EOperMode_Meta)
                i4Ret = IspTuningMgr::getInstance().setOperMode(m_i4SensorDev, iArg1);
            CAM_LOGD("[%s] prev_mode(%d), new_mode(%ld)", __FUNCTION__, i4OperMode, (long)iArg1);
            break;
        case EISPCtrl_GetOperMode:
            *(reinterpret_cast<MUINT32*>(iArg1)) = IspTuningMgr::getInstance().getOperMode(m_i4SensorDev);
            break;
        case EISPCtrl_GetMfbSize:
            IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_GET_MFB_SIZE, iArg1, iArg2);
            break;
        case EISPCtrl_SetLcsoParam:
            getCurrLCSResult(*(ISP_LCS_OUT_INFO_T*)iArg1);
            break;
        default:
            CAM_LOGD("[%s] Unsupport Command(%d)", __FUNCTION__, eISPCtrl);
            return MFALSE;
    }
    return MTRUE;
}

MINT32
HalIspImp::
attachCb(/*IHal3ACb::ECb_T eId, IHal3ACb* pCb*/)
{
    return MTRUE;
}

MINT32
HalIspImp::
detachCb(/*IHal3ACb::ECb_T eId, IHal3ACb* pCb*/)
{
    return MTRUE;
}

MVOID
HalIspImp::
setFDEnable(MBOOL fgEnable)
{
    CAM_LOGD("[%s] fgEnable(%d)", __FUNCTION__, fgEnable);
    m_bFaceDetectEnable = fgEnable;
    IspTuningMgr::getInstance().setFDEnable(m_i4SensorDev, fgEnable);
    if (!m_bFaceDetectEnable)
        m_i4faceNum = 0;
}

MBOOL
HalIspImp::
setFDInfo(MVOID* prFaces)
{
    //TODO
    setFDInfo(prFaces, prFaces);
    return MTRUE;
}

MBOOL
HalIspImp::
setFDInfo(MVOID* prFaces, MVOID* prAFFaces)
{
    CAM_LOGD("[%s] m_bFaceDetectEnable(%d)", __FUNCTION__, m_bFaceDetectEnable);
    if (m_bFaceDetectEnable)
    {
        MtkCameraFaceMetadata *pFaces = (MtkCameraFaceMetadata *)prAFFaces;
        m_i4faceNum = pFaces->number_of_faces;

        IspTuningMgr::getInstance().setFDInfo(m_i4SensorDev, prAFFaces, (m_i4TgWidth * m_i4TgHeight));
    }
    return MTRUE;
}

MBOOL
HalIspImp::
setOTInfo(MVOID* prOT)
{
    return MTRUE;
}

MINT32
HalIspImp::
updateTGInfo()
{
    //Before wait for VSirq of IspDrv, we need to query IHalsensor for the current TG info
    IHalSensorList*const pHalSensorList = MAKE_HalSensorList();
    if (!pHalSensorList)
    {
        CAM_LOGE("MAKE_HalSensorList() == NULL");
        return MFALSE;
    }
    const char* const callerName = "HalISPQueryTG";
    IHalSensor* pHalSensor = pHalSensorList->createSensor(callerName, m_i4SensorIdx);
    //Note that Middleware has configured sensor before
    SensorDynamicInfo senInfo;
    MINT32 i4SensorDevId = pHalSensor->querySensorDynamicInfo(m_i4SensorDev, &senInfo);
    pHalSensor->destroyInstance(callerName);

    CAM_LOGD("m_i4SensorDev = %d, senInfo.TgInfo = %d\n", m_i4SensorDev, senInfo.TgInfo);

    if ((senInfo.TgInfo != CAM_TG_1) && (senInfo.TgInfo != CAM_TG_2))
    {
        CAM_LOGE("RAW sensor is connected with TgInfo: %d\n", senInfo.TgInfo);
        return MFALSE;
    }

    MINT32 i4TgInfo = senInfo.TgInfo; //now, TG info is obtained! TG1 or TG2

    IspTuningMgr::getInstance().setTGInfo(m_i4SensorDev, i4TgInfo);

    m_pCamIO->sendCommand( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_TG_OUT_SIZE, (MINTPTR)&m_i4TgWidth, (MINTPTR)&m_i4TgHeight, 0);
    CAM_LOGD("[%s] TG size(%d,%d)", __FUNCTION__, m_i4TgWidth, m_i4TgHeight);

    return MTRUE;
}

MVOID
HalIspImp::
querySensorStaticInfo()
{
    //Before phone boot up (before opening camera), we can query IHalsensor for the sensor static info (EX: MONO or Bayer)
    SensorStaticInfo sensorStaticInfo;
    IHalSensorList*const pHalSensorList = MAKE_HalSensorList();
    if (!pHalSensorList)
    {
        CAM_LOGE("MAKE_HalSensorList() == NULL");
        return;
    }
    pHalSensorList->querySensorStaticInfo(m_i4SensorDev,&sensorStaticInfo);

    MUINT32 u4RawFmtType = sensorStaticInfo.rawFmtType; // SENSOR_RAW_MONO or SENSOR_RAW_Bayer

    // 3A/ISP mgr can query sensor static information here
    IspTuningMgr::getInstance().sendIspTuningIOCtrl(m_i4SensorDev, IspTuningMgr::E_ISPTUNING_NOTIFY_SENSOR_TYPE, u4RawFmtType, 0);
}

MBOOL
HalIspImp::
validateP1(const ParamIspProfile_T& rParamIspProfile, MBOOL fgPerframe)
{
    m_pTuning->validatePerFrameP1(m_i4SensorDev, rParamIspProfile.rRequestSet, fgPerframe);


    //LCS callback
    ISP_LCS_IN_INFO_T lcs_in_info;
    m_pTuning->getLCSparam(m_i4SensorDev, lcs_in_info);

    if(m_pLcsDrv){
        m_pLcsDrv->updateLCSList_In(lcs_in_info);
    }
    else{
        CAM_LOGE("No LcsDrv");
    }
    return MTRUE;
}

MBOOL
HalIspImp::
setISPInfo(P2Param_T const &rNewP2Param, NSIspTuning::ISP_INFO_T &rIspInfo, MINT32 type)
{
    AAA_TRACE_HAL(setISPInfo);
    // type == 0 would do the all set
    if (type < 1) {
        rIspInfo.rCamInfo.bBypassLCE                        = rNewP2Param.bBypassLCE;
        rIspInfo.rCamInfo.i4P2InImgFmt                      = rNewP2Param.i4P2InImgFmt;
        rIspInfo.rCamInfo.u1P2TuningUpdate                  = rNewP2Param.u1P2TuningUpdate;
        rIspInfo.rCamInfo.NR3D_Data.GMVX                    = rNewP2Param.NR3D_Data[0];
        rIspInfo.rCamInfo.NR3D_Data.GMVY                    = rNewP2Param.NR3D_Data[1];
        rIspInfo.rCamInfo.NR3D_Data.confX                   = rNewP2Param.NR3D_Data[2];
        rIspInfo.rCamInfo.NR3D_Data.confY                   = rNewP2Param.NR3D_Data[3];
        rIspInfo.rCamInfo.NR3D_Data.MAX_GMV                 = rNewP2Param.NR3D_Data[4];
        rIspInfo.rCamInfo.NR3D_Data.frameReset              = rNewP2Param.NR3D_Data[5];
        rIspInfo.rCamInfo.NR3D_Data.GMV_Status              = rNewP2Param.NR3D_Data[6];
        rIspInfo.rCamInfo.NR3D_Data.ISO_cutoff              = rNewP2Param.NR3D_Data[7];
        rIspInfo.rCamInfo.NR3D_Data.isGyroValid             = rNewP2Param.NR3D_Data[8];
        rIspInfo.rCamInfo.NR3D_Data.gyroXAccelX1000         = rNewP2Param.NR3D_Data[9];
        rIspInfo.rCamInfo.NR3D_Data.gyroYAccelX1000         = rNewP2Param.NR3D_Data[10];
        rIspInfo.rCamInfo.NR3D_Data.gyroZAccelX1000         = rNewP2Param.NR3D_Data[11];
        rIspInfo.rCamInfo.NR3D_Data.gyroTimeStampHigh       = rNewP2Param.NR3D_Data[12];
        rIspInfo.rCamInfo.NR3D_Data.gyroTimeStampLow        = rNewP2Param.NR3D_Data[13];

        rIspInfo.rCamInfo.bBypassNR                         = rNewP2Param.bBypassNR;

        rIspInfo.rCamInfo.rIspUsrSelectLevel.eIdx_Edge      = static_cast<EIndex_Isp_Edge_T>(rNewP2Param.i4halEdgeMode);
        rIspInfo.rCamInfo.rIspUsrSelectLevel.eIdx_Bright    = static_cast<EIndex_Isp_Brightness_T>(rNewP2Param.i4BrightnessMode);
        rIspInfo.rCamInfo.rIspUsrSelectLevel.eIdx_Contrast  = static_cast<EIndex_Isp_Contrast_T>(rNewP2Param.i4ContrastMode);
        rIspInfo.rCamInfo.rIspUsrSelectLevel.eIdx_Hue       = static_cast<EIndex_Isp_Hue_T>(rNewP2Param.i4HueMode);
        rIspInfo.rCamInfo.rIspUsrSelectLevel.eIdx_Sat       = static_cast<EIndex_Isp_Saturation_T>(rNewP2Param.i4SaturationMode);
        rIspInfo.rCamInfo.eEdgeMode                         = static_cast<mtk_camera_metadata_enum_android_edge_mode_t>(rNewP2Param.u1appEdgeMode);
        rIspInfo.rCamInfo.eToneMapMode                      = static_cast<mtk_camera_metadata_enum_android_tonemap_mode_t>(rNewP2Param.u1TonemapMode);
    }

    if (type < 2) {
        //     _reprocess  part

        rIspInfo.rCamInfo.fgRPGEnable = !rNewP2Param.u1PGN;

        if (rNewP2Param.u1IspProfile != 255)
        {
            rIspInfo.rCamInfo.rMapping_Info.eIspProfile = static_cast<NSIspTuning::EIspProfile_T>(rNewP2Param.u1IspProfile);
        }
        else
        {
            switch (rNewP2Param.u1CapIntent)
            {
            case MTK_CONTROL_CAPTURE_INTENT_VIDEO_RECORD:
            case MTK_CONTROL_CAPTURE_INTENT_VIDEO_SNAPSHOT:
                rIspInfo.rCamInfo.rMapping_Info.eIspProfile = NSIspTuning::EIspProfile_Video;
                break;
            case MTK_CONTROL_CAPTURE_INTENT_PREVIEW:
            case MTK_CONTROL_CAPTURE_INTENT_ZERO_SHUTTER_LAG:
                rIspInfo.rCamInfo.rMapping_Info.eIspProfile = NSIspTuning::EIspProfile_Preview;
                break;
            case MTK_CONTROL_CAPTURE_INTENT_STILL_CAPTURE:
                if(rIspInfo.rCamInfo.fgRPGEnable){
                    rIspInfo.rCamInfo.rMapping_Info.eIspProfile = NSIspTuning::EIspProfile_Preview;
                }else{
                    rIspInfo.rCamInfo.rMapping_Info.eIspProfile = NSIspTuning::EIspProfile_Capture;
                }
                break;
            }
        }

        rIspInfo.rCamInfo.i4RawType = rNewP2Param.i4RawType;

        if (((!rIspInfo.rCamInfo.fgRPGEnable) && (rNewP2Param.i4RawType == NSIspTuning::ERawType_Pure)) || m_i4CopyLscP1En == 1)
        {
            if (rNewP2Param.rpLscData)
                rIspInfo.rLscData = std::vector<MUINT8>((rNewP2Param.rpLscData)->array(), (rNewP2Param.rpLscData)->array()+(rNewP2Param.rpLscData)->size());
            else
                CAM_LOGD("[%s] No shading entry in metadata\n", __FUNCTION__);
        }

        if(rIspInfo.rLscData.size())
        {
            MUINT32 mu4DumpLscP1En=0;
            MUINT32 mu4DumpLscP1CapEn=0;
            GET_PROP("vendor.debug.camera.dump.p1.lsc", 0, mu4DumpLscP1En);
            GET_PROP("vendor.debug.camera.dump.cap.lsc", 0, mu4DumpLscP1CapEn);
            if(mu4DumpLscP1En || mu4DumpLscP1CapEn && rIspInfo.isCapture)
            {
                char filename[256];
                rIspInfo.hint.IspProfile = rIspInfo.rCamInfo.rMapping_Info.eIspProfile;
                genFileName_LSC(filename, sizeof(filename), &rIspInfo.hint);
                android::sp<IFileCache> fidLscDump;
                fidLscDump = IFileCache::open(filename);
                if (fidLscDump->write(&rIspInfo.rLscData[0], rIspInfo.rLscData.size()) != rIspInfo.rLscData.size())
                {
                    CAM_LOGD("[%s] write error %s", __FUNCTION__, filename);
                }
            }
        }

        rIspInfo.i4UniqueKey = rNewP2Param.i4UniqueKey;

        if (rNewP2Param.rpP1Crop && rNewP2Param.rpRzSize && rNewP2Param.rpRzInSize &&
            (rIspInfo.rCamInfo.fgRPGEnable))  //||
             //rIspInfo.rCamInfo.eIspProfile == NSIspTuning::EIspProfile_N3D_Denoise  ||
             //rIspInfo.rCamInfo.eIspProfile == NSIspTuning::EIspProfile_N3D_Denoise_toGGM))
        {
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZofstX    = rNewP2Param.rpP1Crop->p.x;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZofstY    = rNewP2Param.rpP1Crop->p.y;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZcropW    = rNewP2Param.rpP1Crop->s.w;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZcropH    = rNewP2Param.rpP1Crop->s.h;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutW     = rNewP2Param.rpRzSize->w;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutH     = rNewP2Param.rpRzSize->h;
            rIspInfo.rCamInfo.rCropRzInfo.fgRRZOnOff    = MTRUE;


            rIspInfo.rCamInfo.rCropRzInfo.i4RRZinW      = rNewP2Param.rpRzInSize->w;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZinH      = rNewP2Param.rpRzInSize->h;

            if((rIspInfo.rCamInfo.rCropRzInfo.i4RRZinW != rIspInfo.rCamInfo.rCropRzInfo.i4TGoutW) ||
               (rIspInfo.rCamInfo.rCropRzInfo.i4RRZinH != rIspInfo.rCamInfo.rCropRzInfo.i4TGoutH))
            {
                rIspInfo.rCamInfo.rCropRzInfo.fgFBinOnOff   = MTRUE;
            }
            else
            {
                rIspInfo.rCamInfo.rCropRzInfo.fgFBinOnOff   = MFALSE;
            }
        }
        else
        {
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZofstX    = 0;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZofstY    = 0;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZcropW    = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutW;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZcropH    = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutH;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutW     = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutW;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutH     = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutH;
            rIspInfo.rCamInfo.rCropRzInfo.fgRRZOnOff    = MFALSE;

            rIspInfo.rCamInfo.rCropRzInfo.i4RRZinW      = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutW;
            rIspInfo.rCamInfo.rCropRzInfo.i4RRZinH      = rIspInfo.rCamInfo.rCropRzInfo.i4TGoutH;
            rIspInfo.rCamInfo.rCropRzInfo.fgFBinOnOff   = MFALSE;
        }

        if(rNewP2Param.i4P2InImgFmt ==1){

            MUINT32 ResizeYUV_W = rNewP2Param.ResizeYUV & 0x0000FFFF;
            MUINT32 ResizeYUV_H = rNewP2Param.ResizeYUV >> 16;


            if( ResizeYUV_W != 0 && ResizeYUV_H !=0){
                if((ResizeYUV_W != rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutW) ||
                (ResizeYUV_H != rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutH)){
                rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutW = ResizeYUV_W;
                rIspInfo.rCamInfo.rCropRzInfo.i4RRZoutH = ResizeYUV_H;
                rIspInfo.rCamInfo.rCropRzInfo.fgRRZOnOff = MTRUE;
                }
            }
        }

#if 1
        // CRZ temporarily disable, so rIspP2CropInfo align RRZ info rCropRzInfo
        rIspInfo.rIspP2CropInfo = rIspInfo.rCamInfo.rCropRzInfo;

#else
        if (rNewP2Param.rpP2OriginSize && rNewP2Param.rpP2Crop && rNewP2Param.rpP2RzSize)
        {
            rIspInfo.rIspP2CropInfo.i4FullW     = rNewP2Param.rpP2OriginSize->w;
            rIspInfo.rIspP2CropInfo.i4FullH     = rNewP2Param.rpP2OriginSize->h;
            rIspInfo.rIspP2CropInfo.i4OfstX     = rNewP2Param.rpP2Crop->p.x;
            rIspInfo.rIspP2CropInfo.i4OfstY     = rNewP2Param.rpP2Crop->p.y;
            rIspInfo.rIspP2CropInfo.i4Width     = rNewP2Param.rpP2Crop->s.w;
            rIspInfo.rIspP2CropInfo.i4Height    = rNewP2Param.rpP2Crop->s.h;
            rIspInfo.rIspP2CropInfo.i4RzWidth   = rNewP2Param.rpP2RzSize->w;
            rIspInfo.rIspP2CropInfo.i4RzHeight  = rNewP2Param.rpP2RzSize->h;
            rIspInfo.rIspP2CropInfo.fgOnOff     = MTRUE;
        }
        else
        {
            rIspInfo.rIspP2CropInfo.i4FullW     = rIspInfo.rCamInfo.rCropRzInfo.i4FullW;
            rIspInfo.rIspP2CropInfo.i4FullH     = rIspInfo.rCamInfo.rCropRzInfo.i4FullH;
            rIspInfo.rIspP2CropInfo.i4OfstX     = 0;
            rIspInfo.rIspP2CropInfo.i4OfstY     = 0;
            rIspInfo.rIspP2CropInfo.i4Width     = rIspInfo.rCamInfo.rCropRzInfo.i4FullW;
            rIspInfo.rIspP2CropInfo.i4Height    = rIspInfo.rCamInfo.rCropRzInfo.i4FullH;
            rIspInfo.rIspP2CropInfo.i4RzWidth   = rIspInfo.rCamInfo.rCropRzInfo.i4FullW;
            rIspInfo.rIspP2CropInfo.i4RzHeight  = rIspInfo.rCamInfo.rCropRzInfo.i4FullH;
            rIspInfo.rIspP2CropInfo.fgOnOff     = MFALSE;
        }
#endif

        rIspInfo.rCamInfo.eEdgeMode = static_cast<mtk_camera_metadata_enum_android_edge_mode_t>(rNewP2Param.u1appEdgeMode);
        rIspInfo.rCamInfo.eNRMode = static_cast<mtk_camera_metadata_enum_android_noise_reduction_mode_t>(rNewP2Param.u1NrMode);


        if (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_YUV_Reprocess)
        {
            rIspInfo.rCamInfo.rAEInfo.u4RealISOValue= rNewP2Param.i4ISO;
            rIspInfo.rCamInfo.eIdx_Scene = static_cast<NSIspTuning::EIndex_Scene_T>(0);  //MTK_CONTROL_SCENE_MODE_DISABLED
            rIspInfo.rCamInfo.rMapping_Info.eSensorMode = NSIspTuning::ESensorMode_Capture;
        }
    }

    AAA_TRACE_END_HAL;

    return MTRUE;
}


MBOOL
HalIspImp::
setP2Params(P2Param_T const &rNewP2Param, ResultP2_T* pResultP2)
{
#if 0 //Not use
    AAA_TRACE_HAL(setP2Params);
    if( rNewP2Param.rScaleCropRect.i4Xwidth != 0 && rNewP2Param.rScaleCropRect.i4Yheight != 0 )
        IAeMgr::getInstance().setZoomWinInfo(m_i4SensorDev, rNewP2Param.rScaleCropRect.i4Xoffset,rNewP2Param.rScaleCropRect.i4Yoffset,rNewP2Param.rScaleCropRect.i4Xwidth,rNewP2Param.rScaleCropRect.i4Yheight);
    if( rNewP2Param.rScaleCropRect.i4Xwidth != 0 && rNewP2Param.rScaleCropRect.i4Yheight != 0 )
        IAwbMgr::getInstance().setZoomWinInfo(m_i4SensorDev, rNewP2Param.rScaleCropRect.i4Xoffset,rNewP2Param.rScaleCropRect.i4Yoffset,rNewP2Param.rScaleCropRect.i4Xwidth,rNewP2Param.rScaleCropRect.i4Yheight);
    // AE for Denoise OB2
    IAeMgr::getInstance().enableStereoDenoiseRatio(m_i4SensorDev, rNewP2Param.i4DenoiseMode);
#endif

    //ISP
    IspTuningMgr::getInstance().setIspUserIdx_Bright(m_i4SensorDev, rNewP2Param.i4BrightnessMode);
    IspTuningMgr::getInstance().setIspUserIdx_Hue(m_i4SensorDev, rNewP2Param.i4HueMode);
    IspTuningMgr::getInstance().setIspUserIdx_Sat(m_i4SensorDev, rNewP2Param.i4SaturationMode);
    IspTuningMgr::getInstance().setIspUserIdx_Edge(m_i4SensorDev, rNewP2Param.i4halEdgeMode);
    IspTuningMgr::getInstance().setIspUserIdx_Contrast(m_i4SensorDev, rNewP2Param.i4ContrastMode);

    //IspTuningMgr::getInstance().setToneMapMode(m_i4SensorDev, rNewP2Param.u1TonemapMode);
    if (rNewP2Param.u1TonemapMode == MTK_TONEMAP_MODE_CONTRAST_CURVE)
    {
        MINT32 i = 0;
        std::vector<MFLOAT> vecIn, vecOut;
        MINT32 i4Cnt = rNewP2Param.u4TonemapCurveRedSize/ 2;
        vecIn.resize(i4Cnt);
        vecOut.resize(i4Cnt);
        MFLOAT* pArrayIn = &(vecIn[0]);
        MFLOAT* pArrayOut = &(vecOut[0]);
        const MFLOAT* pCurve = rNewP2Param.pTonemapCurveRed;
        for (i = i4Cnt; i != 0; i--)
        {
            MFLOAT x, y;
            x = *pCurve++;
            y = *pCurve++;
            *pArrayIn++ = x;
            *pArrayOut++ = y;
            pResultP2->vecTonemapCurveRed.push_back(x);
            pResultP2->vecTonemapCurveRed.push_back(y);
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[Red]#%d(%f,%f)", rNewP2Param.i4MagicNum, x, y);
        }
        IspTuningMgr::getInstance().setTonemapCurve_Red(m_i4SensorDev, &(vecIn[0]), &(vecOut[0]), &i4Cnt);

        i4Cnt = rNewP2Param.u4TonemapCurveGreenSize/ 2;
        vecIn.resize(i4Cnt);
        vecOut.resize(i4Cnt);
        pArrayIn = &(vecIn[0]);
        pArrayOut = &(vecOut[0]);
        pCurve = rNewP2Param.pTonemapCurveGreen;
        for (i = i4Cnt; i != 0; i--)
        {
            MFLOAT x, y;
            x = *pCurve++;
            y = *pCurve++;
            *pArrayIn++ = x;
            *pArrayOut++ = y;
            pResultP2->vecTonemapCurveGreen.push_back(x);
            pResultP2->vecTonemapCurveGreen.push_back(y);
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[Green]#%d(%f,%f)", rNewP2Param.i4MagicNum, x, y);
        }
        IspTuningMgr::getInstance().setTonemapCurve_Green(m_i4SensorDev, &(vecIn[0]), &(vecOut[0]), &i4Cnt);

        i4Cnt = rNewP2Param.u4TonemapCurveBlueSize/ 2;
        vecIn.resize(i4Cnt);
        vecOut.resize(i4Cnt);
        pArrayIn = &(vecIn[0]);
        pArrayOut = &(vecOut[0]);
        pCurve = rNewP2Param.pTonemapCurveBlue;
        for (i = i4Cnt; i != 0; i--)
        {
            MFLOAT x, y;
            x = *pCurve++;
            y = *pCurve++;
            *pArrayIn++ = x;
            *pArrayOut++ = y;
            pResultP2->vecTonemapCurveBlue.push_back(x);
            pResultP2->vecTonemapCurveBlue.push_back(y);
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[Blue]#%d(%f,%f)", rNewP2Param.i4MagicNum, x, y);
        }
        IspTuningMgr::getInstance().setTonemapCurve_Blue(m_i4SensorDev, &(vecIn[0]), &(vecOut[0]), &i4Cnt);
    }
    AAA_TRACE_END_HAL;

    return MTRUE;
}

MBOOL
HalIspImp::
generateP2(MINT32 flowType, const NSIspTuning::ISP_INFO_T& rIspInfo, void* pTuningBuf, ResultP2_T* pResultP2)
{
    std::lock_guard<std::mutex> lock(m_P2Mtx);
    AAA_TRACE_HAL(generateP2);

    void* pRegBuf = ((TuningParam*)pTuningBuf)->pRegBuf;
    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[%s] + flow(%d), buf(%p)", __FUNCTION__, flowType, pRegBuf);
    IspTuningMgr::getInstance().validatePerFrameP2(m_i4SensorDev, flowType, rIspInfo, pTuningBuf);
#if CAM3_LSC_FEATURE_EN
    ILscBuf* pLscBuf = NSIspTuning::ILscMgr::getInstance(static_cast<ESensorDev_T>(m_i4SensorDev))->getP2Buf();
    if (pLscBuf)
        ((TuningParam*)pTuningBuf)->pLsc2Buf = pLscBuf->getBuf();
    else
        ((TuningParam*)pTuningBuf)->pLsc2Buf = NULL;
#endif

    if (!((dip_x_reg_t*)pRegBuf)->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_LCE_D1_EN){
        ((TuningParam*)pTuningBuf)->pLcsBuf = NULL;
    }
    ((TuningParam*)pTuningBuf)->pBpc2Buf = IspTuningMgr::getInstance().getDMGItable(m_i4SensorDev);
    IspTuningBufCtrl::getInstance(m_i4SensorDev)->updateHint((void*)(&rIspInfo.hint), rIspInfo.rCamInfo.u4Id);

    // debug info
    if (pResultP2)
    {
#if 0
        if (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == EIspProfile_Capture_MultiPass_HWNR)
        {
            CAM_LOGD_IF(m_3ALogEnable, "[%s] get debug info p2 for Multi_Pass_NR #(%d)", __FUNCTION__, rIspInfo.rCamInfo.u4Id);
            if (0 == pResultP2->vecDbgIspP2_MultiP.size())
            {
                CAM_LOGD_IF(m_3ALogEnable, "[%s] Need to allocate P2 result for Multi_Pass_NR", __FUNCTION__);
                pResultP2->vecDbgIspP2_MultiP.resize(sizeof(DEBUG_RESERVEA_INFO_T));
            }
            DEBUG_RESERVEA_INFO_T& rIspExifDebugInfo = *reinterpret_cast<DEBUG_RESERVEA_INFO_T*>(pResultP2->vecDbgIspP2_MultiP.editArray());
            IspTuningMgr::getInstance().getDebugInfo_MultiPassNR(m_i4SensorDev, rIspInfo, rIspExifDebugInfo, pRegBuf);
        }
#endif
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[%s] get debug info p2 #(%d)", __FUNCTION__, rIspInfo.rCamInfo.u4Id);
        if (0 == pResultP2->vecDbgIspP2.size())
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[%s] Need to allocate P2 result", __FUNCTION__);
            pResultP2->vecDbgIspP2.resize(sizeof(AAA_DEBUG_INFO2_T));
        }

        AAA_DEBUG_INFO2_T& rDbg3AInfo2 = *reinterpret_cast<AAA_DEBUG_INFO2_T*>(pResultP2->vecDbgIspP2.editArray());
        NSIspExifDebug::IspExifDebugInfo_T& rIspExifDebugInfo = rDbg3AInfo2.rISPDebugInfo;
        IspTuningMgr::getInstance().getDebugInfoP2(m_i4SensorDev, rIspInfo, rIspExifDebugInfo, pTuningBuf);

        MBOOL bDump = ::property_get_int32("vendor.debug.tuning.dump_capture", 0);
        if (!rIspInfo.rCamInfo.fgRPGEnable && bDump)
        {
            char filename[512];
            sprintf(filename, "/sdcard/debug/p2dbg_dump_capture-%04d.bin", rIspInfo.i4UniqueKey);
            FILE* fp = fopen(filename, "wb");
            if (fp)
            {
                ::fwrite(rIspExifDebugInfo.P2RegInfo.regDataP2, sizeof(rIspExifDebugInfo.P2RegInfo.regDataP2), 1, fp);
            }
            if (fp)
                fclose(fp);
        }
    }
    //update mapping info
    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_SETPARAM_P2, "[%s] -", __FUNCTION__);
    AAA_TRACE_END_HAL;

    return MTRUE;
}

MBOOL
HalIspImp::
getP2Result(P2Param_T const &rNewP2Param, ResultP2_T* pResultP2)
{
    if (rNewP2Param.u1TonemapMode != MTK_TONEMAP_MODE_CONTRAST_CURVE)
    {
        // Tonemap
        pResultP2->vecTonemapCurveRed.clear();
        pResultP2->vecTonemapCurveGreen.clear();
        pResultP2->vecTonemapCurveBlue.clear();

        MINT32 i = 0;
        MFLOAT *pIn, *pOut;
        MINT32 i4NumPt;
        IspTuningMgr::getInstance().getTonemapCurve_Blue(m_i4SensorDev, pIn, pOut, &i4NumPt);
        for (i = 0; i < i4NumPt; i++)
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P2, "[%s][Blue](%f,%f)", __FUNCTION__, *pIn, *pOut);
            pResultP2->vecTonemapCurveBlue.push_back(*pIn++);
            pResultP2->vecTonemapCurveBlue.push_back(*pOut++);
        }
        IspTuningMgr::getInstance().getTonemapCurve_Green(m_i4SensorDev, pIn, pOut, &i4NumPt);
        for (i = 0; i < i4NumPt; i++)
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P2, "[%s][Green](%f,%f)", __FUNCTION__, *pIn, *pOut);
            pResultP2->vecTonemapCurveGreen.push_back(*pIn++);
            pResultP2->vecTonemapCurveGreen.push_back(*pOut++);
        }
        IspTuningMgr::getInstance().getTonemapCurve_Red(m_i4SensorDev, pIn, pOut, &i4NumPt);
        for (i = 0; i < i4NumPt; i++)
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P2, "[%s][Red](%f,%f)", __FUNCTION__, *pIn, *pOut);
            pResultP2->vecTonemapCurveRed.push_back(*pIn++);
            pResultP2->vecTonemapCurveRed.push_back(*pOut++);
        }
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P2,"[%s] rsize(%d) gsize(%d) bsize(%d)"
            ,__FUNCTION__, (MINT32)pResultP2->vecTonemapCurveRed.size(), (MINT32)pResultP2->vecTonemapCurveGreen.size(), (MINT32)pResultP2->vecTonemapCurveBlue.size());

    }
    return MTRUE;
}

MINT32
HalIspImp::
convertP2ResultToMeta(const ResultP2_T& rResultP2, MetaSet_T* pResult) const
{
    MBOOL fgLog = (HALISP_LOG_CONVERT_P2 & m_u4LogEn);
    MINT32 i4Size = 0;
    if (pResult != NULL)
    {
        // tonemap
        i4Size = rResultP2.vecTonemapCurveBlue.size();
        if (i4Size)
        {
            const MFLOAT* pCurve = &(rResultP2.vecTonemapCurveBlue[0]);
            UPDATE_ENTRY_ARRAY(pResult->appMeta, MTK_TONEMAP_CURVE_BLUE, pCurve, i4Size);
            CAM_LOGD_IF(fgLog, "[%s] B size(%d), P0(%f,%f), P_end(%f,%f)", __FUNCTION__, i4Size, pCurve[0], pCurve[1], pCurve[i4Size-2], pCurve[i4Size-1]);
        }
        i4Size = rResultP2.vecTonemapCurveGreen.size();
        if (i4Size)
        {
            const MFLOAT* pCurve = &(rResultP2.vecTonemapCurveGreen[0]);
            UPDATE_ENTRY_ARRAY(pResult->appMeta, MTK_TONEMAP_CURVE_GREEN, pCurve, i4Size);
            CAM_LOGD_IF(fgLog, "[%s] G size(%d), P0(%f,%f), P_end(%f,%f)", __FUNCTION__, i4Size, pCurve[0], pCurve[1], pCurve[i4Size-2], pCurve[i4Size-1]);
        }
        i4Size = rResultP2.vecTonemapCurveRed.size();
        if (i4Size)
        {
            const MFLOAT* pCurve = &(rResultP2.vecTonemapCurveRed[0]);
            UPDATE_ENTRY_ARRAY(pResult->appMeta, MTK_TONEMAP_CURVE_RED, pCurve, i4Size);
            CAM_LOGD_IF(fgLog, "[%s] R size(%d), P0(%f,%f), P_end(%f,%f)", __FUNCTION__, i4Size, pCurve[0], pCurve[1], pCurve[i4Size-2], pCurve[i4Size-1]);
        }
    }
    return 0;
}

MBOOL
HalIspImp::
_readDump(TuningParam* pTuningBuf, const MetaSet_T& control, MetaSet_T* pResult, ISP_INFO_T* pIspInfo, MINT32 i4Format)
{
#if 1
    FileReadRule rule;
    if (!rule.isREADEnable("ISPHAL") && pIspInfo == NULL)
        return MFALSE;
    if ((pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_Capture) ||
        (pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_After_Blend) ||
        (pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_MFB) ||
        (pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_Single) ||
        (pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_Before_Blend))
    {
        MINT32 i4ReqNo = 0;
        QUERY_ENTRY_SINGLE(control.halMeta, MTK_PIPELINE_REQUEST_NUMBER, i4ReqNo);
        char strDump[512] = {'\0'};
        std::string strispProfileName = IspTuningMgr::getIspProfileName(pIspInfo->rCamInfo.rMapping_Info.eIspProfile);

        CAM_LOGD("[%s] i4ReqNo(%d) ispProfileName(%s) format(%d)",__FUNCTION__, i4ReqNo, strispProfileName.c_str(), i4Format);

        if (i4Format == E_Tuning_Output)
        {
            rule.getFile_P2TUNING(i4ReqNo, strispProfileName.c_str(), strDump, 512);
            FILE* fidTuning = fopen(strDump, "rb");
            if (fidTuning)
            {
                CAM_LOGD("[%s] %s pRegBuf size(%d)",__FUNCTION__, strDump, queryTuningSize());
                fread(pTuningBuf->pRegBuf, queryTuningSize(), 1, fidTuning);
                fclose(fidTuning);
            }
        }

        if (i4Format == E_Lsc_Output)
        {
            rule.getFile_LSC(i4ReqNo, strispProfileName.c_str(), strDump, 512);
            FILE* fidLscRead = fopen(strDump, "rb");
            if (fidLscRead){
                CAM_LOGD("[%s] %s ILscTbl::lscdata",__FUNCTION__, strDump, pIspInfo->rLscData.size());
                fread(pIspInfo->rLscData.data(), pIspInfo->rLscData.size(), 1, fidLscRead);
                fclose(fidLscRead);
            }
        }

        if (i4Format == E_MFB_Output && (pIspInfo->rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_MFB))
        {
            MINT32 i4MfbSize = 0;
            sendIspCtrl(EISPCtrl_GetMfbSize, (MINTPTR)&i4MfbSize, (MINTPTR)NULL);
            if (i4MfbSize == 0)
                return MTRUE;
            rule.getFile_MFB(i4ReqNo, strispProfileName.c_str(), strDump, 512);
            FILE* fidTuning = fopen(strDump, "rb");
            if (fidTuning)
            {
                CAM_LOGD("[%s] %s pMfbBuf size(%d)",__FUNCTION__, strDump, i4MfbSize);
                fread(pTuningBuf->pMfbBuf, i4MfbSize, 1, fidTuning);
                fclose(fidTuning);
            }
        }


    }
#endif
    return MTRUE;
}

IMetadata
HalIspImp::
getReprocStdExif(const MetaSet_T& control)
{
    IMetadata rMetaExif;

    MFLOAT fFNum = 0.0f;
    MFLOAT fFocusLength = 0.0f;
    MUINT8 u1AWBMode = 0;
    MINT32 u4LightSource = 0;
    MUINT8 u1SceneMode = 0;
    MINT32 u4ExpProgram = 0;
    MINT32 u4SceneCapType = 0;
    MUINT8 u1FlashState = 0;
    MINT32 u4FlashLightTimeus = 0;
    MINT32 u4AEComp = 0;
    MINT32 i4AEExpBias = 0;
    MINT32 u4AEISOSpeed = 0;
    MINT64 u8CapExposureTime = 0;
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_LENS_APERTURE, fFNum);
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_LENS_FOCAL_LENGTH , fFocusLength);
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_CONTROL_AWB_MODE, u1AWBMode);

    //LightSource
    switch (u1AWBMode)
    {
        case MTK_CONTROL_AWB_MODE_AUTO:
        case MTK_CONTROL_AWB_MODE_WARM_FLUORESCENT:
        case MTK_CONTROL_AWB_MODE_TWILIGHT:
        case MTK_CONTROL_AWB_MODE_INCANDESCENT:
            u4LightSource = eLightSourceId_Other;
            break;
        case MTK_CONTROL_AWB_MODE_DAYLIGHT:
            u4LightSource = eLightSourceId_Daylight;
            break;
        case MTK_CONTROL_AWB_MODE_FLUORESCENT:
            u4LightSource = eLightSourceId_Fluorescent;
            break;
#if 0
        case MTK_CONTROL_AWB_MODE_TUNGSTEN:
            u4LightSource = eLightSourceId_Tungsten;
            break;
#endif
        case MTK_CONTROL_AWB_MODE_CLOUDY_DAYLIGHT:
            u4LightSource = eLightSourceId_Cloudy;
            break;
        case MTK_CONTROL_AWB_MODE_SHADE:
            u4LightSource = eLightSourceId_Shade;
            break;
        default:
            u4LightSource = eLightSourceId_Other;
            break;
        }

    QUERY_ENTRY_SINGLE(control.appMeta, MTK_CONTROL_SCENE_MODE, u1SceneMode);

    //EXP_Program
    switch (u1SceneMode)
    {
        case MTK_CONTROL_SCENE_MODE_PORTRAIT:
            u4ExpProgram = eExpProgramId_Portrait;
            break;
        case MTK_CONTROL_SCENE_MODE_LANDSCAPE:
            u4ExpProgram = eExpProgramId_Landscape;
            break;
        default:
            u4ExpProgram = eExpProgramId_NotDefined;
            break;
    }

    //SCENE_CAP_TYPE
    switch (u1SceneMode)
    {
        case MTK_CONTROL_SCENE_MODE_DISABLED:
        case MTK_CONTROL_SCENE_MODE_NORMAL:
        case MTK_CONTROL_SCENE_MODE_NIGHT_PORTRAIT:
        case MTK_CONTROL_SCENE_MODE_THEATRE:
        case MTK_CONTROL_SCENE_MODE_BEACH:
        case MTK_CONTROL_SCENE_MODE_SNOW:
        case MTK_CONTROL_SCENE_MODE_SUNSET:
        case MTK_CONTROL_SCENE_MODE_STEADYPHOTO:
        case MTK_CONTROL_SCENE_MODE_FIREWORKS:
        case MTK_CONTROL_SCENE_MODE_SPORTS:
        case MTK_CONTROL_SCENE_MODE_PARTY:
        case MTK_CONTROL_SCENE_MODE_CANDLELIGHT:
            u4SceneCapType = eCapTypeId_Standard;
            break;
        case MTK_CONTROL_SCENE_MODE_PORTRAIT:
            u4SceneCapType = eCapTypeId_Portrait;
            break;
        case MTK_CONTROL_SCENE_MODE_LANDSCAPE:
            u4SceneCapType = eCapTypeId_Landscape;
            break;
        case MTK_CONTROL_SCENE_MODE_NIGHT:
            u4SceneCapType = eCapTypeId_Night;
            break;
        default:
            u4SceneCapType = eCapTypeId_Standard;
            break;
    }

    //FlashTimeUs
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_FLASH_STATE, u1FlashState);
    if (u1FlashState == MTK_FLASH_STATE_FIRED){
        u4FlashLightTimeus = 30000;
    }

    //AE_EXP_BIAS

    IMetadata mMetaStaticInfo = m_pResultPoolObj->getMetaStaticInfo();
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_CONTROL_AE_EXPOSURE_COMPENSATION, u4AEComp );
    // AE Comp Step
    MFLOAT fExpCompStep= 0.0f;
    MRational rStep;
    if (QUERY_ENTRY_SINGLE(mMetaStaticInfo, MTK_CONTROL_AE_COMPENSATION_STEP, rStep))
    {
        fExpCompStep = (MFLOAT) rStep.numerator / rStep.denominator;
        CAM_LOGD("[%s] ExpCompStep(%3.3f), (%d/%d)", __FUNCTION__, fExpCompStep, rStep.numerator, rStep.denominator);
    }
    i4AEExpBias = fExpCompStep*u4AEComp*10;


    QUERY_ENTRY_SINGLE(control.appMeta, MTK_SENSOR_EXPOSURE_TIME, u8CapExposureTime);
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_SENSOR_SENSITIVITY, u4AEISOSpeed);

    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_FNUMBER,              fFNum*10/*rExifInfo.u4FNumber*/);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_FOCAL_LENGTH,         fFocusLength*1000/*rExifInfo.u4FocalLength*/);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_AWB_MODE,             u1AWBMode);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_LIGHT_SOURCE,         u4LightSource);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_EXP_PROGRAM,          u4ExpProgram);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_SCENE_CAP_TYPE,       u4SceneCapType);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_FLASH_LIGHT_TIME_US,  u4FlashLightTimeus);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_AE_METER_MODE,        (MINT32)eMeteringMode_Average);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_AE_EXP_BIAS,          i4AEExpBias);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_CAP_EXPOSURE_TIME,    u8CapExposureTime/1000);
    UPDATE_ENTRY_SINGLE<MINT32>(rMetaExif, MTK_3A_EXIF_AE_ISO_SPEED,         u4AEISOSpeed);

    return rMetaExif;
}

unsigned int
HalIspImp::
queryTuningSize()
{
    return sizeof(dip_x_reg_t);
}

MINT32
HalIspImp::
dumpIsp(MINT32 flowType, const MetaSet_T& control, TuningParam* pTuningBuf, MetaSet_T* pResult)
{
#if 1
    MUINT32 u4readDump = 0;
    FileReadRule rule;
    if (!rule.isDumpEnable("ISPHAL"))
        return MFALSE;
    IMetadata::Memory pCaminfoBuf;
    auto bCamInfoBuf = IMetadata::getEntry<IMetadata::Memory>(&control.halMeta, MTK_PROCESSOR_CAMINFO, pCaminfoBuf);
    NSIspTuning::ISP_INFO_T rIspInfo;
    MINT32 i4IspProfile = -1;
    MUINT8 u1IspProfile = NSIspTuning::EIspProfile_Preview;
    MUINT32 u4DebugInfo = 0;

    extract(&rIspInfo.hint, &control.halMeta);

    if (QUERY_ENTRY_SINGLE(control.halMeta, MTK_3A_ISP_PROFILE, u1IspProfile)){
        i4IspProfile = u1IspProfile;
    }

    P2Param_T rNewP2Param;
    QUERY_ENTRY_SINGLE(control.appMeta, MTK_CONTROL_CAPTURE_INTENT, rNewP2Param.u1CapIntent);
    setISPInfo(rNewP2Param, rIspInfo, 0);

    if (bCamInfoBuf)
    {
     if ((rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_Capture) ||
         (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_Before_Blend) ||
         (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_MFB) ||
         (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_Single) ||
         (rIspInfo.rCamInfo.rMapping_Info.eIspProfile == NSIspTuning::EIspProfile_MFNR_After_Blend)){
            if (pTuningBuf->pRegBuf != NULL)
             {
                 const char *ispProfileName = IspTuningMgr::getIspProfileName(rIspInfo.rCamInfo.rMapping_Info.eIspProfile);
                 char strTuningFile[512] = {'\0'};
                 sprintf(strTuningFile, "/sdcard/camera_dump/%09d-%04d-%04d-%d-%s.p2buf", rIspInfo.hint.UniqueKey, rIspInfo.hint.FrameNo, rIspInfo.hint.RequestNo, m_i4SensorDev, ispProfileName);
                 FILE* fidTuning = fopen(strTuningFile, "wb");
                 if (fidTuning)
                 {
                      CAM_LOGD("[%s] %s pRegBuf size(%d)",__FUNCTION__, strTuningFile, queryTuningSize());
                      fwrite(pTuningBuf->pRegBuf, queryTuningSize(), 1, fidTuning);
                      fclose(fidTuning);
                 }
             }
        }
    }
#endif

    return MTRUE;
    //return MTRUE;
}

MINT32
HalIspImp::
get(MUINT32 frmId, MetaSet_T& result)
{
    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] sensorDev(%d), sensorIdx(%d) R(%d)", __FUNCTION__, m_i4SensorDev, m_i4SensorIdx, frmId);

    /*****************************
     *     To get ISP Result Pointer
     *****************************/
    MINT32 i4Ret = MTRUE;
    // Vector pointer
    AllResult_T *pAllResult = m_pResultPoolObj->getAllResult(frmId);
    // Get result pointer if validate true. Otherwise false
    const ISPResultToMeta_T* pISPResult = (ISPResultToMeta_T*)m_pResultPoolObj->getResult(frmId, E_ISP_RESULTTOMETA, __FUNCTION__);
    const LCSOResultToMeta_T* pLCSOResult = (LCSOResultToMeta_T*)m_pResultPoolObj->getResult(frmId, E_LCSO_RESULTTOMETA, __FUNCTION__);

    if(pISPResult == NULL)
    {
        // ResultPool - Fail to get the specified result, use current.
        // ex:convert 27, get 25 fail, history 25/26/27, use current 27
        MY_LOGE("[%s] Fail to get the specified result", __FUNCTION__);
        MINT32 rHistoryReqMagic[HistorySize] = {0,0,0};
        m_pResultPoolObj->getHistory(rHistoryReqMagic);

        MY_LOGW("[%s] History (Req0, Req1, Req2) = (#%d, #%d, #%d)", __FUNCTION__, rHistoryReqMagic[0], rHistoryReqMagic[1], rHistoryReqMagic[2]);

        for(MINT32 i = (HistorySize-1); i >= 0 ; i--)
        {
            if(rHistoryReqMagic[i] != 0 && rHistoryReqMagic[i] != frmId)
            {
                pAllResult  = m_pResultPoolObj->getAllResult(rHistoryReqMagic[i]);
                pISPResult  = (ISPResultToMeta_T*)m_pResultPoolObj->getResult((rHistoryReqMagic[i]), E_ISP_RESULTTOMETA, __FUNCTION__);
            }
            if(pISPResult != NULL)
            {
                MY_LOGW("[%s] Use Current-MetaResult historyMagic[%d]:%d", __FUNCTION__, i, rHistoryReqMagic[i]);
                break;
            }
        }
    }
    if(pISPResult == NULL)
    {
        MY_LOGW("[%s] Not find result to conver metadata(#%d)", __FUNCTION__, frmId);
        return (-1);
    }

    NSIspTuning::RAWIspCamInfo tempCamInfo;

    tempCamInfo = pISPResult->rCamInfo;

    if(pLCSOResult){
        tempCamInfo.rLCS_Info.rOutSetting = pLCSOResult->rLcsOutInfo;
        if(tempCamInfo.rLCS_Info.rOutSetting.i4FrmId != tempCamInfo.rLCS_Info.rInSetting.i4FrmId){
            CAM_LOGE("LCS Info MisMatch");
        }
    }
    else{
        // turn off LCE
        tempCamInfo.rLCS_Info.rInSetting.fgOnOff = MFALSE ;
    }

    /*****************************
     *     Convert ISP Result to Metadata
     *****************************/
    if(pISPResult)
    {
        UPDATE_MEMORY(result.halMeta, MTK_PROCESSOR_CAMINFO, tempCamInfo);

        // color correction matrix
        if (pAllResult->vecColorCorrectMat.size())
        {
            const MFLOAT* pfMat = &(pAllResult->vecColorCorrectMat[0]);
            IMetadata::IEntry entry(MTK_COLOR_CORRECTION_TRANSFORM);
            for (MINT32 k = 0; k < 9; k++)
            {
                MRational rMat;
                MFLOAT fVal = *pfMat++;
                rMat.numerator = fVal*512;
                rMat.denominator = 512;
                entry.push_back(rMat, Type2Type<MRational>());
                CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] Mat[%d] = (%3.6f, %d)", __FUNCTION__, k, fVal, rMat.numerator);
            }
            result.appMeta.update(MTK_COLOR_CORRECTION_TRANSFORM, entry);
        }
    }
    // Need to over-write CCU data to ISP EXIF
    // Update to resultPool buffer
    const CCUResultInfo_T* pCCUResult = (CCUResultInfo_T*)m_pResultPoolObj->getResult(frmId, E_CCU_RESULTINFO4OVERWRITE, __FUNCTION__);
    if(pCCUResult)
    {
        // protect vector before use vector
        std::lock_guard<std::mutex> Vec_lock(pAllResult->LockVecResult);
        if(pAllResult->vecExifInfo.size() && pAllResult->vecDbgIspInfo.size() > 0)
        {
            // debug exif
            AAA_DEBUG_INFO2_T& rDbg3AInfo2 = *reinterpret_cast<AAA_DEBUG_INFO2_T*>(pAllResult->vecDbgIspInfo.editArray());
            NSIspExifDebug::IspExifDebugInfo_T& rDbgIspInfo = rDbg3AInfo2.rISPDebugInfo;
            // P1 ISP
            IspTuningMgr::getInstance().setDebugInfo4CCU(m_i4SensorDev, pCCUResult->u4Rto, pCCUResult->rOBCResult, rDbgIspInfo);

            IMetadata metaExif;
            QUERY_ENTRY_SINGLE(result.halMeta, MTK_3A_EXIF_METADATA, metaExif);

            // debug info
            IMetadata::Memory dbgIspP1;
            dbgIspP1.appendVector(pAllResult->vecDbgIspInfo);
            UPDATE_ENTRY_SINGLE(metaExif, MTK_3A_EXIF_DBGINFO_ISP_DATA, dbgIspP1);
            UPDATE_ENTRY_SINGLE(result.halMeta, MTK_3A_EXIF_METADATA, metaExif);
        }

        // Need to over-write CCU AEInfo data to CamInfo
        // Update to resultPool buffer
        const AEResultInfo_T  *pAEResultInfo = (AEResultInfo_T*)m_pResultPoolObj->getResult(frmId, E_AE_RESULTINFO, __FUNCTION__);
        if(pAEResultInfo)
        {
            ISPResultToMeta_T     rISPResult;
            rISPResult.rCamInfo = pISPResult->rCamInfo;
            ::memcpy(&rISPResult.rCamInfo.rAEInfo, &pAEResultInfo->AEPerframeInfo.rAEISPInfo, sizeof(AE_ISP_INFO_T));
            m_pResultPoolObj->updateResult(LOG_TAG, frmId, E_ISP_RESULTTOMETA, &rISPResult);
        }
        else
            MY_LOGE("[%s] R(%d) pAEResultInfo is NULL", __FUNCTION__, frmId);
    }

    CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] - pAllResult:%p", __FUNCTION__, pAllResult);
    return i4Ret;
}

MINT32
HalIspImp::
getCur(MUINT32 frmId, MetaSet_T& result)
{
    CAM_LOGD("[%s] sensorDev(%d), sensorIdx(%d) R(%d)", __FUNCTION__, m_i4SensorDev, m_i4SensorIdx, frmId);
    MINT32 i4Ret = 0;
    AllResult_T *pAllResult = NULL;
    MINT32 i4Validate= 0;

    /*****************************
     *     get 2A CurResult from ResultPool
     *****************************/
    // ResultPool - 1. get result (dynamic) of x from 3AMgr
    i4Ret = getResultCur(frmId);//req/stt:5/2
    // ResultPool - 2. Use sttMagic to get ResultCur
    if(1 == i4Ret)
    {
        CAM_LOGD("[%s] get R[%d]", __FUNCTION__, frmId);
        pAllResult = m_pResultPoolObj->getAllResultCur(frmId);
        i4Validate = m_pResultPoolObj->isValidateCur(frmId, E_ISP_RESULTTOMETA);
    }

    /*****************************
     *     get special result
     *****************************/
    if(-1 == i4Ret)
    {
        // ResultPool - Get History
        MINT32 rHistoryReqMagic[HistorySize] = {0,0,0};
        m_pResultPoolObj->getHistory(rHistoryReqMagic);
        CAM_LOGW("[%s] History (Req0, Req1, Req2) = (#%d, #%d, #%d), Fail to get R[%d], current result will be obtained(%d).", __FUNCTION__, rHistoryReqMagic[0], rHistoryReqMagic[1], rHistoryReqMagic[2], frmId, rHistoryReqMagic[2]);
        pAllResult = m_pResultPoolObj->getAllResultLastCur(rHistoryReqMagic[2]);//get the last request EX: req/stt:4/1
        i4Validate = m_pResultPoolObj->isValidate(rHistoryReqMagic[2], E_ISP_RESULTTOMETA);
    }
    if(-2 == i4Ret)
    {
        CAM_LOGW("[%s] Fail to get R[%d], result will be obtained.", __FUNCTION__, frmId);
        pAllResult = m_pResultPoolObj->getAllResult(frmId);//req/stt:2/X
        i4Validate = m_pResultPoolObj->isValidate(frmId, E_ISP_RESULTTOMETA);
    }

    /*****************************
     *     convert result to metadata and update metedata to MW
     *****************************/

    const ISPResultToMeta_T* pISPResult = NULL;
    if(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->isValidate())
        pISPResult = ( (ISPResultToMeta_T*)(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->read()) );
    const LCSOResultToMeta_T* pLCSOResult = (LCSOResultToMeta_T*)m_pResultPoolObj->getResult(frmId, E_LCSO_RESULTTOMETA, __FUNCTION__);

    if(i4Validate == MTRUE && pISPResult != NULL)
    {
        NSIspTuning::RAWIspCamInfo tempCamInfo;

        tempCamInfo = pISPResult->rCamInfo;

        if(pLCSOResult){
            tempCamInfo.rLCS_Info.rOutSetting = pLCSOResult->rLcsOutInfo;
            if(tempCamInfo.rLCS_Info.rOutSetting.i4FrmId != tempCamInfo.rLCS_Info.rInSetting.i4FrmId){
                CAM_LOGE("LCS Info MisMatch");
            }
        }
        else{
            CAM_LOGE("No LCS in Result Pool, FrmID: %d", frmId);
        }

        UPDATE_MEMORY(result.halMeta, MTK_PROCESSOR_CAMINFO, tempCamInfo);

        // color correction matrix
        if (pAllResult->vecColorCorrectMat.size())
        {
            const MFLOAT* pfMat = &(pAllResult->vecColorCorrectMat[0]);
            IMetadata::IEntry entry(MTK_COLOR_CORRECTION_TRANSFORM);
            for (MINT32 k = 0; k < 9; k++)
            {
                MRational rMat;
                MFLOAT fVal = *pfMat++;
                rMat.numerator = fVal*512;
                rMat.denominator = 512;
                entry.push_back(rMat, Type2Type<MRational>());
                CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] Mat[%d] = (%3.6f, %d)", __FUNCTION__, k, fVal, rMat.numerator);
            }
            result.appMeta.update(MTK_COLOR_CORRECTION_TRANSFORM, entry);
        }
    }

    CAM_LOGD("[%s] - Validate:%d", __FUNCTION__, i4Validate);
    return 0;
}

MINT32
HalIspImp::
getResultCur(MINT32 i4FrmId)
{
    std::unique_lock<std::mutex> autoLock(m_rResultMtx);
    MINT32 i4ResultWaitCnt = 3;

    MINT32 i4Ret = 1;

    // ResultPool - get Current All Result with SttMagic
    // 1. Use SttMagic need to judge isValidate.
    // 2. If result validate, can get result address.
    AllResult_T *pAllResult = m_pResultPoolObj->getAllResultCur(i4FrmId);
    ISPResultToMeta_T *pISPResult = NULL;
    if(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->isValidate())
        pISPResult = (ISPResultToMeta_T*)(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->read());

    if(NULL == pISPResult)
    {
        // ResultPool - Wait to get Current All Result with SttMagic
        while (i4ResultWaitCnt)
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] wait result #(%d) i4ResultWaitCnt(%d)", __FUNCTION__, i4FrmId, i4ResultWaitCnt);
            m_rResultCond.wait_for(autoLock, std::chrono::nanoseconds(500000000));
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] wait result done #(%d), i4ResultWaitCnt(%d)", __FUNCTION__, i4FrmId, i4ResultWaitCnt);
            i4ResultWaitCnt--;

            pAllResult = m_pResultPoolObj->getAllResultCur(i4FrmId);
            if(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->isValidate())
                pISPResult = (ISPResultToMeta_T*)(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->read());

            if (pISPResult != NULL)
               break;
        }

        // ResultPool - CaptureStart ReqMagic is 2 3 4 5.., always update Result. If fail to get Current All Result, get last current All Result with ReqMagic 4.
        if(NULL == pISPResult)
        {
            // ResultPool - Get History
            MINT32 rHistoryReqMagic[HistorySize] = {0,0,0};
            m_pResultPoolObj->getHistory(rHistoryReqMagic);

            CAM_LOGW("[%s] History (Req0, Req1, Req2) = (#%d, #%d, #%d), Fail to get ResultCur with ReqMagic(%d), try getting ResultLastCur(%d)", __FUNCTION__,
                        rHistoryReqMagic[0], rHistoryReqMagic[1], rHistoryReqMagic[2], i4FrmId, rHistoryReqMagic[2]);
            i4Ret = -1;

            pAllResult = m_pResultPoolObj->getAllResultLastCur(rHistoryReqMagic[2]);//get req/stt:4/1
            if(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->isValidate())
                pISPResult = (ISPResultToMeta_T*)(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->read());

            // ResultPool - If fail to get last current All Result with last ReqMagic 4, get All Result with ReqMagic 2.
            if(pISPResult == NULL)
            {
                CAM_LOGW("Fail to get ResultLastCur, then get Result with ReqMagic(%d)", i4FrmId);
                pAllResult = m_pResultPoolObj->getAllResult(i4FrmId);//get req/stt:2/xx
                if(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->isValidate())
                    pISPResult = (ISPResultToMeta_T*)(pAllResult->ModuleResultAddr[E_ISP_RESULTTOMETA]->read());

                i4Ret = -2;
                if(pISPResult)
                    CAM_LOGW("[%s] (Req, Req, Stt) = (#%d, #%d, #%d)", __FUNCTION__, i4FrmId, pAllResult->rResultCfg.i4ReqMagic, pAllResult->rResultCfg.i4StatisticMagic);
                else
                    CAM_LOGE("[%s] Ret(%d) pISPResult is NULL", __FUNCTION__, i4Ret);
            }
            else
                CAM_LOGW("[%s] (Req, Req, Stt) = (#%d, #%d, #%d)", __FUNCTION__, i4FrmId, pAllResult->rResultCfg.i4ReqMagic, pAllResult->rResultCfg.i4StatisticMagic);
        }

        // ResultPool - std exif should be use capture start
        AllResult_T *pAllResultAtStart = m_pResultPoolObj->getAllResult(i4FrmId);

        if(NULL != pAllResultAtStart && pAllResult != NULL)
        {
            // protect vector before use vector
            std::lock_guard<std::mutex> Vec_lock(pAllResult->LockVecResult);

            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] (Req, ResultAtStartReq, ResultReq, Stt) = (#%d, #%d, #%d, #%d)", __FUNCTION__, i4FrmId, pAllResultAtStart->rResultCfg.i4ReqMagic, pAllResult->rResultCfg.i4ReqMagic, pAllResult->rResultCfg.i4StatisticMagic);

            MBOOL isIspStartEmpty = pAllResultAtStart->vecDbgIspInfo.empty();
            MBOOL isIspCurEmpty = pAllResult->vecDbgIspInfo.empty();

            if(!isIspStartEmpty && !isIspCurEmpty)
            {
                // get capture start AE setting to update EXIF info
                AAA_DEBUG_INFO2_T& rDbgISPInfoStart = *reinterpret_cast<AAA_DEBUG_INFO2_T*>(pAllResultAtStart->vecDbgIspInfo.editArray());
                AAA_DEBUG_INFO2_T& rDbgISPInfoCur = *reinterpret_cast<AAA_DEBUG_INFO2_T*>(pAllResult->vecDbgIspInfo.editArray());
                rDbgISPInfoCur.rISPDebugInfo = rDbgISPInfoStart.rISPDebugInfo;
            } else
            {
                CAM_LOGE("isIspStartEmpty(%d) isIspCurEmpty(%d)", isIspStartEmpty, isIspCurEmpty);
            }
        }
        else
        {
            CAM_LOGE("Fail get pResultAtStart (#%d) pAllResultAtStart/pAllResult:%p/%p", i4FrmId, pAllResultAtStart, pAllResult);
        }
    }
    else
    {
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] got result (#%d)", __FUNCTION__, i4FrmId);
    }
    return i4Ret;
}

MINT32
HalIspImp::
getCurrResult(MUINT32 i4FrmId)
{
    // ResultPool - To update Vector info
    AllResult_T *pAllResult = m_pResultPoolObj->getAllResult(i4FrmId);
    if(pAllResult == NULL)
        CAM_LOGE("[%s] pAllResult is NULL", __FUNCTION__);

    /*****************************
     *     Get ISP Result
     *****************************/
    std::lock_guard<std::mutex> autoLock(m_rResultMtx);
    if (m_u1ColorCorrectMode != MTK_COLOR_CORRECTION_MODE_TRANSFORM_MATRIX)
    {
        std::lock_guard<std::mutex> Vec_lock(pAllResult->LockVecResult);
        pAllResult->vecColorCorrectMat.resize(9);
        MFLOAT* pfColorCorrectMat = &(pAllResult->vecColorCorrectMat[0]);
        IspTuningMgr::getInstance().getColorCorrectionTransform(m_i4SensorDev,
        pfColorCorrectMat[0], pfColorCorrectMat[1], pfColorCorrectMat[2],
        pfColorCorrectMat[3], pfColorCorrectMat[4], pfColorCorrectMat[5],
        pfColorCorrectMat[6], pfColorCorrectMat[7], pfColorCorrectMat[8]
        );
    }
    ISPResultToMeta_T     rISPResult;
    MBOOL bRet = IspTuningMgr::getInstance().getCamInfo(m_i4SensorDev, rISPResult.rCamInfo);
    if (!bRet)
    {
        CAM_LOGE("Fail to get CamInfo");
    }else{
        LastInfo_T vLastInfo;
        // Backup caminfo
        ::memcpy(&vLastInfo.mBackupCamInfo, &rISPResult.rCamInfo, sizeof(NSIspTuning::RAWIspCamInfo));
        vLastInfo.mBackupCamInfo_copied = MTRUE;
        m_pResultPoolObj->updateLastInfo(vLastInfo);
        CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] Backup caminfo,copied(%d)/mode(%d)/profile(%d)",__FUNCTION__, vLastInfo.mBackupCamInfo_copied,
            vLastInfo.mBackupCamInfo.rMapping_Info.eSensorMode,
            vLastInfo.mBackupCamInfo.rMapping_Info.eIspProfile);
    }
#if MTK_CAM_NEW_NVRAM_SUPPORT
    IdxMgr::createInstance(static_cast<NSIspTuning::ESensorDev_T>(m_i4SensorDev))->setMappingInfo(static_cast<NSIspTuning::ESensorDev_T>(m_i4SensorDev), rISPResult.rCamInfo.rMapping_Info, i4FrmId);
#endif
    m_pResultPoolObj->updateResult(LOG_TAG, i4FrmId, E_ISP_RESULTTOMETA, &rISPResult);

    /*****************************
     *     Get ISP Exif Result
     *****************************/
    if (m_u1IsGetExif || m_bIsCapEnd)
    {
        // protect vector before use vector
        std::lock_guard<std::mutex> Vec_lock(pAllResult->LockVecResult);

        if(pAllResult->vecDbgIspInfo.size()==0)
            pAllResult->vecDbgIspInfo.resize(sizeof(AAA_DEBUG_INFO2_T));

        AAA_DEBUG_INFO2_T& rDbgIspInfo = *reinterpret_cast<AAA_DEBUG_INFO2_T*>(pAllResult->vecDbgIspInfo.editArray());

        if(pAllResult->vecDbgIspInfo.size() != 0)
        {
            CAM_LOGD_IF(m_u4LogEn & HALISP_LOG_GET_P1, "[%s] vecDbgIspInfo - Size(%d) Addr(%p)", __FUNCTION__, (MINT32)pAllResult->vecDbgIspInfo.size(), &rDbgIspInfo);
            IspTuningMgr::getInstance().getDebugInfoP1(m_i4SensorDev, rDbgIspInfo.rISPDebugInfo, MFALSE);
        }
        else
            CAM_LOGE(, "[%s] vecDbgIspInfo - Size(%d) Addr(%p)", __FUNCTION__, (MINT32)pAllResult->vecDbgIspInfo.size(), &rDbgIspInfo);
    }

    /*****************************
     *     condition_variable notify_all()
     *****************************/
    m_rResultCond.notify_all();

    return MTRUE;
}

MVOID
HalIspImp::
resume(MINT32 MagicNum)
{
    // apply 3A module's config
    if (MagicNum > 0)
    {
        RequestSet_T rRequestSet;
        MBOOL bEnable_flk = MFALSE;
        m_pCamIO->sendCommand(NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_FLK_INFO,
                            (MINTPTR)&bEnable_flk, 0, 0);
        rRequestSet.vNumberSet.clear();
        rRequestSet.vNumberSet.push_back(MagicNum);
        IspTuningMgr::getInstance().setFlkEnable(m_i4SensorDev, bEnable_flk);
        m_pTuning->notifyRPGEnable(m_i4SensorDev, MTRUE);   // apply awb gain for init stat
        m_pTuning->validate(m_i4SensorDev, rRequestSet, MTRUE);
    }
}

MINT32
HalIspImp::
getCurrLCSResult(ISP_LCS_OUT_INFO_T const &rLcsOutInfo)
{
    LCSOResultToMeta_T     rLCSResult;

    rLCSResult.rLcsOutInfo = rLcsOutInfo;

    m_pResultPoolObj->updateResult(LOG_TAG, rLCSResult.rLcsOutInfo.i4FrmId, E_LCSO_RESULTTOMETA, &rLCSResult);

    return MTRUE;
}


MINT32
HalIspImp::
InitLCS()
{
    CAM_LOGD_IF(1, "[%s] ++", __FUNCTION__);
    std::lock_guard<std::mutex> lock(m_Lock);

    MINT32 err = LCS_RETURN_NO_ERROR;

    //====== Create LCS Driver ======

    m_pLcsDrv = LcsDrv::CreateInstance(m_i4SensorIdx, m_i4SensorDev, (MINTPTR)this);

    if(m_pLcsDrv == NULL)
    {
        CAM_LOGE("LcsDrv::createInstance fail");
        goto create_fail_exit;
    }
    CAM_LOGD_IF(1, "[%s] -", __FUNCTION__);
    return LCS_RETURN_NO_ERROR;

create_fail_exit:

    if(m_pLcsDrv != NULL)
    {
        m_pLcsDrv->Uninit();
        m_pLcsDrv->DestroyInstance();
        m_pLcsDrv = NULL;
    }
    CAM_LOGD_IF(1, "[%s] --", __FUNCTION__);
    return LCS_RETURN_INVALID_DRIVER;
}


MINT32
HalIspImp::
UninitLCS()
{
    CAM_LOGD_IF(1, "[%s] ++", __FUNCTION__);
    std::lock_guard<std::mutex> lock(m_Lock);

    if(m_pLcsDrv != NULL)
    {
        m_pLcsDrv->Uninit();
        m_pLcsDrv->DestroyInstance();
        m_pLcsDrv = NULL;
    }
    CAM_LOGD_IF(1, "[%s] --", __FUNCTION__);
    return LCS_RETURN_NO_ERROR;
}


MINT32
HalIspImp::
ConfigLcs()
{
    CAM_LOGD_IF(1, "[%s] ++", __FUNCTION__);
    MINT32 err = LCS_RETURN_NO_ERROR;

    err = m_pLcsDrv->Init();
    if(err != LCS_RETURN_NO_ERROR)
    {
        CAM_LOGE("LcsDrv::Init fail");
        return LCS_RETURN_API_FAIL;
    }

    err = m_pLcsDrv->ConfigLcs(); // also register P1 Tuning Notify callback for LCS setting
    if(err != LCS_RETURN_NO_ERROR)
    {
        CAM_LOGE("ConfigLcs fail(%d)", err);
        return LCS_RETURN_API_FAIL;
    }
    CAM_LOGD_IF(1, "[%s] --", __FUNCTION__);
    return LCS_RETURN_NO_ERROR;
}




