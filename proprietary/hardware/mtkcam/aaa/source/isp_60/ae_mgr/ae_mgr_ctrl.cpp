/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "ae_mgr"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include <camera_custom_nvram.h>
#include <awb_param.h>
#include <af_param.h>
#include <flash_param.h>
#include <ae_param.h>
#include <mtkcam/def/common.h>
using namespace NSCam;
#include <faces.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>

#include <ae_tuning_custom.h>
//#include <isp_mgr.h>
#include <isp_tuning.h>
#include <camera_feature.h>
#include <isp_tuning_cam_info.h>
//#include <isp_tuning_mgr.h>
#include <aaa_sensor_mgr.h>
#include "camera_custom_hdr.h"
#include <camera_feature.h>
#include <private/aaa_hal_private.h>
#include <ae_mgr.h>
#include <nvbuf_util.h>
//#include "aaa_state_flow_custom.h"

using namespace NS3Av3;
//using namespace NSIspTuning;
//using namespace NSIspTuningv3;
using namespace NSFeature;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::UpdateSensorParams(AE_SENSOR_PARAM_T strSensorParams)
{
    if(strSensorParams.u8FrameDuration < strSensorParams.u8ExposureTime) {
        m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4FrameDuration = (MUINT32) (strSensorParams.u8ExposureTime / 1000);
        CAM_LOGD("[%s()] i4SensorDev = %d line:%d Frame Duration less than EXposure time: %lld %lld\n", __FUNCTION__, m_eSensorDev, __LINE__,
        (long long)strSensorParams.u8FrameDuration, (long long)strSensorParams.u8ExposureTime);
    } else {
        m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4FrameDuration = (MUINT32) (strSensorParams.u8FrameDuration / 1000);
    }
    m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4ExposureTime = (MUINT32) (strSensorParams.u8ExposureTime / 1000);
    m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4Sensitivity = (MUINT32) (strSensorParams.u4Sensitivity);

    CAM_LOGD("[%s()] i4SensorDev = %d line:%d Idx:%d %d FrameDuration:%d %lld Exposure Time:%d %lld ISO:%d %d\n", __FUNCTION__, m_eSensorDev, __LINE__,
        m_SensorQueueCtrl.uInputIndex, m_SensorQueueCtrl.uOutputIndex,
        m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4FrameDuration, (long long)strSensorParams.u8FrameDuration,
        m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4ExposureTime, (long long)strSensorParams.u8ExposureTime,
        m_SensorQueueCtrl.rSensorParamQueue[m_SensorQueueCtrl.uInputIndex].u4Sensitivity, strSensorParams.u4Sensitivity);

    m_SensorQueueCtrl.uInputIndex++;

    if(m_SensorQueueCtrl.uInputIndex >= AE_SENSOR_MAX_QUEUE) {
        m_SensorQueueCtrl.uInputIndex = 0;
    }


    return S_AE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::bBlackLevelLock(MBOOL bLockBlackLevel)
{
    if(m_bLockBlackLevel != bLockBlackLevel) {
        CAM_LOGD("[%s()] i4SensorDev = %d line:%d BlackLevel:%d %d \n", __FUNCTION__, m_eSensorDev, __LINE__, m_bLockBlackLevel, bLockBlackLevel);
        m_bLockBlackLevel = bLockBlackLevel;
        AAASensorMgr::getInstance().setSensorOBLock((ESensorDev_T)m_eSensorDev, m_bLockBlackLevel);
    }

    return S_AE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::getSensorParams(AE_SENSOR_PARAM_T &a_rSensorInfo)
{
    a_rSensorInfo = m_rSensorCurrentInfo;
    return S_AE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MINT64 AeMgr::getSensorRollingShutter() const
{
    MUINT32 tline = 0, vsize = 0;
    AAASensorMgr::getInstance().getRollingShutter((ESensorDev_T)m_eSensorDev, tline, vsize);
    return (MINT64) tline * vsize;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::CCUManualControl(MUINT32 u4ExpTime,MUINT32 u4AfeGain,MUINT32 u4IspGain,MBOOL EnableManual)
{
 if(EnableManual){
 m_u4CCUManualShutter = u4ExpTime;
 m_u4CCUManualAfeGain = u4AfeGain;
 m_u4CCUManualIspGain = u4IspGain;
 }
 else{
 m_u4CCUManualShutter =0;
 m_u4CCUManualAfeGain =0;
 m_u4CCUManualIspGain =0;
 }
 m_u4CCUManualEnable = EnableManual;
 return S_AE_OK;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::sendAECtrl(EAECtrl_T eAECtrl, MINTPTR iArg1, MINTPTR iArg2, MINTPTR iArg3, MINTPTR iArg4)
{
    MINT32 i4Ret = 0;

    switch (eAECtrl)
    {
        // --------------------------------- Get AE Info ---------------------------------
        case EAECtrl_GetCapDelayFrame:
            *(reinterpret_cast<MINT32*>(iArg1)) = get3ACaptureDelayFrame();
            break;
        case EAECtrl_GetAAOLineByteSize:
            CAM_LOGD( "[%s()] i4SensorDev:%d i4AAOLineByte:%d\n", __FUNCTION__, m_eSensorDev, m_rAEInitInput.i4AAOLineByte);
            *(reinterpret_cast<MINT32*>(iArg1)) = m_rAEInitInput.i4AAOLineByte;
            break;
        case EAECtrl_GetCapPlineTable:
            i4Ret = getAECapPlineTable(reinterpret_cast<MINT32*>(iArg1), *reinterpret_cast<strAETable*>(iArg2));
            break;
        case EAECtrl_GetSensorDelayInfo:
            i4Ret = getAEdelayInfo(reinterpret_cast<MINT32*>(iArg1), reinterpret_cast<MINT32*>(iArg2), reinterpret_cast<MINT32*>(iArg3));
            break;
        case EAECtrl_GetFDMeteringAreaInfo:
            i4Ret = getAEFDMeteringAreaInfo(*reinterpret_cast<std::vector<int>*>(iArg1), (MINT32)iArg2, (MINT32)iArg3);
            break;
        case EAECtrl_GetAEInitExpSetting:
            i4Ret = getAEInitExpoSetting(*reinterpret_cast<AEInitExpoSetting_T*>(iArg1));
            break;
        case EAECtrl_GetISOSpeedMode:
            *(reinterpret_cast<MINT32*>(iArg1)) = (m_u4AEISOSpeed == LIB3A_AE_ISO_SPEED_AUTO) ? m_AEPerFrameInfo.rAEISPInfo.u4RealISOValue : m_u4AEISOSpeed;
            break;
        case EAECtrl_GetAEMaxMeterAreaNum:
            *(reinterpret_cast<MINT32*>(iArg1)) = MAX_METERING_AREAS;
            break;
        case EAECtrl_GetAEMeterMode:
            *(reinterpret_cast<MINT32*>(iArg1)) = m_eAEMeterMode;
            break;
        case EAECtrl_GetCapParams:
            *reinterpret_cast<AE_MODE_CFG_T*>(iArg1) = m_rAEOutput.rCaptureMode[0];
            CAM_LOGD( "[%s()] m_i4SensorDev:%d Capture Exp. mode:%d Shutter:%d Sensor gain:%d Isp gain:%d flare:%d %d ISO:%d\n", __FUNCTION__, m_eSensorDev, m_rAEOutput.rCaptureMode[0].u4ExposureMode, m_rAEOutput.rCaptureMode[0].u4Eposuretime, m_rAEOutput.rCaptureMode[0].u4AfeGain,
                           m_rAEOutput.rCaptureMode[0].u4IspGain, m_rAEOutput.rCaptureMode[0].i2FlareGain, m_rAEOutput.rCaptureMode[0].i2FlareOffset, m_rAEOutput.rCaptureMode[0].u4RealISO);
            break;
        case EAECtrl_GetCCUResultCBActive:
            return getCCUresultCBActive(reinterpret_cast<MVOID*>(iArg1));
        case EAECtrl_GetCurrentPlineTable:
            i4Ret = getCurrentPlineTable(*reinterpret_cast<strAETable*>(iArg1), *reinterpret_cast<strAETable*>(iArg2), *reinterpret_cast<strAETable*>(iArg3), *reinterpret_cast<strAFPlineInfo*>(iArg4));
            break;
        case EAECtrl_GetCurrentPlineTableF:
            i4Ret = getCurrentPlineTableF(*reinterpret_cast<strFinerEvPline*>(iArg1), *reinterpret_cast<strFinerEvPline*>(iArg2));
            break;
        case EAECtrl_GetDebugInfo:
           i4Ret =  getDebugInfo(*reinterpret_cast<AE_DEBUG_INFO_T*>(iArg1), *reinterpret_cast<AE_PLINE_DEBUG_INFO_T*>(iArg2));
            break;
        case EAECtrl_GetEVCompensateIndex:
            *(reinterpret_cast<MINT32*>(iArg1)) = getEVCompensateIndex();
            break;
        case EAECtrl_GetExposureInfo:
            i4Ret = getExposureInfo(*reinterpret_cast<ExpSettingParam_T*>(iArg1));
            break;
        case EAECtrl_GetNVRAParam:
            i4Ret = getNVRAMParam(reinterpret_cast<MVOID *>(iArg1), reinterpret_cast<MUINT32 *>(iArg2));
            break;
        case EAECtrl_GetSensorDeviceInfo:
            i4Ret = getNvramData(m_eSensorDev);
            getSensorDeviceInfo(*reinterpret_cast<AE_DEVICES_INFO_T*>(iArg1));
            break;
        case EAECtrl_GetSensorRollingShutter:
            *(reinterpret_cast<MINT64*>(iArg1)) = getSensorRollingShutter();
            break;
        case EAECtrl_GetIsAELockSupported:
            *(reinterpret_cast<MINT32*>(iArg1)) = MTRUE;//isAELockSupported();
            break;
        case EAECtrl_GetCapDiffEVState:
            switchCapureDiffEVState((MINT8)iArg1,  *reinterpret_cast<strAEOutput*>(iArg2));
            break;
        case EAECtrl_GetExpSettingByShutterISOPriority:
            switchExpSettingByShutterISOpriority(*reinterpret_cast<AE_EXP_SETTING_T*>(iArg1), *reinterpret_cast<AE_EXP_SETTING_T*>(iArg2));
            break;
        case EAECtrl_GetNeedPresetControlCCU:
            return m_pIAeFlowCCU->queryStatus((MUINT32)E_AE_FLOW_CCU_AE_WORKING);

        // --------------------------------- Set AE Info ---------------------------------
        case EAECtrl_SetContinueShot:
            i4Ret = IsAEContinueShot((MBOOL)iArg1);
            break;
        case EAECtrl_SetPlineTableLimitation:
            i4Ret = modifyAEPlineTableLimitation((MBOOL)iArg1, (MBOOL)iArg2, (MUINT32)iArg3, (MUINT32)iArg4);
            break;
        case EAECtrl_SetPresetControlCCU:
            i4Ret = PresetControlCCU();
            break;
        case EAECtrl_SetAAOMode:
            CAM_LOGD( "[setAAOMode] m_i4AAOmode: %d -> %d (0:8/12bits, 1:14bits)\n", m_i4AAOmode, (MUINT32)iArg1);
            break;
        case EAECtrl_SetAAOProcInfo:
            i4Ret = setAAOProcInfo(reinterpret_cast<MVOID *>(iArg1), reinterpret_cast<AAO_PROC_INFO_T const *>(iArg2));
            break;
        case EAECtrl_SetAutoFlickerMode:
            i4Ret = setAEAutoFlickerMode((MUINT32)iArg1);
            break;
        case EAECtrl_SetLimiterMode:
            i4Ret = setAELimiterMode((MBOOL)iArg1);
            break;
        case EAECtrl_SetSMBuffermode:
            i4Ret = setAESMBuffermode((MBOOL)iArg1, (MINT32)iArg2);
            break;
        case EAECtrl_SetState2Converge:
            CAM_LOGD( "[%s] Chage to converge state, Old state:%d\n", __FUNCTION__, m_eAEState);
            m_eAEState = MTK_CONTROL_AE_STATE_CONVERGED;
            break;
        case EAECtrl_SetTargetMode:
            i4Ret = SetAETargetMode((eAETargetMODE) iArg1);
            break;
        case EAECtrl_SetSensorGain:
            CAM_LOGD( "setAfe(%d)",(MUINT32)iArg1);
            AAASensorMgr::getInstance().setSensorGain((ESensorDev_T)m_eSensorDev, (MUINT32)iArg1);
            break;
        case EAECtrl_SetCamScenarioMode:
            i4Ret = setCamScenarioMode((MUINT32)iArg1, (MBOOL)iArg2);
            break;
        case EAECtrl_SetEMVHDRratio:
            i4Ret = setEMVHDRratio((MUINT32)iArg1);
            break;
        case EAECtrl_SetExposureTime:
            CAM_LOGD( "setExp(%d)",(MUINT32)iArg1);
            AAASensorMgr::getInstance().setSensorExpTime((ESensorDev_T)m_eSensorDev, (MUINT32)iArg1);
            break;
        case EAECtrl_SetFDInfo:
            i4Ret = setFDInfo(reinterpret_cast<MVOID *>(iArg1), (MINT32)iArg2, (MINT32)iArg3);
            break;
        case EAECtrl_SetDGNGain:
            setIsp((MINT32)iArg1);
            break;
        case EAECtrl_SetMVHDR3ExpoProcInfo:
            i4Ret = setMVHDR3ExpoProcInfo(reinterpret_cast<MVOID *>(iArg1), (MUINT32)iArg2);
            break;
        case EAECtrl_SetNVRAMIndex:
            i4Ret = setNVRAMIndex((MUINT32)iArg1, (MBOOL)iArg2);
            break;
        case EAECtrl_SetOTInfo:
            i4Ret = setOTInfo(reinterpret_cast<MVOID *>(iArg1));
            break;
        case EAECtrl_SetSensorMode:
            CAM_LOGD( "sensorMode(%d) %d, %d",(MINT32)(iArg1), (MUINT32)(iArg2), (MUINT32)(iArg3));
            i4Ret = setSensorMode((MINT32)iArg1, (MUINT32)iArg2, (MUINT32)iArg3);
            break;
        case EAECtrl_SetStrobeMode:
            i4Ret = setStrobeMode((MBOOL)iArg1);
            break;
        case EAECtrl_SetzCHDRShot:
            i4Ret = setzCHDRShot((MBOOL)iArg1);
            break;
        case EAECtrl_SetUpdateAEBV:
            i4Ret = updateAEBV(reinterpret_cast<MVOID *>(iArg1));
            break;
        case EAECtrl_SetAEScenarioMode:
            i4Ret = updateAEScenarioMode((EIspProfile_T) iArg1);
            break;
        case EAECtrl_SetUnderExpdeltaBVIdx:
            i4Ret = updateAEUnderExpdeltaBVIdx((MINT32)iArg1);
            break;
        case EAECtrl_SetCaptureParams:
            i4Ret = updateCaptureParams(*reinterpret_cast<AE_MODE_CFG_T*>(iArg1));
            break;;
        case EAECtrl_SetPreviewParams:
            i4Ret = updatePreviewParams(*reinterpret_cast<AE_MODE_CFG_T*>(iArg1), (MINT32)iArg2, (MINT32)iArg3);
            break;
        case EAECtrl_SetSensorListenerParams:
            i4Ret = updateSensorListenerParams((MINT32 *)(iArg1));
            break;
        case EAECtrl_SetSensorParams:
            i4Ret = UpdateSensorParams(*reinterpret_cast<AE_SENSOR_PARAM_T*>(iArg1));
            break;
        case EAECtrl_SetStereoDenoiseRatio:
            i4Ret = updateStereoDenoiseRatio((MINT32 *)(iArg1));
            break;
        case EAECtrl_EnableAEOneShotControl:
            i4Ret = enableAEOneShotControl((MBOOL)iArg1);
            break;
        case EAECtrl_EnableAEStereoManualPline:
            i4Ret = enableAEStereoManualPline((MBOOL)iArg1);
            break;
        case EAECtrl_EnableAISManualPline:
            i4Ret = enableAISManualPline((MBOOL)iArg1);
            break;
        case EAECtrl_EnableBMDNManualPline:
            i4Ret = enableBMDNManualPline((MBOOL)iArg1);
            break;
        case EAECtrl_EnableEISRecording:
            i4Ret = enableEISRecording((MBOOL)iArg1);
            break;
        case EAECtrl_EnableFlareInManualControl:
            i4Ret = enableFlareInManualControl((MBOOL)iArg1);
            break;
        case EAECtrl_EnableHDRShot:
            i4Ret = enableHDRShot((MBOOL)iArg1);
            break;
        case EAECtrl_EnableMFHRManualPline:
            i4Ret = enableMFHRManualPline((MBOOL)iArg1);
            break;
        case EAECtrl_EnableAE:
            m_bEnableAE = MTRUE;
            CAM_LOGD( "enableAE()\n");
            break;
        case EAECtrl_DisableAE:
            m_bEnableAE = MFALSE;
            CAM_LOGD( "disableAE()\n");
            break;
        case EAECtrl_SetCCUManualControl:
            i4Ret = CCUManualControl((MUINT32)iArg1, (MUINT32)iArg2,(MUINT32)iArg3, (MBOOL)iArg4);
            break;
        case EAECtrl_SetDoBackAEInfo:
            i4Ret = doBackAEInfo();
            break;
        case EAECtrl_SetDoRestoreAEInfo:
            i4Ret = doRestoreAEInfo((MBOOL)iArg1);
            break;
        case EAECtrl_SetAFAELock:
            i4Ret = setAFAELock((MBOOL)iArg1);
            break;
        case EAECtrl_SetFDenable:
            i4Ret = setFDenable((MBOOL)iArg1);
            break;
        case EAECtrl_SetRestore:
            setRestore((MINT32)iArg1);
            break;
        case EAECtrl_SetSensorbyI2C:
            i4Ret = updateSensorbyI2C();
            break;
        case EAECtrl_SetSensorbyI2CBufferMode:
            i4Ret = updateSensorbyI2CBufferMode();
            break;
        case EAECtrl_SetCCUOnOff:
            i4Ret = setCCUOnOff((MBOOL)iArg1);
            break;
    }
    return i4Ret;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MRESULT AeMgr::setAEParams(AE_PARAM_SET_INFO const &rNewParam)
{
    MINT32 i4Ret = 0;

    CAM_LOGD( "[%s()] i4SensorDev:%d i4MinFps:%d i4MaxFps:%d u4AeMeterMode:%d i4RotateDegree:%d i4IsoSpeedMode:%d rMeteringAreas Cnt:%d Left:%d Top:%d Right:%d Bottom:%d Weight:%d bIsAELock:%d i4ExpIndex: %d fExpCompStep:%f u4AeMode:%d i4DenoiseMode:%d u4AntiBandingMode:%d u4CamMode:%d u4ShotMode:%d u4SceneMode:%d bBlackLvlLock:%d u4ZoomXOffset:%d u4ZoomYOffset:%d u4ZoomWidth:%d u4ZoomHeight:%d u1HdrMode:%d i4ZoomRatio:%d\n",
              __FUNCTION__, m_eSensorDev, rNewParam.i4MinFps, rNewParam.i4MaxFps, rNewParam.u4AeMeterMode, rNewParam.i4RotateDegree,
              rNewParam.i4IsoSpeedMode, rNewParam.rMeteringAreas.u4Count, rNewParam.rMeteringAreas.rAreas[0].i4Left, rNewParam.rMeteringAreas.rAreas[0].i4Top,
              rNewParam.rMeteringAreas.rAreas[0].i4Right, rNewParam.rMeteringAreas.rAreas[0].i4Bottom, rNewParam.rMeteringAreas.rAreas[0].i4Weight,
              rNewParam.bIsAELock, rNewParam.i4ExpIndex, rNewParam.fExpCompStep, rNewParam.u4AeMode, rNewParam.i4DenoiseMode, rNewParam.u4AntiBandingMode,
              rNewParam.u4CamMode, rNewParam.u4ShotMode, rNewParam.u4SceneMode, rNewParam.bBlackLvlLock, rNewParam.u4ZoomXOffset, rNewParam.u4ZoomYOffset,
              rNewParam.u4ZoomWidth, rNewParam.u4ZoomHeight, rNewParam.i4ZoomRatio);

    i4Ret = setAEMinMaxFrameRate(rNewParam.i4MinFps, rNewParam.i4MaxFps)
          | setAEMeteringMode(rNewParam.u4AeMeterMode)
          | setAERotateDegree(rNewParam.i4RotateDegree)
          | setAEISOSpeed(rNewParam.i4IsoSpeedMode)
          | setAEMeteringArea(&rNewParam.rMeteringAreas)
          | setAPAELock(rNewParam.bIsAELock)
          | setAEEVCompIndex(rNewParam.i4ExpIndex, rNewParam.fExpCompStep)
          | setAEMode(rNewParam.u4AeMode)
          | enableStereoDenoiseRatio(rNewParam.i4DenoiseMode)
          | setAEFlickerMode(rNewParam.u4AntiBandingMode)
          | setAECamMode(rNewParam.u4CamMode)
          | setAEShotMode(rNewParam.u4ShotMode)
          | setSceneMode(rNewParam.u4SceneMode)
          | bBlackLevelLock(rNewParam.bBlackLvlLock)
          | setZoomWinInfo(rNewParam.u4ZoomXOffset, rNewParam.u4ZoomYOffset, rNewParam.u4ZoomWidth, rNewParam.u4ZoomHeight)
          | setAEHDRMode(rNewParam.u1HdrMode)
          | setDigZoomRatio(rNewParam.i4ZoomRatio);

    return i4Ret;
}

