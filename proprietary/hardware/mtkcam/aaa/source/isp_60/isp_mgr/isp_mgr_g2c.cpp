/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_g2c"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include "isp_mgr.h"

namespace NSIspTuningv3
{
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  G2C
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_G2C_T&
ISP_MGR_G2C_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_G2C_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_G2C_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_G2C_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_G2C_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_G2C_DEV<ESensorDev_Main>::getInstance();
    }
}

#if 0
template <>
ISP_MGR_G2C_T&
ISP_MGR_G2C_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_G2C_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0A,  conv_0a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0B,  conv_0b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1A,  conv_1a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1B,  conv_1b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2A,  conv_2a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2B,  conv_2b);

    return  (*this);
}

template <>
ISP_MGR_G2C_T&
ISP_MGR_G2C_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_G2C_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0A,  conv_0a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0B,  conv_0b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1A,  conv_1a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1B,  conv_1b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2A,  conv_2a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2B,  conv_2b);


    return  (*this);
}
#endif

MBOOL
ISP_MGR_G2C_T::
apply_P1(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, TuningMgr& rTuning, MINT32 i4SubsampleIdex)
{
    if(SubModuleIndex >= ESubModule_NUM){
        return MFALSE;
    }

    MBOOL bEnable = isEnable(SubModuleIndex);

    reinterpret_cast<REG_G2C_R1_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_Y_OFST = 0;
    reinterpret_cast<REG_G2C_R1_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_U_OFST = 0;
    reinterpret_cast<REG_G2C_R1_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_V_OFST = 0;

    if(bEnable){
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_00 = 153;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_01 = 301;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_CNV_02 = 58;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_10 = -86;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_11 = -170;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_CNV_12 = 256;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_20 = 256;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_21 = -214;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_CNV_22 = -42;
    }
    else{
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_00 = 512;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_01 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_CNV_02 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_10 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_11 = 512;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_CNV_12 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_20 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_21 = 0;
        reinterpret_cast<REG_G2C_R1_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_CNV_22 = 512;
    }

    //Top Control
    switch (SubModuleIndex)
    {
        case EG2C_R1:
            ISP_MGR_CAMCTL_T::getInstance(m_eSensorDev).setEnable_G2C_R1(MTRUE);
            rTuning.updateEngine(eTuningMgrFunc_G2C, MTRUE, i4SubsampleIdex);
            //rTuning.updateEngine(eTuningMgrFunc_G2C_R1, MTRUE, i4SubsampleIdex);
            break;
        case EG2C_R2:
            ISP_MGR_CAMCTL_T::getInstance(m_eSensorDev).setEnable_G2C_R2(MTRUE);
            rTuning.updateEngine(eTuningMgrFunc_G2C_R2, MTRUE, i4SubsampleIdex);
            break;
        default:
            CAM_LOGE("Apply Error Submodule Index: %d", SubModuleIndex);
            return  MFALSE;
    }

    rTuning.tuningMgrWriteRegs(
        static_cast<TUNING_MGR_REG_IO_STRUCT*>(m_ppRegInfoMulti[SubModuleIndex]),
        m_u4RegInfoNum, i4SubsampleIdex);

    dumpRegInfoP1("G2C", SubModuleIndex);

    return  MTRUE;
}

MBOOL
ISP_MGR_G2C_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
    if(SubModuleIndex >= ESubModule_NUM){
        return MFALSE;
    }

    MBOOL bEnable = isEnable(SubModuleIndex);

    reinterpret_cast<G2C_REG_D1A_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_Y_OFST = 0;
    reinterpret_cast<G2C_REG_D1A_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_U_OFST = 0;
    reinterpret_cast<G2C_REG_D1A_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_V_OFST = 0;

    if(bEnable){
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_00 =  153;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_01 =  301;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_CNV_02 =   58;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_10 =  -86;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_11 = -170;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_CNV_12 =  256;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_20 =  256;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_21 = -214;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_CNV_22 =  -42;
    }
    else{
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_Y_OFST = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_U_OFST = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_V_OFST = 0;

        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_00 = 512;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2C_CNV_01 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2C_CNV_02 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_10 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2C_CNV_11 = 512;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2C_CNV_12 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_20 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2C_CNV_21 = 0;
        reinterpret_cast<G2C_REG_D1A_G2C_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2C_CNV_22 = 512;
    }


    //Top Control
    switch (SubModuleIndex)
    {
        case EG2C_D1:
            ISP_WRITE_ENABLE_BITS(pReg, DIPCTL_D1A_DIPCTL_YUV_EN1, DIPCTL_G2C_D1_EN, MTRUE);
            break;
        default:
            CAM_LOGE("Apply Error Submodule Index: %d", SubModuleIndex);
            return  MFALSE;
    }

    writeRegs(static_cast<RegInfo_T*>(m_rIspRegInfo[SubModuleIndex]), m_u4RegInfoNum, pReg);

    dumpRegInfoP2("C2G", SubModuleIndex);

    return  MTRUE;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  G2CX
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_G2CX_T&
ISP_MGR_G2CX_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_G2CX_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_G2CX_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_G2CX_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_G2CX_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_G2CX_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_G2CX_T&
ISP_MGR_G2CX_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_G2CX_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0A,         conv_0a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0B,         conv_0b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1A,         conv_1a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1B,         conv_1b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2A,         conv_2a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2B,         conv_2b);

    return  (*this);
}


template <>
ISP_MGR_G2CX_T&
ISP_MGR_G2CX_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_G2CX_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0A,         conv_0a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0B,         conv_0b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1A,         conv_1a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1B,         conv_1b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2A,         conv_2a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2B,         conv_2b);

    return  (*this);
}
#if 0
template <>
ISP_MGR_G2CX_T&
ISP_MGR_G2CX_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_G2CX_SHADE_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_1,     shade_con_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_2,     shade_con_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_3,     shade_con_3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADE_TAR,       shade_tar);
    PUT_REG_INFO_MULTI(SubModuleIndex, SHADE_SP,        shade_sp);
    return  (*this);
}


template <>
ISP_MGR_G2CX_T&
ISP_MGR_G2CX_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_G2CX_SHADE_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_1,     shade_con_1);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_2,     shade_con_2);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADE_CON_3,     shade_con_3);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADE_TAR,       shade_tar);
    GET_REG_INFO_MULTI(SubModuleIndex, SHADE_SP,        shade_sp);

    return  (*this);
}
#endif

MBOOL
ISP_MGR_G2CX_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
    if(SubModuleIndex >= ESubModule_NUM){
        return MFALSE;
    }

    MBOOL bEnable = isEnable(SubModuleIndex);

    reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2CX_Y_OFST = 0;
    reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2CX_U_OFST = 0;
    reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2CX_V_OFST = 0;

    if(!bEnable){
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2CX_CNV_00 = 512;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.G2CX_CNV_01 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.G2CX_CNV_02 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2CX_CNV_10 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.G2CX_CNV_11 = 512;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.G2CX_CNV_12 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2CX_CNV_20 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.G2CX_CNV_21 = 0;
        reinterpret_cast<G2CX_REG_D1A_G2CX_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.G2CX_CNV_22 = 512;
    }


    //Top Control
    switch (SubModuleIndex)
    {
        case EG2CX_D1:
            ISP_WRITE_ENABLE_BITS(pReg, DIPCTL_D1A_DIPCTL_YUV_EN1, DIPCTL_G2CX_D1_EN, MTRUE);
            break;
        default:
            CAM_LOGE("Apply Error Submodule Index: %d", SubModuleIndex);
            return  MFALSE;
    }

    writeRegs(static_cast<RegInfo_T*>(m_rIspRegInfo[SubModuleIndex]), m_u4RegInfoNum, pReg);

    dumpRegInfoP2("G2CX", SubModuleIndex);

    return  MTRUE;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  C2G
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_C2G_T&
ISP_MGR_C2G_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_C2G_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_C2G_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_C2G_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_C2G_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev(%d)", eSensorDev);
        return  ISP_MGR_C2G_DEV<ESensorDev_Main>::getInstance();
    }
}
#if 0
template <>
ISP_MGR_C2G_T&
ISP_MGR_C2G_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_C2G_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0A,  conv_0a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_0B,  conv_0b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1A,  conv_1a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_1B,  conv_1b);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2A,  conv_2a);
    PUT_REG_INFO_MULTI(SubModuleIndex, CONV_2B,  conv_2b);

    return  (*this);
}


template <>
ISP_MGR_C2G_T&
ISP_MGR_C2G_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_C2G_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0A,  conv_0a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_0B,  conv_0b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1A,  conv_1a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_1B,  conv_1b);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2A,  conv_2a);
    GET_REG_INFO_MULTI(SubModuleIndex, CONV_2B,  conv_2b);


    return  (*this);
}
#endif
MBOOL
ISP_MGR_C2G_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
    if(SubModuleIndex >= ESubModule_NUM){
        return MFALSE;
    }

    MBOOL bEnable = isEnable(SubModuleIndex);

    if(bEnable){
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.C2G_Y_OFST = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.C2G_U_OFST = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.C2G_V_OFST = 0;

        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.C2G_CNV_00 =  512;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.C2G_CNV_01 =    0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.C2G_CNV_02 =  718;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.C2G_CNV_10 =  512;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.C2G_CNV_11 = -176;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.C2G_CNV_12 = -366;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.C2G_CNV_20 =  512;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.C2G_CNV_21 =  907;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.C2G_CNV_22 =    0;
    }
    else{
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.C2G_Y_OFST = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.C2G_U_OFST = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.C2G_V_OFST = 0;

        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.C2G_CNV_00 = 512;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0A))->Bits.C2G_CNV_01 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_0B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_0B))->Bits.C2G_CNV_02 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.C2G_CNV_10 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1A))->Bits.C2G_CNV_11 = 512;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_1B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_1B))->Bits.C2G_CNV_12 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.C2G_CNV_20 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2A*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2A))->Bits.C2G_CNV_21 = 0;
        reinterpret_cast<C2G_REG_D1A_C2G_CONV_2B*>(REG_INFO_VALUE_PTR_MULTI(SubModuleIndex, CONV_2B))->Bits.C2G_CNV_22 = 512;
    }

    //Top Control
    switch (SubModuleIndex)
    {
        case EC2G_D1:
            ISP_WRITE_ENABLE_BITS(pReg, DIPCTL_D1A_DIPCTL_YUV_EN1, DIPCTL_C2G_D1_EN, bEnable);
            break;
        default:
            CAM_LOGE("Apply Error Submodule Index: %d", SubModuleIndex);
            return  MFALSE;
    }

    writeRegs(static_cast<RegInfo_T*>(m_rIspRegInfo[SubModuleIndex]), m_u4RegInfoNum, pReg);

    dumpRegInfoP2("C2G", SubModuleIndex);

    return  MTRUE;
}



}


