/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "isp_mgr_nr3d"

#ifndef ENABLE_MY_LOG
    #define ENABLE_MY_LOG       (1)
#endif

#include <cutils/properties.h>
#include <aaa_types.h>
#include <aaa_error_code.h>
#include <mtkcam/utils/std/Log.h>
#include <camera_custom_nvram.h>
#include "isp_mgr.h"

namespace NSIspTuningv3
{

#define CLIP_NR3D(a,b,c)   ( (a<b)?  b : ((a>c)? c: a) )
#define ADD_AND_WRITE_R2CF_CNT(NR3D_R2C_VAL, NR3D_R2CF_CNT)                                                                     \
    do {                                                                                                                        \
        MUINT32 R2C_VAL = reinterpret_cast<ISP_DIP_X_MDP_TNR_R2C_1_T*>(REG_INFO_VALUE_PTR(DIP_X_MDP_TNR_R2C_1))->NR3D_R2C_VAL;  \
        if (m_##NR3D_R2CF_CNT >= R2C_VAL)                                                                                       \
        {                                                                                                                       \
            m_##NR3D_R2CF_CNT = 0;                                                                                              \
        }                                                                                                                       \
        else                                                                                                                    \
        {                                                                                                                       \
            m_##NR3D_R2CF_CNT++;                                                                                                \
        }                                                                                                                       \
        reinterpret_cast<ISP_DIP_X_MDP_TNR_R2C_3_T*>(REG_INFO_VALUE_PTR(DIP_X_MDP_TNR_R2C_3))->NR3D_R2CF_CNT = m_##NR3D_R2CF_CNT;\
    } while (0)

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// NR3D
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
ISP_MGR_NR3D_T&
ISP_MGR_NR3D_T::
getInstance(ESensorDev_T const eSensorDev)
{
    switch (eSensorDev)
    {
    case ESensorDev_Main: //  Main Sensor
        return  ISP_MGR_NR3D_DEV<ESensorDev_Main>::getInstance();
    case ESensorDev_MainSecond: //  Main Second Sensor
        return  ISP_MGR_NR3D_DEV<ESensorDev_MainSecond>::getInstance();
    case ESensorDev_Sub: //  Sub Sensor
        return  ISP_MGR_NR3D_DEV<ESensorDev_Sub>::getInstance();
    case ESensorDev_SubSecond: //  Main Second Sensor
        return  ISP_MGR_NR3D_DEV<ESensorDev_SubSecond>::getInstance();
    default:
        CAM_LOGE("eSensorDev = %d", eSensorDev);
        return  ISP_MGR_NR3D_DEV<ESensorDev_Main>::getInstance();
    }
}

template <>
ISP_MGR_NR3D_T&
ISP_MGR_NR3D_T::
put(MUINT8 SubModuleIndex, ISP_NVRAM_NR3D_T const& rParam)
{
    PUT_REG_INFO_MULTI(SubModuleIndex, ENG_CON,         eng_con);
    PUT_REG_INFO_MULTI(SubModuleIndex, SIZ,             siz);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_XY,         tile_xy);
    PUT_REG_INFO_MULTI(SubModuleIndex, ON_CON,          on_con);
    PUT_REG_INFO_MULTI(SubModuleIndex, ON_OFF,          on_off);
    PUT_REG_INFO_MULTI(SubModuleIndex, ON_SIZ,          on_siz);
    PUT_REG_INFO_MULTI(SubModuleIndex, TNR_ENABLE,      tnr_enable);
    PUT_REG_INFO_MULTI(SubModuleIndex, FLT_CONFIG,      flt_config);
    PUT_REG_INFO_MULTI(SubModuleIndex, FB_INFO1,        fb_info1);
    PUT_REG_INFO_MULTI(SubModuleIndex, THR_1,           thr_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_1,         curve_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_2,         curve_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_3,         curve_3);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_4,         curve_4);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_5,         curve_5);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_6,         curve_6);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_7,         curve_7);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_8,         curve_8);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_9,         curve_9);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_10,        curve_10);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_11,        curve_11);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_12,        curve_12);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_13,        curve_13);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_14,        curve_14);
    PUT_REG_INFO_MULTI(SubModuleIndex, CURVE_15,        curve_15);
    PUT_REG_INFO_MULTI(SubModuleIndex, R2C_1,           r2c_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, R2C_2,           r2c_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, R2C_3,           r2c_3);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_6,           dbg_6);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_15,          dbg_15);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_16,          dbg_16);
    PUT_REG_INFO_MULTI(SubModuleIndex, DEMO_1,          demo_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, DEMO_2,          demo_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    PUT_REG_INFO_MULTI(SubModuleIndex, DMY_0,           dmy_0);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_17,          dbg_17);
    PUT_REG_INFO_MULTI(SubModuleIndex, INTERR,          interr);
    PUT_REG_INFO_MULTI(SubModuleIndex, FB_INFO2,        fb_info2);
    PUT_REG_INFO_MULTI(SubModuleIndex, FB_INFO3,        fb_info3);
    PUT_REG_INFO_MULTI(SubModuleIndex, FB_INFO4,        fb_info4);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBFISH,          dbfish);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_9,           dbg_9);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_10,          dbg_10);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_11,          dbg_11);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_12,          dbg_12);
    PUT_REG_INFO_MULTI(SubModuleIndex, DBG_7,           dbg_7);
    PUT_REG_INFO_MULTI(SubModuleIndex, DMY_1,           dmy_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, DMY_2,           dmy_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SAVE_INFO1,      save_info1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SAVE_INFO2,      save_info2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_1,     snr_curve_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_2,     snr_curve_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_3,     snr_curve_3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_4,     snr_curve_4);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_5,     snr_curve_5);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_6,     snr_curve_6);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_7,     snr_curve_7);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_CONTROL_1,   snr_control_1);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_THR_2,       snr_thr_2);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_THR_3,       snr_thr_3);
    PUT_REG_INFO_MULTI(SubModuleIndex, SNR_THR_4,       snr_thr_4);
    PUT_REG_INFO_MULTI(SubModuleIndex, IN1_CNT,         in1_cnt);
    PUT_REG_INFO_MULTI(SubModuleIndex, IN2_CNT,         in2_cnt);
    PUT_REG_INFO_MULTI(SubModuleIndex, IN3_CNT,         in3_cnt);
    PUT_REG_INFO_MULTI(SubModuleIndex, OUT_CNT,         out_cnt);
    PUT_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    PUT_REG_INFO_MULTI(SubModuleIndex, TILE_LOSS,       tile_loss);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT1,      mcvp_stat1);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT2,      mcvp_stat2);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT3,      mcvp_stat3);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT4,      mcvp_stat4);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT5,      mcvp_stat5);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT6,      mcvp_stat6);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT7,      mcvp_stat7);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT8,      mcvp_stat8);
    PUT_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT9,      mcvp_stat9);

    return  (*this);
}


template <>
ISP_MGR_NR3D_T&
ISP_MGR_NR3D_T::
get(MUINT8 SubModuleIndex, ISP_NVRAM_NR3D_T & rParam)
{
    GET_REG_INFO_MULTI(SubModuleIndex, ENG_CON,         eng_con);
    GET_REG_INFO_MULTI(SubModuleIndex, SIZ,             siz);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_XY,         tile_xy);
    GET_REG_INFO_MULTI(SubModuleIndex, ON_CON,          on_con);
    GET_REG_INFO_MULTI(SubModuleIndex, ON_OFF,          on_off);
    GET_REG_INFO_MULTI(SubModuleIndex, ON_SIZ,          on_siz);
    GET_REG_INFO_MULTI(SubModuleIndex, TNR_ENABLE,      tnr_enable);
    GET_REG_INFO_MULTI(SubModuleIndex, FLT_CONFIG,      flt_config);
    GET_REG_INFO_MULTI(SubModuleIndex, FB_INFO1,        fb_info1);
    GET_REG_INFO_MULTI(SubModuleIndex, THR_1,           thr_1);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_1,         curve_1);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_2,         curve_2);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_3,         curve_3);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_4,         curve_4);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_5,         curve_5);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_6,         curve_6);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_7,         curve_7);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_8,         curve_8);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_9,         curve_9);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_10,        curve_10);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_11,        curve_11);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_12,        curve_12);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_13,        curve_13);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_14,        curve_14);
    GET_REG_INFO_MULTI(SubModuleIndex, CURVE_15,        curve_15);
    GET_REG_INFO_MULTI(SubModuleIndex, R2C_1,           r2c_1);
    GET_REG_INFO_MULTI(SubModuleIndex, R2C_2,           r2c_2);
    GET_REG_INFO_MULTI(SubModuleIndex, R2C_3,           r2c_3);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_6,           dbg_6);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_15,          dbg_15);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_16,          dbg_16);
    GET_REG_INFO_MULTI(SubModuleIndex, DEMO_1,          demo_1);
    GET_REG_INFO_MULTI(SubModuleIndex, DEMO_2,          demo_2);
    GET_REG_INFO_MULTI(SubModuleIndex, ATPG,            atpg);
    GET_REG_INFO_MULTI(SubModuleIndex, DMY_0,           dmy_0);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_17,          dbg_17);
    GET_REG_INFO_MULTI(SubModuleIndex, INTERR,          interr);
    GET_REG_INFO_MULTI(SubModuleIndex, FB_INFO2,        fb_info2);
    GET_REG_INFO_MULTI(SubModuleIndex, FB_INFO3,        fb_info3);
    GET_REG_INFO_MULTI(SubModuleIndex, FB_INFO4,        fb_info4);
    GET_REG_INFO_MULTI(SubModuleIndex, DBFISH,          dbfish);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_9,           dbg_9);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_10,          dbg_10);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_11,          dbg_11);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_12,          dbg_12);
    GET_REG_INFO_MULTI(SubModuleIndex, DBG_7,           dbg_7);
    GET_REG_INFO_MULTI(SubModuleIndex, DMY_1,           dmy_1);
    GET_REG_INFO_MULTI(SubModuleIndex, DMY_2,           dmy_2);
    GET_REG_INFO_MULTI(SubModuleIndex, SAVE_INFO1,      save_info1);
    GET_REG_INFO_MULTI(SubModuleIndex, SAVE_INFO2,      save_info2);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_1,     snr_curve_1);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_2,     snr_curve_2);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_3,     snr_curve_3);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_4,     snr_curve_4);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_5,     snr_curve_5);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_6,     snr_curve_6);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CURVE_7,     snr_curve_7);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_CONTROL_1,   snr_control_1);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_THR_2,       snr_thr_2);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_THR_3,       snr_thr_3);
    GET_REG_INFO_MULTI(SubModuleIndex, SNR_THR_4,       snr_thr_4);
    GET_REG_INFO_MULTI(SubModuleIndex, IN1_CNT,         in1_cnt);
    GET_REG_INFO_MULTI(SubModuleIndex, IN2_CNT,         in2_cnt);
    GET_REG_INFO_MULTI(SubModuleIndex, IN3_CNT,         in3_cnt);
    GET_REG_INFO_MULTI(SubModuleIndex, OUT_CNT,         out_cnt);
    GET_REG_INFO_MULTI(SubModuleIndex, STATUS,          status);
    GET_REG_INFO_MULTI(SubModuleIndex, TILE_LOSS,       tile_loss);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT1,      mcvp_stat1);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT2,      mcvp_stat2);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT3,      mcvp_stat3);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT4,      mcvp_stat4);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT5,      mcvp_stat5);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT6,      mcvp_stat6);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT7,      mcvp_stat7);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT8,      mcvp_stat8);
    GET_REG_INFO_MULTI(SubModuleIndex, MCVP_STAT9,      mcvp_stat9);

    return  (*this);
}


MBOOL
ISP_MGR_NR3D_T::
apply_P2(MUINT8 SubModuleIndex, const RAWIspCamInfo& rRawIspCamInfo, dip_x_reg_t* pReg)
{
#if 0
    MBOOL bNR3D_EN = isNr3dEnable(); //= isEnable();

    reinterpret_cast<ISP_DIP_X_CAM_TNR_ENG_CON_T*>(REG_INFO_VALUE_PTR(DIP_X_CAM_TNR_ENG_CON))->NR3D_CAM_TNR_EN = isNr3dEnable();
    // NR3D_TNR_Y_EN must equal NR3D_CAM_TNR_EN
    reinterpret_cast<ISP_DIP_X_MDP_TNR_TNR_ENABLE_T*>(REG_INFO_VALUE_PTR(DIP_X_MDP_TNR_TNR_ENABLE))->NR3D_TNR_Y_EN = isNr3dEnable();
    //reinterpret_cast<ISP_DIP_X_CAM_COLOR_START_T*>(REG_INFO_VALUE_PTR(DIP_X_CAM_COLOR_START))->COLOR_DISP_COLOR_START = isColorEnable();

    // TOP
    ISP_WRITE_ENABLE_BITS(pReg, DIP_X_CTL_YUV_EN, NR3D_EN, bNR3D_EN);
    ISP_WRITE_ENABLE_BITS(pReg, DIP_X_CTL_YUV_EN, COLOR_EN, isColorEnable());

    //R2C calculation
    ADD_AND_WRITE_R2CF_CNT(NR3D_R2C_VAL1, NR3D_R2CF_CNT1);
    ADD_AND_WRITE_R2CF_CNT(NR3D_R2C_VAL2, NR3D_R2CF_CNT2);
    ADD_AND_WRITE_R2CF_CNT(NR3D_R2C_VAL3, NR3D_R2CF_CNT3);
    ADD_AND_WRITE_R2CF_CNT(NR3D_R2C_VAL4, NR3D_R2CF_CNT4);

    writeRegs(static_cast<RegInfo_T*>(m_pRegInfo), m_u4RegInfoNum, pReg);

    dumpRegInfo("NR3D");
#endif
    return  MTRUE;
}

static MVOID demoNR3D(MBOOL bEnableSL2E, MRect &fullImg, MRect &onRegion)
{
    CAM_LOGD("demo NR3D_EN(1), SL2E_EN(%d), x,y,w,h = (%d,%d,%d,%d), full x,y,w,h = (%d,%d,%d,%d)",
    bEnableSL2E, onRegion.p.x, onRegion.p.y, onRegion.s.w, onRegion.s.h,
    fullImg.p.x, fullImg.p.y, fullImg.s.w, fullImg.s.h);

    if (onRegion.p.x == 0)
    {
        onRegion.s.w =
            (onRegion.s.w >= fullImg.s.w/2)
            ? fullImg.s.w/2
            : onRegion.s.w;
        onRegion.s.w &= ~1;
    }
    else
    {
        MINT32 tmpVal = fullImg.s.w/2 - onRegion.p.x;
        if (tmpVal >= 0)
        {
            onRegion.s.w = tmpVal;
            onRegion.s.w &= ~1;
        }
        else
        {
            onRegion.s.w = 0;
        }
    }
}

/* strong setting */
/*
adb shell setprop debug.camera.3dnr.level 1
adb shell setprop debug.nr3d.bm.enable 1
adb shell setprop debug.nr3d.bm.q_nl 32
adb shell setprop debug.nr3d.bm.q_sp 0
*/
/* weak setting */
/*
adb shell setprop debug.camera.3dnr.level 1
adb shell setprop debug.nr3d.bm.enable 1
adb shell setprop debug.nr3d.bm.q_nl 12
adb shell setprop debug.nr3d.bm.q_sp 12
*/
static MVOID benchmarkNR3DRegValue(MVOID *pReg)
{
#if 0
    if (pReg == NULL)
    {
        return;
    }
    dip_x_reg_t *pIspPhyReg = (dip_x_reg_t*) pReg;
    static MINT32 bmCount = 0;
    bmCount++;
    // NR3D_FLT_STR_MAX
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_FLT_STR_MAX = ::property_get_int32("debug.nr3d.bm.flt_str_max", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_FLT_STR_MAX);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_FLT_STR_MAX =%d", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_FLT_STR_MAX);
    }
    // NR3D_BDI_THR
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_BDI_THR = ::property_get_int32("debug.nr3d.bm.bdi_thr", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_BDI_THR);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_BDN_THR =%d", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_BDI_THR);
    }
    // NR3D_MV_PEN_W
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_W = ::property_get_int32("debug.nr3d.bm.mv_pen_w", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_W);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_MV_PEN_W =%d", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_W);
    }
    // NR3D_MV_PEN_THR
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_THR = ::property_get_int32("debug.nr3d.bm.mv_pen_thr", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_THR);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_MV_PEN_THR =%d", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_THR);
    }
    // NR3D_Q_NL
    pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_NL= ::property_get_int32("debug.nr3d.bm.q_nl",pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_NL);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_NL =%d", pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_NL);
    }
    // NR3D_Q_SP
    pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_SP= ::property_get_int32("debug.nr3d.bm.q_sp",pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_SP);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_SP =%d", pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_SP);
    }
    // NR3D_BLEND_RATIO_BLKY
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_BLKY= ::property_get_int32("debug.nr3d.bm.br_blky", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_BLKY);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_BLEND_RATIO_BLKY =%d", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_BLKY);
    }
    // NR3D_BLEND_RATIO_DE
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_DE= ::property_get_int32("debug.nr3d.bm.br_de",pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_DE);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_BLEND_RATIO_DE =%d", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_DE);
    }
    // NR3D_BLEND_RATIO_TXTR
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_TXTR= ::property_get_int32("debug.nr3d.bm.br_txtr",pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_TXTR );
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_BLEND_RATIO_TXTR =%d", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_TXTR);
    }
    // NR3D_BLEND_RATIO_MV
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_MV= ::property_get_int32("debug.nr3d.bm.br_mv",pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_MV );
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_BLEND_RATIO_MV =%d", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_MV);
    }

    char tmpStr[PROPERTY_VALUE_MAX];
    MINT32 ADB_NR3D_CUR_SEL_VAL=0;
    ADB_NR3D_CUR_SEL_VAL= ::property_get_int32("debug.nr3d.bm.curve_sel_val", ADB_NR3D_CUR_SEL_VAL);
    MINT32 CMD_NR3D_CURSEL_OFS = 1000;
    MINT32 CMD_NR3D_CURSEL = (ADB_NR3D_CUR_SEL_VAL/CMD_NR3D_CURSEL_OFS);//Type
    MINT32 CMD_NR3D_CTRPNT_OFS = 100;
    MINT32 CMD_NR3D_CTRPNT = (ADB_NR3D_CUR_SEL_VAL - (CMD_NR3D_CURSEL * CMD_NR3D_CURSEL_OFS)) / CMD_NR3D_CTRPNT_OFS;//Point
    MINT32 CMD_NR3D_CTRPNT_VAL = ADB_NR3D_CUR_SEL_VAL - CMD_NR3D_CURSEL * CMD_NR3D_CURSEL_OFS - CMD_NR3D_CTRPNT * CMD_NR3D_CTRPNT_OFS;
    if (CMD_NR3D_CURSEL==1)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==2)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==3)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==4)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==5)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==6)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==7)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_WVAR_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_WVAR_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_11.Bits.NR3D_Q_WVAR_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_11.Bits.NR3D_Q_WVAR_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_11.Bits.NR3D_Q_WVAR_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_11.Bits.NR3D_Q_WVAR_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_11.Bits.NR3D_Q_WVAR_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_12.Bits.NR3D_Q_WVAR_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==8)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_12.Bits.NR3D_Q_WSM_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_12.Bits.NR3D_Q_WSM_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_12.Bits.NR3D_Q_WSM_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_12.Bits.NR3D_Q_WSM_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_13.Bits.NR3D_Q_WSM_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_13.Bits.NR3D_Q_WSM_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_13.Bits.NR3D_Q_WSM_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_13.Bits.NR3D_Q_WSM_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
    }
    else if (CMD_NR3D_CURSEL==9)
    {
        if (CMD_NR3D_CTRPNT==0)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==1)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==2)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==3)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==4)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==5)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==6)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==7)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7 = CMD_NR3D_CTRPNT_VAL;
        }
        else if (CMD_NR3D_CTRPNT==8)
        {
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y8 = CMD_NR3D_CTRPNT_VAL;
        }
    }


	//start of WHITNEY E2 ONLY (registers not exist in E1)
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC = ::property_get_int32("debug.nr3d.bm.r2cenc", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_R2CENC =%d", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC);
    }

	MINT32 ADB_NR3D_R2C_VAL14=
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL1+
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL2*100+
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL3*10000+
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL4*1000000;


	ADB_NR3D_R2C_VAL14= ::property_get_int32("debug.nr3d.bm.r2cval14", ADB_NR3D_R2C_VAL14);//44332211

	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL1=ADB_NR3D_R2C_VAL14%100;
	ADB_NR3D_R2C_VAL14=ADB_NR3D_R2C_VAL14/100;
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL2=ADB_NR3D_R2C_VAL14%100;
	ADB_NR3D_R2C_VAL14=ADB_NR3D_R2C_VAL14/100;
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL3=ADB_NR3D_R2C_VAL14%100;
	ADB_NR3D_R2C_VAL14=ADB_NR3D_R2C_VAL14/100;
	pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL4=ADB_NR3D_R2C_VAL14%100;

	//end of WHITNEY E2 ONLY
	//************************************
    MINT32 ADB_NR3D_QBLKY_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3*1000000;
	//33221100
	//Y3Y2Y1Y0

	MINT32 ADB_NR3D_QBLKY_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7*1000000;

    MINT32 ADB_NR3D_QBLKC_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3*1000000;

	MINT32 ADB_NR3D_QBLKC_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7*1000000;

    MINT32 ADB_NR3D_DTXRLV_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3*1000000;

	MINT32 ADB_NR3D_DTXRLV_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7*1000000;

	MINT32 ADB_NR3D_D1B_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3*1000000;

	MINT32 ADB_NR3D_D1B_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7*1000000;

    MINT32 ADB_NR3D_D2TXRB_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3*1000000;

	MINT32 ADB_NR3D_D2TXRB_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7*1000000;

    MINT32 ADB_NR3D_MV_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3*1000000;

	MINT32 ADB_NR3D_MV_47=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4 +
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7*1000000;

    MINT32 ADB_NR3D_SDL_03=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3*1000000;

	MINT32 ADB_NR3D_SDL_48=
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5*100+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6*10000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7*1000000+
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y8*100000000;

    ADB_NR3D_QBLKY_03= ::property_get_int32("debug.nr3d.bm.blky03", ADB_NR3D_QBLKY_03);
	ADB_NR3D_QBLKY_47= ::property_get_int32("debug.nr3d.bm.blky47", ADB_NR3D_QBLKY_47);
    ADB_NR3D_QBLKC_03= ::property_get_int32("debug.nr3d.bm.blkc03", ADB_NR3D_QBLKC_03);
	ADB_NR3D_QBLKC_47= ::property_get_int32("debug.nr3d.bm.blkc47", ADB_NR3D_QBLKC_47);
    ADB_NR3D_DTXRLV_03= ::property_get_int32("debug.nr3d.bm.dtxrlv03", ADB_NR3D_DTXRLV_03);
	ADB_NR3D_DTXRLV_47= ::property_get_int32("debug.nr3d.bm.dtxrlv47", ADB_NR3D_DTXRLV_47);
    ADB_NR3D_D1B_03= ::property_get_int32("debug.nr3d.bm.d1b03", ADB_NR3D_D1B_03);
	ADB_NR3D_D1B_47= ::property_get_int32("debug.nr3d.bm.d1b47", ADB_NR3D_D1B_47);
    ADB_NR3D_D2TXRB_03= ::property_get_int32("debug.nr3d.bm.d2txrb03", ADB_NR3D_D2TXRB_03);
	ADB_NR3D_D2TXRB_47= ::property_get_int32("debug.nr3d.bm.d2txrb47", ADB_NR3D_D2TXRB_47);
    ADB_NR3D_MV_03= ::property_get_int32("debug.nr3d.bm.mv03", ADB_NR3D_MV_03);
	ADB_NR3D_MV_47= ::property_get_int32("debug.nr3d.bm.mv47", ADB_NR3D_MV_47);
    ADB_NR3D_SDL_03= ::property_get_int32("debug.nr3d.bm.sdl03", ADB_NR3D_SDL_03);
	ADB_NR3D_SDL_48= ::property_get_int32("debug.nr3d.bm.sdl47", ADB_NR3D_SDL_48);


//1.BLKY
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0=ADB_NR3D_QBLKY_03%100;
	ADB_NR3D_QBLKY_03=ADB_NR3D_QBLKY_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1=ADB_NR3D_QBLKY_03%100;
	ADB_NR3D_QBLKY_03=ADB_NR3D_QBLKY_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2=ADB_NR3D_QBLKY_03%100;
	ADB_NR3D_QBLKY_03=ADB_NR3D_QBLKY_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3=ADB_NR3D_QBLKY_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4=ADB_NR3D_QBLKY_47%100;
	ADB_NR3D_QBLKY_47=ADB_NR3D_QBLKY_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5=ADB_NR3D_QBLKY_47%100;
	ADB_NR3D_QBLKY_47=ADB_NR3D_QBLKY_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6=ADB_NR3D_QBLKY_47%100;
	ADB_NR3D_QBLKY_47=ADB_NR3D_QBLKY_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7=ADB_NR3D_QBLKY_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_BLKY_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7
            );
    }

//2.BLKC
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0=ADB_NR3D_QBLKC_03%100;
	ADB_NR3D_QBLKC_03=ADB_NR3D_QBLKC_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1=ADB_NR3D_QBLKC_03%100;
	ADB_NR3D_QBLKC_03=ADB_NR3D_QBLKC_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2=ADB_NR3D_QBLKC_03%100;
	ADB_NR3D_QBLKC_03=ADB_NR3D_QBLKC_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3=ADB_NR3D_QBLKC_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4=ADB_NR3D_QBLKC_47%100;
	ADB_NR3D_QBLKC_47=ADB_NR3D_QBLKC_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5=ADB_NR3D_QBLKC_47%100;
	ADB_NR3D_QBLKC_47=ADB_NR3D_QBLKC_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6=ADB_NR3D_QBLKC_47%100;
	ADB_NR3D_QBLKC_47=ADB_NR3D_QBLKC_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7=ADB_NR3D_QBLKC_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_BLKC_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7
            );
    }

//3.DETXRLV
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0=ADB_NR3D_DTXRLV_03%100;
	ADB_NR3D_DTXRLV_03=ADB_NR3D_DTXRLV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1=ADB_NR3D_DTXRLV_03%100;
	ADB_NR3D_DTXRLV_03=ADB_NR3D_DTXRLV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2=ADB_NR3D_DTXRLV_03%100;
	ADB_NR3D_DTXRLV_03=ADB_NR3D_DTXRLV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3=ADB_NR3D_DTXRLV_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4=ADB_NR3D_DTXRLV_47%100;
	ADB_NR3D_DTXRLV_47=ADB_NR3D_DTXRLV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5=ADB_NR3D_DTXRLV_47%100;
	ADB_NR3D_DTXRLV_47=ADB_NR3D_DTXRLV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6=ADB_NR3D_DTXRLV_47%100;
	ADB_NR3D_DTXRLV_47=ADB_NR3D_DTXRLV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7=ADB_NR3D_DTXRLV_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_DETXTR_LVL_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7
            );
    }

//4.DE1BASE
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0=ADB_NR3D_D1B_03%100;
	ADB_NR3D_D1B_03=ADB_NR3D_D1B_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1=ADB_NR3D_D1B_03%100;
	ADB_NR3D_D1B_03=ADB_NR3D_D1B_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2=ADB_NR3D_D1B_03%100;
	ADB_NR3D_D1B_03=ADB_NR3D_D1B_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3=ADB_NR3D_D1B_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4=ADB_NR3D_D1B_47%100;
	ADB_NR3D_D1B_47=ADB_NR3D_D1B_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5=ADB_NR3D_D1B_47%100;
	ADB_NR3D_D1B_47=ADB_NR3D_D1B_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6=ADB_NR3D_D1B_47%100;
	ADB_NR3D_D1B_47=ADB_NR3D_D1B_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7=ADB_NR3D_D1B_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_DE1_BASE_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7
            );
    }

//5.D2TXRB
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0=ADB_NR3D_D2TXRB_03%100;
	ADB_NR3D_D2TXRB_03=ADB_NR3D_D2TXRB_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1=ADB_NR3D_D2TXRB_03%100;
	ADB_NR3D_D2TXRB_03=ADB_NR3D_D2TXRB_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2=ADB_NR3D_D2TXRB_03%100;
	ADB_NR3D_D2TXRB_03=ADB_NR3D_D2TXRB_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3=ADB_NR3D_D2TXRB_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4=ADB_NR3D_D2TXRB_47%100;
	ADB_NR3D_D2TXRB_47=ADB_NR3D_D2TXRB_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5=ADB_NR3D_D2TXRB_47%100;
	ADB_NR3D_D2TXRB_47=ADB_NR3D_D2TXRB_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6=ADB_NR3D_D2TXRB_47%100;
	ADB_NR3D_D2TXRB_47=ADB_NR3D_D2TXRB_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7=ADB_NR3D_D2TXRB_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_DE2TXTR_BASE_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7
            );
    }

//6.MV
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0=ADB_NR3D_MV_03%100;
	ADB_NR3D_MV_03=ADB_NR3D_MV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1=ADB_NR3D_MV_03%100;
	ADB_NR3D_MV_03=ADB_NR3D_MV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2=ADB_NR3D_MV_03%100;
	ADB_NR3D_MV_03=ADB_NR3D_MV_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3=ADB_NR3D_MV_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4=ADB_NR3D_MV_47%100;
	ADB_NR3D_MV_47=ADB_NR3D_MV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5=ADB_NR3D_MV_47%100;
	ADB_NR3D_MV_47=ADB_NR3D_MV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6=ADB_NR3D_MV_47%100;
	ADB_NR3D_MV_47=ADB_NR3D_MV_47/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7=ADB_NR3D_MV_47%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_MV_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7
            );
    }


//7.SDL
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0=ADB_NR3D_SDL_03%100;
	ADB_NR3D_SDL_03=ADB_NR3D_SDL_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1=ADB_NR3D_SDL_03%100;
	ADB_NR3D_SDL_03=ADB_NR3D_SDL_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2=ADB_NR3D_SDL_03%100;
	ADB_NR3D_SDL_03=ADB_NR3D_SDL_03/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3=ADB_NR3D_SDL_03%100;

	pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4=ADB_NR3D_SDL_48%100;
	ADB_NR3D_SDL_48=ADB_NR3D_SDL_48/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5=ADB_NR3D_SDL_48%100;
	ADB_NR3D_SDL_48=ADB_NR3D_SDL_48/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6=ADB_NR3D_SDL_48%100;
	ADB_NR3D_SDL_48=ADB_NR3D_SDL_48/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7=ADB_NR3D_SDL_48%100;
	ADB_NR3D_SDL_48=ADB_NR3D_SDL_48/100;
	pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y8=ADB_NR3D_SDL_48%100;

    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_Q_SDL_Y0: %d, Y1: %d, Y2: %d, Y3: %d, Y4: %d, Y5: %d, Y6: %d, Y7: %d",
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3,

            pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6,
            pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7
            );
    }


    // NR3D_SL2_OFF
    pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF = ::property_get_int32("debug.nr3d.bm.sl2_off", pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_SL2_OFF =%d", pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF);
    }

    // NR3D_INK_EN
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN = ::property_get_int32("debug.nr3d.bm.ink_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_INK_EN =%d", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN);
    }
    // NR3D_INK_LEVEL_DISP
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_LEVEL_DISP = ::property_get_int32("debug.nr3d.bm.ink_level_disp", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_LEVEL_DISP);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_INK_LEVEL_DISP =%d", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_LEVEL_DISP);
    }
    // NR3D_INK_SEL
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_SEL = ::property_get_int32("debug.nr3d.bm.ink_sel", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_SEL);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_INK_SEL =%d", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_SEL);
    }

    //////////////start of 20170316

    pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_RESET = ::property_get_int32("debug.nr3d.cam_tnr_reset", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_RESET);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_CAM_TNR_RESET =%d", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_RESET);
    }

    pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_EN = ::property_get_int32("debug.nr3d.cam_tnr_en", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_EN);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_CAM_TNR_EN =%d", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_EN);
    }

    pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF = ::property_get_int32("debug.nr3d.sl2_off", pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_SL2_OFF =%d", pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_SL2_OFF );
    }

    pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_ON_EN = ::property_get_int32("debug.nr3d.on_en", pIspPhyReg->DIP_X_NR3D_ON_CON.Bits.NR3D_ON_EN);
    pIspPhyReg->DIP_X_MDP_TNR_TNR_ENABLE.Bits.NR3D_TNR_Y_EN = ::property_get_int32("debug.nr3d.tnr_y_en", pIspPhyReg->DIP_X_MDP_TNR_TNR_ENABLE.Bits.NR3D_TNR_Y_EN);
    pIspPhyReg->DIP_X_MDP_TNR_TNR_ENABLE.Bits.NR3D_TNR_C_EN = ::property_get_int32("debug.nr3d.tnr_c_en", pIspPhyReg->DIP_X_MDP_TNR_TNR_ENABLE.Bits.NR3D_TNR_C_EN);
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_BLKY = ::property_get_int32("debug.nr3d.blend_ratio_blky", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_BLKY);
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_DE = ::property_get_int32("debug.nr3d.blend_ratio_de", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_DE);
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_TXTR = ::property_get_int32("debug.nr3d.blend_ratio_txtr", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_TXTR);
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_MV = ::property_get_int32("debug.nr3d.blend_ratio_mv", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_BLEND_RATIO_MV);
    pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_FLT_STR_MAX = ::property_get_int32("debug.nr3d.flt_str_max", pIspPhyReg->DIP_X_MDP_TNR_FLT_CONFIG.Bits.NR3D_FLT_STR_MAX);
    pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_NL = ::property_get_int32("debug.nr3d.q_nl", pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_NL);
    pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_SP = ::property_get_int32("debug.nr3d.q_sp", pIspPhyReg->DIP_X_MDP_TNR_FB_INFO1.Bits.NR3D_Q_SP);
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_BDI_THR = ::property_get_int32("debug.nr3d.bdi_thr", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_BDI_THR);
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_W = ::property_get_int32("debug.nr3d.mv_pen_w", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_W);
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_THR = ::property_get_int32("debug.nr3d.mv_pen_thr", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_MV_PEN_THR);
    pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_SMALL_SAD_THR = ::property_get_int32("debug.nr3d.small_sad_thr", pIspPhyReg->DIP_X_MDP_TNR_THR_1.Bits.NR3D_SMALL_SAD_THR);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0 = ::property_get_int32("debug.nr3d.q_blky_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1 = ::property_get_int32("debug.nr3d.q_blky_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2 = ::property_get_int32("debug.nr3d.q_blky_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3 = ::property_get_int32("debug.nr3d.q_blky_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4 = ::property_get_int32("debug.nr3d.q_blky_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_1.Bits.NR3D_Q_BLKY_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5 = ::property_get_int32("debug.nr3d.q_blky_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6 = ::property_get_int32("debug.nr3d.q_blky_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7 = ::property_get_int32("debug.nr3d.q_blky_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKY_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0 = ::property_get_int32("debug.nr3d.q_blkc_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1 = ::property_get_int32("debug.nr3d.q_blkc_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_2.Bits.NR3D_Q_BLKC_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2 = ::property_get_int32("debug.nr3d.q_blkc_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3 = ::property_get_int32("debug.nr3d.q_blkc_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4 = ::property_get_int32("debug.nr3d.q_blkc_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5 = ::property_get_int32("debug.nr3d.q_blkc_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6 = ::property_get_int32("debug.nr3d.q_blkc_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_3.Bits.NR3D_Q_BLKC_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7 = ::property_get_int32("debug.nr3d.q_blkc_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_BLKC_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_4.Bits.NR3D_Q_DETXTR_LVL_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7 = ::property_get_int32("debug.nr3d.q_detxtr_lvl_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DETXTR_LVL_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0 = ::property_get_int32("debug.nr3d.q_de1_base_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_5.Bits.NR3D_Q_DE1_BASE_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1 = ::property_get_int32("debug.nr3d.q_de1_base_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2 = ::property_get_int32("debug.nr3d.q_de1_base_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3 = ::property_get_int32("debug.nr3d.q_de1_base_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4 = ::property_get_int32("debug.nr3d.q_de1_base_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5 = ::property_get_int32("debug.nr3d.q_de1_base_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_6.Bits.NR3D_Q_DE1_BASE_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6 = ::property_get_int32("debug.nr3d.q_de1_base_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7 = ::property_get_int32("debug.nr3d.q_de1_base_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE1_BASE_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_7.Bits.NR3D_Q_DE2TXTR_BASE_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7 = ::property_get_int32("debug.nr3d.q_de2txtr_base_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_8.Bits.NR3D_Q_DE2TXTR_BASE_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0 = ::property_get_int32("debug.nr3d.q_mv_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1 = ::property_get_int32("debug.nr3d.q_mv_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2 = ::property_get_int32("debug.nr3d.q_mv_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3 = ::property_get_int32("debug.nr3d.q_mv_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4 = ::property_get_int32("debug.nr3d.q_mv_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_9.Bits.NR3D_Q_MV_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5 = ::property_get_int32("debug.nr3d.q_mv_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6 = ::property_get_int32("debug.nr3d.q_mv_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7 = ::property_get_int32("debug.nr3d.q_mv_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_10.Bits.NR3D_Q_MV_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0 = ::property_get_int32("debug.nr3d.q_sdl_y0", pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y0);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1 = ::property_get_int32("debug.nr3d.q_sdl_y1", pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y1);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2 = ::property_get_int32("debug.nr3d.q_sdl_y2", pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y2);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3 = ::property_get_int32("debug.nr3d.q_sdl_y3", pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y3);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4 = ::property_get_int32("debug.nr3d.q_sdl_y4", pIspPhyReg->DIP_X_MDP_TNR_CURVE_14.Bits.NR3D_Q_SDL_Y4);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5 = ::property_get_int32("debug.nr3d.q_sdl_y5", pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y5);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6 = ::property_get_int32("debug.nr3d.q_sdl_y6", pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y6);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7 = ::property_get_int32("debug.nr3d.q_sdl_y7", pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y7);
    pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y8 = ::property_get_int32("debug.nr3d.q_sdl_y8", pIspPhyReg->DIP_X_MDP_TNR_CURVE_15.Bits.NR3D_Q_SDL_Y8);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL1 = ::property_get_int32("debug.nr3d.r2c_val1", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL1);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL2 = ::property_get_int32("debug.nr3d.r2c_val2", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL2);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL3 = ::property_get_int32("debug.nr3d.r2c_val3", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL3);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL4 = ::property_get_int32("debug.nr3d.r2c_val4", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2C_VAL4);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC = ::property_get_int32("debug.nr3d.r2cenc", pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR1 = ::property_get_int32("debug.nr3d.r2c_txtr_thr1", pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR1);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR2 = ::property_get_int32("debug.nr3d.r2c_txtr_thr2", pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR2);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR3 = ::property_get_int32("debug.nr3d.r2c_txtr_thr3", pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR3);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR4 = ::property_get_int32("debug.nr3d.r2c_txtr_thr4", pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THR4);
    pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THROFF = ::property_get_int32("debug.nr3d.r2c_txtr_throff", pIspPhyReg->DIP_X_MDP_TNR_R2C_2.Bits.NR3D_R2C_TXTR_THROFF);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_FORCE_FLT_STR = ::property_get_int32("debug.nr3d.force_flt_str", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_FORCE_FLT_STR);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN = ::property_get_int32("debug.nr3d.ink_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_INK_EN =%d", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_EN );
    }

    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_Y_EN = ::property_get_int32("debug.nr3d.ink_y_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_Y_EN);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_SEL = ::property_get_int32("debug.nr3d.ink_sel", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_SEL);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_LEVEL_DISP = ::property_get_int32("debug.nr3d.ink_level_disp", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_INK_LEVEL_DISP);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_FORCE_EN = ::property_get_int32("debug.nr3d.force_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_6.Bits.NR3D_FORCE_EN);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_EN = ::property_get_int32("debug.nr3d.osd_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_EN);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_SEL = ::property_get_int32("debug.nr3d.osd_sel", pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_SEL);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_TARGH = ::property_get_int32("debug.nr3d.osd_targh", pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_TARGH);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_TARGV = ::property_get_int32("debug.nr3d.osd_targv", pIspPhyReg->DIP_X_MDP_TNR_DBG_15.Bits.NR3D_OSD_TARGV);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISPH = ::property_get_int32("debug.nr3d.osd_disph", pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISPH);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISPV = ::property_get_int32("debug.nr3d.osd_dispv", pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISPV);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISP_SCALE = ::property_get_int32("debug.nr3d.osd_disp_scale", pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_DISP_SCALE);
    pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_Y_EN = ::property_get_int32("debug.nr3d.osd_y_en", pIspPhyReg->DIP_X_MDP_TNR_DBG_16.Bits.NR3D_OSD_Y_EN);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_EN = ::property_get_int32("debug.nr3d.demo_en", pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_EN);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_SEL = ::property_get_int32("debug.nr3d.demo_sel", pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_SEL);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_TOP = ::property_get_int32("debug.nr3d.demo_top", pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_TOP);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_BOT = ::property_get_int32("debug.nr3d.demo_bot", pIspPhyReg->DIP_X_MDP_TNR_DEMO_1.Bits.NR3D_DEMO_BOT);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_2.Bits.NR3D_DEMO_LEFT = ::property_get_int32("debug.nr3d.demo_left", pIspPhyReg->DIP_X_MDP_TNR_DEMO_2.Bits.NR3D_DEMO_LEFT);
    pIspPhyReg->DIP_X_MDP_TNR_DEMO_2.Bits.NR3D_DEMO_RIGHT = ::property_get_int32("debug.nr3d.demo_right", pIspPhyReg->DIP_X_MDP_TNR_DEMO_2.Bits.NR3D_DEMO_RIGHT);

    //////////////end of 20170316

    //////////////start of 20170623
    // NR3D_UV_Signed
    pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_UV_SIGNED = ::property_get_int32("debug.nr3d.bm.uv_signed", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_UV_SIGNED);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_CAM_TNR_UV_SIGNED =%d", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_UV_SIGNED);
    }
    //NR3D_CAM_TNR_C42_FILT_DIS
    pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_C42_FILT_DIS = ::property_get_int32("debug.nr3d.bm.c42_filt_dis", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_C42_FILT_DIS);
    if (bmCount % 33 == 0)
    {
        CAM_LOGD("NR3D_CAM_TNR_C42_FILT_DIS =%d", pIspPhyReg->DIP_X_CAM_TNR_ENG_CON.Bits.NR3D_CAM_TNR_C42_FILT_DIS);
    }
    //////////////end of 20170623
#endif
}

static MVOID fixTuningValue(dip_x_reg_t *pReg)
{

#if 0
    if (pReg == NULL)
    {
        return;
    }

    dip_x_reg_t *pIspPhyReg = pReg;

    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_TNR_ENABLE, (0x1 << 31) | (0x1 << 30));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_FLT_CONFIG, (0x8 << 27) | (0x8 << 22) |
        (0x8 << 17) | (0x8 << 12) | (0x1C << 6));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_FB_INFO1, (0x4 << 26) | (0x14 << 20));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_THR_1, (0x4 << 26) | (0x8 << 22) | (0x20 << 16) | (0xA << 10));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_1, (0x1C << 26) | (0x1E << 20) |
        (0x20 << 14) | (0x1A << 8) | (0x10 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_2, (0x4 << 26) | (0x0 << 20) |
        (0x0 << 14) | (0x18 << 8) | (0x1A << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_3, (0x8 << 26) | (0x2 << 20) |
        (0x0 << 14) | (0x0 << 8) | (0x0 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_4, (0x0 << 26) | (0x0 << 20) |
        (0x8 << 14) | (0x10 << 8) | (0x15 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_5, (0x1B << 26) | (0x20 << 20) |
        (0x20 << 14) | (0x20 << 8) | (0x20 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_6, (0x20 << 26) | (0x1B << 20) |
        (0x15 << 14) | (0xC << 8) | (0x0 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_7, (0x0 << 26) | (0x0 << 20) |
        (0x0 << 14) | (0x5 << 8) | (0x16 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_8, (0x20 << 26) | (0x20 << 20) |
        (0x20 << 14) | (0x20 << 8) | (0x20 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_9, (0x0 << 26) | (0x3 << 20) |
        (0x7 << 14) | (0xE << 8) | (0x14 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_10, (0x1A << 26) | (0x1E << 20) |
        (0x20 << 14) | (0x10 << 8) | (0x10 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_11, (0x6 << 26) | (0x3 << 20) |
        (0x2 << 14) | (0x1 << 8) | (0x1 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_12, (0x1 << 26) | (0x0 << 20) |
        (0x1 << 14) | (0x3 << 8) | (0x9 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_13, (0xE << 26) | (0x10 << 20) |
        (0x10 << 14) | (0x10 << 8));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_14, (0x3F << 26) | (0x38 << 20) |
        (0x30 << 14) | (0x28 << 8) | (0x20 << 2));
    ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_MDP_TNR_CURVE_15, (0x18 << 26) | (0x10 << 20) |
        (0x8 << 14) | (0x0 << 8));
#endif
}

MBOOL
ISP_MGR_NR3D_T::
post_apply(MUINT8 SubModuleIndex, MBOOL bEnable, void* pReg)
{
#if 0
    dip_x_reg_t *pIspPhyReg = (dip_x_reg_t*)pReg;

    MBOOL       bEnableSL2E = MTRUE; // enable sl2e
    MBOOL       bEnableDemo = MFALSE; // enable demo mode
    MBOOL       bEnableBM = MFALSE; // enable benchmark mode
    MBOOL       bEnable3dnrLog = MFALSE; // enable 3dnr log
    MBOOL       bfixTuning = MFALSE;
    MBOOL       bForceDisableR2C = MFALSE;

    if (::property_get_int32("debug.camera.3dnr.level", 0))
    {
        bEnableSL2E = ::property_get_int32("debug.3dnr.sl2e.enable", 1); // sl2e: default on
        bEnableDemo = ::property_get_int32("debug.3dnr.demo.enable", 0);
        bEnableBM = ::property_get_int32("debug.nr3d.bm.enable", 0);
        bEnable3dnrLog = ::property_get_int32("camera.3dnr.log.level", 0);
        bfixTuning = ::property_get_int32("debug.3dnr.fix.tuning", 0);
        bForceDisableR2C = ::property_get_int32("debug.3dnr.disable.r2c", 0);

        CAM_LOGD_IF(bEnable3dnrLog, "bEnableSL2E(%d), bEnableDemo(%d), bEnableBM(%d), bEnable3dnrLog(%d)",
            bEnableSL2E, bEnableDemo, bEnableBM, bEnable3dnrLog);
    }

    if (pIspPhyReg == NULL)
    {
        CAM_LOGD("post_apply pReg NULL");
        return  MTRUE;
    }

    if (bEnable)
    {
        // turn on top register
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CTL_YUV_EN, NR3D_EN, bEnable);

        // turn on NR3D by NR3D_CAM_TNR_EN
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CAM_TNR_ENG_CON, NR3D_CAM_TNR_EN, bEnable);
        // NR3D_TNR_Y_EN must equal NR3D_CAM_TNR_EN
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_MDP_TNR_TNR_ENABLE, NR3D_TNR_Y_EN, bEnable);
        // nr3d default
        ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_NR3D_ON_CON, 0x00100F00);
        ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_NR3D_ON_OFF, 0x00000000);
        ISP_WRITE_ENABLE_REG(pIspPhyReg, DIP_X_NR3D_ON_SIZ, 0x00000000);

        //-------------------using default tuning value----------------------------------------
        if (bfixTuning)
        {
            fixTuningValue(pIspPhyReg);
        }

        //------------------------------------------------------------
        MRect onRegion = m_onRegion;

        if (bEnableDemo)
        {
            demoNR3D(bEnableSL2E, m_fullImg, onRegion);
        }

        // nr3d_on
        // OFST is relative position to IMGI full image
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_OFF, NR3D_ON_OFST_X, onRegion.p.x + m_fullImg.p.x);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_OFF, NR3D_ON_OFST_Y, onRegion.p.y + m_fullImg.p.y);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_SIZ, NR3D_ON_WD, onRegion.s.w);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_SIZ, NR3D_ON_HT, onRegion.s.h);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_CON, NR3D_ON_EN, bEnable);

        // nr3d_vipi
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_VIPI_OFFSET, NR3D_VIPI_OFFSET, m_vipiOffst);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_VIPI_SIZE, NR3D_VIPI_WIDTH, m_vipiReadSize.w);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_VIPI_SIZE, NR3D_VIPI_HEIGHT, m_vipiReadSize.h);

        MBOOL sl2e_enable = bEnableSL2E;

        if (bEnableSL2E)
        {
            if ((pIspPhyReg->DIP_X_SL2E_RZ.Bits.SL2_HRZ_COMP == 0 || pIspPhyReg->DIP_X_SL2E_RZ.Bits.SL2_VRZ_COMP == 0))
            {
                CAM_LOGD("force disable sl2e! DIP_X_SL2E_RZ = 0");
                sl2e_enable = MFALSE;
            }
        }

        // sl2e
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CTL_YUV_EN, SL2E_EN, sl2e_enable);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_CON, NR3D_SL2_OFF, !sl2e_enable);

        // debug R2C
        if (bForceDisableR2C)
        {
            ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_MDP_TNR_R2C_1, NR3D_R2CENC, 0);
        }

        CAM_LOGD("NR3D_EN(%d), SL2E_EN(%d), x,y,w,h(%d,%d,%d,%d), full x,y,w,h(%d,%d,%d,%d),"
            "vipi offst(%d) w,h(%d,%d), R2C en(%d) cnt(%d,%d,%d,%d)",
            bEnable, sl2e_enable, onRegion.p.x, onRegion.p.y, onRegion.s.w, onRegion.s.h,
            m_fullImg.p.x, m_fullImg.p.y, m_fullImg.s.w, m_fullImg.s.h,
            m_vipiOffst, m_vipiReadSize.w, m_vipiReadSize.h, pIspPhyReg->DIP_X_MDP_TNR_R2C_1.Bits.NR3D_R2CENC,
            pIspPhyReg->DIP_X_MDP_TNR_R2C_3.Bits.NR3D_R2CF_CNT1, pIspPhyReg->DIP_X_MDP_TNR_R2C_3.Bits.NR3D_R2CF_CNT2,
            pIspPhyReg->DIP_X_MDP_TNR_R2C_3.Bits.NR3D_R2CF_CNT3, pIspPhyReg->DIP_X_MDP_TNR_R2C_3.Bits.NR3D_R2CF_CNT4);

        if (bEnableBM)
        {
            benchmarkNR3DRegValue((VOID*)pIspPhyReg);
        }

    }
    else
    {
        CAM_LOGD("turn off NR3D_EN, NR3D_ON_EN, SL2E_EN, NR3D_SL2_OFF");
        // don't turn off NR3D_EN, isColorEnable may need it
        //ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CTL_YUV_EN, NR3D_EN, bEnable);

        // turn off NR3D by NR3D_CAM_TNR_EN
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CAM_TNR_ENG_CON, NR3D_CAM_TNR_EN, bEnable);
        // NR3D_TNR_Y_EN must equal NR3D_CAM_TNR_EN
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_MDP_TNR_TNR_ENABLE, NR3D_TNR_Y_EN, bEnable);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_CON, NR3D_ON_EN, bEnable);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_CTL_YUV_EN, SL2E_EN, bEnable);
        ISP_WRITE_ENABLE_BITS(pIspPhyReg, DIP_X_NR3D_ON_CON, NR3D_SL2_OFF, !bEnable);
    }
#endif
    return  MTRUE;

}

}
