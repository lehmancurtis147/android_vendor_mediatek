################################################################################
ifneq (,$(filter $(TARGET_BOARD_PLATFORM), mt6757p mt6799 mt6799p mt6763 mt6771 mt6765 mt6739 mt6761))

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#-----------------------------------------------------------
LOCAL_SRC_FILES += jpegtool.cpp

LOCAL_SHARED_LIBRARIES += \
    liblog \
    libcutils \
    libutils
#-----------------------------------------------------------
#LOCAL_SHARED_LIBRARIES += libjpeg
#LOCAL_C_INCLUDES += external/jpeg

LOCAL_SHARED_LIBRARIES += libjpeg-alpha_vendor
LOCAL_C_INCLUDES += $(TOP)/$(MTK_PATH_SOURCE)/external/libjpeg-alpha/include
#-----------------------------------------------------------
LOCAL_PRELINK_MODULE := false
LOCAL_MODULE_TAGS := eng
LOCAL_MODULE := jpegtool
LOCAL_MODULE_OWNER := mtk
LOCAL_PROPRIETARY_MODULE := true
#-----------------------------------------------------------
# End of common part ---------------------------------------

include $(MTK_EXECUTABLE)

endif
################################################################################
