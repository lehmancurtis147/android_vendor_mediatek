/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/

#define LOG_TAG "FlickerHal"
#define MTK_LOG_ENABLE 1

/***********************************************************
 * Headers
 **********************************************************/
/* standard headers */
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <utils/threads.h>
#include <cutils/properties.h>
#include <cutils/atomic.h>

/* camera headers */
#include <mtkcam/def/common.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>
#include <private/IopipeUtils.h>

/* custom headers */
#include <aaa_error_code.h>

/* aaa headers */
#include <nvram_drv.h>

#if (CAM3_3A_ISP_30_EN || CAM3_3A_ISP_40_EN || CAM3_3A_ISP_50_EN)
#include <isp_mgr.h>
#include <isp_mgr_flk.h>
#endif
#include <ae_mgr_if.h>

/* aaa common headers */
#include "file_utils.h"
#include "property_utils.h"
#include "time_utils.h"

/* flicker headers */
#include "flicker_hal.h"
#include "flicker_utils.h"

#include <mtkcam/utils/std/Trace.h>

using namespace NS3Av3;
#if (CAM3_3A_ISP_30_EN || CAM3_3A_ISP_40_EN || CAM3_3A_ISP_50_EN)
using namespace NSIspTuningv3;
#endif

/***********************************************************
 * Define macros
 **********************************************************/
#define FLICKER_MAX_LENG (6000)
#define MHAL_FLICKER_WORKING_BUF_SIZE (FLICKER_MAX_LENG * 4 * 3)

#define PROP_FLK_DEF_HZ         "vendor.debug.flk_def_hz" /* (50|60) */
#define PROP_FLK_READ_FILE      "vendor.debug.flk_read_file"
#define PROP_FLK_RATIO          "vendor.debug.flk_ratio"
#define PROP_FLK_CLK            "vendor.debug.flk_clk"
#define PROP_FLK_PIXEL          "vendor.debug.flk_pixel"
#define RPOP_FLK_H              "vendor.debug.flk_h"
#define RPOP_FLK_W              "vendor.debug.flk_w"
#define PROP_FLK_DUMP           "vendor.debug.flk_dump"
#define PROP_FLK_DUMP_PURE      "vendor.debug.flk_dump_pure"
#define PROP_FLK_SWITCH_EN      "vendor.debug.flk_switch_en"
#define PROP_FLK_NOT_ATTACH     "vendor.debug.flk_not_attach"
#define PROP_FLK_DISABLE        "vendor.debug.aaa_flk.disable"

#define FLK_PARA_PREVIEW_FILE     "/sdcard/flk_para_preview.txt"
#define FLK_PARA_VIDEO_FILE       "/sdcard/flk_para_video.txt"
#define FLK_PARA_CAPTURE_FILE     "/sdcard/flk_para_capture.txt"
#define FLK_PARA_SLIM_VIDEO1_FILE "/sdcard/flk_para_slim_video1.txt"
#define FLK_PARA_SLIM_VIDEO2_FILE "/sdcard/flk_para_slim_video2.txt"
#define FLK_PARA_CUSTOM1_FILE     "/sdcard/flk_para_custom1.txt"
#define FLK_PARA_CUSTOM2_FILE     "/sdcard/flk_para_custom2.txt"
#define FLK_PARA_CUSTOM3_FILE     "/sdcard/flk_para_custom3.txt"
#define FLK_PARA_CUSTOM4_FILE     "/sdcard/flk_para_custom4.txt"
#define FLK_PARA_CUSTOM5_FILE     "/sdcard/flk_para_custom5.txt"

#define FLK_DUMP_COUNT_FILE "/data/vendor/flicker/flko/flicker_file_cnt.txt"

/***********************************************************
 * Flicker HAL
 **********************************************************/
int FlickerHal::m_flickerState = HAL_FLICKER_AUTO_OFF;
int FlickerHal::m_attachCount = 0;
Mutex FlickerHal::m_attachLock;

FlickerHal::FlickerHal(int sensorDev)
    : mUsers(0)
    , m_sensorDev(sensorDev)
    , m_sensorId(0)
    , m_isAttach(0)
    , mTgInfo(CAM_TG_NONE)
    , m_u4TgWReal(0)
    , m_u4TgHReal(0)
    , mSensorMode(SENSOR_SCENARIO_ID_NORMAL_PREVIEW)
    , m_u4TgW(0)
    , m_u4TgH(0)
    , m_imgW(0)
    , m_imgH(0)
    , m_winW(0)
    , m_winH(0)
    , m_u4SensorPixelClkFreq(0)
    , m_u4PixelsInLine(0)
    , m_pVectorData1(NULL)
    , m_pVectorData2(NULL)
    , m_flickerMode(MTK_CONTROL_AE_ANTIBANDING_MODE_AUTO)
    , m_maxDetExpUs(70000)
    , m_currentFrame(0)
    , m_currentCycle(0)
    , m_alreadyGetFlickerPara(0)
    , m_p3ASttCtrl(NULL)
{
    m_flkParaArray=(FLICKER_CUST_PARA**)calloc(FLK_MAX_MODE_NUM, sizeof(FLICKER_CUST_PARA*));
    for(int i=0;i<FLK_MAX_MODE_NUM;i++)
        m_flkParaArray[i]=(FLICKER_CUST_PARA*)calloc(FLK_MAX_BIN_NUM, sizeof(FLICKER_CUST_PARA));
    //memset(&m_flkParaArray, 0, FLK_MAX_MODE_NUM*FLK_MAX_BIN_NUM*sizeof(FLICKER_CUST_PARA));

    logD("FlickerHal(): sensorDev(%d).", sensorDev);

    m_maxAttachNum = cust_getMaxAttachNum();
    m_detectCycle = cust_getFlickerDetectFrequency();
    if (m_detectCycle < 2)
        m_detectCycle = 2;
}

FlickerHal::~FlickerHal()
{
    for(int i=0;i<FLK_MAX_MODE_NUM;i++)
    {
        if (m_flkParaArray[i])
        {
            free(m_flkParaArray[i]);
        }
    }
    if (m_flkParaArray)
    {
        free(m_flkParaArray);
    }
}

FlickerHal &FlickerHal::getInstance(int sensorDev)
{
    static FlickerHal singletonMain(ESensorDev_Main);
    static FlickerHal singletonSub(ESensorDev_Sub);
    static FlickerHal singletonMain2(ESensorDev_MainSecond);
    static FlickerHal singletonSub2(ESensorDev_SubSecond);
    static FlickerHal singletonMain3(ESensorDev_MainThird);
    if (sensorDev == ESensorDev_Main)
        return singletonMain;
    else if (sensorDev == ESensorDev_Sub)
        return singletonSub;
    else if (sensorDev == ESensorDev_MainSecond)
        return singletonMain2;
    else if (sensorDev == ESensorDev_SubSecond)
        return singletonSub2;
    else /* if (sensorDev == ESensorDev_MainThird) */
        return singletonMain3;
}

/***********************************************************
 * Flicker life cycle
 **********************************************************/
MRESULT FlickerHal::init(MINT32 const i4SensorIdx)
{
    Mutex::Autolock lock(m_lock);
    logI("init().");

    /* set debug */
    setDebug();

    m_sensorId = i4SensorIdx;
    createBuf();
    if (m_alreadyGetFlickerPara == 0)
    {
        logI("init(). getFlickerParametersAll");
        getFlickerParametersAll(FLK_MAX_MODE_NUM, FLK_MAX_BIN_NUM);
        m_alreadyGetFlickerPara = 1;
    }

    return S_3A_OK;
}

MRESULT FlickerHal::uninit()
{
    Mutex::Autolock lock(m_lock);
    logI("uninit().");
    releaseBuf();
    return S_3A_OK;
}

MBOOL FlickerHal::config()
{
    Mutex::Autolock lock(m_lock);
    setWindowInfo(&m_imgW, &m_imgH, &m_winW, &m_winH);
    return MTRUE;
}


MBOOL FlickerHal::reconfig(MVOID *pDBinInfo __unused, MVOID *pOutRegCfg __unused)
{
/*
    if(pDBinInfo == NULL || pOutRegCfg == NULL) {
            CAM_LOGE("[%s] : pDBinInfo is NULL or pOutRegCfg is NULL", __FUNCTION__);
            return MFALSE;
    }

    Mutex::Autolock lock(m_lock);
    setWindowInfoReconfig(&m_imgW, &m_imgH, &m_winW, &m_winH, pDBinInfo, pOutRegCfg);
*/
    return MTRUE;
}

#if ((!CAM3_3A_ISP_30_EN && CAM3_3A_IP_BASE) && (!CAM3_3A_ISP_40_EN && CAM3_3A_IP_BASE) && (!CAM3_3A_ISP_50_EN && CAM3_3A_IP_BASE))
MVOID FlickerHal::configReg(FLKResultConfig_T *pResultConfig)
{
    memcpy(pResultConfig, &m_sFLKResultConfig, sizeof(FLKResultConfig_T));
    logI("HW-%s configReg Enable(%d)", __FUNCTION__, m_sFLKResultConfig.enableFLKHw);
}
#endif


MBOOL FlickerHal::start(FLK_ATTACH_PRIO_T prio)
{
    Mutex::Autolock lock(m_lock);

    logI("start(): begin. m_sensorDev(%d)", m_sensorDev);
    if (attach(prio))
    {
        logI("start(): failed to attach. m_sensorDev(%d)", m_sensorDev);
        return MFALSE;
    }

    m_p3ASttCtrl = Hal3ASttCtrl::getInstance(m_sensorDev);
    int type = m_p3ASttCtrl->isMvHDREnable();
    int sensorType;

#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    if(type == FEATURE_MVHDR_SUPPORT_3EXPO_VIRTUAL_CHANNEL)
    {
        sensorType = TYPE_3EXPO;
    }
    else
    {
        sensorType = TYPE_NORMAL;
    }
#else
    sensorType = 0;
#endif
    logD("start(): sensorType(%d). m_sensorDev(%d)", sensorType, m_sensorDev);

    if (init_algo(sensorType)) {
        logE("config(): failed to init. m_sensorDev(%d)", m_sensorDev);
        return MFALSE;
    }

    /*
     * Reset framerate active because of that:
     * 1. Sensor driver will set default 'off' when chaning sensor mode.
     * 2. setFlickerFrameRateActive() only update driver when settings changed
     */
    if (m_flickerMode == MTK_CONTROL_AE_ANTIBANDING_MODE_AUTO) {
        IAeMgr::getInstance().setFlickerFrameRateActive(m_sensorDev, 0);
        IAeMgr::getInstance().setFlickerFrameRateActive(m_sensorDev, 1);
    }

    m_currentFrame = 0;
    m_currentCycle = 0;

    logD("start(): end. m_sensorDev(%d)", m_sensorDev);

    return MTRUE;
}

MBOOL FlickerHal::stop()
{
    Mutex::Autolock lock(m_lock);

    logI("stop(): begin. m_sensorDev(%d)", m_sensorDev);

    if (detach()) {
        logI("stop(): failed to detach.  m_sensorDev(%d)", m_sensorDev);
        return MFALSE;
    }

    uninit_algo();

    logD("stop(): end. m_sensorDev(%d)", m_sensorDev);

    return MTRUE;
}

/***********************************************************
 * Flicker life cycle - init
 **********************************************************/
MINT32 FlickerHal::createBuf()
{
    logD("createBuf().");

    if (m_pVectorData1 || m_pVectorData2)
        logE("createBuf(): buffer is not empty.");

    m_pVectorData1 = (MINT32 *)malloc(MHAL_FLICKER_WORKING_BUF_SIZE);
    if (!m_pVectorData1) {
        logE("createBuf(): falied to allocate buffer 1.");
        return -1;
    }
    m_pVectorData2 = (MINT32 *)malloc(MHAL_FLICKER_WORKING_BUF_SIZE);
    if (!m_pVectorData2) {
        logE("createBuf(): falied to allocate buffer 2.");
        free(m_pVectorData1);
        return -1;
    }

    logI("createBuf(). Create buf success.");

    return 0;
}

MVOID FlickerHal::releaseBuf()
{
    logD("releaseBuf().");

    if (m_pVectorData1) {
        free(m_pVectorData1);
        m_pVectorData1 = NULL;
    }
    if (m_pVectorData2) {
        free(m_pVectorData2);
        m_pVectorData2 = NULL;
    }

    logI("releaseBuf(). Release buf success.");
}

/***********************************************************
 * Flicker life cycle - config
 **********************************************************/
MBOOL FlickerHal::getInfo()
{
    logD("getInfo(): m_sensorDev(%d)", m_sensorDev);

    MBOOL flkInfo = MFALSE;
    int disableFlk;

    getPropInt(PROP_FLK_DISABLE, &disableFlk, 0);
    if (disableFlk)
    {
        logI("getInfo(): Flicker disable");
        return MFALSE;
    }

#if (!CAM3_3A_ISP_30_EN)
    /* get flicker info from normal pipe */
    NSCam::NSIoPipe::NSCamIOPipe::INormalPipe* pCamIO =
        (NSCam::NSIoPipe::NSCamIOPipe::INormalPipe *)
        INormalPipeUtils::get()->createDefaultNormalPipe(m_sensorId, LOG_TAG);
    if (!pCamIO) {
        logE("getInfo(): failed to create normal pipe.");
        return MFALSE;
    }

    pCamIO->sendCommand(
            NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_FLK_INFO,
            (MINTPTR)&flkInfo, 0, 0);
    pCamIO->destroyInstance(LOG_TAG);
#else
    if (mTgInfo == ESensorTG_1) {
        flkInfo = MTRUE;
    }
#endif

    logD("getInfo(): m_sensorDev(%d), flkInfo(%d)", m_sensorDev, flkInfo);
    return flkInfo;
}

int FlickerHal::setTGInfo(int tgInfo, int width, int height)
{
    mTgInfo = tgInfo;
    m_u4TgWReal = width;
    m_u4TgHReal = height;
    return 0;
}

int FlickerHal::setSensorMode(int sensorMode, int width, int height)
{
    mSensorMode = sensorMode;
    m_u4TgW = width;
    m_u4TgH = height;
    return 0;
}

MINT32 FlickerHal::setWindowInfo(
        MUINT32 *imgW, MUINT32 *imgH,
        MUINT32 *winW, MUINT32 *winH)
{
    logD("setWindowInfo(): begin");
    FLKWinCFG_T flkWinCfg;
    prepareFlickerCfg(m_u4TgW, m_u4TgH, &flkWinCfg);
#if (CAM3_3A_ISP_30_EN)
    if (mTgInfo != ESensorTG_1) {
        return 0;
    }
    ISP_MGR_FLK_CONFIG_T::getInstance(
            static_cast<ESensorDev_T>(m_sensorDev)).config(m_sensorId, flkWinCfg);
#elif (CAM3_3A_ISP_40_EN)
    ISP_MGR_FLK_CONFIG_T::getInstance(
            static_cast<ESensorDev_T>(m_sensorDev)).config(flkWinCfg, mTgInfo);
#elif (CAM3_3A_ISP_50_EN)
    ISP_MGR_FLK_CONFIG_T::getInstance(
            static_cast<ESensorDev_T>(m_sensorDev)).config(flkWinCfg);
#else
    FLKConfig(&flkWinCfg, (MVOID *)&m_sFLKResultConfig);
#endif
    logI("setWindowInfo(): sensorDev(%d) sensorId(%d) tg(%d) tgImg(%d,%d) configImg(%d,%d) offset(%d,%d) win(%d,%d) dma(%d)",
            m_sensorDev, m_sensorId, mTgInfo, m_u4TgW, m_u4TgH,
            flkWinCfg.m_uImageW, flkWinCfg.m_uImageH,
            flkWinCfg.m_u4OffsetX, flkWinCfg.m_u4OffsetY,
            flkWinCfg.m_u4SizeX, flkWinCfg.m_u4SizeY, flkWinCfg.m_u4DmaSize);

    /* update size */
    *imgW = m_u4TgW - 3;
    *imgH = m_u4TgH - 25;
    *winW = flkWinCfg.m_u4SizeX;
    *winH = flkWinCfg.m_u4SizeY;

    return 0;
}


/*
MINT32 FlickerHal::setWindowInfoReconfig(
        MUINT32 *imgW, MUINT32 *imgH,
        MUINT32 *winW, MUINT32 *winH,
        MVOID *pDBinInfo, MVOID *pOutRegCfg)
{
// FIX-ME, for IP-Base build pass +
//#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
// FIX-ME, for IP-Base build pass -
    BIN_INPUT_INFO *psDBinInfo = static_cast<BIN_INPUT_INFO*>(pDBinInfo);

    logI("setWindowInfoReconfig(): sensorDev(%d), sensorId(%d), tg(%d), tg img(%d,%d).",
            m_sensorDev, m_sensorId, mTgInfo, psDBinInfo->TarQBNOut_W, psDBinInfo->TarBinOut_H);

    FLKWinCFG_T flkWinCfg;
    prepareFlickerCfg(psDBinInfo->TarQBNOut_W, psDBinInfo->TarBinOut_H, &flkWinCfg);

    ISP_MGR_FLK_CONFIG_T::getInstance(
            static_cast<ESensorDev_T>(m_sensorDev)).reconfig(flkWinCfg, pOutRegCfg);

    logI("setWindowInfoReconfig(): config img(%d,%d), offset(%d,%d), win(%d,%d), dma(%d).",
            flkWinCfg.m_uImageW, flkWinCfg.m_uImageH,
            flkWinCfg.m_u4OffsetX, flkWinCfg.m_u4OffsetY,
            flkWinCfg.m_u4SizeX, flkWinCfg.m_u4SizeY, flkWinCfg.m_u4DmaSize);

    // update size
    *imgW = psDBinInfo->TarQBNOut_W - 3;
    *imgH = psDBinInfo->TarBinOut_H - 25;
    *winW = flkWinCfg.m_u4SizeX;
    *winH = flkWinCfg.m_u4SizeY;
//#endif
    return 0;
}
*/

/***********************************************************
 * Flicker life cycle - start
 **********************************************************/
int FlickerHal::isAttach()
{
    Mutex::Autolock lock(m_attachLock);

    int notAttach;
    getPropInt(PROP_FLK_NOT_ATTACH, &notAttach, 0);
    if (notAttach)
        return 0;

    return m_isAttach;
}

int FlickerHal::attach(FLK_ATTACH_PRIO_T prio)
{
    Mutex::Autolock lock(m_attachLock);

    if (!getInfo()) {
        logI("attach(): no flicker. m_sensorDev(%d)", m_sensorDev);
        return -1;
    }

    if (prio == FLK_ATTACH_PRIO_LOW) {
        logI("attach(): No attach due to low priority sensor. m_sensorDev(%d)", m_sensorDev);
        return -1;
    } else { /* FLK_ATTACH_PRIO_MEDIUM or FLK_ATTACH_PRIO_HIGH */
        if (m_attachCount >= m_maxAttachNum) {
            logI("attach(): achieve max attach number(%d). m_sensorDev(%d)", m_maxAttachNum, m_sensorDev);
            return -1;
        }

        m_attachCount++;
        m_isAttach = 1;
    }

    logI("attach(): isAttach(%d), count(%d/%d). m_sensorDev(%d)",
            m_isAttach, m_attachCount, m_maxAttachNum, m_sensorDev);

    return 0;
}

int FlickerHal::detach()
{
    Mutex::Autolock lock(m_attachLock);

    if (!m_isAttach) {
        logI("detach(): not attach before. m_sensorDev(%d)", m_sensorDev);
        return -1;
    }

    m_attachCount--;
    m_isAttach = 0;
    logI("detach(): count(%d/%d). m_sensorDev(%d)", m_attachCount, m_maxAttachNum, m_sensorDev);

    return 0;
}

static inline int fileGetFlickerPara(FILE *fp)
{
    if (!fp) {
        logE("fileGetFlickerPara(): invalid fp.");
        return -1;
    }

    int para;
    if (fscanf(fp, "%d", &para) > 0) {
        logI("fileGetFlickerPara(): %d.", para);
        return para;
    } else {
        logE("fileGetFlickerPara(): failed to get para.");
        return -1;
    }
}
static int getFlickerParametersFile(
        int sensorMode, FLICKER_EXT_PARA *flickerExtPara) __attribute__((unused));
static int getFlickerParametersFile(
        int sensorMode, FLICKER_EXT_PARA *flickerExtPara)
{
    logI("getFlickerParametersFile(): sensorMode(%d).", sensorMode);

    const char *paraFile;
    if (sensorMode == e_sensorModePreview)
        paraFile = FLK_PARA_PREVIEW_FILE;
    else if (sensorMode == e_sensorModeVideo)
        paraFile = FLK_PARA_VIDEO_FILE;
    else if (sensorMode == e_sensorModeCapture)
        paraFile = FLK_PARA_CAPTURE_FILE;
    else if (sensorMode == e_sensorModeVideo1)
        paraFile = FLK_PARA_SLIM_VIDEO1_FILE;
    else if (sensorMode == e_sensorModeVideo2)
        paraFile = FLK_PARA_SLIM_VIDEO2_FILE;
    else if (sensorMode == e_sensorModeCustom1)
        paraFile = FLK_PARA_CUSTOM1_FILE;
    else if (sensorMode == e_sensorModeCustom2)
        paraFile = FLK_PARA_CUSTOM2_FILE;
    else if (sensorMode == e_sensorModeCustom3)
        paraFile = FLK_PARA_CUSTOM3_FILE;
    else if (sensorMode == e_sensorModeCustom4)
        paraFile = FLK_PARA_CUSTOM4_FILE;
    else if (sensorMode == e_sensorModeCustom5)
        paraFile = FLK_PARA_CUSTOM5_FILE;
    else {
        logE("getFlickerParametersFile(): invalid sensor mode.");
        return -1;
    }

    FILE *fp = fopen(paraFile, "r");;
    if (!fp) {
        logE("getFlickerParametersFile(): failed to open para file.");
        return -1;
    }

    flickerExtPara->flickerFreq[0]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[1]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[2]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[3]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[4]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[5]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[6]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[7]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerFreq[8]        = fileGetFlickerPara(fp);
    flickerExtPara->flickerGradThreshold  = fileGetFlickerPara(fp);
    flickerExtPara->flickerSearchRange    = fileGetFlickerPara(fp);
    flickerExtPara->minPastFrames         = fileGetFlickerPara(fp);
    flickerExtPara->maxPastFrames         = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L50.m            = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L50.b_l          = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L50.b_r          = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L50.offset       = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L60.m            = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L60.b_l          = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L60.b_r          = fileGetFlickerPara(fp);
    flickerExtPara->EV50_L60.offset       = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L50.m            = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L50.b_l          = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L50.b_r          = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L50.offset       = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L60.m            = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L60.b_l          = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L60.b_r          = fileGetFlickerPara(fp);
    flickerExtPara->EV60_L60.offset       = fileGetFlickerPara(fp);
    flickerExtPara->EV50_thresholds[0]    = fileGetFlickerPara(fp);
    flickerExtPara->EV50_thresholds[1]    = fileGetFlickerPara(fp);
    flickerExtPara->EV60_thresholds[0]    = fileGetFlickerPara(fp);
    flickerExtPara->EV60_thresholds[1]    = fileGetFlickerPara(fp);
    flickerExtPara->freq_feature_index[0] = fileGetFlickerPara(fp);
    flickerExtPara->freq_feature_index[1] = fileGetFlickerPara(fp);
    fclose(fp);

    return 0;
}

int FlickerHal::getFlickerParametersAll(
        int sensorModeTotal, int binRatioTotal)
{
    /* acquire sensor list */
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();

    /* get sensor id */
    int sensorDevId;
    SensorStaticInfo rSensorStaticInfo;
    rSensorStaticInfo.sensorDevID = 0;
    switch (m_sensorDev) {
    case ESensorDev_Main:
        pIHalSensorList->querySensorStaticInfo(NSCam::SENSOR_DEV_MAIN, &rSensorStaticInfo);
        break;
    case ESensorDev_Sub:
        pIHalSensorList->querySensorStaticInfo(NSCam::SENSOR_DEV_SUB, &rSensorStaticInfo);
        break;
    case ESensorDev_MainSecond:
        pIHalSensorList->querySensorStaticInfo(NSCam::SENSOR_DEV_MAIN_2, &rSensorStaticInfo);
        break;
    case ESensorDev_SubSecond:
        pIHalSensorList->querySensorStaticInfo(NSCam::SENSOR_DEV_SUB_2, &rSensorStaticInfo);
        break;
    case ESensorDev_MainThird:
        pIHalSensorList->querySensorStaticInfo(NSCam::SENSOR_DEV_MAIN_3, &rSensorStaticInfo);
        break;
    default: /* shoult NOT happen */
        logE("getFlickerParametersAll(): invalid sensorDev(%d).", m_sensorDev);
        return MFALSE;
    }
    sensorDevId = rSensorStaticInfo.sensorDevID;

    logI("getFlickerParametersAll(): sensorDevId(%d), m_sensorDev(%d), sensorMode total(%d), binRatio total(%d).",
            sensorDevId, m_sensorDev, sensorModeTotal, binRatioTotal);

    int err = nvGetFlickerParaAll(
            sensorDevId, m_sensorDev, sensorModeTotal, binRatioTotal, &m_flkParaArray);

    // Debug use
    int i=0, j=0;
    for (i=0; i<FLK_MAX_MODE_NUM; i++)
    {
        for (j=0; j<FLK_MAX_BIN_NUM; j++)
        {
            logD("m_flkParaArray[%d][%d].flickerGradThreshold = %d", i, j, m_flkParaArray[i][j].flickerGradThreshold);
            logD("m_flkParaArray[%d][%d].flickerSearchRange = %d", i, j, m_flkParaArray[i][j].flickerSearchRange);
        }

    }

    if (err)
    {
        logE("getFlickerParametersAll(): nvGetFlickerParaAll fail.");
        return MFALSE;
    }

    /* note that, sdcard could be removed */
//    int propFlkReadFile;
//    getPropInt(PROP_FLK_READ_FILE, &propFlkReadFile, 0);
//    if (propFlkReadFile)
//        ret = getFlickerParametersFile(sensorMode, flickerExtPara);

    return MTRUE;
}

MVOID FlickerHal::getTargetParameter(int sensorMode, int ratio, FLICKER_EXT_PARA *para)
{
    logD("getTargetParameter(): sensorMode(%d), ratio(%d)", sensorMode, ratio);

    int ratioIndex = ratio - 1;
    para->flickerFreq[0]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[0];
    para->flickerFreq[1]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[1];
    para->flickerFreq[2]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[2];
    para->flickerFreq[3]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[3];
    para->flickerFreq[4]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[4];
    para->flickerFreq[5]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[5];
    para->flickerFreq[6]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[6];
    para->flickerFreq[7]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[7];
    para->flickerFreq[8]        = m_flkParaArray[sensorMode][ratioIndex].flickerFreq[8];
    para->flickerGradThreshold  = m_flkParaArray[sensorMode][ratioIndex].flickerGradThreshold;
    para->flickerSearchRange    = m_flkParaArray[sensorMode][ratioIndex].flickerSearchRange;
    para->minPastFrames         = m_flkParaArray[sensorMode][ratioIndex].minPastFrames;
    para->maxPastFrames         = m_flkParaArray[sensorMode][ratioIndex].maxPastFrames;
    para->EV50_L50.m            = m_flkParaArray[sensorMode][ratioIndex].EV50_L50.m;
    para->EV50_L50.b_l          = m_flkParaArray[sensorMode][ratioIndex].EV50_L50.b_l;
    para->EV50_L50.b_r          = m_flkParaArray[sensorMode][ratioIndex].EV50_L50.b_r;
    para->EV50_L50.offset       = m_flkParaArray[sensorMode][ratioIndex].EV50_L50.offset;
    para->EV50_L60.m            = m_flkParaArray[sensorMode][ratioIndex].EV50_L60.m;
    para->EV50_L60.b_l          = m_flkParaArray[sensorMode][ratioIndex].EV50_L60.b_l;
    para->EV50_L60.b_r          = m_flkParaArray[sensorMode][ratioIndex].EV50_L60.b_r;
    para->EV50_L60.offset       = m_flkParaArray[sensorMode][ratioIndex].EV50_L60.offset;
    para->EV60_L50.m            = m_flkParaArray[sensorMode][ratioIndex].EV60_L50.m;
    para->EV60_L50.b_l          = m_flkParaArray[sensorMode][ratioIndex].EV60_L50.b_l;
    para->EV60_L50.b_r          = m_flkParaArray[sensorMode][ratioIndex].EV60_L50.b_r;
    para->EV60_L50.offset       = m_flkParaArray[sensorMode][ratioIndex].EV60_L50.offset;
    para->EV60_L60.m            = m_flkParaArray[sensorMode][ratioIndex].EV60_L60.m;
    para->EV60_L60.b_l          = m_flkParaArray[sensorMode][ratioIndex].EV60_L60.b_l;
    para->EV60_L60.b_r          = m_flkParaArray[sensorMode][ratioIndex].EV60_L60.b_r;
    para->EV60_L60.offset       = m_flkParaArray[sensorMode][ratioIndex].EV60_L60.offset;
    para->EV50_thresholds[0]    = m_flkParaArray[sensorMode][ratioIndex].EV50_thresholds[0];
    para->EV50_thresholds[1]    = m_flkParaArray[sensorMode][ratioIndex].EV50_thresholds[1];
    para->EV60_thresholds[0]    = m_flkParaArray[sensorMode][ratioIndex].EV60_thresholds[0];
    para->EV60_thresholds[1]    = m_flkParaArray[sensorMode][ratioIndex].EV60_thresholds[1];
    para->freq_feature_index[0] = m_flkParaArray[sensorMode][ratioIndex].freq_feature_index[0];
    para->freq_feature_index[1] = m_flkParaArray[sensorMode][ratioIndex].freq_feature_index[1];

    logD("getTargetParameter(): para->flickerFreq[0] = %d", para->flickerFreq[0]);
    logD("getTargetParameter(): para->flickerFreq[1] = %d", para->flickerFreq[1]);
    logD("getTargetParameter(): para->flickerFreq[2] = %d", para->flickerFreq[2]);
    logD("getTargetParameter(): para->flickerFreq[3] = %d", para->flickerFreq[3]);
    logD("getTargetParameter(): para->flickerFreq[4] = %d", para->flickerFreq[4]);
    logD("getTargetParameter(): para->flickerFreq[5] = %d", para->flickerFreq[5]);
    logD("getTargetParameter(): para->flickerFreq[6] = %d", para->flickerFreq[6]);
    logD("getTargetParameter(): para->flickerFreq[7] = %d", para->flickerFreq[7]);
    logD("getTargetParameter(): para->flickerFreq[8] = %d", para->flickerFreq[8]);
    logD("getTargetParameter(): para->flickerGradThreshold = %d", para->flickerGradThreshold);
    logD("getTargetParameter(): para->flickerSearchRange = %d", para->flickerSearchRange);
    logD("getTargetParameter(): para->minPastFrames = %d", para->minPastFrames);
    logD("getTargetParameter(): para->maxPastFrames = %d", para->maxPastFrames);
    logD("getTargetParameter(): para->EV50_L50.m = %d", para->EV50_L50.m);
    logD("getTargetParameter(): para->EV50_L50.b_l = %d", para->EV50_L50.b_l);
    logD("getTargetParameter(): para->EV50_L50.b_r = %d", para->EV50_L50.b_r);
    logD("getTargetParameter(): para->EV50_L50.offset = %d", para->EV50_L50.offset);
    logD("getTargetParameter(): para->EV50_L60.m = %d", para->EV50_L60.m);
    logD("getTargetParameter(): para->EV50_L60.b_l = %d", para->EV50_L60.b_l);
    logD("getTargetParameter(): para->EV50_L60.b_r = %d", para->EV50_L60.b_r);
    logD("getTargetParameter(): para->EV50_L60.offset = %d", para->EV50_L60.offset);
    logD("getTargetParameter(): para->EV60_L50.m = %d", para->EV60_L50.m);
    logD("getTargetParameter(): para->EV60_L50.b_l = %d", para->EV60_L50.b_l);
    logD("getTargetParameter(): para->EV60_L50.b_r = %d", para->EV60_L50.b_r);
    logD("getTargetParameter(): para->EV60_L50.offset = %d", para->EV60_L50.offset);
    logD("getTargetParameter(): para->EV60_L60.m = %d", para->EV60_L60.m);
    logD("getTargetParameter(): para->EV60_L60.b_l = %d", para->EV60_L60.b_l);
    logD("getTargetParameter(): para->EV60_L60.b_r = %d", para->EV60_L60.b_r);
    logD("getTargetParameter(): para->EV60_L60.offset = %d", para->EV60_L60.offset);
    logD("getTargetParameter(): para->EV50_thresholds[0] = %d", para->EV50_thresholds[0]);
    logD("getTargetParameter(): para->EV50_thresholds[1] = %d", para->EV50_thresholds[1]);
    logD("getTargetParameter(): para->EV60_thresholds[0] = %d", para->EV60_thresholds[0]);
    logD("getTargetParameter(): para->EV60_thresholds[1] = %d", para->EV60_thresholds[1]);
    logD("getTargetParameter(): para->freq_feature_index[0] = %d", para->freq_feature_index[0]);
    logD("getTargetParameter(): para->freq_feature_index[1] = %d", para->freq_feature_index[1]);
}


MINT32 FlickerHal::init_algo(int sensorType)
{
    logD("init_algo(): begin. mUsers(%d), m_sensorDev(%d)", mUsers, m_sensorDev);

    /* get flicker init hz from custom */
    int initFreq;
    cust_getFlickerHalPara(&initFreq, &m_maxDetExpUs);

    /* get flicker init hz from the last result */
    if (m_flickerState)
        initFreq = m_flickerState;

    /* get flicker init hz from property */
    int propDefFlicker;
    getPropInt(PROP_FLK_DEF_HZ, &propDefFlicker, 0);
    if (propDefFlicker)
        initFreq = propDefFlicker;

    /* set flicker init hz  */
    logD("init_algo(): init flicker Hz(%d).", initFreq);
    setFlickerState(initFreq);

    if (mUsers > 0) {
        logE("init_algo(): mUsers(%d) had created. m_sensorDev(%d)", mUsers, m_sensorDev);
    }
    else
    {
        android_atomic_inc(&mUsers);
    }

    /* acquire sensor list */
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    IHalSensor* pHalSensorObj;
    pHalSensorObj = pIHalSensorList->createSensor("flicker", m_sensorId);
    if (!pHalSensorObj) {
        logE("init_algo(): failed to create instance for pHalSensorObj.");
        return -1;
    }

    /* get pixel clock */
    MINT32 err;
    err = pHalSensorObj->sendCommand(m_sensorDev,
            SENSOR_CMD_GET_PIXEL_CLOCK_FREQ,
            (MUINTPTR)&m_u4SensorPixelClkFreq, 0, 0);
    if (err)
        logE("init_algo(): failed to get pixel clock.");

    /* get line length */
    err = pHalSensorObj->sendCommand(m_sensorDev,
            SENSOR_CMD_GET_FRAME_SYNC_PIXEL_LINE_NUM,
            (MUINTPTR)&m_u4PixelsInLine, 0, 0);
    if (err)
        logE("init_algo(): failed to get line length.");
    m_u4PixelsInLine &= 0x0000FFFF;

    /* release sensor list */
    if (pHalSensorObj)
        pHalSensorObj->destroyInstance("flicker");

    logD("init_algo(): pixel clock(%d), line length(%d), grab height(%d).",
            (int)m_u4SensorPixelClkFreq,
            (int)m_u4PixelsInLine,
            (int)m_imgH);

   /* check binning */
    int ratio = m_u4TgHReal / m_u4TgH;
    getPropInt(PROP_FLK_RATIO, &ratio, ratio);
    if (ratio > 1) {
        m_u4PixelsInLine *= ratio;
        logD("init_algo(): update bin ratio(%d), line length(%u).", ratio, m_u4PixelsInLine);
    }

//    android_atomic_inc(&mUsers);

    /* get sensor mode */
    int sensorMode;
#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_PREVIEW)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModePreview_3HDR;
        else
            sensorMode = e_sensorModePreview;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_CAPTURE)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCapture_3HDR;
        else
            sensorMode = e_sensorModeCapture;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_VIDEO)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeVideoPreview_3HDR;
        else
            sensorMode = e_sensorModeVideoPreview;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_SLIM_VIDEO1)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeVideo1_3HDR;
        else
            sensorMode = e_sensorModeVideo1;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_SLIM_VIDEO2)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeVideo2_3HDR;
        else
            sensorMode = e_sensorModeVideo2;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM1)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCustom1_3HDR;
        else
            sensorMode = e_sensorModeCustom1;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM2)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCustom2_3HDR;
        else
            sensorMode = e_sensorModeCustom2;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM3)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCustom3_3HDR;
        else
            sensorMode = e_sensorModeCustom3;
    }
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM4)
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCustom4_3HDR;
        else
            sensorMode = e_sensorModeCustom4;
    }
    else /* if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM5) */
    {
        if (sensorType == TYPE_3EXPO)
            sensorMode = e_sensorModeCustom5_3HDR;
        else
            sensorMode = e_sensorModeCustom5;
    }
#else
    if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_PREVIEW)
        sensorMode = e_sensorModePreview;
    else if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_CAPTURE)
        sensorMode = e_sensorModeCapture;
    else if (mSensorMode == SENSOR_SCENARIO_ID_NORMAL_VIDEO)
        sensorMode = e_sensorModeVideoPreview;
    else if (mSensorMode == SENSOR_SCENARIO_ID_SLIM_VIDEO1)
        sensorMode = e_sensorModeVideo1;
    else if (mSensorMode == SENSOR_SCENARIO_ID_SLIM_VIDEO2)
        sensorMode = e_sensorModeVideo2;
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM1)
        sensorMode = e_sensorModeCustom1;
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM2)
        sensorMode = e_sensorModeCustom2;
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM3)
        sensorMode = e_sensorModeCustom3;
    else if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM4)
        sensorMode = e_sensorModeCustom4;
    else /* if (mSensorMode == SENSOR_SCENARIO_ID_CUSTOM5) */
        sensorMode = e_sensorModeCustom5;
#endif
    logD("init_algo(): sensorDev(%d), sensorId(%d), sensorMode(%d), ratio(%d).",
            m_sensorDev, m_sensorId, sensorMode, ratio);

    /* get parameters and set to algo */
    FLICKER_EXT_PARA para;
    getTargetParameter(sensorMode, ratio, &para);
    flicker_setExtPara(&para); // algo

    /* get pixel line, height, width and pixel clock from property */
    getPropInt(PROP_FLK_CLK, (int *)&m_u4SensorPixelClkFreq, m_u4SensorPixelClkFreq);
    getPropInt(PROP_FLK_PIXEL, (int *)&m_u4PixelsInLine, m_u4PixelsInLine);
    getPropInt(RPOP_FLK_W, (int *)&m_winW, m_winW);
    getPropInt(RPOP_FLK_H, (int *)&m_winH, m_winH);

    /* init flicker algo */
    logD("init_algo(): flicker_init(%u,%u,%u,%u) +.", m_u4PixelsInLine,
            m_winH * 3, m_winW, m_u4SensorPixelClkFreq);
    long int t = getMs();

#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    if (sensorType == TYPE_3EXPO)
    {
        flicker_init_SONY(m_u4PixelsInLine/2, 96 * 3, m_winW, m_u4SensorPixelClkFreq); // algo
    }
    else
    {
        flicker_init(m_u4PixelsInLine, m_winH * 3, m_winW, m_u4SensorPixelClkFreq); // algo
    }
#else
    flicker_init(m_u4PixelsInLine, m_winH * 3, m_winW, m_u4SensorPixelClkFreq); // algo
#endif

    logI("init_algo(): sensorDev(%d) sensorId(%d) sensorMode(%d) sensorType(%d) initFreq(%d) ratio(%d) clkFreq(%d) pixelsInLine(%d) imgH(%d) winH(%d) winW(%d) mUsers(%d) period(%ld ms)",
        m_sensorDev, m_sensorId, sensorMode, sensorType, initFreq, ratio,
        m_u4SensorPixelClkFreq, m_u4PixelsInLine, m_imgH, m_winH, m_winW, mUsers,
        getMs()-t);
    return err;
}

MINT32 FlickerHal::uninit_algo()
{
    logI("uninit_algo(): mUsers(%d). m_sensorDev(%d)", mUsers, m_sensorDev);

    if (mUsers <= 0)
        return 0;

    android_atomic_dec(&mUsers);
    if (!mUsers)
        flicker_uninit(); // algo

    return 0;
}

/***********************************************************
 * Flicker oprations
 **********************************************************/
static int dumpFLKO(
        const int *buf1, const int *buf2, const int len,
        const int winW, const int winH, const int flickerState,
        const flkSensorInfo *sensorInfo, const flkAEInfo *AEInfo, const int *afStt)
{
    /* update file count */
    int cnt;
    getFileCount(FLK_DUMP_COUNT_FILE, &cnt, 0);
    setFileCount(FLK_DUMP_COUNT_FILE, cnt + 1);

    /* create folder */
    char s[64];
    createDir("/data/vendor/flicker/flko/");
    snprintf(s, sizeof(s), "/data/vendor/flicker/flko/%03d", cnt);
    createDir(s);

    FILE *fp;
    snprintf(s, sizeof(s), "/data/vendor/flicker/flko/%03d/flk.raw", cnt);
    fp = fopen(s, "wb");
    if (!fp) {
        logE("dumpFLKO(): failed to open file(%s).", s);
        return -1;
    }

    fwrite(&len, 1, 4, fp);
    fwrite(buf1, 4, len, fp);
    fwrite(buf2, 4, len, fp);
    fwrite(&winW, 1, 4, fp);
    fwrite(&winH, 1, 4, fp);
    fwrite(&flickerState, 1, 4, fp);
    fwrite(sensorInfo, 1, sizeof(flkSensorInfo), fp);
    fwrite(AEInfo, 1, sizeof(flkAEInfo), fp);
    fwrite(afStt, 4, 9, fp);
    fclose(fp);

    /* dump pure FLKO */
    int dumpPure = 0;
    getPropInt(PROP_FLK_DUMP_PURE, &dumpPure, 0);
    if (dumpPure)
    {
        snprintf(s, sizeof(s), "/data/vendor/flicker/flko/%03d/flk_pure.raw", cnt);
        fp = fopen(s, "wb");
        if (!fp) {
            logE("dumpFLKO(): failed to open file(%s).", s);
            return -1;
        }

        fwrite(buf1, 4, len, fp);
        fwrite(buf2, 4, len, fp);
        fclose(fp);
    }

    return 0;
}

int FlickerHal::update(int sensorType, FlickerInput *in, FlickerOutput *out)
{
    Mutex::Autolock lock(m_lock);

    if (!m_isAttach) {
        logD("update(): sensorDev(%d) skip, not attached.", m_sensorDev);
        return -1;
    }

    if (m_currentFrame++ < 2) {
        logI("update(): skip, not enough frame(%d).", m_currentFrame);
        return -1;
    }

    /* analyze flicker */
    if (m_flickerMode == MTK_CONTROL_AE_ANTIBANDING_MODE_AUTO) {
        /*
         * cycle = 0, copy data.
         * cycle = 1, copy data and do flicker detection.
         * other, do nothing.
         */
        int ret = 0;
        if (m_currentCycle < 2) {
            logD("update(): analyze with sensorDev(%d), exposure time(%d), mVa(%p), cycle(%d).",
                    m_sensorDev, in->aeExpTime, in->pBuf, m_currentCycle);
            ret = analyzeFlickerFrequency(sensorType, in->aeExpTime, in->pBuf);
        }
        m_currentCycle = (m_currentCycle + 1) % m_detectCycle;

        if (ret) {
            logE("update(): failed to analyze(%x).", ret);
            return ret;
        }

    }
    out->flickerResult = m_flickerState;

    return 0;
}

MINT32 FlickerHal::analyzeFlickerFrequency(int sensorType, int exp, MVOID *buf)
{
    if (!m_pVectorData1 || !m_pVectorData2)
    {
        logI("m_pVectorData1 or m_pVectorData2 is NULL.");
        return -1;
    }

    MINT32 *flickerBuf1;
    MINT32 *flickerBuf2;
    if (!m_currentCycle) {
        flickerBuf1 = m_pVectorData1;
        flickerBuf2 = m_pVectorData2;
    } else {
        flickerBuf1 = m_pVectorData2;
        flickerBuf2 = m_pVectorData1;
    }

    MINT32 dataLen = 0;
    MINT32 imgW = m_imgW;
    MINT32 imgH;
    if (m_imgH > FLICKER_MAX_LENG) {
        dataLen = 3 * FLICKER_MAX_LENG / 2;
        imgH = FLICKER_MAX_LENG;
    } else {
        dataLen = 3 * m_imgH / 2;
        imgH = m_imgH;
    }

#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    if (sensorType == TYPE_3EXPO)
    {
        dataLen = 4*96/2;
        MINT32 *flickerDMAAdr = (MINT32 *)buf;
        for (int i = 0; i < dataLen; i++)
        {
            flickerBuf1[2*i] = flickerDMAAdr[2*i];
            flickerBuf1[2*i+1] = flickerDMAAdr[2*i+1];
        }
    }
    else
    {
        MINT32 *flickerDMAAdr = (MINT32 *)buf;
        for (int i = 0; i < dataLen; i++) {
            flickerBuf1[2 * i + 0] = flickerDMAAdr[i] & 0x0000FFFF;
            flickerBuf1[2 * i + 1] = (flickerDMAAdr[i] & 0xFFFF0000) >> 16;
        }
    }
#else
    MINT32 *flickerDMAAdr = (MINT32 *)buf;
    for (int i = 0; i < dataLen; i++) {
        flickerBuf1[2 * i + 0] = flickerDMAAdr[i] & 0x0000FFFF;
        flickerBuf1[2 * i + 1] = (flickerDMAAdr[i] & 0xFFFF0000) >> 16;
    }
#endif

    for (int i = 0; i < imgH; i += 100) {
        logD("analyzeFlickerFrequency(): sta row =%d, %d %d %d", i,
                flickerBuf1[3 * i],
                flickerBuf1[3 * i + 1],
                flickerBuf1[3 * i + 2]);
    }

    /* only copy buffer at cycle 1 */
    if (m_currentCycle == 0)
        return 0;

    /* copy buffer and analyze at cycle 2 */
    flkSensorInfo sensorInfo;
    sensorInfo.pixelClock = m_u4SensorPixelClkFreq;
    sensorInfo.fullLineWidth = m_u4PixelsInLine;
    flkEISVector EISvector;
    memset(&EISvector, 0, sizeof(flkEISVector));
    flkAEInfo AEInfo;
    AEInfo.previewShutterValue = exp;
    MINT32 afStt[9] = {0};
    MINT32 winW = ((imgW / 3) >> 1) << 1;
    MINT32 winH = ((imgH / 3) >> 1) << 1;

    /* dump FLKO */
    int dump;
    getPropInt(PROP_FLK_DUMP, &dump, 0);
    if (dump)
        dumpFLKO(flickerBuf1, flickerBuf2, dataLen * 2, winW, winH,
                m_flickerState, &sensorInfo, &AEInfo, afStt);

    /* detect flicker */
    if (exp < m_maxDetExpUs &&
            ((exp > 8200 && m_flickerState == HAL_FLICKER_AUTO_60HZ ) ||
             (exp > 9800 && m_flickerState == HAL_FLICKER_AUTO_50HZ ))) {

        logD("analyzeFlickerFrequency(): detect flicker win(%d,%d), exp(%d), freq(%d) +.",
                winW, winH, exp, m_flickerState);

        FLICKER_STATUS algoStatus = FK100;
        long int t = getMs();

#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
        if (sensorType == TYPE_3EXPO)
        {
            CAM_TRACE_BEGIN("FLK algo");
            algoStatus = detectFlicker_SW_SONY(
                flickerBuf1, flickerBuf2,
                3, 4, winW, 32, (MINT32)m_flickerState,
                sensorInfo, EISvector, AEInfo, afStt); // algo
            CAM_TRACE_END();
        }
        else
        {
            CAM_TRACE_BEGIN("FLK algo");
            algoStatus = detectFlicker_SW(
                flickerBuf1, flickerBuf2,
                3, 3, winW, winH, (MINT32)m_flickerState,
                sensorInfo, EISvector, AEInfo, afStt); // algo
            CAM_TRACE_END();
        }
#else
        CAM_TRACE_BEGIN("FLK algo");
            algoStatus = detectFlicker_SW(
                flickerBuf1, flickerBuf2,
                3, 3, winW, winH, (MINT32)m_flickerState,
                sensorInfo, EISvector, AEInfo, afStt); // algo
        CAM_TRACE_END();
#endif

        logD("analyzeFlickerFrequency(): detect period(%ld ms) -.", getMs() - t);

        int propFlickerSwitch;
        getPropInt(PROP_FLK_SWITCH_EN, &propFlickerSwitch, -1);
        if (!propFlickerSwitch)
            logI("flicker state not changed due to property fixed");
        else {
            if (algoStatus == FK100 &&
                    m_flickerState == HAL_FLICKER_AUTO_60HZ)
                setFlickerState(HAL_FLICKER_AUTO_50HZ);
            else if (algoStatus == FK120 &&
                    m_flickerState == HAL_FLICKER_AUTO_50HZ)
                setFlickerState(HAL_FLICKER_AUTO_60HZ);
        }
    } else if (exp >= m_maxDetExpUs)
        logI("analyzeFlickerFrequency(): skip, exp is too long(%d).", exp);
    else
        logI("analyzeFlickerFrequency(): skip, exp is too short(%d).", exp);

    logD("analyzeFlickerFrequency(): flicker status(%d). m_sensorDev(%d)", m_flickerState, m_sensorDev);

    return 0;
}

int FlickerHal::getFlickerState(int &state)
{
    Mutex::Autolock lock(m_lock);
    logD("getFlickerState(): state(%d).", m_flickerState);
    state = m_flickerState;
    return 0;
}

int FlickerHal::setFlickerState(int state)
{
    logD("setFlickerState(): %d.", state);

    if (m_flickerState == state)
        return 0;

    logI("setFlickerState(): state change(%d->%d).",
            m_flickerState, state);

    m_flickerState = state;

    /* must be called every time we change the flicker table */
    if (m_flickerState == HAL_FLICKER_AUTO_50HZ)
        set_flicker_state(Hz50); // algo
    else
        set_flicker_state(Hz60); // algo
    reset_flicker_queue(); // algo

    return 0;
}

int FlickerHal::setFlickerMode(int mode)
{
    Mutex::Autolock lock(m_lock);

    logD("setFlickerMode(): %d.", mode);

    if (mode != m_flickerMode) {
        logI("setFlickerMode(): mode change(%d->%d).",
                m_flickerMode, mode);
        if (mode == MTK_CONTROL_AE_ANTIBANDING_MODE_AUTO)
            IAeMgr::getInstance().setFlickerFrameRateActive(m_sensorDev, 1);
        else if (m_flickerMode == MTK_CONTROL_AE_ANTIBANDING_MODE_AUTO)
            IAeMgr::getInstance().setFlickerFrameRateActive(m_sensorDev, 0);

        m_flickerMode = mode;
    }

    /* set flicker state */
    if (m_flickerMode == MTK_CONTROL_AE_ANTIBANDING_MODE_60HZ)
        setFlickerState(HAL_FLICKER_AUTO_60HZ);
    else if (m_flickerMode == MTK_CONTROL_AE_ANTIBANDING_MODE_50HZ)
        setFlickerState(HAL_FLICKER_AUTO_50HZ);

    return 0;
}

MVOID FlickerHal::prepareFlickerCfg(int width, int height, FLKWinCFG_T *flkWinCfg)
{
    int u4ToleranceLine = 20;
    int imageW = width - 3;
    int imageH = height - 25;
    if (imageH > FLICKER_MAX_LENG - 6)
        imageH = FLICKER_MAX_LENG - 6;

    flkWinCfg->m_uImageW = imageW;
    flkWinCfg->m_uImageH = imageH;
    flkWinCfg->m_u4NumX = 3;
    flkWinCfg->m_u4NumY = 3;
    flkWinCfg->m_u4OffsetX = 0;
    flkWinCfg->m_u4OffsetY = 0 + u4ToleranceLine;
    flkWinCfg->m_u4SizeX = ((imageW - flkWinCfg->m_u4OffsetX) / 6) * 2;
    flkWinCfg->m_u4SizeY = ((imageH - flkWinCfg->m_u4OffsetY + u4ToleranceLine) / 6) * 2;
#if (!CAM3_3A_ISP_30_EN)
    flkWinCfg->m_u4DmaSize =
        flkWinCfg->m_u4NumX * flkWinCfg->m_u4NumY * flkWinCfg->m_u4SizeY * 2;
#else
    flkWinCfg->m_u4DmaSize =
        flkWinCfg->m_u4NumX * flkWinCfg->m_u4NumY * flkWinCfg->m_u4SizeY * 2 - 1;
#endif
    flkWinCfg->m_u4SGG3_PGN = 16;
    flkWinCfg->m_u4SGG3_GMR1 = 20;
    flkWinCfg->m_u4SGG3_GMR2 = 29;
    flkWinCfg->m_u4SGG3_GMR3 = 42;
    flkWinCfg->m_u4SGG3_GMR4 = 62;
    flkWinCfg->m_u4SGG3_GMR5 = 88;
    flkWinCfg->m_u4SGG3_GMR6 = 126;
    flkWinCfg->m_u4SGG3_GMR7 = 180;
}

