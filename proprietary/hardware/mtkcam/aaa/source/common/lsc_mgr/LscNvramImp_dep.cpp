/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "lsc_nvram"
#ifndef ENABLE_MY_LOG
#define ENABLE_MY_LOG           (1)
#define GLOBAL_ENABLE_MY_LOG    (1)
#endif

#include "ILscNvram.h"
#include <LscUtil.h>
#include <mtkcam/drv/IHalSensor.h>
#include <nvbuf_util.h>
#include "tsf_tuning_custom.h"
#include <mtkcam/drv/mem/cam_cal_drv.h>

using namespace NSIspTuning;


ILscNvram*
LscNvramImp::
getInstance(ESensorDev_T sensor)
{
    LSC_LOG_BEGIN("eSensorDev(0x%02x)", (MUINT32)sensor);

    switch (sensor)
    {
    default:
    case ESensorDev_Main:       //  Main Sensor
        static LscNvramImp singleton_main(ESensorDev_Main);
        LSC_LOG_END("ESensorDev_Main(%p)", &singleton_main);
        return &singleton_main;
    case ESensorDev_MainSecond: //  Main Second Sensor
        static LscNvramImp singleton_main2(ESensorDev_MainSecond);
        LSC_LOG_END("ESensorDev_MainSecond(%p)", &singleton_main2);
        return &singleton_main2;
    case ESensorDev_Sub:        //  Sub Sensor
        static LscNvramImp singleton_sub(ESensorDev_Sub);
        LSC_LOG_END("ESensorDev_Sub(%p)", &singleton_sub);
        return &singleton_sub;
    case ESensorDev_SubSecond:        //  Sub Second Sensor
        static LscNvramImp singleton_sub2(ESensorDev_SubSecond);
        LSC_LOG_END("ESensorDev_SubSecond(%p)", &singleton_sub2);
        return &singleton_sub2;
    }
}

ILscNvram::E_LSC_OTP_T
LscNvramImp::
importEEPromData()
{
    E_LSC_OTP_T eRet = E_LSC_NO_OTP;
    //MUINT32 i;
    LSC_LOG_BEGIN();

    MINT32 i4SensorDevID;

    switch (m_eSensorDev)
    {
    case ESensorDev_Main:
        i4SensorDevID = NSCam::SENSOR_DEV_MAIN;
        break;
    case ESensorDev_Sub:
        i4SensorDevID = NSCam::SENSOR_DEV_SUB;
        break;
    case ESensorDev_MainSecond:
        i4SensorDevID = NSCam::SENSOR_DEV_MAIN_2;
        break;
    case ESensorDev_Main3D:
        i4SensorDevID = NSCam::SENSOR_DEV_MAIN_3D;
        break;
    case ESensorDev_SubSecond:
        i4SensorDevID = NSCam::SENSOR_DEV_SUB_2;
        break;
    default:
        i4SensorDevID = NSCam::SENSOR_DEV_NONE;
        break;
    }

    CAMERA_CAM_CAL_TYPE_ENUM eCamCalDataType = CAMERA_CAM_CAL_DATA_SHADING_TABLE;
    CAM_CAL_DATA_STRUCT* pCalData = new CAM_CAL_DATA_STRUCT;

    if (pCalData == NULL)
    {
        LSC_ERR("Fail to allocate buffer!");
        return E_LSC_OTP_ERROR;
    }

#ifndef LSC_DBG
    CamCalDrvBase* pCamCalDrvObj = CamCalDrvBase::createInstance();
    if (!pCamCalDrvObj)
    {
        LSC_LOG("pCamCalDrvObj is NULL");
        delete pCalData;
        return E_LSC_NO_OTP;
    }

    MINT32 ret = pCamCalDrvObj->GetCamCalCalData(i4SensorDevID, eCamCalDataType, pCalData);
#else
    MINT32 ret = 0;
    ::memcpy(pCalData, &_rDbgCamCalData, sizeof(CAM_CAL_DATA_STRUCT));
#endif

    LSC_LOG("ret(0x%08x)", ret);
    if (ret & CamCalReturnErr[eCamCalDataType])
    {
        LSC_LOG("Error(%s)", CamCalErrString[eCamCalDataType]);
        m_bIsEEPROMImported = MTRUE;
        delete pCalData;
        return E_LSC_NO_OTP;
    }
    else
    {
        LSC_LOG("Get OK");
    }

    MUINT32 u4Rot = 0;
    CAM_CAL_DATA_VER_ENUM eDataType  = pCalData->DataVer;
    CAM_CAL_LSC_DATA*     pLscData   = NULL;    // union struct

    LSC_LOG("eDataType(%d)", eDataType);
    switch (eDataType)
    {
    case CAM_CAL_SINGLE_EEPROM_DATA:
        LSC_LOG("CAM_CAL_SINGLE_EEPROM_DATA");
    case CAM_CAL_SINGLE_OTP_DATA:
        LSC_LOG("CAM_CAL_SINGLE_OTP_DATA");
        pLscData = &pCalData->SingleLsc.LscTable;
        u4Rot = pCalData->SingleLsc.TableRotation;
        break;
    case CAM_CAL_N3D_DATA:
        LSC_LOG("CAM_CAL_N3D_DATA");
        if (ESensorDev_Main == m_eSensorDev)
        {
            pLscData = &pCalData->N3DLsc.Data[0].LscTable;
            u4Rot = pCalData->N3DLsc.Data[0].TableRotation;
            LSC_LOG("CAM_CAL_N3D_DATA MAIN");
        }
        else
        {
            pLscData = &pCalData->N3DLsc.Data[1].LscTable;
            u4Rot = pCalData->N3DLsc.Data[1].TableRotation;
            LSC_LOG("CAM_CAL_N3D_DATA MAIN2");
        }
        break;
    default:
        LSC_ERR("Unknown eDataType(%d)", eDataType);
        m_bIsEEPROMImported = MTRUE;
        delete pCalData;
        return E_LSC_NO_OTP;
    }

    m_rOtp.TableRotation = u4Rot;
    ::memcpy(&m_rOtp.LscTable, pLscData, sizeof(CAM_CAL_LSC_DATA));
    LSC_LOG("u4Rot(%d), pLscData(%p)", u4Rot, pLscData);

    MUINT8 u1TblType = pLscData->MtkLcsData.MtkLscType;

    if (u1TblType & (1<<0))
    {
        // send table via sensor hal
        eRet = E_LSC_NO_OTP;
        //setSensorShading((MVOID*) &pLscData->SensorLcsData);
    }
    else if (u1TblType & (1<<1))
    {
        // do 1-to-3
        eRet = E_LSC_WITH_MTK_OTP;
        //m_fg1to3 = do123LutToSysram((MVOID*) &pLscData->MtkLcsData);
    }

    m_bIsEEPROMImported = MTRUE;

    delete pCalData;

    LSC_LOG_END();

    return eRet;
}
