#define LOG_TAG "FlashHal"

/* camera headers */
#include "mtkcam/drv/IHalSensor.h"
/* aaa headers */
#if (CAM3_3A_ISP_30_EN || CAM3_3A_ISP_40_EN || CAM3_3A_ISP_50_EN)
#include "isp_tuning_mgr.h"
#endif
#include "strobe_drv.h"

/* aaa common headers */
#include "property_utils.h"
#include "time_utils.h"
#include "math_utils.h"

/* custom headers */
#include "flash_tuning_custom.h"
#include "camera_custom_nvram.h"
#include "flash_param.h"
#include "strobe_param.h"

/* flash headers */
#include "flash_hal.h"
#include "flash_mgr.h"
#include "flash_utils.h"
#include "flash_nvram.h"

/* kernel headers */
#include "kd_camera_feature.h"
#include "ae_param.h"
#include "aaa_sensor_mgr.h"

using namespace NSCam;
#if (CAM3_3A_ISP_30_EN || CAM3_3A_ISP_40_EN || CAM3_3A_ISP_50_EN)
using namespace NSIspTuning;
using namespace NSIspTuningv3;
#endif


/***********************************************************
 * Strobe handler
 **********************************************************/
STROBE_DEVICE_ENUM getStrobeDevice(int sensorDev)
{
    /*
     * typedef enum {
     *    STROBE_DEVICE_NONE = 0,
     *    STROBE_DEVICE_FLASHLIGHT,
     *    STROBE_DEVICE_LED,
     *    STROBE_DEVICE_XENON,
     *    STROBE_DEVICE_DISPLAY,
     *    STROBE_DEVICE_IR,
     * } STROBE_DEVICE_ENUM;
     */
    return cust_getStrobeDevice(sensorDev);
}

STROBE_TYPE_ENUM getStrobeType(int sensorDev)
{
    if (sensorDev == DUAL_CAMERA_MAIN_SENSOR)
        return STROBE_TYPE_REAR;
    if (sensorDev == DUAL_CAMERA_SUB_SENSOR)
        return STROBE_TYPE_FRONT;
    if (sensorDev == DUAL_CAMERA_MAIN_2_SENSOR ||
            sensorDev == DUAL_CAMERA_MAIN_SECOND_SENSOR)
        return STROBE_TYPE_REAR;
    if (sensorDev == DUAL_CAMERA_MAIN_3_SENSOR)
        return STROBE_TYPE_REAR;
    if (sensorDev == DUAL_CAMERA_SUB_2_SENSOR)
        return STROBE_TYPE_FRONT;

    return STROBE_TYPE_NONE;
}

int getStrobeCtNum(int typeId)
{
    /*
     * WARNING, be careful of mapping relationship between
     *
     * typedef enum {
     *    STROBE_TYPE_NONE = 0,
     *    STROBE_TYPE_REAR = 1,
     *    STROBE_TYPE_FRONT = 2,
     * } STROBE_TYPE_ENUM;
     *
     * typedef enum {
     *    DUAL_CAMERA_NONE_SENSOR = 0,
     *    DUAL_CAMERA_MAIN_SENSOR = 1,
     *    DUAL_CAMERA_SUB_SENSOR = 2,
     *    DUAL_CAMERA_MAIN_2_SENSOR = 4,
     *    DUAL_CAMERA_MAIN_SECOND_SENSOR = 4,
     *    DUAL_CAMERA_SUB_2_SENSOR   = 8,
     *    DUAL_CAMERA_SENSOR_MAX
     * } CAMERA_DUAL_CAMERA_SENSOR_ENUM;
     */
    if (cust_isDualFlashSupport(typeId))
        return 2;
    return 1;
}

/***********************************************************
 * Flash Hal
 **********************************************************/
IHalFlash *IHalFlash::getInstance(MINT32 const i4SensorOpenIdx)
{
    IHalSensorList *const pHalSensorList = MAKE_HalSensorList();
    int sensorDev = (!pHalSensorList) ? 0 : pHalSensorList->querySensorDevIdx(i4SensorOpenIdx);
    return FlashHal::getInstance(sensorDev);
}

FlashHal::FlashHal(int sensorDev)
    : mSensorDev(sensorDev)
    , mTorchStatus(0)
    , mDriverFault(0)
    , mHasHw(0)
    , mInCharge(1)
    , mpStrobe(NULL)
    , mpStrobe2(NULL)
    , mpStrobeIRProjector(NULL)
    , mStrobeDevice(STROBE_DEVICE_NONE)
    , mStrobeTypeId(STROBE_TYPE_NONE)
    , mStrobeCtNum(0)
    , mStrobePartId(1)
{
    /* set debug */
    setDebug();

    logI("FlashHal(): sensorDev(%d).", mSensorDev);

    /* clear data structure */
    memset(&mFlashHalTimeInfo, 0, sizeof(mFlashHalTimeInfo));
    memset(&mFlashHalInfo, 0, sizeof(mFlashHalInfo));
    memset(&mPrjPara, 0, sizeof(FLASH_PROJECT_PARA));

    /* setup strobe hardware */
    mStrobeDevice = getStrobeDevice(mSensorDev);
    mStrobeTypeId = getStrobeType(mSensorDev);
    mStrobeCtNum = getStrobeCtNum(mStrobeTypeId);
    mpStrobe = StrobeDrv::getInstance(mStrobeDevice, mStrobeTypeId, 1);
    if (mStrobeCtNum > 1)
        mpStrobe2 = StrobeDrv::getInstance(mStrobeDevice, mStrobeTypeId, 2);

    mpStrobe->getPartId(&mStrobePartId);
    if (mStrobePartId < 1 || mStrobePartId > 2) {
        logE("FlashHal(): invalid part id.");
        mStrobePartId = 1;
    }

#if CAM3_STEREO_FEATURE_EN
    if (cust_isIRProjectorSupport(mSensorDev)){
        mpStrobeIRProjector = StrobeDrv::getInstance(mStrobeDevice, mStrobeTypeId, 2);
    }
#endif

    cust_setFlashPartId(mStrobeTypeId, mStrobePartId); /* WARNING, sernsorDev in custom */
    mpStrobe->hasFlashHw(&mHasHw);

    /* get nvram strobe data */
    int ret;
    NVRAM_CAMERA_STROBE_STRUCT *pNvram;
    ret = FlashNvram::nvReadStrobe(pNvram, mSensorDev);
    if (ret)
        logE("FlashHal(): failed to read nvram(%d).", ret);

    /*
     * Get project parameters.
     * It's fine if failed to get NVRAM. (pNvram is NULL).
     */
    mPrjPara = cust_getFlashProjectPara_V3(mSensorDev, LIB3A_AE_SCENE_AUTO, 0, pNvram);

    /* set strobe info */
    mpStrobe->setStrobeInfo(mPrjPara.dutyNum,
            mPrjPara.coolTimeOutPara.tabNum, mPrjPara.coolTimeOutPara.tabId,
            mPrjPara.coolTimeOutPara.timOutMs, mPrjPara.coolTimeOutPara.coolingTM);
    if (mStrobeCtNum > 1)
        mpStrobe2->setStrobeInfo(mPrjPara.dutyNumLT,
                mPrjPara.coolTimeOutParaLT.tabNum, mPrjPara.coolTimeOutParaLT.tabId,
                mPrjPara.coolTimeOutParaLT.timOutMs, mPrjPara.coolTimeOutParaLT.coolingTM);

    /* setup flash hal info */
    int duty = 0;
    int dutyLt = 0;
    cust_getFlashHalTorchDuty(mStrobeTypeId, &duty, &dutyLt); /* WARNING, sernsorDev in custom */
    mFlashHalInfo[FLASH_HAL_SCENARIO_TORCH].duty = duty;
    mFlashHalInfo[FLASH_HAL_SCENARIO_TORCH].dutyLt = dutyLt;
    /*For its pass. The parameter should be obtained from NVRAM*/
    mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].duty = duty;
    mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].dutyLt = dutyLt;

    if (pNvram) {
#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
        mFlashHalInfo[FLASH_HAL_SCENARIO_VIDEO_TORCH].duty = pNvram->Flash_AE[0].engLevel.torchDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_VIDEO_TORCH].dutyLt = pNvram->Flash_AE[0].engLevelLT.torchDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_AF_LAMP].duty = pNvram->Flash_AE[0].engLevel.afDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_AF_LAMP].dutyLt = pNvram->Flash_AE[0].engLevelLT.afDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_PRE_FLASH].duty = pNvram->Flash_AE[0].engLevel.pfDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_PRE_FLASH].dutyLt = pNvram->Flash_AE[0].engLevelLT.pfDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].duty = pNvram->Flash_AE[0].engLevel.mfDutyMaxL;
        mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].dutyLt = pNvram->Flash_AE[0].engLevelLT.mfDutyMaxL;
#else
        mFlashHalInfo[FLASH_HAL_SCENARIO_VIDEO_TORCH].duty = pNvram->engLevel.torchDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_VIDEO_TORCH].dutyLt = pNvram->engLevelLT.torchDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_AF_LAMP].duty = pNvram->engLevel.afDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_AF_LAMP].dutyLt = pNvram->engLevelLT.afDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_PRE_FLASH].duty = pNvram->engLevel.pfDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_PRE_FLASH].dutyLt = pNvram->engLevelLT.pfDuty;
        mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].duty = pNvram->engLevel.mfDutyMaxL;
        mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].dutyLt = pNvram->engLevelLT.mfDutyMaxL;
#endif
    }
    /* show attribute */
    show();
}

FlashHal *FlashHal::getInstance(int sensorDev)
{
    if (sensorDev == DUAL_CAMERA_MAIN_SENSOR) {
        static FlashHal singleton1(sensorDev);
        return &singleton1;
    } else if (sensorDev == DUAL_CAMERA_SUB_SENSOR) {
        static FlashHal singleton2(sensorDev);
        return &singleton2;
    } else if (sensorDev == DUAL_CAMERA_MAIN_2_SENSOR
            || sensorDev == DUAL_CAMERA_MAIN_SECOND_SENSOR){
        static FlashHal singleton3(sensorDev);
        return &singleton3;
    } else if (sensorDev == DUAL_CAMERA_MAIN_3_SENSOR) {
        static FlashHal singleton5(sensorDev);
        return &singleton5;
    } else { // DUAL_CAMERA_SUB_2_SENSOR
        static FlashHal singleton4(sensorDev);
        return &singleton4;
    }
}

void FlashHal::destroyInstance()
{
}


/***********************************************************
 * Misc
 **********************************************************/
static int verifyScenario(FLASH_HAL_SCENARIO_ENUM scenario)
{
    if (scenario < FLASH_HAL_SCENARIO_TORCH || scenario >= FLASH_HAL_SCENARIO_NUM)
        return -1;
    return 0;
}

void FlashHal::show()
{
    logI("Device: (%d), Type: (%d), CT number: (%d), Part ID: (%d), HW(%d), in charge(%d).",
            mStrobeDevice, mStrobeTypeId, mStrobeCtNum, mStrobePartId, mHasHw, mInCharge);
}

/***********************************************************
 * Life cycle
 **********************************************************/
int FlashHal::init()
{
    logI("init().");

    mpStrobe->init();
    mpStrobe->lowPowerDetectStart(mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].duty);
    if (mStrobeCtNum > 1) {
        mpStrobe2->init();
        mpStrobe2->lowPowerDetectStart(mFlashHalInfo[FLASH_HAL_SCENARIO_LOW_POWER].dutyLt);
    }

    if (mpStrobeIRProjector != NULL)
        mpStrobeIRProjector->init();
    return 0;
}

int FlashHal::uninit()
{
    logI("uninit().");

    mpStrobe->lowPowerDetectEnd();
    mpStrobe->uninit();
    if (mStrobeCtNum > 1) {
        mpStrobe2->lowPowerDetectEnd();
        mpStrobe2->uninit();
    }

    if (mpStrobeIRProjector != NULL)
        mpStrobeIRProjector->uninit();

    return 0;
}

/***********************************************************
 * Attribute
 **********************************************************/
int FlashHal::setInfo(FLASH_HAL_SCENARIO_ENUM scenario, FlashHalInfo info)
{
    /* verify arguments */
    if (verifyScenario(scenario)) {
        logE("setInfo(): invalid arguments.");
        return -1;
    }

    logI("setInfo(): scenario(%d).", scenario);
    mFlashHalInfo[scenario] = info;

    return 0;
}

int FlashHal::setInfoDuty(FLASH_HAL_SCENARIO_ENUM scenario, FlashHalInfo info)
{
    /* verify arguments */
    if (verifyScenario(scenario)) {
        logE("setInfoDuty(): invalid arguments.");
        return -1;
    }

    logI("setInfoDuty(): scenario(%d).", scenario);

    int timeout;
    mpStrobe->getTimeOutTime(info.duty, &timeout);
    if (timeout == ENUM_FLASH_TIME_NO_TIME_OUT)
        timeout = 0;
    info.timeout = timeout;

    if (mStrobeCtNum > 1) {
        mpStrobe2->getTimeOutTime(info.dutyLt, &timeout);
        if (timeout == ENUM_FLASH_TIME_NO_TIME_OUT)
            timeout = 0;
        info.timeoutLt = timeout;
    }
    mFlashHalInfo[scenario] = info;

    return 0;
}

FlashHalTimeInfo FlashHal::getTimeInfo()
{
    logD("getTimeInfo(): main flash period(%d,%d), timeout(%d,%d), is timeout(%d).",
            mFlashHalTimeInfo.mfStartTime, mFlashHalTimeInfo.mfEndTime,
            mFlashHalTimeInfo.mfTimeout, mFlashHalTimeInfo.mfTimeoutLt,
            mFlashHalTimeInfo.mfIsTimeout);
    return mFlashHalTimeInfo;
}

int FlashHal::getDriverFault()
{
    return mDriverFault;
}

int FlashHal::hasHw(int &hasHw)
{
    hasHw = mHasHw;
    if (!hasHw)
        logI("hasHw(): hasHw(%d).", hasHw);
    return 0;
}

int FlashHal::getPartId()
{
    return mStrobePartId;
}

int FlashHal::getInCharge()
{
    return mInCharge;
}

int FlashHal::setInCharge(int inCharge)
{
    mInCharge = inCharge;
    logI("setInCharge(): sensorDev(%d), inCharge(%d).",
            mSensorDev, mInCharge);
    return 0;
}

int FlashHal::isAvailable()
{
    int isAvailable = mHasHw && mInCharge;

    if (!isAvailable)
        logD("isAvailable(): hasHw(%d), inCharge(%d).",
                mHasHw, mInCharge);

    return isAvailable;
}

int FlashHal::getBattVol(int *battVol)
{
    return mpStrobe->getBattVol(battVol);
}

int FlashHal::isLowPower(int *battStatus)
{
    return mpStrobe->isLowPower(battStatus);
}

int FlashHal::isNeedWaitCooling(int curMs, int *waitTimeMs)
{
    int isNeedWait = 1;
    float coolTM;
    float coolTMLt;
    int waitTime;
    int waitTimeLt;

    /* get wait time */
    mpStrobe->getCoolTM(mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].duty, &coolTM);
    waitTime = mFlashHalTimeInfo.mfEndTime +
        (mFlashHalTimeInfo.mfEndTime - mFlashHalTimeInfo.mfStartTime) * coolTM -
        (curMs + 300);
    if (mStrobeCtNum > 1) {
        mpStrobe2->getCoolTM(mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].dutyLt, &coolTMLt);
        waitTimeLt = mFlashHalTimeInfo.mfEndTime +
            (mFlashHalTimeInfo.mfEndTime - mFlashHalTimeInfo.mfStartTime) * coolTMLt -
            (curMs + 300);
        waitTime = max(waitTime, waitTimeLt);
    }

    /* need to wait or not */
    if (waitTime > 5000)
        waitTime = 5000;
    else if (waitTime < 0) {
        isNeedWait = 0;
        waitTime = 0;
    }
    *waitTimeMs = waitTime;

    return isNeedWait;
}

/***********************************************************
 * On/Off function
 **********************************************************/
int FlashHal::isChargerReady(int *chargerStatus)
{
    return mpStrobe->isChargerReady(chargerStatus);
}

int FlashHal::setCharger(int ready)
{
    mpStrobe->setCharger(ready);
    if (mStrobeCtNum > 1)
        mpStrobe2->setCharger(ready);

    return 0;
}

int FlashHal::isFlashOn()
{
    int on = 0;
    mpStrobe->isOn(&on);
    if (mStrobeCtNum > 1) {
        int onLt = 0;
        mpStrobe2->isOn(&onLt);
        on |= onLt;
    }

    return on;
}

int FlashHal::isAFLampOn()
{
    if (!isAvailable()) {
        logD("isAFLampOn(): sensorDev(%d) not available.", mSensorDev);
        return 0;
    }

    int on = isFlashOn();
    logD("isAFLampOn(): (%d).", on);
    return on;
}

int FlashHal::setPreOn()
{
    int ret = 0;

    ret |= mpStrobe->setPreOn();
    if (mStrobeCtNum > 1)
        ret |= mpStrobe2->setPreOn();

    return ret;
}

int FlashHal::setFlashOn(FlashHalInfo info)
{
    Mutex::Autolock lock(mLock);

    logI("setFlashOn(): duty(%d), timeout(%d), lt duty(%d), lt timeout(%d).",
            info.duty, info.timeout, info.dutyLt, info.timeoutLt);

    int isLow = 0;
    isLowPower(&isLow);
    if (isLow) {
        logI("setFlashOn(): is low power.");
        return 0;
    }

    /* set duty */
    int dutyCur;
    int dutyCurLt;
    {
        mpStrobe->getDuty(&dutyCur);
        if (dutyCur != info.duty)
            mpStrobe->setDuty(info.duty);
    }
    if (mStrobeCtNum > 1) {
        mpStrobe2->getDuty(&dutyCurLt);
        if (dutyCurLt != info.dutyLt)
            mpStrobe2->setDuty(info.dutyLt);
    }

    /* set on/off */
    int bOn;
    {
        mpStrobe->isOn(&bOn);
        if (info.duty < 0) {
            mpStrobe->setOnOff(0);
        } else {
            mpStrobe->setTimeOutTime(info.timeout);
            mpStrobe->setOnOff(1);
        }
    }
    if (mStrobeCtNum > 1) {
        mpStrobe2->isOn(&bOn);
        if (info.dutyLt < 0) {
            mpStrobe2->setOnOff(0);
        } else {
            mpStrobe2->setTimeOutTime(info.timeoutLt);
            mpStrobe2->setOnOff(1);
        }
    }

    return 0;
}

int FlashHal::setFlashOn(FLASH_HAL_DEVICE_ENUM device)
{
#if CAM3_STEREO_FEATURE_EN
    int sensorID = cust_getHwTriggerSensor();
#else
    int sensorID = 0;
#endif
    int propDuty=0;
    int propFloodDuty=0;
    int propType=0;
    char value[PROPERTY_VALUE_MAX] = {'\0'};

    property_get("debug.ir.duty", value, "12");
    propDuty = atoi(value);
    property_get("debug.ir.type", value, "1");
    propType = atoi(value);
    property_get("debug.ir.flood.duty", value, "5");
    propFloodDuty = atoi(value);

    Mutex::Autolock lock(mLock);

    switch (device) {
    case FLASH_HAL_DEVICE_IR_FLOOD:
        mpStrobe->setDuty(propFloodDuty);
        mpStrobe->setOnOff(2);
        //NS3Av3::AAASensorMgr::getInstance().setSensorFlashlight((MINT32)sensorID, 3);
        break;
    case FLASH_HAL_DEVICE_IR_FLOOD_SENSOR:
        //mpStrobe->setDuty(propFloodDuty);
        //mpStrobe->setOnOff(1);
        NS3Av3::AAASensorMgr::getInstance().setSensorFlashlight((MINT32)sensorID, 3);
        break;
    case FLASH_HAL_DEVICE_IR_PROJECTOR:
        mpStrobeIRProjector->setDuty(propDuty);
        mpStrobeIRProjector->setOnOff(1);
        mpStrobe->setDuty(propFloodDuty);
        mpStrobe->setOnOff(2);
        NS3Av3::AAASensorMgr::getInstance().setSensorFlashlight((MINT32)sensorID, 2);
        break;
    default:
        return 0;
    }

    return 0;
}

int FlashHal::setFlashOff(FLASH_HAL_DEVICE_ENUM device)
{
#if CAM3_STEREO_FEATURE_EN
    int sensorID = cust_getHwTriggerSensor();
#else
    int sensorID = 0;
#endif
    Mutex::Autolock lock(mLock);

    switch (device) {
    case FLASH_HAL_DEVICE_IR_FLOOD_SENSOR:
        NS3Av3::AAASensorMgr::getInstance().setSensorFlashlight((MINT32)sensorID, 4);
        break;
    case FLASH_HAL_DEVICE_IR_PROJECTOR:
        NS3Av3::AAASensorMgr::getInstance().setSensorFlashlight((MINT32)sensorID, 5);
        // always enable TODO: move to active stereo life cycle
        //mpStrobeIRProjector->setOnOff(0);
        break;
    default:
        return 0;
    }
    return 0;
}

int FlashHal::setFlashOff()
{
    Mutex::Autolock lock(mLock);

    logI("setFlashOff().");

    mpStrobe->setOnOff(0);
    if (mStrobeCtNum > 1)
        mpStrobe2->setOnOff(0);

    mpStrobe->getHwFault(&mDriverFault);
    return 0;
}

int FlashHal::setOnOff(int enable, FlashHalInfo info)
{
    logI("setOnOff(): type(%d), enable(%d).", mStrobeTypeId, enable);

    if (!isAvailable()) {
        logI("setOnOff(): sensorDev(%d) not available.", mSensorDev);
        return -1;
    }

    /* set flash info to ISP */ // TODO: moveout
    FLASH_INFO_T finfo;
    finfo.flashMode = FLASHLIGHT_FORCE_OFF; // TODO: not use
    finfo.isFlash = enable;
    //IspTuningMgr::getInstance().setFlashInfo(mSensorDev, finfo);

    if (enable)
        return setFlashOn(info);
    else
        return setFlashOff();
}

int FlashHal::setOnOff(int enable, FLASH_HAL_DEVICE_ENUM device)
{
    logI("setOnOff(): type(%d), enable(%d), device(%d).", mStrobeTypeId, enable, device);

    /* verify arguments */

    switch (device) {
    case FLASH_HAL_DEVICE_IR_FLOOD_SENSOR:
        break;
    case FLASH_HAL_DEVICE_IR_FLOOD:
        break;
    case FLASH_HAL_DEVICE_IR_PROJECTOR:
        break;
    default:
        return 0;
    }

    if (enable)
        return setFlashOn(device);
    else
        return setFlashOff(device);
}

int FlashHal::setOnOff(int enable, FLASH_HAL_SCENARIO_ENUM scenario)
{
    logI("setOnOff(): type(%d), enable(%d), scenario(%d).", mStrobeTypeId, enable, scenario);

    /* verify arguments */
    if (verifyScenario(scenario)) {
        logE("setOnOff(): invalid arguments.");
        return -1;
    }

    if (!isAvailable()) {
        logI("setOnOff(): sensorDev(%d) not available.", mSensorDev);
        return -1;
    }

    if (scenario == FLASH_HAL_SCENARIO_MAIN_FLASH) {
        /* get timeout */
        int currentTime = getMs();
        if (enable) {
            mFlashHalTimeInfo.mfStartTime = currentTime;
            mFlashHalTimeInfo.mfIsTimeout = 0;
            mFlashHalTimeInfo.mfTimeout = mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].timeout;
            if (mStrobeCtNum > 1)
                mFlashHalTimeInfo.mfTimeoutLt = mFlashHalInfo[FLASH_HAL_SCENARIO_MAIN_FLASH].timeoutLt;

        } else {
            mFlashHalTimeInfo.mfEndTime = currentTime;

            int flashOnPeriod = mFlashHalTimeInfo.mfEndTime - mFlashHalTimeInfo.mfStartTime;
            if (flashOnPeriod > mFlashHalTimeInfo.mfTimeout &&
                    mFlashHalTimeInfo.mfTimeout)
                mFlashHalTimeInfo.mfIsTimeout = 1;
            if (mStrobeCtNum > 1)
                if (flashOnPeriod > mFlashHalTimeInfo.mfTimeoutLt &&
                        mFlashHalTimeInfo.mfTimeoutLt)
                    mFlashHalTimeInfo.mfIsTimeout = 1;
        }
    }

    /* set flash info to ISP */ // TODO: moveout
    if (scenario != FLASH_HAL_SCENARIO_TORCH) {
        FLASH_INFO_T finfo;
        finfo.flashMode = FLASHLIGHT_FORCE_OFF; // TODO: not use
        finfo.isFlash = enable;
        //IspTuningMgr::getInstance().setFlashInfo(mSensorDev, finfo);
    }

    if (enable)
        return setFlashOn(mFlashHalInfo[scenario]);
    else
        return setFlashOff();
}

int FlashHal::getTorchStatus()
{
    logI("getTorchStatus(): torch status(%d).", mTorchStatus);
    return mTorchStatus;
}

int FlashHal::setTorchOnOff(MBOOL enable)
{
    logI("setTorchOnOff(): type(%d), enable(%d).", mStrobeTypeId, enable);

    if (mStrobeTypeId == STROBE_TYPE_FRONT && !cust_isSubFlashSupport())
        return 1;

    /*
     * WARNING, only torch API is different from others.
     * The only control zoon for life cycle is in the APP site.
     * It should call init/uninit before using setTorchMode() API.
     *
     * So this is a workaround that life cycle is followed by torch on/off.
     */
    if (enable == 1) {
        init();
        setOnOff(enable, FLASH_HAL_SCENARIO_TORCH);
        mTorchStatus = 1;
    } else {
        setOnOff(enable, FLASH_HAL_SCENARIO_TORCH);
        uninit();
        mTorchStatus = 0;
    }

	return 0;
}

int FlashHal::setVideoTorchOnOff(int enable)
{
    logI("setVideoTorchOnOff(): enable(%d).", enable);
    setOnOff(enable, FLASH_HAL_SCENARIO_VIDEO_TORCH);
    return 0;
}

int FlashHal::setAfLampOnOff(int enable)
{
    logI("setAfLampOnOff(): enable(%d).", enable);
    setOnOff(enable, FLASH_HAL_SCENARIO_AF_LAMP);
    return 0;
}

int FlashHal::setPfOnOff(int enable)
{
    logI("setPfOnOff(): enable(%d).", enable);
    setOnOff(enable, FLASH_HAL_SCENARIO_PRE_FLASH);
    return 0;
}

int FlashHal::setCaptureFlashOnOff(int enable)
{
    logI("setCaptureFlashOnOff(): enable(%d).", enable);
    setOnOff(enable, FLASH_HAL_SCENARIO_MAIN_FLASH);
    return 0;
}

int FlashHal::getIRSupport()
{
    return !(mpStrobeIRProjector == NULL);
}

/***********************************************************
 * Engineer mode related function
 **********************************************************/
int FlashHal::egGetDutyRange(int *start, int *end)
{
#if 1
    *start = 0;
    *end = 1;
#else
    *start = 0;
    *end = mPrjPara.dutyNum - 1;
#endif
    return 0;
}

int FlashHal::egGetStepRange(int *start, int *end)
{
    *start = 0;
    *end = 0;
    return 0;
}

int FlashHal::egSetMfDutyStep(int duty, int step)
{
    (void)duty;
    (void)step;
    return 0;
}

