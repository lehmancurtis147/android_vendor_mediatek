#define LOG_TAG "FlashUtils"

#include <stdio.h>
#include <string.h>
#include <cmath>

/* camera headers */
#include "mtkcam/aaa/aaa_hal_common.h"
#include "camera_custom_awb_nvram.h"
#include "flash_param.h"
#include "flash_utils.h"
#if (CAM3_3A_ISP_30_EN)
#include <ae_param_flow.h>
#endif

/***********************************************************
 * Define macros
 **********************************************************/

#define PROP_FLASH_AAO_RATIO  "vendor.flash_aao_ratio"


/***********************************************************
 * Sort
 **********************************************************/
template <class T>
void flash_sortxy_xinc(int n, T *x, T *y)
{
    int i;
    int j;
    for (i = 0; i < n; i++)
        for (j = i + 1; j < n; j++) {
            if (x[i] > x[j]) {
                T tmp;
                tmp = x[i];
                x[i] = x[j];
                x[j] = tmp;
                tmp = y[i];
                y[i] = y[j];
                y[j] = tmp;
            }
        }
}
template void flash_sortxy_xinc<int>(int, int *, int *);


/***********************************************************
 * BMP export
 **********************************************************/
template <class T>
int arrayToBmp(const char *fname, T *r, T *g, T *b, int w, int h, double maxVal)
{
    char header[54];
    int *pInt;
    short *pShort;

    /* BMP file header */
    /* type (magic number) */
    header[0] = 'B';
    header[1] = 'M';
    /* size */
    pInt = (int *)(header + 2);
    *pInt = 3 * w * h + 54;
    /* reserved 1 and reserved 2 */
    pInt = (int *)(header + 6);
    *pInt = 0;
    /* offset */
    pInt = (int *)(header + 0xA);
    *pInt = 54;

    /* BMP info header */
    /* size */
    pInt = (int *)(header + 0xE);
    *pInt = 40;
    /* width */
    pInt = (int *)(header + 0x12);
    *pInt = w;
    /* height */
    pInt = (int *)(header + 0x16);
    *pInt = h;
    /* planes */
    pShort = (short *)(header + 0x1A);
    *pShort = 1;
    /* bits */
    pShort= (short *)(header + 0x1C);
    *pShort = 24;
    /* compression */
    pInt = (int *)(header + 0x1E);
    *pInt = 0;
    /* image size */
    pInt = (int *)(header + 0x22);
    *pInt = 0;
    /* x resolution */
    pInt = (int *)(header + 0x26);
    *pInt = 2834; // 72 dpi
    /* y resolution */
    pInt = (int *)(header + 0x2A);
    *pInt = 2834; // 72 dpi
    /* number of colors */
    pInt = (int *)(header + 0x2E);
    *pInt = 0;
    /* important colors */
    pInt = (int *)(header + 0x32);
    *pInt = 0;

    /* raw data */
    int i, j;
    int pos, index = 0;
    int lineBytes;
    unsigned char *data;
    unsigned char *pixeldata;

    lineBytes = ((w * 3 + 3) / 4) * 4;
    data = new unsigned char[lineBytes * h];
    if (!data)
        return -1;

    for (j = 0; j < h; j++) {
        pixeldata = data + (h - 1 - j) * lineBytes;
        pos = 0;
        for (i = 0; i < w; i++) {
            pixeldata[pos] = (unsigned char)(b[index] * 255 / maxVal);
            pos++;
            pixeldata[pos] = (unsigned char)(g[index] * 255 / maxVal);
            pos++;
            pixeldata[pos] = (unsigned char)(r[index] * 255 / maxVal);
            pos++;
            index++;
        }
    }

    /* export to file */
    FILE *fp;
    fp = fopen(fname, "wb");
    if (!fp) {
        delete [] data;
        return -1;
    }
    fwrite(header, 1, 54, fp);
    fwrite(data, 1, lineBytes * h, fp);
    fclose(fp);

    delete [] data;

    return 0;
}


/***********************************************************
 * Window operations
 **********************************************************/
int resizeLine(int l, double rZoom, int *lStart, int *lResize, int *lBin, int lResizeMin, int lResizeMax)
{
    /* verify arguments */
    if (!lStart || !lResize || !lBin) {
        logE("resizeLine(): error arguments.");
        return -1;
    }
    if (lResizeMax < lResizeMin) {
        logE("resizeLine(): error crop size.");
        return -1;
    }

    /* init crop size and bin */
    float lTar = l / rZoom;
    int lResizeRet = ((int)lTar) / 2 * 2;
    int lBinRet = 1;
    lResizeMin = (lResizeMin + 1) & 0xffe;
    lResizeMax = lResizeMax & 0xffe;

    /* get crop size and bin */
    if ((int)lTar > lResizeMax) {
        int i;
        int lResizeNew;
        float err;
        float minErr = 10000;
        for (i = 0; i <= lTar / 2; i++) {
            lResizeNew = lResizeMax - 2 * i;

            /* break if down to crop minimum */
            if (lResizeNew < lResizeMin)
                break;

            /* if target lengtn is multiple with crop size */
            err = lTar - (lTar / lResizeNew) * lResizeNew;
            if (!err) {
                lResizeRet = lResizeNew;
                lBinRet = lTar / lResizeNew;
                break;
            }

            /* get the less error part */
            if (err < minErr) {
                minErr = err;
                lResizeRet = lResizeNew;
                lBinRet = lTar / lResizeNew;
            }
        }
    }
    *lResize = lResizeRet;
    *lBin = lBinRet;
    *lStart = (l - lResizeRet * lBinRet) / 2;

    return 0;
}

int resizeWindow(double rZoom, short *data, int w, int h,
        int wResizeNoZoom, int hResizeNoZoom, short *rzData, int *rzW, int *rzH)
{
    /* verify arguments */
    if (!data || !rzData || !rzW || !rzH) {
        logE("resizeWindow(): error arguments.");
        return -1;
    }
    if (w < wResizeNoZoom || h < hResizeNoZoom) {
        logE("resizeWindow(): error crop size.");
        return -1;
    }

    /* get resize data */
    int wStart;
    int wResize;
    int wBin;
    int hStart;
    int hResize;
    int hBin;

    if (rZoom < 1.05) {
        wResize = wResizeNoZoom;
        hResize = hResizeNoZoom;
        wBin = w / wResize;
        hBin = h / hResize;
        wStart = (w - wBin * wResize) / 2;
        hStart = (h - hBin * hResize) / 2;
    } else {
        resizeLine(w, rZoom, &wStart, &wResize, &wBin, 20, wResizeNoZoom);
        resizeLine(h, rZoom, &hStart, &hResize, &hBin, 15, hResizeNoZoom);
        double wErrRate;
        double hErrRate;
        wErrRate = std::abs((double)(w / rZoom - wResize * wBin) / (w / rZoom));
        hErrRate = std::abs((double)(h / rZoom - hResize * hBin) / (h / rZoom));
        if (wErrRate > 0.1) {
            resizeLine(w, rZoom, &wStart, &wResize, &wBin, 10, wResizeNoZoom);
        }
        if (hErrRate > 0.1) {
            resizeLine(h, rZoom, &hStart, &hResize, &hBin, 10, hResizeNoZoom);
        }
    }

    /* clear resize data */
    memset(rzData, 0, wResize * hResize * sizeof(short));

    /* get resize data with average quad-binning */
    int i;
    int j;
    int iRz;
    int jRz;
    for (j = hStart; j < hStart + hBin * hResize; j++)
        for (i = wStart; i < wStart + wBin * wResize; i++) {
            iRz = (i - wStart) / wBin;
            jRz = (j - hStart) / hBin;
            rzData[iRz + wResize * jRz] += data[i + j * w];
        }
    for (i = 0; i < wResize * hResize; i++)
        rzData[i] = rzData[i] / (wBin * hBin);
    *rzW = wResize;
    *rzH = hResize;

    return 0;
}


/***********************************************************
 * AA statistic
 **********************************************************/
template <class T>
int convertAaSttToY(void *buf, int w, int h, T *y, int gain,
        NS3Av3::EBitMode_T mode)
{
    int index = 0;
    int shift = 4;
    if (mode == NS3Av3::EBitMode_10Bit)
        shift = 2;
    else if (mode == NS3Av3::EBitMode_12Bit)
        shift = 4;
    else if (mode == NS3Av3::EBitMode_14Bit)
        shift = 6;
    else if (mode == NS3Av3::EBitMode_16Bit)
        shift = 8;

#if 0
    /* Platform: Kibo+ */
    unsigned short *py = (unsigned short *)buf + w * h * 4;

    /* Platform: Everest, Olympus, Kibo, Alaska, Whitney, Bianco, Vinson */
    unsigned short *py = (unsigned short *)buf + w * h * 2;
#else
#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    /* Platform: Sylvia */
    int i;
    int j;
    unsigned short *py;
    for(j=0;j<h;j++)
    {
        py = ((unsigned short*)buf)+(23*w/2)*j+10*w;
        for(i=0;i<w;i++)
        {
            y[index]=((*py)*gain) >> shift;
            py++;
            index++;
        }
    }
#elif (CAM3_3A_ISP_30_EN)
    /* Platform: 6739 */
    int i;
    int j;
    unsigned char *py;
    for(j=0;j<h;j++)
    {
        py = ((unsigned char*)buf)+((5+AE_TSF_ENABLE*2)*w)*j+4*w;
        for(i=0;i<w;i++)
        {
            y[index]=((*py)*gain);
            py++;
            index++;
        }
    }
#else
    /* Platform: 6765 */
    int j;
    unsigned short *py;
    py = ((unsigned short*)buf)+w*h*2;
    for(int j=0;j<w*h;j++)
    {
        y[index]=((*py)*gain) >> shift;
        py++;
        index++;
    }
#endif
#endif
    return 0;
}
template int convertAaSttToY<short>(void *, int, int, short *, int,
        NS3Av3::EBitMode_T);

template <class T>
int convertAaSttToYrgb(void *buf, int w, int h, T *y, T *r, T *g, T *b,
        NS3Av3::EBitMode_T mode)
{
    int index = 0;
    int shift = 4;
    if (mode == NS3Av3::EBitMode_10Bit)
        shift = 2;
    else if (mode == NS3Av3::EBitMode_12Bit)
        shift = 4;
    else if (mode == NS3Av3::EBitMode_14Bit)
        shift = 6;
    else if (mode == NS3Av3::EBitMode_16Bit)
        shift = 8;

#if 0
    /* Platform: Kibo+ */
    unsigned char *prgb = (unsigned char *)buf;
    unsigned short *py = (unsigned short *)buf + w * h * 4;
    for (i = 0; i < w * h; i++) {
        r[index] = *(prgb + 1);
        g[index] = *(prgb + 3);
        b[index] = *(prgb + 5);
        y[index] = ((*py) >> shift);
        py++;
        prgb += 8;
        index++;
    }

    /* Platform: Everest, Olympus, Kibo, Alaska, Whitney, Bianco, Vinson */
    unsigned char *prgb = (unsigned char *)buf;
    unsigned short *py = (unsigned short *)buf + w * h * 2;
    for (i = 0; i < w * h; i++) {
        r[index] = *prgb;
        g[index] = *(prgb + 1);
        b[index] = *(prgb + 2);
        y[index] = ((*py) >> shift);
        py++;
        prgb += 4;
        index++;
    }
#else
#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
    /* Platform: Sylvia */
    int i;
    int j;
    unsigned short *py;
    unsigned short *prgb;
    for(j=0;j<h;j++)
    {
        prgb = ((unsigned short*)buf)+23*w*j/2;
        py = ((unsigned short*)buf)+(23*w/2)*j+10*w;
        for(i=0;i<w;i++)
        {
            r[index]=(*prgb) >> shift;
            g[index]=(*(prgb+1)) >> shift;
            b[index]=(*(prgb+2)) >> shift;
            y[index]=(*py) >> shift;
            py++;
            prgb+=4;
            index++;
        }
    }
#elif (CAM3_3A_ISP_30_EN)
    /* Platform: 6739 */
    int i;
    int j;
    unsigned char *py;
    unsigned char *prgb;
    for(j=0;j<h;j++)
    {
        prgb = ((unsigned char*)buf)+(5+AE_TSF_ENABLE*2)*w*j;
        py = ((unsigned char*)buf)+((5+AE_TSF_ENABLE*2)*w)*j+4*w;
        for(i=0;i<w;i++)
        {
            r[index]=(*prgb);
            g[index]=(*(prgb+1));
            b[index]=(*(prgb+2));
            y[index]=(*py);
            py++;
            prgb+=4;
            index++;
        }
    }
#else
    /* Platform: 6765 */
    int j;
    unsigned short *py;
    unsigned char *prgb;
    prgb = ((unsigned char*)buf);
    py = ((unsigned short*)buf)+w*h*2;
    for(int j=0;j<w*h;j++)
    {
        r[index]=*prgb;
        g[index]=*(prgb+1);
        b[index]=*(prgb+2);
        y[index]=(*py) >> shift;
        py++;
        prgb+=4;
        index++;
    }
#endif
#endif
    return 0;
}

int convertAaSttToYrgbBmp(void *buf, int w, int h,
        NS3Av3::EBitMode_T mode, const char *yFile, const char *rgbFile)
{
    short *y;
    short *r;
    short *g;
    short *b;
    y = new short[w * h];
    r = new short[w * h];
    g = new short[w * h];
    b = new short[w * h];
    convertAaSttToYrgb(buf, w, h, y, r, g, b, mode);
    arrayToBmp(yFile, y, y, y, w, h);
    arrayToBmp(rgbFile, r, g, b, w, h);
    delete[] y;
    delete[] r;
    delete[] g;
    delete[] b;

    return 0;
}

void get_1_4_range(int v, int &down, int &up)
{
    char value[PROPERTY_VALUE_MAX] = {'\0'};
    property_get(PROP_FLASH_AAO_RATIO, value, "4.0");
    double aaoRatio = atof(value);

    int v_even;
    double v_1_4; // v/4
    int nv_1_4_2; // half of v/4

    v_even = v;
    if (v % 2 == 1)
        v_even--;
    v_1_4 = v_even / aaoRatio;
    nv_1_4_2 = (int)(v_1_4 / 2.0 + 0.5);
    down = v_even / 2 - nv_1_4_2;
    up = v_even / 2 + (nv_1_4_2 - 1);
    if (v % 2 == 1)
        up++;
}

template <class T>
double cal_1_4_mean(T *v, int w, int h)
{
    int i, j;
    int wst, wed;
    int hst, hed;
    int index;
    int count = 0;
    double sum = 0;

    get_1_4_range(w, wst, wed);
    get_1_4_range(h, hst, hed);
    for (i = wst; i <= wed; i++)
        for (j = hst; j <= hed; j++) {
            index = j * w + i;
            sum += v[index];
            count++;
        }

    if (count > 0)
        sum /= count;

    return sum;
}

int cal_1_4_yrgb_mean(void *buf, int w, int h, double *yrgb_mean)
{
    double *y;
    double *r;
    double *g;
    double *b;
    y = new double[w * h];
    r = new double[w * h];
    g = new double[w * h];
    b = new double[w * h];
    convertAaSttToYrgb(buf, w, h, y, r, g, b, NS3Av3::EBitMode_12Bit);
    /* Warning: yrgb size must >= 4 */
    yrgb_mean[0] = cal_1_4_mean(y, w, h);
    yrgb_mean[1] = cal_1_4_mean(r, w, h);
    yrgb_mean[2] = cal_1_4_mean(g, w, h);
    yrgb_mean[3] = cal_1_4_mean(b, w, h);
    delete[] y;
    delete[] r;
    delete[] g;
    delete[] b;
    return 0;
}


/***********************************************************
 * Project Parameters
 **********************************************************/
void dumpProjectPara(FLASH_PROJECT_PARA *pp)
{
    /* verify arguments */
    if (!pp) {
        logE("dumpProjectPara(): error arguments.");
        return;
    }

    /* project parameters */
    logI("dumpProjectPara():");
    logI("dumpProjectPara(): dutyNum(%d).", pp->dutyNum);
    logI("dumpProjectPara(): maxCapExpTimeUs(%d us).", pp->maxCapExpTimeUs);

    /* tuning parameters */
    FLASH_TUNING_PARA *pt = &pp->tuningPara;
    logI("tuning parameters(%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d).",
            pt->yTarget,                     // 188 (10bit)
            pt->fgWIncreaseLevelbySize,      // 10
            pt->fgWIncreaseLevelbyRef,       // 5
            pt->ambientRefAccuracyRatio,     // 5 (5/256=2%)
            pt->flashRefAccuracyRatio,       // 0 (0/256=0%)
            pt->backlightAccuracyRatio,      // 18 (18/256=7%)
            pt->backlightUnderY,             // 40
            pt->backlightWeakRefRatio,       // 32
            pt->safetyExp,                   // 666444
            pt->maxUsableISO,                // 1200
            pt->yTargetWeight,               // 0 (base:256)
            pt->lowReflectanceThreshold,     // 13 (13/256=5%)
            pt->flashReflectanceWeight,      // 0 (base:256)
            pt->bgSuppressMaxDecreaseEV,     // 20
            pt->bgSuppressMaxOverExpRatio,   // 6 (6/256=2%)
            pt->fgEnhanceMaxIncreaseEV,      // 50
            pt->fgEnhanceMaxOverExpRatio,    // 2
            pt->isFollowCapPline,            // 1
            pt->histStretchMaxFgYTarget,     // 300 (10bit)
            pt->histStretchBrightestYTarget, // 480 (10bit)
            pt->fgSizeShiftRatio,            // 0
            pt->backlitPreflashTriggerLV,    // 90
            pt->backlitMinYTarget,           // 90
            pt->minstameanpass,              // 80
            pt->yDecreEVTarget,              // 188
            pt->yFaceTarget,                 // 188
            pt->cfgFlashPolicy               // 5

                );

    /* cooling time and timeout parameters */
    FLASH_COOL_TIMEOUT_PARA *pct = &pp->coolTimeOutPara;
    logI("dumpProjectPara(): tabNum(%d).", pct->tabNum);

    logI("dumpProjectPara(): tabId(%d %d %d %d %d %d %d %d %d %d).",
            pct->tabId[0], pct->tabId[1], pct->tabId[2], pct->tabId[3],
            pct->tabId[4], pct->tabId[5], pct->tabId[6], pct->tabId[7],
            pct->tabId[8], pct->tabId[9]);

    logI("dumpProjectPara(): coolingTM(%f %f %f %f %f %f %f %f %f %f).",
            pct->coolingTM[0], pct->coolingTM[1], pct->coolingTM[2], pct->coolingTM[3],
            pct->coolingTM[4], pct->coolingTM[5], pct->coolingTM[6], pct->coolingTM[7],
            pct->coolingTM[8], pct->coolingTM[9]);

    logI("dumpProjectPara(): timOutMs(%d %d %d %d %d %d %d %d %d %d).",
            pct->timOutMs[0], pct->timOutMs[1], pct->timOutMs[2], pct->timOutMs[3],
            pct->timOutMs[4], pct->timOutMs[5], pct->timOutMs[6], pct->timOutMs[7],
            pct->timOutMs[8], pct->timOutMs[9]);
}

#if (!CAM3_3A_ISP_30_EN && !CAM3_3A_ISP_40_EN)
/***********************************************************
 * Copy Project Parameters From NVRAM
 **********************************************************/
void copyTuningPara(FLASH_TUNING_PARA* p, NVRAM_FLASH_TUNING_PARA* nv_p)
{
    p->yTarget = nv_p->yTarget;
    p->fgWIncreaseLevelbySize = nv_p->fgWIncreaseLevelbySize;
    p->fgWIncreaseLevelbyRef = nv_p->fgWIncreaseLevelbyRef;
    p->ambientRefAccuracyRatio = nv_p->ambientRefAccuracyRatio;
    p->flashRefAccuracyRatio = nv_p->flashRefAccuracyRatio;
    p->backlightAccuracyRatio = nv_p->backlightAccuracyRatio;
    p->backlightUnderY = nv_p->backlightUnderY;
    p->backlightWeakRefRatio = nv_p->backlightWeakRefRatio;
    p->safetyExp = nv_p->safetyExp;
    p->maxUsableISO = nv_p->maxUsableISO;
    p->yTargetWeight = nv_p->yTargetWeight;
    p->lowReflectanceThreshold = nv_p->lowReflectanceThreshold;
    p->flashReflectanceWeight = nv_p->flashReflectanceWeight;
    p->bgSuppressMaxDecreaseEV = nv_p->bgSuppressMaxDecreaseEV;
    p->bgSuppressMaxOverExpRatio = nv_p->bgSuppressMaxOverExpRatio;
    p->fgEnhanceMaxIncreaseEV = nv_p->fgEnhanceMaxIncreaseEV;
    p->fgEnhanceMaxOverExpRatio = nv_p->fgEnhanceMaxOverExpRatio;
    p->isFollowCapPline = nv_p->isFollowCapPline;
    p->histStretchMaxFgYTarget = nv_p->histStretchMaxFgYTarget;
    p->histStretchBrightestYTarget = nv_p->histStretchBrightestYTarget;
    p->fgSizeShiftRatio = nv_p->fgSizeShiftRatio;
    p->backlitPreflashTriggerLV = nv_p->backlitPreflashTriggerLV;
    p->backlitMinYTarget = nv_p->backlitMinYTarget;
    p->minstameanpass = nv_p->minstameanpass;
    p->yDecreEVTarget = nv_p->yDecreEVTarget;
    p->yFaceTarget = nv_p->yFaceTarget;
    p->cfgFlashPolicy = nv_p->cfgFlashPolicy;
    p->enablePreflashAE = 0;

    ALOGD("%s(): yTarget(%d), policy(%d).", __FUNCTION__, p->yTarget, p->cfgFlashPolicy);
}

void copyTuningParaDualFlash(FLASH_TUNING_PARA* p, NVRAM_DUAL_FLASH_TUNING_PARA* nv)
{
    p->dualFlashPref.toleranceEV_pos = nv->toleranceEV_pos;
    p->dualFlashPref.toleranceEV_neg = nv->toleranceEV_neg;
    p->dualFlashPref.XYWeighting = nv->XYWeighting;
    p->dualFlashPref.useAwbPreferenceGain = nv->useAwbPreferenceGain;
    for (int i = 0; i < 4; i++) {
        p->dualFlashPref.envOffsetIndex[i] = nv->envOffsetIndex[i];
        p->dualFlashPref.envXrOffsetValue[i] = nv->envXrOffsetValue[i];
        p->dualFlashPref.envYrOffsetValue[i] = nv->envYrOffsetValue[i];
    }
    p->dualFlashPref.VarianceTolerance = nv->VarianceTolerance;
    p->dualFlashPref.ChooseColdOrWarm = nv->ChooseColdOrWarm;
}
#endif
