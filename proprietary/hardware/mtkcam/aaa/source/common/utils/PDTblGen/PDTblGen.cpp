/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#define LOG_TAG "PDTblGen"

#include <functional>
#include <algorithm>
#include <map>
#include <iostream>
#include <sys/stat.h>

//
#include "private/PDTblGen.h"

// sensor information
#include <kd_imgsensor_define.h>
#include <mtkcam/drv/IHalSensor.h>

// log
#include <mtkcam/utils/std/Log.h>

// hw buffer
#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>

// isp information
#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>

//
#include <pd_buf_common.h>

// property
#include <cutils/properties.h>

using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSCamIOPipe;


class PDTblGenImpl : public IPDTblGen
{
    enum PD_TYPE
    {
        PD_TYPE_L,
        PD_TYPE_R,
        PD_TYPE_NUM
    };

    struct point
    {
        unsigned int x;
        unsigned int y;
        PD_TYPE type;

        point() :
            x(0),
            y(0),
            type(PD_TYPE_NUM)
        {}

        point(unsigned int _x, unsigned int _y, PD_TYPE _type) :
            x(_x),
            y(_y),
            type(_type)
        {}

        point& operator =(point &_in)
        {
            x=_in.x;
            y=_in.y;
            type=_in.type;
            return (*this);
        }
        bool   operator <=(point &_in)
        {
            return ((y<_in.y)||(y==_in.y && x<_in.x));
        }
    };

    struct TblBuf
    {
        TblInfo tblInfo;
        IImageBuffer *tblmem;

        TblBuf() :
            tblInfo(),
            tblmem(nullptr)
        {}

        TblBuf& operator =(TblBuf &_in)
        {
            tblInfo = _in.tblInfo;
            tblmem  = _in.tblmem;
            return (*this);
        }

        TblBuf( TblInfo &_tblInfo, IImageBuffer *_tblmem)
        {
            tblInfo = _tblInfo;
            tblmem  = _tblmem;
        }

    };

    struct TblData
    {
        TblBuf full;
        TblBuf bin;

        TblData() :
            full(),
            bin()
        {}

    };


private:
    int mDgbLogLv;
    map<int, map<int, TblData>> mTblBufMap;
    IHalSensor* mIHalSensor;
    MSize mSz_TG;
    MSize mSz_BIN;
    mutable Mutex mLock;

    /**
     * @Brief :
     *          Arrary of PD pixels' coordinate is sorted by ascendant order.
     * @Param :
     *          [ in] src   : source arrary of pd pixels' coordinate.
     *          [ in] begin : define the begin index of sorting range in source arrary.
     *          [ in] end   : define the end index of sorting range in source arrary.
     *          [ in] dst   : destination arrary.
     * @Return:
     *          [out] NA.
     */
    void mergeSort( point *src,
                    unsigned char begin,
                    unsigned char end,
                    point *dst);

    /**
     * @Brief :
     *          Table generator.
     * @Param :
     *          [ in] numblkX    : describe how many repeated PD blocks in X axis.
     *          [ in] numblkY    : describe how many repeated PD blocks in Y axis.
     *          [ in] pitchX     : describe x size for one PD block.
     *          [ in] pitchY     : describe y size for one PD block.
     *          [ in] numPixs    : describe how many PD pixels in one PD block.
     *          [ in] sortedPixs : PD pixles' coordinate which is sorted by ascendant order.
     * @Return:
     *          [out] table information.
     */
    PDTblGenImpl::TblBuf generateTbl( unsigned short numblkX,
                                      unsigned short numblkY,
                                      unsigned short pitchX,
                                      unsigned short pitchY,
                                      unsigned short numPixs,
                                      point *sortedPixs);
    /**
     * @Brief :
     *          Transform PD pixels' coordinate from 4:3 coordinate to current coordinate which is corresponding to sensor mode.
     *          The information, pd_info, is quired from sensor driver directly.
     * @Param :
     *          [ in] pd_info    : PD block information.
     *          [ in] scenario   : Current sensor scenario, and it is used to query crop information.
     *          [ in] numPixs    : Describe how many PD pixels in one PD block.
     *          [out] outAllPixs : Output PD pixles' coordinate which is applied crop, mirror and flip information.
     * @Return:
     *          [out] true  : information is correct inside pd_info
     *                false : information is not correct inside pd_info, please check sensor dirver setting.
     */
    bool transformCoordinate( SET_PD_BLOCK_INFO_T &pd_info,
                              int const scenario,
                              unsigned short numPixs,
                              point *outAllPixs);

public:
    /**
     * @Brief :
     *          Default constructor
     * @Param :
     *          [ in] NA.
     * @Return:
     *          [out] NA.
     */
    PDTblGenImpl();

    /**
     * @Brief :
     *          Default destructor
     * @Param :
     *          [ in] NA.
     * @Return:
     *          [out] NA.
     */
    virtual ~PDTblGenImpl();

    /**
     * @Brief :
     *          Interface : Start pd table generate utility
     * @Param :
     *          [ in] sensorDev : It is quired from sensor driver by using IHalSensorList API.
     *          [ in] sensorIdx : provide by MW
     * @Return:
     *          [out] status.
     */
    virtual bool start( int const sensorDev, int const sensorIdx);

    /**
     * @Brief :
     *          Interface : Stop pd table generate utility
     * @Param :
     *          [ in] sensorDev.
     * @Return:
     *          [out] status.
     */
    virtual bool stop( int const sensorDev);

    /**
     * @Brief :
     *          Interface : get BPCI tables for frontal binning module is disable and enable.
     * @Param :
     *          [ in] sensorDev
     *          [ in] scenario
     *          [out] outTbl : tables for disabling and enabling frontal binning module.
     * @Return:
     *          [out] status.
     */
    virtual bool getTbl( int const sensorDev, int const scenario, Tbl &outTbl);
};

/******************************************************************************
 *
 ******************************************************************************/
static INormalPipeModule* getNormalPipeModule()
{
    static auto pModule = INormalPipeModule::get();
    CAM_LOGE_IF(!pModule, "INormalPipeModule::get() fail");
    return pModule;
}

static MVOID* createDefaultNormalPipe(MUINT32 sensorIndex, char const* szCallerName)
{
    auto pModule = getNormalPipeModule();
    if  ( ! pModule )
    {
        CAM_LOGE("getNormalPipeModule() fail");
        return NULL;
    }

    //  Select CamIO version
    size_t count = 0;
    MUINT32 const* version = NULL;
    int err = pModule->get_sub_module_api_version(&version, &count, sensorIndex);
    if  ( err < 0 || ! count || ! version )
    {
        CAM_LOGE(
            "[%d] INormalPipeModule::get_sub_module_api_version - err:%#x count:%zu version:%p",
            sensorIndex, err, count, version
        );
        return NULL;
    }

    MUINT32 const selected_version = *(version + count - 1); //Select max. version
    CAM_LOGD("[%d] count:%zu Selected CamIO Version:%0#x", sensorIndex, count, selected_version);

    MVOID* pPipe = NULL;
    pModule->createSubModule(sensorIndex, szCallerName, selected_version, (MVOID**)&pPipe);
    return pPipe;
}

/******************************************************************************
 *                                  IPDTblGen
 ******************************************************************************/
IPDTblGen* IPDTblGen::getInstance()
{
    static PDTblGenImpl	singleton;
    return &singleton;
}

/******************************************************************************
 *                                 PDTblGenImpl
 ******************************************************************************/
PDTblGenImpl::PDTblGenImpl() :
    mDgbLogLv(0),
    mIHalSensor(nullptr),
    mSz_TG(0,0),
    mSz_BIN(0,0)
{}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PDTblGenImpl::~PDTblGenImpl()
{
    //buffer are distroyed when cameraserver is destroyed.
    for( map<int, map<int, TblData>>::iterator itrBufMap=mTblBufMap.begin(); itrBufMap!=mTblBufMap.end(); itrBufMap++)
    {
        for( map<int, TblData>::iterator itrTblData=itrBufMap->second.begin(); itrTblData!=itrBufMap->second.end(); itrTblData++)
        {
            //unlock buffer until free buffer
            std::string strName = LOG_TAG;
            itrTblData->second.full.tblmem->unlockBuf(strName.c_str());
            itrTblData->second.bin.tblmem->unlockBuf(strName.c_str());

            // clean all stored tables when camera service is destroyed.
            sp<IImageBuffer> spfulltbl = itrTblData->second.full.tblmem;
            sp<IImageBuffer> spbintbl  = itrTblData->second.bin.tblmem;
            itrTblData->second.full.tblmem = NULL;
            itrTblData->second.bin.tblmem  = NULL;
        }

        // clean all tables when camera service is destroyed.
        itrBufMap->second.clear();
    }
    mTblBufMap.clear();
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
bool PDTblGenImpl::start( int const sensorDev, int const sensorIdx)
{
    Mutex::Autolock lock( mLock);

    bool ret=false;

    mDgbLogLv = property_get_int32("vendor.debug.pdinfo.enable", 0);

    // get IHalSensor instance
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    // get normalPipe instance
    INormalPipe* const normalPipe = (INormalPipe*)createDefaultNormalPipe( sensorIdx, LOG_TAG);

    //
    if( normalPipe)
    {
        if( pIHalSensorList)
        {
            // query pd sensor type
            SensorStaticInfo sensorStaticInfo;
            pIHalSensorList->querySensorStaticInfo( sensorDev, &sensorStaticInfo);
            IMGSENSOR_PDAF_SUPPORT_TYPE_ENUM pdaf_support = (IMGSENSOR_PDAF_SUPPORT_TYPE_ENUM)sensorStaticInfo.PDAF_Support;

            if( /* checked pd sensor type */
                pdaf_support==PDAF_SUPPORT_RAW ||
                pdaf_support==PDAF_SUPPORT_CAMSV ||
                pdaf_support==PDAF_SUPPORT_CAMSV_LEGACY ||
                pdaf_support==PDAF_SUPPORT_RAW_LEGACY)
            {
                //get pd buffer type from custom setting.
                MUINT32 pdBufType = GetPDBuf_Type( sensorDev, sensorStaticInfo.sensorDevID);

                CAM_LOGD("dev[%d] sensor ID(0x%x) pd buf type(0x%x) pd sensor type(%d)",
                         sensorDev,
                         sensorStaticInfo.sensorDevID,
                         pdBufType,
                         pdaf_support);

                if( /* checking supporting pd buffer type */
                    ((pdBufType&MSK_CATEGORY_OPEN  )==0) &&
                    ((pdBufType&MSK_CATEGORY_DUALPD)==0))
                {

                    mIHalSensor = pIHalSensorList->createSensor( LOG_TAG, sensorDev);

                    // HalSensor must be successuflly created, because pd block information must be queried from sensor driver.
                    if( mIHalSensor)
                    {
                        bool res;
                        // TG size
                        res = normalPipe->sendCommand( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_TG_OUT_SIZE, (MINTPTR)(&mSz_TG.w), (MINTPTR)(&mSz_TG.h), 0);
                        // TG after BIN Blk size : for HPF coordinate setting.
                        res = normalPipe->sendCommand( NSCam::NSIoPipe::NSCamIOPipe::ENPipeCmd_GET_BIN_INFO, (MINTPTR)(&mSz_BIN.w), (MINTPTR)(&mSz_BIN.h), 0);

                        // find device. If devices is not found, add a new device
                        if(mTblBufMap.find(sensorDev)!= mTblBufMap.end())
                        {
                            CAM_LOGD("dev[%d] is started. update information : TG(%d, %d) BIN(%d %d)\n", sensorDev, mSz_TG.w, mSz_TG.h, mSz_BIN.w, mSz_BIN.h);
                        }
                        else
                        {
                            CAM_LOGD("dev[%d] create device. TG(%d, %d) BIN(%d %d)\n", sensorDev, mSz_TG.w, mSz_TG.h, mSz_BIN.w, mSz_BIN.h);
                            mTblBufMap.insert( pair<int, map<int, TblData>>(sensorDev, map<int, TblData>()));
                        }

                        ret = true;
                    }
                    else
                    {
                        ret = false;
                        CAM_LOGE( "dev[%d] Fail to create instance IHalSensor(%p)", sensorDev, mIHalSensor);
                    }
                }
                else
                {
                    ret = false;
                    CAM_LOGD( "dev[%d] Not supported pd buffer type(0x%x)", sensorDev, pdBufType);
                }
            }
            else
            {
                ret = false;
                CAM_LOGD( "dev[%d] Not supported pd sensor type(%d)", sensorDev, pdaf_support);
            }
        }
        else
        {
            ret = false;
            CAM_LOGE( "dev[%d] Get HalSensorList fail %p", sensorDev, pIHalSensorList);
        }
        normalPipe->destroyInstance( LOG_TAG);
    }
    else
    {
        ret = false;
        CAM_LOGE( "dev[%d] Fail to create instance normalPipe(%p)", sensorDev, normalPipe);
    }

    if( ret==false)
    {
        CAM_LOGD( "dev[%d] module is not started!!", sensorDev);
    }
    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
bool PDTblGenImpl::stop( int const sensorDev)
{
    Mutex::Autolock lock( mLock);

    if( mIHalSensor)
    {
        mIHalSensor->destroyInstance(LOG_TAG);
        mIHalSensor = nullptr;
    }

    CAM_LOGD( "dev[%d] stop module", sensorDev);
    return true;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
bool PDTblGenImpl::getTbl( int const sensorDev, int const scenario, Tbl &outTbl)
{
    Mutex::Autolock lock( mLock);

    bool ret = false;
    memset( &outTbl, 0, sizeof(Tbl));

    // if device is not started correctly, return directly
    if( !mIHalSensor)
    {
        CAM_LOGD_IF(mDgbLogLv, "dev(%d) is not started can not get IHalSensor, scenario(%d)\n", sensorDev, scenario);
    }
    else
    {
        // Get pd block information from sensor driver.
        SET_PD_BLOCK_INFO_T pd_info;
        memset( &pd_info, 0, sizeof(SET_PD_BLOCK_INFO_T));
        mIHalSensor->sendCommand( sensorDev, SENSOR_CMD_GET_SENSOR_PDAF_INFO, (MINTPTR)&scenario, (MINTPTR)&pd_info, 0);

        /*******************************************
         * Try to find tables' information from data struct.
         * If table is not found, generating tables for enabling and disabling frontal binning mode.
         * After tables are generated, talbes are stored by camera scenario.
         *******************************************/
        map<int, map<int, TblData>>::iterator itrDev = mTblBufMap.find(sensorDev);
        if( itrDev==mTblBufMap.end())
        {
            CAM_LOGD_IF(mDgbLogLv, "dev(%d) scenario(%d) device is not started, no tables are in data base\n", sensorDev, scenario);
        }
        else
        {
            ret = true;

            map<int, TblData>::iterator itrTbl = itrDev->second.find(scenario);

            if( itrTbl==itrDev->second.end())
            {
                /*******************************************
                 * Table is not calculated in this scenario.
                 * Both full size and binning size tables are calculated.
                 * Tables are stored after calculation.
                 *******************************************/
                CAM_LOGD("dev(%d) scenario(%d) generating table\n", sensorDev, scenario);

                //
                unsigned short numPair    = pd_info.i4PairNum;
                unsigned short numAllPixs = 2*numPair;

                // allocate resource
                point *allPixs = new point[numAllPixs];
                point *allPixsSorted = new point[numAllPixs];

                // 1. coordinate pre-processing and checking PD block information
                ret = transformCoordinate( pd_info, scenario, numAllPixs, allPixs);
                if( ret)
                {
                    //2. sorting pd pixels' coordinate
                    memcpy( allPixsSorted, allPixs, sizeof(point)*numAllPixs); // initial working buffer
                    mergeSort(allPixs, 0, numAllPixs, allPixsSorted);

                    if( mDgbLogLv)
                    {
                        for(unsigned int i=0; i<numAllPixs; i++)
                        {
                            CAM_LOGD("%2d, x:%3d, y:%3d, type:%d\n", i, allPixsSorted[i].x,  allPixsSorted[i].y,  allPixsSorted[i].type);
                        }
                    }

                    // 3. generating full table
                    TblBuf tblBufFull = generateTbl( pd_info.i4BlockNumX,
                                                     pd_info.i4BlockNumY,
                                                     pd_info.i4PitchX,
                                                     pd_info.i4PitchY,
                                                     numAllPixs,
                                                     allPixsSorted);

                    // 4. calculating pd coordinator when frontal binning is enabled
                    point *allPixsSortedBin = allPixs;
                    for( unsigned short i=0; i<numAllPixs; i++)
                    {
                        unsigned int  xdis = allPixsSorted[i].x+1;
                        unsigned int  xgup = xdis>>2;
                        unsigned int  xoff = xdis-(xgup<<2);
                        unsigned int _xdis = (xgup<<1) + ( xoff ? (xoff-1)%2+1 : 0);

                        unsigned int  ydis = allPixsSorted[i].y+1;
                        unsigned int  ygup = ydis>>2;
                        unsigned int  yoff = ydis-(ygup<<2);
                        unsigned int _ydis = (ygup<<1) + ( yoff ? (yoff-1)%2+1 : 0);

                        point binCoordinate( _xdis-1, _ydis-1, allPixsSorted[i].type);
                        allPixsSortedBin[i] = binCoordinate;
                    }
                    if( mDgbLogLv)
                    {
                        for(unsigned int i=0; i<numAllPixs; i++)
                        {
                            CAM_LOGD("%2d, x:%3d, y:%3d, type:%d\n", i, allPixsSortedBin[i].x,  allPixsSortedBin[i].y,  allPixsSortedBin[i].type);
                        }
                    }

                    // 5. generating binning table
                    TblBuf tblBufBin = generateTbl( pd_info.i4BlockNumX,
                                                    pd_info.i4BlockNumY,
                                                    pd_info.i4PitchX>>1,
                                                    pd_info.i4PitchY>>1,
                                                    numAllPixs,
                                                    allPixsSortedBin);

                    // 6. store tables
                    itrDev->second.insert( pair<int, TblData>(scenario, TblData()));
                    itrDev->second[scenario].full = tblBufFull;
                    itrDev->second[scenario].bin  = tblBufBin;

                    // output
                    outTbl.tbl      = itrDev->second[scenario].full.tblInfo;
                    outTbl.tbl_bin  = itrDev->second[scenario].bin.tblInfo;
                    outTbl.pbuf     = itrDev->second[scenario].full.tblmem;
                    outTbl.pbuf_bin = itrDev->second[scenario].bin.tblmem;
                }
                else
                {
                    CAM_LOGW("coordinate pre-processing and checking PD block information fail\n");
                }

                // delete resource
                delete []allPixs;
                delete []allPixsSorted;
            }
            else
            {
                CAM_LOGD_IF(mDgbLogLv, "dev(%d) scenario(%d) Read from data base directly\n", sensorDev, scenario);
                outTbl.tbl     = itrDev->second[scenario].full.tblInfo;
                outTbl.tbl_bin = itrDev->second[scenario].bin.tblInfo;
                outTbl.pbuf     = itrDev->second[scenario].full.tblmem;
                outTbl.pbuf_bin = itrDev->second[scenario].bin.tblmem;
            }
        }
    }


    // debug
    if(ret)
    {
        if(mDgbLogLv)
        {
            //
            MINT32 err = 0;

            err = mkdir( "/sdcard/pdtbl", S_IRWXU | S_IRWXG | S_IRWXO);
            CAM_LOGD( "create folder /sdcard/pdtbl for dump data. status(%d)", err);

            //
            char fileName[256];
            FILE *fp = nullptr;

            sprintf(fileName,
                    "/sdcard/pdtbl/pd_tbl_dev_%d_scenario_%d_sz_%d_full",
                    sensorDev,
                    scenario,
                    (outTbl.tbl.tbl_xsz+1));
            fp = fopen(fileName, "w");
            if( fp)
            {
                CAM_LOGD( "dump file : %s", fileName);
                fwrite( reinterpret_cast<void *>(outTbl.tbl.tbl_va), 1, (outTbl.tbl.tbl_xsz+1), fp);
                fclose( fp);
            }

            sprintf(fileName,
                    "/sdcard/pdtbl/pd_tbl_dev_%d_scenario_%d_sz_%d_bin",
                    sensorDev,
                    scenario,
                    (outTbl.tbl_bin.tbl_xsz+1));
            fp = fopen(fileName, "w");
            if( fp)
            {
                CAM_LOGD( "dump file : %s", fileName);
                fwrite( reinterpret_cast<void *>(outTbl.tbl_bin.tbl_va), 1, (outTbl.tbl_bin.tbl_xsz+1), fp);
                fclose( fp);
            }
        }
    }

    return ret;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void
PDTblGenImpl::mergeSort( point *src,
                         unsigned char begin,
                         unsigned char end,
                         point *dst)
{
    if(end - begin < 2)
        return;

    unsigned char middle = (begin + end) / 2;

    // recursively sort
    mergeSort(dst, begin,  middle, src);
    mergeSort(dst, middle,    end, src);

    // merge the resulting
    unsigned char i = begin, j = middle;
    for(unsigned char k = begin; k < end; k++)
    {
        if(i<middle && (j>=end || src[i]<=src[j]))
        {
            dst[k] = src[i];
            i = i + 1;
        }
        else
        {
            dst[k] = src[j];
            j = j + 1;
        }
    }
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
PDTblGenImpl::TblBuf
PDTblGenImpl::generateTbl( unsigned short numblkX,
                           unsigned short numblkY,
                           unsigned short pitchX,
                           unsigned short pitchY,
                           unsigned short numPixs,
                           point *sortedPixs)
{
    /*********************
     * calculate table size
     *********************/
    unsigned int tbl_xsz = 0;
    for(unsigned int curIdx=0, nxtIdx=0; curIdx<numPixs; )
    {
        // Define the index range of sorted pixels' array, and y coordinate is the same in this range
        for(nxtIdx=curIdx; nxtIdx<numPixs; nxtIdx++)
        {
            if(sortedPixs[nxtIdx].y==sortedPixs[curIdx].y)
                continue;
            else
                break;
        }

        CAM_LOGD("curIdx(%3d) nxtIdx(%3d) y_coord(%4d)", curIdx, nxtIdx, sortedPixs[curIdx].y);

        // for each row
        tbl_xsz+=8;

        // for each PD in the row
        for(unsigned int i=curIdx; i<nxtIdx; i++)
            tbl_xsz+=2;

        // next pitch
        curIdx = nxtIdx;
    }
    tbl_xsz *= numblkY;

    CAM_LOGD("numblk(%d,%d) pitch_sz(%d, %d) tbl_xsz(%d)", numblkX, numblkY, pitchX, pitchY, tbl_xsz);


    /*********************
     * create buffer
     *********************/
    TblInfo retInfo;
    IImageBuffer *retBuf = nullptr;

    unsigned int buf_Stride_InBytes[3]   = {tbl_xsz, 0, 0};
    unsigned int buf_Boundary_InBytes[3] = {0, 0, 0};
    IImageBufferAllocator::ImgParam bufParam((EImageFormat)eImgFmt_STA_BYTE, MSize(tbl_xsz, 1), buf_Stride_InBytes, buf_Boundary_InBytes, 1);

    std::string strName = LOG_TAG;
    sp<IIonImageBufferHeap> pHeap = IIonImageBufferHeap::create(strName.c_str(), bufParam);

    if( pHeap!=nullptr)
    {
        retBuf = pHeap->createImageBuffer();

        if( retBuf!=nullptr)
        {
            // lock buffer : Before access buffer, buffer should be locked.
            if( retBuf->lockBuf(strName.c_str(), (eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ | eBUFFER_USAGE_HW_CAMERA_WRITE)))
            {
                //
                unsigned short min_pd_roi_x = 0xFFFF;
                unsigned short min_pd_roi_y = 0xFFFF;
                unsigned short max_pd_roi_x = 0x0;
                unsigned short max_pd_roi_y = 0x0;

                /*********************
                 * generate table information
                 *********************/
                char *bpc_table = (char *) retBuf->getBufVA(0);

                unsigned int pdo_ysz=0;
                unsigned int max_x_num_per_pitch=0;
                unsigned int table_index = 0;
                unsigned short *ptrTbl = reinterpret_cast<unsigned short*>(bpc_table);

                for(unsigned int it=0; it<numblkY; it++)  // numbers of repeating PD_BLOCK in vertical direction
                {

                    for(unsigned int curIdx=0, nxtIdx=0; curIdx<numPixs; )
                    {
                        // Define the index range of sorted pixels' array, and y coordinate is the same in this range
                        for(nxtIdx=curIdx; nxtIdx<numPixs; nxtIdx++)
                        {
                            if(sortedPixs[nxtIdx].y==sortedPixs[curIdx].y)
                                continue;
                            else
                                break;
                        }


                        //C:  PD table(8: BPC table) XXX:  y coordinate = current_y  XX CX
                        unsigned short current_y = sortedPixs[curIdx].y + it*pitchY;
                        ptrTbl[table_index++] = 0xC000|(0x3FFF&current_y);
                        pdo_ysz++;

                        //0:  PD table(8: BPC table) XXX:  x coordinate = current_x  XX 0X
                        unsigned short current_x = sortedPixs[curIdx].x;
                        ptrTbl[table_index++] = 0x0000|(0x3FFF&current_x);

                        //1069 defines the total pixel length between first PD and the last PD. In this case, 1069(HEX) = 4201 pixel width
                        unsigned short x_total_distance = (numblkX-1)*pitchX + (sortedPixs[nxtIdx-1].x-sortedPixs[curIdx].x) + 1;
                        ptrTbl[table_index++] = 0x0000|(0x3FFF&x_total_distance);

                        //0001 number of PD types in current line = PNUM(0001) + 1 = 2 PD types (L and R, list in the following lines)
                        // from current_idx to index-1 ((index-1)-current_idx+1)  +1 means for the next pitch's first pd; -1: represent 1 for 2 kind of PDs
                        unsigned short x_num_per_pitch = nxtIdx-curIdx;
                        ptrTbl[table_index++] = 0x0000|(0x3FFF&(x_num_per_pitch-1));

                        //for each PD
                        for(unsigned int i=curIdx; i<nxtIdx; i++)
                        {
                            //4008  P1: distance between the first PD (R type) and the second PD (8 pixel)   R:4 L:0
                            unsigned int dis = (i==nxtIdx-1)                                   ?
                                               (pitchX+sortedPixs[curIdx].x) - sortedPixs[i].x :
                                               (sortedPixs[i+1].x-sortedPixs[i].x);

                            ptrTbl[table_index++] = sortedPixs[i].type==PD_TYPE_R ?
                                                    0x4000|(0x3FFF&dis) :
                                                    0x0000|(0x3FFF&dis);
                        }


                        curIdx = nxtIdx;
                        max_x_num_per_pitch = x_num_per_pitch>max_x_num_per_pitch ? x_num_per_pitch : max_x_num_per_pitch;

                        //Debug information
                        min_pd_roi_x = (current_x<min_pd_roi_x) ? current_x : min_pd_roi_x;
                        max_pd_roi_x = (max_pd_roi_x<(x_total_distance+sortedPixs[curIdx].x)) ? x_total_distance+sortedPixs[curIdx].x : max_pd_roi_x;
                        min_pd_roi_y = (current_y<min_pd_roi_y) ? current_y : min_pd_roi_y;
                        max_pd_roi_y = (max_pd_roi_y<current_y) ? current_y : max_pd_roi_y;
                    }
                }

                CAM_LOGD("pd_roi : minX(%d), minY(%d), maxX(%d), maxY(%d)\n", min_pd_roi_x, min_pd_roi_y, max_pd_roi_x, max_pd_roi_y);

                // maximum pd pixel in a line (numbers of pixel-1)* 2byte/pixel
                unsigned int x_num_per_row = max_x_num_per_pitch*numblkX;
                unsigned int pdo_xsz       = x_num_per_row*2;//((((bpci_info.PDO_XSIZE)>>4)<<4)+15)*2;

                retInfo.memID   = (unsigned int) retBuf->getFD(0);
                retInfo.tbl_xsz = (unsigned int) tbl_xsz-1; //BYTE
                retInfo.tbl_ysz = (unsigned int) 0;
                retInfo.tbl_pa  = (void*) retBuf->getBufPA(0);
                retInfo.tbl_va  = (void*) retBuf->getBufVA(0);
                retInfo.pdo_xsz = (unsigned int) pdo_xsz-1;
                retInfo.pdo_ysz = (unsigned int) pdo_ysz-1;

                CAM_LOGD("[%s] bpci tbl info : memID(%d) tbl_xsz(%d) tbl_ysz(%d) tbl_pa(%p) tbl_va(%p) pdo_xsz(%d) pdo_ysz(%d), buffer is lock by %s property(0x%x)",
                         __FUNCTION__,
                         retInfo.memID,
                         retInfo.tbl_xsz,
                         retInfo.tbl_ysz,
                         retInfo.tbl_pa,
                         retInfo.tbl_va,
                         retInfo.pdo_xsz,
                         retInfo.pdo_ysz,
                         strName.c_str(),
                         (eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READ | eBUFFER_USAGE_HW_CAMERA_WRITE));
            }
            else
            {
                CAM_LOGE("[%s] ImageBuffer lock fail",  strName.c_str());

            }
        }
        else
        {
            CAM_LOGE("[%s] pHeap->createImageBuffer fail %d", strName.c_str(), tbl_xsz);
        }
    }
    else
    {
        CAM_LOGE("[%s] IIonImageBufferHeap::create fail %d", strName.c_str(), tbl_xsz);
    }

    return TblBuf( retInfo, retBuf);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
bool
PDTblGenImpl::transformCoordinate( SET_PD_BLOCK_INFO_T &pd_info,
                                   int const scenario,
                                   unsigned short numPixs,
                                   point *outAllPixs)
{
    memset(outAllPixs, 0, sizeof(point)*numPixs);

    unsigned int pitch_x    = pd_info.i4PitchX;
    unsigned int pitch_y    = pd_info.i4PitchY;
    unsigned int nblk_x     = pd_info.i4BlockNumX;
    unsigned int nblk_y     = pd_info.i4BlockNumY;
    unsigned int pairNum    = pd_info.i4PairNum;
    unsigned int full_lOffx = pd_info.i4OffsetX;
    unsigned int full_lOffy = pd_info.i4OffsetY;

    CAM_LOGD( "SensorMode(%d), SensorInfo : OffsetX(%d), OffsetY(%d), PitchX(%d), PitchY(%d), PairNu(%d), SubBlkW(%d), SubBlkH(%d), BlockNumX(%d), BlockNumY(%d), LeFirst(%d)",
              scenario,
              full_lOffx,
              full_lOffy,
              pitch_x,
              pitch_y,
              pairNum,
              pd_info.i4SubBlkW,
              pd_info.i4SubBlkH,
              nblk_x,
              nblk_y,
              pd_info.i4LeFirst);

    /*******************************************************************************
     * basic check pd block information.
     *******************************************************************************/
    if( /* check block number and pitch information */
        ( nblk_x  == 0) ||
        ( nblk_y  == 0) ||
        ( pitch_x == 0) ||
        ( pitch_y == 0) ||
        ( (unsigned int)(mSz_TG.w) < (nblk_x*pitch_x+full_lOffx)) ||
        ( (unsigned int)(mSz_TG.h) < (nblk_y*pitch_y+full_lOffy)))
    {
        CAM_LOGE("block number is not correct (%d)<(%d*%d+%d) or (%d)<(%d*%d+%d), checking sensor driver\n",
                 mSz_TG.w,
                 nblk_x,
                 pitch_x,
                 full_lOffx,
                 mSz_TG.h,
                 nblk_y,
                 pitch_y,
                 full_lOffy);

        return false;
    }
    else if( /* check pairNum information */
        ((pairNum*pd_info.i4SubBlkH*pd_info.i4SubBlkW) != (pitch_x*pitch_y)) ||
        (pairNum != (numPixs/2)))
    {
        CAM_LOGE("pairNum is not correct (%d)!=(%d/%d)*(%d/%d) or (%d)!=(%d/2), checking sensor driver\n",
                 pd_info.i4PairNum,
                 pd_info.i4PitchX,
                 pd_info.i4SubBlkH,
                 pd_info.i4PitchY,
                 pd_info.i4SubBlkW,
                 pd_info.i4PairNum,
                 numPixs);

        return false;
    }

    /*******************************************************************************
     * Reference crop region and mirrorflip inction to modify PD block information.
     *******************************************************************************/
    // Current image related full size coordinate
    unsigned int crop_x = pd_info.i4Crop[scenario][0];
    unsigned int crop_y = pd_info.i4Crop[scenario][1];

    // Current pd block offset related to full size coordinate
    int shift_x = crop_x-full_lOffx;
    if( shift_x<=0)
    {
        shift_x = -crop_x;
    }
    else if( shift_x%pitch_x)
    {
        shift_x = ( (shift_x+(pitch_x-1)) / pitch_x) * pitch_x - crop_x;
    }
    else
    {
        shift_x = ( shift_x / pitch_x) * pitch_x - crop_x;
    }


    int shift_y = crop_y-full_lOffy;
    if( shift_y<=0)
    {
        shift_y = -crop_y;
    }
    else if( shift_y%pitch_y)
    {
        shift_y = ( (shift_y+(pitch_y-1)) / pitch_y) * pitch_y - crop_y;
    }
    else
    {
        shift_y = ( shift_y / pitch_y) * pitch_y - crop_y;
    }

    CAM_LOGD( "coordinate shift in current sensor mode (%d, %d)\n", shift_x, shift_y);

    /*******************************************************************************
     * calculate pd pixels' position by orientation and crop information for general separate function
     *******************************************************************************/
    unsigned int cur_lOffx = full_lOffx + shift_x;
    unsigned int cur_lOffy = full_lOffy + shift_y;
    unsigned int cur_rOffx = mSz_TG.w - cur_lOffx - pitch_x * nblk_x;
    unsigned int cur_rOffy = mSz_TG.h - cur_lOffy - pitch_y * nblk_y;

    CAM_LOGD("mirror_flip(%x), block offset : left side(%d, %d) right sied(%d, %d)\n", pd_info.iMirrorFlip, cur_lOffx, cur_lOffy, cur_rOffx, cur_rOffy);

    for(unsigned int Pidx=0, k=0; Pidx<pairNum; Pidx++, k+=2)
    {
        unsigned int PosL_X = pd_info.i4PosL[Pidx][0];
        unsigned int PosL_Y = pd_info.i4PosL[Pidx][1];
        unsigned int PosR_X = pd_info.i4PosR[Pidx][0];
        unsigned int PosR_Y = pd_info.i4PosR[Pidx][1];

        if( /* boundary check */
            ((PosL_X-full_lOffx)<pitch_x) && ((PosL_Y-full_lOffy)<pitch_y) &&
            ((PosR_X-full_lOffx)<pitch_x) && ((PosR_Y-full_lOffy)<pitch_y))
        {
            /* shift coordinate by crop information */
            PosL_X += shift_x;
            PosL_Y += shift_y;
            PosR_X += shift_x;
            PosR_Y += shift_y;

            /* mirror*/
            if(pd_info.iMirrorFlip & 0x1)
            {
                PosL_X = pitch_x - (PosL_X - cur_lOffx) - 1 + cur_rOffx;
                PosR_X = pitch_x - (PosR_X - cur_lOffx) - 1 + cur_rOffx;
            }

            /* flip*/
            if(pd_info.iMirrorFlip & 0x2)
            {
                PosL_Y = pitch_y - (PosL_Y - cur_lOffy) - 1 + cur_rOffy;
                PosR_Y = pitch_y - (PosR_Y - cur_lOffy) - 1 + cur_rOffy;
            }

            /* Final*/
            point LCoordinate( PosL_X, PosL_Y, PD_TYPE_L);
            point RCoordinate( PosR_X, PosR_Y, PD_TYPE_R);


            /* Output*/
            outAllPixs[k  ] = LCoordinate;
            outAllPixs[k+1] = RCoordinate;

            CAM_LOGD("PDPos [L][%3d %3d]->[%3d %3d], [R][%3d %3d]->[%3d %3d]\n",
                     pd_info.i4PosL[Pidx][0],
                     pd_info.i4PosL[Pidx][1],
                     outAllPixs[k].x,
                     outAllPixs[k].y,
                     pd_info.i4PosR[Pidx][0],
                     pd_info.i4PosR[Pidx][1],
                     outAllPixs[k+1].x,
                     outAllPixs[k+1].y);
        }
        else
        {
            CAM_LOGE("PD coordinate is not sutible at pair index(%d), PDPos [L](%3d %3d) [R](%3d %3d)\n",
                     Pidx,
                     PosL_X,
                     PosL_Y,
                     PosR_X,
                     PosR_Y);

            return false;
        }
    }
    return true;
}