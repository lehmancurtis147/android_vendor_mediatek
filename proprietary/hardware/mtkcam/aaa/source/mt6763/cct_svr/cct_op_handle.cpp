/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2008
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
#define LOG_TAG "cct_handle"

#include <fcntl.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <sys/poll.h>
#include <unistd.h>

#include <pthread.h>

#include "kd_imgsensor_define.h"

#include "sensor_drv.h"
#include <mtkcam/drv/IHalSensor.h>

//#include "CctIF.h"   			// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk
//#include "AcdkCommon.h"		// path:vendor/mediatek/proprietary/hardware/mtkcam/include/mtkcam/main/acdk

#include "cct_feature.h"

#include "cct_op_data.h"
#include "cct_op_handle.h"


#include <aaa_types.h>
#include <aaa_error_code.h>
#include <aaa_hal_if.h>
#include "awb_mgr_if.h"

#include <isp_reg.h>
#include <af_feature.h>
#include <af_algo_if.h>
#include "af_mgr_if.h"
#include <mtkcam/def/common.h>
using namespace NSCam;
#include "ae_mgr_if.h"

#include "awb_param.h"
#include "af_param.h"
#include "ae_param.h"
#include "flash_mgr.h"
#include "isp_tuning_mgr.h"
#include "isp_mgr.h"
#include <lsc/ILscMgr.h>
#include <ILscNvram.h>
#include <nvbuf_util.h>
//#include <mtkcam/aaa/aaa_hal_common.h>
//#include "IHal3A.h"
#include "isp_tuning_idx.h"

using namespace NSIspTuningv3;
using namespace NS3Av3;
using namespace NSCam;


#define ISP_ID_TEXT_LENGTH          (8)

char gb_MT6757P_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6757p";
char gb_MT6799P_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6799p";
char gb_MT6763_ISP_ID_TEXT[ISP_ID_TEXT_LENGTH] = "6763";


#define CCT_AWB_NVRAM_TBL_NO            (2)     //(AWB_NVRAM_IDX_NUM)

#define CCT_CMD_OUTBUF_SIZE_COUNT       (FT_CCT_OP_SENSOR_TYPE_OP_NO + FT_CCT_OP_3A_TYPE_OP_NO + FT_CCT_OP_ISP_TYPE_OP_NO + FT_CCT_OP_EMCAM_TYPE_OP_NO)

MINT32 giCctCmdOutBufSize[CCT_CMD_OUTBUF_SIZE_COUNT][2] = {
   {  FT_CCT_OP_GET_SENSOR,                     sizeof(ACDK_CCT_SENSOR_INFO_STRUCT) },
   {  FT_CCT_OP_SET_SENSOR_REG,                 0 },
   {  FT_CCT_OP_GET_SENSOR_REG,                 sizeof(ACDK_CCT_REG_RW_STRUCT) },
   {  FT_CCT_OP_LSC_GET_SENSOR_RESOLUTION,      sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT) },

   {  FT_CCT_OP_AE_GET_ON_OFF,                  sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_ON_OFF,                  0 },
   {  FT_CCT_OP_AE_GET_BAND,                    sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_BAND,                    0 },
   {  FT_CCT_OP_AE_GET_METERING_MODE,           sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_METERING_MODE,           0 },
   {  FT_CCT_OP_AE_GET_SCENE_MODE,              sizeof(MINT32) },
   {  FT_CCT_OP_AE_SET_SCENE_MODE,              0 },
   {  FT_CCT_OP_AE_GET_AUTO_PARA,               sizeof(ACDK_AE_MODE_CFG_T) },
   {  FT_CCT_OP_AE_SET_AUTO_PARA,               0 },
   {  FT_CCT_OP_AE_GET_CAPTURE_PARA,            sizeof(ACDK_AE_MODE_CFG_T) },
   {  FT_CCT_OP_AE_SET_CAPTURE_PARA,            0 },
   {  FT_CCT_OP_AF_GET_RANGE,                   sizeof(FOCUS_RANGE_T) },
   {  FT_CCT_OP_AF_GET_POS,                     sizeof(MINT32) },
   {  FT_CCT_OP_AF_SET_POS,                     0 },
   {  FT_CCT_OP_AWB_GET_ON_OFF,                 sizeof(MINT32) },
   {  FT_CCT_OP_AWB_SET_ON_OFF,                 0 },
   {  FT_CCT_OP_AWB_GET_LIGHT_PROB,             sizeof(AWB_LIGHT_PROBABILITY_T) },
   {  FT_CCT_OP_AWB_GET_MODE,                   sizeof(MINT32) },
   {  FT_CCT_OP_AWB_SET_MODE,                   0 },
   {  FT_CCT_OP_AWB_GET_GAIN,                   sizeof(AWB_GAIN_T) },
   {  FT_CCT_OP_AWB_SET_GAIN,                   0 },
   {  FT_CCT_OP_FLASH_GET_MODE,                 sizeof(MINT32) },
   {  FT_CCT_OP_FLASH_SET_MODE,                 0 },

   {  FT_CCT_OP_GET_ID,                         ISP_ID_TEXT_LENGTH },
   {  FT_CCT_OP_ISP_GET_ON_OFF,                 sizeof(MINT32) },
   {  FT_CCT_OP_ISP_SET_ON_OFF,                 0 },
   {  FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF,       sizeof(MINT32) },
   {  FT_CCT_OP_ISP_SET_CCM_FIXED_ON_OFF,       0 },
   {  FT_CCT_OP_ISP_GET_CCM_MATRIX,             sizeof(ACDK_CCT_CCM_STRUCT) },
   {  FT_CCT_OP_ISP_SET_CCM_MATRIX,             0 },
   {  FT_CCT_OP_ISP_GET_INDEX,                  sizeof(CUSTOM_NVRAM_REG_INDEX) },
   {  FT_CCT_OP_GET_SHADING_ON_OFF,             sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) },
   {  FT_CCT_OP_SET_SHADING_ON_OFF,             0 },
   {  FT_CCT_OP_GET_SHADING_INDEX,              sizeof(MINT32) },
   {  FT_CCT_OP_SET_SHADING_INDEX,              0 },
   {  FT_CCT_OP_GET_SHADING_TSF_ON_OFF,         sizeof(MINT32) },
   {  FT_CCT_OP_SET_SHADING_TSF_ON_OFF,         0 },

   {  FT_CCT_OP_AF_BRECKET_STEP,                0 },
   {  FT_CCT_OP_AE_BRECKET_STEP,                0 },
   {  FT_CCT_OP_AF_SET_AFMODE,                  0 },
   {  FT_CCT_OP_AF_AUTOFOCUS,                   0 },
   {  FT_CCT_OP_AF_FULL_SCAN_SET_INTERVAL,      0 },
   {  FT_CCT_OP_AF_FULL_SCAN_SET_DACSTEP,       0 },
   {  FT_CCT_OP_AF_FULL_SCAN_TRIGGER,           0 },
   {  FT_CCT_OP_AF_SET_AREA,                    0 },
   {  FT_CCT_OP_FLASH_CALIBRATION,              0 },
   {  FT_CCT_OP_AE_SET_CAPTURE_ISO,             0 },
   {  FT_CCT_OP_AE_SET_CAPTURE_SENSOR_GAIN,     0 },
   {  FT_CCT_OP_AE_APPLY_CAPTURE_AE_PARAM,      0 },
   {  FT_CCT_OP_AE_SET_CAPTURE_EXP_TIME_US,     0 },
   {  FT_CCT_OP_AE_SET_VHDR_RATIO,              0 },
   {  FT_CCT_OP_AE_SET_AE_MODE,                 0 },
   {  FT_CCT_OP_AWB_SET_MTK_ENABLE,             0 },
   {  FT_CCT_OP_AWB_SET_SENSOR_ENABLE,          0 },
   {  FT_CCT_OP_AE_EV_CALIBRATION,              0 },
   {  FT_CCT_OP_SET_RESULT_FILE_PATH,           0 }

//   {  FT_CCT_OP_ISP_GET_NVRAM_DATA,             0 },
//   {  FT_CCT_OP_ISP_SET_NVRAM_DATA,             0 },
//   {  FT_CCT_OP_ISP_SAVE_NVRAM_DATA,            0 },

};

MINT32 giCctNvramCmdOutBufSize[CCT_NVRAM_DATA_ENUM_MAX][2] = {
   {  CCT_NVRAM_DATA_LSC_PARA,          sizeof(winmo_cct_shading_comp_struct) },
   {  CCT_NVRAM_DATA_LSC_TABLE,         sizeof(CCT_SHADING_TAB_STRUCT) },
   {  CCT_NVRAM_DATA_LSC,               sizeof(NVRAM_CAMERA_SHADING_STRUCT) },
   {  CCT_NVRAM_DATA_AE_PLINE,          sizeof(AE_PLINETABLE_T) },
   {  CCT_NVRAM_DATA_AE,                sizeof(AE_NVRAM_T) * CAM_SCENARIO_NUM },                   // need to support 7 scenarios ... Done
   {  CCT_NVRAM_DATA_AF,                sizeof(NVRAM_LENS_PARA_STRUCT) },       // need to support 7 scenarios
   {  CCT_NVRAM_DATA_AWB,               sizeof(AWB_NVRAM_T) * CAM_SCENARIO_NUM },   // need to support 7 scenarios ... Done
   {  CCT_NVRAM_DATA_ISP,               sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) },
   {  CCT_NVRAM_DATA_FEATURE,           sizeof(NVRAM_CAMERA_FEATURE_STRUCT) },
   {  CCT_NVRAM_DATA_STROBE,            sizeof(NVRAM_CAMERA_STROBE_STRUCT) },
   {  CCT_NVRAM_DATA_FLASH_AWB,         sizeof(FLASH_AWB_NVRAM_T) }
};


CctHandle*
CctHandle::
createInstance(CAMERA_DUAL_CAMERA_SENSOR_ENUM eSensorEnum)
{
    NVRAM_CAMERA_ISP_PARAM_STRUCT*  pbuf_isp;
    NVRAM_CAMERA_SHADING_STRUCT*    pbuf_shd;
    NVRAM_CAMERA_3A_STRUCT*    pbuf_3a;
    NVRAM_LENS_PARA_STRUCT*    pbuf_ln;
    int err;
    NvramDrvBase*	pnvram_drv;

    ALOGD("CctHandle createInstance +");
    pnvram_drv = NvramDrvBase::createInstance();

//    ALOGD("Init sensor +");
//    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
//    pIHalSensorList->searchSensors();
//    int sensorCount = pIHalSensorList->queryNumberOfSensors();
//    ALOGD("Init sensor -, sensorCount(%d)", sensorCount);

    NvBufUtil::getInstance().setAndroidMode(0);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_ISP, eSensorEnum, (void*&)pbuf_isp);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_SHADING, eSensorEnum, (void*&)pbuf_shd);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_3A, eSensorEnum, (void*&)pbuf_3a);
    err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_LENS, eSensorEnum, (void*&)pbuf_ln);

    ALOGD("CctHandle createInstance -");
    return new CctHandle(pbuf_isp, pbuf_shd, pbuf_3a, pbuf_ln, pnvram_drv, eSensorEnum);
}


void
CctHandle::
destroyInstance()
{
    delete this;
}

CctHandle::
CctHandle( NVRAM_CAMERA_ISP_PARAM_STRUCT* pbuf_isp, NVRAM_CAMERA_SHADING_STRUCT* pbuf_shd, NVRAM_CAMERA_3A_STRUCT* pbuf_3a, NVRAM_LENS_PARA_STRUCT* pbuf_ln, NvramDrvBase*	pnvram_drv, MUINT32 isensor_dev)
    : m_eSensorEnum((CAMERA_DUAL_CAMERA_SENSOR_ENUM)isensor_dev)
    , m_bGetSensorStaticInfo(MFALSE)
    , m_pIHal3A(NULL)
    , m_pNvramDrv( pnvram_drv )
    , m_rBuf_ISP( *pbuf_isp )
    , m_rISPComm( m_rBuf_ISP.ISPComm )
    , m_rISPRegs( m_rBuf_ISP.ISPRegs )
    , m_rISPRegsIdx( m_rBuf_ISP.ISPRegs.Idx )
    , m_rBuf_SD( *pbuf_shd )
    , m_rBuf_3A( *pbuf_3a )
    , m_rBuf_LN( *pbuf_ln )
//    , m_rISPPca( m_rBuf_ISP.ISPPca )
{
    ALOGD("CctHandle construct +");
    ALOGD("Sensor Dev (%d) is used", m_eSensorEnum);
    mSensorDev = m_eSensorEnum;

    ALOGD("CctHandle construct -");
}


CctHandle::
~CctHandle()
{
    m_pIHal3A->send3ACtrl(E3ACtrl_Enable3ASetParams, MTRUE, 0);
    m_pIHal3A->send3ACtrl(E3ACtrl_SetOperMode, NSIspTuning::EOperMode_Normal, 0);
}


void
CctHandle::
init()
{
    ALOGD("CctHandle init +");
    if (m_eSensorEnum == DUAL_CAMERA_MAIN_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(0, "CctHandle");
    } else if (m_eSensorEnum == DUAL_CAMERA_SUB_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(1, "CctHandle");
    } else if (m_eSensorEnum == DUAL_CAMERA_MAIN_2_SENSOR ) {
        m_pIHal3A = MAKE_Hal3A(2, "CctHandle");
    } else {
        m_pIHal3A = MAKE_Hal3A(3, "CctHandle");
    }

    m_pIHal3A->send3ACtrl(E3ACtrl_Enable3ASetParams, MFALSE, 0);
    m_pIHal3A->send3ACtrl(E3ACtrl_SetOperMode, NSIspTuning::EOperMode_Meta, 0);

    ALOGD("CctHandle init -");
}



MINT32
CctHandle::
cct_getSensorStaticInfo()
{
    if (m_bGetSensorStaticInfo) return CCTIF_NO_ERROR;

    MINT32 err = SENSOR_NO_ERROR;
    IHalSensorList*const pHalSensorList = MAKE_HalSensorList();
    pHalSensorList->querySensorStaticInfo(mSensorDev,&m_SensorStaticInfo);
    m_bGetSensorStaticInfo = MTRUE;
    return SENSOR_NO_ERROR;
}


MINT32
CctHandle::
cct_QuerySensor(MVOID *a_pCCTSensorInfoOut, MUINT32 *pRealParaOutLen)
{

    MINT32 err = SENSOR_NO_ERROR;
#if 0
    char *str;
    str = (char *) a_pCCTSensorInfoOut;
    strcpy(str, "cct_QuerySensor executed");
    *pRealParaOutLen = strlen(str);
#else
    cct_getSensorStaticInfo();

    ACDK_CCT_SENSOR_INFO_STRUCT *pSensorEngInfoOut = (ACDK_CCT_SENSOR_INFO_STRUCT*)a_pCCTSensorInfoOut;
    pSensorEngInfoOut->DeviceId = m_SensorStaticInfo.sensorDevID;
    pSensorEngInfoOut->Type = static_cast<ACDK_CCT_REG_TYPE_ENUM>(m_SensorStaticInfo.sensorType);
    pSensorEngInfoOut->StartPixelBayerPtn = static_cast<ACDK_SENSOR_OUTPUT_DATA_FORMAT_ENUM>(m_SensorStaticInfo.sensorFormatOrder);
    pSensorEngInfoOut->GrabXOffset = 0;
    pSensorEngInfoOut->GrabYOffset = 0;

    printf("[CCTOPQuerySensor] Id = 0x%x\n", pSensorEngInfoOut->DeviceId);
    printf("[CCTOPQuerySensor] Type = %d\n", pSensorEngInfoOut->Type);
    printf("[CCTOPQuerySensor] StartPixelBayerPtn = %d\n", pSensorEngInfoOut->StartPixelBayerPtn);
    printf("[CCTOPQuerySensor] GrabXOffset = %d\n", pSensorEngInfoOut->GrabXOffset);
    printf("[CCTOPQuerySensor] GrabYOffset = %d\n", pSensorEngInfoOut->GrabYOffset);

    ALOGD("[CCTOPQuerySensor] Id = 0x%x\n", pSensorEngInfoOut->DeviceId);
    ALOGD("[CCTOPQuerySensor] Type = %d\n", pSensorEngInfoOut->Type);
    ALOGD("[CCTOPQuerySensor] StartPixelBayerPtn = %d\n", pSensorEngInfoOut->StartPixelBayerPtn);
    ALOGD("[CCTOPQuerySensor] GrabXOffset = %d\n", pSensorEngInfoOut->GrabXOffset);
    ALOGD("[CCTOPQuerySensor] GrabYOffset = %d\n", pSensorEngInfoOut->GrabYOffset);

    *pRealParaOutLen = sizeof(ACDK_CCT_SENSOR_INFO_STRUCT);
#endif
    return err;
}


MINT32
CctHandle::
cct_GetSensorRes(MVOID *pCCTSensorResOut, MUINT32 *pRealParaOutLen)
{
    MINT32 err = SENSOR_NO_ERROR;
#if 0
    char *str;
    str = (char *) pCCTSensorResOut;
    strcpy(str, "cct_GetSensorRes executed");
    *pRealParaOutLen = strlen(str);
#else
    cct_getSensorStaticInfo();

    ACDK_CCT_SENSOR_RESOLUTION_STRUCT *pSensorResolution = (ACDK_CCT_SENSOR_RESOLUTION_STRUCT *)pCCTSensorResOut;
    pSensorResolution->SensorPreviewWidth  = m_SensorStaticInfo.previewWidth;
    pSensorResolution->SensorPreviewHeight = m_SensorStaticInfo.previewHeight;
    pSensorResolution->SensorFullWidth     = m_SensorStaticInfo.captureWidth;
    pSensorResolution->SensorFullHeight    = m_SensorStaticInfo.captureHeight;
    pSensorResolution->SensorVideoWidth    = m_SensorStaticInfo.videoWidth;
    pSensorResolution->SensorVideoHeight   = m_SensorStaticInfo.videoHeight;
    pSensorResolution->SensorVideo1Width   = m_SensorStaticInfo.video1Width;
    pSensorResolution->SensorVideo1Height  = m_SensorStaticInfo.video1Height;
    pSensorResolution->SensorVideo2Width   = m_SensorStaticInfo.video2Width;
    pSensorResolution->SensorVideo2Height  = m_SensorStaticInfo.video2Height;
    pSensorResolution->SensorCustom1Width   = m_SensorStaticInfo.SensorCustom1Width;
    pSensorResolution->SensorCustom1Height  = m_SensorStaticInfo.SensorCustom1Height;
    pSensorResolution->SensorCustom2Width   = m_SensorStaticInfo.SensorCustom2Width;
    pSensorResolution->SensorCustom2Height  = m_SensorStaticInfo.SensorCustom2Height;
    pSensorResolution->SensorCustom3Width   = m_SensorStaticInfo.SensorCustom3Width;
    pSensorResolution->SensorCustom3Height  = m_SensorStaticInfo.SensorCustom3Height;
    pSensorResolution->SensorCustom4Width   = m_SensorStaticInfo.SensorCustom4Width;
    pSensorResolution->SensorCustom4Height  = m_SensorStaticInfo.SensorCustom4Height;
    pSensorResolution->SensorCustom5Width   = m_SensorStaticInfo.SensorCustom5Width;
    pSensorResolution->SensorCustom5Height  = m_SensorStaticInfo.SensorCustom5Height;

    printf("[CCTOPGetSensorRes] PreviewWidth = %d, PreviewHeight = %d\n", pSensorResolution->SensorPreviewWidth, pSensorResolution->SensorPreviewHeight);
    printf("[CCTOPGetSensorRes] SensorFullWidth = %d, SensorFullHeight = %d\n", pSensorResolution->SensorFullWidth, pSensorResolution->SensorFullHeight);
    printf("[CCTOPGetSensorRes] SensorVideoWidth = %d, SensorVideoHeight = %d\n", pSensorResolution->SensorVideoWidth, pSensorResolution->SensorVideoHeight);
    printf("[CCTOPGetSensorRes] SensorVideo1Width = %d, SensorVideo1Height = %d\n", pSensorResolution->SensorVideo1Width, pSensorResolution->SensorVideo1Height);
    printf("[CCTOPGetSensorRes] SensorVideo2Width = %d, SensorVideo2Height = %d\n", pSensorResolution->SensorVideo2Width, pSensorResolution->SensorVideo2Height);
    printf("[CCTOPGetSensorRes] SensorCustom1Width = %d, SensorCustom1Height = %d\n", pSensorResolution->SensorCustom1Width, pSensorResolution->SensorCustom1Height);
    printf("[CCTOPGetSensorRes] SensorCustom2Width = %d, SensorCustom2Height = %d\n", pSensorResolution->SensorCustom2Width, pSensorResolution->SensorCustom2Height);
    printf("[CCTOPGetSensorRes] SensorCustom3Width = %d, SensorCustom3Height = %d\n", pSensorResolution->SensorCustom3Width, pSensorResolution->SensorCustom3Height);
    printf("[CCTOPGetSensorRes] SensorCustom4Width = %d, SensorCustom4Height = %d\n", pSensorResolution->SensorCustom4Width, pSensorResolution->SensorCustom4Height);
    printf("[CCTOPGetSensorRes] SensorCustom5Width = %d, SensorCustom5Height = %d\n", pSensorResolution->SensorCustom5Width, pSensorResolution->SensorCustom5Height);

    ALOGD("[CCTOPGetSensorRes] PreviewWidth = %d, PreviewHeight = %d\n", pSensorResolution->SensorPreviewWidth, pSensorResolution->SensorPreviewHeight);
    ALOGD("[CCTOPGetSensorRes] SensorFullWidth = %d, SensorFullHeight = %d\n", pSensorResolution->SensorFullWidth, pSensorResolution->SensorFullHeight);
    ALOGD("[CCTOPGetSensorRes] SensorVideoWidth = %d, SensorVideoHeight = %d\n", pSensorResolution->SensorVideoWidth, pSensorResolution->SensorVideoHeight);
    ALOGD("[CCTOPGetSensorRes] SensorVideo1Width = %d, SensorVideo1Height = %d\n", pSensorResolution->SensorVideo1Width, pSensorResolution->SensorVideo1Height);
    ALOGD("[CCTOPGetSensorRes] SensorVideo2Width = %d, SensorVideo2Height = %d\n", pSensorResolution->SensorVideo2Width, pSensorResolution->SensorVideo2Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom1Width = %d, SensorCustom1Height = %d\n", pSensorResolution->SensorCustom1Width, pSensorResolution->SensorCustom1Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom2Width = %d, SensorCustom2Height = %d\n", pSensorResolution->SensorCustom2Width, pSensorResolution->SensorCustom2Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom3Width = %d, SensorCustom3Height = %d\n", pSensorResolution->SensorCustom3Width, pSensorResolution->SensorCustom3Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom4Width = %d, SensorCustom4Height = %d\n", pSensorResolution->SensorCustom4Width, pSensorResolution->SensorCustom4Height);
    ALOGD("[CCTOPGetSensorRes] SensorCustom5Width = %d, SensorCustom5Height = %d\n", pSensorResolution->SensorCustom5Width, pSensorResolution->SensorCustom5Height);

    *pRealParaOutLen = sizeof(ACDK_CCT_SENSOR_RESOLUTION_STRUCT);
#endif
    return err;
}


MINT32
CctHandle::
cct_ReadSensorReg(MVOID *puParaIn, MVOID *puParaOut, MUINT32 *pu4RealParaOutLen)
{
    MINT32 err = SENSOR_NO_ERROR;
    ACDK_SENSOR_FEATURE_ENUM eSensorFeature = SENSOR_FEATURE_GET_REGISTER;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoIn = (PACDK_CCT_REG_RW_STRUCT)puParaIn;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoOut = (PACDK_CCT_REG_RW_STRUCT)puParaOut;
    MUINT32 Data[2], sensorParaLen;
    IHalSensor *pHalSensorObj;

    ALOGD("[ACDK_CCT_OP_READ_SENSOR_REG]\n");

    Data[0] = pSensorRegInfoIn->RegAddr;
    Data[1] = 0;

    sensorParaLen = 2 * sizeof(MUINT32);

    // Sensor hal init
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    pHalSensorObj = pIHalSensorList->createSensor("cct_sensor_access", 0);

    if(pHalSensorObj == NULL) {
        ALOGE("[AAA Sensor Mgr] Can not create SensorHal obj\n");
        return SENSOR_INVALID_SENSOR;
    }

    err = pHalSensorObj->sendCommand(mSensorDev, SENSOR_CMD_SET_CCT_FEATURE_CONTROL, (MUINTPTR)&eSensorFeature, (MUINTPTR)&Data[0], (MUINTPTR)&sensorParaLen);

    pSensorRegInfoOut->RegAddr = pSensorRegInfoIn->RegAddr;
    pSensorRegInfoOut->RegData = Data[1];

    *pu4RealParaOutLen = sizeof(ACDK_CCT_REG_RW_STRUCT);

    if (err != SENSOR_NO_ERROR) {
        ALOGE("[CCTOReadSensorReg() error]\n");
        return err;
    }

    ALOGD("[CCTOReadSensorReg] regAddr = %x, regData = %x\n", Data[0], Data[1]);

    return err;
}

/*******************************************************************************
*
********************************************************************************/
MINT32
CctHandle::
cct_WriteSensorReg(MVOID *puParaIn)
{
    MINT32 err = SENSOR_NO_ERROR;
    ACDK_SENSOR_FEATURE_ENUM eSensorFeature = SENSOR_FEATURE_SET_REGISTER;
    PACDK_CCT_REG_RW_STRUCT pSensorRegInfoIn = (PACDK_CCT_REG_RW_STRUCT)puParaIn;
    MUINT32 Data[2], sensorParaLen;
    IHalSensor *pHalSensorObj;

    ALOGD("[ACDK_CCT_OP_WRITE_SENSOR_REG]\n");

    Data[0] = pSensorRegInfoIn->RegAddr;
    Data[1] = pSensorRegInfoIn->RegData;

    sensorParaLen = 2 * sizeof(MUINT32);

    // Sensor hal init
    IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
    pHalSensorObj = pIHalSensorList->createSensor("cct_sensor_access", 0);

    if(pHalSensorObj == NULL) {
        ALOGE("[AAA Sensor Mgr] Can not create SensorHal obj\n");
        return SENSOR_INVALID_SENSOR;
    }

    err = pHalSensorObj->sendCommand(mSensorDev, SENSOR_CMD_SET_CCT_FEATURE_CONTROL, (MUINTPTR)&eSensorFeature, (MUINTPTR)&Data[0], (MUINTPTR)&sensorParaLen);

    if (err != SENSOR_NO_ERROR) {
        ALOGE("[CCTOPWriteSensorReg() error]\n");
        return err;
    }

    ALOGD("[CCTOPWriteSensorReg] regAddr = %x, regData = %x\n", Data[0], Data[1]);

    return err;
}

MVOID
CctHandle::
setIspOnOff_SL2F(MBOOL const fgOn)
{
    ISP_MGR_SL2F_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuning::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_DBS(MBOOL const fgOn)
{
    ISP_MGR_DBS_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuning::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}


MVOID
CctHandle::
setIspOnOff_OBC(MBOOL const fgOn)
{
    ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_BPC(MBOOL const fgOn)
{
    MUINT32 const u4Index = m_rISPRegsIdx.BNR_BPC;

    //m_rISPRegs.BPC[u4Index].con.bits.BPC_ENABLE = fgOn;
    //m_rISPRegs.BNR_BPC[u4Index].con.bits.BPC_EN = fgOn; //definition change: CAM_BPC_CON CAM+0800H

    ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTBPCEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}


MVOID
CctHandle::
setIspOnOff_NR1(MBOOL const fgOn)
{
    MUINT32 const u4Index = m_rISPRegsIdx.BNR_NR1;

    //m_rISPRegs.BNR_NR1[u4Index].con.bits.NR1_CT_EN = fgOn;

    ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_PDC(MBOOL const fgOn)
{
    MUINT32 const u4Index = m_rISPRegsIdx.BNR_PDC;

    //m_rISPRegs.BNR_PDC[u4Index].con.bits.PDC_EN = fgOn;

    ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_RMM(MBOOL const fgOn)
{

    ISP_MGR_RMM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_RNR(MBOOL const fgOn)
{

    ISP_MGR_RNR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_SL2(MBOOL const fgOn)
{

    ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}


MVOID
CctHandle::
setIspOnOff_UDM(MBOOL const fgOn)
{

    MUINT32 u4Index = m_rISPRegsIdx.UDM;

    ISP_MGR_UDM::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);

    (fgOn == MTRUE) ? (ISP_MGR_UDM::getInstance((ESensorDev_T)m_eSensorEnum).put(m_rISPRegs.UDM[u4Index]))
                    : (ISP_MGR_UDM::getInstance((ESensorDev_T)m_eSensorEnum).put(m_rISPRegs.UDM[NVRAM_UDM_TBL_NUM - 1]));

    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_CFA::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_CFA::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_CCM(MBOOL const fgOn)
{
    ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_GGM(MBOOL const fgOn)
{
    ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_IHDR_GGM(MBOOL const fgOn)
{
//    ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
//    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
    //ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_PCA(MBOOL const fgOn)
{
    ISP_MGR_PCA_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}



MVOID
CctHandle::
setIspOnOff_ANR(MBOOL const fgOn)
{

    ISP_MGR_NBC_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTANR1Enable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_ANR2(MBOOL const fgOn)
{

    ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTANR2Enable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_CCR(MBOOL const fgOn)
{

    ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTCCREnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_BOK(MBOOL const fgOn)
{

    ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTBOKEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_HFG(MBOOL const fgOn)
{

    ISP_MGR_HFG_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_EE(MBOOL const fgOn)
{
    MUINT32 const u4Index = m_rISPRegsIdx.EE;

// Choo
//    ISP_DIP_X_SEEE_SRK_CTRL_T &EECtrl_SRK = m_rISPRegs.EE[u4Index].srk_ctrl.bits;
//    ISP_DIP_X_SEEE_CLIP_CTRL_T &EECtrl_Clip = m_rISPRegs.EE[u4Index].clip_ctrl.bits;

//    EECtrl_Clip.SEEE_OVRSH_CLIP_EN = fgOn;//definition change: CAM_SEEE_CLIP_CTRL CAM+AA4H

    ISP_MGR_SEEE_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEEEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);

}

MVOID
CctHandle::
setIspOnOff_NR3D(MBOOL const fgOn)
{
    //Temp. Mark
    ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).setNr3dEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_MFB(MBOOL const fgOn)
{
    //Temp. Mark
    ISP_MGR_MFB_T::getInstance((ESensorDev_T)m_eSensorEnum).setEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_MIXER3(MBOOL const fgOn)
{
    //Temp. Mark
    ISP_MGR_MIXER3_T::getInstance((ESensorDev_T)m_eSensorEnum).setEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_COLOR(MBOOL const fgOn)
{
    ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTColorEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_HLR(MBOOL const fgOn)
{
    //ISP_MGR_HLR_T::getInstance((ESensorDev_T)m_eSensorEnum).setCCTEnable(fgOn);
    //NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] HLR not enabled, fgOn(%d)", __FUNCTION__, fgOn);
}

MVOID
CctHandle::
setIspOnOff_ABF(MBOOL const fgOn)
{
    ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).setABFEnable(fgOn);
    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
    MY_LOG("[%s] fgOn = %d", __FUNCTION__, fgOn);
}

MBOOL
CctHandle::
getIspOnOff_SL2F() const
{
    return ISP_MGR_SL2F_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_DBS() const
{
    return ISP_MGR_DBS_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}


MBOOL
CctHandle::
getIspOnOff_OBC() const
{
    return ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_BPC() const
{
    return  ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTBPCEnable();
}


MBOOL
CctHandle::
getIspOnOff_NR1() const
{
    return  ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_PDC() const
{
    return  ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTPDCEnable();
}

MBOOL
CctHandle::
getIspOnOff_RMM() const
{

    return  ISP_MGR_RMM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();

}

MBOOL
CctHandle::
getIspOnOff_RNR() const
{

    return  ISP_MGR_RNR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();

}

MBOOL
CctHandle::
getIspOnOff_SL2() const
{

    return  ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();

}


MBOOL
CctHandle::
getIspOnOff_UDM() const
{

    return  ISP_MGR_UDM::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();

}

MBOOL
CctHandle::
getIspOnOff_CCM() const
{
    return ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_GGM() const
{
    return  ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_IHDR_GGM() const
{
    return MFALSE;   // temp.
//    return  ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_PCA() const
{
    return  ISP_MGR_PCA_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_ANR() const
{

        return (ISP_MGR_NBC_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTANR1Enable());
}

MBOOL
CctHandle::
getIspOnOff_ANR2() const
{

        return (ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTANR2Enable());
}


MBOOL
CctHandle::
getIspOnOff_CCR() const
{

        return ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTCCREnable();

}

MBOOL
CctHandle::
getIspOnOff_BOK() const
{

        return ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTBOKEnable();

}

MBOOL
CctHandle::
getIspOnOff_HFG() const
{
    return ISP_MGR_HFG_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_EE() const
{
    return ISP_MGR_SEEE_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEEEnable();
}

MBOOL
CctHandle::
getIspOnOff_NR3D() const
{
    return ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).isNr3dEnable();
}

MBOOL
CctHandle::
getIspOnOff_MFB() const
{
    return MFALSE;
    //Temp. Mark
    //return ISP_MGR_MFB_T::getInstance((ESensorDev_T)m_eSensorEnum).isEnable();
}

MBOOL
CctHandle::
getIspOnOff_MIXER3() const
{
    return MFALSE;
    //Temp. Mark
    //return ISP_MGR_MIXER3_T::getInstance((ESensorDev_T)m_eSensorEnum).isEnable();
}

MBOOL
CctHandle::
getIspOnOff_COLOR() const
{
    return  ISP_MGR_NR3D_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTColorEnable();
}

MBOOL
CctHandle::
getIspOnOff_HLR() const
{
    return  MFALSE;
    //return  ISP_MGR_HLR_T::getInstance((ESensorDev_T)m_eSensorEnum).isCCTEnable();
}

MBOOL
CctHandle::
getIspOnOff_ABF() const
{
    return  ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).isABFEnable();
}

MINT32
CctHandle::
setIspOnOff(MUINT32 const u4Category, MBOOL const fgOn)
{
#define SET_ISP_ON_OFF(_category)\
    case ISP_CATEGORY_##_category:\
        setIspOnOff_##_category(fgOn);\
        MY_LOG("[setIspOnOff] < %s >", #_category);\
        break

    switch  ( u4Category )
    {
        SET_ISP_ON_OFF(SL2F);
        SET_ISP_ON_OFF(DBS);
        SET_ISP_ON_OFF(OBC);
        SET_ISP_ON_OFF(BPC);    //BNR_BPC
        SET_ISP_ON_OFF(NR1);    //BNR_NR1
        SET_ISP_ON_OFF(PDC);    //BNR_PDC
        SET_ISP_ON_OFF(RMM);
        SET_ISP_ON_OFF(RNR);
        SET_ISP_ON_OFF(SL2);
        SET_ISP_ON_OFF(UDM);
        SET_ISP_ON_OFF(CCM);
        SET_ISP_ON_OFF(GGM);         // new
//        SET_ISP_ON_OFF(IHDR_GGM);    // new
        SET_ISP_ON_OFF(ANR);
        SET_ISP_ON_OFF(ANR2);
        SET_ISP_ON_OFF(CCR);
        SET_ISP_ON_OFF(BOK);         // new
        SET_ISP_ON_OFF(HFG);
        SET_ISP_ON_OFF(EE);
//        SET_ISP_ON_OFF(NR3D);        // new   // based on Choo, CCT should not control NR3D
        SET_ISP_ON_OFF(MFB);
        SET_ISP_ON_OFF(MIXER3);
        SET_ISP_ON_OFF(PCA);       // new
        SET_ISP_ON_OFF(COLOR);       // new
        SET_ISP_ON_OFF(HLR);       // new
        SET_ISP_ON_OFF(ABF);       // new

        default:
            MY_ERR("[setIspOnOff] Unsupported Category(%d)", u4Category);
            return  CCTIF_BAD_PARAM;
    }
    MY_LOG("[%s] (u4Category, fgOn) = (%d, %d)", __FUNCTION__, u4Category, fgOn);
    return  CCTIF_NO_ERROR;
}


MINT32
CctHandle::
getIspOnOff(MUINT32 const u4Category, MBOOL& rfgOn) const
{
#define GET_ISP_ON_OFF(_category)\
    case ISP_CATEGORY_##_category:\
        MY_LOG("[getIspOnOff] < %s >", #_category);\
        rfgOn = getIspOnOff_##_category();\
        break

    switch  ( u4Category )
    {
        GET_ISP_ON_OFF(SL2F);
        GET_ISP_ON_OFF(DBS);
        GET_ISP_ON_OFF(OBC);
        GET_ISP_ON_OFF(BPC);    //BNR_BPC
        GET_ISP_ON_OFF(NR1);    //BNR_NR1
        GET_ISP_ON_OFF(PDC);    //BNR_PDC
        GET_ISP_ON_OFF(RMM);
        GET_ISP_ON_OFF(RNR);
        GET_ISP_ON_OFF(SL2);
        GET_ISP_ON_OFF(UDM);
        GET_ISP_ON_OFF(CCM);
        GET_ISP_ON_OFF(GGM);        // new
//        GET_ISP_ON_OFF(IHDR_GGM);   // new
        GET_ISP_ON_OFF(ANR);
        GET_ISP_ON_OFF(ANR2);
        GET_ISP_ON_OFF(CCR);
        GET_ISP_ON_OFF(BOK);        // new
        GET_ISP_ON_OFF(HFG);
        GET_ISP_ON_OFF(EE);
        GET_ISP_ON_OFF(NR3D);       // new
        GET_ISP_ON_OFF(MFB);
        GET_ISP_ON_OFF(MIXER3);
        GET_ISP_ON_OFF(PCA);      // new
        GET_ISP_ON_OFF(COLOR);      // new
        GET_ISP_ON_OFF(HLR);      // new
        GET_ISP_ON_OFF(ABF);      // new

        default:
            MY_ERR("[getIspOnOff] Unsupported Category(%d)", u4Category);
            return  CCTIF_BAD_PARAM;
    }
    MY_LOG("[%s] (u4Category, rfgOn) = (%d, %d)", __FUNCTION__, u4Category, rfgOn);
    return  CCTIF_NO_ERROR;
}


MINT32
CctHandle::
cct_HandleSensorOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MINT32 err = CCTIF_NO_ERROR;
    ALOGD("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    switch(op) {
    case FT_CCT_OP_GET_SENSOR:
        if (puParaOut != NULL)
            err = cct_QuerySensor(puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_SWITCH_SENSOR:
        break;
    case FT_CCT_OP_SET_SENSOR_REG:
        if (u4ParaInLen == sizeof(ACDK_CCT_REG_RW_STRUCT) && puParaIn != NULL)
            err = cct_WriteSensorReg(puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_GET_SENSOR_REG:
        if (u4ParaInLen == sizeof(ACDK_CCT_REG_RW_STRUCT) && puParaIn != NULL && puParaOut != NULL)
            err = cct_ReadSensorReg(puParaIn, puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_LSC_GET_SENSOR_RESOLUTION:
        if (puParaOut != NULL)
            err = cct_GetSensorRes(puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_TEST_SET_PROP:       // cmd for test only
        if (puParaIn != NULL && u4ParaInLen > 0) {
            char cmd[1024];
            char val[1024];
            MINT32 cmdlen, vallen;

            cmdlen = strlen((char *)puParaIn);
            strncpy(cmd, (char *)puParaIn, cmdlen);

            vallen = strlen((char *)(puParaIn+cmdlen+1));
            strncpy(val, (char *)(puParaIn+cmdlen+1), vallen);

            ALOGD("Property %s set to %s",cmd,val);
            property_set( cmd, val );
        }
        else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_TEST_GET_PROP:       // cmd for test only
        if (puParaIn != NULL && u4ParaInLen > 0) {
            char cmd[1024];
            char val[1024];
            MINT32 cmdlen;

            cmdlen = strlen((char *)puParaIn);

            strncpy(cmd, (char *)puParaIn, cmdlen);
            property_get( cmd, val, NULL );
            ALOGD("Property %s is %s",cmd,val);
        }
        else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;
    }
    return err;
}


MINT32
CctHandle::
cct_Handle3AOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    ALOGD("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    int ret;
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 *i32In = NULL;
    MUINT32 mode;
    MBOOL enable;

    switch(op) {
    case FT_CCT_OP_AE_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEEnable(mSensorDev);
            else
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_BAND:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetFlickerMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_BAND:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetFlickerMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_METERING_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetMeteringMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_METERING_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetMeteringMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_SCENE_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetAEScene(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_SCENE_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            err = NS3Av3::IAeMgr::getInstance().CCTOPAESetAEScene(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_AUTO_PARA:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetExpParam(mSensorDev, (VOID *)puParaIn, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_AUTO_PARA:
        if ( u4ParaInLen == sizeof(ACDK_AE_MODE_CFG_T))
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyExpParam(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_GET_CAPTURE_PARA:
        if (puParaOut != NULL) {
            err = NS3Av3::IAeMgr::getInstance().CCTOGetCaptureParams(mSensorDev, (VOID *)puParaOut);
            *pu4RealParaOutLen = sizeof(ACDK_AE_MODE_CFG_T);
        } else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_CAPTURE_PARA:
        if ( u4ParaInLen == sizeof(ACDK_AE_MODE_CFG_T))
            err = NS3Av3::IAeMgr::getInstance().CCTOSetCaptureParams(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AF_GET_RANGE:
        if (puParaOut != NULL)
            err = NS3Av3::IAfMgr::getInstance().CCTOPAFGetFocusRange(mSensorDev, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AF_GET_POS:
        if (puParaOut != NULL)
            err = NS3Av3::IAfMgr::getInstance().CCTOPAFGetBestPos(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AF_SET_POS:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if (*i32In < 0)
                err = NS3Av3::IAfMgr::getInstance().CCTOPAFOpeartion(mSensorDev);
            else
                err = NS3Av3::IAfMgr::getInstance().CCTOPMFOpeartion(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBEnable(mSensorDev);
            else
                err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_LIGHT_PROB:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetLightProb(mSensorDev, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_GET_MODE:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetAWBMode(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_MODE:
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if(*i32In < (MINT32)LIB3A_AWB_MODE_NUM)
              err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSetAWBMode(mSensorDev, *i32In);
        }
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AWB_GET_GAIN:
        if (puParaOut != NULL)
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetAWBGain(mSensorDev, (VOID *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AWB_SET_GAIN:
        if (u4ParaInLen == sizeof(AWB_GAIN_T))
            err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSetAWBGain(mSensorDev, (VOID *)puParaIn);
        else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_FLASH_GET_MODE:
        if (puParaOut != NULL) {
            MY_LOG("ACDK_CCT_OP_FLASH_GET_INFO line=%d\n",__LINE__);
            ret = FlashMgr::getInstance().cctGetFlashInfo(mSensorDev, (int*)puParaOut);
            *pu4RealParaOutLen = sizeof(MINT32);
            if(ret!=1)
                err=CCTIF_BAD_CTRL_CODE;
        } else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_FLASH_SET_MODE:
        if (u4ParaInLen == sizeof(MINT32)) {
            MY_LOG("ACDK_CCT_OP_FLASH_ENABLE line=%d\n",__LINE__);
            CCT_FL_MODE_T mode = (CCT_FL_MODE_T) (*(MUINT32 *)puParaIn);
            if (mode == CCT_FL_MODE_OFF) {    //disable
                ret = FlashMgr::getInstance().cctFlashEnable(mSensorDev, 0);
            }
            else if (mode == CCT_FL_MODE_HI_TEMP) {   //enable ... high color temp
                ret = FlashMgr::getInstance().cctFlashEnable(mSensorDev, 1);
                ret = FlashMgr::getInstance().clearManualFlash(mSensorDev);
                ret = FlashMgr::getInstance().setManualFlash(mSensorDev, 1, 0);
            }
            else if (mode == CCT_FL_MODE_LO_TEMP) {   //enable ... low color temp
                ret = FlashMgr::getInstance().cctFlashEnable(mSensorDev, 1);
                ret = FlashMgr::getInstance().clearManualFlash(mSensorDev);
                ret = FlashMgr::getInstance().setManualFlash(mSensorDev, 0, 1);
            }
            else if (mode == CCT_FL_MODE_MIX_TEMP) {   //enable ... mix both color temp
                ret = FlashMgr::getInstance().cctFlashEnable(mSensorDev, 1);
                ret = FlashMgr::getInstance().clearManualFlash(mSensorDev);
                ret = FlashMgr::getInstance().setManualFlash(mSensorDev, 1, 1);
            }
            else
                ret = 0;
            if(ret!=1)
                err=CCTIF_BAD_CTRL_CODE;
        } else {
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;

    }
    return err;
}


MINT32
CctHandle::
cct_HandleIspOp(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MBOOL ret;
    MINT32 err = CCTIF_NO_ERROR;

    switch(op) {
    case FT_CCT_OP_GET_ID:
        {
        if ( ISP_ID_TEXT_LENGTH != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
        }
        else {
            *pu4RealParaOutLen = 0;
            memset((void *)puParaOut, 0, ISP_ID_TEXT_LENGTH );
            strncpy((char *)puParaOut, gb_MT6763_ISP_ID_TEXT, ISP_ID_TEXT_LENGTH);
            *pu4RealParaOutLen = ISP_ID_TEXT_LENGTH;
            MY_LOG("( FT_CCT_OP_GET_ID ) done, ID:%s\n", (char *)(puParaOut));
        }
        }
        break;
    case FT_CCT_OP_ISP_GET_ON_OFF:
        {
        if  ( sizeof(CCT_ISP_CATEGORY_T) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if  ( sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        CCT_ISP_CATEGORY_T const eCategory = *reinterpret_cast<CCT_ISP_CATEGORY_T*>(puParaIn);
        MBOOL&       rfgEnable = reinterpret_cast<ACDK_CCT_FUNCTION_ENABLE_STRUCT*>(puParaOut)->Enable;

        err = getIspOnOff(eCategory, rfgEnable);
        *pu4RealParaOutLen = sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT);
        MY_LOG("[-FT_CCT_OP_ISP_GET_ON_OFF] (eCategory, rfgEnable)=(%d, %d)", eCategory, rfgEnable);
        }
        break;
    case FT_CCT_OP_ISP_SET_ON_OFF:
        {
        if  ( (sizeof(CCT_ISP_CATEGORY_T) + sizeof(MBOOL)) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        CCT_ISP_CATEGORY_T const eCategory = *reinterpret_cast<CCT_ISP_CATEGORY_T const*>(puParaIn);
        MBOOL enable = (MBOOL) *(puParaIn + 4);

        if (enable)
            err = setIspOnOff(eCategory, 1);
        else
            err = setIspOnOff(eCategory, 0);

        MY_LOG("[-FT_CCT_OP_ISP_SET_ON_OFF] eCategory(%d), err(%x)", eCategory, err);
        }
        break;
    case FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF:
        {
        MINT32 en = 0, dyccm = 0;

        MY_LOG("[FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF]\n");
        if  ( sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut )
            return  CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        
        dyccm = NSIspTuningv3::IspTuningMgr::getInstance().getDynamicCCM((MINT32)m_eSensorEnum);
        if(dyccm < 0) {
            MY_LOG("( FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF ) fail, dyccm=%d\n", dyccm);
            err = CCTIF_UNKNOWN_ERROR;
            break;
        }
        
        en = (MFALSE == dyccm)? MTRUE:MFALSE; //switch true/false
        reinterpret_cast<ACDK_CCT_FUNCTION_ENABLE_STRUCT*>(puParaOut)->Enable = en;
        *pu4RealParaOutLen = sizeof(ACDK_CCT_FUNCTION_ENABLE_STRUCT);
        err = CCTIF_NO_ERROR;
        
        MY_LOG("( FT_CCT_OP_ISP_GET_CCM_FIXED_ON_OFF ) done, en=%d\n", en);
        }
        break;
    case FT_CCT_OP_ISP_SET_CCM_FIXED_ON_OFF:
        {
        if(puParaIn == NULL)
        {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MY_LOG("Enable Dynamic CCM!!\n");
        MBOOL enable = (MBOOL) *(puParaIn);

        if (enable)
            ret = NSIspTuningv3::IspTuningMgr::getInstance().setDynamicCCM((MINT32)m_eSensorEnum, MFALSE);
        else
            ret = NSIspTuningv3::IspTuningMgr::getInstance().setDynamicCCM((MINT32)m_eSensorEnum, MTRUE);

        *pu4RealParaOutLen = 0;
        if (ret == MTRUE)
            err = CCTIF_NO_ERROR;
        else
            err = CCTIF_UNKNOWN_ERROR;
        }
        break;
    case FT_CCT_OP_ISP_GET_CCM_MATRIX:
        {
        if  ( sizeof(ACDK_CCT_CCM_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_CCM_STRUCT*const     pDst = reinterpret_cast<ACDK_CCT_CCM_STRUCT*>(puParaOut);
        ISP_NVRAM_CCM_T ccm;

        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).reset();
        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).get(ccm);

        pDst->M11 = ccm.cnv_1.bits.G2G_CNV_00;//ccm.conv0a.bits.G2G_CNV_00;
        pDst->M12 = ccm.cnv_1.bits.G2G_CNV_01;//ccm.conv0a.bits.G2G_CNV_01;
        pDst->M13 = ccm.cnv_2.bits.G2G_CNV_02;//ccm.conv0b.bits.G2G_CNV_02;
        pDst->M21 = ccm.cnv_3.bits.G2G_CNV_10;//ccm.conv1a.bits.G2G_CNV_10;
        pDst->M22 = ccm.cnv_3.bits.G2G_CNV_11;//ccm.conv1a.bits.G2G_CNV_11;
        pDst->M23 = ccm.cnv_4.bits.G2G_CNV_12;//ccm.conv1b.bits.G2G_CNV_12;
        pDst->M31 = ccm.cnv_5.bits.G2G_CNV_20;//ccm.conv2a.bits.G2G_CNV_20;
        pDst->M32 = ccm.cnv_5.bits.G2G_CNV_21;//ccm.conv2a.bits.G2G_CNV_21;
        pDst->M33 = ccm.cnv_6.bits.G2G_CNV_22;//ccm.conv2b.bits.G2G_CNV_22;

        *pu4RealParaOutLen = sizeof(ACDK_CCT_CCM_STRUCT);

        MY_LOG("[ACDK_CCT_V2_OP_AWB_GET_CURRENT_CCM]\n");
        MY_LOG("M11 0x%03X\n", pDst->M11);
        MY_LOG("M12 0x%03X\n", pDst->M12);
        MY_LOG("M13 0x%03X\n", pDst->M13);
        MY_LOG("M21 0x%03X\n", pDst->M21);
        MY_LOG("M22 0x%03X\n", pDst->M22);
        MY_LOG("M23 0x%03X\n", pDst->M23);
        MY_LOG("M31 0x%03X\n", pDst->M31);
        MY_LOG("M32 0x%03X\n", pDst->M32);
        MY_LOG("M33 0x%03X\n", pDst->M33);

        MY_LOG("( FT_CCT_OP_ISP_GET_CCM_MATRIX ) done\n");
        }
        break;
    case FT_CCT_OP_ISP_SET_CCM_MATRIX:
        {
        if  ( sizeof(ACDK_CCT_CCM_STRUCT) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MY_LOG("[ACDK_CCT_V2_OP_AWB_SET_CURRENT_CCM]\n");

        ISP_NVRAM_CCM_T rDst;
        ACDK_CCT_CCM_STRUCT*const       pSrc = reinterpret_cast<ACDK_CCT_CCM_STRUCT*>(puParaIn);
    /*
        rDst.conv0a.bits.G2G_CNV_00 = pSrc->M11;
        rDst.conv0a.bits.G2G_CNV_01 = pSrc->M12;
        rDst.conv0b.bits.G2G_CNV_02 = pSrc->M13;
        rDst.conv1a.bits.G2G_CNV_10 = pSrc->M21;
        rDst.conv1a.bits.G2G_CNV_11 = pSrc->M22;
        rDst.conv1b.bits.G2G_CNV_12 = pSrc->M23;
        rDst.conv2a.bits.G2G_CNV_20 = pSrc->M31;
        rDst.conv2a.bits.G2G_CNV_21 = pSrc->M32;
        rDst.conv2b.bits.G2G_CNV_22 = pSrc->M33;
    */
        rDst.cnv_1.bits.G2G_CNV_00 = pSrc->M11;
        rDst.cnv_1.bits.G2G_CNV_01 = pSrc->M12;
        rDst.cnv_2.bits.G2G_CNV_02 = pSrc->M13;
        rDst.cnv_3.bits.G2G_CNV_10 = pSrc->M21;
        rDst.cnv_3.bits.G2G_CNV_11 = pSrc->M22;
        rDst.cnv_4.bits.G2G_CNV_12 = pSrc->M23;
        rDst.cnv_5.bits.G2G_CNV_20 = pSrc->M31;
        rDst.cnv_5.bits.G2G_CNV_21 = pSrc->M32;
        rDst.cnv_6.bits.G2G_CNV_22 = pSrc->M33;

        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).reset();
        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).put(rDst);
        NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);
        //ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(EIspProfile_Preview);
        //NSIspTuningv3::ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).apply(NSIspTuningv3::EIspProfile_NormalCapture);
        usleep(200000);
        MY_LOG("M11 0x%03X", pSrc->M11);
        MY_LOG("M12 0x%03X", pSrc->M12);
        MY_LOG("M13 0x%03X", pSrc->M13);
        MY_LOG("M21 0x%03X", pSrc->M21);
        MY_LOG("M22 0x%03X", pSrc->M22);
        MY_LOG("M23 0x%03X", pSrc->M23);
        MY_LOG("M31 0x%03X", pSrc->M31);
        MY_LOG("M32 0x%03X", pSrc->M32);
        MY_LOG("M33 0x%03X", pSrc->M33);

        MY_LOG("( FT_CCT_OP_ISP_SET_CCM_MATRIX ) done\n");
        }
        break;
    case FT_CCT_OP_ISP_GET_INDEX:
        {
        if ( sizeof(CUSTOM_NVRAM_REG_INDEX) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if  ( sizeof(ACDK_CCT_QUERY_ISP_INDEX_INPUT_STRUCT) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_QUERY_ISP_INDEX_INPUT_STRUCT *pIspIndexInput = reinterpret_cast<ACDK_CCT_QUERY_ISP_INDEX_INPUT_STRUCT*>(puParaIn);
        MVOID* pIndex = IspTuningMgr::getInstance().getDefaultISPIndex(m_eSensorEnum, pIspIndexInput->profile, pIspIndexInput->sensorMode, pIspIndexInput->iso_idx);

        memcpy(puParaOut, pIndex, sizeof(CUSTOM_NVRAM_REG_INDEX));
        *pu4RealParaOutLen = sizeof(CUSTOM_NVRAM_REG_INDEX);

        MY_LOG("FT_CCT_OP_ISP_GET_INDEX done\n");
        }
        break;
    case FT_CCT_OP_GET_SHADING_ON_OFF:
        {
        if ( sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) != u4ParaOutLen || ! pu4RealParaOutLen || ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_MODULE_CTRL_STRUCT*const pShadingPara = reinterpret_cast<ACDK_CCT_MODULE_CTRL_STRUCT*>(puParaOut);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pShadingPara->Enable = pLscMgr->getOnOff();
        *pu4RealParaOutLen = sizeof(ACDK_CCT_MODULE_CTRL_STRUCT);
        MY_LOG("[%s] GET_SHADING_ON_OFF(%s)", __FUNCTION__, (pShadingPara->Enable ? "On":"Off"));
        }
        break;
    case FT_CCT_OP_SET_SHADING_ON_OFF:
        {
        if ( sizeof(ACDK_CCT_MODULE_CTRL_STRUCT) != u4ParaInLen || ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        ACDK_CCT_MODULE_CTRL_STRUCT*const pShadingPara = reinterpret_cast<ACDK_CCT_MODULE_CTRL_STRUCT*>(puParaIn);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pLscMgr->setOnOff(pShadingPara->Enable);
        pLscMgr->updateLsc();

        MY_LOG("[%s] SET_SHADING_ON_OFF(%s)", __FUNCTION__, (pShadingPara->Enable ? "On":"Off"));
        }
        break;
    case FT_CCT_OP_GET_SHADING_INDEX:
        {
        if  ( ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MUINT32 *pShadingIndex = reinterpret_cast<MUINT32*>(puParaOut);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        *pShadingIndex = pLscMgr->getCTIdx();

        MY_LOG("[%s] GET_SHADING_INDEX(%d)", __FUNCTION__, *pShadingIndex);

        }
        break;
    case FT_CCT_OP_SET_SHADING_INDEX:
        {
        if ( ! puParaIn ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        *pu4RealParaOutLen = 0;
        MUINT32 u4CCT = *reinterpret_cast<MUINT8*>(puParaIn);

        MY_LOG("[%s] SET_SHADING_INDEX(%d)", __FUNCTION__, u4CCT);

        IspTuningMgr::getInstance().enableDynamicShading(m_eSensorEnum, MFALSE);
        IspTuningMgr::getInstance().setIndex_Shading(m_eSensorEnum, u4CCT);
        }
        break;
    case FT_CCT_OP_GET_SHADING_TSF_ON_OFF:
        {
        if  ( ! puParaOut ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        UINT32 u4OnOff;
        *pu4RealParaOutLen = 0;
        MY_LOG("[%s] + GET_SHADING_TSF_ONOFF", __FUNCTION__);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        u4OnOff = pLscMgr->getTsfOnOff();
        *(reinterpret_cast<UINT32*>(puParaOut)) = u4OnOff;

        MY_LOG("[%s] - GET_SHADING_TSF_ONOFF(%d)", __FUNCTION__, u4OnOff);
        }
        break;
    case FT_CCT_OP_SET_SHADING_TSF_ON_OFF:
        {
        if(puParaIn == NULL)
        {
            err = CCTIF_BAD_PARAM;
            break;
        }
        UINT32 u4OnOff = *(reinterpret_cast<UINT32*>(puParaIn));
        *pu4RealParaOutLen = 0;
        MY_LOG("[%s] + SET_SHADING_TSF_ONOFF(%d)", __FUNCTION__, u4OnOff);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        pLscMgr->setTsfOnOff(u4OnOff);

        MY_LOG("[%s] - SET_SHADING_TSF_ONOFF", __FUNCTION__);
        }
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;
    }
    return err;
}


MINT32
CctHandle::
cct_HandleNvramOp(CCT_OP_ID op,
                    MUINT32 u4ParaInLen,
                    MUINT8 *puParaIn,
                    MUINT32 u4ParaOutLen,
                    MUINT8 *puParaOut,
                    MUINT32 *pu4RealParaOutLen )
{
    CCT_NVRAM_DATA_T dtype;
    MINT32 inSize, outSize;
    MINT32 *pRealOutSize = NULL;
    MUINT32 inOffset;
    MUINT8 *pInBuf, *pOutBuf;
    MINT32 status = 0;

    dtype = *((CCT_NVRAM_DATA_T *) puParaIn);

    switch(op) {
    case FT_CCT_OP_ISP_GET_NVRAM_DATA:
        if  ( ! puParaOut ) {
            break;
        }
        //cctNvram_GetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 outSize, void *pOutBuf, MINT32 *pRealOutSize)
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T));
        outSize = u4ParaOutLen;
        pOutBuf = puParaOut;
        pRealOutSize = (MINT32 *) pu4RealParaOutLen;
        status = cctNvram_GetNvramData(dtype, inSize, pInBuf, outSize, (void *)pOutBuf, pRealOutSize);
        break;
    case FT_CCT_OP_ISP_SET_NVRAM_DATA:
        //cctNvram_SetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf)
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T));
        status = cctNvram_SetNvramData(dtype, inSize, pInBuf);
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_ISP_SET_PARTIAL_NVRAM_DATA:
        // For this command FT_CCT_OP_ISP_SET_PARTIAL_NVRAM_DATA, there are 2 parameters in the beginning of
        // puParaIn buffer. The 1st parameter is dtype (data type) and the 2nd is inOffset (in buffer offset)
        //
        inOffset = *(((CCT_NVRAM_DATA_T *) puParaIn)+1);
        inSize = u4ParaInLen - sizeof(CCT_NVRAM_DATA_T) - sizeof(MUINT32);
        pInBuf = (puParaIn + sizeof(CCT_NVRAM_DATA_T) + sizeof(MUINT32));
        status = cctNvram_SetPartialNvramData(dtype, inSize, inOffset, pInBuf);
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_ISP_SAVE_NVRAM_DATA:
        //cctNvram_SaveNvramData(CCT_NVRAM_DATA_T dataType)
        status = cctNvram_SaveNvramData(dtype);
        *pu4RealParaOutLen = 0;
        break;
    default:
        ALOGD("Not support cmd %d", (int)op);
        break;

    }
    return status;
}


MINT32
CctHandle::
cct_HandleEmcamOp(CCT_OP_ID op,
                    MUINT32 u4ParaInLen,
                    MUINT8 *puParaIn,
                    MUINT32 u4ParaOutLen,
                    MUINT8 *puParaOut,
                    MUINT32 *pu4RealParaOutLen )
{
    MY_LOG("[%s] op(%d), u4ParaInLen(%d), puParaIn(%p), u4ParaOutLen(%d), puParaOut(%p), pu4RealParaOutLen(%p)", __FUNCTION__, \
      op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);

    int ret;
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 *i32In = NULL;
    MUINT32 u4SubCmdRealOutLen = 0;
    MBOOL enable;
    AE_MODE_CFG_T ae_cfg;
    ACDK_AE_MODE_CFG_T acdk_ae_cfg;
    // lambda function for saving result to file
    auto saveResult = [this](android::String8 sResult) -> MBOOL
    {
        FILE *fp = NULL;
        if ((fp = fopen(this->mEmResultPath.string(), "wb")) != NULL)
        {

            fwrite(sResult.string(), sResult.length(), 1, fp);
            fclose(fp);
            //MY_LOG("fwrite to %s, %s",this->mEmResultPath.string(),sResult.string());
            return MTRUE;
        }
        else
        {
            MY_LOG("fopen %s fail!",this->mEmResultPath.string());
            return MFALSE;
        }
    };
    //
    android::String8 sResult = android::String8::format("Result=Fail\n");
    saveResult(sResult);
    //
    switch(op) {

    case FT_CCT_OP_AF_BRECKET_STEP:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_BRECKET_STEP");
        if (u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            FOCUS_RANGE_T af_range;
            MINT32 currentPos = 0;
            err = NS3Av3::IAfMgr::getInstance().CCTOPAFGetFocusRange(mSensorDev, (VOID *)&af_range, &u4SubCmdRealOutLen);
            if( err != S_AF_OK || u4SubCmdRealOutLen != sizeof(FOCUS_RANGE_T) ){
                MY_LOG("[%s] CCTOPAFGetFocusRange failed", __FUNCTION__);
                break;
            }

            ACDK_AF_INFO_T af_info;
            err = NS3Av3::IAfMgr::getInstance().CCTOPAFGetAFInfo(mSensorDev, &af_info, &u4SubCmdRealOutLen);
            if( err != S_AF_OK || u4SubCmdRealOutLen != sizeof(ACDK_AF_INFO_T) ){
                MY_LOG("[%s] CCTOPAFGetAFInfo failed", __FUNCTION__);
                break;
            }
            currentPos = af_info.i4CurrPos;
            //
            MINT32 manualPos;
            i32In = (MINT32 *)puParaIn;
            manualPos = currentPos + *i32In;
            MY_LOG("AF breacket set manual af POS: %d=%d+%d, manual range = (%d, %d)", manualPos, currentPos, *i32In, af_range.i4InfPos, af_range.i4MacroPos);
            if(manualPos > af_range.i4InfPos && manualPos < af_range.i4MacroPos){
                err = NS3Av3::IAfMgr::getInstance().CCTOPMFOpeartion(mSensorDev, manualPos);
                if( err != S_AF_OK ){
                    MY_LOG("[%s] CCTOPMFOpeartion failed", __FUNCTION__);
                    break;
                }
            } else {
                err = NS3Av3::IAfMgr::getInstance().CCTOPAFOpeartion(mSensorDev);
                if( err != S_AF_OK ){
                    MY_LOG("[%s] CCTOPMFOpeartion failed", __FUNCTION__);
                    break;
                }
            }
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("[%s] CCTIF_BAD_PARAM", __FUNCTION__);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AE_BRECKET_STEP:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AE_BRECKET_STEP");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            //get realISO
            MINT64 isoBase = 100;
            FrameOutputParam_T RTParams;
            err = IAeMgr::getInstance().getRTParams(mSensorDev, *reinterpret_cast<FrameOutputParam_T*>(&RTParams));
            if( err != S_AE_OK )
            {
                MY_LOG("[%s] getRTParams fail", __FUNCTION__);
            }
            else
            {
                isoBase = (MINT64)RTParams.u4RealISOValue* 1024 / (MINT64)RTParams.u4PreviewSensorGain_x1024* 1024 / (MINT64)RTParams.u4PreviewISPGain_x1024;
                MY_LOG("setManualAEControl: isoBase(%lld),u4RealISOValue(%ld),u4PreviewSensorGain_x1024(%ld),u4PreviewISPGain_x1024(%ld)",
                        isoBase, RTParams.u4RealISOValue, RTParams.u4PreviewSensorGain_x1024, RTParams.u4PreviewISPGain_x1024);
            }
            //get current EV value
            strAEOutput aeOutput;
            i32In = (MINT32 *) puParaIn;
            IAeMgr::getInstance().switchCapureDiffEVState(mSensorDev, (MINT8) (*i32In/10), aeOutput);
            MY_LOG("setAeDiffEvValue : %d ", ((*i32In)/10));
            //set EVValue
            //AE off
            {
                MUINT32 aeMode = MTK_CONTROL_AE_MODE_OFF;
                IAeMgr::getInstance().setAEMode(mSensorDev, aeMode);
                MY_LOG("set MTK_CONTROL_AE_MODE (OFF)");
            }
            AE_SENSOR_PARAM_T strSensorParams;
            strSensorParams.u4Sensitivity   = ( MUINT32 )( ( MINT64 ) aeOutput.EvSetting.u4AfeGain*aeOutput.EvSetting.u4IspGain*isoBase/1024/1024);
            strSensorParams.u8ExposureTime  = ((MUINT32)aeOutput.EvSetting.u4Eposuretime)*1000;
            //strSensorParams.u8FrameDuration = rNewParam.i8FrameDuration;
            MY_LOG("set MTK_SENSOR_SENSITIVITY (%d)", strSensorParams.u4Sensitivity);
            MY_LOG("set MTK_SENSOR_EXPOSURE_TIME (%d) ", strSensorParams.u8ExposureTime);
            IAeMgr::getInstance().UpdateSensorParams(mSensorDev, strSensorParams);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("[%s] CCTIF_BAD_PARAM", __FUNCTION__);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AF_SET_AFMODE:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_SET_AFMODE");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            MY_LOG("setAFMode(%d)",*i32In);
            err = NS3Av3::IAfMgr::getInstance().setAFMode(mSensorDev, *i32In);
            if( err != S_AF_OK ){
                MY_LOG("[%s] setAFMode failed", __FUNCTION__);
                break;
            }
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("[%s] CCTIF_BAD_PARAM", __FUNCTION__);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AE_SET_AE_MODE:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AE_SET_AE_MODE");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            MY_LOG("setAEMode(%d)",*i32In);
            err = NS3Av3::IAeMgr::getInstance().setAEMode(mSensorDev, *i32In);
            if( err != S_AF_OK ){
                MY_LOG("[%s] setAEMode failed", __FUNCTION__);
                break;
            }
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
            err = CCTIF_BAD_PARAM;
        break;
    case FT_CCT_OP_AF_AUTOFOCUS:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_AUTOFOCUS");
        err = NS3Av3::IAfMgr::getInstance().CCTOPAFOpeartion(mSensorDev);
        if( err != S_AF_OK ){
            MY_LOG("[%s] CCTOPMFOpeartion failed", __FUNCTION__);
            break;
        }
        //
        sResult = android::String8::format("Result=OK\n");
        saveResult(sResult);
        break;
    case FT_CCT_OP_AF_FULL_SCAN_SET_INTERVAL:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_FULL_SCAN_SET_INTERVAL");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            MY_LOG("setFullScanInterval(%d)",*i32In);
            mEmFullScanInterval = *i32In;
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
            err = CCTIF_BAD_PARAM;
        break;
    case FT_CCT_OP_AF_FULL_SCAN_SET_DACSTEP:
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_FULL_SCAN_SET_DACSTEP");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            MY_LOG("setFullScanDacStep(%d)",*i32In);
            mEmFullScanDacStep = *i32In;
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
            err = CCTIF_BAD_PARAM;
        break;
    case FT_CCT_OP_AF_FULL_SCAN_TRIGGER: {
        *pu4RealParaOutLen = 0;
        MY_LOG("FT_CCT_OP_AF_FULL_SCAN_TRIGGER");
        MINT32 i4FullScanStep = ((mEmFullScanInterval & 0xFFFF) << 16) + ((mEmFullScanDacStep) & 0xFFFF);
        MY_LOG("setFullScanstep(%d)",i4FullScanStep);
        err = NS3Av3::IAfMgr::getInstance().setFullScanstep(mSensorDev, i4FullScanStep);
        err = NS3Av3::IAfMgr::getInstance().CCTOPAFOpeartion(mSensorDev);
        usleep(200*1000);   //sleep 100ms (need wait at least 2 frame)
        while(!(NS3Av3::IAfMgr::getInstance().CCTOPCheckAutoFocusDone(mSensorDev)))
        {
            usleep(500*1000);   //sleep 500ms
            MY_LOG("still wait AutoFocusDone");
        }
        MY_LOG("AutoFocusDone");
        if( err != S_AF_OK ){
            MY_LOG("[%s] setFullScanstep failed", __FUNCTION__);
            break;
        }
        //
        sResult = android::String8::format("Result=OK\n");
        saveResult(sResult);
        //
        break;
    }
    case FT_CCT_OP_AF_SET_AREA:
        MY_LOG("FT_CCT_OP_AF_SET_AREA");
        *pu4RealParaOutLen = 0;
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if(*i32In <= 0 || *i32In > 100)
            {
                MY_LOG("set CCTOPAFSetAfArea(%d), the value is invalid (must between 1~100)",*i32In);
            }
            else
            {
                err = NS3Av3::IAfMgr::getInstance().CCTOPAFSetAfArea(mSensorDev, *i32In);
                MY_LOG("set CCTOPAFSetAfArea(%d)",*i32In);
                //
                sResult = android::String8::format("Result=OK\n");
                saveResult(sResult);
            }
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AE_GET_ON_OFF:
        if (puParaOut != NULL)
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetEnableInfo(mSensorDev, (MINT32 *)puParaOut, pu4RealParaOutLen);
        else {
            err = CCTIF_BAD_PARAM;
            *pu4RealParaOutLen = 0;
        }
        break;
    case FT_CCT_OP_AE_SET_ON_OFF:
        if (u4ParaInLen == sizeof(MBOOL)) {
            enable = (MBOOL) *puParaIn;
            if (enable)
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEEnable(mSensorDev);
            else
                err = NS3Av3::IAeMgr::getInstance().CCTOPAEDisable(mSensorDev);
        } else
            err = CCTIF_BAD_PARAM;
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_FLASH_CALIBRATION:
        MY_LOG("FT_CCT_OP_FLASH_CALIBRATION");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            if(*i32In>=1)
            {
                MY_LOG("i32In(%d),cctSetSpModeQuickCalibration2",*i32In);
                err = FlashMgr::getInstance().cctSetSpModeQuickCalibration2(mSensorDev);
            }
            else
            {
                MY_LOG("i32In(%d),cctSetSpModeNormal",*i32In);
                err = FlashMgr::getInstance().cctSetSpModeNormal(mSensorDev);
            }
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_SET_CAPTURE_ISO:
        MY_LOG("FT_CCT_OP_AE_SET_CAPTURE_ISO");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            mEmCaptureIso = *i32In;
            mEmCaptureGainMode = 1;
            MY_LOG("set mEmCaptureIso(%d) mEmCaptureGainMode(%d)",mEmCaptureIso,mEmCaptureGainMode);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_SET_CAPTURE_SENSOR_GAIN:
        MY_LOG("FT_CCT_OP_AE_SET_CAPTURE_SENSOR_GAIN");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            mEmCaptureSensorGain = *i32In;
            mEmCaptureGainMode = 0;
            MY_LOG("set mEmCaptureSensorGain(%d) mEmCaptureGainMode(%d)",mEmCaptureSensorGain,mEmCaptureGainMode);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_SET_CAPTURE_EXP_TIME_US:
        MY_LOG("FT_CCT_OP_AE_SET_CAPTURE_EXP_TIME_US");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            mEmCaptureExpTimeUs = *i32In;
            MY_LOG("set mEmCaptureExpTimeUs(%d)",mEmCaptureExpTimeUs);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_APPLY_CAPTURE_AE_PARAM:
        MY_LOG("FT_CCT_OP_AE_APPLY_CAPTURE_AE_PARAM");
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            err = NS3Av3::IAeMgr::getInstance().getPreviewParams(mSensorDev, ae_cfg);
            //
            acdk_ae_cfg.u4ExposureMode = ae_cfg.u4ExposureMode;
            acdk_ae_cfg.u4Eposuretime = ae_cfg.u4Eposuretime;
            acdk_ae_cfg.u4GainMode = mEmCaptureGainMode;
            acdk_ae_cfg.u4AfeGain = ae_cfg.u4AfeGain;
            acdk_ae_cfg.u4IspGain = ae_cfg.u4IspGain;
            acdk_ae_cfg.u4ISO = ae_cfg.u4RealISO;
            acdk_ae_cfg.u2FrameRate = ae_cfg.u2FrameRate;
            acdk_ae_cfg.u2CaptureFlareGain = ae_cfg.i2FlareGain;
            acdk_ae_cfg.u2CaptureFlareValue = ae_cfg.i2FlareOffset;
            //
            if(mEmCaptureGainMode == 1) //iso
            {
                if(mEmCaptureIso == 0)
                {
                    MY_LOG("mEmCaptureGainMode == 1 but mEmCaptureIso == 0, no need to update capture iso");
                }
                else
                {
                    acdk_ae_cfg.u4GainMode = 1;
                    acdk_ae_cfg.u4ISO = mEmCaptureIso;
                    MY_LOG("apply u4GainMode(%d) u4ISO(%d)",acdk_ae_cfg.u4GainMode,acdk_ae_cfg.u4ISO);
                }
            }
            else    //sensor gain
            {
                if(mEmCaptureSensorGain == 0)
                {
                    MY_LOG("mEmCaptureGainMode == 0 but mEmCaptureSensorGain == 0, no need to update capture sensor gain");
                }
                else
                {
                    acdk_ae_cfg.u4GainMode = 0;
                    acdk_ae_cfg.u4AfeGain = mEmCaptureSensorGain;
                    MY_LOG("apply u4GainMode(%d) u4AfeGain(%d)",acdk_ae_cfg.u4GainMode,acdk_ae_cfg.u4AfeGain);
                }
            }
            //
            if(mEmCaptureExpTimeUs == 0)
            {
                MY_LOG("mEmCaptureExpTimeUs == 0 , no need to update capture exp time");
            }
            else
            {
                acdk_ae_cfg.u4Eposuretime = mEmCaptureExpTimeUs;
                MY_LOG("apply u4Eposuretime(%d)",acdk_ae_cfg.u4Eposuretime);
            }
            //
            err = NS3Av3::IAeMgr::getInstance().CCTOSetCaptureParams(mSensorDev, (VOID *)(&acdk_ae_cfg));
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        *pu4RealParaOutLen = 0;
        break;
    case FT_CCT_OP_AE_SET_VHDR_RATIO:
        MY_LOG("FT_CCT_OP_AE_SET_VHDR_RATIO");
        *pu4RealParaOutLen = 0;
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            IAeMgr::getInstance().setEMVHDRratio(mSensorDev, (MUINT32)(*i32In));
            MY_LOG("set setEMVHDRratio(%d)",*i32In);
            //
            //AE mode off
            {
                MUINT32 aeMode = MTK_CONTROL_AE_MODE_OFF;
                IAeMgr::getInstance().setAEMode(mSensorDev, aeMode);
                MY_LOG("set MTK_CONTROL_AE_MODE (OFF)");
            }
            //UpdateSensorParams to apply hdr ratio
            AE_SENSOR_PARAM_T strSensorParams;
            IAeMgr::getInstance().getSensorParams(mSensorDev, strSensorParams);
            IAeMgr::getInstance().UpdateSensorParams(mSensorDev, strSensorParams);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AWB_SET_MTK_ENABLE:
        MY_LOG("FT_CCT_OP_AWB_SET_MTK_ENABLE");
        *pu4RealParaOutLen = 0;
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            m_pIHal3A->send3ACtrl(E3ACtrl_SetAwbBypCalibration, *i32In, 0);
            MY_LOG("send3ACtrl(E3ACtrl_SetAwbBypCalibration , %d, 0)",*i32In);
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AWB_SET_SENSOR_ENABLE:
        MY_LOG("FT_CCT_OP_AWB_SET_SENSOR_ENABLE");
        *pu4RealParaOutLen = 0;
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            IHalSensor *pHalSensorObj;
            // Sensor hal init
            IHalSensorList* const pIHalSensorList = MAKE_HalSensorList();
            pHalSensorObj = pIHalSensorList->createSensor("cct_sensor_access", 0);

            if(pHalSensorObj == NULL) {
                MY_LOG("[AAA Sensor Mgr] Can not create SensorHal obj\n");
                err = CCTIF_UNKNOWN_ERROR;
            }
            else
            {
                MINT32 errSensor = SENSOR_NO_ERROR;
                errSensor = pHalSensorObj->sendCommand(pIHalSensorList->querySensorDevIdx(mSensorDev),
                                                            SENSOR_CMD_SET_SENSOR_OTP_AWB_CMD,(MUINTPTR)i32In,0,0);
                MY_LOG("pHalSensorObj->sendCommand(SENSOR_CMD_SET_SENSOR_OTP_AWB_CMD , %d, 0)",*i32In);
                
                if (errSensor != SENSOR_NO_ERROR) {
                    MY_LOGE("pHalSensorObj->sendCommand(SENSOR_CMD_SET_SENSOR_OTP_AWB_CMD), err=%d", errSensor);
                    err = CCTIF_UNKNOWN_ERROR;
                }
            }
            //
            sResult = android::String8::format("Result=OK\n");
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_AE_EV_CALIBRATION:
        MY_LOG("FT_CCT_OP_AE_EV_CALIBRATION");
        *pu4RealParaOutLen = 0;
        if(u4ParaInLen == sizeof(MINT32) && puParaIn != NULL)
        {
            i32In = (MINT32 *)puParaIn;
            MINT32 iInputValue = *i32In;
            MINT32 iAECurrentEV = 0;
            MINT32 i4BVOffset = 0;
            MUINT32 iOutLen = 0;
            MINT32 iResult = 0;
            m_pIHal3A->send3ACtrl(NS3Av3::E3ACtrl_GetCurrentEV, reinterpret_cast<MINTPTR>(&iAECurrentEV), reinterpret_cast<MINTPTR>(&iOutLen));
            m_pIHal3A->send3ACtrl(NS3Av3::E3ACtrl_GetBVOffset, reinterpret_cast<MINTPTR>(&i4BVOffset), reinterpret_cast<MINTPTR>(&iOutLen));
            iResult = (iAECurrentEV + i4BVOffset) - iInputValue + 50;
            MY_LOG("iResult(%d) = (iAECurrentEV(%d) + i4BVOffset(%d)) - iInputValue(%d) + 50",iResult, iAECurrentEV, i4BVOffset, iInputValue);
            //
            sResult = android::String8::format("Result=%d\n",iResult);
            saveResult(sResult);
        }
        else
        {
            MY_LOG("CCTIF_BAD_PARAM u4ParaInLen(%d),puParaIn(%p)",u4ParaInLen,puParaIn);
            err = CCTIF_BAD_PARAM;
        }
        break;
    case FT_CCT_OP_SET_RESULT_FILE_PATH:
        MY_LOG("FT_CCT_OP_SET_RESULT_FILE_PATH");
        *pu4RealParaOutLen = 0;
        char cmd[1024];
        memset(cmd,0,1024);
        MINT32 cmdlen;
        cmdlen = u4ParaInLen;
        strncpy(cmd, (char *)puParaIn, cmdlen);
        //
        mEmResultPath = String8(cmd);
        MY_LOG("mEmResultPath = %s, length(%d), u4ParaInLen(%d)",mEmResultPath.string(),mEmResultPath.length(),u4ParaInLen);
        //
        sResult = android::String8::format("Result=OK\n");
        saveResult(sResult);
        //
        break;
    default:
        MY_LOG("Not support cmd %d", (int)op);
        break;

    }
    return err;
}


MINT32
CctHandle::
cct_OpDispatch(CCT_OP_ID op,
                MUINT32 u4ParaInLen,
                MUINT8 *puParaIn,
                MUINT32 u4ParaOutLen,
                MUINT8 *puParaOut,
                MUINT32 *pu4RealParaOutLen )
{
    MINT32 status = 0;

    if (op >= FT_CCT_OP_SENSOR_START && op < FT_CCT_OP_3A_START ) {
        status = cct_HandleSensorOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_3A_START && op < FT_CCT_OP_ISP_START ) {
        status = cct_Handle3AOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_ISP_START && op < FT_CCT_OP_NVRAM_START ) {
        status = cct_HandleIspOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_SHELL_START ) {
        status = cct_HandleNvramOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else if (op >= FT_CCT_OP_EMCAM_START && op < FT_CCT_OP_END ) {
        status = cct_HandleEmcamOp(op, u4ParaInLen, puParaIn, u4ParaOutLen, puParaOut, pu4RealParaOutLen);
    } else {
        //???
    }

    return status;
}


MINT32
CctHandle::
cct_GetCctOutBufSize(CCT_OP_ID op, MINT32 dtype)
{
    MINT32 idx;

    if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_END ) {
        // determine the output data size by according the op and dtype
        if ( op == FT_CCT_OP_ISP_SAVE_NVRAM_DATA || op == FT_CCT_OP_ISP_SET_NVRAM_DATA )
            return 0;
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_DATA ) {
            if (dtype >= CCT_NVRAM_DATA_ENUM_MAX )
                return -2;      // data type not found
            else
                return giCctNvramCmdOutBufSize[dtype][1];
        }
        return -1;
    } else {
        // determine the output data size by according the op only
        idx=0;
        for(int i = 0; i < CCT_CMD_OUTBUF_SIZE_COUNT; i++)
        {
            if(giCctCmdOutBufSize[i][0] == op)
            {
                idx = i;
                break;
            }
        }

        if (giCctCmdOutBufSize[idx][0] == op)
            return giCctCmdOutBufSize[idx][1];
        else
            return -1;      // OP_NOT_FOUND
    }
}


MINT32
CctHandle::
cct_GetCctOutBufSize(CCT_OP_ID op, MUINT32 u4ParaInLen, MUINT8 *puParaIn)
{
    MINT32 idx;
    MINT32 dtype;

    if (op >= FT_CCT_OP_NVRAM_START && op < FT_CCT_OP_NVRAM_TYPE_MAX ) {
        // determine the output data size by according the op and dtype
        if ( puParaIn == NULL || u4ParaInLen < 4 )
            return -2;
        if ( op == FT_CCT_OP_ISP_SAVE_NVRAM_DATA || op == FT_CCT_OP_ISP_SET_NVRAM_DATA )
            return 0;
        if ( op == FT_CCT_OP_ISP_GET_NVRAM_DATA ) {
            if (u4ParaInLen >= sizeof(MINT32)) {
                dtype = *((MINT32 *)puParaIn);
                if (dtype >= CCT_NVRAM_DATA_ENUM_MAX )
                    return -2;      // data type not found
                else {
                    ALOGD("CCT NVRAM cmd enum[%d] Out Buffer size:%d",dtype,giCctNvramCmdOutBufSize[dtype][1]);
                    return giCctNvramCmdOutBufSize[dtype][1];
                }
            } else
                return -2;
        }
        return -1;
    } else {
        // determine the output data size by according the op only
        idx=0;
        for(int i = 0; i < CCT_CMD_OUTBUF_SIZE_COUNT; i++)
        {
            if(giCctCmdOutBufSize[i][0] == op)
            {
                idx = i;
                break;
            }
        }

        if (giCctCmdOutBufSize[idx][0] == op) {
            ALOGD("CCT cmd Out Buffer size:%d",giCctCmdOutBufSize[idx][1]);
            return giCctCmdOutBufSize[idx][1];
        } else
            return 0;      // for test only
            //return -1;      // OP_NOT_FOUND
    }
}


MINT32
CctHandle::
cctNvram_GetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf, MINT32 outSize, void *pOutBuf, MINT32 *pRealOutSize)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        {
        winmo_cct_shading_comp_struct*const pShadingPara = reinterpret_cast<winmo_cct_shading_comp_struct*>(pOutBuf);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        if (pLscMgr == NULL)
        {
            MY_ERR("GET_SHADING_PARA fail! NULL pLscMgr!");
            err = CCTIF_BAD_PARAM;
            break;
        }
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        if (pTbl == NULL)
        {
            MY_ERR("GET_SHADING_PARA fail to get table!");
            err = CCTIF_BAD_PARAM;
            break;
        }
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        pShadingPara->SHADING_EN          = pLscMgr->getOnOff();
        pShadingPara->SHADINGBLK_XNUM     = rCfg.rCfgBlk.i4BlkX+1;
        pShadingPara->SHADINGBLK_YNUM     = rCfg.rCfgBlk.i4BlkY+1;
        pShadingPara->SHADINGBLK_WIDTH    = rCfg.rCfgBlk.i4BlkW;
        pShadingPara->SHADINGBLK_HEIGHT   = rCfg.rCfgBlk.i4BlkH;
        pShadingPara->SHADING_RADDR       = 0;
        pShadingPara->SD_LWIDTH           = rCfg.rCfgBlk.i4BlkLastW;
        pShadingPara->SD_LHEIGHT          = rCfg.rCfgBlk.i4BlkLastH;
        pShadingPara->SDBLK_RATIO00       = 32;
        pShadingPara->SDBLK_RATIO01       = 32;
        pShadingPara->SDBLK_RATIO10       = 32;
        pShadingPara->SDBLK_RATIO11       = 32;
        *pRealOutSize = sizeof(winmo_cct_shading_comp_struct);
        }
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        CCT_SHADING_TAB_STRUCT *CctTabPtr;

        if  (sizeof(CCT_SHADING_TAB_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if  ((inSize < 4) || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        CctTabPtr = (CCT_SHADING_TAB_STRUCT *) pInBuf;

        CCT_SHADING_TAB_STRUCT*const pShadingtabledata  = reinterpret_cast<CCT_SHADING_TAB_STRUCT*> (pOutBuf);
        pShadingtabledata->length= MAX_SHADING_PvwFrm_SIZE;
        pShadingtabledata->color_temperature = CctTabPtr->color_temperature;
        pShadingtabledata->offset = CctTabPtr->offset;
        pShadingtabledata->mode= CctTabPtr->mode;

        NSIspTuning::ESensorMode_T eLscScn = (NSIspTuning::ESensorMode_T)pShadingtabledata->mode;
        MUINT8* pDst = (MUINT8*)(pShadingtabledata->table);
        MUINT32 u4CtIdx = pShadingtabledata->color_temperature;
        MUINT32 u4Size = pShadingtabledata->length;

        ALOGD("[%s +] GET_SHADING_TABLE: SensorMode(%d),CT(%d),Src(%p),Size(%d)", __FUNCTION__,
            eLscScn, u4CtIdx, pDst, u4Size);

        if (pDst == NULL)
        {
            MY_ERR("GET_SHADING_TABLE: NULL pDst");
            err = CCTIF_BAD_PARAM;
            break;
        }

        if (u4CtIdx >= 4)
        {
            MY_ERR("GET_SHADING_TABLE: Wrong CtIdx(%d)", u4CtIdx);
            err = CCTIF_BAD_PARAM;
            break;
        }

        //NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        //const ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();

        // read from nvram buffer
        MUINT32* pSrc = pLscNvram->getLut(ESensorMode_Capture, u4CtIdx);
        ::memcpy(pDst, pSrc, u4Size*sizeof(MUINT32));

        ALOGD("[%s -] GET_SHADING_TABLE", __FUNCTION__);
        *pRealOutSize = sizeof(CCT_SHADING_TAB_STRUCT);
        }
        break;
    case CCT_NVRAM_DATA_LSC:
        if  (sizeof(NVRAM_CAMERA_SHADING_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ::memcpy(pOutBuf, &m_rBuf_SD, sizeof(NVRAM_CAMERA_SHADING_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_SHADING_STRUCT);
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        if  (sizeof(AE_PLINETABLE_T) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
// [hal1] pointer size align to 8 bytes, but CCT V2.0 align to 4 bytes, need to handle it
// [hal3] 32/64bits load might has different pointer size, pointer size always align to 8 bytes for ae pline
#ifdef CAM_CCT_HAL1_AEPLINE
        {
            MY_LOG("CCT get NVRAM_DATA_AE_PLINE for HAL-1");

            MUINT32 u4OriSize  = sizeof(AE_PLINETABLE_T);

            AE_PLINETABLE_T xOri;
            AE_PLINETABLE_T_CCT xNew;

            MY_LOG("CCT get NVRAM_DATA_AE_PLINE for HAL1 CCTOPAEGetPlineNVRAM start");
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetPlineNVRAM(mSensorDev, (VOID *) &xOri, (MUINT32 *) &u4OriSize);
            MY_LOG("CCT get NVRAM_DATA_AE_PLINE for HAL1 CCTOPAEGetPlineNVRAM end");
            ::memcpy(&(xNew.sAEScenePLineMapping), &(xOri.sAEScenePLineMapping), sizeof(strAESceneMapping));
            ::memcpy(&(xNew.AEPlineInfo), &(xOri.AEPlineInfo), sizeof(strAEPLineInfomation));
            ::memcpy(&(xNew.AEGainList), &(xOri.AEGainList), sizeof(strAEPLineGainList));
            for(MINT16 indexAE = 0; indexAE < MAX_PLINE_TABLE; indexAE++ )
            {
                ::memcpy(&(xNew.AEPlineTable.sPlineTable[indexAE]), &(xOri.AEPlineTable.sPlineTable[indexAE]), sizeof(strAETable) - 8);
            }
            ::memcpy(pOutBuf, &xNew, sizeof(AE_PLINETABLE_T_CCT));
            *pRealOutSize = sizeof(AE_PLINETABLE_T_CCT);
        }
#else
        {
            MY_LOG("CCT get NVRAM_DATA_AE_PLINE for HAL-3");
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetPlineNVRAM(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);
        }
#endif
        break;
    case CCT_NVRAM_DATA_AE:
        if  (sizeof(AE_NVRAM_T)*CAM_SCENARIO_NUM != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &(m_rBuf_3A.rAENVRAM[0]), sizeof(AE_NVRAM_T)*AE_NVRAM_IDX_NUM);
        //*pRealOutSize = sizeof(AE_NVRAM_T)*AE_NVRAM_IDX_NUM;

        //or
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetNVRAMParam(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
    case CCT_NVRAM_DATA_AF:
        if  (sizeof(NVRAM_LENS_PARA_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &m_rBuf_LN, sizeof(NVRAM_LENS_PARA_STRUCT));
        //*pRealOutSize = sizeof(NVRAM_LENS_PARA_STRUCT);

        //or
        err = NS3Av3::IAfMgr::getInstance().CCTOPAFGetNVRAMParam(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
    case CCT_NVRAM_DATA_AWB:
        if  (sizeof(AWB_NVRAM_T)*CAM_SCENARIO_NUM != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //::memcpy(pOutBuf, &(m_rBuf_3A.rAWBNVRAM[0]), sizeof(AWB_NVRAM_T)*CCT_AWB_NVRAM_TBL_NO);
        //*pRealOutSize = sizeof(AWB_NVRAM_T)*CCT_AWB_NVRAM_TBL_NO;

        //or
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBGetNVRAMParam(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);

        break;
    case CCT_NVRAM_DATA_ISP:
        if  (sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ::memcpy(pOutBuf, &m_rBuf_ISP, sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        *pRealOutSize = sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT);
        break;
    case CCT_NVRAM_DATA_FEATURE:
        {
        if  (sizeof(NVRAM_CAMERA_FEATURE_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_FEATURE_STRUCT* pNvram;
        NVRAM_CAMERA_FEATURE_STRUCT* pCctNvram = reinterpret_cast<NVRAM_CAMERA_FEATURE_STRUCT*>(pOutBuf);
        err = NvBufUtil::getInstance().getBufAndRead(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pNvram, 1);
        *pCctNvram = *pNvram;
        *pRealOutSize = sizeof(NVRAM_CAMERA_FEATURE_STRUCT);
        }
        break;
    case CCT_NVRAM_DATA_STROBE:
        if  (sizeof(NVRAM_CAMERA_STROBE_STRUCT) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ret = FlashMgr::getInstance().cctReadNvramToPcMeta(mSensorDev, (VOID *)pOutBuf, (MUINT32 *) pRealOutSize);
        if(ret!=1)
            err=CCTIF_UNKNOWN_ERROR;
        *pRealOutSize = sizeof(NVRAM_CAMERA_STROBE_STRUCT);
        break;
    case CCT_NVRAM_DATA_FLASH_AWB:
        if  (sizeof(FLASH_AWB_NVRAM_T) != outSize || ! pRealOutSize || ! pOutBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ::memcpy(pOutBuf, &(m_rBuf_3A.rFlashAWBNVRAM), sizeof(FLASH_AWB_NVRAM_T));
        *pRealOutSize = sizeof(FLASH_AWB_NVRAM_T);
        break;
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }

    return err;
}


MINT32
CctHandle::
cctNvram_SetNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT8 *pInBuf)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        {
        if  ( sizeof(winmo_cct_shading_comp_struct) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        winmo_cct_shading_comp_struct*const pShadingPara = reinterpret_cast<winmo_cct_shading_comp_struct*>(pInBuf);

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        // only change grid number to NVRAM, do not let HW take effect immediately.
        pLscData->GridXNum = pShadingPara->SHADINGBLK_XNUM + 1;
        pLscData->GridYNum = pShadingPara->SHADINGBLK_YNUM + 1;
        pLscData->Width    = rCfg.i4ImgWd;
        pLscData->Height   = rCfg.i4ImgHt;
        }
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        if  ( sizeof(CCT_SHADING_TAB_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }

        CCT_SHADING_TAB_STRUCT*const pShadingtabledata  = reinterpret_cast<CCT_SHADING_TAB_STRUCT*> (pInBuf);

        NSIspTuning::ESensorMode_T eLscScn = (NSIspTuning::ESensorMode_T)pShadingtabledata->mode;
        //MUINT8* pSrc = (MUINT8*)(pShadingtabledata->pBuffer);
        MUINT8* pSrc = (MUINT8*)(pShadingtabledata->table);
        MUINT32 u4CtIdx = pShadingtabledata->color_temperature;
        MUINT32 u4Size = pShadingtabledata->length;

        MY_LOG("[%s +] SET_SHADING_TABLE: SensorMode(%d),CT(%d),Src(%p),Size(%d)", __FUNCTION__,
            eLscScn, u4CtIdx, pSrc, u4Size);

        if (pSrc == NULL)
        {
            MY_ERR("SET_SHADING_TABLE: NULL pSrc");
            err = CCTIF_BAD_PARAM;
            break;
        }

        if (u4CtIdx >= 4)
        {
            MY_ERR("SET_SHADING_TABLE: Wrong CtIdx(%d)", u4CtIdx);
            err = CCTIF_BAD_PARAM;
            break;
        }

        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        //const ISP_SHADING_STRUCT* pLscData = pLscNvram->getLscNvram();

        // write to nvram buffer
        MUINT32* pDst = pLscNvram->getLut(ESensorMode_Capture, u4CtIdx);
        ::memcpy(pDst, pSrc, u4Size*sizeof(MUINT32));
        // reset flow to validate ?
        pLscMgr->CCTOPReset();
        }
        break;
    case CCT_NVRAM_DATA_LSC:
        // This case combined CCT_NVRAM_DATA_LSC_PARA and CCT_NVRAM_DATA_LSC_TABLE
        {
        if  ( sizeof(NVRAM_CAMERA_SHADING_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_SHADING_STRUCT* const pShadingNvram  = reinterpret_cast<NVRAM_CAMERA_SHADING_STRUCT*> (pInBuf);
        // get LSC nvram
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);
        ISP_SHADING_STRUCT* pDst = pLscNvram->getLscNvram();
        // need to overwrite sensor real witdth, height
        NSIspTuning::ILscMgr* pLscMgr = NSIspTuning::ILscMgr::getInstance((ESensorDev_T)m_eSensorEnum);
        const NSIspTuning::ILscTbl* pTbl = pLscMgr->getCapLut(2);
        const NSIspTuning::ILscTable::Config rCfg = pTbl->getConfig();

        // write to nvram buffer
        ::memcpy(pDst, &(pShadingNvram->Shading), sizeof(ISP_SHADING_STRUCT));
        pDst->Width    = rCfg.i4ImgWd;
        pDst->Height   = rCfg.i4ImgHt;

        // reset flow to validate ?
        pLscMgr->CCTOPReset();
        }
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        if  ( sizeof(AE_PLINETABLE_T) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
// [hal3] 32/64bits load might has different pointer size, pointer size always align to 8 bytes for ae pline
// [hal1] pointer size align to 8 bytes, but CCT V2.0 align to 4 bytes, need to handle it
#ifdef CAM_CCT_HAL1_AEPLINE
        {
            MY_LOG("CCT set NVRAM_DATA_AE_PLINE for HAL-1");

            AE_PLINETABLE_T xNew;
            MUINT32 u4OriSize  = sizeof(AE_PLINETABLE_T);

            err = NS3Av3::IAeMgr::getInstance().CCTOPAEGetPlineNVRAM(mSensorDev, (VOID *) &xNew, (MUINT32 *) &u4OriSize);

            ::memcpy(&(xNew.sAEScenePLineMapping), &(((AE_PLINETABLE_T_CCT*)pInBuf)->sAEScenePLineMapping), sizeof(strAESceneMapping));
            ::memcpy(&(xNew.AEPlineInfo), &(((AE_PLINETABLE_T_CCT*)pInBuf)->AEPlineInfo), sizeof(strAEPLineInfomation));
            ::memcpy(&(xNew.AEGainList), &(((AE_PLINETABLE_T_CCT*)pInBuf)->AEGainList), sizeof(strAEPLineGainList));

            for(MINT16 indexAE = 0; indexAE < MAX_PLINE_TABLE; indexAE++ )
            {
                ::memcpy(&(xNew.AEPlineTable.sPlineTable[indexAE]), &(((AE_PLINETABLE_T_CCT*)pInBuf)->AEPlineTable.sPlineTable[indexAE]), sizeof(strAETable) - 8);
            }

            err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyPlineNVRAM(mSensorDev, (VOID *) &xNew);
        }
#else
        {
            MY_LOG("CCT set NVRAM_DATA_AE_PLINE for HAL-3");
            err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyPlineNVRAM(mSensorDev, (VOID *)pInBuf);
        }
#endif
        break;
    case CCT_NVRAM_DATA_AE:
        {
        if  ( sizeof(AE_NVRAM_T) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAeMgr::getInstance().CCTOPAEApplyNVRAMParam(mSensorDev, (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
    case CCT_NVRAM_DATA_AF:
        {
        if  ( sizeof(NVRAM_LENS_DATA_PARA_STRUCT) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAfMgr::getInstance().CCTOPAFApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAfMgr::getInstance().CCTOPAFApplyNVRAMParam(mSensorDev, (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
    case CCT_NVRAM_DATA_AWB:
        {
        if  ( sizeof(AWB_NVRAM_T) != (inSize-4) || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        MINT32 scenarioMode = *(MINT32 *)pInBuf;
        //err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBApplyNVRAMParam(mSensorDev, (VOID *)(pInBuf+4), scenarioMode);
        break;
        }
    case CCT_NVRAM_DATA_ISP:
        {
        // TO DO ......
        // There is no related implementation found in the previous CCT. Need to add the implementation here.
        if ( sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT) != inSize || ! pInBuf) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        //NVRAM_CAMERA_ISP_PARAM_STRUCT* pIspParam = reinterpret_cast<NVRAM_CAMERA_ISP_PARAM_STRUCT*>(pInBuf);
        //err = cctNvram_SetIspNvramData( &(pIspParam->ISPRegs) );
        memcpy((void *)&m_rBuf_ISP, (void *)pInBuf, sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT));
        break;
        }
    case CCT_NVRAM_DATA_FEATURE:
        {
        if  ( sizeof(NVRAM_CAMERA_FEATURE_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        NVRAM_CAMERA_FEATURE_STRUCT* pNvram;
        NVRAM_CAMERA_FEATURE_STRUCT* pCctNvram = reinterpret_cast<NVRAM_CAMERA_FEATURE_STRUCT*>(pInBuf);

        //test only
        MUINT32 th_old;
        MUINT32 th_apply;
        MUINT32 th_new;

        err = NvBufUtil::getInstance().getBuf(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pNvram);

        //test only
        th_old = pNvram->mfll.mfll_iso_th;
        th_apply = pCctNvram->mfll.mfll_iso_th;

        *pNvram = *pCctNvram;

        //test only
        NVRAM_CAMERA_FEATURE_STRUCT* pReadNvram;
        err = NvBufUtil::getInstance().getBuf(CAMERA_NVRAM_DATA_FEATURE, mSensorDev, (void*&)pReadNvram);
        th_new = pReadNvram->mfll.mfll_iso_th;
        MY_LOG("apply mfll_iso_th old(%d) apply(%d) new(%d)", th_old, th_apply, th_new );
        }
        break;
    case CCT_NVRAM_DATA_STROBE:
        if  ( sizeof(NVRAM_CAMERA_STROBE_STRUCT) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        ret = FlashMgr::getInstance().cctSetNvdataMeta(mSensorDev, pInBuf, inSize);
        if(ret!=1)
            err=CCTIF_UNKNOWN_ERROR;
        break;
    case CCT_NVRAM_DATA_FLASH_AWB:
        if  ( sizeof(FLASH_AWB_NVRAM_T) !=  inSize || ! pInBuf ) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        err = NS3Av3::IAwbMgr::getInstance().CCTOPFlashAWBApplyNVRAMParam(mSensorDev, (VOID *)pInBuf);
        break;
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }
    return err;
}


MINT32
CctHandle::
cctNvram_SetPartialNvramData(CCT_NVRAM_DATA_T dataType, MINT32 inSize, MUINT32 bufOffset, MUINT8 *pInBuf)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    switch (dataType) {
    case CCT_NVRAM_DATA_ISP:
        {
        MUINT8 *pIspParamBuf;
        if ( inSize <= 0 || ! pInBuf || bufOffset >= sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT)) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        if ( (inSize + bufOffset) > sizeof(NVRAM_CAMERA_ISP_PARAM_STRUCT)) {
            err = CCTIF_BAD_PARAM;
            break;
        }
        pIspParamBuf = (MUINT8 *)&m_rBuf_ISP;
        pIspParamBuf += bufOffset;
        memcpy((void *)pIspParamBuf, (void *)pInBuf, inSize);
        }
        break;
    default:
        err = CCTIF_BAD_PARAM;
        break;
    }
    return err;
}



MINT32
CctHandle::
cctNvram_SaveNvramData(CCT_NVRAM_DATA_T dataType)
{
    MINT32 err = CCTIF_NO_ERROR;
    MINT32 ret;

    switch (dataType) {
    case CCT_NVRAM_DATA_LSC_PARA:
        break;
    case CCT_NVRAM_DATA_LSC_TABLE:
        {
        NSIspTuning::ILscNvram* pLscNvram = NSIspTuning::ILscNvram::getInstance((ESensorDev_T)m_eSensorEnum);

        if (pLscNvram->writeNvramTbl()) {
            MY_LOG("[%s] SDTBL_SAVE_TO_NVRAM OK", __FUNCTION__);
        }
        else {
            MY_ERR("SDTBL_SAVE_TO_NVRAM fail");
            err = CCTIF_UNKNOWN_ERROR;
        }
        }
        break;
    case CCT_NVRAM_DATA_AE_PLINE:
        err = NS3Av3::IAeMgr::getInstance().CCTOPAESavePlineNVRAM(mSensorDev);
        break;
    case CCT_NVRAM_DATA_AE:
        err = NS3Av3::IAeMgr::getInstance().CCTOPAESaveNVRAMParam(mSensorDev);
        break;
    case CCT_NVRAM_DATA_AF:
        err = NS3Av3::IAfMgr::getInstance().CCTOPAFSaveNVRAMParam(mSensorDev);
        break;
    case CCT_NVRAM_DATA_AWB:
        err = NS3Av3::IAwbMgr::getInstance().CCTOPAWBSaveNVRAMParam(mSensorDev);
        break;
    case CCT_NVRAM_DATA_ISP:
        MY_LOG("IMP_CCT_CTRL( ACDK_CCT_OP_ISP_SAVE_TO_NVRAM )");

        ret = NvBufUtil::getInstance().write(CAMERA_NVRAM_DATA_ISP, m_eSensorEnum);
        if  ( 0 != ret ) {
            MY_ERR("[ACDK_CCT_OP_ISP_SAVE_TO_NVRAM] write fail err=%d(0x%x)\n", ret , ret );
            err = CCTIF_UNKNOWN_ERROR;
        } else
            MY_LOG("IMP_CCT_CTRL( ACDK_CCT_OP_ISP_SAVE_TO_NVRAM ) done");
        break;
    case CCT_NVRAM_DATA_FEATURE:
        err = NvBufUtil::getInstance().write(CAMERA_NVRAM_DATA_FEATURE, mSensorDev);
        break;
    case CCT_NVRAM_DATA_STROBE:
        MY_LOG("ACDK_CCT_OP_STROBE_WRITE_NVRAM");
        ret = FlashMgr::getInstance().cctWriteNvram(mSensorDev);
        if(ret!=1)
            err=CCTIF_UNKNOWN_ERROR;
        break;
    case CCT_NVRAM_DATA_FLASH_AWB:
        err = NS3Av3::IAwbMgr::getInstance().CCTOPFlashAWBSaveNVRAMParam(mSensorDev);
        break;
    default:
        ALOGD("Not support cmd %d", (int)dataType);
        break;
    }
    return err;
}


MINT32
CctHandle::
cctNvram_SetIspNvramData(ISP_NVRAM_REGISTER_STRUCT *pIspRegs)
{
    MINT32 index;

    index = pIspRegs->Idx.SL2F;
    if (index < NVRAM_SL2F_TBL_NUM) {
        m_rISPRegs.SL2F[index] = (pIspRegs->SL2F[index]);
        m_rISPRegsIdx.SL2F = static_cast<MUINT8>(index);
        ISP_MGR_SL2F_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2F[index]));
        ISP_MGR_SL2G_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2F[index]));
    }

    index = pIspRegs->Idx.DBS;
    if (index < NVRAM_DBS_TBL_NUM) {
        m_rISPRegs.DBS[index] = (pIspRegs->DBS[index]);
        m_rISPRegsIdx.DBS = static_cast<MUINT8>(index);
        ISP_MGR_DBS_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->DBS[index]));
        ISP_MGR_DBS2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->DBS[index]));
    }

    index = pIspRegs->Idx.OBC;
    if (index < NVRAM_OBC_TBL_NUM) {
        m_rISPRegs.OBC[index] = (pIspRegs->OBC[index]);
        m_rISPRegsIdx.OBC = static_cast<MUINT8>(index);
        ISP_MGR_OBC_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->OBC[index]));
        ISP_MGR_OBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->OBC[index]));
    }

    index = pIspRegs->Idx.BNR_BPC;
    if (index < NVRAM_BPC_TBL_NUM) {
        m_rISPRegs.BNR_BPC[index] = (pIspRegs->BNR_BPC[index]);
        m_rISPRegsIdx.BNR_BPC = static_cast<MUINT8>(index);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_BPC[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_BPC[index]));
    }

    index = pIspRegs->Idx.BNR_NR1;
    if (index < NVRAM_NR1_TBL_NUM) {
        m_rISPRegs.BNR_NR1[index] = (pIspRegs->BNR_NR1[index]);
        m_rISPRegsIdx.BNR_NR1 = static_cast<MUINT8>(index);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_NR1[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_NR1[index]));
    }

    index = pIspRegs->Idx.BNR_PDC;
    if (index < NVRAM_PDC_TBL_NUM) {
        m_rISPRegs.BNR_PDC[index] = (pIspRegs->BNR_PDC[index]);
        m_rISPRegsIdx.BNR_PDC = static_cast<MUINT8>(index);
        ISP_MGR_BNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_PDC[index]));
        ISP_MGR_BNR2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->BNR_PDC[index]));
    }
#if 0
    index = pIspRegs->Idx.RMM;
    if (index < NVRAM_RMM_TBL_NUM) {
        m_rISPRegs.RMM[index] = (pIspRegs->RMM[index]);
        m_rISPRegsIdx.RMM = static_cast<MUINT8>(index);
        ISP_MGR_RMM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RMM[index]));
        ISP_MGR_RMM2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RMM[index]));
    }
#endif
    index = pIspRegs->Idx.RNR;
    if (index < NVRAM_RNR_TBL_NUM) {
        m_rISPRegs.RNR[index] = (pIspRegs->RNR[index]);
        m_rISPRegsIdx.RNR = static_cast<MUINT8>(index);
        ISP_MGR_RNR_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->RNR[index]));
    }

    index = pIspRegs->Idx.SL2;
    if (index < NVRAM_SL2_TBL_NUM) {
        m_rISPRegs.SL2[index] = (pIspRegs->SL2[index]);
        m_rISPRegsIdx.SL2 = static_cast<MUINT8>(index);
        ISP_MGR_SL2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->SL2[index]));
    }

    index = pIspRegs->Idx.UDM;
    if (index < NVRAM_UDM_TBL_NUM) {
        m_rISPRegs.UDM[index] = (pIspRegs->UDM[index]);
        m_rISPRegsIdx.UDM = static_cast<MUINT8>(index);
        ISP_MGR_UDM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->UDM[index]));
    }

#if 0
    index = pIspRegs->Idx.CCM;
    if (index < NVRAM_CCM_TBL_NUM) {
        m_rISPRegs.CCM[index] = (pIspRegs->CCM[index]);
        m_rISPRegsIdx.CCM = static_cast<MUINT8>(index);
        ISP_MGR_CCM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->CCM[index]));
    }

    index = pIspRegs->Idx.GGM;
    if (index < NVRAM_GGM_TBL_NUM) {
        m_rISPRegs.GGM[index] = (pIspRegs->GGM[index]);
        m_rISPRegsIdx.GGM = static_cast<MUINT8>(index);
        ISP_MGR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->GGM[index]));
    }

    index = pIspRegs->Idx.IHDR_GGM;
    if (index < NVRAM_IHDR_GGM_TBL_NUM) {
        m_rISPRegs.IHDR_GGM[index] = (pIspRegs->IHDR_GGM[index]);
        m_rISPRegsIdx.IHDR_GGM = static_cast<MUINT8>(index);
        //
        // Need to check how to apply the IHDR_GGM setting to the hardware. There is no ISP_MGR_IHDR_GGM_T existed.
        //ISP_MGR_IHDR_GGM_T::getInstance((ESensorDev_T)m_eSensorEnum).put(&(pIspRegs->IHDR_GGM[index]));     //????????
    }
#endif

    index = pIspRegs->Idx.ANR;
    if (index < NVRAM_ANR_TBL_NUM) {
        m_rISPRegs.ANR[index] = (pIspRegs->ANR[index]);
        m_rISPRegsIdx.ANR = static_cast<MUINT8>(index);
        ISP_MGR_NBC_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->ANR[index]));
    }

    index = pIspRegs->Idx.ANR2;
    if (index < NVRAM_ANR2_TBL_NUM) {
        m_rISPRegs.ANR2[index] = (pIspRegs->ANR2[index]);
        m_rISPRegsIdx.ANR2 = static_cast<MUINT8>(index);
        ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->ANR2[index]));
    }

    index = pIspRegs->Idx.CCR;
    if (index < NVRAM_CCR_TBL_NUM) {
        m_rISPRegs.CCR[index] = (pIspRegs->CCR[index]);
        m_rISPRegsIdx.CCR = static_cast<MUINT8>(index);
        ISP_MGR_NBC2_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->CCR[index]));
    }

    index = pIspRegs->Idx.HFG;
    if (index < NVRAM_HFG_TBL_NUM) {
        m_rISPRegs.HFG[index] = (pIspRegs->HFG[index]);
        m_rISPRegsIdx.HFG = static_cast<MUINT8>(index);
        ISP_MGR_HFG_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->HFG[index]));
    }

    index = pIspRegs->Idx.EE;
    if (index < NVRAM_EE_TBL_NUM) {
        m_rISPRegs.EE[index] = (pIspRegs->EE[index]);
        m_rISPRegsIdx.EE = static_cast<MUINT8>(index);
        ISP_MGR_SEEE_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->EE[index]));
    }

    index = pIspRegs->Idx.MFB;
    if (index < NVRAM_MFB_TBL_NUM) {
        m_rISPRegs.MFB[index] = (pIspRegs->MFB[index]);
        m_rISPRegsIdx.MFB = static_cast<MUINT8>(index);
        ISP_MGR_MFB_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->MFB[index]));
    }

    index = pIspRegs->Idx.MIXER3;
    if (index < NVRAM_MIXER3_TBL_NUM) {
        m_rISPRegs.MIXER3[index] = (pIspRegs->MIXER3[index]);
        m_rISPRegsIdx.MIXER3 = static_cast<MUINT8>(index);
        ISP_MGR_MIXER3_T::getInstance((ESensorDev_T)m_eSensorEnum).put((pIspRegs->MIXER3[index]));
    }

    NSIspTuningv3::IspTuningMgr::getInstance().forceValidate((MINT32)m_eSensorEnum);

    MY_LOG("[%s] done ", __FUNCTION__);
    return  CCTIF_NO_ERROR;
}

