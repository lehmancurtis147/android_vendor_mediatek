#include "dpe_default_frame_0.h"
unsigned int dpe_default_frame_0_golden_dpe_confo_l_size = dpe_default_frame_0_dpe_confo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_confo_l_frame = &dpe_default_frame_0_dpe_confo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_confo_r_size = dpe_default_frame_0_dpe_confo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_confo_r_frame = &dpe_default_frame_0_dpe_confo_r_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_dvo_l_size = dpe_default_frame_0_dpe_dvo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_dvo_l_frame = &dpe_default_frame_0_dpe_dvo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_dvo_r_size = dpe_default_frame_0_dpe_dvo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_dvo_r_frame = &dpe_default_frame_0_dpe_dvo_r_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_respo_l_size = dpe_default_frame_0_dpe_respo_l_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_respo_l_frame = &dpe_default_frame_0_dpe_respo_l_frame_00_01[0];

unsigned int dpe_default_frame_0_golden_dpe_respo_r_size = dpe_default_frame_0_dpe_respo_r_frame_00_01_size;
char* dpe_default_frame_0_golden_dpe_respo_r_frame = &dpe_default_frame_0_dpe_respo_r_frame_00_01[0];

char* dpe_default_frame_0_in_dpe_wmf_dpi_frame_0 = &dpe_default_frame_0_dpe_wmf_dpi_frame_00_00_0[0];

char* dpe_default_frame_0_in_dpe_wmf_dpi_frame_1 = &dpe_default_frame_0_dpe_wmf_dpi_frame_00_00_1[0];

char* dpe_default_frame_0_in_dpe_wmf_dpi_frame_2 = &dpe_default_frame_0_dpe_wmf_dpi_frame_00_00_2[0];

char* dpe_default_frame_0_in_dpe_wmf_dpi_frame_3 = &dpe_default_frame_0_dpe_wmf_dpi_frame_00_00_3[0];

char* dpe_default_frame_0_in_dpe_wmf_dpi_frame_4 = &dpe_default_frame_0_dpe_wmf_dpi_frame_00_00_4[0];

unsigned int dpe_default_frame_0_golden_dpe_wmf_dpo_0_size = dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_0_size;
char* dpe_default_frame_0_golden_dpe_wmf_dpo_frame_0 = &dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_0[0];

unsigned int dpe_default_frame_0_golden_dpe_wmf_dpo_1_size = dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_1_size;
char* dpe_default_frame_0_golden_dpe_wmf_dpo_frame_1 = &dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_1[0];

unsigned int dpe_default_frame_0_golden_dpe_wmf_dpo_2_size = dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_2_size;
char* dpe_default_frame_0_golden_dpe_wmf_dpo_frame_2 = &dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_2[0];

unsigned int dpe_default_frame_0_golden_dpe_wmf_dpo_3_size = dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_3_size;
char* dpe_default_frame_0_golden_dpe_wmf_dpo_frame_3 = &dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_3[0];

unsigned int dpe_default_frame_0_golden_dpe_wmf_dpo_4_size = dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_4_size;
char* dpe_default_frame_0_golden_dpe_wmf_dpo_frame_4 = &dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_4[0];

char* dpe_default_frame_0_in_dpe_wmf_imgi_frame_0 = &dpe_default_frame_0_dpe_wmf_imgi_frame_00_00_0[0];

char* dpe_default_frame_0_in_dpe_wmf_imgi_frame_1 = &dpe_default_frame_0_dpe_wmf_imgi_frame_00_00_1[0];

char* dpe_default_frame_0_in_dpe_wmf_imgi_frame_2 = &dpe_default_frame_0_dpe_wmf_imgi_frame_00_00_2[0];

char* dpe_default_frame_0_in_dpe_wmf_imgi_frame_3 = &dpe_default_frame_0_dpe_wmf_imgi_frame_00_00_3[0];

char* dpe_default_frame_0_in_dpe_wmf_imgi_frame_4 = &dpe_default_frame_0_dpe_wmf_imgi_frame_00_00_4[0];

char* dpe_default_frame_0_in_dpe_wmf_maski_frame_0 = &dpe_default_frame_0_dpe_wmf_maski_frame_00_00_0[0];

char* dpe_default_frame_0_in_dpe_wmf_maski_frame_1 = &dpe_default_frame_0_dpe_wmf_maski_frame_00_00_1[0];

char* dpe_default_frame_0_in_dpe_wmf_tbli_frame_0 = &dpe_default_frame_0_dpe_wmf_tbli_frame_00_00_0[0];

char* dpe_default_frame_0_in_dpe_wmf_tbli_frame_1 = &dpe_default_frame_0_dpe_wmf_tbli_frame_00_00_1[0];

char* dpe_default_frame_0_in_dpe_wmf_tbli_frame_2 = &dpe_default_frame_0_dpe_wmf_tbli_frame_00_00_2[0];

char* dpe_default_frame_0_in_dpe_wmf_tbli_frame_3 = &dpe_default_frame_0_dpe_wmf_tbli_frame_00_00_3[0];

char* dpe_default_frame_0_in_dpe_wmf_tbli_frame_4 = &dpe_default_frame_0_dpe_wmf_tbli_frame_00_00_4[0];


void dpe_default_getframe_0GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2,
	unsigned long* golden_dpe_wmf_dpo_frame_3,
	unsigned long* golden_dpe_wmf_dpo_frame_4
)
{
*golden_dpe_confo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_confo_l_frame_00_01[0];
*golden_dpe_confo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_confo_r_frame_00_01[0];
*golden_dpe_dvo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_dvo_l_frame_00_01[0];
*golden_dpe_dvo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_dvo_r_frame_00_01[0];
*golden_dpe_respo_l_frame = (unsigned long)&dpe_default_frame_0_dpe_respo_l_frame_00_01[0];
*golden_dpe_respo_r_frame = (unsigned long)&dpe_default_frame_0_dpe_respo_r_frame_00_01[0];
*golden_dpe_wmf_dpo_frame_0 = (unsigned long)&dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_0[0];
*golden_dpe_wmf_dpo_frame_1 = (unsigned long)&dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_1[0];
*golden_dpe_wmf_dpo_frame_2 = (unsigned long)&dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_2[0];
*golden_dpe_wmf_dpo_frame_3 = (unsigned long)&dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_3[0];
*golden_dpe_wmf_dpo_frame_4 = (unsigned long)&dpe_default_frame_0_dpe_wmf_dpo_frame_00_00_4[0];
}
