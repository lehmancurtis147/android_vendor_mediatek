/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.
////////////////////////////////////////////////////////////////////////////////
// AcdkCLITest.cpp  $Revision$
////////////////////////////////////////////////////////////////////////////////

//! \file  AcdkCLITest.cpp
//! \brief



#include <vector>
#include <list>
#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>


//#include "isp_drv_cam.h"
#include "isp_drv_dip.h"
#include "isp_drv_dip_phy.h"
//#include "uni_drv.h"

#undef LOG_TAG
#define LOG_TAG "Test_DipDrv"

int IspDrvDipFunc(MUINT32 _case){
    int ret = 0;
    printf("enter IspDrvDipFunc _case(%d)\n",_case);
    switch(_case){  // 0 for phy, 1 for vir
        case 0: // Test isp_drv
            {
                PhyDipDrv* pDrvDip = (PhyDipDrv*)PhyDipDrvImp::createInstance(DIP_HW_A);
                MUINT32 reg;
                //
                pDrvDip->init("isp_drv_dip test");
                reg = 0x04;

                pDrvDip->writeReg(reg,0x54121);

                printf("DIP_HW_A:0x22004+0x%x:0x%x\n",reg,pDrvDip->readReg(reg));

                reg = 0x1004;
                pDrvDip->writeReg(reg,0x12345678);
                printf("DIP_HW_A:0x22004+0x%x:0x%x\n",reg,pDrvDip->readReg(reg));

                reg = 0x4004;
                pDrvDip->writeReg(reg,0x1230456);
                printf("DIP_HW_A:0x22004+0x%x:0x%x\n",reg,pDrvDip->readReg(reg));

                printf("Press any key to continue\n");
                //char s = getchar();

                pDrvDip->uninit("isp_drv_dip test");
                pDrvDip->destroyInstance();
            }
            break;
        case 1: // Test isp_drv_dip_phy
            {
                PhyDipDrv* pDrvDipPhy = (PhyDipDrv*)PhyDipDrv::createInstance(DIP_HW_A);
                MUINT32 regW, regR, cnt;
                //
                pDrvDipPhy->init("isp_drv_dip_phy test");
                cnt = 0;
                //
                cnt++;
                regW = 0x45122;
                DIP_WRITE_PHY_REG(pDrvDipPhy, DIP_X_CTL_YUV_EN, regW);
                regR = DIP_READ_PHY_REG(pDrvDipPhy,DIP_X_CTL_YUV_EN);
                printf("[Test(%d):DIP_WRITE_PHY_REG/DIP_READ_PHY_REG]regW(0x%x),regR(0x%x)\n",cnt,regW,regR);
                if(regW != regR) {
                    printf("[Error] value of regW and regR are not the same.\n");
                }
                //
                cnt++;
                regW = 0x01;
                DIP_WRITE_PHY_BITS(pDrvDipPhy, DIP_X_CTL_YUV_EN, MFB_EN, regW);
                regR = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_CTL_YUV_EN, MFB_EN);
                printf("[Test(%d):DIP_WRITE_PHY_BITS/DIP_READ_PHY_BITS]regW(0x%x),regR(0x%x)\n",cnt,regW,regR);
                if(regW != regR) {
                    printf("[Error] value of regW and regR are not the same.\n");
                }
                //
                cnt++;
                regW = 0x00;
                DIP_WRITE_PHY_BITS(pDrvDipPhy, DIP_X_CTL_YUV_EN, MFB_EN, regW);
                regR = DIP_READ_PHY_BITS(pDrvDipPhy, DIP_X_CTL_YUV_EN, MFB_EN);
                printf("[Test(%d):DIP_WRITE_PHY_BITS/DIP_READ_PHY_BITS]regW(0x%x),regR(0x%x)\n",cnt,regW,regR);
                if(regW != regR) {
                    printf("[Error] value of regW and regR are not the same.\n");
                }

                printf("####################################################\n");
                //
                printf("Press any key to continue\n");
                //char s = getchar();
                //
                pDrvDipPhy->uninit("isp_drv_dip test");
                pDrvDipPhy->destroyInstance();
            }
            break;
        case 2: // Test isp_drv_dip
            {
                static PhyDipDrv* m_pDrvDipPhy;
                static VirDipDrv* m_ispDrvDipTh0;
                static VirDipDrv* m_ispDrvDipTh1;
                static VirDipDrv* m_ispDrvDipTh2;
                //
                DIP_HW_MODULE hwModule = DIP_HW_A;
                char userNameTh0[30]="user_isp_drv_dip_th0";
                char userNameTh1[30]="user_isp_drv_dip_th1";
                char userNameTh2[30]="user_isp_drv_dip_th2";
                MUINT32 addrOfst,regNum;
                char s;
                //
                //m_ispDrvDipTh0 = IspDrvDip::createInstance(hwModule, dipTh0, burstQueIdx, dupCqIdx, userNameTh0);
                //m_ispDrvDipTh1 = IspDrvDip::createInstance(hwModule, dipTh1, burstQueIdx, dupCqIdx, userNameTh1);
                //m_ispDrvDipTh2 = IspDrvDip::createInstance(hwModule, dipTh2, burstQueIdx, dupCqIdx, userNameTh2);
                m_pDrvDipPhy  = (PhyDipDrv*)DipDrv::createInstance(hwModule);
                m_ispDrvDipTh0 = (VirDipDrv*)DipDrv::getVirDipDrvInstance(hwModule, m_pDrvDipPhy->m_pIspRegMap);
                m_ispDrvDipTh1 = (VirDipDrv*)DipDrv::getVirDipDrvInstance(hwModule, m_pDrvDipPhy->m_pIspRegMap);
                m_ispDrvDipTh2 = (VirDipDrv*)DipDrv::getVirDipDrvInstance(hwModule, m_pDrvDipPhy->m_pIspRegMap);
                m_ispDrvDipTh0->init(userNameTh0);
                m_ispDrvDipTh1->init(userNameTh1);
                m_ispDrvDipTh2->init(userNameTh2);
                //
                m_ispDrvDipTh0->getCQModuleInfo(DIP_A_CTL,addrOfst,regNum);
                printf("[Th0]DIP_A_CTL - addrOfst(0x%x),regNum(0x%x)\n",addrOfst,regNum);
                m_ispDrvDipTh0->cqAddModule(DIP_A_CTL);
                //
                m_ispDrvDipTh0->getCQModuleInfo(DIP_A_CTL_DONE,addrOfst,regNum);
                printf("[Th0]DIP_A_CTL_DONE - addrOfst(0x%x),regNum(0x%x)\n",addrOfst,regNum);
                m_ispDrvDipTh0->cqAddModule(DIP_A_CTL_DONE);
                //
                m_ispDrvDipTh0->getCQModuleInfo(DIP_A_IMG3BO_CRSP,addrOfst,regNum);
                printf("[Th0]DIP_A_IMG3BO_CRSP - addrOfst(0x%x),regNum(0x%x)\n",addrOfst,regNum);
                m_ispDrvDipTh0->cqAddModule(DIP_A_IMG3BO_CRSP);
                //
                m_ispDrvDipTh0->getCQModuleInfo(DIP_A_IMGCI,addrOfst,regNum);
                printf("[Th0]DIP_A_IMGCI - addrOfst(0x%x),regNum(0x%x)\n",addrOfst,regNum);
                m_ispDrvDipTh0->cqAddModule(DIP_A_IMGCI);
                //
                //
                #if 1 //kk test
                m_ispDrvDipTh1->getCQModuleInfo(DIP_A_CTL_TDR,addrOfst,regNum);
                printf("[Th1]DIP_A_CTL_TDR - addrOfst(0x%x),regNum(0x%x)",addrOfst,regNum);
                m_ispDrvDipTh1->cqAddModule(DIP_A_CTL_TDR);
                //
                m_ispDrvDipTh1->getCQModuleInfo(DIP_A_TDRI,addrOfst,regNum);
                printf("[Th1]DIP_A_CTL_DONE - addrOfst(0x%x),regNum(0x%x)",addrOfst,regNum);
                m_ispDrvDipTh1->cqAddModule(DIP_A_TDRI);
                //
                m_ispDrvDipTh1->getCQModuleInfo(DIP_A_IMG3BO_CRSP,addrOfst,regNum);
                printf("[Th1]DIP_A_IMG3BO_CRSP - addrOfst(0x%x),regNum(0x%x)",addrOfst,regNum);
                m_ispDrvDipTh1->cqAddModule(DIP_A_IMG3BO_CRSP);
                //
                m_ispDrvDipTh1->getCQModuleInfo(DIP_A_IMGCI,addrOfst,regNum);
                printf("[Th1]DIP_A_IMGCI - addrOfst(0x%x),regNum(0x%x)",addrOfst,regNum);
                m_ispDrvDipTh1->cqAddModule(DIP_A_IMGCI);
                #endif
                //
                printf("####################################################\n");
                //
                printf("Press any key to continue\n");
                s = getchar();
                //
                m_ispDrvDipTh0->uninit(userNameTh0);
                m_ispDrvDipTh1->uninit(userNameTh1);
                m_ispDrvDipTh2->uninit(userNameTh2);
                //
                m_ispDrvDipTh0->destroyInstance();
                m_ispDrvDipTh1->destroyInstance();
                m_ispDrvDipTh2->destroyInstance();
            }
            break;
        case 3:
            //ret = CAM_SIGNAL_CTRL();
            break;
        default:
            printf("RW path err(0x%x)",_case);
            ret = -1;
            break;
    }

    return ret;

}

/*thread_call_back*/
#define MACRO_RW (1)

extern int IspDrvDip_LDVT(void);
extern int IspDrvFDVT_LDVT(void);

int Test_IspDrv(int argc, char** argv)
{
    MUINT32 HwModoule;
    MUINT32 RW_Path = 0;
	
    (void)argc;(void)argv;
    printf("##############################\n");
    printf("case 1: DIP\n");
	printf("case 4: DIP's LDVT\n");
    printf("case 5: FDVT's LDVT\n");
    printf("select test hw module\n");
    printf("##############################\n");

    char s = getchar();
    HwModoule = (MUINT32)atoi((const char*)&s);

    getchar();

    switch(HwModoule){
        case 0:
        case 1:
        case 2:
            printf("##############################\n");
            printf("case 0: R/W to physical\n");
            printf("case 1: R/W to virtual\n");
            printf("case 3: (Jessy UT test) R/W to physical, directly from isp_drv.cpp\n");
            printf("case 4: WR to virtual and WR to physical via CQ0\n");
            printf("case 5: Concurrent CAM_A and CAM_B register R/W access\n");

            if(HwModoule != 1){
                printf("case 2: wait signal (vsync only so far)\n");
            }
            printf("select path\n");
            printf("##############################\n");
            s = getchar();
            RW_Path = atoi((const char*)&s);

            printf("test case :HW module:0x%x, RW path:0x%x\n",HwModoule,RW_Path);
            break;
        default:
            break;
    }

    switch(HwModoule){
        case 1:
            RW_Path = 2; //kk test
            return IspDrvDipFunc(RW_Path);
            break;
        case 4:
            return IspDrvDip_LDVT();
            break;
        case 5:
            return IspDrvFDVT_LDVT();
            break;
    }
    return 0;
}
