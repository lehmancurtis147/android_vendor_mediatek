/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.


#define LOG_TAG "iopipetest"

#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>

#include <ispio_pipe_ports.h>
#include <imem_drv.h>
#include <isp_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
//
//thread
#include <utils/threads.h>
#include <mtkcam/def/PriorityDefs.h>
//thread priority
#include <system/thread_defs.h>
#include <sys/resource.h>
#include <utils/ThreadDefs.h>
#include <pthread.h>
#include <semaphore.h>

#include "isp_drv_dip_phy.h"
#include <mtkcam/drv/def/ICam_type.h>
//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSPostProc;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

	printf("writing %d bytes to file [%s]\n", size, fname);

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

/*******************************************************************************
*  Main Function
*
*  iopieTest 2 0 1 3
*  basic path(single thread): p2a, 1 in / 2 out without crop/resize
*                   -> testType=0: isp only+frame mode
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 1 1 1
*  multi-frame(single thread): two different frames for p2a path, 1 in / 2 out without crop/resize for each frames
*                   -> testType=0: isp only+frame mode
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 2 1 3
*  multi-thread test:
*              thread 1:  p2a(two frames), 1 in / 2 out without crop/resize for each frame
*              thread 2:  p2a(one frame), 1 in / 2 out without crop/resize
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 3 1 1
*  basic vss test:
*              capture:  p2a(one frame) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopieTest 2 4 1 6
*  multi-thread vss test:
*              thread 1_prv:  p2a(one frame) in loop, 1 in / 2 out without crop/resize for each frame
*              thread 2_cap:  p2a(one frame), 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 5 1 3
*  multi-process test:
*              process1, thread 1_prv:  p2a(single frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*              process2, thread 2_prv:  p2a(single frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 6 1 2
*  multi-process test:
*              process1, thread 1_prv:  p2a(multi frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*              process2, thread 2_prv:  p2a(multi frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 7 1 1
*  skip tpipe calculation test:  (//three same frames in a single enqueued package)
*               -> check comparison rule in a single package (frame with frame)
*               basic path(single thread, multi-frame): p2a, 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 8 1 2
*  skip tpipe calculation test 2:  (//three frames in a single enqueued package, the first and second frames are the same, and the third frame is different)
*               -> check comparison rule in a single package (frame with frame) and between different package (frame3)
*               basic path(single thread, multi-frame): p2a, 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 9 1 3
*  sw direct link test(isp-mdp-venc):  (//three frames in the same setting and direct link to codec)
*
*  iopipeTest 2 10 0 1
*  p2a with feo output, for eis usage, with srz1 disabled
*
*  iopipeTest 2 10 1 1
*  p2a with feo output, for eis usage, with srz1 enabled
*
*  iopipeTest 2 10 2 1
*  p2a with feo output, for vsdof usage, with srz1 disabled
*
*  iopipeTest 2 10 3 1
*  p2a with feo output, for vsdof usage, with srz1 enabled
*
*  iopipeTest 2 11 1 1
*  p2a yuv input, direct link mode
*
*  iopipeTest 2 13 1 1
*  p2a raw input, isp only + tpipe mode
*
*  iopipeTest 2 15 x 1
*  tpipe ctrl extension, isp only + tpipe mode
*
*  iopipeTest 2 16 x 1
*  tpipe ctrl extension, isp-mdp direct link + tpipe mode
*
*  iopipeTest 2 17 1 10
*  skip tpipe mechainsm verification, specially simulation for isp does not change setting but mdp does
*
*  iopipeTest 2 18 1 3
*  multi-vss handle
*
*  iopipeTest 2 19 1 1
*  p2a flow with fullG ufo in, isp-mdp direct link (wdmao+wroto+img2o+img3o)
*                   -> testType=0: Imgi with bayer10 full-g (Single mode)
*                   -> testType=1: Imgi with bayer12 full-g (Single mode)
*                   -> testType=2: Imgi with bayer10 full-g (Twin mode)
*                   -> testType=3: Imgi with bayer12 full-g (Twin mode)
*
*  iopieTest 2 20 1 3
*  basic path with fullHD(single thread): p2a with full-HD input, 1 in / 2 out without crop/resize
*                   -> testType=0: isp only+tpipe mode
*                   -> testType=1: direct link+tpipe mode (PORT_WROTO with 90 degree)
*
*  iopieTest 2 24 0 3
*  WUV path(single thread): wuv, 1 in / 2 out without crop/resize
*                   -> testType=0: isp only+frame mode
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 28 0 3
*  MDP_CROP2 path(single thread): MDP_CROP2, 1 in / 2 out with crop/resize
*                   -> testType=0: WDMA+WROT both use MDP_CROP
*                   -> testType=1: WDMA+WROT both use MDP_CROP2
*                   -> testType=2: WDMA use MDP_CROP, WROTO use MDP_CROP2
*                   -> testType=3: WDMA use MDP_CROP2, WROTO use MDP_CROP
*
*  iopieTest 2 29 0 3
*  mfb mixing path(single thread): MFB_MIXING, 3 in / 3 out with crop/resize
*                   -> testType=0: input IMGI+VIPI+DMGI; output IMG3O+WDMAO+WROTO
*
*  iopipeTest 2 31 1 1
*  p2a flow with bayer ufo in, isp-mdp direct link (wdmao+wroto+img2o+img3o)
*                   -> testType=0: Imgi with bayer10 NON-full-g
*                   -> testType=1: Imgi with bayer12 NON-full-g
*
*  iopipeTest 2 99 1 1
*  - single frame debug test
********************************************************************************/
int basicP2A( int type,int loopNum);
MVOID basicP2ACallback(QParams& rParams);
MBOOL g_basicP2ACallback = MFALSE;
int basicMultiFrame(int type,int loopNum);
MVOID basicMultiFrameCallback(QParams& rParams);
MBOOL g_basicMultiFrameCallback = MFALSE;
int multiThread(int type,int loopNum);
MVOID multiThreadCallback(QParams& rParams);
MBOOL g_multiThreadCallback = MFALSE;
MVOID onThreadCallback(QParams& rParams);
MBOOL g_onThreadCallback = MFALSE;
int basicVss(int type,int loopNum);
MVOID basicVssCallback(QParams& rParams);
MBOOL g_basicVssCallback = MFALSE;
int multiThreadVss(int type,int loopNum);
MVOID multiThreadVssCallback(QParams& rParams);
MBOOL g_multiThreadVssCallback = MFALSE;
MVOID vssCaptureThreadCallback(QParams& rParams);
MBOOL g_vssCaptureThreadCallback = MFALSE;
int multiProcess(int testNum, int type,int loopNum);
int testSkipTpipe( int type,int loopNum);
int testSkipTpipe2( int type,int loopNum);
int testSkipTpipe3( int type,int loopNum);
int testIspMdpVencDL( int type,int loopNum);
int basicP2AwithFEO( int type,int loopNum);
MVOID basicP2AwithFEOCallback(QParams& rParams);
MBOOL g_basicP2AwithFEOCallback = MFALSE;
int basicP2AwithYUVinput( int type,int loopNum);
int basicP2A_isponly_withTpipe(int type,int loopNum);
int testpath( int type,int loopNum);
int testMFB_Blending(int type,int loopNum);
int testRawInRawOut(int type,int loopNum);
int testRGBInYuvOut(int type,int loopNum);
int testRawInUnPakRawOut(int type,int loopNum);
int testTPIPECtrlExtension_isponly(int type,int loopNum);
int testTPIPECtrlExtension_directlink(int type,int loopNum);
int multiVss_test(int type,int loopNum);
int basicP2AwithFullG_UFO_in(int type,int loopNum);
MVOID basicP2AwithFullG_UFO_inCallback(QParams& rParams);
MBOOL g_basicP2AwithFullG_UFO_inCallback = MFALSE;
int basicP2AwithFullHD( int type,int loopNum);
int FEO_Y16_Dump(int type,int loopNum);
int basicWUV( int type,int loopNum);
MVOID basicWUVCallback(QParams& rParams);
MBOOL g_basicWUVCallback = MFALSE;
int P2B_BOKEH_YUV(int type,int loopNum);
int P2B_BOKEH_YUV_2(int type,int loopNum);
int P2B_BOKEH_YUV_3(int type,int loopNum);
MVOID p2bBokehYUVCallback(QParams& rParams);
MBOOL g_p2bBokehYUVCallback = MFALSE;
int P2A_FM(int type,int loopNum);
MVOID P2A_FMCallback(QParams& rParams);
MBOOL g_P2A_FMCallback = MFALSE;
int basicMDPCROPx( int type,int loopNum);
MVOID basicMDPCROPxCallback(QParams& rParams);
MBOOL g_basicMDPCROPxCallback = MFALSE;
int testMFB_Mixing( int type,int loopNum);
int testMFB_Mixing_Small(int testType,int loopNum);
MVOID testMFB_MixingCallback(QParams& rParams);
MBOOL g_testMFB_MixingCallback = MFALSE;
int basicP2AwithBayer_UFO_in(int type,int loopNum);
MVOID basicP2AwithBayer_UFO_inCallback(QParams& rParams);
MBOOL g_basicP2AwithBayer_UFO_inCallback = MFALSE;
int testLPCNR(int type,int loopNum);
MVOID testLPCNR_Callback(QParams& rParams);
MBOOL g_testLPCNR_Callback = MFALSE;

int test_iopipe(int argc, char** argv)
{
    int ret = 0;
    if(argc<4)
    {
        printf("wrong paramerter number(%d)\n",argc);
        return 0;
    }


    printf("##############################\n");
    printf("\n");
    printf("##############################\n");

    int testNum = atoi(argv[1]);
    int testType = atoi(argv[2]);
    int loopNum = atoi(argv[3]);


    switch(testNum)
    {
        case 0:
        	ret=basicP2A(testType,loopNum);
        	break;
        case 1:
        	ret=basicMultiFrame(testType,loopNum);
        	break;
        case 2:
            ret=multiThread(testType,loopNum);
        	break;
       case 3:
            ret=basicVss(testType,loopNum);
        	break;
       case 4:
            ret=multiThreadVss(testType,loopNum);
        	break;
       case 5:
       case 6:
            ret=multiProcess(testNum, testType,loopNum);
        	break;
       case 7:
            ret=testSkipTpipe(testType,loopNum);
        	break;
        case 8:
            ret=testSkipTpipe2(testType,loopNum);
        	break;
        case 9:
            ret=testIspMdpVencDL(testType,loopNum);
            break;
        case 10:
            ret=basicP2AwithFEO(testType, loopNum);
            break;
        case 11:
            ret=basicP2AwithYUVinput(testType, loopNum);
            break;
        case 12:
            ret=testMFB_Blending(testType,loopNum);
            break;
        case 13:
            ret=basicP2A_isponly_withTpipe(testType,loopNum);
            break;
        case 14:
            ret=testRawInRawOut(testType,loopNum);
            break;
        case 15:
            ret=testTPIPECtrlExtension_isponly(testType,loopNum);
            break;
        case 16:
            ret=testTPIPECtrlExtension_directlink(testType, loopNum);
            break;
        case 17:
            ret=testSkipTpipe3(testType,loopNum);
        	break;
        case 18:
            ret=multiVss_test(testType,loopNum);
        	break;
        case 19:
            ret=basicP2AwithFullG_UFO_in(testType,loopNum);
        	break;
        case 20:
        	ret=basicP2AwithFullHD(testType,loopNum);
        	break;
        case 21:
            ret=testRGBInYuvOut(testType,loopNum);
            break;
        case 22:
            ret=testRawInUnPakRawOut(testType,loopNum);
            break;
        case 23:
            ret=FEO_Y16_Dump(testType,loopNum);
            break;
        case 24:
            ret=basicWUV(testType,loopNum);
            break;
        case 25:
            ret=P2B_BOKEH_YUV(testType,loopNum);
            break;
        case 26:
            ret=P2B_BOKEH_YUV_2(testType,loopNum);
            break;
        case 27:
            ret=P2A_FM(testType,loopNum);
            break;
        case 28:
            ret=basicMDPCROPx(testType,loopNum);
            break;
        case 29:
            ret=testMFB_Mixing(testType,loopNum);
            break;
        case 30:
            ret=P2B_BOKEH_YUV_3(testType,loopNum);
            break;
        case 31:
            ret=basicP2AwithBayer_UFO_in(testType,loopNum);
            break;
        case 32:
            ret=testLPCNR(testType,loopNum);
            break;
        case 33:
            ret=testMFB_Mixing_Small(testType,loopNum);   // 2 33 0 1 (no tuning) , 2 55 1 1 (tuning)
            break;
        case 99:
            ret=testpath(testType,loopNum);
            break;
        default:
        	break;
    }

    ret = 1;
    return ret;
}


enum tuning_tag
{
    tuning_tag_G2G = 0,
    tuning_tag_G2C,
    tuning_tag_GGM,
    tuning_tag_GGM2,
    tuning_tag_UDM,
    tuning_tag_NBC2,
    tuning_tag_NBC2_BOKENEFFECT,
    tuning_tag_SL2E,
    tuning_tag_RMM2,
    tuning_tag_RMG2,
    tuning_tag_P2A,
    tuning_tag_P2B,
    tuning_tag_Mixing,
    tuning_tag_DFE
};


void setUNP_D1_UT(dip_x_reg_t* pIspReg){
	//setUnp
	pIspReg->UNP_D1A_UNP_OFST.Raw = 0x00;    
}

void setUFD_D1_UT(dip_x_reg_t* pIspReg, MUINT32 fgModeRegBit_ufd, UFDG_META_INFO* pUfdParam=NULL){
	//setUfd
	if (fgModeRegBit_ufd == 1)    
	{        
		//pIspReg->UFD_D1A_UFD_BS2_AU_CON.Bits.UFOD_SEL= 0x0; // ToDo Justin, No UFOD_SEL
	}   
	else    
	{        
		//pIspReg->UFD_D1A_UFD_BS2_AU_CON.Bits.UFOD_SEL= 0x1; // ToDo Justin, No UFOD_SEL
	}
	pIspReg->UFD_D1A_UFD_CON.Bits.UFD_TCCT_BYP = 0x1;
	if( pUfdParam != NULL)  // pUfdParam == 1 means that not NULL
	{
		pIspReg->UFD_D1A_UFD_BS2_AU_CON.Bits.UFD_BOND_MODE = pUfdParam->UFDG_BOND_MODE;
		pIspReg->UFD_D1A_UFD_AU2_CON.Bits.UFD_AU2_SIZE = pUfdParam->UFDG_AU2_SIZE;
		pIspReg->UFD_D1A_UFD_ADDRESS_CON1.Raw = pUfdParam->UFDG_BITSTREAM_OFST_ADDR;  // ToDO: Justin, UFD_D1A_UFD_ADDRESS_CON1 / UFD_D1A_UFD_ADDRESS_CON2 / UFD_D1A_UFD_ADDRESS_CON3
		pIspReg->UFD_D1A_UFD_BS2_AU_CON.Bits.UFD_BS2_AU_START = pUfdParam->UFDG_BS_AU_START; // Todo Justin, UFD_D1A_UFD_BS2_AU_CON / UFD_D1A_UFD_BS3_AU_CON / UFD_D1A_UFD_BS4_AU_CON
	}
	else
	{
		printf("pUfdParam is NULL!");
	}
}

void setUDM_D1_UT(dip_x_reg_t* pIspReg, MUINT32 fgModeRegBit_udm){

	//setUdm = setDM (ISP60)
	pIspReg->DM_D1A_DM_INTP_CRS.Raw =  0x0002F004;
	pIspReg->DM_D1A_DM_INTP_NAT.Raw =  0x1430053F;
	pIspReg->DM_D1A_DM_INTP_AUG.Raw =  0x00500500;
	pIspReg->DM_D1A_DM_LUMA_LUT1.Raw =	0x052A30DC;
	pIspReg->DM_D1A_DM_LUMA_LUT2.Raw =	0x02A9124F;
	pIspReg->DM_D1A_DM_SL_CTL.Raw =  0x0039B4A0;
	pIspReg->DM_D1A_DM_HFTD_CTL.Raw =	0x0A529400;
	pIspReg->DM_D1A_DM_NR_STR.Raw =  0x81028000;
	pIspReg->DM_D1A_DM_NR_ACT.Raw =   0x00000050;
	pIspReg->DM_D1A_DM_HF_STR.Raw =  0x84210000;
	pIspReg->DM_D1A_DM_HF_ACT1.Raw =   0x46FF1EFF;
	pIspReg->DM_D1A_DM_HF_ACT2.Raw =  0x001EFF55;
	pIspReg->DM_D1A_DM_CLIP.Raw =	0x00942064;
	pIspReg->DM_D1A_DM_DSB.Raw =   0x00000000|fgModeRegBit_udm;
	pIspReg->DM_D1A_DM_TILE_EDGE.Raw =	 0x0000000F;

	pIspReg->DM_D1A_DM_P1_ACT.Raw =   0x000000FF;
	pIspReg->DM_D1A_DM_LR_RAT.Raw =   0x00000418;

	pIspReg->DM_D1A_DM_HFTD_CTL2.Raw =	 0x00000019;
	pIspReg->DM_D1A_DM_EST_CTL.Raw =   0x00000035;
	pIspReg->DM_D1A_DM_SPARE_2.Raw =   0x00000000;
	pIspReg->DM_D1A_DM_SPARE_3.Raw =   0x00000000;

	pIspReg->DM_D1A_DM_INT_CTL.Raw =   0x00000035;
	pIspReg->DM_D1A_DM_EE.Raw =   0x00000410;


	pIspReg->DM_D1A_DM_LMT.Raw =   0x0;    //Todo   Justin
	pIspReg->DM_D1A_DM_RCCC.Raw =   0x0;   //Todo   Justin

	//enFgMode = imageioPackage.isp_top_ctl.FMT_SEL.Bits.FG_MODE
	printf("fdmode bit/reg(0x%x/0x%x)",fgModeRegBit_udm, 0x007FA800|fgModeRegBit_udm);
}

void setCCM_D1_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_RGB_EN2.Bits.DIPCTL_CCM_D1_EN = 1;//enable bit
	pIspReg->CCM_D1A_CCM_CNV_1.Raw = 0x00000200;
	pIspReg->CCM_D1A_CCM_CNV_2.Raw = 0x00000000;
	pIspReg->CCM_D1A_CCM_CNV_3.Raw = 0x02000000;
	pIspReg->CCM_D1A_CCM_CNV_4.Raw = 0x00000000;
	pIspReg->CCM_D1A_CCM_CNV_5.Raw = 0x00000000;
	pIspReg->CCM_D1A_CCM_CNV_6.Raw = 0x00000200;
	pIspReg->CCM_D1A_CCM_CTRL.Raw =  0x00000009;
	pIspReg->CCM_D1A_CCM_CFC_CTRL1.Raw = 0x0FDF0020;
	pIspReg->CCM_D1A_CCM_CFC_CTRL2.Raw = 0x1CE739CE;
}


void setCCM_D2_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_RGB_EN2.Bits.DIPCTL_CCM_D2_EN = 1;//enable bit
	pIspReg->CCM_D2A_CCM_CNV_1.Raw = 0x00000200;
	pIspReg->CCM_D2A_CCM_CNV_2.Raw = 0x00000000;
	pIspReg->CCM_D2A_CCM_CNV_3.Raw = 0x02000000;
	pIspReg->CCM_D2A_CCM_CNV_4.Raw = 0x00000000;
	pIspReg->CCM_D2A_CCM_CNV_5.Raw = 0x00000000;
	pIspReg->CCM_D2A_CCM_CNV_6.Raw = 0x00000200;
	pIspReg->CCM_D2A_CCM_CTRL.Raw =	0x00000009;
	pIspReg->CCM_D2A_CCM_CFC_CTRL1.Raw = 0x0FDF0020; 
	pIspReg->CCM_D2A_CCM_CFC_CTRL2.Raw = 0x1CE739CE;
}

void setCCM_D3_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_CCM_D3_EN = 1;//enable bit
	pIspReg->CCM_D3A_CCM_CNV_1.Raw = 0x00000200;
	pIspReg->CCM_D3A_CCM_CNV_2.Raw = 0x00000000;
	pIspReg->CCM_D3A_CCM_CNV_3.Raw = 0x02000000;
	pIspReg->CCM_D3A_CCM_CNV_4.Raw = 0x00000000;
	pIspReg->CCM_D3A_CCM_CNV_5.Raw = 0x00000000;
	pIspReg->CCM_D3A_CCM_CNV_6.Raw = 0x00000200;
	pIspReg->CCM_D3A_CCM_CTRL.Raw = 0x00000009;
	pIspReg->CCM_D3A_CCM_CFC_CTRL1.Raw = 0x0FDF0020; 
	pIspReg->CCM_D3A_CCM_CFC_CTRL2.Raw = 0x1CE739CE;
}


void setLCE_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->LCE_D1A_LCE_IMAGE_SIZE.Bits.LCE_IMAGE_WD = 0 /*pIspReg->DMAImgi.dma_cfg.size.w*/;  //ToDo: Justin   w / h size
	pIspReg->LCE_D1A_LCE_IMAGE_SIZE.Bits.LCE_IMAGE_HT = 0 /*pIspReg->DMAImgi.dma_cfg.size.h*/;  //ToDo: Justin   w / h size
}


//Todo Justin, below Register name must change to ISP60 naming

void setGGM_D1_UT(dip_x_reg_t* pIspReg){    //Todo comment is old
	pIspReg->DIPCTL_D1A_DIPCTL_RGB_EN2.Bits.DIPCTL_GGM_D1_EN = 1;//enable bit
	pIspReg->GGM_D1A_GGM_LUT[0].Raw = 0x00000000; /*0x00002180	DIP_A_GGM_LUT[0] */
	pIspReg->GGM_D1A_GGM_LUT[1].Raw = 0x00200802; /*0x00002184	DIP_A_GGM_LUT[1] */
	pIspReg->GGM_D1A_GGM_LUT[2].Raw = 0x00401004; /*0x00002188	DIP_A_GGM_LUT[2] */
	pIspReg->GGM_D1A_GGM_LUT[3].Raw = 0x00601806; /*0x0000218C	DIP_A_GGM_LUT[3] */
	pIspReg->GGM_D1A_GGM_LUT[4].Raw = 0x00802008; /*0x00002190	DIP_A_GGM_LUT[4] */ 
	pIspReg->GGM_D1A_GGM_LUT[5].Raw = 0x00a0280a; /*0x00002194	DIP_A_GGM_LUT[5] */ 
	pIspReg->GGM_D1A_GGM_LUT[6].Raw = 0x00c0300c; /*0x00002198	DIP_A_GGM_LUT[6] */ 
	pIspReg->GGM_D1A_GGM_LUT[7].Raw = 0x00e0380e; /*0x0000219C	DIP_A_GGM_LUT[7] */ 
	pIspReg->GGM_D1A_GGM_LUT[8].Raw = 0x01004010; /*0x000021A0	DIP_A_GGM_LUT[8] */  
	pIspReg->GGM_D1A_GGM_LUT[9].Raw = 0x01204812; /*0x000021A4	DIP_A_GGM_LUT[9] */  
	pIspReg->GGM_D1A_GGM_LUT[10].Raw = 0x01405014; /*0x000021A8  DIP_A_GGM_LUT[10] */  
	pIspReg->GGM_D1A_GGM_LUT[11].Raw = 0x01605816; /*0x000021AC  DIP_A_GGM_LUT[11] */  
	pIspReg->GGM_D1A_GGM_LUT[12].Raw = 0x01806018; /*0x000021B0  DIP_A_GGM_LUT[12] */	
	pIspReg->GGM_D1A_GGM_LUT[13].Raw = 0x01a0681a; /*0x000021B4  DIP_A_GGM_LUT[13] */	
	pIspReg->GGM_D1A_GGM_LUT[14].Raw = 0x01c0701c; /*0x000021B8  DIP_A_GGM_LUT[14] */	
	pIspReg->GGM_D1A_GGM_LUT[15].Raw = 0x01e0781e; /*0x000021BC  DIP_A_GGM_LUT[15] */	
	pIspReg->GGM_D1A_GGM_LUT[16].Raw = 0x02008020; /*0x000021C0  DIP_A_GGM_LUT[16] */  
	pIspReg->GGM_D1A_GGM_LUT[17].Raw = 0x02208822; /*0x000021C4  DIP_A_GGM_LUT[17] */  
	pIspReg->GGM_D1A_GGM_LUT[18].Raw = 0x02409024; /*0x000021C8  DIP_A_GGM_LUT[18] */  
	pIspReg->GGM_D1A_GGM_LUT[19].Raw = 0x02609826; /*0x000021CC  DIP_A_GGM_LUT[19] */  
	pIspReg->GGM_D1A_GGM_LUT[20].Raw = 0x0280a028; /*0x000021D0  DIP_A_GGM_LUT[20] */	
	pIspReg->GGM_D1A_GGM_LUT[21].Raw = 0x02a0a82a; /*0x000021D4  DIP_A_GGM_LUT[21] */	
	pIspReg->GGM_D1A_GGM_LUT[22].Raw = 0x02c0b02c; /*0x000021D8  DIP_A_GGM_LUT[22] */	
	pIspReg->GGM_D1A_GGM_LUT[23].Raw = 0x02e0b82e; /*0x000021DC  DIP_A_GGM_LUT[23] */	
	pIspReg->GGM_D1A_GGM_LUT[24].Raw = 0x0300c030; /*0x000021E0  DIP_A_GGM_LUT[24] */	 
	pIspReg->GGM_D1A_GGM_LUT[25].Raw = 0x0320c832; /*0x000021E4  DIP_A_GGM_LUT[25] */	 
	pIspReg->GGM_D1A_GGM_LUT[26].Raw = 0x0340d034; /*0x000021E8  DIP_A_GGM_LUT[26] */	 
	pIspReg->GGM_D1A_GGM_LUT[27].Raw = 0x0360d836; /*0x000021EC  DIP_A_GGM_LUT[27] */	 
	pIspReg->GGM_D1A_GGM_LUT[28].Raw = 0x0380e038; /*0x000021F0  DIP_A_GGM_LUT[28] */	  
	pIspReg->GGM_D1A_GGM_LUT[29].Raw = 0x03a0e83a; /*0x000021F4  DIP_A_GGM_LUT[29] */	  
	pIspReg->GGM_D1A_GGM_LUT[30].Raw = 0x03c0f03c; /*0x000021F8  DIP_A_GGM_LUT[30] */	  
	pIspReg->GGM_D1A_GGM_LUT[31].Raw = 0x03e0f83e; /*0x000021FC  DIP_A_GGM_LUT[31] */	  
	pIspReg->GGM_D1A_GGM_LUT[32].Raw = 0x04010040; /*0x00002200  DIP_A_GGM_LUT[32] */  
	pIspReg->GGM_D1A_GGM_LUT[33].Raw = 0x04210842; /*0x00002204  DIP_A_GGM_LUT[33] */  
	pIspReg->GGM_D1A_GGM_LUT[34].Raw = 0x04411044; /*0x00002208  DIP_A_GGM_LUT[34] */  
	pIspReg->GGM_D1A_GGM_LUT[35].Raw = 0x04611846; /*0x0000220C  DIP_A_GGM_LUT[35] */  
	pIspReg->GGM_D1A_GGM_LUT[36].Raw = 0x04812048; /*0x00002210  DIP_A_GGM_LUT[36] */  
	pIspReg->GGM_D1A_GGM_LUT[37].Raw = 0x04a1284a; /*0x00002214  DIP_A_GGM_LUT[37] */  
	pIspReg->GGM_D1A_GGM_LUT[38].Raw = 0x04c1304c; /*0x00002218  DIP_A_GGM_LUT[38] */  
	pIspReg->GGM_D1A_GGM_LUT[39].Raw = 0x04e1384e; /*0x0000221C  DIP_A_GGM_LUT[39] */  
	pIspReg->GGM_D1A_GGM_LUT[40].Raw = 0x05014050; /*0x00002220  DIP_A_GGM_LUT[40] */	
	pIspReg->GGM_D1A_GGM_LUT[41].Raw = 0x05214852; /*0x00002224  DIP_A_GGM_LUT[41] */	
	pIspReg->GGM_D1A_GGM_LUT[42].Raw = 0x05415054; /*0x00002228  DIP_A_GGM_LUT[42] */	
	pIspReg->GGM_D1A_GGM_LUT[43].Raw = 0x05615856; /*0x0000222C  DIP_A_GGM_LUT[43] */	
	pIspReg->GGM_D1A_GGM_LUT[44].Raw = 0x05816058; /*0x00002230  DIP_A_GGM_LUT[44] */	 
	pIspReg->GGM_D1A_GGM_LUT[45].Raw = 0x05a1685a; /*0x00002234  DIP_A_GGM_LUT[45] */	 
	pIspReg->GGM_D1A_GGM_LUT[46].Raw = 0x05c1705c; /*0x00002238  DIP_A_GGM_LUT[46] */	 
	pIspReg->GGM_D1A_GGM_LUT[47].Raw = 0x05e1785e; /*0x0000223C  DIP_A_GGM_LUT[47] */	 
	pIspReg->GGM_D1A_GGM_LUT[48].Raw = 0x06018060; /*0x00002240  DIP_A_GGM_LUT[48] */	  
	pIspReg->GGM_D1A_GGM_LUT[49].Raw = 0x06218862; /*0x00002244  DIP_A_GGM_LUT[49] */	  
	pIspReg->GGM_D1A_GGM_LUT[50].Raw = 0x06419064; /*0x00002248  DIP_A_GGM_LUT[50] */	  
	pIspReg->GGM_D1A_GGM_LUT[51].Raw = 0x06619866; /*0x0000224C  DIP_A_GGM_LUT[51] */	  
	pIspReg->GGM_D1A_GGM_LUT[52].Raw = 0x0681a068; /*0x00002250  DIP_A_GGM_LUT[52] */	   
	pIspReg->GGM_D1A_GGM_LUT[53].Raw = 0x06a1a86a; /*0x00002254  DIP_A_GGM_LUT[53] */	   
	pIspReg->GGM_D1A_GGM_LUT[54].Raw = 0x06c1b06c; /*0x00002258  DIP_A_GGM_LUT[54] */	   
	pIspReg->GGM_D1A_GGM_LUT[55].Raw = 0x06e1b86e; /*0x0000225C  DIP_A_GGM_LUT[55] */	   
	pIspReg->GGM_D1A_GGM_LUT[56].Raw = 0x0701c070; /*0x00002260  DIP_A_GGM_LUT[56] */  
	pIspReg->GGM_D1A_GGM_LUT[57].Raw = 0x0721c872; /*0x00002264  DIP_A_GGM_LUT[57] */  
	pIspReg->GGM_D1A_GGM_LUT[58].Raw = 0x0741d074; /*0x00002268  DIP_A_GGM_LUT[58] */  
	pIspReg->GGM_D1A_GGM_LUT[59].Raw = 0x0761d876; /*0x0000226C  DIP_A_GGM_LUT[59] */  
	pIspReg->GGM_D1A_GGM_LUT[60].Raw = 0x0781e078; /*0x00002270  DIP_A_GGM_LUT[60] */  
	pIspReg->GGM_D1A_GGM_LUT[61].Raw = 0x07a1e87a; /*0x00002274  DIP_A_GGM_LUT[61] */  
	pIspReg->GGM_D1A_GGM_LUT[62].Raw = 0x07c1f07c; /*0x00002278  DIP_A_GGM_LUT[62] */  
	pIspReg->GGM_D1A_GGM_LUT[63].Raw = 0x07e1f87e; /*0x0000227C  DIP_A_GGM_LUT[63] */  
	pIspReg->GGM_D1A_GGM_LUT[64].Raw = 0x08020080; /*0x00002280  DIP_A_GGM_LUT[64] */ 
	pIspReg->GGM_D1A_GGM_LUT[65].Raw = 0x08421084; /*0x00002284  DIP_A_GGM_LUT[65] */ 
	pIspReg->GGM_D1A_GGM_LUT[66].Raw = 0x08822088; /*0x00002288  DIP_A_GGM_LUT[66] */ 
	pIspReg->GGM_D1A_GGM_LUT[67].Raw = 0x08c2308c; /*0x0000228C  DIP_A_GGM_LUT[67] */ 
	pIspReg->GGM_D1A_GGM_LUT[68].Raw = 0x09024090; /*0x00002290  DIP_A_GGM_LUT[68] */ 
	pIspReg->GGM_D1A_GGM_LUT[69].Raw = 0x09425094; /*0x00002294  DIP_A_GGM_LUT[69] */ 
	pIspReg->GGM_D1A_GGM_LUT[70].Raw = 0x09826098; /*0x00002298  DIP_A_GGM_LUT[70] */ 
	pIspReg->GGM_D1A_GGM_LUT[71].Raw = 0x09c2709c; /*0x0000229C  DIP_A_GGM_LUT[71] */ 
	pIspReg->GGM_D1A_GGM_LUT[72].Raw = 0x0a0280a0; /*0x000022A0  DIP_A_GGM_LUT[72] */ 
	pIspReg->GGM_D1A_GGM_LUT[73].Raw = 0x0a4290a4; /*0x000022A4  DIP_A_GGM_LUT[73] */ 
	pIspReg->GGM_D1A_GGM_LUT[74].Raw = 0x0a82a0a8; /*0x000022A8  DIP_A_GGM_LUT[74] */ 
	pIspReg->GGM_D1A_GGM_LUT[75].Raw = 0x0ac2b0ac; /*0x000022AC  DIP_A_GGM_LUT[75] */ 
	pIspReg->GGM_D1A_GGM_LUT[76].Raw = 0x0b02c0b0; /*0x000022B0  DIP_A_GGM_LUT[76] */  
	pIspReg->GGM_D1A_GGM_LUT[77].Raw = 0x0b42d0b4; /*0x000022B4  DIP_A_GGM_LUT[77] */  
	pIspReg->GGM_D1A_GGM_LUT[78].Raw = 0x0b82e0b8; /*0x000022B8  DIP_A_GGM_LUT[78] */  
	pIspReg->GGM_D1A_GGM_LUT[79].Raw = 0x0bc2f0bc; /*0x000022BC  DIP_A_GGM_LUT[79] */  
	pIspReg->GGM_D1A_GGM_LUT[80].Raw = 0x0c0300c0; /*0x000022C0  DIP_A_GGM_LUT[80] */	
	pIspReg->GGM_D1A_GGM_LUT[81].Raw = 0x0c4310c4; /*0x000022C4  DIP_A_GGM_LUT[81] */	
	pIspReg->GGM_D1A_GGM_LUT[82].Raw = 0x0c8320c8; /*0x000022C8  DIP_A_GGM_LUT[82] */	
	pIspReg->GGM_D1A_GGM_LUT[83].Raw = 0x0cc330cc; /*0x000022CC  DIP_A_GGM_LUT[83] */	
	pIspReg->GGM_D1A_GGM_LUT[84].Raw = 0x0d0340d0; /*0x000022D0  DIP_A_GGM_LUT[84] */	
	pIspReg->GGM_D1A_GGM_LUT[85].Raw = 0x0d4350d4; /*0x000022D4  DIP_A_GGM_LUT[85] */	
	pIspReg->GGM_D1A_GGM_LUT[86].Raw = 0x0d8360d8; /*0x000022D8  DIP_A_GGM_LUT[86] */	
	pIspReg->GGM_D1A_GGM_LUT[87].Raw = 0x0dc370dc; /*0x000022DC  DIP_A_GGM_LUT[87] */	
	pIspReg->GGM_D1A_GGM_LUT[88].Raw = 0x0e0380e0; /*0x000022E0  DIP_A_GGM_LUT[88] */ 
	pIspReg->GGM_D1A_GGM_LUT[89].Raw = 0x0e4390e4; /*0x000022E4  DIP_A_GGM_LUT[89] */ 
	pIspReg->GGM_D1A_GGM_LUT[90].Raw = 0x0e83a0e8; /*0x000022E8  DIP_A_GGM_LUT[90] */ 
	pIspReg->GGM_D1A_GGM_LUT[91].Raw = 0x0ec3b0ec; /*0x000022EC  DIP_A_GGM_LUT[91] */ 
	pIspReg->GGM_D1A_GGM_LUT[92].Raw = 0x0f03c0f0; /*0x000022F0  DIP_A_GGM_LUT[92] */ 
	pIspReg->GGM_D1A_GGM_LUT[93].Raw = 0x0f43d0f4; /*0x000022F4  DIP_A_GGM_LUT[93] */ 
	pIspReg->GGM_D1A_GGM_LUT[94].Raw = 0x0f83e0f8; /*0x000022F8  DIP_A_GGM_LUT[94] */ 
	pIspReg->GGM_D1A_GGM_LUT[95].Raw = 0x0fc3f0fc; /*0x000022FC  DIP_A_GGM_LUT[95] */ 
	pIspReg->GGM_D1A_GGM_LUT[96].Raw = 0x10040100; /*0x00002300  DIP_A_GGM_LUT[96] */  
	pIspReg->GGM_D1A_GGM_LUT[97].Raw = 0x10842108; /*0x00002304  DIP_A_GGM_LUT[97] */  
	pIspReg->GGM_D1A_GGM_LUT[98].Raw = 0x11044110; /*0x00002308  DIP_A_GGM_LUT[98] */  
	pIspReg->GGM_D1A_GGM_LUT[99].Raw = 0x11846118; /*0x0000230C  DIP_A_GGM_LUT[99] */  
	pIspReg->GGM_D1A_GGM_LUT[100].Raw = 0x12048120; /*0x00002310  DIP_A_GGM_LUT[100] */ 
	pIspReg->GGM_D1A_GGM_LUT[101].Raw = 0x1284a128; /*0x00002314  DIP_A_GGM_LUT[101] */ 
	pIspReg->GGM_D1A_GGM_LUT[102].Raw = 0x1304c130; /*0x00002318  DIP_A_GGM_LUT[102] */ 
	pIspReg->GGM_D1A_GGM_LUT[103].Raw = 0x1384e138; /*0x0000231C  DIP_A_GGM_LUT[103] */ 
	pIspReg->GGM_D1A_GGM_LUT[104].Raw = 0x14050140; /*0x00002320  DIP_A_GGM_LUT[104] */ 
	pIspReg->GGM_D1A_GGM_LUT[105].Raw = 0x14852148; /*0x00002324  DIP_A_GGM_LUT[105] */ 
	pIspReg->GGM_D1A_GGM_LUT[106].Raw = 0x15054150; /*0x00002328  DIP_A_GGM_LUT[106] */ 
	pIspReg->GGM_D1A_GGM_LUT[107].Raw = 0x15856158; /*0x0000232C  DIP_A_GGM_LUT[107] */ 
	pIspReg->GGM_D1A_GGM_LUT[108].Raw = 0x16058160; /*0x00002330  DIP_A_GGM_LUT[108] */ 
	pIspReg->GGM_D1A_GGM_LUT[109].Raw = 0x1685a168; /*0x00002334  DIP_A_GGM_LUT[109] */ 
	pIspReg->GGM_D1A_GGM_LUT[110].Raw = 0x1705c170; /*0x00002338  DIP_A_GGM_LUT[110] */ 
	pIspReg->GGM_D1A_GGM_LUT[111].Raw = 0x1785e178; /*0x0000233C  DIP_A_GGM_LUT[111] */ 
	pIspReg->GGM_D1A_GGM_LUT[112].Raw = 0x18060180; /*0x00002340  DIP_A_GGM_LUT[112] */ 
	pIspReg->GGM_D1A_GGM_LUT[113].Raw = 0x18862188; /*0x00002344  DIP_A_GGM_LUT[113] */ 
	pIspReg->GGM_D1A_GGM_LUT[114].Raw = 0x19064190; /*0x00002348  DIP_A_GGM_LUT[114] */ 
	pIspReg->GGM_D1A_GGM_LUT[115].Raw = 0x19866198; /*0x0000234C  DIP_A_GGM_LUT[115] */ 
	pIspReg->GGM_D1A_GGM_LUT[116].Raw = 0x1a0681a0; /*0x00002350  DIP_A_GGM_LUT[116] */ 
	pIspReg->GGM_D1A_GGM_LUT[117].Raw = 0x1a86a1a8; /*0x00002354  DIP_A_GGM_LUT[117] */ 
	pIspReg->GGM_D1A_GGM_LUT[118].Raw = 0x1b06c1b0; /*0x00002358  DIP_A_GGM_LUT[118] */ 
	pIspReg->GGM_D1A_GGM_LUT[119].Raw = 0x1b86e1b8; /*0x0000235C  DIP_A_GGM_LUT[119] */ 
	pIspReg->GGM_D1A_GGM_LUT[120].Raw = 0x1c0701c0; /*0x00002360  DIP_A_GGM_LUT[120] */ 
	pIspReg->GGM_D1A_GGM_LUT[121].Raw = 0x1c8721c8; /*0x00002364  DIP_A_GGM_LUT[121] */ 
	pIspReg->GGM_D1A_GGM_LUT[122].Raw = 0x1d0741d0; /*0x00002368  DIP_A_GGM_LUT[122] */ 
	pIspReg->GGM_D1A_GGM_LUT[123].Raw = 0x1d8761d8; /*0x0000236C  DIP_A_GGM_LUT[123] */ 
	pIspReg->GGM_D1A_GGM_LUT[124].Raw = 0x1e0781e0; /*0x00002370  DIP_A_GGM_LUT[124] */  
	pIspReg->GGM_D1A_GGM_LUT[125].Raw = 0x1e87a1e8; /*0x00002374  DIP_A_GGM_LUT[125] */  
	pIspReg->GGM_D1A_GGM_LUT[126].Raw = 0x1f07c1f0; /*0x00002378  DIP_A_GGM_LUT[126] */  
	pIspReg->GGM_D1A_GGM_LUT[127].Raw = 0x1f87e1f8; /*0x0000237C  DIP_A_GGM_LUT[127] */  
	pIspReg->GGM_D1A_GGM_LUT[128].Raw = 0x20080200; /*0x00002380  DIP_A_GGM_LUT[128] */  
	pIspReg->GGM_D1A_GGM_LUT[129].Raw = 0x20882208; /*0x00002384  DIP_A_GGM_LUT[129] */  
	pIspReg->GGM_D1A_GGM_LUT[130].Raw = 0x21084210; /*0x00002388  DIP_A_GGM_LUT[130] */  
	pIspReg->GGM_D1A_GGM_LUT[131].Raw = 0x21886218; /*0x0000238C  DIP_A_GGM_LUT[131] */  
	pIspReg->GGM_D1A_GGM_LUT[132].Raw = 0x22088220; /*0x00002390  DIP_A_GGM_LUT[132] */ 
	pIspReg->GGM_D1A_GGM_LUT[133].Raw = 0x2288a228; /*0x00002394  DIP_A_GGM_LUT[133] */ 
	pIspReg->GGM_D1A_GGM_LUT[134].Raw = 0x2308c230; /*0x00002398  DIP_A_GGM_LUT[134] */ 
	pIspReg->GGM_D1A_GGM_LUT[135].Raw = 0x2388e238; /*0x0000239C  DIP_A_GGM_LUT[135] */ 
	pIspReg->GGM_D1A_GGM_LUT[136].Raw = 0x24090240; /*0x000023A0  DIP_A_GGM_LUT[136] */ 
	pIspReg->GGM_D1A_GGM_LUT[137].Raw = 0x24892248; /*0x000023A4  DIP_A_GGM_LUT[137] */ 
	pIspReg->GGM_D1A_GGM_LUT[138].Raw = 0x25094250; /*0x000023A8  DIP_A_GGM_LUT[138] */ 
	pIspReg->GGM_D1A_GGM_LUT[139].Raw = 0x25896258; /*0x000023AC  DIP_A_GGM_LUT[139] */ 
	pIspReg->GGM_D1A_GGM_LUT[140].Raw = 0x26098260; /*0x000023B0  DIP_A_GGM_LUT[140] */ 
	pIspReg->GGM_D1A_GGM_LUT[141].Raw = 0x2689a268; /*0x000023B4  DIP_A_GGM_LUT[141] */ 
	pIspReg->GGM_D1A_GGM_LUT[142].Raw = 0x2709c270; /*0x000023B8  DIP_A_GGM_LUT[142] */ 
	pIspReg->GGM_D1A_GGM_LUT[143].Raw = 0x2789e278; /*0x000023BC  DIP_A_GGM_LUT[143] */ 
	pIspReg->GGM_D1A_GGM_LUT[144].Raw = 0x280a0280; /*0x000023C0  DIP_A_GGM_LUT[144] */ 
	pIspReg->GGM_D1A_GGM_LUT[145].Raw = 0x288a2288; /*0x000023C4  DIP_A_GGM_LUT[145] */ 
	pIspReg->GGM_D1A_GGM_LUT[146].Raw = 0x290a4290; /*0x000023C8  DIP_A_GGM_LUT[146] */ 
	pIspReg->GGM_D1A_GGM_LUT[147].Raw = 0x298a6298; /*0x000023CC  DIP_A_GGM_LUT[147] */ 
	pIspReg->GGM_D1A_GGM_LUT[148].Raw = 0x2a0a82a0; /*0x000023D0  DIP_A_GGM_LUT[148] */ 
	pIspReg->GGM_D1A_GGM_LUT[149].Raw = 0x2a8aa2a8; /*0x000023D4  DIP_A_GGM_LUT[149] */ 
	pIspReg->GGM_D1A_GGM_LUT[150].Raw = 0x2b0ac2b0; /*0x000023D8  DIP_A_GGM_LUT[150] */ 
	pIspReg->GGM_D1A_GGM_LUT[151].Raw = 0x2b8ae2b8; /*0x000023DC  DIP_A_GGM_LUT[151] */ 
	pIspReg->GGM_D1A_GGM_LUT[152].Raw = 0x2c0b02c0; /*0x000023E0  DIP_A_GGM_LUT[152] */ 
	pIspReg->GGM_D1A_GGM_LUT[153].Raw = 0x2c8b22c8; /*0x000023E4  DIP_A_GGM_LUT[153] */ 
	pIspReg->GGM_D1A_GGM_LUT[154].Raw = 0x2d0b42d0; /*0x000023E8  DIP_A_GGM_LUT[154] */ 
	pIspReg->GGM_D1A_GGM_LUT[155].Raw = 0x2d8b62d8; /*0x000023EC  DIP_A_GGM_LUT[155] */ 
	pIspReg->GGM_D1A_GGM_LUT[156].Raw = 0x2e0b82e0; /*0x000023F0  DIP_A_GGM_LUT[156] */ 
	pIspReg->GGM_D1A_GGM_LUT[157].Raw = 0x2e8ba2e8; /*0x000023F4  DIP_A_GGM_LUT[157] */ 
	pIspReg->GGM_D1A_GGM_LUT[158].Raw = 0x2f0bc2f0; /*0x000023F8  DIP_A_GGM_LUT[158] */ 
	pIspReg->GGM_D1A_GGM_LUT[159].Raw = 0x2f8be2f8; /*0x000023FC  DIP_A_GGM_LUT[159] */ 
	pIspReg->GGM_D1A_GGM_LUT[160].Raw = 0x300c0300; /*0x00002400  DIP_A_GGM_LUT[160] */ 
	pIspReg->GGM_D1A_GGM_LUT[161].Raw = 0x308c2308; /*0x00002404  DIP_A_GGM_LUT[161] */ 
	pIspReg->GGM_D1A_GGM_LUT[162].Raw = 0x310c4310; /*0x00002408  DIP_A_GGM_LUT[162] */ 
	pIspReg->GGM_D1A_GGM_LUT[163].Raw = 0x318c6318; /*0x0000240C  DIP_A_GGM_LUT[163] */ 
	pIspReg->GGM_D1A_GGM_LUT[164].Raw = 0x320c8320; /*0x00002410  DIP_A_GGM_LUT[164] */ 
	pIspReg->GGM_D1A_GGM_LUT[165].Raw = 0x328ca328; /*0x00002414  DIP_A_GGM_LUT[165] */ 
	pIspReg->GGM_D1A_GGM_LUT[166].Raw = 0x330cc330; /*0x00002418  DIP_A_GGM_LUT[166] */ 
	pIspReg->GGM_D1A_GGM_LUT[167].Raw = 0x338ce338; /*0x0000241C  DIP_A_GGM_LUT[167] */ 
	pIspReg->GGM_D1A_GGM_LUT[168].Raw = 0x340d0340; /*0x00002420  DIP_A_GGM_LUT[168] */ 
	pIspReg->GGM_D1A_GGM_LUT[169].Raw = 0x348d2348; /*0x00002424  DIP_A_GGM_LUT[169] */ 
	pIspReg->GGM_D1A_GGM_LUT[170].Raw = 0x350d4350; /*0x00002428  DIP_A_GGM_LUT[170] */ 
	pIspReg->GGM_D1A_GGM_LUT[171].Raw = 0x358d6358; /*0x0000242C  DIP_A_GGM_LUT[171] */ 
	pIspReg->GGM_D1A_GGM_LUT[172].Raw = 0x360d8360; /*0x00002430  DIP_A_GGM_LUT[172] */ 
	pIspReg->GGM_D1A_GGM_LUT[173].Raw = 0x368da368; /*0x00002434  DIP_A_GGM_LUT[173] */ 
	pIspReg->GGM_D1A_GGM_LUT[174].Raw = 0x370dc370; /*0x00002438,	DIP_A_GGM_LUT[174] */ 
	pIspReg->GGM_D1A_GGM_LUT[175].Raw = 0x378de378; /*0x0000243C,	DIP_A_GGM_LUT[175] */ 
	pIspReg->GGM_D1A_GGM_LUT[176].Raw = 0x380e0380; /*0x00002440,	DIP_A_GGM_LUT[176] */ 
	pIspReg->GGM_D1A_GGM_LUT[177].Raw = 0x388e2388; /*0x00002444,	DIP_A_GGM_LUT[177] */ 
	pIspReg->GGM_D1A_GGM_LUT[178].Raw = 0x390e4390; /*0x00002448,	DIP_A_GGM_LUT[178] */ 
	pIspReg->GGM_D1A_GGM_LUT[179].Raw = 0x398e6398; /*0x0000244C,	DIP_A_GGM_LUT[179] */ 
	pIspReg->GGM_D1A_GGM_LUT[180].Raw = 0x3a0e83a0; /*0x00002450,	DIP_A_GGM_LUT[180] */ 
	pIspReg->GGM_D1A_GGM_LUT[181].Raw = 0x3a8ea3a8; /*0x00002454,	DIP_A_GGM_LUT[182] */ 
	pIspReg->GGM_D1A_GGM_LUT[182].Raw = 0x3b0ec3b0; /*0x00002458,	DIP_A_GGM_LUT[182] */ 
	pIspReg->GGM_D1A_GGM_LUT[183].Raw = 0x3b8ee3b8; /*0x0000245C,	DIP_A_GGM_LUT[183] */ 
	pIspReg->GGM_D1A_GGM_LUT[184].Raw = 0x3c0f03c0; /*0x00002460,	DIP_A_GGM_LUT[184] */ 
	pIspReg->GGM_D1A_GGM_LUT[185].Raw = 0x3c8f23c8; /*0x00002464,	DIP_A_GGM_LUT[185] */ 
	pIspReg->GGM_D1A_GGM_LUT[186].Raw = 0x3d0f43d0; /*0x00002468,	DIP_A_GGM_LUT[186] */ 
	pIspReg->GGM_D1A_GGM_LUT[187].Raw = 0x3d8f63d8; /*0x0000246C,	DIP_A_GGM_LUT[187] */ 
	pIspReg->GGM_D1A_GGM_LUT[188].Raw = 0x3e0f83e0; /*0x00002470,	DIP_A_GGM_LUT[188] */ 
	pIspReg->GGM_D1A_GGM_LUT[189].Raw = 0x3e0f83e0; /*0x00002474,	DIP_A_GGM_LUT[189] */ 
	pIspReg->GGM_D1A_GGM_LUT[190].Raw = 0x3e0f83e0; /*0x00002478,	DIP_A_GGM_LUT[190] */ 
	pIspReg->GGM_D1A_GGM_LUT[191].Raw = 0x3e0f83e0; /*0x0000247C,	DIP_A_GGM_LUT[191] */ 
	pIspReg->GGM_D1A_GGM_CTRL.Raw = 0x100307FF; /* 0x15024480: DIP_X_GGM_CTRL */
	pIspReg->GGM_D1A_GGM_SRAM_PINGPONG.Raw = 0;    // Todo Justin
}


void setGGM_D2_UT(dip_x_reg_t* pIspReg){
	pIspReg->DIPCTL_D1A_DIPCTL_RGB_EN2.Bits.DIPCTL_GGM_D2_EN = 1;//enable bit
	pIspReg->GGM_D2A_GGM_LUT[0].Raw = 0x00000000; /*0x15024640	DIP_A_GGM2_LUT[0] */
	pIspReg->GGM_D2A_GGM_LUT[1].Raw = 0x00200802; /*0x15024644	DIP_A_GGM2_LUT[1] */
	pIspReg->GGM_D2A_GGM_LUT[2].Raw = 0x00401004; /*0x15024648	DIP_A_GGM2_LUT[2] */
	pIspReg->GGM_D2A_GGM_LUT[3].Raw = 0x00601806; /*0x1502464C	DIP_A_GGM2_LUT[3] */
	pIspReg->GGM_D2A_GGM_LUT[4].Raw = 0x00802008; /*0x15024650	DIP_A_GGM2_LUT[4] */ 
	pIspReg->GGM_D2A_GGM_LUT[5].Raw = 0x00a0280a; /*0x15024654	DIP_A_GGM2_LUT[5] */ 
	pIspReg->GGM_D2A_GGM_LUT[6].Raw = 0x00c0300c; /*0x15024658	DIP_A_GGM2_LUT[6] */ 
	pIspReg->GGM_D2A_GGM_LUT[7].Raw = 0x00e0380e; /*0x1502465C	DIP_A_GGM2_LUT[7] */ 
	pIspReg->GGM_D2A_GGM_LUT[8].Raw = 0x01004010; /*0x15024660	DIP_A_GGM2_LUT[8] */  
	pIspReg->GGM_D2A_GGM_LUT[9].Raw = 0x01204812; /*0x15024664	DIP_A_GGM2_LUT[9] */  
	pIspReg->GGM_D2A_GGM_LUT[10].Raw = 0x01405014; /*0x15024668  DIP_A_GGM2_LUT[10] */	
	pIspReg->GGM_D2A_GGM_LUT[11].Raw = 0x01605816; /*0x1502466C  DIP_A_GGM2_LUT[11] */	
	pIspReg->GGM_D2A_GGM_LUT[12].Raw = 0x01806018; /*0x15024670  DIP_A_GGM2_LUT[12] */	 
	pIspReg->GGM_D2A_GGM_LUT[13].Raw = 0x01a0681a; /*0x15024674  DIP_A_GGM2_LUT[13] */	 
	pIspReg->GGM_D2A_GGM_LUT[14].Raw = 0x01c0701c; /*0x15024678  DIP_A_GGM2_LUT[14] */	 
	pIspReg->GGM_D2A_GGM_LUT[15].Raw = 0x01e0781e; /*0x1502467C  DIP_A_GGM2_LUT[15] */	 
	pIspReg->GGM_D2A_GGM_LUT[16].Raw = 0x02008020; /*0x15024680  DIP_A_GGM2_LUT[16] */	
	pIspReg->GGM_D2A_GGM_LUT[17].Raw = 0x02208822; /*0x15024684  DIP_A_GGM2_LUT[17] */	
	pIspReg->GGM_D2A_GGM_LUT[18].Raw = 0x02409024; /*0x15024688  DIP_A_GGM2_LUT[18] */	
	pIspReg->GGM_D2A_GGM_LUT[19].Raw = 0x02609826; /*0x1502468C  DIP_A_GGM2_LUT[19] */	
	pIspReg->GGM_D2A_GGM_LUT[20].Raw = 0x0280a028; /*0x15024690  DIP_A_GGM2_LUT[20] */	 
	pIspReg->GGM_D2A_GGM_LUT[21].Raw = 0x02a0a82a; /*0x15024694  DIP_A_GGM2_LUT[21] */	 
	pIspReg->GGM_D2A_GGM_LUT[22].Raw = 0x02c0b02c; /*0x15024698  DIP_A_GGM2_LUT[22] */	 
	pIspReg->GGM_D2A_GGM_LUT[23].Raw = 0x02e0b82e; /*0x1502469C  DIP_A_GGM2_LUT[23] */	 
	pIspReg->GGM_D2A_GGM_LUT[24].Raw = 0x0300c030; /*0x150246A0  DIP_A_GGM2_LUT[24] */	  
	pIspReg->GGM_D2A_GGM_LUT[25].Raw = 0x0320c832; /*0x150246A4  DIP_A_GGM2_LUT[25] */	  
	pIspReg->GGM_D2A_GGM_LUT[26].Raw = 0x0340d034; /*0x150246A8  DIP_A_GGM2_LUT[26] */	  
	pIspReg->GGM_D2A_GGM_LUT[27].Raw = 0x0360d836; /*0x150246AC  DIP_A_GGM2_LUT[27] */	  
	pIspReg->GGM_D2A_GGM_LUT[28].Raw = 0x0380e038; /*0x150246B0  DIP_A_GGM2_LUT[28] */	   
	pIspReg->GGM_D2A_GGM_LUT[29].Raw = 0x03a0e83a; /*0x150246B4  DIP_A_GGM2_LUT[29] */	   
	pIspReg->GGM_D2A_GGM_LUT[30].Raw = 0x03c0f03c; /*0x150246B8  DIP_A_GGM2_LUT[30] */	   
	pIspReg->GGM_D2A_GGM_LUT[31].Raw = 0x03e0f83e; /*0x150246BC  DIP_A_GGM2_LUT[31] */	   
	pIspReg->GGM_D2A_GGM_LUT[32].Raw = 0x04010040; /*0x150246C0  DIP_A_GGM2_LUT[32] */	
	pIspReg->GGM_D2A_GGM_LUT[33].Raw = 0x04210842; /*0x150246C4  DIP_A_GGM2_LUT[33] */	
	pIspReg->GGM_D2A_GGM_LUT[34].Raw = 0x04411044; /*0x150246C8  DIP_A_GGM2_LUT[34] */	
	pIspReg->GGM_D2A_GGM_LUT[35].Raw = 0x04611846; /*0x150246CC  DIP_A_GGM2_LUT[35] */	
	pIspReg->GGM_D2A_GGM_LUT[36].Raw = 0x04812048; /*0x150246D0  DIP_A_GGM2_LUT[36] */	
	pIspReg->GGM_D2A_GGM_LUT[37].Raw = 0x04a1284a; /*0x150246D4  DIP_A_GGM2_LUT[37] */	
	pIspReg->GGM_D2A_GGM_LUT[38].Raw = 0x04c1304c; /*0x150246D8  DIP_A_GGM2_LUT[38] */	
	pIspReg->GGM_D2A_GGM_LUT[39].Raw = 0x04e1384e; /*0x150246DC  DIP_A_GGM2_LUT[39] */	
	pIspReg->GGM_D2A_GGM_LUT[40].Raw = 0x05014050; /*0x150246E0  DIP_A_GGM2_LUT[40] */	 
	pIspReg->GGM_D2A_GGM_LUT[41].Raw = 0x05214852; /*0x150246E4  DIP_A_GGM2_LUT[41] */	 
	pIspReg->GGM_D2A_GGM_LUT[42].Raw = 0x05415054; /*0x150246E8  DIP_A_GGM2_LUT[42] */	 
	pIspReg->GGM_D2A_GGM_LUT[43].Raw = 0x05615856; /*0x150246EC  DIP_A_GGM2_LUT[43] */	 
	pIspReg->GGM_D2A_GGM_LUT[44].Raw = 0x05816058; /*0x150246F0  DIP_A_GGM2_LUT[44] */	  
	pIspReg->GGM_D2A_GGM_LUT[45].Raw = 0x05a1685a; /*0x150246F4  DIP_A_GGM2_LUT[45] */	  
	pIspReg->GGM_D2A_GGM_LUT[46].Raw = 0x05c1705c; /*0x150246F8  DIP_A_GGM2_LUT[46] */	  
	pIspReg->GGM_D2A_GGM_LUT[47].Raw = 0x05e1785e; /*0x150246FC  DIP_A_GGM2_LUT[47] */	  
	pIspReg->GGM_D2A_GGM_LUT[48].Raw = 0x06018060; /*0x15024700  DIP_A_GGM2_LUT[48] */	   
	pIspReg->GGM_D2A_GGM_LUT[49].Raw = 0x06218862; /*0x15024704  DIP_A_GGM2_LUT[49] */	   
	pIspReg->GGM_D2A_GGM_LUT[50].Raw = 0x06419064; /*0x15024708  DIP_A_GGM2_LUT[50] */	   
	pIspReg->GGM_D2A_GGM_LUT[51].Raw = 0x06619866; /*0x1502470C  DIP_A_GGM2_LUT[51] */	   
	pIspReg->GGM_D2A_GGM_LUT[52].Raw = 0x0681a068; /*0x15024710  DIP_A_GGM2_LUT[52] */		
	pIspReg->GGM_D2A_GGM_LUT[53].Raw = 0x06a1a86a; /*0x15024714  DIP_A_GGM2_LUT[53] */		
	pIspReg->GGM_D2A_GGM_LUT[54].Raw = 0x06c1b06c; /*0x15024718  DIP_A_GGM2_LUT[54] */		
	pIspReg->GGM_D2A_GGM_LUT[55].Raw = 0x06e1b86e; /*0x1502471C  DIP_A_GGM2_LUT[55] */		
	pIspReg->GGM_D2A_GGM_LUT[56].Raw = 0x0701c070; /*0x15024720  DIP_A_GGM2_LUT[56] */	
	pIspReg->GGM_D2A_GGM_LUT[57].Raw = 0x0721c872; /*0x15024724  DIP_A_GGM2_LUT[57] */	
	pIspReg->GGM_D2A_GGM_LUT[58].Raw = 0x0741d074; /*0x15024728  DIP_A_GGM2_LUT[58] */	
	pIspReg->GGM_D2A_GGM_LUT[59].Raw = 0x0761d876; /*0x1502472C  DIP_A_GGM2_LUT[59] */	
	pIspReg->GGM_D2A_GGM_LUT[60].Raw = 0x0781e078; /*0x15024730  DIP_A_GGM2_LUT[60] */	
	pIspReg->GGM_D2A_GGM_LUT[61].Raw = 0x07a1e87a; /*0x15024734  DIP_A_GGM2_LUT[61] */	
	pIspReg->GGM_D2A_GGM_LUT[62].Raw = 0x07c1f07c; /*0x15024738  DIP_A_GGM2_LUT[62] */	
	pIspReg->GGM_D2A_GGM_LUT[63].Raw = 0x07e1f87e; /*0x1502473C  DIP_A_GGM2_LUT[63] */	
	pIspReg->GGM_D2A_GGM_LUT[64].Raw = 0x08020080; /*0x15024740  DIP_A_GGM2_LUT[64] */ 
	pIspReg->GGM_D2A_GGM_LUT[65].Raw = 0x08421084; /*0x15024744  DIP_A_GGM2_LUT[65] */ 
	pIspReg->GGM_D2A_GGM_LUT[66].Raw = 0x08822088; /*0x15024748  DIP_A_GGM2_LUT[66] */ 
	pIspReg->GGM_D2A_GGM_LUT[67].Raw = 0x08c2308c; /*0x1502474C  DIP_A_GGM2_LUT[67] */ 
	pIspReg->GGM_D2A_GGM_LUT[68].Raw = 0x09024090; /*0x15024750  DIP_A_GGM2_LUT[68] */ 
	pIspReg->GGM_D2A_GGM_LUT[69].Raw = 0x09425094; /*0x15024754  DIP_A_GGM2_LUT[69] */ 
	pIspReg->GGM_D2A_GGM_LUT[70].Raw = 0x09826098; /*0x15024758  DIP_A_GGM2_LUT[70] */ 
	pIspReg->GGM_D2A_GGM_LUT[71].Raw = 0x09c2709c; /*0x1502475C  DIP_A_GGM2_LUT[71] */ 
	pIspReg->GGM_D2A_GGM_LUT[72].Raw = 0x0a0280a0; /*0x15024760  DIP_A_GGM2_LUT[72] */ 
	pIspReg->GGM_D2A_GGM_LUT[73].Raw = 0x0a4290a4; /*0x15024764  DIP_A_GGM2_LUT[73] */ 
	pIspReg->GGM_D2A_GGM_LUT[74].Raw = 0x0a82a0a8; /*0x15024768  DIP_A_GGM2_LUT[74] */ 
	pIspReg->GGM_D2A_GGM_LUT[75].Raw = 0x0ac2b0ac; /*0x1502476C  DIP_A_GGM2_LUT[75] */ 
	pIspReg->GGM_D2A_GGM_LUT[76].Raw = 0x0b02c0b0; /*0x15024770  DIP_A_GGM2_LUT[76] */	
	pIspReg->GGM_D2A_GGM_LUT[77].Raw = 0x0b42d0b4; /*0x15024774  DIP_A_GGM2_LUT[77] */	
	pIspReg->GGM_D2A_GGM_LUT[78].Raw = 0x0b82e0b8; /*0x15024778  DIP_A_GGM2_LUT[78] */	
	pIspReg->GGM_D2A_GGM_LUT[79].Raw = 0x0bc2f0bc; /*0x1502477C  DIP_A_GGM2_LUT[79] */	
	pIspReg->GGM_D2A_GGM_LUT[80].Raw = 0x0c0300c0; /*0x15024780  DIP_A_GGM2_LUT[80] */	 
	pIspReg->GGM_D2A_GGM_LUT[81].Raw = 0x0c4310c4; /*0x15024784  DIP_A_GGM2_LUT[81] */	 
	pIspReg->GGM_D2A_GGM_LUT[82].Raw = 0x0c8320c8; /*0x15024788  DIP_A_GGM2_LUT[82] */	 
	pIspReg->GGM_D2A_GGM_LUT[83].Raw = 0x0cc330cc; /*0x1502478C  DIP_A_GGM2_LUT[83] */	 
	pIspReg->GGM_D2A_GGM_LUT[84].Raw = 0x0d0340d0; /*0x15024790  DIP_A_GGM2_LUT[84] */	 
	pIspReg->GGM_D2A_GGM_LUT[85].Raw = 0x0d4350d4; /*0x15024794  DIP_A_GGM2_LUT[85] */	 
	pIspReg->GGM_D2A_GGM_LUT[86].Raw = 0x0d8360d8; /*0x15024798  DIP_A_GGM2_LUT[86] */	 
	pIspReg->GGM_D2A_GGM_LUT[87].Raw = 0x0dc370dc; /*0x1502479C  DIP_A_GGM2_LUT[87] */	 
	pIspReg->GGM_D2A_GGM_LUT[88].Raw = 0x0e0380e0; /*0x150247A0  DIP_A_GGM2_LUT[88] */ 
	pIspReg->GGM_D2A_GGM_LUT[89].Raw = 0x0e4390e4; /*0x150247A4  DIP_A_GGM2_LUT[89] */ 
	pIspReg->GGM_D2A_GGM_LUT[90].Raw = 0x0e83a0e8; /*0x150247A8  DIP_A_GGM2_LUT[90] */ 
	pIspReg->GGM_D2A_GGM_LUT[91].Raw = 0x0ec3b0ec; /*0x150247AC  DIP_A_GGM2_LUT[91] */ 
	pIspReg->GGM_D2A_GGM_LUT[92].Raw = 0x0f03c0f0; /*0x150247B0  DIP_A_GGM2_LUT[92] */ 
	pIspReg->GGM_D2A_GGM_LUT[93].Raw = 0x0f43d0f4; /*0x150247B4  DIP_A_GGM2_LUT[93] */ 
	pIspReg->GGM_D2A_GGM_LUT[94].Raw = 0x0f83e0f8; /*0x150247B8  DIP_A_GGM2_LUT[94] */ 
	pIspReg->GGM_D2A_GGM_LUT[95].Raw = 0x0fc3f0fc; /*0x150247BC  DIP_A_GGM2_LUT[95] */ 
	pIspReg->GGM_D2A_GGM_LUT[96].Raw = 0x10040100; /*0x150247C0  DIP_A_GGM2_LUT[96] */	
	pIspReg->GGM_D2A_GGM_LUT[97].Raw = 0x10842108; /*0x150247C4  DIP_A_GGM2_LUT[97] */	
	pIspReg->GGM_D2A_GGM_LUT[98].Raw = 0x11044110; /*0x150247C8  DIP_A_GGM2_LUT[98] */	
	pIspReg->GGM_D2A_GGM_LUT[99].Raw = 0x11846118; /*0x150247CC  DIP_A_GGM2_LUT[99] */	
	pIspReg->GGM_D2A_GGM_LUT[100].Raw = 0x12048120; /*0x150247D0  DIP_A_GGM2_LUT[100] */ 
	pIspReg->GGM_D2A_GGM_LUT[101].Raw = 0x1284a128; /*0x150247D4  DIP_A_GGM2_LUT[101] */ 
	pIspReg->GGM_D2A_GGM_LUT[102].Raw = 0x1304c130; /*0x150247D8  DIP_A_GGM2_LUT[102] */ 
	pIspReg->GGM_D2A_GGM_LUT[103].Raw = 0x1384e138; /*0x150247DC  DIP_A_GGM2_LUT[103] */ 
	pIspReg->GGM_D2A_GGM_LUT[104].Raw = 0x14050140; /*0x150247E0  DIP_A_GGM2_LUT[104] */ 
	pIspReg->GGM_D2A_GGM_LUT[105].Raw = 0x14852148; /*0x150247E4  DIP_A_GGM2_LUT[105] */ 
	pIspReg->GGM_D2A_GGM_LUT[106].Raw = 0x15054150; /*0x150247E8  DIP_A_GGM2_LUT[106] */ 
	pIspReg->GGM_D2A_GGM_LUT[107].Raw = 0x15856158; /*0x150247EC  DIP_A_GGM2_LUT[107] */ 
	pIspReg->GGM_D2A_GGM_LUT[108].Raw = 0x16058160; /*0x150247F0  DIP_A_GGM2_LUT[108] */ 
	pIspReg->GGM_D2A_GGM_LUT[109].Raw = 0x1685a168; /*0x150247F4  DIP_A_GGM2_LUT[109] */ 
	pIspReg->GGM_D2A_GGM_LUT[110].Raw = 0x1705c170; /*0x150247F8  DIP_A_GGM2_LUT[110] */ 
	pIspReg->GGM_D2A_GGM_LUT[111].Raw = 0x1785e178; /*0x150247FC  DIP_A_GGM2_LUT[111] */ 
	pIspReg->GGM_D2A_GGM_LUT[112].Raw = 0x18060180; /*0x15024800  DIP_A_GGM2_LUT[112] */ 
	pIspReg->GGM_D2A_GGM_LUT[113].Raw = 0x18862188; /*0x15024804  DIP_A_GGM2_LUT[113] */ 
	pIspReg->GGM_D2A_GGM_LUT[114].Raw = 0x19064190; /*0x15024808  DIP_A_GGM2_LUT[114] */ 
	pIspReg->GGM_D2A_GGM_LUT[115].Raw = 0x19866198; /*0x1502480C  DIP_A_GGM2_LUT[115] */ 
	pIspReg->GGM_D2A_GGM_LUT[116].Raw = 0x1a0681a0; /*0x15024810  DIP_A_GGM2_LUT[116] */ 
	pIspReg->GGM_D2A_GGM_LUT[117].Raw = 0x1a86a1a8; /*0x15024814  DIP_A_GGM2_LUT[117] */ 
	pIspReg->GGM_D2A_GGM_LUT[118].Raw = 0x1b06c1b0; /*0x15024818  DIP_A_GGM2_LUT[118] */ 
	pIspReg->GGM_D2A_GGM_LUT[119].Raw = 0x1b86e1b8; /*0x1502481C  DIP_A_GGM2_LUT[119] */ 
	pIspReg->GGM_D2A_GGM_LUT[120].Raw = 0x1c0701c0; /*0x15024820  DIP_A_GGM2_LUT[120] */ 
	pIspReg->GGM_D2A_GGM_LUT[121].Raw = 0x1c8721c8; /*0x15024824  DIP_A_GGM2_LUT[121] */ 
	pIspReg->GGM_D2A_GGM_LUT[122].Raw = 0x1d0741d0; /*0x15024828  DIP_A_GGM2_LUT[122] */ 
	pIspReg->GGM_D2A_GGM_LUT[123].Raw = 0x1d8761d8; /*0x1502482C  DIP_A_GGM2_LUT[123] */ 
	pIspReg->GGM_D2A_GGM_LUT[124].Raw = 0x1e0781e0; /*0x15024830  DIP_A_GGM2_LUT[124] */  
	pIspReg->GGM_D2A_GGM_LUT[125].Raw = 0x1e87a1e8; /*0x15024834  DIP_A_GGM2_LUT[125] */  
	pIspReg->GGM_D2A_GGM_LUT[126].Raw = 0x1f07c1f0; /*0x15024838  DIP_A_GGM2_LUT[126] */  
	pIspReg->GGM_D2A_GGM_LUT[127].Raw = 0x1f87e1f8; /*0x1502483C  DIP_A_GGM2_LUT[127] */  
	pIspReg->GGM_D2A_GGM_LUT[128].Raw = 0x20080200; /*0x15024840  DIP_A_GGM2_LUT[128] */  
	pIspReg->GGM_D2A_GGM_LUT[129].Raw = 0x20882208; /*0x15024844  DIP_A_GGM2_LUT[129] */  
	pIspReg->GGM_D2A_GGM_LUT[130].Raw = 0x21084210; /*0x15024848  DIP_A_GGM2_LUT[130] */  
	pIspReg->GGM_D2A_GGM_LUT[131].Raw = 0x21886218; /*0x1502484C  DIP_A_GGM2_LUT[131] */  
	pIspReg->GGM_D2A_GGM_LUT[132].Raw = 0x22088220; /*0x15024850  DIP_A_GGM2_LUT[132] */ 
	pIspReg->GGM_D2A_GGM_LUT[133].Raw = 0x2288a228; /*0x15024854  DIP_A_GGM2_LUT[133] */ 
	pIspReg->GGM_D2A_GGM_LUT[134].Raw = 0x2308c230; /*0x15024858  DIP_A_GGM2_LUT[134] */ 
	pIspReg->GGM_D2A_GGM_LUT[135].Raw = 0x2388e238; /*0x1502485C  DIP_A_GGM2_LUT[135] */ 
	pIspReg->GGM_D2A_GGM_LUT[136].Raw = 0x24090240; /*0x15024860  DIP_A_GGM2_LUT[136] */ 
	pIspReg->GGM_D2A_GGM_LUT[137].Raw = 0x24892248; /*0x15024864  DIP_A_GGM2_LUT[137] */ 
	pIspReg->GGM_D2A_GGM_LUT[138].Raw = 0x25094250; /*0x15024868  DIP_A_GGM2_LUT[138] */ 
	pIspReg->GGM_D2A_GGM_LUT[139].Raw = 0x25896258; /*0x1502486C  DIP_A_GGM2_LUT[139] */ 
	pIspReg->GGM_D2A_GGM_LUT[140].Raw = 0x26098260; /*0x15024870  DIP_A_GGM2_LUT[140] */ 
	pIspReg->GGM_D2A_GGM_LUT[141].Raw = 0x2689a268; /*0x15024874  DIP_A_GGM2_LUT[141] */ 
	pIspReg->GGM_D2A_GGM_LUT[142].Raw = 0x2709c270; /*0x15024878  DIP_A_GGM2_LUT[142] */ 
	pIspReg->GGM_D2A_GGM_LUT[143].Raw = 0x2789e278; /*0x1502487C  DIP_A_GGM2_LUT[143] */ 
	pIspReg->GGM_D2A_GGM_LUT[144].Raw = 0x280a0280; /*0x15024880  DIP_A_GGM2_LUT[144] */ 
	pIspReg->GGM_D2A_GGM_LUT[145].Raw = 0x288a2288; /*0x15024884  DIP_A_GGM2_LUT[145] */ 
	pIspReg->GGM_D2A_GGM_LUT[146].Raw = 0x290a4290; /*0x15024888  DIP_A_GGM2_LUT[146] */ 
	pIspReg->GGM_D2A_GGM_LUT[147].Raw = 0x298a6298; /*0x1502488C  DIP_A_GGM2_LUT[147] */ 
	pIspReg->GGM_D2A_GGM_LUT[148].Raw = 0x2a0a82a0; /*0x15024890  DIP_A_GGM2_LUT[148] */ 
	pIspReg->GGM_D2A_GGM_LUT[149].Raw = 0x2a8aa2a8; /*0x15024894  DIP_A_GGM2_LUT[149] */ 
	pIspReg->GGM_D2A_GGM_LUT[150].Raw = 0x2b0ac2b0; /*0x15024898  DIP_A_GGM2_LUT[150] */ 
	pIspReg->GGM_D2A_GGM_LUT[151].Raw = 0x2b8ae2b8; /*0x1502489C  DIP_A_GGM2_LUT[151] */ 
	pIspReg->GGM_D2A_GGM_LUT[152].Raw = 0x2c0b02c0; /*0x150248A0  DIP_A_GGM2_LUT[152] */ 
	pIspReg->GGM_D2A_GGM_LUT[153].Raw = 0x2c8b22c8; /*0x150248A4  DIP_A_GGM2_LUT[153] */ 
	pIspReg->GGM_D2A_GGM_LUT[154].Raw = 0x2d0b42d0; /*0x150248A8  DIP_A_GGM2_LUT[154] */ 
	pIspReg->GGM_D2A_GGM_LUT[155].Raw = 0x2d8b62d8; /*0x150248AC  DIP_A_GGM2_LUT[155] */ 
	pIspReg->GGM_D2A_GGM_LUT[156].Raw = 0x2e0b82e0; /*0x150248B0  DIP_A_GGM2_LUT[156] */ 
	pIspReg->GGM_D2A_GGM_LUT[157].Raw = 0x2e8ba2e8; /*0x150248B4  DIP_A_GGM2_LUT[157] */ 
	pIspReg->GGM_D2A_GGM_LUT[158].Raw = 0x2f0bc2f0; /*0x150248B8  DIP_A_GGM2_LUT[158] */ 
	pIspReg->GGM_D2A_GGM_LUT[159].Raw = 0x2f8be2f8; /*0x150248BC  DIP_A_GGM2_LUT[159] */ 
	pIspReg->GGM_D2A_GGM_LUT[160].Raw = 0x300c0300; /*0x150248C0  DIP_A_GGM2_LUT[160] */ 
	pIspReg->GGM_D2A_GGM_LUT[161].Raw = 0x308c2308; /*0x150248C4  DIP_A_GGM2_LUT[161] */ 
	pIspReg->GGM_D2A_GGM_LUT[162].Raw = 0x310c4310; /*0x150248C8  DIP_A_GGM2_LUT[162] */ 
	pIspReg->GGM_D2A_GGM_LUT[163].Raw = 0x318c6318; /*0x150248CC  DIP_A_GGM2_LUT[163] */ 
	pIspReg->GGM_D2A_GGM_LUT[164].Raw = 0x320c8320; /*0x150248D0  DIP_A_GGM2_LUT[164] */ 
	pIspReg->GGM_D2A_GGM_LUT[165].Raw = 0x328ca328; /*0x150248D4  DIP_A_GGM2_LUT[165] */ 
	pIspReg->GGM_D2A_GGM_LUT[166].Raw = 0x330cc330; /*0x150248D8  DIP_A_GGM2_LUT[166] */ 
	pIspReg->GGM_D2A_GGM_LUT[167].Raw = 0x338ce338; /*0x150248DC  DIP_A_GGM2_LUT[167] */ 
	pIspReg->GGM_D2A_GGM_LUT[168].Raw = 0x340d0340; /*0x150248E0  DIP_A_GGM2_LUT[168] */ 
	pIspReg->GGM_D2A_GGM_LUT[169].Raw = 0x348d2348; /*0x150248E4  DIP_A_GGM2_LUT[169] */ 
	pIspReg->GGM_D2A_GGM_LUT[170].Raw = 0x350d4350; /*0x150248E8  DIP_A_GGM2_LUT[170] */ 
	pIspReg->GGM_D2A_GGM_LUT[171].Raw = 0x358d6358; /*0x150248EC  DIP_A_GGM2_LUT[171] */ 
	pIspReg->GGM_D2A_GGM_LUT[172].Raw = 0x360d8360; /*0x150248F0  DIP_A_GGM2_LUT[172] */ 
	pIspReg->GGM_D2A_GGM_LUT[173].Raw = 0x368da368; /*0x150248F4  DIP_A_GGM2_LUT[173] */ 
	pIspReg->GGM_D2A_GGM_LUT[174].Raw = 0x370dc370; /*0x150248F8.Raw =	 DIP_A_GGM2_LUT[174] */ 
	pIspReg->GGM_D2A_GGM_LUT[175].Raw = 0x378de378; /*0x150248FC.Raw =	 DIP_A_GGM2_LUT[175] */ 
	pIspReg->GGM_D2A_GGM_LUT[176].Raw = 0x380e0380; /*0x15024900.Raw =	 DIP_A_GGM2_LUT[176] */ 
	pIspReg->GGM_D2A_GGM_LUT[177].Raw = 0x388e2388; /*0x15024904.Raw =	 DIP_A_GGM2_LUT[177] */ 
	pIspReg->GGM_D2A_GGM_LUT[178].Raw = 0x390e4390; /*0x15024908.Raw =	 DIP_A_GGM2_LUT[178] */ 
	pIspReg->GGM_D2A_GGM_LUT[179].Raw = 0x398e6398; /*0x1502490C.Raw =	 DIP_A_GGM2_LUT[179] */ 
	pIspReg->GGM_D2A_GGM_LUT[180].Raw = 0x3a0e83a0; /*0x15024910.Raw =	 DIP_A_GGM2_LUT[180] */ 
	pIspReg->GGM_D2A_GGM_LUT[181].Raw = 0x3a8ea3a8; /*0x15024914.Raw =	 DIP_A_GGM2_LUT[182] */ 
	pIspReg->GGM_D2A_GGM_LUT[182].Raw = 0x3b0ec3b0; /*0x15024918.Raw =	 DIP_A_GGM2_LUT[182] */ 
	pIspReg->GGM_D2A_GGM_LUT[183].Raw = 0x3b8ee3b8; /*0x1502491C.Raw =	 DIP_A_GGM2_LUT[183] */ 
	pIspReg->GGM_D2A_GGM_LUT[184].Raw = 0x3c0f03c0; /*0x15024920.Raw =	 DIP_A_GGM2_LUT[184] */ 
	pIspReg->GGM_D2A_GGM_LUT[185].Raw = 0x3c8f23c8; /*0x15024924.Raw =	 DIP_A_GGM2_LUT[185] */ 
	pIspReg->GGM_D2A_GGM_LUT[186].Raw = 0x3d0f43d0; /*0x15024928.Raw =	 DIP_A_GGM2_LUT[186] */ 
	pIspReg->GGM_D2A_GGM_LUT[187].Raw = 0x3d8f63d8; /*0x1502492C.Raw =	 DIP_A_GGM2_LUT[187] */ 
	pIspReg->GGM_D2A_GGM_LUT[188].Raw = 0x3e0f83e0; /*0x15024930.Raw =	 DIP_A_GGM2_LUT[188] */ 
	pIspReg->GGM_D2A_GGM_LUT[189].Raw = 0x3e0f83e0; /*0x15024934.Raw =	 DIP_A_GGM2_LUT[189] */ 
	pIspReg->GGM_D2A_GGM_LUT[190].Raw = 0x3e0f83e0; /*0x15024938.Raw =	 DIP_A_GGM2_LUT[190] */ 
	pIspReg->GGM_D2A_GGM_LUT[191].Raw = 0x3e0f83e0; /*0x1502493C.Raw =	 DIP_A_GGM2_LUT[191] */ 
	pIspReg->GGM_D2A_GGM_CTRL.Raw = 0x100307FF; /* 0x15024940: DIP_A_GGM2_CTRL */
	pIspReg->GGM_D2A_GGM_SRAM_PINGPONG.Raw = 0;    // Todo Justin

}

void setGGM_D3_UT(dip_x_reg_t* pIspReg){
	pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_GGM_D3_EN = 1;//enable bit
	pIspReg->GGM_D3A_GGM_LUT[0].Raw = 0x00000000;
	pIspReg->GGM_D3A_GGM_LUT[1].Raw = 0x00200802;
	pIspReg->GGM_D3A_GGM_LUT[2].Raw = 0x00401004;
	pIspReg->GGM_D3A_GGM_LUT[3].Raw = 0x00601806;
	pIspReg->GGM_D3A_GGM_LUT[4].Raw = 0x00802008;
	pIspReg->GGM_D3A_GGM_LUT[5].Raw = 0x00a0280a;
	pIspReg->GGM_D3A_GGM_LUT[6].Raw = 0x00c0300c;
	pIspReg->GGM_D3A_GGM_LUT[7].Raw = 0x00e0380e;
	pIspReg->GGM_D3A_GGM_LUT[8].Raw = 0x01004010;
	pIspReg->GGM_D3A_GGM_LUT[9].Raw = 0x01204812;
	pIspReg->GGM_D3A_GGM_LUT[10].Raw = 0x01405014;  
	pIspReg->GGM_D3A_GGM_LUT[11].Raw = 0x01605816;  
	pIspReg->GGM_D3A_GGM_LUT[12].Raw = 0x01806018;	
	pIspReg->GGM_D3A_GGM_LUT[13].Raw = 0x01a0681a;	
	pIspReg->GGM_D3A_GGM_LUT[14].Raw = 0x01c0701c;	
	pIspReg->GGM_D3A_GGM_LUT[15].Raw = 0x01e0781e;	
	pIspReg->GGM_D3A_GGM_LUT[16].Raw = 0x02008020;  
	pIspReg->GGM_D3A_GGM_LUT[17].Raw = 0x02208822;  
	pIspReg->GGM_D3A_GGM_LUT[18].Raw = 0x02409024;  
	pIspReg->GGM_D3A_GGM_LUT[19].Raw = 0x02609826;  
	pIspReg->GGM_D3A_GGM_LUT[20].Raw = 0x0280a028;	
	pIspReg->GGM_D3A_GGM_LUT[21].Raw = 0x02a0a82a;	
	pIspReg->GGM_D3A_GGM_LUT[22].Raw = 0x02c0b02c;	
	pIspReg->GGM_D3A_GGM_LUT[23].Raw = 0x02e0b82e;	
	pIspReg->GGM_D3A_GGM_LUT[24].Raw = 0x0300c030;	 
	pIspReg->GGM_D3A_GGM_LUT[25].Raw = 0x0320c832;	 
	pIspReg->GGM_D3A_GGM_LUT[26].Raw = 0x0340d034;	 
	pIspReg->GGM_D3A_GGM_LUT[27].Raw = 0x0360d836;	 
	pIspReg->GGM_D3A_GGM_LUT[28].Raw = 0x0380e038;	  
	pIspReg->GGM_D3A_GGM_LUT[29].Raw = 0x03a0e83a;	  
	pIspReg->GGM_D3A_GGM_LUT[30].Raw = 0x03c0f03c;	  
	pIspReg->GGM_D3A_GGM_LUT[31].Raw = 0x03e0f83e;	  
	pIspReg->GGM_D3A_GGM_LUT[32].Raw = 0x04010040;  
	pIspReg->GGM_D3A_GGM_LUT[33].Raw = 0x04210842;  
	pIspReg->GGM_D3A_GGM_LUT[34].Raw = 0x04411044;  
	pIspReg->GGM_D3A_GGM_LUT[35].Raw = 0x04611846;  
	pIspReg->GGM_D3A_GGM_LUT[36].Raw = 0x04812048;  
	pIspReg->GGM_D3A_GGM_LUT[37].Raw = 0x04a1284a;  
	pIspReg->GGM_D3A_GGM_LUT[38].Raw = 0x04c1304c;  
	pIspReg->GGM_D3A_GGM_LUT[39].Raw = 0x04e1384e;  
	pIspReg->GGM_D3A_GGM_LUT[40].Raw = 0x05014050;	
	pIspReg->GGM_D3A_GGM_LUT[41].Raw = 0x05214852;	
	pIspReg->GGM_D3A_GGM_LUT[42].Raw = 0x05415054;	
	pIspReg->GGM_D3A_GGM_LUT[43].Raw = 0x05615856;	
	pIspReg->GGM_D3A_GGM_LUT[44].Raw = 0x05816058;	 
	pIspReg->GGM_D3A_GGM_LUT[45].Raw = 0x05a1685a;	 
	pIspReg->GGM_D3A_GGM_LUT[46].Raw = 0x05c1705c;	 
	pIspReg->GGM_D3A_GGM_LUT[47].Raw = 0x05e1785e;	 
	pIspReg->GGM_D3A_GGM_LUT[48].Raw = 0x06018060;	  
	pIspReg->GGM_D3A_GGM_LUT[49].Raw = 0x06218862;	  
	pIspReg->GGM_D3A_GGM_LUT[50].Raw = 0x06419064;	  
	pIspReg->GGM_D3A_GGM_LUT[51].Raw = 0x06619866;	  
	pIspReg->GGM_D3A_GGM_LUT[52].Raw = 0x0681a068;	   
	pIspReg->GGM_D3A_GGM_LUT[53].Raw = 0x06a1a86a;	   
	pIspReg->GGM_D3A_GGM_LUT[54].Raw = 0x06c1b06c;	   
	pIspReg->GGM_D3A_GGM_LUT[55].Raw = 0x06e1b86e;	   
	pIspReg->GGM_D3A_GGM_LUT[56].Raw = 0x0701c070;  
	pIspReg->GGM_D3A_GGM_LUT[57].Raw = 0x0721c872;  
	pIspReg->GGM_D3A_GGM_LUT[58].Raw = 0x0741d074;  
	pIspReg->GGM_D3A_GGM_LUT[59].Raw = 0x0761d876;  
	pIspReg->GGM_D3A_GGM_LUT[60].Raw = 0x0781e078;  
	pIspReg->GGM_D3A_GGM_LUT[61].Raw = 0x07a1e87a;  
	pIspReg->GGM_D3A_GGM_LUT[62].Raw = 0x07c1f07c;  
	pIspReg->GGM_D3A_GGM_LUT[63].Raw = 0x07e1f87e;  
	pIspReg->GGM_D3A_GGM_LUT[64].Raw = 0x08020080; 
	pIspReg->GGM_D3A_GGM_LUT[65].Raw = 0x08421084; 
	pIspReg->GGM_D3A_GGM_LUT[66].Raw = 0x08822088; 
	pIspReg->GGM_D3A_GGM_LUT[67].Raw = 0x08c2308c; 
	pIspReg->GGM_D3A_GGM_LUT[68].Raw = 0x09024090; 
	pIspReg->GGM_D3A_GGM_LUT[69].Raw = 0x09425094; 
	pIspReg->GGM_D3A_GGM_LUT[70].Raw = 0x09826098; 
	pIspReg->GGM_D3A_GGM_LUT[71].Raw = 0x09c2709c; 
	pIspReg->GGM_D3A_GGM_LUT[72].Raw = 0x0a0280a0; 
	pIspReg->GGM_D3A_GGM_LUT[73].Raw = 0x0a4290a4; 
	pIspReg->GGM_D3A_GGM_LUT[74].Raw = 0x0a82a0a8; 
	pIspReg->GGM_D3A_GGM_LUT[75].Raw = 0x0ac2b0ac; 
	pIspReg->GGM_D3A_GGM_LUT[76].Raw = 0x0b02c0b0;  
	pIspReg->GGM_D3A_GGM_LUT[77].Raw = 0x0b42d0b4;  
	pIspReg->GGM_D3A_GGM_LUT[78].Raw = 0x0b82e0b8;  
	pIspReg->GGM_D3A_GGM_LUT[79].Raw = 0x0bc2f0bc;  
	pIspReg->GGM_D3A_GGM_LUT[80].Raw = 0x0c0300c0;	
	pIspReg->GGM_D3A_GGM_LUT[81].Raw = 0x0c4310c4;	
	pIspReg->GGM_D3A_GGM_LUT[82].Raw = 0x0c8320c8;	
	pIspReg->GGM_D3A_GGM_LUT[83].Raw = 0x0cc330cc;	
	pIspReg->GGM_D3A_GGM_LUT[84].Raw = 0x0d0340d0;	
	pIspReg->GGM_D3A_GGM_LUT[85].Raw = 0x0d4350d4;	
	pIspReg->GGM_D3A_GGM_LUT[86].Raw = 0x0d8360d8;	
	pIspReg->GGM_D3A_GGM_LUT[87].Raw = 0x0dc370dc;	
	pIspReg->GGM_D3A_GGM_LUT[88].Raw = 0x0e0380e0; 
	pIspReg->GGM_D3A_GGM_LUT[89].Raw = 0x0e4390e4; 
	pIspReg->GGM_D3A_GGM_LUT[90].Raw = 0x0e83a0e8; 
	pIspReg->GGM_D3A_GGM_LUT[91].Raw = 0x0ec3b0ec; 
	pIspReg->GGM_D3A_GGM_LUT[92].Raw = 0x0f03c0f0; 
	pIspReg->GGM_D3A_GGM_LUT[93].Raw = 0x0f43d0f4; 
	pIspReg->GGM_D3A_GGM_LUT[94].Raw = 0x0f83e0f8; 
	pIspReg->GGM_D3A_GGM_LUT[95].Raw = 0x0fc3f0fc; 
	pIspReg->GGM_D3A_GGM_LUT[96].Raw = 0x10040100;  
	pIspReg->GGM_D3A_GGM_LUT[97].Raw = 0x10842108;  
	pIspReg->GGM_D3A_GGM_LUT[98].Raw = 0x11044110;  
	pIspReg->GGM_D3A_GGM_LUT[99].Raw = 0x11846118;  
	pIspReg->GGM_D3A_GGM_LUT[100].Raw = 0x12048120; 
	pIspReg->GGM_D3A_GGM_LUT[101].Raw = 0x1284a128; 
	pIspReg->GGM_D3A_GGM_LUT[102].Raw = 0x1304c130; 
	pIspReg->GGM_D3A_GGM_LUT[103].Raw = 0x1384e138; 
	pIspReg->GGM_D3A_GGM_LUT[104].Raw = 0x14050140; 
	pIspReg->GGM_D3A_GGM_LUT[105].Raw = 0x14852148; 
	pIspReg->GGM_D3A_GGM_LUT[106].Raw = 0x15054150; 
	pIspReg->GGM_D3A_GGM_LUT[107].Raw = 0x15856158; 
	pIspReg->GGM_D3A_GGM_LUT[108].Raw = 0x16058160; 
	pIspReg->GGM_D3A_GGM_LUT[109].Raw = 0x1685a168; 
	pIspReg->GGM_D3A_GGM_LUT[110].Raw = 0x1705c170; 
	pIspReg->GGM_D3A_GGM_LUT[111].Raw = 0x1785e178; 
	pIspReg->GGM_D3A_GGM_LUT[112].Raw = 0x18060180; 
	pIspReg->GGM_D3A_GGM_LUT[113].Raw = 0x18862188; 
	pIspReg->GGM_D3A_GGM_LUT[114].Raw = 0x19064190; 
	pIspReg->GGM_D3A_GGM_LUT[115].Raw = 0x19866198; 
	pIspReg->GGM_D3A_GGM_LUT[116].Raw = 0x1a0681a0; 
	pIspReg->GGM_D3A_GGM_LUT[117].Raw = 0x1a86a1a8; 
	pIspReg->GGM_D3A_GGM_LUT[118].Raw = 0x1b06c1b0; 
	pIspReg->GGM_D3A_GGM_LUT[119].Raw = 0x1b86e1b8; 
	pIspReg->GGM_D3A_GGM_LUT[120].Raw = 0x1c0701c0; 
	pIspReg->GGM_D3A_GGM_LUT[121].Raw = 0x1c8721c8; 
	pIspReg->GGM_D3A_GGM_LUT[122].Raw = 0x1d0741d0; 
	pIspReg->GGM_D3A_GGM_LUT[123].Raw = 0x1d8761d8; 
	pIspReg->GGM_D3A_GGM_LUT[124].Raw = 0x1e0781e0;  
	pIspReg->GGM_D3A_GGM_LUT[125].Raw = 0x1e87a1e8;  
	pIspReg->GGM_D3A_GGM_LUT[126].Raw = 0x1f07c1f0;  
	pIspReg->GGM_D3A_GGM_LUT[127].Raw = 0x1f87e1f8;  
	pIspReg->GGM_D3A_GGM_LUT[128].Raw = 0x20080200;  
	pIspReg->GGM_D3A_GGM_LUT[129].Raw = 0x20882208;  
	pIspReg->GGM_D3A_GGM_LUT[130].Raw = 0x21084210;  
	pIspReg->GGM_D3A_GGM_LUT[131].Raw = 0x21886218;  
	pIspReg->GGM_D3A_GGM_LUT[132].Raw = 0x22088220; 
	pIspReg->GGM_D3A_GGM_LUT[133].Raw = 0x2288a228; 
	pIspReg->GGM_D3A_GGM_LUT[134].Raw = 0x2308c230; 
	pIspReg->GGM_D3A_GGM_LUT[135].Raw = 0x2388e238; 
	pIspReg->GGM_D3A_GGM_LUT[136].Raw = 0x24090240; 
	pIspReg->GGM_D3A_GGM_LUT[137].Raw = 0x24892248; 
	pIspReg->GGM_D3A_GGM_LUT[138].Raw = 0x25094250; 
	pIspReg->GGM_D3A_GGM_LUT[139].Raw = 0x25896258; 
	pIspReg->GGM_D3A_GGM_LUT[140].Raw = 0x26098260; 
	pIspReg->GGM_D3A_GGM_LUT[141].Raw = 0x2689a268; 
	pIspReg->GGM_D3A_GGM_LUT[142].Raw = 0x2709c270; 
	pIspReg->GGM_D3A_GGM_LUT[143].Raw = 0x2789e278; 
	pIspReg->GGM_D3A_GGM_LUT[144].Raw = 0x280a0280; 
	pIspReg->GGM_D3A_GGM_LUT[145].Raw = 0x288a2288; 
	pIspReg->GGM_D3A_GGM_LUT[146].Raw = 0x290a4290; 
	pIspReg->GGM_D3A_GGM_LUT[147].Raw = 0x298a6298; 
	pIspReg->GGM_D3A_GGM_LUT[148].Raw = 0x2a0a82a0; 
	pIspReg->GGM_D3A_GGM_LUT[149].Raw = 0x2a8aa2a8; 
	pIspReg->GGM_D3A_GGM_LUT[150].Raw = 0x2b0ac2b0; 
	pIspReg->GGM_D3A_GGM_LUT[151].Raw = 0x2b8ae2b8; 
	pIspReg->GGM_D3A_GGM_LUT[152].Raw = 0x2c0b02c0; 
	pIspReg->GGM_D3A_GGM_LUT[153].Raw = 0x2c8b22c8; 
	pIspReg->GGM_D3A_GGM_LUT[154].Raw = 0x2d0b42d0; 
	pIspReg->GGM_D3A_GGM_LUT[155].Raw = 0x2d8b62d8; 
	pIspReg->GGM_D3A_GGM_LUT[156].Raw = 0x2e0b82e0; 
	pIspReg->GGM_D3A_GGM_LUT[157].Raw = 0x2e8ba2e8; 
	pIspReg->GGM_D3A_GGM_LUT[158].Raw = 0x2f0bc2f0; 
	pIspReg->GGM_D3A_GGM_LUT[159].Raw = 0x2f8be2f8; 
	pIspReg->GGM_D3A_GGM_LUT[160].Raw = 0x300c0300; 
	pIspReg->GGM_D3A_GGM_LUT[161].Raw = 0x308c2308; 
	pIspReg->GGM_D3A_GGM_LUT[162].Raw = 0x310c4310; 
	pIspReg->GGM_D3A_GGM_LUT[163].Raw = 0x318c6318; 
	pIspReg->GGM_D3A_GGM_LUT[164].Raw = 0x320c8320; 
	pIspReg->GGM_D3A_GGM_LUT[165].Raw = 0x328ca328; 
	pIspReg->GGM_D3A_GGM_LUT[166].Raw = 0x330cc330; 
	pIspReg->GGM_D3A_GGM_LUT[167].Raw = 0x338ce338; 
	pIspReg->GGM_D3A_GGM_LUT[168].Raw = 0x340d0340; 
	pIspReg->GGM_D3A_GGM_LUT[169].Raw = 0x348d2348; 
	pIspReg->GGM_D3A_GGM_LUT[170].Raw = 0x350d4350; 
	pIspReg->GGM_D3A_GGM_LUT[171].Raw = 0x358d6358; 
	pIspReg->GGM_D3A_GGM_LUT[172].Raw = 0x360d8360; 
	pIspReg->GGM_D3A_GGM_LUT[173].Raw = 0x368da368; 
	pIspReg->GGM_D3A_GGM_LUT[174].Raw = 0x370dc370; 
	pIspReg->GGM_D3A_GGM_LUT[175].Raw = 0x378de378; 
	pIspReg->GGM_D3A_GGM_LUT[176].Raw = 0x380e0380; 
	pIspReg->GGM_D3A_GGM_LUT[177].Raw = 0x388e2388; 
	pIspReg->GGM_D3A_GGM_LUT[178].Raw = 0x390e4390; 
	pIspReg->GGM_D3A_GGM_LUT[179].Raw = 0x398e6398; 
	pIspReg->GGM_D3A_GGM_LUT[180].Raw = 0x3a0e83a0; 
	pIspReg->GGM_D3A_GGM_LUT[181].Raw = 0x3a8ea3a8; 
	pIspReg->GGM_D3A_GGM_LUT[182].Raw = 0x3b0ec3b0; 
	pIspReg->GGM_D3A_GGM_LUT[183].Raw = 0x3b8ee3b8; 
	pIspReg->GGM_D3A_GGM_LUT[184].Raw = 0x3c0f03c0; 
	pIspReg->GGM_D3A_GGM_LUT[185].Raw = 0x3c8f23c8; 
	pIspReg->GGM_D3A_GGM_LUT[186].Raw = 0x3d0f43d0; 
	pIspReg->GGM_D3A_GGM_LUT[187].Raw = 0x3d8f63d8; 
	pIspReg->GGM_D3A_GGM_LUT[188].Raw = 0x3e0f83e0; 
	pIspReg->GGM_D3A_GGM_LUT[189].Raw = 0x3e0f83e0; 
	pIspReg->GGM_D3A_GGM_LUT[190].Raw = 0x3e0f83e0; 
	pIspReg->GGM_D3A_GGM_LUT[191].Raw = 0x3e0f83e0; 
	pIspReg->GGM_D3A_GGM_CTRL.Raw = 0x100307FF;
	pIspReg->GGM_D3A_GGM_SRAM_PINGPONG.Raw = 0;    // Todo Justin
}


void setC24_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->C24_D1A_C24_TILE_EDGE.Raw = 0x0f;
}


void setC02_D2_UT(dip_x_reg_t* pIspReg){
	// setC02b = setC02_D2_UT
	pIspReg->C02_D2A_C02_CON.Raw =  0x01f;														
	pIspReg->C02_D2A_C02_CROP_CON1.Raw =  0x00;													
	pIspReg->C02_D2A_C02_CROP_CON2.Raw =  0x00;													
	pIspReg->C02_D2A_C02_CROP_CON1.Bits.C02_CROP_XSTART = 0x0;									
	pIspReg->C02_D2A_C02_CROP_CON1.Bits.C02_CROP_XEND = 0x0 /*imageioPackage.DMAImgi.dma_cfg.size.xsiz*/; // Todo Justin
	pIspReg->C02_D2A_C02_CROP_CON2.Bits.C02_CROP_YSTART = 0x0;									
	pIspReg->C02_D2A_C02_CROP_CON2.Bits.C02_CROP_YEND = 0x0/*imageioPackage.DMAImgi.dma_cfg.size.h-1*/;  // Todo Justin
}

void setG2C_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->G2C_D1A_G2C_CONV_0A.Raw = 0x012D0099;			   
	pIspReg->G2C_D1A_G2C_CONV_0B.Raw = 0x0000003A;	   
	pIspReg->G2C_D1A_G2C_CONV_1A.Raw = 0x075607AA;	   
	pIspReg->G2C_D1A_G2C_CONV_1B.Raw = 0x00000100;	   
	pIspReg->G2C_D1A_G2C_CONV_2A.Raw = 0x072A0100;	   
	pIspReg->G2C_D1A_G2C_CONV_2B.Raw = 0x000007D6;	   
//		pIspReg->G2C_D1A_G2C_SHADE_CON_1.Raw =	0x0118000E;  //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_SHADE_CON_2.Raw = 0x0074B740;   //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_SHADE_CON_3.Raw =	0x00000133;  //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_SHADE_TAR.Raw = 0x079F0A5A;     //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_SHADE_SP.Raw =  0x00000000;     //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_CFC_CON_1.Raw = 0x03f70080;     //TODO G2CX only
//		pIspReg->G2C_D1A_G2C_CFC_CON_2.Raw =  0x1CE539CE;    //TODO G2CX only
}

void setG2CX_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_G2CX_D1_EN = 1;//enable bit
	pIspReg->G2CX_D1A_G2CX_CONV_0A.Raw = 0x012D0099;			   
	pIspReg->G2CX_D1A_G2CX_CONV_0B.Raw = 0x0000003A;	   
	pIspReg->G2CX_D1A_G2CX_CONV_1A.Raw = 0x075607AA;	   
	pIspReg->G2CX_D1A_G2CX_CONV_1B.Raw = 0x00000100;	   
	pIspReg->G2CX_D1A_G2CX_CONV_2A.Raw = 0x072A0100;	   
	pIspReg->G2CX_D1A_G2CX_CONV_2B.Raw = 0x000007D6;	   
	pIspReg->G2CX_D1A_G2CX_SHADE_CON_1.Raw =	0x0118000E;
	pIspReg->G2CX_D1A_G2CX_SHADE_CON_2.Raw = 0x0074B740; 
	pIspReg->G2CX_D1A_G2CX_SHADE_CON_3.Raw =	0x00000133;
	pIspReg->G2CX_D1A_G2CX_SHADE_TAR.Raw = 0x079F0A5A;   
	pIspReg->G2CX_D1A_G2CX_SHADE_SP.Raw =  0x00000000;   
	pIspReg->G2CX_D1A_G2CX_CFC_CON_1.Raw = 0x03f70080;   
	pIspReg->G2CX_D1A_G2CX_CFC_CON_2.Raw =  0x1CE539CE;  
}

void setC42_D2_UT(dip_x_reg_t* pIspReg){
	pIspReg->C42_D2A_C42_CON.Raw = 0xf0;
}

void setSRZ_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->SRZ_D1A_SRZ_CONTROL.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.ctrl;
	pIspReg->SRZ_D1A_SRZ_IN_IMG.Bits.SRZ_IN_WD  = 0;  // Todo Justin, imageioPackage.srz1Cfg.inout_size.in_w;
	pIspReg->SRZ_D1A_SRZ_IN_IMG.Bits.SRZ_IN_HT  = 0;  // Todo Justin, imageioPackage.srz1Cfg.inout_size.in_h;
	pIspReg->SRZ_D1A_SRZ_OUT_IMG.Bits.SRZ_OUT_WD   = 0;  // Todo Justin, imageioPackage.srz1Cfg.inout_size.out_w;
	pIspReg->SRZ_D1A_SRZ_OUT_IMG.Bits.SRZ_OUT_HT  = 0;  // Todo Justin, imageioPackage.srz1Cfg.inout_size.out_h;
	pIspReg->SRZ_D1A_SRZ_HORI_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.h_step;
	pIspReg->SRZ_D1A_SRZ_VERT_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.v_step;
	pIspReg->SRZ_D1A_SRZ_HORI_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.crop.x;
	pIspReg->SRZ_D1A_SRZ_HORI_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.crop.floatX;
	pIspReg->SRZ_D1A_SRZ_VERT_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.crop.y;
	pIspReg->SRZ_D1A_SRZ_VERT_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz1Cfg.crop.floatY;

}

void setSRZ_D3_UT(dip_x_reg_t* pIspReg){
	pIspReg->SRZ_D3A_SRZ_CONTROL.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.ctrl;
	pIspReg->SRZ_D3A_SRZ_IN_IMG.Bits.SRZ_IN_WD  = 0;  // Todo Justin, imageioPackage.srz3Cfg.inout_size.in_w;
	pIspReg->SRZ_D3A_SRZ_IN_IMG.Bits.SRZ_IN_HT  = 0;  // Todo Justin, imageioPackage.srz3Cfg.inout_size.in_h;
	pIspReg->SRZ_D3A_SRZ_OUT_IMG.Bits.SRZ_OUT_WD   = 0;  // Todo Justin, imageioPackage.srz3Cfg.inout_size.out_w;
	pIspReg->SRZ_D3A_SRZ_OUT_IMG.Bits.SRZ_OUT_HT  = 0;  // Todo Justin, imageioPackage.srz3Cfg.inout_size.out_h;
	pIspReg->SRZ_D3A_SRZ_HORI_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.h_step;
	pIspReg->SRZ_D3A_SRZ_VERT_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.v_step;
	pIspReg->SRZ_D3A_SRZ_HORI_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.crop.x;
	pIspReg->SRZ_D3A_SRZ_HORI_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.crop.floatX;
	pIspReg->SRZ_D3A_SRZ_VERT_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.crop.y;
	pIspReg->SRZ_D3A_SRZ_VERT_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz3Cfg.crop.floatY;
}

void setSRZ_D4_UT(dip_x_reg_t* pIspReg){
	pIspReg->SRZ_D4A_SRZ_CONTROL.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.ctrl;
	pIspReg->SRZ_D4A_SRZ_IN_IMG.Bits.SRZ_IN_WD  = 0;  // Todo Justin, imageioPackage.srz4Cfg.inout_size.in_w;
	pIspReg->SRZ_D4A_SRZ_IN_IMG.Bits.SRZ_IN_HT  = 0;  // Todo Justin, imageioPackage.srz4Cfg.inout_size.in_h;
	pIspReg->SRZ_D4A_SRZ_OUT_IMG.Bits.SRZ_OUT_WD   = 0;  // Todo Justin, imageioPackage.srz4Cfg.inout_size.out_w;
	pIspReg->SRZ_D4A_SRZ_OUT_IMG.Bits.SRZ_OUT_HT  = 0;  // Todo Justin, imageioPackage.srz4Cfg.inout_size.out_h;
	pIspReg->SRZ_D4A_SRZ_HORI_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.h_step;
	pIspReg->SRZ_D4A_SRZ_VERT_STEP.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.v_step;
	pIspReg->SRZ_D4A_SRZ_HORI_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.crop.x;
	pIspReg->SRZ_D4A_SRZ_HORI_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.crop.floatX;
	pIspReg->SRZ_D4A_SRZ_VERT_INT_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.crop.y;
	pIspReg->SRZ_D4A_SRZ_VERT_SUB_OFST.Raw  = 0;  // Todo Justin, imageioPackage.srz4Cfg.crop.floatY;
}


void setMIX_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_Y_EN = 0x1;
	         
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_Y_DEFAULT = 0x0;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_UV_EN = 0x1;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_UV_DEFAULT = 0x0;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_WT_SEL = 0x1;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_B0 = 0x0;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_B1 = 0xff;
	pIspReg->MIX_D1A_MIX_CTRL0.Bits.MIX_DT = 0x1;
	pIspReg->MIX_D1A_MIX_CTRL1.Bits.MIX_M0 = 0x0;
	pIspReg->MIX_D1A_MIX_CTRL1.Bits.MIX_M1 = 0xff;
}

void setMIX_D2_UT(dip_x_reg_t* pIspReg){
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_Y_EN = 0x1;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_Y_DEFAULT = 0x0;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_UV_EN = 0x1;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_UV_DEFAULT = 0x0;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_WT_SEL = 0x1;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_B0 = 0x0;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_B1 = 0xff;
	pIspReg->MIX_D2A_MIX_CTRL0.Bits.MIX_DT = 0x1;
	pIspReg->MIX_D2A_MIX_CTRL1.Bits.MIX_M0 = 0x0;
	pIspReg->MIX_D2A_MIX_CTRL1.Bits.MIX_M1 = 0xff;
}

void setCRSP_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->CRSP_D1A_CRSP_CTRL.Bits.CRSP_HORI_EN = 0x0;
	pIspReg->CRSP_D1A_CRSP_CTRL.Bits.CRSP_VERT_EN = 0x1;//the same with crsp_en
	pIspReg->CRSP_D1A_CRSP_STEP_OFST.Bits.CRSP_STEP_X = 0x4;
	pIspReg->CRSP_D1A_CRSP_STEP_OFST.Bits.CRSP_STEP_Y = 0x4;
	pIspReg->CRSP_D1A_CRSP_STEP_OFST.Bits.CRSP_OFST_X = 0x0;
	pIspReg->CRSP_D1A_CRSP_STEP_OFST.Bits.CRSP_OFST_Y = 0x1;
	pIspReg->CRSP_D1A_CRSP_OUT_IMG.Bits.CRSP_WD = 0/*imageioPackage.crspCfg.out.w*/;//desImgW; //Todo Justin
	pIspReg->CRSP_D1A_CRSP_OUT_IMG.Bits.CRSP_HT = 0/*imageioPackage.crspCfg.out.h*/;//desImgH; //Todo Justin
	pIspReg->CRSP_D1A_CRSP_CTRL.Bits.CRSP_CROP_EN = 0x1;
	pIspReg->CRSP_D1A_CRSP_CROP_X.Bits.CRSP_CROP_XSTART = 0x0;
	pIspReg->CRSP_D1A_CRSP_CROP_X.Bits.CRSP_CROP_XEND = 0/*imageioPackage.crspCfg.out.w-1*/;  //Todo Justin
	pIspReg->CRSP_D1A_CRSP_CROP_Y.Bits.CRSP_CROP_YSTART = 0x0;
	pIspReg->CRSP_D1A_CRSP_CROP_Y.Bits.CRSP_CROP_YEND = 0/*imageioPackage.crspCfg.out.h-1*/;  //Todo Justin
}


void setDFE_D1_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN2.Bits.DIPCTL_DFE_D1_EN = 1;//enable bit
	pIspReg->DFE_D1A_DFE_FE_CTRL1.Raw = 0xAD;
}



void setC24_D2_UT(dip_x_reg_t* pIspReg){
	pIspReg->C24_D2A_C24_TILE_EDGE.Raw = 0xf; //default(0x0f)
}


void setC02_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->C02_D1A_C02_CON.Bits.C02_TPIPE_EDGE = 0x1F;
	pIspReg->C02_D1A_C02_CROP_CON1.Bits.C02_CROP_XSTART = 0x0;
	pIspReg->C02_D1A_C02_CROP_CON1.Bits.C02_CROP_XEND = 0/*imageioPackage.DMAVipi.dma_cfg.size.xsize-1*/; //Todo Justin
	pIspReg->C02_D1A_C02_CROP_CON2.Bits.C02_CROP_YSTART = 0x0;
	pIspReg->C02_D1A_C02_CROP_CON2.Bits.C02_CROP_YEND = 0/*imageioPackage.DMAVipi.dma_cfg.size.h-1*/;     //Todo Justin
}


void setFM_D1_UT(dip_x_reg_t* pIspReg){
	pIspReg->DMGI_D1A_DMGI_CON.Raw = 0x10000019;
	pIspReg->DMGI_D1A_DMGI_CON2.Raw = 0x00160010;
	pIspReg->DMGI_D1A_DMGI_CON3.Raw = 0x00000000;
	pIspReg->DEPI_D1A_DEPI_CON.Raw = 0x20000020;
	pIspReg->DEPI_D1A_DEPI_CON2.Raw = 0x001B000F;
	pIspReg->DEPI_D1A_DEPI_CON3.Raw = 0x001F000C;
}


void set_UT(dip_x_reg_t* pIspReg){

}

void setDM_D1_UT(dip_x_reg_t* pIspReg, int enFgMode){
    pIspReg->DIPCTL_D1A_DIPCTL_RGB_EN1.Bits.DIPCTL_DM_D1_EN = 1;//enable bit    
    pIspReg->DM_D1A_DM_INTP_CRS.Raw = 0x0002F004;
    pIspReg->DM_D1A_DM_INTP_NAT.Raw = 0x1430053F;
    pIspReg->DM_D1A_DM_INTP_AUG.Raw = 0x00500500;
    pIspReg->DM_D1A_DM_LUMA_LUT1.Raw = 0x052A30DC;
    pIspReg->DM_D1A_DM_LUMA_LUT2.Raw = 0x02A9124F;
    pIspReg->DM_D1A_DM_SL_CTL.Raw = 0x0039B4A0;
    pIspReg->DM_D1A_DM_HFTD_CTL.Raw = 0x0A529400;
    pIspReg->DM_D1A_DM_NR_STR.Raw = 0x81028000;
    pIspReg->DM_D1A_DM_NR_ACT.Raw = 0x00000050;
    pIspReg->DM_D1A_DM_HF_STR.Raw = 0x84210000;
    pIspReg->DM_D1A_DM_HF_ACT1.Raw = 0x46FF1EFF;
    pIspReg->DM_D1A_DM_HF_ACT2.Raw = 0x001EFF55;
    pIspReg->DM_D1A_DM_CLIP.Raw = 0x00942064;
    pIspReg->DM_D1A_DM_DSB.Raw = 0x0|enFgMode;
    pIspReg->DM_D1A_DM_TILE_EDGE.Raw = 0x0000000F;
    pIspReg->DM_D1A_DM_P1_ACT.Raw = 0x000000FF;
    pIspReg->DM_D1A_DM_LR_RAT.Raw = 0x00000418;
    pIspReg->DM_D1A_DM_HFTD_CTL2.Raw = 0x00000019;
    pIspReg->DM_D1A_DM_EST_CTL.Raw = 0x00000035;
    pIspReg->DM_D1A_DM_SPARE_2.Raw = 0x00000000;
    pIspReg->DM_D1A_DM_SPARE_3.Raw = 0x00000000;
    pIspReg->DM_D1A_DM_INT_CTL.Raw = 0x00000035;
    pIspReg->DM_D1A_DM_EE.Raw = 0x00000410;
}

void setC2G_D1_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_G2CX_D1_EN = 1;//enable bit    
    pIspReg->C2G_D1A_C2G_CONV_0A.Raw = 0x00000200;
    pIspReg->C2G_D1A_C2G_CONV_0B.Raw = 0x000002ce;
    pIspReg->C2G_D1A_C2G_CONV_1A.Raw = 0x07500200;
    pIspReg->C2G_D1A_C2G_CONV_1B.Raw = 0x00000692;
    pIspReg->C2G_D1A_C2G_CONV_2A.Raw = 0x038b0200;
    pIspReg->C2G_D1A_C2G_CONV_2B.Raw = 0x00000000;    
}

void setIGGM_D1_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_IGGM_D1_EN = 1;//enable bit
    pIspReg->IGGM_D1A_IGGM_LUT_RG[0].Raw = 0x2d8050e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[1].Raw = 0xa4602fd;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[2].Raw = 0xcab04d0;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[3].Raw = 0x25700e6;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[4].Raw = 0xa370f17;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[5].Raw = 0x38907f7;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[6].Raw = 0xccd0852;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[7].Raw = 0xacc02f6;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[8].Raw = 0x5a90021;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[9].Raw = 0x1b903a8;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[10].Raw = 0xae90422;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[11].Raw = 0x9e400a3;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[12].Raw = 0x2490568;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[13].Raw = 0x909054b;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[14].Raw = 0x47c0a5a;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[15].Raw = 0xbe406e9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[16].Raw = 0x5d60a69;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[17].Raw = 0x5cf02f5;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[18].Raw = 0xe420e48;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[19].Raw = 0x444024b;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[20].Raw = 0xf900000;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[21].Raw = 0x74d06bb;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[22].Raw = 0x26a0ad4;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[23].Raw = 0x29a0ca4;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[24].Raw = 0xa470f72;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[25].Raw = 0x18d0f92;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[26].Raw = 0xacd0338;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[27].Raw = 0x25d0b04;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[28].Raw = 0xe9d0352;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[29].Raw = 0x6f0fe2 ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[30].Raw = 0x8080648;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[31].Raw = 0x75801d0;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[32].Raw = 0xac8059f;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[33].Raw = 0xca10fba;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[34].Raw = 0x9cf0067;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[35].Raw = 0xa300e45;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[36].Raw = 0x2ff0a76;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[37].Raw = 0x5300e0a;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[38].Raw = 0x290b5e ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[39].Raw = 0x9930081;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[40].Raw = 0xfa60efd;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[41].Raw = 0x3a0edf ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[42].Raw = 0x3870741;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[43].Raw = 0xef906c9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[44].Raw = 0x5020fc9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[45].Raw = 0xc2f0e4e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[46].Raw = 0xba70f16;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[47].Raw = 0xdef0f92;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[48].Raw = 0x3560a42;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[49].Raw = 0x3690753;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[50].Raw = 0xcc102a2;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[51].Raw = 0x7210e5f;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[52].Raw = 0xa400052;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[53].Raw = 0xf4e0de1;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[54].Raw = 0xc8601f3;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[55].Raw = 0x62b067d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[56].Raw = 0xdd30f8e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[57].Raw = 0x7c60d70;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[58].Raw = 0x1630098;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[59].Raw = 0xe7708ef;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[60].Raw = 0x100418 ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[61].Raw = 0x2ad05b6;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[62].Raw = 0x82600bb;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[63].Raw = 0x6aa016d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[64].Raw = 0xe3100fc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[65].Raw = 0xbf905a0;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[66].Raw = 0x34f05b8;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[67].Raw = 0x4dc0200;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[68].Raw = 0x465073e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[69].Raw = 0x8470eb7;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[70].Raw = 0x40eb2  ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[71].Raw = 0x4ce017a;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[72].Raw = 0x68502fc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[73].Raw = 0x65d0906;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[74].Raw = 0xd1c0176;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[75].Raw = 0xc9e0572;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[76].Raw = 0x75c0299;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[77].Raw = 0x1c807a6;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[78].Raw = 0xa0f0a2d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[79].Raw = 0x53100c9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[80].Raw = 0x5930078;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[81].Raw = 0xc940683;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[82].Raw = 0xbd70602;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[83].Raw = 0x41c0fa1;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[84].Raw = 0x9b10a31;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[85].Raw = 0xe060eed;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[86].Raw = 0x1450155;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[87].Raw = 0x9af01e8;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[88].Raw = 0x24c0139;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[89].Raw = 0x5fa021f;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[90].Raw = 0xa910dcc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[91].Raw = 0xe6a0870;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[92].Raw = 0x4640c01;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[93].Raw = 0x62b076d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[94].Raw = 0xbf7013c;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[95].Raw = 0xc6a0a8e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[96].Raw = 0xde0ea8 ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[97].Raw = 0xb1c0fa3;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[98].Raw = 0xb6f0927;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[99].Raw = 0xad802ff;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[100].Raw = 0x3870793;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[101].Raw = 0x42003dc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[102].Raw = 0xc750698;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[103].Raw = 0x37a0ecc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[104].Raw = 0xfb9092e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[105].Raw = 0x4820946;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[106].Raw = 0x5bd0e79;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[107].Raw = 0x53d0747;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[108].Raw = 0x6e507d5;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[109].Raw = 0xc4e0d17;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[110].Raw = 0xb330c41;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[111].Raw = 0x140044d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[112].Raw = 0x35a08b7;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[113].Raw = 0x7ec06ac;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[114].Raw = 0x5880e42;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[115].Raw = 0x6000027;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[116].Raw = 0xc480c74;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[117].Raw = 0xcb70bf8;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[118].Raw = 0xaf80c05;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[119].Raw = 0x3d90cd3;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[120].Raw = 0x9c809c4;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[121].Raw = 0x33d0220;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[122].Raw = 0xbfa060a;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[123].Raw = 0x85005db;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[124].Raw = 0xa8802f2;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[125].Raw = 0x8150259;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[126].Raw = 0x4b0cc4 ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[127].Raw = 0x54b0fb1;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[128].Raw = 0x6d02a9 ;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[129].Raw = 0x3110413;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[130].Raw = 0xb290624;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[131].Raw = 0x10d0391;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[132].Raw = 0xde70a10;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[133].Raw = 0x49a01f5;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[134].Raw = 0xa78069f;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[135].Raw = 0x6e60591;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[136].Raw = 0xa0b0a10;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[137].Raw = 0xf8c0ddc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[138].Raw = 0xcf20488;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[139].Raw = 0xf8b05ef;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[140].Raw = 0x30906cf;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[141].Raw = 0xd5f0da6;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[142].Raw = 0x5c10263;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[143].Raw = 0xc620745;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[144].Raw = 0xea40a23;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[145].Raw = 0xacd0156;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[146].Raw = 0x24f0fe8;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[147].Raw = 0xadc0038;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[148].Raw = 0x37a0cbf;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[149].Raw = 0x13606bb;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[150].Raw = 0xf1a05db;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[151].Raw = 0xc0d08eb;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[152].Raw = 0xecf0eec;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[153].Raw = 0x3c10179;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[154].Raw = 0xa8f04fc;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[155].Raw = 0x69e0c1f;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[156].Raw = 0x2df0a70;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[157].Raw = 0x82505de;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[158].Raw = 0x81d0d44;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[159].Raw = 0xf7c0ddd;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[160].Raw = 0xec100b9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[161].Raw = 0x2c10226;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[162].Raw = 0xf6c00ac;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[163].Raw = 0x58301f4;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[164].Raw = 0x66d0e9b;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[165].Raw = 0xc0307e2;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[166].Raw = 0x3d70779;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[167].Raw = 0x61c05da;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[168].Raw = 0xd8d0c32;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[169].Raw = 0x26602af;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[170].Raw = 0xf8509d7;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[171].Raw = 0xc730309;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[172].Raw = 0xedf0445;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[173].Raw = 0xd2e0454;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[174].Raw = 0xb270dce;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[175].Raw = 0xcda0fde;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[176].Raw = 0x1f80f30;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[177].Raw = 0x88104a9;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[178].Raw = 0x1450f8e;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[179].Raw = 0xfb801af;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[180].Raw = 0xdad012b;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[181].Raw = 0x5b60cc5;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[182].Raw = 0x711007d;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[183].Raw = 0x2b9066c;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[184].Raw = 0x5a20c47;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[185].Raw = 0xa1309de;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[186].Raw = 0x9c10662;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[187].Raw = 0x299074b;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[188].Raw = 0x4200b72;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[189].Raw = 0xc150386;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[190].Raw = 0x2ef0250;
    pIspReg->IGGM_D1A_IGGM_LUT_RG[191].Raw = 0x9030b73;

    pIspReg->IGGM_D1A_IGGM_LUT_B[0].Raw = 0x3b3;
    pIspReg->IGGM_D1A_IGGM_LUT_B[1].Raw = 0xe77;
    pIspReg->IGGM_D1A_IGGM_LUT_B[2].Raw = 0x969;
    pIspReg->IGGM_D1A_IGGM_LUT_B[3].Raw = 0x872;
    pIspReg->IGGM_D1A_IGGM_LUT_B[4].Raw = 0x755;
    pIspReg->IGGM_D1A_IGGM_LUT_B[5].Raw = 0xd6e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[6].Raw = 0xa2f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[7].Raw = 0x9fa;
    pIspReg->IGGM_D1A_IGGM_LUT_B[8].Raw = 0x4f ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[9].Raw = 0xfee;
    pIspReg->IGGM_D1A_IGGM_LUT_B[10].Raw = 0xa53;
    pIspReg->IGGM_D1A_IGGM_LUT_B[11].Raw = 0x936;
    pIspReg->IGGM_D1A_IGGM_LUT_B[12].Raw = 0xa3 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[13].Raw = 0xf3d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[14].Raw = 0x6bb;
    pIspReg->IGGM_D1A_IGGM_LUT_B[15].Raw = 0xca0;
    pIspReg->IGGM_D1A_IGGM_LUT_B[16].Raw = 0xb0a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[17].Raw = 0x565;
    pIspReg->IGGM_D1A_IGGM_LUT_B[18].Raw = 0x52f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[19].Raw = 0x5dd;
    pIspReg->IGGM_D1A_IGGM_LUT_B[20].Raw = 0x13 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[21].Raw = 0x404;
    pIspReg->IGGM_D1A_IGGM_LUT_B[22].Raw = 0x99a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[23].Raw = 0x72a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[24].Raw = 0xaf9;
    pIspReg->IGGM_D1A_IGGM_LUT_B[25].Raw = 0x8ef;
    pIspReg->IGGM_D1A_IGGM_LUT_B[26].Raw = 0xdec;
    pIspReg->IGGM_D1A_IGGM_LUT_B[27].Raw = 0xa6c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[28].Raw = 0x123;
    pIspReg->IGGM_D1A_IGGM_LUT_B[29].Raw = 0x852;
    pIspReg->IGGM_D1A_IGGM_LUT_B[30].Raw = 0xd7a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[31].Raw = 0xd29;
    pIspReg->IGGM_D1A_IGGM_LUT_B[32].Raw = 0xa18;
    pIspReg->IGGM_D1A_IGGM_LUT_B[33].Raw = 0xe77;
    pIspReg->IGGM_D1A_IGGM_LUT_B[34].Raw = 0x7fa;
    pIspReg->IGGM_D1A_IGGM_LUT_B[35].Raw = 0x19b;
    pIspReg->IGGM_D1A_IGGM_LUT_B[36].Raw = 0x1ec;
    pIspReg->IGGM_D1A_IGGM_LUT_B[37].Raw = 0x885;
    pIspReg->IGGM_D1A_IGGM_LUT_B[38].Raw = 0x82e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[39].Raw = 0x62e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[40].Raw = 0xc7c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[41].Raw = 0xc76;
    pIspReg->IGGM_D1A_IGGM_LUT_B[42].Raw = 0xbd5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[43].Raw = 0xcb7;
    pIspReg->IGGM_D1A_IGGM_LUT_B[44].Raw = 0x607;
    pIspReg->IGGM_D1A_IGGM_LUT_B[45].Raw = 0x7b9;
    pIspReg->IGGM_D1A_IGGM_LUT_B[46].Raw = 0x5c5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[47].Raw = 0xab8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[48].Raw = 0x39f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[49].Raw = 0xde1;
    pIspReg->IGGM_D1A_IGGM_LUT_B[50].Raw = 0xbfa;
    pIspReg->IGGM_D1A_IGGM_LUT_B[51].Raw = 0x3f5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[52].Raw = 0x54b;
    pIspReg->IGGM_D1A_IGGM_LUT_B[53].Raw = 0xa48;
    pIspReg->IGGM_D1A_IGGM_LUT_B[54].Raw = 0x734;
    pIspReg->IGGM_D1A_IGGM_LUT_B[55].Raw = 0xb6d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[56].Raw = 0x332;
    pIspReg->IGGM_D1A_IGGM_LUT_B[57].Raw = 0x724;
    pIspReg->IGGM_D1A_IGGM_LUT_B[58].Raw = 0xbbf;
    pIspReg->IGGM_D1A_IGGM_LUT_B[59].Raw = 0xa6d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[60].Raw = 0xbca;
    pIspReg->IGGM_D1A_IGGM_LUT_B[61].Raw = 0x73e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[62].Raw = 0xe37;
    pIspReg->IGGM_D1A_IGGM_LUT_B[63].Raw = 0x638;
    pIspReg->IGGM_D1A_IGGM_LUT_B[64].Raw = 0x612;
    pIspReg->IGGM_D1A_IGGM_LUT_B[65].Raw = 0x5ab;
    pIspReg->IGGM_D1A_IGGM_LUT_B[66].Raw = 0xd66;
    pIspReg->IGGM_D1A_IGGM_LUT_B[67].Raw = 0x91d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[68].Raw = 0xbcc;
    pIspReg->IGGM_D1A_IGGM_LUT_B[69].Raw = 0xdb1;
    pIspReg->IGGM_D1A_IGGM_LUT_B[70].Raw = 0xb03;
    pIspReg->IGGM_D1A_IGGM_LUT_B[71].Raw = 0x94d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[72].Raw = 0xc14;
    pIspReg->IGGM_D1A_IGGM_LUT_B[73].Raw = 0x8b4;
    pIspReg->IGGM_D1A_IGGM_LUT_B[74].Raw = 0x1e2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[75].Raw = 0xdfc;
    pIspReg->IGGM_D1A_IGGM_LUT_B[76].Raw = 0x5  ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[77].Raw = 0xecf;
    pIspReg->IGGM_D1A_IGGM_LUT_B[78].Raw = 0xaf4;
    pIspReg->IGGM_D1A_IGGM_LUT_B[79].Raw = 0x7fe;
    pIspReg->IGGM_D1A_IGGM_LUT_B[80].Raw = 0x2e9;
    pIspReg->IGGM_D1A_IGGM_LUT_B[81].Raw = 0xed2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[82].Raw = 0xb71;
    pIspReg->IGGM_D1A_IGGM_LUT_B[83].Raw = 0x51f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[84].Raw = 0x574;
    pIspReg->IGGM_D1A_IGGM_LUT_B[85].Raw = 0x9a5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[86].Raw = 0x5d8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[87].Raw = 0x88f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[88].Raw = 0x32c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[89].Raw = 0xa1e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[90].Raw = 0x342;
    pIspReg->IGGM_D1A_IGGM_LUT_B[91].Raw = 0x8bc;
    pIspReg->IGGM_D1A_IGGM_LUT_B[92].Raw = 0x89f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[93].Raw = 0x44b;
    pIspReg->IGGM_D1A_IGGM_LUT_B[94].Raw = 0x2  ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[95].Raw = 0x10 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[96].Raw = 0x39d;
    pIspReg->IGGM_D1A_IGGM_LUT_B[97].Raw = 0x9d2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[98].Raw = 0x7e8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[99].Raw = 0xc02;
    pIspReg->IGGM_D1A_IGGM_LUT_B[100].Raw = 0xae8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[101].Raw = 0x60b;
    pIspReg->IGGM_D1A_IGGM_LUT_B[102].Raw = 0xc18;
    pIspReg->IGGM_D1A_IGGM_LUT_B[103].Raw = 0xe0 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[104].Raw = 0x762;
    pIspReg->IGGM_D1A_IGGM_LUT_B[105].Raw = 0xc64;
    pIspReg->IGGM_D1A_IGGM_LUT_B[106].Raw = 0xf51;
    pIspReg->IGGM_D1A_IGGM_LUT_B[107].Raw = 0x2ec;
    pIspReg->IGGM_D1A_IGGM_LUT_B[108].Raw = 0x215;
    pIspReg->IGGM_D1A_IGGM_LUT_B[109].Raw = 0x5b2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[110].Raw = 0xea3;
    pIspReg->IGGM_D1A_IGGM_LUT_B[111].Raw = 0xd41;
    pIspReg->IGGM_D1A_IGGM_LUT_B[112].Raw = 0x536;
    pIspReg->IGGM_D1A_IGGM_LUT_B[113].Raw = 0xccf;
    pIspReg->IGGM_D1A_IGGM_LUT_B[114].Raw = 0x7e7;
    pIspReg->IGGM_D1A_IGGM_LUT_B[115].Raw = 0x195;
    pIspReg->IGGM_D1A_IGGM_LUT_B[116].Raw = 0x2ab;
    pIspReg->IGGM_D1A_IGGM_LUT_B[117].Raw = 0xbd1;
    pIspReg->IGGM_D1A_IGGM_LUT_B[118].Raw = 0x7b ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[119].Raw = 0x2ba;
    pIspReg->IGGM_D1A_IGGM_LUT_B[120].Raw = 0x5b7;
    pIspReg->IGGM_D1A_IGGM_LUT_B[121].Raw = 0xec5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[122].Raw = 0x19a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[123].Raw = 0xb7e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[124].Raw = 0xcab;
    pIspReg->IGGM_D1A_IGGM_LUT_B[125].Raw = 0x98c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[126].Raw = 0x8ba;
    pIspReg->IGGM_D1A_IGGM_LUT_B[127].Raw = 0x6b7;
    pIspReg->IGGM_D1A_IGGM_LUT_B[128].Raw = 0xca4;
    pIspReg->IGGM_D1A_IGGM_LUT_B[129].Raw = 0x6d8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[130].Raw = 0x7d2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[131].Raw = 0xdd1;
    pIspReg->IGGM_D1A_IGGM_LUT_B[132].Raw = 0xa65;
    pIspReg->IGGM_D1A_IGGM_LUT_B[133].Raw = 0x20e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[134].Raw = 0x38c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[135].Raw = 0x57a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[136].Raw = 0x2cf;
    pIspReg->IGGM_D1A_IGGM_LUT_B[137].Raw = 0x1d8;
    pIspReg->IGGM_D1A_IGGM_LUT_B[138].Raw = 0x8f1;
    pIspReg->IGGM_D1A_IGGM_LUT_B[139].Raw = 0x937;
    pIspReg->IGGM_D1A_IGGM_LUT_B[140].Raw = 0x2bf;
    pIspReg->IGGM_D1A_IGGM_LUT_B[141].Raw = 0xca6;
    pIspReg->IGGM_D1A_IGGM_LUT_B[142].Raw = 0x966;
    pIspReg->IGGM_D1A_IGGM_LUT_B[143].Raw = 0x7f2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[144].Raw = 0x2ba;
    pIspReg->IGGM_D1A_IGGM_LUT_B[145].Raw = 0xb9a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[146].Raw = 0x39c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[147].Raw = 0xc93;
    pIspReg->IGGM_D1A_IGGM_LUT_B[148].Raw = 0xf92;
    pIspReg->IGGM_D1A_IGGM_LUT_B[149].Raw = 0x32b;
    pIspReg->IGGM_D1A_IGGM_LUT_B[150].Raw = 0x305;
    pIspReg->IGGM_D1A_IGGM_LUT_B[151].Raw = 0xccc;
    pIspReg->IGGM_D1A_IGGM_LUT_B[152].Raw = 0xa16;
    pIspReg->IGGM_D1A_IGGM_LUT_B[153].Raw = 0xc4e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[154].Raw = 0x6c4;
    pIspReg->IGGM_D1A_IGGM_LUT_B[155].Raw = 0x379;
    pIspReg->IGGM_D1A_IGGM_LUT_B[156].Raw = 0x46e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[157].Raw = 0x61f;
    pIspReg->IGGM_D1A_IGGM_LUT_B[158].Raw = 0x865;
    pIspReg->IGGM_D1A_IGGM_LUT_B[159].Raw = 0x945;
    pIspReg->IGGM_D1A_IGGM_LUT_B[160].Raw = 0xa27;
    pIspReg->IGGM_D1A_IGGM_LUT_B[161].Raw = 0x943;
    pIspReg->IGGM_D1A_IGGM_LUT_B[162].Raw = 0x15e;
    pIspReg->IGGM_D1A_IGGM_LUT_B[163].Raw = 0x3c3;
    pIspReg->IGGM_D1A_IGGM_LUT_B[164].Raw = 0xe93;
    pIspReg->IGGM_D1A_IGGM_LUT_B[165].Raw = 0x859;
    pIspReg->IGGM_D1A_IGGM_LUT_B[166].Raw = 0x80a;
    pIspReg->IGGM_D1A_IGGM_LUT_B[167].Raw = 0x11c;
    pIspReg->IGGM_D1A_IGGM_LUT_B[168].Raw = 0xf82;
    pIspReg->IGGM_D1A_IGGM_LUT_B[169].Raw = 0xfea;
    pIspReg->IGGM_D1A_IGGM_LUT_B[170].Raw = 0xb87;
    pIspReg->IGGM_D1A_IGGM_LUT_B[171].Raw = 0x463;
    pIspReg->IGGM_D1A_IGGM_LUT_B[172].Raw = 0x6a2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[173].Raw = 0x115;
    pIspReg->IGGM_D1A_IGGM_LUT_B[174].Raw = 0x945;
    pIspReg->IGGM_D1A_IGGM_LUT_B[175].Raw = 0x952;
    pIspReg->IGGM_D1A_IGGM_LUT_B[176].Raw = 0x97 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[177].Raw = 0x2a2;
    pIspReg->IGGM_D1A_IGGM_LUT_B[178].Raw = 0x2a5;
    pIspReg->IGGM_D1A_IGGM_LUT_B[179].Raw = 0x7c0;
    pIspReg->IGGM_D1A_IGGM_LUT_B[180].Raw = 0x359;
    pIspReg->IGGM_D1A_IGGM_LUT_B[181].Raw = 0x281;
    pIspReg->IGGM_D1A_IGGM_LUT_B[182].Raw = 0xf89;
    pIspReg->IGGM_D1A_IGGM_LUT_B[183].Raw = 0x614;
    pIspReg->IGGM_D1A_IGGM_LUT_B[184].Raw = 0x862;
    pIspReg->IGGM_D1A_IGGM_LUT_B[185].Raw = 0x4f ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[186].Raw = 0xf69;
    pIspReg->IGGM_D1A_IGGM_LUT_B[187].Raw = 0xd84;
    pIspReg->IGGM_D1A_IGGM_LUT_B[188].Raw = 0xb30;
    pIspReg->IGGM_D1A_IGGM_LUT_B[189].Raw = 0x64 ;
    pIspReg->IGGM_D1A_IGGM_LUT_B[190].Raw = 0xe74;
    pIspReg->IGGM_D1A_IGGM_LUT_B[191].Raw = 0xfd7;
    
    pIspReg->IGGM_D1A_IGGM_CTRL.Raw = 0x1FFF;

}


void setYNR_D1_UT(dip_x_reg_t* pIspReg){

}



void setCNR_D1_UT(dip_x_reg_t* pIspReg){
    pIspReg->DIPCTL_D1A_DIPCTL_YUV_EN1.Bits.DIPCTL_CNR_D1_EN = 1;//enable bit
    
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_CNR_SL2_LINK = 1;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_TILE_EDGE = 15;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_MODE = 0;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_CNR_VER_C_REF_Y = 1;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_CNR_SCALE_MODE = 3;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_LBIT_MODE = 0;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_VIDEO_MODE = 0;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_BPC_EN = 1;
    pIspReg->CNR_D1A_CNR_CNR_CON1.Bits.CNR_CNR_ENC = 1;
    pIspReg->CNR_D1A_CNR_CNR_CON2.Bits.CNR_CNR_FLT_C = 0;
    pIspReg->CNR_D1A_CNR_CNR_CON2.Bits.CNR_CNR_C_SM_EDGE = 1;
    pIspReg->CNR_D1A_CNR_CNR_YAD1.Bits.CNR_CNR_K_TH_C = 8;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT1.Bits.CNR_CNR_Y_CPX3 = 160;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT1.Bits.CNR_CNR_Y_CPX2 = 100;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT1.Bits.CNR_CNR_Y_CPX1 = 40;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT2.Bits.CNR_CNR_Y_SCALE_CPY3 = 16;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT2.Bits.CNR_CNR_Y_SCALE_CPY2 = 16;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT2.Bits.CNR_CNR_Y_SCALE_CPY1 = 16;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT2.Bits.CNR_CNR_Y_SCALE_CPY0 = 16;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT3.Bits.CNR_CNR_Y_SCALE_SP3 = -8;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT3.Bits.CNR_CNR_Y_SCALE_SP2 = 0;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT3.Bits.CNR_CNR_Y_SCALE_SP1 = 0;
    pIspReg->CNR_D1A_CNR_CNR_Y4LUT3.Bits.CNR_CNR_Y_SCALE_SP0 = 0;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT1.Bits.CNR_CNR_SL2_X3 = 192;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT1.Bits.CNR_CNR_SL2_X2 = 128;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT1.Bits.CNR_CNR_SL2_X1 = 64;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT2.Bits.CNR_CNR_SL2_GAIN3 = 25;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT2.Bits.CNR_CNR_SL2_GAIN2 = 22;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT2.Bits.CNR_CNR_SL2_GAIN1 = 18;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT2.Bits.CNR_CNR_SL2_GAIN0 = 16;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT3.Bits.CNR_CNR_SL2_SP3 = 4;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT3.Bits.CNR_CNR_SL2_SP2 = 6;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT3.Bits.CNR_CNR_SL2_SP1 = 8;
    pIspReg->CNR_D1A_CNR_CNR_L4LUT3.Bits.CNR_CNR_SL2_SP0 = 4;
    pIspReg->CNR_D1A_CNR_CNR_CAD.Bits.CNR_CNR_C_MODE = 0;
    pIspReg->CNR_D1A_CNR_CNR_CAD.Bits.CNR_CNR_C_L_DIFF_TH = 37;
    pIspReg->CNR_D1A_CNR_CNR_CAD.Bits.CNR_CNR_PTC_GAIN_TH = 16;
    pIspReg->CNR_D1A_CNR_CNR_CAD.Bits.CNR_CNR_C_GAIN = 8;
    pIspReg->CNR_D1A_CNR_CNR_CB_VRNG.Bits.CNR_CNR_CB_V_RNG4 = 19;
    pIspReg->CNR_D1A_CNR_CNR_CB_VRNG.Bits.CNR_CNR_CB_V_RNG3 = 14;
    pIspReg->CNR_D1A_CNR_CNR_CB_VRNG.Bits.CNR_CNR_CB_V_RNG2 = 10;
    pIspReg->CNR_D1A_CNR_CNR_CB_VRNG.Bits.CNR_CNR_CB_V_RNG1 = 5;
    pIspReg->CNR_D1A_CNR_CNR_CB_HRNG.Bits.CNR_CNR_CB_H_RNG4 = 15;
    pIspReg->CNR_D1A_CNR_CNR_CB_HRNG.Bits.CNR_CNR_CB_H_RNG3 = 11;
    pIspReg->CNR_D1A_CNR_CNR_CB_HRNG.Bits.CNR_CNR_CB_H_RNG2 = 8;
    pIspReg->CNR_D1A_CNR_CNR_CB_HRNG.Bits.CNR_CNR_CB_H_RNG1 = 4;
    pIspReg->CNR_D1A_CNR_CNR_CR_VRNG.Bits.CNR_CNR_CR_V_RNG4 = 19;
    pIspReg->CNR_D1A_CNR_CNR_CR_VRNG.Bits.CNR_CNR_CR_V_RNG3 = 14;
    pIspReg->CNR_D1A_CNR_CNR_CR_VRNG.Bits.CNR_CNR_CR_V_RNG2 = 10;
    pIspReg->CNR_D1A_CNR_CNR_CR_VRNG.Bits.CNR_CNR_CR_V_RNG1 = 5;
    pIspReg->CNR_D1A_CNR_CNR_CR_HRNG.Bits.CNR_CNR_CR_H_RNG4 = 15;
    pIspReg->CNR_D1A_CNR_CNR_CR_HRNG.Bits.CNR_CNR_CR_H_RNG3 = 11;
    pIspReg->CNR_D1A_CNR_CNR_CR_HRNG.Bits.CNR_CNR_CR_H_RNG2 = 8;
    pIspReg->CNR_D1A_CNR_CNR_CR_HRNG.Bits.CNR_CNR_CR_H_RNG1 = 4;
    pIspReg->CNR_D1A_CNR_CNR_SL2.Bits.CNR_BPC_LM_WT = 8;
    pIspReg->CNR_D1A_CNR_CNR_SL2.Bits.CNR_CNR_SL2_C_GAIN = 4;
    pIspReg->CNR_D1A_CNR_CNR_MED1.Bits.CNR_BPC_LCL_TH = 36;
    pIspReg->CNR_D1A_CNR_CNR_MED1.Bits.CNR_BPC_MCD_SL = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED1.Bits.CNR_BPC_MCD_TH = 5;
    pIspReg->CNR_D1A_CNR_CNR_MED1.Bits.CNR_BPC_COR_SL = 4;
    pIspReg->CNR_D1A_CNR_CNR_MED1.Bits.CNR_BPC_COR_TH = 5;
    pIspReg->CNR_D1A_CNR_CNR_MED2.Bits.CNR_BPC_SCL_LV = 9;
    pIspReg->CNR_D1A_CNR_CNR_MED2.Bits.CNR_BPC_SCL_SL = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED2.Bits.CNR_BPC_SCL_TH = 4;
    pIspReg->CNR_D1A_CNR_CNR_MED2.Bits.CNR_BPC_LCL_LV = 12;
    pIspReg->CNR_D1A_CNR_CNR_MED2.Bits.CNR_BPC_LCL_SL = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED3.Bits.CNR_BPC_Y0 = 11;
    pIspReg->CNR_D1A_CNR_CNR_MED3.Bits.CNR_BPC_VAR = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED3.Bits.CNR_BPC_NCL_LV = 9;
    pIspReg->CNR_D1A_CNR_CNR_MED3.Bits.CNR_BPC_NCL_SL = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED3.Bits.CNR_BPC_NCL_TH = 59;
    pIspReg->CNR_D1A_CNR_CNR_MED4.Bits.CNR_BPC_Y4 = 25;
    pIspReg->CNR_D1A_CNR_CNR_MED4.Bits.CNR_BPC_Y3 = 23;
    pIspReg->CNR_D1A_CNR_CNR_MED4.Bits.CNR_BPC_Y2 = 19;
    pIspReg->CNR_D1A_CNR_CNR_MED4.Bits.CNR_BPC_Y1 = 15;
    pIspReg->CNR_D1A_CNR_CNR_MED5.Bits.CNR_BPC_NCL_OFT = 0;
    pIspReg->CNR_D1A_CNR_CNR_MED5.Bits.CNR_BPC_SCL_OFT = 0;
    pIspReg->CNR_D1A_CNR_CNR_MED5.Bits.CNR_BPC_LCL_OFT = 0;
    pIspReg->CNR_D1A_CNR_CNR_MED6.Bits.CNR_BPC_DK_Y4 = 25;
    pIspReg->CNR_D1A_CNR_CNR_MED6.Bits.CNR_BPC_DK_Y3 = 23;
    pIspReg->CNR_D1A_CNR_CNR_MED6.Bits.CNR_BPC_DK_Y2 = 19;
    pIspReg->CNR_D1A_CNR_CNR_MED6.Bits.CNR_BPC_DK_Y1 = 15;
    pIspReg->CNR_D1A_CNR_CNR_MED7.Bits.CNR_BPC_DK_Y0 = 11;
    pIspReg->CNR_D1A_CNR_CNR_MED7.Bits.CNR_BPC_NCL_LV_CEN = 9;
    pIspReg->CNR_D1A_CNR_CNR_MED7.Bits.CNR_BPC_SCL_LV_CEN = 9;
    pIspReg->CNR_D1A_CNR_CNR_MED7.Bits.CNR_BPC_LCL_LV_CEN = 12;
    pIspReg->CNR_D1A_CNR_CNR_MED8.Bits.CNR_BPC_BND_TH_EDGE = 26;
    pIspReg->CNR_D1A_CNR_CNR_MED8.Bits.CNR_BPC_BND_TH_HI = 26;
    pIspReg->CNR_D1A_CNR_CNR_MED8.Bits.CNR_BPC_BND_TH_LO = 26;
    pIspReg->CNR_D1A_CNR_CNR_MED8.Bits.CNR_BPC_BND_SL = 2;
    pIspReg->CNR_D1A_CNR_CNR_MED8.Bits.CNR_BPC_OUT_TH = 0;
    pIspReg->CNR_D1A_CNR_CNR_MED9.Bits.CNR_BPC_EDG_TH = 27;
    pIspReg->CNR_D1A_CNR_CNR_MED9.Bits.CNR_BPC_EDG_SL = 0;
    pIspReg->CNR_D1A_CNR_CNR_MED9.Bits.CNR_BPC_LINE_X0 = 64;
    pIspReg->CNR_D1A_CNR_CNR_MED9.Bits.CNR_BPC_LINE_TH0 = 50;
    pIspReg->CNR_D1A_CNR_CNR_MED9.Bits.CNR_BPC_LINE_TH1 = 255;
    pIspReg->CNR_D1A_CNR_CNR_MED10.Bits.CNR_BPC_LINE_SL0 = 40;
    pIspReg->CNR_D1A_CNR_CNR_MED10.Bits.CNR_BPC_LINE_TH_SL = 3;
    pIspReg->CNR_D1A_CNR_CNR_MED11.Bits.CNR_BPC_SL2_LINK = 1;
    pIspReg->CNR_D1A_CNR_CNR_MED11.Bits.CNR_BPC_SL2_X1 = 64;
    pIspReg->CNR_D1A_CNR_CNR_MED11.Bits.CNR_BPC_SL2_X2 = 128;
    pIspReg->CNR_D1A_CNR_CNR_MED11.Bits.CNR_BPC_SL2_X3 = 192;
    pIspReg->CNR_D1A_CNR_CNR_MED12.Bits.CNR_BPC_SL2_GAIN0 = 16;
    pIspReg->CNR_D1A_CNR_CNR_MED12.Bits.CNR_BPC_SL2_GAIN1 = 16;
    pIspReg->CNR_D1A_CNR_CNR_MED12.Bits.CNR_BPC_SL2_GAIN2 = 16;
    pIspReg->CNR_D1A_CNR_CNR_MED12.Bits.CNR_BPC_SL2_GAIN3 = 16;
    pIspReg->CNR_D1A_CNR_CNR_MED13.Bits.CNR_BPC_SL2_SP0 = 4;
    pIspReg->CNR_D1A_CNR_CNR_MED13.Bits.CNR_BPC_SL2_SP1 = 8;
    pIspReg->CNR_D1A_CNR_CNR_MED13.Bits.CNR_BPC_SL2_SP2 = 6;
    pIspReg->CNR_D1A_CNR_CNR_MED13.Bits.CNR_BPC_SL2_SP3 = 4;
    pIspReg->CNR_D1A_CNR_CNR_ACTC.Bits.CNR_CNR_C_DITH_V = 0;
    pIspReg->CNR_D1A_CNR_CNR_ACTC.Bits.CNR_CNR_C_DITH_U = 0;
    pIspReg->CNR_D1A_CNR_CNR_ACTC.Bits.CNR_CNR_ACT_BLD_BASE_C = 64;
    pIspReg->CNR_D1A_CNR_CNR_ACTC.Bits.CNR_CNR_RSV = 0;
    pIspReg->CNR_D1A_CNR_CNR_RSV1.Bits.CNR_CNR_RSV1 = 0;
    pIspReg->CNR_D1A_CNR_CNR_RSV2.Bits.CNR_CNR_RSV2 = 2147483647;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_Y_CPX3 = 255;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_UV_GAIN2 = 0;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_UV_GAIN_MODE = 1;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_OR_MODE = 0;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_SL2_MODE = 0;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_SL2_LINK = 1;
    pIspReg->CNR_D1A_CNR_CCR_CON.Bits.CNR_CCR_EN = 0;
    pIspReg->CNR_D1A_CNR_CCR_YLUT.Bits.CNR_CCR_Y_CPY1 = 16;
    pIspReg->CNR_D1A_CNR_CCR_YLUT.Bits.CNR_CCR_Y_SP1 = 24;
    pIspReg->CNR_D1A_CNR_CCR_YLUT.Bits.CNR_CCR_Y_CPX2 = 96;
    pIspReg->CNR_D1A_CNR_CCR_YLUT.Bits.CNR_CCR_Y_CPX1 = 32;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT.Bits.CNR_CCR_UV_GAIN1 = 64;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT.Bits.CNR_CCR_UV_X3 = 255;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT.Bits.CNR_CCR_UV_X2 = 37;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT.Bits.CNR_CCR_UV_X1 = 5;
    pIspReg->CNR_D1A_CNR_CCR_YLUT2.Bits.CNR_CCR_Y_CPY2 = 64;
    pIspReg->CNR_D1A_CNR_CCR_YLUT2.Bits.CNR_CCR_Y_CPY0 = 0;
    pIspReg->CNR_D1A_CNR_CCR_YLUT2.Bits.CNR_CCR_Y_SP2 = 0;
    pIspReg->CNR_D1A_CNR_CCR_YLUT2.Bits.CNR_CCR_Y_SP0 = 16;
    pIspReg->CNR_D1A_CNR_CCR_SAT_CTRL.Bits.CNR_CCR_CEN_V = 0;
    pIspReg->CNR_D1A_CNR_CCR_SAT_CTRL.Bits.CNR_CCR_CEN_U = 0;
    pIspReg->CNR_D1A_CNR_CCR_SAT_CTRL.Bits.CNR_CCR_MODE = 1;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT_SP.Bits.CNR_CCR_UV_GAIN_SP2 = 0;
    pIspReg->CNR_D1A_CNR_CCR_UVLUT_SP.Bits.CNR_CCR_UV_GAIN_SP1 = 0;
    pIspReg->CNR_D1A_CNR_CCR_HUE1.Bits.CNR_CCR_HUE_X2 = 16;
    pIspReg->CNR_D1A_CNR_CCR_HUE1.Bits.CNR_CCR_HUE_X1 = 0;
    pIspReg->CNR_D1A_CNR_CCR_HUE2.Bits.CNR_CCR_HUE_X4 = 286;
    pIspReg->CNR_D1A_CNR_CCR_HUE2.Bits.CNR_CCR_HUE_X3 = 270;
    pIspReg->CNR_D1A_CNR_CCR_HUE3.Bits.CNR_CCR_HUE_GAIN2 = 0;
    pIspReg->CNR_D1A_CNR_CCR_HUE3.Bits.CNR_CCR_HUE_GAIN1 = 64;
    pIspReg->CNR_D1A_CNR_CCR_HUE3.Bits.CNR_CCR_HUE_SP2 = -128;
    pIspReg->CNR_D1A_CNR_CCR_HUE3.Bits.CNR_CCR_HUE_SP1 = -127;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT1.Bits.CNR_CCR_SL2_X3 = 192;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT1.Bits.CNR_CCR_SL2_X2 = 128;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT1.Bits.CNR_CCR_SL2_X1 = 64;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT2.Bits.CNR_CCR_SL2_GAIN3 = 32;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT2.Bits.CNR_CCR_SL2_GAIN2 = 24;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT2.Bits.CNR_CCR_SL2_GAIN1 = 20;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT2.Bits.CNR_CCR_SL2_GAIN0 = 16;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT3.Bits.CNR_CCR_SL2_SP3 = 16;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT3.Bits.CNR_CCR_SL2_SP2 = 16;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT3.Bits.CNR_CCR_SL2_SP1 = 8;
    pIspReg->CNR_D1A_CNR_CCR_L4LUT3.Bits.CNR_CCR_SL2_SP0 = 8;
    pIspReg->CNR_D1A_CNR_ABF_CON1.Bits.CNR_ABF_NSR_IDX = 1;
    pIspReg->CNR_D1A_CNR_ABF_CON1.Bits.CNR_ABF_BIL_IDX = 2;
    pIspReg->CNR_D1A_CNR_ABF_CON1.Bits.CNR_ABF_EN = 0;
    pIspReg->CNR_D1A_CNR_ABF_CON2.Bits.CNR_ABF_BF_U_OFST = 20;
    pIspReg->CNR_D1A_CNR_ABF_RCON.Bits.CNR_ABF_R2 = -190;
    pIspReg->CNR_D1A_CNR_ABF_RCON.Bits.CNR_ABF_R1 = 190;
    pIspReg->CNR_D1A_CNR_ABF_YLUT.Bits.CNR_ABF_Y3 = 252;
    pIspReg->CNR_D1A_CNR_ABF_YLUT.Bits.CNR_ABF_Y2 = 236;
    pIspReg->CNR_D1A_CNR_ABF_YLUT.Bits.CNR_ABF_Y1 = 1;
    pIspReg->CNR_D1A_CNR_ABF_YLUT.Bits.CNR_ABF_Y0 = 0;
    pIspReg->CNR_D1A_CNR_ABF_CXLUT.Bits.CNR_ABF_CX3 = 210;
    pIspReg->CNR_D1A_CNR_ABF_CXLUT.Bits.CNR_ABF_CX2 = 196;
    pIspReg->CNR_D1A_CNR_ABF_CXLUT.Bits.CNR_ABF_CX1 = 108;
    pIspReg->CNR_D1A_CNR_ABF_CXLUT.Bits.CNR_ABF_CX0 = 96;
    pIspReg->CNR_D1A_CNR_ABF_CYLUT.Bits.CNR_ABF_CY3 = 205;
    pIspReg->CNR_D1A_CNR_ABF_CYLUT.Bits.CNR_ABF_CY2 = 184;
    pIspReg->CNR_D1A_CNR_ABF_CYLUT.Bits.CNR_ABF_CY1 = 130;
    pIspReg->CNR_D1A_CNR_ABF_CYLUT.Bits.CNR_ABF_CY0 = 122;
    pIspReg->CNR_D1A_CNR_ABF_YSP.Bits.CNR_ABF_Y_SP1 = -8;
    pIspReg->CNR_D1A_CNR_ABF_YSP.Bits.CNR_ABF_Y_SP0 = 511;
    pIspReg->CNR_D1A_CNR_ABF_CXSP.Bits.CNR_ABF_CX_SP1 = -36;
    pIspReg->CNR_D1A_CNR_ABF_CXSP.Bits.CNR_ABF_CX_SP0 = 42;
    pIspReg->CNR_D1A_CNR_ABF_CYSP.Bits.CNR_ABF_CY_SP1 = -24;
    pIspReg->CNR_D1A_CNR_ABF_CYSP.Bits.CNR_ABF_CY_SP0 = 64;
    pIspReg->CNR_D1A_CNR_ABF_CLP.Bits.CNR_ABF_STHRE_B = 230;
    pIspReg->CNR_D1A_CNR_ABF_CLP.Bits.CNR_ABF_STHRE_G = 230;
    pIspReg->CNR_D1A_CNR_ABF_CLP.Bits.CNR_ABF_STHRE_R = 230;
    pIspReg->CNR_D1A_CNR_ABF_RSV1.Bits.CNR_ABF_RSV = 0;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLUR_MUL = 1;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLUR_SHIFT = 0;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLUR_OFFSET = 2;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLUR_MAX = 12;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLEND_START = 1;
    pIspReg->CNR_D1A_CNR_BOK_TUN.Bits.CNR_BOK_BLEND_SLOPE = 8;
}




void SetDefaultTuning(dip_x_reg_t* pIspReg, MUINT32* tuningBuf, tuning_tag tag, int enFgMode, UFDG_META_INFO* pUfdParam=NULL)
{
	printf("SetDefaultTuning_JW\n");
	MUINT32 fgModeRegBit_ufd = (enFgMode&0x01);   //TODO: Justin, setUfd
	MUINT32 fgModeRegBit_udm = (enFgMode&0x01) << 10;   //TODO: Justin, setUdm

    switch(tag)
    {
        case tuning_tag_P2A:
            setDM_D1_UT(pIspReg,enFgMode);
            setCCM_D1_UT(pIspReg);
            setGGM_D1_UT(pIspReg);
            setG2CX_D1_UT(pIspReg);
            setC2G_D1_UT(pIspReg);
            setIGGM_D1_UT(pIspReg);
            setGGM_D3_UT(pIspReg);
        break;
        case tuning_tag_DFE:
            setDM_D1_UT(pIspReg,enFgMode);
            setCCM_D1_UT(pIspReg);
            setGGM_D1_UT(pIspReg);
            setG2CX_D1_UT(pIspReg);
            setDFE_D1_UT(pIspReg);
        break;
        case tuning_tag_P2B:
            setG2CX_D1_UT(pIspReg);  //G2C_D1
            setC2G_D1_UT(pIspReg);
            setIGGM_D1_UT(pIspReg);
            setGGM_D3_UT(pIspReg);
        break;
        case tuning_tag_Mixing:
            setG2CX_D1_UT(pIspReg);  //G2C_D1
            setC2G_D1_UT(pIspReg);
            setIGGM_D1_UT(pIspReg);
            setGGM_D3_UT(pIspReg);
        break;
        default:            
	//setUnp
	setUNP_D1_UT(pIspReg);

	//setUfd
	setUFD_D1_UT(pIspReg,fgModeRegBit_ufd,pUfdParam);

	//setUdm = setDM (ISP60)
	setUDM_D1_UT(pIspReg,fgModeRegBit_udm);

	//setG2g = setCCM(ISP60)
	setCCM_D1_UT(pIspReg);

	//setG2g2 = setCCM2(ISP60)
	setCCM_D2_UT(pIspReg);

	//setLCE
	setLCE_D1_UT(pIspReg);

	//setGgm
	setGGM_D1_UT(pIspReg);

	setGGM_D2_UT(pIspReg);
	setC24_D1_UT(pIspReg);
	setC02_D2_UT(pIspReg);
	setG2C_D1_UT(pIspReg);
	setG2CX_D1_UT(pIspReg);
	setC42_D2_UT(pIspReg);
	setSRZ_D1_UT(pIspReg);
	setSRZ_D3_UT(pIspReg);
	setSRZ_D4_UT(pIspReg);
	setMIX_D1_UT(pIspReg);
	setMIX_D2_UT(pIspReg);
	setCRSP_D1_UT(pIspReg);
	setDFE_D1_UT(pIspReg);
	setC24_D2_UT(pIspReg);
	setC02_D1_UT(pIspReg);
	setFM_D1_UT(pIspReg);

        break;



    }

}



void SetDefaultTuning_old(dip_x_reg_t* pIspReg, MUINT32* tuningBuf, tuning_tag tag, int enFgMode)
{
#if 0
    printf("SetDefaultTuning (%d), enFgMode(%d)n", tag, enFgMode);
    MUINT32 fgModeRegBit = (enFgMode&0x01)<<10;
    switch(tag)
    {
        case tuning_tag_NBC2:
            pIspReg->DIP_X_CTL_YUV_EN.Bits.NBC2_EN = 1;//enable bit
            pIspReg->DIP_X_NBC2_ANR2_CON1.Raw = 0x00000001;//boken effect => NBC2_ANR2_MODE = 1, boken mode will use srz3 and srz4 to get data. sl2c:NBC2_ANR2_SL2_LINK=1
            pIspReg->DIP_X_NBC2_ANR2_CON2.Raw = 0x00000100;
            pIspReg->DIP_X_NBC2_ANR2_YAD1.Raw = 0x80000000;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT1.Raw = 0x00A07828;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT2.Raw = 0x08101010;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT3.Raw = 0x1e1c0000;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT1.Raw = 0x00C08040;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT2.Raw = 0x100C0A08;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT3.Raw = 0x02020101;
            pIspReg->DIP_X_NBC2_ANR2_CAD.Raw = 0x00180a8a;
            pIspReg->DIP_X_NBC2_ANR2_PTC.Raw = 0x06040302;
            pIspReg->DIP_X_NBC2_ANR2_SL2.Raw = 0x00040006;
            pIspReg->DIP_X_NBC2_ANR2_MED1.Raw = 0x20305405;
            pIspReg->DIP_X_NBC2_ANR2_MED2.Raw = 0x1030c103;
            pIspReg->DIP_X_NBC2_ANR2_MED3.Raw = 0x0A310330;
            pIspReg->DIP_X_NBC2_ANR2_MED4.Raw = 0x1613100D;
            pIspReg->DIP_X_NBC2_ANR2_MED5.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ANR2_ACTC.Raw = 0x10083000;
            pIspReg->DIP_X_NBC2_CCR_CON.Raw = 0xFF000101;//sl2c:NBC2_CCR_SL2_LINK = 1
            pIspReg->DIP_X_NBC2_CCR_YLUT.Raw = 0x10186020;
            pIspReg->DIP_X_NBC2_CCR_UVLUT.Raw = 0x40FF2505;
            pIspReg->DIP_X_NBC2_CCR_YLUT2.Raw = 0x40000010;
            pIspReg->DIP_X_NBC2_CCR_SAT_CTRL.Raw = 0x00000001;
            pIspReg->DIP_X_NBC2_CCR_UVLUT_SP.Raw = 0x00000040;
            pIspReg->DIP_X_NBC2_CCR_HUE1.Raw = 0x00100000;
            pIspReg->DIP_X_NBC2_CCR_HUE2.Raw = 0x011e010E;
            pIspReg->DIP_X_NBC2_CCR_HUE3.Raw = 0x00407f80;
            pIspReg->DIP_X_NBC2_CCR_L4LUT1.Raw = 0x00C08040;
            pIspReg->DIP_X_NBC2_CCR_L4LUT2.Raw = 0x100C0A08;
            pIspReg->DIP_X_NBC2_CCR_L4LUT3.Raw = 0x02020101;
            pIspReg->DIP_X_NBC2_BOK_CON.Raw = 0x000140000;
            pIspReg->DIP_X_NBC2_BOK_TUN.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_BOK_OFF.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CON1.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CON2.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_RCON.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_YLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CXLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CYLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_YSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CXSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CYSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CLP.Raw = 0x00000000;
            break;
        case tuning_tag_NBC2_BOKENEFFECT:
            //pIspReg->DIP_X_NBC2_ANR2_CON1.Raw = 0x010F1003;
            pIspReg->DIP_X_CTL_YUV_EN.Bits.NBC2_EN = 1;//enable bit
            pIspReg->DIP_X_NBC2_ANR2_CON1.Raw = 0x00001001;//boken effect => NBC2_ANR2_MODE = 1, boken mode will use srz3 and srz4 to get data.
            pIspReg->DIP_X_NBC2_ANR2_CON2.Raw = 0x00000100;
            pIspReg->DIP_X_NBC2_ANR2_YAD1.Raw = 0x80000000;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT1.Raw = 0x00A07828;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT2.Raw = 0x08101010;
            pIspReg->DIP_X_NBC2_ANR2_Y4LUT3.Raw = 0x1e1c0000;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT1.Raw = 0x00C08040;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT2.Raw = 0x100C0A08;
            pIspReg->DIP_X_NBC2_ANR2_L4LUT3.Raw = 0x02020101;
            pIspReg->DIP_X_NBC2_ANR2_CAD.Raw = 0x00180a8a;
            pIspReg->DIP_X_NBC2_ANR2_PTC.Raw = 0x06040302;
            pIspReg->DIP_X_NBC2_ANR2_SL2.Raw = 0x00040006;
            pIspReg->DIP_X_NBC2_ANR2_MED1.Raw = 0x20305405;
            pIspReg->DIP_X_NBC2_ANR2_MED2.Raw = 0x1030c103;
            pIspReg->DIP_X_NBC2_ANR2_MED3.Raw = 0x0A310330;
            pIspReg->DIP_X_NBC2_ANR2_MED4.Raw = 0x1613100D;
            pIspReg->DIP_X_NBC2_ANR2_MED5.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ANR2_ACTC.Raw = 0x10083000;
            pIspReg->DIP_X_NBC2_CCR_CON.Raw = 0xFF000101;
            pIspReg->DIP_X_NBC2_CCR_YLUT.Raw = 0x10186020;
            pIspReg->DIP_X_NBC2_CCR_UVLUT.Raw = 0x40FF2505;
            pIspReg->DIP_X_NBC2_CCR_YLUT2.Raw = 0x40000010;
            pIspReg->DIP_X_NBC2_CCR_SAT_CTRL.Raw = 0x00000001;
            pIspReg->DIP_X_NBC2_CCR_UVLUT_SP.Raw = 0x00000040;
            pIspReg->DIP_X_NBC2_CCR_HUE1.Raw = 0x00100000;
            pIspReg->DIP_X_NBC2_CCR_HUE2.Raw = 0x011e010E;
            pIspReg->DIP_X_NBC2_CCR_HUE3.Raw = 0x00407f80;
            pIspReg->DIP_X_NBC2_CCR_L4LUT1.Raw = 0x00C08040;
            pIspReg->DIP_X_NBC2_CCR_L4LUT2.Raw = 0x100C0A08;
            pIspReg->DIP_X_NBC2_CCR_L4LUT3.Raw = 0x02020101;
            pIspReg->DIP_X_NBC2_BOK_CON.Raw = 0x000140000;
            pIspReg->DIP_X_NBC2_BOK_TUN.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_BOK_OFF.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CON1.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CON2.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_RCON.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_YLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CXLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CYLUT.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_YSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CXSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CYSP.Raw = 0x00000000;
            pIspReg->DIP_X_NBC2_ABF_CLP.Raw = 0x00000000;
            break;

        case tuning_tag_SL2E:

            pIspReg->DIP_X_SL2E_CEN.Raw = 0x052a091e;
            pIspReg->DIP_X_SL2E_RR_CON0.Raw = 0x06c80364;
            pIspReg->DIP_X_SL2E_RR_CON1.Raw = 0x4000090B;
            pIspReg->DIP_X_SL2E_GAIN.Raw = 0x00ffc080;
            pIspReg->DIP_X_SL2E_RZ.Raw = 0x13551355;
            pIspReg->DIP_X_SL2E_XOFF.Raw = 0x00000000;
            pIspReg->DIP_X_SL2E_YOFF.Raw = 0x00000000;
            pIspReg->DIP_X_SL2E_SLP_CON0.Raw = 0x000012e0;
            pIspReg->DIP_X_SL2E_SLP_CON1.Raw = 0x000012e0;
            pIspReg->DIP_X_SL2E_SLP_CON2.Raw = 0x00001c4c;
            pIspReg->DIP_X_SL2E_SLP_CON3.Raw = 0x00001bdb;
            pIspReg->DIP_X_SL2E_SIZE.Raw = 0x00000000;
            break;
        case tuning_tag_RMM2:
            pIspReg->DIP_X_RMM2_OSC.Raw = 0xF01143EC; 
            pIspReg->DIP_X_RMM2_MC.Raw = 0x00040245;
            pIspReg->DIP_X_RMM2_REVG_1.Raw = 0x04000400; 
            pIspReg->DIP_X_RMM2_REVG_2.Raw = 0x04000400;
            pIspReg->DIP_X_RMM2_LEOS.Raw = 0x00000200; 
            pIspReg->DIP_X_RMM2_MC2.Raw = 0x00050080;
            pIspReg->DIP_X_RMM2_DIFF_LB.Raw = 0x00000000; 
            pIspReg->DIP_X_RMM2_MA.Raw = 0x08088408;
            pIspReg->DIP_X_RMM2_TUNE.Raw = 0x70800013;
            break;
        case tuning_tag_RMG2:
            pIspReg->DIP_X_RMG2_HDR_CFG.Raw = 0xFFA000E2; 
            pIspReg->DIP_X_RMG2_HDR_GAIN.Raw = 0x00400040;
            pIspReg->DIP_X_RMG2_HDR_CFG2.Raw = 0x00000400;
            break;

        case tuning_tag_G2G:
            pIspReg->DIP_X_CTL_RGB_EN.Bits.G2G_EN = 1;//enable bit
            pIspReg->DIP_X_G2G_CNV_1.Raw = 0x00000200;
            pIspReg->DIP_X_G2G_CNV_2.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_3.Raw = 0x02000000;
            pIspReg->DIP_X_G2G_CNV_4.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_5.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_6.Raw = 0x00000200;
            pIspReg->DIP_X_G2G_CTRL.Raw = 0x00000009;
            break;
        case tuning_tag_G2C:
            pIspReg->DIP_X_CTL_YUV_EN.Bits.G2C_EN = 1;//enable bit
            pIspReg->DIP_X_G2C_CONV_0A.Raw = 0x012D0099;
            pIspReg->DIP_X_G2C_CONV_0B.Raw = 0x0000003A;
            pIspReg->DIP_X_G2C_CONV_1A.Raw = 0x075607AA;
            pIspReg->DIP_X_G2C_CONV_1B.Raw = 0x00000100;
            pIspReg->DIP_X_G2C_CONV_2A.Raw = 0x072A0100;
            pIspReg->DIP_X_G2C_CONV_2B.Raw = 0x000007D6;
            pIspReg->DIP_X_G2C_SHADE_CON_1.Raw = 0x0118000E;
            pIspReg->DIP_X_G2C_SHADE_CON_2.Raw = 0x0074B740;
            pIspReg->DIP_X_G2C_SHADE_CON_3.Raw = 0x00000133;
            pIspReg->DIP_X_G2C_SHADE_TAR.Raw = 0x079F0A5A;
            pIspReg->DIP_X_G2C_SHADE_SP.Raw = 0x00000000;
            pIspReg->DIP_X_G2C_CFC_CON_1.Raw = 0x03f70080;
            pIspReg->DIP_X_G2C_CFC_CON_2.Raw = 0x1CE539CE;
            break;
        case tuning_tag_GGM:
            //tuningBuf[0x1000>>2] = 0x08000800;
            pIspReg->DIP_X_CTL_RGB_EN.Bits.GGM_EN = 1;//enable bit
            tuningBuf[0x00002180 >> 2] = 0x00000000; /*0x00002180  DIP_A_GGM_LUT[0] */
            tuningBuf[0x00002184 >> 2] = 0x00200802; /*0x00002184  DIP_A_GGM_LUT[1] */
            tuningBuf[0x00002188 >> 2] = 0x00401004; /*0x00002188  DIP_A_GGM_LUT[2] */
            tuningBuf[0x0000218C >> 2] = 0x00601806; /*0x0000218C  DIP_A_GGM_LUT[3] */
            tuningBuf[0x00002190 >> 2] = 0x00802008; /*0x00002190  DIP_A_GGM_LUT[4] */ 
            tuningBuf[0x00002194 >> 2] = 0x00a0280a; /*0x00002194  DIP_A_GGM_LUT[5] */ 
            tuningBuf[0x00002198 >> 2] = 0x00c0300c; /*0x00002198  DIP_A_GGM_LUT[6] */ 
            tuningBuf[0x0000219C >> 2] = 0x00e0380e; /*0x0000219C  DIP_A_GGM_LUT[7] */ 
            tuningBuf[0x000021A0 >> 2] = 0x01004010; /*0x000021A0  DIP_A_GGM_LUT[8] */  
            tuningBuf[0x000021A4 >> 2] = 0x01204812; /*0x000021A4  DIP_A_GGM_LUT[9] */  
            tuningBuf[0x000021A8 >> 2] = 0x01405014; /*0x000021A8  DIP_A_GGM_LUT[10] */  
            tuningBuf[0x000021AC >> 2] = 0x01605816; /*0x000021AC  DIP_A_GGM_LUT[11] */  
            tuningBuf[0x000021B0 >> 2] = 0x01806018; /*0x000021B0  DIP_A_GGM_LUT[12] */   
            tuningBuf[0x000021B4 >> 2] = 0x01a0681a; /*0x000021B4  DIP_A_GGM_LUT[13] */   
            tuningBuf[0x000021B8 >> 2] = 0x01c0701c; /*0x000021B8  DIP_A_GGM_LUT[14] */   
            tuningBuf[0x000021BC >> 2] = 0x01e0781e; /*0x000021BC  DIP_A_GGM_LUT[15] */   
            tuningBuf[0x000021C0 >> 2] = 0x02008020; /*0x000021C0  DIP_A_GGM_LUT[16] */  
            tuningBuf[0x000021C4 >> 2] = 0x02208822; /*0x000021C4  DIP_A_GGM_LUT[17] */  
            tuningBuf[0x000021C8 >> 2] = 0x02409024; /*0x000021C8  DIP_A_GGM_LUT[18] */  
            tuningBuf[0x000021CC >> 2] = 0x02609826; /*0x000021CC  DIP_A_GGM_LUT[19] */  
            tuningBuf[0x000021D0 >> 2] = 0x0280a028; /*0x000021D0  DIP_A_GGM_LUT[20] */   
            tuningBuf[0x000021D4 >> 2] = 0x02a0a82a; /*0x000021D4  DIP_A_GGM_LUT[21] */   
            tuningBuf[0x000021D8 >> 2] = 0x02c0b02c; /*0x000021D8  DIP_A_GGM_LUT[22] */   
            tuningBuf[0x000021DC >> 2] = 0x02e0b82e; /*0x000021DC  DIP_A_GGM_LUT[23] */   
            tuningBuf[0x000021E0 >> 2] = 0x0300c030; /*0x000021E0  DIP_A_GGM_LUT[24] */    
            tuningBuf[0x000021E4 >> 2] = 0x0320c832; /*0x000021E4  DIP_A_GGM_LUT[25] */    
            tuningBuf[0x000021E8 >> 2] = 0x0340d034; /*0x000021E8  DIP_A_GGM_LUT[26] */    
            tuningBuf[0x000021EC >> 2] = 0x0360d836; /*0x000021EC  DIP_A_GGM_LUT[27] */    
            tuningBuf[0x000021F0 >> 2] = 0x0380e038; /*0x000021F0  DIP_A_GGM_LUT[28] */     
            tuningBuf[0x000021F4 >> 2] = 0x03a0e83a; /*0x000021F4  DIP_A_GGM_LUT[29] */     
            tuningBuf[0x000021F8 >> 2] = 0x03c0f03c; /*0x000021F8  DIP_A_GGM_LUT[30] */     
            tuningBuf[0x000021FC >> 2] = 0x03e0f83e; /*0x000021FC  DIP_A_GGM_LUT[31] */     
            tuningBuf[0x00002200 >> 2] = 0x04010040; /*0x00002200  DIP_A_GGM_LUT[32] */  
            tuningBuf[0x00002204 >> 2] = 0x04210842; /*0x00002204  DIP_A_GGM_LUT[33] */  
            tuningBuf[0x00002208 >> 2] = 0x04411044; /*0x00002208  DIP_A_GGM_LUT[34] */  
            tuningBuf[0x0000220C >> 2] = 0x04611846; /*0x0000220C  DIP_A_GGM_LUT[35] */  
            tuningBuf[0x00002210 >> 2] = 0x04812048; /*0x00002210  DIP_A_GGM_LUT[36] */  
            tuningBuf[0x00002214 >> 2] = 0x04a1284a; /*0x00002214  DIP_A_GGM_LUT[37] */  
            tuningBuf[0x00002218 >> 2] = 0x04c1304c; /*0x00002218  DIP_A_GGM_LUT[38] */  
            tuningBuf[0x0000221C >> 2] = 0x04e1384e; /*0x0000221C  DIP_A_GGM_LUT[39] */  
            tuningBuf[0x00002220 >> 2] = 0x05014050; /*0x00002220  DIP_A_GGM_LUT[40] */   
            tuningBuf[0x00002224 >> 2] = 0x05214852; /*0x00002224  DIP_A_GGM_LUT[41] */   
            tuningBuf[0x00002228 >> 2] = 0x05415054; /*0x00002228  DIP_A_GGM_LUT[42] */   
            tuningBuf[0x0000222C >> 2] = 0x05615856; /*0x0000222C  DIP_A_GGM_LUT[43] */   
            tuningBuf[0x00002230 >> 2] = 0x05816058; /*0x00002230  DIP_A_GGM_LUT[44] */    
            tuningBuf[0x00002234 >> 2] = 0x05a1685a; /*0x00002234  DIP_A_GGM_LUT[45] */    
            tuningBuf[0x00002238 >> 2] = 0x05c1705c; /*0x00002238  DIP_A_GGM_LUT[46] */    
            tuningBuf[0x0000223C >> 2] = 0x05e1785e; /*0x0000223C  DIP_A_GGM_LUT[47] */    
            tuningBuf[0x00002240 >> 2] = 0x06018060; /*0x00002240  DIP_A_GGM_LUT[48] */     
            tuningBuf[0x00002244 >> 2] = 0x06218862; /*0x00002244  DIP_A_GGM_LUT[49] */     
            tuningBuf[0x00002248 >> 2] = 0x06419064; /*0x00002248  DIP_A_GGM_LUT[50] */     
            tuningBuf[0x0000224C >> 2] = 0x06619866; /*0x0000224C  DIP_A_GGM_LUT[51] */     
            tuningBuf[0x00002250 >> 2] = 0x0681a068; /*0x00002250  DIP_A_GGM_LUT[52] */      
            tuningBuf[0x00002254 >> 2] = 0x06a1a86a; /*0x00002254  DIP_A_GGM_LUT[53] */      
            tuningBuf[0x00002258 >> 2] = 0x06c1b06c; /*0x00002258  DIP_A_GGM_LUT[54] */      
            tuningBuf[0x0000225C >> 2] = 0x06e1b86e; /*0x0000225C  DIP_A_GGM_LUT[55] */      
            tuningBuf[0x00002260 >> 2] = 0x0701c070; /*0x00002260  DIP_A_GGM_LUT[56] */  
            tuningBuf[0x00002264 >> 2] = 0x0721c872; /*0x00002264  DIP_A_GGM_LUT[57] */  
            tuningBuf[0x00002268 >> 2] = 0x0741d074; /*0x00002268  DIP_A_GGM_LUT[58] */  
            tuningBuf[0x0000226C >> 2] = 0x0761d876; /*0x0000226C  DIP_A_GGM_LUT[59] */  
            tuningBuf[0x00002270 >> 2] = 0x0781e078; /*0x00002270  DIP_A_GGM_LUT[60] */  
            tuningBuf[0x00002274 >> 2] = 0x07a1e87a; /*0x00002274  DIP_A_GGM_LUT[61] */  
            tuningBuf[0x00002278 >> 2] = 0x07c1f07c; /*0x00002278  DIP_A_GGM_LUT[62] */  
            tuningBuf[0x0000227C >> 2] = 0x07e1f87e; /*0x0000227C  DIP_A_GGM_LUT[63] */  
            tuningBuf[0x00002280 >> 2] = 0x08020080; /*0x00002280  DIP_A_GGM_LUT[64] */ 
            tuningBuf[0x00002284 >> 2] = 0x08421084; /*0x00002284  DIP_A_GGM_LUT[65] */ 
            tuningBuf[0x00002288 >> 2] = 0x08822088; /*0x00002288  DIP_A_GGM_LUT[66] */ 
            tuningBuf[0x0000228C >> 2] = 0x08c2308c; /*0x0000228C  DIP_A_GGM_LUT[67] */ 
            tuningBuf[0x00002290 >> 2] = 0x09024090; /*0x00002290  DIP_A_GGM_LUT[68] */ 
            tuningBuf[0x00002294 >> 2] = 0x09425094; /*0x00002294  DIP_A_GGM_LUT[69] */ 
            tuningBuf[0x00002298 >> 2] = 0x09826098; /*0x00002298  DIP_A_GGM_LUT[70] */ 
            tuningBuf[0x0000229C >> 2] = 0x09c2709c; /*0x0000229C  DIP_A_GGM_LUT[71] */ 
            tuningBuf[0x000022A0 >> 2] = 0x0a0280a0; /*0x000022A0  DIP_A_GGM_LUT[72] */ 
            tuningBuf[0x000022A4 >> 2] = 0x0a4290a4; /*0x000022A4  DIP_A_GGM_LUT[73] */ 
            tuningBuf[0x000022A8 >> 2] = 0x0a82a0a8; /*0x000022A8  DIP_A_GGM_LUT[74] */ 
            tuningBuf[0x000022AC >> 2] = 0x0ac2b0ac; /*0x000022AC  DIP_A_GGM_LUT[75] */ 
            tuningBuf[0x000022B0 >> 2] = 0x0b02c0b0; /*0x000022B0  DIP_A_GGM_LUT[76] */  
            tuningBuf[0x000022B4 >> 2] = 0x0b42d0b4; /*0x000022B4  DIP_A_GGM_LUT[77] */  
            tuningBuf[0x000022B8 >> 2] = 0x0b82e0b8; /*0x000022B8  DIP_A_GGM_LUT[78] */  
            tuningBuf[0x000022BC >> 2] = 0x0bc2f0bc; /*0x000022BC  DIP_A_GGM_LUT[79] */  
            tuningBuf[0x000022C0 >> 2] = 0x0c0300c0; /*0x000022C0  DIP_A_GGM_LUT[80] */   
            tuningBuf[0x000022C4 >> 2] = 0x0c4310c4; /*0x000022C4  DIP_A_GGM_LUT[81] */   
            tuningBuf[0x000022C8 >> 2] = 0x0c8320c8; /*0x000022C8  DIP_A_GGM_LUT[82] */   
            tuningBuf[0x000022CC >> 2] = 0x0cc330cc; /*0x000022CC  DIP_A_GGM_LUT[83] */   
            tuningBuf[0x000022D0 >> 2] = 0x0d0340d0; /*0x000022D0  DIP_A_GGM_LUT[84] */   
            tuningBuf[0x000022D4 >> 2] = 0x0d4350d4; /*0x000022D4  DIP_A_GGM_LUT[85] */   
            tuningBuf[0x000022D8 >> 2] = 0x0d8360d8; /*0x000022D8  DIP_A_GGM_LUT[86] */   
            tuningBuf[0x000022DC >> 2] = 0x0dc370dc; /*0x000022DC  DIP_A_GGM_LUT[87] */   
            tuningBuf[0x000022E0 >> 2] = 0x0e0380e0; /*0x000022E0  DIP_A_GGM_LUT[88] */ 
            tuningBuf[0x000022E4 >> 2] = 0x0e4390e4; /*0x000022E4  DIP_A_GGM_LUT[89] */ 
            tuningBuf[0x000022E8 >> 2] = 0x0e83a0e8; /*0x000022E8  DIP_A_GGM_LUT[90] */ 
            tuningBuf[0x000022EC >> 2] = 0x0ec3b0ec; /*0x000022EC  DIP_A_GGM_LUT[91] */ 
            tuningBuf[0x000022F0 >> 2] = 0x0f03c0f0; /*0x000022F0  DIP_A_GGM_LUT[92] */ 
            tuningBuf[0x000022F4 >> 2] = 0x0f43d0f4; /*0x000022F4  DIP_A_GGM_LUT[93] */ 
            tuningBuf[0x000022F8 >> 2] = 0x0f83e0f8; /*0x000022F8  DIP_A_GGM_LUT[94] */ 
            tuningBuf[0x000022FC >> 2] = 0x0fc3f0fc; /*0x000022FC  DIP_A_GGM_LUT[95] */ 
            tuningBuf[0x00002300 >> 2] = 0x10040100; /*0x00002300  DIP_A_GGM_LUT[96] */  
            tuningBuf[0x00002304 >> 2] = 0x10842108; /*0x00002304  DIP_A_GGM_LUT[97] */  
            tuningBuf[0x00002308 >> 2] = 0x11044110; /*0x00002308  DIP_A_GGM_LUT[98] */  
            tuningBuf[0x0000230C >> 2] = 0x11846118; /*0x0000230C  DIP_A_GGM_LUT[99] */  
            tuningBuf[0x00002310 >> 2] = 0x12048120; /*0x00002310  DIP_A_GGM_LUT[100] */ 
            tuningBuf[0x00002314 >> 2] = 0x1284a128; /*0x00002314  DIP_A_GGM_LUT[101] */ 
            tuningBuf[0x00002318 >> 2] = 0x1304c130; /*0x00002318  DIP_A_GGM_LUT[102] */ 
            tuningBuf[0x0000231C >> 2] = 0x1384e138; /*0x0000231C  DIP_A_GGM_LUT[103] */ 
            tuningBuf[0x00002320 >> 2] = 0x14050140; /*0x00002320  DIP_A_GGM_LUT[104] */ 
            tuningBuf[0x00002324 >> 2] = 0x14852148; /*0x00002324  DIP_A_GGM_LUT[105] */ 
            tuningBuf[0x00002328 >> 2] = 0x15054150; /*0x00002328  DIP_A_GGM_LUT[106] */ 
            tuningBuf[0x0000232C >> 2] = 0x15856158; /*0x0000232C  DIP_A_GGM_LUT[107] */ 
            tuningBuf[0x00002330 >> 2] = 0x16058160; /*0x00002330  DIP_A_GGM_LUT[108] */ 
            tuningBuf[0x00002334 >> 2] = 0x1685a168; /*0x00002334  DIP_A_GGM_LUT[109] */ 
            tuningBuf[0x00002338 >> 2] = 0x1705c170; /*0x00002338  DIP_A_GGM_LUT[110] */ 
            tuningBuf[0x0000233C >> 2] = 0x1785e178; /*0x0000233C  DIP_A_GGM_LUT[111] */ 
            tuningBuf[0x00002340 >> 2] = 0x18060180; /*0x00002340  DIP_A_GGM_LUT[112] */ 
            tuningBuf[0x00002344 >> 2] = 0x18862188; /*0x00002344  DIP_A_GGM_LUT[113] */ 
            tuningBuf[0x00002348 >> 2] = 0x19064190; /*0x00002348  DIP_A_GGM_LUT[114] */ 
            tuningBuf[0x0000234C >> 2] = 0x19866198; /*0x0000234C  DIP_A_GGM_LUT[115] */ 
            tuningBuf[0x00002350 >> 2] = 0x1a0681a0; /*0x00002350  DIP_A_GGM_LUT[116] */ 
            tuningBuf[0x00002354 >> 2] = 0x1a86a1a8; /*0x00002354  DIP_A_GGM_LUT[117] */ 
            tuningBuf[0x00002358 >> 2] = 0x1b06c1b0; /*0x00002358  DIP_A_GGM_LUT[118] */ 
            tuningBuf[0x0000235C >> 2] = 0x1b86e1b8; /*0x0000235C  DIP_A_GGM_LUT[119] */ 
            tuningBuf[0x00002360 >> 2] = 0x1c0701c0; /*0x00002360  DIP_A_GGM_LUT[120] */ 
            tuningBuf[0x00002364 >> 2] = 0x1c8721c8; /*0x00002364  DIP_A_GGM_LUT[121] */ 
            tuningBuf[0x00002368 >> 2] = 0x1d0741d0; /*0x00002368  DIP_A_GGM_LUT[122] */ 
            tuningBuf[0x0000236C >> 2] = 0x1d8761d8; /*0x0000236C  DIP_A_GGM_LUT[123] */ 
            tuningBuf[0x00002370 >> 2] = 0x1e0781e0; /*0x00002370  DIP_A_GGM_LUT[124] */  
            tuningBuf[0x00002374 >> 2] = 0x1e87a1e8; /*0x00002374  DIP_A_GGM_LUT[125] */  
            tuningBuf[0x00002378 >> 2] = 0x1f07c1f0; /*0x00002378  DIP_A_GGM_LUT[126] */  
            tuningBuf[0x0000237C >> 2] = 0x1f87e1f8; /*0x0000237C  DIP_A_GGM_LUT[127] */  
            tuningBuf[0x00002380 >> 2] = 0x20080200; /*0x00002380  DIP_A_GGM_LUT[128] */  
            tuningBuf[0x00002384 >> 2] = 0x20882208; /*0x00002384  DIP_A_GGM_LUT[129] */  
            tuningBuf[0x00002388 >> 2] = 0x21084210; /*0x00002388  DIP_A_GGM_LUT[130] */  
            tuningBuf[0x0000238C >> 2] = 0x21886218; /*0x0000238C  DIP_A_GGM_LUT[131] */  
            tuningBuf[0x00002390 >> 2] = 0x22088220; /*0x00002390  DIP_A_GGM_LUT[132] */ 
            tuningBuf[0x00002394 >> 2] = 0x2288a228; /*0x00002394  DIP_A_GGM_LUT[133] */ 
            tuningBuf[0x00002398 >> 2] = 0x2308c230; /*0x00002398  DIP_A_GGM_LUT[134] */ 
            tuningBuf[0x0000239C >> 2] = 0x2388e238; /*0x0000239C  DIP_A_GGM_LUT[135] */ 
            tuningBuf[0x000023A0 >> 2] = 0x24090240; /*0x000023A0  DIP_A_GGM_LUT[136] */ 
            tuningBuf[0x000023A4 >> 2] = 0x24892248; /*0x000023A4  DIP_A_GGM_LUT[137] */ 
            tuningBuf[0x000023A8 >> 2] = 0x25094250; /*0x000023A8  DIP_A_GGM_LUT[138] */ 
            tuningBuf[0x000023AC >> 2] = 0x25896258; /*0x000023AC  DIP_A_GGM_LUT[139] */ 
            tuningBuf[0x000023B0 >> 2] = 0x26098260; /*0x000023B0  DIP_A_GGM_LUT[140] */ 
            tuningBuf[0x000023B4 >> 2] = 0x2689a268; /*0x000023B4  DIP_A_GGM_LUT[141] */ 
            tuningBuf[0x000023B8 >> 2] = 0x2709c270; /*0x000023B8  DIP_A_GGM_LUT[142] */ 
            tuningBuf[0x000023BC >> 2] = 0x2789e278; /*0x000023BC  DIP_A_GGM_LUT[143] */ 
            tuningBuf[0x000023C0 >> 2] = 0x280a0280; /*0x000023C0  DIP_A_GGM_LUT[144] */ 
            tuningBuf[0x000023C4 >> 2] = 0x288a2288; /*0x000023C4  DIP_A_GGM_LUT[145] */ 
            tuningBuf[0x000023C8 >> 2] = 0x290a4290; /*0x000023C8  DIP_A_GGM_LUT[146] */ 
            tuningBuf[0x000023CC >> 2] = 0x298a6298; /*0x000023CC  DIP_A_GGM_LUT[147] */ 
            tuningBuf[0x000023D0 >> 2] = 0x2a0a82a0; /*0x000023D0  DIP_A_GGM_LUT[148] */ 
            tuningBuf[0x000023D4 >> 2] = 0x2a8aa2a8; /*0x000023D4  DIP_A_GGM_LUT[149] */ 
            tuningBuf[0x000023D8 >> 2] = 0x2b0ac2b0; /*0x000023D8  DIP_A_GGM_LUT[150] */ 
            tuningBuf[0x000023DC >> 2] = 0x2b8ae2b8; /*0x000023DC  DIP_A_GGM_LUT[151] */ 
            tuningBuf[0x000023E0 >> 2] = 0x2c0b02c0; /*0x000023E0  DIP_A_GGM_LUT[152] */ 
            tuningBuf[0x000023E4 >> 2] = 0x2c8b22c8; /*0x000023E4  DIP_A_GGM_LUT[153] */ 
            tuningBuf[0x000023E8 >> 2] = 0x2d0b42d0; /*0x000023E8  DIP_A_GGM_LUT[154] */ 
            tuningBuf[0x000023EC >> 2] = 0x2d8b62d8; /*0x000023EC  DIP_A_GGM_LUT[155] */ 
            tuningBuf[0x000023F0 >> 2] = 0x2e0b82e0; /*0x000023F0  DIP_A_GGM_LUT[156] */ 
            tuningBuf[0x000023F4 >> 2] = 0x2e8ba2e8; /*0x000023F4  DIP_A_GGM_LUT[157] */ 
            tuningBuf[0x000023F8 >> 2] = 0x2f0bc2f0; /*0x000023F8  DIP_A_GGM_LUT[158] */ 
            tuningBuf[0x000023FC >> 2] = 0x2f8be2f8; /*0x000023FC  DIP_A_GGM_LUT[159] */ 
            tuningBuf[0x00002400 >> 2] = 0x300c0300; /*0x00002400  DIP_A_GGM_LUT[160] */ 
            tuningBuf[0x00002404 >> 2] = 0x308c2308; /*0x00002404  DIP_A_GGM_LUT[161] */ 
            tuningBuf[0x00002408 >> 2] = 0x310c4310; /*0x00002408  DIP_A_GGM_LUT[162] */ 
            tuningBuf[0x0000240C >> 2] = 0x318c6318; /*0x0000240C  DIP_A_GGM_LUT[163] */ 
            tuningBuf[0x00002410 >> 2] = 0x320c8320; /*0x00002410  DIP_A_GGM_LUT[164] */ 
            tuningBuf[0x00002414 >> 2] = 0x328ca328; /*0x00002414  DIP_A_GGM_LUT[165] */ 
            tuningBuf[0x00002418 >> 2] = 0x330cc330; /*0x00002418  DIP_A_GGM_LUT[166] */ 
            tuningBuf[0x0000241C >> 2] = 0x338ce338; /*0x0000241C  DIP_A_GGM_LUT[167] */ 
            tuningBuf[0x00002420 >> 2] = 0x340d0340; /*0x00002420  DIP_A_GGM_LUT[168] */ 
            tuningBuf[0x00002424 >> 2] = 0x348d2348; /*0x00002424  DIP_A_GGM_LUT[169] */ 
            tuningBuf[0x00002428 >> 2] = 0x350d4350; /*0x00002428  DIP_A_GGM_LUT[170] */ 
            tuningBuf[0x0000242C >> 2] = 0x358d6358; /*0x0000242C  DIP_A_GGM_LUT[171] */ 
            tuningBuf[0x00002430 >> 2] = 0x360d8360; /*0x00002430  DIP_A_GGM_LUT[172] */ 
            tuningBuf[0x00002434 >> 2] = 0x368da368; /*0x00002434  DIP_A_GGM_LUT[173] */ 
            tuningBuf[0x00002438 >> 2] = 0x370dc370; /*0x00002438,   DIP_A_GGM_LUT[174] */ 
            tuningBuf[0x0000243C >> 2] = 0x378de378; /*0x0000243C,   DIP_A_GGM_LUT[175] */ 
            tuningBuf[0x00002440 >> 2] = 0x380e0380; /*0x00002440,   DIP_A_GGM_LUT[176] */ 
            tuningBuf[0x00002444 >> 2] = 0x388e2388; /*0x00002444,   DIP_A_GGM_LUT[177] */ 
            tuningBuf[0x00002448 >> 2] = 0x390e4390; /*0x00002448,   DIP_A_GGM_LUT[178] */ 
            tuningBuf[0x0000244C >> 2] = 0x398e6398; /*0x0000244C,   DIP_A_GGM_LUT[179] */ 
            tuningBuf[0x00002450 >> 2] = 0x3a0e83a0; /*0x00002450,   DIP_A_GGM_LUT[180] */ 
            tuningBuf[0x00002454 >> 2] = 0x3a8ea3a8; /*0x00002454,   DIP_A_GGM_LUT[182] */ 
            tuningBuf[0x00002458 >> 2] = 0x3b0ec3b0; /*0x00002458,   DIP_A_GGM_LUT[182] */ 
            tuningBuf[0x0000245C >> 2] = 0x3b8ee3b8; /*0x0000245C,   DIP_A_GGM_LUT[183] */ 
            tuningBuf[0x00002460 >> 2] = 0x3c0f03c0; /*0x00002460,   DIP_A_GGM_LUT[184] */ 
            tuningBuf[0x00002464 >> 2] = 0x3c8f23c8; /*0x00002464,   DIP_A_GGM_LUT[185] */ 
            tuningBuf[0x00002468 >> 2] = 0x3d0f43d0; /*0x00002468,   DIP_A_GGM_LUT[186] */ 
            tuningBuf[0x0000246C >> 2] = 0x3d8f63d8; /*0x0000246C,   DIP_A_GGM_LUT[187] */ 
            tuningBuf[0x00002470 >> 2] = 0x3e0f83e0; /*0x00002470,   DIP_A_GGM_LUT[188] */ 
            tuningBuf[0x00002474 >> 2] = 0x3e0f83e0; /*0x00002474,   DIP_A_GGM_LUT[189] */ 
            tuningBuf[0x00002478 >> 2] = 0x3e0f83e0; /*0x00002478,   DIP_A_GGM_LUT[190] */ 
            tuningBuf[0x0000247C >> 2] = 0x3e0f83e0; /*0x0000247C,   DIP_A_GGM_LUT[191] */ 
            tuningBuf[0x00002480 >> 2] = 0x100307FF; /* 0x15024480: DIP_X_GGM_CTRL */

            break;
        case tuning_tag_GGM2:
            //tuningBuf[0x1000>>2] = 0x08000800;
            pIspReg->DIP_X_CTL_RGB_EN.Bits.GGM2_EN = 1;//enable bit
            tuningBuf[0x00002640 >> 2] = 0x00000000; /*0x15024640  DIP_A_GGM2_LUT[0] */ 
            tuningBuf[0x00002644 >> 2] = 0x00200802; /*0x15024644  DIP_A_GGM2_LUT[1] */ 
            tuningBuf[0x00002648 >> 2] = 0x00401004; /*0x15024648  DIP_A_GGM2_LUT[2] */ 
            tuningBuf[0x0000264C >> 2] = 0x00601806; /*0x1502464C  DIP_A_GGM2_LUT[3] */ 
            tuningBuf[0x00002650 >> 2] = 0x00802008; /*0x15024650  DIP_A_GGM2_LUT[4] */ 
            tuningBuf[0x00002654 >> 2] = 0x00a0280a; /*0x15024654  DIP_A_GGM2_LUT[5] */ 
            tuningBuf[0x00002658 >> 2] = 0x00c0300c; /*0x15024658  DIP_A_GGM2_LUT[6] */ 
            tuningBuf[0x0000265C >> 2] = 0x00e0380e; /*0x1502465C  DIP_A_GGM2_LUT[7] */ 
            tuningBuf[0x00002660 >> 2] = 0x01004010; /*0x15024660  DIP_A_GGM2_LUT[8] */ 
            tuningBuf[0x00002664 >> 2] = 0x01204812; /*0x15024664  DIP_A_GGM2_LUT[9] */ 
            tuningBuf[0x00002668 >> 2] = 0x01405014; /*0x15024668  DIP_A_GGM2_LUT[10] */
            tuningBuf[0x0000266C >> 2] = 0x01605816; /*0x1502466C  DIP_A_GGM2_LUT[11] */
            tuningBuf[0x00002670 >> 2] = 0x01806018; /*0x15024670  DIP_A_GGM2_LUT[12] */
            tuningBuf[0x00002674 >> 2] = 0x01a0681a; /*0x15024674  DIP_A_GGM2_LUT[13] */
            tuningBuf[0x00002678 >> 2] = 0x01c0701c; /*0x15024678  DIP_A_GGM2_LUT[14] */
            tuningBuf[0x0000267C >> 2] = 0x01e0781e; /*0x1502467C  DIP_A_GGM2_LUT[15] */
            tuningBuf[0x00002680 >> 2] = 0x02008020; /*0x15024680  DIP_A_GGM2_LUT[16] */
            tuningBuf[0x00002684 >> 2] = 0x02208822; /*0x15024684  DIP_A_GGM2_LUT[17] */
            tuningBuf[0x00002688 >> 2] = 0x02409024; /*0x15024688  DIP_A_GGM2_LUT[18] */
            tuningBuf[0x0000268C >> 2] = 0x02609826; /*0x1502468C  DIP_A_GGM2_LUT[19] */
            tuningBuf[0x00002690 >> 2] = 0x0280a028; /*0x15024690  DIP_A_GGM2_LUT[20] */
            tuningBuf[0x00002694 >> 2] = 0x02a0a82a; /*0x15024694  DIP_A_GGM2_LUT[21] */
            tuningBuf[0x00002698 >> 2] = 0x02c0b02c; /*0x15024698  DIP_A_GGM2_LUT[22] */
            tuningBuf[0x0000269C >> 2] = 0x02e0b82e; /*0x1502469C  DIP_A_GGM2_LUT[23] */
            tuningBuf[0x000026A0 >> 2] = 0x0300c030; /*0x150246A0  DIP_A_GGM2_LUT[24] */
            tuningBuf[0x000026A4 >> 2] = 0x0320c832; /*0x150246A4  DIP_A_GGM2_LUT[25] */
            tuningBuf[0x000026A8 >> 2] = 0x0340d034; /*0x150246A8  DIP_A_GGM2_LUT[26] */
            tuningBuf[0x000026AC >> 2] = 0x0360d836; /*0x150246AC  DIP_A_GGM2_LUT[27] */
            tuningBuf[0x000026B0 >> 2] = 0x0380e038; /*0x150246B0  DIP_A_GGM2_LUT[28] */
            tuningBuf[0x000026B4 >> 2] = 0x03a0e83a; /*0x150246B4  DIP_A_GGM2_LUT[29] */
            tuningBuf[0x000026B8 >> 2] = 0x03c0f03c; /*0x150246B8  DIP_A_GGM2_LUT[30] */
            tuningBuf[0x000026BC >> 2] = 0x03e0f83e; /*0x150246BC  DIP_A_GGM2_LUT[31] */
            tuningBuf[0x000026C0 >> 2] = 0x04010040; /*0x150246C0  DIP_A_GGM2_LUT[32] */
            tuningBuf[0x000026C4 >> 2] = 0x04210842; /*0x150246C4  DIP_A_GGM2_LUT[33] */
            tuningBuf[0x000026C8 >> 2] = 0x04411044; /*0x150246C8  DIP_A_GGM2_LUT[34] */
            tuningBuf[0x000026CC >> 2] = 0x04611846; /*0x150246CC  DIP_A_GGM2_LUT[35] */
            tuningBuf[0x000026D0 >> 2] = 0x04812048; /*0x150246D0  DIP_A_GGM2_LUT[36] */
            tuningBuf[0x000026D4 >> 2] = 0x04a1284a; /*0x150246D4  DIP_A_GGM2_LUT[37] */
            tuningBuf[0x000026D8 >> 2] = 0x04c1304c; /*0x150246D8  DIP_A_GGM2_LUT[38] */
            tuningBuf[0x000026DC >> 2] = 0x04e1384e; /*0x150246DC  DIP_A_GGM2_LUT[39] */
            tuningBuf[0x000026E0 >> 2] = 0x05014050; /*0x150246E0  DIP_A_GGM2_LUT[40] */
            tuningBuf[0x000026E4 >> 2] = 0x05214852; /*0x150246E4  DIP_A_GGM2_LUT[41] */
            tuningBuf[0x000026E8 >> 2] = 0x05415054; /*0x150246E8  DIP_A_GGM2_LUT[42] */
            tuningBuf[0x000026EC >> 2] = 0x05615856; /*0x150246EC  DIP_A_GGM2_LUT[43] */
            tuningBuf[0x000026F0 >> 2] = 0x05816058; /*0x150246F0  DIP_A_GGM2_LUT[44] */
            tuningBuf[0x000026F4 >> 2] = 0x05a1685a; /*0x150246F4  DIP_A_GGM2_LUT[45] */
            tuningBuf[0x000026F8 >> 2] = 0x05c1705c; /*0x150246F8  DIP_A_GGM2_LUT[46] */
            tuningBuf[0x000026FC >> 2] = 0x05e1785e; /*0x150246FC  DIP_A_GGM2_LUT[47] */
            tuningBuf[0x00002700 >> 2] = 0x06018060; /*0x15024700  DIP_A_GGM2_LUT[48] */
            tuningBuf[0x00002704 >> 2] = 0x06218862; /*0x15024704  DIP_A_GGM2_LUT[49] */
            tuningBuf[0x00002708 >> 2] = 0x06419064; /*0x15024708  DIP_A_GGM2_LUT[50] */
            tuningBuf[0x0000270C >> 2] = 0x06619866; /*0x1502470C  DIP_A_GGM2_LUT[51] */
            tuningBuf[0x00002710 >> 2] = 0x0681a068; /*0x15024710  DIP_A_GGM2_LUT[52] */
            tuningBuf[0x00002714 >> 2] = 0x06a1a86a; /*0x15024714  DIP_A_GGM2_LUT[53] */
            tuningBuf[0x00002718 >> 2] = 0x06c1b06c; /*0x15024718  DIP_A_GGM2_LUT[54] */
            tuningBuf[0x0000271C >> 2] = 0x06e1b86e; /*0x1502471C  DIP_A_GGM2_LUT[55] */
            tuningBuf[0x00002720 >> 2] = 0x0701c070; /*0x15024720  DIP_A_GGM2_LUT[56] */
            tuningBuf[0x00002724 >> 2] = 0x0721c872; /*0x15024724  DIP_A_GGM2_LUT[57] */
            tuningBuf[0x00002728 >> 2] = 0x0741d074; /*0x15024728  DIP_A_GGM2_LUT[58] */
            tuningBuf[0x0000272C >> 2] = 0x0761d876; /*0x1502472C  DIP_A_GGM2_LUT[59] */
            tuningBuf[0x00002730 >> 2] = 0x0781e078; /*0x15024730  DIP_A_GGM2_LUT[60] */
            tuningBuf[0x00002734 >> 2] = 0x07a1e87a; /*0x15024734  DIP_A_GGM2_LUT[61] */
            tuningBuf[0x00002738 >> 2] = 0x07c1f07c; /*0x15024738  DIP_A_GGM2_LUT[62] */
            tuningBuf[0x0000273C >> 2] = 0x07e1f87e; /*0x1502473C  DIP_A_GGM2_LUT[63] */
            tuningBuf[0x00002740 >> 2] = 0x08020080; /*0x15024740  DIP_A_GGM2_LUT[64] */
            tuningBuf[0x00002744 >> 2] = 0x08421084; /*0x15024744  DIP_A_GGM2_LUT[65] */
            tuningBuf[0x00002748 >> 2] = 0x08822088; /*0x15024748  DIP_A_GGM2_LUT[66] */
            tuningBuf[0x0000274C >> 2] = 0x08c2308c; /*0x1502474C  DIP_A_GGM2_LUT[67] */
            tuningBuf[0x00002750 >> 2] = 0x09024090; /*0x15024750  DIP_A_GGM2_LUT[68] */
            tuningBuf[0x00002754 >> 2] = 0x09425094; /*0x15024754  DIP_A_GGM2_LUT[69] */
            tuningBuf[0x00002758 >> 2] = 0x09826098; /*0x15024758  DIP_A_GGM2_LUT[70] */
            tuningBuf[0x0000275C >> 2] = 0x09c2709c; /*0x1502475C  DIP_A_GGM2_LUT[71] */
            tuningBuf[0x00002760 >> 2] = 0x0a0280a0; /*0x15024760  DIP_A_GGM2_LUT[72] */
            tuningBuf[0x00002764 >> 2] = 0x0a4290a4; /*0x15024764  DIP_A_GGM2_LUT[73] */
            tuningBuf[0x00002768 >> 2] = 0x0a82a0a8; /*0x15024768  DIP_A_GGM2_LUT[74] */
            tuningBuf[0x0000276C >> 2] = 0x0ac2b0ac; /*0x1502476C  DIP_A_GGM2_LUT[75] */
            tuningBuf[0x00002770 >> 2] = 0x0b02c0b0; /*0x15024770  DIP_A_GGM2_LUT[76] */
            tuningBuf[0x00002774 >> 2] = 0x0b42d0b4; /*0x15024774  DIP_A_GGM2_LUT[77] */
            tuningBuf[0x00002778 >> 2] = 0x0b82e0b8; /*0x15024778  DIP_A_GGM2_LUT[78] */
            tuningBuf[0x0000277C >> 2] = 0x0bc2f0bc; /*0x1502477C  DIP_A_GGM2_LUT[79] */
            tuningBuf[0x00002780 >> 2] = 0x0c0300c0; /*0x15024780  DIP_A_GGM2_LUT[80] */
            tuningBuf[0x00002784 >> 2] = 0x0c4310c4; /*0x15024784  DIP_A_GGM2_LUT[81] */
            tuningBuf[0x00002788 >> 2] = 0x0c8320c8; /*0x15024788  DIP_A_GGM2_LUT[82] */
            tuningBuf[0x0000278C >> 2] = 0x0cc330cc; /*0x1502478C  DIP_A_GGM2_LUT[83] */
            tuningBuf[0x00002790 >> 2] = 0x0d0340d0; /*0x15024790  DIP_A_GGM2_LUT[84] */
            tuningBuf[0x00002794 >> 2] = 0x0d4350d4; /*0x15024794  DIP_A_GGM2_LUT[85] */
            tuningBuf[0x00002798 >> 2] = 0x0d8360d8; /*0x15024798  DIP_A_GGM2_LUT[86] */
            tuningBuf[0x0000279C >> 2] = 0x0dc370dc; /*0x1502479C  DIP_A_GGM2_LUT[87] */
            tuningBuf[0x000027A0 >> 2] = 0x0e0380e0; /*0x150247A0  DIP_A_GGM2_LUT[88] */
            tuningBuf[0x000027A4 >> 2] = 0x0e4390e4; /*0x150247A4  DIP_A_GGM2_LUT[89] */
            tuningBuf[0x000027A8 >> 2] = 0x0e83a0e8; /*0x150247A8  DIP_A_GGM2_LUT[90] */
            tuningBuf[0x000027AC >> 2] = 0x0ec3b0ec; /*0x150247AC  DIP_A_GGM2_LUT[91] */
            tuningBuf[0x000027B0 >> 2] = 0x0f03c0f0; /*0x150247B0  DIP_A_GGM2_LUT[92] */
            tuningBuf[0x000027B4 >> 2] = 0x0f43d0f4; /*0x150247B4  DIP_A_GGM2_LUT[93] */
            tuningBuf[0x000027B8 >> 2] = 0x0f83e0f8; /*0x150247B8  DIP_A_GGM2_LUT[94] */
            tuningBuf[0x000027BC >> 2] = 0x0fc3f0fc; /*0x150247BC  DIP_A_GGM2_LUT[95] */
            tuningBuf[0x000027C0 >> 2] = 0x10040100; /*0x150247C0  DIP_A_GGM2_LUT[96] */
            tuningBuf[0x000027C4 >> 2] = 0x10842108; /*0x150247C4  DIP_A_GGM2_LUT[97] */
            tuningBuf[0x000027C8 >> 2] = 0x11044110; /*0x150247C8  DIP_A_GGM2_LUT[98] */
            tuningBuf[0x000027CC >> 2] = 0x11846118; /*0x150247CC  DIP_A_GGM2_LUT[99] */
            tuningBuf[0x000027D0 >> 2] = 0x12048120; /*0x150247D0  DIP_A_GGM2_LUT[100] */
            tuningBuf[0x000027D4 >> 2] = 0x1284a128; /*0x150247D4  DIP_A_GGM2_LUT[101] */
            tuningBuf[0x000027D8 >> 2] = 0x1304c130; /*0x150247D8  DIP_A_GGM2_LUT[102] */
            tuningBuf[0x000027DC >> 2] = 0x1384e138; /*0x150247DC  DIP_A_GGM2_LUT[103] */
            tuningBuf[0x000027E0 >> 2] = 0x14050140; /*0x150247E0  DIP_A_GGM2_LUT[104] */
            tuningBuf[0x000027E4 >> 2] = 0x14852148; /*0x150247E4  DIP_A_GGM2_LUT[105] */
            tuningBuf[0x000027E8 >> 2] = 0x15054150; /*0x150247E8  DIP_A_GGM2_LUT[106] */
            tuningBuf[0x000027EC >> 2] = 0x15856158; /*0x150247EC  DIP_A_GGM2_LUT[107] */
            tuningBuf[0x000027F0 >> 2] = 0x16058160; /*0x150247F0  DIP_A_GGM2_LUT[108] */
            tuningBuf[0x000027F4 >> 2] = 0x1685a168; /*0x150247F4  DIP_A_GGM2_LUT[109] */
            tuningBuf[0x000027F8 >> 2] = 0x1705c170; /*0x150247F8  DIP_A_GGM2_LUT[110] */
            tuningBuf[0x000027FC >> 2] = 0x1785e178; /*0x150247FC  DIP_A_GGM2_LUT[111] */
            tuningBuf[0x00002800 >> 2] = 0x18060180; /*0x15024800  DIP_A_GGM2_LUT[112] */
            tuningBuf[0x00002804 >> 2] = 0x18862188; /*0x15024804  DIP_A_GGM2_LUT[113] */
            tuningBuf[0x00002808 >> 2] = 0x19064190; /*0x15024808  DIP_A_GGM2_LUT[114] */
            tuningBuf[0x0000280C >> 2] = 0x19866198; /*0x1502480C  DIP_A_GGM2_LUT[115] */
            tuningBuf[0x00002810 >> 2] = 0x1a0681a0; /*0x15024810  DIP_A_GGM2_LUT[116] */
            tuningBuf[0x00002814 >> 2] = 0x1a86a1a8; /*0x15024814  DIP_A_GGM2_LUT[117] */
            tuningBuf[0x00002818 >> 2] = 0x1b06c1b0; /*0x15024818  DIP_A_GGM2_LUT[118] */
            tuningBuf[0x0000281C >> 2] = 0x1b86e1b8; /*0x1502481C  DIP_A_GGM2_LUT[119] */
            tuningBuf[0x00002820 >> 2] = 0x1c0701c0; /*0x15024820  DIP_A_GGM2_LUT[120] */
            tuningBuf[0x00002824 >> 2] = 0x1c8721c8; /*0x15024824  DIP_A_GGM2_LUT[121] */
            tuningBuf[0x00002828 >> 2] = 0x1d0741d0; /*0x15024828  DIP_A_GGM2_LUT[122] */
            tuningBuf[0x0000282C >> 2] = 0x1d8761d8; /*0x1502482C  DIP_A_GGM2_LUT[123] */
            tuningBuf[0x00002830 >> 2] = 0x1e0781e0; /*0x15024830  DIP_A_GGM2_LUT[124] */
            tuningBuf[0x00002834 >> 2] = 0x1e87a1e8; /*0x15024834  DIP_A_GGM2_LUT[125] */
            tuningBuf[0x00002838 >> 2] = 0x1f07c1f0; /*0x15024838  DIP_A_GGM2_LUT[126] */
            tuningBuf[0x0000283C >> 2] = 0x1f87e1f8; /*0x1502483C  DIP_A_GGM2_LUT[127] */
            tuningBuf[0x00002840 >> 2] = 0x20080200; /*0x15024840  DIP_A_GGM2_LUT[128] */
            tuningBuf[0x00002844 >> 2] = 0x20882208; /*0x15024844  DIP_A_GGM2_LUT[129] */
            tuningBuf[0x00002848 >> 2] = 0x21084210; /*0x15024848  DIP_A_GGM2_LUT[130] */
            tuningBuf[0x0000284C >> 2] = 0x21886218; /*0x1502484C  DIP_A_GGM2_LUT[131] */
            tuningBuf[0x00002850 >> 2] = 0x22088220; /*0x15024850  DIP_A_GGM2_LUT[132] */
            tuningBuf[0x00002854 >> 2] = 0x2288a228; /*0x15024854  DIP_A_GGM2_LUT[133] */
            tuningBuf[0x00002858 >> 2] = 0x2308c230; /*0x15024858  DIP_A_GGM2_LUT[134] */
            tuningBuf[0x0000285C >> 2] = 0x2388e238; /*0x1502485C  DIP_A_GGM2_LUT[135] */
            tuningBuf[0x00002860 >> 2] = 0x24090240; /*0x15024860  DIP_A_GGM2_LUT[136] */
            tuningBuf[0x00002864 >> 2] = 0x24892248; /*0x15024864  DIP_A_GGM2_LUT[137] */
            tuningBuf[0x00002868 >> 2] = 0x25094250; /*0x15024868  DIP_A_GGM2_LUT[138] */
            tuningBuf[0x0000286C >> 2] = 0x25896258; /*0x1502486C  DIP_A_GGM2_LUT[139] */
            tuningBuf[0x00002870 >> 2] = 0x26098260; /*0x15024870  DIP_A_GGM2_LUT[140] */
            tuningBuf[0x00002874 >> 2] = 0x2689a268; /*0x15024874  DIP_A_GGM2_LUT[141] */
            tuningBuf[0x00002878 >> 2] = 0x2709c270; /*0x15024878  DIP_A_GGM2_LUT[142] */
            tuningBuf[0x0000287C >> 2] = 0x2789e278; /*0x1502487C  DIP_A_GGM2_LUT[143] */
            tuningBuf[0x00002880 >> 2] = 0x280a0280; /*0x15024880  DIP_A_GGM2_LUT[144] */
            tuningBuf[0x00002884 >> 2] = 0x288a2288; /*0x15024884  DIP_A_GGM2_LUT[145] */
            tuningBuf[0x00002888 >> 2] = 0x290a4290; /*0x15024888  DIP_A_GGM2_LUT[146] */
            tuningBuf[0x0000288C >> 2] = 0x298a6298; /*0x1502488C  DIP_A_GGM2_LUT[147] */
            tuningBuf[0x00002890 >> 2] = 0x2a0a82a0; /*0x15024890  DIP_A_GGM2_LUT[148] */
            tuningBuf[0x00002894 >> 2] = 0x2a8aa2a8; /*0x15024894  DIP_A_GGM2_LUT[149] */
            tuningBuf[0x00002898 >> 2] = 0x2b0ac2b0; /*0x15024898  DIP_A_GGM2_LUT[150] */
            tuningBuf[0x0000289C >> 2] = 0x2b8ae2b8; /*0x1502489C  DIP_A_GGM2_LUT[151] */
            tuningBuf[0x000028A0 >> 2] = 0x2c0b02c0; /*0x150248A0  DIP_A_GGM2_LUT[152] */
            tuningBuf[0x000028A4 >> 2] = 0x2c8b22c8; /*0x150248A4  DIP_A_GGM2_LUT[153] */
            tuningBuf[0x000028A8 >> 2] = 0x2d0b42d0; /*0x150248A8  DIP_A_GGM2_LUT[154] */
            tuningBuf[0x000028AC >> 2] = 0x2d8b62d8; /*0x150248AC  DIP_A_GGM2_LUT[155] */
            tuningBuf[0x000028B0 >> 2] = 0x2e0b82e0; /*0x150248B0  DIP_A_GGM2_LUT[156] */
            tuningBuf[0x000028B4 >> 2] = 0x2e8ba2e8; /*0x150248B4  DIP_A_GGM2_LUT[157] */
            tuningBuf[0x000028B8 >> 2] = 0x2f0bc2f0; /*0x150248B8  DIP_A_GGM2_LUT[158] */
            tuningBuf[0x000028BC >> 2] = 0x2f8be2f8; /*0x150248BC  DIP_A_GGM2_LUT[159] */
            tuningBuf[0x000028C0 >> 2] = 0x300c0300; /*0x150248C0  DIP_A_GGM2_LUT[160] */
            tuningBuf[0x000028C4 >> 2] = 0x308c2308; /*0x150248C4  DIP_A_GGM2_LUT[161] */
            tuningBuf[0x000028C8 >> 2] = 0x310c4310; /*0x150248C8  DIP_A_GGM2_LUT[162] */
            tuningBuf[0x000028CC >> 2] = 0x318c6318; /*0x150248CC  DIP_A_GGM2_LUT[163] */
            tuningBuf[0x000028D0 >> 2] = 0x320c8320; /*0x150248D0  DIP_A_GGM2_LUT[164] */
            tuningBuf[0x000028D4 >> 2] = 0x328ca328; /*0x150248D4  DIP_A_GGM2_LUT[165] */
            tuningBuf[0x000028D8 >> 2] = 0x330cc330; /*0x150248D8  DIP_A_GGM2_LUT[166] */
            tuningBuf[0x000028DC >> 2] = 0x338ce338; /*0x150248DC  DIP_A_GGM2_LUT[167] */
            tuningBuf[0x000028E0 >> 2] = 0x340d0340; /*0x150248E0  DIP_A_GGM2_LUT[168] */
            tuningBuf[0x000028E4 >> 2] = 0x348d2348; /*0x150248E4  DIP_A_GGM2_LUT[169] */
            tuningBuf[0x000028E8 >> 2] = 0x350d4350; /*0x150248E8  DIP_A_GGM2_LUT[170] */
            tuningBuf[0x000028EC >> 2] = 0x358d6358; /*0x150248EC  DIP_A_GGM2_LUT[171] */
            tuningBuf[0x000028F0 >> 2] = 0x360d8360; /*0x150248F0  DIP_A_GGM2_LUT[172] */
            tuningBuf[0x000028F4 >> 2] = 0x368da368; /*0x150248F4  DIP_A_GGM2_LUT[173] */
            tuningBuf[0x000028F8 >> 2] = 0x370dc370; /*0x150248F8,   DIP_A_GGM2_LUT[174]*/
            tuningBuf[0x000028FC >> 2] = 0x378de378; /*0x150248FC,   DIP_A_GGM2_LUT[175]*/
            tuningBuf[0x00002900 >> 2] = 0x380e0380; /*0x15024900,   DIP_A_GGM2_LUT[176]*/
            tuningBuf[0x00002904 >> 2] = 0x388e2388; /*0x15024904,   DIP_A_GGM2_LUT[177]*/
            tuningBuf[0x00002908 >> 2] = 0x390e4390; /*0x15024908,   DIP_A_GGM2_LUT[178]*/
            tuningBuf[0x0000290C >> 2] = 0x398e6398; /*0x1502490C,   DIP_A_GGM2_LUT[179]*/
            tuningBuf[0x00002910 >> 2] = 0x3a0e83a0; /*0x15024910,   DIP_A_GGM2_LUT[180]*/
            tuningBuf[0x00002914 >> 2] = 0x3a8ea3a8; /*0x15024914,   DIP_A_GGM2_LUT[182]*/
            tuningBuf[0x00002918 >> 2] = 0x3b0ec3b0; /*0x15024918,   DIP_A_GGM2_LUT[182]*/
            tuningBuf[0x0000291C >> 2] = 0x3b8ee3b8; /*0x1502491C,   DIP_A_GGM2_LUT[183]*/
            tuningBuf[0x00002920 >> 2] = 0x3c0f03c0; /*0x15024920,   DIP_A_GGM2_LUT[184]*/
            tuningBuf[0x00002924 >> 2] = 0x3c8f23c8; /*0x15024924,   DIP_A_GGM2_LUT[185]*/
            tuningBuf[0x00002928 >> 2] = 0x3d0f43d0; /*0x15024928,   DIP_A_GGM2_LUT[186]*/
            tuningBuf[0x0000292C >> 2] = 0x3d8f63d8; /*0x1502492C,   DIP_A_GGM2_LUT[187]*/
            tuningBuf[0x00002930 >> 2] = 0x3e0f83e0; /*0x15024930,   DIP_A_GGM2_LUT[188]*/
            tuningBuf[0x00002934 >> 2] = 0x3e0f83e0; /*0x15024934,   DIP_A_GGM2_LUT[189]*/
            tuningBuf[0x00002938 >> 2] = 0x3e0f83e0; /*0x15024938,   DIP_A_GGM2_LUT[190]*/
            tuningBuf[0x0000293C >> 2] = 0x3e0f83e0; /*0x1502493C,   DIP_A_GGM2_LUT[191]*/
            tuningBuf[0x00002940 >> 2] = 0x100307FF; /* 0x15024940: DIP_A_GGM2_CTRL */              

            break;
        case tuning_tag_UDM:
            pIspReg->DIP_X_UDM_INTP_CRS.Raw = 0x0002F004;                              /* 1540, 0x15023540, DIP_A_UDM_INTP_CRS */
            pIspReg->DIP_X_UDM_INTP_NAT.Raw = 0x1430053F;                              /* 1544, 0x15023544, DIP_A_UDM_INTP_NAT */
            pIspReg->DIP_X_UDM_INTP_AUG.Raw = 0x00500500;                              /* 1548, 0x15023548, DIP_A_UDM_INTP_AUG */
            pIspReg->DIP_X_UDM_LUMA_LUT1.Raw = 0x052A30DC;                             /* 154C, 0x1502354C, DIP_A_UDM_LUMA_LUT1 */
            pIspReg->DIP_X_UDM_LUMA_LUT2.Raw = 0x02A9124F;                             /* 1550, 0x15023550, DIP_A_UDM_LUMA_LUT2 */
            pIspReg->DIP_X_UDM_SL_CTL.Raw = 0x0039B4A0;                                /* 1554, 0x15023554, DIP_A_UDM_SL_CTL */
            pIspReg->DIP_X_UDM_HFTD_CTL.Raw = 0x0A529400;                              /* 1558, 0x15023558, DIP_A_UDM_HFTD_CTL */
            pIspReg->DIP_X_UDM_NR_STR.Raw = 0x81028000;                                /* 155C, 0x1502355C, DIP_A_UDM_NR_STR */
            pIspReg->DIP_X_UDM_NR_ACT.Raw = 0x00000050;                                /* 1560, 0x15023560, DIP_A_UDM_NR_ACT */
            pIspReg->DIP_X_UDM_HF_STR.Raw = 0x84210000;                                /* 1564, 0x15023564, DIP_A_UDM_HF_STR */
            pIspReg->DIP_X_UDM_HF_ACT1.Raw =0x46FF1EFF;                               /* 1568, 0x15023568, DIP_A_UDM_HF_ACT1 */
            pIspReg->DIP_X_UDM_HF_ACT2.Raw = 0x001EFF55;                               /* 156C, 0x1502356C, DIP_A_UDM_HF_ACT2 */
            pIspReg->DIP_X_UDM_CLIP.Raw = 0x00942064;                                  /* 1570, 0x15023570, DIP_A_UDM_CLIP */
            pIspReg->DIP_X_UDM_DSB.Raw = 0x00000000;                                   /* 1574, 0x15023574, DIP_A_UDM_DSB */
            pIspReg->DIP_X_UDM_TILE_EDGE.Raw = 0x0000000F;                             /* 1578, 0x15023578, DIP_A_UDM_TILE_EDGE */
            pIspReg->DIP_X_UDM_P1_ACT.Raw = 0x000000FF;                                /* 157C, 0x1502357C, DIP_A_UDM_P1_ACT */
            pIspReg->DIP_X_UDM_LR_RAT.Raw = 0x00000418;                                /* 1580, 0x15023580, DIP_A_UDM_LR_RAT */
            pIspReg->DIP_X_UDM_HFTD_CTL2.Raw = 0x00000019;                             /* 1584, 0x15023584, DIP_A_UDM_HFTD_CTL2 */
            pIspReg->DIP_X_UDM_EST_CTL.Raw = 0x00000035;                               /* 1588, 0x15023588, DIP_A_UDM_EST_CTL */
            pIspReg->DIP_X_UDM_SPARE_2.Raw = 0x00000000;                               /* 158C, 0x1502358C, DIP_A_UDM_SPARE_2 */
            pIspReg->DIP_X_UDM_SPARE_3.Raw = 0x00000000;                               /* 1590, 0x15023590, DIP_A_UDM_SPARE_3 */
            pIspReg->DIP_X_UDM_INT_CTL.Raw = 0x00000035;                               /* 1594, 0x15023594, DIP_A_UDM_INT_CTL */
            pIspReg->DIP_X_UDM_EE.Raw= 0x00000410;                                    /* 1598, 0x15023598, DIP_A_UDM_EE */

            break;
        default:
            break;
    }
#endif
}




#include "pic/imgi_1280x720_bayer10.h"


int testRawInRawOut(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);
    PhyDipDrv* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (PhyDipDrv*)DipDrv::createInstance(DIP_HW_A);
    g_pDrvDipPhy->init("testRawInRawOut");


    int ret=0;
    printf("--- [testRawInRawOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testRawInRawOut");
    printf("--- [testRawInRawOut(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRawInRawOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRawInRawOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRawInRawOut(%d)...push src done]\n", type);


   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRawInRawOut(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=buf_imgi.size;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {(_mfbo_w_*10/8),0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRawInRawOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRawInRawOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRawInRawOut(%d)...push dst done\n]", type);


    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRawInRawOut(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_imgi_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_mfbo_%dx%d.raw", type, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out1.virtAddr), buf_out1.size);


        //sprintf(filename, "/data/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testRawInRawOut...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");

        usleep(5000000);
    }while (1);



    //free
    srcBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    outBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testRawInRawOut(%d)...free memory done\n]", type);

    //
    pStream->uninit("testRawInRawOut");
    pStream->destroyInstance();
    printf("--- [testRawInRawOut(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("testRawInRawOut");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}

#include "pic/P2A_FG/imgi_4208_2368_rgb48_test2.h"

/*********************************************************************************/
int testRGBInYuvOut(int type,int loopNum)
{
    int ret=0;
    printf("--- [testRGBInYuvOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testRGBInYuvOut");
    printf("--- [testRGBInYuvOut(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    //MUINT32 _imgi_w_=640, _imgi_h_=480;
    MUINT32 _imgi_w_=4208, _imgi_h_=2368;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(imgi_4208_2368_rgb48_item2); //imgi_4208_2368_rgb48_test2.h
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(imgi_4208_2368_rgb48_item2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRGBInYuvOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 6 , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_RGB48),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRGBInYuvOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRGBInYuvOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRGBInYuvOut(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRGBInYuvOut(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRGBInYuvOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRGBInYuvOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRGBInYuvOut(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRGBInYuvOut(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRGBInYuvOut(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testRGBInYuvOut(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRGBInYuvOut(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[testRGBInYuvOut(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [testRGBInYuvOut(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [testRGBInYuvOut(%d)...free memory done\n]", type);

    //
    pStream->uninit("testRGBInYuvOut");
    pStream->destroyInstance();
    printf("--- [testRGBInYuvOut(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


int testRawInUnPakRawOut(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);
    int ret=0;
    PhyDipDrv* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (PhyDipDrv*)DipDrv::createInstance(DIP_HW_A);
    g_pDrvDipPhy->init("testRawInRawOut");


    printf("--- [testRawInRawOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testRawInRawOut");
    printf("--- [testRawInRawOut(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRawInRawOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRawInRawOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRawInRawOut(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRawInRawOut(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=buf_imgi.size;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    //MUINT32 bufStridesInBytes_1[3] = {(_mfbo_w_*10/8),0,0};
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    //portBufInfo_1.
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),
                                            MSize(2048,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRawInRawOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);

    outBuffer->setExtParam(MSize(_imgi_w_, _imgi_h_));

    
    printf("--- [testRawInRawOut(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRawInRawOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRawInRawOut(%d)...push dst done\n]", type);

    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRawInRawOut(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_imgi_%dx%d.raw", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 10/8);

        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_mfbo_%dx%d.raw", type, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out1.virtAddr), buf_out1.size);


        //sprintf(filename, "/system/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testRawInRawOut...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");
        usleep(5000000);
    }while (1);

    //free
    srcBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    outBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testRawInRawOut(%d)...free memory done\n]", type);

    //
    pStream->uninit("testRawInRawOut");
    pStream->destroyInstance();
    printf("--- [testRawInRawOut(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("testRawInRawOut");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}




#include "pic/MFB/imgci.h"
#include "pic/MFB/B1_320x240_yuy2.h"
#include "pic/imgi_640x480_yuy2.h"



#define MFB_FULL_SIZE_W 640 //2592
#define MFB_FULL_SIZE_H 480 //1944

/*********************************************************************************/
int testMFB_Blending(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);

    PhyDipDrv* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (PhyDipDrv*)DipDrv::createInstance(DIP_HW_A);
    g_pDrvDipPhy->init("MFB_Blending");

    int ret=0;
    printf("--- [testMFB_Blending(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testMFB_Blending");
    printf("--- [testMFB_Blending(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Bld;

    //input image
    //int _imgi_w_=2592, _imgi_h_=1944;
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    //buf_imgi.size=sizeof(g_imgi_array);
    buf_imgi.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgi);
    //memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array), buf_imgi.size);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_640x480_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testMFB_Blending", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testMFB_Blending(%d)...push src done]\n", type);


    //int _imgbi_w_=640, _imgbi_h_=480;
    MUINT32 _imgbi_w_=320, _imgbi_h_=240;
    IMEM_BUF_INFO buf_imgbi;
    //buf_imgbi.size=sizeof(g_imgbi_array);
    buf_imgbi.size=_imgbi_w_*_imgbi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgbi);

    //memcpy( (MUINT8*)(buf_imgbi.virtAddr), (MUINT8*)(g_imgbi_array), buf_imgbi.size);
    memcpy( (MUINT8*)(buf_imgbi.virtAddr), (MUINT8*)(B1_320x240_yuy2), buf_imgbi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* imgbi_srcBuffer;
    MINT32 imgbi_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgbi_bufStridesInBytes[3] = {_imgbi_w_*2, 0, 0};
    PortBufInfo_v1 imgbi_portBufInfo = PortBufInfo_v1( buf_imgbi.memID,buf_imgbi.virtAddr,0,buf_imgbi.bufSecu, buf_imgbi.bufCohe);
    IImageBufferAllocator::ImgParam imgbi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgbi_w_, _imgbi_h_), imgbi_bufStridesInBytes, imgbi_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgbi_pHeap;
    imgbi_pHeap = ImageBufferHeap::create( "testMFB_Blending", imgbi_imgParam,imgbi_portBufInfo,true);
    imgbi_srcBuffer = imgbi_pHeap->createImageBuffer();
    imgbi_srcBuffer->incStrong(imgbi_srcBuffer);
    imgbi_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGBI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_2;
    src_2.mPortID=PORT_IMGBI;
    src_2.mBuffer=imgbi_srcBuffer;
    src_2.mPortID.group=0;
    frameParams.mvIn.push_back(src_2);
    printf("--- [testMFB_Blending(%d)...push src_2 done]\n", type);

    MUINT32 _imgci_w_=MFB_FULL_SIZE_W, _imgci_h_=MFB_FULL_SIZE_H;
    IMEM_BUF_INFO buf_imgci;
    buf_imgci.size=MFB_FULL_SIZE_W*MFB_FULL_SIZE_H;
    mpImemDrv->allocVirtBuf(&buf_imgci);
    //memset( (MUINT8*)(buf_imgci.virtAddr), 0x0, buf_imgci.size);
    //memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgci_array), buf_imgci.size);
    memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgci_array), _imgci_w_*_imgci_h_);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* imgci_srcBuffer;
    MINT32 imgci_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgci_bufStridesInBytes[3] = {MFB_FULL_SIZE_W, 0, 0};
    PortBufInfo_v1 imgci_portBufInfo = PortBufInfo_v1( buf_imgci.memID,buf_imgci.virtAddr,0,buf_imgci.bufSecu, buf_imgci.bufCohe);
    IImageBufferAllocator::ImgParam imgci_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_imgci_w_, _imgci_h_), imgci_bufStridesInBytes, imgci_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgci_pHeap;
    imgci_pHeap = ImageBufferHeap::create( "testMFB_Blending", imgci_imgParam,imgci_portBufInfo,true);
    imgci_srcBuffer = imgci_pHeap->createImageBuffer();
    imgci_srcBuffer->incStrong(imgci_srcBuffer);
    imgci_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGCI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_3;
    src_3.mPortID=PORT_IMGCI;
    src_3.mBuffer=imgci_srcBuffer;
    src_3.mPortID.group=0;
    frameParams.mvIn.push_back(src_3);

    printf("--- [testMFB_Blending(%d)...push src_3 done]\n", type);


    dip_x_reg_t tuningDat;
#if 0
    MF_TAG_IN_MFB_CAM_MFB_LL_CON2 : 10000406
    MF_TAG_IN_MFB_CAM_MFB_LL_CON3 : 00040C11
    MF_TAG_IN_MFB_CAM_MFB_LL_CON4 : 10100130
    MF_TAG_IN_MFB_CAM_MFB_LL_CON5 : 0002F06E
    MF_TAG_IN_MFB_CAM_MFB_LL_CON6 : 0000FF00
#endif
#if 0
    tuningDat.DIP_X_MFB_CON.Raw = 0x00004031;
    tuningDat.DIP_X_MFB_LL_CON1.Raw = 0x00000201;
    tuningDat.DIP_X_MFB_LL_CON2.Raw = 0x140A0606;
    //tuningDat.DIP_X_MFB_LL_CON3.Raw =   0x07980A20;
    tuningDat.DIP_X_MFB_LL_CON3.Raw =   (_imgi_h_<<16)+ _imgi_w_;
    tuningDat.DIP_X_MFB_LL_CON4.Raw = 0x00000000;
    tuningDat.DIP_X_MFB_EDGE.Raw = 0x0000000F;

    tuningDat.DIP_X_CTL_YUV_EN.Bits.G2C_EN = 1;//enable bit
    tuningDat.DIP_X_G2C_CONV_0A.Raw = 0x0200;
    tuningDat.DIP_X_G2C_CONV_0B.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_1A.Raw = 0x02000000;
    tuningDat.DIP_X_G2C_CONV_1B.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_2A.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_2B.Raw = 0x0200;

#endif

    frameParams.mTuningData = (MVOID*)&tuningDat;
    //android::Vector<MVOID*> mvTuningData;        //v1&v3 usage:  for p2 tuning data
    //m_camPass2Param.pTuningIspReg = (dip_x_reg_t *)pPipePackageInfo->pTuningQue;// check tuning enable bit on isp_function_dip


   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testMFB_Blending(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=_mfbo_w_*_mfbo_h_;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_w_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testMFB_Blending", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Blending(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testMFB_Blending", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testMFB_Blending(%d)...push dst done\n]", type);

#if 0

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_MODE,0x1);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_BRZ_EN,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_DB_EN,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_TH_E,0xa5);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_MODE,0x2);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_WT_MODE1,0x1);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_WT_MODE2,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_CLIP_TH1,0x40);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_CLIP_TH2,0x1e);


    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_MAX_WT,6);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_DT1,0x1b);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_TH1,0xf);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_TH2,0x35);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_XSIZE,0xa20);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_XOFST,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_YSIZE,0x798);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON4 ,BLD_LL_DB_XDIST,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON4 ,BLD_LL_DB_YDIST,0x0);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_EDGE ,BLD_TILE_EDGE,0xf);
#endif
//   RAL: DIP_A_MFB_EDGE.BLD_TILE_EDGE = 0xf                 # bits  4, 0x15022824,  0

    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testMFB_Blending(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgi_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgbi_%dx%d.yuv", type, _imgbi_w_,_imgbi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgbi.virtAddr), _imgbi_w_ *_imgbi_h_ * 2);


        //sprintf(filename, "/data/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testMFB_Blending...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");

        usleep(5000000);
    }while (1);



    //free
    srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    imgbi_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgbi);

    imgci_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgci);


    outBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testMFB_Blending(%d)...free memory done\n]", type);

    //
    pStream->uninit("testMFB_Blending");
    pStream->destroyInstance();
    printf("--- [testMFB_Blending(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("MFB_Blending");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}

#include "pic/P2A_FG/imgi_640_480_10.h"

/*********************************************************************************/
MVOID basicP2ACallback(QParams& rParams)
{
	printf("--- [basicP2A callback func]\n");

	g_basicP2ACallback = MTRUE;
}

int basicP2A(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A");
    printf("--- [basicP2A(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicP2A(%d)...push dst done\n]", type);

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_P2A, 1);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);

    g_basicP2ACallback = MFALSE;
    enqueParams.mpfnCallback = basicP2ACallback;
    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicP2ACallback)
            {
                break;
            }
        }while(1);
        g_basicP2ACallback = MFALSE;
		
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [basicP2A(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[basicP2A(%d_%d)..deque done\n]", type, i);
        }


        if (enqueParams.mvFrameParams.size()>0){
            //dump image
            if(type==0)
            {
                char filename[256];
                sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
                saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
                char filename2[256];
                sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
                saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            }
            else
            {

                char filename[256];
                sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
                saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
                char filename2[256];
                sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
                saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
    			//char filename3[256];
                //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
                //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            }
        }
        printf("--- [basicP2A(%d_%d)...save file done\n]", type,i);
    }

    //free
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    srcBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A");
    pStream->destroyInstance();
    printf("--- [basicP2A(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "pic/imgi_720x540_b10.h"

/*********************************************************************************/
MVOID basicMultiFrameCallback(QParams& rParams)
{
	printf("--- [basicMultiFrame callback func]\n");

	g_basicMultiFrameCallback = MTRUE;
}

int basicMultiFrame( int type,int loopNum)
{
    int ret=0;
    printf("--- [basicMultiFrame(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicMultiFrame");
    printf("--- [basicMultiFrame(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicMultiFrame(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicMultiFrame(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicMultiFrame(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [basicMultiFrame(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [basicMultiFrame(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);

    g_basicMultiFrameCallback = MFALSE;
    enqueParams.mpfnCallback = basicMultiFrameCallback;
    printf("--- [basicMultiFrame(%d)_frame2...push dst done\n]", type);



    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicMultiFrame(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicMultiFrame(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicMultiFrameCallback)
            {
                break;
            }
        }while(1);
        g_basicMultiFrameCallback = MFALSE;
		
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [basicMultiFrame(%d_%d)..deque fail\n]", type,i);
        //}
        //else
        {
            printf("---[basicMultiFrame(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_2(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_2(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    printf("--- [basicMultiFrame(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicMultiFrame");
    pStream->destroyInstance();
    printf("--- [basicMultiFrame(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
MVOID onThreadCallback(QParams& rParams)
{
	printf("--- [onThreadLoop callback func]\n");

	g_onThreadCallback = MTRUE;
}

static sem_t     mSem_ThreadEnd;
static pthread_t           mThread;
static int           mThread_loopNum=0;
static int           mThread_type=0;

//
#define PR_SET_NAME 15
static MVOID* onThreadLoop(MVOID *arg)
{
    arg;
    int ret=0;
    //[1] set thread
    // set thread name
    ::prctl(PR_SET_NAME,"onThreadLoop",0,0,0);
    // set policy/priority
    int const policy    = SCHED_OTHER;
    int const priority    = NICE_CAMERA_PASS2;
    //
    struct sched_param sched_p;
    ::sched_getparam(0, &sched_p);
    if(policy == SCHED_OTHER)
    {    //    Note: "priority" is nice-value priority.
        sched_p.sched_priority = 0;
        ::sched_setscheduler(0, policy, &sched_p);
        ::setpriority(PRIO_PROCESS, 0, priority);
    }
    else
    {    //    Note: "priority" is real-time priority.
        sched_p.sched_priority = priority;
        ::sched_setscheduler(0, policy, &sched_p);
    }
    //
    printf(
        "policy:(expect, result)=(%d, %d), priority:(expect, result)=(%d, %d)\n"
        , policy, ::sched_getscheduler(0)
        , priority, sched_p.sched_priority
    );
    //    detach thread => cannot be join, it means that thread would release resource after exit
    ::pthread_detach(::pthread_self());

    //
    printf("--- [onThreadLoop(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", mThread_type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("onThreadLoop");
    printf("--- [onThreadLoop(%d)...pStream init done]\n", mThread_type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [onThreadLoop(%d)...flag -1 ]\n", mThread_type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "onThreadLoop", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [onThreadLoop(%d)...flag -8]\n", mThread_type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [onThreadLoop(%d)...push src done]\n", mThread_type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [onThreadLoop(%d)...push crop information done\n]", mThread_type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "onThreadLoop", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(mThread_type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "onThreadLoop", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(mThread_type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    g_onThreadCallback = MFALSE;
    enqueParams.mpfnCallback = onThreadCallback;
    printf("--- [onThreadLoop(%d)...push dst done\n]", mThread_type);


    for(int i=0;i<mThread_loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [onThreadLoop(%d_%d)...flush done\n]", mThread_type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [onThreadLoop(%d_%d)..enque fail\n]", mThread_type, i);
        }
        else
        {
            printf("---[onThreadLoop(%d_%d)..enque done\n]",mThread_type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [onThreadLoop(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_onThreadCallback)
            {
                break;
            }
        }while(1);
        g_onThreadCallback = MFALSE;
		
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [onThreadLoop(%d_%d)..deque fail\n]",mThread_type, i);
        //}
        //else
        {
            printf("---[onThreadLoop(%d_%d)..deque done\n]", mThread_type, i);
        }


        //dump image
        if(mThread_type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [onThreadLoop(%d_%d)...save file done\n]", mThread_type,i);
    }

    //free
    srcBuffer->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [onThreadLoop(%d)...free memory done\n]", mThread_type);

    //
    pStream->uninit("onThreadLoop");
    pStream->destroyInstance();
    printf("--- [onThreadLoop(%d)...pStream uninit done\n]", mThread_type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();


    ::sem_post(&mSem_ThreadEnd);
    return NULL;
}

//
MVOID multiThreadCallback(QParams& rParams)
{
	printf("--- [multiThread callback func]\n");

	g_multiThreadCallback = MTRUE;
}

int multiThread(int type,int loopNum)
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiThread(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiThread");
    printf("--- [multiThread(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    //create another thread
/*
    typedef long pthread_t;
    typedef struct {
        uint32_t flags;
        void* stack_base;
        size_t stack_size;
        size_t guard_size;
        int32_t sched_policy;
        int32_t sched_priority;
      #ifdef __LP64__
        char __reserved[16];
      #endif
    } pthread_attr_t;
*/
    mThread_loopNum=loopNum;
    mThread_type=type;
#ifdef __LP64__
    pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
    pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
    pthread_create(&mThread, &attr, onThreadLoop, NULL);


    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiThread(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiThread_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThread(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiThread(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiThread(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiThread_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiThread_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [multiThread(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [multiThread(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThread(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [multiThread(%d)_frame1...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [multiThread(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    g_multiThreadCallback = MFALSE;
    enqueParams.mpfnCallback = multiThreadCallback;
    printf("--- [multiThread(%d)_frame2...push dst done\n]", type);



    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiThread(%d_%d)...flush done\n]", type,i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThread(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[multiThread(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiThread(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_multiThreadCallback)
            {
                break;
            }
        }while(1);
        g_multiThreadCallback = MFALSE;
		
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [multiThread(%d_%d)..deque fail\n]", type,i);
        //}
        //else
        {
            printf("---[multiThread(%d_%d)..deque done\n]", type,i);
        }

        if(type==0)
        {
            //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiThread_1(%d)...save file done\n]", type);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
        }
        else
        {
            //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiThread_1(%d)...save file done\n]", type);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);

        }
        printf("--- [multiThread_2(%d)...save file done\n]", type);
    }

    //free
    srcBuffer->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    printf("--- [multiThread(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiThread");
    pStream->destroyInstance();
    printf("--- [multiThread(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    printf("--- [multiThread(%d)...wait mSem_ThreadEnd\n]", type);
    ::sem_wait(&mSem_ThreadEnd);
    return ret;
}


/*********************************************************************************/

#include "pic/imgi_3264x1836_bayer10.h"

MVOID basicVssCallback(QParams& rParams)
{
	printf("--- [basicVss callback func]\n");

	g_basicVssCallback = MTRUE;
}

int basicVss(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicVss(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicVss");
    printf("--- [basicVss(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=3264, _imgi_h_=1836;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_3264x1836_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_3264x1836_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicVss(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicVss", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicVss(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicVss(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicVss(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicVss", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicVss", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_NBC2, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_SL2E, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_RMM2, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_RMG2, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

	g_basicVssCallback = MFALSE;
	enqueParams.mpfnCallback = basicVssCallback;
    printf("--- [basicVss(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicVss(%d_%d)...flush done\n]", type, i);


        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicVss(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicVss(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicVss(%d)...enter while...........\n]", type);
       //while(1);
       do{
            usleep(100000);
            if (MTRUE == g_basicVssCallback)
            {
                break;
            }
        }while(1);
        g_basicVssCallback = MFALSE;


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [basicVss(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[basicVss(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicVss_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicVss_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [basicVss(%d_%d)...save file done\n]", type,i);
    }

    //free
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    srcBuffer->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [basicVss(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicVss");
    pStream->destroyInstance();
    printf("--- [basicVss(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();
    return ret;
}

/*********************************************************************************/
//
MVOID vssCaptureThreadCallback(QParams& rParams)
{
	printf("--- [vssCaptureThread callback func]\n");

	g_vssCaptureThreadCallback = MTRUE;
}

static MVOID* vssCaptureThread(MVOID *arg) //capture thread
{
    arg;
    int ret=0;
    //[1] set thread
    // set thread name
    ::prctl(PR_SET_NAME,"vssCaptureThread",0,0,0);
    // set policy/priority
    int const policy    = SCHED_OTHER;
    int const priority    = NICE_CAMERA_PASS2;
    //
    struct sched_param sched_p;
    ::sched_getparam(0, &sched_p);
    if(policy == SCHED_OTHER)
    {    //    Note: "priority" is nice-value priority.
        sched_p.sched_priority = 0;
        ::sched_setscheduler(0, policy, &sched_p);
        ::setpriority(PRIO_PROCESS, 0, priority);
    }
    else
    {    //    Note: "priority" is real-time priority.
        sched_p.sched_priority = priority;
        ::sched_setscheduler(0, policy, &sched_p);
    }
    //
    printf(
        "policy:(expect, result)=(%d, %d), priority:(expect, result)=(%d, %d)\n"
        , policy, ::sched_getscheduler(0)
        , priority, sched_p.sched_priority
    );
    //    detach thread => cannot be join, it means that thread would release resource after exit
    ::pthread_detach(::pthread_self());

    //
    printf("--- [vssCaptureThread(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", mThread_type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("vssCaptureThread");
    printf("--- [vssCaptureThread(%d)...pStream init done]\n", mThread_type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=3264, _imgi_h_=1836;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_3264x1836_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_3264x1836_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [vssCaptureThread(%d)...flag -1 ]\n", mThread_type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "vssCaptureThread", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [vssCaptureThread(%d)...flag -8]\n", mThread_type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [vssCaptureThread(%d)...push src done]\n", mThread_type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [vssCaptureThread(%d)...push crop information done\n]", mThread_type);

    //output dma
    IMEM_BUF_INFO buf_wmdao;
    buf_wmdao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wmdao);
    memset((MUINT8*)buf_wmdao.virtAddr, 0xffffffff, buf_wmdao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wmdao.memID,buf_wmdao.virtAddr,0,buf_wmdao.bufSecu, buf_wmdao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "vssCaptureThread", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "vssCaptureThread", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
	g_vssCaptureThreadCallback = MFALSE;
	enqueParams.mpfnCallback = vssCaptureThreadCallback;
    printf("--- [vssCaptureThread(%d)...push dst done\n]", mThread_type);


    for(int i=0;i<mThread_loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wmdao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [vssCaptureThread(%d_%d)...flush done\n]", mThread_type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [vssCaptureThread(%d_%d)..enque fail\n]", mThread_type, i);
        }
        else
        {
            printf("---[vssCaptureThread(%d_%d)..enque done\n]",mThread_type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [vssCaptureThread(%d)...enter while...........\n]", type);
       //while(1);
		do{
			usleep(100000);
			if (MTRUE == g_vssCaptureThreadCallback)
			{
				break;
			}
		}while(1);
		g_vssCaptureThreadCallback = MFALSE;


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [vssCaptureThread(%d_%d)..deque fail\n]",mThread_type, i);
        //}
        //else
        {
            printf("---[vssCaptureThread(%d_%d)..deque done\n]", mThread_type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_vssCaptureThread_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),mThread_type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_vssCaptureThread_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),mThread_type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [vssCaptureThread(%d_%d)...save file done\n]", mThread_type,i);
    }

    //free
    srcBuffer->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_wmdao);
    outBuffer_2->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [vssCaptureThread(%d)...free memory done\n]", mThread_type);

    //
    pStream->uninit("vssCaptureThread");
    pStream->destroyInstance();
    printf("--- [vssCaptureThread(%d)...pStream uninit done\n]", mThread_type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();


    ::sem_post(&mSem_ThreadEnd);
    return NULL;
}

//
MVOID multiThreadVssCallback(QParams& rParams)
{
	printf("--- [multiThreadVss callback func]\n");

	g_multiThreadVssCallback = MTRUE;
}

int multiThreadVss(int type,int loopNum) //prv thread
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiThreadVss(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiThreadVss");
    printf("--- [multiThreadVss(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiThreadVss(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiThreadVss", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThreadVss(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiThreadVss(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiThreadVss(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiThreadVss", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiThreadVss", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
	g_multiThreadVssCallback = MFALSE;
	enqueParams.mpfnCallback = multiThreadVssCallback;
    printf("--- [multiThreadVss(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiThreadVss(%d_%d)...flush done\n]", type, i);


        //
        if(i==0)
        {
            //create vss capture thread
            mThread_loopNum=loopNum/3;
            mThread_type=type;
#ifdef __LP64__
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
            pthread_create(&mThread, &attr, vssCaptureThread, NULL);
        }

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThreadVss(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[multiThreadVss(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiThreadVss(%d)...enter while...........\n]", type);
       //while(1);
		do{
			usleep(100000);
			if (MTRUE == g_multiThreadVssCallback)
			{
				break;
			}
		}while(1);
		g_multiThreadVssCallback = MFALSE;


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [multiThreadVss(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[multiThreadVss(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_multiThreadVss_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_multiThreadVss_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [multiThreadVss(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [multiThreadVss(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiThreadVss");
    pStream->destroyInstance();
    printf("--- [multiThreadVss(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    printf("--- [multiThreadVss(%d)...wait mSem_ThreadEnd\n]", type);
    ::sem_wait(&mSem_ThreadEnd);

    return ret;
}


/*********************************************************************************/
//
int basicP2A_another(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_another(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_another");
    printf("--- [basicP2A_another(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_another(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_another", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_another(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_another(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_another(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_another", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_another", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicP2A_another(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_another(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_another(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_another(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_another(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_another(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_another(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicP2A_another(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_another(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_another");
    pStream->destroyInstance();
    printf("--- [basicP2A_another(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicMultiFrame_another( int type,int loopNum)
{
    int ret=0;
    printf("--- [basicMultiFrame_another(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicMultiFrame_another");
    printf("--- [basicMultiFrame_another(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicMultiFrame_another(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
   crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicMultiFrame_another(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicMultiFrame_another(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=1280, _imgi_h_frame2_=720;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=720, _imgi_h_frame3_=540;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push dst done\n]", type);

    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicMultiFrame_another(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame_another(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame_another(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicMultiFrame_another(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame_another(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame_another(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_another_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_another_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [basicMultiFrame_another_3(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_another_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_another_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [basicMultiFrame_another_3(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
    printf("--- [basicMultiFrame_another(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicMultiFrame_another");
    pStream->destroyInstance();
    printf("--- [basicMultiFrame_another(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


//
int multiProcess(int testNum, int type,int loopNum) //multi-process entry
{
    int ret=0;
    printf("--- [multiProcess(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);

    //create process
    pid_t child_pid;
    child_pid = fork ();

    switch(testNum)
    {
        case 5:
            if (child_pid != 0)
            {
                printf("[MultiProcess(%d_%d)...]this is the parent process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                printf("[MultiProcess(%d_%d)...]the child¡¦s process ID is 0x%x", type, testNum,(MUINT32) child_pid);
                ret=basicP2A(type, loopNum);
            }
            else
            {
                printf("[MultiProcess(%d_%d)...]this is the child process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                ret=basicP2A_another(type, loopNum);
            }
            break;
        case 6:
            if (child_pid != 0)
            {
                printf("[MultiProcess(%d_%d)...]this is the parent process, with id 0x%x",type, testNum,(MUINT32) getpid ());
                printf("[MultiProcess(%d_%d)...]the child¡¦s process ID is 0x%x", type, testNum,(MUINT32) child_pid);
                ret=basicMultiFrame(type, loopNum);
            }
            else
            {
                printf("[MultiProcess(%d_%d)...]this is the child process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                ret=basicMultiFrame_another(type, loopNum);
            }
            break;
        default:
            printf("--- [multiProcess(%d)...] this testNum_(%d) is not for multi-process\n", type, testNum);
            break;
    }

    return 0;
}


/*********************************************************************************/
//three same frames in a single enqueued package
int testSkipTpipe( int type,int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe");
    printf("--- [testSkipTpipe(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=720, _imgi_h_=540;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testSkipTpipe(%d)_frame1...push dst done\n]", type);

#if 1
    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [testSkipTpipe(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [testSkipTpipe(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [testSkipTpipe(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=720, _imgi_h_frame3_=540;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [testSkipTpipe(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [testSkipTpipe(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [testSkipTpipe(%d)_frame3...push dst done\n]", type);
#endif
    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        #if 1
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);
        #endif

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [testSkipTpipe(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe_1(%d_%d)...save file done\n]", type,i);
            //
            #if 1
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/dasystemta/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe_3(%d_%d)...save file done\n]", type,i);
            #endif
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe_1(%d_%d)...save file done\n]", type,i);
            //
            #if 1
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe_3(%d_%d)...save file done\n]", type,i);
            #endif
        }
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2);
#if 1
    srcBuffer_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
#endif
    printf("--- [testSkipTpipe(%d)...free memory done\n]", type);

    //
    pStream->uninit("testSkipTpipe");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
//three frames in a single enqueued package, the first and second frames are the same, and the third frame is different
int testSkipTpipe2( int type,int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe2(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe2");
    printf("--- [testSkipTpipe2(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=720, _imgi_h_=540;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe2(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe2(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testSkipTpipe2(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [testSkipTpipe2(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [testSkipTpipe2(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [testSkipTpipe2(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=1280, _imgi_h_frame3_=720;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [testSkipTpipe2(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [testSkipTpipe2(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [testSkipTpipe2(%d)_frame3...push dst done\n]", type);

    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe2(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe2(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe2(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [testSkipTpipe2(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe2(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe2(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe2_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe2_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe2_3(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe2_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe2_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe2_3(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
    printf("--- [testSkipTpipe2(%d)...free memory done\n]", type);

    //
    pStream->uninit("testSkipTpipe2");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe2(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "pic/imgi_1600x1200_bayer10.h"

//simulate isp always keep same setting, but mdp does not
int testSkipTpipe3(int type, int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe3...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe3");
    printf("--- [testSkipTpipe3...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe3...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe3...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe3...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe3...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    buf_out2.size=_output_2o_w_*_output_2o_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [testSkipTpipe3(%d)...push dst done\n]", type);

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    bool two_out=true;
    for(int i=0;i<loopNum;i++)
    {
        frameParams.mvOut.clear();

        if(i<3)
        {   //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==3)
        { //mdp 1 out
            frameParams.mvOut.push_back(dst);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            two_out=false;
        }
        else if(i==4)
        {
            //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==5)
        {
            //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==6)
        {//mdp 1 out
            frameParams.mvOut.push_back(dst);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            two_out=false;
        }
        else
        {//mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }


        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe3(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe3(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[testSkipTpipe3(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe3(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[testSkipTpipe3(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testSkipTpipe3_%d_process0x%x_package%d_wroto_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        if(two_out)
        {
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe3_%d_process0x%x_package%d_wdmao_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        }
        printf("--- [testSkipTpipe3(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [testSkipTpipe3...free memory done\n]");

    //
    pStream->uninit("testSkipTpipe3");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe3...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
//
#include "pic/imgi_1280x736_bayer10.h"

int testIspMdpVencDL( int type,int loopNum)
{
     int ret=0;
     printf("--- [testIspMdpVencDL(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
     NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
     pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
     pStream->init("testIspMdpVencDL");
     printf("--- [testIspMdpVencDL(%d)...pStream init done]\n", type);
     DipIMemDrv* mpImemDrv=NULL;
     mpImemDrv=DipIMemDrv::createInstance();
     mpImemDrv->init();

     QParams enqueParams;
     FrameParams frameParams;
     FrameParams frameParams2;

     /////////////////////////////////////////////////////////////////
     //frame 1
     //frame tag
     frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
     //input image
     MUINT32 _imgi_w_=1280, _imgi_h_=736;
     IMEM_BUF_INFO buf_imgi;
     buf_imgi.size=sizeof(g_imgi_array_1280x736_b10);
     mpImemDrv->allocVirtBuf(&buf_imgi);
     memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x736_b10), buf_imgi.size);
     //imem buffer 2 image heap
     printf("--- [testIspMdpVencDL(%d) frame1...flag -1 ]\n", type);
     IImageBuffer* srcBuffer;
     MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
     MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
     PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
     IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
     sp<ImageBufferHeap> pHeap;
     pHeap = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam,portBufInfo,true);
     srcBuffer = pHeap->createImageBuffer();
     srcBuffer->incStrong(srcBuffer);
     srcBuffer->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     printf("--- [testIspMdpVencDL(%d)_frame1...flag -8]\n", type);
     Input src;
     src.mPortID=PORT_IMGI;
     src.mBuffer=srcBuffer;
     src.mPortID.group=0;
     frameParams.mvIn.push_back(src);
     printf("--- [testIspMdpVencDL(%d)_frame1...push src done]\n", type);

    //crop information
     MCrpRsInfo crop;
     crop.mFrameGroup=0;
     crop.mGroupID=1;
     MCrpRsInfo crop2;
     crop2.mFrameGroup=0;
     crop2.mGroupID=2;
     MCrpRsInfo crop3;
     crop3.mFrameGroup=0;
     crop3.mGroupID=3;
     crop.mCropRect.p_fractional.x=0;
     crop.mCropRect.p_fractional.y=0;
     crop.mCropRect.p_integral.x=0;
     crop.mCropRect.p_integral.y=0;
     crop.mCropRect.s.w=_imgi_w_;
     crop.mCropRect.s.h=_imgi_h_;
     crop.mResizeDst.w=_imgi_w_;
     crop.mResizeDst.h=_imgi_h_;
     crop2.mCropRect.p_fractional.x=0;
     crop2.mCropRect.p_fractional.y=0;
     crop2.mCropRect.p_integral.x=0;
     crop2.mCropRect.p_integral.y=0;
     crop2.mCropRect.s.w=_imgi_w_;
     crop2.mCropRect.s.h=_imgi_h_;
     crop2.mResizeDst.w=_imgi_w_;
     crop2.mResizeDst.h=_imgi_h_;
     crop3.mCropRect.p_fractional.x=0;
     crop3.mCropRect.p_fractional.y=0;
     crop3.mCropRect.p_integral.x=0;
     crop3.mCropRect.p_integral.y=0;
     crop3.mCropRect.s.w=_imgi_w_;
     crop3.mCropRect.s.h=_imgi_h_;
     crop3.mResizeDst.w=_imgi_w_;
     crop3.mResizeDst.h=_imgi_h_;
     frameParams.mvCropRsInfo.push_back(crop);
     frameParams.mvCropRsInfo.push_back(crop2);
     frameParams.mvCropRsInfo.push_back(crop3);
     printf("--- [testIspMdpVencDL(%d)_frame1...push crop information done\n]", type);

     //output dma
     IMEM_BUF_INFO buf_out1;
     buf_out1.size=_imgi_w_*_imgi_h_*2;
     mpImemDrv->allocVirtBuf(&buf_out1);
     memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
     IImageBuffer* outBuffer=NULL;
     MUINT32 bufStridesInBytes_1[3] = {_imgi_w_,_imgi_w_/2, _imgi_w_/2};
     PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),
                                             MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 3);
     sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam_1,portBufInfo_1,true);
     outBuffer = pHeap_1->createImageBuffer();
     outBuffer->incStrong(outBuffer);
     outBuffer->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst;
     #if 0 //only support direct link
     if(type==0)
     {
         dst.mPortID=PORT_IMG2O;
     }
     else
     {
     #endif
         dst.mPortID=PORT_VENC_STREAMO;
     //}
     dst.mBuffer=outBuffer;
     dst.mPortID.group=0;
     frameParams.mvOut.push_back(dst);   

     IMEM_BUF_INFO buf_out2;
     buf_out2.size=_imgi_w_*_imgi_h_*2;
     mpImemDrv->allocVirtBuf(&buf_out2);
     memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
     IImageBuffer* outBuffer_2=NULL;
     MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
     PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
     sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam_2,portBufInfo_2,true);
     outBuffer_2 = pHeap_2->createImageBuffer();
     outBuffer_2->incStrong(outBuffer_2);
     outBuffer_2->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_2;
     if(type==0)
     {
         dst_2.mPortID=PORT_IMG3O;
     }
     else
     {
         dst_2.mPortID=PORT_WROTO;
     }
     dst_2.mBuffer=outBuffer_2;
     dst_2.mPortID.group=0;
     frameParams.mvOut.push_back(dst_2); 
     enqueParams.mvFrameParams.push_back(frameParams);
     printf("--- [testIspMdpVencDL(%d)_frame1...push dst done\n]", type);


     /////////////////////////////////////////////////////////////////
     //frame 2
//     enqueParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);
     //input image
     MUINT32 _imgi_w_frame2_=1280, _imgi_h_frame2_=736;
     IMEM_BUF_INFO buf_imgi_frame2;
     buf_imgi_frame2.size=sizeof(g_imgi_array_1280x736_b10);
     mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
     memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(g_imgi_array_1280x736_b10), buf_imgi_frame2.size);
     //imem buffer 2 image heap
     printf("--- [testIspMdpVencDL(%d) frame2...flag -1 ]\n", type);
     IImageBuffer* srcBuffer_frame2;
     MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
     MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
     PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
     sp<ImageBufferHeap> pHeap_frame2;
     pHeap_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_frame2,portBufInfo_frame2,true);
     srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
     srcBuffer_frame2->incStrong(srcBuffer_frame2);
     srcBuffer_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     printf("--- [testIspMdpVencDL(%d)_frame2...flag -8]\n", type);
     Input src_frame2;
     src_frame2.mPortID=PORT_IMGI;
     src_frame2.mBuffer=srcBuffer_frame2;
     src_frame2.mPortID.group=1;
//     frameParams2.mvIn.push_back(src_frame2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push src done]\n", type);
     //crop information
     MCrpRsInfo crop_frame2;
     crop_frame2.mFrameGroup=1;
     crop_frame2.mGroupID=1;
     MCrpRsInfo crop2_frame2;
     crop2_frame2.mFrameGroup=1;
     crop2_frame2.mGroupID=2;
     MCrpRsInfo crop3_frame2;
     crop3_frame2.mFrameGroup=1;
     crop3_frame2.mGroupID=3;
     crop_frame2.mCropRect.p_fractional.x=0;
     crop_frame2.mCropRect.p_fractional.y=0;
     crop_frame2.mCropRect.p_integral.x=0;
     crop_frame2.mCropRect.p_integral.y=0;
     crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop_frame2.mResizeDst.h=_imgi_h_frame2_;
     crop2_frame2.mCropRect.p_fractional.x=0;
     crop2_frame2.mCropRect.p_fractional.y=0;
     crop2_frame2.mCropRect.p_integral.x=0;
     crop2_frame2.mCropRect.p_integral.y=0;
     crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
     crop3_frame2.mCropRect.p_fractional.x=0;
     crop3_frame2.mCropRect.p_fractional.y=0;
     crop3_frame2.mCropRect.p_integral.x=0;
     crop3_frame2.mCropRect.p_integral.y=0;
     crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
     frameParams2.mvCropRsInfo.push_back(crop_frame2);
     frameParams2.mvCropRsInfo.push_back(crop2_frame2);
     frameParams2.mvCropRsInfo.push_back(crop3_frame2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push crop information done\n]", type);

     //output dma
     IMEM_BUF_INFO buf_out1_frame2;
     buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
     mpImemDrv->allocVirtBuf(&buf_out1_frame2);
     memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
     IImageBuffer* outBuffer_frame2=NULL;
     MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_, _imgi_w_frame2_/2, _imgi_w_frame2_/2};
     PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),
                                             MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 3);
     sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
     outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
     outBuffer_frame2->incStrong(outBuffer_frame2);
     outBuffer_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_frame2;
     #if 0
     if(type==0)
     {
         dst_frame2.mPortID=PORT_IMG2O;
     }
     else
     {
     #endif
         dst_frame2.mPortID=PORT_VENC_STREAMO;
     //}
     dst_frame2.mBuffer=outBuffer_frame2;
     dst_frame2.mPortID.group=1;
//     frameParams2.mvOut.push_back(dst_frame2);    

     IMEM_BUF_INFO buf_out2_frame2;
     buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
     mpImemDrv->allocVirtBuf(&buf_out2_frame2);
     memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
     IImageBuffer* outBuffer_2_frame2=NULL;
     MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
     PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
     sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
     outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
     outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
     outBuffer_2_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_2_frame2;
     if(type==0)
     {
         dst_2_frame2.mPortID=PORT_IMG3O;
     }
     else
     {
         dst_2_frame2.mPortID=PORT_WROTO;
     }
     dst_2_frame2.mBuffer=outBuffer_2_frame2;
     dst_2_frame2.mPortID.group=1;
//     frameParams2.mvOut.push_back(dst_2_frame2);  
     enqueParams.mvFrameParams.push_back(frameParams2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push dst done\n]", type);

    //
    ret=pStream->sendCommand(ESDCmd_CONFIG_VENC_DIRLK, 120, _imgi_w_,_imgi_h_);
    printf("--- [testIspMdpVencDL(%d)_ESDCmd_CONFIG_VENC_DIRLK done\n]", type);
    //
     for(int i=0;i<loopNum;i++)
     {
         memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0x00000000, buf_out1.size);
         memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0x00000000, buf_out2.size);
         //memset((MUINT8*)(enqueParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
         //memset((MUINT8*)(enqueParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

         /////////////////////////////////////////////////////////////////
         //buffer operation
         mpImemDrv->cacheFlushAll();
         printf("--- [testIspMdpVencDL(%d_%d)...flush done\n]", type,i);



         //enque
         ret=pStream->enque(enqueParams);
         if(!ret)
         {
             printf("---ERRRRRRRRR [testIspMdpVencDL(%d_%d)..enque fail\n]", type,i);
         }
         else
         {
             printf("---[testIspMdpVencDL(%d_%d)..enque done\n]", type,i);
         }

         //temp use while to observe in CVD
         //printf("--- [testIspMdpVencDL(%d)...enter while...........\n]", type);
        //while(1);


         //deque
         //wait a momet in fpga
         //usleep(5000000);
         QParams dequeParams;
         ret=pStream->deque(dequeParams);
         if(!ret)
         {
             printf("---ERRRRRRRRR [testIspMdpVencDL(%d_%d)..deque fail\n]", type,i);
         }
         else
         {
             printf("---[testIspMdpVencDL(%d_%d)..deque done\n]", type,i);
         }


         //dump image
         if(type==0)
         {
             char filename[256];
             sprintf(filename, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_venco_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             char filename2[256];
             sprintf(filename2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             printf("--- [testIspMdpVencDL_1(%d_%d)...save file done\n]", type,i);
             //
             #if 0
             char filename_frame2[256];
             sprintf(filename_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             char filename2_frame2[256];
             sprintf(filename2_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             #endif
             printf("--- [testIspMdpVencDL_2(%d_%d)...save file done\n]", type,i);
         }
         else
         {
             char filename[256];
             sprintf(filename, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_venco_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             char filename2[256];
             sprintf(filename2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             printf("--- [testIspMdpVencDL_1(%d_%d)...save file done\n]", type,i);
             //
             #if 0
             char filename_frame2[256];
             sprintf(filename_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             char filename2_frame2[256];
             sprintf(filename2_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             #endif
             printf("--- [testIspMdpVencDL_2(%d_%d)...save file done\n]", type,i);
         }
     }
     //
    ret=pStream->sendCommand(ESDCmd_RELEASE_VENC_DIRLK);
    printf("--- [testIspMdpVencDL(%d)ESDCmd_RELEASE_VENC_DIRLK done\n]", type);

     //free
     srcBuffer->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_imgi);
     outBuffer->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out1);
     outBuffer_2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out2);
     srcBuffer_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
     outBuffer_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out1_frame2);
     outBuffer_2_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out2_frame2);
     printf("--- [testIspMdpVencDL(%d)...free memory done\n]", type);

     //
     pStream->uninit("testIspMdpVencDL");
     pStream->destroyInstance();
     printf("--- [testIspMdpVencDL(%d)...pStream uninit done\n]", type);
     mpImemDrv->uninit();
     mpImemDrv->destroyInstance();

     return ret;

}


/*********************************************************************************/

int testpath(int type,int loopNum)
{
    int ret=0;
    printf("--- [testpath(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testpath");
    printf("--- [testpath(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testpath(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testpath", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testpath(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testpath(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
//    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
//    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testpath(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testpath", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testpath", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
//    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [testpath(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
//        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testpath(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testpath(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testpath(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [testpath(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testpath(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[testpath(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
//            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
//            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        }
        printf("--- [testpath(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [testpath(%d)...free memory done\n]", type);

    //
    pStream->uninit("testpath");
    pStream->destroyInstance();
    printf("--- [testpath(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
#include "pic/imgi_1600x1200_yuy2.h"

int basicP2A_yuvIn(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn");
    printf("--- [basicP2A_yuvIn(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicP2A_yuvIn2(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn2(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn2");
    printf("--- [basicP2A_yuvIn2(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn2(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn2(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn2(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn2(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn2(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn2(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn2(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn2(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn2(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn2(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn2(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn2_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn2_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn2(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn2(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn2");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn2(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicP2A_yuvIn3(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn3(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn3");
    printf("--- [basicP2A_yuvIn3(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn3(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn3(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn3(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn3(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn3(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn3(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn3(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn3(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn3(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn3(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn3(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn3_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn3_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn3(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn3(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn3");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn3(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


//
int basicP2AwithYUVinput(int type,int loopNum)
{
    int ret=0;
    //
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_yuvIn(loopNum); //wdmao + wroto
            break;
        case 1 :
            ret=basicP2A_yuvIn2(loopNum); //wdmao + img2o
            break;
        case 2 :
            ret=basicP2A_yuvIn3(loopNum); //wdmao + img3o
            break;
    }

    return ret;
}


/*********************************************************************************/


int P2ArawInwithFEO(int loopNum) //without srz1 enabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO");
    printf("--- [P2ArawInwithFEO...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO(%d)...push dst done\n]", type);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    MUINT32 _feo_w_ = _output_w_ /16 * 56;
    MUINT32 _feo_h_ = _output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    #if 0
    ModuleInfo fe_module;
    fe_module.moduleTag = EDipModule_FE;
    fe_module.frameGroup=0;
    _FE_REG_CFG_    *mpfeParam = new _FE_REG_CFG_;
    mpfeParam->fe_mode = 1;
    fe_module.moduleStruct   = reinterpret_cast<MVOID*> (mpfeParam);
    enqueParams.mvModuleData.push_back(fe_module);
    #else
    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_DFE, 0);
     //fe module via tuning
//    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);
    g_basicP2AwithFEOCallback = MFALSE;
    enqueParams.mpfnCallback = basicP2AwithFEOCallback;

    #endif

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithFEOCallback)
            {
                break;
            }
        }while(1);
        g_basicP2AwithFEOCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2ArawInwithFEO(%d)..deque fail\n]", i);
        //}
        //else
        {
            printf("---[P2ArawInwithFEO(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO2(int loopNum) //need revise, enable srz1 crop
{
    int ret=0;
    printf("--- [P2ArawInwithFEO2...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO2");
    printf("--- [P2ArawInwithFEO2...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO2...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO2...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO2...push src done]\n");

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=400;
    crop.mCropRect.p_integral.y=400;
    crop.mCropRect.s.w=_imgi_w_ - 400 - 200;
    crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO2...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO2(%d)...push dst done\n]", type);


    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_DFE, 0);
     //fe module via tuning
//    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);

    g_basicP2AwithFEOCallback = MFALSE;
    enqueParams.mpfnCallback = basicP2AwithFEOCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO2(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO2(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO2(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithFEOCallback)
            {
                break;
            }
        }while(1);
        g_basicP2AwithFEOCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2ArawInwithFEO2(%d)..deque fail\n]", i);
        //}
        //else
        {
            printf("---[P2ArawInwithFEO2(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ );
        printf("--- [P2ArawInwithFEO2(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    delete mpsrz1Param;
    //
    printf("--- [P2ArawInwithFEO2...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO2");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO2...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO3(int loopNum) //vsdof usage, with srz1 disabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO3...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO3");
    printf("--- [P2ArawInwithFEO3...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO3...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO3...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO3...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_img2o_;
    crop.mResizeDst.h=_output_h_img2o_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO3...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out1_wroto;
    buf_out1_wroto.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_wroto);
    memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
    IImageBuffer* outBuffer_wroto=NULL;
    MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_1_wroto,portBufInfo_1_wroto,true);
    outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
    outBuffer_wroto->incStrong(outBuffer_wroto);
    outBuffer_wroto->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_wroto;
    dst_wroto.mPortID=PORT_WROTO;
    dst_wroto.mBuffer=outBuffer_wroto;
    dst_wroto.mPortID.group=0;
    frameParams.mvOut.push_back(dst_wroto);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO3(%d)...push dst done\n]", type);

    IMEM_BUF_INFO buf_out2_img2o;
    buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_img2o);
    memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
    IImageBuffer* outBuffer_2_img2o=NULL;
    MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_2_img2o,portBufInfo_2_img2o,true);
    outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
    outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
    outBuffer_2_img2o->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_img2o;
    dst_2_img2o.mPortID=PORT_IMG2O;
    dst_2_img2o.mBuffer=outBuffer_2_img2o;
    dst_2_img2o.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_img2o);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    MUINT32 _feo_w_ = _output_w_ /16 * 56;
    MUINT32 _feo_h_ = _output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_DFE, 0);
     //fe module via tuning
//    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    g_basicP2AwithFEOCallback = MFALSE;
    enqueParams.mpfnCallback = basicP2AwithFEOCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO3(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO3(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO3(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithFEOCallback)
            {
                break;
            }
        }while(1);
        g_basicP2AwithFEOCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2ArawInwithFEO3(%d)..deque fail\n]", i);
        //}
        //else
        {
            printf("---[P2ArawInwithFEO3(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename_wroto[256];
        sprintf(filename_wroto, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2_img2o[256];
        sprintf(filename2_img2o, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
        saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO3(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_wroto->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out1_wroto);
    outBuffer_2->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_img2o->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out2_img2o);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO3...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO3");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO3...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO4(int loopNum) //vsdof usage, with srz1 enabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO4...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO4");
    printf("--- [P2ArawInwithFEO4...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO4...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO4...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO4...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_img2o_;
    crop.mResizeDst.h=_output_h_img2o_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO4...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out1_wroto;
    buf_out1_wroto.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_wroto);
    memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
    IImageBuffer* outBuffer_wroto=NULL;
    MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_1_wroto,portBufInfo_1_wroto,true);
    outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
    outBuffer_wroto->incStrong(outBuffer_wroto);
    outBuffer_wroto->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_wroto;
    dst_wroto.mPortID=PORT_WROTO;
    dst_wroto.mBuffer=outBuffer_wroto;
    dst_wroto.mPortID.group=0;
    frameParams.mvOut.push_back(dst_wroto);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO4(%d)...push dst done\n]", type);

    IMEM_BUF_INFO buf_out2_img2o;
    buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_img2o);
    memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
    IImageBuffer* outBuffer_2_img2o=NULL;
    MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_2_img2o,portBufInfo_2_img2o,true);
    outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
    outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
    outBuffer_2_img2o->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_img2o;
    dst_2_img2o.mPortID=PORT_IMG2O;
    dst_2_img2o.mBuffer=outBuffer_2_img2o;
    dst_2_img2o.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_img2o);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_DFE, 0);
     //fe module via tuning
//    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);

    g_basicP2AwithFEOCallback = MFALSE;
    enqueParams.mpfnCallback = basicP2AwithFEOCallback;


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO4(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO4(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO4(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithFEOCallback)
            {
                break;
            }
        }while(1);
        g_basicP2AwithFEOCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2ArawInwithFEO4(%d)..deque fail\n]", i);
        //}
        //else
        {
            printf("---[P2ArawInwithFEO4(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename_wroto[256];
        sprintf(filename_wroto, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2_img2o[256];
        sprintf(filename2_img2o, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
        saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO4(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_wroto->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out1_wroto);
    outBuffer_2->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_img2o->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out2_img2o);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO4...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO4");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO4...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

MVOID basicP2AwithFEOCallback(QParams& rParams)
{
    printf("--- [basicP2AwithFEO callback func]\n");

    //char filename[256];
    //sprintf(filename, "/data/P2iopipe_basicP2AwithFEO_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
    //char filename2[256];
    //sprintf(filename2, "/data/P2iopipe_basicP2AwithFEO_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename2, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);

    g_basicP2AwithFEOCallback = MTRUE;
}
//
int basicP2AwithFEO(int type,int loopNum)
{
    int ret=0;
    //
    switch(type)
    {
        case 0 :
        case 1 :
        default:
            ret=P2ArawInwithFEO(loopNum); //for eis, with srz1 disabled
            break;
        case 2 :
            ret=P2ArawInwithFEO3(loopNum); //for vsdof, with srz1 disabled
            break;
        case 3 :
            ret=P2ArawInwithFEO4(loopNum); //for vsdof, with srz1 disabled
            break;
    }

    return ret;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
int basicP2A_isponly_withTpipe(int type, int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_isponly_withTpipe...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_isponly_withTpipe");
    printf("--- [basicP2A_isponly_withTpipe...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_isponly_withTpipe...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_isponly_withTpipe...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_isponly_withTpipe...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    if(type==0)
    {
        crop.mCropRect.p_fractional.x=0;
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=0;
        crop.mCropRect.p_integral.y=0;
        crop.mCropRect.s.w=_imgi_w_;
        crop.mCropRect.s.h=_imgi_h_;
        crop.mResizeDst.w=_imgi_w_;
        crop.mResizeDst.h=_imgi_h_;
    }
    else
    {
        crop.mCropRect.p_fractional.x=0;  //for img2o
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=600;
        crop.mCropRect.p_integral.y=400;
        crop.mCropRect.s.w=_imgi_w_ - 600 - 200;
        crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_isponly_withTpipe...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    if(type!=0)
    {
        buf_out2.size=_output_2o_w_*_output_2o_h_*2;
        mpImemDrv->allocVirtBuf(&buf_out2);
        memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
        MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
        PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
        IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
        sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam_2,portBufInfo_2,true);
        outBuffer_2 = pHeap_2->createImageBuffer();
        outBuffer_2->incStrong(outBuffer_2);
        outBuffer_2->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
        Output dst_2;
        dst_2.mPortID=PORT_IMG2O;
        dst_2.mBuffer=outBuffer_2;
        dst_2.mPortID.group=0;
        frameParams.mvOut.push_back(dst_2);	
    //    printf("--- [basicP2A_isponly_withTpipe(%d)...push dst done\n]", type);
    }

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        if(type!=0)
        {
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        }

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_isponly_withTpipe(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_isponly_withTpipe(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_isponly_withTpipe(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_isponly_withTpipe(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_isponly_withTpipe(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_isponly_withTpipe_%d_process0x%x_package%d_img3o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        if(type!=0)
        {
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_isponly_withTpipe_%d_process0x%x_package%d_img2o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        }
        printf("--- [basicP2A_isponly_withTpipe(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_isponly_withTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_isponly_withTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1);
    if(type!=0)
    {
        outBuffer_2->unlockBuf("basicP2A_isponly_withTpipe");
        mpImemDrv->freeVirtBuf(&buf_out2);
    }
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [basicP2A_isponly_withTpipe...free memory done\n]");

    //
    pStream->uninit("basicP2A_isponly_withTpipe");
    pStream->destroyInstance();
    printf("--- [basicP2A_isponly_withTpipe...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////

//
int testTPIPECtrlExtension_isponly(int type,int loopNum)
{
    int ret=0;
    //
    printf("--- [testTPIPECtrlExtension_isponly] type(%d) _ start\n]",type);
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_isponly_withTpipe(0, loopNum);
            break;
        case 1 :
            ret=basicP2A_isponly_withTpipe(1, loopNum);
            break;
        case 2:
            ret=P2ArawInwithFEO2(loopNum);
            break;
    }
    printf("--- [testTPIPECtrlExtension_isponly] type(%d) _ end\n]",type);

    return ret;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////
int basicP2A_differentViewAngle(int type, int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_differentViewAngle...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_differentViewAngle");
    printf("--- [basicP2A_differentViewAngle...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_differentViewAngle...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_differentViewAngle...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_differentViewAngle...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _mdp_output_w_=1280, _mdp_output_h_=960;
    MUINT32 _mdp_output_w_2=1080, _mdp_output_h_2=720;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    if(type==0)
    {
        crop.mCropRect.p_fractional.x=0;
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=0;
        crop.mCropRect.p_integral.y=0;
        crop.mCropRect.s.w=_imgi_w_;
        crop.mCropRect.s.h=_imgi_h_;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    else
    {
        crop.mCropRect.p_fractional.x=0;  //for img2o
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=600;
        crop.mCropRect.p_integral.y=400;
        crop.mCropRect.s.w=_imgi_w_ - 600 - 200;
        crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=300;
    crop2.mCropRect.p_integral.y=200;
    crop2.mCropRect.s.w=_imgi_w_ - 300 - 400;
    crop2.mCropRect.s.h=_imgi_h_ - 200 - 400;
    crop2.mResizeDst.w=_mdp_output_w_;
    crop2.mResizeDst.h=_mdp_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=500;
    crop3.mCropRect.p_integral.y=300;
    crop3.mCropRect.s.w=_imgi_w_ - 500 - 400;
    crop3.mCropRect.s.h=_imgi_h_ - 300 - 400;
    crop3.mResizeDst.w=_mdp_output_w_2;
    crop3.mResizeDst.h=_mdp_output_h_2;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_differentViewAngle...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_mdp_output_w_*_mdp_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mdp_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_,_mdp_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    //output dma
    IMEM_BUF_INFO buf_out1_1;
    buf_out1_1.size=_mdp_output_w_2*_mdp_output_h_2*2;
    mpImemDrv->allocVirtBuf(&buf_out1_1);
    memset((MUINT8*)buf_out1_1.virtAddr, 0xffffffff, buf_out1_1.size);
    IImageBuffer* outBuffer_1_1=NULL;
    MUINT32 bufStridesInBytes_1_1[3] = {_mdp_output_w_2*2,0,0};
    PortBufInfo_v1 portBufInfo_1_1 = PortBufInfo_v1( buf_out1_1.memID,buf_out1_1.virtAddr,0,buf_out1_1.bufSecu, buf_out1_1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_2,_mdp_output_h_2),  bufStridesInBytes_1_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_1_1,portBufInfo_1_1,true);
    outBuffer_1_1 = pHeap_1_1->createImageBuffer();
    outBuffer_1_1->incStrong(outBuffer_1_1);
    outBuffer_1_1->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_1_1;
    dst_1_1.mPortID=PORT_WROTO;
    dst_1_1.mBuffer=outBuffer_1_1;
    dst_1_1.mPortID.group=0;
    frameParams.mvOut.push_back(dst_1_1);	

    //
    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    buf_out2.size=_output_2o_w_*_output_2o_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	

    //
    IMEM_BUF_INFO buf_out2_1;
    IImageBuffer* outBuffer_2_1=NULL;
    buf_out2_1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_1);
    memset((MUINT8*)buf_out2_1.virtAddr, 0xffffffff, buf_out2_1.size);
    MUINT32 bufStridesInBytes_2_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_1 = PortBufInfo_v1( buf_out2_1.memID,buf_out2_1.virtAddr,0,buf_out2_1.bufSecu, buf_out2_1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_2_1,portBufInfo_2_1,true);
    outBuffer_2_1 = pHeap_2_1->createImageBuffer();
    outBuffer_2_1->incStrong(outBuffer_2_1);
    outBuffer_2_1->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_1;
    dst_2_1.mPortID=PORT_IMG3O;
    dst_2_1.mBuffer=outBuffer_2_1;
    dst_2_1.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_1);	
//    printf("--- [basicP2A_differentViewAngle(%d)...push dst done\n]", type);

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_1.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_1.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_differentViewAngle(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_differentViewAngle(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_differentViewAngle(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_differentViewAngle(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_differentViewAngle(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_wmdao_%dx%d.yuv",type, (MUINT32) getpid (),i, _mdp_output_w_,_mdp_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mdp_output_w_ *_mdp_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_wroto_%dx%d.yuv",type, (MUINT32) getpid (),i, _mdp_output_w_2, _mdp_output_h_2);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _mdp_output_w_2 *_mdp_output_h_2 * 2);

        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_img2o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        char filename4[256];
        sprintf(filename4, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_img3o_%dx%d.yuv",type, (MUINT32) getpid (),i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename4, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [basicP2A_differentViewAngle(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_1_1->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1_1);
    outBuffer_2->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_1->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2_1);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [basicP2A_differentViewAngle...free memory done\n]");

    //
    pStream->uninit("basicP2A_differentViewAngle");
    pStream->destroyInstance();
    printf("--- [basicP2A_differentViewAngle...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2AwithFEO_differentViewAngle(int loopNum) //need revise, enable srz1 crop
{
    int ret=0;
    printf("--- [P2AwithFEO_differentViewAngle...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2AwithFEO_differentViewAngle");
    printf("--- [P2AwithFEO_differentViewAngle...pStream init done]\n");
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;    

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2AwithFEO_differentViewAngle...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2AwithFEO_differentViewAngle...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2AwithFEO_differentViewAngle...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _mdp_output_w_=1280, _mdp_output_h_=960;
    MUINT32 _mdp_output_w_2=1080, _mdp_output_h_2=720;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=400;
    crop.mCropRect.p_integral.y=400;
    crop.mCropRect.s.w=_imgi_w_ - 400 - 200;
    crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=300;
    crop2.mCropRect.p_integral.y=100;
    crop2.mCropRect.s.w=_output_w_ - 300;
    crop2.mCropRect.s.h=_output_h_ - 100;
    crop2.mResizeDst.w=_mdp_output_w_;
    crop2.mResizeDst.h=_mdp_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=500;
    crop3.mCropRect.p_integral.y=600;
    crop3.mCropRect.s.w=_output_w_ - 500;
    crop3.mCropRect.s.h=_output_h_ - 600;
    crop3.mResizeDst.w=_mdp_output_w_2;
    crop3.mResizeDst.h=_mdp_output_h_2;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2AwithFEO_differentViewAngle...push crop information done\n]");

    //mdp output dma
    IMEM_BUF_INFO mdp_buf_out1;
    mdp_buf_out1.size=_mdp_output_w_*_mdp_output_h_*2;
    mpImemDrv->allocVirtBuf(&mdp_buf_out1);
    memset((MUINT8*)mdp_buf_out1.virtAddr, 0xffffffff, mdp_buf_out1.size);
    IImageBuffer* mdp_outBuffer=NULL;
    MUINT32 mdp_bufStridesInBytes_1[3] = {_mdp_output_w_*2,0,0};
    PortBufInfo_v1 mdp_portBufInfo_1 = PortBufInfo_v1( mdp_buf_out1.memID,mdp_buf_out1.virtAddr,0,mdp_buf_out1.bufSecu, mdp_buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam mdp_imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_,_mdp_output_h_),  mdp_bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> mdp_pHeap_1 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", mdp_imgParam_1,mdp_portBufInfo_1,true);
    mdp_outBuffer = mdp_pHeap_1->createImageBuffer();
    mdp_outBuffer->incStrong(mdp_outBuffer);
    mdp_outBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output mdp_dst;
    mdp_dst.mPortID=PORT_WDMAO;
    mdp_dst.mBuffer=mdp_outBuffer;
    mdp_dst.mPortID.group=0;
    frameParams.mvOut.push_back(mdp_dst);   
    //
    IMEM_BUF_INFO mdp_buf_out2;
    mdp_buf_out2.size=_mdp_output_w_2*_mdp_output_h_2*2;
    mpImemDrv->allocVirtBuf(&mdp_buf_out2);
    memset((MUINT8*)mdp_buf_out2.virtAddr, 0xffffffff, mdp_buf_out2.size);
    IImageBuffer* mdp_outBuffer2=NULL;
    MUINT32 mdp_bufStridesInBytes_2[3] = {_mdp_output_w_2*2,0,0};
    PortBufInfo_v1 mdp_portBufInfo_2 = PortBufInfo_v1( mdp_buf_out2.memID,mdp_buf_out2.virtAddr,0,mdp_buf_out2.bufSecu, mdp_buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam mdp_imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_2,_mdp_output_h_2),  mdp_bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> mdp_pHeap_2 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", mdp_imgParam_2,mdp_portBufInfo_2,true);
    mdp_outBuffer2 = mdp_pHeap_2->createImageBuffer();
    mdp_outBuffer2->incStrong(mdp_outBuffer2);
    mdp_outBuffer2->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output mdp_dst2;
    mdp_dst2.mPortID=PORT_WROTO;
    mdp_dst2.mBuffer=mdp_outBuffer2;
    mdp_dst2.mPortID.group=0;
    frameParams.mvOut.push_back(mdp_dst2);   


    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2AwithFEO_differentViewAngle(%d)...push dst done\n]", type);


    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
//    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, mdp_buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, mdp_buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2AwithFEO_differentViewAngle(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2AwithFEO_differentViewAngle(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2AwithFEO_differentViewAngle(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2AwithFEO_differentViewAngle(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[P2AwithFEO_differentViewAngle(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _mdp_output_w_,_mdp_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mdp_output_w_ *_mdp_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _mdp_output_w_2,_mdp_output_h_2);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _mdp_output_w_2 *_mdp_output_h_2 * 2);

        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename4[256];
        sprintf(filename4, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename4, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename5[256];
        sprintf(filename5, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename5, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ );
        printf("--- [P2AwithFEO_differentViewAngle(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    mdp_outBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&mdp_buf_out1);
    mdp_outBuffer2->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&mdp_buf_out2);
    outBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    delete mpsrz1Param;
    //
    printf("--- [P2AwithFEO_differentViewAngle...free memory done\n]");

    //
    pStream->uninit("P2AwithFEO_differentViewAngle");
    pStream->destroyInstance();
    printf("--- [P2AwithFEO_differentViewAngle...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int testTPIPECtrlExtension_directlink(int type,int loopNum)
{
    int ret=0;
    //
    printf("--- [testTPIPECtrlExtension_directlink] type(%d) _ start\n]",type);
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_differentViewAngle(1, loopNum);
            break;
        case 1:
            ret=P2AwithFEO_differentViewAngle(loopNum);
            break;
    }
    printf("--- [testTPIPECtrlExtension_directlink] type(%d) _ end\n]",type);

    return ret;
}

/////////////////////////////////////////////////////////////////////////////////////
int multiVss_test(int type,int loopNum)
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiVss_test(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiVss_test");
    printf("--- [multiVss_test(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiVss_test(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiVss_test", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiVss_test(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiVss_test(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiVss_test(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiVss_test", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiVss_test", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [multiVss_test(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiVss_test(%d_%d)...flush done\n]", type, i);

        if(i==0)
        {
            //create vss capture thread
            mThread_loopNum=loopNum;
            mThread_type=type;
#ifdef __LP64__
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
            pthread_create(&mThread, &attr, vssCaptureThread, NULL);
            usleep(130000);//wait another thread config done
        }

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiVss_test(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[multiVss_test(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiVss_test(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiVss_test(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[multiVss_test(%d_%d)..deque done\n]", type, i);
        }

        if(i==(loopNum-1))
        {
        //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiVss_test_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiVss_test_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiVss_test(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [multiVss_test(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiVss_test");
    pStream->destroyInstance();
    printf("--- [multiVss_test(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    ::sem_wait(&mSem_ThreadEnd);
    return ret;
}

#include "pic/UFO/imgi_fg_960_540_10.h"
#include "pic/UFO/ufdi_16_540_10.h"
#include "pic/UFO/imgi_fg_960_540_12.h"
#include "pic/UFO/ufdi_16_540_12.h"
// Twin
#include "pic/UFO/imgi_fg_1920_1080_10.h"
#include "pic/UFO/ufdi_32_1080_10.h"
#include "pic/UFO/imgi_fg_1920_1080_12.h"
#include "pic/UFO/ufdi_32_1080_12.h"
MVOID basicP2AwithFullG_UFO_inCallback(QParams& rParams)
{
    printf("--- [basicP2AwithFullG_UFO_in callback func]\n");

    g_basicP2AwithFullG_UFO_inCallback = MTRUE;
}

int basicP2AwithFullG_UFO_in(int type,int loopNum)
{
	int ret=0;
	printf("--- [basicP2AwithFullG_UFO_in...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicP2AwithFullG_UFO_in");
	printf("--- [basicP2AwithFullG_UFO_in...pStream init done]\n");
	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
    FrameParams frameParams;

    UFDG_META_INFO pUfdParam;

	//frame tag
	frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

	MUINT32 _imgi_w_, _imgi_h_, _ufdi_w_, _ufdi_h_;
	IMEM_BUF_INFO buf_imgi;
	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
	IImageBuffer* srcBuffer;
	//IMEM_BUF_INFO buf_ufdi;
	//IImageBuffer* srcBuffer_ufdi;
	MUINT32 stride_imgi;
	if (type==0)
	{
		//input image(imgi)
		_imgi_w_=960;//1600;
		_imgi_h_=540;//1200;
		_ufdi_w_=16;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_960_540_10) + sizeof(ufo_fg_g_ufdi_array_16_540_10) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_960_540_10), sizeof(ufo_fg_g_imgi_array_960_540_10));
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_960_540_10)), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_10), sizeof(ufo_fg_g_ufdi_array_16_540_10));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*10/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0;
        pUfdParam.UFDG_BS_AU_START = 0;
        pUfdParam.UFDG_AU2_SIZE = 0;
        pUfdParam.UFDG_BOND_MODE = 1;
		
		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_960_540_10);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
		
		MINT reqImgFormat = eImgFmt_UFO_FG_BAYER10;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		#if 0
		//input dma(ufdi)
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_10);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_10), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
		#endif
	}
	else if (type==1)
	{
		//input image(imgi)
		_imgi_w_=960;//1600;
		_imgi_h_=540;//1200;
		_ufdi_w_=16;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_960_540_12)+sizeof(ufo_fg_g_ufdi_array_16_540_12) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_960_540_12), buf_imgi.size);
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_960_540_12)), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_12), sizeof(ufo_fg_g_ufdi_array_16_540_12));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*12/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0;
        pUfdParam.UFDG_BS_AU_START = 0;
        pUfdParam.UFDG_AU2_SIZE = 0;
        pUfdParam.UFDG_BOND_MODE = 1;

		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_960_540_12);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);

		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG_BAYER12),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);

		MINT reqImgFormat = eImgFmt_UFO_FG_BAYER12;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		//input dma(ufdi)
		#if 0
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_12);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_12), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
		#endif
	}
	else if (type==2)
	{
		//input image(imgi)
		_imgi_w_=1920;//1600;
		_imgi_h_=1080;//1200;
		_ufdi_w_=32;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_1920_1080_10) + sizeof(ufo_fg_g_ufdi_array_32_1080_10) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_1920_1080_10), sizeof(ufo_fg_g_imgi_array_1920_1080_10));
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_1920_1080_10)), (MUINT8*)(ufo_fg_g_ufdi_array_32_1080_10), sizeof(ufo_fg_g_ufdi_array_32_1080_10));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*10/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0x780;
        pUfdParam.UFDG_BS_AU_START = 0x10;
        pUfdParam.UFDG_AU2_SIZE = 0xE;
        pUfdParam.UFDG_BOND_MODE = 0x1;

		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_1920_1080_10);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
		
		MINT reqImgFormat = eImgFmt_UFO_FG_BAYER10;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		#if 0
		//input dma(ufdi)
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_10);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_10), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
		#endif
	}
	else if (type==3)
	{
		//input image(imgi)
		_imgi_w_=1920;//1600;
		_imgi_h_=1080;//1200;
		_ufdi_w_=32;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_1920_1080_12) + sizeof(ufo_fg_g_ufdi_array_32_1080_12) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_1920_1080_12), sizeof(ufo_fg_g_imgi_array_1920_1080_12));
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_1920_1080_12)), (MUINT8*)(ufo_fg_g_ufdi_array_32_1080_12), sizeof(ufo_fg_g_ufdi_array_32_1080_12));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*12/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0x870;
        pUfdParam.UFDG_BS_AU_START = 0x0F;
        pUfdParam.UFDG_AU2_SIZE = 0xF;
        pUfdParam.UFDG_BOND_MODE = 0x1;

		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_fg_g_imgi_array_1920_1080_12);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG_BAYER12),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
		
		MINT reqImgFormat = eImgFmt_UFO_FG_BAYER12;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		#if 0
		//input dma(ufdi)
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_10);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_10), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
		#endif
	}
	printf("--- [basicP2AwithFullG_UFO_in...push src done]\n");

	//
	MUINT32 _output_w_=_imgi_w_, _output_h_=_imgi_h_;
	MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
	//crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_output_w_img2o_;
	crop.mResizeDst.h=_output_h_img2o_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_output_w_;
	crop2.mResizeDst.h=_output_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_output_w_;
	crop3.mResizeDst.h=_output_h_;
	frameParams.mvCropRsInfo.push_back(crop);
	frameParams.mvCropRsInfo.push_back(crop2);
	frameParams.mvCropRsInfo.push_back(crop3);
	printf("--- [basicP2AwithFullG_UFO_in...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst;
	dst.mPortID=PORT_WDMAO;
	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams.mvOut.push_back(dst);

	IMEM_BUF_INFO buf_out1_wroto;
	buf_out1_wroto.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1_wroto);
	memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
	IImageBuffer* outBuffer_wroto=NULL;
	MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1_wroto,portBufInfo_1_wroto,true);
	outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
	outBuffer_wroto->incStrong(outBuffer_wroto);
	outBuffer_wroto->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_wroto;
	dst_wroto.mPortID=PORT_WROTO;
	dst_wroto.mBuffer=outBuffer_wroto;
	dst_wroto.mPortID.group=0;
	frameParams.mvOut.push_back(dst_wroto);

	IMEM_BUF_INFO buf_out2;
	buf_out2.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out2);
	memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
	IImageBuffer* outBuffer_2=NULL;
	MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2,portBufInfo_2,true);
	outBuffer_2 = pHeap_2->createImageBuffer();
	outBuffer_2->incStrong(outBuffer_2);
	outBuffer_2->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2;
	dst_2.mPortID=PORT_IMG3O;
	dst_2.mBuffer=outBuffer_2;
	dst_2.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2);
	//	  printf("--- [basicP2AwithFullG_UFO_in(%d)...push dst done\n]", type);

	IMEM_BUF_INFO buf_out2_img2o;
	buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
	mpImemDrv->allocVirtBuf(&buf_out2_img2o);
	memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
	IImageBuffer* outBuffer_2_img2o=NULL;
	MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
	PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2_img2o,portBufInfo_2_img2o,true);
	outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
	outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
	outBuffer_2_img2o->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2_img2o;
	dst_2_img2o.mPortID=PORT_IMG2O;
	dst_2_img2o.mBuffer=outBuffer_2_img2o;
	dst_2_img2o.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2_img2o);
#if 0
	//feo, set fe_mode as 1
	IMEM_BUF_INFO buf_out_feo;
	MUINT32 _feo_output_w_=800, _feo_output_h_=640;
	MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
	MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
	buf_out_feo.size=_feo_w_*_feo_h_;
	mpImemDrv->allocVirtBuf(&buf_out_feo);
	memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
	IImageBuffer* outBuffer_feo=NULL;
	MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
	PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_feo,portBufInfo_feo,true);
	outBuffer_feo = pHeap_feo->createImageBuffer();
	outBuffer_feo->incStrong(outBuffer_feo);
	outBuffer_feo->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_feo;
	dst_feo.mPortID=PORT_FEO;
	dst_feo.mBuffer=outBuffer_feo;
	dst_feo.mPortID.group=0;
	frameParams.mvOut.push_back(dst_feo);
#endif
#if 0
	IMEM_BUF_INFO pTuningQueBuf;
	pTuningQueBuf.size = sizeof(dip_x_reg_t);
	mpImemDrv->allocVirtBuf(&pTuningQueBuf);
	mpImemDrv->mapPhyAddr(&pTuningQueBuf);
	memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
	dip_x_reg_t *pIspReg;
	pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
	printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
	 //fe module via tuning
//	pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;	//fe mode =1
	frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
#endif
#if 0
	//srz1 config
	ModuleInfo srz1_module;
	srz1_module.moduleTag = EDipModule_SRZ1;
	srz1_module.frameGroup=0;
	_SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
	mpsrz1Param->in_w = _imgi_w_;
	mpsrz1Param->in_h = _imgi_h_;
	mpsrz1Param->crop_floatX = 0;
	mpsrz1Param->crop_floatY = 0;
	mpsrz1Param->crop_x = 100;
	mpsrz1Param->crop_y = 100;
	mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
	mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
	mpsrz1Param->out_w = _feo_output_w_;
	mpsrz1Param->out_h = _feo_output_h_;
	srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
	frameParams.mvModuleData.push_back(srz1_module);
#endif
    enqueParams.mvFrameParams.push_back(frameParams);
	g_basicP2AwithFullG_UFO_inCallback = MFALSE;
	enqueParams.mpfnCallback = basicP2AwithFullG_UFO_inCallback;

	for(int i=0;i<loopNum;i++)
	{
		memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
		memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
		memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
		//memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

		//buffer operation
		mpImemDrv->cacheFlushAll();
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flush done\n]", i);

		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
			printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d)..enque fail\n]", i);
		}
		else
		{
			printf("---[basicP2AwithFullG_UFO_in(%d)..enque done\n]", i);
		}

		//deque
		//wait a momet in fpga
		//usleep(5000000);
		do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithFullG_UFO_inCallback)
		{
                break;
		}
        }while(1);
        g_basicP2AwithFullG_UFO_inCallback = MFALSE;
		
		//QParams dequeParams;
		//ret=pStream->deque(dequeParams);
		//if(!ret)
		//{
		//	printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d)..deque fail\n]", i);
		//}
		//else
		{
			printf("---[basicP2AwithFullG_UFO_in(%d)..deque done\n]", i);
		}


		//dump image
		if (type==0)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG10_Single_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG10_Single_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG10_Single_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG10_Single_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		else if (type==1)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG12_Single_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG12_Single_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG12_Single_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG12_Single_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		else if (type==2)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG10_Twin_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG10_Twin_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG10_Twin_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG10_Twin_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		else if (type==3)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG12_Twin_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG12_Twin_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG12_Twin_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG12_Twin_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		printf("--- [basicP2AwithFullG_UFO_in(%d)...save file done\n]",i);
	}

	//free
	srcBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_imgi);
	//srcBuffer_ufdi->unlockBuf("basicP2AwithFullG_UFO_in");
	//mpImemDrv->freeVirtBuf(&buf_ufdi);
	outBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1);
	outBuffer_wroto->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1_wroto);
	outBuffer_2->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2);
	outBuffer_2_img2o->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2_img2o);
	//outBuffer_feo->unlockBuf("basicP2AwithFullG_UFO_in");
	//mpImemDrv->freeVirtBuf(&buf_out_feo);
	//mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
	//mpImemDrv->freeVirtBuf(&pTuningQueBuf);
	//
	printf("--- [basicP2AwithFullG_UFO_in...free memory done\n]");

	//
	pStream->uninit("basicP2AwithFullG_UFO_in");
	pStream->destroyInstance();
	printf("--- [basicP2AwithFullG_UFO_in...pStream uninit done\n]");
	mpImemDrv->uninit();
	mpImemDrv->destroyInstance();

	return ret;

#if 0
	 int ret=0;
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
	 NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	 pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	 pStream->init("basicP2AwithFullG_UFO_in");
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...pStream init done]\n", type);
	 DipIMemDrv* mpImemDrv=NULL;
	 mpImemDrv=DipIMemDrv::createInstance();
	 mpImemDrv->init();

	 QParams enqueParams;

	 //frame tag
	 enqueParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	 //input image(imgi)
	 int _imgi_w_=640, _imgi_h_=480;
	 IMEM_BUF_INFO buf_imgi;
	 buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
	 mpImemDrv->allocVirtBuf(&buf_imgi);
	 memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
	 //imem buffer 2 image heap
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
	 IImageBuffer* srcBuffer;
	 MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
	 MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
	 PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap;
	 pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
	 srcBuffer = pHeap->createImageBuffer();
	 srcBuffer->incStrong(srcBuffer);
	 srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
	 Input src;
	 src.mPortID=PORT_IMGI;
	 src.mBuffer=srcBuffer;
	 src.mPortID.group=0;
	 enqueParams.mvIn.push_back(src);

	 //input dma(ufdi)
	 int _ufdi_w_=16, _ufdi_h_=480;
	 IMEM_BUF_INFO buf_ufdi;
	 buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_480_10);
	 mpImemDrv->allocVirtBuf(&buf_ufdi);
	 memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_480_10), buf_ufdi.size);
	 //imem buffer 2 image heap
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
	 IImageBuffer* srcBuffer_ufdi;
	 MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
	 MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
	 PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
	 sp<ImageBufferHeap> pHeap_ufdi;
	 pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
	 srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
	 srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
	 srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
	 Input src_ufdi;
	 src_ufdi.mPortID=PORT_UFDI;
	 src_ufdi.mBuffer=srcBuffer_ufdi;
	 src_ufdi.mPortID.group=0;
	 enqueParams.mvIn.push_back(src_ufdi);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push src done]\n", type);

	//crop information
	 MCrpRsInfo crop;
	 crop.mFrameGroup=0;
	 crop.mGroupID=1;
	 MCrpRsInfo crop2;
	 crop2.mFrameGroup=0;
	 crop2.mGroupID=2;
	 MCrpRsInfo crop3;
	 crop3.mFrameGroup=0;
	 crop3.mGroupID=3;
	 crop.mCropRect.p_fractional.x=0;
	 crop.mCropRect.p_fractional.y=0;
	 crop.mCropRect.p_integral.x=0;
	 crop.mCropRect.p_integral.y=0;
	 crop.mCropRect.s.w=_imgi_w_;
	 crop.mCropRect.s.h=_imgi_h_;
	 crop.mResizeDst.w=_imgi_w_;
	 crop.mResizeDst.h=_imgi_h_;
	 crop2.mCropRect.p_fractional.x=0;
	 crop2.mCropRect.p_fractional.y=0;
	 crop2.mCropRect.p_integral.x=0;
	 crop2.mCropRect.p_integral.y=0;
	 crop2.mCropRect.s.w=_imgi_w_;
	 crop2.mCropRect.s.h=_imgi_h_;
	 crop2.mResizeDst.w=_imgi_w_;
	 crop2.mResizeDst.h=_imgi_h_;
	 crop3.mCropRect.p_fractional.x=0;
	 crop3.mCropRect.p_fractional.y=0;
	 crop3.mCropRect.p_integral.x=0;
	 crop3.mCropRect.p_integral.y=0;
	 crop3.mCropRect.s.w=_imgi_w_;
	 crop3.mCropRect.s.h=_imgi_h_;
	 crop3.mResizeDst.w=_imgi_w_;
	 crop3.mResizeDst.h=_imgi_h_;
	 enqueParams.mvCropRsInfo.push_back(crop);
	 enqueParams.mvCropRsInfo.push_back(crop2);
	 enqueParams.mvCropRsInfo.push_back(crop3);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push crop information done\n]", type);

	 //output dma
	 IMEM_BUF_INFO buf_out1;
	 buf_out1.size=_imgi_w_*_imgi_h_*2;
	 mpImemDrv->allocVirtBuf(&buf_out1);
	 memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
	 IImageBuffer* outBuffer=NULL;
	 MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
	 PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											 MSize(_imgi_w_,_imgi_h_),	bufStridesInBytes_1, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1,portBufInfo_1,true);
	 outBuffer = pHeap_1->createImageBuffer();
	 outBuffer->incStrong(outBuffer);
	 outBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 Output dst;
	 if(type==0)
	 {
		 dst.mPortID=PORT_IMG2O;
	 }
	 else
	 {
		 dst.mPortID=PORT_WDMAO;
	 }
	 dst.mBuffer=outBuffer;
	 dst.mPortID.group=0;
	 enqueParams.mvOut.push_back(dst);

	 IMEM_BUF_INFO buf_out2;
	 buf_out2.size=_imgi_w_*_imgi_h_*2;
	 mpImemDrv->allocVirtBuf(&buf_out2);
	 memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
	 IImageBuffer* outBuffer_2=NULL;
	 MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
	 PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),	bufStridesInBytes_2, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2,portBufInfo_2,true);
	 outBuffer_2 = pHeap_2->createImageBuffer();
	 outBuffer_2->incStrong(outBuffer_2);
	 outBuffer_2->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 Output dst_2;
	 if(type==0)
	 {
		 dst_2.mPortID=PORT_IMG3O;
	 }
	 else
	 {
		 dst_2.mPortID=PORT_WROTO;
	 }
	 dst_2.mBuffer=outBuffer_2;
	 dst_2.mPortID.group=0;
	 enqueParams.mvOut.push_back(dst_2);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push dst done\n]", type);


	 for(int i=0;i<loopNum;i++)
	 {
		 memset((MUINT8*)(enqueParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		 memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		 //buffer operation
		 mpImemDrv->cacheFlushAll();
		 printf("--- [basicP2AwithFullG_UFO_in(%d_%d)...flush done\n]", type, i);

		 //enque
		 ret=pStream->enque(enqueParams);
		 if(!ret)
		 {
			 printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d_%d)..enque fail\n]", type, i);
		 }
		 else
		 {
			 printf("---[basicP2AwithFullG_UFO_in(%d_%d)..enque done\n]",type, i);
		 }

		 //temp use while to observe in CVD
		 //printf("--- [basicP2AwithFullG_UFO_in(%d)...enter while...........\n]", type);
		//while(1);


		 //deque
		 //wait a momet in fpga
		 //usleep(5000000);
		 QParams dequeParams;
		 ret=pStream->deque(dequeParams);
		 if(!ret)
		 {
			 printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d_%d)..deque fail\n]",type, i);
		 }
		 else
		 {
			 printf("---[basicP2AwithFullG_UFO_in(%d_%d)..deque done\n]", type, i);
		 }


		 //dump image
		 if(type==0)
		 {
			 char filename[256];
			 sprintf(filename, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			 char filename2[256];
			 sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		 }
		 else
		 {

			 char filename[256];
			 sprintf(filename, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			 char filename2[256];
			 sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		 }
		 printf("--- [basicP2AwithFullG_UFO_in(%d_%d)...save file done\n]", type,i);
	 }

	 //free
	 srcBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_imgi);
	 srcBuffer_ufdi->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_ufdi);
	 outBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_out1);
	 outBuffer_2->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_out2);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...free memory done\n]", type);

	 //
	 pStream->uninit("basicP2AwithFullG_UFO_in");
	 pStream->destroyInstance();
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...pStream uninit done\n]", type);
	 mpImemDrv->uninit();
	 mpImemDrv->destroyInstance();

	 return ret;
#endif
}

#include "pic/P2A_FG/imgi_1920_1080_10.h"
/*********************************************************************************/
int basicP2AwithFullHD(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2AwithFullHD(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2AwithFullHD");
    printf("--- [basicP2AwithFullHD(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1920, _imgi_h_=1080;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_1920_1080_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_1920_1080_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2AwithFullHD(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2AwithFullHD(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2AwithFullHD(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    if(type==0)
    {
	    crop3.mResizeDst.w=_imgi_w_;
        crop3.mResizeDst.h=_imgi_h_;
    }
    else
    {
        crop3.mResizeDst.w=_imgi_h_;
        crop3.mResizeDst.h=_imgi_w_;
    }
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2AwithFullHD(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 _out_w_, _out_h_;
    if(type==0)
    {
        _out_w_ = _imgi_w_;
        _out_h_ = _imgi_h_;
    }
    else
    {
        _out_w_ = _imgi_h_;
        _out_h_ = _imgi_w_;
    }
    MUINT32 bufStridesInBytes_2[3] = {_out_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_out_w_,_out_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
        dst_2.mTransform = 4;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;

    frameParams.mvOut.push_back(dst_2);
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicP2AwithFullHD(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2AwithFullHD(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2AwithFullHD(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2AwithFullHD(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2AwithFullHD(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2AwithFullHD(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2AwithFullHD(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _out_w_,_out_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _out_w_ *_out_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _out_w_,_out_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _out_w_ *_out_h_ * 2);
        }
        printf("--- [basicP2AwithFullHD(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2AwithFullHD(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2AwithFullHD");
    pStream->destroyInstance();
    printf("--- [basicP2AwithFullHD(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}



#include "pic/depi_4000_56_byte8.h"
#include "pic/dmgi_4000_56_byte8.h"
#include "pic/imgi_960x540_yuy2.h"
#include "pic/imgi_256x256_yv12.h"
#include "pic/imgi_256x256_yuy2.h"
MVOID p2bBokehYUVCallback(QParams& rParams)
{
	printf("--- [P2B_BOKEH_YUV callback func]\n");

	g_p2bBokehYUVCallback = MTRUE;
}

int P2B_BOKEH_YUV(int type,int loopNum)
{
    int ret=0;
    printf("--- [P2B_BOKEH_YUV(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2B_BOKEH_YUV");
    printf("--- [P2B_BOKEH_YUV(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Bokeh;

    //input image
    MUINT32 _imgi_w_=960, _imgi_h_=540;
    MUINT32 _imgbi_w_=_imgi_w_/2, _imgbi_h_=_imgi_h_/2;
    MUINT32 _imgci_w_=_imgi_w_/2, _imgci_h_=_imgi_h_/2;
    MUINT32 _depi_w_=4000, _depi_h_=56;
    MUINT32 _dmgi_w_=4000, _dmgi_h_=56;
    MUINT32 _depi_stride_ = 4000, _dmgi_stride_ = 4000;

    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_960x540_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_960x540_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2B_BOKEH_YUV(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    printf("--- [P2B_BOKEH_YUV: memid(0x%x), VA(0x%x), PA(0x%x) ]\n", buf_imgi.memID, buf_imgi.virtAddr, buf_imgi.phyAddr);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2B_BOKEH_YUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2B_BOKEH_YUV(%d)...flag -8]\n", type);
    printf("--- [P2B_BOKEH_YUV: srcBuffer, VA(0x%x), PA(0x%x) ]\n", srcBuffer->getBufVA(0), srcBuffer->getBufPA(0));

    
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2B_BOKEH_YUV(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2B_BOKEH_YUV(%d)...push crop information done\n]", type);

    //input dma
    IMEM_BUF_INFO buf_depi;
    buf_depi.size=sizeof(g_depi_4000_56_byte8);
    mpImemDrv->allocVirtBuf(&buf_depi);
    memset((MUINT8*)buf_depi.virtAddr, 0xffffffff, buf_depi.size);
    IImageBuffer* inDepiBuffer=NULL;
    MUINT32 depi_bufStridesInBytes[3] = {_depi_w_,0,0};
    PortBufInfo_v1 depi_portBufInfo = PortBufInfo_v1( buf_depi.memID,buf_depi.virtAddr,0,buf_depi.bufSecu, buf_depi.bufCohe);
    IImageBufferAllocator::ImgParam depi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_depi_w_,_depi_h_),  depi_bufStridesInBytes, depi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdepiHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV", depi_imgParam,depi_portBufInfo,true);
    inDepiBuffer = pdepiHeap->createImageBuffer();
    inDepiBuffer->incStrong(inDepiBuffer);
    inDepiBuffer->lockBuf("P2B_BOKEH_YUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input depi_input;
    depi_input.mPortID=PORT_YNR_LCEI;
    depi_input.mBuffer=inDepiBuffer;
    depi_input.mPortID.group=0;
    frameParams.mvIn.push_back(depi_input);   

    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(g_dmgi_4000_56_byte8);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memset((MUINT8*)buf_dmgi.virtAddr, 0xffffffff, buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_w_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("P2B_BOKEH_YUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_YNR_FACEI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams.mvIn.push_back(dmgi_input);  


    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2B_BOKEH_YUV", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2B_BOKEH_YUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   



    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2B_BOKEH_YUV", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2B_BOKEH_YUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    printf("--- [P2B_BOKEH_YUV(%d)...push dst done\n]", type);
#if 1

	//srz3 config
    ModuleInfo srz3_module;
    srz3_module.moduleTag = EDipModule_SRZ3;
    srz3_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz3Param = new _SRZ_SIZE_INFO_;
    mpsrz3Param->in_w = _imgi_w_/4;
    mpsrz3Param->in_h = _imgi_h_/4;
    mpsrz3Param->crop_floatX = 0;
    mpsrz3Param->crop_floatY = 0;
    mpsrz3Param->crop_x = 0;
    mpsrz3Param->crop_y = 0;
    mpsrz3Param->crop_w = _imgi_w_/4;
    mpsrz3Param->crop_h = _imgi_h_/4;
    mpsrz3Param->out_w = _imgi_w_;
    mpsrz3Param->out_h = _imgi_h_;
    srz3_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz3Param);
    frameParams.mvModuleData.push_back(srz3_module);
	
	//srz4 config
    ModuleInfo srz4_module;
    srz4_module.moduleTag = EDipModule_SRZ4;
    srz4_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz4Param = new _SRZ_SIZE_INFO_;
    mpsrz4Param->in_w = _imgi_w_/4;
    mpsrz4Param->in_h = _imgi_h_/4;
    mpsrz4Param->crop_floatX = 0;
    mpsrz4Param->crop_floatY = 0;
    mpsrz4Param->crop_x = 0;
    mpsrz4Param->crop_y = 0;
    mpsrz4Param->crop_w = _imgi_w_/4;
    mpsrz4Param->crop_h = _imgi_h_/4;
    mpsrz4Param->out_w = _imgi_w_;
    mpsrz4Param->out_h = _imgi_h_;
    srz4_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz4Param);
    frameParams.mvModuleData.push_back(srz4_module);
    
    printf("Add Tuning buffer P2B\n");
    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer P2B VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_P2B, 1);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);

    enqueParams.mvFrameParams.push_back(frameParams);
    g_p2bBokehYUVCallback = MFALSE;
    enqueParams.mpfnCallback = p2bBokehYUVCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2B_BOKEH_YUV(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[P2B_BOKEH_YUV(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(40000000);

        //fgets();
        //char buf[4096];
        //fgets(buf, sizeof(buf), stdin);

        //printf("--- [P2B_BOKEH_YUV(%d)...enter while...........\n]", type);
        //fgets();
        //getchar();
        
        //while(1);
        do{
            usleep(100000);
            if (MTRUE == g_p2bBokehYUVCallback)
            {
                break;
            }
        }while(1);
        g_p2bBokehYUVCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2B_BOKEH_YUV(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[P2B_BOKEH_YUV(%d_%d)..deque done\n]", type, i);
        }

#if 1
        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            //char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
#endif
        printf("--- [P2B_BOKEH_YUV(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free
    srcBuffer->unlockBuf("P2B_BOKEH_YUV");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    inDepiBuffer->unlockBuf("P2B_BOKEH_YUV");
    mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("P2B_BOKEH_YUV");
    mpImemDrv->freeVirtBuf(&buf_dmgi);

    outBuffer->unlockBuf("P2B_BOKEH_YUV");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2B_BOKEH_YUV");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [P2B_BOKEH_YUV(%d)...free memory done\n]", type);

    //
    pStream->uninit("P2B_BOKEH_YUV");
    pStream->destroyInstance();
    printf("--- [P2B_BOKEH_YUV(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}


int P2B_BOKEH_YUV_2(int type,int loopNum)
{
    int ret=0;
    printf("--- [P2B_BOKEH_YUV_2(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2B_BOKEH_YUV_2");
    printf("--- [P2B_BOKEH_YUV_2(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Bokeh;

    //input image
    MUINT32 _imgi_w_=256, _imgi_h_=256;
    MUINT32 _imgbi_w_=_imgi_w_/2, _imgbi_h_=_imgi_h_/2;
    MUINT32 _imgci_w_=_imgi_w_/2, _imgci_h_=_imgi_h_/2;
    MUINT32 _depi_w_=4000, _depi_h_=56;
    MUINT32 _dmgi_w_=4000, _dmgi_h_=56;
    MUINT32 _depi_stride_ = 4000, _dmgi_stride_ = 4000;

    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_yuv420_3plane_y_256_256_s256)+sizeof(g_yuv420_3plane_u_128_128_s256)+sizeof(g_yuv420_3plane_v_128_128_s256);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_yuv420_3plane_y_256_256_s256), sizeof(g_yuv420_3plane_y_256_256_s256));
    memcpy( (MUINT8*)(buf_imgi.virtAddr+(_imgi_w_*_imgi_h_)), (MUINT8*)(g_yuv420_3plane_u_128_128_s256), sizeof(g_yuv420_3plane_u_128_128_s256));
    memcpy( (MUINT8*)(buf_imgi.virtAddr+(_imgi_w_*_imgi_h_)+(_imgi_w_*_imgi_h_/4)), (MUINT8*)(g_yuv420_3plane_v_128_128_s256), sizeof(g_yuv420_3plane_v_128_128_s256));

    //imem buffer 2 image heap
    printf("--- [P2B_BOKEH_YUV_2(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_, _imgi_w_/2, _imgi_w_/2};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    printf("--- [P2B_BOKEH_YUV_2: memid(0x%x), VA(0x%x), PA(0x%x) ]\n", buf_imgi.memID, buf_imgi.virtAddr, buf_imgi.phyAddr);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_I420),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV_2", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2B_BOKEH_YUV_2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2B_BOKEH_YUV_2(%d)...flag -8]\n", type);
    printf("--- [P2B_BOKEH_YUV_2: srcBuffer, VA(0x%x), PA(0x%x) ]\n", srcBuffer->getBufVA(0), srcBuffer->getBufPA(0));

    
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2B_BOKEH_YUV_2(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2B_BOKEH_YUV_2(%d)...push crop information done\n]", type);

    //input dma
    IMEM_BUF_INFO buf_depi;
    buf_depi.size=sizeof(g_depi_4000_56_byte8);
    mpImemDrv->allocVirtBuf(&buf_depi);
    memset((MUINT8*)buf_depi.virtAddr, 0xffffffff, buf_depi.size);
    IImageBuffer* inDepiBuffer=NULL;
    MUINT32 depi_bufStridesInBytes[3] = {_depi_w_,0,0};
    PortBufInfo_v1 depi_portBufInfo = PortBufInfo_v1( buf_depi.memID,buf_depi.virtAddr,0,buf_depi.bufSecu, buf_depi.bufCohe);
    IImageBufferAllocator::ImgParam depi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_depi_w_,_depi_h_),  depi_bufStridesInBytes, depi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdepiHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV_2", depi_imgParam,depi_portBufInfo,true);
    inDepiBuffer = pdepiHeap->createImageBuffer();
    inDepiBuffer->incStrong(inDepiBuffer);
    inDepiBuffer->lockBuf("P2B_BOKEH_YUV_2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input depi_input;
    depi_input.mPortID=PORT_YNR_LCEI;
    depi_input.mBuffer=inDepiBuffer;
    depi_input.mPortID.group=0;
    frameParams.mvIn.push_back(depi_input);   

    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(g_dmgi_4000_56_byte8);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memset((MUINT8*)buf_dmgi.virtAddr, 0xffffffff, buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_w_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV_2", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("P2B_BOKEH_YUV_2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_YNR_FACEI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams.mvIn.push_back(dmgi_input);  


    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2B_BOKEH_YUV_2", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2B_BOKEH_YUV_2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   



    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2B_BOKEH_YUV_2", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2B_BOKEH_YUV_2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    printf("--- [P2B_BOKEH_YUV_2(%d)...push dst done\n]", type);

    //srz3 config
    ModuleInfo srz3_module;
    srz3_module.moduleTag = EDipModule_SRZ3;
    srz3_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz3Param = new _SRZ_SIZE_INFO_;
    mpsrz3Param->in_w = _dmgi_w_;
    mpsrz3Param->in_h = _dmgi_h_;
    mpsrz3Param->crop_floatX = 0;
    mpsrz3Param->crop_floatY = 0;
    mpsrz3Param->crop_x = 0;
    mpsrz3Param->crop_y = 0;
    mpsrz3Param->crop_w = _dmgi_w_;
    mpsrz3Param->crop_h = _dmgi_h_;
    mpsrz3Param->out_w = _dmgi_w_;
    mpsrz3Param->out_h = _dmgi_h_;
    srz3_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz3Param);
    frameParams.mvModuleData.push_back(srz3_module);

    //srz4 config
    ModuleInfo srz4_module;
    srz4_module.moduleTag = EDipModule_SRZ4;
    srz4_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz4Param = new _SRZ_SIZE_INFO_;
    mpsrz4Param->in_w = _depi_w_;
    mpsrz4Param->in_h = _depi_h_;
    mpsrz4Param->crop_floatX = 0;
    mpsrz4Param->crop_floatY = 0;
    mpsrz4Param->crop_x = 0;
    mpsrz4Param->crop_y = 0;
    mpsrz4Param->crop_w = _depi_w_;
    mpsrz4Param->crop_h = _depi_h_;
    mpsrz4Param->out_w = _depi_w_;
    mpsrz4Param->out_h = _depi_h_;
    srz4_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz4Param);
    frameParams.mvModuleData.push_back(srz4_module);


#if 1
    enqueParams.mvFrameParams.push_back(frameParams);
    g_p2bBokehYUVCallback = MFALSE;
    enqueParams.mpfnCallback = p2bBokehYUVCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2B_BOKEH_YUV_2(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[P2B_BOKEH_YUV_2(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(40000000);

        //fgets();
        //char buf[4096];
        //fgets(buf, sizeof(buf), stdin);

        //printf("--- [P2B_BOKEH_YUV_2(%d)...enter while...........\n]", type);
        //fgets();
        //getchar();
        
        //while(1);
        do{
            usleep(100000);
            if (MTRUE == g_p2bBokehYUVCallback)
            {
                break;
            }
        }while(1);
        g_p2bBokehYUVCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2B_BOKEH_YUV_2(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[P2B_BOKEH_YUV_2(%d_%d)..deque done\n]", type, i);
        }

#if 1
        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_2_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_2_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_2_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_2_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            //char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
#endif
        printf("--- [P2B_BOKEH_YUV(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free

    delete mpsrz3Param;
    delete mpsrz4Param;

    srcBuffer->unlockBuf("P2B_BOKEH_YUV_2");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    inDepiBuffer->unlockBuf("P2B_BOKEH_YUV_2");
    mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("P2B_BOKEH_YUV_2");
    mpImemDrv->freeVirtBuf(&buf_dmgi);

    outBuffer->unlockBuf("P2B_BOKEH_YUV_2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2B_BOKEH_YUV_2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [P2B_BOKEH_YUV_2(%d)...free memory done\n]", type);

    //
    pStream->uninit("P2B_BOKEH_YUV_2");
    pStream->destroyInstance();
    printf("--- [P2B_BOKEH_YUV_2(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}



#include "pic/P2B/boken_depi.h"
int P2B_BOKEH_YUV_3(int type,int loopNum)
{
    int ret=0;
    printf("--- [P2B_BOKEH_YUV_3(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2B_BOKEH_YUV_3");
    printf("--- [P2B_BOKEH_YUV_3(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Bokeh;

    //input image
    MUINT32 _imgi_w_=256, _imgi_h_=256;
    MUINT32 _imgbi_w_=_imgi_w_/2, _imgbi_h_=_imgi_h_/2;
    MUINT32 _imgci_w_=_imgi_w_/2, _imgci_h_=_imgi_h_/2;
    MUINT32 _dmgi_w_=256, _dmgi_h_=144;
    MUINT32 _dmgi_stride_ = 256;

    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_yuv420_3plane_y_256_256_s256)+sizeof(g_yuv420_3plane_u_128_128_s256)+sizeof(g_yuv420_3plane_v_128_128_s256);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_yuv420_3plane_y_256_256_s256), sizeof(g_yuv420_3plane_y_256_256_s256));
    memcpy( (MUINT8*)(buf_imgi.virtAddr+(_imgi_w_*_imgi_h_)), (MUINT8*)(g_yuv420_3plane_u_128_128_s256), sizeof(g_yuv420_3plane_u_128_128_s256));
    memcpy( (MUINT8*)(buf_imgi.virtAddr+(_imgi_w_*_imgi_h_)+(_imgi_w_*_imgi_h_/4)), (MUINT8*)(g_yuv420_3plane_v_128_128_s256), sizeof(g_yuv420_3plane_v_128_128_s256));

    //imem buffer 2 image heap
    printf("--- [P2B_BOKEH_YUV_3(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_, _imgi_w_/2, _imgi_w_/2};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    printf("--- [P2B_BOKEH_YUV_3: memid(0x%x), VA(0x%x), PA(0x%x) ]\n", buf_imgi.memID, buf_imgi.virtAddr, buf_imgi.phyAddr);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_I420),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV_3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2B_BOKEH_YUV_3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2B_BOKEH_YUV_3(%d)...flag -8]\n", type);
    printf("--- [P2B_BOKEH_YUV_3: srcBuffer, VA(0x%x), PA(0x%x) ]\n", srcBuffer->getBufVA(0), srcBuffer->getBufPA(0));

    
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2B_BOKEH_YUV_3(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2B_BOKEH_YUV_3(%d)...push crop information done\n]", type);

    //input dma
    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(boken_depi);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memset((MUINT8*)buf_dmgi.virtAddr, 0xffffffff, buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_w_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "P2B_BOKEH_YUV_3", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("P2B_BOKEH_YUV_3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_YNR_FACEI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams.mvIn.push_back(dmgi_input);  


    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2B_BOKEH_YUV_3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2B_BOKEH_YUV_3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   



    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2B_BOKEH_YUV_3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2B_BOKEH_YUV_3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    printf("--- [P2B_BOKEH_YUV_3(%d)...push dst done\n]", type);

    //srz3 config
    ModuleInfo srz3_module;
    srz3_module.moduleTag = EDipModule_SRZ3;
    srz3_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz3Param = new _SRZ_SIZE_INFO_;
    mpsrz3Param->in_w = _dmgi_w_;
    mpsrz3Param->in_h = _dmgi_h_;
    mpsrz3Param->crop_floatX = 0;
    mpsrz3Param->crop_floatY = 0;
    mpsrz3Param->crop_x = 0;
    mpsrz3Param->crop_y = 0;
    mpsrz3Param->crop_w = _dmgi_w_;
    mpsrz3Param->crop_h = _dmgi_h_;
    mpsrz3Param->out_w = _dmgi_w_;
    mpsrz3Param->out_h = _dmgi_h_;
    srz3_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz3Param);
    frameParams.mvModuleData.push_back(srz3_module);

#if 0
    //srz4 config
    ModuleInfo srz4_module;
    srz4_module.moduleTag = EDipModule_SRZ4;
    srz4_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz4Param = new _SRZ_SIZE_INFO_;
    mpsrz4Param->in_w = _depi_w_;
    mpsrz4Param->in_h = _depi_h_;
    mpsrz4Param->crop_floatX = 0;
    mpsrz4Param->crop_floatY = 0;
    mpsrz4Param->crop_x = 0;
    mpsrz4Param->crop_y = 0;
    mpsrz4Param->crop_w = _depi_w_;
    mpsrz4Param->crop_h = _depi_h_;
    mpsrz4Param->out_w = _depi_w_;
    mpsrz4Param->out_h = _depi_h_;
    srz4_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz4Param);
    frameParams.mvModuleData.push_back(srz4_module);
#endif

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_NBC2, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_SL2E, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_RMM2, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_RMG2, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);



#if 1
    g_p2bBokehYUVCallback = MFALSE;
    enqueParams.mpfnCallback = p2bBokehYUVCallback;

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2B_BOKEH_YUV_3(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[P2B_BOKEH_YUV_3(%d_%d)..enque done\n]",type, i);
        }

        do{
            usleep(100000);
            if (MTRUE == g_p2bBokehYUVCallback)
            {
                break;
            }
        }while(1);
        g_p2bBokehYUVCallback = MFALSE;

        printf("--- [P2B_BOKEH_YUV_3(%d)...enter while...........\n]", type);

#if 1
        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_3_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_3_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_P2B_BOKEH_YUV_3_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_P2B_BOKEH_YUV_3_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
#endif
        printf("--- [P2B_BOKEH_YUV(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free

    delete mpsrz3Param;
    //delete mpsrz4Param;

    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    srcBuffer->unlockBuf("P2B_BOKEH_YUV_3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    //inDepiBuffer->unlockBuf("P2B_BOKEH_YUV_3");
    //mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("P2B_BOKEH_YUV_3");
    mpImemDrv->freeVirtBuf(&buf_dmgi);

    outBuffer->unlockBuf("P2B_BOKEH_YUV_3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2B_BOKEH_YUV_3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [P2B_BOKEH_YUV_3(%d)...free memory done\n]", type);

    //
    pStream->uninit("P2B_BOKEH_YUV_3");
    pStream->destroyInstance();
    printf("--- [P2B_BOKEH_YUV_3(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "pic/FM/case1/depi.h"
#include "pic/FM/case1/dmgi.h"
#include "pic/FM/case1/mfbo_a.h"

MVOID P2A_FMCallback(QParams& rParams)
{
    printf("--- [P2A_FM callback func]\n");
		
    g_P2A_FMCallback = MTRUE;
}

int P2A_FM(int type,int loopNum)
{
    int ret=0;
    printf("--- [P2A_FM(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2A_FM");
    printf("--- [P2A_FM(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();
    IMEM_BUF_INFO pTuningQueBuf;

    QParams enqueParams;
    dip_x_reg_t *pIspTuningBuffer;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FM;

    //input image
    //MUINT32 _mfbo_w_=1280, _mfbo_h_=720;
    MUINT32 _depi_w_=4000, _depi_h_=56;
    MUINT32 _dmgi_w_=4000, _dmgi_h_=56;
    MUINT32 _mfbo_w_=200, _mfbo_h_=56;
    MUINT32 _depi_stride_ = 4000, _dmgi_stride_ = 4000, _mfbo_stride_ = 200;

    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);

    //input dma
    IMEM_BUF_INFO buf_depi;
    buf_depi.size=sizeof(g_depi_array);
    mpImemDrv->allocVirtBuf(&buf_depi);
    memcpy( (MUINT8*)(buf_depi.virtAddr), (MUINT8*)(g_depi_array), buf_depi.size);
    IImageBuffer* inDepiBuffer=NULL;
    MUINT32 depi_bufStridesInBytes[3] = {_depi_w_,0,0};
    PortBufInfo_v1 depi_portBufInfo = PortBufInfo_v1( buf_depi.memID,buf_depi.virtAddr,0,buf_depi.bufSecu, buf_depi.bufCohe);
    IImageBufferAllocator::ImgParam depi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_depi_w_,_depi_h_),  depi_bufStridesInBytes, depi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdepiHeap = ImageBufferHeap::create( "P2A_FM", depi_imgParam,depi_portBufInfo,true);
    inDepiBuffer = pdepiHeap->createImageBuffer();
    inDepiBuffer->incStrong(inDepiBuffer);
    inDepiBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input depi_input;
    depi_input.mPortID=PORT_RFEOI;
    depi_input.mBuffer=inDepiBuffer;
    depi_input.mPortID.group=0;
    frameParams.mvIn.push_back(depi_input);   

    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(g_dmgi_array);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memcpy( (MUINT8*)(buf_dmgi.virtAddr), (MUINT8*)(g_dmgi_array), buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_w_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "P2A_FM", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_LFEOI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams.mvIn.push_back(dmgi_input);  


    IMEM_BUF_INFO buf_out1;
    buf_out1.size=1280*720*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_stride_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufStridesInBytes_1, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2A_FM", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    //dst.mPortID=PORT_PAK2O;
    dst.mPortID=PORT_TIMGO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   
    printf("--- [P2A_FM(%d)...push dst done\n]", type);


    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);

    //Tuning Value Setting
    pIspTuningBuffer = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    pIspTuningBuffer->DIPCTL_D1A_DIPCTL_YUV_EN2.Raw=0x10000000;
    //pIspTuningBuffer->FM_D1A_FM_SIZE_CTRL.Raw=0x394C1010;
    //pIspTuningBuffer->FM_D1A_FM_TH_CON0_CTRL.Raw=0x3ff5a;
    pIspTuningBuffer->FM_D1A_FM_SIZE_CTRL.Raw=0x38640F09;
    frameParams.mTuningData = (MVOID*)(pIspTuningBuffer);
#if 1
    enqueParams.mvFrameParams.push_back(frameParams);
    g_P2A_FMCallback = MFALSE;
    enqueParams.mpfnCallback = P2A_FMCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2A_FM(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[P2A_FM(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(40000000);

        //fgets();
        //char buf[4096];
        //fgets(buf, sizeof(buf), stdin);

        //printf("--- [P2A_FM(%d)...enter while...........\n]", type);
        //fgets();
        //getchar();
        
        //while(1);
        do{
            usleep(100000);
            if (MTRUE == g_P2A_FMCallback)
            {
                break;
            }
        }while(1);
        g_P2A_FMCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2A_FM(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[P2A_FM(%d_%d)..deque done\n]", type, i);
        }

#if 1
        //dump image

        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2A_FM_process0x%x_type%d_package%d_mfbo_%dx%d.yuv",(MUINT32) getpid (), type,i, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mfbo_w_ *_mfbo_h_ * 2);


#endif
        printf("--- [P2A_FM(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free
    inDepiBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_dmgi);

    outBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_out1);
    printf("--- [P2A_FM(%d)...free memory done\n]", type);

    //
    pStream->uninit("P2A_FM");
    pStream->destroyInstance();
    printf("--- [P2A_FM(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}

/*********************************************************************************/
#include "pic/LPCNR/depi.h"
#include "pic/LPCNR/dmgi.h"
#include "pic/LPCNR/lcei.h"

MVOID testLPCNR_Callback(QParams& rParams)
{
    printf("--- [testLPCNR callback func]\n");

    g_testLPCNR_Callback = MTRUE;
}

int testLPCNR(int type,int loopNum)
{
    int ret=0;
    printf("--- [testLPCNR(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testLPCNR");
    printf("--- [testLPCNR(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();
    
    QParams enqueParams;
    FrameParams frameParams,frameParams2;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_LPCNR;
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_LPCNR;

    //input image
    MUINT32 _depi_w_=364, _depi_h_=80;
    MUINT32 _dmgi_w_=364, _dmgi_h_=160;
    MUINT32 _lcei_w_=546, _lcei_h_=80; // same as timgo_p1
    MUINT32 _timgo_p1_w_=546, _timgo_p2_w_=364, _timgo_h_=80;
    MUINT32 _depi_stride_ = 364, _dmgi_stride_ = 364, _lcei_stride_ = 364*12/8, _timgo_p1_stride_ = 364*12/8, _timgo_p2_stride_ = 364;
    
    //input dma
    IMEM_BUF_INFO buf_depi;
    buf_depi.size=_depi_w_*_depi_h_;
    mpImemDrv->allocVirtBuf(&buf_depi);
    memcpy( (char*)(buf_depi.virtAddr), (char*)(g_lp_depi_array), buf_depi.size);
    IImageBuffer* inDepiBuffer=NULL;
    MUINT32 depi_bufStridesInBytes[3] = {_depi_stride_,0,0};
    PortBufInfo_v1 depi_portBufInfo = PortBufInfo_v1( buf_depi.memID,buf_depi.virtAddr,0,buf_depi.bufSecu, buf_depi.bufCohe);
    IImageBufferAllocator::ImgParam depi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_depi_w_,_depi_h_),  depi_bufStridesInBytes, depi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdepiHeap = ImageBufferHeap::create( "testLPCNR", depi_imgParam,depi_portBufInfo,true);
    inDepiBuffer = pdepiHeap->createImageBuffer();
    inDepiBuffer->incStrong(inDepiBuffer);
    inDepiBuffer->lockBuf("testLPCNR",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input depi_input;
    depi_input.mPortID=PORT_ORIGUVI;
    depi_input.mBuffer=inDepiBuffer;
    depi_input.mPortID.group=0;
    frameParams.mvIn.push_back(depi_input);
    frameParams2.mvIn.push_back(depi_input);

    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=_dmgi_w_*_dmgi_h_;
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memcpy( (char*)(buf_dmgi.virtAddr), (char*)(g_lp_dmgi_array), buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_stride_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "testLPCNR", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("testLPCNR",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_ORIGYI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams2.mvIn.push_back(dmgi_input);

    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_timgo_p1_w_*_timgo_h_;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_timgo_p1_stride_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_timgo_p1_w_,_timgo_h_),  bufStridesInBytes_1, bufStridesInBytes_1, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testLPCNR", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testLPCNR",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IIRUVO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   
    printf("--- [testLPCNR(%d)...push dst done\n]", type);


//    IMEM_BUF_INFO buf_lcei;
//    buf_lcei.size=_lcei_w_*_lcei_h_;
//    mpImemDrv->allocVirtBuf(&buf_lcei);
//    memcpy( (char*)(buf_lcei.virtAddr), (char*)(g_lp_lcei_array), buf_lcei.size);
//    IImageBuffer* inLceiBuffer=NULL;
//    MUINT32 lcei_bufStridesInBytes[3] = {_lcei_stride_,0,0};
//    PortBufInfo_v1 lcei_portBufInfo = PortBufInfo_v1( buf_lcei.memID,buf_lcei.virtAddr,0,buf_lcei.bufSecu, buf_lcei.bufCohe);
//    IImageBufferAllocator::ImgParam lcei_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
//                                            MSize(_lcei_w_,_lcei_h_),  lcei_bufStridesInBytes, lcei_bufStridesInBytes, 1);
//    sp<ImageBufferHeap> plceiHeap = ImageBufferHeap::create( "testLPCNR", lcei_imgParam,lcei_portBufInfo,true);
//    inLceiBuffer = plceiHeap->createImageBuffer();
//    inLceiBuffer->incStrong(inLceiBuffer);
//    inLceiBuffer->lockBuf("testLPCNR",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input lcei_input;
    lcei_input.mPortID=PORT_IIRUVI;
    lcei_input.mBuffer=outBuffer;
    lcei_input.mPortID.group=0;
    frameParams2.mvIn.push_back(lcei_input);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_timgo_p2_w_*_timgo_h_;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_timgo_p2_stride_,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_timgo_p2_w_,_timgo_h_),  bufStridesInBytes_2, bufStridesInBytes_2, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testLPCNR", imgParam_2,portBufInfo_2,true);
    outBuffer2 = pHeap_2->createImageBuffer();
    outBuffer2->incStrong(outBuffer2);
    outBuffer2->lockBuf("testLPCNR",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst2;
    dst2.mPortID=PORT_IIRUVO;
    dst2.mBuffer=outBuffer2;
    dst2.mPortID.group=0;
    frameParams2.mvOut.push_back(dst2);
    printf("--- [testLPCNR(%d)...push dst2 done\n]", type);

    //Tuning Value Setting
    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    pIspReg->LPCNR_D1A_LPCNR_TOP.Raw = 0x00000042;
    pIspReg->LPCNR_D1A_LPCNR_ATPG.Raw = 0x0;
    pIspReg->LPCNR_D1A_LPCNR_SIZE.Bits.LPCNR_IN_HSIZE = _depi_w_/2;
    pIspReg->LPCNR_D1A_LPCNR_SIZE.Bits.LPCNR_IN_VSIZE = _depi_h_;
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    
    IMEM_BUF_INFO pTuningQueBuf2;
    pTuningQueBuf2.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf2);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf2);
    memset((MUINT8*)(pTuningQueBuf2.virtAddr), 0x0, pTuningQueBuf2.size);
    dip_x_reg_t *pIspReg2;
    pIspReg2 = (dip_x_reg_t *)pTuningQueBuf2.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf2.virtAddr);
    pIspReg2->LPCNR_D1A_LPCNR_TOP.Raw = 0x00000043;
    pIspReg2->LPCNR_D1A_LPCNR_ATPG.Raw = 0x0;
    pIspReg2->LPCNR_D1A_LPCNR_SIZE.Bits.LPCNR_IN_HSIZE = _depi_w_/2;
    pIspReg2->LPCNR_D1A_LPCNR_SIZE.Bits.LPCNR_IN_VSIZE = _depi_h_;
    frameParams2.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf2.virtAddr);

#if 1
    enqueParams.mvFrameParams.push_back(frameParams);
    enqueParams.mvFrameParams.push_back(frameParams2);
    g_testLPCNR_Callback = MFALSE;
    enqueParams.mpfnCallback = testLPCNR_Callback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);
        
        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testLPCNR(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testLPCNR(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);

        //deque
        //wait a momet in fpga
        //usleep(40000000);

        //fgets();
        //char buf[4096];
        //fgets(buf, sizeof(buf), stdin);

        //printf("--- [testLPCNR(%d)...enter while...........\n]", type);
        //fgets();
        //getchar();
        
        //while(1);
        do{
            usleep(100000);
            if (MTRUE == g_testLPCNR_Callback)
            {
                break;
            }
        }while(1);
        g_testLPCNR_Callback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [testLPCNR(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[testLPCNR(%d_%d)..deque done\n]", type, i);
        }

#if 1
        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testLPCNR_process0x%x_type%d_package%d_timgo_%dx%d.yuv",(MUINT32) getpid (), type,i, _timgo_p2_w_,_timgo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _timgo_p2_w_ * _timgo_h_);


#endif
        printf("--- [testLPCNR(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf2);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf2);
    inDepiBuffer->unlockBuf("testLPCNR");
    mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("testLPCNR");
    mpImemDrv->freeVirtBuf(&buf_dmgi);
//    inLceiBuffer->unlockBuf("testLPCNR");
//    mpImemDrv->freeVirtBuf(&buf_lcei);

    outBuffer->unlockBuf("testLPCNR");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer2->unlockBuf("testLPCNR");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [testLPCNR(%d)...free memory done\n]", type);

    //
    pStream->uninit("testLPCNR");
    pStream->destroyInstance();
    printf("--- [testLPCNR(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}


/*********************************************************************************/
int FEO_Y16_Dump(int type,int loopNum)
{
    int ret=0;
    printf("--- [FEO_Y16_Dump(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("FEO_Y16_Dump");
    printf("--- [FEO_Y16_Dump(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Y16Dump;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [FEO_Y16_Dump(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "FEO_Y16_Dump", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("FEO_Y16_Dump",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [FEO_Y16_Dump(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [FEO_Y16_Dump(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [FEO_Y16_Dump(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_Y16),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "FEO_Y16_Dump", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("FEO_Y16_Dump",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_FEO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        //printf("=================================== mvOut[0] VA = 0x%x\n", frameParams.mvOut[0].mBuffer->getBufVA(0));
        //printf("=================================== mvOut[0] PA = 0x%x\n", frameParams.mvOut[0].mBuffer->getBufPA(0));
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0x00000000, buf_out1.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [FEO_Y16_Dump(%d_%d)...flush done\n]", type, i);

        //enque
        //getchar();
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [FEO_Y16_Dump(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[FEO_Y16_Dump(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[FEO_Y16_Dump(%d_%d)..deque done\n]", type, i);
        }

        char filename[256];
        sprintf(filename, "/data/P2iopipe_FEO_Y16_Dump_process0x%x_type%d_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [FEO_Y16_Dump(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("FEO_Y16_Dump");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("FEO_Y16_Dump");
    mpImemDrv->freeVirtBuf(&buf_out1);
    printf("--- [FEO_Y16_Dump(%d)...free memory done\n]", type);

    //
    pStream->uninit("FEO_Y16_Dump");
    pStream->destroyInstance();
    printf("--- [FEO_Y16_Dump(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
MVOID basicWUVCallback(QParams& rParams)
{
	printf("--- [basicWUV callback func]\n");

	//char filename[256];
    //sprintf(filename, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
    //char filename2[256];
    //sprintf(filename2, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename2, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		
	g_basicWUVCallback = MTRUE;
}

int basicWUV(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicWUV(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicWUV");
    printf("--- [basicWUV(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_WUV;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicWUV(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicWUV", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicWUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicWUV(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicWUV(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicWUV(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicWUV", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicWUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicWUV", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicWUV",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicWUV(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
	g_basicWUVCallback = MFALSE;
	enqueParams.mpfnCallback = basicWUVCallback;
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicWUV(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicWUV(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicWUV(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicWUV(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        do{
            usleep(100000);
            if (MTRUE == g_basicWUVCallback)
            {
                break;
            }
        }while(1);
        g_basicWUVCallback = MFALSE;
        
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [basicWUV(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[basicWUV(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicWUV_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicWUV(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicWUV");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicWUV");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicWUV");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicWUV(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicWUV");
    pStream->destroyInstance();
    printf("--- [basicWUV(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
MVOID basicMDPCROPxCallback(QParams& rParams)
{
	printf("--- [basicMDPCROPx callback func]\n");

	//char filename[256];
    //sprintf(filename, "/data/P2iopipe_basicMDPCROPx_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
    //char filename2[256];
    //sprintf(filename2, "/data/P2iopipe_basicMDPCROPx_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
    //saveBufToFile(filename2, reinterpret_cast<MUINT8*>(rParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		
	g_basicMDPCROPxCallback = MTRUE;
}

int basicMDPCROPx(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicMDPCROPx(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicMDPCROPx");
    printf("--- [basicMDPCROPx(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicMDPCROPx(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicMDPCROPx", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicMDPCROPx",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMDPCROPx(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicMDPCROPx(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    switch (type)
    {
        case 0:
            crop2.mMdpGroup = 0;
            crop3.mMdpGroup = 0;
            break;
        case 1:
            crop2.mMdpGroup = 1;
            crop3.mMdpGroup = 1;
            break;
        case 2:
            crop2.mMdpGroup = 0;
            crop3.mMdpGroup = 1;
            break;
		case 3:
            crop2.mMdpGroup = 1;
            crop3.mMdpGroup = 0;
            break;
    }
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicMDPCROPx(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicMDPCROPx", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicMDPCROPx",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicMDPCROPx", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicMDPCROPx",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicMDPCROPx(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
    g_basicMDPCROPxCallback = MFALSE;
    enqueParams.mpfnCallback = basicMDPCROPxCallback;
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicMDPCROPx(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMDPCROPx(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicMDPCROPx(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicMDPCROPx(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        do{
            usleep(100000);
            if (MTRUE == g_basicMDPCROPxCallback)
            {
                break;
            }
        }while(1);
        g_basicMDPCROPxCallback = MFALSE;
        
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [basicMDPCROPx(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[basicMDPCROPx(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMDPCROPx_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMDPCROPx_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicMDPCROPx_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicMDPCROPx(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicMDPCROPx");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicMDPCROPx");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicMDPCROPx");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicMDPCROPx(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicMDPCROPx");
    pStream->destroyInstance();
    printf("--- [basicMDPCROPx(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
#include "pic/MFB_MIXING/imgi_3840x2160_yuv422.h"
#include "pic/MFB_MIXING/vipi_3840x2160_yuv422.h"
#include "pic/MFB_MIXING/dmgi_3840x2160_byte8.h"

MVOID testMFB_MixingCallback(QParams& rParams)
{
    printf("--- [testMFB_Mixing callback func]\n");

    g_testMFB_MixingCallback = MTRUE;
}

int testMFB_Mixing(int type,int loopNum)
{
    int ret=0;
    printf("--- [testMFB_Mixing(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testMFB_Mixing");
    printf("--- [testMFB_Mixing(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Mix;

    //input image
    MUINT32 _imgi_w_=3840, _imgi_h_=2160;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(mfb_mixing_yuv422_g_imgi_array_3840x2160);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(mfb_mixing_yuv422_g_imgi_array_3840x2160), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...imgi flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testMFB_Mixing", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...imgi flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testMFB_Mixing(%d)...push imgi done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testMFB_Mixing(%d)...push crop information done\n]", type);

	IMEM_BUF_INFO buf_vipi;
    buf_vipi.size=sizeof(mfb_mixing_yuv422_g_vipi_array_3840x2160);
    mpImemDrv->allocVirtBuf(&buf_vipi);
    memcpy( (MUINT8*)(buf_vipi.virtAddr), (MUINT8*)(mfb_mixing_yuv422_g_vipi_array_3840x2160), buf_vipi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...vipi flag -1 ]\n", type);
    IImageBuffer* srcBuffer2;
    MINT32 bufBoundaryInBytes2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes2[3] = {_imgi_w_ * 2, 0, 0};
    PortBufInfo_v1 portBufInfo2 = PortBufInfo_v1( buf_vipi.memID,buf_vipi.virtAddr,0,buf_vipi.bufSecu, buf_vipi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes2, bufBoundaryInBytes2, 1);
    sp<ImageBufferHeap> pHeap2;
    pHeap2 = ImageBufferHeap::create( "testMFB_Mixing", imgParam2,portBufInfo2,true);
    srcBuffer2 = pHeap2->createImageBuffer();
    srcBuffer2->incStrong(srcBuffer2);
    srcBuffer2->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...vipi flag -8]\n", type);
    Input src2;
    src2.mPortID=PORT_VIPI;
    src2.mBuffer=srcBuffer2;
    src2.mPortID.group=0;
    frameParams.mvIn.push_back(src2);
    printf("--- [testMFB_Mixing(%d)...push vipi done]\n", type);

	IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(mfb_mixing_byte8_g_dmgi_array_3840x2160);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memcpy( (MUINT8*)(buf_dmgi.virtAddr), (MUINT8*)(mfb_mixing_byte8_g_dmgi_array_3840x2160), buf_dmgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...dmgi flag -1 ]\n", type);
    IImageBuffer* srcBuffer3;
    MINT32 bufBoundaryInBytes3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes3[3] = {_imgi_w_, 0, 0};
    PortBufInfo_v1 portBufInfo3 = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam3 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes3, bufBoundaryInBytes3, 1);
    sp<ImageBufferHeap> pHeap3;
    pHeap3 = ImageBufferHeap::create( "testMFB_Mixing", imgParam3,portBufInfo3,true);
    srcBuffer3 = pHeap3->createImageBuffer();
    srcBuffer3->incStrong(srcBuffer3);
    srcBuffer3->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...dmgi flag -8]\n", type);
    Input src3;
    src3.mPortID=PORT_YNR_FACEI;
    src3.mBuffer=srcBuffer3;
    src3.mPortID.group=0;
    frameParams.mvIn.push_back(src3);
    printf("--- [testMFB_Mixing(%d)...push dmgi done]\n", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG2O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	

	IMEM_BUF_INFO buf_out3;
    buf_out3.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out3);
    memset((MUINT8*)buf_out3.virtAddr, 0xffffffff, buf_out3.size);
    IImageBuffer* outBuffer_3=NULL;
    MUINT32 bufStridesInBytes_3[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_3 = PortBufInfo_v1( buf_out3.memID,buf_out3.virtAddr,0,buf_out3.bufSecu, buf_out3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_3, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_3 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_3,portBufInfo_3,true);
    outBuffer_3 = pHeap_3->createImageBuffer();
    outBuffer_3->incStrong(outBuffer_3);
    outBuffer_3->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_3;
    dst_3.mPortID=PORT_WDMAO;
    dst_3.mBuffer=outBuffer_3;
    dst_3.mPortID.group=0;
    frameParams.mvOut.push_back(dst_3);	

    IMEM_BUF_INFO buf_out4;
    buf_out4.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out4);
    memset((MUINT8*)buf_out4.virtAddr, 0xffffffff, buf_out4.size);
    IImageBuffer* outBuffer_4=NULL;
    MUINT32 bufStridesInBytes_4[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_4 = PortBufInfo_v1( buf_out4.memID,buf_out4.virtAddr,0,buf_out4.bufSecu, buf_out4.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_4 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_4, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_4 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_4,portBufInfo_4,true);
    outBuffer_4 = pHeap_4->createImageBuffer();
    outBuffer_4->incStrong(outBuffer_4);
    outBuffer_4->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_4;
    dst_4.mPortID=PORT_WROTO;
    dst_4.mBuffer=outBuffer_4;
    dst_4.mPortID.group=0;
    frameParams.mvOut.push_back(dst_4);	
    printf("--- [testMFB_Mixing(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
	g_testMFB_MixingCallback = MFALSE;
	enqueParams.mpfnCallback = testMFB_MixingCallback;
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out3.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out4.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [testMFB_Mixing(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Mixing(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testMFB_Mixing(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [testMFB_Mixing(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        do{
            usleep(100000);
            if (MTRUE == g_testMFB_MixingCallback)
            {
                break;
            }
        }while(1);
        g_testMFB_MixingCallback = MFALSE;
        
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [testMFB_Mixing(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[testMFB_Mixing(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			char filename3[256];
            sprintf(filename3, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename4[256];
            sprintf(filename4, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename4, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [testMFB_Mixing(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    srcBuffer2->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_vipi);
    srcBuffer3->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_dmgi);
    outBuffer->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_3->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out3);
    outBuffer_4->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out4);
    printf("--- [testMFB_Mixing(%d)...free memory done\n]", type);

    //
    pStream->uninit("testMFB_Mixing");
    pStream->destroyInstance();
    printf("--- [testMFB_Mixing(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

#include "pic/imgi_960x540_byte8_half-yuy2.h"

int testMFB_Mixing_Small(int type,int loopNum)
{
    int ret=0;
    printf("--- [testMFB_Mixing(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testMFB_Mixing");
    printf("--- [testMFB_Mixing(%d)...pStream init done]\n", type);
    DipIMemDrv* mpImemDrv=NULL;
    mpImemDrv=DipIMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Mix;

    //input image
    MUINT32 _imgi_w_=960, _imgi_h_=540;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_960x540_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_960x540_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...imgi flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testMFB_Mixing", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...imgi flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testMFB_Mixing(%d)...push imgi done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testMFB_Mixing(%d)...push crop information done\n]", type);

    IMEM_BUF_INFO buf_vipi;
    buf_vipi.size=sizeof(g_imgi_array_960x540_yuy2);
    mpImemDrv->allocVirtBuf(&buf_vipi);
    memcpy( (MUINT8*)(buf_vipi.virtAddr), (MUINT8*)(g_imgi_array_960x540_yuy2), buf_vipi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...vipi flag -1 ]\n", type);
    IImageBuffer* srcBuffer2;
    MINT32 bufBoundaryInBytes2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes2[3] = {_imgi_w_ * 2, 0, 0};
    PortBufInfo_v1 portBufInfo2 = PortBufInfo_v1( buf_vipi.memID,buf_vipi.virtAddr,0,buf_vipi.bufSecu, buf_vipi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes2, bufBoundaryInBytes2, 1);
    sp<ImageBufferHeap> pHeap2;
    pHeap2 = ImageBufferHeap::create( "testMFB_Mixing", imgParam2,portBufInfo2,true);
    srcBuffer2 = pHeap2->createImageBuffer();
    srcBuffer2->incStrong(srcBuffer2);
    srcBuffer2->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...vipi flag -8]\n", type);
    Input src2;
    src2.mPortID=PORT_VIPI;
    src2.mBuffer=srcBuffer2;
    src2.mPortID.group=0;
    frameParams.mvIn.push_back(src2);
    printf("--- [testMFB_Mixing(%d)...push vipi done]\n", type);

	IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(g_imgi_array_960x540_byte8);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memcpy( (MUINT8*)(buf_dmgi.virtAddr), (MUINT8*)(g_imgi_array_960x540_byte8), buf_dmgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Mixing(%d)...dmgi flag -1 ]\n", type);
    IImageBuffer* srcBuffer3;
    MINT32 bufBoundaryInBytes3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes3[3] = {_imgi_w_, 0, 0};
    PortBufInfo_v1 portBufInfo3 = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam3 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes3, bufBoundaryInBytes3, 1);
    sp<ImageBufferHeap> pHeap3;
    pHeap3 = ImageBufferHeap::create( "testMFB_Mixing", imgParam3,portBufInfo3,true);
    srcBuffer3 = pHeap3->createImageBuffer();
    srcBuffer3->incStrong(srcBuffer3);
    srcBuffer3->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Mixing(%d)...dmgi flag -8]\n", type);
    Input src3;
    src3.mPortID=PORT_YNR_FACEI;    //  DMGI  change name to PORT_YNR_FACEI
    src3.mBuffer=srcBuffer3;
    src3.mPortID.group=0;
    frameParams.mvIn.push_back(src3);
    printf("--- [testMFB_Mixing(%d)...push dmgi done]\n", type);
    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    printf("Justin, Mixing Debu2\n");
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);

    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG2O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	
    
    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
	IMEM_BUF_INFO buf_out3;
    buf_out3.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out3);
    memset((MUINT8*)buf_out3.virtAddr, 0xffffffff, buf_out3.size);
    IImageBuffer* outBuffer_3=NULL;
    MUINT32 bufStridesInBytes_3[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_3 = PortBufInfo_v1( buf_out3.memID,buf_out3.virtAddr,0,buf_out3.bufSecu, buf_out3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_3, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_3 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_3,portBufInfo_3,true);
    outBuffer_3 = pHeap_3->createImageBuffer();
    outBuffer_3->incStrong(outBuffer_3);
    outBuffer_3->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_3;
    dst_3.mPortID=PORT_WDMAO;
    dst_3.mBuffer=outBuffer_3;
    dst_3.mPortID.group=0;
    frameParams.mvOut.push_back(dst_3);	
    IMEM_BUF_INFO buf_out4;
    buf_out4.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out4);
    memset((MUINT8*)buf_out4.virtAddr, 0xffffffff, buf_out4.size);
    IImageBuffer* outBuffer_4=NULL;
    MUINT32 bufStridesInBytes_4[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_4 = PortBufInfo_v1( buf_out4.memID,buf_out4.virtAddr,0,buf_out4.bufSecu, buf_out4.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_4 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_4, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_4 = ImageBufferHeap::create( "testMFB_Mixing", imgParam_4,portBufInfo_4,true);
    outBuffer_4 = pHeap_4->createImageBuffer();
    outBuffer_4->incStrong(outBuffer_4);
    outBuffer_4->lockBuf("testMFB_Mixing",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_4;
    dst_4.mPortID=PORT_WROTO;
    dst_4.mBuffer=outBuffer_4;
    dst_4.mPortID.group=0;
    frameParams.mvOut.push_back(dst_4);	
    printf("--- [testMFB_Mixing(%d)...push dst done\n]", type);


    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer Mixing VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_Mixing, 1);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);


    enqueParams.mvFrameParams.push_back(frameParams);
	g_testMFB_MixingCallback = MFALSE;
	enqueParams.mpfnCallback = testMFB_MixingCallback;
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out3.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out4.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [testMFB_Mixing(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Mixing(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testMFB_Mixing(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [testMFB_Mixing(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        do{
            usleep(100000);
            if (MTRUE == g_testMFB_MixingCallback)
            {
                break;
            }
        }while(1);
        g_testMFB_MixingCallback = MFALSE;
        
        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [testMFB_Mixing(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[testMFB_Mixing(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			char filename3[256];
            sprintf(filename3, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename4[256];
            sprintf(filename4, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename4, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_testMFB_Mixing_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [testMFB_Mixing(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    srcBuffer2->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_vipi);
    srcBuffer3->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_dmgi);
    outBuffer->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_3->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out3);
    outBuffer_4->unlockBuf("testMFB_Mixing");
    mpImemDrv->freeVirtBuf(&buf_out4);
    printf("--- [testMFB_Mixing(%d)...free memory done\n]", type);

    //
    pStream->uninit("testMFB_Mixing");
    pStream->destroyInstance();
    printf("--- [testMFB_Mixing(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "pic/UFO/imgi_640_480_10.h"
#include "pic/UFO/imgi_640_480_12.h"
#include "pic/UFO/ufdi_16_480_10.h"
#include "pic/UFO/ufdi_16_480_12.h"
MVOID basicP2AwithBayer_UFO_inCallback(QParams& rParams)
{
    printf("--- [basicP2AwithBayer_UFO_in callback func]\n");

    g_basicP2AwithBayer_UFO_inCallback = MTRUE;
}

int basicP2AwithBayer_UFO_in(int type,int loopNum)
{
	int ret=0;
	printf("--- [basicP2AwithBayer_UFO_in...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicP2AwithBayer_UFO_in");
	printf("--- [basicP2AwithBayer_UFO_in...pStream init done]\n");
	DipIMemDrv* mpImemDrv=NULL;
	mpImemDrv=DipIMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
    FrameParams frameParams;

    UFDG_META_INFO pUfdParam;

	//frame tag
	frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

	MUINT32 _imgi_w_, _imgi_h_, _ufdi_w_, _ufdi_h_;
	IMEM_BUF_INFO buf_imgi;
	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
	IImageBuffer* srcBuffer;
	//IMEM_BUF_INFO buf_ufdi;
	//IImageBuffer* srcBuffer_ufdi;
	MUINT32 stride_imgi;
	if (type==0)
	{
		//input image(imgi)
		_imgi_w_=640;//1600;
		_imgi_h_=480;//1200;
		_ufdi_w_=16;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_g_imgi_array_640_480_10) + sizeof(ufo_g_ufdi_array_16_480_10) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_g_imgi_array_640_480_10), sizeof(ufo_g_imgi_array_640_480_10));
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_g_imgi_array_640_480_10)), (MUINT8*)(ufo_g_ufdi_array_16_480_10), sizeof(ufo_g_ufdi_array_16_480_10));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithBayer_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*10/8))+15)>>4)<<4;
		printf("--- [basicP2AwithBayer_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0;
        pUfdParam.UFDG_BS_AU_START = 0;
        pUfdParam.UFDG_AU2_SIZE = 0;
        pUfdParam.UFDG_BOND_MODE = 0;
		
		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_g_imgi_array_640_480_10);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam,portBufInfo,true);

		MINT reqImgFormat = eImgFmt_UFO_BAYER10;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithBayer_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

	}
	else
	{
		//input image(imgi)
		_imgi_w_=640;//1600;
		_imgi_h_=480;//1200;
		_ufdi_w_=16;
		_ufdi_h_=_imgi_h_;
		buf_imgi.size=sizeof(ufo_g_imgi_array_640_480_12)+sizeof(ufo_g_ufdi_array_16_480_12) + sizeof(pUfdParam);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_g_imgi_array_640_480_12), buf_imgi.size);
		memcpy( (MUINT8*)(buf_imgi.virtAddr+sizeof(ufo_g_imgi_array_640_480_12)), (MUINT8*)(ufo_g_ufdi_array_16_480_12), sizeof(ufo_g_ufdi_array_16_480_12));
		//imem buffer 2 image heap
		printf("--- [basicP2AwithBayer_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*12/8))+15)>>4)<<4;
		printf("--- [basicP2AwithBayer_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, _ufdi_w_, sizeof(pUfdParam)};

        pUfdParam.UFDG_BITSTREAM_OFST_ADDR = 0;
        pUfdParam.UFDG_BS_AU_START = 0;
        pUfdParam.UFDG_AU2_SIZE = 0;
        pUfdParam.UFDG_BOND_MODE = 0;

		//PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		// multi plane
        MINT32 imgi_memID[3];
        MUINTPTR imgi_virtAddr[3];
        imgi_memID[0] = buf_imgi.memID;
        imgi_memID[1] = buf_imgi.memID;
        imgi_memID[2] = buf_imgi.memID;
        imgi_virtAddr[0] = buf_imgi.virtAddr;
        imgi_virtAddr[1] = buf_imgi.virtAddr+sizeof(ufo_g_imgi_array_640_480_12);
        imgi_virtAddr[2] = reinterpret_cast<MUINTPTR>(&pUfdParam);

        PortBufInfo_v1 portBufInfo = PortBufInfo_v1( imgi_memID,imgi_virtAddr,3,0,buf_imgi.bufSecu, buf_imgi.bufCohe);

		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_UFO_BAYER12),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 3);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam,portBufInfo,true);

		MINT reqImgFormat = eImgFmt_UFO_BAYER12;
		ImgBufCreator creator(reqImgFormat);
		srcBuffer = pHeap->createImageBuffer(&creator);
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithBayer_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

	}
	printf("--- [basicP2AwithBayer_UFO_in...push src done]\n");

	//
	MUINT32 _output_w_=_imgi_w_, _output_h_=_imgi_h_;
	MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
	//crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_output_w_img2o_;
	crop.mResizeDst.h=_output_h_img2o_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_output_w_;
	crop2.mResizeDst.h=_output_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_output_w_;
	crop3.mResizeDst.h=_output_h_;
	frameParams.mvCropRsInfo.push_back(crop);
	frameParams.mvCropRsInfo.push_back(crop2);
	frameParams.mvCropRsInfo.push_back(crop3);
	printf("--- [basicP2AwithBayer_UFO_in...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst;
	dst.mPortID=PORT_WDMAO;
	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams.mvOut.push_back(dst);

	IMEM_BUF_INFO buf_out1_wroto;
	buf_out1_wroto.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1_wroto);
	memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
	IImageBuffer* outBuffer_wroto=NULL;
	MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam_1_wroto,portBufInfo_1_wroto,true);
	outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
	outBuffer_wroto->incStrong(outBuffer_wroto);
	outBuffer_wroto->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_wroto;
	dst_wroto.mPortID=PORT_WROTO;
	dst_wroto.mBuffer=outBuffer_wroto;
	dst_wroto.mPortID.group=0;
	frameParams.mvOut.push_back(dst_wroto);

	IMEM_BUF_INFO buf_out2;
	buf_out2.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out2);
	memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
	IImageBuffer* outBuffer_2=NULL;
	MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam_2,portBufInfo_2,true);
	outBuffer_2 = pHeap_2->createImageBuffer();
	outBuffer_2->incStrong(outBuffer_2);
	outBuffer_2->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2;
	dst_2.mPortID=PORT_IMG3O;
	dst_2.mBuffer=outBuffer_2;
	dst_2.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2);
	//	  printf("--- [basicP2AwithBayer_UFO_in(%d)...push dst done\n]", type);

	IMEM_BUF_INFO buf_out2_img2o;
	buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
	mpImemDrv->allocVirtBuf(&buf_out2_img2o);
	memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
	IImageBuffer* outBuffer_2_img2o=NULL;
	MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
	PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "basicP2AwithBayer_UFO_in", imgParam_2_img2o,portBufInfo_2_img2o,true);
	outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
	outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
	outBuffer_2_img2o->lockBuf("basicP2AwithBayer_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2_img2o;
	dst_2_img2o.mPortID=PORT_IMG2O;
	dst_2_img2o.mBuffer=outBuffer_2_img2o;
	dst_2_img2o.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2_img2o);

    enqueParams.mvFrameParams.push_back(frameParams);
	g_basicP2AwithBayer_UFO_inCallback = MFALSE;
	enqueParams.mpfnCallback = basicP2AwithBayer_UFO_inCallback;

	for(int i=0;i<loopNum;i++)
	{
		memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
		memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
		memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
		//memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

		//buffer operation
		mpImemDrv->cacheFlushAll();
		printf("--- [basicP2AwithBayer_UFO_in(%d)...flush done\n]", i);

		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
			printf("---ERRRRRRRRR [basicP2AwithBayer_UFO_in(%d)..enque fail\n]", i);
		}
		else
		{
			printf("---[basicP2AwithBayer_UFO_in(%d)..enque done\n]", i);
		}

		//deque
		//wait a momet in fpga
		//usleep(5000000);
		do{
            usleep(100000);
            if (MTRUE == g_basicP2AwithBayer_UFO_inCallback)
		{
                break;
		}
        }while(1);
        g_basicP2AwithBayer_UFO_inCallback = MFALSE;
		
		//QParams dequeParams;
		//ret=pStream->deque(dequeParams);
		//if(!ret)
		//{
		//	printf("---ERRRRRRRRR [basicP2AwithBayer_UFO_in(%d)..deque fail\n]", i);
		//}
		//else
		{
			printf("---[basicP2AwithBayer_UFO_in(%d)..deque done\n]", i);
		}


		//dump image
		if (type==0)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithBayer10_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithBayer10_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithBayer10_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithBayer10_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithBayer10_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		else
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithBayer12_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithBayer12_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithBayer12_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithBayer12_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			//char filename3[256];
			//sprintf(filename3, "/data/P2iopipe_basicP2AwithBayer12_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			//saveBufToFile(filename3, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		printf("--- [basicP2AwithBayer_UFO_in(%d)...save file done\n]",i);
	}

	//free
	srcBuffer->unlockBuf("basicP2AwithBayer_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_imgi);
	//srcBuffer_ufdi->unlockBuf("basicP2AwithBayer_UFO_in");
	//mpImemDrv->freeVirtBuf(&buf_ufdi);
	outBuffer->unlockBuf("basicP2AwithBayer_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1);
	outBuffer_wroto->unlockBuf("basicP2AwithBayer_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1_wroto);
	outBuffer_2->unlockBuf("basicP2AwithBayer_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2);
	outBuffer_2_img2o->unlockBuf("basicP2AwithBayer_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2_img2o);
	//outBuffer_feo->unlockBuf("basicP2AwithBayer_UFO_in");
	//mpImemDrv->freeVirtBuf(&buf_out_feo);
	//mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
	//mpImemDrv->freeVirtBuf(&pTuningQueBuf);
	//
	printf("--- [basicP2AwithBayer_UFO_in...free memory done\n]");

	//
	pStream->uninit("basicP2AwithBayer_UFO_in");
	pStream->destroyInstance();
	printf("--- [basicP2AwithBayer_UFO_in...pStream uninit done\n]");
	mpImemDrv->uninit();
	mpImemDrv->destroyInstance();

	return ret;

}
