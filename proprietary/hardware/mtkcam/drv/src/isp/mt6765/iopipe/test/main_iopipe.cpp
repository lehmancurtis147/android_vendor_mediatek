/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

///////////////////////////////////////////////////////////////////////////////
// No Warranty
// Except as may be otherwise agreed to in writing, no warranties of any
// kind, whether express or implied, are given by MTK with respect to any MTK
// Deliverables or any use thereof, and MTK Deliverables are provided on an
// "AS IS" basis.  MTK hereby expressly disclaims all such warranties,
// including any implied warranties of merchantability, non-infringement and
// fitness for a particular purpose and any warranties arising out of course
// of performance, course of dealing or usage of trade.  Parties further
// acknowledge that Company may, either presently and/or in the future,
// instruct MTK to assist it in the development and the implementation, in
// accordance with Company's designs, of certain softwares relating to
// Company's product(s) (the "Services").  Except as may be otherwise agreed
// to in writing, no warranties of any kind, whether express or implied, are
// given by MTK with respect to the Services provided, and the Services are
// provided on an "AS IS" basis.  Company further acknowledges that the
// Services may contain errors, that testing is important and Company is
// solely responsible for fully testing the Services and/or derivatives
// thereof before they are used, sublicensed or distributed.  Should there be
// any third party action brought against MTK, arising out of or relating to
// the Services, Company agree to fully indemnify and hold MTK harmless.
// If the parties mutually agree to enter into or continue a business
// relationship or other arrangement, the terms and conditions set forth
// hereunder shall remain effective and, unless explicitly stated otherwise,
// shall prevail in the event of a conflict in the terms in any agreements
// entered into between the parties.
////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2008, MediaTek Inc.
// All rights reserved.
//
// Unauthorized use, practice, perform, copy, distribution, reproduction,
// or disclosure of this information in whole or in part is prohibited.


#define LOG_TAG "iopipetest"
//	#define EP_SMALL_PICTURE
#include <vector>

#include <sys/time.h>
#include <sys/stat.h>
#include <sys/prctl.h>

#include <stdio.h>
#include <stdlib.h>
//
#include <errno.h>
#include <fcntl.h>

#include <mtkcam/def/common.h>

//
#include <mtkcam/drv/iopipe/PostProc/INormalStream.h>

#include <ispio_pipe_ports.h>
#include <imem_drv.h>
#include <isp_drv.h>

#include <mtkcam/utils/imgbuf/IImageBuffer.h>
#include <utils/StrongPointer.h>
#include <mtkcam/utils/std/common.h>
#include <mtkcam/utils/imgbuf/ImageBufferHeap.h>
//
//thread
#include <utils/threads.h>
#include <mtkcam/def/PriorityDefs.h>
//thread priority
#include <system/thread_defs.h>
#include <sys/resource.h>
#include <utils/ThreadDefs.h>
#include <pthread.h>
#include <semaphore.h>

#include "isp_drv_dip_phy.h"
//
using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSPostProc;

/******************************************************************************
* save the buffer to the file
*******************************************************************************/
static bool
saveBufToFile(char const*const fname, MUINT8 *const buf, MUINT32 const size)
{
    int nw, cnt = 0;
    uint32_t written = 0;

    //LOG_INF("(name, buf, size) = (%s, %x, %d)", fname, buf, size);
    //LOG_INF("opening file [%s]\n", fname);
    int fd = ::open(fname, O_RDWR | O_CREAT, S_IRWXU);
    if (fd < 0) {
        printf(": failed to create file [%s]: %s \n", fname, ::strerror(errno));
        return false;
    }

	printf("writing %d bytes to file [%s]\n", size, fname);

    //LOG_INF("writing %d bytes to file [%s]\n", size, fname);
    while (written < size) {
        nw = ::write(fd,
                     buf + written,
                     size - written);
        if (nw < 0) {
            printf(": failed to write to file [%s]: %s\n", fname, ::strerror(errno));
            break;
        }
        written += nw;
        cnt++;
    }
    //LOG_INF("done writing %d bytes to file [%s] in %d passes\n", size, fname, cnt);
    ::close(fd);
    return true;
}

/*******************************************************************************
*  Main Function
*
*  iopieTest 2 0 1 3
*  basic path(single thread): p2a, 1 in / 2 out without crop/resize
*                   -> testType=0: isp only+frame mode
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 1 1 1
*  multi-frame(single thread): two different frames for p2a path, 1 in / 2 out without crop/resize for each frames
*                   -> testType=0: isp only+frame mode
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 2 1 3
*  multi-thread test:
*              thread 1:  p2a(two frames), 1 in / 2 out without crop/resize for each frame
*              thread 2:  p2a(one frame), 1 in / 2 out without crop/resize
*                   -> testType=1: direct link+tpipe mode
*
*  iopieTest 2 3 1 1
*  basic vss test:
*              capture:  p2a(one frame) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopieTest 2 4 1 6
*  multi-thread vss test:
*              thread 1_prv:  p2a(one frame) in loop, 1 in / 2 out without crop/resize for each frame
*              thread 2_cap:  p2a(one frame), 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 5 1 3
*  multi-process test:
*              process1, thread 1_prv:  p2a(single frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*              process2, thread 2_prv:  p2a(single frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 6 1 2
*  multi-process test:
*              process1, thread 1_prv:  p2a(multi frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*              process2, thread 2_prv:  p2a(multi frame in the enque request) in loop, 1 in / 2 out without crop/resize for each frame
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 7 1 1
*  skip tpipe calculation test:  (//three same frames in a single enqueued package)
*               -> check comparison rule in a single package (frame with frame)
*               basic path(single thread, multi-frame): p2a, 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 8 1 2
*  skip tpipe calculation test 2:  (//three frames in a single enqueued package, the first and second frames are the same, and the third frame is different)
*               -> check comparison rule in a single package (frame with frame) and between different package (frame3)
*               basic path(single thread, multi-frame): p2a, 1 in / 2 out without crop/resize
*                   -> only direct link+tpipe mode
*
*  iopipeTest 2 9 1 3
*  sw direct link test(isp-mdp-venc):  (//three frames in the same setting and direct link to codec)
*
*  iopipeTest 2 10 0 1
*  p2a with feo output, for eis usage, with srz1 disabled
*
*  iopipeTest 2 10 1 1
*  p2a with feo output, for eis usage, with srz1 enabled
*
*  iopipeTest 2 10 2 1
*  p2a with feo output, for vsdof usage, with srz1 disabled
*
*  iopipeTest 2 10 3 1
*  p2a with feo output, for vsdof usage, with srz1 enabled
*
*  iopipeTest 2 11 1 1
*  p2a yuv input, direct link mode
*
*  iopipeTest 2 13 1 1
*  p2a raw input, isp only + tpipe mode
*
*  iopipeTest 2 15 x 1
*  tpipe ctrl extension, isp only + tpipe mode
*
*  iopipeTest 2 16 x 1
*  tpipe ctrl extension, isp-mdp direct link + tpipe mode
*
*  iopipeTest 2 17 1 10
*  skip tpipe mechainsm verification, specially simulation for isp does not change setting but mdp does
*
*  iopipeTest 2 18 1 3
*  multi-vss handle
*
*  iopipeTest 2 19 1 1
*  p2a flow with fullG ufo in, isp-mdp direct link (wdmao+wroto+img2o+img3o+srz1/feo)
*                   -> testType=0: Imgi with bayer10 full-g
*                   -> testType=1: Imgi with bayer12 full-g
*
*  iopieTest 2 20 1 3
*  basic path with fullHD(single thread): p2a with full-HD input, 1 in / 2 out without crop/resize
*                   -> testType=0: isp only+tpipe mode
*                   -> testType=1: direct link+tpipe mode (PORT_WROTO with 90 degree)
*
*  iopipeTest 2 99 1 1
*  - single frame debug test
********************************************************************************/
int basicP2A( int type,int loopNum);
int basicP2A_mdpsubpixel( int type,int loopNum);
int basicP2A_smallOutput( int type,int loopNum);
int basicMultiFrame(int type,int loopNum);
int multiThread(int type,int loopNum);
int basicVss(int type,int loopNum);
int multiThreadVss(int type,int loopNum);
int multiProcess(int testNum, int type,int loopNum);
int testSkipTpipe( int type,int loopNum);
int testSkipTpipe2( int type,int loopNum);
int testSkipTpipe3( int type,int loopNum);
int testIspMdpVencDL( int type,int loopNum);
int basicP2AwithFEO( int type,int loopNum);
int basicP2AwithYUVinput( int type,int loopNum);
int basicP2A_isponly_withTpipe(int type,int loopNum);
int testpath( int type,int loopNum);
int testMFB_Blending(int type,int loopNum);
int testRawInRawOut(int type,int loopNum);
int testRGBInYuvOut(int type,int loopNum);
int testRawInUnPakRawOut(int type,int loopNum);
int testTPIPECtrlExtension_isponly(int type,int loopNum);
int testTPIPECtrlExtension_directlink(int type,int loopNum);
int multiVss_test(int type,int loopNum);
int basicP2AwithFullG_UFO_in(int type,int loopNum);
int basicP2AwithFullHD( int type,int loopNum);
int basicP2A_smallOutput_64x64(int type,int loopNum);
int P2A_FM(int type,int loopNum);
MVOID P2A_FMCallback(QParams& rParams);
MBOOL g_P2A_FMCallback = MFALSE;



int test_iopipe(int argc, char** argv)
{
    int ret = 0;
    if(argc<4)
    {
        printf("wrong paramerter number(%d)\n",argc);
        return 0;
    }


    printf("##############################\n");
    printf("\n");
    printf("##############################\n");

    int testNum = atoi(argv[1]);
    int testType = atoi(argv[2]);
    int loopNum = atoi(argv[3]);


    switch(testNum)
    {
        case 0:
        	ret=basicP2A(testType,loopNum);
        	break;
        case 1:
        	ret=basicMultiFrame(testType,loopNum);
        	break;
        case 2:
            ret=multiThread(testType,loopNum);
        	break;
       case 3:
            ret=basicVss(testType,loopNum);
        	break;
       case 4:
            ret=multiThreadVss(testType,loopNum);
        	break;
       case 5:
       case 6:
            ret=multiProcess(testNum, testType,loopNum);
        	break;
       case 7:
            ret=testSkipTpipe(testType,loopNum);
        	break;
        case 8:
            ret=testSkipTpipe2(testType,loopNum);
        	break;
        case 9:
            ret=testIspMdpVencDL(testType,loopNum);
            break;
        case 10:
            ret=basicP2AwithFEO(testType, loopNum);
            break;
        case 11:
            ret=basicP2AwithYUVinput(testType, loopNum);
            break;
        case 12:
            ret=testMFB_Blending(testType,loopNum);
            break;
        case 13:
            ret=basicP2A_isponly_withTpipe(testType,loopNum);
            break;
        case 14:
            ret=testRawInRawOut(testType,loopNum);
            break;
        case 15:
            ret=testTPIPECtrlExtension_isponly(testType,loopNum);
            break;
        case 16:
            ret=testTPIPECtrlExtension_directlink(testType, loopNum);
            break;
        case 17:
            ret=testSkipTpipe3(testType,loopNum);
        	break;
        case 18:
            ret=multiVss_test(testType,loopNum);
        	break;
        case 19:
            ret=basicP2AwithFullG_UFO_in(testType,loopNum);
        	break;
        case 20:
        	ret=basicP2AwithFullHD(testType,loopNum);
        	break;
        case 21:
            ret=testRGBInYuvOut(testType,loopNum);
            break;
        case 22:
            ret=testRawInUnPakRawOut(testType,loopNum);
            break;
		case 27:            
			ret=P2A_FM(testType,loopNum);            
			break;
		case 30:
			ret=basicP2A_smallOutput(testType,loopNum);
			break;
		case 31:
			ret=basicP2A_smallOutput_64x64(testType,loopNum);
			break;
        case 32:
        	ret=basicP2A_mdpsubpixel(testType,loopNum);
        	break;
        case 99:
            ret=testpath(testType,loopNum);
            break;
        default:
        	break;
    }

    ret = 1;
    return ret;
}


enum tuning_tag
{
    tuning_tag_G2G = 0,
    tuning_tag_G2C,
    tuning_tag_GGM,
    tuning_tag_UDM,
};
void SetDefaultTuning(dip_x_reg_t* pIspReg, MUINT32* tuningBuf, tuning_tag tag, int enFgMode)
{
    printf("SetDefaultTuning (%d), enFgMode(%d)n", tag, enFgMode);
    MUINT32 fgModeRegBit = (enFgMode&0x01)<<10;
    switch(tag)
    {
        case tuning_tag_G2G:
            pIspReg->DIP_X_CTL_RGB_EN.Bits.G2G_EN = 1;//enable bit
            pIspReg->DIP_X_G2G_CNV_1.Raw = 0x00000200;
            pIspReg->DIP_X_G2G_CNV_2.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_3.Raw = 0x02000000;
            pIspReg->DIP_X_G2G_CNV_4.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_5.Raw = 0x00000000;
            pIspReg->DIP_X_G2G_CNV_6.Raw = 0x00000200;
            pIspReg->DIP_X_G2G_CTRL.Raw = 0x00000009;
            break;
        case tuning_tag_G2C:
            pIspReg->DIP_X_CTL_YUV_EN.Bits.G2C_EN = 1;//enable bit
            pIspReg->DIP_X_G2C_CONV_0A.Raw = 0x012D0099;
            pIspReg->DIP_X_G2C_CONV_0B.Raw = 0x0000003A;
            pIspReg->DIP_X_G2C_CONV_1A.Raw = 0x075607AA;
            pIspReg->DIP_X_G2C_CONV_1B.Raw = 0x00000100;
            pIspReg->DIP_X_G2C_CONV_2A.Raw = 0x072A0100;
            pIspReg->DIP_X_G2C_CONV_2B.Raw = 0x000007D6;
            pIspReg->DIP_X_G2C_SHADE_CON_1.Raw = 0x0118000E;
            pIspReg->DIP_X_G2C_SHADE_CON_2.Raw = 0x0074B740;
            pIspReg->DIP_X_G2C_SHADE_CON_3.Raw = 0x00000133;
            pIspReg->DIP_X_G2C_SHADE_TAR.Raw = 0x079F0A5A;
            pIspReg->DIP_X_G2C_SHADE_SP.Raw = 0x00000000;
            pIspReg->DIP_X_G2C_CFC_CON_1.Raw = 0x03f70080;
            pIspReg->DIP_X_G2C_CFC_CON_2.Raw = 0x29485294;
            break;
        case tuning_tag_GGM:
            //tuningBuf[0x1000>>2] = 0x08000800;
            pIspReg->DIP_X_CTL_RGB_EN.Bits.GGM_EN = 1;//enable bit
            tuningBuf[0x00001000>>2] = 0x08000800; /* 0x15023000: DIP_A_GGM_LUT_RB[0] */
            tuningBuf[0x00001004>>2] = 0x08020802; /* 0x15023004: DIP_A_GGM_LUT_RB[1] */
            tuningBuf[0x00001008>>2] = 0x08040804; /* 0x15023008: DIP_A_GGM_LUT_RB[2] */
            tuningBuf[0x0000100C>>2] = 0x08060806; /* 0x1502300C: DIP_A_GGM_LUT_RB[3] */
            tuningBuf[0x00001010>>2] = 0x08080808; /* 0x15023010: DIP_A_GGM_LUT_RB[4] */
            tuningBuf[0x00001014>>2] = 0x080A080A; /* 0x15023014: DIP_A_GGM_LUT_RB[5] */
            tuningBuf[0x00001018>>2] = 0x080C080C; /* 0x15023018: DIP_A_GGM_LUT_RB[6] */
            tuningBuf[0x0000101C>>2] = 0x080E080E; /* 0x1502301C: DIP_A_GGM_LUT_RB[7] */
            tuningBuf[0x00001020>>2] = 0x08100810; /* 0x15023020: DIP_A_GGM_LUT_RB[8] */
            tuningBuf[0x00001024>>2] = 0x08120812; /* 0x15023024: DIP_A_GGM_LUT_RB[9] */
            tuningBuf[0x00001028>>2] = 0x08140814; /* 0x15023028: DIP_A_GGM_LUT_RB[10] */
            tuningBuf[0x0000102C>>2] = 0x08160816; /* 0x1502302C: DIP_A_GGM_LUT_RB[11] */
            tuningBuf[0x00001030>>2] = 0x08180818; /* 0x15023030: DIP_A_GGM_LUT_RB[12] */
            tuningBuf[0x00001034>>2] = 0x081A081A; /* 0x15023034: DIP_A_GGM_LUT_RB[13] */
            tuningBuf[0x00001038>>2] = 0x081C081C; /* 0x15023038: DIP_A_GGM_LUT_RB[14] */
            tuningBuf[0x0000103C>>2] = 0x081E081E; /* 0x1502303C: DIP_A_GGM_LUT_RB[15] */
            tuningBuf[0x00001040>>2] = 0x08200820; /* 0x15023040: DIP_A_GGM_LUT_RB[16] */
            tuningBuf[0x00001044>>2] = 0x08220822; /* 0x15023044: DIP_A_GGM_LUT_RB[17] */
            tuningBuf[0x00001048>>2] = 0x08240824; /* 0x15023048: DIP_A_GGM_LUT_RB[18] */
            tuningBuf[0x0000104C>>2] = 0x08260826; /* 0x1502304C: DIP_A_GGM_LUT_RB[19] */
            tuningBuf[0x00001050>>2] = 0x08280828; /* 0x15023050: DIP_A_GGM_LUT_RB[20] */
            tuningBuf[0x00001054>>2] = 0x082A082A; /* 0x15023054: DIP_A_GGM_LUT_RB[21] */
            tuningBuf[0x00001058>>2] = 0x082C082C; /* 0x15023058: DIP_A_GGM_LUT_RB[22] */
            tuningBuf[0x0000105C>>2] = 0x082E082E; /* 0x1502305C: DIP_A_GGM_LUT_RB[23] */
            tuningBuf[0x00001060>>2] = 0x08300830; /* 0x15023060: DIP_A_GGM_LUT_RB[24] */
            tuningBuf[0x00001064>>2] = 0x08320832; /* 0x15023064: DIP_A_GGM_LUT_RB[25] */
            tuningBuf[0x00001068>>2] = 0x08340834; /* 0x15023068: DIP_A_GGM_LUT_RB[26] */
            tuningBuf[0x0000106C>>2] = 0x08360836; /* 0x1502306C: DIP_A_GGM_LUT_RB[27] */
            tuningBuf[0x00001070>>2] = 0x08380838; /* 0x15023070: DIP_A_GGM_LUT_RB[28] */
            tuningBuf[0x00001074>>2] = 0x083A083A; /* 0x15023074: DIP_A_GGM_LUT_RB[29] */
            tuningBuf[0x00001078>>2] = 0x083C083C; /* 0x15023078: DIP_A_GGM_LUT_RB[30] */
            tuningBuf[0x0000107C>>2] = 0x083E083E; /* 0x1502307C: DIP_A_GGM_LUT_RB[31] */
            tuningBuf[0x00001080>>2] = 0x08400840; /* 0x15023080: DIP_A_GGM_LUT_RB[32] */
            tuningBuf[0x00001084>>2] = 0x08420842; /* 0x15023084: DIP_A_GGM_LUT_RB[33] */
            tuningBuf[0x00001088>>2] = 0x08440844; /* 0x15023088: DIP_A_GGM_LUT_RB[34] */
            tuningBuf[0x0000108C>>2] = 0x08460846; /* 0x1502308C: DIP_A_GGM_LUT_RB[35] */
            tuningBuf[0x00001090>>2] = 0x08480848; /* 0x15023090: DIP_A_GGM_LUT_RB[36] */
            tuningBuf[0x00001094>>2] = 0x084A084A; /* 0x15023094: DIP_A_GGM_LUT_RB[37] */
            tuningBuf[0x00001098>>2] = 0x084C084C; /* 0x15023098: DIP_A_GGM_LUT_RB[38] */
            tuningBuf[0x0000109C>>2] = 0x084E084E; /* 0x1502309C: DIP_A_GGM_LUT_RB[39] */
            tuningBuf[0x000010A0>>2] = 0x08500850; /* 0x150230A0: DIP_A_GGM_LUT_RB[40] */
            tuningBuf[0x000010A4>>2] = 0x08520852; /* 0x150230A4: DIP_A_GGM_LUT_RB[41] */
            tuningBuf[0x000010A8>>2] = 0x08540854; /* 0x150230A8: DIP_A_GGM_LUT_RB[42] */
            tuningBuf[0x000010AC>>2] = 0x08560856; /* 0x150230AC: DIP_A_GGM_LUT_RB[43] */
            tuningBuf[0x000010B0>>2] = 0x08580858; /* 0x150230B0: DIP_A_GGM_LUT_RB[44] */
            tuningBuf[0x000010B4>>2] = 0x085A085A; /* 0x150230B4: DIP_A_GGM_LUT_RB[45] */
            tuningBuf[0x000010B8>>2] = 0x085C085C; /* 0x150230B8: DIP_A_GGM_LUT_RB[46] */
            tuningBuf[0x000010BC>>2] = 0x085E085E; /* 0x150230BC: DIP_A_GGM_LUT_RB[47] */
            tuningBuf[0x000010C0>>2] = 0x08600860; /* 0x150230C0: DIP_A_GGM_LUT_RB[48] */
            tuningBuf[0x000010C4>>2] = 0x08620862; /* 0x150230C4: DIP_A_GGM_LUT_RB[49] */
            tuningBuf[0x000010C8>>2] = 0x08640864; /* 0x150230C8: DIP_A_GGM_LUT_RB[50] */
            tuningBuf[0x000010CC>>2] = 0x08660866; /* 0x150230CC: DIP_A_GGM_LUT_RB[51] */
            tuningBuf[0x000010D0>>2] = 0x08680868; /* 0x150230D0: DIP_A_GGM_LUT_RB[52] */
            tuningBuf[0x000010D4>>2] = 0x086A086A; /* 0x150230D4: DIP_A_GGM_LUT_RB[53] */
            tuningBuf[0x000010D8>>2] = 0x086C086C; /* 0x150230D8: DIP_A_GGM_LUT_RB[54] */
            tuningBuf[0x000010DC>>2] = 0x086E086E; /* 0x150230DC: DIP_A_GGM_LUT_RB[55] */
            tuningBuf[0x000010E0>>2] = 0x08700870; /* 0x150230E0: DIP_A_GGM_LUT_RB[56] */
            tuningBuf[0x000010E4>>2] = 0x08720872; /* 0x150230E4: DIP_A_GGM_LUT_RB[57] */
            tuningBuf[0x000010E8>>2] = 0x08740874; /* 0x150230E8: DIP_A_GGM_LUT_RB[58] */
            tuningBuf[0x000010EC>>2] = 0x08760876; /* 0x150230EC: DIP_A_GGM_LUT_RB[59] */
            tuningBuf[0x000010F0>>2] = 0x08780878; /* 0x150230F0: DIP_A_GGM_LUT_RB[60] */
            tuningBuf[0x000010F4>>2] = 0x087A087A; /* 0x150230F4: DIP_A_GGM_LUT_RB[61] */
            tuningBuf[0x000010F8>>2] = 0x087C087C; /* 0x150230F8: DIP_A_GGM_LUT_RB[62] */
            tuningBuf[0x000010FC>>2] = 0x087E087E; /* 0x150230FC: DIP_A_GGM_LUT_RB[63] */
            tuningBuf[0x00001100>>2] = 0x10801080; /* 0x15023100: DIP_A_GGM_LUT_RB[64] */
            tuningBuf[0x00001104>>2] = 0x10841084; /* 0x15023104: DIP_A_GGM_LUT_RB[65] */
            tuningBuf[0x00001108>>2] = 0x10881088; /* 0x15023108: DIP_A_GGM_LUT_RB[66] */
            tuningBuf[0x0000110C>>2] = 0x108C108C; /* 0x1502310C: DIP_A_GGM_LUT_RB[67] */
            tuningBuf[0x00001110>>2] = 0x10901090; /* 0x15023110: DIP_A_GGM_LUT_RB[68] */
            tuningBuf[0x00001114>>2] = 0x10941094; /* 0x15023114: DIP_A_GGM_LUT_RB[69] */
            tuningBuf[0x00001118>>2] = 0x10981098; /* 0x15023118: DIP_A_GGM_LUT_RB[70] */
            tuningBuf[0x0000111C>>2] = 0x109C109C; /* 0x1502311C: DIP_A_GGM_LUT_RB[71] */
            tuningBuf[0x00001120>>2] = 0x10A010A0; /* 0x15023120: DIP_A_GGM_LUT_RB[72] */
            tuningBuf[0x00001124>>2] = 0x10A410A4; /* 0x15023124: DIP_A_GGM_LUT_RB[73] */
            tuningBuf[0x00001128>>2] = 0x10A810A8; /* 0x15023128: DIP_A_GGM_LUT_RB[74] */
            tuningBuf[0x0000112C>>2] = 0x10AC10AC; /* 0x1502312C: DIP_A_GGM_LUT_RB[75] */
            tuningBuf[0x00001130>>2] = 0x10B010B0; /* 0x15023130: DIP_A_GGM_LUT_RB[76] */
            tuningBuf[0x00001134>>2] = 0x10B410B4; /* 0x15023134: DIP_A_GGM_LUT_RB[77] */
            tuningBuf[0x00001138>>2] = 0x10B810B8; /* 0x15023138: DIP_A_GGM_LUT_RB[78] */
            tuningBuf[0x0000113C>>2] = 0x10BC10BC; /* 0x1502313C: DIP_A_GGM_LUT_RB[79] */
            tuningBuf[0x00001140>>2] = 0x10C010C0; /* 0x15023140: DIP_A_GGM_LUT_RB[80] */
            tuningBuf[0x00001144>>2] = 0x10C410C4; /* 0x15023144: DIP_A_GGM_LUT_RB[81] */
            tuningBuf[0x00001148>>2] = 0x10C810C8; /* 0x15023148: DIP_A_GGM_LUT_RB[82] */
            tuningBuf[0x0000114C>>2] = 0x10CC10CC; /* 0x1502314C: DIP_A_GGM_LUT_RB[83] */
            tuningBuf[0x00001150>>2] = 0x10D010D0; /* 0x15023150: DIP_A_GGM_LUT_RB[84] */
            tuningBuf[0x00001154>>2] = 0x10D410D4; /* 0x15023154: DIP_A_GGM_LUT_RB[85] */
            tuningBuf[0x00001158>>2] = 0x10D810D8; /* 0x15023158: DIP_A_GGM_LUT_RB[86] */
            tuningBuf[0x0000115C>>2] = 0x10DC10DC; /* 0x1502315C: DIP_A_GGM_LUT_RB[87] */
            tuningBuf[0x00001160>>2] = 0x10E010E0; /* 0x15023160: DIP_A_GGM_LUT_RB[88] */
            tuningBuf[0x00001164>>2] = 0x10E410E4; /* 0x15023164: DIP_A_GGM_LUT_RB[89] */
            tuningBuf[0x00001168>>2] = 0x10E810E8; /* 0x15023168: DIP_A_GGM_LUT_RB[90] */
            tuningBuf[0x0000116C>>2] = 0x10EC10EC; /* 0x1502316C: DIP_A_GGM_LUT_RB[91] */
            tuningBuf[0x00001170>>2] = 0x10F010F0; /* 0x15023170: DIP_A_GGM_LUT_RB[92] */
            tuningBuf[0x00001174>>2] = 0x10F410F4; /* 0x15023174: DIP_A_GGM_LUT_RB[93] */
            tuningBuf[0x00001178>>2] = 0x10F810F8; /* 0x15023178: DIP_A_GGM_LUT_RB[94] */
            tuningBuf[0x0000117C>>2] = 0x10FC10FC; /* 0x1502317C: DIP_A_GGM_LUT_RB[95] */
            tuningBuf[0x00001180>>2] = 0x21002100; /* 0x15023180: DIP_A_GGM_LUT_RB[96] */
            tuningBuf[0x00001184>>2] = 0x21082108; /* 0x15023184: DIP_A_GGM_LUT_RB[97] */
            tuningBuf[0x00001188>>2] = 0x21102110; /* 0x15023188: DIP_A_GGM_LUT_RB[98] */
            tuningBuf[0x0000118C>>2] = 0x21182118; /* 0x1502318C: DIP_A_GGM_LUT_RB[99] */
            tuningBuf[0x00001190>>2] = 0x21202120; /* 0x15023190: DIP_A_GGM_LUT_RB[100] */
            tuningBuf[0x00001194>>2] = 0x21282128; /* 0x15023194: DIP_A_GGM_LUT_RB[101] */
            tuningBuf[0x00001198>>2] = 0x21302130; /* 0x15023198: DIP_A_GGM_LUT_RB[102] */
            tuningBuf[0x0000119C>>2] = 0x21382138; /* 0x1502319C: DIP_A_GGM_LUT_RB[103] */
            tuningBuf[0x000011A0>>2] = 0x21402140; /* 0x150231A0: DIP_A_GGM_LUT_RB[104] */
            tuningBuf[0x000011A4>>2] = 0x21482148; /* 0x150231A4: DIP_A_GGM_LUT_RB[105] */
            tuningBuf[0x000011A8>>2] = 0x21502150; /* 0x150231A8: DIP_A_GGM_LUT_RB[106] */
            tuningBuf[0x000011AC>>2] = 0x21582158; /* 0x150231AC: DIP_A_GGM_LUT_RB[107] */
            tuningBuf[0x000011B0>>2] = 0x21602160; /* 0x150231B0: DIP_A_GGM_LUT_RB[108] */
            tuningBuf[0x000011B4>>2] = 0x21682168; /* 0x150231B4: DIP_A_GGM_LUT_RB[109] */
            tuningBuf[0x000011B8>>2] = 0x21702170; /* 0x150231B8: DIP_A_GGM_LUT_RB[110] */
            tuningBuf[0x000011BC>>2] = 0x21782178; /* 0x150231BC: DIP_A_GGM_LUT_RB[111] */
            tuningBuf[0x000011C0>>2] = 0x21802180; /* 0x150231C0: DIP_A_GGM_LUT_RB[112] */
            tuningBuf[0x000011C4>>2] = 0x21882188; /* 0x150231C4: DIP_A_GGM_LUT_RB[113] */
            tuningBuf[0x000011C8>>2] = 0x21902190; /* 0x150231C8: DIP_A_GGM_LUT_RB[114] */
            tuningBuf[0x000011CC>>2] = 0x21982198; /* 0x150231CC: DIP_A_GGM_LUT_RB[115] */
            tuningBuf[0x000011D0>>2] = 0x21A021A0; /* 0x150231D0: DIP_A_GGM_LUT_RB[116] */
            tuningBuf[0x000011D4>>2] = 0x21A821A8; /* 0x150231D4: DIP_A_GGM_LUT_RB[117] */
            tuningBuf[0x000011D8>>2] = 0x21B021B0; /* 0x150231D8: DIP_A_GGM_LUT_RB[118] */
            tuningBuf[0x000011DC>>2] = 0x21B821B8; /* 0x150231DC: DIP_A_GGM_LUT_RB[119] */
            tuningBuf[0x000011E0>>2] = 0x21C021C0; /* 0x150231E0: DIP_A_GGM_LUT_RB[120] */
            tuningBuf[0x000011E4>>2] = 0x21C821C8; /* 0x150231E4: DIP_A_GGM_LUT_RB[121] */
            tuningBuf[0x000011E8>>2] = 0x21D021D0; /* 0x150231E8: DIP_A_GGM_LUT_RB[122] */
            tuningBuf[0x000011EC>>2] = 0x21D821D8; /* 0x150231EC: DIP_A_GGM_LUT_RB[123] */
            tuningBuf[0x000011F0>>2] = 0x21E021E0; /* 0x150231F0: DIP_A_GGM_LUT_RB[124] */
            tuningBuf[0x000011F4>>2] = 0x21E821E8; /* 0x150231F4: DIP_A_GGM_LUT_RB[125] */
            tuningBuf[0x000011F8>>2] = 0x21F021F0; /* 0x150231F8: DIP_A_GGM_LUT_RB[126] */
            tuningBuf[0x000011FC>>2] = 0x21F821F8; /* 0x150231FC: DIP_A_GGM_LUT_RB[127] */
            tuningBuf[0x00001200>>2] = 0x82008200; /* 0x15023200: DIP_A_GGM_LUT_RB[128] */
            tuningBuf[0x00001204>>2] = 0x82208220; /* 0x15023204: DIP_A_GGM_LUT_RB[129] */
            tuningBuf[0x00001208>>2] = 0x82408240; /* 0x15023208: DIP_A_GGM_LUT_RB[130] */
            tuningBuf[0x0000120C>>2] = 0x82608260; /* 0x1502320C: DIP_A_GGM_LUT_RB[131] */
            tuningBuf[0x00001210>>2] = 0x82808280; /* 0x15023210: DIP_A_GGM_LUT_RB[132] */
            tuningBuf[0x00001214>>2] = 0x82A082A0; /* 0x15023214: DIP_A_GGM_LUT_RB[133] */
            tuningBuf[0x00001218>>2] = 0x82C082C0; /* 0x15023218: DIP_A_GGM_LUT_RB[134] */
            tuningBuf[0x0000121C>>2] = 0x82E082E0; /* 0x1502321C: DIP_A_GGM_LUT_RB[135] */
            tuningBuf[0x00001220>>2] = 0x83008300; /* 0x15023220: DIP_A_GGM_LUT_RB[136] */
            tuningBuf[0x00001224>>2] = 0x83208320; /* 0x15023224: DIP_A_GGM_LUT_RB[137] */
            tuningBuf[0x00001228>>2] = 0x83408340; /* 0x15023228: DIP_A_GGM_LUT_RB[138] */
            tuningBuf[0x0000122C>>2] = 0x83608360; /* 0x1502322C: DIP_A_GGM_LUT_RB[139] */
            tuningBuf[0x00001230>>2] = 0x83808380; /* 0x15023230: DIP_A_GGM_LUT_RB[140] */
            tuningBuf[0x00001234>>2] = 0x83A083A0; /* 0x15023234: DIP_A_GGM_LUT_RB[141] */
            tuningBuf[0x00001238>>2] = 0x83C083C0; /* 0x15023238: DIP_A_GGM_LUT_RB[142] */
            tuningBuf[0x0000123C>>2] = 0x7FE07FE0; /* 0x1502323C: DIP_A_GGM_LUT_RB[143] */
            tuningBuf[0x00001240>>2] = 0x00000800; /* 0x15023240: DIP_A_GGM_LUT_G[0] */
            tuningBuf[0x00001244>>2] = 0x00000802; /* 0x15023244: DIP_A_GGM_LUT_G[1] */
            tuningBuf[0x00001248>>2] = 0x00000804; /* 0x15023248: DIP_A_GGM_LUT_G[2] */
            tuningBuf[0x0000124C>>2] = 0x00000806; /* 0x1502324C: DIP_A_GGM_LUT_G[3] */
            tuningBuf[0x00001250>>2] = 0x00000808; /* 0x15023250: DIP_A_GGM_LUT_G[4] */
            tuningBuf[0x00001254>>2] = 0x0000080A; /* 0x15023254: DIP_A_GGM_LUT_G[5] */
            tuningBuf[0x00001258>>2] = 0x0000080C; /* 0x15023258: DIP_A_GGM_LUT_G[6] */
            tuningBuf[0x0000125C>>2] = 0x0000080E; /* 0x1502325C: DIP_A_GGM_LUT_G[7] */
            tuningBuf[0x00001260>>2] = 0x00000810; /* 0x15023260: DIP_A_GGM_LUT_G[8] */
            tuningBuf[0x00001264>>2] = 0x00000812; /* 0x15023264: DIP_A_GGM_LUT_G[9] */
            tuningBuf[0x00001268>>2] = 0x00000814; /* 0x15023268: DIP_A_GGM_LUT_G[10] */
            tuningBuf[0x0000126C>>2] = 0x00000816; /* 0x1502326C: DIP_A_GGM_LUT_G[11] */
            tuningBuf[0x00001270>>2] = 0x00000818; /* 0x15023270: DIP_A_GGM_LUT_G[12] */
            tuningBuf[0x00001274>>2] = 0x0000081A; /* 0x15023274: DIP_A_GGM_LUT_G[13] */
            tuningBuf[0x00001278>>2] = 0x0000081C; /* 0x15023278: DIP_A_GGM_LUT_G[14] */
            tuningBuf[0x0000127C>>2] = 0x0000081E; /* 0x1502327C: DIP_A_GGM_LUT_G[15] */
            tuningBuf[0x00001280>>2] = 0x00000820; /* 0x15023280: DIP_A_GGM_LUT_G[16] */
            tuningBuf[0x00001284>>2] = 0x00000822; /* 0x15023284: DIP_A_GGM_LUT_G[17] */
            tuningBuf[0x00001288>>2] = 0x00000824; /* 0x15023288: DIP_A_GGM_LUT_G[18] */
            tuningBuf[0x0000128C>>2] = 0x00000826; /* 0x1502328C: DIP_A_GGM_LUT_G[19] */
            tuningBuf[0x00001290>>2] = 0x00000828; /* 0x15023290: DIP_A_GGM_LUT_G[20] */
            tuningBuf[0x00001294>>2] = 0x0000082A; /* 0x15023294: DIP_A_GGM_LUT_G[21] */
            tuningBuf[0x00001298>>2] = 0x0000082C; /* 0x15023298: DIP_A_GGM_LUT_G[22] */
            tuningBuf[0x0000129C>>2] = 0x0000082E; /* 0x1502329C: DIP_A_GGM_LUT_G[23] */
            tuningBuf[0x000012A0>>2] = 0x00000830; /* 0x150232A0: DIP_A_GGM_LUT_G[24] */
            tuningBuf[0x000012A4>>2] = 0x00000832; /* 0x150232A4: DIP_A_GGM_LUT_G[25] */
            tuningBuf[0x000012A8>>2] = 0x00000834; /* 0x150232A8: DIP_A_GGM_LUT_G[26] */
            tuningBuf[0x000012AC>>2] = 0x00000836; /* 0x150232AC: DIP_A_GGM_LUT_G[27] */
            tuningBuf[0x000012B0>>2] = 0x00000838; /* 0x150232B0: DIP_A_GGM_LUT_G[28] */
            tuningBuf[0x000012B4>>2] = 0x0000083A; /* 0x150232B4: DIP_A_GGM_LUT_G[29] */
            tuningBuf[0x000012B8>>2] = 0x0000083C; /* 0x150232B8: DIP_A_GGM_LUT_G[30] */
            tuningBuf[0x000012BC>>2] = 0x0000083E; /* 0x150232BC: DIP_A_GGM_LUT_G[31] */
            tuningBuf[0x000012C0>>2] = 0x00000840; /* 0x150232C0: DIP_A_GGM_LUT_G[32] */
            tuningBuf[0x000012C4>>2] = 0x00000842; /* 0x150232C4: DIP_A_GGM_LUT_G[33] */
            tuningBuf[0x000012C8>>2] = 0x00000844; /* 0x150232C8: DIP_A_GGM_LUT_G[34] */
            tuningBuf[0x000012CC>>2] = 0x00000846; /* 0x150232CC: DIP_A_GGM_LUT_G[35] */
            tuningBuf[0x000012D0>>2] = 0x00000848; /* 0x150232D0: DIP_A_GGM_LUT_G[36] */
            tuningBuf[0x000012D4>>2] = 0x0000084A; /* 0x150232D4: DIP_A_GGM_LUT_G[37] */
            tuningBuf[0x000012D8>>2] = 0x0000084C; /* 0x150232D8: DIP_A_GGM_LUT_G[38] */
            tuningBuf[0x000012DC>>2] = 0x0000084E; /* 0x150232DC: DIP_A_GGM_LUT_G[39] */
            tuningBuf[0x000012E0>>2] = 0x00000850; /* 0x150232E0: DIP_A_GGM_LUT_G[40] */
            tuningBuf[0x000012E4>>2] = 0x00000852; /* 0x150232E4: DIP_A_GGM_LUT_G[41] */
            tuningBuf[0x000012E8>>2] = 0x00000854; /* 0x150232E8: DIP_A_GGM_LUT_G[42] */
            tuningBuf[0x000012EC>>2] = 0x00000856; /* 0x150232EC: DIP_A_GGM_LUT_G[43] */
            tuningBuf[0x000012F0>>2] = 0x00000858; /* 0x150232F0: DIP_A_GGM_LUT_G[44] */
            tuningBuf[0x000012F4>>2] = 0x0000085A; /* 0x150232F4: DIP_A_GGM_LUT_G[45] */
            tuningBuf[0x000012F8>>2] = 0x0000085C; /* 0x150232F8: DIP_A_GGM_LUT_G[46] */
            tuningBuf[0x000012FC>>2] = 0x0000085E; /* 0x150232FC: DIP_A_GGM_LUT_G[47] */
            tuningBuf[0x00001300>>2] = 0x00000860; /* 0x15023300: DIP_A_GGM_LUT_G[48] */
            tuningBuf[0x00001304>>2] = 0x00000862; /* 0x15023304: DIP_A_GGM_LUT_G[49] */
            tuningBuf[0x00001308>>2] = 0x00000864; /* 0x15023308: DIP_A_GGM_LUT_G[50] */
            tuningBuf[0x0000130C>>2] = 0x00000866; /* 0x1502330C: DIP_A_GGM_LUT_G[51] */
            tuningBuf[0x00001310>>2] = 0x00000868; /* 0x15023310: DIP_A_GGM_LUT_G[52] */
            tuningBuf[0x00001314>>2] = 0x0000086A; /* 0x15023314: DIP_A_GGM_LUT_G[53] */
            tuningBuf[0x00001318>>2] = 0x0000086C; /* 0x15023318: DIP_A_GGM_LUT_G[54] */
            tuningBuf[0x0000131C>>2] = 0x0000086E; /* 0x1502331C: DIP_A_GGM_LUT_G[55] */
            tuningBuf[0x00001320>>2] = 0x00000870; /* 0x15023320: DIP_A_GGM_LUT_G[56] */
            tuningBuf[0x00001324>>2] = 0x00000872; /* 0x15023324: DIP_A_GGM_LUT_G[57] */
            tuningBuf[0x00001328>>2] = 0x00000874; /* 0x15023328: DIP_A_GGM_LUT_G[58] */
            tuningBuf[0x0000132C>>2] = 0x00000876; /* 0x1502332C: DIP_A_GGM_LUT_G[59] */
            tuningBuf[0x00001330>>2] = 0x00000878; /* 0x15023330: DIP_A_GGM_LUT_G[60] */
            tuningBuf[0x00001334>>2] = 0x0000087A; /* 0x15023334: DIP_A_GGM_LUT_G[61] */
            tuningBuf[0x00001338>>2] = 0x0000087C; /* 0x15023338: DIP_A_GGM_LUT_G[62] */
            tuningBuf[0x0000133C>>2] = 0x0000087E; /* 0x1502333C: DIP_A_GGM_LUT_G[63] */
            tuningBuf[0x00001340>>2] = 0x00001080; /* 0x15023340: DIP_A_GGM_LUT_G[64] */
            tuningBuf[0x00001344>>2] = 0x00001084; /* 0x15023344: DIP_A_GGM_LUT_G[65] */
            tuningBuf[0x00001348>>2] = 0x00001088; /* 0x15023348: DIP_A_GGM_LUT_G[66] */
            tuningBuf[0x0000134C>>2] = 0x0000108C; /* 0x1502334C: DIP_A_GGM_LUT_G[67] */
            tuningBuf[0x00001350>>2] = 0x00001090; /* 0x15023350: DIP_A_GGM_LUT_G[68] */
            tuningBuf[0x00001354>>2] = 0x00001094; /* 0x15023354: DIP_A_GGM_LUT_G[69] */
            tuningBuf[0x00001358>>2] = 0x00001098; /* 0x15023358: DIP_A_GGM_LUT_G[70] */
            tuningBuf[0x0000135C>>2] = 0x0000109C; /* 0x1502335C: DIP_A_GGM_LUT_G[71] */
            tuningBuf[0x00001360>>2] = 0x000010A0; /* 0x15023360: DIP_A_GGM_LUT_G[72] */
            tuningBuf[0x00001364>>2] = 0x000010A4; /* 0x15023364: DIP_A_GGM_LUT_G[73] */
            tuningBuf[0x00001368>>2] = 0x000010A8; /* 0x15023368: DIP_A_GGM_LUT_G[74] */
            tuningBuf[0x0000136C>>2] = 0x000010AC; /* 0x1502336C: DIP_A_GGM_LUT_G[75] */
            tuningBuf[0x00001370>>2] = 0x000010B0; /* 0x15023370: DIP_A_GGM_LUT_G[76] */
            tuningBuf[0x00001374>>2] = 0x000010B4; /* 0x15023374: DIP_A_GGM_LUT_G[77] */
            tuningBuf[0x00001378>>2] = 0x000010B8; /* 0x15023378: DIP_A_GGM_LUT_G[78] */
            tuningBuf[0x0000137C>>2] = 0x000010BC; /* 0x1502337C: DIP_A_GGM_LUT_G[79] */
            tuningBuf[0x00001380>>2] = 0x000010C0; /* 0x15023380: DIP_A_GGM_LUT_G[80] */
            tuningBuf[0x00001384>>2] = 0x000010C4; /* 0x15023384: DIP_A_GGM_LUT_G[81] */
            tuningBuf[0x00001388>>2] = 0x000010C8; /* 0x15023388: DIP_A_GGM_LUT_G[82] */
            tuningBuf[0x0000138C>>2] = 0x000010CC; /* 0x1502338C: DIP_A_GGM_LUT_G[83] */
            tuningBuf[0x00001390>>2] = 0x000010D0; /* 0x15023390: DIP_A_GGM_LUT_G[84] */
            tuningBuf[0x00001394>>2] = 0x000010D4; /* 0x15023394: DIP_A_GGM_LUT_G[85] */
            tuningBuf[0x00001398>>2] = 0x000010D8; /* 0x15023398: DIP_A_GGM_LUT_G[86] */
            tuningBuf[0x0000139C>>2] = 0x000010DC; /* 0x1502339C: DIP_A_GGM_LUT_G[87] */
            tuningBuf[0x000013A0>>2] = 0x000010E0; /* 0x150233A0: DIP_A_GGM_LUT_G[88] */
            tuningBuf[0x000013A4>>2] = 0x000010E4; /* 0x150233A4: DIP_A_GGM_LUT_G[89] */
            tuningBuf[0x000013A8>>2] = 0x000010E8; /* 0x150233A8: DIP_A_GGM_LUT_G[90] */
            tuningBuf[0x000013AC>>2] = 0x000010EC; /* 0x150233AC: DIP_A_GGM_LUT_G[91] */
            tuningBuf[0x000013B0>>2] = 0x000010F0; /* 0x150233B0: DIP_A_GGM_LUT_G[92] */
            tuningBuf[0x000013B4>>2] = 0x000010F4; /* 0x150233B4: DIP_A_GGM_LUT_G[93] */
            tuningBuf[0x000013B8>>2] = 0x000010F8; /* 0x150233B8: DIP_A_GGM_LUT_G[94] */
            tuningBuf[0x000013BC>>2] = 0x000010FC; /* 0x150233BC: DIP_A_GGM_LUT_G[95] */
            tuningBuf[0x000013C0>>2] = 0x00002100; /* 0x150233C0: DIP_A_GGM_LUT_G[96] */
            tuningBuf[0x000013C4>>2] = 0x00002108; /* 0x150233C4: DIP_A_GGM_LUT_G[97] */
            tuningBuf[0x000013C8>>2] = 0x00002110; /* 0x150233C8: DIP_A_GGM_LUT_G[98] */
            tuningBuf[0x000013CC>>2] = 0x00002118; /* 0x150233CC: DIP_A_GGM_LUT_G[99] */
            tuningBuf[0x000013D0>>2] = 0x00002120; /* 0x150233D0: DIP_A_GGM_LUT_G[100] */
            tuningBuf[0x000013D4>>2] = 0x00002128; /* 0x150233D4: DIP_A_GGM_LUT_G[101] */
            tuningBuf[0x000013D8>>2] = 0x00002130; /* 0x150233D8: DIP_A_GGM_LUT_G[102] */
            tuningBuf[0x000013DC>>2] = 0x00002138; /* 0x150233DC: DIP_A_GGM_LUT_G[103] */
            tuningBuf[0x000013E0>>2] = 0x00002140; /* 0x150233E0: DIP_A_GGM_LUT_G[104] */
            tuningBuf[0x000013E4>>2] = 0x00002148; /* 0x150233E4: DIP_A_GGM_LUT_G[105] */
            tuningBuf[0x000013E8>>2] = 0x00002150; /* 0x150233E8: DIP_A_GGM_LUT_G[106] */
            tuningBuf[0x000013EC>>2] = 0x00002158; /* 0x150233EC: DIP_A_GGM_LUT_G[107] */
            tuningBuf[0x000013F0>>2] = 0x00002160; /* 0x150233F0: DIP_A_GGM_LUT_G[108] */
            tuningBuf[0x000013F4>>2] = 0x00002168; /* 0x150233F4: DIP_A_GGM_LUT_G[109] */
            tuningBuf[0x000013F8>>2] = 0x00002170; /* 0x150233F8: DIP_A_GGM_LUT_G[110] */
            tuningBuf[0x000013FC>>2] = 0x00002178; /* 0x150233FC: DIP_A_GGM_LUT_G[111] */
            tuningBuf[0x00001400>>2] = 0x00002180; /* 0x15023400: DIP_A_GGM_LUT_G[112] */
            tuningBuf[0x00001404>>2] = 0x00002188; /* 0x15023404: DIP_A_GGM_LUT_G[113] */
            tuningBuf[0x00001408>>2] = 0x00002190; /* 0x15023408: DIP_A_GGM_LUT_G[114] */
            tuningBuf[0x0000140C>>2] = 0x00002198; /* 0x1502340C: DIP_A_GGM_LUT_G[115] */
            tuningBuf[0x00001410>>2] = 0x000021A0; /* 0x15023410: DIP_A_GGM_LUT_G[116] */
            tuningBuf[0x00001414>>2] = 0x000021A8; /* 0x15023414: DIP_A_GGM_LUT_G[117] */
            tuningBuf[0x00001418>>2] = 0x000021B0; /* 0x15023418: DIP_A_GGM_LUT_G[118] */
            tuningBuf[0x0000141C>>2] = 0x000021B8; /* 0x1502341C: DIP_A_GGM_LUT_G[119] */
            tuningBuf[0x00001420>>2] = 0x000021C0; /* 0x15023420: DIP_A_GGM_LUT_G[120] */
            tuningBuf[0x00001424>>2] = 0x000021C8; /* 0x15023424: DIP_A_GGM_LUT_G[121] */
            tuningBuf[0x00001428>>2] = 0x000021D0; /* 0x15023428: DIP_A_GGM_LUT_G[122] */
            tuningBuf[0x0000142C>>2] = 0x000021D8; /* 0x1502342C: DIP_A_GGM_LUT_G[123] */
            tuningBuf[0x00001430>>2] = 0x000021E0; /* 0x15023430: DIP_A_GGM_LUT_G[124] */
            tuningBuf[0x00001434>>2] = 0x000021E8; /* 0x15023434: DIP_A_GGM_LUT_G[125] */
            tuningBuf[0x00001438>>2] = 0x000021F0; /* 0x15023438: DIP_A_GGM_LUT_G[126] */
            tuningBuf[0x0000143C>>2] = 0x000021F8; /* 0x1502343C: DIP_A_GGM_LUT_G[127] */
            tuningBuf[0x00001440>>2] = 0x00008200; /* 0x15023440: DIP_A_GGM_LUT_G[128] */
            tuningBuf[0x00001444>>2] = 0x00008220; /* 0x15023444: DIP_A_GGM_LUT_G[129] */
            tuningBuf[0x00001448>>2] = 0x00008240; /* 0x15023448: DIP_A_GGM_LUT_G[130] */
            tuningBuf[0x0000144C>>2] = 0x00008260; /* 0x1502344C: DIP_A_GGM_LUT_G[131] */
            tuningBuf[0x00001450>>2] = 0x00008280; /* 0x15023450: DIP_A_GGM_LUT_G[132] */
            tuningBuf[0x00001454>>2] = 0x000082A0; /* 0x15023454: DIP_A_GGM_LUT_G[133] */
            tuningBuf[0x00001458>>2] = 0x000082C0; /* 0x15023458: DIP_A_GGM_LUT_G[134] */
            tuningBuf[0x0000145C>>2] = 0x000082E0; /* 0x1502345C: DIP_A_GGM_LUT_G[135] */
            tuningBuf[0x00001460>>2] = 0x00008300; /* 0x15023460: DIP_A_GGM_LUT_G[136] */
            tuningBuf[0x00001464>>2] = 0x00008320; /* 0x15023464: DIP_A_GGM_LUT_G[137] */
            tuningBuf[0x00001468>>2] = 0x00008340; /* 0x15023468: DIP_A_GGM_LUT_G[138] */
            tuningBuf[0x0000146C>>2] = 0x00008360; /* 0x1502346C: DIP_A_GGM_LUT_G[139] */
            tuningBuf[0x00001470>>2] = 0x00008380; /* 0x15023470: DIP_A_GGM_LUT_G[140] */
            tuningBuf[0x00001474>>2] = 0x000083A0; /* 0x15023474: DIP_A_GGM_LUT_G[141] */
            tuningBuf[0x00001478>>2] = 0x000083C0; /* 0x15023478: DIP_A_GGM_LUT_G[142] */
            tuningBuf[0x0000147C>>2] = 0x00007FE0; /* 0x1502347C: DIP_A_GGM_LUT_G[143] */
            tuningBuf[0x00001480>>2] = 0x00000000; /* 0x15023480: DIP_A_GGM_CTRL */
			#if 0
            tuningBuf[0x00003000>>2] = 0x51FC32A2; /* 0x15025000: DIP_B_GGM_LUT_RB[0] */
            tuningBuf[0x00003004>>2] = 0x7CD35AAB; /* 0x15025004: DIP_B_GGM_LUT_RB[1] */
            tuningBuf[0x00003008>>2] = 0x832D3C37; /* 0x15025008: DIP_B_GGM_LUT_RB[2] */
            tuningBuf[0x0000300C>>2] = 0x19837F05; /* 0x1502500C: DIP_B_GGM_LUT_RB[3] */
            tuningBuf[0x00003010>>2] = 0xC5849CE7; /* 0x15025010: DIP_B_GGM_LUT_RB[4] */
            tuningBuf[0x00003014>>2] = 0xF302BF18; /* 0x15025014: DIP_B_GGM_LUT_RB[5] */
            tuningBuf[0x00003018>>2] = 0xAFB97024; /* 0x15025018: DIP_B_GGM_LUT_RB[6] */
            tuningBuf[0x0000301C>>2] = 0x4D0CC69E; /* 0x1502501C: DIP_B_GGM_LUT_RB[7] */
            tuningBuf[0x00003020>>2] = 0xC4F0D74B; /* 0x15025020: DIP_B_GGM_LUT_RB[8] */
            tuningBuf[0x00003024>>2] = 0x66A581F1; /* 0x15025024: DIP_B_GGM_LUT_RB[9] */
            tuningBuf[0x00003028>>2] = 0x4FF5E73F; /* 0x15025028: DIP_B_GGM_LUT_RB[10] */
            tuningBuf[0x0000302C>>2] = 0xC4718E8E; /* 0x1502502C: DIP_B_GGM_LUT_RB[11] */
            tuningBuf[0x00003030>>2] = 0x9C86DCB9; /* 0x15025030: DIP_B_GGM_LUT_RB[12] */
            tuningBuf[0x00003034>>2] = 0xBC3F0C2E; /* 0x15025034: DIP_B_GGM_LUT_RB[13] */
            tuningBuf[0x00003038>>2] = 0xA0204380; /* 0x15025038: DIP_B_GGM_LUT_RB[14] */
            tuningBuf[0x0000303C>>2] = 0x70314E74; /* 0x1502503C: DIP_B_GGM_LUT_RB[15] */
            tuningBuf[0x00003040>>2] = 0x5C52EDA8; /* 0x15025040: DIP_B_GGM_LUT_RB[16] */
            tuningBuf[0x00003044>>2] = 0x0C77F482; /* 0x15025044: DIP_B_GGM_LUT_RB[17] */
            tuningBuf[0x00003048>>2] = 0x2CE55E8C; /* 0x15025048: DIP_B_GGM_LUT_RB[18] */
            tuningBuf[0x0000304C>>2] = 0xC2B86EF5; /* 0x1502504C: DIP_B_GGM_LUT_RB[19] */
            tuningBuf[0x00003050>>2] = 0x118F80F7; /* 0x15025050: DIP_B_GGM_LUT_RB[20] */
            tuningBuf[0x00003054>>2] = 0xF430D1D9; /* 0x15025054: DIP_B_GGM_LUT_RB[21] */
            tuningBuf[0x00003058>>2] = 0x543E5522; /* 0x15025058: DIP_B_GGM_LUT_RB[22] */
            tuningBuf[0x0000305C>>2] = 0xF0F5A76C; /* 0x1502505C: DIP_B_GGM_LUT_RB[23] */
            tuningBuf[0x00003060>>2] = 0x446E7727; /* 0x15025060: DIP_B_GGM_LUT_RB[24] */
            tuningBuf[0x00003064>>2] = 0xE1A63E64; /* 0x15025064: DIP_B_GGM_LUT_RB[25] */
            tuningBuf[0x00003068>>2] = 0xEC939B44; /* 0x15025068: DIP_B_GGM_LUT_RB[26] */
            tuningBuf[0x0000306C>>2] = 0xC8ABC865; /* 0x1502506C: DIP_B_GGM_LUT_RB[27] */
            tuningBuf[0x00003070>>2] = 0x8BE4B9AD; /* 0x15025070: DIP_B_GGM_LUT_RB[28] */
            tuningBuf[0x00003074>>2] = 0x0CE97EDB; /* 0x15025074: DIP_B_GGM_LUT_RB[29] */
            tuningBuf[0x00003078>>2] = 0x38FD6F8D; /* 0x15025078: DIP_B_GGM_LUT_RB[30] */
            tuningBuf[0x0000307C>>2] = 0xA34FC705; /* 0x1502507C: DIP_B_GGM_LUT_RB[31] */
            tuningBuf[0x00003080>>2] = 0x875961C7; /* 0x15025080: DIP_B_GGM_LUT_RB[32] */
            tuningBuf[0x00003084>>2] = 0x1C4F550D; /* 0x15025084: DIP_B_GGM_LUT_RB[33] */
            tuningBuf[0x00003088>>2] = 0x7B40D1D2; /* 0x15025088: DIP_B_GGM_LUT_RB[34] */
            tuningBuf[0x0000308C>>2] = 0xB069AF63; /* 0x1502508C: DIP_B_GGM_LUT_RB[35] */
            tuningBuf[0x00003090>>2] = 0x8AD7D124; /* 0x15025090: DIP_B_GGM_LUT_RB[36] */
            tuningBuf[0x00003094>>2] = 0xF9EDA6D1; /* 0x15025094: DIP_B_GGM_LUT_RB[37] */
            tuningBuf[0x00003098>>2] = 0x5D907A88; /* 0x15025098: DIP_B_GGM_LUT_RB[38] */
            tuningBuf[0x0000309C>>2] = 0x2E233543; /* 0x1502509C: DIP_B_GGM_LUT_RB[39] */
            tuningBuf[0x000030A0>>2] = 0x80AD5315; /* 0x150250A0: DIP_B_GGM_LUT_RB[40] */
            tuningBuf[0x000030A4>>2] = 0xC3E272CB; /* 0x150250A4: DIP_B_GGM_LUT_RB[41] */
            tuningBuf[0x000030A8>>2] = 0x846AD653; /* 0x150250A8: DIP_B_GGM_LUT_RB[42] */
            tuningBuf[0x000030AC>>2] = 0x9A89C881; /* 0x150250AC: DIP_B_GGM_LUT_RB[43] */
            tuningBuf[0x000030B0>>2] = 0x5A90B9ED; /* 0x150250B0: DIP_B_GGM_LUT_RB[44] */
            tuningBuf[0x000030B4>>2] = 0x0BD2F910; /* 0x150250B4: DIP_B_GGM_LUT_RB[45] */
            tuningBuf[0x000030B8>>2] = 0xEFE451E9; /* 0x150250B8: DIP_B_GGM_LUT_RB[46] */
            tuningBuf[0x000030BC>>2] = 0x0E57DB60; /* 0x150250BC: DIP_B_GGM_LUT_RB[47] */
            tuningBuf[0x000030C0>>2] = 0x1444C642; /* 0x150250C0: DIP_B_GGM_LUT_RB[48] */
            tuningBuf[0x000030C4>>2] = 0x0E04AFD6; /* 0x150250C4: DIP_B_GGM_LUT_RB[49] */
            tuningBuf[0x000030C8>>2] = 0x2BD9F148; /* 0x150250C8: DIP_B_GGM_LUT_RB[50] */
            tuningBuf[0x000030CC>>2] = 0x9D1D1E6E; /* 0x150250CC: DIP_B_GGM_LUT_RB[51] */
            tuningBuf[0x000030D0>>2] = 0xEBEE3E03; /* 0x150250D0: DIP_B_GGM_LUT_RB[52] */
            tuningBuf[0x000030D4>>2] = 0x1EBB3E11; /* 0x150250D4: DIP_B_GGM_LUT_RB[53] */
            tuningBuf[0x000030D8>>2] = 0x2CC18D75; /* 0x150250D8: DIP_B_GGM_LUT_RB[54] */
            tuningBuf[0x000030DC>>2] = 0xEA162348; /* 0x150250DC: DIP_B_GGM_LUT_RB[55] */
            tuningBuf[0x000030E0>>2] = 0xE3E7EB69; /* 0x150250E0: DIP_B_GGM_LUT_RB[56] */
            tuningBuf[0x000030E4>>2] = 0x7ACFE8FD; /* 0x150250E4: DIP_B_GGM_LUT_RB[57] */
            tuningBuf[0x000030E8>>2] = 0xA4253C0A; /* 0x150250E8: DIP_B_GGM_LUT_RB[58] */
            tuningBuf[0x000030EC>>2] = 0x8B03FFA2; /* 0x150250EC: DIP_B_GGM_LUT_RB[59] */
            tuningBuf[0x000030F0>>2] = 0xE8994F52; /* 0x150250F0: DIP_B_GGM_LUT_RB[60] */
            tuningBuf[0x000030F4>>2] = 0xCF5DDB50; /* 0x150250F4: DIP_B_GGM_LUT_RB[61] */
            tuningBuf[0x000030F8>>2] = 0xA6BF21A2; /* 0x150250F8: DIP_B_GGM_LUT_RB[62] */
            tuningBuf[0x000030FC>>2] = 0xB98A101C; /* 0x150250FC: DIP_B_GGM_LUT_RB[63] */
            tuningBuf[0x00003100>>2] = 0x9BDA2515; /* 0x15025100: DIP_B_GGM_LUT_RB[64] */
            tuningBuf[0x00003104>>2] = 0xF9A256DF; /* 0x15025104: DIP_B_GGM_LUT_RB[65] */
            tuningBuf[0x00003108>>2] = 0xD84834D4; /* 0x15025108: DIP_B_GGM_LUT_RB[66] */
            tuningBuf[0x0000310C>>2] = 0x9FD42127; /* 0x1502510C: DIP_B_GGM_LUT_RB[67] */
            tuningBuf[0x00003110>>2] = 0x357C27D1; /* 0x15025110: DIP_B_GGM_LUT_RB[68] */
            tuningBuf[0x00003114>>2] = 0x053D1CD3; /* 0x15025114: DIP_B_GGM_LUT_RB[69] */
            tuningBuf[0x00003118>>2] = 0x758FA6EB; /* 0x15025118: DIP_B_GGM_LUT_RB[70] */
            tuningBuf[0x0000311C>>2] = 0xB4D85B4C; /* 0x1502511C: DIP_B_GGM_LUT_RB[71] */
            tuningBuf[0x00003120>>2] = 0x85F839D1; /* 0x15025120: DIP_B_GGM_LUT_RB[72] */
            tuningBuf[0x00003124>>2] = 0xFFE9F8ED; /* 0x15025124: DIP_B_GGM_LUT_RB[73] */
            tuningBuf[0x00003128>>2] = 0x45EF466C; /* 0x15025128: DIP_B_GGM_LUT_RB[74] */
            tuningBuf[0x0000312C>>2] = 0xF2875267; /* 0x1502512C: DIP_B_GGM_LUT_RB[75] */
            tuningBuf[0x00003130>>2] = 0x396A9866; /* 0x15025130: DIP_B_GGM_LUT_RB[76] */
            tuningBuf[0x00003134>>2] = 0xB3DE96C4; /* 0x15025134: DIP_B_GGM_LUT_RB[77] */
            tuningBuf[0x00003138>>2] = 0x1085FA77; /* 0x15025138: DIP_B_GGM_LUT_RB[78] */
            tuningBuf[0x0000313C>>2] = 0x97BBB97B; /* 0x1502513C: DIP_B_GGM_LUT_RB[79] */
            tuningBuf[0x00003140>>2] = 0xD9B2A1B9; /* 0x15025140: DIP_B_GGM_LUT_RB[80] */
            tuningBuf[0x00003144>>2] = 0x5FCFCE67; /* 0x15025144: DIP_B_GGM_LUT_RB[81] */
            tuningBuf[0x00003148>>2] = 0x969A3174; /* 0x15025148: DIP_B_GGM_LUT_RB[82] */
            tuningBuf[0x0000314C>>2] = 0xF94C1AF7; /* 0x1502514C: DIP_B_GGM_LUT_RB[83] */
            tuningBuf[0x00003150>>2] = 0x0A95B6C1; /* 0x15025150: DIP_B_GGM_LUT_RB[84] */
            tuningBuf[0x00003154>>2] = 0x0D245257; /* 0x15025154: DIP_B_GGM_LUT_RB[85] */
            tuningBuf[0x00003158>>2] = 0xF042416F; /* 0x15025158: DIP_B_GGM_LUT_RB[86] */
            tuningBuf[0x0000315C>>2] = 0x270D5666; /* 0x1502515C: DIP_B_GGM_LUT_RB[87] */
            tuningBuf[0x00003160>>2] = 0xD1DF52FC; /* 0x15025160: DIP_B_GGM_LUT_RB[88] */
            tuningBuf[0x00003164>>2] = 0xA93F054E; /* 0x15025164: DIP_B_GGM_LUT_RB[89] */
            tuningBuf[0x00003168>>2] = 0xDA58E517; /* 0x15025168: DIP_B_GGM_LUT_RB[90] */
            tuningBuf[0x0000316C>>2] = 0x827E0CCA; /* 0x1502516C: DIP_B_GGM_LUT_RB[91] */
            tuningBuf[0x00003170>>2] = 0x4E469D0B; /* 0x15025170: DIP_B_GGM_LUT_RB[92] */
            tuningBuf[0x00003174>>2] = 0xD9D8B48C; /* 0x15025174: DIP_B_GGM_LUT_RB[93] */
            tuningBuf[0x00003178>>2] = 0xFC7ACAF0; /* 0x15025178: DIP_B_GGM_LUT_RB[94] */
            tuningBuf[0x0000317C>>2] = 0x85EA04C7; /* 0x1502517C: DIP_B_GGM_LUT_RB[95] */
            tuningBuf[0x00003180>>2] = 0x8292AA9D; /* 0x15025180: DIP_B_GGM_LUT_RB[96] */
            tuningBuf[0x00003184>>2] = 0xA75FFC20; /* 0x15025184: DIP_B_GGM_LUT_RB[97] */
            tuningBuf[0x00003188>>2] = 0xA4CDA426; /* 0x15025188: DIP_B_GGM_LUT_RB[98] */
            tuningBuf[0x0000318C>>2] = 0xF4CF7855; /* 0x1502518C: DIP_B_GGM_LUT_RB[99] */
            tuningBuf[0x00003190>>2] = 0x1ABC3489; /* 0x15025190: DIP_B_GGM_LUT_RB[100] */
            tuningBuf[0x00003194>>2] = 0x1DD15E31; /* 0x15025194: DIP_B_GGM_LUT_RB[101] */
            tuningBuf[0x00003198>>2] = 0x52944250; /* 0x15025198: DIP_B_GGM_LUT_RB[102] */
            tuningBuf[0x0000319C>>2] = 0xBC53E69F; /* 0x1502519C: DIP_B_GGM_LUT_RB[103] */
            tuningBuf[0x000031A0>>2] = 0x40B1AF29; /* 0x150251A0: DIP_B_GGM_LUT_RB[104] */
            tuningBuf[0x000031A4>>2] = 0x91AEEBAE; /* 0x150251A4: DIP_B_GGM_LUT_RB[105] */
            tuningBuf[0x000031A8>>2] = 0xDFCC883F; /* 0x150251A8: DIP_B_GGM_LUT_RB[106] */
            tuningBuf[0x000031AC>>2] = 0xFE93F521; /* 0x150251AC: DIP_B_GGM_LUT_RB[107] */
            tuningBuf[0x000031B0>>2] = 0xC3FE399C; /* 0x150251B0: DIP_B_GGM_LUT_RB[108] */
            tuningBuf[0x000031B4>>2] = 0xB06CCBF5; /* 0x150251B4: DIP_B_GGM_LUT_RB[109] */
            tuningBuf[0x000031B8>>2] = 0xA1941704; /* 0x150251B8: DIP_B_GGM_LUT_RB[110] */
            tuningBuf[0x000031BC>>2] = 0xC8A9CE6A; /* 0x150251BC: DIP_B_GGM_LUT_RB[111] */
            tuningBuf[0x000031C0>>2] = 0xB466055C; /* 0x150251C0: DIP_B_GGM_LUT_RB[112] */
            tuningBuf[0x000031C4>>2] = 0xD2CF41C1; /* 0x150251C4: DIP_B_GGM_LUT_RB[113] */
            tuningBuf[0x000031C8>>2] = 0x2F900ED7; /* 0x150251C8: DIP_B_GGM_LUT_RB[114] */
            tuningBuf[0x000031CC>>2] = 0xD0A13E6D; /* 0x150251CC: DIP_B_GGM_LUT_RB[115] */
            tuningBuf[0x000031D0>>2] = 0x23EB776C; /* 0x150251D0: DIP_B_GGM_LUT_RB[116] */
            tuningBuf[0x000031D4>>2] = 0xF8329688; /* 0x150251D4: DIP_B_GGM_LUT_RB[117] */
            tuningBuf[0x000031D8>>2] = 0x04BF03BA; /* 0x150251D8: DIP_B_GGM_LUT_RB[118] */
            tuningBuf[0x000031DC>>2] = 0xFE0383A3; /* 0x150251DC: DIP_B_GGM_LUT_RB[119] */
            tuningBuf[0x000031E0>>2] = 0xB9F354D1; /* 0x150251E0: DIP_B_GGM_LUT_RB[120] */
            tuningBuf[0x000031E4>>2] = 0x1FC774F3; /* 0x150251E4: DIP_B_GGM_LUT_RB[121] */
            tuningBuf[0x000031E8>>2] = 0x8950DC74; /* 0x150251E8: DIP_B_GGM_LUT_RB[122] */
            tuningBuf[0x000031EC>>2] = 0x58C6BA69; /* 0x150251EC: DIP_B_GGM_LUT_RB[123] */
            tuningBuf[0x000031F0>>2] = 0x807E3B2D; /* 0x150251F0: DIP_B_GGM_LUT_RB[124] */
            tuningBuf[0x000031F4>>2] = 0xF2342D86; /* 0x150251F4: DIP_B_GGM_LUT_RB[125] */
            tuningBuf[0x000031F8>>2] = 0x809CBF51; /* 0x150251F8: DIP_B_GGM_LUT_RB[126] */
            tuningBuf[0x000031FC>>2] = 0x2FFC258F; /* 0x150251FC: DIP_B_GGM_LUT_RB[127] */
            tuningBuf[0x00003200>>2] = 0x9FE0EF0E; /* 0x15025200: DIP_B_GGM_LUT_RB[128] */
            tuningBuf[0x00003204>>2] = 0x05BE409B; /* 0x15025204: DIP_B_GGM_LUT_RB[129] */
            tuningBuf[0x00003208>>2] = 0x3FEDF830; /* 0x15025208: DIP_B_GGM_LUT_RB[130] */
            tuningBuf[0x0000320C>>2] = 0xD991AED8; /* 0x1502520C: DIP_B_GGM_LUT_RB[131] */
            tuningBuf[0x00003210>>2] = 0x95B77374; /* 0x15025210: DIP_B_GGM_LUT_RB[132] */
            tuningBuf[0x00003214>>2] = 0x92B3573D; /* 0x15025214: DIP_B_GGM_LUT_RB[133] */
            tuningBuf[0x00003218>>2] = 0x267E8F95; /* 0x15025218: DIP_B_GGM_LUT_RB[134] */
            tuningBuf[0x0000321C>>2] = 0x9F030BEC; /* 0x1502521C: DIP_B_GGM_LUT_RB[135] */
            tuningBuf[0x00003220>>2] = 0x1C0B9A54; /* 0x15025220: DIP_B_GGM_LUT_RB[136] */
            tuningBuf[0x00003224>>2] = 0x53454A3A; /* 0x15025224: DIP_B_GGM_LUT_RB[137] */
            tuningBuf[0x00003228>>2] = 0xC44FF7FB; /* 0x15025228: DIP_B_GGM_LUT_RB[138] */
            tuningBuf[0x0000322C>>2] = 0x8B920BAF; /* 0x1502522C: DIP_B_GGM_LUT_RB[139] */
            tuningBuf[0x00003230>>2] = 0xBB36387E; /* 0x15025230: DIP_B_GGM_LUT_RB[140] */
            tuningBuf[0x00003234>>2] = 0x19D78E97; /* 0x15025234: DIP_B_GGM_LUT_RB[141] */
            tuningBuf[0x00003238>>2] = 0x1B3BBF42; /* 0x15025238: DIP_B_GGM_LUT_RB[142] */
            tuningBuf[0x0000323C>>2] = 0x1086B7BD; /* 0x1502523C: DIP_B_GGM_LUT_RB[143] */
            tuningBuf[0x00003240>>2] = 0x0000AF3A; /* 0x15025240: DIP_B_GGM_LUT_G[0] */
            tuningBuf[0x00003244>>2] = 0x00004C1D; /* 0x15025244: DIP_B_GGM_LUT_G[1] */
            tuningBuf[0x00003248>>2] = 0x000061FD; /* 0x15025248: DIP_B_GGM_LUT_G[2] */
            tuningBuf[0x0000324C>>2] = 0x000088A7; /* 0x1502524C: DIP_B_GGM_LUT_G[3] */
            tuningBuf[0x00003250>>2] = 0x00002412; /* 0x15025250: DIP_B_GGM_LUT_G[4] */
            tuningBuf[0x00003254>>2] = 0x0000332D; /* 0x15025254: DIP_B_GGM_LUT_G[5] */
            tuningBuf[0x00003258>>2] = 0x0000F137; /* 0x15025258: DIP_B_GGM_LUT_G[6] */
            tuningBuf[0x0000325C>>2] = 0x00007A64; /* 0x1502525C: DIP_B_GGM_LUT_G[7] */
            tuningBuf[0x00003260>>2] = 0x000064ED; /* 0x15025260: DIP_B_GGM_LUT_G[8] */
            tuningBuf[0x00003264>>2] = 0x000083FA; /* 0x15025264: DIP_B_GGM_LUT_G[9] */
            tuningBuf[0x00003268>>2] = 0x0000C410; /* 0x15025268: DIP_B_GGM_LUT_G[10] */
            tuningBuf[0x0000326C>>2] = 0x000076FD; /* 0x1502526C: DIP_B_GGM_LUT_G[11] */
            tuningBuf[0x00003270>>2] = 0x0000310F; /* 0x15025270: DIP_B_GGM_LUT_G[12] */
            tuningBuf[0x00003274>>2] = 0x000076DC; /* 0x15025274: DIP_B_GGM_LUT_G[13] */
            tuningBuf[0x00003278>>2] = 0x0000B6B4; /* 0x15025278: DIP_B_GGM_LUT_G[14] */
            tuningBuf[0x0000327C>>2] = 0x00003CEF; /* 0x1502527C: DIP_B_GGM_LUT_G[15] */
            tuningBuf[0x00003280>>2] = 0x000077F2; /* 0x15025280: DIP_B_GGM_LUT_G[16] */
            tuningBuf[0x00003284>>2] = 0x0000902D; /* 0x15025284: DIP_B_GGM_LUT_G[17] */
            tuningBuf[0x00003288>>2] = 0x00009E30; /* 0x15025288: DIP_B_GGM_LUT_G[18] */
            tuningBuf[0x0000328C>>2] = 0x0000FA5C; /* 0x1502528C: DIP_B_GGM_LUT_G[19] */
            tuningBuf[0x00003290>>2] = 0x0000A3AB; /* 0x15025290: DIP_B_GGM_LUT_G[20] */
            tuningBuf[0x00003294>>2] = 0x0000F9E5; /* 0x15025294: DIP_B_GGM_LUT_G[21] */
            tuningBuf[0x00003298>>2] = 0x0000F6DA; /* 0x15025298: DIP_B_GGM_LUT_G[22] */
            tuningBuf[0x0000329C>>2] = 0x00002058; /* 0x1502529C: DIP_B_GGM_LUT_G[23] */
            tuningBuf[0x000032A0>>2] = 0x00001DB8; /* 0x150252A0: DIP_B_GGM_LUT_G[24] */
            tuningBuf[0x000032A4>>2] = 0x00003EE7; /* 0x150252A4: DIP_B_GGM_LUT_G[25] */
            tuningBuf[0x000032A8>>2] = 0x0000B8B1; /* 0x150252A8: DIP_B_GGM_LUT_G[26] */
            tuningBuf[0x000032AC>>2] = 0x000090F3; /* 0x150252AC: DIP_B_GGM_LUT_G[27] */
            tuningBuf[0x000032B0>>2] = 0x00001637; /* 0x150252B0: DIP_B_GGM_LUT_G[28] */
            tuningBuf[0x000032B4>>2] = 0x00007895; /* 0x150252B4: DIP_B_GGM_LUT_G[29] */
            tuningBuf[0x000032B8>>2] = 0x00003BF4; /* 0x150252B8: DIP_B_GGM_LUT_G[30] */
            tuningBuf[0x000032BC>>2] = 0x0000AF04; /* 0x150252BC: DIP_B_GGM_LUT_G[31] */
            tuningBuf[0x000032C0>>2] = 0x0000119A; /* 0x150252C0: DIP_B_GGM_LUT_G[32] */
            tuningBuf[0x000032C4>>2] = 0x0000A4C0; /* 0x150252C4: DIP_B_GGM_LUT_G[33] */
            tuningBuf[0x000032C8>>2] = 0x000000B9; /* 0x150252C8: DIP_B_GGM_LUT_G[34] */
            tuningBuf[0x000032CC>>2] = 0x0000531E; /* 0x150252CC: DIP_B_GGM_LUT_G[35] */
            tuningBuf[0x000032D0>>2] = 0x0000BEAC; /* 0x150252D0: DIP_B_GGM_LUT_G[36] */
            tuningBuf[0x000032D4>>2] = 0x00004E6B; /* 0x150252D4: DIP_B_GGM_LUT_G[37] */
            tuningBuf[0x000032D8>>2] = 0x00005BE5; /* 0x150252D8: DIP_B_GGM_LUT_G[38] */
            tuningBuf[0x000032DC>>2] = 0x000008F7; /* 0x150252DC: DIP_B_GGM_LUT_G[39] */
            tuningBuf[0x000032E0>>2] = 0x0000D97D; /* 0x150252E0: DIP_B_GGM_LUT_G[40] */
            tuningBuf[0x000032E4>>2] = 0x000069F5; /* 0x150252E4: DIP_B_GGM_LUT_G[41] */
            tuningBuf[0x000032E8>>2] = 0x00002048; /* 0x150252E8: DIP_B_GGM_LUT_G[42] */
            tuningBuf[0x000032EC>>2] = 0x000088A8; /* 0x150252EC: DIP_B_GGM_LUT_G[43] */
            tuningBuf[0x000032F0>>2] = 0x0000C246; /* 0x150252F0: DIP_B_GGM_LUT_G[44] */
            tuningBuf[0x000032F4>>2] = 0x0000EFE7; /* 0x150252F4: DIP_B_GGM_LUT_G[45] */
            tuningBuf[0x000032F8>>2] = 0x00002821; /* 0x150252F8: DIP_B_GGM_LUT_G[46] */
            tuningBuf[0x000032FC>>2] = 0x00005662; /* 0x150252FC: DIP_B_GGM_LUT_G[47] */
            tuningBuf[0x00003300>>2] = 0x0000EF56; /* 0x15025300: DIP_B_GGM_LUT_G[48] */
            tuningBuf[0x00003304>>2] = 0x00000C9A; /* 0x15025304: DIP_B_GGM_LUT_G[49] */
            tuningBuf[0x00003308>>2] = 0x0000C677; /* 0x15025308: DIP_B_GGM_LUT_G[50] */
            tuningBuf[0x0000330C>>2] = 0x0000D528; /* 0x1502530C: DIP_B_GGM_LUT_G[51] */
            tuningBuf[0x00003310>>2] = 0x0000A1AA; /* 0x15025310: DIP_B_GGM_LUT_G[52] */
            tuningBuf[0x00003314>>2] = 0x0000C734; /* 0x15025314: DIP_B_GGM_LUT_G[53] */
            tuningBuf[0x00003318>>2] = 0x0000E931; /* 0x15025318: DIP_B_GGM_LUT_G[54] */
            tuningBuf[0x0000331C>>2] = 0x00001E41; /* 0x1502531C: DIP_B_GGM_LUT_G[55] */
            tuningBuf[0x00003320>>2] = 0x0000FF7E; /* 0x15025320: DIP_B_GGM_LUT_G[56] */
            tuningBuf[0x00003324>>2] = 0x0000827C; /* 0x15025324: DIP_B_GGM_LUT_G[57] */
            tuningBuf[0x00003328>>2] = 0x0000AC78; /* 0x15025328: DIP_B_GGM_LUT_G[58] */
            tuningBuf[0x0000332C>>2] = 0x000070F7; /* 0x1502532C: DIP_B_GGM_LUT_G[59] */
            tuningBuf[0x00003330>>2] = 0x000054D4; /* 0x15025330: DIP_B_GGM_LUT_G[60] */
            tuningBuf[0x00003334>>2] = 0x0000950D; /* 0x15025334: DIP_B_GGM_LUT_G[61] */
            tuningBuf[0x00003338>>2] = 0x0000D624; /* 0x15025338: DIP_B_GGM_LUT_G[62] */
            tuningBuf[0x0000333C>>2] = 0x00002151; /* 0x1502533C: DIP_B_GGM_LUT_G[63] */
            tuningBuf[0x00003340>>2] = 0x00004241; /* 0x15025340: DIP_B_GGM_LUT_G[64] */
            tuningBuf[0x00003344>>2] = 0x00001A91; /* 0x15025344: DIP_B_GGM_LUT_G[65] */
            tuningBuf[0x00003348>>2] = 0x0000C2E7; /* 0x15025348: DIP_B_GGM_LUT_G[66] */
            tuningBuf[0x0000334C>>2] = 0x0000FBF6; /* 0x1502534C: DIP_B_GGM_LUT_G[67] */
            tuningBuf[0x00003350>>2] = 0x0000CDD3; /* 0x15025350: DIP_B_GGM_LUT_G[68] */
            tuningBuf[0x00003354>>2] = 0x00005C1F; /* 0x15025354: DIP_B_GGM_LUT_G[69] */
            tuningBuf[0x00003358>>2] = 0x00002A50; /* 0x15025358: DIP_B_GGM_LUT_G[70] */
            tuningBuf[0x0000335C>>2] = 0x0000ED09; /* 0x1502535C: DIP_B_GGM_LUT_G[71] */
            tuningBuf[0x00003360>>2] = 0x00006FA8; /* 0x15025360: DIP_B_GGM_LUT_G[72] */
            tuningBuf[0x00003364>>2] = 0x0000BBC4; /* 0x15025364: DIP_B_GGM_LUT_G[73] */
            tuningBuf[0x00003368>>2] = 0x00003E82; /* 0x15025368: DIP_B_GGM_LUT_G[74] */
            tuningBuf[0x0000336C>>2] = 0x0000BE3C; /* 0x1502536C: DIP_B_GGM_LUT_G[75] */
            tuningBuf[0x00003370>>2] = 0x0000756E; /* 0x15025370: DIP_B_GGM_LUT_G[76] */
            tuningBuf[0x00003374>>2] = 0x00009E14; /* 0x15025374: DIP_B_GGM_LUT_G[77] */
            tuningBuf[0x00003378>>2] = 0x0000EF5C; /* 0x15025378: DIP_B_GGM_LUT_G[78] */
            tuningBuf[0x0000337C>>2] = 0x0000B770; /* 0x1502537C: DIP_B_GGM_LUT_G[79] */
            tuningBuf[0x00003380>>2] = 0x000057C0; /* 0x15025380: DIP_B_GGM_LUT_G[80] */
            tuningBuf[0x00003384>>2] = 0x0000A47B; /* 0x15025384: DIP_B_GGM_LUT_G[81] */
            tuningBuf[0x00003388>>2] = 0x000041B0; /* 0x15025388: DIP_B_GGM_LUT_G[82] */
            tuningBuf[0x0000338C>>2] = 0x0000E787; /* 0x1502538C: DIP_B_GGM_LUT_G[83] */
            tuningBuf[0x00003390>>2] = 0x000067E3; /* 0x15025390: DIP_B_GGM_LUT_G[84] */
            tuningBuf[0x00003394>>2] = 0x00002BC6; /* 0x15025394: DIP_B_GGM_LUT_G[85] */
            tuningBuf[0x00003398>>2] = 0x0000BBD8; /* 0x15025398: DIP_B_GGM_LUT_G[86] */
            tuningBuf[0x0000339C>>2] = 0x0000057A; /* 0x1502539C: DIP_B_GGM_LUT_G[87] */
            tuningBuf[0x000033A0>>2] = 0x00003BFF; /* 0x150253A0: DIP_B_GGM_LUT_G[88] */
            tuningBuf[0x000033A4>>2] = 0x00000122; /* 0x150253A4: DIP_B_GGM_LUT_G[89] */
            tuningBuf[0x000033A8>>2] = 0x0000D958; /* 0x150253A8: DIP_B_GGM_LUT_G[90] */
            tuningBuf[0x000033AC>>2] = 0x000035A9; /* 0x150253AC: DIP_B_GGM_LUT_G[91] */
            tuningBuf[0x000033B0>>2] = 0x0000A94E; /* 0x150253B0: DIP_B_GGM_LUT_G[92] */
            tuningBuf[0x000033B4>>2] = 0x0000D3F6; /* 0x150253B4: DIP_B_GGM_LUT_G[93] */
            tuningBuf[0x000033B8>>2] = 0x00000D3F; /* 0x150253B8: DIP_B_GGM_LUT_G[94] */
            tuningBuf[0x000033BC>>2] = 0x00009276; /* 0x150253BC: DIP_B_GGM_LUT_G[95] */
            tuningBuf[0x000033C0>>2] = 0x0000E1DC; /* 0x150253C0: DIP_B_GGM_LUT_G[96] */
            tuningBuf[0x000033C4>>2] = 0x0000AFB4; /* 0x150253C4: DIP_B_GGM_LUT_G[97] */
            tuningBuf[0x000033C8>>2] = 0x0000F79F; /* 0x150253C8: DIP_B_GGM_LUT_G[98] */
            tuningBuf[0x000033CC>>2] = 0x00001FF1; /* 0x150253CC: DIP_B_GGM_LUT_G[99] */
            tuningBuf[0x000033D0>>2] = 0x00000A7B; /* 0x150253D0: DIP_B_GGM_LUT_G[100] */
            tuningBuf[0x000033D4>>2] = 0x0000BC4D; /* 0x150253D4: DIP_B_GGM_LUT_G[101] */
            tuningBuf[0x000033D8>>2] = 0x0000F204; /* 0x150253D8: DIP_B_GGM_LUT_G[102] */
            tuningBuf[0x000033DC>>2] = 0x00008334; /* 0x150253DC: DIP_B_GGM_LUT_G[103] */
            tuningBuf[0x000033E0>>2] = 0x0000A4A4; /* 0x150253E0: DIP_B_GGM_LUT_G[104] */
            tuningBuf[0x000033E4>>2] = 0x000001E0; /* 0x150253E4: DIP_B_GGM_LUT_G[105] */
            tuningBuf[0x000033E8>>2] = 0x00009C5D; /* 0x150253E8: DIP_B_GGM_LUT_G[106] */
            tuningBuf[0x000033EC>>2] = 0x00008D42; /* 0x150253EC: DIP_B_GGM_LUT_G[107] */
            tuningBuf[0x000033F0>>2] = 0x00006921; /* 0x150253F0: DIP_B_GGM_LUT_G[108] */
            tuningBuf[0x000033F4>>2] = 0x0000AD72; /* 0x150253F4: DIP_B_GGM_LUT_G[109] */
            tuningBuf[0x000033F8>>2] = 0x00006E43; /* 0x150253F8: DIP_B_GGM_LUT_G[110] */
            tuningBuf[0x000033FC>>2] = 0x0000D9C8; /* 0x150253FC: DIP_B_GGM_LUT_G[111] */
            tuningBuf[0x00003400>>2] = 0x00008FBE; /* 0x15025400: DIP_B_GGM_LUT_G[112] */
            tuningBuf[0x00003404>>2] = 0x00005E0B; /* 0x15025404: DIP_B_GGM_LUT_G[113] */
            tuningBuf[0x00003408>>2] = 0x0000CBB1; /* 0x15025408: DIP_B_GGM_LUT_G[114] */
            tuningBuf[0x0000340C>>2] = 0x0000C41C; /* 0x1502540C: DIP_B_GGM_LUT_G[115] */
            tuningBuf[0x00003410>>2] = 0x000080D3; /* 0x15025410: DIP_B_GGM_LUT_G[116] */
            tuningBuf[0x00003414>>2] = 0x0000F698; /* 0x15025414: DIP_B_GGM_LUT_G[117] */
            tuningBuf[0x00003418>>2] = 0x0000F16F; /* 0x15025418: DIP_B_GGM_LUT_G[118] */
            tuningBuf[0x0000341C>>2] = 0x00009D18; /* 0x1502541C: DIP_B_GGM_LUT_G[119] */
            tuningBuf[0x00003420>>2] = 0x00006923; /* 0x15025420: DIP_B_GGM_LUT_G[120] */
            tuningBuf[0x00003424>>2] = 0x000009FA; /* 0x15025424: DIP_B_GGM_LUT_G[121] */
            tuningBuf[0x00003428>>2] = 0x0000CBF8; /* 0x15025428: DIP_B_GGM_LUT_G[122] */
            tuningBuf[0x0000342C>>2] = 0x0000E856; /* 0x1502542C: DIP_B_GGM_LUT_G[123] */
            tuningBuf[0x00003430>>2] = 0x00005476; /* 0x15025430: DIP_B_GGM_LUT_G[124] */
            tuningBuf[0x00003434>>2] = 0x00002008; /* 0x15025434: DIP_B_GGM_LUT_G[125] */
            tuningBuf[0x00003438>>2] = 0x0000E70F; /* 0x15025438: DIP_B_GGM_LUT_G[126] */
            tuningBuf[0x0000343C>>2] = 0x0000DAFB; /* 0x1502543C: DIP_B_GGM_LUT_G[127] */
            tuningBuf[0x00003440>>2] = 0x00001F75; /* 0x15025440: DIP_B_GGM_LUT_G[128] */
            tuningBuf[0x00003444>>2] = 0x0000D91F; /* 0x15025444: DIP_B_GGM_LUT_G[129] */
            tuningBuf[0x00003448>>2] = 0x00004430; /* 0x15025448: DIP_B_GGM_LUT_G[130] */
            tuningBuf[0x0000344C>>2] = 0x0000375E; /* 0x1502544C: DIP_B_GGM_LUT_G[131] */
            tuningBuf[0x00003450>>2] = 0x000027CB; /* 0x15025450: DIP_B_GGM_LUT_G[132] */
            tuningBuf[0x00003454>>2] = 0x0000D6E6; /* 0x15025454: DIP_B_GGM_LUT_G[133] */
            tuningBuf[0x00003458>>2] = 0x0000BD2D; /* 0x15025458: DIP_B_GGM_LUT_G[134] */
            tuningBuf[0x0000345C>>2] = 0x00004148; /* 0x1502545C: DIP_B_GGM_LUT_G[135] */
            tuningBuf[0x00003460>>2] = 0x0000E03E; /* 0x15025460: DIP_B_GGM_LUT_G[136] */
            tuningBuf[0x00003464>>2] = 0x0000B386; /* 0x15025464: DIP_B_GGM_LUT_G[137] */
            tuningBuf[0x00003468>>2] = 0x00005405; /* 0x15025468: DIP_B_GGM_LUT_G[138] */
            tuningBuf[0x0000346C>>2] = 0x00007CF4; /* 0x1502546C: DIP_B_GGM_LUT_G[139] */
            tuningBuf[0x00003470>>2] = 0x0000810A; /* 0x15025470: DIP_B_GGM_LUT_G[140] */
            tuningBuf[0x00003474>>2] = 0x0000AF77; /* 0x15025474: DIP_B_GGM_LUT_G[141] */
            tuningBuf[0x00003478>>2] = 0x000032BE; /* 0x15025478: DIP_B_GGM_LUT_G[142] */
            tuningBuf[0x0000347C>>2] = 0x000019F6; /* 0x1502547C: DIP_B_GGM_LUT_G[143] */
            tuningBuf[0x00003480>>2] = 0x00000001; /* 0x15025480: DIP_B_GGM_CTRL */
			#endif
            break;
        case tuning_tag_UDM:
            pIspReg->DIP_X_CTL_RGB_EN.Bits.UDM_EN = 1;//enable bit
            pIspReg->DIP_X_UDM_INTP_CRS.Raw = 0x38303060;
            pIspReg->DIP_X_UDM_INTP_NAT.Raw = 0x1430063F;
            pIspReg->DIP_X_UDM_INTP_AUG.Raw = 0x00600600;
            pIspReg->DIP_X_UDM_LUMA_LUT1.Raw = 0x07FF0100;
            pIspReg->DIP_X_UDM_LUMA_LUT2.Raw = 0x02008020;
            pIspReg->DIP_X_UDM_SL_CTL.Raw = 0x003FFFE0;
            pIspReg->DIP_X_UDM_HFTD_CTL.Raw = 0x08421000;
            pIspReg->DIP_X_UDM_NR_STR.Raw = 0x81028000;
            pIspReg->DIP_X_UDM_NR_ACT.Raw = 0x00000050;
            pIspReg->DIP_X_UDM_HF_STR.Raw = 0x00000000;
            pIspReg->DIP_X_UDM_HF_ACT1.Raw = 0x145034DC;
            pIspReg->DIP_X_UDM_HF_ACT2.Raw = 0x0034FF55;
            pIspReg->DIP_X_UDM_CLIP.Raw = 0x00DF2064;
            pIspReg->DIP_X_UDM_DSB.Raw = 0x007FA800|fgModeRegBit;
            pIspReg->DIP_X_UDM_TILE_EDGE.Raw = 0x0000000F;
            pIspReg->DIP_X_UDM_DSL.Raw = 0x00000000;
            //pIspReg->DIP_X_UDM_SPARE_1.Raw = 0x00000000;
            pIspReg->DIP_X_UDM_SPARE_2.Raw = 0x00000000;
            pIspReg->DIP_X_UDM_SPARE_3.Raw = 0x00000000;
            break;
        default:
            break;
    }
}




#include "pic/imgi_1280x720_bayer10.h"


int testRawInRawOut(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);
    IspDrvDipPhy* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (IspDrvDipPhy*)IspDrvDipPhy::createInstance(DIP_A);
    g_pDrvDipPhy->init("testRawInRawOut");


    int ret=0;
    printf("--- [testRawInRawOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testRawInRawOut");
    printf("--- [testRawInRawOut(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRawInRawOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRawInRawOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRawInRawOut(%d)...push src done]\n", type);


   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRawInRawOut(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=buf_imgi.size;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {(_mfbo_w_*10/8),0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRawInRawOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRawInRawOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRawInRawOut(%d)...push dst done\n]", type);


    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRawInRawOut(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_imgi_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testRawInRawOut_process_type%d_package_mfbo_%dx%d.raw", type, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out1.virtAddr), buf_out1.size);


        //sprintf(filename, "/data/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testRawInRawOut...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");

        usleep(5000000);
    }while (1);



    //free
    srcBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    outBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testRawInRawOut(%d)...free memory done\n]", type);

    //
    pStream->uninit("testRawInRawOut");
    pStream->destroyInstance();
    printf("--- [testRawInRawOut(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("testRawInRawOut");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}

#include "../../imageio/test/DIP_pics/P2A_FG/imgi_4208_2368_rgb48_test2.h"

/*********************************************************************************/
int testRGBInYuvOut(int type,int loopNum)
{
    int ret=0;
    printf("--- [testRGBInYuvOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A");
    printf("--- [testRGBInYuvOut(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    //MUINT32 _imgi_w_=640, _imgi_h_=480;
    MUINT32 _imgi_w_=4208, _imgi_h_=2368;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(imgi_4208_2368_rgb48_item2); //imgi_4208_2368_rgb48_test2.h
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(imgi_4208_2368_rgb48_item2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRGBInYuvOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_ * 6 , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_RGB48),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRGBInYuvOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRGBInYuvOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRGBInYuvOut(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRGBInYuvOut(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRGBInYuvOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRGBInYuvOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRGBInYuvOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRGBInYuvOut(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRGBInYuvOut(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRGBInYuvOut(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testRGBInYuvOut(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRGBInYuvOut(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[testRGBInYuvOut(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testRGBInYuvOut_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [testRGBInYuvOut(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testRGBInYuvOut");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [testRGBInYuvOut(%d)...free memory done\n]", type);

    //
    pStream->uninit("testRGBInYuvOut");
    pStream->destroyInstance();
    printf("--- [testRGBInYuvOut(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


int testRawInUnPakRawOut(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);
    int ret=0;
    IspDrvDipPhy* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (IspDrvDipPhy*)IspDrvDipPhy::createInstance(DIP_A);
    g_pDrvDipPhy->init("testRawInRawOut");


    printf("--- [testRawInRawOut(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testRawInRawOut");
    printf("--- [testRawInRawOut(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    
    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testRawInRawOut(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testRawInRawOut", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testRawInRawOut(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testRawInRawOut(%d)...push src done]\n", type);
    

   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testRawInRawOut(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;   
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=buf_imgi.size;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    //MUINT32 bufStridesInBytes_1[3] = {(_mfbo_w_*10/8),0,0};
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    //portBufInfo_1.
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),
                                            MSize(2048,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testRawInRawOut", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    
    outBuffer->setExtParam(MSize(_imgi_w_, _imgi_h_));
    
    
    printf("--- [testRawInRawOut(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;    
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testRawInRawOut", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testRawInRawOut",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2); 
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testRawInRawOut(%d)...push dst done\n]", type);

    
    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testRawInRawOut(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testRawInRawOut(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testRawInRawOut(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_imgi_%dx%d.raw", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 10/8);

        sprintf(filename, "/system/P2iopipe_testRawInRawOut_process_type%d_package_mfbo_%dx%d.raw", type, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out1.virtAddr), buf_out1.size);


        //sprintf(filename, "/system/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testRawInRawOut...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");
    
        usleep(5000000);
    }while (1);


    
    //free 
    srcBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    outBuffer->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testRawInRawOut");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testRawInRawOut(%d)...free memory done\n]", type);
    
    //
    pStream->uninit("testRawInRawOut");
    pStream->destroyInstance();
    printf("--- [testRawInRawOut(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("testRawInRawOut");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}




#include "pic/MFB/imgci.h"
#include "pic/MFB/B1_320x240_yuy2.h"
#include "pic/imgi_640x480_yuy2.h"



#define MFB_FULL_SIZE_W 640 //2592
#define MFB_FULL_SIZE_H 480 //1944

/*********************************************************************************/
int testMFB_Blending(int type,int loopNum)
{
    printf("type(%d), loopNum(%d)\n", type, loopNum);

    IspDrvDipPhy* g_pDrvDipPhy = NULL;
    g_pDrvDipPhy = (IspDrvDipPhy*)IspDrvDipPhy::createInstance(DIP_A);
    g_pDrvDipPhy->init("MFB_Blending");

    int ret=0;
    printf("--- [testMFB_Blending(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testMFB_Blending");
    printf("--- [testMFB_Blending(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_MFB_Bld;

    //input image
    //int _imgi_w_=2592, _imgi_h_=1944;
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    //buf_imgi.size=sizeof(g_imgi_array);
    buf_imgi.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgi);
    //memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array), buf_imgi.size);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_640x480_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testMFB_Blending", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testMFB_Blending(%d)...push src done]\n", type);


    //int _imgbi_w_=640, _imgbi_h_=480;
    MUINT32 _imgbi_w_=320, _imgbi_h_=240;
    IMEM_BUF_INFO buf_imgbi;
    //buf_imgbi.size=sizeof(g_imgbi_array);
    buf_imgbi.size=_imgbi_w_*_imgbi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgbi);

    //memcpy( (MUINT8*)(buf_imgbi.virtAddr), (MUINT8*)(g_imgbi_array), buf_imgbi.size);
    memcpy( (MUINT8*)(buf_imgbi.virtAddr), (MUINT8*)(B1_320x240_yuy2), buf_imgbi.size);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* imgbi_srcBuffer;
    MINT32 imgbi_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgbi_bufStridesInBytes[3] = {_imgbi_w_*2, 0, 0};
    PortBufInfo_v1 imgbi_portBufInfo = PortBufInfo_v1( buf_imgbi.memID,buf_imgbi.virtAddr,0,buf_imgbi.bufSecu, buf_imgbi.bufCohe);
    IImageBufferAllocator::ImgParam imgbi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgbi_w_, _imgbi_h_), imgbi_bufStridesInBytes, imgbi_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgbi_pHeap;
    imgbi_pHeap = ImageBufferHeap::create( "testMFB_Blending", imgbi_imgParam,imgbi_portBufInfo,true);
    imgbi_srcBuffer = imgbi_pHeap->createImageBuffer();
    imgbi_srcBuffer->incStrong(imgbi_srcBuffer);
    imgbi_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGBI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_2;
    src_2.mPortID=PORT_IMGBI;
    src_2.mBuffer=imgbi_srcBuffer;
    src_2.mPortID.group=0;
    frameParams.mvIn.push_back(src_2);
    printf("--- [testMFB_Blending(%d)...push src_2 done]\n", type);

    MUINT32 _imgci_w_=MFB_FULL_SIZE_W, _imgci_h_=MFB_FULL_SIZE_H;
    IMEM_BUF_INFO buf_imgci;
    buf_imgci.size=MFB_FULL_SIZE_W*MFB_FULL_SIZE_H;
    mpImemDrv->allocVirtBuf(&buf_imgci);
    //memset( (MUINT8*)(buf_imgci.virtAddr), 0x0, buf_imgci.size);
    //memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgci_array), buf_imgci.size);
    memcpy( (MUINT8*)(buf_imgci.virtAddr), (MUINT8*)(g_imgci_array), _imgci_w_*_imgci_h_);
    //imem buffer 2 image heap
    printf("--- [testMFB_Blending(%d)...flag -1 ]\n", type);
    IImageBuffer* imgci_srcBuffer;
    MINT32 imgci_bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 imgci_bufStridesInBytes[3] = {MFB_FULL_SIZE_W, 0, 0};
    PortBufInfo_v1 imgci_portBufInfo = PortBufInfo_v1( buf_imgci.memID,buf_imgci.virtAddr,0,buf_imgci.bufSecu, buf_imgci.bufCohe);
    IImageBufferAllocator::ImgParam imgci_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_imgci_w_, _imgci_h_), imgci_bufStridesInBytes, imgci_bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> imgci_pHeap;
    imgci_pHeap = ImageBufferHeap::create( "testMFB_Blending", imgci_imgParam,imgci_portBufInfo,true);
    imgci_srcBuffer = imgci_pHeap->createImageBuffer();
    imgci_srcBuffer->incStrong(imgci_srcBuffer);
    imgci_srcBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- PORT_IMGCI : [testMFB_Blending(%d)...flag -8]\n", type);

    Input src_3;
    src_3.mPortID=PORT_IMGCI;
    src_3.mBuffer=imgci_srcBuffer;
    src_3.mPortID.group=0;
    frameParams.mvIn.push_back(src_3);

    printf("--- [testMFB_Blending(%d)...push src_3 done]\n", type);


    dip_x_reg_t tuningDat;
#if 0
    MF_TAG_IN_MFB_CAM_MFB_LL_CON2 : 10000406
    MF_TAG_IN_MFB_CAM_MFB_LL_CON3 : 00040C11
    MF_TAG_IN_MFB_CAM_MFB_LL_CON4 : 10100130
    MF_TAG_IN_MFB_CAM_MFB_LL_CON5 : 0002F06E
    MF_TAG_IN_MFB_CAM_MFB_LL_CON6 : 0000FF00
#endif

    tuningDat.DIP_X_MFB_CON.Raw = 0x00004031;
    tuningDat.DIP_X_MFB_LL_CON1.Raw = 0x00000201;
    tuningDat.DIP_X_MFB_LL_CON2.Raw = 0x140A0606;
    //tuningDat.DIP_X_MFB_LL_CON3.Raw =   0x07980A20;
    tuningDat.DIP_X_MFB_LL_CON3.Raw =   (_imgi_h_<<16)+ _imgi_w_;
    tuningDat.DIP_X_MFB_LL_CON4.Raw = 0x00000000;
    tuningDat.DIP_X_MFB_EDGE.Raw = 0x0000000F;

    tuningDat.DIP_X_CTL_YUV_EN.Bits.G2C_EN = 1;//enable bit
    tuningDat.DIP_X_G2C_CONV_0A.Raw = 0x0200;
    tuningDat.DIP_X_G2C_CONV_0B.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_1A.Raw = 0x02000000;
    tuningDat.DIP_X_G2C_CONV_1B.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_2A.Raw = 0x0;
    tuningDat.DIP_X_G2C_CONV_2B.Raw = 0x0200;



    frameParams.mTuningData = (MVOID*)&tuningDat;
    //android::Vector<MVOID*> mvTuningData;        //v1&v3 usage:  for p2 tuning data
    //m_camPass2Param.pTuningIspReg = (dip_x_reg_t *)pPipePackageInfo->pTuningQue;// check tuning enable bit on isp_function_dip


   //crop information
    MCrpRsInfo crop;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testMFB_Blending(%d)...push crop information done\n]", type);

    //output dma

    IMEM_BUF_INFO buf_out1;
    MUINT32 _mfbo_w_= _imgi_w_;
    MUINT32 _mfbo_h_= _imgi_h_;
    buf_out1.size=_mfbo_w_*_mfbo_h_;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_w_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testMFB_Blending", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testMFB_Blending(%d)...mfbo done\n]", type);
#if 1
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);
#endif


    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testMFB_Blending", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testMFB_Blending",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testMFB_Blending(%d)...push dst done\n]", type);

#if 0

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_MODE,0x1);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_BRZ_EN,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_DB_EN,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_CON ,BLD_LL_TH_E,0xa5);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_MODE,0x2);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_WT_MODE1,0x1);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_FLT_WT_MODE2,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_CLIP_TH1,0x40);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON1 ,BLD_LL_CLIP_TH2,0x1e);


    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_MAX_WT,6);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_DT1,0x1b);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_TH1,0xf);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_X_MFB_LL_CON2 ,BLD_LL_TH2,0x35);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_XSIZE,0xa20);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_XOFST,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON3 ,BLD_LL_OUT_YSIZE,0x798);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON4 ,BLD_LL_DB_XDIST,0x0);
    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_LL_CON4 ,BLD_LL_DB_YDIST,0x0);

    DIP_WRITE_PHY_BITS(DIP_A,g_pDrvDipPhy, DIP_A_MFB_EDGE ,BLD_TILE_EDGE,0xf);
#endif
//   RAL: DIP_A_MFB_EDGE.BLD_TILE_EDGE = 0xf                 # bits  4, 0x15022824,  0

    //for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testMFB_Blending(%d_)...flush done\n]", type);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..enque fail\n]", type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..enque done\n]",type);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testMFB_Blending(%d_)..deque fail\n]",type);
        }
        else
        {
            printf("---[testMFB_Blending(%d_)..deque done\n]", type);
        }



        //dump image
#if 1
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_img2o_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_out2.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgi_%dx%d.yuv", type, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgi.virtAddr), _imgi_w_ *_imgi_h_ * 2);

        sprintf(filename, "/data/P2iopipe_testMFB_Blending_process_type%d_package_imgbi_%dx%d.yuv", type, _imgbi_w_,_imgbi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(buf_imgbi.virtAddr), _imgbi_w_ *_imgbi_h_ * 2);


        //sprintf(filename, "/data/P2iopipe_testMFB_Blending_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        //saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
#endif
        //printf("--- [testMFB_Blending(%d_%d)...save file done\n]", type,i);
        printf("--- [testMFB_Blending...save file done\n]");

    }

    do
    {
        printf("---sleep ing !!\n]");

        usleep(5000000);
    }while (1);



    //free
    srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgi);

    imgbi_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgbi);

    imgci_srcBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_imgci);


    outBuffer->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out1);

    outBuffer_2->unlockBuf("testMFB_Blending");
    mpImemDrv->freeVirtBuf(&buf_out2);

    printf("--- [testMFB_Blending(%d)...free memory done\n]", type);

    //
    pStream->uninit("testMFB_Blending");
    pStream->destroyInstance();
    printf("--- [testMFB_Blending(%d)...pStream uninit done\n]", type);

    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    // release IspDrvDipPhy obj
    g_pDrvDipPhy->uninit("MFB_Blending");
    g_pDrvDipPhy->destroyInstance();


    return ret;
}



#include "../../imageio/test/DIP_pics/P2A_FG/imgi_640_480_10.h"


/*********************************************************************************/
int basicP2A(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A");
    printf("--- [basicP2A(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicP2A(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicP2A(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A");
    pStream->destroyInstance();
    printf("--- [basicP2A(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
int basicP2A_mdpsubpixel(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_mdpsubpixel(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_mdpsubpixel");
    printf("--- [basicP2A_mdpsubpixel(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_mdpsubpixel(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_mdpsubpixel", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_mdpsubpixel",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_mdpsubpixel(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_mdpsubpixel(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.w_fractional=111111111; //54253
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mCropRect.h_fractional=222222222; //108506
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.w_fractional=333333333; //162760
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mCropRect.h_fractional=444444444; //217013
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_mdpsubpixel(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_mdpsubpixel", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_mdpsubpixel",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_mdpsubpixel", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_mdpsubpixel",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicP2A_mdpsubpixel(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_mdpsubpixel(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_mdpsubpixel(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_mdpsubpixel(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_mdpsubpixel(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_mdpsubpixel(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_mdpsubpixel(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_mdpsubpixel_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_mdpsubpixel_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
	    printf("--- [basicP2A_mdpsubpixel(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_mdpsubpixel");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_mdpsubpixel");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_mdpsubpixel");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_mdpsubpixel(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_mdpsubpixel");
    pStream->destroyInstance();
    printf("--- [basicP2A_mdpsubpixel(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "../../imageio/test/DIP_pics/imgi_256x256_yuy2.h"

/*********************************************************************************/
int basicP2A_smallOutput(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A");
    printf("--- [basicP2A(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=256, _imgi_h_=256;
    IMEM_BUF_INFO buf_imgi;
    //buf_imgi.size=sizeof(g_yuv422_1plane_y_256_256_s512);
	buf_imgi.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_yuv422_1plane_y_256_256_s512), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicP2A(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicP2A(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A");
	printf("--- [basicP2A(%d)...uninit done\n]", type);
    pStream->destroyInstance();
    printf("--- [basicP2A(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}




/*********************************************************************************/
int basicP2A_smallOutput_64x64(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A");
    printf("--- [basicP2A(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=64, _imgi_h_=48;
    IMEM_BUF_INFO buf_imgi;
    //buf_imgi.size=sizeof(g_yuv422_1plane_y_256_256_s512);
	buf_imgi.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_yuv422_1plane_y_256_256_s512), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {_imgi_w_*2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    printf("--- [basicP2A(%d)...push dst done\n]", type);

    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			//char filename3[256];
            //sprintf(filename3, "/data/P2iopipe_basicP2A_process0x%x_type%d_package%d_imgi_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            //saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvIn[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicP2A(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A");
	printf("--- [basicP2A(%d)...uninit done\n]", type);
    pStream->destroyInstance();
    printf("--- [basicP2A(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}



#include "pic/imgi_720x540_b10.h"

/*********************************************************************************/
int basicMultiFrame( int type,int loopNum)
{
    int ret=0;
    printf("--- [basicMultiFrame(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicMultiFrame");
    printf("--- [basicMultiFrame(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicMultiFrame(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicMultiFrame(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicMultiFrame_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicMultiFrame_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicMultiFrame(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [basicMultiFrame(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [basicMultiFrame(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "basicMultiFrame_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("basicMultiFrame_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [basicMultiFrame(%d)_frame2...push dst done\n]", type);



    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicMultiFrame(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicMultiFrame(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_2(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_2(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    printf("--- [basicMultiFrame(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicMultiFrame");
    pStream->destroyInstance();
    printf("--- [basicMultiFrame(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/


static sem_t     mSem_ThreadEnd;
static pthread_t           mThread;
static int           mThread_loopNum=0;
static int           mThread_type=0;

//
#define PR_SET_NAME 15
static MVOID* onThreadLoop(MVOID *arg)
{
    arg;
    int ret=0;
    //[1] set thread
    // set thread name
    ::prctl(PR_SET_NAME,"onThreadLoop",0,0,0);
    // set policy/priority
    int const policy    = SCHED_OTHER;
    int const priority    = NICE_CAMERA_PASS2;
    //
    struct sched_param sched_p;
    ::sched_getparam(0, &sched_p);
    if(policy == SCHED_OTHER)
    {    //    Note: "priority" is nice-value priority.
        sched_p.sched_priority = 0;
        ::sched_setscheduler(0, policy, &sched_p);
        ::setpriority(PRIO_PROCESS, 0, priority);
    }
    else
    {    //    Note: "priority" is real-time priority.
        sched_p.sched_priority = priority;
        ::sched_setscheduler(0, policy, &sched_p);
    }
    //
    printf(
        "policy:(expect, result)=(%d, %d), priority:(expect, result)=(%d, %d)\n"
        , policy, ::sched_getscheduler(0)
        , priority, sched_p.sched_priority
    );
    //    detach thread => cannot be join, it means that thread would release resource after exit
    ::pthread_detach(::pthread_self());

    //
    printf("--- [onThreadLoop(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", mThread_type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("onThreadLoop");
    printf("--- [onThreadLoop(%d)...pStream init done]\n", mThread_type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [onThreadLoop(%d)...flag -1 ]\n", mThread_type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "onThreadLoop", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [onThreadLoop(%d)...flag -8]\n", mThread_type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [onThreadLoop(%d)...push src done]\n", mThread_type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [onThreadLoop(%d)...push crop information done\n]", mThread_type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "onThreadLoop", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(mThread_type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "onThreadLoop", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("onThreadLoop",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(mThread_type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [onThreadLoop(%d)...push dst done\n]", mThread_type);


    for(int i=0;i<mThread_loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [onThreadLoop(%d_%d)...flush done\n]", mThread_type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [onThreadLoop(%d_%d)..enque fail\n]", mThread_type, i);
        }
        else
        {
            printf("---[onThreadLoop(%d_%d)..enque done\n]",mThread_type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [onThreadLoop(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [onThreadLoop(%d_%d)..deque fail\n]",mThread_type, i);
        }
        else
        {
            printf("---[onThreadLoop(%d_%d)..deque done\n]", mThread_type, i);
        }


        //dump image
        if(mThread_type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_onThreadLoop_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), mThread_type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [onThreadLoop(%d_%d)...save file done\n]", mThread_type,i);
    }

    //free
    srcBuffer->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("onThreadLoop");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [onThreadLoop(%d)...free memory done\n]", mThread_type);

    //
    pStream->uninit("onThreadLoop");
    pStream->destroyInstance();
    printf("--- [onThreadLoop(%d)...pStream uninit done\n]", mThread_type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();


    ::sem_post(&mSem_ThreadEnd);
    return NULL;
}

//
int multiThread(int type,int loopNum)
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiThread(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiThread");
    printf("--- [multiThread(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    //create another thread
/*
    typedef long pthread_t;
    typedef struct {
        uint32_t flags;
        void* stack_base;
        size_t stack_size;
        size_t guard_size;
        int32_t sched_policy;
        int32_t sched_priority;
      #ifdef __LP64__
        char __reserved[16];
      #endif
    } pthread_attr_t;
*/
    mThread_loopNum=loopNum;
    mThread_type=type;
#ifdef __LP64__
    pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
    pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
    pthread_create(&mThread, &attr, onThreadLoop, NULL);


    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiThread(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiThread_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThread(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiThread(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiThread(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiThread_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiThread_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiThread_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [multiThread(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [multiThread(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThread(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [multiThread(%d)_frame1...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [multiThread(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "multiThread_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("multiThread_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [multiThread(%d)_frame2...push dst done\n]", type);



    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(enqueParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiThread(%d_%d)...flush done\n]", type,i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThread(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[multiThread(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiThread(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThread(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[multiThread(%d_%d)..deque done\n]", type,i);
        }

        if(type==0)
        {
            //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiThread_1(%d)...save file done\n]", type);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
        }
        else
        {
            //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiThread_1(%d)...save file done\n]", type);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (),type, i,_imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_multiThread_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);

        }
        printf("--- [multiThread_2(%d)...save file done\n]", type);
    }

    //free
    srcBuffer->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("multiThread");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    printf("--- [multiThread(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiThread");
    pStream->destroyInstance();
    printf("--- [multiThread(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    printf("--- [multiThread(%d)...wait mSem_ThreadEnd\n]", type);
    ::sem_wait(&mSem_ThreadEnd);
    return ret;
}


/*********************************************************************************/

#include "pic/imgi_3264x1836_bayer10.h"

int basicVss(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicVss(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicVss");
    printf("--- [basicVss(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=3264, _imgi_h_=1836;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_3264x1836_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_3264x1836_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicVss(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicVss", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicVss(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicVss(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicVss(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicVss", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicVss", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicVss(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicVss(%d_%d)...flush done\n]", type, i);


        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicVss(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicVss(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicVss(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicVss(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicVss(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicVss_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicVss_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [basicVss(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("basicVss");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [basicVss(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicVss");
    pStream->destroyInstance();
    printf("--- [basicVss(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();
    return ret;
}

/*********************************************************************************/
//
static MVOID* vssCaptureThread(MVOID *arg) //capture thread
{
    arg;
    int ret=0;
    //[1] set thread
    // set thread name
    ::prctl(PR_SET_NAME,"vssCaptureThread",0,0,0);
    // set policy/priority
    int const policy    = SCHED_OTHER;
    int const priority    = NICE_CAMERA_PASS2;
    //
    struct sched_param sched_p;
    ::sched_getparam(0, &sched_p);
    if(policy == SCHED_OTHER)
    {    //    Note: "priority" is nice-value priority.
        sched_p.sched_priority = 0;
        ::sched_setscheduler(0, policy, &sched_p);
        ::setpriority(PRIO_PROCESS, 0, priority);
    }
    else
    {    //    Note: "priority" is real-time priority.
        sched_p.sched_priority = priority;
        ::sched_setscheduler(0, policy, &sched_p);
    }
    //
    printf(
        "policy:(expect, result)=(%d, %d), priority:(expect, result)=(%d, %d)\n"
        , policy, ::sched_getscheduler(0)
        , priority, sched_p.sched_priority
    );
    //    detach thread => cannot be join, it means that thread would release resource after exit
    ::pthread_detach(::pthread_self());

    //
    printf("--- [vssCaptureThread(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", mThread_type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("vssCaptureThread");
    printf("--- [vssCaptureThread(%d)...pStream init done]\n", mThread_type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=3264, _imgi_h_=1836;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_3264x1836_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_3264x1836_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [vssCaptureThread(%d)...flag -1 ]\n", mThread_type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "vssCaptureThread", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [vssCaptureThread(%d)...flag -8]\n", mThread_type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [vssCaptureThread(%d)...push src done]\n", mThread_type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [vssCaptureThread(%d)...push crop information done\n]", mThread_type);

    //output dma
    IMEM_BUF_INFO buf_wmdao;
    buf_wmdao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wmdao);
    memset((MUINT8*)buf_wmdao.virtAddr, 0xffffffff, buf_wmdao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wmdao.memID,buf_wmdao.virtAddr,0,buf_wmdao.bufSecu, buf_wmdao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "vssCaptureThread", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "vssCaptureThread", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("vssCaptureThread",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [vssCaptureThread(%d)...push dst done\n]", mThread_type);


    for(int i=0;i<mThread_loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wmdao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [vssCaptureThread(%d_%d)...flush done\n]", mThread_type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [vssCaptureThread(%d_%d)..enque fail\n]", mThread_type, i);
        }
        else
        {
            printf("---[vssCaptureThread(%d_%d)..enque done\n]",mThread_type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [vssCaptureThread(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [vssCaptureThread(%d_%d)..deque fail\n]",mThread_type, i);
        }
        else
        {
            printf("---[vssCaptureThread(%d_%d)..deque done\n]", mThread_type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_vssCaptureThread_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),mThread_type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_vssCaptureThread_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),mThread_type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [vssCaptureThread(%d_%d)...save file done\n]", mThread_type,i);
    }

    //free
    srcBuffer->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_wmdao);
    outBuffer_2->unlockBuf("vssCaptureThread");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [vssCaptureThread(%d)...free memory done\n]", mThread_type);

    //
    pStream->uninit("vssCaptureThread");
    pStream->destroyInstance();
    printf("--- [vssCaptureThread(%d)...pStream uninit done\n]", mThread_type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();


    ::sem_post(&mSem_ThreadEnd);
    return NULL;
}

//
int multiThreadVss(int type,int loopNum) //prv thread
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiThreadVss(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiThreadVss");
    printf("--- [multiThreadVss(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiThreadVss(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiThreadVss", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiThreadVss(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiThreadVss(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiThreadVss(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiThreadVss", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiThreadVss", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiThreadVss",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [multiThreadVss(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiThreadVss(%d_%d)...flush done\n]", type, i);


        //
        if(i==0)
        {
            //create vss capture thread
            mThread_loopNum=loopNum/3;
            mThread_type=type;
#ifdef __LP64__
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
            pthread_create(&mThread, &attr, vssCaptureThread, NULL);
        }

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThreadVss(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[multiThreadVss(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiThreadVss(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiThreadVss(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[multiThreadVss(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_multiThreadVss_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_multiThreadVss_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [multiThreadVss(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("multiThreadVss");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [multiThreadVss(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiThreadVss");
    pStream->destroyInstance();
    printf("--- [multiThreadVss(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    printf("--- [multiThreadVss(%d)...wait mSem_ThreadEnd\n]", type);
    ::sem_wait(&mSem_ThreadEnd);

    return ret;
}


/*********************************************************************************/
//
int basicP2A_another(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_another(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_another");
    printf("--- [basicP2A_another(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1280, _imgi_h_=720;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_another(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_another", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_another(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_another(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_another(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_another", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_another", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_another",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicP2A_another(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_another(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_another(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_another(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_another(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_another(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_another(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_another_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        }
        printf("--- [basicP2A_another(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_another");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_another(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_another");
    pStream->destroyInstance();
    printf("--- [basicP2A_another(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicMultiFrame_another( int type,int loopNum)
{
    int ret=0;
    printf("--- [basicMultiFrame_another(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicMultiFrame_another");
    printf("--- [basicMultiFrame_another(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=640, _imgi_h_=480;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicMultiFrame_another(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
   crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicMultiFrame_another(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicMultiFrame_another_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicMultiFrame_another_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicMultiFrame_another(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=1280, _imgi_h_frame2_=720;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "basicMultiFrame_another_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("basicMultiFrame_another_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [basicMultiFrame_another(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=720, _imgi_h_frame3_=540;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [basicMultiFrame_another(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicMultiFrame_another(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "basicMultiFrame_another_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("basicMultiFrame_another_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [basicMultiFrame_another(%d)_frame3...push dst done\n]", type);

    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicMultiFrame_another(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame_another(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame_another(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicMultiFrame_another(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicMultiFrame_another(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[basicMultiFrame_another(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_another_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_another_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [basicMultiFrame_another_3(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [basicMultiFrame_another_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [basicMultiFrame_another_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_basicMultiFrame_another_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [basicMultiFrame_another_3(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("basicMultiFrame_another");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
    printf("--- [basicMultiFrame_another(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicMultiFrame_another");
    pStream->destroyInstance();
    printf("--- [basicMultiFrame_another(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


//
int multiProcess(int testNum, int type,int loopNum) //multi-process entry
{
    int ret=0;
    printf("--- [multiProcess(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);

    //create process
    pid_t child_pid;
    child_pid = fork ();

    switch(testNum)
    {
        case 5:
            if (child_pid != 0)
            {
                printf("[MultiProcess(%d_%d)...]this is the parent process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                printf("[MultiProcess(%d_%d)...]the child process ID is 0x%x", type, testNum,(MUINT32) child_pid);
                ret=basicP2A(type, loopNum);
            }
            else
            {
                printf("[MultiProcess(%d_%d)...]this is the child process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                ret=basicP2A_another(type, loopNum);
            }
            break;
        case 6:
            if (child_pid != 0)
            {
                printf("[MultiProcess(%d_%d)...]this is the parent process, with id 0x%x",type, testNum,(MUINT32) getpid ());
                printf("[MultiProcess(%d_%d)...]the child process ID is 0x%x", type, testNum,(MUINT32) child_pid);
                ret=basicMultiFrame(type, loopNum);
            }
            else
            {
                printf("[MultiProcess(%d_%d)...]this is the child process, with id 0x%x",type, testNum, (MUINT32) getpid ());
                ret=basicMultiFrame_another(type, loopNum);
            }
            break;
        default:
            printf("--- [multiProcess(%d)...] this testNum_(%d) is not for multi-process\n", type, testNum);
            break;
    }

    return 0;
}


/*********************************************************************************/
//three same frames in a single enqueued package
int testSkipTpipe( int type,int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe");
    printf("--- [testSkipTpipe(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=720, _imgi_h_=540;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testSkipTpipe(%d)_frame1...push dst done\n]", type);

#if 1
    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [testSkipTpipe(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [testSkipTpipe(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testSkipTpipe_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("testSkipTpipe_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [testSkipTpipe(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=720, _imgi_h_frame3_=540;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [testSkipTpipe(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [testSkipTpipe(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "testSkipTpipe_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("testSkipTpipe_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [testSkipTpipe(%d)_frame3...push dst done\n]", type);
#endif
    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        #if 1
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);
        #endif

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [testSkipTpipe(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe_1(%d_%d)...save file done\n]", type,i);
            //
            #if 1
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/dasystemta/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe_3(%d_%d)...save file done\n]", type,i);
            #endif
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe_1(%d_%d)...save file done\n]", type,i);
            //
            #if 1
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe_3(%d_%d)...save file done\n]", type,i);
            #endif
        }
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2);
#if 1
    srcBuffer_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("testSkipTpipe");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
#endif
    printf("--- [testSkipTpipe(%d)...free memory done\n]", type);

    //
    pStream->uninit("testSkipTpipe");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
//three frames in a single enqueued package, the first and second frames are the same, and the third frame is different
int testSkipTpipe2( int type,int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe2(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe2");
    printf("--- [testSkipTpipe2(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;
    FrameParams frameParams2;
    FrameParams frameParams3;

    /////////////////////////////////////////////////////////////////
    //frame 1
    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_=720, _imgi_h_=540;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame1...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame1...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe2(%d)_frame1...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe2(%d)_frame1...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe2_frame1", imgParam_1,portBufInfo_1,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe2_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [testSkipTpipe2(%d)_frame1...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 2
    frameParams2.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame2_=720, _imgi_h_frame2_=540;
    IMEM_BUF_INFO buf_imgi_frame2;
    buf_imgi_frame2.size=sizeof(p2a_bayer_g_imgi_array_720_540_10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
    memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(p2a_bayer_g_imgi_array_720_540_10), buf_imgi_frame2.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame2...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame2;
    MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_frame2;
    pHeap_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_frame2,portBufInfo_frame2,true);
    srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
    srcBuffer_frame2->incStrong(srcBuffer_frame2);
    srcBuffer_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame2...flag -8]\n", type);
    Input src_frame2;
    src_frame2.mPortID=PORT_IMGI;
    src_frame2.mBuffer=srcBuffer_frame2;
    src_frame2.mPortID.group=1;
    frameParams2.mvIn.push_back(src_frame2);
    printf("--- [testSkipTpipe2(%d)_frame2...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame2;
    crop_frame2.mFrameGroup=1;
    crop_frame2.mGroupID=1;
    MCrpRsInfo crop2_frame2;
    crop2_frame2.mFrameGroup=1;
    crop2_frame2.mGroupID=2;
    MCrpRsInfo crop3_frame2;
    crop3_frame2.mFrameGroup=1;
    crop3_frame2.mGroupID=3;
    crop_frame2.mCropRect.p_fractional.x=0;
    crop_frame2.mCropRect.p_fractional.y=0;
    crop_frame2.mCropRect.p_integral.x=0;
    crop_frame2.mCropRect.p_integral.y=0;
    crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop2_frame2.mCropRect.p_fractional.x=0;
    crop2_frame2.mCropRect.p_fractional.y=0;
    crop2_frame2.mCropRect.p_integral.x=0;
    crop2_frame2.mCropRect.p_integral.y=0;
    crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
    crop3_frame2.mCropRect.p_fractional.x=0;
    crop3_frame2.mCropRect.p_fractional.y=0;
    crop3_frame2.mCropRect.p_integral.x=0;
    crop3_frame2.mCropRect.p_integral.y=0;
    crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
    crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
    crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
    crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
    frameParams2.mvCropRsInfo.push_back(crop_frame2);
    frameParams2.mvCropRsInfo.push_back(crop2_frame2);
    frameParams2.mvCropRsInfo.push_back(crop3_frame2);
    printf("--- [testSkipTpipe2(%d)_frame2...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame2;
    buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame2);
    memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
    IImageBuffer* outBuffer_frame2=NULL;
    MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
    outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
    outBuffer_frame2->incStrong(outBuffer_frame2);
    outBuffer_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame2;
    if(type==0)
    {
        dst_frame2.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame2.mPortID=PORT_WDMAO;
    }
    dst_frame2.mBuffer=outBuffer_frame2;
    dst_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_frame2);	

    IMEM_BUF_INFO buf_out2_frame2;
    buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame2);
    memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
    IImageBuffer* outBuffer_2_frame2=NULL;
    MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
    sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testSkipTpipe2_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
    outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
    outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
    outBuffer_2_frame2->lockBuf("testSkipTpipe2_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame2;
    if(type==0)
    {
        dst_2_frame2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame2.mPortID=PORT_WROTO;
    }
    dst_2_frame2.mBuffer=outBuffer_2_frame2;
    dst_2_frame2.mPortID.group=1;
    frameParams2.mvOut.push_back(dst_2_frame2);	
    enqueParams.mvFrameParams.push_back(frameParams2);
    printf("--- [testSkipTpipe2(%d)_frame2...push dst done\n]", type);

    /////////////////////////////////////////////////////////////////
    //frame 3
    frameParams3.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
    //input image
    MUINT32 _imgi_w_frame3_=1280, _imgi_h_frame3_=720;
    IMEM_BUF_INFO buf_imgi_frame3;
    buf_imgi_frame3.size=sizeof(g_imgi_array_1280x720_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi_frame3);
    memcpy( (MUINT8*)(buf_imgi_frame3.virtAddr), (MUINT8*)(g_imgi_array_1280x720_b10), buf_imgi_frame3.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe2(%d) frame3...flag -1 ]\n", type);
    IImageBuffer* srcBuffer_frame3;
    MINT32 bufBoundaryInBytes_frame3[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes_frame3[3] = {(_imgi_w_frame3_*10/8), 0, 0};
    PortBufInfo_v1 portBufInfo_frame3 = PortBufInfo_v1( buf_imgi_frame3.memID,buf_imgi_frame3.virtAddr,0,buf_imgi_frame3.bufSecu, buf_imgi_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame3_, _imgi_h_frame3_), bufStridesInBytes_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_frame3;
    pHeap_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_frame3,portBufInfo_frame3,true);
    srcBuffer_frame3 = pHeap_frame3->createImageBuffer();
    srcBuffer_frame3->incStrong(srcBuffer_frame3);
    srcBuffer_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe2(%d)_frame3...flag -8]\n", type);
    Input src_frame3;
    src_frame3.mPortID=PORT_IMGI;
    src_frame3.mBuffer=srcBuffer_frame3;
    src_frame3.mPortID.group=2;
    frameParams3.mvIn.push_back(src_frame3);
    printf("--- [testSkipTpipe2(%d)_frame3...push src done]\n", type);
    //crop information
    MCrpRsInfo crop_frame3;
    crop_frame3.mFrameGroup=2;
    crop_frame3.mGroupID=1;
    MCrpRsInfo crop2_frame3;
    crop2_frame3.mFrameGroup=2;
    crop2_frame3.mGroupID=2;
    MCrpRsInfo crop3_frame3;
    crop3_frame3.mFrameGroup=2;
    crop3_frame3.mGroupID=3;
    crop_frame3.mCropRect.p_fractional.x=0;
    crop_frame3.mCropRect.p_fractional.y=0;
    crop_frame3.mCropRect.p_integral.x=0;
    crop_frame3.mCropRect.p_integral.y=0;
    crop_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop2_frame3.mCropRect.p_fractional.x=0;
    crop2_frame3.mCropRect.p_fractional.y=0;
    crop2_frame3.mCropRect.p_integral.x=0;
    crop2_frame3.mCropRect.p_integral.y=0;
    crop2_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop2_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop2_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop2_frame3.mResizeDst.h=_imgi_h_frame3_;
    crop3_frame3.mCropRect.p_fractional.x=0;
    crop3_frame3.mCropRect.p_fractional.y=0;
    crop3_frame3.mCropRect.p_integral.x=0;
    crop3_frame3.mCropRect.p_integral.y=0;
    crop3_frame3.mCropRect.s.w=_imgi_w_frame3_;
    crop3_frame3.mCropRect.s.h=_imgi_h_frame3_;
    crop3_frame3.mResizeDst.w=_imgi_w_frame3_;
    crop3_frame3.mResizeDst.h=_imgi_h_frame3_;
    frameParams3.mvCropRsInfo.push_back(crop_frame3);
    frameParams3.mvCropRsInfo.push_back(crop2_frame3);
    frameParams3.mvCropRsInfo.push_back(crop3_frame3);
    printf("--- [testSkipTpipe2(%d)_frame3...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1_frame3;
    buf_out1_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_frame3);
    memset((MUINT8*)buf_out1_frame3.virtAddr, 0xffffffff, buf_out1_frame3.size);
    IImageBuffer* outBuffer_frame3=NULL;
    MUINT32 bufStridesInBytes_1_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_frame3 = PortBufInfo_v1( buf_out1_frame3.memID,buf_out1_frame3.virtAddr,0,buf_out1_frame3.bufSecu, buf_out1_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_1_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_1_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_1_frame3,portBufInfo_1_frame3,true);
    outBuffer_frame3 = pHeap_1_frame3->createImageBuffer();
    outBuffer_frame3->incStrong(outBuffer_frame3);
    outBuffer_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_frame3;
    if(type==0)
    {
        dst_frame3.mPortID=PORT_IMG2O;
    }
    else
    {
        dst_frame3.mPortID=PORT_WDMAO;
    }
    dst_frame3.mBuffer=outBuffer_frame3;
    dst_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_frame3);	

    IMEM_BUF_INFO buf_out2_frame3;
    buf_out2_frame3.size=_imgi_w_frame3_*_imgi_h_frame3_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_frame3);
    memset((MUINT8*)buf_out2_frame3.virtAddr, 0xffffffff, buf_out2_frame3.size);
    IImageBuffer* outBuffer_2_frame3=NULL;
    MUINT32 bufStridesInBytes_2_frame3[3] = {_imgi_w_frame3_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_frame3 = PortBufInfo_v1( buf_out2_frame3.memID,buf_out2_frame3.virtAddr,0,buf_out2_frame3.bufSecu, buf_out2_frame3.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_frame3 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame3_,_imgi_h_frame3_),  bufStridesInBytes_2_frame3, bufBoundaryInBytes_frame3, 1);
    sp<ImageBufferHeap> pHeap_2_frame3 = ImageBufferHeap::create( "testSkipTpipe2_frame3", imgParam_2_frame3,portBufInfo_2_frame3,true);
    outBuffer_2_frame3 = pHeap_2_frame3->createImageBuffer();
    outBuffer_2_frame3->incStrong(outBuffer_2_frame3);
    outBuffer_2_frame3->lockBuf("testSkipTpipe2_frame3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_frame3;
    if(type==0)
    {
        dst_2_frame3.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2_frame3.mPortID=PORT_WROTO;
    }
    dst_2_frame3.mBuffer=outBuffer_2_frame3;
    dst_2_frame3.mPortID.group=2;
    frameParams3.mvOut.push_back(dst_2_frame3);	
    enqueParams.mvFrameParams.push_back(frameParams3);
    printf("--- [testSkipTpipe2(%d)_frame3...push dst done\n]", type);

    //
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams2.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
        memset((MUINT8*)(frameParams2.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);
        memset((MUINT8*)(frameParams3.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame3.size);
        memset((MUINT8*)(frameParams3.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame3.size);

        /////////////////////////////////////////////////////////////////
        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe2(%d_%d)...flush done\n]", type,i);



        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe2(%d_%d)..enque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe2(%d_%d)..enque done\n]", type,i);
        }

        //temp use while to observe in CVD
        //printf("--- [testSkipTpipe2(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe2(%d_%d)..deque fail\n]", type,i);
        }
        else
        {
            printf("---[testSkipTpipe2(%d_%d)..deque done\n]", type,i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_img2o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe2_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe2_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe2_3(%d_%d)...save file done\n]", type,i);
        }
        else
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [testSkipTpipe2_1(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame2[256];
            sprintf(filename_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            char filename2_frame2[256];
            sprintf(filename2_frame2, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
            saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
            printf("--- [testSkipTpipe2_2(%d_%d)...save file done\n]", type,i);
            //
            char filename_frame3[256];
            sprintf(filename_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            char filename2_frame3[256];
            sprintf(filename2_frame3, "/data/P2iopipe_testSkipTpipe2_process0x%x_type%d_package%d_frame3_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame3_,_imgi_h_frame3_);
            saveBufToFile(filename2_frame3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[2].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame3_ *_imgi_h_frame3_ * 2);
            printf("--- [testSkipTpipe2_3(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    srcBuffer_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
    outBuffer_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1_frame2);
    outBuffer_2_frame2->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2_frame2);
    srcBuffer_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_imgi_frame3);
    outBuffer_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out1_frame3);
    outBuffer_2_frame3->unlockBuf("testSkipTpipe2");
    mpImemDrv->freeVirtBuf(&buf_out2_frame3);
    printf("--- [testSkipTpipe2(%d)...free memory done\n]", type);

    //
    pStream->uninit("testSkipTpipe2");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe2(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


#include "pic/imgi_1600x1200_bayer10.h"

//simulate isp always keep same setting, but mdp does not
int testSkipTpipe3(int type, int loopNum)
{
    int ret=0;
    printf("--- [testSkipTpipe3...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testSkipTpipe3");
    printf("--- [testSkipTpipe3...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testSkipTpipe3...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testSkipTpipe3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testSkipTpipe3...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testSkipTpipe3...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testSkipTpipe3...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testSkipTpipe3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    buf_out2.size=_output_2o_w_*_output_2o_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testSkipTpipe3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testSkipTpipe3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [testSkipTpipe3(%d)...push dst done\n]", type);

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    bool two_out=true;
    for(int i=0;i<loopNum;i++)
    {
        frameParams.mvOut.clear();

        if(i<3)
        {   //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==3)
        { //mdp 1 out
            frameParams.mvOut.push_back(dst);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            two_out=false;
        }
        else if(i==4)
        {
            //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==5)
        {
            //mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }
        else if (i==6)
        {//mdp 1 out
            frameParams.mvOut.push_back(dst);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            two_out=false;
        }
        else
        {//mdp 2 out
            frameParams.mvOut.push_back(dst);	
            frameParams.mvOut.push_back(dst_2);	
            memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
            two_out=true;
        }


        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testSkipTpipe3(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe3(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[testSkipTpipe3(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testSkipTpipe3(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[testSkipTpipe3(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_testSkipTpipe3_%d_process0x%x_package%d_wroto_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        if(two_out)
        {
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testSkipTpipe3_%d_process0x%x_package%d_wdmao_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        }
        printf("--- [testSkipTpipe3(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testSkipTpipe3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [testSkipTpipe3...free memory done\n]");

    //
    pStream->uninit("testSkipTpipe3");
    pStream->destroyInstance();
    printf("--- [testSkipTpipe3...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
//
#include "pic/imgi_1280x736_bayer10.h"

int testIspMdpVencDL( int type,int loopNum)
{
     int ret=0;
     printf("--- [testIspMdpVencDL(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
     NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
     pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
     pStream->init("testIspMdpVencDL");
     printf("--- [testIspMdpVencDL(%d)...pStream init done]\n", type);
     IMemDrv* mpImemDrv=NULL;
     mpImemDrv=IMemDrv::createInstance();
     mpImemDrv->init();

     QParams enqueParams;
     FrameParams frameParams;
     FrameParams frameParams2;

     /////////////////////////////////////////////////////////////////
     //frame 1
     //frame tag
     frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;
     //input image
     MUINT32 _imgi_w_=1280, _imgi_h_=736;
     IMEM_BUF_INFO buf_imgi;
     buf_imgi.size=sizeof(g_imgi_array_1280x736_b10);
     mpImemDrv->allocVirtBuf(&buf_imgi);
     memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1280x736_b10), buf_imgi.size);
     //imem buffer 2 image heap
     printf("--- [testIspMdpVencDL(%d) frame1...flag -1 ]\n", type);
     IImageBuffer* srcBuffer;
     MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
     MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
     PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
     IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
     sp<ImageBufferHeap> pHeap;
     pHeap = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam,portBufInfo,true);
     srcBuffer = pHeap->createImageBuffer();
     srcBuffer->incStrong(srcBuffer);
     srcBuffer->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     printf("--- [testIspMdpVencDL(%d)_frame1...flag -8]\n", type);
     Input src;
     src.mPortID=PORT_IMGI;
     src.mBuffer=srcBuffer;
     src.mPortID.group=0;
     frameParams.mvIn.push_back(src);
     printf("--- [testIspMdpVencDL(%d)_frame1...push src done]\n", type);

    //crop information
     MCrpRsInfo crop;
     crop.mFrameGroup=0;
     crop.mGroupID=1;
     MCrpRsInfo crop2;
     crop2.mFrameGroup=0;
     crop2.mGroupID=2;
     MCrpRsInfo crop3;
     crop3.mFrameGroup=0;
     crop3.mGroupID=3;
     crop.mCropRect.p_fractional.x=0;
     crop.mCropRect.p_fractional.y=0;
     crop.mCropRect.p_integral.x=0;
     crop.mCropRect.p_integral.y=0;
     crop.mCropRect.s.w=_imgi_w_;
     crop.mCropRect.s.h=_imgi_h_;
     crop.mResizeDst.w=_imgi_w_;
     crop.mResizeDst.h=_imgi_h_;
     crop2.mCropRect.p_fractional.x=0;
     crop2.mCropRect.p_fractional.y=0;
     crop2.mCropRect.p_integral.x=0;
     crop2.mCropRect.p_integral.y=0;
     crop2.mCropRect.s.w=_imgi_w_;
     crop2.mCropRect.s.h=_imgi_h_;
     crop2.mResizeDst.w=_imgi_w_;
     crop2.mResizeDst.h=_imgi_h_;
     crop3.mCropRect.p_fractional.x=0;
     crop3.mCropRect.p_fractional.y=0;
     crop3.mCropRect.p_integral.x=0;
     crop3.mCropRect.p_integral.y=0;
     crop3.mCropRect.s.w=_imgi_w_;
     crop3.mCropRect.s.h=_imgi_h_;
     crop3.mResizeDst.w=_imgi_w_;
     crop3.mResizeDst.h=_imgi_h_;
     frameParams.mvCropRsInfo.push_back(crop);
     frameParams.mvCropRsInfo.push_back(crop2);
     frameParams.mvCropRsInfo.push_back(crop3);
     printf("--- [testIspMdpVencDL(%d)_frame1...push crop information done\n]", type);

     //output dma
     IMEM_BUF_INFO buf_out1;
     buf_out1.size=_imgi_w_*_imgi_h_*2;
     mpImemDrv->allocVirtBuf(&buf_out1);
     memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
     IImageBuffer* outBuffer=NULL;
     MUINT32 bufStridesInBytes_1[3] = {_imgi_w_,_imgi_w_/2, _imgi_w_/2};
     PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),
                                             MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 3);
     sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam_1,portBufInfo_1,true);
     outBuffer = pHeap_1->createImageBuffer();
     outBuffer->incStrong(outBuffer);
     outBuffer->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst;
     #if 0 //only support direct link
     if(type==0)
     {
         dst.mPortID=PORT_IMG2O;
     }
     else
     {
     #endif
         dst.mPortID=PORT_VENC_STREAMO;
     //}
     dst.mBuffer=outBuffer;
     dst.mPortID.group=0;
     frameParams.mvOut.push_back(dst);   

     IMEM_BUF_INFO buf_out2;
     buf_out2.size=_imgi_w_*_imgi_h_*2;
     mpImemDrv->allocVirtBuf(&buf_out2);
     memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
     IImageBuffer* outBuffer_2=NULL;
     MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
     PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
     sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testIspMdpVencDL_frame1", imgParam_2,portBufInfo_2,true);
     outBuffer_2 = pHeap_2->createImageBuffer();
     outBuffer_2->incStrong(outBuffer_2);
     outBuffer_2->lockBuf("testIspMdpVencDL_frame1",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_2;
     if(type==0)
     {
         dst_2.mPortID=PORT_IMG3O;
     }
     else
     {
         dst_2.mPortID=PORT_WROTO;
     }
     dst_2.mBuffer=outBuffer_2;
     dst_2.mPortID.group=0;
     frameParams.mvOut.push_back(dst_2); 
     enqueParams.mvFrameParams.push_back(frameParams);
     printf("--- [testIspMdpVencDL(%d)_frame1...push dst done\n]", type);


     /////////////////////////////////////////////////////////////////
     //frame 2
//     enqueParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);
     //input image
     MUINT32 _imgi_w_frame2_=1280, _imgi_h_frame2_=736;
     IMEM_BUF_INFO buf_imgi_frame2;
     buf_imgi_frame2.size=sizeof(g_imgi_array_1280x736_b10);
     mpImemDrv->allocVirtBuf(&buf_imgi_frame2);
     memcpy( (MUINT8*)(buf_imgi_frame2.virtAddr), (MUINT8*)(g_imgi_array_1280x736_b10), buf_imgi_frame2.size);
     //imem buffer 2 image heap
     printf("--- [testIspMdpVencDL(%d) frame2...flag -1 ]\n", type);
     IImageBuffer* srcBuffer_frame2;
     MINT32 bufBoundaryInBytes_frame2[3] = {0, 0, 0};
     MUINT32 bufStridesInBytes_frame2[3] = {(_imgi_w_frame2_*10/8), 0, 0};
     PortBufInfo_v1 portBufInfo_frame2 = PortBufInfo_v1( buf_imgi_frame2.memID,buf_imgi_frame2.virtAddr,0,buf_imgi_frame2.bufSecu, buf_imgi_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_frame2_, _imgi_h_frame2_), bufStridesInBytes_frame2, bufBoundaryInBytes_frame2, 1);
     sp<ImageBufferHeap> pHeap_frame2;
     pHeap_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_frame2,portBufInfo_frame2,true);
     srcBuffer_frame2 = pHeap_frame2->createImageBuffer();
     srcBuffer_frame2->incStrong(srcBuffer_frame2);
     srcBuffer_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     printf("--- [testIspMdpVencDL(%d)_frame2...flag -8]\n", type);
     Input src_frame2;
     src_frame2.mPortID=PORT_IMGI;
     src_frame2.mBuffer=srcBuffer_frame2;
     src_frame2.mPortID.group=1;
//     frameParams2.mvIn.push_back(src_frame2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push src done]\n", type);
     //crop information
     MCrpRsInfo crop_frame2;
     crop_frame2.mFrameGroup=1;
     crop_frame2.mGroupID=1;
     MCrpRsInfo crop2_frame2;
     crop2_frame2.mFrameGroup=1;
     crop2_frame2.mGroupID=2;
     MCrpRsInfo crop3_frame2;
     crop3_frame2.mFrameGroup=1;
     crop3_frame2.mGroupID=3;
     crop_frame2.mCropRect.p_fractional.x=0;
     crop_frame2.mCropRect.p_fractional.y=0;
     crop_frame2.mCropRect.p_integral.x=0;
     crop_frame2.mCropRect.p_integral.y=0;
     crop_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop_frame2.mResizeDst.h=_imgi_h_frame2_;
     crop2_frame2.mCropRect.p_fractional.x=0;
     crop2_frame2.mCropRect.p_fractional.y=0;
     crop2_frame2.mCropRect.p_integral.x=0;
     crop2_frame2.mCropRect.p_integral.y=0;
     crop2_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop2_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop2_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop2_frame2.mResizeDst.h=_imgi_h_frame2_;
     crop3_frame2.mCropRect.p_fractional.x=0;
     crop3_frame2.mCropRect.p_fractional.y=0;
     crop3_frame2.mCropRect.p_integral.x=0;
     crop3_frame2.mCropRect.p_integral.y=0;
     crop3_frame2.mCropRect.s.w=_imgi_w_frame2_;
     crop3_frame2.mCropRect.s.h=_imgi_h_frame2_;
     crop3_frame2.mResizeDst.w=_imgi_w_frame2_;
     crop3_frame2.mResizeDst.h=_imgi_h_frame2_;
     frameParams2.mvCropRsInfo.push_back(crop_frame2);
     frameParams2.mvCropRsInfo.push_back(crop2_frame2);
     frameParams2.mvCropRsInfo.push_back(crop3_frame2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push crop information done\n]", type);

     //output dma
     IMEM_BUF_INFO buf_out1_frame2;
     buf_out1_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
     mpImemDrv->allocVirtBuf(&buf_out1_frame2);
     memset((MUINT8*)buf_out1_frame2.virtAddr, 0xffffffff, buf_out1_frame2.size);
     IImageBuffer* outBuffer_frame2=NULL;
     MUINT32 bufStridesInBytes_1_frame2[3] = {_imgi_w_frame2_, _imgi_w_frame2_/2, _imgi_w_frame2_/2};
     PortBufInfo_v1 portBufInfo_1_frame2 = PortBufInfo_v1( buf_out1_frame2.memID,buf_out1_frame2.virtAddr,0,buf_out1_frame2.bufSecu, buf_out1_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_1_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YV12),
                                             MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_1_frame2, bufBoundaryInBytes_frame2, 3);
     sp<ImageBufferHeap> pHeap_1_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_1_frame2,portBufInfo_1_frame2,true);
     outBuffer_frame2 = pHeap_1_frame2->createImageBuffer();
     outBuffer_frame2->incStrong(outBuffer_frame2);
     outBuffer_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_frame2;
     #if 0
     if(type==0)
     {
         dst_frame2.mPortID=PORT_IMG2O;
     }
     else
     {
     #endif
         dst_frame2.mPortID=PORT_VENC_STREAMO;
     //}
     dst_frame2.mBuffer=outBuffer_frame2;
     dst_frame2.mPortID.group=1;
//     frameParams2.mvOut.push_back(dst_frame2);    

     IMEM_BUF_INFO buf_out2_frame2;
     buf_out2_frame2.size=_imgi_w_frame2_*_imgi_h_frame2_*2;
     mpImemDrv->allocVirtBuf(&buf_out2_frame2);
     memset((MUINT8*)buf_out2_frame2.virtAddr, 0xffffffff, buf_out2_frame2.size);
     IImageBuffer* outBuffer_2_frame2=NULL;
     MUINT32 bufStridesInBytes_2_frame2[3] = {_imgi_w_frame2_*2,0,0};
     PortBufInfo_v1 portBufInfo_2_frame2 = PortBufInfo_v1( buf_out2_frame2.memID,buf_out2_frame2.virtAddr,0,buf_out2_frame2.bufSecu, buf_out2_frame2.bufCohe);
     IImageBufferAllocator::ImgParam imgParam_2_frame2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_frame2_,_imgi_h_frame2_),  bufStridesInBytes_2_frame2, bufBoundaryInBytes_frame2, 1);
     sp<ImageBufferHeap> pHeap_2_frame2 = ImageBufferHeap::create( "testIspMdpVencDL_frame2", imgParam_2_frame2,portBufInfo_2_frame2,true);
     outBuffer_2_frame2 = pHeap_2_frame2->createImageBuffer();
     outBuffer_2_frame2->incStrong(outBuffer_2_frame2);
     outBuffer_2_frame2->lockBuf("testIspMdpVencDL_frame2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
     Output dst_2_frame2;
     if(type==0)
     {
         dst_2_frame2.mPortID=PORT_IMG3O;
     }
     else
     {
         dst_2_frame2.mPortID=PORT_WROTO;
     }
     dst_2_frame2.mBuffer=outBuffer_2_frame2;
     dst_2_frame2.mPortID.group=1;
//     frameParams2.mvOut.push_back(dst_2_frame2);  
     enqueParams.mvFrameParams.push_back(frameParams2);
     printf("--- [testIspMdpVencDL(%d)_frame2...push dst done\n]", type);

    //
    ret=pStream->sendCommand(ESDCmd_CONFIG_VENC_DIRLK, 120, _imgi_w_,_imgi_h_);
    printf("--- [testIspMdpVencDL(%d)_ESDCmd_CONFIG_VENC_DIRLK done\n]", type);
    //
     for(int i=0;i<loopNum;i++)
     {
         memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0x00000000, buf_out1.size);
         memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0x00000000, buf_out2.size);
         //memset((MUINT8*)(enqueParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_frame2.size);
         //memset((MUINT8*)(enqueParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_frame2.size);

         /////////////////////////////////////////////////////////////////
         //buffer operation
         mpImemDrv->cacheFlushAll();
         printf("--- [testIspMdpVencDL(%d_%d)...flush done\n]", type,i);



         //enque
         ret=pStream->enque(enqueParams);
         if(!ret)
         {
             printf("---ERRRRRRRRR [testIspMdpVencDL(%d_%d)..enque fail\n]", type,i);
         }
         else
         {
             printf("---[testIspMdpVencDL(%d_%d)..enque done\n]", type,i);
         }

         //temp use while to observe in CVD
         //printf("--- [testIspMdpVencDL(%d)...enter while...........\n]", type);
        //while(1);


         //deque
         //wait a momet in fpga
         //usleep(5000000);
         QParams dequeParams;
         ret=pStream->deque(dequeParams);
         if(!ret)
         {
             printf("---ERRRRRRRRR [testIspMdpVencDL(%d_%d)..deque fail\n]", type,i);
         }
         else
         {
             printf("---[testIspMdpVencDL(%d_%d)..deque done\n]", type,i);
         }


         //dump image
         if(type==0)
         {
             char filename[256];
             sprintf(filename, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_venco_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             char filename2[256];
             sprintf(filename2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             printf("--- [testIspMdpVencDL_1(%d_%d)...save file done\n]", type,i);
             //
             #if 0
             char filename_frame2[256];
             sprintf(filename_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             char filename2_frame2[256];
             sprintf(filename2_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             #endif
             printf("--- [testIspMdpVencDL_2(%d_%d)...save file done\n]", type,i);
         }
         else
         {
             char filename[256];
             sprintf(filename, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_venco_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             char filename2[256];
             sprintf(filename2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame1_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
             saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
             printf("--- [testIspMdpVencDL_1(%d_%d)...save file done\n]", type,i);
             //
             #if 0
             char filename_frame2[256];
             sprintf(filename_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_wdmao_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             char filename2_frame2[256];
             sprintf(filename2_frame2, "/data/P2iopipe_testIspMdpVencDL_process0x%x_type%d_package%d_frame2_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_frame2_,_imgi_h_frame2_);
             saveBufToFile(filename2_frame2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[1].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_frame2_ *_imgi_h_frame2_ * 2);
             #endif
             printf("--- [testIspMdpVencDL_2(%d_%d)...save file done\n]", type,i);
         }
     }
     //
    ret=pStream->sendCommand(ESDCmd_RELEASE_VENC_DIRLK);
    printf("--- [testIspMdpVencDL(%d)ESDCmd_RELEASE_VENC_DIRLK done\n]", type);

     //free
     srcBuffer->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_imgi);
     outBuffer->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out1);
     outBuffer_2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out2);
     srcBuffer_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_imgi_frame2);
     outBuffer_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out1_frame2);
     outBuffer_2_frame2->unlockBuf("testIspMdpVencDL");
     mpImemDrv->freeVirtBuf(&buf_out2_frame2);
     printf("--- [testIspMdpVencDL(%d)...free memory done\n]", type);

     //
     pStream->uninit("testIspMdpVencDL");
     pStream->destroyInstance();
     printf("--- [testIspMdpVencDL(%d)...pStream uninit done\n]", type);
     mpImemDrv->uninit();
     mpImemDrv->destroyInstance();

     return ret;

}


/*********************************************************************************/

int testpath(int type,int loopNum)
{
    int ret=0;
    printf("--- [testpath(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("testpath");
    printf("--- [testpath(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [testpath(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "testpath", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [testpath(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [testpath(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
//    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
//    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [testpath(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "testpath", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "testpath", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("testpath",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
//    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [testpath(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
//        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [testpath(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testpath(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[testpath(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [testpath(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [testpath(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[testpath(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
//            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_testpath_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
//            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        }
        printf("--- [testpath(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("testpath");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [testpath(%d)...free memory done\n]", type);

    //
    pStream->uninit("testpath");
    pStream->destroyInstance();
    printf("--- [testpath(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

/*********************************************************************************/
#include "pic/imgi_1600x1200_yuy2.h"

int basicP2A_yuvIn(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn");
    printf("--- [basicP2A_yuvIn(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicP2A_yuvIn2(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn2(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn2");
    printf("--- [basicP2A_yuvIn2(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn2(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn2(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn2(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn2(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn2", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn2(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn2(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn2(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn2(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn2(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn2(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn2(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn2_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn2_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn2(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn2(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn2");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn2(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int basicP2A_yuvIn3(int loopNum)
{
    int ret=0;
    int type=0;
    printf("--- [basicP2A_yuvIn3(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_yuvIn3");
    printf("--- [basicP2A_yuvIn3(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_yuy2);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_yuy2), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_yuvIn3(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*2) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_yuvIn3(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_yuvIn3(%d)...push src done]\n", type);

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_yuvIn3(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_yuvIn3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_yuvIn3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
//    printf("--- [basicP2A_yuvIn3(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_yuvIn3(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn3(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn3(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A_yuvIn3(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_yuvIn3(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2A_yuvIn3(%d_%d)..deque done\n]", type, i);
        }


        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_yuvIn3_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_yuvIn3_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (), type,i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        printf("--- [basicP2A_yuvIn3(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2A_yuvIn3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2A_yuvIn3(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2A_yuvIn3");
    pStream->destroyInstance();
    printf("--- [basicP2A_yuvIn3(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


//
int basicP2AwithYUVinput(int type,int loopNum)
{
    int ret=0;
    //
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_yuvIn(loopNum); //wdmao + wroto
            break;
        case 1 :
            ret=basicP2A_yuvIn2(loopNum); //wdmao + img2o
            break;
        case 2 :
            ret=basicP2A_yuvIn3(loopNum); //wdmao + img3o
            break;
    }

    return ret;
}


/*********************************************************************************/


int P2ArawInwithFEO(int loopNum) //without srz1 enabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO");
    printf("--- [P2ArawInwithFEO...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image

    IMEM_BUF_INFO buf_imgi;
#ifndef EP_SMALL_PICTURE
	MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
#else
	MUINT32 _imgi_w_=640, _imgi_h_=480;
	buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
#endif
    mpImemDrv->allocVirtBuf(&buf_imgi);

#ifndef EP_SMALL_PICTURE
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
#else
	memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
#endif

    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO...push src done]\n");

    //
#ifndef EP_SMALL_PICTURE
    MUINT32 _output_w_=1600, _output_h_=1200;
#else
	MUINT32 _output_w_=640, _output_h_=480;
#endif
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    //printf("--- [P2ArawInwithFEO(%d)...push dst done\n]", type);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    MUINT32 _feo_w_ = _output_w_ /16 * 56;
    MUINT32 _feo_h_ = _output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    #if 0
    ModuleInfo fe_module;
    fe_module.moduleTag = EDipModule_FE;
    fe_module.frameGroup=0;
    _FE_REG_CFG_    *mpfeParam = new _FE_REG_CFG_;
    mpfeParam->fe_mode = 1;
    fe_module.moduleStruct   = reinterpret_cast<MVOID*> (mpfeParam);
    enqueParams.mvModuleData.push_back(fe_module);
    #else
    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    #endif

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO(%d)..deque fail\n]", i);
			do{
				printf("---sleep ing !!\n]");
				usleep(5000000);
			}while(1);
        }
        else
        {
            printf("---[P2ArawInwithFEO(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO2(int loopNum) //need revise, enable srz1 crop
{
    int ret=0;
    printf("--- [P2ArawInwithFEO2...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO2");
    printf("--- [P2ArawInwithFEO2...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO2...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO2...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO2...push src done]\n");

    //
    MUINT32 _output_w_=640, _output_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=400;
    crop.mCropRect.p_integral.y=400;
    crop.mCropRect.s.w=(_imgi_w_ - 400 - 200);
    crop.mCropRect.s.h=(_imgi_h_ - 400 - 200);
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO2...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO2(%d)...push dst done\n]", type);


    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO2", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO2",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO2(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO2(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO2(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO2(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO2(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO2_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ );
        printf("--- [P2ArawInwithFEO2(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO2");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    delete mpsrz1Param;
    //
    printf("--- [P2ArawInwithFEO2...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO2");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO2...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO3(int loopNum) //vsdof usage, with srz1 disabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO3...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO3");
    printf("--- [P2ArawInwithFEO3...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO3...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO3...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO3...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_img2o_;
    crop.mResizeDst.h=_output_h_img2o_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO3...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out1_wroto;
    buf_out1_wroto.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_wroto);
    memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
    IImageBuffer* outBuffer_wroto=NULL;
    MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_1_wroto,portBufInfo_1_wroto,true);
    outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
    outBuffer_wroto->incStrong(outBuffer_wroto);
    outBuffer_wroto->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_wroto;
    dst_wroto.mPortID=PORT_WROTO;
    dst_wroto.mBuffer=outBuffer_wroto;
    dst_wroto.mPortID.group=0;
    frameParams.mvOut.push_back(dst_wroto);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO3(%d)...push dst done\n]", type);

    IMEM_BUF_INFO buf_out2_img2o;
    buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_img2o);
    memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
    IImageBuffer* outBuffer_2_img2o=NULL;
    MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_2_img2o,portBufInfo_2_img2o,true);
    outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
    outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
    outBuffer_2_img2o->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_img2o;
    dst_2_img2o.mPortID=PORT_IMG2O;
    dst_2_img2o.mBuffer=outBuffer_2_img2o;
    dst_2_img2o.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_img2o);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    MUINT32 _feo_w_ = _output_w_ /16 * 56;
    MUINT32 _feo_h_ = _output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO3", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO3",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO3(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO3(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO3(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO3(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO3(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename_wroto[256];
        sprintf(filename_wroto, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2_img2o[256];
        sprintf(filename2_img2o, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
        saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO3_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO3(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_wroto->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out1_wroto);
    outBuffer_2->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_img2o->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out2_img2o);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO3");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO3...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO3");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO3...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2ArawInwithFEO4(int loopNum) //vsdof usage, with srz1 enabled
{
    int ret=0;
    printf("--- [P2ArawInwithFEO4...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2ArawInwithFEO4");
    printf("--- [P2ArawInwithFEO4...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2ArawInwithFEO4...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2ArawInwithFEO4...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2ArawInwithFEO4...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_output_w_img2o_;
    crop.mResizeDst.h=_output_h_img2o_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2ArawInwithFEO4...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out1_wroto;
    buf_out1_wroto.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1_wroto);
    memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
    IImageBuffer* outBuffer_wroto=NULL;
    MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_1_wroto,portBufInfo_1_wroto,true);
    outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
    outBuffer_wroto->incStrong(outBuffer_wroto);
    outBuffer_wroto->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_wroto;
    dst_wroto.mPortID=PORT_WROTO;
    dst_wroto.mBuffer=outBuffer_wroto;
    dst_wroto.mPortID.group=0;
    frameParams.mvOut.push_back(dst_wroto);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG3O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2ArawInwithFEO4(%d)...push dst done\n]", type);

    IMEM_BUF_INFO buf_out2_img2o;
    buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_img2o);
    memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
    IImageBuffer* outBuffer_2_img2o=NULL;
    MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_2_img2o,portBufInfo_2_img2o,true);
    outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
    outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
    outBuffer_2_img2o->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_img2o;
    dst_2_img2o.mPortID=PORT_IMG2O;
    dst_2_img2o.mBuffer=outBuffer_2_img2o;
    dst_2_img2o.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_img2o);

    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2ArawInwithFEO4", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2ArawInwithFEO4",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2ArawInwithFEO4(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO4(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO4(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2ArawInwithFEO4(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[P2ArawInwithFEO4(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename_wroto[256];
        sprintf(filename_wroto, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename2_img2o[256];
        sprintf(filename2_img2o, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
        saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2ArawInwithFEO4_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
        printf("--- [P2ArawInwithFEO4(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_wroto->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out1_wroto);
    outBuffer_2->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_img2o->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out2_img2o);
    outBuffer_feo->unlockBuf("P2ArawInwithFEO4");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [P2ArawInwithFEO4...free memory done\n]");

    //
    pStream->uninit("P2ArawInwithFEO4");
    pStream->destroyInstance();
    printf("--- [P2ArawInwithFEO4...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


//
int basicP2AwithFEO(int type,int loopNum)
{
    int ret=0;
    //
    switch(type)
    {
        case 0 :
        case 1 :
        default:
            ret=P2ArawInwithFEO(loopNum); //for eis, with srz1 disabled
            break;
        case 2 :
            ret=P2ArawInwithFEO3(loopNum); //for vsdof, with srz1 disabled
            break;
        case 3 :
            ret=P2ArawInwithFEO4(loopNum); //for vsdof, with srz1 disabled
            break;
    }

    return ret;
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////
int basicP2A_isponly_withTpipe(int type, int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_isponly_withTpipe...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_isponly_withTpipe");
    printf("--- [basicP2A_isponly_withTpipe...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_isponly_withTpipe...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_isponly_withTpipe...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_isponly_withTpipe...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    if(type==0)
    {
        crop.mCropRect.p_fractional.x=0;
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=0;
        crop.mCropRect.p_integral.y=0;
        crop.mCropRect.s.w=_imgi_w_;
        crop.mCropRect.s.h=_imgi_h_;
        crop.mResizeDst.w=_imgi_w_;
        crop.mResizeDst.h=_imgi_h_;
    }
    else
    {
        crop.mCropRect.p_fractional.x=0;  //for img2o
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=600;
        crop.mCropRect.p_integral.y=400;
        crop.mCropRect.s.w=_imgi_w_ - 600 - 200;
        crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_output_w_;
    crop2.mResizeDst.h=_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_output_w_;
    crop3.mResizeDst.h=_output_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_isponly_withTpipe...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    if(type!=0)
    {
        buf_out2.size=_output_2o_w_*_output_2o_h_*2;
        mpImemDrv->allocVirtBuf(&buf_out2);
        memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
        MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
        PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
        IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
        sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_isponly_withTpipe", imgParam_2,portBufInfo_2,true);
        outBuffer_2 = pHeap_2->createImageBuffer();
        outBuffer_2->incStrong(outBuffer_2);
        outBuffer_2->lockBuf("basicP2A_isponly_withTpipe",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
        Output dst_2;
        dst_2.mPortID=PORT_IMG2O;
        dst_2.mBuffer=outBuffer_2;
        dst_2.mPortID.group=0;
        frameParams.mvOut.push_back(dst_2);	
    //    printf("--- [basicP2A_isponly_withTpipe(%d)...push dst done\n]", type);
    }

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        if(type!=0)
        {
            memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        }

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_isponly_withTpipe(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_isponly_withTpipe(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_isponly_withTpipe(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_isponly_withTpipe(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_isponly_withTpipe(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_isponly_withTpipe_%d_process0x%x_package%d_img3o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        if(type!=0)
        {
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2A_isponly_withTpipe_%d_process0x%x_package%d_img2o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        }
        printf("--- [basicP2A_isponly_withTpipe(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_isponly_withTpipe");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_isponly_withTpipe");
    mpImemDrv->freeVirtBuf(&buf_out1);
    if(type!=0)
    {
        outBuffer_2->unlockBuf("basicP2A_isponly_withTpipe");
        mpImemDrv->freeVirtBuf(&buf_out2);
    }
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [basicP2A_isponly_withTpipe...free memory done\n]");

    //
    pStream->uninit("basicP2A_isponly_withTpipe");
    pStream->destroyInstance();
    printf("--- [basicP2A_isponly_withTpipe...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////

//
int testTPIPECtrlExtension_isponly(int type,int loopNum)
{
    int ret=0;
    //
    printf("--- [testTPIPECtrlExtension_isponly] type(%d) _ start\n]",type);
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_isponly_withTpipe(0, loopNum);
            break;
        case 1 :
            ret=basicP2A_isponly_withTpipe(1, loopNum);
            break;
        case 2:
            ret=P2ArawInwithFEO2(loopNum);
            break;
    }
    printf("--- [testTPIPECtrlExtension_isponly] type(%d) _ end\n]",type);

    return ret;
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////
int basicP2A_differentViewAngle(int type, int loopNum)
{
    int ret=0;
    printf("--- [basicP2A_differentViewAngle...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2A_differentViewAngle");
    printf("--- [basicP2A_differentViewAngle...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2A_differentViewAngle...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2A_differentViewAngle...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2A_differentViewAngle...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _mdp_output_w_=1280, _mdp_output_h_=960;
    MUINT32 _mdp_output_w_2=1080, _mdp_output_h_2=720;
    MUINT32 _output_2o_w_=640, _output_2o_h_=480;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    if(type==0)
    {
        crop.mCropRect.p_fractional.x=0;
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=0;
        crop.mCropRect.p_integral.y=0;
        crop.mCropRect.s.w=_imgi_w_;
        crop.mCropRect.s.h=_imgi_h_;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    else
    {
        crop.mCropRect.p_fractional.x=0;  //for img2o
        crop.mCropRect.p_fractional.y=0;
        crop.mCropRect.p_integral.x=600;
        crop.mCropRect.p_integral.y=400;
        crop.mCropRect.s.w=_imgi_w_ - 600 - 200;
        crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
        crop.mResizeDst.w=_output_2o_w_;
        crop.mResizeDst.h=_output_2o_h_;
    }
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=300;
    crop2.mCropRect.p_integral.y=200;
    crop2.mCropRect.s.w=_imgi_w_ - 300 - 400;
    crop2.mCropRect.s.h=_imgi_h_ - 200 - 400;
    crop2.mResizeDst.w=_mdp_output_w_;
    crop2.mResizeDst.h=_mdp_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=500;
    crop3.mCropRect.p_integral.y=300;
    crop3.mCropRect.s.w=_imgi_w_ - 500 - 400;
    crop3.mCropRect.s.h=_imgi_h_ - 300 - 400;
    crop3.mResizeDst.w=_mdp_output_w_2;
    crop3.mResizeDst.h=_mdp_output_h_2;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2A_differentViewAngle...push crop information done\n]");

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_mdp_output_w_*_mdp_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mdp_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_,_mdp_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    //output dma
    IMEM_BUF_INFO buf_out1_1;
    buf_out1_1.size=_mdp_output_w_2*_mdp_output_h_2*2;
    mpImemDrv->allocVirtBuf(&buf_out1_1);
    memset((MUINT8*)buf_out1_1.virtAddr, 0xffffffff, buf_out1_1.size);
    IImageBuffer* outBuffer_1_1=NULL;
    MUINT32 bufStridesInBytes_1_1[3] = {_mdp_output_w_2*2,0,0};
    PortBufInfo_v1 portBufInfo_1_1 = PortBufInfo_v1( buf_out1_1.memID,buf_out1_1.virtAddr,0,buf_out1_1.bufSecu, buf_out1_1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_2,_mdp_output_h_2),  bufStridesInBytes_1_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_1_1,portBufInfo_1_1,true);
    outBuffer_1_1 = pHeap_1_1->createImageBuffer();
    outBuffer_1_1->incStrong(outBuffer_1_1);
    outBuffer_1_1->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_1_1;
    dst_1_1.mPortID=PORT_WROTO;
    dst_1_1.mBuffer=outBuffer_1_1;
    dst_1_1.mPortID.group=0;
    frameParams.mvOut.push_back(dst_1_1);	

    //
    IMEM_BUF_INFO buf_out2;
    IImageBuffer* outBuffer_2=NULL;
    buf_out2.size=_output_2o_w_*_output_2o_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    MUINT32 bufStridesInBytes_2[3] = {_output_2o_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_2o_w_,_output_2o_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	

    //
    IMEM_BUF_INFO buf_out2_1;
    IImageBuffer* outBuffer_2_1=NULL;
    buf_out2_1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2_1);
    memset((MUINT8*)buf_out2_1.virtAddr, 0xffffffff, buf_out2_1.size);
    MUINT32 bufStridesInBytes_2_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2_1 = PortBufInfo_v1( buf_out2_1.memID,buf_out2_1.virtAddr,0,buf_out2_1.bufSecu, buf_out2_1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2_1 = ImageBufferHeap::create( "basicP2A_differentViewAngle", imgParam_2_1,portBufInfo_2_1,true);
    outBuffer_2_1 = pHeap_2_1->createImageBuffer();
    outBuffer_2_1->incStrong(outBuffer_2_1);
    outBuffer_2_1->lockBuf("basicP2A_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2_1;
    dst_2_1.mPortID=PORT_IMG3O;
    dst_2_1.mBuffer=outBuffer_2_1;
    dst_2_1.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2_1);	
//    printf("--- [basicP2A_differentViewAngle(%d)...push dst done\n]", type);

    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);
    enqueParams.mvFrameParams.push_back(frameParams);

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_1.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_1.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2A_differentViewAngle(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_differentViewAngle(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_differentViewAngle(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2A_differentViewAngle(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[basicP2A_differentViewAngle(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_wmdao_%dx%d.yuv",type, (MUINT32) getpid (),i, _mdp_output_w_,_mdp_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mdp_output_w_ *_mdp_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_wroto_%dx%d.yuv",type, (MUINT32) getpid (),i, _mdp_output_w_2, _mdp_output_h_2);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _mdp_output_w_2 *_mdp_output_h_2 * 2);

        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_img2o_%dx%d.yuv",type, (MUINT32) getpid (),i, _output_2o_w_,_output_2o_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_2o_w_ *_output_2o_h_ * 2);
        char filename4[256];
        sprintf(filename4, "/data/P2iopipe_basicP2A_differentViewAngle_%d_process0x%x_package%d_img3o_%dx%d.yuv",type, (MUINT32) getpid (),i, _imgi_w_,_imgi_h_);
        saveBufToFile(filename4, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
        printf("--- [basicP2A_differentViewAngle(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_1_1->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1_1);
    outBuffer_2->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_2_1->unlockBuf("basicP2A_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2_1);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    //
    printf("--- [basicP2A_differentViewAngle...free memory done\n]");

    //
    pStream->uninit("basicP2A_differentViewAngle");
    pStream->destroyInstance();
    printf("--- [basicP2A_differentViewAngle...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int P2AwithFEO_differentViewAngle(int loopNum) //need revise, enable srz1 crop
{
    int ret=0;
    printf("--- [P2AwithFEO_differentViewAngle...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2AwithFEO_differentViewAngle");
    printf("--- [P2AwithFEO_differentViewAngle...pStream init done]\n");
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;    

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FE;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [P2AwithFEO_differentViewAngle...flag -1 ]\n");
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [P2AwithFEO_differentViewAngle...flag -8]\n");
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [P2AwithFEO_differentViewAngle...push src done]\n");

    //
    MUINT32 _output_w_=1600, _output_h_=1200;
    MUINT32 _mdp_output_w_=1280, _mdp_output_h_=960;
    MUINT32 _mdp_output_w_2=1080, _mdp_output_h_2=720;
   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=400;
    crop.mCropRect.p_integral.y=400;
    crop.mCropRect.s.w=_imgi_w_ - 400 - 200;
    crop.mCropRect.s.h=_imgi_h_ - 400 - 200;
    crop.mResizeDst.w=_output_w_;
    crop.mResizeDst.h=_output_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=300;
    crop2.mCropRect.p_integral.y=100;
    crop2.mCropRect.s.w=_output_w_ - 300;
    crop2.mCropRect.s.h=_output_h_ - 100;
    crop2.mResizeDst.w=_mdp_output_w_;
    crop2.mResizeDst.h=_mdp_output_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=500;
    crop3.mCropRect.p_integral.y=600;
    crop3.mCropRect.s.w=_output_w_ - 500;
    crop3.mCropRect.s.h=_output_h_ - 600;
    crop3.mResizeDst.w=_mdp_output_w_2;
    crop3.mResizeDst.h=_mdp_output_h_2;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [P2AwithFEO_differentViewAngle...push crop information done\n]");

    //mdp output dma
    IMEM_BUF_INFO mdp_buf_out1;
    mdp_buf_out1.size=_mdp_output_w_*_mdp_output_h_*2;
    mpImemDrv->allocVirtBuf(&mdp_buf_out1);
    memset((MUINT8*)mdp_buf_out1.virtAddr, 0xffffffff, mdp_buf_out1.size);
    IImageBuffer* mdp_outBuffer=NULL;
    MUINT32 mdp_bufStridesInBytes_1[3] = {_mdp_output_w_*2,0,0};
    PortBufInfo_v1 mdp_portBufInfo_1 = PortBufInfo_v1( mdp_buf_out1.memID,mdp_buf_out1.virtAddr,0,mdp_buf_out1.bufSecu, mdp_buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam mdp_imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_,_mdp_output_h_),  mdp_bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> mdp_pHeap_1 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", mdp_imgParam_1,mdp_portBufInfo_1,true);
    mdp_outBuffer = mdp_pHeap_1->createImageBuffer();
    mdp_outBuffer->incStrong(mdp_outBuffer);
    mdp_outBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output mdp_dst;
    mdp_dst.mPortID=PORT_WDMAO;
    mdp_dst.mBuffer=mdp_outBuffer;
    mdp_dst.mPortID.group=0;
    frameParams.mvOut.push_back(mdp_dst);   
    //
    IMEM_BUF_INFO mdp_buf_out2;
    mdp_buf_out2.size=_mdp_output_w_2*_mdp_output_h_2*2;
    mpImemDrv->allocVirtBuf(&mdp_buf_out2);
    memset((MUINT8*)mdp_buf_out2.virtAddr, 0xffffffff, mdp_buf_out2.size);
    IImageBuffer* mdp_outBuffer2=NULL;
    MUINT32 mdp_bufStridesInBytes_2[3] = {_mdp_output_w_2*2,0,0};
    PortBufInfo_v1 mdp_portBufInfo_2 = PortBufInfo_v1( mdp_buf_out2.memID,mdp_buf_out2.virtAddr,0,mdp_buf_out2.bufSecu, mdp_buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam mdp_imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_mdp_output_w_2,_mdp_output_h_2),  mdp_bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> mdp_pHeap_2 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", mdp_imgParam_2,mdp_portBufInfo_2,true);
    mdp_outBuffer2 = mdp_pHeap_2->createImageBuffer();
    mdp_outBuffer2->incStrong(mdp_outBuffer2);
    mdp_outBuffer2->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output mdp_dst2;
    mdp_dst2.mPortID=PORT_WROTO;
    mdp_dst2.mBuffer=mdp_outBuffer2;
    mdp_dst2.mPortID.group=0;
    frameParams.mvOut.push_back(mdp_dst2);   


    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_IMG3O;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_output_w_*_output_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_IMG2O;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
//    printf("--- [P2AwithFEO_differentViewAngle(%d)...push dst done\n]", type);


    //feo, set fe_mode as 1
    IMEM_BUF_INFO buf_out_feo;
    int _feo_output_w_=800, _feo_output_h_=640;
    MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
    MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
    buf_out_feo.size=_feo_w_*_feo_h_;
    mpImemDrv->allocVirtBuf(&buf_out_feo);
    memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
    IImageBuffer* outBuffer_feo=NULL;
    MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
    PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "P2AwithFEO_differentViewAngle", imgParam_feo,portBufInfo_feo,true);
    outBuffer_feo = pHeap_feo->createImageBuffer();
    outBuffer_feo->incStrong(outBuffer_feo);
    outBuffer_feo->lockBuf("P2AwithFEO_differentViewAngle",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_feo;
    dst_feo.mPortID=PORT_FEO;
    dst_feo.mBuffer=outBuffer_feo;
    dst_feo.mPortID.group=0;
    frameParams.mvOut.push_back(dst_feo);	


    IMEM_BUF_INFO pTuningQueBuf;
    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
    dip_x_reg_t *pIspReg;
    pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
    SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
     //fe module via tuning
    pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;   //fe mode =1
    frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);

    //srz1 config
    ModuleInfo srz1_module;
    srz1_module.moduleTag = EDipModule_SRZ1;
    srz1_module.frameGroup=0;
    _SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
    mpsrz1Param->in_w = _imgi_w_;
    mpsrz1Param->in_h = _imgi_h_;
    mpsrz1Param->crop_floatX = 0;
    mpsrz1Param->crop_floatY = 0;
    mpsrz1Param->crop_x = 100;
    mpsrz1Param->crop_y = 100;
    mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
    mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
    mpsrz1Param->out_w = _feo_output_w_;
    mpsrz1Param->out_h = _feo_output_h_;
    srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
    frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);
    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, mdp_buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, mdp_buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
        memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [P2AwithFEO_differentViewAngle(%d)...flush done\n]", i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2AwithFEO_differentViewAngle(%d)..enque fail\n]", i);
        }
        else
        {
            printf("---[P2AwithFEO_differentViewAngle(%d)..enque done\n]", i);
        }

        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2AwithFEO_differentViewAngle(%d)..deque fail\n]", i);
        }
        else
        {
            printf("---[P2AwithFEO_differentViewAngle(%d)..deque done\n]", i);
        }


        //dump image
        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _mdp_output_w_,_mdp_output_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mdp_output_w_ *_mdp_output_h_ * 2);
        char filename2[256];
        sprintf(filename2, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _mdp_output_w_2,_mdp_output_h_2);
        saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _mdp_output_w_2 *_mdp_output_h_2 * 2);

        char filename3[256];
        sprintf(filename3, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename4[256];
        sprintf(filename4, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
        saveBufToFile(filename4, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
        char filename5[256];
        sprintf(filename5, "/data/P2iopipe_P2AwithFEO_differentViewAngle_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
        saveBufToFile(filename5, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ );
        printf("--- [P2AwithFEO_differentViewAngle(%d)...save file done\n]",i);
    }

    //free
    srcBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    mdp_outBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&mdp_buf_out1);
    mdp_outBuffer2->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&mdp_buf_out2);
    outBuffer->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out2);
    outBuffer_feo->unlockBuf("P2AwithFEO_differentViewAngle");
    mpImemDrv->freeVirtBuf(&buf_out_feo);
    mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
    mpImemDrv->freeVirtBuf(&pTuningQueBuf);
    delete mpsrz1Param;
    //
    printf("--- [P2AwithFEO_differentViewAngle...free memory done\n]");

    //
    pStream->uninit("P2AwithFEO_differentViewAngle");
    pStream->destroyInstance();
    printf("--- [P2AwithFEO_differentViewAngle...pStream uninit done\n]");
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}

//
int testTPIPECtrlExtension_directlink(int type,int loopNum)
{
    int ret=0;
    //
    printf("--- [testTPIPECtrlExtension_directlink] type(%d) _ start\n]",type);
    switch(type)
    {
        case 0 :
        default:
            ret=basicP2A_differentViewAngle(1, loopNum);
            break;
        case 1:
            ret=P2AwithFEO_differentViewAngle(loopNum);
            break;
    }
    printf("--- [testTPIPECtrlExtension_directlink] type(%d) _ end\n]",type);

    return ret;
}

/////////////////////////////////////////////////////////////////////////////////////
int multiVss_test(int type,int loopNum)
{
    int ret=0;
    sem_init(&mSem_ThreadEnd, 0, 0);
    printf("--- [multiVss_test(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("multiVss_test");
    printf("--- [multiVss_test(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();


    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Vss;

    //input image
    MUINT32 _imgi_w_=1600, _imgi_h_=1200;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(g_imgi_array_1600x1200_b10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(g_imgi_array_1600x1200_b10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [multiVss_test(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) , 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "multiVss_test", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [multiVss_test(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [multiVss_test(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    crop3.mResizeDst.w=_imgi_w_;
    crop3.mResizeDst.h=_imgi_h_;
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [multiVss_test(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_wdmao;
    buf_wdmao.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wdmao);
    memset((MUINT8*)buf_wdmao.virtAddr, 0xffffffff, buf_wdmao.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_wdmao.memID,buf_wdmao.virtAddr,0,buf_wdmao.bufSecu, buf_wdmao.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "multiVss_test", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_WDMAO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);	

    IMEM_BUF_INFO buf_wroto;
    buf_wroto.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_wroto);
    memset((MUINT8*)buf_wroto.virtAddr, 0xffffffff, buf_wroto.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_wroto.memID,buf_wroto.virtAddr,0,buf_wroto.bufSecu, buf_wroto.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "multiVss_test", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("multiVss_test",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    dst_2.mPortID=PORT_WROTO;
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;
    frameParams.mvOut.push_back(dst_2);	
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [multiVss_test(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_wdmao.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_wroto.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [multiVss_test(%d_%d)...flush done\n]", type, i);

        if(i==0)
        {
            //create vss capture thread
            mThread_loopNum=loopNum;
            mThread_type=type;
#ifdef __LP64__
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2, {0}};
#else
            pthread_attr_t const attr = {0, NULL, 1024 * 1024, 4096, SCHED_OTHER, NICE_CAMERA_PASS2};
#endif
            pthread_create(&mThread, &attr, vssCaptureThread, NULL);
            usleep(130000);//wait another thread config done
        }

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiVss_test(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[multiVss_test(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [multiVss_test(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [multiVss_test(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[multiVss_test(%d_%d)..deque done\n]", type, i);
        }

        if(i==(loopNum-1))
        {
        //dump image
            char filename[256];
            sprintf(filename, "/data/P2iopipe_multiVss_test_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_multiVss_test_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            printf("--- [multiVss_test(%d_%d)...save file done\n]", type,i);
        }
    }

    //free
    srcBuffer->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_wdmao);
    outBuffer_2->unlockBuf("multiVss_test");
    mpImemDrv->freeVirtBuf(&buf_wroto);
    printf("--- [multiVss_test(%d)...free memory done\n]", type);

    //
    pStream->uninit("multiVss_test");
    pStream->destroyInstance();
    printf("--- [multiVss_test(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    ::sem_wait(&mSem_ThreadEnd);
    return ret;
}

#include "pic/UFO/imgi_fg_960_540_10.h"
#include "pic/UFO/ufdi_16_540_10.h"
#include "pic/UFO/imgi_fg_960_540_12.h"
#include "pic/UFO/ufdi_16_540_12.h"
int basicP2AwithFullG_UFO_in(int type,int loopNum)
{
	int ret=0;
	printf("--- [basicP2AwithFullG_UFO_in...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n");
	NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	pStream->init("basicP2AwithFullG_UFO_in");
	printf("--- [basicP2AwithFullG_UFO_in...pStream init done]\n");
	IMemDrv* mpImemDrv=NULL;
	mpImemDrv=IMemDrv::createInstance();
	mpImemDrv->init();

	QParams enqueParams;
    FrameParams frameParams;

	//frame tag
	frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

	MUINT32 _imgi_w_, _imgi_h_;
	IMEM_BUF_INFO buf_imgi;
	MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
	IImageBuffer* srcBuffer;
	IMEM_BUF_INFO buf_ufdi;
	IImageBuffer* srcBuffer_ufdi;
	MUINT32 stride_imgi;
	if (type==0)
	{
		//input image(imgi)
		_imgi_w_=960;//1600;
		_imgi_h_=540;//1200;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_960_540_10);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_960_540_10), buf_imgi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*10/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, 0, 0};
		PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
		srcBuffer = pHeap->createImageBuffer();
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		//input dma(ufdi)
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_10);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_10), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
	}
	else
	{
		//input image(imgi)
		_imgi_w_=960;//1600;
		_imgi_h_=540;//1200;
		buf_imgi.size=sizeof(ufo_fg_g_imgi_array_960_540_12);
		mpImemDrv->allocVirtBuf(&buf_imgi);
		memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(ufo_fg_g_imgi_array_960_540_12), buf_imgi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in...flag -1 ]\n");
		stride_imgi = ((((_imgi_w_*12/8) * 3 / 2)+15)>>4)<<4;
		printf("--- [basicP2AwithFullG_UFO_in...stride imgi(%d)]\n", stride_imgi);
		MUINT32 bufStridesInBytes[3] = {stride_imgi, 0, 0};
		PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER12),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
		sp<ImageBufferHeap> pHeap;
		pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
		srcBuffer = pHeap->createImageBuffer();
		srcBuffer->incStrong(srcBuffer);
		srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in...flag -8]\n");
		Input src;
		src.mPortID=PORT_IMGI;
		src.mBuffer=srcBuffer;
		src.mPortID.group=0;
		frameParams.mvIn.push_back(src);

		//input dma(ufdi)
		MUINT32 _ufdi_w_=16, _ufdi_h_=_imgi_h_;
		buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_540_12);
		mpImemDrv->allocVirtBuf(&buf_ufdi);
		memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_540_12), buf_ufdi.size);
		//imem buffer 2 image heap
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
		MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
		MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
		PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
		IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
		sp<ImageBufferHeap> pHeap_ufdi;
		pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
		srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
		srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
		srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
		Input src_ufdi;
		src_ufdi.mPortID=PORT_UFDI;
		src_ufdi.mBuffer=srcBuffer_ufdi;
		src_ufdi.mPortID.group=0;
	 	frameParams.mvIn.push_back(src_ufdi);
	}
	printf("--- [basicP2AwithFullG_UFO_in...push src done]\n");

	//
	MUINT32 _output_w_=960, _output_h_=540;
	MUINT32 _output_w_img2o_=640, _output_h_img2o_=480;
	//crop information
	MCrpRsInfo crop;
	crop.mFrameGroup=0;
	crop.mGroupID=1;
	MCrpRsInfo crop2;
	crop2.mFrameGroup=0;
	crop2.mGroupID=2;
	MCrpRsInfo crop3;
	crop3.mFrameGroup=0;
	crop3.mGroupID=3;
	crop.mCropRect.p_fractional.x=0;
	crop.mCropRect.p_fractional.y=0;
	crop.mCropRect.p_integral.x=0;
	crop.mCropRect.p_integral.y=0;
	crop.mCropRect.s.w=_imgi_w_;
	crop.mCropRect.s.h=_imgi_h_;
	crop.mResizeDst.w=_output_w_img2o_;
	crop.mResizeDst.h=_output_h_img2o_;
	crop2.mCropRect.p_fractional.x=0;
	crop2.mCropRect.p_fractional.y=0;
	crop2.mCropRect.p_integral.x=0;
	crop2.mCropRect.p_integral.y=0;
	crop2.mCropRect.s.w=_imgi_w_;
	crop2.mCropRect.s.h=_imgi_h_;
	crop2.mResizeDst.w=_output_w_;
	crop2.mResizeDst.h=_output_h_;
	crop3.mCropRect.p_fractional.x=0;
	crop3.mCropRect.p_fractional.y=0;
	crop3.mCropRect.p_integral.x=0;
	crop3.mCropRect.p_integral.y=0;
	crop3.mCropRect.s.w=_imgi_w_;
	crop3.mCropRect.s.h=_imgi_h_;
	crop3.mResizeDst.w=_output_w_;
	crop3.mResizeDst.h=_output_h_;
	frameParams.mvCropRsInfo.push_back(crop);
	frameParams.mvCropRsInfo.push_back(crop2);
	frameParams.mvCropRsInfo.push_back(crop3);
	printf("--- [basicP2AwithFullG_UFO_in...push crop information done\n]");

	//output dma
	IMEM_BUF_INFO buf_out1;
	buf_out1.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1);
	memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
	IImageBuffer* outBuffer=NULL;
	MUINT32 bufStridesInBytes_1[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1,portBufInfo_1,true);
	outBuffer = pHeap_1->createImageBuffer();
	outBuffer->incStrong(outBuffer);
	outBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst;
	dst.mPortID=PORT_WDMAO;
	dst.mBuffer=outBuffer;
	dst.mPortID.group=0;
	frameParams.mvOut.push_back(dst);

	IMEM_BUF_INFO buf_out1_wroto;
	buf_out1_wroto.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out1_wroto);
	memset((MUINT8*)buf_out1_wroto.virtAddr, 0xffffffff, buf_out1_wroto.size);
	IImageBuffer* outBuffer_wroto=NULL;
	MUINT32 bufStridesInBytes_1_wroto[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_1_wroto = PortBufInfo_v1( buf_out1_wroto.memID,buf_out1_wroto.virtAddr,0,buf_out1_wroto.bufSecu, buf_out1_wroto.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_1_wroto = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											MSize(_output_w_,_output_h_),  bufStridesInBytes_1_wroto, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_1_wroto = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1_wroto,portBufInfo_1_wroto,true);
	outBuffer_wroto = pHeap_1_wroto->createImageBuffer();
	outBuffer_wroto->incStrong(outBuffer_wroto);
	outBuffer_wroto->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_wroto;
	dst_wroto.mPortID=PORT_WROTO;
	dst_wroto.mBuffer=outBuffer_wroto;
	dst_wroto.mPortID.group=0;
	frameParams.mvOut.push_back(dst_wroto);

	IMEM_BUF_INFO buf_out2;
	buf_out2.size=_output_w_*_output_h_*2;
	mpImemDrv->allocVirtBuf(&buf_out2);
	memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
	IImageBuffer* outBuffer_2=NULL;
	MUINT32 bufStridesInBytes_2[3] = {_output_w_*2,0,0};
	PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_,_output_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2,portBufInfo_2,true);
	outBuffer_2 = pHeap_2->createImageBuffer();
	outBuffer_2->incStrong(outBuffer_2);
	outBuffer_2->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2;
	dst_2.mPortID=PORT_IMG3O;
	dst_2.mBuffer=outBuffer_2;
	dst_2.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2);
	//	  printf("--- [basicP2AwithFullG_UFO_in(%d)...push dst done\n]", type);

	IMEM_BUF_INFO buf_out2_img2o;
	buf_out2_img2o.size=_output_w_img2o_*_output_h_img2o_*2;
	mpImemDrv->allocVirtBuf(&buf_out2_img2o);
	memset((MUINT8*)buf_out2_img2o.virtAddr, 0xffffffff, buf_out2_img2o.size);
	IImageBuffer* outBuffer_2_img2o=NULL;
	MUINT32 bufStridesInBytes_2_img2o[3] = {_output_w_img2o_*2,0,0};
	PortBufInfo_v1 portBufInfo_2_img2o = PortBufInfo_v1( buf_out2_img2o.memID,buf_out2_img2o.virtAddr,0,buf_out2_img2o.bufSecu, buf_out2_img2o.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_2_img2o = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_output_w_img2o_,_output_h_img2o_),  bufStridesInBytes_2_img2o, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_2_img2o = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2_img2o,portBufInfo_2_img2o,true);
	outBuffer_2_img2o = pHeap_2_img2o->createImageBuffer();
	outBuffer_2_img2o->incStrong(outBuffer_2_img2o);
	outBuffer_2_img2o->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_2_img2o;
	dst_2_img2o.mPortID=PORT_IMG2O;
	dst_2_img2o.mBuffer=outBuffer_2_img2o;
	dst_2_img2o.mPortID.group=0;
	frameParams.mvOut.push_back(dst_2_img2o);

	//feo, set fe_mode as 1
	IMEM_BUF_INFO buf_out_feo;
	MUINT32 _feo_output_w_=800, _feo_output_h_=640;
	MUINT32 _feo_w_ = _feo_output_w_ /16 * 56;
	MUINT32 _feo_h_ = _feo_output_h_ >> (5-1);
	buf_out_feo.size=_feo_w_*_feo_h_;
	mpImemDrv->allocVirtBuf(&buf_out_feo);
	memset((MUINT8*)buf_out_feo.virtAddr, 0xffffffff, buf_out_feo.size);
	IImageBuffer* outBuffer_feo=NULL;
	MUINT32 bufStridesInBytes_feo[3] = {_feo_w_,0,0};
	PortBufInfo_v1 portBufInfo_feo = PortBufInfo_v1( buf_out_feo.memID,buf_out_feo.virtAddr,0,buf_out_feo.bufSecu, buf_out_feo.bufCohe);
	IImageBufferAllocator::ImgParam imgParam_feo = IImageBufferAllocator::ImgParam((eImgFmt_BAYER8),MSize(_feo_w_,_feo_h_),  bufStridesInBytes_feo, bufBoundaryInBytes, 1);
	sp<ImageBufferHeap> pHeap_feo = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_feo,portBufInfo_feo,true);
	outBuffer_feo = pHeap_feo->createImageBuffer();
	outBuffer_feo->incStrong(outBuffer_feo);
	outBuffer_feo->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	Output dst_feo;
	dst_feo.mPortID=PORT_FEO;
	dst_feo.mBuffer=outBuffer_feo;
	dst_feo.mPortID.group=0;
	frameParams.mvOut.push_back(dst_feo);

	IMEM_BUF_INFO pTuningQueBuf;
	pTuningQueBuf.size = sizeof(dip_x_reg_t);
	mpImemDrv->allocVirtBuf(&pTuningQueBuf);
	mpImemDrv->mapPhyAddr(&pTuningQueBuf);
	memset((MUINT8*)(pTuningQueBuf.virtAddr), 0x0, pTuningQueBuf.size);
	dip_x_reg_t *pIspReg;
	pIspReg = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
	printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2G, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_G2C, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_GGM, 0);
	SetDefaultTuning(pIspReg, (MUINT32*)(pTuningQueBuf.virtAddr), tuning_tag_UDM, 0);
	 //fe module via tuning
	pIspReg->DIP_X_FE_CTRL1.Raw=0xAD;	//fe mode =1
	frameParams.mTuningData = reinterpret_cast<MVOID*>(pTuningQueBuf.virtAddr);

	//srz1 config
	ModuleInfo srz1_module;
	srz1_module.moduleTag = EDipModule_SRZ1;
	srz1_module.frameGroup=0;
	_SRZ_SIZE_INFO_    *mpsrz1Param = new _SRZ_SIZE_INFO_;
	mpsrz1Param->in_w = _imgi_w_;
	mpsrz1Param->in_h = _imgi_h_;
	mpsrz1Param->crop_floatX = 0;
	mpsrz1Param->crop_floatY = 0;
	mpsrz1Param->crop_x = 100;
	mpsrz1Param->crop_y = 100;
	mpsrz1Param->crop_w = mpsrz1Param->in_w - 100 - 100;
	mpsrz1Param->crop_h = mpsrz1Param->in_h - 100 - 100;
	mpsrz1Param->out_w = _feo_output_w_;
	mpsrz1Param->out_h = _feo_output_h_;
	srz1_module.moduleStruct   = reinterpret_cast<MVOID*> (mpsrz1Param);
	frameParams.mvModuleData.push_back(srz1_module);
    enqueParams.mvFrameParams.push_back(frameParams);

	for(int i=0;i<loopNum;i++)
	{
		memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out1_wroto.size);
		memset((MUINT8*)(frameParams.mvOut[2].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);
		memset((MUINT8*)(frameParams.mvOut[3].mBuffer->getBufVA(0)), 0xffffffff, buf_out2_img2o.size);
		memset((MUINT8*)(frameParams.mvOut[4].mBuffer->getBufVA(0)), 0xffffffff, buf_out_feo.size);

		//buffer operation
		mpImemDrv->cacheFlushAll();
		printf("--- [basicP2AwithFullG_UFO_in(%d)...flush done\n]", i);

		//enque
		ret=pStream->enque(enqueParams);
		if(!ret)
		{
			printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d)..enque fail\n]", i);
		}
		else
		{
			printf("---[basicP2AwithFullG_UFO_in(%d)..enque done\n]", i);
		}

		//deque
		//wait a momet in fpga
		//usleep(5000000);
		QParams dequeParams;
		ret=pStream->deque(dequeParams);
		if(!ret)
		{
			printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d)..deque fail\n]", i);
		}
		else
		{
			printf("---[basicP2AwithFullG_UFO_in(%d)..deque done\n]", i);
		}


		//dump image
		if (type==0)
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			char filename3[256];
			sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG10_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		else
		{
			char filename[256];
			sprintf(filename, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename_wroto[256];
			sprintf(filename_wroto, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename_wroto, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2[256];
			sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_,_output_h_);
			saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[2].mBuffer->getBufVA(0)), _output_w_ *_output_h_ * 2);
			char filename2_img2o[256];
			sprintf(filename2_img2o, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (),i, _output_w_img2o_,_output_h_img2o_);
			saveBufToFile(filename2_img2o, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[3].mBuffer->getBufVA(0)), _output_w_img2o_ *_output_h_img2o_ * 2);
			char filename3[256];
			sprintf(filename3, "/data/P2iopipe_basicP2AwithFullG12_UFO_in_process0x%x_package%d_feo_%dx%d.yuv",(MUINT32) getpid (),i, _feo_w_,_feo_h_);
			saveBufToFile(filename3, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[4].mBuffer->getBufVA(0)), _feo_w_ *_feo_h_ * 2);
		}
		printf("--- [basicP2AwithFullG_UFO_in(%d)...save file done\n]",i);
	}

	//free
	srcBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_imgi);
	srcBuffer_ufdi->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_ufdi);
	outBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1);
	outBuffer_wroto->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out1_wroto);
	outBuffer_2->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2);
	outBuffer_2_img2o->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out2_img2o);
	outBuffer_feo->unlockBuf("basicP2AwithFullG_UFO_in");
	mpImemDrv->freeVirtBuf(&buf_out_feo);
	mpImemDrv->unmapPhyAddr(&pTuningQueBuf);
	mpImemDrv->freeVirtBuf(&pTuningQueBuf);
	//
	printf("--- [basicP2AwithFullG_UFO_in...free memory done\n]");

	//
	pStream->uninit("basicP2AwithFullG_UFO_in");
	pStream->destroyInstance();
	printf("--- [basicP2AwithFullG_UFO_in...pStream uninit done\n]");
	mpImemDrv->uninit();
	mpImemDrv->destroyInstance();

	return ret;

#if 0
	 int ret=0;
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
	 NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
	 pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
	 pStream->init("basicP2AwithFullG_UFO_in");
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...pStream init done]\n", type);
	 IMemDrv* mpImemDrv=NULL;
	 mpImemDrv=IMemDrv::createInstance();
	 mpImemDrv->init();

	 QParams enqueParams;

	 //frame tag
	 enqueParams.mvStreamTag.push_back(NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal);

	 //input image(imgi)
	 int _imgi_w_=640, _imgi_h_=480;
	 IMEM_BUF_INFO buf_imgi;
	 buf_imgi.size=sizeof(p2a_fg_g_imgi_array_640_480_10);
	 mpImemDrv->allocVirtBuf(&buf_imgi);
	 memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_640_480_10), buf_imgi.size);
	 //imem buffer 2 image heap
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
	 IImageBuffer* srcBuffer;
	 MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
	 MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
	 PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap;
	 pHeap = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam,portBufInfo,true);
	 srcBuffer = pHeap->createImageBuffer();
	 srcBuffer->incStrong(srcBuffer);
	 srcBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
	 Input src;
	 src.mPortID=PORT_IMGI;
	 src.mBuffer=srcBuffer;
	 src.mPortID.group=0;
	 enqueParams.mvIn.push_back(src);

	 //input dma(ufdi)
	 int _ufdi_w_=16, _ufdi_h_=480;
	 IMEM_BUF_INFO buf_ufdi;
	 buf_ufdi.size=sizeof(ufo_fg_g_ufdi_array_16_480_10);
	 mpImemDrv->allocVirtBuf(&buf_ufdi);
	 memcpy( (MUINT8*)(buf_ufdi.virtAddr), (MUINT8*)(ufo_fg_g_ufdi_array_16_480_10), buf_ufdi.size);
	 //imem buffer 2 image heap
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -1 ]\n", type);
	 IImageBuffer* srcBuffer_ufdi;
	 MINT32 bufBoundaryInBytes_ufdi[3] = {0, 0, 0};
	 MUINT32 bufStridesInBytes_ufdi[3] = {_ufdi_w_, 0, 0};
	 PortBufInfo_v1 portBufInfo_ufdi = PortBufInfo_v1( buf_ufdi.memID,buf_ufdi.virtAddr,0,buf_ufdi.bufSecu, buf_ufdi.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_ufdi = IImageBufferAllocator::ImgParam((eImgFmt_UFO_FG),MSize(_ufdi_w_, _ufdi_h_), bufStridesInBytes_ufdi, bufBoundaryInBytes_ufdi, 1);
	 sp<ImageBufferHeap> pHeap_ufdi;
	 pHeap_ufdi = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_ufdi,portBufInfo_ufdi,true);
	 srcBuffer_ufdi = pHeap_ufdi->createImageBuffer();
	 srcBuffer_ufdi->incStrong(srcBuffer_ufdi);
	 srcBuffer_ufdi->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...flag -8]\n", type);
	 Input src_ufdi;
	 src_ufdi.mPortID=PORT_UFDI;
	 src_ufdi.mBuffer=srcBuffer_ufdi;
	 src_ufdi.mPortID.group=0;
	 enqueParams.mvIn.push_back(src_ufdi);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push src done]\n", type);

	//crop information
	 MCrpRsInfo crop;
	 crop.mFrameGroup=0;
	 crop.mGroupID=1;
	 MCrpRsInfo crop2;
	 crop2.mFrameGroup=0;
	 crop2.mGroupID=2;
	 MCrpRsInfo crop3;
	 crop3.mFrameGroup=0;
	 crop3.mGroupID=3;
	 crop.mCropRect.p_fractional.x=0;
	 crop.mCropRect.p_fractional.y=0;
	 crop.mCropRect.p_integral.x=0;
	 crop.mCropRect.p_integral.y=0;
	 crop.mCropRect.s.w=_imgi_w_;
	 crop.mCropRect.s.h=_imgi_h_;
	 crop.mResizeDst.w=_imgi_w_;
	 crop.mResizeDst.h=_imgi_h_;
	 crop2.mCropRect.p_fractional.x=0;
	 crop2.mCropRect.p_fractional.y=0;
	 crop2.mCropRect.p_integral.x=0;
	 crop2.mCropRect.p_integral.y=0;
	 crop2.mCropRect.s.w=_imgi_w_;
	 crop2.mCropRect.s.h=_imgi_h_;
	 crop2.mResizeDst.w=_imgi_w_;
	 crop2.mResizeDst.h=_imgi_h_;
	 crop3.mCropRect.p_fractional.x=0;
	 crop3.mCropRect.p_fractional.y=0;
	 crop3.mCropRect.p_integral.x=0;
	 crop3.mCropRect.p_integral.y=0;
	 crop3.mCropRect.s.w=_imgi_w_;
	 crop3.mCropRect.s.h=_imgi_h_;
	 crop3.mResizeDst.w=_imgi_w_;
	 crop3.mResizeDst.h=_imgi_h_;
	 enqueParams.mvCropRsInfo.push_back(crop);
	 enqueParams.mvCropRsInfo.push_back(crop2);
	 enqueParams.mvCropRsInfo.push_back(crop3);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push crop information done\n]", type);

	 //output dma
	 IMEM_BUF_INFO buf_out1;
	 buf_out1.size=_imgi_w_*_imgi_h_*2;
	 mpImemDrv->allocVirtBuf(&buf_out1);
	 memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
	 IImageBuffer* outBuffer=NULL;
	 MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
	 PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
											 MSize(_imgi_w_,_imgi_h_),	bufStridesInBytes_1, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_1,portBufInfo_1,true);
	 outBuffer = pHeap_1->createImageBuffer();
	 outBuffer->incStrong(outBuffer);
	 outBuffer->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 Output dst;
	 if(type==0)
	 {
		 dst.mPortID=PORT_IMG2O;
	 }
	 else
	 {
		 dst.mPortID=PORT_WDMAO;
	 }
	 dst.mBuffer=outBuffer;
	 dst.mPortID.group=0;
	 enqueParams.mvOut.push_back(dst);

	 IMEM_BUF_INFO buf_out2;
	 buf_out2.size=_imgi_w_*_imgi_h_*2;
	 mpImemDrv->allocVirtBuf(&buf_out2);
	 memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
	 IImageBuffer* outBuffer_2=NULL;
	 MUINT32 bufStridesInBytes_2[3] = {_imgi_w_*2,0,0};
	 PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
	 IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_imgi_w_,_imgi_h_),	bufStridesInBytes_2, bufBoundaryInBytes, 1);
	 sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullG_UFO_in", imgParam_2,portBufInfo_2,true);
	 outBuffer_2 = pHeap_2->createImageBuffer();
	 outBuffer_2->incStrong(outBuffer_2);
	 outBuffer_2->lockBuf("basicP2AwithFullG_UFO_in",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
	 Output dst_2;
	 if(type==0)
	 {
		 dst_2.mPortID=PORT_IMG3O;
	 }
	 else
	 {
		 dst_2.mPortID=PORT_WROTO;
	 }
	 dst_2.mBuffer=outBuffer_2;
	 dst_2.mPortID.group=0;
	 enqueParams.mvOut.push_back(dst_2);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...push dst done\n]", type);


	 for(int i=0;i<loopNum;i++)
	 {
		 memset((MUINT8*)(enqueParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
		 memset((MUINT8*)(enqueParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

		 //buffer operation
		 mpImemDrv->cacheFlushAll();
		 printf("--- [basicP2AwithFullG_UFO_in(%d_%d)...flush done\n]", type, i);

		 //enque
		 ret=pStream->enque(enqueParams);
		 if(!ret)
		 {
			 printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d_%d)..enque fail\n]", type, i);
		 }
		 else
		 {
			 printf("---[basicP2AwithFullG_UFO_in(%d_%d)..enque done\n]",type, i);
		 }

		 //temp use while to observe in CVD
		 //printf("--- [basicP2AwithFullG_UFO_in(%d)...enter while...........\n]", type);
		//while(1);


		 //deque
		 //wait a momet in fpga
		 //usleep(5000000);
		 QParams dequeParams;
		 ret=pStream->deque(dequeParams);
		 if(!ret)
		 {
			 printf("---ERRRRRRRRR [basicP2AwithFullG_UFO_in(%d_%d)..deque fail\n]",type, i);
		 }
		 else
		 {
			 printf("---[basicP2AwithFullG_UFO_in(%d_%d)..deque done\n]", type, i);
		 }


		 //dump image
		 if(type==0)
		 {
			 char filename[256];
			 sprintf(filename, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			 char filename2[256];
			 sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		 }
		 else
		 {

			 char filename[256];
			 sprintf(filename, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
			 char filename2[256];
			 sprintf(filename2, "/data/P2iopipe_basicP2AwithFullG_UFO_in_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
			 saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvOut[1].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
		 }
		 printf("--- [basicP2AwithFullG_UFO_in(%d_%d)...save file done\n]", type,i);
	 }

	 //free
	 srcBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_imgi);
	 srcBuffer_ufdi->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_ufdi);
	 outBuffer->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_out1);
	 outBuffer_2->unlockBuf("basicP2AwithFullG_UFO_in");
	 mpImemDrv->freeVirtBuf(&buf_out2);
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...free memory done\n]", type);

	 //
	 pStream->uninit("basicP2AwithFullG_UFO_in");
	 pStream->destroyInstance();
	 printf("--- [basicP2AwithFullG_UFO_in(%d)...pStream uninit done\n]", type);
	 mpImemDrv->uninit();
	 mpImemDrv->destroyInstance();

	 return ret;
#endif
}

#include "../../imageio/test/DIP_pics/P2A_FG/imgi_1920_1080_10.h"
/*********************************************************************************/
int basicP2AwithFullHD(int type,int loopNum)
{
    int ret=0;
    printf("--- [basicP2AwithFullHD(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("basicP2AwithFullHD");
    printf("--- [basicP2AwithFullHD(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();

    QParams enqueParams;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_Normal;

    //input image
    MUINT32 _imgi_w_=1920, _imgi_h_=1080;
    IMEM_BUF_INFO buf_imgi;
    buf_imgi.size=sizeof(p2a_fg_g_imgi_array_1920_1080_10);
    mpImemDrv->allocVirtBuf(&buf_imgi);
    memcpy( (MUINT8*)(buf_imgi.virtAddr), (MUINT8*)(p2a_fg_g_imgi_array_1920_1080_10), buf_imgi.size);
    //imem buffer 2 image heap
    printf("--- [basicP2AwithFullHD(%d)...flag -1 ]\n", type);
    IImageBuffer* srcBuffer;
    MINT32 bufBoundaryInBytes[3] = {0, 0, 0};
    MUINT32 bufStridesInBytes[3] = {(_imgi_w_*10/8) * 3 / 2, 0, 0};
    PortBufInfo_v1 portBufInfo = PortBufInfo_v1( buf_imgi.memID,buf_imgi.virtAddr,0,buf_imgi.bufSecu, buf_imgi.bufCohe);
    IImageBufferAllocator::ImgParam imgParam = IImageBufferAllocator::ImgParam((eImgFmt_FG_BAYER10),MSize(_imgi_w_, _imgi_h_), bufStridesInBytes, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap;
    pHeap = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam,portBufInfo,true);
    srcBuffer = pHeap->createImageBuffer();
    srcBuffer->incStrong(srcBuffer);
    srcBuffer->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    printf("--- [basicP2AwithFullHD(%d)...flag -8]\n", type);
    Input src;
    src.mPortID=PORT_IMGI;
    src.mBuffer=srcBuffer;
    src.mPortID.group=0;
    frameParams.mvIn.push_back(src);
    printf("--- [basicP2AwithFullHD(%d)...push src done]\n", type);

   //crop information
    MCrpRsInfo crop;
    crop.mFrameGroup=0;
    crop.mGroupID=1;
    MCrpRsInfo crop2;
    crop2.mFrameGroup=0;
    crop2.mGroupID=2;
    MCrpRsInfo crop3;
    crop3.mFrameGroup=0;
    crop3.mGroupID=3;
    crop.mCropRect.p_fractional.x=0;
    crop.mCropRect.p_fractional.y=0;
    crop.mCropRect.p_integral.x=0;
    crop.mCropRect.p_integral.y=0;
    crop.mCropRect.s.w=_imgi_w_;
    crop.mCropRect.s.h=_imgi_h_;
    crop.mResizeDst.w=_imgi_w_;
    crop.mResizeDst.h=_imgi_h_;
    crop2.mCropRect.p_fractional.x=0;
    crop2.mCropRect.p_fractional.y=0;
    crop2.mCropRect.p_integral.x=0;
    crop2.mCropRect.p_integral.y=0;
    crop2.mCropRect.s.w=_imgi_w_;
    crop2.mCropRect.s.h=_imgi_h_;
    crop2.mResizeDst.w=_imgi_w_;
    crop2.mResizeDst.h=_imgi_h_;
    crop3.mCropRect.p_fractional.x=0;
    crop3.mCropRect.p_fractional.y=0;
    crop3.mCropRect.p_integral.x=0;
    crop3.mCropRect.p_integral.y=0;
    crop3.mCropRect.s.w=_imgi_w_;
    crop3.mCropRect.s.h=_imgi_h_;
    if(type==0)
    {
	    crop3.mResizeDst.w=_imgi_w_;
        crop3.mResizeDst.h=_imgi_h_;
    }
    else
    {
        crop3.mResizeDst.w=_imgi_h_;
        crop3.mResizeDst.h=_imgi_w_;
    }
    frameParams.mvCropRsInfo.push_back(crop);
    frameParams.mvCropRsInfo.push_back(crop2);
    frameParams.mvCropRsInfo.push_back(crop3);
    printf("--- [basicP2AwithFullHD(%d)...push crop information done\n]", type);

    //output dma
    IMEM_BUF_INFO buf_out1;
    buf_out1.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_imgi_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),
                                            MSize(_imgi_w_,_imgi_h_),  bufStridesInBytes_1, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    if(type==0)
    {
        dst.mPortID=PORT_IMG2O;
    }
    else
    {
        dst.mPortID=PORT_WDMAO;
    }
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);

    IMEM_BUF_INFO buf_out2;
    buf_out2.size=_imgi_w_*_imgi_h_*2;
    mpImemDrv->allocVirtBuf(&buf_out2);
    memset((MUINT8*)buf_out2.virtAddr, 0xffffffff, buf_out2.size);
    IImageBuffer* outBuffer_2=NULL;
    MUINT32 _out_w_, _out_h_;
    if(type==0)
    {
        _out_w_ = _imgi_w_;
        _out_h_ = _imgi_h_;
    }
    else
    {
        _out_w_ = _imgi_h_;
        _out_h_ = _imgi_w_;
    }
    MUINT32 bufStridesInBytes_2[3] = {_out_w_*2,0,0};
    PortBufInfo_v1 portBufInfo_2 = PortBufInfo_v1( buf_out2.memID,buf_out2.virtAddr,0,buf_out2.bufSecu, buf_out2.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_2 = IImageBufferAllocator::ImgParam((eImgFmt_YUY2),MSize(_out_w_,_out_h_),  bufStridesInBytes_2, bufBoundaryInBytes, 1);
    sp<ImageBufferHeap> pHeap_2 = ImageBufferHeap::create( "basicP2AwithFullHD", imgParam_2,portBufInfo_2,true);
    outBuffer_2 = pHeap_2->createImageBuffer();
    outBuffer_2->incStrong(outBuffer_2);
    outBuffer_2->lockBuf("basicP2AwithFullHD",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst_2;
    if(type==0)
    {
        dst_2.mPortID=PORT_IMG3O;
    }
    else
    {
        dst_2.mPortID=PORT_WROTO;
        dst_2.mTransform = 4;
    }
    dst_2.mBuffer=outBuffer_2;
    dst_2.mPortID.group=0;

    frameParams.mvOut.push_back(dst_2);
    enqueParams.mvFrameParams.push_back(frameParams);
    printf("--- [basicP2AwithFullHD(%d)...push dst done\n]", type);


    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);
        memset((MUINT8*)(frameParams.mvOut[1].mBuffer->getBufVA(0)), 0xffffffff, buf_out2.size);

        //buffer operation
        mpImemDrv->cacheFlushAll();
        printf("--- [basicP2AwithFullHD(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2AwithFullHD(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[basicP2AwithFullHD(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2AwithFullHD(%d)...enter while...........\n]", type);
       //while(1);


        //deque
        //wait a momet in fpga
        //usleep(5000000);
        QParams dequeParams;
        ret=pStream->deque(dequeParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [basicP2AwithFullHD(%d_%d)..deque fail\n]",type, i);
        }
        else
        {
            printf("---[basicP2AwithFullHD(%d_%d)..deque done\n]", type, i);
        }


        //dump image
        if(type==0)
        {
            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_img2o_%dx%d.yuv",(MUINT32) getpid (), type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_img3o_%dx%d.yuv",(MUINT32) getpid (),type,i, _out_w_,_out_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _out_w_ *_out_h_ * 2);
        }
        else
        {

            char filename[256];
            sprintf(filename, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_wdmao_%dx%d.yuv",(MUINT32) getpid (),type,i, _imgi_w_,_imgi_h_);
            saveBufToFile(filename, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _imgi_w_ *_imgi_h_ * 2);
            char filename2[256];
            sprintf(filename2, "/data/P2iopipe_basicP2AwithFullHD_process0x%x_type%d_package%d_wroto_%dx%d.yuv",(MUINT32) getpid (), type,i, _out_w_,_out_h_);
            saveBufToFile(filename2, reinterpret_cast<MUINT8*>(dequeParams.mvFrameParams[0].mvOut[1].mBuffer->getBufVA(0)), _out_w_ *_out_h_ * 2);
        }
        printf("--- [basicP2AwithFullHD(%d_%d)...save file done\n]", type,i);
    }

    //free
    srcBuffer->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_imgi);
    outBuffer->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_out1);
    outBuffer_2->unlockBuf("basicP2AwithFullHD");
    mpImemDrv->freeVirtBuf(&buf_out2);
    printf("--- [basicP2AwithFullHD(%d)...free memory done\n]", type);

    //
    pStream->uninit("basicP2AwithFullHD");
    pStream->destroyInstance();
    printf("--- [basicP2AwithFullHD(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;
}




#include "pic/FM/case1/depi.h"
#include "pic/FM/case1/dmgi.h"
#include "pic/FM/case1/mfbo_a.h"

MVOID P2A_FMCallback(QParams& rParams)
{
    printf("--- [P2A_FM callback func]\n");
		
    g_P2A_FMCallback = MTRUE;
}

int P2A_FM(int type,int loopNum)
{
    int ret=0;
    printf("--- [P2A_FM(%d)...enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr]\n", type);
    NSCam::NSIoPipe::NSPostProc::INormalStream* pStream;
    pStream= NSCam::NSIoPipe::NSPostProc::INormalStream::createInstance(0xFFFF);
    pStream->init("P2A_FM");
    printf("--- [P2A_FM(%d)...pStream init done]\n", type);
    IMemDrv* mpImemDrv=NULL;
    mpImemDrv=IMemDrv::createInstance();
    mpImemDrv->init();
    IMEM_BUF_INFO pTuningQueBuf;

    QParams enqueParams;
    dip_x_reg_t *pIspTuningBuffer;
    FrameParams frameParams;

    //frame tag
    frameParams.mStreamTag = NSCam::NSIoPipe::NSPostProc::ENormalStreamTag_FM;

    //input image
    //MUINT32 _mfbo_w_=1280, _mfbo_h_=720;
    MUINT32 _depi_w_=4000, _depi_h_=56;
    MUINT32 _dmgi_w_=4000, _dmgi_h_=56;
    MUINT32 _mfbo_w_=200, _mfbo_h_=56;
    MUINT32 _depi_stride_ = 4000, _dmgi_stride_ = 4000, _mfbo_stride_ = 200;

    pTuningQueBuf.size = sizeof(dip_x_reg_t);
    mpImemDrv->allocVirtBuf(&pTuningQueBuf);
    mpImemDrv->mapPhyAddr(&pTuningQueBuf);
    printf("Tuning buffer VA address %lu \n",pTuningQueBuf.virtAddr);

    //input dma
    IMEM_BUF_INFO buf_depi;
    buf_depi.size=sizeof(g_depi_array);
    mpImemDrv->allocVirtBuf(&buf_depi);
    memcpy( (MUINT8*)(buf_depi.virtAddr), (MUINT8*)(g_depi_array), buf_depi.size);
    IImageBuffer* inDepiBuffer=NULL;
    MUINT32 depi_bufStridesInBytes[3] = {_depi_w_,0,0};
    PortBufInfo_v1 depi_portBufInfo = PortBufInfo_v1( buf_depi.memID,buf_depi.virtAddr,0,buf_depi.bufSecu, buf_depi.bufCohe);
    IImageBufferAllocator::ImgParam depi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_depi_w_,_depi_h_),  depi_bufStridesInBytes, depi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdepiHeap = ImageBufferHeap::create( "P2A_FM", depi_imgParam,depi_portBufInfo,true);
    inDepiBuffer = pdepiHeap->createImageBuffer();
    inDepiBuffer->incStrong(inDepiBuffer);
    inDepiBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input depi_input;
    depi_input.mPortID=PORT_DEPI;
    depi_input.mBuffer=inDepiBuffer;
    depi_input.mPortID.group=0;
    frameParams.mvIn.push_back(depi_input);   

    IMEM_BUF_INFO buf_dmgi;
    buf_dmgi.size=sizeof(g_dmgi_array);
    mpImemDrv->allocVirtBuf(&buf_dmgi);
    memcpy( (MUINT8*)(buf_dmgi.virtAddr), (MUINT8*)(g_dmgi_array), buf_dmgi.size);
    IImageBuffer* inDmgiBuffer=NULL;
    MUINT32 dmgi_bufStridesInBytes[3] = {_dmgi_w_,0,0};
    PortBufInfo_v1 dmgi_portBufInfo = PortBufInfo_v1( buf_dmgi.memID,buf_dmgi.virtAddr,0,buf_dmgi.bufSecu, buf_dmgi.bufCohe);
    IImageBufferAllocator::ImgParam dmgi_imgParam = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_dmgi_w_,_dmgi_h_),  dmgi_bufStridesInBytes, dmgi_bufStridesInBytes, 1);
    sp<ImageBufferHeap> pdmgiHeap = ImageBufferHeap::create( "P2A_FM", dmgi_imgParam,dmgi_portBufInfo,true);
    inDmgiBuffer = pdmgiHeap->createImageBuffer();
    inDmgiBuffer->incStrong(inDmgiBuffer);
    inDmgiBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Input dmgi_input;
    dmgi_input.mPortID=PORT_DMGI;
    dmgi_input.mBuffer=inDmgiBuffer;
    dmgi_input.mPortID.group=0;
    frameParams.mvIn.push_back(dmgi_input);  


    IMEM_BUF_INFO buf_out1;
    buf_out1.size=1280*720*2;
    mpImemDrv->allocVirtBuf(&buf_out1);
    memset((MUINT8*)buf_out1.virtAddr, 0xffffffff, buf_out1.size);
    IImageBuffer* outBuffer=NULL;
    MUINT32 bufStridesInBytes_1[3] = {_mfbo_stride_,0,0};
    PortBufInfo_v1 portBufInfo_1 = PortBufInfo_v1( buf_out1.memID,buf_out1.virtAddr,0,buf_out1.bufSecu, buf_out1.bufCohe);
    IImageBufferAllocator::ImgParam imgParam_1 = IImageBufferAllocator::ImgParam((eImgFmt_STA_BYTE),
                                            MSize(_mfbo_w_,_mfbo_h_),  bufStridesInBytes_1, bufStridesInBytes_1, 1);
    sp<ImageBufferHeap> pHeap_1 = ImageBufferHeap::create( "P2A_FM", imgParam_1,portBufInfo_1,true);
    outBuffer = pHeap_1->createImageBuffer();
    outBuffer->incStrong(outBuffer);
    outBuffer->lockBuf("P2A_FM",eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
    Output dst;
    dst.mPortID=PORT_MFBO;
    dst.mBuffer=outBuffer;
    dst.mPortID.group=0;
    frameParams.mvOut.push_back(dst);   
    printf("--- [P2A_FM(%d)...push dst done\n]", type);

    //Tuning Value Setting
    pIspTuningBuffer = (dip_x_reg_t *)pTuningQueBuf.virtAddr;
    pIspTuningBuffer->DIP_X_CTL_YUV2_EN.Raw=0x00000001;
    pIspTuningBuffer->DIP_X_FM_SIZE.Raw=0x38640F09;
    frameParams.mTuningData = (MVOID*)(pIspTuningBuffer);
#if 1
    enqueParams.mvFrameParams.push_back(frameParams);
    g_P2A_FMCallback = MFALSE;
    enqueParams.mpfnCallback = P2A_FMCallback;

    for(int i=0;i<loopNum;i++)
    {
        memset((MUINT8*)(frameParams.mvOut[0].mBuffer->getBufVA(0)), 0xffffffff, buf_out1.size);

        //buffer operation
        //mpImemDrv->cacheFlushAll();
        //printf("--- [basicP2A(%d_%d)...flush done\n]", type, i);

        //enque
        ret=pStream->enque(enqueParams);
        if(!ret)
        {
            printf("---ERRRRRRRRR [P2A_FM(%d_%d)..enque fail\n]", type, i);
        }
        else
        {
            printf("---[P2A_FM(%d_%d)..enque done\n]",type, i);
        }

        //temp use while to observe in CVD
        //printf("--- [basicP2A(%d)...enter while...........\n]", type);
        //while(1);


        //deque
        //wait a momet in fpga
        //usleep(40000000);

        //fgets();
        //char buf[4096];
        //fgets(buf, sizeof(buf), stdin);

        //printf("--- [P2A_FM(%d)...enter while...........\n]", type);
        //fgets();
        //getchar();
        
        //while(1);
        do{
            usleep(100000);
            if (MTRUE == g_P2A_FMCallback)
            {
                break;
            }
        }while(1);
        g_P2A_FMCallback = MFALSE;

        //QParams dequeParams;
        //ret=pStream->deque(dequeParams);
        //if(!ret)
        //{
        //    printf("---ERRRRRRRRR [P2A_FM(%d_%d)..deque fail\n]",type, i);
        //}
        //else
        {
            printf("---[P2A_FM(%d_%d)..deque done\n]", type, i);
        }

#if 1
        //dump image

        char filename[256];
        sprintf(filename, "/data/P2iopipe_P2A_FM_process0x%x_type%d_package%d_mfbo_%dx%d.yuv",(MUINT32) getpid (), type,i, _mfbo_w_,_mfbo_h_);
        saveBufToFile(filename, reinterpret_cast<MUINT8*>(enqueParams.mvFrameParams[0].mvOut[0].mBuffer->getBufVA(0)), _mfbo_w_ *_mfbo_h_ * 2);


#endif
        printf("--- [P2A_FM(%d_%d)...save file done\n]", type,i);
    }
#endif
    //free
    inDepiBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_depi);
    inDmgiBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_dmgi);

    outBuffer->unlockBuf("P2A_FM");
    mpImemDrv->freeVirtBuf(&buf_out1);
    printf("--- [P2A_FM(%d)...free memory done\n]", type);

    //
    pStream->uninit("P2A_FM");
    pStream->destroyInstance();
    printf("--- [P2A_FM(%d)...pStream uninit done\n]", type);
    mpImemDrv->uninit();
    mpImemDrv->destroyInstance();

    return ret;


}
