using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSIoPipe;
using namespace NSOwe;


#define MEM_PREPARE_COPY(buf, src_buf_addr, src_size)\
IMEM_BUF_INFO buf; \
buf.size=src_size; \
printf(#buf".size:%d",buf.size); \
mpImemDrv->allocVirtBuf(&buf); \
mpImemDrv->mapPhyAddr(&buf); \
memcpy( (MUINT8*)(buf.virtAddr), (MUINT8*)(src_buf_addr), buf.size); \
mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf);

#define MEM_PREPARE_SET(buf, val, src_size) \
IMEM_BUF_INFO buf; \
buf.size=src_size; \
printf(#buf".size:%d",buf.size); \
mpImemDrv->allocVirtBuf(&buf); \
mpImemDrv->mapPhyAddr(&buf); \
memset( (MUINT8*)(buf.virtAddr), val, buf.size); \
mpImemDrv->cacheSyncbyRange(IMEM_CACHECTRL_ENUM_FLUSH, &buf);


#define FIELD(REG, MASK, SHIFT) (((REG)&(MASK))>>SHIFT)
#define ARRAY_SIZE(B) (sizeof(B)/sizeof(B[0]))
#define min(X,Y) ((X) < (Y) ? (X) : (Y))

/* Engine Size Math */
#define WDMA_XSIZE(X, H_CROP_E, H_CROP_S, SKIP) ((min((H_CROP_E + 1), X) - H_CROP_S) / (SKIP + 1))
#define WDMA_YSIZE(Y, V_CROP_E, V_CROP_S) (min((V_CROP_E + 1), Y) - V_CROP_S)


/* WMFE MACROS */
#define WMFE_ENABLE 0x1
#define WMFE_ENABLE_0 0x1
#define WMFE_ENABLE_1 0x1
#define WMFE_ENABLE_2 0x1
#define WMFE_MASK_EN_0 0x1
#define WMFE_MASK_EN_1 0x1
#define WMFE_MASK_EN_2 0x0

extern int owe_default();
