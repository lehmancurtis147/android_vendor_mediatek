extern char frame_4_dpe_wmf_dpi_frame_00_00_0[];
#define frame_4_dpe_wmf_dpi_frame_00_00_0_size 32400
extern char* frame_4_in_dpe_wmf_dpi_frame_0;

extern char frame_4_dpe_wmf_dpi_frame_00_00_1[];
#define frame_4_dpe_wmf_dpi_frame_00_00_1_size 32400
extern char* frame_4_in_dpe_wmf_dpi_frame_1;

extern char frame_4_dpe_wmf_dpi_frame_00_00_2[];
#define frame_4_dpe_wmf_dpi_frame_00_00_2_size 32400
extern char* frame_4_in_dpe_wmf_dpi_frame_2;

extern char frame_4_dpe_wmf_dpi_frame_00_00_3[];
#define frame_4_dpe_wmf_dpi_frame_00_00_3_size 259200
extern char frame_4_dpe_wmf_dpi_frame_00_00_4[];
#define frame_4_dpe_wmf_dpi_frame_00_00_4_size 32400
extern char frame_4_dpe_wmf_dpo_frame_00_00_0[];
#define frame_4_dpe_wmf_dpo_frame_00_00_0_size 32400
extern unsigned int frame_4_golden_dpe_wmf_dpo_0_size;
extern char* frame_4_golden_dpe_wmf_dpo_frame_0;

extern char frame_4_dpe_wmf_dpo_frame_00_00_1[];
#define frame_4_dpe_wmf_dpo_frame_00_00_1_size 32400
extern unsigned int frame_4_golden_dpe_wmf_dpo_1_size;
extern char* frame_4_golden_dpe_wmf_dpo_frame_1;

extern char frame_4_dpe_wmf_dpo_frame_00_00_2[];
#define frame_4_dpe_wmf_dpo_frame_00_00_2_size 32400
extern unsigned int frame_4_golden_dpe_wmf_dpo_2_size;
extern char* frame_4_golden_dpe_wmf_dpo_frame_2;

extern char frame_4_dpe_wmf_dpo_frame_00_00_3[];
#define frame_4_dpe_wmf_dpo_frame_00_00_3_size 129600
extern char frame_4_dpe_wmf_dpo_frame_00_00_4[];
#define frame_4_dpe_wmf_dpo_frame_00_00_4_size 129600
extern char frame_4_dpe_wmf_imgi_frame_00_00_0[];
#define frame_4_dpe_wmf_imgi_frame_00_00_0_size 64800
extern char* frame_4_in_dpe_wmf_imgi_frame_0;

extern char frame_4_dpe_wmf_imgi_frame_00_00_1[];
#define frame_4_dpe_wmf_imgi_frame_00_00_1_size 64800
extern char* frame_4_in_dpe_wmf_imgi_frame_1;

extern char frame_4_dpe_wmf_imgi_frame_00_00_2[];
#define frame_4_dpe_wmf_imgi_frame_00_00_2_size 64800
extern char* frame_4_in_dpe_wmf_imgi_frame_2;

extern char frame_4_dpe_wmf_imgi_frame_00_00_3[];
#define frame_4_dpe_wmf_imgi_frame_00_00_3_size 259200
extern char frame_4_dpe_wmf_imgi_frame_00_00_4[];
#define frame_4_dpe_wmf_imgi_frame_00_00_4_size 259200
extern char frame_4_dpe_wmf_maski_frame_00_00_0[];
#define frame_4_dpe_wmf_maski_frame_00_00_0_size 32400
extern char* frame_4_in_dpe_wmf_maski_frame_0;

extern char frame_4_dpe_wmf_maski_frame_00_00_1[];
#define frame_4_dpe_wmf_maski_frame_00_00_1_size 32400
extern char* frame_4_in_dpe_wmf_maski_frame_1;

extern char frame_4_dpe_wmf_tbli_frame_00_00_0[];
#define frame_4_dpe_wmf_tbli_frame_00_00_0_size 256
extern char* frame_4_in_dpe_wmf_tbli_frame_0;

extern char frame_4_dpe_wmf_tbli_frame_00_00_1[];
#define frame_4_dpe_wmf_tbli_frame_00_00_1_size 256
extern char* frame_4_in_dpe_wmf_tbli_frame_1;

extern char frame_4_dpe_wmf_tbli_frame_00_00_2[];
#define frame_4_dpe_wmf_tbli_frame_00_00_2_size 256
extern char* frame_4_in_dpe_wmf_tbli_frame_2;

extern char frame_4_dpe_wmf_tbli_frame_00_00_3[];
#define frame_4_dpe_wmf_tbli_frame_00_00_3_size 256
extern char frame_4_dpe_wmf_tbli_frame_00_00_4[];
#define frame_4_dpe_wmf_tbli_frame_00_00_4_size 256
extern void getframe_4GoldPointer(
	unsigned long* golden_dpe_dvo_l_frame,
	unsigned long* golden_dpe_dvo_r_frame,
	unsigned long* golden_dpe_confo_l_frame,
	unsigned long* golden_dpe_confo_r_frame,
	unsigned long* golden_dpe_respo_l_frame,
	unsigned long* golden_dpe_respo_r_frame,
	unsigned long* golden_dpe_wmf_dpo_frame_0,
	unsigned long* golden_dpe_wmf_dpo_frame_1,
	unsigned long* golden_dpe_wmf_dpo_frame_2
);
