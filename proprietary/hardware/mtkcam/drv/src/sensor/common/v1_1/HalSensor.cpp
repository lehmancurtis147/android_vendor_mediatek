/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/HalSensor"

#include "MyUtils.h"
#ifdef USING_MTK_LDVT
#include "uvvf.h"
#endif
#include <mtkcam/def/common.h>

#include <string.h>
#include <cutils/properties.h>
#include <fcntl.h>
#include <sys/ioctl.h>

/******************************************************************************
 *
 ******************************************************************************/
#ifndef USING_MTK_LDVT
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)
#else
#define MY_LOGV(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        VV_MSG("[%s] " fmt, __FUNCTION__, ##arg)
#endif

#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

//hwsync drv
#include <mtkcam/drv/IHwSyncDrv.h>
HWSyncDrv* mpHwSyncDrv=NULL;

//judge boot mode
static MUINT32 get_boot_mode(void)
{
    MINT32 fd;
    size_t s;
    char boot_mode[4] = {'0'};
    //MT_NORMAL_BOOT 0 , MT_META_BOOT 1, MT_RECOVERY_BOOT 2, MT_SW_REBOOT 3
    //MT_FACTORY_BOOT 4, MT_ADVMETA_BOOT 5
    fd = open("/sys/class/BOOT/BOOT/boot/boot_mode", O_RDONLY);
    if (fd < 0)
    {
        MY_LOGW("fail to open: %s\n", "/sys/class/BOOT/BOOT/boot/boot_mode");
        return 0;
    }

    s = read(fd, (void *)&boot_mode, sizeof(boot_mode) - 1);
    close(fd);

    if(s <= 0)
    {
        MY_LOGW("could not read boot mode sys file\n");
        return 0;
    }

    boot_mode[s] = '\0';
    MY_LOGD("Boot Mode %d\n",atoi(boot_mode));
    return atoi(boot_mode);
}

/******************************************************************************
 *
 ******************************************************************************/
HalSensor::
PerData::
PerData()
{
}

/******************************************************************************
 *
 ******************************************************************************/
HalSensor::
~HalSensor()
{
}

/******************************************************************************
 *
 ******************************************************************************/
HalSensor::
HalSensor()
    : IHalSensor(),
    mMutex(),
    mSensorDataMap(),
    mSensorIdx(IMGSENSOR_SENSOR_IDX_NONE),
    mScenarioId(0),
    mHdrMode(0),
    mPdafMode(0),
    mFramerate(0)
{
    memset(&mSensorDynamicInfo, 0, sizeof(SensorDynamicInfo));
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
HalSensor::
destroyInstance(
    char const* szCallerName
)
{
    HalSensorList::singleton()->closeSensor(this, szCallerName);
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
HalSensor::
onDestroy()
{
    MY_LOGD("#Sensor:%zu", mSensorDataMap.size());

    Mutex::Autolock _l(mMutex);

    if (mSensorIdx == IMGSENSOR_SENSOR_IDX_NONE) {
        mSensorDataMap.clear();
    } else {
        MY_LOGI("Forget to powerOff before destroying. mSensorIdx:%d", mSensorIdx);
    }
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HalSensor::
onCreate(
    SortedVector<MUINT>const& vSensorIndex
)
{
    MY_LOGD("+ #Sensor:%zu", vSensorIndex.size());

    Mutex::Autolock _l(mMutex);

    mSensorDataMap.clear();
    mSensorDataMap.setCapacity(vSensorIndex.size());
    for (MUINT i = 0; i < vSensorIndex.size(); i++)
    {
        MUINT const uSensorIndex = vSensorIndex[i];

        sp<PerData> pData = new PerData;
        mSensorDataMap.add(uSensorIndex, pData);
    }

    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HalSensor::
isMatch(SortedVector<MUINT>const& vSensorIndex) const
{
    if  (vSensorIndex.size() != mSensorDataMap.size())
    {
        //MY_LOGD("isMatch vSensorIndex:%d, mSensorDataMap:%d\n", vSensorIndex.size(),mSensorDataMap.size());//ToDo: remove
        return  MFALSE;
    }

    for (MUINT i = 0; i < vSensorIndex.size(); i++)
    {
        if  ( vSensorIndex[i] != mSensorDataMap.keyAt(i) )
        {
            //MY_LOGD("isMatch vSensorIndex[i]:%d, mSensorDataMap[i]:%d\n", vSensorIndex[i],mSensorDataMap.keyAt(i));//ToDo:remove
            return  MFALSE;
        }
    }

    return  MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HalSensor::
powerOn(
    char  const *szCallerName,
    MUINT const  uCountOfIndex,
    MUINT const *pArrayOfIndex
)
{
    IMGSENSOR_SENSOR_IDX sensorIdx = (IMGSENSOR_SENSOR_IDX)HalSensorList::singleton()->queryEnumInfoByIndex(*pArrayOfIndex)->getDeviceId();
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    ImgSensorDrv *pSensorDrv = ImgSensorDrv::getInstance(sensorIdx);

#ifdef CONFIG_MTK_CAM_SECURE
    MINT32 u32Enable = 0;
#endif

    (void)uCountOfIndex;

    Mutex::Autolock _l(mMutex);

    MY_LOGD("sensorIdx : %d\n ", sensorIdx);

    if (mSensorIdx != IMGSENSOR_SENSOR_IDX_NONE) {
         MY_LOGD("sensorIdx(0x%x) is already powerOn %x\n ", sensorIdx, mSensorIdx);
         return MTRUE;
    }

    if (pSeninfDrv->init() < 0) {
        MY_LOGE("pSeninfDrv->init() fail ");
        return MFALSE;
    }

    if (pSensorDrv->init(sensorIdx) < 0) {
        MY_LOGE("pSensorDrv->init() fail ");
        return MFALSE;
    }

    if (setSensorMclk(sensorIdx, 1) < 0) {
        MY_LOGE("setSensorMclk fail n");
        return MFALSE;
    }

    if (setSensorMclkDrivingCurrent(sensorIdx) < 0) {
        MY_LOGE("initial IO driving current fail ");
        return MFALSE;
    }

#ifdef CONFIG_MTK_CAM_SECURE
      if(HalSensorList::singleton()->querySecureState()) {
        u32Enable = 1;
        pSensorDrv->sendCommand(CMD_SENSOR_OPEN_SECURE_SESSION);
        pSensorDrv->sendCommand(CMD_SENSOR_SET_AS_SECURE_DRIVER, (MUINTPTR)&u32Enable);
    } else {
        u32Enable = 0;
        //pSensorDrv->sendCommand(CMD_SENSOR_CLOSE_SECURE_SESSION);
        pSensorDrv->sendCommand(CMD_SENSOR_SET_AS_SECURE_DRIVER, (MUINTPTR)&u32Enable);
    }
#endif

    MBOOL ret = MTRUE;
    // Open sensor, try to open 3 time
    for (int j =0; j < 3; j++) {
        if ((ret = pSensorDrv->open()) < 0) {
            MY_LOGE("pSensorDrv->open fail, retry = %d ", j);
        }
        else {
            break;
        }
    }

    if (ret < 0) {
        MY_LOGE("pSensorDrv->open fail");

#ifdef CONFIG_MTK_CAM_SECURE
        if(HalSensorList::singleton()->querySecureState()) {
            MINT32 u32Enable = 0;
            pSensorDrv->sendCommand(CMD_SENSOR_CLOSE_SECURE_SESSION);
            pSensorDrv->sendCommand(CMD_SENSOR_SET_AS_SECURE_DRIVER, (MUINTPTR)&u32Enable);
        }
#endif

        return MFALSE;
    }

    mSensorIdx = sensorIdx;

    //hwsync driver, create instance and do init
    switch(get_boot_mode())
    {
        case 1:
        case 4:
            break;
        default:
            mpHwSyncDrv=HWSyncDrv::createInstance();
            mpHwSyncDrv->init(HW_SYNC_USER_HALSENSOR, *pArrayOfIndex);
            break;
    }

    MY_LOGD("- <%s> mSensorIdx = 0x%x\n", szCallerName, mSensorIdx);

    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL
HalSensor::
powerOff(
    char  const *szCallerName,
    MUINT const  uCountOfIndex,
    MUINT const *pArrayOfIndex
)
{
    IMGSENSOR_SENSOR_IDX sensorIdx = (IMGSENSOR_SENSOR_IDX)HalSensorList::singleton()->queryEnumInfoByIndex(*pArrayOfIndex)->getDeviceId();
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    ImgSensorDrv *pSensorDrv;
    SensorDynamicInfo *pSensorDynamicInfo = &mSensorDynamicInfo;

    (void)uCountOfIndex;

    Mutex::Autolock _l(mMutex);

    if (mSensorIdx == IMGSENSOR_SENSOR_IDX_NONE || mSensorIdx != sensorIdx) {
        MY_LOGE("<%s> power off fail. mSensorIdx = %d, sensorIdx = %d\n", szCallerName, mSensorIdx, sensorIdx);
        return MFALSE;
    }

    MY_LOGD("mSensorIdx : %d\n ", mSensorIdx);

    //hwsync driver, do uninit and destroy instance
    switch(get_boot_mode()) {
    case 1:
    case 4:
        break;
    default:
        if (mpHwSyncDrv)
        {
            mpHwSyncDrv->uninit(HW_SYNC_USER_HALSENSOR, *pArrayOfIndex);
            mpHwSyncDrv->destroyInstance();
        }
        break;
    }

    pSensorDrv = ImgSensorDrv::getInstance(mSensorIdx);

    if (pSensorDrv) {
        if ((pSensorDrv->close()) < 0) {
            MY_LOGE("pSensorDrv->close fail");
            return MFALSE;
        }

#ifdef CONFIG_MTK_CAM_SECURE
    if(HalSensorList::singleton()->querySecureState()) {
        pSensorDrv->sendCommand(CMD_SENSOR_CLOSE_SECURE_SESSION);
        int u32Enable = 0;
        pSensorDrv->sendCommand(CMD_SENSOR_SET_AS_SECURE_DRIVER, (MUINTPTR)&u32Enable);
    }
#endif

        if(pSensorDrv->uninit() < 0) {
            MY_LOGE("pSensorDrv->uninit fail");
            return MFALSE;
        }
    } else {
        MY_LOGE("pSensorDrv is NULL");
        return MFALSE;
    }

    /*seninf mux disable*/
    pSensorDynamicInfo->TgInfo   =
    pSensorDynamicInfo->HDRInfo  =
    pSensorDynamicInfo->PDAFInfo = CAM_TG_NONE;
    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);
    seninfMUXReleaseAll(port);

    /* Disable Sensor interface Clock divider*/
    if(setSensorMclk(mSensorIdx, false) < 0) {
        MY_LOGE("setSensorMclk fail n");
        return MFALSE;
    }

    MY_LOGD("- <%s> mSensorIdx = 0x%x\n", szCallerName, mSensorIdx);
    mSensorIdx = IMGSENSOR_SENSOR_IDX_NONE;

    SENINF_CSI_MIPI csiMipi;
    csiMipi.pCsiInfo = pSeninfDrv->getCSIInfo(port);

    pSeninfDrv->powerOff((void *)&csiMipi);

    if (pSeninfDrv->uninit() < 0) {
        MY_LOGE("pSeninfDrv->uninit fail");
        return MFALSE;
    }

    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL HalSensor::querySensorDynamicInfo(
   MUINT32 indexDual,
   SensorDynamicInfo *pSensorDynamicInfo
)
{
    (void)indexDual;
    memcpy(pSensorDynamicInfo, &mSensorDynamicInfo, sizeof(SensorDynamicInfo));
    return MTRUE;
}

/******************************************************************************
 *
 ******************************************************************************/
MBOOL HalSensor::configure(
    MUINT const         uCountOfParam,
    ConfigParam const  *pConfigParam
)
{
    MINT32 ret = MFALSE;
    HALSENSOR_SENINF_CSI csi;
    IMGSENSOR_SENSOR_IDX sensorIdx = (IMGSENSOR_SENSOR_IDX)HalSensorList::singleton()->queryEnumInfoByIndex(pConfigParam->index)->getDeviceId();

    (void)uCountOfParam;

    Mutex::Autolock _l(mMutex);

    if (mSensorIdx == IMGSENSOR_SENSOR_IDX_NONE || mSensorIdx != sensorIdx) {
        MY_LOGE("configure fail. mSensorIdx = %d, sensorIdx = %d\n", mSensorIdx, sensorIdx);
        return MFALSE;
    }

    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(mSensorIdx);
    SeninfDrv    *const pSeninfDrv = SeninfDrv::getInstance();

    pSensorDrv->sendCommand(CMD_SENSOR_SET_STREAMING_SUSPEND);

    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);
    csi.pCsiInfo = pSeninfDrv->getCSIInfo(port);
    MY_LOGD("CSI2_IP = %d seninfSrc = %d srcType = %d\n", csi.pCsiInfo->port, csi.pCsiInfo->seninf, csi.pCsiInfo->srcType);

    csi.pConfigParam = pConfigParam;
    csi.pInfo = &(pSensorDrv->getDrvInfo()->info);
    csi.sensorIdx = mSensorIdx;

    switch (csi.pCsiInfo->srcType) {
    case MIPI_SENSOR:
        ret = configureMipi(csi);
        break;
    case SERIAL_SENSOR:
        ret = configureSerial(csi);
        break;
    case PARALLEL_SENSOR:
        ret = configureParallel(csi);
        break;
    default:
        break;
    }

    if (pConfigParam->HDRMode) { // vHDR mode
        MUINT hdr_lv = 0xffffffff;
        if (pConfigParam->exposureTime > 0 && pConfigParam->exposureTime_se > 0 && pConfigParam->gain > 0) {
            if (pConfigParam->HDRMode == SENSOR_VHDR_MODE_ZVHDR && pConfigParam->gain_se > 0) { // ZVHDR mode: needs dual gain
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_HDR_SHUTTER, (MUINTPTR)&(pConfigParam->exposureTime), (MUINTPTR)&(pConfigParam->exposureTime_se), (MUINTPTR)&(hdr_lv));
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_SENSOR_DUAL_GAIN, (MUINTPTR)&(pConfigParam->gain), (MUINTPTR)&(pConfigParam->gain_se));
            }  else if(csi.pInfo->HDR_Support == MVHDR_SUPPORT_MultiCAMSV) { //3HDR mode: needs triple shutter/gain
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_HDR_TRI_SHUTTER, (MUINTPTR)&(pConfigParam->exposureTime), (MUINTPTR)&(pConfigParam->exposureTime_me), (MUINTPTR)&(pConfigParam->exposureTime_se));
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_HDR_TRI_GAIN, (MUINTPTR)&(pConfigParam->gain), (MUINTPTR)&(pConfigParam->gain_me), (MUINTPTR)&(pConfigParam->gain_se));
            } else {
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_HDR_SHUTTER, (MUINTPTR)&(pConfigParam->exposureTime), (MUINTPTR)&(pConfigParam->exposureTime_se), (MUINTPTR)&(hdr_lv));
                ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_SENSOR_GAIN, (MUINTPTR)&(pConfigParam->gain));
            }
        }
    } else { // normal mode
        if (pConfigParam->exposureTime > 0 && pConfigParam->gain > 0) {
            ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_SENSOR_EXP_TIME, (MUINTPTR)&(pConfigParam->exposureTime));
            ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_SENSOR_GAIN, (MUINTPTR)&(pConfigParam->gain));
        }
    }

    return (ret == 0);
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::seninfMUXReleaseAll(CUSTOM_CFG_CSI_PORT port)
{
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

    for(int i = SENINF_MUX1; i < SENINF_MUX_NUM; i++)
        if(pSeninfDrv->getSeninfTopMuxCtrl((SENINF_MUX_ENUM)i) == pSeninfDrv->getCSIInfo(port)->seninf && pSeninfDrv->isMUXUsed((SENINF_MUX_ENUM)i))
            pSeninfDrv->disableMUX((SENINF_MUX_ENUM)i);

    return 0;
}

/******************************************************************************
 *
 ******************************************************************************/
SENINF_MUX_ENUM HalSensor::seninfMUXArbitration(SENINF_MUX_ENUM searchStart, SENINF_MUX_ENUM searchEnd)
{
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

    MINT i = searchStart;

    while(i < searchEnd) {
        if(!pSeninfDrv->isMUXUsed((SENINF_MUX_ENUM)i))
            break;
        i++;
    }

    if(i == searchEnd) {
        MY_LOGE("No free MUX for CAM\n");
        i = SENINF_MUX_ERROR;
    }

    return (SENINF_MUX_ENUM)i;
}

/******************************************************************************
 *
 ******************************************************************************/
#ifdef HALSENSOR_AUTO_DESKEW
MINT HalSensor::setAutoDeskew(IMGSENSOR_SENSOR_IDX sensorIdx, MUINT32 cropWidth, MUINT32 cropHeight, MUINT32 frameRate, MUINT8 u1MIPILaneNum, TG_FORMAT_ENUM rawSensorBit)
{
    MINT ret = 0;

    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(sensorIdx);

    ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_STREAMING_RESUME);

#define MAIN_CSI_AUTO_DESKEW_THRESHOLD 1427*1000000 /*5642x4224x240x10= 1427_374080(1.5Gbps)*/
#define SUB_CSI_AUTO_DESKEW_THRESHOLD 1427*1000000
#define CSI_BLANKING_RATIO 1000
    int bpp;

    switch(rawSensorBit) {
    case RAW_8BIT_FMT:
        bpp = 8;
        break;

    case RAW_10BIT_FMT:
    case RAW_12BIT_FMT:
    case RAW_14BIT_FMT:
        bpp = 10;
        break;

    case YUV422_FMT:
        bpp = 16;
        break;

    default:
        bpp = 10;
        break;
    }

    MY_LOGD("cropWidth %d, cropHeight %d,frameRate %d,u1MIPILaneNum %d,CSI_BLANKING_RATIO %d\n",
    cropWidth, cropHeight, frameRate, u1MIPILaneNum+1, CSI_BLANKING_RATIO);
    MY_LOGD("main lane speed = %ld, auto deskew threshold = %ld\n",
    ((((cropWidth * cropHeight)/1000) * (frameRate/10)*bpp)/(u1MIPILaneNum+1)),
    (MAIN_CSI_AUTO_DESKEW_THRESHOLD/CSI_BLANKING_RATIO));

    if(((((cropWidth * cropHeight)/1000) * (frameRate/10)*bpp)/(u1MIPILaneNum+1))
        > (MAIN_CSI_AUTO_DESKEW_THRESHOLD/1000)){

        //Main Cam over 1.5Gbps should enable deskew funciton
    }

    //SW Deskew Function
#ifdef SW_DESKEW
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    pSeninfDrv->autoDeskewCalibrationSeninf(sensorIdx, pSensorPara->u1MIPILaneNum+1);
#endif
    ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_STREAMING_SUSPEND);

    return ret;
}
#endif

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::configureMipi(HALSENSOR_SENINF_CSI &csi)
{
    int i;
    MINT32 ret = 0;
    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(csi.sensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

    ACDK_SENSOR_INFO_STRUCT *pInfo        = csi.pInfo;
    const ConfigParam       *pConfigParam = csi.pConfigParam;

    SENINF_CSI_MIPI csiMipi;
    MUINT32 hdrMode = 0, pdafMode = 0, framerate = 0;

    if (pInfo->SensroInterfaceType != SENSOR_INTERFACE_TYPE_MIPI)
        MY_LOGW("Sensor type doesn't match CSI port SensroInterfaceType = %d, srcType = %d\n", pInfo->SensroInterfaceType, csi.pCsiInfo->srcType);

    /*0: NO PDAF, 1: PDAF Raw Data mode, 2:PDAF VC mode(HDR), 3:PDAF VC mode(Binning), 4: PDAF DualPD Raw Data mode, 5: PDAF DualPD VC mode*/
    pSensorDrv->sendCommand(CMD_SENSOR_GET_SENSOR_PDAF_CAPACITY, (MUINTPTR)&(pConfigParam->scenarioId), (MUINTPTR)&pdafMode);
    MY_LOGD("PDAF on/off = %d, PDAF support mode = %d\n", pdafMode, pInfo->PDAF_Support);

    hdrMode = pConfigParam->HDRMode;

    char value[PROPERTY_VALUE_MAX] = {'\0'};

    //Test Mode use property parameter
    {
        property_get("vendor.debug.senif.hdrmode", value, "0");
        int hdrModeTest = atoi(value);
        if(hdrModeTest == 1 || hdrModeTest == 2 || hdrModeTest == 9)
        {
            hdrMode = hdrModeTest;
        }
    }
    //PDAFTest Mode use property parameter
    {
        property_get("vendor.debug.senif.pdafmode", value, "0");
        int PDAFModeTest = atoi(value);
        if(PDAFModeTest == 1)
        {
            pdafMode = 1;
        }
    }

    MY_LOGD("SenDev=%d, scenario=%d, HDR=%d\n",
        csi.sensorIdx,
        pConfigParam->scenarioId,
        hdrMode);

    csiMipi.enable          = 1;
    csiMipi.dataheaderOrder = pInfo->SensorPacketECCOrder;
    csiMipi.dlaneNum        = pInfo->SensorMIPILaneNumber;
    csiMipi.csi_type        = (pInfo->MIPIsensorType == MIPI_CPHY) ? CSI2_2_5G_CPHY : CSI2_2_5G;
    csiMipi.pCsiInfo        = csi.pCsiInfo;

    MUINT8 *pDPCMType = &pInfo->IMGSENSOR_DPCM_TYPE_PRE;
    csiMipi.dpcm = (pConfigParam->scenarioId > SENSOR_SCENARIO_ID_SLIM_VIDEO2) ?
                    pInfo->IMGSENSOR_DPCM_TYPE_PRE :
                    pDPCMType[pConfigParam->scenarioId];

    MUINT32           inDataFmt = 0;
    TG_FORMAT_ENUM    inDataType;

    ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_INPUT_BIT_ORDER, (MUINTPTR)&inDataFmt);

    // Source is from sensor
    switch(pSensorDrv->getType()) {
        case IMAGE_SENSOR_TYPE_RAW: {
            // RAW
            csiMipi.padSel = PAD_10BIT;//pad2cam_data_sel
            inDataType = RAW_10BIT_FMT;//cam_tg_input_fmt
            break;
        }

        case IMAGE_SENSOR_TYPE_RAW8: {
            // RAW
            csiMipi.padSel = (inDataFmt) ? PAD_8BIT_7_0 : PAD_8BIT_9_2;
            inDataType = RAW_8BIT_FMT;
            break;
        }

        case IMAGE_SENSOR_TYPE_RAW12: {
            // RAW
            csiMipi.padSel = PAD_10BIT;
            inDataType = RAW_12BIT_FMT;
            break;
        }

        case IMAGE_SENSOR_TYPE_YUV:
        case IMAGE_SENSOR_TYPE_YCBCR: {
            // Yuv422 or YCbCr
            csiMipi.padSel = (inDataFmt) ? PAD_8BIT_7_0 : PAD_8BIT_9_2;
            inDataType = YUV422_FMT;
            break;
        }

        case IMAGE_SENSOR_TYPE_RGB565: {
            // RGB565
            csiMipi.padSel = (inDataFmt) ? PAD_8BIT_7_0 : PAD_8BIT_9_2;
            inDataType = RGB565_MIPI_FMT;
            break;
        }

        case IMAGE_SENSOR_TYPE_RGB888: {
            // RGB888
            csiMipi.padSel = (inDataFmt) ? PAD_8BIT_7_0 : PAD_8BIT_9_2;
            inDataType = RGB888_MIPI_FMT;
            break;
        }

        case IMAGE_SENSOR_TYPE_JPEG: {
            csiMipi.padSel = (inDataFmt) ? PAD_8BIT_7_0 : PAD_8BIT_9_2;
            inDataType = JPEG_FMT;
            break;
        }

        default:
            break;
    }

    SENSOR_VC_INFO_STRUCT vcInfo;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_SENSOR_VC_INFO, (MUINTPTR)&vcInfo, (MUINTPTR)&(pConfigParam->scenarioId));
    MY_LOGD("VC0_ID(%d),VC0_DataType(%d),VC1_ID(%d),VC1_DataType(%d),VC2_ID(%d),VC2_DataType(%d),VC3_ID(%d),VC3_DataType(%d),VC4_ID(%d),VC4_DataType(%d),VC5_ID(%d),VC5_DataType(%d)",
            vcInfo.VC0_ID,
            vcInfo.VC0_DataType,
            vcInfo.VC1_ID,
            vcInfo.VC1_DataType,
            vcInfo.VC2_ID,
            vcInfo.VC2_DataType,
            vcInfo.VC3_ID,
            vcInfo.VC3_DataType,
            vcInfo.VC4_ID,
            vcInfo.VC4_DataType,
            vcInfo.VC5_ID,
            vcInfo.VC5_DataType);
    ret = pSeninfDrv->setSeninfVC(
            csi.pCsiInfo->seninf,
            (vcInfo.VC0_DataType<<2)|(vcInfo.VC0_ID&0x03),
            (vcInfo.VC1_DataType<<2)|(vcInfo.VC1_ID&0x03),
            (vcInfo.VC2_DataType<<2)|(vcInfo.VC2_ID&0x03),
            (vcInfo.VC3_DataType<<2)|(vcInfo.VC3_ID&0x03),
            (vcInfo.VC4_DataType<<2)|(vcInfo.VC4_ID&0x03),
            (vcInfo.VC5_DataType<<2)|(vcInfo.VC5_ID&0x03));

    pSeninfDrv->setSeninfCsi((void *)&csiMipi, MIPI_SENSOR);

    SensorDynamicInfo *pSensorDynamicInfo = &mSensorDynamicInfo;
    SENINF_MUX_ENUM    muxSelect;

    seninfLowPowerConfigure((MSDK_SCENARIO_ID_ENUM)pConfigParam->scenarioId);

    //Fixed pixel mode
    pSensorDynamicInfo->HDRPixelMode  =
    pSensorDynamicInfo->PDAFPixelMode = SENINF_PIXEL_MODE_CAMSV;

    {
        property_get("debug.seninf.pixelmode", value, "-1");
        int pixelMode = atoi(value);
        if (pixelMode >= ONE_PIXEL_MODE && pixelMode <= FOUR_PIXEL_MODE) {
            pSensorDynamicInfo->pixelMode = pixelMode;
        }
    }

    for(i = 0; i < HDR_DATA_MAX_NUM; i++) {
        pSensorDynamicInfo->PixelMode[i] = SENINF_PIXEL_MODE_CAMSV;
        pSensorDynamicInfo->CamInfo[i] = CAM_TG_NONE;
    }

    pSensorDynamicInfo->TgInfo   =
    pSensorDynamicInfo->HDRInfo  =
    pSensorDynamicInfo->PDAFInfo = CAM_TG_NONE;

    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);
    seninfMUXReleaseAll(port);

    pSeninfDrv->mutexLock();
    muxSelect = seninfMUXArbitration(SENINF_CAM_MUX_MIN, SENINF_CAM_MUX_MAX);
    if (muxSelect != SENINF_MUX_ERROR) {
        pSeninfDrv->enableMUX(muxSelect);
        pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     MIPI_SENSOR,
                                     inDataType,
                                     pSensorDynamicInfo->pixelMode);
        pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
        pSensorDynamicInfo->TgInfo = muxSelect - SENINF_MUX1 + CAM_TG_1;
    }
    pSeninfDrv->mutexUnlock();

    if (hdrMode == SENSOR_VHDR_MODE_MVHDR) { /*0:NO HDR, 1:iHDR, 2:mvHDR, 3:zHDR, 4:four-cell mVHDR*/
        if(pInfo->HDR_Support == MVHDR_SUPPORT_MultiCAMSV) {
            pSeninfDrv->mutexLock();
            for (i = 0; i < HDR_DATA_MAX_NUM; i++) { /*Y HIST, AE HIST, Flicker, Embedded Data*/
                int VIRTUAL_CHANNEL = VIRTUAL_CHANNEL_1;
                VIRTUAL_CHANNEL += i;
                muxSelect = seninfMUXArbitration(SENINF_CAMSV_MUX_MIN, SENINF_CAMSV_MUX_MAX);
                if(muxSelect != SENINF_MUX_ERROR) {
                    pSeninfDrv->enableMUX(muxSelect);
                    pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                             pInfo->SensorHsyncPolarity ? 0 : 1,
                                             pInfo->SensorHsyncPolarity ? 0 : 1,
                                             (SENINF_SOURCE_ENUM)VIRTUAL_CHANNEL,
                                             inDataType,
                                             pSensorDynamicInfo->PixelMode[i]);
                    pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
                    pSensorDynamicInfo->CamInfo[i] = muxSelect - SENINF_CAM_MUX_MAX + CAM_SV_1; /*tell p1 which camsv is used*/
                    MY_LOGD("BK_ 3HDR pSensorDynamicInfo->CamInfo[%d]=%d, muxSelect=%d, VIRTUAL_CHANNEL=%d, pSensorDynamicInfo->PixelMode[i]=%d",
                        i, pSensorDynamicInfo->CamInfo[i], muxSelect, VIRTUAL_CHANNEL, pSensorDynamicInfo->PixelMode[i]); /*HDRInfo is for P1*/
                }
            }
            pSeninfDrv->mutexUnlock();
        } else {
            pSeninfDrv->mutexLock();
            muxSelect = seninfMUXArbitration(SENINF_CAMSV_MUX_MIN, SENINF_CAMSV_MUX_MAX);
            if (muxSelect != SENINF_MUX_ERROR) {
                pSeninfDrv->enableMUX(muxSelect);
                pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                             pInfo->SensorHsyncPolarity ? 0 : 1,
                                             pInfo->SensorHsyncPolarity ? 0 : 1,
                                             VIRTUAL_CHANNEL_1,
                                             inDataType,
                                             pSensorDynamicInfo->HDRPixelMode);
                pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
                pSensorDynamicInfo->HDRInfo = muxSelect - SENINF_CAMSV_MUX_MIN + CAM_SV_1;
            }
            pSeninfDrv->mutexUnlock();
        }
    }

    if (pdafMode &&
            (pInfo->PDAF_Support == PDAF_SUPPORT_CAMSV ||
            pInfo->PDAF_Support == PDAF_SUPPORT_CAMSV_LEGACY ||
            pInfo->PDAF_Support == PDAF_SUPPORT_CAMSV_DUALPD)) {
        pSeninfDrv->mutexLock();
        muxSelect = seninfMUXArbitration(SENINF_CAMSV_MUX_MIN, SENINF_CAMSV_MUX_MAX);
        if (muxSelect != SENINF_MUX_ERROR) {
            pSeninfDrv->enableMUX(muxSelect);
            pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                         pInfo->SensorHsyncPolarity ? 0 : 1,
                                         pInfo->SensorHsyncPolarity ? 0 : 1,
                                         VIRTUAL_CHANNEL_2,
                                         inDataType,
                                         pSensorDynamicInfo->PDAFPixelMode);
            pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
            pSensorDynamicInfo->PDAFInfo = muxSelect - SENINF_CAMSV_MUX_MIN + CAM_SV_1;
        }
        pSeninfDrv->mutexUnlock();
    }

    pSensorDynamicInfo->TgCLKInfo = SENINF_TIMESTAMP_CLK;

    MY_LOGD("Tg usage infomation[%d] : HDR = %d, PDAF = %d\n", pSensorDynamicInfo->TgInfo, pSensorDynamicInfo->HDRInfo, pSensorDynamicInfo->PDAFInfo);

    if (pConfigParam->framerate)
        // Unit : FPS , Driver Unit : 10*FPS
        framerate = pConfigParam->framerate * 10;
    else
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_DEFAULT_FRAME_RATE_BY_SCENARIO, (MUINTPTR)&(pConfigParam->scenarioId), (MUINTPTR)&framerate);

    MY_LOGD("framerate = %d\n", framerate);

    mScenarioId = pConfigParam->scenarioId;
    mHdrMode    = hdrMode;
    mPdafMode   = pdafMode;
    mFramerate  = framerate;

    pSensorDrv->setScenario((MSDK_SCENARIO_ID_ENUM)pConfigParam->scenarioId, framerate, hdrMode, pdafMode);

#ifdef HALSENSOR_AUTO_DESKEW
    setAutoDeskew(csi.sensorIdx, pConfigParam->crop.w, pConfigParam->crop.h, pSensorDrv->getType(), inDataType);
#endif

    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::configureSerial(HALSENSOR_SENINF_CSI &csi)
{
    SENINF_CSI_SCAM csiScam;
    SENINF_MUX_ENUM muxSelect;
    SensorDynamicInfo *pSensorDynamicInfo = &mSensorDynamicInfo;
    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(csi.sensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    ACDK_SENSOR_INFO_STRUCT *pInfo = csi.pInfo;
    MUINT32 framerate;

    const ConfigParam *pConfigParam = csi.pConfigParam;

    MINT32 ret = 0;

    if (csi.pInfo->SensroInterfaceType != SENSOR_INTERFACE_TYPE_SERIAL)
        MY_LOGW("Sensor type doesn't match CSI port SensroInterfaceType = %d, srcType = %d\n", csi.pInfo->SensroInterfaceType, csi.pCsiInfo->srcType);

    csiScam.enable             = 1;
    csiScam.SCAM_DataNumber    = (MUINT)pInfo->SCAM_DataNumber;
    csiScam.SCAM_DDR_En        = (MUINT)pInfo->SCAM_DDR_En;
    csiScam.SCAM_CLK_INV       = (MUINT)pInfo->SCAM_CLK_INV;
    csiScam.SCAM_DEFAULT_DELAY = (MUINT)pInfo->SCAM_DEFAULT_DELAY;
    csiScam.SCAM_CRC_En        = (MUINT)pInfo->SCAM_CRC_En;
    csiScam.SCAM_SOF_src       = (MUINT)pInfo->SCAM_SOF_src;
    csiScam.SCAM_Timout_Cali   = (MUINT)pInfo->SCAM_Timout_Cali;

    //Fixed pixel mode
    pSensorDynamicInfo->pixelMode     = SENINF_PIXEL_MODE_CAM;
    pSensorDynamicInfo->HDRPixelMode  =
    pSensorDynamicInfo->PDAFPixelMode = SENINF_PIXEL_MODE_CAMSV;

    pSensorDynamicInfo->TgInfo   =
    pSensorDynamicInfo->HDRInfo  =
    pSensorDynamicInfo->PDAFInfo = CAM_TG_NONE;

    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);
    seninfMUXReleaseAll(port);

    pSeninfDrv->mutexLock();
    muxSelect = seninfMUXArbitration(SENINF_CAM_MUX_MIN, SENINF_CAM_MUX_MAX);
    if(muxSelect != SENINF_MUX_ERROR) {
        pSeninfDrv->enableMUX(muxSelect);
        pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     SERIAL_SENSOR,
                                     YUV422_FMT,
                                     pSensorDynamicInfo->pixelMode);
        pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
        pSensorDynamicInfo->TgInfo = muxSelect - SENINF_MUX1 + CAM_TG_1;
    }
    pSeninfDrv->mutexUnlock();

    pSeninfDrv->setSeninfCsi((void *)&csiScam, SERIAL_SENSOR);

    pSensorDynamicInfo->TgCLKInfo = SENINF_TIMESTAMP_CLK;

    if (pConfigParam->framerate)
        // Unit : FPS , Driver Unit : 10*FPS
        framerate = pConfigParam->framerate * 10;
    else
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_DEFAULT_FRAME_RATE_BY_SCENARIO, (MUINTPTR)&(pConfigParam->scenarioId), (MUINTPTR)&framerate);

    MY_LOGD("framerate = %d\n", framerate);

    mScenarioId = pConfigParam->scenarioId;
    mHdrMode    = pConfigParam->HDRMode;
    mPdafMode   = 0;
    mFramerate  = framerate;

    pSensorDrv->setScenario((MSDK_SCENARIO_ID_ENUM)pConfigParam->scenarioId, framerate, pConfigParam->HDRMode, 0);

    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::configureParallel(HALSENSOR_SENINF_CSI &csi)
{
    SENINF_CSI_PARALLEL csiParallel;
    SENINF_MUX_ENUM muxSelect;
    SensorDynamicInfo *pSensorDynamicInfo = &mSensorDynamicInfo;
    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(csi.sensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    ACDK_SENSOR_INFO_STRUCT *pInfo = csi.pInfo;
    MUINT32 framerate;

    const ConfigParam *pConfigParam = csi.pConfigParam;

    MINT32 ret = 0;

    if (pInfo->SensroInterfaceType != SENSOR_INTERFACE_TYPE_PARALLEL)
        MY_LOGW("Sensor type doesn't match CSI port SensroInterfaceType = %d, srcType = %d\n", csi.pInfo->SensroInterfaceType, csi.pCsiInfo->srcType);

    //Fixed pixel mode
    pSensorDynamicInfo->pixelMode     = SENINF_PIXEL_MODE_CAM;
    pSensorDynamicInfo->HDRPixelMode  =
    pSensorDynamicInfo->PDAFPixelMode = SENINF_PIXEL_MODE_CAMSV;

    pSensorDynamicInfo->TgInfo   =
    pSensorDynamicInfo->HDRInfo  =
    pSensorDynamicInfo->PDAFInfo = CAM_TG_NONE;

    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);
    seninfMUXReleaseAll(port);

    pSeninfDrv->mutexLock();
    muxSelect = seninfMUXArbitration(SENINF_CAM_MUX_MIN, SENINF_CAM_MUX_MAX);
    if(muxSelect != SENINF_MUX_ERROR) {
        pSeninfDrv->enableMUX(muxSelect);
        pSeninfDrv->setSeninfMuxCtrl(muxSelect,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     pInfo->SensorHsyncPolarity ? 0 : 1,
                                     PARALLEL_SENSOR,
                                     YUV422_FMT,
                                     pSensorDynamicInfo->pixelMode);
        pSeninfDrv->setSeninfTopMuxCtrl(muxSelect, csi.pCsiInfo->seninf);
        pSensorDynamicInfo->TgInfo = muxSelect - SENINF_MUX1 + CAM_TG_1;
    }
    pSeninfDrv->mutexUnlock();

    csiParallel.enable = 1;
    pSeninfDrv->setSeninfCsi((void *)&csiParallel, PARALLEL_SENSOR);

    pSensorDynamicInfo->TgCLKInfo = SENINF_TIMESTAMP_CLK;

    if (pConfigParam->framerate)
        // Unit : FPS , Driver Unit : 10*FPS
        framerate = pConfigParam->framerate * 10;
    else
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_DEFAULT_FRAME_RATE_BY_SCENARIO, (MUINTPTR)&(pConfigParam->scenarioId), (MUINTPTR)&framerate);

    MY_LOGD("framerate = %d\n", framerate);

    mScenarioId = pConfigParam->scenarioId;
    mHdrMode    = pConfigParam->HDRMode;
    mPdafMode   = 0;
    mFramerate  = framerate;

    pSensorDrv->setScenario((MSDK_SCENARIO_ID_ENUM)pConfigParam->scenarioId, framerate, pConfigParam->HDRMode, 0);

    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::setSensorMclk(IMGSENSOR_SENSOR_IDX sensorIdx, MBOOL pcEn)
{
    SENINF_MCLK_PARA mclk_para;
    MINT32  ret = 0;

    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(sensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif
    ACDK_SENSOR_INFO_STRUCT *pInfo = &(pSensorDrv->getDrvInfo()->info);

    mclk_para.sensorIdx       = sensorIdx;
    mclk_para.mclkFreq        = pInfo->SensorClockFreq;
    mclk_para.mclkPolarityLow = pInfo->SensorClockPolarity;
    mclk_para.mclkFallingCnt  = pInfo->SensorClockFallingCount;
    mclk_para.mclkRisingCnt   = pInfo->SensorClockRisingCount;

    ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_MCLK_CONNECTION, (MUINTPTR)&mclk_para.mclkIdx);

    ret = pSeninfDrv->configMclk(&mclk_para, pcEn);
    if (ret < 0) {
        MY_LOGE("configMclk fail\n");
        return ret;
    }

    MY_LOGD("sensorIdx %d, mclk_src %d, SensorMCLKPLL %d",
        sensorIdx, mclk_para.mclkIdx, mclk_para.mclkPLL);

    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::sendCommand(
    MUINT    indexDual,
    MUINTPTR cmd,
    MUINTPTR arg1,
    MUINTPTR arg2,
    MUINTPTR arg3)
{
    MINT32 ret = 0;
    IMGSENSOR_SENSOR_IDX sensorDevId = IMGSENSOR_SENSOR_IDX_MAP(indexDual);

    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(sensorDevId);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

#ifdef CONFIG_MTK_CAM_SECURE
    MUINT32 isSecure = 0;
#endif

    if(sensorDevId == IMGSENSOR_SENSOR_IDX_NONE) {
        MY_LOGE("Wrong sensor index: %d\n", indexDual);
        return SENSOR_UNKNOWN_ERROR;
    }

    switch (cmd) {
    case SENSOR_CMD_GET_SENSOR_PIXELMODE:
        *(MUINT32 *)arg3 = mSensorDynamicInfo.pixelMode;
        break;

    case SENSOR_CMD_GET_SENSOR_N3D_DIFFERENCE_COUNT:
        ret = pSeninfDrv->getN3DDiffCnt((MUINT32 *)arg1);
        if(ret < 0) {
            MY_LOGE("[sendCommand] SENSOR_CMD_SET_N3D_CONFIG fail! \n");
        }
        break;

    case SENSOR_CMD_GET_SENSOR_POWER_ON_STETE: /*LSC funciton need open after sensor Power On*/
        *((MUINT32*) arg1) = (mSensorIdx != IMGSENSOR_SENSOR_IDX_NONE) ? 1<<mSensorIdx : 0;
        break;

    case SENSOR_CMD_DEBUG_P1_DQ_SENINF_STATUS: /*Pass1 deque fail would call this command*/
        ret = pSensorDrv->sendCommand(CMD_SENSOR_SET_DEBUG_DUMP);
        ret = pSeninfDrv->sendCommand(CMD_SENINF_DEBUG_TASK);

        break;
    case SENSOR_CMD_DEBUG_GET_SENINF_METER:
        ret =  pSeninfDrv->sendCommand(CMD_SENINF_DEBUG_PIXEL_METER, arg1);
        break;
    case SENSOR_CMD_SET_SENINF_CAM_TG_MUX:
    {
        SensorDynamicInfo *psensorDynamicInfo = &mSensorDynamicInfo;
        MUINT32            muxSrcInit   = (*(MUINT32 *)arg1 > CAM_TG_2) ? CAM_SV_1 : CAM_TG_1;
        MUINT32            muxSrcBase   = (*(MUINT32 *)arg1 > CAM_TG_2) ? SENINF_CAMSV_MUX_MIN : SENINF_CAM_MUX_MIN;
        MUINT32            targetTgInit = (*(MUINT32 *)arg2 > CAM_TG_2) ? CAM_SV_1 : CAM_TG_1;
        MUINT32            targetTgBase = (*(MUINT32 *)arg2 > CAM_TG_2) ? SENINF_CAMSV_MUX_MIN : SENINF_CAM_MUX_MIN;

        MUINT32           *psvInfo;
        MUINT32           *psvInfoEnd;

        if (psensorDynamicInfo->TgInfo == *(MUINT32 *)arg1) {
            psensorDynamicInfo->TgInfo = *(MUINT32 *)arg2;
        } else {
            if (pSensorDrv->getDrvInfo()->info.HDR_Support == MVHDR_SUPPORT_MultiCAMSV) {
                psvInfo    = &psensorDynamicInfo->CamInfo[0];
                psvInfoEnd = &psensorDynamicInfo->pixelMode;
            } else {
                psvInfo    = &psensorDynamicInfo->HDRInfo;
                psvInfoEnd = &psensorDynamicInfo->CamInfo[0];
            }

            while (*psvInfo != *(MUINT32 *)arg1 && psvInfo < psvInfoEnd) {
                psvInfo++;
            }

            if (psvInfo == psvInfoEnd) {
                MY_LOGE("Error Cam MUX setting originalCamTG = %d targetCamTG = %d", *(MUINT32 *)arg1, *(MUINT32 *)arg2);
            } else {
                *psvInfo = *(MUINT32 *)arg2;
            }
        }
        MY_LOGD("SENSOR_CMD_SET_SENINF_CAM_TG_MUX: original = %d, target = %d\n", *(MUINT32 *)arg1, *(MUINT32 *)arg2);
        ret = pSeninfDrv->setSeninfCamTGMuxCtrl(*(MUINT32 *)arg2 - targetTgInit + targetTgBase, (SENINF_MUX_ENUM)(*(MUINT32 *)arg1 - muxSrcInit + muxSrcBase));
#ifdef CONFIG_MTK_CAM_SECURE
        ret = pSeninfDrv->sendCommand(CMD_SENINF_SYNC_REG_TO_PA);
#endif
      break;
    }

    case SENSOR_CMD_SET_TEST_MODEL:
    {
#define HALSENSOR_TEST_MODEL_ISP_CLOCK 6000000
#define HALSENSOR_TEST_MODEL_VSYNC_TH  16
        /* FPS = ISP clock / ((TM_DUMMYPXL + TM_PXL) * (TM_VSYNC + TM_LINE))*/

        MUINT32 ispClock   = HALSENSOR_TEST_MODEL_ISP_CLOCK;
        MINT    dummyPixel = *(MUINT32 *)arg1 >> 7;
        MUINT32 vsync      = ispClock / *(MUINT32 *)arg3 - (*(MUINT32 *)arg1 + dummyPixel) - *(MUINT32 *)arg2;

        ret = pSeninfDrv->init();
        ret = pSeninfDrv->setSeninfTopMuxCtrl(SENINF_MUX1, SENINF_1);
        ret = pSeninfDrv->enableMUX(SENINF_MUX1);
        ret = pSeninfDrv->setSeninfMuxCtrl(SENINF_MUX1, 0, 0, TEST_MODEL, RAW_10BIT_FMT, 0);

        if(vsync < ((*(MUINT32 *)arg3) * (*(MUINT32 *)arg2))>>10) {
            MY_LOGW("[sendCommand] SENSOR_CMD_SET_TEST_MODEL fail, set to lower framerate! %d\n",
                    ispClock / ((dummyPixel + *(MUINT32 *)arg1) * (vsync + *(MUINT32 *)arg2)));
            ret = pSeninfDrv->setTestModel(true, dummyPixel, vsync, *(MUINT32 *)arg2, *(MUINT32 *)arg1);
        } else {
            ret = pSeninfDrv->setTestModel(true, dummyPixel, vsync, *(MUINT32 *)arg2, *(MUINT32 *)arg1);
        }
        break;
    }
    case SENSOR_CMD_GET_VERTICAL_BLANKING:
    {
        SensorCropWinInfo cropInfo;
        MUINT32 lineNum;
        MUINT32 pixelClock;
        MUINT32 frameRate;

        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_FRAME_SYNC_PIXEL_LINE_NUM, (MUINTPTR)&lineNum);
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_SENSOR_CROP_WIN_INFO, (MUINTPTR)&mScenarioId, (MUINTPTR)&cropInfo);
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_PIXEL_CLOCK_FREQ, (MUINTPTR)&pixelClock);
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_DEFAULT_FRAME_RATE_BY_SCENARIO, (MUINTPTR)&mScenarioId, (MUINTPTR)&frameRate);

        if(pixelClock != 0 && frameRate != 0) {
            *((MUINT32*) arg1) = (MUINT32)((double)10000000 / frameRate - (double)cropInfo.h2_tg_size * (lineNum & 0xFFFF) * 1000000 / pixelClock);
        } else {
            *((MUINT32*) arg1) = 0;
            MY_LOGW("Wrong pixel clock or framerate %d %d\n", pixelClock, frameRate);
        }

        break;
    }

    case SENSOR_CMD_GET_SENSOR_ROLLING_SHUTTER:
    {
        SensorCropWinInfo cropInfo;
        MUINT32 pixelClock = 0;
        MUINT32 lineLength = 0;

        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_FRAME_SYNC_PIXEL_LINE_NUM, (MUINTPTR)&lineLength);
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_SENSOR_CROP_WIN_INFO, (MUINTPTR)&mScenarioId, (MUINTPTR)&cropInfo);
        ret = pSensorDrv->sendCommand(CMD_SENSOR_GET_PIXEL_CLOCK_FREQ, (MUINTPTR)&pixelClock);

        if (pixelClock != 0) {
            *((MUINT32*) arg1) = (MUINT32)((double)(lineLength & 0xFFFF) * cropInfo.h2_tg_size * 1000000000 / pixelClock); /* unit: ns */
        } else {
            *((MUINT32*) arg1) = 0;
            MY_LOGW("Wrong pixel clock\n");
        }
        break;
    }

    default:
        ret = pSensorDrv->sendCommand(cmd, arg1, arg2, arg3);
        if(ret < 0) {
            MY_LOGE("[sendCommand] sendCommand fail! %" PRIxPTR "\n", cmd);
        }
        break;
    }

    return ret;
}

/******************************************************************************
 *
 ******************************************************************************/
MINT HalSensor::setSensorMclkDrivingCurrent(IMGSENSOR_SENSOR_IDX sensorIdx)
{
    MINT32 ret = 0;
    MUINT32 mclkSrc;
    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(sensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

    pSensorDrv->sendCommand(CMD_SENSOR_GET_MCLK_CONNECTION, (MUINTPTR)&mclkSrc);
    ret = pSeninfDrv->setMclkIODrivingCurrent(mclkSrc, pSensorDrv->getDrvInfo()->info.SensorDrivingCurrent);
    if (ret < 0) {
        MY_LOGE("The driving current for cam%d is wrong\n", sensorIdx);
    }

    return ret;
}

/*******************************************************************************
*
********************************************************************************/
MINT32 HalSensor::setDebugInfo(IBaseCamExif *pIBaseCamExif)
{
    (void)pIBaseCamExif;
    return 0;
}

/*******************************************************************************
*
********************************************************************************/
MINT32 HalSensor::reset()
{
    MINT32 ret = 0;
    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(mSensorIdx);
#ifdef CONFIG_MTK_CAM_SECURE
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance(HalSensorList::singleton()->querySecureState());
#else
    SeninfDrv *pSeninfDrv = SeninfDrv::getInstance();
#endif

    Mutex::Autolock _l(mMutex);
#ifdef CONFIG_MTK_CAM_SECURE
    pSensorDrv->sendCommand(CMD_SENSOR_SET_STREAMING_SUSPEND);
#endif
    if ((pSensorDrv->close()) < 0) {
        MY_LOGE("pSensorDrv->close fail");
        return MFALSE;
    }

    for (int j =0; j < 3; j++) {
        if ((ret = pSensorDrv->open()) < 0) {
            MY_LOGE("pSensorDrv->open fail, retry = %d ", j);
        }
        else {
            break;
        }
    }

    CUSTOM_CFG_CSI_PORT port;
    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_SENSOR_PORT, (MUINTPTR)&port);

    pSeninfDrv->reset(pSeninfDrv->getCSIInfo(port)->seninf);
    pSensorDrv->setScenario((MSDK_SCENARIO_ID_ENUM)mScenarioId, mFramerate, mHdrMode, mPdafMode);

#ifdef CONFIG_MTK_CAM_SECURE
    pSensorDrv->sendCommand(CMD_SENSOR_SET_STREAMING_RESUME);
#endif

    return ret;
}

MINT HalSensor::seninfLowPowerConfigure(MSDK_SCENARIO_ID_ENUM scenarioId)
{
    MINT32 ret = 0;
    MUINT32 mipiPixelRate = 0;
    MUINT32 frameRate = 0;
    SensorCropWinInfo cropInfo;

    ImgSensorDrv *const pSensorDrv = ImgSensorDrv::getInstance(mSensorIdx);

    SensorDynamicInfo *pSensorDynamicInfo = &mSensorDynamicInfo;
    //unused
    //SENSORDRV_INFO_STRUCT              *pSensorDrvInfo  = pSensorDrv->getDrvInfo();
    //ACDK_SENSOR_INFO_STRUCT            *pInfo           = &pSensorDrvInfo->info;
    //ACDK_SENSOR_RESOLUTION_INFO_STRUCT *pResolutionInfo = &pSensorDrvInfo->resolutionInfo;

    pSensorDrv->sendCommand(CMD_SENSOR_GET_MIPI_PIXEL_RATE, (MUINTPTR)&scenarioId, (MUINTPTR)&mipiPixelRate);

    if (!mipiPixelRate) {
        pSensorDrv->sendCommand(CMD_SENSOR_GET_SENSOR_CROP_WIN_INFO, (MUINTPTR)&scenarioId, (MUINTPTR)&cropInfo);
        pSensorDrv->sendCommand(CMD_SENSOR_GET_DEFAULT_FRAME_RATE_BY_SCENARIO, (MUINTPTR)&scenarioId, (MUINTPTR)&frameRate);
        mipiPixelRate = cropInfo.w2_tg_size * cropInfo.h2_tg_size * frameRate * 0.125;
    }

    MY_LOGD("mipiPixelRate = %d\n", mipiPixelRate);

    //TODO: cam clock query from kernel
    pSensorDynamicInfo->pixelMode = (mipiPixelRate > 364000000 * 2) ? FOUR_PIXEL_MODE : TWO_PIXEL_MODE;

    //TODO: Dynamically adjust seninf clock

    return ret;
}

