/*
 * Copyright (C) 2018 MediaTek Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See http://www.gnu.org/licenses/gpl-2.0.html for more details.
 */

#ifndef __SENINF_TEE_COMMON_H__
#define __SENINF_TEE_COMMON_H__

#include "seninf_reg.h"

#define SENINF_TEE_REG_ADDR_BASE  0x1A040000
#define SENINF_TEE_REG_MAP_REGION 0x8000

typedef enum {
    SENINF_TEE_REG_ADDR_SENINF_TOP_MUX_CTRL     = 0x0008,
    SENINF_TEE_REG_ADDR_SENINF_TOP_CAM_MUX_CTRL = 0x0010,
    SENINF_TEE_REG_ADDR_SENINF1_MUX_CTRL        = 0x0D00,
    SENINF_TEE_REG_ADDR_SENINF1_MUX_CTRL_EXT    = 0x0D3C,
    SENINF_TEE_REG_ADDR_SENINF2_MUX_CTRL        = 0x1D00,
    SENINF_TEE_REG_ADDR_SENINF2_MUX_CTRL_EXT    = 0x1D3C,
    SENINF_TEE_REG_ADDR_SENINF3_MUX_CTRL        = 0x2D00,
    SENINF_TEE_REG_ADDR_SENINF3_MUX_CTRL_EXT    = 0x2D3C,
    SENINF_TEE_REG_ADDR_SENINF4_MUX_CTRL        = 0x3D00,
    SENINF_TEE_REG_ADDR_SENINF4_MUX_CTRL_EXT    = 0x3D3C,
    SENINF_TEE_REG_ADDR_SENINF5_MUX_CTRL        = 0x4D00,
    SENINF_TEE_REG_ADDR_SENINF5_MUX_CTRL_EXT    = 0x4D3C,
    SENINF_TEE_REG_ADDR_SENINF6_MUX_CTRL        = 0x5D00,
    SENINF_TEE_REG_ADDR_SENINF6_MUX_CTRL_EXT    = 0x5D3C
} SENINF_TEE_REG_ADDR;

typedef struct {
    SENINF1_REG_MUX_CTRL                            SENINF1_MUX_CTRL;
    SENINF1_REG_MUX_CTRL_EXT                        SENINF1_MUX_CTRL_EXT;
} SENINF_TEE_REG_MUX;

typedef struct {
    SENINF_REG_TOP_MUX_CTRL                         SENINF_TOP_MUX_CTRL;
    SENINF_REG_TOP_CAM_MUX_CTRL                     SENINF_TOP_CAM_MUX_CTRL;
    SENINF1_REG_MUX_CTRL                            SENINF1_MUX_CTRL;
    SENINF1_REG_MUX_CTRL_EXT                        SENINF1_MUX_CTRL_EXT;
    SENINF2_REG_MUX_CTRL                            SENINF2_MUX_CTRL;
    SENINF2_REG_MUX_CTRL_EXT                        SENINF2_MUX_CTRL_EXT;
    SENINF3_REG_MUX_CTRL                            SENINF3_MUX_CTRL;
    SENINF3_REG_MUX_CTRL_EXT                        SENINF3_MUX_CTRL_EXT;
    SENINF4_REG_MUX_CTRL                            SENINF4_MUX_CTRL;
    SENINF4_REG_MUX_CTRL_EXT                        SENINF4_MUX_CTRL_EXT;
    SENINF5_REG_MUX_CTRL                            SENINF5_MUX_CTRL;
    SENINF5_REG_MUX_CTRL_EXT                        SENINF5_MUX_CTRL_EXT;
    SENINF6_REG_MUX_CTRL                            SENINF6_MUX_CTRL;
    SENINF6_REG_MUX_CTRL_EXT                        SENINF6_MUX_CTRL_EXT;
} SENINF_TEE_REG;

#define SENINF_TEE_REG_GET_ADDR(reg) (SENINF_TEE_REG_ADDR_##reg + SENINF_TEE_REG_ADDR_BASE)

typedef enum {
    SENINF_TEE_CMD_SYNC_TO_PA,
    SENINF_TEE_CMD_SYNC_TO_VA,
} SENINF_TEE_CMD;

typedef void* seninf_ca_open_session_t(void);
typedef int seninf_ca_close_session_t(void*);
typedef int seninf_ca_sync_to_pa_t(void*, void *);
typedef int seninf_ca_sync_to_va_t(void*, void *);

#define SENINF_CA_LIB "libimgsensorca.so"
#define SENINF_CA_OPEN_SESSION "seninf_ca_open_session"
#define SENINF_CA_CLOSE_SESSION "seninf_ca_close_session"
#define SENINF_CA_SYNC_TO_PA "seninf_ca_sync_to_pa"
#define SENINF_CA_SYNC_TO_VA "seninf_ca_sync_to_va"

#endif

