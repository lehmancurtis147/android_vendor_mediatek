/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "ULogGuard"
#include <cstring>
#include <vector>
#include <string>
#include <algorithm>
#include <signal.h>
#include <unistd.h>
#if MTKCAM_HAVE_AEE_FEATURE == 1
#include <aee.h>
#endif
#include <sys/types.h>
#include <sys/prctl.h>
#include <utils/AndroidThreads.h>
#include <cutils/properties.h>
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/debug/debug.h>
#include "ULogInt.h"
#include "ULogTable.h"
#include "ULogGuard.h"
#include "ULogRTDiag.h"


// 0x1: function; 0x2: request
static const char * const ULOG_GUARD_RESET_PROP_NAME = "vendor.debug.camera.ulog.guardreset";
#ifdef ULOG_GUARD_PLATFORM_DEFAULT_RESET_FLAGS
static const unsigned int ULOG_GUARD_DEFAULT_RESET_FLAGS = ULOG_GUARD_PLATFORM_DEFAULT_RESET_FLAGS;
#else
static const unsigned int ULOG_GUARD_DEFAULT_RESET_FLAGS = 0x3;
#endif

static const int ULOG_GUARD_FUNC_WARNING_MSEC = 11000;
static const int ULOG_GUARD_DEFAULT_FUNC_TIMEOUT_MSEC = 16000;
static const int ULOG_GUARD_MAX_FUNC_TIMEOUT_MSEC = 60000;
static const int ULOG_GUARD_REQ_WARNING_MSEC = 30000;
static const int ULOG_GUARD_DEFAULT_REQ_TIMEOUT_MSEC = 60000;

#if MTKCAM_HAVE_AEE_FEATURE == 1
static const char * const ULOG_AEE_NAME = "Camera/ULogGuard";
static const unsigned int ULOG_AEE_DB_FLAGS =
    DB_OPT_NE_JBT_TRACES | DB_OPT_PROCESS_COREDUMP | DB_OPT_PROC_MEM | DB_OPT_PID_SMAPS |
    DB_OPT_LOW_MEMORY_KILLER | DB_OPT_DUMPSYS_PROCSTATS | DB_OPT_DUMP_DISPLAY;
#endif


using namespace android;


// coverity[+kill]
__attribute__((noinline))
extern "C"
void FUNCTION_TIMEOUT_PleaseCheckLogToFindOwner(
    const NSCam::Utils::ULog::ThreadGuardData *guardData)
{
    (void)guardData;

#if MTKCAM_HAVE_AEE_FEATURE == 1
    aee_system_exception(
        ULOG_AEE_NAME,
        NULL,
        ULOG_AEE_DB_FLAGS,
        "%s/%s TIMEOUT\nCRDISPATCH_KEY:%s function TIMEOUT",
        NSCam::Utils::ULog::getModuleName(guardData->firstModuleId),
        guardData->firstFuncName,
        NSCam::Utils::ULog::getModuleName(guardData->firstModuleId));
#else
    raise(SIGABRT);
#endif

    raise(SIGKILL); // make sure, KILL can not be caught
}


// coverity[+kill]
__attribute__((noinline))
extern "C"
void REQUEST_TIMEOUT_PleaseCheckLogToFindOwner(NSCam::Utils::ULog::ModuleId suspect)
{
    (void)suspect;

#if MTKCAM_HAVE_AEE_FEATURE == 1
    if (suspect != 0) {
        aee_system_exception(
            ULOG_AEE_NAME,
            NULL,
            ULOG_AEE_DB_FLAGS,
            "Request TIMEOUT\nCRDISPATCH_KEY:Camera request TIMEOUT, key module: %s",
            NSCam::Utils::ULog::getModuleName(suspect));
    } else {
        aee_system_exception(
            ULOG_AEE_NAME,
            NULL,
            ULOG_AEE_DB_FLAGS,
            "Request TIMEOUT");
    }
#else
    raise(SIGABRT);
#endif

    raise(SIGKILL); // make sure, KILL can not be caught
}


namespace NSCam {
namespace Utils {
namespace ULog {

// MUST be monotonic
const clockid_t ULOG_GUARD_CLOCK_ID = CLOCK_MONOTONIC;

ULogGuardMonitor::RequestQueue::RequestQueue()
{
    mUnused.reserve(MAX_REQUEST_NUM);
    for (size_t i = 0; i < MAX_REQUEST_NUM; i++)
        mUnused.push_back(&mRequestSlots[i]);
}


template <typename _F>
bool ULogGuardMonitor::RequestQueue::remove(_F &&match)
{
    // Ideally, request is FIFO, it will be efficieny if search from begin.
    // However, the serial of AppRequest may be duplicated, we must search
    // from end
    int i = static_cast<int>(mInFlight.size() - 1);
    for ( ; i >= 0; i--) {
        if (match(*mInFlight[i]))
            break;
    }

    if (i >= 0) {
        mUnused.push_back(mInFlight[i]);
        mInFlight.erase(mInFlight.begin() + i);
        return true;
    }

    return false;
}


template <typename ... _T>
inline void ULogGuardMonitor::RequestQueue::emplace_back(_T&& ... args)
{
    RequestRecord *slot = mUnused.back();
    mUnused.pop_back();
    slot->set(std::forward<_T>(args) ...);
    mInFlight.push_back(slot);
}


inline void ULogGuardMonitor::RequestQueue::pop_front()
{
    mUnused.push_back(mInFlight.front());
    mInFlight.pop_front();
}


ULogGuardMonitor::ULogGuardMonitor() :
    mContinueRunning(false), mGuardNumber(0), mIsReqGuardEnabled(false)
{
}


ULogGuardMonitor::~ULogGuardMonitor()
{
}


void ULogGuardMonitor::start()
{
    mContinueRunning = true;
    mThread = std::thread(&ULogGuardMonitor::run, this);
}


void ULogGuardMonitor::stop()
{
    {
        std::unique_lock<std::mutex> lock(mMutex);
        mContinueRunning = false;
        mCond.notify_all();
    }

    if (mThread.joinable()) {
        mThread.join();
    }
}


inline void ULogGuardMonitor::registerThread(ThreadGuardData *guardData)
{
    std::unique_lock<std::mutex> lock(mMutex);
    mGuardData.push_back(guardData);
}


inline void ULogGuardMonitor::unregisterThread(ThreadGuardData *guardData)
{
    std::unique_lock<std::mutex> lock(mMutex);
    for (auto iter = mGuardData.begin(); iter != mGuardData.end(); iter++) {
        if (*iter == guardData) {
            mGuardData.erase(iter);
            break;
        }
    }
}


inline void ULogGuardMonitor::incGuardNumber()
{
    if (mGuardNumber.fetch_add(1, std::memory_order_relaxed) == 0) {
        // We can obtain mMutex only if the guard thread is waiting
        // the mCond.notify_all() will always waken the waiting
        // So it is safe that not putting mGuardNumber in the critical section
        std::unique_lock<std::mutex> lock(mMutex);
        mCond.notify_all();
    }
}


inline void ULogGuardMonitor::decGuardNumber()
{
    // coverity[side_effect_free : FALSE]
    mGuardNumber.fetch_sub(1, std::memory_order_relaxed);
}


void ULogGuardMonitor::registerReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial, int timeoutMs)
{
    if (!mIsReqGuardEnabled.load(std::memory_order_relaxed))
        return;

    if (__unlikely(requestType != REQ_APP_REQUEST))
        return;

    std::unique_lock<std::mutex> lock(mMutex);
    if (__likely(!mAppRequests.isFull())) {
        timespec enterTimeStamp;
        clock_gettime(ULOG_GUARD_CLOCK_ID, &(enterTimeStamp));
        mAppRequests.emplace_back(moduleId, requestType, requestSerial, enterTimeStamp, timeoutMs);
        incGuardNumber();
    }
}


void ULogGuardMonitor::unregisterReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial)
{
    if (!mIsReqGuardEnabled.load(std::memory_order_relaxed))
        return;

    if (__unlikely(requestType != REQ_APP_REQUEST))
        return;

    auto isTheSameRequest = [moduleId, requestType, requestSerial] (const RequestRecord &record) {
        return (record.moduleId == moduleId && record.requestType == requestType && record.requestSerial == requestSerial);
    };

    std::unique_lock<std::mutex> lock(mMutex);
    if (__likely(mAppRequests.remove(isTheSameRequest))) {
        decGuardNumber();
    }
}


void ULogGuardMonitor::registerFinalizer(android::sp<ULogGuard::IFinalizer> &finalizer)
{
    std::unique_lock<std::mutex> lock(mMutex);

    // Clean garbage
    auto it = mFinalizers.begin();
    while (it != mFinalizers.end()) {
        if (it->promote() == NULL) {
            it = mFinalizers.erase(it);
        } else {
            it++;
        }
    }

    mFinalizers.emplace_back(finalizer);
}


inline bool ULogGuardMonitor::getCpuLoading(CpuLoading &loading)
{
    if (ULogger::isModeEnabled(ULogger::TEXT_FILE_LOG | ULogger::PASSIVE_LOG)) {
        // We have permission only if "setenforce 0"
        int statFd = open("/proc/stat", O_RDONLY);

        if (statFd > 0) {
            char buffer[100];
            char cpu[10];

            loading.runningTime = 0;
            loading.idleTime = 0;

            int n = read(statFd, buffer, sizeof(buffer));
            if (n > 10) {
                long user, nice, system, idle, iowait, irq, softirq;
                if (sscanf(buffer, "%s %ld %ld %ld %ld %ld %ld %ld", cpu, &user, &nice, &system, &idle, &iowait, &irq, &softirq) == 8) {
                    loading.runningTime = user + nice + system + iowait + irq + softirq;
                    loading.idleTime = idle;
                }
            }

            close(statFd);

            return true;
        }
    }

    loading.runningTime = 0;
    loading.idleTime = 0;

    return false;
}


inline long ULogGuardMonitor::getCpuLoadingPercentage(const CpuLoading &prev, const CpuLoading &curr)
{
    long totalRunningDiff = curr.runningTime - prev.runningTime;
    long totalTimeDiff = totalRunningDiff + (curr.idleTime - prev.idleTime);
    if (totalTimeDiff <= 0)
        return -1;
    return totalRunningDiff * 100 / totalTimeDiff;
}


void ULogGuardMonitor::run()
{
    androidSetThreadName("ULogGuardMonitor");
    androidSetThreadPriority(getTid(), android::PRIORITY_BACKGROUND);

    int funcTimeoutMs = 0;
    int reqTimeoutMs = 0;
    int resetFlag = property_get_int32(ULOG_GUARD_RESET_PROP_NAME, ULOG_GUARD_DEFAULT_RESET_FLAGS);

    if (resetFlag & 0x1) {
        #if 0
        if (::property_get_bool("ro.config.low_ram", false))
            funcTimeoutMs = ULOG_GUARD_DEFAULT_FUNC_TIMEOUT_MSEC * 2;
        else
        #endif
            funcTimeoutMs = ULOG_GUARD_DEFAULT_FUNC_TIMEOUT_MSEC;
    }
    if (resetFlag & 0x2) {
        reqTimeoutMs = ULOG_GUARD_DEFAULT_REQ_TIMEOUT_MSEC;
        mIsReqGuardEnabled.store(true, std::memory_order_relaxed);
    }

    std::atomic_thread_fence(std::memory_order_release);

    ULOG_IMP_LOGI("Running: default func timeout/reset = %d/%d ms; req timeout = %d ms",
        funcTimeoutMs, ULOG_GUARD_MAX_FUNC_TIMEOUT_MSEC, reqTimeoutMs);

    std::unique_lock<std::mutex> lock(mMutex);
    CpuLoading previousLoading;
    getCpuLoading(previousLoading);
    bool detectCpuLoading = false;
    const ThreadGuardData *printedGuardData = nullptr;
    static constexpr RequestSerial INVALID_SERIAL = 0xfffffff;
    RequestSerial warnedAppRequest = INVALID_SERIAL;

    timespec showAlive;
    clock_gettime(ULOG_GUARD_CLOCK_ID, &showAlive);

    while (mContinueRunning) {
        mCond.wait_for(lock, std::chrono::milliseconds(1000));

        if (!mContinueRunning)
            break;

        if (mGuardNumber.load(std::memory_order_relaxed) == 0) {
            mCond.wait(lock);

            if (!mContinueRunning)
                break;

            continue;
        }

        long loadingPercentage = -1;
        CpuLoading currentLoading;
        if (detectCpuLoading) {
            // getCpuLoading has relatively large overhead, we try to avoid if unnecessary
            if (getCpuLoading(currentLoading))
                loadingPercentage = getCpuLoadingPercentage(previousLoading, currentLoading);
        }

        int maxTimeoutElapsedMs = 0;
        const ThreadGuardData *timeoutGuardData = nullptr;

        detectCpuLoading = false;
        timespec now;
        clock_gettime(ULOG_GUARD_CLOCK_ID, &now);
        for (const ThreadGuardData *guardData : mGuardData) {
            std::unique_lock<std::mutex> dataLock(guardData->dataMutex);
            if (guardData->depth > 0) {
                int elapsedMs = timeDiffMs(guardData->firstTimeStamp, now);
                int timeoutMs = funcTimeoutMs;
                if (funcTimeoutMs > 0 && guardData->timeoutMs > 0)
                    timeoutMs = guardData->timeoutMs;
                if (timeoutMs > 0 && elapsedMs > timeoutMs) {
                    CAM_ULOGE(guardData->firstModuleId,
                        "Thread tid=%d M[%s:%x] %s executed %d ms(>%d) TOO LONG; frameData = %p; CPU loading = %ld%%",
                        guardData->tid, getModuleName(guardData->firstModuleId), guardData->firstModuleId,
                        guardData->firstFuncName, elapsedMs, timeoutMs,
                        guardData->firstFrameData, loadingPercentage);

                    if (elapsedMs > maxTimeoutElapsedMs) {
                        maxTimeoutElapsedMs = elapsedMs;
                        timeoutGuardData = guardData;
                    }
                } else if (elapsedMs > ULOG_GUARD_FUNC_WARNING_MSEC) {
                    CAM_ULOGW(guardData->firstModuleId,
                        "Thread tid=%d M[%s:%x] %s executed %d ms; frameData = %p; CPU loading = %ld%%",
                        guardData->tid, getModuleName(guardData->firstModuleId), guardData->firstModuleId,
                        guardData->firstLongFuncName, timeDiffMs(guardData->firstTimeStamp, now),
                        guardData->firstFrameData, loadingPercentage);
                }

                if (elapsedMs >= ULOG_GUARD_FUNC_WARNING_MSEC - 1500) {
                    detectCpuLoading = true;
                }
            }
        }

        if (maxTimeoutElapsedMs > 0 && timeoutGuardData != nullptr) {
            std::unique_lock<std::mutex> dataLock(timeoutGuardData->dataMutex);

            if (timeoutGuardData != printedGuardData) {
                ULogger::logTimeout(timeoutGuardData->firstModuleId, timeoutGuardData->tid,
                    timeoutGuardData->firstLongFuncName, maxTimeoutElapsedMs);
                printedGuardData = timeoutGuardData;
            }

            bool abortCamera = true;
        #if !defined(ULOG_GUARD_FAST_RESET)
            // ANR can trigger camera debug dump
            // For non-user load, we allow more time to wait for any possible ANR
            if (maxTimeoutElapsedMs > ULOG_GUARD_MAX_FUNC_TIMEOUT_MSEC) {
                ULogger::logTimeout(timeoutGuardData->firstModuleId, timeoutGuardData->tid,
                    timeoutGuardData->firstLongFuncName, maxTimeoutElapsedMs);
                abortCamera = true;
            } else {
                abortCamera = false;
            }
        #endif
            if (abortCamera) {
                mFinalizer = std::thread(&ULogGuardMonitor::finalize, this);
                mFinalCond.wait_for(lock, std::chrono::milliseconds(3000));

                FUNCTION_TIMEOUT_PleaseCheckLogToFindOwner(timeoutGuardData);
            }
        } else {
            printedGuardData = nullptr;
        }

        if (!mAppRequests.isEmpty()) {
            const RequestRecord &oldest = mAppRequests.front();
            int elapsedMs = timeDiffMs(oldest.enterTimeStamp, now);
            int timeoutMs = reqTimeoutMs;
            if (reqTimeoutMs > 0 && oldest.timeoutMs > 0)
                timeoutMs = oldest.timeoutMs;
            if (timeoutMs > 0 && elapsedMs > timeoutMs) {
                CAM_ULOGE_R(oldest.moduleId, oldest.requestType, oldest.requestSerial,
                    "executed %d ms(>%d) TIMEOUT; CPU loading = %ld%% FORCE STOP",
                    elapsedMs, timeoutMs, loadingPercentage);

                ModuleId suspect = 0;
                if (ULogRuntimeDiag::isEnabled()) {
                    suspect = NSCam::Utils::ULog::ULogRTDiagImpl::get().timeoutPickSuspect(
                        oldest.requestSerial, timeoutMs);
                }

                char message[256];
                if (suspect != 0) {
                    snprintf(message, sizeof(message), "R %s:%u NO EXIT(suspect: M[%s:%x])",
                        getRequestTypeName(oldest.requestType), oldest.requestSerial,
                        getModuleName(suspect), suspect);
                } else {
                    snprintf(message, sizeof(message), "R %s:%u NO EXIT",
                        getRequestTypeName(oldest.requestType), oldest.requestSerial);
                }
                ULogger::logTimeout(oldest.moduleId, 0, message, elapsedMs);

                mFinalizer = std::thread(&ULogGuardMonitor::finalize, this);
                mFinalCond.wait_for(lock, std::chrono::milliseconds(3000));

                REQUEST_TIMEOUT_PleaseCheckLogToFindOwner(suspect);
            } else if (elapsedMs > ULOG_GUARD_REQ_WARNING_MSEC) {
                if (reqTimeoutMs <= 0 && elapsedMs > ULOG_GUARD_DEFAULT_REQ_TIMEOUT_MSEC) {
                    CAM_ULOGE_R(oldest.moduleId, oldest.requestType, oldest.requestSerial,
                        "executed %d ms TOO LONG; CPU loading = %ld%% STOP WARNING",
                        elapsedMs, loadingPercentage);
                    mAppRequests.pop_front();
                } else {
                    CAM_ULOGW_R(oldest.moduleId, oldest.requestType, oldest.requestSerial,
                        "executed %d ms TOO LONG; CPU loading = %ld%%",
                        elapsedMs, loadingPercentage);
                }
                if (warnedAppRequest != oldest.requestSerial) {
                    if (ULogRuntimeDiag::isEnabled()) {
                        // Print suspects
                        NSCam::Utils::ULog::ULogRTDiagImpl::get().timeoutPickSuspect(
                            oldest.requestSerial, elapsedMs);
                    }
                    warnedAppRequest = oldest.requestSerial;
                }
            } else {
                warnedAppRequest = INVALID_SERIAL;
            }

            if (elapsedMs >= ULOG_GUARD_REQ_WARNING_MSEC - 2500) {
                detectCpuLoading = true;
            }
        }

        if (detectCpuLoading) {
            if (currentLoading.runningTime > 0)
                previousLoading = currentLoading;
            else
                getCpuLoading(previousLoading);
        }

        previousLoading = currentLoading;

        if (now.tv_sec - showAlive.tv_sec >= 3) {
            showAlive = now;
            ULOG_IMP_LOGD("Running: %zu threads, %zu requests; %d guards in monitor. Timeout = %d,%d msec",
                mGuardData.size(), mAppRequests.size(), mGuardNumber.load(std::memory_order_relaxed),
                funcTimeoutMs, reqTimeoutMs);
        }
    }

    ULOG_IMP_LOGI("Stopped: %zu threads, %zu requests; %d guards in monitor",
        mGuardData.size(), mAppRequests.size(), mGuardNumber.load(std::memory_order_relaxed));
}


void ULogGuardMonitor::finalize()
{
    std::list<android::wp<ULogGuard::IFinalizer>> finalizers;

    {
        std::unique_lock<std::mutex> lock(mMutex);
        finalizers = mFinalizers;
    }

    ULOG_IMP_LOGI("Run finalizers");

    ULogGuard::Status status;
    for (auto it : finalizers) {
        sp<ULogGuard::IFinalizer> finalizer = it.promote();
        if (finalizer != NULL) {
            finalizer->onTimeout(status);
        }
    }

    ULOG_IMP_LOGI("Start debuggee dump");

    auto pDbgMgr = IDebuggeeManager::get();
    if (pDbgMgr != nullptr) {
        LogPrinter printer(LOG_TAG);
        std::vector<std::string> options;
        pDbgMgr->debug(printer, options);
    }

    std::this_thread::sleep_for(std::chrono::milliseconds(200));

    mFinalCond.notify_all();
}


void ULogGuard::onInit()
{
    ULogGuardMonitor::getSingleton().start();
}


void ULogGuard::onUninit()
{
    ULogGuardMonitor::getSingleton().stop();
}


thread_local std::unique_ptr<ThreadGuardData> sThreadGuardData(nullptr);


ThreadGuardData::ThreadGuardData() :
    dataMutex(), depth(0), timeoutMs(0),
    firstModuleId(0), firstTimeStamp{0, 0}, firstFuncName(nullptr), firstLongFuncName(nullptr), firstFrameData(nullptr)
{
    tid = gettid();
    ULogGuardMonitor::getSingleton().registerThread(this);
}


ThreadGuardData::~ThreadGuardData()
{
    // Since it is thread-local data, register/unregister will be the same thread
    ULogGuardMonitor::getSingleton().unregisterThread(this);
}


void ULogGuard::registerFuncGuard(ModuleId moduleId, const char * /* tag */,
    const char *funcName, const char *longFuncName, int timeoutMs, int *outGuardId)
{
    if (__unlikely(sThreadGuardData == nullptr)) {
        sThreadGuardData.reset(new ThreadGuardData);
    }

    ThreadGuardData *guardData = sThreadGuardData.get();
    {
        std::unique_lock<std::mutex> dataLock(guardData->dataMutex);
        // Maybe we can have a lock-free design, but not necessary so far
        guardData->depth++;
        if (__likely(guardData->depth == 1)) {
            guardData->timeoutMs = timeoutMs;
            guardData->firstModuleId = moduleId;
            clock_gettime(ULOG_GUARD_CLOCK_ID, &(guardData->firstTimeStamp));
            guardData->firstFuncName = funcName; // We assumed funcName is a literal(verified)
            guardData->firstLongFuncName = longFuncName;
            guardData->firstFrameData = outGuardId;
        }
    }

    ULogGuardMonitor::getSingleton().incGuardNumber();

    *outGuardId = ((guardData->tid << 8) | guardData->depth);
}


void ULogGuard::unregisterFuncGuard(int /* guardId */)
{
    {
        std::unique_lock<std::mutex> dataLock(sThreadGuardData->dataMutex);
        sThreadGuardData->depth--;
    }

    ULogGuardMonitor::getSingleton().decGuardNumber();
}


void ULogGuard::registerReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial, int timeoutMs)
{
    ULogGuardMonitor::getSingleton().registerReqGuard(moduleId, requestType, requestSerial, timeoutMs);
}


void ULogGuard::unregisterReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial)
{
    ULogGuardMonitor::getSingleton().unregisterReqGuard(moduleId, requestType, requestSerial);
}


void ULogGuard::registerFinalizer(android::sp<IFinalizer> finalizer)
{
    ULogGuardMonitor::getSingleton().registerFinalizer(finalizer);
}


}
}
}


