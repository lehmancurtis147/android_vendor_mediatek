/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "ULogFile"

#include <cerrno>
#include <cstdio>
#include <cstring>
#include <string>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/prctl.h>
#include <unistd.h>
#include <time.h>
#include <utils/AndroidThreads.h>
#include <cutils/properties.h>
#include <log/log.h>
#include <libladder.h>
#include <mtkcam/utils/std/Log.h>
#include "ULogInt.h"
#include "ULogTable.h"
#include "ULogFile.h"
#include "ULogRTDiag.h"


static const char * const ULOG_FOLDERPATH_PROP_NAME = "vendor.debug.camera.ulog.folder";
static const char * const ULOG_FOLDERPATH_DEFAULT = "/sdcard/mtklog/cam_ulog";
static const char * const ULOG_PASSIVE_MAX_BUFFER_KB_PROP_NAME = "vendor.debug.camera.ulog.maxkb";
static const char * const ULOG_FILE_HOOK_NE_PROP_NAME = "vendor.debug.camera.ulog.hookne";
static const char * const ULOG_FILE_MONOCLOCK_PROP_NAME = "vendor.debug.camera.ulog.monoclock";


#define FULOG_DBG(fmt, arg...) do { } while(0)
// #define FULOG_DBG(fmt, arg...) CAM_LOGD("[%s]" fmt, __func__, ##arg)


namespace NSCam {
namespace Utils {
namespace ULog {
namespace File {

bool FileULogWriter::open(bool newSession, const char *prefix, bool inSignal)
{
    if (newSession) {
        timespec timeStamp;
        clock_gettime(ANDROID_CLOCK_ID, &timeStamp);
        strftime(mSessionTimestamp, sizeof(mSessionTimestamp), "%Y_%m%d_%H%M%S", gmtime(&timeStamp.tv_sec));
        mFileSerial = 1;
    }

    char filePath[256];
    snprintf(filePath, sizeof(filePath), "%s/%scam_ulog_%s_%02d.txt",
        mFolderPath.c_str(), prefix, mSessionTimestamp, mFileSerial);

    mFd = ::open(filePath, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    if (mFd < 0) {
        if (!inSignal) {
            ULOG_IMP_LOGE("FileULogWriter: file open failed %s", filePath);
        }

        mFolderPath = "/data/vendor";
        snprintf(filePath, sizeof(filePath), "%s/%scam_ulog_%s_%02d.txt",
            mFolderPath.c_str(), prefix, mSessionTimestamp, mFileSerial);
        mFd = ::open(filePath, O_CREAT | O_WRONLY, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (mFd < 0) {
            if (!inSignal) {
                ULOG_IMP_LOGE("NOTE: \"adb shell setenforce 0\" to give writing permission. Then restart camerahalserver. !!!");
            }
            return false;
        }
    }

    if (!inSignal) {
        ULOG_IMP_LOGI("Write ulog to %s", filePath);
    }

    mFileSerial++;
    mBufferFilled = 0;
    mBufferEmpty = getBufferBegin();
    mBytesWritten = 0;

    return true;
}


void FileULogWriter::close()
{
    if (mFd >= 0) {
        flush();
        ::close(mFd);
        mFd = -1;
    }
}


void FileULogWriter::flush()
{
    if (mBufferFilled > 0 && mFd >= 0) {
        ::write(mFd, getBufferBegin(), mBufferFilled);
        ::fsync(mFd);
        mBufferFilled = 0;
        mBufferEmpty = getBufferBegin();
    }
}


inline void FileULogWriter::write(size_t size, bool isCompleted)
{
    mBufferFilled += size;
    mBufferEmpty += size;
    if (__unlikely(mBufferFilled + __CAM_ULOG_DETAILS_BUFFER_SIZE__ + 100 >= BUFFER_SIZE)) {
        flush();
    }

    mBytesWritten += size;
    if (__unlikely(mBytesWritten > MAX_FILE_SIZE && isCompleted)) {
        close();
        open(false);

        if (mFd >= 0) {
            static const char * const BEGIN_TAG = "-- CONTINUED --\n";
            ::write(mFd, const_cast<char*>(BEGIN_TAG), strlen(BEGIN_TAG));
        }
    }
}


#define HEADER_FORMAT_D ".%06ld %5d %5d D ULog    : "
#define HEADER_FORMAT_I ".%06ld %5d %5d I ULog    : "
#define HEADER_ARGUMENTS(logHeader) (logHeader->timeStamp.tv_nsec / 1000), logHeader->pid, logHeader->tid
#define HEADER_FORMAT_TAG ".%06ld %5d %5d %c %-8s: "


void FileULogWriter::onLogEnter(const LogEnter *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%u M[%s:%x] +  :%s #%u\n",
        HEADER_ARGUMENTS(log), getRequestTypeName(log->requestTypeId), log->requestSerial,
        getModuleName(log->moduleId), log->moduleId, log->tag, log->logSerial);

    write(nPrinted, true);
}


void FileULogWriter::onLogExit(const LogExit *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%u M[%s:%x] -  :%s #%u\n",
        HEADER_ARGUMENTS(log), getRequestTypeName(log->requestTypeId), log->requestSerial,
        getModuleName(log->moduleId), log->moduleId, log->tag, log->logSerial);

    write(nPrinted, true);
}


void FileULogWriter::onLogDiscard(const LogDiscard *log)
{
    int nRequests = log->numOfRequestSerials;

    if (nRequests == 1) {
        write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

        size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%u M[%s:%x] - (discard)  :%s #%u\n",
            HEADER_ARGUMENTS(log),
            getRequestTypeName(log->requestTypeId), log->requestSerialList[0],
            getModuleName(log->moduleId), log->moduleId, log->tag, log->logSerial);
        write(nPrinted, true);
    } else {
        char reqBuffer[256]; // can be optimized

        const RequestSerial *requestSerialList = log->requestSerialList;
        while (nRequests > 0) {
            write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

            size_t nReqPrinted = printIntArray(reqBuffer, sizeof(reqBuffer), requestSerialList, nRequests);
            nRequests -= nReqPrinted;
            requestSerialList += nReqPrinted;
            size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%s M[%s:%x] - (discard) cont:%d  :%s #%u\n",
                HEADER_ARGUMENTS(log),
                getRequestTypeName(log->requestTypeId), reqBuffer,
                getModuleName(log->moduleId), log->moduleId, nRequests, log->tag, log->logSerial);
            write(nPrinted, true);
        }
    }
}


void FileULogWriter::onLogSubreqs(const LogSubreqs *log)
{
    int nSubreqs = log->numOfSubreqSerials;

    if (nSubreqs == 1) {
        write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));
        size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%u -> R %s:%u genOn M[%s:%x]  :%s #%u\n",
            HEADER_ARGUMENTS(log),
            getRequestTypeName(log->requestTypeId), log->requestSerial,
            getRequestTypeName(log->subrequestTypeId), log->subrequestSerialList[0],
            getModuleName(log->moduleId), log->moduleId, log->tag, log->logSerial);
        write(nPrinted, true);
    } else {
        char reqBuffer[256];

        const RequestSerial *subrequestSerialList = log->subrequestSerialList;
        while (nSubreqs > 0) {
            write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

            size_t nReqPrinted = printIntArray(reqBuffer, sizeof(reqBuffer), log->subrequestSerialList, nSubreqs);
            nSubreqs -= nReqPrinted;
            subrequestSerialList += nReqPrinted;
            size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "R %s:%u -> R %s:%s genOn M[%s:%x] cont:%d  :%s #%u\n",
                HEADER_ARGUMENTS(log),
                getRequestTypeName(log->requestTypeId), log->requestSerial,
                getRequestTypeName(log->subrequestTypeId), reqBuffer + 1,
                getModuleName(log->moduleId), log->moduleId, nSubreqs, log->tag, log->logSerial);
            write(nPrinted, true);
        }
    }
}


void FileULogWriter::writeModuleList(const ModuleId *coModuleList, int nModules)
{
    while (nModules > 0) {
        if (nModules >= 3) {
            size_t nPrinted = snprintf(getBuffer(), getBufferSize(), " M[%s] M[%s] M[%s]",
                getModuleName(coModuleList[0]), getModuleName(coModuleList[1]), getModuleName(coModuleList[2]));
            write(nPrinted);
            nModules -= 3;
            coModuleList += 3;
        } else if (nModules >= 2) {
            size_t nPrinted = snprintf(getBuffer(), getBufferSize(), " M[%s] M[%s]",
                getModuleName(coModuleList[0]), getModuleName(coModuleList[1]));
            write(nPrinted);
            nModules -= 2;
            coModuleList += 2;
        } else if (nModules >= 1) {
            size_t nPrinted = snprintf(getBuffer(), getBufferSize(), " M[%s]", getModuleName(coModuleList[0]));
            write(nPrinted);
            nModules -= 1;
            coModuleList += 1;
        }
    }
}


void FileULogWriter::onLogPathDiv(const LogPathDiv *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_D "R %s:%u  M[%s:%x] divTo",
        HEADER_ARGUMENTS(log),
        getRequestTypeName(log->requestTypeId), log->requestSerial,
        getModuleName(log->moduleId), log->moduleId);
    write(nPrinted);

    writeModuleList(log->coModuleList, log->numOfCoModules);

    nPrinted = snprintf(getBuffer(), getBufferSize(), "  :%s\n", log->tag);
    write(nPrinted, true);
}


void FileULogWriter::onLogPathJoin(const LogPathJoin *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_D "R %s:%u  M[%s:%x] joinFrom",
        HEADER_ARGUMENTS(log),
        getRequestTypeName(log->requestTypeId), log->requestSerial,
        getModuleName(log->moduleId), log->moduleId);
    write(nPrinted);

    writeModuleList(log->coModuleList, log->numOfCoModules);

    nPrinted = snprintf(getBuffer(), getBufferSize(), "  :%s\n", log->tag);
    write(nPrinted, true);
}


void FileULogWriter::onLogFunc(const LogFunc *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = 0;
    if ((log->lifeTag & ~EXIT_BIT) == API_ENTER) {
        nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "[%s/%s] %c  :%s #%u\n",
            HEADER_ARGUMENTS(log), getModuleName(log->moduleId), log->funcName,
            ((log->lifeTag & EXIT_BIT) ? '-' : '+'), log->tag, log->logSerial);
    } else {
        nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_I "[%s/%s] %c #%u\n",
            HEADER_ARGUMENTS(log), log->tag, log->funcName,
            ((log->lifeTag & EXIT_BIT) ? '-' : '+'), log->logSerial);
    }

    write(nPrinted, true);
}


void FileULogWriter::onLogFuncExt(const LogFuncExt *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = 0;
    if ((log->lifeTag & ~EXIT_BIT) == API_ENTER) {
        nPrinted = snprintf(getBuffer(), getBufferSize(),
            HEADER_FORMAT_I "[%s/%s] %c (0x%" PRIxPTR ",0x%" PRIxPTR ",0x%" PRIxPTR ")  :%s #%u\n",
            HEADER_ARGUMENTS(log), getModuleName(log->moduleId), log->funcName,
            ((log->lifeTag & EXIT_BIT) ? '-' : '+'),
            log->values[0], log->values[1], log->values[2],
            log->tag, log->logSerial);
    } else {
        nPrinted = snprintf(getBuffer(), getBufferSize(),
            HEADER_FORMAT_I "[%s/%s] %c (0x%" PRIxPTR ",0x%" PRIxPTR ",0x%" PRIxPTR ") #%u\n",
            HEADER_ARGUMENTS(log), log->tag, log->funcName,
            ((log->lifeTag & EXIT_BIT) ? '-' : '+'),
            log->values[0], log->values[1], log->values[2],
            log->logSerial);
    }

    write(nPrinted, true);
}


void FileULogWriter::onLogDetails(const LogDetails *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    char levelChar = 'D';
    switch (log->detailsType) {
    case DETAILS_ERROR:   levelChar = 'E'; break;
    case DETAILS_WARNING: levelChar = 'W'; break;
    case DETAILS_DECISION_KEY:
    case DETAILS_INFO:    levelChar = 'I'; break;
    case DETAILS_DEBUG:   levelChar = 'D'; break;
    default:              levelChar = 'V'; break;
    }

    size_t nPrinted = 0;
    if (log->requestTypeId == REQ_INVALID_ID) {
        nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_TAG "%s\n",
            HEADER_ARGUMENTS(log), levelChar, log->tag, log->content);
    } else {
        if (__unlikely(log->detailsType <= DETAILS_WARNING)) {
            nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_TAG "R %s:%u %s M[%s:%x]\n",
                HEADER_ARGUMENTS(log), levelChar, log->tag, getRequestTypeName(log->requestTypeId), log->requestSerial,
                log->content, getModuleName(log->moduleId), log->moduleId);
        } else {
            nPrinted = snprintf(getBuffer(), getBufferSize(), HEADER_FORMAT_TAG "R %s:%u %s\n",
                HEADER_ARGUMENTS(log), levelChar, log->tag, getRequestTypeName(log->requestTypeId), log->requestSerial,
                log->content);
        }
    }

    write(nPrinted, true);
}


bool FileULogWriter::writeOneLine(const char *prefix, const char *str, const char *&next)
{
    if (str[0] == '\0')
        return false;

    char buffer[512];

    size_t lineEnd = 0;
    while (lineEnd < sizeof(buffer) - 1 &&
           str[lineEnd] != '\n' && str[lineEnd] != '\0')
    {
        lineEnd++;
    }

    memcpy(buffer, str, lineEnd);
    buffer[lineEnd] = '\n';

    ::write(mFd, prefix, ::strlen(prefix));
    ::write(mFd, buffer, lineEnd + 1);

    if (str[lineEnd] == '\0') {
        return false;
    } else if (str[lineEnd] == '\n') {
        next = str + lineEnd + 1;
    } else if (lineEnd > 0) {
        next = str + lineEnd;
    } else {
        return false;
    }

    return true;
}


void FileULogWriter::onLogTimeout(const LogTimeout *log)
{
    write(strftime(getBuffer(), getBufferSize(), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec)));

    size_t nPrinted = 0;
    if (log->tid > 0) {
        nPrinted = snprintf(getBuffer(), getBufferSize(),
            HEADER_FORMAT_TAG "TIMEOUT M[%s:%x] tid=%d %s, executed %d ms\n",
            HEADER_ARGUMENTS(log), 'E', "ULog", getModuleName(log->moduleId), log->moduleId,
            log->tid, log->guardTarget, log->elapsedMs);
    } else {
        nPrinted = snprintf(getBuffer(), getBufferSize(),
            HEADER_FORMAT_TAG "TIMEOUT %s, executed %d ms\n",
            HEADER_ARGUMENTS(log), 'E', "ULog", log->guardTarget, log->elapsedMs);
    }

    write(nPrinted, true);
    flush();

    if (mFd > 0) {
        char timestamp[40];
        strftime(timestamp, sizeof(timestamp), ULOG_TIME_FORMAT, gmtime(&log->timeStamp.tv_sec));
        char header[128];
        snprintf(header, sizeof(header), "%s" HEADER_FORMAT_TAG, timestamp, HEADER_ARGUMENTS(log), 'I', "ULog");

        // Print backtraces
        std::string bt;
        UnwindCurProcessBT(&bt);
        const char *str = bt.c_str();
        while (writeOneLine(header, str, str))
            ; // Print until end

        if (ULogRuntimeDiag::isEnabled())
            ULogRTDiagImpl::get().dumpToFile(mFd, header);

        ::fsync(mFd);
    }
}


void FileULogWriter::writeString(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    size_t nPrinted = vsnprintf(getBuffer(), getBufferSize(), format, args);
    va_end(args);

    write(nPrinted, true);
}


FileULoggerBase::FileULoggerBase() :
    mClockId(ANDROID_CLOCK_ID), mContinueWorking(false), mLogSerial(0), mNeHookLevel(0), mSigWriter(nullptr)
{
    mPid = static_cast<int>(getpid());
    mActiveBuffer = std::make_unique<Buffer>();

    if (property_get_int32(ULOG_FILE_MONOCLOCK_PROP_NAME, 0) == 1) {
        mClockId = CLOCK_MONOTONIC;
    }

    memset(&mMySigSegvAction, 0, sizeof(struct sigaction));
    memset(&mMySigBusAction, 0, sizeof(struct sigaction));
    memset(&mOldSigSegvAction, 0, sizeof(struct sigaction));
    memset(&mOldSigBusAction, 0, sizeof(struct sigaction));
}


void FileULoggerBase::getFolderPath(char *folderPath, size_t bufferSize)
{
    property_get(ULOG_FOLDERPATH_PROP_NAME, folderPath, ULOG_FOLDERPATH_DEFAULT);

    int ret = mkdir(folderPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ret == 0 || errno == EEXIST)
        return;

    ULOG_IMP_LOGE("mkdir(%s) failed, errno = %d", folderPath, errno);

    strncpy(folderPath, "/data/vendor/cam_ulog", bufferSize);
    ret = mkdir(folderPath, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    if (ret == 0 || errno == EEXIST)
        return;

    strncpy(folderPath, "/data/vendor", bufferSize);
}


char* FileULoggerBase::getTimeStamp(clockid_t clockId, char *buffer, size_t bufferSize)
{
    char *bufferTail = buffer;

    timespec timeStamp;
    clock_gettime(clockId, &timeStamp);
    size_t secLen = strftime(bufferTail, bufferSize, ULOG_TIME_FORMAT, gmtime(&timeStamp.tv_sec));
    bufferTail += secLen;
    bufferSize -= secLen;
    snprintf(bufferTail, bufferSize, ".%06ld", (timeStamp.tv_nsec / 1000));

    return buffer;
}


inline std::unique_ptr<FileULoggerBase::Buffer> FileULoggerBase::allocEmptyBuffer()
{
    std::unique_ptr<Buffer> empty;

    if (mEmptyBuffers.empty()) {
        empty = std::make_unique<Buffer>();
        std::atomic_thread_fence(std::memory_order_release);
    } else {
        // LIFO for page locality
        empty = std::move(mEmptyBuffers.back());
        mEmptyBuffers.pop_back();
    }

    return empty;
}


bool FileULoggerBase::allocLogSpace(size_t size, LogHeader **logSpace, Buffer **activeBuffer, unsigned int *logSerial)
{
    if (sizeof(void*) == 4) // compile-time optimized
        size = (size + 3) & ~(0x3);
    else
        size = (size + 7) & ~(0x7);

    if (__unlikely(size > BUFFER_SIZE)) {
        ULOG_IMP_LOGE("Log size too large: %zu", size);
        return false;
    }

    LogHeader *header = nullptr;

    {
        // mBufferMutex is only to protect space allocation
        // Writing can be done simutaneously, but syncronized with mNumOfWriting
        std::unique_lock<std::mutex> lock(mBufferMutex);

        if (__unlikely(mActiveBuffer->mEmptySize < size)) {
            mActiveBuffer->mIsTerminated.store(true, std::memory_order_relaxed);
            mToBeFlushed.emplace_back(std::move(mActiveBuffer));
            mActiveBuffer = allocEmptyBuffer();
            mBufferCond.notify_one();
        }

        header = reinterpret_cast<LogHeader*>(mActiveBuffer->mNextEmpty);
        header->type = INVALID;
        header->size = size;
        // For NE hook, I hope the INVALID can be effective first
        // But it is not economical to add release fence here
        *activeBuffer = mActiveBuffer.get();
        // coverity[side_effect_free : FALSE]
        mActiveBuffer->mNumOfWriting.fetch_add(1, std::memory_order_relaxed);
        mActiveBuffer->mNextEmpty += size;
        mActiveBuffer->mEmptySize -= size;

        if (logSerial != nullptr)
            *logSerial = getLogSerial();
    }

    header->pid = getPid();
    header->tid = getTid();
    clock_gettime(mClockId, &(header->timeStamp));

    *logSpace = header;

    FULOG_DBG("size = %zu, %p of buffer %p", size, *logSpace, *activeBuffer);

    return true;
}


inline void FileULoggerBase::writeLogDone(Buffer *activeBuffer, LogHeader *header, LogType type)
{
    if (__unlikely(mNeHookLevel > 0)) {
        std::atomic_thread_fence(std::memory_order_release);
    }

    header->type = type; // was INVALID

    if (__unlikely(
        activeBuffer->mNumOfWriting.fetch_sub(1, std::memory_order_release) == 1 &&
        activeBuffer->mIsTerminated.load(std::memory_order_relaxed)))
    {
        // The last log to be written in the buffer, wake up flush thread
        mBufferCond.notify_one();
    }
}


inline size_t FileULoggerBase::safeStrlen(const char *str, size_t maxLen)
{
    size_t n = 0;
    const char *c = str;
    while (*c != '\0' && n < maxLen) { // Manually unroll
        n++; c++;
        if (*c == '\0') break;
        n++; c++;
        if (*c == '\0') break;
        n++; c++;
        if (*c == '\0') break;
        n++; c++;
    }

    return (n < maxLen) ? n : maxLen;
}


inline size_t FileULoggerBase::safeStrncpy(char *dest, const char *src, size_t size)
{
    // We have calculated the string length in size, use memcpy() for efficiency
    memcpy(dest, src, size);
    dest[size - 1] = '\0';
    // coverity[overflow_sink : FALSE]
    return size - 1;
}


void FileULoggerBase::onLogEnter(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, RequestSerial requestSerial)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogEnter) + tagSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogEnter *log = reinterpret_cast<LogEnter*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->requestSerial = requestSerial;
    log->requestTypeId = requestTypeId;
    // We have allocated sufficient space for tag
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, ENTER);
}


void FileULoggerBase::onLogExit(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, RequestSerial requestSerial)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogExit) + tagSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogExit *log = reinterpret_cast<LogExit*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->requestSerial = requestSerial;
    log->requestTypeId = requestTypeId;
    // We have allocated sufficient space for tag
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, EXIT);
}


void FileULoggerBase::onLogDiscard(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, const RequestSerial *requestSerialList, size_t n)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t reqListSize = sizeof(RequestSerial) * n;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogDiscard) + reqListSize + tagSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogDiscard *log = reinterpret_cast<LogDiscard*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->requestTypeId = requestTypeId;
    log->numOfRequestSerials = n;
    static_assert(sizeof(log->requestSerialList[0]) == sizeof(requestSerialList[0]),
        "Type of requestSerialList should be the same for memcpy");
    memcpy(log->requestSerialList, requestSerialList, sizeof(log->requestSerialList[0]) * n);
    log->tag = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogDiscard) + reqListSize);
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, DISCARD);
}


void FileULoggerBase::onLogSubreqs(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, RequestSerial requestSerial,
    RequestTypeId subrequestTypeId, const RequestSerial *subrequestSerialList, size_t n)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t subreqListSize = sizeof(RequestSerial) * n;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogSubreqs) + subreqListSize + tagSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogSubreqs *log = reinterpret_cast<LogSubreqs*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->requestTypeId = requestTypeId;
    log->requestSerial = requestSerial;
    log->subrequestTypeId = subrequestTypeId;
    log->numOfSubreqSerials = n;
    static_assert(sizeof(log->subrequestSerialList[0]) == sizeof(subrequestSerialList[0]),
        "Type of subrequestSerialList should be the same for memcpy");
    memcpy(log->subrequestSerialList, subrequestSerialList, sizeof(log->subrequestSerialList[0]) * n);
    log->tag = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogSubreqs) + subreqListSize);
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, SUBREQS);
}


void FileULoggerBase::onLogPathDiv(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, RequestSerial requestSerial,
    const ModuleId *toModuleIdList, size_t n)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t coModuleListSize = sizeof(ModuleId) * n;

    if (__unlikely( !allocLogSpace(sizeof(LogPathDiv) + coModuleListSize + tagSize, &dataBuffer, &activeBuffer, nullptr) ))
        return;

    LogPathDiv *log = reinterpret_cast<LogPathDiv*>(dataBuffer);
    log->moduleId = moduleId;
    log->requestTypeId = requestTypeId;
    log->requestSerial = requestSerial;
    log->numOfCoModules = n;
    static_assert(sizeof(log->coModuleList[0]) == sizeof(toModuleIdList[0]),
        "Type of coModuleList should be the same for memcpy");
    memcpy(log->coModuleList, toModuleIdList, sizeof(log->coModuleList[0]) * n);
    log->tag = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogPathDiv) + coModuleListSize);
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, PATH_DIV);
}


void FileULoggerBase::onLogPathJoin(ModuleId moduleId, const char *tag, RequestTypeId requestTypeId, RequestSerial requestSerial,
    const ModuleId *fromModuleIdList, size_t n)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t coModuleListSize = sizeof(ModuleId) * n;

    if (__unlikely( !allocLogSpace(sizeof(LogPathJoin) + coModuleListSize + tagSize, &dataBuffer, &activeBuffer, nullptr) ))
        return;

    LogPathJoin *log = reinterpret_cast<LogPathJoin*>(dataBuffer);
    log->moduleId = moduleId;
    log->requestTypeId = requestTypeId;
    log->requestSerial = requestSerial;
    log->numOfCoModules = n;
    static_assert(sizeof(log->coModuleList[0]) == sizeof(fromModuleIdList[0]),
        "Type of coModuleList should be the same for memcpy");
    memcpy(log->coModuleList, fromModuleIdList, sizeof(log->coModuleList[0]) * n);
    log->tag = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogPathJoin) + coModuleListSize);
    safeStrncpy(log->tag, tag, tagSize);

    writeLogDone(activeBuffer, dataBuffer, PATH_JOIN);
}


void FileULoggerBase::onLogFuncLife(ModuleId moduleId, const char *tag, const char *funcName, FuncLifeTag lifeTag)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    // Can we hold the pointer of funcName instead of content copy?
    // Yes, but can not improve significantly and will be unsafe some day.
    size_t funcNameSize = safeStrlen(funcName) + 1;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogFunc) + tagSize + funcNameSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogFunc *log = reinterpret_cast<LogFunc*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->lifeTag = lifeTag;
    safeStrncpy(log->tag, tag, tagSize);
    log->funcName = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogFunc) + tagSize);
    safeStrncpy(log->funcName, funcName, funcNameSize);

    writeLogDone(activeBuffer, dataBuffer, FUNC_LIFE);
}


void FileULoggerBase::onLogFuncLifeExt(ModuleId moduleId, const char *tag, const char *funcName, FuncLifeTag lifeTag,
    std::intptr_t v1, std::intptr_t v2, std::intptr_t v3)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t funcNameSize = safeStrlen(funcName) + 1;
    unsigned int logSerial = 0;

    if (__unlikely( !allocLogSpace(sizeof(LogFuncExt) + tagSize + funcNameSize, &dataBuffer, &activeBuffer, &logSerial) ))
        return;

    LogFuncExt *log = reinterpret_cast<LogFuncExt*>(dataBuffer);
    log->logSerial = logSerial;
    log->moduleId = moduleId;
    log->values[0] = v1;
    log->values[1] = v2;
    log->values[2] = v3;
    log->lifeTag = lifeTag;
    safeStrncpy(log->tag, tag, tagSize);
    log->funcName = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogFuncExt) + tagSize);
    safeStrncpy(log->funcName, funcName, funcNameSize);

    writeLogDone(activeBuffer, dataBuffer, FUNC_LIFE_EXT);
}


void FileULoggerBase::onLogDetails(ModuleId moduleId, const char *tag, DetailsType type,
    RequestTypeId requestTypeId, RequestSerial requestSerial, const char *content, size_t contentLen)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t tagSize = safeStrlen(tag) + 1;
    size_t contentSize = contentLen + 1;
    if (__unlikely(contentSize > 512))
        contentSize = 512;

    if (__unlikely( !allocLogSpace(sizeof(LogDetails) + tagSize + contentSize, &dataBuffer, &activeBuffer, nullptr) ))
        return;

    LogDetails *log = reinterpret_cast<LogDetails*>(dataBuffer);
    log->moduleId = moduleId;
    log->requestSerial = requestSerial;
    log->requestTypeId = requestTypeId;
    log->detailsType = type;
    safeStrncpy(log->tag, tag, tagSize);
    log->content = reinterpret_cast<char*>(reinterpret_cast<intptr_t>(log) + sizeof(LogDetails) + tagSize);
    safeStrncpy(log->content, content, contentSize);

    writeLogDone(activeBuffer, dataBuffer, DETAILS);
}


void FileULoggerBase::onTimeout(ModuleId moduleId, int guardTid, const char *guardTarget, int elapsedMs)
{
    LogHeader *dataBuffer = nullptr;
    Buffer *activeBuffer;
    size_t targetNameSize = safeStrlen(guardTarget) + 1;

    if (__unlikely( !allocLogSpace(sizeof(LogTimeout) + targetNameSize, &dataBuffer, &activeBuffer, nullptr) ))
        return;

    LogTimeout *log = reinterpret_cast<LogTimeout*>(dataBuffer);
    log->tid = guardTid;
    log->moduleId = moduleId;
    log->elapsedMs = elapsedMs;
    safeStrncpy(log->guardTarget, guardTarget, targetNameSize);

    writeLogDone(activeBuffer, dataBuffer, GUARD_TIMEOUT);
}


inline void FileULoggerBase::writeTo(FileULogWriter &writer, const LogHeader *header)
{
    switch (header->type) {
    case ENTER: {
        const LogEnter *logEnter = static_cast<const LogEnter*>(header);
        writer.onLogEnter(logEnter);
        break;
        }
    case EXIT: {
        const LogExit *logExit = static_cast<const LogExit*>(header);
        writer.onLogExit(logExit);
        break;
        }
    case DISCARD: {
        const LogDiscard *logDiscard = static_cast<const LogDiscard*>(header);
        writer.onLogDiscard(logDiscard);
        break;
        }
    case SUBREQS: {
        const LogSubreqs *logSubreqs = static_cast<const LogSubreqs*>(header);
        writer.onLogSubreqs(logSubreqs);
        break;
        }
    case PATH_DIV: {
        const LogPathDiv *logPathDiv = static_cast<const LogPathDiv*>(header);
        writer.onLogPathDiv(logPathDiv);
        break;
        }
    case PATH_JOIN: {
        const LogPathJoin *logPathJoin = static_cast<const LogPathJoin*>(header);
        writer.onLogPathJoin(logPathJoin);
        break;
        }
    case FUNC_LIFE: {
        const LogFunc *logFunc = static_cast<const LogFunc*>(header);
        writer.onLogFunc(logFunc);
        break;
        }
    case FUNC_LIFE_EXT: {
        const LogFuncExt *logFuncExt = static_cast<const LogFuncExt*>(header);
        writer.onLogFuncExt(logFuncExt);
        break;
        }
    case DETAILS: {
        const LogDetails *logDetails = static_cast<const LogDetails*>(header);
        writer.onLogDetails(logDetails);
        break;
        }
    case GUARD_TIMEOUT: {
        const LogTimeout *logTimeout = static_cast<const LogTimeout*>(header);
        writer.onLogTimeout(logTimeout);
        break;
        }
    default:
        break;
    }

}


FileULogger::FileULogger() : mWriter(), mIsFlushing(false)
{
}


void FileULogger::onInit()
{
    char folderPath[128];
    getFolderPath(folderPath, sizeof(folderPath));
    mWriter.setFolder(folderPath);

    if (mWriter.open(true)) {
        mContinueWorking.store(true, std::memory_order_release);
        mFlushThread = std::thread(&FileULogger::flushThreadEntry, this);
    }

    initHook();
}


void FileULogger::onUninit()
{
    uninitHook();

    if (mContinueWorking.load(std::memory_order_relaxed)) {
        {
            std::unique_lock<std::mutex> lock(mBufferMutex);
            mContinueWorking.store(false, std::memory_order_release);
            mBufferCond.notify_all();
        }

        mFlushThread.join();

        std::unique_lock<std::mutex> lock(mBufferMutex);
        if (mIsFlushing) {
            mIsFlushing = false;
            mFlushDone.notify_all();
        }

        mWriter.close();
    }
}


void FileULogger::onFlush(int waitDoneSec)
{
    std::unique_lock<std::mutex> lock(mBufferMutex);

    mIsFlushing = true;
    if (mActiveBuffer->hasData()) {
        mActiveBuffer->mIsTerminated.store(true, std::memory_order_relaxed);
        mToBeFlushed.emplace_back(std::move(mActiveBuffer));
        mActiveBuffer = allocEmptyBuffer();
    }

    mBufferCond.notify_all();

    if (waitDoneSec > 0 && mFlushThread.joinable()) {
        for (int n = 0; mIsFlushing && n < waitDoneSec; n++) {
            mFlushDone.wait_for(lock, std::chrono::milliseconds(1000));
        }
    }
}


void FileULogger::onTimeout(ModuleId moduleId, int guardTid, const char *guardTarget, int elapsedMs)
{
    FileULoggerBase::onTimeout(moduleId, guardTid, guardTarget, elapsedMs);
    FileULogger::onFlush(5);

    ULogger::onTimeout(moduleId, guardTid, guardTarget, elapsedMs);
}


void FileULogger::flushThreadEntry()
{
    androidSetThreadName("FileULogger");

    static constexpr int PRIORITY_LEVEL[] = {
        android::PRIORITY_LOWEST,
        android::PRIORITY_BACKGROUND,
        android::PRIORITY_NORMAL
    };
    static constexpr int PRIORITY_LEVEL_MAX = sizeof(PRIORITY_LEVEL) / sizeof(PRIORITY_LEVEL[0]) - 1;
    int currentPriority = 0;
    androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);

    ULOG_IMP_LOGI("File ULogger is running");

    auto continueWorking = [this] {
        return (mContinueWorking.load(std::memory_order_relaxed) ||
                !mToBeFlushed.empty() ||
                mActiveBuffer->hasData());
    };

    unsigned int nLogWritten = 0;
    while (continueWorking()) {
        Buffer *writingBuffer = nullptr;

        {
            std::unique_lock<std::mutex> lock(mBufferMutex);

            while (continueWorking()) {
                if (mToBeFlushed.empty()) {
                    if (mIsFlushing) {
                        mIsFlushing = false;
                        mFlushDone.notify_all();
                    }

                    if (currentPriority > 0) {
                        currentPriority--; // Slow down
                        androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);
                    }

                    if (mContinueWorking.load(std::memory_order_relaxed)) {
                        mBufferCond.wait_for(lock, std::chrono::milliseconds(1000));

                        if (!continueWorking())
                            break;
                    } else {
                        // Stopped but there may be something to be flushed
                        // We don't wait
                    }

                    if (mToBeFlushed.empty() && mActiveBuffer->hasData()) {
                        mActiveBuffer->mIsTerminated.store(true, std::memory_order_release);
                        if (mActiveBuffer->mNumOfWriting.load(std::memory_order_acquire) == 0) {
                            mToBeFlushed.emplace_back(std::move(mActiveBuffer));
                            mActiveBuffer = allocEmptyBuffer();
                            writingBuffer = mToBeFlushed.front().get();
                            break;
                        } else {
                            mToBeFlushed.emplace_back(std::move(mActiveBuffer));
                            mActiveBuffer = allocEmptyBuffer();
                            mBufferCond.wait_for(lock, std::chrono::milliseconds(100));
                        }
                    }
                } else {
                    if (currentPriority < PRIORITY_LEVEL_MAX &&
                        (mToBeFlushed.size() > 3 || mIsFlushing))
                    {
                        // We adjust the priority according to the number of pending log
                        currentPriority = PRIORITY_LEVEL_MAX;
                        androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);
                    }

                    while (mToBeFlushed.front()->mNumOfWriting.load(std::memory_order_acquire) > 0) {
                        mBufferCond.wait_for(lock, std::chrono::milliseconds(200));
                    }

                    // Reserve the buffer in mToBeFlushed for NE flush
                    writingBuffer = mToBeFlushed.front().get();
                    break;
                }
            }
        }

        // writingBuffer shall not be nullptr when exit above loop
        // Unless stopped
        if (writingBuffer == nullptr)
            break;

        const LogHeader *header = reinterpret_cast<const LogHeader*>(writingBuffer->getData());
        intptr_t filled = writingBuffer->mNextEmpty - reinterpret_cast<intptr_t>(writingBuffer->getData());
        intptr_t printed = 0;
        while (printed < filled) {
            writeTo(mWriter, header);
            printed += static_cast<intptr_t>(header->size);
            header = reinterpret_cast<const LogHeader*>(
                reinterpret_cast<intptr_t>(header) + static_cast<intptr_t>(header->size));
            nLogWritten++;
        }

        mWriter.flush();

        if (mClockId != ANDROID_CLOCK_ID) {
            char monoTimeStamp[40], realTimeStamp[40];
            getTimeStamp(CLOCK_MONOTONIC, monoTimeStamp, sizeof(monoTimeStamp));
            getTimeStamp(CLOCK_REALTIME, realTimeStamp, sizeof(realTimeStamp));
            ULOG_IMP_LOGD("File ULogger: %u logs was written; mono = %s, real = %s", nLogWritten, monoTimeStamp, realTimeStamp);
        } else {
            ULOG_IMP_LOGD("File ULogger: %u logs was written", nLogWritten);
        }

        {
            std::unique_lock<std::mutex> lock(mBufferMutex);

            writingBuffer->clear();
            if (mEmptyBuffers.size() >= 3)
                mEmptyBuffers.pop_front();
            mEmptyBuffers.emplace_back(std::move(mToBeFlushed.front()));

            mToBeFlushed.pop_front();
        }
    }

    {
        std::unique_lock<std::mutex> lock(mBufferMutex);
        if (mIsFlushing) {
            mIsFlushing = false;
            mFlushDone.notify_all();
        }
    }

    ULOG_IMP_LOGI("File ULogger stopped");
}


PassiveULogger::PassiveULogger() : mIsFlushing(false)
{
}


void PassiveULogger::onInit()
{
    mContinueWorking.store(true, std::memory_order_release);
    mFlushThread = std::thread(&PassiveULogger::flushThreadEntry, this);

    initHook();
}


void PassiveULogger::onUninit()
{
    uninitHook();

    if (mContinueWorking.load(std::memory_order_relaxed)) {
        {
            std::unique_lock<std::mutex> lock(mBufferMutex);
            mContinueWorking.store(false, std::memory_order_release);
            mBufferCond.notify_all();
        }

        mFlushThread.join();

        if (mIsFlushing) {
            mIsFlushing = false;
            mFlushDone.notify_all();
        }
    }
}


void PassiveULogger::onFlush(int waitDoneSec)
{
    std::unique_lock<std::mutex> lock(mBufferMutex);

    mIsFlushing = true;
    if (mActiveBuffer->hasData()) {
        mActiveBuffer->mIsTerminated.store(true, std::memory_order_relaxed);
        mToBeFlushed.emplace_back(std::move(mActiveBuffer));
        mActiveBuffer = allocEmptyBuffer();
    }

    mBufferCond.notify_all();

    if (waitDoneSec > 0 && mFlushThread.joinable()) {
        for (int n = 0; mIsFlushing && n < waitDoneSec; n++) {
            mFlushDone.wait_for(lock, std::chrono::milliseconds(1000));
        }
    }
}


void PassiveULogger::onTimeout(ModuleId moduleId, int guardTid, const char *guardTarget, int elapsedMs)
{
    FileULoggerBase::onTimeout(moduleId, guardTid, guardTarget, elapsedMs);
    PassiveULogger::onFlush(5);

    ULogger::onTimeout(moduleId, guardTid, guardTarget, elapsedMs);
}


void PassiveULogger::flushThreadEntry()
{
    androidSetThreadName("PassiveULogger");

    static constexpr int PRIORITY_LEVEL[] = {
        android::PRIORITY_LOWEST,
        android::PRIORITY_NORMAL
    };
    static constexpr int PRIORITY_LEVEL_MAX = sizeof(PRIORITY_LEVEL) / sizeof(PRIORITY_LEVEL[0]) - 1;
    int currentPriority = 0;
    androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);

    int maxKb = property_get_int32(ULOG_PASSIVE_MAX_BUFFER_KB_PROP_NAME, BUFFER_SIZE * 8 / 1024);
    int nBuffersInQueue = (maxKb * 1024 / BUFFER_SIZE) - 2;
    if (nBuffersInQueue < 1)
        nBuffersInQueue = 1;
    maxKb = (nBuffersInQueue + 2) * BUFFER_SIZE / 1024;
    int nDiscarded = 0;

    ULOG_IMP_LOGI("Passive ULogger is running, maxKb = %d KB, buffers = %d+2", maxKb, nBuffersInQueue);

    auto continueWorking = [this] {
        return (mContinueWorking.load(std::memory_order_relaxed));
    };

    unsigned int nLogWritten = 0;
    while (continueWorking()) {
        Buffer *writingBuffer = nullptr;

        {
            std::unique_lock<std::mutex> lock(mBufferMutex);

            while (continueWorking()) {
                if (mToBeFlushed.empty()) {
                    if (mIsFlushing) {
                        mWriter.close();
                        mIsFlushing = false;
                        nLogWritten = 0;
                        mFlushDone.notify_all();
                        ULOG_IMP_LOGI("Passive log flushed.");
                    }

                    if (currentPriority > 0) {
                        currentPriority--;
                        androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);
                    }
                } else {
                    if (currentPriority < PRIORITY_LEVEL_MAX && mIsFlushing) {
                        // Raise priority for flush
                        currentPriority = PRIORITY_LEVEL_MAX;
                        androidSetThreadPriority(getTid(), PRIORITY_LEVEL[currentPriority]);
                    }

                    if (mIsFlushing ||
                        mToBeFlushed.size() > static_cast<size_t>(nBuffersInQueue))
                    {
                        while (mToBeFlushed.front()->mNumOfWriting.load(std::memory_order_acquire) > 0) {
                            mBufferCond.wait_for(lock, std::chrono::milliseconds(200));
                        }

                        // Reserve the buffer in mToBeFlushed for NE flush
                        writingBuffer = mToBeFlushed.front().get();
                        break;
                    }
                }

                mBufferCond.wait_for(lock, std::chrono::milliseconds(1000));
            }
        }

        // writingBuffer shall not be nullptr when exit above loop
        // Unless stopped
        if (writingBuffer == nullptr)
            break;

        if (mIsFlushing) {
            if (!mWriter.isOpened()) {
                char folderPath[128];
                getFolderPath(folderPath, sizeof(folderPath));
                mWriter.setFolder(folderPath);
                mWriter.open();
                nLogWritten = 0;
            }

            if (mWriter.isOpened()) {
                const LogHeader *header = reinterpret_cast<const LogHeader*>(writingBuffer->getData());
                intptr_t filled = writingBuffer->mNextEmpty - reinterpret_cast<intptr_t>(writingBuffer->getData());
                intptr_t printed = 0;
                while (printed < filled) {
                    writeTo(mWriter, header);
                    printed += static_cast<intptr_t>(header->size);
                    header = reinterpret_cast<const LogHeader*>(
                        reinterpret_cast<intptr_t>(header) + static_cast<intptr_t>(header->size));
                    nLogWritten++;
                }

                mWriter.flush();
                ULOG_IMP_LOGD("Passive ULogger: %u logs was written", nLogWritten);
            } else {
                mIsFlushing = false;
                mFlushDone.notify_all();
                continue;
            }
        } else {
            nDiscarded++;
            if ((nDiscarded & 0x7) == 0) {
                ULOG_IMP_LOGI("Passive ULogger is running, %d log buffers was discarded", nDiscarded);
            } else {
                ULOG_IMP_LOGD("Passive ULogger is running, %d log buffers was discarded", nDiscarded);
            }
        }

        writingBuffer->clear();

        {
            std::unique_lock<std::mutex> lock(mBufferMutex);

            if (mEmptyBuffers.size() > 0)
                mEmptyBuffers.pop_front();
            mEmptyBuffers.emplace_back(std::move(mToBeFlushed.front()));

            mToBeFlushed.pop_front();
        }
    }

    {
        std::unique_lock<std::mutex> lock(mBufferMutex);
        if (mIsFlushing) {
            mIsFlushing = false;
            mFlushDone.notify_all();
        }
    }

    ULOG_IMP_LOGW("Passive ULogger stopped");
}


FileULoggerBase *FileULoggerBase::sActiveLogger = nullptr;

void FileULoggerBase::initHook()
{
    sActiveLogger = this;
    int hookNe = property_get_int32(ULOG_FILE_HOOK_NE_PROP_NAME, 0);

    if (hookNe > 0 && mNeHookLevel == 0) {
        mSigWriter.reset(new FileULogWriter);
        char folderPath[128];
        getFolderPath(folderPath, sizeof(folderPath));
        mSigWriter->setFolder(folderPath);

        mNeHookLevel = hookNe;

        std::atomic_thread_fence(std::memory_order_release);

        sigaction(SIGSEGV, nullptr, &mOldSigSegvAction);
        mMySigSegvAction.sa_handler = nullptr;
        mMySigSegvAction.sa_sigaction = &FileULoggerBase::sigHandler;
        mMySigSegvAction.sa_mask = mOldSigSegvAction.sa_mask;
        mMySigSegvAction.sa_flags = SA_SIGINFO;
        mMySigSegvAction.sa_restorer = mOldSigSegvAction.sa_restorer;
        sigaction(SIGSEGV, &mMySigSegvAction, nullptr);

        sigaction(SIGBUS, nullptr, &mOldSigBusAction);
        mMySigBusAction.sa_handler = nullptr;
        mMySigBusAction.sa_sigaction = &FileULoggerBase::sigHandler;
        mMySigBusAction.sa_mask = mOldSigBusAction.sa_mask;
        mMySigBusAction.sa_flags = SA_SIGINFO;
        mMySigBusAction.sa_restorer = mOldSigBusAction.sa_restorer;
        sigaction(SIGBUS, &mMySigBusAction, nullptr);
    }
}


void FileULoggerBase::uninitHook()
{
    if (mNeHookLevel > 0) {
        mNeHookLevel = 0;
        sigaction(SIGSEGV, &mOldSigSegvAction, nullptr);
        sigaction(SIGBUS, &mOldSigBusAction, nullptr);
        std::atomic_thread_fence(std::memory_order_release);
    }
}


inline void FileULoggerBase::sigWriteBuffer(FileULogWriter &writer, Buffer *writingBuffer)
{
    if (writingBuffer == NULL) // possible if the buffer is moving
        return;

    const LogHeader *header = reinterpret_cast<const LogHeader*>(writingBuffer->getData());
    // Make sure buffer will always be cleared before free
    intptr_t filled = writingBuffer->mNextEmpty - reinterpret_cast<intptr_t>(writingBuffer->getData());
    intptr_t printed = 0;
    while (printed < filled) {
        if (header->type != INVALID)
            writeTo(writer, header);
        printed += static_cast<intptr_t>(header->size);
        header = reinterpret_cast<const LogHeader*>(
            reinterpret_cast<intptr_t>(header) + static_cast<intptr_t>(header->size));
    }
}


void FileULoggerBase::sigHandler(int sig, siginfo_t *info, void *ucontext)
{
    const char *sigType = "NE";
    struct sigaction *oldAction = nullptr;

    if (sig == SIGSEGV) {
        sigType = "SIGSEGV";
        oldAction = &(sActiveLogger->mOldSigSegvAction);
        sigaction(SIGSEGV, oldAction, nullptr);
    } else if (sig == SIGBUS) {
        sigType = "SIGBUS";
        oldAction = &(sActiveLogger->mOldSigBusAction);
        sigaction(SIGBUS, oldAction, nullptr);
    }

    if (sActiveLogger != nullptr) {
        FileULogWriter &writer = *(sActiveLogger->mSigWriter.get());
        if (writer.open(true, "NE_", true)) {
            writer.writeString("%s : addr = %p\n", sigType, info->si_addr);

            intptr_t filledInActive = sActiveLogger->mActiveBuffer->mNextEmpty -
                    reinterpret_cast<intptr_t>(sActiveLogger->mActiveBuffer->getData());
            if (sActiveLogger->mNeHookLevel >= 2 || filledInActive < 64 * 200) {
                // The data structure in mToBeFlushed may be not valid, but we just try
                for (std::unique_ptr<Buffer> &buffer : sActiveLogger->mToBeFlushed) {
                    sigWriteBuffer(writer, buffer.get());
                }
            }

            sigWriteBuffer(writer, sActiveLogger->mActiveBuffer.get());
            writer.close();
        }
    }

    if (oldAction != nullptr) {
        if ((oldAction->sa_flags & SA_SIGINFO) &&
            (oldAction->sa_sigaction != nullptr))
        {
            (*oldAction->sa_sigaction)(sig, info, ucontext);
        } else if (oldAction->sa_handler != nullptr) {
            (*oldAction->sa_handler)(sig);
        } else if (oldAction->sa_sigaction != nullptr) {
            (*oldAction->sa_sigaction)(sig, info, ucontext);
        } else {
            raise(sig);
        }
    } else {
        raise(sig);
    }
}


}
}
}
}

