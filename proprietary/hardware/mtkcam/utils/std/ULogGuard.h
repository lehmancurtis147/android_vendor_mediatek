/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2016. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef __MTKCAM_UTILS_STD_ULOG_GUARD_H__
#define __MTKCAM_UTILS_STD_ULOG_GUARD_H__

#include <cstdint>
#include <atomic>
#include <list>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <memory>
#include <vector>
#include <deque>
#include <utility>
#include <time.h>
#include <mtkcam/utils/std/ULog.h>


namespace NSCam {
namespace Utils {
namespace ULog {


struct ThreadGuardData
{
    mutable std::mutex dataMutex;
    int tid;
    int depth;
    int timeoutMs;
    ModuleId firstModuleId;
    timespec firstTimeStamp;
    const char *firstFuncName;
    const char *firstLongFuncName;
    void *firstFrameData;

    ThreadGuardData();
    ~ThreadGuardData();
};


class ULogGuardMonitor
{
    friend class ULogGuard;

public:
    static ULogGuardMonitor sSingleton;

    static ULogGuardMonitor& getSingleton() {
        return sSingleton;
    }

    void registerThread(ThreadGuardData *guardData);
    void unregisterThread(ThreadGuardData *guardData);

private:
    struct RequestRecord {
        ModuleId moduleId;
        RequestTypeId requestType;
        RequestSerial requestSerial;
        int timeoutMs;
        timespec enterTimeStamp;

        RequestRecord() : moduleId(0), requestType(REQ_INVALID_ID), requestSerial(0), timeoutMs(0), enterTimeStamp{0, 0} { }

        void set(ModuleId _moduleId, RequestTypeId _requestType, RequestSerial _requestSerial, timespec _enterTimeStamp, int _timeoutMs) {
            moduleId = _moduleId;
            requestType = _requestType;
            requestSerial = _requestSerial;
            timeoutMs = _timeoutMs;
            enterTimeStamp = _enterTimeStamp;
        }
    };

    class RequestQueue {
    public:
        static constexpr size_t MAX_REQUEST_NUM = 64;

        RequestQueue();

        bool isFull() const {
            return mUnused.empty();
        }

        bool isEmpty() const {
            return mInFlight.empty();
        }

        template <typename ... _T>
        void emplace_back(_T&& ... args);

        template <typename _F>
        bool remove(_F &&match);

        RequestRecord &front() {
            return *(mInFlight.front());
        }

        void pop_front();

        size_t size() const {
            return mInFlight.size();
        }

    private:
        RequestRecord mRequestSlots[MAX_REQUEST_NUM];
        std::vector<RequestRecord*> mUnused;
        std::deque<RequestRecord*> mInFlight;
    };

    struct CpuLoading {
        long runningTime;
        long idleTime;

        CpuLoading() : runningTime(0), idleTime(0) { }
        CpuLoading &operator=(const CpuLoading&) = default;
    };

    std::mutex mMutex;
    std::list<const ThreadGuardData *> mGuardData;
    std::thread mThread;
    std::thread mFinalizer;
    std::condition_variable mCond;
    std::condition_variable mFinalCond;
    bool mContinueRunning;
    std::atomic_int mGuardNumber;
    std::atomic_bool mIsReqGuardEnabled;
    RequestQueue mAppRequests;
    std::list<android::wp<ULogGuard::IFinalizer>> mFinalizers;

    ULogGuardMonitor();
    ~ULogGuardMonitor();

    void start();
    void stop();
    void run();
    void finalize();
    void incGuardNumber();
    void decGuardNumber();
    void registerReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial, int timeoutMs);
    void unregisterReqGuard(ModuleId moduleId, RequestTypeId requestType, RequestSerial requestSerial);
    void registerFinalizer(android::sp<ULogGuard::IFinalizer> &finalizer);

    static bool getCpuLoading(CpuLoading &loading);
    static long getCpuLoadingPercentage(const CpuLoading &prev, const CpuLoading &curr);
};


}
}
}

#endif

