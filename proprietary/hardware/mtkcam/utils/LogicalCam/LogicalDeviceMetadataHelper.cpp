/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/Util/LogicalDeviceMetadataHelper"

#include "MyUtils.h"
#include "LogicalDeviceMetadataHelper.h"
#include <dlfcn.h>

/******************************************************************************
*
*******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("(%d)[%s] " fmt, ::gettid(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

using namespace android;
using namespace NSCam::NSLogicalDeviceMetadataHelper;
/******************************************************************************
 *
 ******************************************************************************/
LogicalDeviceMetadataHelper::
~LogicalDeviceMetadataHelper()
{
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
LogicalDeviceMetadataHelper::
constructStaticMetadata(
    int32_t deviceId,
    std::string deviceName,
    bool bBackSide,
    IMetadata &mtkMetadata
)
{
    char const*const
    logcalDeviceStaticMetadataTypeNames[] =
    {
        "LENS",
        "SENSOR",
        "TUNING_3A",
        "FLASHLIGHT",
        "SCALER",
        //"FEATURE",
        //"CAMERA",
        //"REQUEST",
        NULL
    };
    size_t count = (sizeof(logcalDeviceStaticMetadataTypeNames)/sizeof(char const*)) ;
    std::vector<bool> vMap(count, false);
    //
    for (int i = 0; NULL != logcalDeviceStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = logcalDeviceStaticMetadataTypeNames[i];
        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_DEVICE_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, deviceName.c_str());
        status = impConstructStaticMetadata_by_SymbolName(deviceId, s8Symbol_Sensor, bBackSide, mtkMetadata);
        if  ( OK == status ) {
            MY_LOGD("ckh: load logical device (%s)", s8Symbol_Sensor.string());
            vMap[i] = true;
            continue;
        }
        //
        MY_LOGW_IF(0, "Fail for %s", s8Symbol_Sensor.string());
    }
    //
    for (int i = 0; NULL != logcalDeviceStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = logcalDeviceStaticMetadataTypeNames[i];
        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_PROJECT_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, deviceName.c_str());
        status = impConstructStaticMetadata_by_SymbolName(deviceId, s8Symbol_Sensor, bBackSide, mtkMetadata);
        if  ( OK == status ) {
            MY_LOGD("ckh: load logical device (%s)", s8Symbol_Sensor.string());
            vMap[i] = true;
            continue;
        }
        //
        MY_LOGE_IF(0, "Fail for both %s", s8Symbol_Sensor.string());
    }
    //
    for (int i = 0; NULL != logcalDeviceStaticMetadataTypeNames[i]; i++) {
        if ( vMap[i]==false ) {
            MY_LOGE("Fail to load %s in all PLATFORM/PROJECT combinations", logcalDeviceStaticMetadataTypeNames[i] );
            return NAME_NOT_FOUND;
        }
    }
    //
    return  OK;
}
/******************************************************************************
 *
 ******************************************************************************/
status_t
LogicalDeviceMetadataHelper::
impConstructStaticMetadata_by_SymbolName(
    int32_t deviceId,
    String8 const&      s8Symbol,
    bool bBackSide,
    IMetadata &metadata
)
{
typedef status_t (*PFN_T)(
        IMetadata &         metadata,
        Info const&         info
    );
    Info info(deviceId, bBackSide, s8Symbol.string());
    //
    PFN_T pfn = (PFN_T)::dlsym(RTLD_DEFAULT, s8Symbol.string());
    if  ( ! pfn ) {
        MY_LOGW_IF(1, "%s not found", s8Symbol.string());
        return  NAME_NOT_FOUND;
    }
    //
    status_t const status = pfn(metadata, info);
    MY_LOGI_IF(0, "%s: returns status[%s(%d)]", s8Symbol.string(), ::strerror(-status), -status);
    //
    return  status;
}