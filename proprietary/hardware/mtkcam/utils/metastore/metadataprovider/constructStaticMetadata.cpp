/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "MtkCam/MetadataProvider.constructStatic"
//
#include "MyUtils.h"
//
#include <dlfcn.h>
//
#include <mtkcam/drv/IHalSensor.h>
#include <mtkcam/aaa/IDngInfo.h>
#include <mtkcam/utils/metadata/IMetadataConverter.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
#include <cutils/properties.h>
#include <mtkcam/utils/LogicalCam/IHalLogicalDeviceList.h>
#include <camera_custom_logicaldevice.h>
//
#include <custom_metadata/custom_metadata_tag.h>
//
#include <vector>

/******************************************************************************
 *
 ******************************************************************************/

//#define MY_LOGI(fmt, arg...)    do {XLOGI(LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
//#define MY_LOGD(fmt, arg...)    do {XLOGD(LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
//#define MY_LOGE(fmt, arg...)    do {XLOGE("error" LOG_TAG fmt, ##arg);printf(LOG_TAG fmt "\n", ##arg);} while (0)
#define FUNCTION_LOG_START      MY_LOGD("[%s] - E.", __FUNCTION__)
#define FUNCTION_LOG_END        do {if(!ret) MY_LOGE("[%s] fail", __FUNCTION__); MY_LOGD("[%s] - X. ret=%d", __FUNCTION__, ret);} while(0)
#define FUNCTION_LOG_END_MUM    MY_LOGD("[%s] - X.", __FUNCTION__)

#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%s] " fmt, __FUNCTION__, ##arg)

#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%s] " fmt, __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%s] " fmt, __FUNCTION__, ##arg)

//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)

// 64KB per 1 exif section size and we have 5 app exif section
#define EXIF_H_SIZE 65536*5

/******************************************************************************
 *
 ******************************************************************************/

status_t
MetadataProvider::
impConstructStaticMetadata_by_SymbolName(
    String8 const&      s8Symbol,
    IMetadata &metadata
)
{
typedef status_t (*PFN_T)(
        IMetadata &         metadata,
        Info const&         info
    );
    //
    PFN_T pfn = (PFN_T)::dlsym(RTLD_DEFAULT, s8Symbol.string());
    if  ( ! pfn ) {
        MY_LOGW_IF(1, "%s not found", s8Symbol.string());
        return  NAME_NOT_FOUND;
    }
    //
    status_t const status = pfn(metadata, mInfo);
    MY_LOGI_IF(0, "%s: returns status[%s(%d)]", s8Symbol.string(), ::strerror(-status), -status);
    //
    return  status;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
MetadataProvider::
impConstructStaticMetadata(
    IMetadata &metadata
)
{
    size_t count = (sizeof(kStaticMetadataTypeNames)/sizeof(char const*)) ;
    std::vector<bool> vMap(count, false);
    //
    for (int i = 0; NULL != kStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = kStaticMetadataTypeNames[i];
        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_DEVICE_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, mInfo.getSensorDrvName());
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Sensor, metadata);
        if  ( OK == status ) {
            vMap[i] = true;
            continue;
        }
        //
        String8 const s8Symbol_Common = String8::format("%s_DEVICE_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, "COMMON");
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Common, metadata);
        if  ( OK == status ) {
            vMap[i] = true;
            continue;
        }
        //
        MY_LOGW_IF(0, "Fail for both %s & %s", s8Symbol_Sensor.string(), s8Symbol_Common.string());
    }
    //
    for (int i = 0; NULL != kStaticMetadataTypeNames[i]; i++)
    {
        char const*const pTypeName = kStaticMetadataTypeNames[i];
        status_t status = OK;
        //
        String8 const s8Symbol_Sensor = String8::format("%s_PROJECT_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, mInfo.getSensorDrvName());
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Sensor, metadata);
        if  ( OK == status ) {
            vMap[i] = true;
            continue;
        }
        //
        String8 const s8Symbol_Common = String8::format("%s_PROJECT_%s_%s", PREFIX_FUNCTION_STATIC_METADATA, pTypeName, "COMMON");
        status = impConstructStaticMetadata_by_SymbolName(s8Symbol_Common, metadata);
        if  ( OK == status ) {
            vMap[i] = true;
            continue;
        }
        //
        MY_LOGE_IF(0, "Fail for both %s & %s", s8Symbol_Sensor.string(), s8Symbol_Common.string());
    }
    //
    for (int i = 0; NULL != kStaticMetadataTypeNames[i]; i++) {
        if ( vMap[i]==false ) {
            MY_LOGE("Fail to load %s in all PLATFORM/PROJECT combinations", kStaticMetadataTypeNames[i] );
            return NAME_NOT_FOUND;
        }
    }
    //
    return  OK;
}


/******************************************************************************
 *
 ******************************************************************************/
status_t
MetadataProvider::
constructStaticMetadata(sp<IMetadataConverter> pConverter, camera_metadata*& rpDstMetadata, IMetadata& mtkMetadata)
{
    MY_LOGD("construct static metadata\n");

    status_t status = OK;

    //-----(1)-----//
    //get static informtation from customization (with camera_metadata format)
    //calculate its entry count and data count
    if  ( OK != (status = impConstructStaticMetadata(mtkMetadata)) ) {
        MY_LOGE("Unable evaluate the size for camera static info - status[%s(%d)]\n", ::strerror(-status), -status);
        return  status;
    }
    MY_LOGD("Allocating %d entries from customization", mtkMetadata.count());

    //-----(2.1)------//
    //get static informtation from sensor hal moduls (with IMetadata format)
    IMetadata sensorMetadata = MAKE_HalLogicalDeviceList()->queryStaticInfo(mInfo.getDeviceId());
    MY_LOGD("Allocating %d entries from sensor HAL", sensorMetadata.count());

    //
#if 1
    IMetadata rDngMeta = MAKE_DngInfo(LOG_TAG, (MAKE_HalLogicalDeviceList()->getSensorId(mInfo.getDeviceId()))[0])->getStaticMetadata();
#else
    IMetadata rDngMeta;
#endif
    MY_LOGD("Allocating %d entries from Dng Info", rDngMeta.count());

    //--- (2.1.1) --- //
    //merge.
    sensorMetadata += rDngMeta;
    for (size_t i = 0; i < sensorMetadata.count(); i++)
    {
        IMetadata::Tag_t mTag = sensorMetadata.entryAt(i).tag();
        mtkMetadata.update(mTag, sensorMetadata.entryAt(i));
    }
    MY_LOGD("Allocating %d entries from customization + sensor HAL + Dng Info", mtkMetadata.count());

#if 0
    //-----(2.2)------//
    //get static informtation from other hal moduls (with IMetadata format)
    IMetadata halmetadata = MAKE_HalLogicalDeviceList()->queryStaticInfo(mInfo.getDeviceId());

    //calculate its entry count and data count
    entryCount = 0;
    dataCount = 0;


    status = AndroidMetadata::getIMetadata_dataCount(halmetadata, entryCount, dataCount);
    if (status != OK)
    {
        MY_LOGE("get Imetadata count error - status[%s(%d)", ::strerror(-status), -status);
        return status;
    }

    MY_LOGD(
        "Allocating %d entries, %d extra bytes from HAL modules",
        entryCount, dataCount
    );

    addOrSizeInfo.mEntryCount += entryCount;
    addOrSizeInfo.mDataCount += dataCount;

#endif

    //overwrite
    updateData(mtkMetadata);
    //
    #if (PLATFORM_SDK_VERSION >= 21)
    pConverter->convert(mtkMetadata, rpDstMetadata);
    //
    ::sort_camera_metadata(rpDstMetadata);
    #endif

    return  status;
}


/******************************************************************************
 *
 ******************************************************************************/
template<class T>
struct converter {
    converter( T const& tag, T const& srcFormat, T const& dstFormat, IMetadata& data) {
        IMetadata::IEntry entry = data.entryFor(tag);
        copy( srcFormat, dstFormat, entry);
        data.update(tag, entry);
    }

    void copy( T const& srcFormat, T const& dstFormat, IMetadata::IEntry& entry) {
        T input = MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS_INPUT;
        for(size_t i = 0; i < entry.count(); i+=4) {
            if (entry.itemAt(i, Type2Type<T>())!= srcFormat
                || entry.itemAt(i+3, Type2Type< T >()) == input) {
                continue;
            }
            entry.push_back(dstFormat, Type2Type< T >());
            entry.push_back(entry.itemAt(i+1, Type2Type< T >()), Type2Type< T >());
            entry.push_back(entry.itemAt(i+2, Type2Type< T >()), Type2Type< T >());
            entry.push_back(entry.itemAt(i+3, Type2Type< T >()), Type2Type< T >());
        }
    };
};

void
MetadataProvider::
updateData(IMetadata &rMetadata)
{
    {
        MINT32 maxJpegsize = 0;
        IMetadata::IEntry blobEntry = rMetadata.entryFor(MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS);
        for(size_t i = 0; i < blobEntry.count(); i+=4) {
            if (blobEntry.itemAt(i, Type2Type<MINT32>())!= HAL_PIXEL_FORMAT_BLOB) {
                continue;
            }
            //avaiblable blob size list should order in descedning.
            MSize maxBlob = MSize(blobEntry.itemAt(i+1, Type2Type<MINT32>()),
                            blobEntry.itemAt(i+2, Type2Type<MINT32>()));
            // use the worst buffer size
            MINT32 jpegsize = (MINT32)(maxBlob.size()*2) + EXIF_H_SIZE; //*2 + exif max header size
            if (jpegsize > maxJpegsize) {
                maxJpegsize = jpegsize;
            }
            IMetadata::IEntry entry(MTK_JPEG_MAX_SIZE);
            entry.push_back(maxJpegsize, Type2Type< MINT32 >());
            rMetadata.update(MTK_JPEG_MAX_SIZE, entry);
         }
    }

    // update implementation defined
    {
        converter<MINT32>(
            MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_MIN_FRAME_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_STALL_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_IMPLEMENTATION_DEFINED,
            rMetadata
        );
    }

    // update yv12
    {
        converter<MINT32>(
            MTK_SCALER_AVAILABLE_STREAM_CONFIGURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_MIN_FRAME_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
        //
        converter<MINT64>(
            MTK_SCALER_AVAILABLE_STALL_DURATIONS,
            HAL_PIXEL_FORMAT_YCbCr_420_888, HAL_PIXEL_FORMAT_YV12,
            rMetadata
        );
    }

    // update HDR Request Common Type
    {
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_HDR_FEATURE_HDR_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        availReqEntry.push_back(MTK_HDR_FEATURE_SESSION_PARAM_HDR_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_HDR_FEATURE_HDR_DETECTION_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
        availCharactsEntry.push_back(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_PHOTO , Type2Type< MINT32 >());
        availCharactsEntry.push_back(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_VIDEO , Type2Type< MINT32 >());
        rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
    }

    {
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_EIS_FEATURE_EIS_MODE, Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);
    }

    // update Available vHDR Mode & HDR Modes
    {

        // -----------------
        // -- isHDRSensor --
        // -----------------
        // Available vHDR mode
        IMetadata::IEntry availVhdrEntry = rMetadata.entryFor(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES);
        if(availVhdrEntry.isEmpty()){
            IMetadata::IEntry entry(MTK_HDR_FEATURE_AVAILABLE_VHDR_MODES);
            entry.push_back(MTK_HDR_FEATURE_VHDR_MODE_OFF, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
            availVhdrEntry = entry;
        }
        MBOOL isHDRSensor = isHdrSensor(availVhdrEntry.count());

        // --------------------------
        // -- isSingleFrameSupport --
        // --------------------------
        IMetadata::IEntry singleFrameHdrEntry = rMetadata.entryFor(MTK_HDR_FEATURE_AVAILABLE_SINGLE_FRAME_HDR);
        MBOOL isSingleFrameSupport = (singleFrameHdrEntry.count() > 0)
                    && (singleFrameHdrEntry.itemAt(0, Type2Type<MUINT8>()) == MTK_HDR_FEATURE_SINGLE_FRAME_HDR_SUPPORTED);
        MINT32 singleFrameProp = property_get_int32("debug.camera.hal3.singleFrame", -1);
        isSingleFrameSupport = (singleFrameProp != -1) ? (singleFrameProp > 0) : isSingleFrameSupport;

        // ----------------------
        // -- hdrDetectionMode --
        // ----------------------
        MINT32 hdrDetectionMode = MTKCAM_HDR_DETECTION_MODE; /* 1 : hdr sensor,  2 : generic sensor, 3 : all sensors*/
        // HDR Detection support force switch
        MINT32 hdrDetectProp = property_get_int32("debug.camera.hal3.hdrDetection", 0);
        hdrDetectionMode = (hdrDetectProp != -1) ? hdrDetectProp : hdrDetectionMode;


        // update availHdrPhoto & availHdrVideo Metadata
        updateHdrData(isHDRSensor, isSingleFrameSupport, hdrDetectionMode, rMetadata);

    }

    // update Available 3DNR Mode
    {
        IMetadata::IEntry avail3DNREntry = rMetadata.entryFor(MTK_NR_FEATURE_AVAILABLE_3DNR_MODES);
#ifndef NR3D_SUPPORTED
        avail3DNREntry.clear();
        avail3DNREntry.push_back(MTK_NR_FEATURE_3DNR_MODE_OFF, Type2Type< MINT32 >());
        rMetadata.update(avail3DNREntry.tag(), avail3DNREntry);
#else
        IMetadata::IEntry availSessionEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
        if (!availSessionEntry.isEmpty())
        {
            availSessionEntry.push_back(MTK_NR_FEATURE_3DNR_MODE, Type2Type< MINT32 >());
            rMetadata.update(availSessionEntry.tag(), availSessionEntry);
        }
        else
        {
            IMetadata::IEntry entry(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
            entry.push_back(MTK_NR_FEATURE_3DNR_MODE, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
        }
#endif
    }

#if 1 //MTKCAM_HAVE_MFB_SUPPORT fill Default value = off even MFB is not support
    // update AIS Request Common Type
    MY_LOGD("MTKCAM_HAVE_MFB_SUPPORT = %d", MTKCAM_HAVE_MFB_SUPPORT);
    IMetadata::IEntry availAisModeEntry = rMetadata.entryFor(MTK_MFNR_FEATURE_AVAILABLE_AIS_MODES);
    if(availAisModeEntry.isEmpty()){
        MY_LOGD("availAisModeEntry is empty");
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_MFNR_FEATURE_AIS_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_MFNR_FEATURE_AIS_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry entry(MTK_MFNR_FEATURE_AVAILABLE_AIS_MODES);
        entry.push_back(MTK_MFNR_FEATURE_AIS_OFF , Type2Type< MINT32 >());
        rMetadata.update(entry.tag(), entry);
    }

    // update MFB Request Common Type
    IMetadata::IEntry availMfbModeEntry = rMetadata.entryFor(MTK_MFNR_FEATURE_AVAILABLE_MFB_MODES);
    if(availMfbModeEntry.isEmpty()){
        MY_LOGD("availMfbModeEntry is empty");
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_MFNR_FEATURE_MFB_MODE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_MFNR_FEATURE_MFB_RESULT , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);

        IMetadata::IEntry entry(MTK_MFNR_FEATURE_AVAILABLE_MFB_MODES);
        entry.push_back(MTK_MFNR_FEATURE_MFB_OFF , Type2Type< MINT32 >());
#if (MTKCAM_HAVE_MFB_SUPPORT == 1)
        entry.push_back(MTK_MFNR_FEATURE_MFB_MFLL , Type2Type< MINT32 >());
        entry.push_back(MTK_MFNR_FEATURE_MFB_AUTO , Type2Type< MINT32 >());
#elif (MTKCAM_HAVE_MFB_SUPPORT == 2)
        entry.push_back(MTK_MFNR_FEATURE_MFB_AIS , Type2Type< MINT32 >());
        entry.push_back(MTK_MFNR_FEATURE_MFB_AUTO , Type2Type< MINT32 >());
#elif (MTKCAM_HAVE_MFB_SUPPORT == 3)
        entry.push_back(MTK_MFNR_FEATURE_MFB_MFLL , Type2Type< MINT32 >());
        entry.push_back(MTK_MFNR_FEATURE_MFB_AIS , Type2Type< MINT32 >());
        entry.push_back(MTK_MFNR_FEATURE_MFB_AUTO , Type2Type< MINT32 >());
#endif
        rMetadata.update(entry.tag(), entry);
    }
#endif // MTKCAM_HAVE_MFB_SUPPORT

    // update Streaming Request Common Type
    {
        IMetadata::IEntry availReqEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availReqEntry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE , Type2Type< MINT32 >());
        rMetadata.update(availReqEntry.tag(), availReqEntry);

        IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
        availCharactsEntry.push_back(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES , Type2Type< MINT32 >());
        rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
    }
    // update Streaming Available EIS ControlFlow
    {
        IMetadata::IEntry availRecordEntry = rMetadata.entryFor(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES);
        if(availRecordEntry.isEmpty()){
            IMetadata::IEntry entry(MTK_STREAMING_FEATURE_AVAILABLE_RECORD_STATES);
            entry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE_PREVIEW, Type2Type< MINT32 >());
            entry.push_back(MTK_STREAMING_FEATURE_RECORD_STATE_RECORD, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
        }
    }

    {
        // add BGService REQUEST key
        IMetadata::IEntry availRequestEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        availRequestEntry.push_back(MTK_BGSERVICE_FEATURE_IMAGEREADERID , Type2Type< MINT32 >());
        rMetadata.update(availRequestEntry.tag(), availRequestEntry);
        // add BGService result key
        IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        availResultEntry.push_back(MTK_BGSERVICE_FEATURE_IMAGEREADERID , Type2Type< MINT32 >());
        rMetadata.update(availResultEntry.tag(), availResultEntry);
        // add BGService session key
        IMetadata::IEntry sessionKeyEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
        sessionKeyEntry.push_back(MTK_BGSERVICE_FEATURE_IMAGEREADERID , Type2Type< MINT32 >());
        rMetadata.update(sessionKeyEntry.tag(), sessionKeyEntry);
    }


#ifndef EIS_SUPPORTED
    //update EIS support
    {
        IMetadata::IEntry availEISEntry = rMetadata.entryFor(MTK_CONTROL_AVAILABLE_VIDEO_STABILIZATION_MODES);
        if (!availEISEntry.isEmpty())
        {
            availEISEntry.clear();
            availEISEntry.push_back(MTK_CONTROL_VIDEO_STABILIZATION_MODE_OFF, Type2Type< MUINT8 >());
            rMetadata.update(availEISEntry.tag(), availEISEntry);
        }
        IMetadata::IEntry availAdvEISEntry = rMetadata.entryFor(MTK_EIS_FEATURE_EIS_MODE);
        if (!availAdvEISEntry.isEmpty())
        {
            availAdvEISEntry.clear();
            availAdvEISEntry.push_back(MTK_EIS_FEATURE_EIS_MODE_OFF, Type2Type< MINT32 >());
            rMetadata.update(availAdvEISEntry.tag(), availAdvEISEntry);
        }
    }
#else
    {
        IMetadata::IEntry availSessionEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
        if (!availSessionEntry.isEmpty())
        {
            availSessionEntry.push_back(MTK_EIS_FEATURE_EIS_MODE, Type2Type< MINT32 >());
            rMetadata.update(availSessionEntry.tag(), availSessionEntry);
        }
        else
        {
            IMetadata::IEntry entry(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
            entry.push_back(MTK_EIS_FEATURE_EIS_MODE, Type2Type< MINT32 >());
            rMetadata.update(entry.tag(), entry);
        }
    }
#endif
    // update multi-cam feature mode to static metadata
    // vendor tag
    {
        {
            // for multi-cam logica device added metadata.
            // to store manual update metadata for sensor driver.
            IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
            availCharactsEntry.push_back(MTK_MULTI_CAM_FEATURE_SENSOR_MANUAL_UPDATED , Type2Type< MINT32 >());
            rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
        }
        //
        auto pHalDeviceList = MAKE_HalLogicalDeviceList();
        auto physicIdsList = pHalDeviceList->getSensorId(
                                    mInfo.getDeviceId());
        if(physicIdsList.size() > 1)
        {
            MBOOL needAddCharactersticsKeys = MFALSE;
            auto supportedFeature = pHalDeviceList->getSupportedFeature(mInfo.getDeviceId());
            // add feature mode
            {
                IMetadata::IEntry entry(MTK_MULTI_CAM_FEATURE_AVAILABLE_MODE);
                if(supportedFeature & DEVICE_FEATURE_ZOOM)
                {
                    MY_LOGD("deviceid(%d) support zoom feature", mInfo.getDeviceId());
                    entry.push_back(MTK_MULTI_CAM_FEATURE_MODE_ZOOM, Type2Type< MINT32 >());
                    needAddCharactersticsKeys = MTRUE;
                }
                if(supportedFeature & DEVICE_FEATURE_VSDOF)
                {
                    MY_LOGD("deviceid(%d) support vsdof feature", mInfo.getDeviceId());
                    entry.push_back(MTK_MULTI_CAM_FEATURE_MODE_VSDOF, Type2Type< MINT32 >());
                    needAddCharactersticsKeys = MTRUE;
                }
                if(supportedFeature & DEVICE_FEATURE_DENOISE)
                {
                    MY_LOGD("deviceid(%d) support denoise feature", mInfo.getDeviceId());
                    entry.push_back(MTK_MULTI_CAM_FEATURE_MODE_DENOISE, Type2Type< MINT32 >());
                    needAddCharactersticsKeys = MTRUE;
                }
                rMetadata.update(entry.tag(), entry);
                if(needAddCharactersticsKeys)
                {
                    MY_LOGD("Add charactersticsKeys for feature mode (v1.1)");

                    IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
                    availCharactsEntry.push_back(MTK_MULTI_CAM_FEATURE_AVAILABLE_MODE , Type2Type< MINT32 >());
                    rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
                    //
                    IMetadata::IEntry availRequestEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
                    availRequestEntry.push_back(MTK_MULTI_CAM_FEATURE_MODE , Type2Type< MINT32 >());
                    rMetadata.update(availRequestEntry.tag(), availRequestEntry);
                    //
                    IMetadata::IEntry sessionKeyEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
                    sessionKeyEntry.push_back(MTK_MULTI_CAM_FEATURE_MODE , Type2Type< MINT32 >());
                    rMetadata.update(sessionKeyEntry.tag(), sessionKeyEntry);
                }
            }
            // for vsdof specificy.
            if(supportedFeature & DEVICE_FEATURE_VSDOF)
            {
                // if vsdof mode, add preview mode(full/half)
                IMetadata::IEntry entry(MTK_VSDOF_FEATURE_AVAILABLE_PREVIEW_MODE);
                entry.push_back(MTK_VSDOF_FEATURE_PREVIEW_MODE_FULL, Type2Type< MINT32 >());
                entry.push_back(MTK_VSDOF_FEATURE_PREVIEW_MODE_HALF, Type2Type< MINT32 >());
                rMetadata.update(entry.tag(), entry);
                //MTK_STEREO_FEATURE_SUPPORTED_DOF_LEVEL
                IMetadata::IEntry entry1(MTK_STEREO_FEATURE_SUPPORTED_DOF_LEVEL);
                entry1.push_back(15, Type2Type< MINT32 >()); // workaround: temp write 15
                rMetadata.update(entry1.tag(), entry1);
                // add Characterstics key
                MY_LOGD("Add characterstics keys for vsdof(1.0)");
                IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
                availCharactsEntry.push_back(MTK_VSDOF_FEATURE_AVAILABLE_PREVIEW_MODE , Type2Type< MINT32 >());
                availCharactsEntry.push_back(MTK_STEREO_FEATURE_SUPPORTED_DOF_LEVEL , Type2Type< MINT32 >());
                rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
                // add request key
                MY_LOGD("Add request keys for vsdof(1.2)");
                IMetadata::IEntry availRequestEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
                availRequestEntry.push_back(MTK_STEREO_FEATURE_DOF_LEVEL , Type2Type< MINT32 >());
                availRequestEntry.push_back(MTK_VSDOF_FEATURE_PREVIEW_MODE , Type2Type< MINT32 >());
                availRequestEntry.push_back(MTK_VSDOF_FEATURE_PREVIEW_SIZE , Type2Type< MINT32 >());
                availRequestEntry.push_back(MTK_VSDOF_FEATURE_CAPTURE_WARNING_MSG , Type2Type< MINT32 >());
                rMetadata.update(availRequestEntry.tag(), availRequestEntry);
                // add result key
                MY_LOGD("Add result keys for vsdof(1.1)");
                IMetadata::IEntry availResultEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
                availResultEntry.push_back(MTK_STEREO_FEATURE_WARNING , Type2Type< MINT32 >());
                availResultEntry.push_back(MTK_STEREO_FEATURE_RESULT_DOF_LEVEL , Type2Type< MINT32 >());
                availResultEntry.push_back(MTK_VSDOF_FEATURE_WARNING, Type2Type< MINT32 >());
                rMetadata.update(availResultEntry.tag(), availResultEntry);
                // add session key
                MY_LOGD("Add session key for vsdof(1.0)");
                IMetadata::IEntry sessionKeyEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);
                sessionKeyEntry.push_back(MTK_VSDOF_FEATURE_PREVIEW_MODE , Type2Type< MINT32 >());
                sessionKeyEntry.push_back(MTK_VSDOF_FEATURE_PREVIEW_SIZE , Type2Type< MINT32 >());
                rMetadata.update(sessionKeyEntry.tag(), sessionKeyEntry);
            }
        }
    }
    // update logic device related metadata tag
#if 0 // disable multi-cam logical device setting
    {
        auto pHalDeviceList = MAKE_HalLogicalDeviceList();
        auto physicIdsList = pHalDeviceList->getSensorId(
                                    mInfo.getDeviceId());
        if(physicIdsList.size() > 1)
        {
            // support multi-cam
            {
                MY_LOGD("add request available capabilities.");
                IMetadata::IEntry entry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CAPABILITIES);
                entry.push_back(MTK_REQUEST_AVAILABLE_CAPABILITIES_LOGICAL_MULTI_CAMERA , Type2Type< MUINT8 >());
                rMetadata.update(entry.tag(), entry);
            }
            // update logic physic ids
            {
                std::string idsListString;
                IMetadata::IEntry entry(MTK_LOGICAL_MULTI_CAMERA_PHYSICAL_IDS);
                for(auto id : physicIdsList)
                {
                    // ascii
                    entry.push_back(id + 48 , Type2Type< MUINT8 >());
                    entry.push_back('\0' , Type2Type< MUINT8 >());
                    idsListString += std::to_string(id);
                    idsListString += " ";
                }
                MY_LOGD("update logic id (%s:%x)", idsListString.c_str(), MTK_LOGICAL_MULTI_CAMERA_PHYSICAL_IDS);
                rMetadata.update(entry.tag(), entry);
                // add characteristics key
                IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
                availCharactsEntry.push_back(MTK_LOGICAL_MULTI_CAMERA_PHYSICAL_IDS , Type2Type< MINT32 >());
                rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
            }
            // update sync type
            {
                auto syncType = pHalDeviceList->getSyncType(
                                    mInfo.getDeviceId());
                if(SensorSyncType::NOT_SUPPORT != syncType)
                {
                    MUINT8 value = MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE;
                    switch(syncType)
                    {
                        case SensorSyncType::APPROXIMATE:
                            value = MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE;
                            break;
                        case SensorSyncType::CALIBRATED:
                            value = MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED;
                            break;
                        default:
                            MY_LOGE("invaild sync type");
                            break;
                    }
                    IMetadata::IEntry entry(MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE);
                    entry.push_back(value, Type2Type< MUINT8 >());
                    rMetadata.update(entry.tag(), entry);
                    auto toString = [&value]()
                    {
                        switch(value)
                        {
                            case MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_APPROXIMATE:
                                return "Approximate";
                            case MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE_CALIBRATED:
                                return "Calibrated";
                            default:
                                return "not support";
                        }
                    };
                    MY_LOGD("update sync type (%s:%x)", toString(), MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE);
                    // add characteristics key
                    IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
                    availCharactsEntry.push_back(MTK_LOGICAL_MULTI_CAMERA_SENSOR_SYNC_TYPE , Type2Type< MINT32 >());
                    rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
                }
            }
        }
    }
#endif
    //  update zsl data.
    {
        updateZslData(rMetadata);
    }
    //
    //  android.request.availableRequestKeys
    {
        IMetadata::IEntry availRequestEntry  = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        //
        availRequestEntry.push_back( MTK_CONTROL_CAPTURE_KEEP_FOR_REPROCESS, Type2Type< MINT32 >());
        //
        rMetadata.update(availRequestEntry.tag(), availRequestEntry);
    }
}

MBOOL
MetadataProvider::
isHdrSensor(MUINT const availVhdrEntryCount)
{
    MBOOL isHDRSensor = (availVhdrEntryCount > 1);
    char strVhdrLog[100];
    memset(strVhdrLog, '\0', sizeof(strVhdrLog));

    //query sensor static info from sensor driver to decide Hal3 vHDR support
    /*NSCam::IHalSensorList *pSensorHalList = NULL;
    pSensorHalList = MAKE_HalSensorList();*/
    IHalLogicalDeviceList* pHalDeviceList;
    pHalDeviceList = MAKE_HalLogicalDeviceList();
    if(pHalDeviceList == NULL)
    {
        MY_LOGE("pHalDeviceList::get fail");
    } else {
        MUINT32 sensorDev = (MUINT32)pHalDeviceList->querySensorDevIdx(mInfo.getDeviceId());
        NSCam::SensorStaticInfo sensorStaticInfo;
        pHalDeviceList->querySensorStaticInfo(sensorDev, &sensorStaticInfo);
        isHDRSensor = (sensorStaticInfo.HDR_Support > 0) ? isHDRSensor : false;
        sprintf(strVhdrLog, " sensorDev:%d, sensorStaticInfo.HDR_Support:%d,",
            sensorDev, sensorStaticInfo.HDR_Support);
    }

    //force set ON/OFF Hal3 vHDR support
    MINT32 vhdrHal3Prop = property_get_int32("debug.camera.hal3.vhdrSupport", -1);
    isHDRSensor = (vhdrHal3Prop != -1) ? (vhdrHal3Prop > 0) : isHDRSensor;
    MY_LOGD("isHDRSensor:%d, vhdrHal3Prop:%d,%s availVhdrEntry.count():%d",
        isHDRSensor, vhdrHal3Prop, strVhdrLog, availVhdrEntryCount);

    return isHDRSensor;
}

MVOID
MetadataProvider::
updateHdrData(MBOOL const isHDRSensor, MBOOL const isSingleFrameSupport, MINT32 const hdrDetectionMode,
    IMetadata &rMetadata)
{
    // Available HDR modes for Photo & Video
    IMetadata::IEntry availHdrPhotoEntry(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_PHOTO);
    IMetadata::IEntry availHdrVideoEntry(MTK_HDR_FEATURE_AVAILABLE_HDR_MODES_VIDEO);

    // --- MODE_OFF ----
    availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_OFF, Type2Type< MINT32 >());
    availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_OFF, Type2Type< MINT32 >());

#if (MTKCAM_HAVE_VHDR_SUPPORT == 1)
    // --- MODE_VIDEO_ON ----
    if(isHDRSensor)
    {
        availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_ON, Type2Type< MINT32 >());
        availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_ON, Type2Type< MINT32 >());
        //if(isSingleFrameSupport)
            //availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_ON, Type2Type< MINT32 >());
    }
#endif

#if (MTKCAM_HAVE_HDR_SUPPORT == 1)
    // --- MODE_ON ----
    availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_ON, Type2Type< MINT32 >());
    /* Video mode not support MODE_ON*/

    // --- MODE_AUTO ----
    if (hdrDetectionMode == 3
        || (hdrDetectionMode == 2 && !isHDRSensor)
        || (hdrDetectionMode == 1 && isHDRSensor))
    {
        availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_AUTO, Type2Type< MINT32 >());
    }
    /* Video mode not support MODE_AUTO*/

    // --- MODE_VIDEO_AUTO ----
    if(isHDRSensor && (hdrDetectionMode == 1 || hdrDetectionMode == 3))
    {
        availHdrVideoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_AUTO, Type2Type< MINT32 >());
        if(isSingleFrameSupport)
            availHdrPhotoEntry.push_back(MTK_HDR_FEATURE_HDR_MODE_VIDEO_AUTO, Type2Type< MINT32 >());
    }
#endif// MTKCAM_HAVE_HDR_SUPPORT endif

    rMetadata.update(availHdrPhotoEntry.tag(), availHdrPhotoEntry);
    rMetadata.update(availHdrVideoEntry.tag(), availHdrVideoEntry);
}


MVOID
MetadataProvider::
updateZslData(IMetadata &rMetadata)
{
    // step1. determine sensor type is 4cell or not.
    //        currently, we do not support zsl behavior in 4cell sensor.
    bool is4CellSensor = false;
    //query sensor static info from sensor driver to decide Hal3 zsl support.
    /*NSCam::IHalSensorList *pSensorHalList = NULL;
    pSensorHalList = MAKE_HalSensorList();*/
    IHalLogicalDeviceList* pHalDeviceList;
    pHalDeviceList = MAKE_HalLogicalDeviceList();
    if(pHalDeviceList == NULL)
    {
        MY_LOGE("pHalDeviceList::get fail");
        return;
    } else {
        MUINT32 sensorDev = (MUINT32)pHalDeviceList->querySensorDevIdx(mInfo.getDeviceId());
        NSCam::SensorStaticInfo sensorStaticInfo;
        pHalDeviceList->querySensorStaticInfo(sensorDev, &sensorStaticInfo);
        uint32_t u4RawFmtType = sensorStaticInfo.rawFmtType;
        is4CellSensor = ((u4RawFmtType == SENSOR_RAW_4CELL ||
                          u4RawFmtType == SENSOR_RAW_4CELL_BAYER ||
                          u4RawFmtType == SENSOR_RAW_4CELL_HW_BAYER) ? true : false);
        if ( is4CellSensor ) {
            MY_LOGI("SensorStaticInfo 4CellSensor_Support(type:%d) (support:%d), skip zsl setting.", u4RawFmtType, is4CellSensor);
            return;
        }
    }

    // step2. update android.request.availableRequestKeys/availableResultKeys/availableCharacteristicsKeys/availableSessionKeys
    {
        IMetadata::IEntry availRequestEntry  = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_REQUEST_KEYS);
        IMetadata::IEntry availResultEntry   = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_RESULT_KEYS);
        IMetadata::IEntry availCharactsEntry = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_CHARACTERISTICS_KEYS);
        IMetadata::IEntry availSessionEntry  = rMetadata.entryFor(MTK_REQUEST_AVAILABLE_SESSION_KEYS);

        // 2-1. update AOSP tag MTK_CONTROL_ENABLE_ZSL
        availRequestEntry.push_back( MTK_CONTROL_ENABLE_ZSL, Type2Type< MINT32 >());
        availResultEntry.push_back(  MTK_CONTROL_ENABLE_ZSL, Type2Type< MINT32 >());

        // 2-2. update static in vendortag MTK_CONTROL_CAPTURE_AVAILABLE_ZSL_MODES / MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE
        availCharactsEntry.push_back( MTK_CONTROL_CAPTURE_AVAILABLE_ZSL_MODES, Type2Type< MINT32 >());
        availCharactsEntry.push_back( MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE, Type2Type< MINT32 >());

        // 2-3. update control/result/session in vendortag MTK_CONTROL_CAPTURE_ZSL_MODE
        availRequestEntry.push_back( MTK_CONTROL_CAPTURE_ZSL_MODE,  Type2Type< MINT32 >());
        availResultEntry.push_back(  MTK_CONTROL_CAPTURE_ZSL_MODE,  Type2Type< MINT32 >());
        availSessionEntry.push_back( MTK_CONTROL_CAPTURE_ZSL_MODE,  Type2Type< MINT32 >());

        rMetadata.update(availRequestEntry.tag(), availRequestEntry);
        rMetadata.update(availResultEntry.tag(), availResultEntry);
        rMetadata.update(availCharactsEntry.tag(), availCharactsEntry);
        rMetadata.update(availSessionEntry.tag(), availSessionEntry);
    }

    // step3. list available zsl modes.
    IMetadata::IEntry availZslModes = rMetadata.entryFor(MTK_CONTROL_CAPTURE_AVAILABLE_ZSL_MODES);
    if ( availZslModes.isEmpty() ) {
        IMetadata::IEntry entry(MTK_CONTROL_CAPTURE_AVAILABLE_ZSL_MODES);
        entry.push_back(MTK_CONTROL_CAPTURE_ZSL_MODE_OFF, Type2Type<MUINT8>());
        if ( !is4CellSensor )
            entry.push_back(MTK_CONTROL_CAPTURE_ZSL_MODE_ON, Type2Type<MUINT8>());
        rMetadata.update(entry.tag(), entry);
    } else {
        MY_LOGI("user defines availableZslModes in custom files");
    }

    // step4. update default zsl setting as off.
    //        do not update if already defined in custom files.
    IMetadata::IEntry defaultZslMode = rMetadata.entryFor(MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE);
    if ( defaultZslMode.isEmpty() ) {
        IMetadata::IEntry entry(MTK_CONTROL_CAPTURE_DEFAULT_ZSL_MODE);
        entry.push_back(MTK_CONTROL_CAPTURE_ZSL_MODE_OFF, Type2Type<MUINT8>());
        rMetadata.update(entry.tag(), entry);
    } else {
        MY_LOGI("user defines defaultZslMode in custom files");
    }
}


