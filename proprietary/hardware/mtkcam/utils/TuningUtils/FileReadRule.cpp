/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define LOG_TAG "mtkcam-aaa-util"
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/TuningUtils/FileReadRule.h>
#include <fstream>
#include <cutils/properties.h>
#include <string>
#include <cstring>
#include <mtkcam/utils/TuningUtils/FileDumpNamingRule.h>
#include "CommonRule.h"
#if MTK_DP_VERSION
#include "debug_exif/aaa/dbg_isp_param.h"
#endif
#include <mtkcam/def/common.h>
#include <mtkcam/utils/sys/IFileCache.h>
#include <utils/RefBase.h>

/*
 *
 ******************************************************************************/
//
namespace NSCam {
namespace TuningUtils {

static
inline void
removeRedundancy(std::string* pStr){
    std::string::size_type i = 0;
    i = pStr->find_first_of(";", i);
    *pStr = pStr->substr(i+1, pStr->length());
    i = 0;
    while (i < pStr->length()) {
        i = pStr->find_first_of("\n\r ", i);
        if (i == std::string::npos) {
            break;
        }
        pStr->erase(i);
    }
}
FileReadRule::FileReadRule()
{
}
void FileReadRule::getFile_RAW(int reqNum, const char* ispProfile, IImageBuffer *pbuf, const char *pUserString, int SensorID)
{
    if (!isREADEnable(pUserString))
       return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("raw");
    std::string str_ispProfile(ispProfile);

    char str_reqNum[50];
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            i = str.find_first_of(",", i);
            if ((std::string::npos != str_ispProfile.find("MFNR"))){
                // mfnr needs check req
                if (str.substr(0,i).compare(str_reqNum) == 0){
                    removeRedundancy(&str);
                    break;
                }
            } else {
                removeRedundancy(&str);
                break;
            }
        }
    }
    ALOGD("DEBUG load imgo from %s", str.c_str());
    pbuf->unlockBuf(pUserString);
    pbuf->loadFromFile(str.c_str());
    pbuf->syncCache();
    const auto __usage = (eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE);
    pbuf->lockBuf(pUserString, __usage);
}
void FileReadRule::getFile_LCSO(int reqNum, const char* ispProfile, IImageBuffer *pbuf, const char *pUserString, int SensorID)
{
    if (!isREADEnable(pUserString))
       return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("lcso");
    std::string str_ispProfile(ispProfile);

    char str_reqNum[50] = {'\0'};
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            i = str.find_first_of(",", i);
            if ((std::string::npos != str_ispProfile.find("MFNR"))){
                // mfnr needs check req
                if (str.substr(0,i).compare(str_reqNum) == 0){
                    removeRedundancy(&str);
                    break;
                }
            } else {
                removeRedundancy(&str);
                break;
            }
        }
    }

    ALOGD("DEBUG load LCSO from %s", str.c_str());
    pbuf->unlockBuf(pUserString);
    pbuf->loadFromFile(str.c_str());
    pbuf->syncCache();
    const auto __usage = (eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE);
    pbuf->lockBuf(pUserString, __usage);
}
void FileReadRule::getFile_LSC(int reqNum, const char* ispProfile, char *filename, int filesize, const char *pUserString)
{
    if(filename == NULL)
        return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("lsc");
    std::string str_ispProfile(ispProfile);
    char *ptr = filename;

    char str_reqNum[50];
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            i = str.find_first_of(",", i);
            if ((std::string::npos != str_ispProfile.find("MFNR"))){
                // mfnr needs check req
                if (str.substr(0,i).compare(str_reqNum) == 0){
                    str = str.substr(i+1,str.length());
                    i = str.find_first_of(";", i);
                    //tuning & shading table needs check isp profile, raw & lcso just need req num
                    if (str.substr(0,i).find(str_ispProfile) != std::string::npos){
                        removeRedundancy(&str);
                        break;
                    }
                }
            } else {
                removeRedundancy(&str);
                break;
            }
        }
    }
    ALOGD("[%s] path(%s)",__FUNCTION__,str.c_str() );
    snprintf(ptr, filesize, "%s", str.c_str());
}

void FileReadRule::getFile_RRZO(int reqNum, const char* ispProfile, IImageBuffer *pbuf, const char *pUserString, int SensorID)
{
    if (!isREADEnable(pUserString))
       return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("rrzo");
    std::string str_ispProfile(ispProfile);

    char str_reqNum[50] = {'\0'};
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            i = str.find_first_of(",", i);
            if ((std::string::npos != str_ispProfile.find("MFNR"))){
                // mfnr needs check req
                if (str.substr(0,i).compare(str_reqNum) == 0){
                    removeRedundancy(&str);
                    break;
                }
            } else {
                removeRedundancy(&str);
                break;
            }
        }
    }

    ALOGD("DEBUG load RRZO from %s", str.c_str());
    pbuf->unlockBuf(pUserString);
    pbuf->loadFromFile(str.c_str());
    pbuf->syncCache();
    const auto __usage = (eBUFFER_USAGE_SW_READ_OFTEN | eBUFFER_USAGE_SW_WRITE_OFTEN | eBUFFER_USAGE_HW_CAMERA_READWRITE);
    pbuf->lockBuf(pUserString, __usage);
}

void FileReadRule::getFile_P2TUNING(int reqNum, const char* ispProfile, char *filename, int filesize, const char *pUserString)
{
    if(filename == NULL)
        return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("tuning");
    std::string str_ispProfile(ispProfile);
    char *ptr = filename;
    char str_reqNum[50];
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            if ((std::string::npos != str_ispProfile.find("MFNR"))){
                // mfnr needs check req
                i = 0;
                i = str.find_first_of(",", i);
                if (str.substr(0,i).compare(str_reqNum) == 0){
                    str = str.substr(i+1,str.length());
                    i = str.find_first_of(";", i);
                    //tuning & shading table needs check isp profile, raw & lcso just need req num
                    if (str.substr(0,i).find(str_ispProfile) != std::string::npos){
                        removeRedundancy(&str);
                        break;
                    }
                }
            } else {
                removeRedundancy(&str);
                break;
            }
        }
    }

    ALOGD("[%s] path(%s)",__FUNCTION__,str.c_str());
    snprintf(ptr, filesize, "%s", str.c_str());
}

void FileReadRule::getFile_MFB(int reqNum, const char* ispProfile, char *filename, int filesize, const char *pUserString)
{
    if(filename == NULL)
        return;
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("mfb");
    std::string str_ispProfile(ispProfile);
    char *ptr = filename;
    char str_reqNum[50];
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str))
    {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            str = str.substr(i+1,str.length());
            removeRedundancy(&str);
            break;
        }
    }

    ALOGD("[%s] path(%s)",__FUNCTION__,str.c_str());
    snprintf(ptr, filesize, "%s", str.c_str());
}

void FileReadRule::getFile_YUV(int reqNum, const char *ispProfile, IImageBuffer *pbuf, const char *pUserString, YUV_PORT type, int SensorID)
{
    if (!isREADEnable(pUserString))
       return;
    else if (type != YUV_PORT_IMG3O)
       return;

    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("img3o");
    std::string str_ispProfile(ispProfile);

    char str_reqNum[50];
    sprintf(str_reqNum, "%04d", reqNum);

    while (std::getline(infile, str)) {
        std::string::size_type i = 0;
        i = str.find_first_of(",", i);
        if (str.substr(0,i).compare(str_format) == 0){ //format
            removeRedundancy(&str);
            ALOGD("DEBUG load yuv from %s", str.c_str());
            pbuf->saveToFile(str.c_str());
            break;
        }
    }
}
int FileReadRule::getFile_uniqueKey(const char *pUserString, int SensorID){
    char _config[1024] = {'\0'};
    ::property_get("vendor.debug.camera.dumpin.path.config", _config, "");
    std::ifstream infile(_config);
    std::string str;
    std::string str_format("uniqueKey");


    while (std::getline(infile, str))
    {
        if (std::string::npos != str.find(str_format)){ //format
            removeRedundancy(&str);
            ALOGD("DEBUG uniqueKey from %d", atoi(str.c_str()));
            return atoi(str.c_str());
        }
    }
    return 0;
}
bool FileReadRule::isREADEnable(const char *pUserString)
{
    int _dump = property_get_int32("vendor.debug.camera.dumpin.en", 0);
    if ((_dump&0x2) == 2)
       return true;
    else
       return false;
}
bool FileReadRule::isDumpEnable(const char *pUserString)
{
    int _dump = property_get_int32("vendor.debug.camera.dumpin.en", 0);
    if ((_dump&0x1) == 1)
       return true;
    else
       return false;
}

bool FileReadRule::DumpP2ForDP(const char* fname, const char* pP2Reg, int fsize)
{
#if MTK_DP_VERSION
    if (fsize > (P2_BUFFER_SIZE*4))
    {
        ALOGD("P2 input size larger than DP structure size(%d)", fsize);
    }

    char strDump[512] = {'\0'};
    sprintf(strDump, "%s_p2.reg", fname);

    NSIspExifDebug::IspExifDebugInfo_T rDebugInfo;
    rDebugInfo.P2RegInfo.u4TableSize = P2_BUFFER_SIZE+1;
    rDebugInfo.P2RegInfo.u4HwVersion = MTK_DP_VERSION;
    memcpy(&rDebugInfo.P2RegInfo.regDataP2, pP2Reg, P2_BUFFER_SIZE*4);

    android::sp<IFileCache> fidDump;
    fidDump = IFileCache::open(strDump);

    if (fidDump->write(&rDebugInfo.P2RegInfo, sizeof(rDebugInfo.P2RegInfo)) != sizeof(rDebugInfo.P2RegInfo)){
        CAM_LOGW("[%s] write config error to %s", __FUNCTION__, strDump);
        return false;
    } else {
        ALOGD("[%s] %s pRegBuf size(%d)",__FUNCTION__, strDump, fsize);
        return true;
    }
#else
    return false;
#endif
}

bool FileReadRule::DumpP1ForDP(const char* fname, const char* pP1Reg, int fsize)
{
#if MTK_DP_VERSION
    if (fsize > (P1_BUFFER_SIZE*4))
    {
        ALOGD("P1 input size larger than DP structure size(%d)", fsize);
    }

    char strDump[512] = {'\0'};
    sprintf(strDump, "%s_p1.reg", fname);

    NSIspExifDebug::IspExifDebugInfo_T rDebugInfo;
    rDebugInfo.P1RegInfo.u4TableSize = P1_BUFFER_SIZE+1;
    rDebugInfo.P1RegInfo.u4HwVersion = MTK_DP_VERSION;
    memcpy(&(rDebugInfo.P1RegInfo.regData), pP1Reg, P1_BUFFER_SIZE*4);

    android::sp<IFileCache> fidDump;
    fidDump = IFileCache::open(strDump);

    if (fidDump->write(&rDebugInfo.P1RegInfo, sizeof(rDebugInfo.P1RegInfo)) != sizeof(rDebugInfo.P1RegInfo)){
        CAM_LOGW("[%s] write config error to %s", __FUNCTION__, strDump);
        return false;
    } else {
        ALOGD("[%s] %s pRegBuf size(%d)",__FUNCTION__, strDump, fsize);
        return true;
    }
#else
    return false;
#endif
}

bool FileReadRule::DumpMfbForDP(const char* fname, const char* pMfbReg, int fsize)
{
#if MTK_DP_VERSION
    if (fsize > (MFB_BUFFER_SIZE*4))
    {
        ALOGD("MFB input size larger than DP structure size(%d)", fsize);
    }

    char strDump[512] = {'\0'};
    sprintf(strDump, "/sdcard/camera_dump/%s_mfb.reg", fname);

    NSIspExifDebug::IspExifDebugInfo_T rDebugInfo;
    rDebugInfo.MFBRegInfo.u4TableSize = MFB_BUFFER_SIZE+1;
    rDebugInfo.MFBRegInfo.u4HwVersion = MTK_DP_VERSION;
    memcpy(&(rDebugInfo.MFBRegInfo.regDataMFB), pMfbReg, MFB_BUFFER_SIZE*4);

    android::sp<IFileCache> fidDump;
    fidDump = IFileCache::open(strDump);

    if (fidDump->write(&rDebugInfo.MFBRegInfo, sizeof(rDebugInfo.MFBRegInfo)) != sizeof(rDebugInfo.MFBRegInfo)){
        CAM_LOGW("[%s] write config error to %s", __FUNCTION__, strDump);
        return false;
    } else {
        ALOGD("[%s] %s pRegBuf size(%d)",__FUNCTION__, strDump, fsize);
        return true;
    }
#else
    return false;
#endif
}

} //namespace FileDump
} //namespace NSCam

