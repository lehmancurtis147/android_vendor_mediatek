/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2018. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */


// Standard C header file
// Android system/core header file

// mtkcam custom header file

// mtkcam global header file
#include <featurePipe/vsdof/util/vsdof_util.h>
// Module header file
// Local header file
#include "ADPNode.h"
#include "../DepthMapPipe_Common.h"
#include "../DepthMapPipeUtils.h"
#include "./bufferPoolMgr/BaseBufferHandler.h"
// logging
#undef PIPE_CLASS_TAG
#define PIPE_CLASS_TAG "ADPNode"
#include <featurePipe/core/include/PipeLog.h>

namespace NSCam {
namespace NSCamFeature {
namespace NSFeaturePipe_DepthMap {

using namespace VSDOF::util;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  Instantiation.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

ADPNode::
ADPNode(
    const char *name,
    DepthMapPipeNodeID nodeID,
    PipeNodeConfigs config
)
: DepthMapPipeNode(name, nodeID, config)
{
    MY_LOGD("[Constructor]");
    this->addWaitQueue(&mJobQueue);
}

ADPNode::
~ADPNode()
{
    MY_LOGD("[Destructor]");
}


MVOID
ADPNode::
cleanUp()
{
    VSDOF_LOGD("+");

    if(mpADPHAL)
    {
        delete mpADPHAL;
        mpADPHAL = NULL;
    }
    mJobQueue.clear();

    VSDOF_LOGD("-");
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  DepthMapPipeNode Public Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

MBOOL
ADPNode::
onInit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
ADPNode::
onUninit()
{
    VSDOF_INIT_LOG("+");
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
ADPNode::
onThreadStart()
{
    VSDOF_INIT_LOG("+");
    CAM_TRACE_NAME("ADPNode::onThreadStart");
    // create ADP instance - Preview/Record
    ADP_HAL_INIT_PARAM initParam;
    initParam.isSecurity = MFALSE;
    mpADPHAL  = ADP_HAL::createInstance(initParam);
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
ADPNode::
onThreadStop()
{
    VSDOF_INIT_LOG("+");
    cleanUp();
    VSDOF_INIT_LOG("-");
    return MTRUE;
}

MBOOL
ADPNode::
onData(DataID data, DepthMapRequestPtr& pRequest)
{
    MBOOL ret = MTRUE;
    VSDOF_LOGD("+ : dataID=%s reqId=%d", ID2Name(data), pRequest->getRequestNo());

    switch(data)
    {
        case DPE_TO_ADP_DISPARITY:
            mJobQueue.enque(pRequest);
            break;
        default:
            MY_LOGW("Unrecongnized DataID=%d", data);
            ret = MFALSE;
            break;
    }

    VSDOF_LOGD("-");
    return ret;
}


MBOOL
ADPNode::
onThreadLoop()
{
    DepthMapRequestPtr pRequest;
    if( !waitAnyQueue() )
    {
        return MFALSE;
    }
    // deque prioritized request
    if( !mJobQueue.deque(pRequest) )
    {
        return MFALSE;
    }
    // mark on-going-request start
    this->incExtThreadDependency();
    VSDOF_PRFLOG("threadLoop start, reqID=%d", pRequest->getRequestNo());
    CAM_TRACE_NAME("ADPNode::onThreadLoop");
    //
    MBOOL bRet = MFALSE;
    if(pRequest->getRequestAttr().opState == eSTATE_NORMAL)
    {
        // process done are launched inside it
        bRet = performADPALGO(pRequest);
    }
    else
    {
        MY_LOGE("reqID=%d, not support this state:%d",
                pRequest->getRequestNo(), pRequest->getRequestAttr().opState);
    }
    // error handling
    if(!bRet)
    {
        MY_LOGE("ADP operation failed: reqID=%d", pRequest->getRequestNo());
        // if error occur in the queued-flow, skip this operation and call queue-done
        if(pRequest->isQueuedDepthRequest(mpPipeOption))
            handleData(QUEUED_FLOW_DONE, pRequest);
        else
            handleData(ERROR_OCCUR_NOTIFY, pRequest);
    }
    //
    pRequest->getBufferHandler()->onProcessDone(getNodeId());
    // mark on-going-request end
    this->decExtThreadDependency();
    return bRet;
}


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//  ADPNode Private Operations.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
MBOOL
ADPNode::
performADPALGO(DepthMapRequestPtr& pRequest)
{
    CAM_TRACE_NAME("ADPNode::performADPALGO");
    sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
    DepthMapBufferID outAppBID = mapQueuedBufferID(pRequest, mpPipeOption, BID_META_OUT_APP);
    IMetadata* pOutAppMeta = pRequest->getBufferHandler()->requestMetadata(getNodeId(), outAppBID);
    //
    ADP_HAL_IN_DATA inData;
    ADP_HAL_OUT_DATA outData;
    // input data
    if(!tryGetMetadata<MFLOAT>(pOutAppMeta, MTK_STEREO_FEATURE_RESULT_DISTANCE, inData.focalLensFactor))
        MY_LOGE("Failed to query distance.");
    MBOOL bRet = pBufferHandler->getEnqueBuffer(getNodeId(), BID_DPE_OUT_DMP_L, inData.dispMapL);
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), BID_DPE_OUT_DMP_R, inData.dispMapR);
    // conf map
    DepthMapBufferID cfmID = (STEREO_SENSOR_REAR_MAIN_TOP == StereoSettingProvider::getSensorRelativePosition())
                             ? BID_DPE_OUT_CFM_L
                             : BID_DPE_OUT_CFM_R;
    bRet &= pBufferHandler->getEnqueBuffer(getNodeId(), cfmID, inData.confidenceMap);
    // output buffer
    if(pRequest->isQueuedDepthRequest(mpPipeOption))
        outData.depthmap =  pBufferHandler->requestBuffer(getNodeId(), BID_ADP_INTERNAL_DEPTHMAP);
    else
        outData.depthmap =  pBufferHandler->requestBuffer(getNodeId(), BID_ADP_OUT_DEPTH);
    // timer
    pRequest->mTimer.startADP();
    //
    debugInOutData(inData, outData);
    VSDOF_PRFLOG("start ADP ALGO, reqID=%d", pRequest->getRequestNo());
    CAM_TRACE_BEGIN("ADPNode::N3DHALRun");
    //
    bRet = mpADPHAL->ADPHALRun(inData, outData);
    CAM_TRACE_END();
    pRequest->mTimer.stopADP();
    //
    VSDOF_PRFTIME_LOG("finished ADP ALGO, reqID=%d, exec-time=%d msec",
            pRequest->getRequestNo(), pRequest->mTimer.getElapsedADP());
    //
    if(bRet)
    {
        //store into depth storage
        if(pRequest->isQueuedDepthRequest(mpPipeOption))
        {
            DepthBufferInfo depthInfo;
            depthInfo.miReqIdx = pRequest->getRequestNo();
            pBufferHandler->getEnquedSmartBuffer(getNodeId(), BID_ADP_INTERNAL_DEPTHMAP, depthInfo.mpDepthBuffer);
            depthInfo.mpDepthBuffer->mImageBuffer->syncCache(eCACHECTRL_FLUSH);
            mpDepthStorage->setStoredData(depthInfo);
            this->handleDump(ADP_OUT_INTERNAL_DEPTHMAP, pRequest);
            //
            this->handleData(QUEUED_FLOW_DONE, pRequest);
        }
        else
        {
            pRequest->setOutputBufferReady(BID_ADP_OUT_DEPTH);
            this->handleDataAndDump(ADP_OUT_DEPTH, pRequest);
        }
    }

    return bRet;
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
ADPNode::
debugInOutData(
    ADP_HAL_IN_DATA inData,
    ADP_HAL_OUT_DATA outData
)
{
    #define DEBUG_BUFFER_SETUP(buf) \
        VSDOF_LOGD("ADPNode buf:" # buf);\
        VSDOF_LOGD("Image buffer size=%dx%d:", buf->getImgSize().w, buf->getImgSize().h);

    DEBUG_BUFFER_SETUP(inData.dispMapL);
    DEBUG_BUFFER_SETUP(inData.dispMapR);
    DEBUG_BUFFER_SETUP(inData.confidenceMap);
    DEBUG_BUFFER_SETUP(outData.depthmap);
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
ADPNode::
onFlush()
{
    MY_LOGD("+ extDep=%d", this->getExtThreadDependency());
    DepthMapRequestPtr pRequest;
    while( mJobQueue.deque(pRequest) )
    {
        sp<BaseBufferHandler> pBufferHandler = pRequest->getBufferHandler();
        pBufferHandler->onProcessDone(getNodeId());
    }
    DepthMapPipeNode::onFlush();
    MY_LOGD("-");
}

}; //NSFeaturePipe_DepthMap
}; //NSCamFeature
}; //NSCam

