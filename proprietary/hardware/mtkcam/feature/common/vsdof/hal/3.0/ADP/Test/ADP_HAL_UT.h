/********************************************************************************************
 *     LEGAL DISCLAIMER
 *
 *     (Header of MediaTek Software/Firmware Release or Documentation)
 *
 *     BY OPENING OR USING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 *     THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE") RECEIVED
 *     FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON AN "AS-IS" BASIS
 *     ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES, EXPRESS OR IMPLIED,
 *     INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR
 *     A PARTICULAR PURPOSE OR NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY
 *     WHATSOEVER WITH RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 *     INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK
 *     ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
 *     NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S SPECIFICATION
 *     OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
 *
 *     BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE LIABILITY WITH
 *     RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION,
 *     TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE
 *     FEES OR SERVICE CHARGE PAID BY BUYER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 *     THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE WITH THE LAWS
 *     OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF LAWS PRINCIPLES.
 ************************************************************************************************/
#ifndef N3D_HAL_UT_H_
#define N3D_HAL_UT_H_

#include <limits.h>
#include <gtest/gtest.h>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <mtkcam/feature/stereo/hal/stereo_size_provider.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>

#include <adp_hal.h>
#include <mtkcam/utils/imgbuf/IImageBuffer.h>

#include "../../inc/stereo_dp_util.h"

#include <fstream>

using namespace std;
using namespace android;
using namespace NSCam;
using namespace NSCam::v1::Stereo;
using namespace StereoHAL;

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "ADP_HAL_UT"

#define UT_CASE_PATH            "/sdcard/ADP_HAL_UT"
#define UT_CASE_IN_FOLDER       UT_CASE_PATH"/in"
#define UT_CASE_GOLDEN_FOLDER   UT_CASE_PATH"/golden"
#define UT_CASE_OUT_FOLDER      UT_CASE_PATH"/out"

#define MY_LOGD(fmt, arg...)    if(LOG_ENABLED) { printf("[D][%s]" fmt"\n", __func__, ##arg); }
#define MY_LOGI(fmt, arg...)    if(LOG_ENABLED) { printf("[I][%s]" fmt"\n", __func__, ##arg); }
#define MY_LOGW(fmt, arg...)    printf("[W][%s] WRN(%5d):" fmt"\n", __func__, __LINE__, ##arg)
#define MY_LOGE(fmt, arg...)    printf("[E][%s] %s ERROR(%5d):" fmt"\n", __func__,__FILE__, __LINE__, ##arg)

// #define FUNC_START MY_LOGD("[%s] +", __FUNCTION__)
// #define FUNC_END   MY_LOGD("[%s] -", __FUNCTION__)

#define PRINT_SIZE  1

inline void print(const char *tag, MSize size)
{
#if PRINT_SIZE
    printf("%s: %dx%d\n", tag, size.w, size.h);
#endif
}

inline void print(const char *tag, MRect rect)
{
#if PRINT_SIZE
    printf("%s: (%d, %d), %dx%d\n", tag, rect.p.x, rect.p.y, rect.s.w, rect.s.h);
#endif
}

inline void print(const char *tag, StereoArea area)
{
#if PRINT_SIZE
    printf("%s: Size %dx%d, Padding %dx%d, StartPt (%d, %d), ContentSize %dx%d\n", tag,
           area.size.w, area.size.h, area.padding.w, area.padding.h,
           area.startPt.x, area.startPt.y, area.contentSize().w, area.contentSize().h);
#endif
}

template<class T>
inline bool isEqual(T value, T expect)
{
    if(value != expect) {
        print("[Value ]", value);
        print("[Expect]", expect);

        return false;
    }

    return true;
}

#define MYEXPECT_EQ(val1, val2) EXPECT_TRUE(isEqual(val1, val2))

class ADPHALUTBase: public ::testing::Test
{
public:
    ADPHALUTBase() {}
    virtual ~ADPHALUTBase() {}

protected:
    virtual void SetUp() {
        // StereoSettingProvider::enableTestMode();
        StereoSettingProvider::setStereoProfile(STEREO_SENSOR_PROFILE_FRONT_FRONT);
        StereoSettingProvider::setStereoFeatureMode(E_STEREO_FEATURE_ACTIVE_STEREO);
        StereoSettingProvider::setImageRatio(eRatio_16_9);
        StereoSettingProvider::setVSDoFZoom(0);

        loadBuffers();

        _inParams.dispMapL = _dispMapL.get();
        _inParams.dispMapR = _dispMapR.get();

        _output.depthmap = _depthmap.get();

        _adpHAL = ADP_HAL::createInstance();
    }

    virtual void TearDown() {
        delete _adpHAL;
        _adpHAL = NULL;
    }

    virtual void init()
    {

    }

    virtual void loadBuffers()
    {
        char filePath[256];
        MSize size(360, 640);

        sprintf(filePath, UT_CASE_IN_FOLDER"/DV_HW_L.raw");
        StereoDpUtil::allocImageBuffer(LOG_TAG, eImgFmt_Y16, size, !IS_ALLOC_GB, _dispMapL);
        readImage(filePath, _dispMapL.get());

        sprintf(filePath, UT_CASE_IN_FOLDER"/DV_HW_R.raw");
        StereoDpUtil::allocImageBuffer(LOG_TAG, eImgFmt_Y16, size, !IS_ALLOC_GB, _dispMapR);
        readImage(filePath, _dispMapR.get());

        StereoDpUtil::allocImageBuffer(LOG_TAG, eImgFmt_Y16, size, !IS_ALLOC_GB, _depthmap);
    }

    virtual bool readImage(char *path, IImageBuffer *image)
    {
        struct stat st;
        ::memset(&st, 0, sizeof(struct stat));

        if(stat(path, &st) == -1) {
            printf("%s does not exist\n", path);
            return false;
        }

        FILE *fp = fopen(path, "r");
        if(fp) {
            for(size_t p = 0; p < image->getPlaneCount(); ++p) {
                fread((void *)image->getBufVA(p), 1, image->getBufSizeInBytes(p), fp);
            }

            fclose(fp);
            fp = NULL;
        } else {
            MY_LOGE("Cannot open %s, err: %s", path, strerror(errno));
            return false;
        }

        return true;
    }

    virtual void readBuffer(char *path, void *&buffer)
    {
        struct stat st;
        ::memset(&st, 0, sizeof(struct stat));

        if(stat(path, &st) == -1) {
            MY_LOGE("%s does not exist", path);
            return;
        }

        FILE *fp = fopen(path, "r");
        if(fp) {
            if(NULL == buffer &&
               st.st_size > 0)
            {
                buffer = new MUINT8[st.st_size];
            }

            fread(buffer, 1, st.st_size, fp);
            fclose(fp);
            fp = NULL;
        } else {
            MY_LOGE("Cannot open %s, err: %s", path, strerror(errno));
        }
    }

protected:
    ADP_HAL *_adpHAL = NULL;
    ADP_HAL_IN_DATA  _inParams;
    ADP_HAL_OUT_DATA _output;

    sp<IImageBuffer> _dispMapL;
    sp<IImageBuffer> _dispMapR;
    sp<IImageBuffer> _depthmap;

    bool LOG_ENABLED = StereoSettingProvider::isLogEnabled("debug.STEREO.log.hal.adp.ut");
};

#endif