#include "ADP_HAL_UT.h"

class ADP_HAL_UT : public ADPHALUTBase
{
public:
    ADP_HAL_UT() { init(); }
    virtual ~ADP_HAL_UT() {}
protected:
    virtual const char *getUTCaseName() { return "ADP_HAL_UT"; }
};

TEST_F(ADP_HAL_UT, TEST)
{
    _adpHAL->ADPHALRun(_inParams, _output);

    //Dump output
    char dumpPath[256];
    MSize dumpSize = _output.depthmap->getImgSize();
    sprintf(dumpPath, UT_CASE_OUT_FOLDER"/Depth_%dx%d.y", dumpSize.w, dumpSize.h);
    _output.depthmap->saveToFile(dumpPath);
    // EXPECT_TRUE(!isResultEmpty());
    // EXPECT_TRUE(isBitTrue());
}
