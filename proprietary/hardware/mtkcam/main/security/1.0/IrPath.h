/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein
 * is confidential and proprietary to MediaTek Inc. and/or its licensors.
 * Without the prior written permission of MediaTek inc. and/or its licensors,
 * any reproduction, modification, use or disclosure of MediaTek Software,
 * and information contained herein, in whole or in part, shall be strictly prohibited.
 */
/* MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
 * AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
 * NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
 * SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
 * SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
 * THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
 * THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
 * CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
 * SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
 * CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
 * AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
 * OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
 * MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek Software")
 * have been modified by MediaTek Inc. All revisions are subject to any receiver's
 * applicable license agreements with MediaTek Inc.
 */

#ifndef _SECURE_IRPATH_H_
#define _SECURE_IRPATH_H_

#include "IPath.h"
#include <utils/RefBase.h>
#include <utils/StrongPointer.h>
#include <semaphore.h>
#include <mtkcam/utils/imgbuf/ISecureImageBufferHeap.h>
#include <mtkcam/utils/metadata/client/mtk_metadata_tag.h>
//
#include <mtkcam/pipeline/pipeline/IPipelineDAG.h>
#include <mtkcam/pipeline/pipeline/IPipelineNode.h>

#include <mtkcam/pipeline/pipeline/PipelineContext.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/StreamId.h>
#include <mtkcam/pipeline/utils/streambuf/StreamBufferPool.h>
#include <mtkcam/pipeline/utils/streambuf/StreamBuffers.h>
#include <mtkcam/pipeline/utils/streaminfo/MetaStreamInfo.h>
#include <mtkcam/pipeline/utils/streaminfo/ImageStreamInfo.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/buffer/BufferPoolImp.h>
//
#include <mtkcam/middleware/v1/LegacyPipeline/buffer/StreamBufferProviderFactory.h>
//
#include <mtkcam/utils/imgbuf/IIonImageBufferHeap.h>
//
#include <mtkcam/drv/IHalSensor.h>
//
#include <mtkcam/pipeline/hwnode/NodeId.h>
#include <mtkcam/pipeline/hwnode/P1Node.h>
//
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
//
#include <mtkcam/utils/metastore/ITemplateRequest.h>
#include <mtkcam/utils/metastore/IMetadataProvider.h>
#include <hardware/camera3.h> // for template
//
#include <mtkcam/drv/iopipe/CamIO/INormalPipe.h>

#include <mtkcam/utils/metadata/IMetadataTagSet.h>
#include <mtkcam/utils/metadata/IMetadataConverter.h>

#include <mtkcam/main/security/utils/Debug.h>

#include <mtkcam/middleware/v1/camshot/BufferCallbackHandler.h>

#include <mtkcam/main/security/ISecureCamera.h>
#include <mtkcam/main/security/depth/1.0/IInputListener.h>
#include <mtkcam/main/security/utils/SecureBufferPool.h>



using namespace NSCam;
using namespace NSCam::v1;
using namespace v3;
using namespace NSCam::v3::Utils;
using namespace android;
using namespace NSCam::v3::NSPipelineContext;
using namespace std;

namespace NSCam {
namespace security {

// ---------------------------------------------------------------------------

enum STREAM_ID{
    STREAM_ID_RAW1 = 1,     // resized raw
    STREAM_ID_RAW2,         // full raw
    //
    STREAM_ID_METADATA_CONTROL_APP,
    STREAM_ID_METADATA_CONTROL_HAL,
    STREAM_ID_METADATA_RESULT_P1_APP,
    STREAM_ID_METADATA_RESULT_P1_HAL,
};

enum NODE_ID{
    NODE_ID_NODE1 = 1,
    NODE_ID_FAKE
};

class IrPath;
// ---------------------------------------------------------------------------

class CallbackListener
{
    public:
        virtual ~CallbackListener() = default;

        virtual MVOID   onMetaReceived(
                MUINT32         const requestNo,
                StreamId_T      const streamId,
                MBOOL           const errorResult,
                IMetadata       const result) = 0;

        virtual MVOID   onDataReceived(
                MUINT32 const requestNo,
                StreamId_T const streamId,
                android::sp<IImageBuffer>& pBuffer) = 0;
};

class ImageCallback
: public IImageCallback
{
    public:
        ImageCallback(
                CallbackListener* pListener,
                MUINT32 const data
                )
            : mpListener(pListener)
              , mData(data)
    {}
    public:   ////    interface of IImageCallback
        /**
         *
         * Received result buffer.
         *
         * @param[in] RequestNo : request number.
         *
         * @param[in] pBuffer : IImageBuffer.
         *
         */
        virtual MERROR onResultReceived(
                MUINT32 const RequestNo,
                StreamId_T const streamId,
                MBOOL   const   /*errorBuffer*/,
                android::sp<IImageBuffer>& pBuffer
                ) {
            if( mpListener ) {
                mpListener->onDataReceived(
                        RequestNo, streamId, pBuffer
                        );
            }
            return OK;
        }
    protected:
        CallbackListener* mpListener;
        MUINT32 const mData;
};

class MetadataListener
: public virtual ResultProcessor::IListener
{
    public:
        MetadataListener(
                CallbackListener* pListener
                )
            : mpListener(pListener)
        {}
        virtual ~MetadataListener() = default;

        // interface of ResultProcessor::IListener
        virtual void onResultReceived(
                MUINT32 const requestNo,
                StreamId_T const streamId,
                MBOOL const errorResult,
                IMetadata const result
                ) override
        {
            if (mpListener)
            {
                mpListener->onMetaReceived(
                        requestNo, streamId, errorResult, result);
            }
        };

        virtual void onFrameEnd(
                MUINT32 const /*requestNo*/) override {};

        virtual String8 getUserName() override { return String8(LOG_TAG); }

    protected:
        CallbackListener* mpListener;

    private:
        MetadataListener() = delete;
};

static void StreamingLoop(IrPath& camera);

// TODO: assure all APIs are thread-safe
class IrPath final : public IPath, public CallbackListener
{
    public:
        IrPath(int maxBuffers);

        ~IrPath();

        // interface of IPath
        void destroyInstance() override;

        Result init(const std::unordered_map<StreamID, IrisStream>& streamMap) override;

        Result unInit() override;

        Result setSrcDev(unsigned int devID) override;

        Result setShutterTime(uint32_t time) override;

        Result setGainValue(uint32_t value) override;

        Result sensorPower(bool turnON) override;

        Result StreamingOn() override;

        Result StreamingOff() override;

        Result getSensorSetting(NSCam::security::SensorStaticInfo& sensorSetting) override;

        std::vector<StreamInfo> getStreamInfo(
                const NSCam::security::SensorStaticInfo& sensorSetting,
                const std::vector<StreamID>& identifiers) override;

        void onBufferReleased() override;

        void registerCallback(Callback* cb, void* priv);

        //
        void inline setListener(std::shared_ptr<IInputListener> inputListener) { mInputListener = inputListener; };
        // TODO: Need to refactor
        void getStreamInfo(sp<IImageStreamInfo> &imageStreamInfo);

    private:
        friend void StreamingLoop(IrPath& camera);

        void prepareConfiguration();

        void setupMetaStreamInfo();

        void setupImageStreamInfo();

        void setupRawBufferPool();

        void setupPipelineContext();

        void setupRequestBuilder();

        void setupInitialMeta(IMetadata *appMeta, IMetadata *halMeta);

        sp<IMetaStreamBuffer> get_default_request();

        IMetaStreamBuffer* createMetaStreamBuffer(
                android::sp<IMetaStreamInfo> pStreamInfo,
                IMetadata const& rSettings,
                MBOOL const repeating
                );

        void processRequest();

        void finishPipelineContext();

        // interface of CallbackListener
        MVOID   onMetaReceived(
                MUINT32         const requestNo,
                StreamId_T      const streamId,
                MBOOL           const errorResult,
                IMetadata       const result) override;

        MVOID   onDataReceived(
                MUINT32 const requestNo,
                StreamId_T const streamId,
                android::sp<IImageBuffer>& pBuffer) override;

        void dumpBuffer(android::sp<IImageBuffer>& pBuffer);

        void setState(const State newState);
        State getState();

        std::pair<State, std::mutex> mState;

        IHalSensor* mpSensorHalObj;
        //
        unsigned int mSensorId;
        std::atomic<int> mDevID;
        MUINT32 mRequestTemplate;

        P1Node::SensorParams mSensorParam;
        P1Node::ConfigParams mP1ConfigParam;
        //
        MSize mRrzoSize;
        MINT mRrzoFormat;
        size_t mRrzoStride;
        //
        MSize mImgoSize;
        MINT mImgoFormat;
        size_t mImgoStride;
        //
        android::sp<PipelineContext> mContext;
        //
        // StreamInfos
        sp<IMetaStreamInfo> mControlMeta_App;
        sp<IMetaStreamInfo> mControlMeta_Hal;
        sp<IMetaStreamInfo> mResultMeta_P1_App;
        sp<IMetaStreamInfo> mResultMeta_P1_Hal;
        //
        sp<IImageStreamInfo> mImage_RrzoRaw;
        sp<IImageStreamInfo> mImage_ImgoRaw;

        // sp<SecureBufferPool>   mFullRawPool;
        // sp<CallbackBufferPool> mResizedRawPool;

        sp<CallbackBufferPool>   mFullRawPool;
        sp<SecureBufferPool>     mResizedRawPool;

        sp<StreamBufferProvider> mImgoProducer;
        sp<StreamBufferProvider> mRrzoProducer;

        sp<BufferCallbackHandler> mCallbackHandler;

        sp<ImageCallback> mCallback;

        sp<MetadataListener> mMetaListener;

        // requestBuilder
        sp<RequestBuilder> mRequestBuilderP1;

        sp<ResultProcessor> mResultProcessor;
        sp<TimestampProcessor> mTimestampProcessor;

        std::thread mStreamWorker;

        sem_t mSemStreamLoop;

        sem_t mSemStreamLoopDone;

        MINT32 mEnableDump;

        // callback lists
        using RegisteredCallback =
            std::unordered_map<iris_callback_descriptor_t,
            std::pair<iris_callback_function_pointer_t, void*>>;
        mutable std::mutex mRegisteredCallbacksLock;
        RegisteredCallback mRegisteredCallbacks;

        mutable std::mutex mStreamMapLock;
        std::unordered_map<StreamID, IrisStream> mStreamMap;

        // Need to modify here
        static int sSensorNum;
        // Listener
        std::shared_ptr<IInputListener> mInputListener;
        // Package for callback to DepthCallback
        std::unordered_map<MUINT32, DepthCallbackInfo> mBufferCallback;
        // Init value
        bool    mbInitDone;
}; // class IrPath

} // namespace security
} // namespace NSCam

#endif // _SECURE_IRPATH_H_