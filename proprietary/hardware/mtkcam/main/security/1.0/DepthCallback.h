#ifndef _DEPTH_CALLBACK_H_
#define _DEPTH_CALLBACK_H_

#include <mtkcam/main/security/ISecureCamera.h>
#include <mtkcam/main/security/depth/1.0/IInputListener.h>
#include <mtkcam/main/security/utils/BufferQueue.h>
#include "IrisTypes.h"
// Depth module
#include <mtkcam/feature/stereo/StereoCamEnum.h>
#include <mtkcam/feature/stereo/pipe/IDepthMapPipe.h>
#include <mtkcam/feature/stereo/pipe/IDepthMapEffectRequest.h>
#include <mtkcam/feature/stereo/pipe/IDualFeatureRequest.h>

#include "../utils/WorkPool.h"

#include <mtkcam/utils/std/Sync.h>

#include <unordered_map>
#include <atomic>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <future>


#define BUFFER_USAGE (GRALLOC_USAGE_SW_READ_OFTEN  |    \
                      GRALLOC_USAGE_SW_WRITE_OFTEN |    \
                      GRALLOC_USAGE_HW_CAMERA_READ |    \
                      GRALLOC_USAGE_HW_CAMERA_WRITE)

using namespace NSCam::NSCamFeature::NSDualFeature;
//using namespace NSCam::NSCamFeature::NSFeaturePipe;
using namespace NSCam::NSCamFeature::NSFeaturePipe_DepthMap;
using std::shared_ptr;
using std::weak_ptr;

namespace NSCam {
namespace security {

// ---------------------------------------------------------------------------

class DepthCallback;
struct StreamInfo; // define in IPath
using Result = android::status_t;
static void depthThreadHandler(DepthCallback& callback);

// ---------------------------------------------------------------------------

class AutoLockImg{
public:
    AutoLockImg(sp<IImageBuffer> img)
        : mImg(img)
    {
        if(mImg.get()) {
            mImg->lockBuf(DEBUG_LOG_TAG, eBUFFER_USAGE_HW_CAMERA_READWRITE | eBUFFER_USAGE_SW_READ_OFTEN);
        }
    }
    ~AutoLockImg(){
        if(mImg.get()) {
            mImg->unlockBuf(DEBUG_LOG_TAG);
            mImg = nullptr;
        }
    }
private:
    sp<IImageBuffer> mImg;
};

class DepthConsumerListener : public IrisBufferQueue::ConsumerListener
{
public:
    DepthConsumerListener();
    ~DepthConsumerListener() = default;
public:
    // interface of IrisBufferQueue::ConsumerListener
    virtual void onBufferQueued(const void* opaqueMessage);
    // interface of DepthConsumerListener
    void init(unsigned int openId, const std::unordered_map<StreamID, IrisStream>& streamMap);
    void inline setListener(std::shared_ptr<IInputListener> inputListener) { mInputListener = inputListener; };
    void releaseBuffer(const std::pair<int, int>& index);
private:
    unsigned int mOpenId;
    std::shared_ptr<IInputListener> mInputListener;
    // stream map is decided at init() and invalidated at unInit()
    // NOTE: for simplicity and efficiency, map can be regarded as READ ONLY
    //       after init() and before unInit() so as to no need to adopt lock
    mutable std::mutex mStreamMapLock;
    std::unordered_map<StreamID, IrisStream> mStreamMap;

    mutable std::mutex mMapLock;
    std::condition_variable mMapCondition;

    // 1. key_type is ION file descriptor and is the same as IrisBuffer.ion_fd
    // 2. this map records in-flight buffers and removes them after
    //    all buffers are consumed and returned
    // 3. this map may exist different streams
    std::unordered_map<int, IrisBufferQueue::IrisBuffer> mMap;
};

// ---------------------------------------------------------------------------

// Used to send input buffers to depth pipe & determine frames sync
// by timestamps
class DepthCallback : public IInputListener
{
public:
    // Interface of IInputListener, we use it to listen to RgbPath.
    // For IMGO
    virtual void onBufferAvailable(unsigned int openId, sp<IImageBuffer> inputBuffer);
    // Interface of IInputListener, we use it to listen to DepthConsumerListener(BufferQueue).
    // For RRZO
    virtual void onMetaAvailable(unsigned int openId, DepthCallbackInfo callbackInfo);
public:

    DepthCallback();

    ~DepthCallback() = default;

    void init(std::vector<StreamInfo> vStreamInfo);

    void unInit();

    void registerCallback(Callback* cb, void* priv);

    void setListener(weak_ptr<DepthConsumerListener> mainListener, weak_ptr<DepthConsumerListener> subListener);
private:
    friend void depthThreadHandler(DepthCallback& Callback);
    void requestExit();

    bool threadLoop();
    bool postprocThreadLoop();

    void notify(const iris_callback_descriptor_t des, const Buffer& result);

    bool cameraCallback(const void* param);

    Result dequeuebuffer(unsigned int openId, DepthResult &inputBuffer);

    Result depthPostProcessing(DepthResult &mainInfo, DepthResult &subInfo);

    bool isTimeSync(uint64_t t1, uint64_t t2, int32_t &dropCamId);

    void releaseBufferToBufferQueue(weak_ptr<DepthConsumerListener> listener, int ionFd);

    // For depth postprocessing
    // TODO: Decouple depth postprocessing from DepthCallback
    sp<IDepthMapEffectRequest> prepareEnqueRequest(int request_id, DepthMapPipeOpState eState
                                                            , sp<IImageBuffer> rrzMain, sp<IImageBuffer> rrzSub);
    static MVOID depthCompleteCallback(MVOID* tag, ResultState state, sp<IDualFeatureRequest>& request);
    //
    mutable std::mutex            mResultQueueLock;
    std::condition_variable       mResultQueueCondition;
    std::thread                   mCallbackWorker;
    //
    std::thread                   mDepthWorker;
    //
    std::atomic<bool>             mRequestExit;
    //Input queue naming
    using InputQueT = std::vector<sp<IImageBuffer>>;
    using ResultQueT = std::vector<DepthResult>;
    // For callback. MainQueue and SubQueue are container of IMGO buffer
    // whitch sent from RgbPath.
    mutable std::mutex            mMainQueueLock;
    std::condition_variable       mMainQueueCondition;
    InputQueT                     mMainQueue;

    mutable std::mutex            mMainResultQueueLock;
    std::condition_variable       mMainResultQueueCondition;
    ResultQueT                    mMainResultQueue;
    //
    mutable std::mutex            mSubQueueLock;
    std::condition_variable       mSubQueueCondition;
    InputQueT                     mSubQueue;

    mutable std::mutex            mSubResultQueueLock;
    std::condition_variable       mSubResultQueueCondition;
    ResultQueT                    mSubResultQueue;
    //
    using DepthQueT = std::vector<std::pair<DepthResult, DepthResult>>;
    mutable std::mutex            mDepthProcLock;
    // Used to notify postproc thread to
    // do postprocessing
    std::condition_variable       mDepthProcCondition;
    DepthQueT                     mDepthProcQueue;

    // For depth
    int32_t                       mReqCount;
    // Use to indicate Postprocessing done
    static std::mutex             mDepthComepeteLock;
    static std::condition_variable       mDepthComepeteCondition;
    sp<WorkPool>                  mDepthPool;
    sp<IImageStreamInfo>          mDepthStreamInfo;
    sp<IImageBuffer>              mDepthImage;
    IDepthMapPipe*                mDepthPipe;
    bool                          mbDepthInit;
    bool                          mbNeedCbIr;

    // callback lists
    using RegisteredCallback =
        std::unordered_map<iris_callback_descriptor_t,
        std::pair<iris_callback_function_pointer_t, void*>>;
    mutable std::mutex mRegisteredCallbacksLock;
    RegisteredCallback mRegisteredCallbacks;

    // DepthConsumerListener. We use it to release buffers.
    weak_ptr<DepthConsumerListener> mMainListener;
    weak_ptr<DepthConsumerListener> mSubListener;

    // For buffer dump
    int mDebugDump;
    std::atomic<bool>               mbIsProcExecute;
    std::future<void>               mPostThread;
}; // class DepthCallback

} // namespace security
} // namespace NSCam

#endif // _DEPTH_CALLBACK_H_
