/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "DepthCamera"

#include "DepthCamera.h"
#include "IrisCallback.h"
#include "DepthCallback.h"

// TODO: add rules for normal or secure path compilation
#include "NirPath.h"
#include "IrPath.h"

#include <linux/ioctl.h>
#include <fcntl.h>

#include <mtkcam/main/security/utils/BufferQueue.h>
#include <mtkcam/main/security/utils/Debug.h>
#include <mtkcam/utils/std/TypeTraits.h>
#include <mtkcam/feature/stereo/hal/stereo_setting_provider.h>


#ifdef LEGACY_PATH // legacy path
#define RESIZED_RAW_OUTPUT
#endif

#define SUB_PATH_OK

using namespace NSCam::security;

// ---------------------------------------------------------------------------

// the amount of ISP working buffers
static constexpr uint32_t kISPWorkingBufferCount = 12;

// ---------------------------------------------------------------------------

template <typename IMPL>
shared_ptr<IMPL> createInstance(int maxBuffers, IPath** interface)
{
    IRIS_LOGD("create instance: maxBuffers(%d)", maxBuffers);

    std::shared_ptr<IMPL> __impl(new IMPL(maxBuffers));
    *interface = __impl.get();

    return __impl;
}

//Removed to secure
ISecureCamera* createDepthCamera()
{
    return new DepthCamera();
}

// ---------------------------------------------------------------------------

DepthCamera::DepthCamera()
{
    AutoLog();
}

DepthCamera::~DepthCamera()
{
    AutoLog();
}

Result DepthCamera::open(Path path)
{
    AutoLog();

    mSecureCameraProxy.reset(new DepthCameraProxy(path));
    return OK;
}

Result DepthCamera::close()
{
    AutoLog();

    mSecureCameraProxy = nullptr;
    return OK;
}

Result DepthCamera::init()
{
    AutoLog();

    if (mSecureCameraProxy->init() != OK)
    {
        IRIS_LOGE("camera init failed");
        unInit();
        return NO_INIT;
    }

    return OK;
}

Result DepthCamera::unInit()
{
    AutoLog();

    if (mSecureCameraProxy != nullptr)
    {
      return mSecureCameraProxy->unInit();
    }

    return OK;
}

Result DepthCamera::sendCommand(Command cmd, intptr_t arg1, intptr_t arg2)
{
    AutoLog();
    IRIS_LOGD("sendCommand cmd(%d)", cmd);

    switch (cmd)
    {
        case Command::STREAMING_ON:
        {
            IRIS_LOGD("STREAMING_ON");
            return mSecureCameraProxy->streamingOn();
        }
        case Command::STREAMING_OFF:
        {
            IRIS_LOGD("STREAMING_OFF");
            return mSecureCameraProxy->streamingOff();
        }
        case Command::REGISTER_CB_FUNC:
        {
            IRIS_LOGD("REGISTER_CB_FUNC");
            return mSecureCameraProxy->registerCallback(reinterpret_cast<Callback*>(arg1), reinterpret_cast<void*>(arg2));
        }
        case Command::RETURN_CB_DATA:
        {
            IRIS_LOGD("RETURN_CB_DATA");
            return mSecureCameraProxy->returnCallbackData(reinterpret_cast<Buffer*>(arg2));
        }
        case Command::SET_SRC_DEV:
        {
            IRIS_LOGD("SET_SRC_DEV");
            return mSecureCameraProxy->setSrcDev(arg1);
        }
        case Command::SET_SHUTTER_TIME:
        {
            IRIS_LOGD("SET_SHUTTER_TIME");
            return mSecureCameraProxy->setShutterTime(arg1);
        }
        case Command::SET_GAIN_VALUE:
        {
            IRIS_LOGD("SET_GAIN_VALUE");
            return mSecureCameraProxy->setGainValue(arg1);
        }
        case Command::SET_SENSOR_CONFIG:
        {
            IRIS_LOGD("SET_SENSOR_CONFIG");
            return mSecureCameraProxy->setSensorConfig(
                    reinterpret_cast<const Configuration*>(arg1));
        }
        case Command::GET_SENSOR_CONFIG:
        {
            IRIS_LOGD("GET_SENSOR_CONFIG");
            const Configuration config = mSecureCameraProxy->getSensorConfig();
            arg1 = reinterpret_cast<intptr_t>(&config);
            break;
        }
        default:
        {
            IRIS_LOGE("unknown command(%u)", NSCam::toLiteral<Command>(cmd));
            return INVALID_OPERATION;
        }
    }
    return OK;
}

// ---------------------------------------------------------------------------

DepthCameraProxy::DepthCameraProxy(Path path)
    : mMainPath(nullptr)
    , mSubPath(nullptr)
{
    AutoLog();

    mDethCallback.reset(new DepthCallback());
    if (!mDethCallback)
        IRIS_LOGE("create callback failed");

    mMainRgbPath = createInstance<IrPath>(kISPWorkingBufferCount, &mMainPath);
#ifdef SUB_PATH_OK
    mSubRgbPath  = createInstance<IrPath>(kISPWorkingBufferCount, &mSubPath);
#endif
    if (!mMainPath) //|| !mSubRgbPath)
        IRIS_LOGE("create IPath failed");

    mFullSizedStream.reset(new IrisBufferQueue(kISPWorkingBufferCount, false, SecType::mem_secure, 0));
    mSubFullSizedStream.reset(new IrisBufferQueue(kISPWorkingBufferCount, false, SecType::mem_secure, 0));

#ifdef RESIZED_RAW_OUTPUT
    mResizedStream.reset(new IrisBufferQueue(kISPWorkingBufferCount, false, SecType::mem_secure, 0));
#endif
}

DepthCameraProxy::~DepthCameraProxy()
{
    AutoLog();

    if (mMainPath)
    {
        mMainPath = nullptr;
    }
#ifdef SUB_PATH_OK
    if (mSubPath)
    {
        mSubPath = nullptr;
    }
#endif
}

Result DepthCameraProxy::init()
{
    AutoLog();

    std::unordered_map<StreamID, IrisStream> streamMap;
    std::unordered_map<StreamID, IrisStream> subStreamMap;

    // NOTE: Full-sized stream must exist
    // TODO: Need to rename
    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        if (!mFullSizedStream)
        {
            IRIS_LOGE("Invalid buffer queue");
            return NO_INIT;
        }
        streamMap.emplace(StreamID::RESIZED_RAW, mFullSizedStream);
        if (!mSubFullSizedStream)
        {
            IRIS_LOGE("Invalid buffer queue");
            return NO_INIT;
        }
        subStreamMap.emplace(StreamID::RESIZED_RAW, mSubFullSizedStream);
    }

#if 0
    {
        std::lock_guard<std::mutex> _l(mResizedStreamLock);
        if (mResizedStream)
            streamMap.emplace(StreamID::RESIZED_RAW, mResizedStream);
    }
#endif
    //Add BufferQueue to RGB Path
    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mMainPath)
        {
            IRIS_LOGE("Invalid path");
            return NO_INIT;
        }
        mMainPath->init(streamMap);
    #ifdef SUB_PATH_OK
        mSubPath->init(subStreamMap);
    #endif
    }

    // TODO: Need to remove openID hard code
    // Setup DepthListener
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    IRIS_LOGD("Derek id(%d, %d)", main1Idx, main2Idx);

    mMainConsumer.reset(new DepthConsumerListener);
    mMainConsumer->init(main1Idx, streamMap);
    mSubConsumer.reset(new DepthConsumerListener);
    mSubConsumer->init(main2Idx, subStreamMap);

    // We get our streming info (RRZO/IMGO)
    SensorStaticInfo sensorSetting; // unused sensorSetting
    std::vector<StreamID> identifiers;
    std::vector<StreamInfo> vStreamInfo;

    identifiers.push_back(StreamID::RESIZED_RAW);
    vStreamInfo = mMainPath->getStreamInfo(sensorSetting, identifiers);

    // Set up BufferParam so that bufferQueue can allocate buffer
    // according to bufferParam.
    // NOTE: the order of identifiers and streamInfo are the same with each other

    IrisBufferQueue::BufferParam param(
            vStreamInfo[0].size.w, vStreamInfo[0].size.h,
            vStreamInfo[0].stride, vStreamInfo[0].format,
            vStreamInfo[0].sizeInBytes,
            IrisBufferQueue::CacheType::CACHEABLE);

    IrisBufferQueue::BufferParam subParam = param;


    // TODO:Set up sub input
    sp<IImageStreamInfo> imageStreamInfo;
    // Query streamInfo from IrPath
    mMainRgbPath->getStreamInfo(imageStreamInfo);
    // Set streamInfo to BufferQueue
    mFullSizedStream->setStreamInfo(imageStreamInfo);
    mFullSizedStream->setBufferParam(std::move(param));
    mFullSizedStream->setConsumerListener(mMainConsumer);
    mMainConsumer->setListener(mDethCallback);

    // Set streamInfo to BufferQueue
    mSubFullSizedStream->setStreamInfo(imageStreamInfo);
    mSubFullSizedStream->setBufferParam(std::move(subParam));
    mSubFullSizedStream->setConsumerListener(mSubConsumer);
    mSubConsumer->setListener(mDethCallback);


    // End setting streamInfo

    {
        std::lock_guard<std::mutex> _l(mDethCallbackLock);
        if (!mDethCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        // TODO: Open it
        mDethCallback->init(vStreamInfo);
    }

    // Connect DepthCallback to DepthListeners
    mDethCallback->setListener(mMainConsumer, mSubConsumer);

    // Used to listen to fullRaw result
    mMainRgbPath->setListener(mDethCallback);
#ifdef SUB_PATH_OK
    mSubRgbPath->setListener(mDethCallback);
#endif
    return OK;
}

Result DepthCameraProxy::unInit()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (mMainPath)
        {
            err = mMainPath->unInit();
            if (err != OK)
            {
                IRIS_LOGE("uninit failed");
                return err;
            }

            mMainPath->destroyInstance();
        }
        //
    #ifdef SUB_PATH_OK
        if (mSubPath)
        {
            err = mSubPath->unInit();
            if (err != OK)
            {
                IRIS_LOGE("uninit failed");
                return err;
            }

            mSubPath->destroyInstance();
        }
    #endif
    }

    {
        std::lock_guard<std::mutex> _l(mDethCallbackLock);
        if (!mDethCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        mDethCallback->unInit();
    }

    {
        std::lock_guard<std::mutex> _l(mFullSizedStreamLock);
        // NOTE: If use_count returns 1, there are no other owners.
        if (mFullSizedStream && (mFullSizedStream.use_count() > 1))
            IRIS_LOGW("IrisBufferQueueFullSized is managed and not release elsewhere");

        // NOTE: If use_count returns 1, there are no other owners.
        if (mSubFullSizedStream && (mSubFullSizedStream.use_count() > 1))
            IRIS_LOGW("mSubFullSizedStream is managed and not release elsewhere");

        mFullSizedStream = nullptr;
        mSubFullSizedStream = nullptr;
    }

    return err;
}

Result DepthCameraProxy::streamingOn()
{
    AutoLog();

    Result err = OK;

    // configure buffer queue parameters
    {
        std::lock_guard<std::mutex> _l(mPathLock);
        // Streaming on main path
        if (mMainPath->StreamingOn() != OK)
        {
            IRIS_LOGE("preview start fail(err=0x%x)",err);
            return UNKNOWN_ERROR;
        }
    #ifdef SUB_PATH_OK
        // Streaming on sub path
        if (mSubPath->StreamingOn() != OK)
        {
            IRIS_LOGE("preview start fail(err=0x%x)",err);
            return UNKNOWN_ERROR;
        }
    #endif
    }

    return OK;
}

Result DepthCameraProxy::streamingOff()
{
    AutoLog();

    Result err = OK;

    {
        std::lock_guard<std::mutex> _l(mPathLock);
        if (!mMainPath)
        {
            IRIS_LOGE("Invalid path");
            return NO_INIT;
        }
        err = mMainPath->StreamingOff();
        IRIS_LOGE_IF(err != OK, "streaming off failed");
        #ifdef SUB_PATH_OK
        //
        if (!mSubPath)
        {
            IRIS_LOGE("Invalid path");
            return NO_INIT;
        }
        err = mSubPath->StreamingOff();
        IRIS_LOGE_IF(err != OK, "streaming off failed");
        #endif
    }

    {
        std::lock_guard<std::mutex> _l(mDethCallbackLock);
        if (!mDethCallback)
        {
            IRIS_LOGE("invalid callback thread");
            return NO_INIT;
        }
        mDethCallback->unInit();
    }

    return err;
}

Result DepthCameraProxy::returnCallbackData(const Buffer* data)
{
    AutoLog();

    if (CC_UNLIKELY(!data))
    {
        IRIS_LOGE("invalid callback data");
        return BAD_VALUE;
    }

    // pass Iris Hal RETURN_CB_DATA message to IrisCallback
    std::lock_guard<std::mutex> _l(mCallbackLock);
    mCallback->releaseBuffer(data->attribute.identifier);

    return OK;
}

Result DepthCameraProxy::registerCallback(Callback* cb, void* priv)
{
    AutoLog();

#if 1
    std::lock_guard<std::mutex> _l(mDethCallbackLock);
    if (!mDethCallback)
    {
        IRIS_LOGE("invalid callback thread");
        return NO_INIT;
    }

    mDethCallback->registerCallback(cb, priv);

#else
    {
        if (!mMainRgbPath)
        {
            IRIS_LOGE("invalid RGB path");
            return NO_INIT;
        }
        // callback to RGB path is realized by the derived class
        mMainRgbPath->registerCallback(cb, priv);
    }
#endif
    return OK;
}

Result DepthCameraProxy::setSensorConfig(const Configuration *config)
{
    AutoLog();
    return OK;
}

Configuration DepthCameraProxy::getSensorConfig() const
{
    std::lock_guard<std::mutex> _l(mConfigurationLock);
    return mConfig;
}

Result DepthCameraProxy::setSrcDev(unsigned int openID)
{
    AutoLog();

    std::lock_guard<std::mutex> _l(mPathLock);
    if (!mMainPath)
    {
        IRIS_LOGE("Invalid path");
        return NO_INIT;
    }

    Result err = OK;
     // Setup DepthListener
    int32_t main1Idx, main2Idx;
    StereoSettingProvider::getStereoSensorIndex(main1Idx, main2Idx);

    err = mMainPath->setSrcDev(main1Idx);
    if (err != OK)
    {
        IRIS_LOGE("set source camera device failed");
        return err;
    }
#ifdef SUB_PATH_OK
    err = mSubPath->setSrcDev(main2Idx);
    if (err != OK)
    {
        IRIS_LOGE("set source camera device failed");
        return err;
    }
#endif
    return err;
}

Result DepthCameraProxy::setShutterTime(uint32_t time __attribute__((unused)))
{
    AutoLog();
    return OK;
}

Result DepthCameraProxy::setGainValue(uint32_t gain __attribute__((unused)))
{
    AutoLog();
    return OK;
}
