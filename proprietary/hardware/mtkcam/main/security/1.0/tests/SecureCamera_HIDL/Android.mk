LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

# securecamera_test - Unit tests of secure camera interface operations
LOCAL_MODULE := securecamera_test
LOCAL_MODULE_TAGS := debug eng tests
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := main.cpp

LOCAL_CFLAGS := -DLOG_TAG=\"$(LOCAL_MODULE)\" -DUSE_SYSTRACE

LOCAL_HEADER_LIBRARIES := libhardware_headers

LOCAL_C_INCLUDES := \
	$(MTK_PATH_CAM)/include

LOCAL_STATIC_LIBRARIES := \
	android.hardware.camera.common@1.0-helper \
	libmtkcam_ionhelper

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libmtkcam_stdutils \
	libmtkcam_imgbuf \
	libhidlbase \
	android.hardware.camera.common@1.0 \
	android.hardware.camera.provider@2.4 \
	android.hardware.graphics.mapper@2.0 \
	vendor.mediatek.hardware.camera.security@1.0

# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
$(info MTK ION is enabled)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
LOCAL_SHARED_LIBRARIES += \
	libion \
	libion_mtk
endif

# MTK TEE (GenieZone)
ifeq ($(strip $(MTK_ENABLE_GENIEZONE)), yes)
$(info MTK TEE (GenieZone) is enabled)
LOCAL_CFLAGS += -DUSING_MTK_TEE
LOCAL_C_INCLUDES += \
	$(MTK_PATH_SOURCE)/geniezone/external/uree/include
LOCAL_STATIC_LIBRARIES += libgz_uree
endif

include $(BUILD_NATIVE_TEST)

endif # MTK_CAM_SECURITY_SUPPORT
