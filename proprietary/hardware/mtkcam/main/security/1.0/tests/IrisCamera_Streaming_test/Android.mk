LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

ifeq ($(strip $(MTK_IRIS_SUPPORT)), yes)

-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := iriscam_streaming_test
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := \
    IrisCamera_Main.cpp

LOCAL_C_INCLUDES := \
    $(MTK_PATH_CAM)/include

LOCAL_SHARED_LIBRARIES := \
	liblog \
	libutils \
	libcutils \
	libmtkcam_stdutils \
	libdl

ifeq ($(MTKCAM_IP_BASE), 1) # non-legacy parts
LOCAL_SHARED_LIBRARIES += \
	libimageio \
	libmtkcam_imgbuf \
	libmtkcam_hwutils
else # legacy parts
LOCAL_CFLAGS += -DLEGACY_PATH
LOCAL_C_INCLUDES += \
	$(MTK_PATH_CAM)/legacy/include \
	$(MTK_PATH_CAM)/legacy/include/mtkcam \
	$(MTK_MTKCAM_PLATFORM) \
	$(MTK_MTKCAM_PLATFORM)/include
LOCAL_SHARED_LIBRARIES += \
	libcam_utils \
	libcam_hwutils
endif

LOCAL_MODULE_TAGS := eng userdebug

LOCAL_CFLAGS += $(MTKCAM_CFLAGS)
LOCAL_CFLAGS := -DLOG_TAG=\"iriscam_streaming_test\"

include $(MTK_EXECUTABLE)

endif # MTK_IRIS_SUPPORT
