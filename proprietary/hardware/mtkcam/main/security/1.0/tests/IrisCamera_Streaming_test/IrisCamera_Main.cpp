/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2017. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */
#define DEBUG_LOG_TAG "Iristest"

#include <dlfcn.h>
#include <mtkcam/main/security/ISecureCamera.h>
#include <mtkcam/main/security/utils/Debug.h>

#include <memory>
#include <condition_variable>
#include <chrono>

#ifdef __LP64__
#define MODULE_PATH "/system/vendor/lib64/libmtkcam_security.so"
#else
#define MODULE_PATH "/system/vendor/lib/libmtkcam_security.so"
#endif

using namespace NSCam::security;

#define OK android::OK

// ------------------------------------------------------------------------

static std::mutex gCallbackLock;
static std::condition_variable gCallbackCondition;

static constexpr std::chrono::duration<int64_t> kCallbackTimeout =
    std::chrono::seconds(1);

// NOTE: Please revise the valid setting for the objective image sensor
// Valid setting for IMX132
//static int32_t kShutterTimeInUS = 30000;
//static int32_t kSensorGain = 1023;

// Valid setting for OV8856
static int32_t kShutterTimeInUS = 2044;
static int32_t kSensorGain = 264;

// ------------------------------------------------------------------------

Buffer g_raw_addr;
bool gResultExist = false;

Result __attribute__((unused)) __addrCallback(const Buffer& buffer
                                                    __attribute__((unused)))
{
    std::lock_guard<std::mutex> _l(gCallbackLock);
    g_raw_addr = buffer;

    gResultExist = true;

    gCallbackCondition.notify_one();

    return OK;
}

// ------------------------------------------------------------------------

int main()
{
    Configuration set_config;
    Configuration get_config;
//    Buffer* p_data = &g_raw_addr;
    int32_t shutter_time_us = kShutterTimeInUS; // 30 msec
    int32_t sensor_gain_value = kSensorGain;
    int32_t acceptedGain = 0;
    int* pt = &acceptedGain;

    // set sensor configuration
    set_config.mode = Mode::IRIS;
    set_config.size.w = 640;
    set_config.size.h = 480;

    int32_t src_dev = 2; // Camera-ID
    bool res = true;

    // the types of the class factories
    typedef ISecureCamera* create_t();

    // load IrisCamera library
    void* irisObj = dlopen(MODULE_PATH, RTLD_NOW);
    if (!irisObj)
    {
        IRIS_LOGE("cannot open iris camera");
        return 1;
    }

    // reset error
    dlerror();

    create_t* getSecureCamera =
        (create_t*)dlsym(irisObj, "getSecureCamera");
    const char* dlsym_error = dlerror();
    if (dlsym_error)
    {
        IRIS_LOGE("cannot load symbol create instance %s", dlsym_error);
        dlclose(irisObj);
        return 1;
    }
    IRIS_LOGD("loaded symbol create instance");

    ISecureCamera* pIrisCam = getSecureCamera();
    if (!pIrisCam)
    {
        return false;
    }

    Callback cbAddr = Callback::createCallback<IRIS_PFN_CALLBACK_ADDR>(
            IRIS_CALLBACK_ADDR, __addrCallback);

    IRIS_LOGD("+");
    IRIS_LOGD("OPEN!");
    if (OK != pIrisCam->open(NSCam::security::Path::IR))
    {
        goto err;
    }
    IRIS_LOGD("OPEN!");
    IRIS_LOGD("-");

    IRIS_LOGD("+");
    IRIS_LOGD("SET SRC DEV");
    // set Camera-ID
    if (OK !=
            pIrisCam->sendCommand(Command::SET_SRC_DEV,
                                (intptr_t)src_dev))
    {
        goto err;
    }
    IRIS_LOGD("SET SRC DEV done");
    IRIS_LOGD("-");

    IRIS_LOGD("+");
    IRIS_LOGD("INIT!");
    if (OK != pIrisCam->init())
    {
        goto err;
    }
    IRIS_LOGD("INIT done");
    IRIS_LOGD("-");

//    Callback cb_handle(IRIS_CB(IRIS_CALLBACK_RAW_HANDLE, __handleCallback));
//    Callback cb_buffer(IRIS_CB(IRIS_CALLBACK_RAW_BUFFER, __bufferCallback));
    IRIS_LOGD("+");
    IRIS_LOGD("register callback!");
    if (OK !=
            pIrisCam->sendCommand(Command::REGISTER_CB_FUNC,
                                (intptr_t)&cbAddr))
    {
        goto err;
    }
    IRIS_LOGD("register callback done!");
    IRIS_LOGD("-");


    IRIS_LOGD("+");
    IRIS_LOGD("SET SENSOR CONFIG!");
    if (OK !=
        pIrisCam->sendCommand(Command::SET_SENSOR_CONFIG,
                                (intptr_t)&set_config))
    {
        goto err;
    }
    IRIS_LOGD("SET SENSOR CONFIG done!");
    IRIS_LOGD("-");

    IRIS_LOGD("+");
    IRIS_LOGD("GET SENSOR CONFIG!");
    // get sensor configuration
    if (OK !=
        pIrisCam->sendCommand(Command::GET_SENSOR_CONFIG,
                                (intptr_t)&get_config))
    {
        goto err;
    }
    IRIS_LOGD("size.width = %d", get_config.size.w);
    IRIS_LOGD("size.height = %d", get_config.size.h);

    IRIS_LOGD("GET_SENSOR_CONFIG done!");
    IRIS_LOGD("-");

    IRIS_LOGD("+");
    IRIS_LOGD("SET_SHUTTER_TIME");
    // set shutter time
    if (OK !=
        pIrisCam->sendCommand(Command::SET_SHUTTER_TIME,
                                (intptr_t)shutter_time_us))
    {
        goto err;
    }
    IRIS_LOGD("SET_SHUTTER_TIME done!");
    IRIS_LOGD("-");

    IRIS_LOGD("+");
    IRIS_LOGD("SET_GAIN_VALUE!");
    IRIS_LOGD("before gain = %d", acceptedGain);
    // set sensor gain
    if (OK !=
        pIrisCam->sendCommand(Command::SET_GAIN_VALUE,
                                (intptr_t)sensor_gain_value,
                                (intptr_t)&acceptedGain))
    {
        goto err;
    }

    IRIS_LOGD("SET_GAIN_VALUE done!");
    IRIS_LOGD("-");

    IRIS_LOGD("after gain = %d", acceptedGain);

    IRIS_LOGD("+");
    IRIS_LOGD("STREAMING ON !");
    // streaming-on; start preview
    if (OK != pIrisCam->sendCommand(Command::STREAMING_ON))
    {
        goto err;
    }
    IRIS_LOGD("STREAMING ON done");
    IRIS_LOGD("-");


    while(1)
    {
        std::unique_lock<std::mutex> _l(gCallbackLock);
        gCallbackCondition.wait_for(_l, kCallbackTimeout,
        [&]
        {
            if (gResultExist)
            {
                IRIS_LOGD("find available callback result");
                gResultExist = false;
                return true;
            }

            return false;
        });

        if (g_raw_addr.addr.virtualAddress != nullptr) {
            IRIS_LOGD("get data = %p\n", g_raw_addr.addr);
            // return callback data to IrisCamera
            if(OK !=
                pIrisCam->sendCommand(Command::RETURN_CB_DATA, IRIS_CALLBACK_ADDR,
                                    (intptr_t)&g_raw_addr)) {
                goto err;
            }
//            break;
        }
    }

    IRIS_LOGD("after reture data = %p\n", g_raw_addr.addr);

    IRIS_LOGD("STREAMING_OFF");
    // streaming-off; stop preview
    if (OK != pIrisCam->sendCommand(Command::STREAMING_OFF))
    {
        goto err;
    }
    IRIS_LOGD("STREAMING_OFF done");

    if (OK != pIrisCam->unInit())
    {
        goto err;
    }

    IRIS_LOGD("CLOSE");
    if (OK != pIrisCam->close())
    {
        goto err;
    }
    IRIS_LOGD("CLOSE done");

    goto exit;

err:
    res = false;

exit:
    delete pIrisCam;
    dlclose(irisObj);

    return (res == true) ? 1 : 0;
}
