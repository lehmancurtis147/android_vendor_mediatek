$(info devicemgr: HAL Version=$(CAMERA_HAL_VERSION))
ifneq ($(CAMERA_HAL_VERSION), 3)
ifneq ($(strip $(MTK_EMULATOR_SUPPORT)), yes)

LOCAL_PATH := $(call my-dir)

include $(call all-makefiles-under, $(LOCAL_PATH))

endif # MTK_EMULATOR_SUPPORT
endif # CAMERA_HAL_VERSION
