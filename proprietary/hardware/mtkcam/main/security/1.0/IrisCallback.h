#ifndef _IRIS_CALLBACK_H_
#define _IRIS_CALLBACK_H_

#include <mtkcam/main/security/ISecureCamera.h>
#include <mtkcam/main/security/utils/BufferQueue.h>
#include "IrisTypes.h"

#include <mtkcam/utils/std/Sync.h>

#include <unordered_map>
#include <vector>
#include <mutex>
#include <condition_variable>
#include <thread>

namespace NSCam {
namespace security {

// ---------------------------------------------------------------------------

class IrisCallback;
static void handler(IrisCallback& callback);

// ---------------------------------------------------------------------------

class IrisCallback final
    : public IrisBufferQueue::ConsumerListener
{
public:
    IrisCallback();

    ~IrisCallback() = default;

    void init(const std::unordered_map<StreamID, IrisStream>& streamMap);

    void unInit();

    void registerCallback(Callback* cb);

    // interface of IrisBufferQueue::ConsumerListener
    void onBufferQueued(const void* opaqueMessage) override;

    // return comsumed buffer back to buffer queue
    // NOTE: index is a pair of Iris::StreamID and ION file descriptor
    void releaseBuffer(const std::pair<int, int>& index);

private:
    friend void handler(IrisCallback& Callback);

    using ResultQueT = std::vector<Buffer>;
    mutable std::mutex mResultQueueLock;
    std::condition_variable mResultQueueCondition;
    ResultQueT mResultQueue;
    bool mRequestExit;

    // callback lists
    using IrisRegisteredCallback =
        std::unordered_map<iris_callback_descriptor_t,
            iris_callback_function_pointer_t>;
    mutable std::mutex mRegisteredCallbacksLock;
    IrisRegisteredCallback mRegisteredCallbacks;

    std::thread mCallbackWorker;

    // stream map is decided at init() and invalidated at unInit()
    // NOTE: for simplicity and efficiency, map can be regarded as READ ONLY
    //       after init() and before unInit() so as to no need to adopt lock
    mutable std::mutex mStreamMapLock;
    std::unordered_map<StreamID, IrisStream> mStreamMap;

    mutable std::mutex mTimelineLock;
    android::sp<NSCam::Utils::Sync::ITimeline> mTimeline;
    int mTimelineCounter;

    mutable std::mutex mMapLock;
    std::condition_variable mMapCondition;

    // 1. key_type is ION file descriptor and is the same as IrisBuffer.ion_fd
    // 2. this map records in-flight buffers and removes them after
    //    all buffers are consumed and returned
    // 3. this map may exist different streams
    std::unordered_map<int, IrisBufferQueue::IrisBuffer> mMap;

    void requestExit();

    bool threadLoop();

    void notify(const iris_callback_descriptor_t des, const Buffer& result);

    bool cameraCallback(const void* param);
}; // class IrisCallback

} // namespace security
} // namespace NSCam

#endif // _IRIS_CALLBACK_H_
