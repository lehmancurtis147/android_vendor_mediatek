/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#define DEBUG_LOG_TAG "MtkCam/SecureBufferPool"
//
#include <mtkcam/utils/std/Log.h>
#include <mtkcam/utils/std/Trace.h>
#include <mtkcam/utils/std/Format.h>
#include <mtkcam/utils/metadata/hal/mtk_platform_metadata_tag.h>
#include <mtkcam/main/security/utils/SecureBufferPool.h>
#include "../1.0/IPath.h"


using namespace android;
using namespace NSCam;
using namespace NSCam::security;
using namespace NSCam::v3;

static bool kIsSecurePath = false;

/******************************************************************************
 *
 ******************************************************************************/
#define MY_LOGV(fmt, arg...)        CAM_LOGV("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGD(fmt, arg...)        CAM_LOGD("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGI(fmt, arg...)        CAM_LOGI("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGW(fmt, arg...)        CAM_LOGW("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGE(fmt, arg...)        CAM_LOGE("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGA(fmt, arg...)        CAM_LOGA("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
#define MY_LOGF(fmt, arg...)        CAM_LOGF("[%d:%s] " fmt, getOpenId(), __FUNCTION__, ##arg)
//
#define MY_LOGV_IF(cond, ...)       do { if ( (cond) ) { MY_LOGV(__VA_ARGS__); } }while(0)
#define MY_LOGD_IF(cond, ...)       do { if ( (cond) ) { MY_LOGD(__VA_ARGS__); } }while(0)
#define MY_LOGI_IF(cond, ...)       do { if ( (cond) ) { MY_LOGI(__VA_ARGS__); } }while(0)
#define MY_LOGW_IF(cond, ...)       do { if ( (cond) ) { MY_LOGW(__VA_ARGS__); } }while(0)
#define MY_LOGE_IF(cond, ...)       do { if ( (cond) ) { MY_LOGE(__VA_ARGS__); } }while(0)
#define MY_LOGA_IF(cond, ...)       do { if ( (cond) ) { MY_LOGA(__VA_ARGS__); } }while(0)
#define MY_LOGF_IF(cond, ...)       do { if ( (cond) ) { MY_LOGF(__VA_ARGS__); } }while(0)
//
#define MY_LOGD1(...)               MY_LOGD_IF((mLogLevel>=1),__VA_ARGS__)
#define MY_LOGD2(...)               MY_LOGD_IF((mLogLevel>=2),__VA_ARGS__)
#define MY_LOGD3(...)               MY_LOGD_IF((mLogLevel>=3),__VA_ARGS__)
//
#define FUNC_START                  MY_LOGD1("+")
#define FUNC_END                    MY_LOGD1("-")
//
#define SECURE_DUMP_PATH "/sdcard/raw/"

/******************************************************************************
 *
 ******************************************************************************/
SecureBufferPool::
SecureBufferPool(
    MINT32  openId,
    sp<IImageStreamInfo> pStreamInfo)
    : mOpenId(openId)
    , mStreamInfo(pStreamInfo)
{
    mLogLevel = ::property_get_int32("debug.camera.log", 0);
    if ( mLogLevel == 0 ) {
        mLogLevel = ::property_get_int32("debug.camera.log.sBuffP", 0);
    }
    //
}


/******************************************************************************
 *
 ******************************************************************************/
SecureBufferPool::
~SecureBufferPool()
{
    Mutex::Autolock _l(mLock);
    mBufferQueue = nullptr;
    mStreamInfo = nullptr;
    mIrisBufferMap.clear();
}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
SecureBufferPool::
acquireFromPool(
    char const*           szCallerName,
    MINT32                rRequestNo,
    sp<IImageBufferHeap>& rpBuffer,
    MUINT32&              rTransform
)
{
    MY_LOGD2("%s", szCallerName);

    Mutex::Autolock _l(mLock);

    return dequeueBufferFromBufferQueue(
            szCallerName,
            rRequestNo,
            rpBuffer,
            rTransform
            );

}

/******************************************************************************
 *
 ******************************************************************************/
MERROR
SecureBufferPool::
dequeueBufferFromBufferQueue(
    char const*           szCallerName __attribute__((unused)),
    MINT32                rRequestNo,
    sp<IImageBufferHeap>& rpBuffer,
    MUINT32&              rTransform
)
{
    std::shared_ptr<IrisBufferQueue>    &pBufferQueue = mBufferQueue;

    if(CC_UNLIKELY(pBufferQueue.get() == nullptr)) {
        MY_LOGE("There is no bufferQueue");
        return NO_MEMORY;
    }

    IrisBufferQueue::IrisBuffer buffer;

    pBufferQueue->dequeueBuffer(&buffer, false /*async*/, false /*isSecure*/);

    MY_LOGD2("dequeue buffer(%d)", buffer.ion_fd);

    rpBuffer = buffer.buffer_heap;
    rTransform  = mStreamInfo->getTransform();
    mIrisBufferMap.add(rRequestNo, buffer);
    return OK;
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
SecureBufferPool::
releaseToPool(
    char const*          szCallerName,
    MINT32               rRequestNo,
    sp<IImageBufferHeap> rpBuffer,
    MUINT64              rTimeStamp,
    bool                 rErrorResult
)
{
    MY_LOGD2("%s", szCallerName);

    Mutex::Autolock _l(mLock);
    return enqueBufferToBufferQueue(
            szCallerName,
            rRequestNo,
            rpBuffer,
            rTimeStamp,
            rErrorResult
            );
}


/******************************************************************************
 *
 ******************************************************************************/
MERROR
SecureBufferPool::
enqueBufferToBufferQueue(
    char const*          szCallerName,
    MINT32               rRequestNo,
    sp<IImageBufferHeap> rpBuffer,
    MUINT64              rTimeStamp __attribute__((unused)),
    bool                 rErrorResult __attribute__((unused))
)
{
    //FUNC_START;
    std::shared_ptr<IrisBufferQueue>    &pBufferQueue = mBufferQueue;

    if(CC_UNLIKELY(pBufferQueue.get() == nullptr)) {
        MY_LOGE("There is no bufferQueue");
        return NO_MEMORY;
    }

    IrisBufferQueue::IrisBuffer buffer;
    ssize_t index = mIrisBufferMap.indexOfKey(rRequestNo);

    if ( index < 0 ) {
        MY_LOGE("[%s][%d] rpBuffer:%p not found.", szCallerName, rRequestNo, rpBuffer.get());
        return UNKNOWN_ERROR;
    }

    buffer = mIrisBufferMap.editValueFor(rRequestNo);
    MY_LOGD2("securebuffer req(%d) fd(%d)", rRequestNo, buffer.ion_fd);
    pBufferQueue->queueBuffer(&buffer);
    // for test
#if 0
    IrisBufferQueue::IrisBuffer getBuffer;

    pBufferQueue->acquireBuffer(&getBuffer);
    pBufferQueue->releaseBuffer(getBuffer.index, -1);
#endif
    // end test
    mIrisBufferMap.removeItem(rRequestNo);
    return OK;
}
/******************************************************************************
 *
 ******************************************************************************/
MVOID
SecureBufferPool::
setBufferQueue(std::shared_ptr<IrisBufferQueue> pBufferQueue)
{
    mBufferQueue = pBufferQueue;
    return;
}

/******************************************************************************
 *
 ******************************************************************************/
char const*
SecureBufferPool::
poolName() const
{
    return "SecureBufferPool";
}

/******************************************************************************
 *
 ******************************************************************************/
MVOID
SecureBufferPool::
dumpPool() const
{
}

void SecureBufferPool::
dumpBuffer(android::sp<IImageBuffer>& pBuffer)
{
    //AutoLog();
    char tmp[512];
    static int frameNo = 0;
    int openId = 0;

    int stride = pBuffer->getImgSize().w * Utils::Format::queryPlaneBitsPerPixel(pBuffer->getImgFormat(), 0) / 8;

    MY_LOGD("stride : %d", stride);

    ::snprintf(tmp, sizeof(tmp), "%sbuf(%d)__%d_%d_%d_%d.raw", SECURE_DUMP_PATH, openId, frameNo++,
                                                    pBuffer->getImgSize().w,
                                                    pBuffer->getImgSize().h,
                                                    stride);

    int fd = ::open(tmp, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
    if(fd < 0)
    {
        MY_LOGE("fail to open %s", tmp);
        return;
    }
    for (size_t i = 0; i < pBuffer->getPlaneCount(); i++)
    {
        MUINT8* pBuf = (MUINT8*)pBuffer->getBufVA(0);
        for (size_t j = 0; j < pBuffer->getImgSize().h; j++)
        {
            ::write(fd, pBuf, stride);
            pBuf += pBuffer->getBufStridesInBytes(0);
        }
    }

    if (fd >= 0) ::close(fd);
}

