$(info devicemgr: HAL Version=$(CAMERA_HAL_VERSION))
ifneq ($(CAMERA_HAL_VERSION), 3)

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

-include $(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

LOCAL_MODULE := libmtkcam_ionhelper
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk

LOCAL_ADDITIONAL_DEPENDENCIES := $(LOCAL_PATH)/Android.mk

MTK_PATH_CAM := $(MTK_PATH_SOURCE)/hardware/mtkcam

LOCAL_SRC_FILES := \
	IonHelper.cpp

LOCAL_HEADER_LIBRARIES := liblog_headers libutils_headers

LOCAL_C_INCLUDES := \
	$(MTK_PATH_CAM)/include \

LOCAL_CFLAGS += $(MTKCAM_CFLAGS) -DLOG_TAG=\"MtkCam/Security\"

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)
LOCAL_CFLAGS += -DMTK_CAM_SECURITY_SUPPORT
endif # MTK_CAM_SECURITY_SUPPORT

# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
endif # MTK_ION_SUPPORT

# MTK TEE (GenieZone)
ifeq ($(strip $(MTK_ENABLE_GENIEZONE)), yes)
$(info MTK TEE (GenieZone) is enabled)
LOCAL_CFLAGS += -DUSING_MTK_TEE
LOCAL_C_INCLUDES += \
	$(MTK_PATH_SOURCE)/geniezone/external/uree/include
endif # MTK_ENABLE_GENIEZONE

include $(MTK_STATIC_LIBRARY)

endif # CAMERA_HAL_VERSION
