
#define LOG_TAG "FRAlgoCA"

#include "FRAlgoCA.h"

// === All FR method: start ===
#include "FRAlgoCA_AS.h"
#include "FRAlgoCA_dummy.h"
#include "FRAlgoCA_2D.h"
// === All FR methods: end ===

#include <fr_err_def.h>
#include <fr_ta_types.h>

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>
#include <cutils/properties.h>

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <mtkcam/utils/std/Format.h>



#define DEBUG_FRALGOCA_LOG_LEVEL "debug.fralgoca.loglevel"
#define FRALGOCA_LOG_DEF_VALUE 1

#define FRALGOCA_FLOW_SIMULATION

#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif

#if 1
#define FUNC_START if (mLogLevel) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (mLogLevel) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif


#ifdef MTEE_USED
#define MTEE_FR_SVC_NAME "com.mediatek.geniezone.fralgo"
#endif

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

FRAlgoCA* FRAlgoCA::createInstance(char const *pCaller, const FRCFG_ENUM frConfig)
{
    ALOGD("FRAlgoCA: %s +: frConfig=%d", __FUNCTION__, (int)frConfig);
    switch(frConfig)
    {
    case FRCFG_FPP_AS:
        ALOGD("FRAlgoCA: %s -: create FRCFG_FPP_AS: %d", __FUNCTION__, frConfig);
        return new FRAlgoCA_AS(pCaller, frConfig);
    case FRCFG_FPP_3DSL:
        // TODO:
        break;
    case FRCFG_FPP_2D:
        ALOGD("FRAlgoCA: %s -: create FRCFG_FPP_2D: %d", __FUNCTION__, frConfig);
        return new FRAlgoCA_2D(pCaller, frConfig);
        break;
    case FRCFG_ST_AS:
        // TODO:
        ALOGD("FRAlgoCA: %s -: NOT yet support: FRCFG_ST_AS: %d", __FUNCTION__, frConfig);
        break;
    case FRCFG_ST_3DSL:
        // TODO:
        ALOGD("FRAlgoCA: %s -: NOT yet support: FRCFG_ST_3DSL: %d", __FUNCTION__, frConfig);
        break;
    case FRCFG_ST_2D:
        ALOGD("FRAlgoCA: %s -: create FRCFG_ST_2D: %d", __FUNCTION__, frConfig);
                // TODO:
//        return new FRAlgoCA_2D(pCaller, frConfig);
        break;
    case FRCFG_DUMMY:
        ALOGD("FRAlgoCA: %s -: create FRCFG_DUMMY: %d", __FUNCTION__, frConfig);
        return new FRAlgoCA_dummy(pCaller, frConfig);
    default:
        return NULL;
    }

    return NULL;
}

FRAlgoCA::FRAlgoCA(char const *pCaller, const FRCFG_ENUM frConfig)
{
    //!!NOTES: loglevel is only inited once in constructor
    mLogLevel = ::property_get_int32(DEBUG_FRALGOCA_LOG_LEVEL, FRALGOCA_LOG_DEF_VALUE);

    mFrConfig = frConfig;
    if (pCaller != NULL)
    {
        strncpy(mCallerName, pCaller, FRCATA_USER_MAX_NAME_LEN-1);
        mCallerName[FRCATA_USER_MAX_NAME_LEN-1] = 0;
    }

    FUNC_START;
    mIsInited = 0;
    reset();
    FUNC_END;
}

FRAlgoCA::~FRAlgoCA()
{
    FUNC_START;
    reset();
    FUNC_END;
}

ISecureCameraClientCallback* FRAlgoCA::getSecureCameraClientCallback()
{
    if (mSecureCamCliCb == NULL)
    {
        return new FRAlgoCA::SecureCamClientCallback(this);
    }
    else
    {
        return mSecureCamCliCb.get();
    }
}

hidl_death_recipient* FRAlgoCA::getHidlDeathRecipient()
{
    if (mHidlDeathRecipient == NULL)
    {
        return new FRAlgoCA::HidlDeathRecipient(this);
    }
    else
    {
        return mHidlDeathRecipient.get();
    }
}

int FRAlgoCA::seccam_init()
{
    FUNC_START;

    if (mHidlSecureCam != NULL)
    {
        ALOGW("%s: mHidlSecureCam(%p) is already inited", __FUNCTION__, mHidlSecureCam.get());
        FUNC_END;
        return FR_ERR_SECCAM_ALREADY_INITED;
    }

    Return<bool> linked = false;
    ::android::hardware::camera::common::V1_0::Status err = ::android::hardware::camera::common::V1_0::Status::OK;

    // prepare securecam callback
    // FR-custom
    mSecureCamCliCb = getSecureCameraClientCallback();
    mHidlDeathRecipient = getHidlDeathRecipient();

    // create secure hidl service
    mHidlSecureCam = ISecureCamera::getService("internal/0");
    if (mHidlSecureCam == NULL) {
        ALOGE("!!err: %s: failed to get secure camera interface", __FUNCTION__);
        FUNC_END;
        return FR_ERR_SECCAM_GET_SVC_FAILED;
    }
    else
    {
        ALOGD("%s: get secure camera interface done", __FUNCTION__);
    }

    // error handling when service is dead
    linked = mHidlSecureCam->linkToDeath(
                this->mHidlDeathRecipient, /*cookie*/ this->mKCookie);
    if (!linked.isOk()) {
        ALOGE("!!err: %s: Transaction error in linking to SecureCamera death: %s",
                __FUNCTION__, linked.description().c_str());
        FUNC_END;
        return FR_ERR_SECCAM_LINK_TO_DEATH_1_FAILED;
    } else if (!linked) {
        ALOGE("!!err: %s: Unable to link to SecureCamera death notifications",
                __FUNCTION__);
        FUNC_END;
        return FR_ERR_SECCAM_LINK_TO_DEATH_1_FAILED;
    }

    // get camera id list
    std::vector<std::string> devices;
    mHidlSecureCam->getCameraIdList([&err, &devices](
            ::android::hardware::camera::common::V1_0::Status idStatus,
            const hidl_vec<hidl_string>& cameraDeviceIDs) {
        err = idStatus;
        if (err == Status::OK) {
            for (size_t i = 0; i < cameraDeviceIDs.size(); i++) {
                devices.push_back(cameraDeviceIDs[i]);
            }
        } });
    for (auto&& str : devices)
    {
        CAM_LOGD("available secure camera device(%s)", str.c_str());
    }

    if (devices.empty())
    {
        ALOGW("%s: unavailable secure camera device, exit...", __FUNCTION__);
        FUNC_END;
        return FR_ERR_SECCAM_NO_AVAIL_DEVICES;
    }

    mSecCamDeviceList = hidl_vec<hidl_string>({devices.front()});
    ALOGD("%s: mSecCamDeviceList = %s", __FUNCTION__, (mSecCamDeviceList.data())->c_str());

    const hidl_string device(devices.front());

#define LOCAL_REGISTER_SECCAM_CB_BEFORE_OPEN_CAMERA 1

#if LOCAL_REGISTER_SECCAM_CB_BEFORE_OPEN_CAMERA // register callback changed before open since 5/E
    // register callback (HIDL server to HIDL client)
    err = mHidlSecureCam->registerCallback(mSecureCamCliCb);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: register callback (%s): NG", __FUNCTION__, device.c_str());
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->registerCallback: OK(%s)\n", __FUNCTION__, device.c_str());
    }
#endif

    // open the first secure camera
    err = mHidlSecureCam->open(mSecCamDeviceList);
    if (err != Status::OK)
    {
         ALOGE("!!err: %s: open secure camera failed(%s)", __FUNCTION__, device.c_str());
         FUNC_END;
        return FR_ERR_SECCAM_OPEN_FAILED;
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->open: OK(%s)\n", __FUNCTION__, device.c_str());
    }


    // initialize the first secure camera
    if (mHidlSecureCam->initialize(mSecCamDeviceList) != Status::OK)
    {
        ALOGE("!!err: %s: initialize secure camera failed(%s)", __FUNCTION__, device.c_str());
        FUNC_END;
        return FR_ERR_SECCAM_INIT_FAILED;
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->initialize: OK(%s)\n", __FUNCTION__, device.c_str());
    }

#if LOCAL_REGISTER_SECCAM_CB_BEFORE_OPEN_CAMERA // register callback changed before open since 5/E
    // ori:
#else
    // register callback (HIDL server to HIDL client)
    err = mHidlSecureCam->registerCallback(mSecureCamCliCb);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: register callback (%s): NG", __FUNCTION__, device.c_str());
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->registerCallback: OK(%s)\n", __FUNCTION__, device.c_str());
    }
#endif


#ifdef FRALGOCA_FLOW_SIMULATION
    sim_securecam_registerCallback();
#endif

#if 0 // call sequence changed after 05/E
    // register callback (HIDL server to HIDL client)
    err = mHidlSecureCam->registerCallback(mSecureCamCliCb);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: register callback (%s): NG", __FUNCTION__, device.c_str());
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->registerCallback: OK(%s)\n", __FUNCTION__, device.c_str());
    }
#endif
    FUNC_END;
    return FR_ERR_OK;
}

int FRAlgoCA::tee_init(const char *pFRSvcName)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    TZ_RESULT retTzRet;
    // step-1: create session
    retTzRet = UREE_CreateSession(pFRSvcName, &mMTeeSession);
    if (retTzRet != TZ_RESULT_SUCCESS)
    {
        ALOGE("!!err: %s:: create algota session failed: tzRet: %d", __FUNCTION__, retTzRet);
        return FR_ERR_TEE_CREATE_SESSION_FAILED;
    }
    else
    {
        ALOGD("%s:: create algota session ok, session=0x%x", __FUNCTION__, &mMTeeSession);
    }
    FUNC_END;
    return FR_ERR_OK;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoCA::fralgo_init()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    TZ_RESULT ret;
    MTEEC_PARAM param[4];
    uint32_t types;

     /* !!NOTES: PARAM format
    // param[0]: TZPT_VALUE_INPUT: value.a = [frConfig]
    // param[1]: TZPT_VALUE_INOUT: value.a = [errCode]/default: FR_ERR_ALGO_OK
    // param[2]: TZPT_VALUE_INPUT: value.a = [debugCfg]/default: 0
    */
    const int PIDX_FRCFG = 0;
    const int PIDX_FR_RET = 1;
    const int PIDX_FR_DBG_CFG = 2;

    param[PIDX_FRCFG].value.a = mFrConfig;
    param[PIDX_FR_RET].value.a = FR_ERR_OK;

    param[PIDX_FR_DBG_CFG].value.a = 0;
    if (mPropTaLogLevel)
    {
        FRAS_TA_FEATURE_MASK_ENABLE_LOGLEVEL(param[PIDX_FR_DBG_CFG].value.a);
    }
    if (mPropTaCallAlgo)
    {
        FRAS_TA_FEATURE_MASK_ENABLE_CALL_ALGO(param[PIDX_FR_DBG_CFG].value.a);
    }
    if (mPropTaChkPolicy)
    {
        FRAS_TA_FEATURE_MASK_ENABLE_CHK_POLICY(param[PIDX_FR_DBG_CFG].value.a);
    }

    ALOGD("%s: mFrConfig=%d, debugCfg=0x%x", __FUNCTION__, param[PIDX_FRCFG].value.a, param[PIDX_FR_DBG_CFG].value.a);
    types = TZ_ParamTypes3(TZPT_VALUE_INPUT, TZPT_VALUE_INOUT, TZPT_VALUE_INPUT);
    ret = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_INIT, types, param);

    if (ret != TZ_RESULT_SUCCESS)
    {
        FUNC_END;
        return FR_ERR_TEE_SERVICE_CALL_FAILED;
    }
    FUNC_END;
    return param[PIDX_FR_RET].value.a;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoCA::workthread_init()
{
    FUNC_START;

    int ret = FR_ERR_OK;
    ::sem_init(&mSemDataIn, 0, 0);
    ::sem_init(&mSemThreadEnd, 0, 0);
    if (createFRWorkThread() != 0) // virtual function
    {
        ret = FR_ERR_FRALGO_CREATE_THREAD_FAILED;
        ALOGE("!!err: %s:: create thread failed", __FUNCTION__);
    }
    else
    {
        ALOGD("%s::create FR working thread OK", __FUNCTION__);
    }

    FUNC_END;
    return ret;
}


int FRAlgoCA::init()
{
    // step-1: create session
    // step-2: create SecureCam and call its init(..) function
    // step-3: create thread to handle SecureCam callback and callback to App

    FUNC_START;

    Mutex::Autolock lock(mLock);

    int ret = 0;

    if (mIsInited != 0)
    {
        ALOGE("!!err: %s::FRAlgoCA is already inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_ALREADY_INITED; // already initied
    }

    if ((ret = tee_init(MTEE_FR_SVC_NAME)) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = seccam_init()) != FR_ERR_OK && ret != FR_ERR_SECCAM_ALREADY_INITED)
    {
        goto err_init;
    }

    if ((ret = fralgo_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    // create thread to handle SecureCam callback and callback to App
    // pthread init
    if ((ret = workthread_init()) != FR_ERR_OK)
    {
        goto err_init;
    }


#ifdef FRALGOCA_FLOW_SIMULATION
        sim_MTEE_CA_to_TA();
        sim_MTEE_FACEPP_LIB();
#endif

    mState = FRCA_STATE_ALIVE;
    mIsInited = 1;

    return FR_ERR_OK;

err_init:

    ALOGW("%s:: FRAlgoCA init failed %d", __FUNCTION__, ret);
    reset();

    FUNC_END;
    return ret;
}

int FRAlgoCA::uninit()
{
    FUNC_START;

    {
        Mutex::Autolock lock(mLock);
        mState = FRCA_STATE_UNINIT; // change uninit state first to stop FRWorkThread and OnBufferThread
    }

    Mutex::Autolock lock(mLock);
    reset();
    FUNC_END;

    return FR_ERR_OK;
}

int FRAlgoCA::seccam_uninit()
{
    FUNC_START;

    ::android::hardware::camera::common::V1_0::Status err = ::android::hardware::camera::common::V1_0::Status::OK;

    // uninitialize the first secure camera
    if (mHidlSecureCam != NULL)
    {
#if FRAS_CA_SHOULD_BE_RESTORED // TODO: delete here?
        // do nothing because stopCapture is done in StopSaveFeature and StopCompareFeature
#else
        ALOGD("%s: going to call : mHidlSecureCam(%p)->uninitialize(%s)...  ", __FUNCTION__, mHidlSecureCam.get(), (mSecCamDeviceList.data())->c_str());
        err = mHidlSecureCam->stopCapture(mSecCamDeviceList);
        if (err != Status::OK)
        {
            ALOGE("!!err: %s: mHidlSecureCam->stopCapture(%s): NG", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }
        else
        {
            ALOGD("%s: mHidlSecureCam->stopCapture(%s): OK ", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }
#endif
        err = mHidlSecureCam->uninitialize(mSecCamDeviceList);
        if (err != Status::OK)
        {
            ALOGE("!!err: %s: mHidlSecureCam->uninitialize(%s): NG", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }
        else
        {
            ALOGD("%s: mHidlSecureCam->uninitialize(%s): OK ", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }

        // close the first secure camera
        err = mHidlSecureCam->close(mSecCamDeviceList);
        if (err != Status::OK)
        {
            ALOGE("!!err: %s: mHidlSecureCam->close(%s): NG", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }
        else
        {
            ALOGD("%s: mHidlSecureCam->close(%s): OK ", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        }
    }
    mHidlSecureCam = NULL;
    mSecureCamCliCb = NULL;
    mHidlDeathRecipient = NULL;

    mSecCamDeviceList = hidl_vec<hidl_string>();

    FUNC_END;

    return FR_ERR_OK;
}

int FRAlgoCA::tee_uninit()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;


    // MTeeSession
    if (mMTeeSession != (UREE_SESSION_HANDLE) (-1))
    {
        TZ_RESULT tzRet;
        tzRet = UREE_CloseSession(mMTeeSession);
        if (tzRet != TZ_RESULT_SUCCESS)
        {
            ALOGE("!!err: %s, UREE_CloseSession: NG, ret=%d", __FUNCTION__, tzRet);
        }
        else
        {
            ALOGD("%s, UREE_CloseSession: OK: ret=%d", __FUNCTION__, tzRet);
        }
    }
    mMTeeSession = (UREE_SESSION_HANDLE) (-1);

    FUNC_END;

    return FR_ERR_OK;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

int FRAlgoCA::fralgo_uninit()
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    TZ_RESULT ret;
    MTEEC_PARAM param[4];
    uint32_t types;

    /* !!NOTES: PARAM format
        // param[0]: TZPT_VALUE_INPUT: value.a = [frConfig]
        // param[1]: TZPT_VALUE_INOUT: value.a = [errCode]/default: FR_ERR_ALGO_OK
    */
    const int PIDX_FRCFG = 0;
    const int PIDX_FR_RET = 1;

    param[PIDX_FRCFG].value.a = mFrConfig;
    param[PIDX_FR_RET].value.a = FR_ERR_OK;

    types = TZ_ParamTypes2(TZPT_VALUE_INPUT, TZPT_VALUE_INOUT);
    ret = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_UNINIT, types, param);
    ret = TZ_RESULT_ERROR_GENERIC;

    FUNC_END;

    return param[PIDX_FR_RET].value.a;

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif

}

int FRAlgoCA::workthread_uninit()
{
    FUNC_START;

    if (mFRWorkThread != (pthread_t) 0)
    {
        ALOGD("%s: wait mSemDataIn", __FUNCTION__);
        ::sem_post(&mSemDataIn);
        ALOGD("%s: wait mSemThreadEnd", __FUNCTION__);
        ::sem_wait(&mSemThreadEnd);
        ALOGD("%s: got mSemThreadEnd", __FUNCTION__);
        pthread_join(mFRWorkThread, NULL);
        ALOGD("%s: mFRDequeThread closed", __FUNCTION__);

        ::sem_destroy(&mSemDataIn);
        ::sem_destroy(&mSemThreadEnd);
    }
    mFRWorkThread = (pthread_t)0;

    FUNC_END;

    return FR_ERR_OK;
}

void FRAlgoCA::reset()
{
    FUNC_START;

    int ret = 0;

    mState = FRCA_STATE_UNINIT; // to stop FR thread
    if (mIsInited != 0)
    {
        if ((ret = seccam_uninit()) != FR_ERR_OK)
        {
            goto err_uninit;
        }

        // pthread
        if ((ret = workthread_uninit()) != FR_ERR_OK)
        {
            goto err_uninit;
        }

        if ((ret = fralgo_uninit()) != FR_ERR_OK)
        {
            goto err_uninit;
        }

        if ((ret = tee_uninit()) != FR_ERR_OK)
        {
            goto err_uninit;
        }

    }

    mUserName[0] = 0;
//    mLogLevel // set in FRAlgoCA constructor

    mState = FRCA_STATE_NONE;
    mCallerName[0] = 0;

    mKCookie = 0;

    mMTeeSession = (UREE_SESSION_HANDLE) (-1);
    mHidlSecureCam = NULL;
    mSecureCamCliCb = NULL;
    mHidlDeathRecipient = NULL;
    mSecCamDeviceList = hidl_vec<hidl_string>();
    mCurrCmd = FRALGOCA_CMD_NONE;

    mFRAlgoCACliCb = NULL;

    mIsInited = 0;

    FUNC_END;
    return;

err_uninit:

    ALOGW("%s:: FRAlgoCA reset failed %d", __FUNCTION__, ret);

    FUNC_END;
    return;
}

void FRAlgoCA::registerCallback(const sp<IFRAlgoCAClientCallback>& clientCallback)
{
    FUNC_START;
    if (mFRAlgoCACliCb != NULL)
    {
        ALOGW("%s: already registered: oldCliCb: %p, newCliCb: %p", __FUNCTION__, mFRAlgoCACliCb.get(), clientCallback.get());
    }
    mFRAlgoCACliCb = clientCallback;
    FUNC_END;
}

FRCA_STATE_ENUM FRAlgoCA::getFRCAState(void)
{
    FUNC_START;
    FUNC_END;
    return mState;
}

void FRAlgoCA::setFRCAState(const FRCA_STATE_ENUM &aState)
{
    FUNC_START;
    ALOGD("aState(%d),mState(%d)",aState,mState);
    Mutex::Autolock lock(mLock);
    mState = aState;
    FUNC_END;
}

FRCA_CMD FRAlgoCA::getCurrCmd()
{
    FUNC_START;
    FUNC_END;
    return mCurrCmd;
}

void FRAlgoCA::setCurrCmd(const FRCA_CMD &newCmd)
{
    FUNC_START;
    ALOGD("newCmd(%d),currCmd(%d)", newCmd, mCurrCmd);
    Mutex::Autolock lock(mLock);
    mCurrCmd = newCmd;
    FUNC_END;
}

void FRAlgoCA::callbackFRAlgoCAClient(FRCA_CMD cmd, int retCode)
{
    FUNC_START;
    if (mFRAlgoCACliCb != NULL)
    {
        mFRAlgoCACliCb->onFRAlgoCAClientCallback(cmd, retCode);
    }
    FUNC_END;
}

int FRAlgoCA::handleStartSaveFeature(intptr_t arg1, intptr_t arg2)
{
    /* !!NOTES:
        arg1 --> c_str::userName
        art2 --> int::userNameLen
    */

    int ret = FR_ERR_OK;
    Status err = Status::OK;
    const char *pUserName = (char*) arg1;

    FUNC_START;
    // === error Handling ===
    if (mIsInited == 0)
    {
        ALOGE("!!err: %s: FRAlgoCA NOT inited", __FUNCTION__);
        ret = FR_ERR_FRALGO_NOT_INITED; // already initied
        goto err_handleStartSaveFeature;
    }

    if (arg1 == 0 || arg2 <= 0)
    {
        ALOGE("!!err: %s: user name empty: pUserName: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_EMPTY;
        goto err_handleStartSaveFeature;
    }
    if (arg2 >= FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s: user name too long: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_TOO_LONG;
        goto err_handleStartSaveFeature;
    }

    // === Start processiong ===
    strncpy(mUserName, pUserName, arg2);
    mUserName[arg2]  = 0;

    if ((ret = seccam_init()) != FR_ERR_OK && ret != FR_ERR_SECCAM_ALREADY_INITED)
    {
        ALOGE("!!err: %s: seccam_init failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        ret = FR_ERR_SECCAM_INIT_FAILED;
        goto err_handleStartSaveFeature;
    }

    err = mHidlSecureCam->startCapture(mSecCamDeviceList);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: startCapture failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        ret = FR_ERR_SECCAM_START_CAPTURE_FAILED;
        goto err_handleStartSaveFeature;
    }

    //!!NOTES: Buffers will be retrevied at onBufferAvailable callback (..)

    FUNC_END;
    return FR_ERR_OK;

err_handleStartSaveFeature:
    {
        int ret2 = seccam_uninit();
        if (ret2 != FR_ERR_OK)
        {
            // don't care
            ALOGW("!!warn: %s: seccam_uninit failed", __FUNCTION__);
        }
    }

    mUserName[0] = 0;
    FUNC_END;
    return ret;
}

int FRAlgoCA::handleStopSaveFeature(intptr_t arg1, intptr_t arg2)
{
    (void) arg1;
    (void) arg2;

    int ret = FR_ERR_OK;
    Status err = Status::OK;

    FUNC_START;
    if (mIsInited == 0)
    {
        ALOGE("!!err: %s::FRAlgoCA NOT inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_NOT_INITED; // already initied
    }

    // stop capture
    ALOGD("%s: to call SecureCam::startCapture", __FUNCTION__);
    err = mHidlSecureCam->stopCapture(mSecCamDeviceList);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: startCapture failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        return FR_ERR_SECCAM_STOP_CAPTURE_FAILED;
    }
    ALOGD("%s: SecureCam::startCapture DONE", __FUNCTION__);

    ret = seccam_uninit();
    if (ret != FR_ERR_OK)
    {
        // don't care much
        ALOGW("!!warn: %s: seccam_uninit failed", __FUNCTION__);
    }
    ALOGD("%s: SecureCam::seccam_uninit DONE", __FUNCTION__);

    FUNC_END;

    return ret;
}

int FRAlgoCA::handleDeleteFeature(intptr_t arg1, intptr_t arg2)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    /* !!NOTES:
        arg1 --> c_str::userName
        art2 --> int::userNameLen
    */

    int ret = FR_ERR_OK;
    const char *pUserName = (char*) arg1;
    const int PIDX_USER_NAME = 0;
    const int PIDX_RET_VALUE = 1;
    TZ_RESULT retTee;
    MTEEC_PARAM param[4];
    uint32_t types;

    FUNC_START;

    if (arg1 == 0 || arg2 <= 0)
    {
        ALOGE("!!err: %s: user name empty: pUserName: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_EMPTY;
        goto err_handleDeleteFeature;
    }
    if (arg2 >= FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s::!!err: user name too long: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_TOO_LONG;
        goto err_handleDeleteFeature;
    }
//    ALOGD("%s:: user name: %s, size: %d", __FUNCTION__, (char*)arg1, arg2);

    // passing delete command
     /* !!NOTES: PARAM format
     // param[0]: TZPT_MEM_INPUT: mem.buffer= userName
     // param[0]: TZPT_MEM_INPUT: mem.size= userNameLen
     // param[1]: TZPT_VLAUE_INOUT: ret-value
    */

    param[PIDX_USER_NAME].mem.buffer = (void*) arg1;
    param[PIDX_USER_NAME].mem.size = arg2;
    param[PIDX_RET_VALUE].value.a = FR_ERR_OK; // for return value

    types = TZ_ParamTypes2(TZPT_MEM_INPUT, TZPT_VALUE_INOUT);

    ALOGD("%s:: debug: FRTA_CMD_DELETE_FEATURE=%d", __FUNCTION__, FRTA_CMD_DELETE_FEATURE);

    retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_DELETE_FEATURE, types, param);

    if (retTee != TZ_RESULT_SUCCESS)
    {
        ALOGE("!!err: %s: UREE_TeeServiceCall failed: ret=%d", __FUNCTION__, retTee);
        ret = FR_ERR_FRALGO_SERVICE_CALL_FAILED;
        goto err_handleDeleteFeature;
    }
    ret = param[PIDX_RET_VALUE].value.a;

    mUserName[0] = 0;

    FUNC_END;
    return FR_ERR_OK;

err_handleDeleteFeature:
    FUNC_END;
    return ret;

#else // non-MTK_CAM_SECURITY_SUPPORT
    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif

}

int FRAlgoCA::handleStartCompareFeature(intptr_t arg1, intptr_t arg2)
{
    /* !!NOTES:
        arg1 --> c_str::userName
        art2 --> int::userNameLen
    */

    int ret = FR_ERR_OK;
    Status err = Status::OK;
    const char *pUserName = (char*) arg1;

    FUNC_START;
    // === error Handling ===
    if (mIsInited == 0)
    {
        ALOGE("!!err: %s: FRAlgoCA NOT inited", __FUNCTION__);
        ret = FR_ERR_FRALGO_NOT_INITED; // already initied
        goto err_handleStartCompareFeature;
    }

    if (arg1 == 0 || arg2 <= 0)
    {
        ALOGE("!!err: %s: user name empty: pUserName: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_EMPTY;
        goto err_handleStartCompareFeature;
    }
    if (arg2 >= FRCATA_USER_MAX_NAME_LEN)
    {
        ALOGE("!!err: %s: user name too long: %p, arg2: %d", __FUNCTION__, pUserName, arg2);
        ret = FR_ERR_FRALGO_USER_NAME_TOO_LONG;
        goto err_handleStartCompareFeature;
    }

    // === Start processiong ===
    strncpy(mUserName, pUserName, arg2);
    mUserName[arg2]  = 0;

    if ((ret = seccam_init()) != FR_ERR_OK && ret != FR_ERR_SECCAM_ALREADY_INITED)
    {
        ALOGE("!!err: %s: seccam_init failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        ret = FR_ERR_SECCAM_INIT_FAILED;
        goto err_handleStartCompareFeature;
    }

    err = mHidlSecureCam->startCapture(mSecCamDeviceList);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: startCapture failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        ret = FR_ERR_SECCAM_START_CAPTURE_FAILED;
        goto err_handleStartCompareFeature;
    }

    //!!NOTES: Buffers will be retrevied at onBufferAvailable callback (..)

    FUNC_END;
    return FR_ERR_OK;

err_handleStartCompareFeature:
    {
        int ret2 = seccam_uninit();
        if (ret2 != FR_ERR_OK)
        {
            // don't care
            ALOGW("!!warn: %s: seccam_uninit failed", __FUNCTION__);
        }
    }

    mUserName[0] = 0;
    FUNC_END;
    return ret;
}

int FRAlgoCA::handleStopCompareFeature(intptr_t arg1, intptr_t arg2)
{
    int ret = FR_ERR_OK;
    Status err = Status::OK;

    FUNC_START;
    if (mIsInited == 0)
    {
        ALOGE("!!err: %s::FRAlgoCA NOT inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_NOT_INITED; // already initied
    }
    // stop capture
    ALOGD("%s: to call SecureCam::startCapture", __FUNCTION__);
    err = mHidlSecureCam->stopCapture(mSecCamDeviceList);
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: startCapture failed(s%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
        return FR_ERR_SECCAM_STOP_CAPTURE_FAILED;
    }
    ALOGD("%s: SecureCam::startCapture DONE", __FUNCTION__);

    ret = seccam_uninit();
    if (ret != FR_ERR_OK)
    {
        // don't care much
        ALOGW("!!warn: %s: seccam_uninit failed(%s)", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
    }

    FUNC_END;

    return ret;
}

int FRAlgoCA::sendCmd(FRCA_CMD_ENUM cmd, intptr_t arg1, intptr_t arg2)
{
    int ret = FR_ERR_OK;

    Mutex::Autolock lock(mLock);

    FUNC_START;
    switch(cmd)
    {
    case FRALGOCA_CMD_START_SAVE_FEATURE:
        mState = FRCA_STATE_SAVE_FEATURE;
        ret = this->handleStartSaveFeature(arg1, arg2);
        break;
    case FRALGOCA_CMD_STOP_SAVE_FEATURE:
        mState = FRCA_STATE_STOP_FEATURE;
        ret = this->handleStopSaveFeature(arg1, arg2);
        break;
    case FRALGOCA_CMD_DELETE_FEATURE:
        mState = FRCA_STATE_DELETE_FEATURE;
        ret = this->handleDeleteFeature(arg1, arg2);
        break;
    case FRALGOCA_CMD_START_COMPARE_FEATURE:
        mState = FRCA_STATE_COMPARE_FEATURE;
        ret = this->handleStartCompareFeature(arg1, arg2);
        break;
    case FRALGOCA_CMD_STOP_COMPARE_FEATURE:
        mState = FRCA_STATE_STOP_FEATURE;
        ret = this->handleStopCompareFeature(arg1, arg2);
        break;
    default:
        ret = FR_ERR_CMD_UNKNOWN;
        ALOGE("!!err: %s: unknown case", __FUNCTION__);
        break;
    }

    if (ret == FR_ERR_OK)
    {
        mCurrCmd = cmd;
    }

    FUNC_END;

    return  ret;
}


void *FRAlgoCA::FRWorkThreadLoop(void *arg)
{
    ALOGD("%s +", __FUNCTION__);
    FRAlgoCA *_this = reinterpret_cast<FRAlgoCA *>(arg);

    FRCA_STATE_ENUM eState = _this->getFRCAState();

    while (eState != FRCA_STATE_UNINIT)
    {
        ::sem_wait(&_this->mSemDataIn);
        eState = _this->getFRCAState();
        switch(eState)
        {
        case FRCA_STATE_ALIVE:
            // TODO:
            ALOGD("%s: FRCA_STATE_ALIVE", __FUNCTION__);
            break;
        case FRCA_STATE_UNINIT:
            ALOGD("%s: FRCA_STATE_UNINIT", __FUNCTION__);
            break;
        default:
            ALOGE("!!err: %s: State Error", __FUNCTION__);
            break;
        }
        eState = _this->getFRCAState();
    }

    ::sem_post(&_this->mSemThreadEnd);
    ALOGD("%s -", __FUNCTION__);

    return NULL;
}

int FRAlgoCA::createFRWorkThread()
{
    FUNC_START;
    pthread_create(&mFRWorkThread, NULL, FRWorkThreadLoop, this);
    FUNC_END;
    return 0;
}

void FRAlgoCA::HidlDeathRecipient::serviceDied(
        uint64_t cookie, const wp<IBase>& who)
{
    ALOGD("%s +", __FUNCTION__);

    (void) who;

    if (cookie != mKCookie) {
        ALOGW("%s: Unexpected serviceDied cookie %" PRIu64 ", expected %" PRIu32,
                __FUNCTION__, cookie, mKCookie);
    }

    // TODO: do something here if necessary when the HAL server has gone

    // release HIDL interface
    auto parent = mwpFRAlgoCA.promote();
    if (parent.get())
    {
        if (parent->mHidlDeathRecipient != NULL)
        {
            parent->mHidlDeathRecipient.clear();
        }
    }

    ALOGD("%s -", __FUNCTION__);

}

Return<void> FRAlgoCA::SecureCamClientCallback::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
    // TODO: FR-custom

    ALOGD("%s +", __FUNCTION__);

    ALOGD("%s -", __FUNCTION__);
    return Void();
}



// === simulation test ===
#ifdef FRALGOCA_FLOW_SIMULATION
void FRAlgoCA::sim_MTEE_CA_to_TA(void)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    FUNC_START;

    ALOGD("mkdbg: %s called", __FUNCTION__);

    TZ_RESULT ret;
    MTEEC_PARAM param[4];
    uint32_t types;

    param[0].value.a = 000;
    param[1].value.a = 111;
//    param[2].value.a = 222; // param[2] should be written by TA
    types = TZ_ParamTypes3(TZPT_VALUE_INPUT, TZPT_VALUE_INPUT, TZPT_VALUE_OUTPUT);

    ALOGD("mkdbg: TZ_ParamTypes3 called: types: %d", types);

    ret = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_TEST_ION_SHARE, types, param);
    ALOGD("mkdbg: UREE_TeeServiceCall(333) called: ret: %d, param: %d", ret, param[2].value.a);

    FUNC_END;
#else // non-MTK_CAM_SECURITY_SUPPORT
    return;
#endif
}

void FRAlgoCA::sim_MTEE_FACEPP_LIB(void)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
        FUNC_START;

        ALOGD("mkdbg: %s called", __FUNCTION__);

        TZ_RESULT ret;
        MTEEC_PARAM param[4];
        uint32_t types;

        param[0].value.a = 000;
        param[1].value.a = 000;
        types = TZ_ParamTypes2(TZPT_VALUE_INPUT, TZPT_VALUE_INPUT);
        ret = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_TEST_FPP_LIB_INIT, types, param);
        ALOGD("mkdbg: UREE_TeeServiceCall(444) called: ret: %d", ret);

        FUNC_END;
#else  // non-MTK_CAM_SECURITY_SUPPORT
        return;
#endif
}

void FRAlgoCA::sim_securecam_registerCallback(void)
{
#if 0 // markInfo: comment out temporarily
    // register callback (HIDL server to HIDL client)
    const sp<FRAlgoCA::SecureCamClientCallback> &sp_cliCb_ori = this->mSecureCamCliCb;
    const sp<ISecureCameraClientCallback> &sp_cliCb_from_api_i = this->getISecureCameraClientCallback_i();
    const sp<FRAlgoCA::SecureCamClientCallback> &sp_cliCb_from_api_real = this->getISecureCameraClientCallback_real();

    ALOGD("%s: mkdbg: ori_cli_p.get=%p, p_from_api_i.get=%p, p_from_api_real.get=%p\n",
        __FUNCTION__,
        sp_cliCb_ori.get(),
        sp_cliCb_from_api_i.get(),
        sp_cliCb_from_api_real.get()
        );

#if 0 // mkdbg: debug usage
    ::android::hardware::camera::common::V1_0::Status err =
        ::android::hardware::camera::common::V1_0::Status::OK;
    err = mHidlSecureCam->registerCallback_test(
        sp_cliCb_ori,
        sp_cliCb_from_api_i,
        sp_cliCb_from_api_real
    );
    if (err != Status::OK)
    {
        ALOGE("!!err: %s: registerCallback_test (%s): NG", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
    }
    else
    {
        ALOGD("%s: gHidlSecureCamera->registerCallback_test: OK(%s)\n", __FUNCTION__, (mSecCamDeviceList.data())->c_str());
    }
#endif

#endif
}
#endif // FRALGOCA_FLOW_SIMULATION


}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

