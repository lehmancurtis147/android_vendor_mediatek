#ifndef VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_H
#define VENDOR_MEDIATEK_HARDWARE_CAMERA_FRHANDLER_FRALGOCA_H

#include <utils/RefBase.h> // android::RefBase

#include <pthread.h>
#include <semaphore.h>
#include <utils/Mutex.h>  

// === CA related ===
#include "HandleImporter.h"
using ::android::hardware::camera::common::V1_0::helper::HandleImporter;
#include <mtkcam/main/security/utils/IonHelper.h>

// === FA TA related ===
#include "fr_ta_types.h"

#define MTEE_USED

#ifdef MTEE_USED
#include <uree/system.h>
#include <uree/mem.h>
#endif

// === debug usage: start===
#define FRALGOCA_DUMP_NEEDED 1
// === debug usage: end

// SecureCamera related -- start
#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCamera.h>
#include <vendor/mediatek/hardware/camera/security/1.0/ISecureCameraClientCallback.h>
// SecureCamera related -- end

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {


using ::android::sp;
using ::android::wp;
using ::android::Mutex;
using ::android::RefBase;

using ::android::hidl::base::V1_0::IBase;

using ::android::RefBase;

using ::android::hardware::Return;
// using ::android::hardware::Status;
using ::android::hardware::Void;
using ::android::hardware::hidl_death_recipient;
using ::android::hardware::hidl_handle;
using ::android::hardware::hidl_vec;
using ::android::hardware::hidl_string;

using ::android::hardware::camera::common::V1_0::Status;

using vendor::mediatek::hardware::camera::security::V1_0::ISecureCamera;
using vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback;

using vendor::mediatek::hardware::camera::security::V1_0::Stream;


struct IFRAlgoCAClientCallback : public ::android::hidl::base::V1_0::IBase
{
    virtual void onFRAlgoCAClientCallback(const int32_t fr_cmd_id, const int32_t errCode) = 0;
};


class FRAlgoCA : public android::RefBase {

private:
    // FR-custom: start
    struct SecureCamClientCallback : virtual public ISecureCameraClientCallback
    {
        SecureCamClientCallback(const wp<FRAlgoCA>& frAlgoCA, uint64_t cookie=0) : mwpFRAlgoCA(frAlgoCA), mKCookie(cookie) {}
        // Implementation of vendor::mediatek::hardware::camera::security::V1_0::ISecureCameraClientCallback
        virtual Return<void> onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream) override;

        wp<FRAlgoCA> mwpFRAlgoCA;
        const uint64_t mKCookie;
    };
    // FR-custom: end

    struct HidlDeathRecipient : virtual public hidl_death_recipient
    {
        HidlDeathRecipient(const wp<FRAlgoCA>& frAlgoCA, uint64_t cookie=0) : mwpFRAlgoCA(frAlgoCA), mKCookie(cookie) {}
        // Implementation of hidl_death_recipient interface
        void serviceDied(uint64_t cookie, const wp<IBase>& who) override;
    
        wp<FRAlgoCA> mwpFRAlgoCA;
        const uint64_t mKCookie;
    };


public:
    // FR-custom: start
    FRAlgoCA(const char *pCaller, const FRCFG_ENUM frConfig);
    virtual ~FRAlgoCA();
    virtual void reset();
    virtual int createFRWorkThread(); // every FRAlgoCA should have their own FRThreadLoop

public: // All the functions the derived class need to implement
    virtual int init() = 0;
    virtual int uninit() = 0;
    virtual ISecureCameraClientCallback* getSecureCameraClientCallback() = 0;
    virtual int fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream) = 0;
    // FR-custom: end

public: // FRAlgoCA public APIs
    static FRAlgoCA *createInstance(char const *pUserName, const FRCFG_ENUM frConfig);

    void registerCallback(const sp<IFRAlgoCAClientCallback>& clientCallback); // use parent
    int sendCmd(FRCA_CMD_ENUM cmd, intptr_t arg1, intptr_t arg2);

    FRCA_STATE_ENUM getFRCAState(void);
    void setFRCAState(const FRCA_STATE_ENUM &aState);
    
    void callbackFRAlgoCAClient(FRCA_CMD cmd, int retCode);

    void setCurrCmd(const FRCA_CMD &aCmd);
    FRCA_CMD getCurrCmd();

protected:
    hidl_death_recipient* getHidlDeathRecipient();

    int seccam_init();
    int seccam_uninit();
    int tee_init(const char *pFRSvcName);
    int tee_uninit();
    int fralgo_init(); // for AlgoCA to override
    int fralgo_uninit();// for AlgoCA to override
    int workthread_init();// for AlgoCA to override
    int workthread_uninit();// for AlgoCA to override

    int handleStartSaveFeature(intptr_t arg1, intptr_t arg2);
    int handleStopSaveFeature(intptr_t arg1, intptr_t arg2);
    int handleDeleteFeature(intptr_t arg1, intptr_t arg2);
    int handleStartCompareFeature(intptr_t arg1, intptr_t arg2);
    int handleStopCompareFeature(intptr_t arg1, intptr_t arg2);

    static void *FRWorkThreadLoop(void *arg);

public: // MTEE related functions
    // === simulation related ===
    void sim_MTEE_CA_to_TA(void);
    void sim_MTEE_FACEPP_LIB(void);
    void sim_securecam_registerCallback(void);

public:
    // mutex
    mutable Mutex    mLock;
    // pthread
    pthread_t mFRWorkThread;
    sem_t mSemDataIn;
    sem_t mSemThreadEnd;

protected:
    // common
    char mUserName[FRCATA_USER_MAX_NAME_LEN];
    char mCallerName[FRCATA_USER_MAX_NAME_LEN];
    int mIsInited;
    int mLogLevel;
    FRCA_STATE_ENUM mState;


    uint64_t mKCookie;
    FRCFG_ENUM mFrConfig;
    FRCA_CMD_ENUM mCurrCmd;

    // securecam
    sp<ISecureCamera> mHidlSecureCam;
    sp<ISecureCameraClientCallback> mSecureCamCliCb;
    sp<hidl_death_recipient> mHidlDeathRecipient;
    sp<IFRAlgoCAClientCallback> mFRAlgoCACliCb;
    hidl_vec<hidl_string> mSecCamDeviceList;

    // MTEE related
    UREE_SESSION_HANDLE mMTeeSession;

public:
	// property
	int32_t mPropNeedPrepFRInput; // for fralgo_prepareFRInput use
protected:
	int32_t mPropSeccamCbBufDump;
	int32_t mPropSrcType;
	int32_t mPropCaLogLevel;
	int32_t mPropCaCallTa;
	int32_t mPropTaLogLevel;
	int32_t mPropTaCallAlgo;
	int32_t mPropTaChkPolicy;
};


}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware

#endif


