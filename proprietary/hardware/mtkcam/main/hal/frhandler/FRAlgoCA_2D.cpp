
#define LOG_TAG "FRAlgoCA_2D"

#include "FRAlgoCA_2D.h"
#include <fr_err_def.h>

#include <cutils/log.h>
#include <mtkcam/utils/std/Log.h>
#include <cutils/properties.h>

#include <ion/ion.h>
#include <ion.h>
#include <linux/mtk_ion.h>
#include <linux/ion_drv.h>

#include <mtkcam/utils/std/Format.h>

#define DEBUG_FRALGOCA_2D_LOG_LEVEL "debug.fralgoca_2d.loglevel"
#define FRALGOCAAS_LOG_DEF_VALUE 1

#ifdef FUNC_START
#undef FUNC_START
#endif
#ifdef FUNC_START
#undef FUNC_END
#endif

#if 1
#define FUNC_START if (mLogLevel) { ALOGD("%s +", __FUNCTION__); }
#define FUNC_END if (mLogLevel) { ALOGD("%s -", __FUNCTION__); }
#else
#define FUNC_START
#define FUNC_END
#endif



#ifdef MTEE_USED
#ifdef MTEE_FR_SVC_NAME
    #undef MTEE_FR_SVC_NAME
#endif
#define MTEE_FR_SVC_NAME "com.mediatek.geniezone.fralgo"
#endif

// === mkdbg: debug usage ===
#define FRALGOCA_2D_DUMP_NEEDED 1
#define FRALGOCA_2D_LOG_ON 1
#define FRALGOCA_2D_LOG_OFF 0
// === mkdbg: end

namespace vendor {
namespace mediatek {
namespace hardware {
namespace camera {
namespace frhandler {

// === FRAlgo2DInputCollector APIs ===
FRAlgo2DInputCollector::FRAlgo2DInputCollector()
{
    // resource to be init-ed before reset()
    mLogLevel = FRALGOCA_2D_LOG_ON; //!!NOTES: default log level = 1
    mImportedBuffer = NULL;
    mIonHandle = 0;

    mIonDevFd = mt_ion_open(LOG_TAG);
    if (mIonDevFd < 0)
    {
        ALOGE("%s: open Ion device failed!", __FUNCTION__);
        mIonDevFd = -1;
    }

    reset();
}

FRAlgo2DInputCollector::~FRAlgo2DInputCollector()
{
    FUNC_START;

    reset();

    // resource to be released after reset()
    if (mIonDevFd > 0)
    {
        ion_close(mIonDevFd);
        mIonDevFd = -1;
    }

    FUNC_END;
}


void FRAlgo2DInputCollector::reset()
{
    FUNC_START;
    if (mImportedBuffer != NULL)
    {
        mHandleImporter.freeBuffer(mImportedBuffer);
        mImportedBuffer = NULL;
        mBuf.gzHandle = 0;
    }
    if (mIonDevFd > 0)
    {
        // free ionHandle
        releaseSecureInfo(mIonHandle);
        mIonHandle = 0;
        //!!NOTES: mIonDevFd won't be freed until FRAlgoCollector destructor
    }

    mBuf.size =  0;
    mBuf.width =  0;
    mBuf.height =  0;
    mBuf.stride =  0;
    mBuf.format =  0;
    mBuf.timestampHigh = 0;
    mBuf.timestampLow = 0;

    mBuf.chksum =  FRCATA_BUF_CHECKSUM;

    mState = FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW;

    mBuf.reserved3 = 0;
    mBuf.reserved4 = 0;
    mBuf.reserved5 = 0;
    mBuf.reserved6 = 0;

    FUNC_END;
}

int FRAlgo2DInputCollector::setData(
    InputCollectState_ENUM eState, uint64_t bufferId, const hidl_handle& hidl_buffer, const Stream& stream)
{
    FUNC_START;

    int ret = FR_ERR_OK;
    mBuf.bufferId = bufferId;
    TZ_RESULT retTee;

    if (hidl_buffer == nullptr)
    {
        ALOGE("%s: !!buffer should not be nullptr", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_IC_INVALID_ARG;
    }

    if (eState == FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW)
    {
        // reset the previous allocated resources for (RAW)
        reset();

        mImportedBuffer = hidl_buffer.getNativeHandle();
        
        auto secureInfo = acquireSecureInfo(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW, (mImportedBuffer)->data[0]);
        mBuf.gzHandle = secureInfo.first;
        uint32_t secureMemorySize = static_cast<uint32_t>(secureInfo.second);
        if (secureInfo.second != secureMemorySize)
        {
            ALOGE("%s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != secureMemorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %llu, secureMemorySize: %d ", __FUNCTION__, stream.size, secureMemorySize);
        }
        mBuf.size = secureMemorySize;

        mBuf.width = stream.width;
        mBuf.height = stream.height;
        mBuf.stride = stream.stride;
        mBuf.format = stream.format;
        mBuf.timestampHigh = 0; // !!NOTES: no need if hw-sync
        mBuf.timestampLow = 0; // // !!NOTES: no need if hw-sync
    }
    else if (eState == FRAlgo2DInputCollector::FRAIC_WAIT_FOR_YUV)
    {
        mImportedBuffer = hidl_buffer.getNativeHandle();
        mHandleImporter.importBuffer(mImportedBuffer);

        auto secureInfo = acquireSecureInfo(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_YUV, (mImportedBuffer)->data[0]);
        mBuf.gzHandle = secureInfo.first;
        uint32_t secureMemorySize = static_cast<uint32_t>(secureInfo.second);
        if (secureInfo.second != secureMemorySize)
        {
            ALOGE("%s: mispatch size after narrowed down", __FUNCTION__);
            ret = FR_ERR_FRALGO_IC_MISMATCH_SIZE;
        }
        if (stream.size != secureMemorySize)
        {
            ALOGW("%s: mispatch size: stream.size: %d, secureMemorySize: %d ", __FUNCTION__, stream.size, secureMemorySize);
        }
        mBuf.size = secureMemorySize;

        mBuf.width = stream.width;
        mBuf.height = stream.height;
        mBuf.stride = stream.stride;
        mBuf.format = stream.format;
        mBuf.timestampHigh = 0; // !!NOTES: no need if hw-sync
        mBuf.timestampLow = 0; // // !!NOTES: no need if hw-sync
    }
    else
    {
        ALOGE("%s: !!err strange state: %d", __FUNCTION__, eState);
        // reset the previous buffer
        reset();

    }
    
    FUNC_END;
    return ret;
}

std::pair<int /* phyAddr */, unsigned int/*size*/> FRAlgo2DInputCollector::acquireSecureInfo(InputCollectState_ENUM eState, const int shareFD)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
    ion_user_handle_t ionHandle = 0;

    if (mIonDevFd > 0)
    {
        int ret =  ion_import(mIonDevFd, shareFD, &ionHandle);
        ALOGD("%s: eState: %d, ionHandle imported : %d", __FUNCTION__, eState, ionHandle);
    }
    
    struct ion_sys_data sys_data;
    sys_data.sys_cmd = ION_SYS_GET_PHYS;
    sys_data.get_phys_param.handle = ionHandle;
    if (ion_custom_ioctl(mIonDevFd, ION_CMD_SYSTEM, &sys_data))
    {
        ALOGE("%s: ion_custom_ioctl failed to get secure handle", __FUNCTION__);
        return std::pair<int, unsigned int>();
    }

    if (eState ==  FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW)
    {
        mIonHandle = ionHandle;
    }
    else if (eState ==  FRAlgo2DInputCollector::FRAIC_WAIT_FOR_YUV)
    {
        mIonHandle = ionHandle;
    }
    else
    {
        ALOGE("%s: unknown state: %d", __FUNCTION__, eState);
    }

    // transfer secure handle to gz secure handle
    TZ_RESULT ret;
    UREE_SHAREDMEM_HANDLE gz_shm_handle;
    ret = UREE_ION_TO_SHM_HANDLE(sys_data.get_phys_param.phy_addr, &gz_shm_handle);
    if (ret != TZ_RESULT_SUCCESS)
    {
        ALOGE("%s: UREE_ION_TO_SHM_HANDLE failed: ret=%d", __FUNCTION__, ret);
        return std::pair<int, unsigned int>();
    }

    ALOGI("%s: Secure memory: ionHandle(%d), size(%lu), "
            "gzHandle(%d)",
            __FUNCTION__,
            sys_data.get_phys_param.handle, sys_data.get_phys_param.len,
            gz_shm_handle
            );

    return std::pair<int, unsigned int>(gz_shm_handle, sys_data.get_phys_param.len);

#else // non-#ifdef MTK_CAM_SECURITY_SUPPORT
    return std::pair<int, unsigned int>();
#endif
}

void FRAlgo2DInputCollector::releaseSecureInfo(const ion_user_handle_t& ionHandle)
{
    if (ionHandle > 0)
    {
        ion_free(mIonDevFd, ionHandle);
    }
}

void FRAlgo2DInputCollector::dumpBuffer()
{
#if FRALGOCA_2D_DUMP_NEEDED
    NSCam::security::IonHelper helper;
    size_t dataSize = 0;
    int sharedFD = 0;

    if (mImportedBuffer != NULL)
    {
        sharedFD = (mImportedBuffer)->data[0];

        ALOGD("%s: dump RAW-Buf: sharedFD=%d, size=%d, %dx%d, stride=%d, format=%d ",
            __FUNCTION__,
            sharedFD, mBuf.size, mBuf.width, mBuf.height, mBuf.stride, mBuf.format);
            
        helper.dumpBuffer(sharedFD, 
            mBuf.size, mBuf.width, mBuf.height, mBuf.stride, mBuf.format, true);
    }
#endif // FRALGOCA_2D_DUMP_NEEDED
}
FRAlgo2DInputCollector::InputCollectState_ENUM FRAlgo2DInputCollector::getCurrState()
{
    return mState;
}

void FRAlgo2DInputCollector::setCurrState(FRAlgo2DInputCollector::InputCollectState_ENUM state)
{
    ALOGD("%s: oldState: %d, newState: %d", __FUNCTION__, mState, state);
    mState = state;
}


// === FRAlgoCA_2D APIs ===
FRAlgoCA_2D::FRAlgoCA_2D(char const *pCaller, const FRCFG_ENUM frConfig)
    :FRAlgoCA(pCaller, frConfig)
{
    FUNC_START;
    mIsInited = 0;
    reset();
    FUNC_END;
}

FRAlgoCA_2D::~FRAlgoCA_2D()
{
    FUNC_START;
    reset();
    FUNC_END;
}

ISecureCameraClientCallback* FRAlgoCA_2D::getSecureCameraClientCallback()
{
    if (mSecureCamCliCb == NULL)
    {
        return new FRAlgoCA_2D::SecureCamClientCallback(this);
    }
    else
    {
        return mSecureCamCliCb.get();
    }
}

int FRAlgoCA_2D::init()
{
    // step-1: create session
    // step-2: create SecureCam and call its init(..) function

    FUNC_START;

    Mutex::Autolock lock(mLock);

    int ret = 0;

    if (mIsInited != 0)
    {
        ALOGE("%s::FRAlgoCA is already inited", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_ALREADY_INITED; // already initied
    }

    if ((ret = FRAlgoCA::tee_init(MTEE_FR_SVC_NAME)) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::seccam_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    if ((ret = FRAlgoCA::fralgo_init()) != FR_ERR_OK)
    {
        goto err_init;
    }

    mState = FRCA_STATE_ALIVE;
    mIsInited = 1;

    return FR_ERR_OK;

err_init:

    ALOGW("%s:: FRAlgoCA init failed %d", __FUNCTION__, ret);
    reset();

    FUNC_END;
    return ret;
}

int FRAlgoCA_2D::uninit()
{
    // FR-custom
    FUNC_START;

    {
        Mutex::Autolock lock(mLock);
        mState = FRCA_STATE_UNINIT; // change uninit state first to stop FRWorkThread and OnBufferThread
    }

    Mutex::Autolock lock(mLock);
    reset();
    
    FUNC_END;
    return FR_ERR_OK;
}

void FRAlgoCA_2D::reset()
{
    FUNC_START;

    FRAlgoCA::reset();

    // TODO: reset other member var here
    FUNC_END;
    return;
}

int FRAlgoCA_2D::fralgo_prepareFRInput(uint64_t bufferId, const hidl_handle& buffer, const Stream& stream)
{
#ifdef MTK_CAM_SECURITY_SUPPORT
// FR-custom
    FUNC_START;

    if (mState == FRCA_STATE_UNINIT)
    {
        ALOGD("%s: State: FRCA_STATE_UNINIT, no need to process", __FUNCTION__);
        FUNC_END;
        return FR_ERR_FRALGO_NOT_INITED;
    }

    int ret = FR_ERR_ALGO_OK;
    TZ_RESULT retTee;
    MTEEC_PARAM param[4];
    uint32_t types;

    /* Assume buffer is correct */
    FRAlgo2DInputCollector::InputCollectState_ENUM currState = mFRAlgoIC.getCurrState();

    ALOGD("%s: currCmd: %d, currState: %d",  __FUNCTION__, mCurrCmd, currState);

//    if (currState == FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW /* stream.format == 111 */) // TODO:
    if (currState == FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW && stream.width==1304) // TODO:
    {
            mFRAlgoIC.reset();

            if ((ret=mFRAlgoIC.setData(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW, bufferId, buffer, stream)) != FR_ERR_OK)
            {
                ALOGD("%s: currCmd: %d, currState: %d, setData fail: ret=%d, go back to WAIT_FOR_RAW state",  __FUNCTION__, mCurrCmd, currState, ret);
                mFRAlgoIC.setCurrState(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW);
                FUNC_END;
                return ret;
            }

             // set state to FRAIC_WAIT_FOR_ALGO_HANDLING
             mFRAlgoIC.setCurrState(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_ALGO_HANDLING);

            param[0].value.a = FRCATA_2D_BUF_NUM_NEEDED; // # num of buffers
            param[0].value.b = FR_ERR_ALGO_OK; // --> FRAlgoTA ret-value

            param[1].mem.buffer = (void*) mUserName; // # num of buffers
            param[1].mem.size = FRCATA_USER_MAX_NAME_LEN;
            param[2].mem.buffer = (void*) &(mFRAlgoIC.mBuf);
            param[2].mem.size = FRCATA_BUF_FIXED_SIZE;
        
#if FRALGOCA_2D_DUMP_NEEDED
            // === Prepare RAW debug buffer ===
            const char *kMemSrvName = "com.mediatek.geniezone.srv.mem";
            UREE_SESSION_HANDLE memSrvSession;

            // create session for shared memory
            retTee = UREE_CreateSession(kMemSrvName, &memSrvSession);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                ALOGE("%s: create mem sesion failed(%d)", __FUNCTION__, ret);
            }

            // Allocate a normal memory  (maybe used as FRAlgo member for debug usage only)
            std::unique_ptr<char [], std::function<void (char[]) >>
                nonSecureSharedMem(reinterpret_cast<char *>(memalign(sysconf(_SC_PAGESIZE), mFRAlgoIC.mBuf.size)), [] (char memory[])
                {
                    ALOGD("Release nonSecureSharedMem(%s)", FRCATA_FACEID_POSTFIX_2D_RAW);
                    free(memory);
                });
            // Register IR Shared Memory to FRAlgoTA
            UREE_SHAREDMEM_HANDLE sharedMemoryHandle;
            UREE_SHAREDMEM_PARAM sharedMemoryParam {
                    .buffer = nonSecureSharedMem.get(), 
                    .size = mFRAlgoIC.mBuf.size
                };
            retTee = UREE_RegisterSharedmem(memSrvSession, &sharedMemoryHandle, &sharedMemoryParam);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                ALOGE("%s: IR UREE_RegisterSharedmem failed: ret=%d",__FUNCTION__, retTee);
            }
            mFRAlgoIC.mBuf.reserved6 = sharedMemoryHandle;
            // now wait for MTEE service to use it
#endif
        
            types = TZ_ParamTypes3(TZPT_VALUE_INOUT, TZPT_MEM_INPUT, TZPT_MEM_INOUT);
            if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
            {
                retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_SAVE_FEATURE, types, param);
                ret = param[0].value.b;
            }
            else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
            {
                retTee = UREE_TeeServiceCall(mMTeeSession, FRTA_CMD_START_COMPARE_FEATURE, types, param);
                ret = param[0].value.b;
            }
            else
            {
                // do nothing
                ALOGW("%s: wrong cmd: %d", __FUNCTION__, mCurrCmd);
                mFRAlgoIC.setCurrState(FRAlgo2DInputCollector::FRAIC_STOP);
                ret = FR_ERR_FRALGO_WRONG_CMD;
            }

#if FRALGOCA_2D_DUMP_NEEDED
            // MTEE service finished using nonSecureSharedMem, now unregister shared memory
            retTee = UREE_UnregisterSharedmem(memSrvSession, sharedMemoryHandle);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                ALOGE("%s: UREE_UnregisterSharedmem failed(RAW): ret=%d",__FUNCTION__, retTee);
            }
            // save the sharedMemoroy data to file
            {
                char fileName[512];
                static int sSaveFCountFile = 0;
                static int sCompFCountFile = 0;
                static int sUnknownCountFile = 0;
                if (mCurrCmd == FRALGOCA_CMD_START_SAVE_FEATURE)
                {
                    ::snprintf(fileName, sizeof(fileName), 
                        "%s/mySaveF_%s_%.3d_%dx%d_%zu.raw", 
                        FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_2D_RAW, ++sSaveFCountFile, mFRAlgoIC.mBuf.width, mFRAlgoIC.mBuf.height,  mFRAlgoIC.mBuf.stride);
                } 
                else if (mCurrCmd == FRALGOCA_CMD_START_COMPARE_FEATURE)
                {
                    ::snprintf(fileName, sizeof(fileName), 
                        "%s/myCompF_%s_%.3d_%dx%d_%zu.raw", 
                        FRCATA_FACEID_FOLDER, FRCATA_FACEID_POSTFIX_2D_RAW, ++sUnknownCountFile, mFRAlgoIC.mBuf.width, mFRAlgoIC.mBuf.height,  mFRAlgoIC.mBuf.stride);
                }
                else
                {
                    ::snprintf(fileName, sizeof(fileName), 
                        "%smyUnknown%.3d_%dx%d_%zu.raw", 
                        "/sdcard/", ++sCompFCountFile, mFRAlgoIC.mBuf.width, mFRAlgoIC.mBuf.height,  mFRAlgoIC.mBuf.stride);
                }
                    
                int widthInBytes = (mFRAlgoIC.mBuf.width * NSCam::Utils::Format::queryPlaneBitsPerPixel(mFRAlgoIC.mBuf.format, 0)) >> 3;
                char *pBaseAddr = nonSecureSharedMem.get();

                int dumpFileFD = ::open(fileName, O_RDWR | O_CREAT | O_TRUNC, S_IRWXU);
                if (dumpFileFD > 0)
                {
                    for (int i = 0; i < mFRAlgoIC.mBuf.height; i++)
                    {
                    ::write(dumpFileFD, pBaseAddr, widthInBytes);
                    pBaseAddr += mFRAlgoIC.mBuf.stride;
                    }
                    ::close(dumpFileFD);
                }
                else
                {
                    ALOGE("%s: fail to open %s", __FUNCTION__, fileName);
                }
            }

            // MTEE service finished using nonSecureSharedMem, now unregister shared memory
            retTee = UREE_UnregisterSharedmem(memSrvSession, sharedMemoryHandle);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                ALOGE("%s: UREE_UnregisterSharedmem(depth) failed: ret=%d",__FUNCTION__, retTee);
            }

            // close session for shared memory
            retTee = UREE_CloseSession(memSrvSession);
            if (retTee != TZ_RESULT_SUCCESS)
            {
                ALOGE("%s: close mem sesion failed(%d)", __FUNCTION__, retTee);
            }

#endif // FRALGOCA_DUMP_NEEDED
        
        //#ifdef FRALGOCA_2D_DUMP_NEEDED
        //            mFRAlgoIC.dumpBuffer();
        //#endif // FRALGOCA_2D_DUMP_NEEDED
                    
        
        // TODO: release buffer to SecureCam
        
        // no matter the return value is right or wrong, go back to wait for initial state
        mFRAlgoIC.setCurrState(FRAlgo2DInputCollector::FRAIC_WAIT_FOR_YUV);
    }
    else if (currState == FRAlgo2DInputCollector::FRAIC_WAIT_FOR_YUV && stream.width==640) // TODO:
    {
        // TODO:
        // 1. processiong
        // 2. release buffer to SecureCam
    }
    else
    {
        ALOGW("%s: strange state: %d,  to-be-state: %d",
                __FUNCTION__, currState, FRAlgo2DInputCollector::FRAIC_WAIT_FOR_RAW);

        // TODO: release buffer to SecureCam
        mFRAlgoIC.reset();
        // reset state to FRAIC_WAIT_FOR_RAW and release the related resource
        ret = FR_ERR_FRALGO_WAIT_FOR_2D_RAW;
    }

    FUNC_END;

    return ret;
#else // non-MTK_CAM_SECURITY_SUPPORT

    return FR_ERR_CMD_NOT_IMPLEMETED;
#endif
}

Return<void> FRAlgoCA_2D::SecureCamClientCallback::onBufferAvailable(
                Status status, uint64_t bufferId,
                const hidl_handle& buffer, const Stream& stream)
{
// FR-custom
    ALOGD("%s +", __FUNCTION__);

    int ret = FR_ERR_OK;

    // === Error Handling ===
    if (status != Status::OK)
    {
        ALOGW("%s: !!!err!!!: status(%d), bufferId(%" PRIu64 ") buffer(%p) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
                __FUNCTION__, 
                status, 
                bufferId,
                buffer.getNativeHandle(),
                stream.size, 
                stream.width, 
                stream.height, 
                stream.stride, 
                stream.format);

        ALOGD("%s -: err: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_STATUS_ERROR", __FUNCTION__);
        return Void();
    }

    buffer_handle_t importedBuffer = buffer.getNativeHandle();
    if (buffer == nullptr || importedBuffer == NULL)
    {
        ALOGE("%s -: status: %d: FR_ERR_FRALGO_ONBUFAVAIL_SECCAM_BUF_ERROR", __FUNCTION__, status);
        return Void();
    }

    ALOGD("%s: status(%d), bufferId(%" PRIu64 ") buffer(%p, data[0]=%d, numFds=%d, numInts=%d) dataSize(%llu) size(%dx%d) stride(%d) format(0x%x)",
            __FUNCTION__,
            status, 
            bufferId,
            buffer.getNativeHandle(), importedBuffer->data[0], importedBuffer->numFds,  importedBuffer->numInts,
            stream.size,
            stream.width,
            stream.height,
            stream.stride,
            stream.format);

    // === Start Processing ===
    // Area where Mutex lock is needed
    const sp<FRAlgoCA> spFRAlgoCA_2D = mwpFRAlgoCA_2D.promote();
    if (spFRAlgoCA_2D != NULL)
    {
        FRCA_STATE currState = spFRAlgoCA_2D->getFRCAState();
        if (currState == FRCA_STATE_UNINIT || currState == FRCA_STATE_STOP_FEATURE)
        {
            ALOGD("%s: State: FRCA_STATE=%d, no need to process further", __FUNCTION__, currState);
            return Void();
        }

        Mutex::Autolock lock(spFRAlgoCA_2D->mLock);

        ret = spFRAlgoCA_2D->fralgo_prepareFRInput(bufferId, buffer, stream);
        if (ret != FR_ERR_OK)
        {
            ALOGE("%s: FRAlgoCA_AS::prepareFRInput err: %d", __FUNCTION__, ret);
        }

        // callback to FRHandler
        FRCA_CMD currCmd = spFRAlgoCA_2D->getCurrCmd();
        spFRAlgoCA_2D->callbackFRAlgoCAClient(currCmd, ret);
    }

    ALOGD("%s -", __FUNCTION__);

    return Void();
}

}  // namespace implementation
}  // namespace V1_0
}  // namespace frhandler
}  // namespace camera
}  // namespace hardware


