#ifndef _FR_TA_ALGO_ERR_H__
#define _FR_TA_ALGO_ERR_H__

// !!NOTES: the following error code should be included in the header of FRAlgo code in TEE
// and map all the FRAlgo error code to the following error definition

#define FR_ERR_ALGO_OK              0x0000

// === API return related ===
#define FR_ERR_ALGO_RETURN_BASE 90000
#define FR_ERR_ALGO_RETURN_CODE_OK             (FR_ERR_ALGO_RETURN_BASE + 0) 
#define FR_ERR_ALGO_RETURN_INVALID_ARGUMENT               (FR_ERR_ALGO_RETURN_BASE + 1) 
#define FR_ERR_ALGO_RETURN_NULLPTR_ERROR               (FR_ERR_ALGO_RETURN_BASE + 2) 
#define FR_ERR_ALGO_RETURN_MALLOC_FAILED               (FR_ERR_ALGO_RETURN_BASE + 3) 
#define FR_ERR_ALGO_RETURN_NO_IMAGE_DATA               (FR_ERR_ALGO_RETURN_BASE + 4) 
#define FR_ERR_ALGO_RETURN_NO_DETECTION               (FR_ERR_ALGO_RETURN_BASE + 5) 
#define FR_ERR_ALGO_RETURN_ALREADY_INIT               (FR_ERR_ALGO_RETURN_BASE + 6) 
#define FR_ERR_ALGO_RETURN_NO_INIT_YET               (FR_ERR_ALGO_RETURN_BASE + 7) 
#define FR_ERR_ALGO_RETURN_FAILURE               (FR_ERR_ALGO_RETURN_BASE + 8) 

#define FR_ERR_ALGO_WAIT_FOR_DEPTH_BUF               (FR_ERR_ALGO_RETURN_BASE + 9)
#define FR_ERR_ALGO_UNKNOWN_STATE               (FR_ERR_ALGO_RETURN_BASE + 10)
#define FR_ERR_ALGO_GENERIC_ERROR        (FR_ERR_ALGO_RETURN_BASE + 11)

// === DETECT related ===
#define FR_ERR_ALGO_DETECT_BASE              91000
#define FR_ERR_ALGO_DETECT_OK              (FR_ERR_ALGO_DETECT_BASE + 0)
#define FR_ERR_ALGO_DETECT_NOT_FOUND               (FR_ERR_ALGO_DETECT_BASE + 1)

#define FR_ERR_ALGO_DETECT_BAD_QUALITY              (FR_ERR_ALGO_DETECT_BASE + 2)
#define FR_ERR_ALGO_DETECT_TOO_SMALL              (FR_ERR_ALGO_DETECT_BASE + 3)
#define FR_ERR_ALGO_DETECT_TOO_LARGE              (FR_ERR_ALGO_DETECT_BASE + 4)

#define FR_ERR_ALGO_DETECT_LEFT              (FR_ERR_ALGO_DETECT_BASE + 5)
#define FR_ERR_ALGO_DETECT_TOP              (FR_ERR_ALGO_DETECT_BASE + 6)
#define FR_ERR_ALGO_DETECT_RIGHT              (FR_ERR_ALGO_DETECT_BASE + 7)
#define FR_ERR_ALGO_DETECT_BOTTOM              (FR_ERR_ALGO_DETECT_BASE + 8)

#define FR_ERR_ALGO_DETECT_BLUR              (FR_ERR_ALGO_DETECT_BASE + 9)
#define FR_ERR_ALGO_DETECT_INCOMPLETE              (FR_ERR_ALGO_DETECT_BASE + 10)

#define FR_ERR_ALGO_DETECT_EYE_OCCLUSION              (FR_ERR_ALGO_DETECT_BASE + 11)
#define FR_ERR_ALGO_DETECT_MOUTH_OCCLUSION              (FR_ERR_ALGO_DETECT_BASE + 12)
#define FR_ERR_ALGO_DETECT_FACE_ROTATED_LEFT              (FR_ERR_ALGO_DETECT_BASE + 13)
#define FR_ERR_ALGO_DETECT_FACE_RISE              (FR_ERR_ALGO_DETECT_BASE + 14)
#define FR_ERR_ALGO_DETECT_FACE_ROTATED_RIGHT              (FR_ERR_ALGO_DETECT_BASE + 15)
#define FR_ERR_ALGO_DETECT_FACE_DOWN              (FR_ERR_ALGO_DETECT_BASE + 16)
#define FR_ERR_ALGO_DETECT_FACE_MULTI              (FR_ERR_ALGO_DETECT_BASE + 17)

#define FR_ERR_ALGO_DETECT_DARKLIGHT              (FR_ERR_ALGO_DETECT_BASE + 18)
#define FR_ERR_ALGO_DETECT_HIGHLIGHT              (FR_ERR_ALGO_DETECT_BASE + 19)
#define FR_ERR_ALGO_DETECT_HALF_SHADOW              (FR_ERR_ALGO_DETECT_BASE + 20)

#define FR_ERR_ALGO_DETECT_EYE_CLOSED              (FR_ERR_ALGO_DETECT_BASE + 21)
#define FR_ERR_ALGO_DETECT_EYE_CLOSED_UNKNOW              (FR_ERR_ALGO_DETECT_BASE + 22)
#define FR_ERR_ALGO_DETECT_GOOD_FOR_ENROLL              (FR_ERR_ALGO_DETECT_BASE + 23)

#define FR_ERR_ALGO_DETECT_FAILURE              (FR_ERR_ALGO_DETECT_BASE + 24)


// === LIVE related ===
#define FR_ERR_ALGO_LIVE_BASE              92000

#define FR_ERR_ALGO_LIVE_SUCCESS              (FR_ERR_ALGO_LIVE_BASE + 0)
#define FR_ERR_ALGO_LIVE_FAILURE              (FR_ERR_ALGO_LIVE_BASE + 1)
#define FR_ERR_ALGO_LIVE_UNKNOWN              (FR_ERR_ALGO_LIVE_BASE + 2)
#define FR_ERR_ALGO_LIVE_NO_FACE              (FR_ERR_ALGO_LIVE_BASE + 3)

// === GET FEATURE related ===
#define FR_ERR_ALGO_GET_FEATURE_BASE              93000

#define FR_ERR_ALGO_GET_FEATURE_OK              (FR_ERR_ALGO_GET_FEATURE_BASE + 0)
#define FR_ERR_ALGO_GET_FEATURE_NO_FACE              (FR_ERR_ALGO_GET_FEATURE_BASE + 0)

// === COMPARE related ===
#define FR_ERR_ALGO_COMPARE_BASE              94000

#define FR_ERR_ALGO_COMPARE_SUCCESS              (FR_ERR_ALGO_COMPARE_BASE + 0)
#define FR_ERR_ALGO_COMPARE_FAILURE              (FR_ERR_ALGO_COMPARE_BASE + 1)

// === ATTENTION related ===
#define FR_ERR_ALGO_ATTENTION_BASE              95000

#define FR_ERR_ALGO_ATTENTION_OK              (FR_ERR_ALGO_ATTENTION_BASE + 0)
#define FR_ERR_ALGO_ATTENTION_NO_FOCUS              (FR_ERR_ALGO_ATTENTION_BASE + 1)
#define FR_ERR_ALGO_ATTENTION_NO_FACE              (FR_ERR_ALGO_ATTENTION_BASE + 2)

// === RESTORE related ===
#define FR_ERR_ALGO_RESTORE_BASE              96000
#define FR_ERR_ALGO_RESTORE_OK              (FR_ERR_ALGO_RESTORE_BASE + 0)
#define FR_ERR_ALGO_RESTORE_NO_FACE              (FR_ERR_ALGO_RESTORE_BASE + 1)

// === MTEE related ===
#define FR_ERR_MTEE_COMMON_BASE                              97000
#define FR_ERR_MTEE_REGISTER_SHAREDMEM_FAIL         (FR_ERR_MTEE_COMMON_BASE + 1)
#define FR_ERR_MTEE_UNREGISTER_SHAREDMEM_FAIL              (FR_ERR_MTEE_COMMON_BASE + 2)
#define FR_ERR_MTEE_QUERY_SHAREDMEM_FAIL              (FR_ERR_MTEE_COMMON_BASE + 3)

//!!NOTES: don't go over 100 FR_ERR_MTEE_XXX

// === MTEE: FaceID related ===
#define FR_ERR_MTEE_FACEID_BASE                              97100
#define FR_ERR_MTEE_FACEID_OPEN_SESSION_FAIL              (FR_ERR_MTEE_FACEID_BASE + 1)
#define FR_ERR_MTEE_FACEID_OPEN_FILE_FAIL              (FR_ERR_MTEE_FACEID_BASE + 2)
#define FR_ERR_MTEE_FACEID_WRITE_FAIL              (FR_ERR_MTEE_FACEID_BASE + 3)
#define FR_ERR_MTEE_FACEID_READ_FAIL              (FR_ERR_MTEE_FACEID_BASE + 4)


#endif // _FR_TA_ALGO_ERR_H__

