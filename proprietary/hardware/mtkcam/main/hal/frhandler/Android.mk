$(info frhandler_use_camera_hal_version=$(CAMERA_HAL_VERSION))
ifneq ($(CAMERA_HAL_VERSION), 3)

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

#-----------------------------------------------------------
-include $(TOP)/$(MTK_PATH_SOURCE)/hardware/mtkcam/mtkcam.mk

#-----------------------------------------------------------
LOCAL_SRC_FILES += FRHandler.cpp \
            FRClientCallback.cpp \
			FRAlgoCA.cpp

LOCAL_SRC_FILES += FRAlgoCA_AS.cpp
LOCAL_SRC_FILES += FRAlgoCA_dummy.cpp
LOCAL_SRC_FILES += FRAlgoCA_2D.cpp

#-----------------------------------------------------------
LOCAL_C_INCLUDES += $(MTKCAM_C_INCLUDES)
LOCAL_C_INCLUDES += $(MTK_PATH_SOURCE)/hardware/mtkcam/include

LOCAL_C_INCLUDES += $(TOP)/system/core/include
LOCAL_C_INCLUDES += $(TOP)/

# mkdbg: start: M-TEE related
LOCAL_C_INCLUDES += vendor/mediatek/proprietary/geniezone/external/uree/libgz_uree \
                    vendor/mediatek/proprietary/geniezone/external/uree/include \
					vendor/mediatek/proprietary/geniezone/trusty/app/faceR
# mkdbg: end

# mkdbg: start: ION related
# MTK extension of ION memory allocator
ifeq ($(MTK_ION_SUPPORT), yes)
LOCAL_CFLAGS += -DUSING_MTK_ION
LOCAL_C_INCLUDES += \
	system/core/libion/include \
	$(MTK_PATH_SOURCE)/external/libion_mtk/include
endif # MTK_ION_SUPPORT


# mkdbg: end

#-----------------------------------------------------------
LOCAL_CFLAGS += $(MTKCAM_CFLAGS)
#-----------------------------------------------------------
LOCAL_WHOLE_STATIC_LIBRARIES :=

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)
# mkdbg: MTEE related
LOCAL_STATIC_LIBRARIES += libgz_uree
endif

#-----------------------------------------------------------
LOCAL_SHARED_LIBRARIES += liblog
LOCAL_SHARED_LIBRARIES += libutils
LOCAL_SHARED_LIBRARIES += libcutils
LOCAL_SHARED_LIBRARIES += libmtkcam_stdutils

#
LOCAL_SHARED_LIBRARIES += libhidlbase
LOCAL_SHARED_LIBRARIES += libhidltransport
#ori:
#LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.camera.frhandler@1.0_vendor
LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.camera.frhandler@1.0
LOCAL_SHARED_LIBRARIES += android.hardware.camera.common@1.0
LOCAL_SHARED_LIBRARIES += android.hardware.camera.device@3.2
#LOCAL_SHARED_LIBRARIES += android.hidl.base@1.0

LOCAL_SHARED_LIBRARIES += android.hidl.memory@1.0
LOCAL_SHARED_LIBRARIES += libhidlmemory

ifeq ($(MTK_ION_SUPPORT), yes)
	LOCAL_SHARED_LIBRARIES += libion
	LOCAL_SHARED_LIBRARIES += libion_mtk
	LOCAL_SHARED_LIBRARIES += android.hardware.graphics.mapper@2.0

	LOCAL_STATIC_LIBRARIES += android.hardware.camera.common@1.0-helper
	LOCAL_STATIC_LIBRARIES += libmtkcam_ionhelper
endif # MTK_ION_SUPPORT

# ori:
# LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.camera.security@1.0_vendor
LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.camera.security@1.0

#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ifeq ($(strip $(MTK_CAM_SECURITY_SUPPORT)), yes)
LOCAL_CFLAGS += -DMTK_CAM_SECURITY_SUPPORT
endif

LOCAL_MODULE := vendor.mediatek.hardware.camera.frhandler@1.0-impl
LOCAL_PROPRIETARY_MODULE := true
LOCAL_MODULE_OWNER := mtk
LOCAL_MODULE_RELATIVE_PATH := hw

LOCAL_REQUIRED_MODULES :=

#-----------------------------------------------------------
include $(MTK_SHARED_LIBRARY)

include $(call all-makefiles-under, $(LOCAL_PATH))

endif # CAMERA_HAL_VERSION

