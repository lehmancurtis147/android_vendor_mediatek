/* Copyright Statement:
 *
 * This software/firmware and related documentation ("MediaTek Software") are
 * protected under relevant copyright laws. The information contained herein is
 * confidential and proprietary to MediaTek Inc. and/or its licensors. Without
 * the prior written permission of MediaTek inc. and/or its licensors, any
 * reproduction, modification, use or disclosure of MediaTek Software, and
 * information contained herein, in whole or in part, shall be strictly
 * prohibited.
 *
 * MediaTek Inc. (C) 2010. All rights reserved.
 *
 * BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
 * THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
 * RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER
 * ON AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL
 * WARRANTIES, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR
 * NONINFRINGEMENT. NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH
 * RESPECT TO THE SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY,
 * INCORPORATED IN, OR SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES
 * TO LOOK ONLY TO SUCH THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO.
 * RECEIVER EXPRESSLY ACKNOWLEDGES THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO
 * OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES CONTAINED IN MEDIATEK
 * SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE
 * RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
 * STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S
 * ENTIRE AND CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE
 * RELEASED HEREUNDER WILL BE, AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE
 * MEDIATEK SOFTWARE AT ISSUE, OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE
 * CHARGE PAID BY RECEIVER TO MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
 *
 * The following software/firmware and/or related documentation ("MediaTek
 * Software") have been modified by MediaTek Inc. All revisions are subject to
 * any receiver's applicable license agreements with MediaTek Inc.
 */

#ifndef _CAM_IDX_MGR_H_
#define _CAM_IDX_MGR_H_

#include <stdarg.h>
#include <vector>
#include <mtkcam/def/common.h>
#include "tuning_mapping/cam_idx_struct_ext.h"
#include "tuning_mapping/cam_idx_struct_int.h"
#include "isp_tuning/ver1/isp_tuning_cam_info.h"
/*
extra include path needed to use isp_tuning_cam_info.h
LOCAL_C_INCLUDES += $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/$(MTK_CAM_SW_VERSION)
LOCAL_C_INCLUDES += $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/aaa
LOCAL_C_INCLUDES += $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/aaa/$(MTK_CAM_SW_VERSION)
LOCAL_C_INCLUDES += $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/isp_tuning
LOCAL_C_INCLUDES += $(MTK_PATH_CUSTOM_PLATFORM)/hal/inc/isp_tuning/$(MTK_CAM_SW_VERSION)
*/
#include "isp_tuning_sensor.h"
#include "camera_custom_nvram.h"

#include <utils/Mutex.h>
#include <list>
#include <isp_tuning_custom.h>

#define MAPPING_INFO_LIST_SIZE (60)
#define IDXMGRIMP_SIZE  (4)

#define INVALID_SCENARIO    0xFFFF

using namespace android;
using namespace std;

typedef struct {
    MUINT16 idx;
    MUINT16 scenario;
    MUINT16 group;
} IDX_QUERY_RESULT;

typedef struct {
    MUINT16 idx;
    MUINT16 scenario;
    char* pScenario;
    MUINT16 group;
} IDXMGR_QUERY_RESULT;

typedef struct {
    CAM_IDX_QRY_COMB qry;
    MUINT32 u4FrmId;
} ISP_MAPPING_INFO_T;

class IdxBase
{
public:
    virtual ~IdxBase() {}
    virtual MVOID init(EModule_T, IDX_BASE_T*, MUINT16*)         = 0;
    virtual IDX_QUERY_RESULT query(const CAM_IDX_QRY_COMB& qry)  = 0;

protected:
    void mismatchHandling(EModule_T mod,  const CAM_IDX_QRY_COMB& qry);
};

class IdxMask : public IdxBase
{
public:
    IdxMask();
    virtual ~IdxMask();
    virtual MVOID init(EModule_T, IDX_BASE_T*, MUINT16*);
    virtual IDX_QUERY_RESULT query(const CAM_IDX_QRY_COMB& qry);

private:
    MVOID genKey(std::vector<MUINT16>& factor);
    MINT32 compareKey(MUINT32* key_in, MUINT32* key_golden);

    EModule_T m_mod;
    MUINT16 dim_ns;
    MUINT16* dims;
    //MUINT16* expand;
    MUINT32 entry_ns;
    MUINT32 key_sz;
    IDX_MASK_ENTRY* entry;
    MUINT16* acc_factor_ns;
    MUINT32* key;
};

class IdxDM : public IdxBase
{
public:
    IdxDM();
    virtual ~IdxDM();
    virtual MVOID init(EModule_T, IDX_BASE_T*, MUINT16*);
    virtual IDX_QUERY_RESULT query(const CAM_IDX_QRY_COMB& qry);

private:

    EModule_T m_mod;
    MUINT16 dim_ns;
    MUINT16* dims;
    //MUINT16* expand;
    MUINT16* acc_factor_ns;
    MUINT16* idx_array;
    MUINT16* scenarios;
};

typedef MUINT32 (IspTuningCustom::*mappingfn)(MUINT32, MVOID*);
#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

class IdxMgrImplBase
{
public:
    static IdxMgrImplBase* getInstance(NSIspTuning::ESensorDev_T eSensorDev);

    IdxMgrImplBase();
    virtual ~IdxMgrImplBase();

    virtual MVOID init(MVOID* const pNvram_Idx) = 0;
    virtual IDXMGR_QUERY_RESULT query(EModule_T mod, const CAM_IDX_QRY_COMB& qry) = 0;
    virtual MBOOL updateMapping_List(MUINT32 FrmId, const CAM_IDX_QRY_COMB& qry) = 0;
    virtual MBOOL updateMapping_List_By_Dim(MUINT32 FrmId, MUINT32 dim, MVOID* input) = 0;
    virtual MUINT32 addMappingFunction(MUINT32 dim, MUINT32 (IspTuningCustom::*function)(MUINT32, MVOID*), IspTuningCustom& tuning) = 0;
    virtual MBOOL getMapping_List(MUINT32 FrmId, CAM_IDX_QRY_COMB& qry) = 0;
    virtual MBOOL getMapping_List_Last(CAM_IDX_QRY_COMB& qry) = 0;

private:
    virtual MVOID clearMapping_List() = 0;
};

template <NSIspTuning::ESensorDev_T sensorDev>
class IdxMgrImpl: public IdxMgrImplBase
{
public:
    static IdxMgrImplBase* getInstance(MVOID* const pNvram_Idx);

    IdxMgrImpl();
    ~IdxMgrImpl();

    virtual MVOID init(MVOID* const pNvram_Idx);
    virtual IDXMGR_QUERY_RESULT query(EModule_T mod, const CAM_IDX_QRY_COMB& qry);
    virtual MBOOL updateMapping_List(MUINT32 FrmId, const CAM_IDX_QRY_COMB& qry);
    virtual MBOOL updateMapping_List_By_Dim(MUINT32 FrmId, MUINT32 dim, MVOID* input);
    virtual MUINT32 addMappingFunction(MUINT32 dim, MUINT32 (IspTuningCustom::*function)(MUINT32, MVOID*), IspTuningCustom& tuning);
    virtual MBOOL getMapping_List(MUINT32 FrmId, CAM_IDX_QRY_COMB& qry);
    virtual MBOOL getMapping_List_Last(CAM_IDX_QRY_COMB& qry);

private:
    IdxBase* classFactory(EModule_T, IDX_BASE_T*, MUINT16*);
    virtual MVOID clearMapping_List();

    list<ISP_MAPPING_INFO_T> MappingInfoQ;
    Mutex m_lock;
    IdxBase* m_pMobObj[EModule_NUM];
    MUINT16* m_pFactorNs;
    MUINT16 m_scenarioNs;
    char (*m_pScenarios)[][64];
    mappingfn m_funcs[EDim_NUM];
    IspTuningCustom* m_pIspTuningCustom;
};

template <NSIspTuning::ESensorDev_T sensorDev>
IdxMgrImplBase* IdxMgrImpl<sensorDev>::
getInstance(MVOID* const pNvram_Idx)
{
    static IdxMgrImpl<sensorDev> singleton;

    static struct link
    {
        link(IdxMgrImpl<sensorDev>& r, MVOID* const pIdx)
        {
            r.init(pIdx);
        }
    } link_singleton(singleton, pNvram_Idx);

    return  &singleton;
}

template <NSIspTuning::ESensorDev_T sensorDev>
IdxMgrImpl<sensorDev>::IdxMgrImpl()
: m_pFactorNs(NULL), m_scenarioNs(0), m_pScenarios(NULL)
{
    memset(m_pMobObj, 0, sizeof(m_pMobObj));
    for ( int dim = 0; dim < EDim_NUM; dim++ )
        m_funcs[dim] = NULL;
}

template <NSIspTuning::ESensorDev_T sensorDev>
IdxMgrImpl<sensorDev>::~IdxMgrImpl()
{
    Mutex::Autolock lock(m_lock);

    for (MINT32 i = 0; i < EModule_NUM; i++)
    {
        if (m_pMobObj[i])
        {
            delete m_pMobObj[i];
        }
    }
    memset(m_pMobObj, 0, sizeof(m_pMobObj));

    clearMapping_List();
}

template <NSIspTuning::ESensorDev_T sensorDev>
MVOID IdxMgrImpl<sensorDev>::init(MVOID* const pNvram_Idx)
{
    Mutex::Autolock lock(m_lock);

    if (pNvram_Idx == NULL)
    {
        assert((pNvram_Idx != NULL) && "[IdxMgrImpl::init] invalid input parameter");
        return;
    }

    IDX_MODULE_ARRAY*  pModArray = (IDX_MODULE_ARRAY*)pNvram_Idx;

    for (MINT32 i = 0; i < EModule_NUM; i++)
    {
        IdxBase*pBase = classFactory((EModule_T)i, pModArray->modules[i], pModArray->idx_factor_ns);
        assert((pBase != NULL) && "[IdxMgrImpl::init] classFactory output NULL pointer");
        m_pMobObj[i] = pBase;
    }
    m_pFactorNs = pModArray->idx_factor_ns;
    m_scenarioNs = pModArray->scenario_ns;
    m_pScenarios = pModArray->scenarios;
    m_pIspTuningCustom = NULL;
}

template <NSIspTuning::ESensorDev_T sensorDev>
IDXMGR_QUERY_RESULT IdxMgrImpl<sensorDev>::query(EModule_T mod, const CAM_IDX_QRY_COMB& qry)
{
    Mutex::Autolock lock(m_lock);

    IDX_QUERY_RESULT rtnVal = {0, INVALID_SCENARIO, 0};
    IDXMGR_QUERY_RESULT rtnVal2 = {0, 0, NULL, 0};

    for (MUINT32 i = 0; i < EDim_NUM; i++)
    {
        if (qry.query[i] >= m_pFactorNs[i])
        {
            assert(0 && "[IdxMgrImpl::query] query fail, dimension out of range");
            return rtnVal2;
        }
    }

    IdxBase* pMod = m_pMobObj[mod];

    if (pMod != NULL)
    {
        rtnVal = pMod->query(qry);
    }

    if (rtnVal.scenario >= m_scenarioNs)
    {
        rtnVal.scenario = m_scenarioNs - 1;
    }

    rtnVal2.idx = rtnVal.idx;
    rtnVal2.scenario = rtnVal.scenario;
    rtnVal2.pScenario = (*m_pScenarios)[rtnVal.scenario];
    rtnVal2.group = rtnVal.group;

    return rtnVal2;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MBOOL IdxMgrImpl<sensorDev>::updateMapping_List(MUINT32 FrmId, const CAM_IDX_QRY_COMB& qry)
{
    Mutex::Autolock lock(m_lock);

    MINT32 i4Ret = MFALSE;
    MINT32 i4Pos = 0;
    MINT32 i4Size = MappingInfoQ.size();

    ISP_MAPPING_INFO_T temp;
    temp.u4FrmId = FrmId;
    temp.qry = qry;

    list<ISP_MAPPING_INFO_T>::iterator it = MappingInfoQ.begin();

    for (it = MappingInfoQ.begin(); it != MappingInfoQ.end(); it++, i4Pos++)
    {
        if (it->u4FrmId == FrmId)
        {
            //MY_LOGD("overwirte LCSList_Out FrmID: %d", FrmId);
            it->qry = qry;
            i4Ret = MTRUE;
            break;
        }
    }

    if (i4Pos == i4Size)
    {
        MappingInfoQ.push_back(temp);
        i4Ret = MTRUE;
    }

    // remove item
    if (MappingInfoQ.size() > MAPPING_INFO_LIST_SIZE)
    {
        MappingInfoQ.erase(MappingInfoQ.begin());
    }
    return i4Ret;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MUINT32 IdxMgrImpl<sensorDev>::addMappingFunction(MUINT32 dim, MUINT32 (IspTuningCustom::*function)(MUINT32, MVOID*), IspTuningCustom& tuning){
    m_funcs[dim] = function;
    m_pIspTuningCustom = &tuning;
    return 0;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MBOOL IdxMgrImpl<sensorDev>::updateMapping_List_By_Dim(MUINT32 FrmId, MUINT32 dim, MVOID* input)
{
    Mutex::Autolock lock(m_lock);

    MINT32 i4Ret = MFALSE;
    MINT32 i4Pos = 0;
    MINT32 i4Size = MappingInfoQ.size();
    ISP_MAPPING_INFO_T temp;
    MUINT32 value = 0;

    if (m_funcs[dim] != NULL && m_pIspTuningCustom != NULL)
        value = CALL_MEMBER_FN(*m_pIspTuningCustom ,m_funcs[dim])(dim ,input);
    else
        value = *(MUINT32*)input;

    list<ISP_MAPPING_INFO_T>::iterator it = MappingInfoQ.begin();

    for (it = MappingInfoQ.begin(); it != MappingInfoQ.end(); it++, i4Pos++)
    {
        if (it->u4FrmId == FrmId)
        {
#if BUILD_VERSION > 1
            if (dim == EDim_ISO){
                for(MINT32 group_index = 0; group_index < NVRAM_ISP_REGS_ISO_GROUP_NUM; group_index++)
                memcpy(&(it->qry.eISO_Idx), input, sizeof(EISO_T)*NVRAM_ISP_REGS_ISO_GROUP_NUM);
            } else {
                it->qry.query[dim] = value;
            }
#else
            it->qry.query[dim] = value;
#endif
            i4Ret = MTRUE;
            break;
        }
    }

    if (i4Pos == i4Size)
    {
        if (i4Size == 0)
        {
            temp.qry.query[dim] = value;
            temp.u4FrmId = FrmId;
            MappingInfoQ.push_back(temp);
            i4Ret = MTRUE;
        }
        else
        {//copy last item in list to temp and push it into end of the list
            it = MappingInfoQ.end();
            it--;
            temp.qry = it->qry;
            temp.qry.query[dim] = value;
            temp.u4FrmId = FrmId;
            MappingInfoQ.push_back(temp);
            i4Ret = MTRUE;
        }
        
    }

    // remove item
    if (MappingInfoQ.size() > MAPPING_INFO_LIST_SIZE)
    {
        MappingInfoQ.erase(MappingInfoQ.begin());
    }

    return i4Ret;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MBOOL IdxMgrImpl<sensorDev>::getMapping_List(MUINT32 FrmId, CAM_IDX_QRY_COMB& qry)
{
    Mutex::Autolock lock(m_lock);

    MBOOL i4Ret = MFALSE;
    MINT32 i4Pos = 0;
    MINT32 i4Size = MappingInfoQ.size();
    list<ISP_MAPPING_INFO_T>::iterator it = MappingInfoQ.begin();
    for (; it != MappingInfoQ.end(); it++, i4Pos++)
    {
        if (it->u4FrmId == (MUINT32)FrmId)
        {
            qry = it->qry;
            i4Ret = MTRUE;
            //MY_LOGD("[%s] OK i4Pos(%d)", __FUNCTION__, i4Pos);
            break;
        }
    }

    if (i4Pos == i4Size)
    {
        // does not exist
        //MY_LOGD("[%s] NG i4Pos(%d)", __FUNCTION__, i4Pos);
        i4Ret = MFALSE;
    }

    return i4Ret;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MBOOL IdxMgrImpl<sensorDev>::getMapping_List_Last(CAM_IDX_QRY_COMB& qry)
{
    Mutex::Autolock lock(m_lock);

    if (!MappingInfoQ.empty())
    {
        list<ISP_MAPPING_INFO_T>::iterator it = MappingInfoQ.end();
        it--;
        qry = it->qry;
        return MTRUE;
    }

    return MFALSE;
}

template <NSIspTuning::ESensorDev_T sensorDev>
MVOID IdxMgrImpl<sensorDev>::clearMapping_List()
{
    MappingInfoQ.clear();
}

template <NSIspTuning::ESensorDev_T sensorDev>
IdxBase* IdxMgrImpl<sensorDev>::classFactory(EModule_T mod, IDX_BASE_T* pBase, MUINT16* pFactorNs)
{
    if ((!pBase) || (!pFactorNs))
    {
        return NULL;
    }

    IdxBase* pOut = NULL;
    MUINT8 algo_type = pBase->type;

    switch (algo_type)
    {
    case IDX_ALGO_MASK:
        pOut = new IdxMask();
        if (pOut)
        {
            pOut->init(mod, pBase, pFactorNs);
        }
        break;

    case IDX_ALGO_DM:
        pOut = new IdxDM();
        if (pOut)
        {
            pOut->init(mod, pBase, pFactorNs);
        }
        break;

    case IDX_ALGO_TREE:
        pOut = NULL;
        break;

    case IDX_ALGO_HASH:
        pOut = NULL;
        break;

    default:
        pOut = NULL;
        break;
    }

    return pOut;
}

class IdxMgr
{
public:
    static IdxMgr* createInstance(NSIspTuning::ESensorDev_T eSensorDev);

    IdxMgr();
    virtual ~IdxMgr();

    virtual MUINT16 query(NSIspTuning::ESensorDev_T eSensorDev, EModule_T mod, const CAM_IDX_QRY_COMB& qry, MUINT16& u2ScenarioIdx);
    virtual MUINT16 query(NSIspTuning::ESensorDev_T eSensorDev, EModule_T mod, const CAM_IDX_QRY_COMB& qry, const char* caller);
    virtual MUINT16 query(NSIspTuning::ESensorDev_T eSensorDev, EModule_T mod, MUINT32 FrmId);
    MVOID setMappingInfo(NSIspTuning::ESensorDev_T eSensorDev, const CAM_IDX_QRY_COMB& output , MUINT32 FrmId);
    MVOID setMappingInfoByDim(NSIspTuning::ESensorDev_T eSensorDev, MUINT32 FrmId, MUINT32 dim, MVOID* value);
    MVOID getMappingInfo(NSIspTuning::ESensorDev_T eSensorDev, CAM_IDX_QRY_COMB& output , MUINT32 FrmId);
    MUINT32 addMappingFunction(NSIspTuning::ESensorDev_T eSensorDev, MUINT32 dim, MUINT32 (IspTuningCustom::*function)(MUINT32, MVOID*), IspTuningCustom& tuning);

private:
    IdxMgrImplBase* m_pIdxMgrImpBase[IDXMGRIMP_SIZE];
};

#endif
