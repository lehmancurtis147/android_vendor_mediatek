#ifndef __CHRIST_ASNYC_TASK_H__
#define __CHRIST_ASNYC_TASK_H__
#include "blitRow32Case.h"

class MtkAsyncTask {
public:
    MtkAsyncTask();
    void doAsyncTask(SkImageInfo &decodeInfo, SkBitmap &decodingBitmap, SkBitmap &dstbm);
    void onDoAsnycTask(SkImageInfo &decodeInfo, SkBitmap &decodingBitmap, SkBitmap &dstbm);
    
};
#endif
