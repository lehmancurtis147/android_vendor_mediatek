#ifndef __SMARTPA_PARAM_H__
#define __SMARTPA_PARAM_H__

#include <stdint.h>

#define SPK_ORD	(2)

struct iir_1st_filt_param {
	int64_t b0;
	int64_t b1;
	int64_t a1;
}__attribute__((packed));

struct iir_2nd_filt_param {
	int32_t b0;
	int32_t b1;
	int32_t b2;
	int32_t a1;
	int32_t a2;
}__attribute__((packed));

struct param_eq
{
	int32_t Nbands;
	int32_t filt_b[10][3];
	int32_t filt_a[10][2];
}__attribute__((packed));

struct param_mbdrc {
	int32_t Nbands;
	int32_t delay;
	int32_t predict_offset;
	int32_t filt_b[5][6];
	int32_t filt_a[5][4];
	int32_t mode_rms[5];
	int32_t band_skip[5];
	int32_t alpha[5];
	int32_t omega[5];
	int32_t At[5];
	int32_t Rt[5];
	int32_t thres0[5];
	int32_t thres1[5];
	int32_t thres2[5];
	int32_t ratio0[5];
	int32_t ratio1[5];
	int32_t makeup[5];
}__attribute__((packed));

struct spk_pro_parameter {
	int32_t dump_l;
	int32_t dump_r;
	int32_t dcb_filt_en;

	int32_t farrow_interp_en;
	int32_t delay_est_en;

	struct iir_1st_filt_param dcb_filt;

	struct iir_2nd_filt_param pil_filt_param;
	struct iir_2nd_filt_param sig_filt_param;

	int32_t delay_est_mu;
	int32_t delay_est_diff_smpls;
	int32_t delay_est_max_delay;
	int32_t delay_est_min_delay;
	int32_t delay_est_bpf_cos_phi2;
	int32_t delay_est_bpf_sin_phi2;


	int32_t tone_det_cos_phi2;
	int32_t tone_det_sin_phi2;
	int32_t tone_det_mu;
	int32_t tone_det_log2_dev ;
	int32_t tone_det_sgl_tone_ratio;
	int32_t tone_det_act_thres;
	int32_t tone_det_recovery_smpls;
	int32_t rx_act_recovery_smpls;

	int32_t dcr_est_mu;
	int32_t dcr_est_cvg_thres0;
	int32_t dcr_est_cvg_thres1;

	int32_t atc_kp;
	int32_t atc_ki;
	int32_t atc_kd;
	int32_t max_dcr;
	int32_t max_dcr_dmg;
	int32_t min_atc_gain;

	int32_t imp_est_ord;
	int32_t imp_est_win;
	int32_t imp_est_mu0_exp;
	int32_t imp_est_mu1_exp;

	int32_t fres_damage_ratio;
	int32_t fres_recovery_smpls;
	int32_t fres_stepsize;

	int32_t exc_release_rate;
	int32_t exc_hold_th;
	int32_t exc_hold_smpls;
	int32_t sig_release_rate;
	int32_t sig_hold_th;

	int32_t max_sig;
	int32_t max_exc;
	int32_t max_sig_dmg;
	int32_t max_exc_dmg;
	int32_t mode_sel_dmg;
	int32_t volume;
	int32_t ramp_speed;

	int32_t tx_act_thres;
	int32_t pil_gain;
	int32_t pil_norm_freq;
	int32_t pil_zero_thres;

	int32_t bypass_num;
	int32_t bypass_cnt;

	struct param_eq eq;
	struct param_mbdrc mbdrc;
	struct param_mbdrc sbdrc;

	int32_t mul_one;
	int32_t calib_y[2*SPK_ORD+1];
	int32_t calib_dcr;

	int32_t spk_pro_enable;
	int32_t eq_drc_switch;

	int32_t est_delay;
	int32_t fres_set_fres;
	int32_t t0;
	int32_t pro_gain_switch;
	int32_t alpha_speaker;
	int32_t max_temperature;
	int32_t eq_drc_ord;
 
	int32_t imp_est_single_tone_mu1_exp;
	int32_t monitor_on;
	struct iir_2nd_filt_param vi_sensing_filt_param;
	int32_t vi_sensing_lpf_en;
	int32_t deci_ratio;
	struct iir_2nd_filt_param deci_filt_param;
	struct iir_1st_filt_param exc_lpf_param;
	int32_t input_gain;
	int32_t version;
	int32_t single_tone_update;
	int32_t sample_rate;
	int32_t tuning_enable;
	int32_t monitor_data;
	int32_t chip_rev;
	struct iir_2nd_filt_param sig_deci_filt;
	struct iir_2nd_filt_param sig_deci_filt2;
	int32_t dump_enable;
	int32_t comp_filt_coef[9];
	int32_t fir_enable;
	int32_t bypass_lib;
	int32_t vo_thr_error;
	int32_t flags;
}__attribute__((packed));

struct sec_header {
	char tag[8];
	uint16_t BHV;
	uint16_t MSHV;
	uint32_t TBS;
	char ICTagString[16];
	char BinDesc[96];
	char date[16];
	uint32_t EHO;
	uint32_t EHS;
	uint32_t EDS;
	uint32_t CRC;
	char AuthorInfo[80];
	uint8_t upub_key[144];
	uint8_t uinfo_sig[128];
	uint8_t hinfo_sig[128];
} __attribute__((packed));

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */
void dynamic_link_arsi_assign_lib_fp(AurisysLibInterface *lib);
#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __SMARTPA_PARAM_H__ */
