#include <cutils/properties.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/fs.h>

#define LOG_TAG "libmrdump.so"
#include <log/log.h>

#include <memory.h>
#include <mrdump_user.h>
#include <mrdump_user_private.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/system_properties.h>
#include <sys/types.h>
#include <unistd.h>

#define MRDUMP_KVER "/sys/module/mrdump/version"
#define MRDUMP_LVER "/sys/module/mrdump/parameters/lk"
#define MRDUMP_EXPDB_NAME "/expdb"
#define MRDUMP_OFFSET 3145728
#define MRDUMP_SIG "MRDUMP08"

struct __attribute__((__packed__)) mrdump_cblock_result {
    char sig[9];
    char status[128];
    char log_buf[2048];
};

static const char *fstab_path_prefix[] = {
    "/vendor/etc/fstab",
    "/fstab",
    NULL
};

static int file_is_exist(const char *path)
{
    struct stat s;
    if (!path)
        return 0;
    if (stat(path, &s) != 0)
        return 0;
    return 1;
}

static int file_read_string(const char* path, char *content, int len)
{
    int fd = open(path, O_RDONLY);
    if (fd < 0) {
        return -1;
    }
    if (len <= 0) {
        return -2;
    }

    /* Preserved NULL byte */
    len--;
    int size = 0;
    do {
        int ret = read(fd, content + size, len - size);
        if (ret <= 0)
            break;
        size = size + ret;
    } while (size < len);
    content[size] = 0;

    close(fd);
    return size;
}

static char *get_device_node_from_fstab(const char *fstab, const char *mountp)
{
    int ret;
    FILE *fp;
    char c, myline[256];
    char *delim="\x09\x20";
    char *DeviceNode, *MountPoint;

    DeviceNode = NULL;
    fp = fopen(fstab, "r");
    if(fp == NULL)
        return NULL;

    while(!feof(fp)) {

        /* getline, not include newline(delimeter) itself */
        ret = fscanf(fp, "%[^\n]", myline);

        /* strtok strings */
        if(ret > 0) {
            if(myline[0] == '/') {
                DeviceNode = strtok(myline, delim);
                MountPoint = strtok(NULL, delim);
                if(MountPoint != NULL) {
                    if(!strcmp(MountPoint, mountp)) {
                        fclose(fp);
                        return strdup(DeviceNode);
                    }
                }
            }
        }

        /* clear newline character */
        ret = fscanf(fp, "%c", &c);
        if (ret != 1) {
            ALOGE("%s: not EOL.", __func__);
            fclose(fp);
            return NULL;
        }
    }
    fclose(fp);
    return NULL;

}

char *get_partition_path(const char *mountp)
{
    int i;
    char *DeviceNode = NULL;
    char fstab_filename[PROPERTY_VALUE_MAX];

    /* get hardware parts */
    char propbuf[PROPERTY_VALUE_MAX];
    if(property_get("ro.hardware", propbuf, NULL) == 0)
        property_get("ro.board.platform", propbuf, "");

    /* search for prefix */
    for(i = 0; fstab_path_prefix[i] != NULL; i++) {
        snprintf(fstab_filename, sizeof(fstab_filename), "%s.%s", fstab_path_prefix[i], propbuf);
        if(file_is_exist(fstab_filename))
            DeviceNode =  get_device_node_from_fstab(fstab_filename, mountp);
        if(DeviceNode)
            return DeviceNode;
    }

    /* search for path: /fstab */
    DeviceNode =  get_device_node_from_fstab("/fstab", mountp);
    return DeviceNode;
}

int expdb_open(struct partinfo *partinfo)
{
    uint64_t part_size;
    uint32_t part_blksize;

    memset(partinfo, 0, sizeof(struct partinfo));

    char *pp = get_partition_path(MRDUMP_EXPDB_NAME);
    if (pp == NULL) {
        ALOGE("%s: No expdb partition found", __func__);
        return -1;
    }

    int fd = open(pp, O_RDWR);
    if (fd < 0) {
        ALOGE("%s: open expdb failed(%d)", __func__, errno);
        free(pp);
        return -1;
    }
    free(pp);

    if (ioctl(fd, BLKGETSIZE64, &part_size) < 0) {
        ALOGE("%s, get expdb partition size fail(%d)", __func__, errno);
        close(fd);
        return -1;
    }

    if (ioctl(fd, BLKSSZGET, &part_blksize) < 0) {
        ALOGE("%s, get sector size fail(%d)", __func__, errno);
        close(fd);
        return -1;
    }

    partinfo->fd = fd;
    partinfo->size = part_size;
    partinfo->blksize = part_blksize;

    return 0;
}

int mrdump_is_supported(void)
{
    char kversion[16], lversion[16];
    int klen, llen;

    bzero(kversion, sizeof(kversion));
    bzero(lversion, sizeof(lversion));

    if (file_read_string(MRDUMP_KVER, kversion, sizeof(kversion)) < 0) {
        ALOGE("%s: cannot get kernel version\n", __func__);
        return 0;
    }

    klen = strlen(kversion);
    if (klen == 0) {
        ALOGE("%s: null kernel version\n", __func__);
        return 0;
    }

    if (file_read_string(MRDUMP_LVER, lversion, sizeof(lversion)) < 0) {
        ALOGE("%s: cannot get lk version\n", __func__);
        return 0;
    }

    llen = strlen(lversion);
    if (llen == 0) {
        ALOGE("%s: null lk version\n", __func__);
        return 0;
    }

    if ((klen != llen) || (strncmp(kversion, lversion, klen) != 0)) {
        ALOGE("%s: kernel and lk version mismatched.\n", __func__);
        return 0;
    }

    ALOGI("%s: true\n", __func__);
    return 1;
}

bool mrdump_status_clear(void)
{
    struct partinfo partinfo;

    if (expdb_open(&partinfo) >= 0) {
	if (lseek64(partinfo.fd, partinfo.size - MRDUMP_OFFSET, SEEK_SET) < 0) {
	    ALOGE("%s: Can't seek part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}

	struct mrdump_cblock_result cblock_result;
	if (read(partinfo.fd, &cblock_result, sizeof(struct mrdump_cblock_result)) != sizeof(struct mrdump_cblock_result)) {
	    ALOGE("%s: Can't read part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}
	memset(cblock_result.status, 0, sizeof(cblock_result.status));
	strncpy(cblock_result.status, "CLEAR", 5);

	if (lseek64(partinfo.fd, partinfo.size - MRDUMP_OFFSET, SEEK_SET) < 0) {
	    ALOGE("%s: Can't seek part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}
	if (write(partinfo.fd, &cblock_result, sizeof(struct mrdump_cblock_result)) != sizeof(struct mrdump_cblock_result)) {
	    ALOGE("%s: Can't write part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}
	close(partinfo.fd);
	return true;
    }
    return false;
}

bool mrdump_status_get(struct mrdump_status_result *result)
{
    memset(result, 0, sizeof(struct mrdump_status_result));
    result->struct_size = sizeof(struct mrdump_status_result);

    struct partinfo partinfo;
    if (expdb_open(&partinfo) >= 0) {
	if (lseek64(partinfo.fd, partinfo.size - MRDUMP_OFFSET, SEEK_SET) < 0) {
	    ALOGE("%s: Can't seek part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}

	struct mrdump_cblock_result cblock_result;
	if (read(partinfo.fd, &cblock_result, sizeof(struct mrdump_cblock_result)) != sizeof(struct mrdump_cblock_result)) {
	    ALOGE("%s: Can't read part fd %d\n", __func__, partinfo.fd);
	    close(partinfo.fd);
	    return false;
	}
	close(partinfo.fd);

	if (strcmp(cblock_result.sig, MRDUMP_SIG) != 0) {
	    ALOGE("%s: Signature mismatched\n", __func__);
	    return false;
	}
	/* Copy/parsing status line */
	strncpy(result->status_line, cblock_result.status, sizeof(cblock_result.status));
	result->status_line[sizeof(result->status_line) - 1] = 0;

	char *saveptr;
	cblock_result.status[sizeof(cblock_result.status) - 1] = 0;
	char *strval = strtok_r(cblock_result.status, "\n", &saveptr);
	if (strval != NULL) {
	    if (strcmp(strval, "OK") == 0) {
		result->status = MRDUMP_STATUS_OK;
		result->output = MRDUMP_OUTPUT_NULL;

		do {
		    strval = strtok_r(NULL, "\n", &saveptr);
		    if (strval != NULL) {
                        if (strncmp(strval, "OUTPUT:", 7) == 0) {
			    if (strcmp(strval + 7, "EXT4_DATA") == 0) {
				result->output = MRDUMP_OUTPUT_EXT4_DATA;
			    }
			    else if (strcmp(strval + 7, "VFAT_INT_STORAGE") == 0) {
				result->output = MRDUMP_OUTPUT_VFAT_INT_STORAGE;
			    }
			    else {
				return false;
			    }
			}
			else if (strncmp(strval, "MODE:", 5) == 0) {
			    strlcpy(result->mode, strval + 5, sizeof(result->mode));
			}
		    }
		} while (strval != NULL);
	    }
	    else if (strcmp(strval, "NONE") == 0) {
		result->status = MRDUMP_STATUS_NONE;
	    }
	    else if (strcmp(strval, "CLEAR") == 0) {
		result->status = MRDUMP_STATUS_NONE;
	    }
	    else {
		result->status = MRDUMP_STATUS_FAILED;
	    }
	}
	else {
	    ALOGE("%s: status parsing error \"%s\"\n", __func__, cblock_result.status);
	    return false;
	}

	strncpy(result->log_buf, cblock_result.log_buf, sizeof(cblock_result.log_buf));
	result->log_buf[sizeof(result->log_buf) - 1] = 0;
	return true;
    }
    return false;
}
