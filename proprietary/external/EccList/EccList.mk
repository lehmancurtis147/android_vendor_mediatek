#
# Ecc List XML File
#

LOCAL_PATH := vendor/mediatek/proprietary/external/EccList

ECC_FILES:=$(wildcard $(LOCAL_PATH)/ecc_list*.xml)
CDMA_ECC_FILES:=$(wildcard $(LOCAL_PATH)/cdma_ecc_list*.xml)

PRODUCT_COPY_FILES += $(foreach file,$(ECC_FILES), $(file):$(TARGET_COPY_OUT_VENDOR)/etc/$(notdir $(file)):mtk)
PRODUCT_COPY_FILES += $(foreach file,$(CDMA_ECC_FILES), $(file):$(TARGET_COPY_OUT_VENDOR)/etc/$(notdir $(file)):mtk)
