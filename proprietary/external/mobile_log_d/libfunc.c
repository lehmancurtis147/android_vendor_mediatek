#define MTK_LOG_ENABLE 1
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <libgen.h>
#include <log/log.h>
#include <sys/stat.h>
#include <cutils/properties.h>
//#include <selinux/android.h>

#include "config.h"
#include "global_var.h"
#include "mlog.h"
#include "libfunc.h"

int create_dir(const char* whole_path)
{
	char temp[255],dname[255];

	ALOGI("create dir %s",whole_path);
	strncpy(temp,whole_path,sizeof(temp)-1);
	strncpy(dname, dirname(temp),sizeof(dname)-1);

	if (access(dname,F_OK) != 0) {
		create_dir(dname);
	}

	if (TEMP_FAILURE_RETRY(mkdir(whole_path, 0775)) == -1) {
		ALOGE("mkdir %s fail(%s)",whole_path, strerror(errno));
		if (errno == EEXIST)
			return 1;
		else
			return 0;
	}

	return 1;
}


int notify_client(int clientfd, char *msg)
{
	ALOGD("write %s to client %d ", msg, clientfd);
	write(clientfd, msg, strlen(msg));
	ALOGD("write to client over");

	return 1;
}

int is_mblog_running()
{
	char is_running[PROPERTY_VALUE_MAX];
	property_get(PROP_RUNNING, is_running, "0");
	return atoi(is_running);
}

char *set_cur_logpath(char *logpath)
{
	if (cur_logging_path[0] != 0x00)
		snprintf(last_logging_path, sizeof(last_logging_path), "%s", cur_logging_path);

	snprintf(cur_logging_path, sizeof(cur_logging_path), "%s", logpath);
	property_set(PROP_PATH, logpath);

	MLOGI_DATA("set cur path: %s", logpath);
	return cur_logging_path;
}

void copy_file(char *srcfile, char *desfile)
{
	unsigned long ret, total_size;
	int need_exit=0;
	FILE *desfp, *srcfp;
	char cpbuffer[8192];

	if (access(srcfile, F_OK) != 0)
		return;

	if ((desfp = fopen(desfile, "a+")) == NULL) {
		MLOGE_BOTH("open %s error when copy(%s)",desfile,strerror(errno));
		return;
	}

	total_size = 0;
	if ((srcfp = fopen(srcfile, "rb")) != NULL) {
		while (!feof(srcfp)) {
			if (g_mblog_status == STOPPED) {
				MLOGE_BOTH("status is topped");
				need_exit=1;
				break;
			}

			memset(cpbuffer, 0x0, sizeof(cpbuffer));
			ret = fread(cpbuffer, sizeof(char), sizeof(cpbuffer), srcfp);
			if (ret != fwrite(cpbuffer, sizeof(char), ret, desfp)) {
				MLOGE_BOTH("fwrite to %s error! %s(%d)",srcfile,strerror(errno),errno);
				continue;
			}
		}

		fclose(srcfp);
		if (remove(srcfile) < 0)
			MLOGE_BOTH("remove %s fail(%s)", srcfile, strerror(errno));
	}

	fclose(desfp);
	if (need_exit)
		pthread_exit(0);
	usleep(100000);
}
