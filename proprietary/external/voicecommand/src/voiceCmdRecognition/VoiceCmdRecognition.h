
/*******************************************************************************
 *
 * Filename:
 * ---------
 * VoiceCmdRecognition.h
 *
 * Project:
 * --------
 *   Android
 *
 * Description:
 * ------------
 *   The voice command handler APIs.
 *
 * Author:
 * -------
 *   Donglei Ji(mtk80823)
 *
 *******************************************************************************/

#ifndef _VOICE_CMD_RECOGNITION_H_
#define _VOICE_CMD_RECOGNITION_H_

#include<pthread.h>

#include "../common/AudioStream.h"
#include "voiceunlock2/include/VowEngine_training.h"

using namespace android;

#define ID_RIFF 0x46464952
#define ID_WAVE 0x45564157
#define ID_FMT  0x20746d66
#define ID_DATA 0x61746164

#define FORMAT_PCM 1

#define FILE_NAME_LEN_MAX 256
#define PCM_FILE_GAIN 16      /* boost 24dB when writing waveform for voicecommand playing */

struct wav_header {
	int riff_id;
	int riff_sz;
	int riff_fmt;
	int fmt_id;
	int fmt_sz;
	short audio_format;
	short num_channels;
	int sample_rate;
	int byte_rate;         /* sample_rate * num_channels * bps / 8 */
	short block_align;     /* num_channels * bps / 8 */
	short bits_per_sample;
	int data_id;
	int data_sz;
};

enum voice_recognition_mode {
    VOICE_IDLE_MODE = 0x00,
    VOICE_PW_TRAINING_MODE    = 0x02,

    VOICE_RECOGNIZE_MODE_ALL = VOICE_PW_TRAINING_MODE

};

enum voice_msg {
    VOICE_ERROR       = -1,
    VOICE_TRAINING = 1
};

enum voice_mode {
    VOICE_INVALID_MODE,
    VOICE_NORMAL_MODE,
    VOICE_HEADSET_MODE,
    VOICE_HANDFREE_MODE,
    
    VOICE_MODE_NUM_MAX
};

enum voice_wakeup {
    VOICE_UNLOCK,
    VOICE_WAKEUP_NO_RECOGNIZE,
    VOICE_WAKEUP_RECOGNIZE,

    VOICE_WAKE_UP_MODE_NUM
};

class VoiceCmdRecognitionListener: virtual public RefBase
{
public:
    virtual void notify(int message, int ext1, int ext2, char **ext3 = NULL) = 0;
};

class AudioStream;
class VoiceCmdRecognition: virtual public RefBase
{
public:
    VoiceCmdRecognition(audio_source_t inputSource=AUDIO_SOURCE_UNPROCESSED, unsigned int sampleRate=16000, unsigned int channelCount=1);
    ~VoiceCmdRecognition();
    VoiceCmdRecognition(const VoiceCmdRecognition &);
    VoiceCmdRecognition & operator=(const VoiceCmdRecognition &);

    status_t initCheck();
    status_t setVoicePasswordFile(int fd, int64_t offset, int64_t length);
    status_t setVoicePatternFile(int fd, int64_t offset, int64_t length);
    status_t setVoicePatternFile(const char *path);
    status_t setVoiceUBMFile(const char *path);
    status_t setVoiceFeatureFile(int fd, int64_t offset, int64_t length);
    status_t setCommandId(int id);
    status_t setInputMode(int input_mode);

    /*for voice wakeup feature - SWIP in framework*/
    status_t setVoiceTrainingMode(int mode);
    status_t setVoiceWakeupInfoPath(const char * path);
    status_t setVoiceWakeupMode(int mode);
    status_t continueVoiceTraining();

    status_t startCaptureVoice(unsigned int mode);
    status_t stopCaptureVoice(unsigned int mode);
    status_t startVoiceTraining();
    status_t getVoiceIntensity(int *maxAmplitude);
    status_t setListener(const sp<VoiceCmdRecognitionListener>& listener);
    void writeWavFile(const short *pBuf, int64_t length);
    void notify(int message, int ext1, int ext2, char **ext3 = NULL);
    void recordLockSignal();
    int recordLockWait(int delayMS = 0);

    sp<AudioStream> m_pAudioStream;

    unsigned int m_RecognitionMode;
    bool m_bStarted;
    bool m_bNeedToWait;
    bool m_bNeedToRelease;
    bool m_bSpecificRefMic;
private:
    status_t voiceRecognitionInit(unsigned int mode);
    status_t voiceRecognitionRelease(unsigned int mode);
    status_t startAudioStream();

    sp<VoiceCmdRecognitionListener> m_pListener;

    pthread_mutex_t m_RecordMutex;
    pthread_cond_t m_RecordExitCond;
	
    struct wav_header m_WavHeader;
    audio_source_t m_InputSource;
    
    int m_SampleRate;
    int m_Channels; 
    int m_PasswordFd;
    int m_PatternFd;
    int m_FeatureFd;
    int m_CommandId;
    int m_VoiceMode;
    int m_WakeupMode;

    pthread_t m_Tid;

    char *m_pStrWakeupInfoPath;
	
    char m_strPatternPath[FILE_NAME_LEN_MAX];
    char m_strUBMPath[FILE_NAME_LEN_MAX];
    bool m_enrolling;
};
#endif
