/*******************************************************************************
 *
 * Filename:
 * ---------
 * VoiceCmdRecognition.cpp
 *
 * Project:
 * --------
 *   Android
 *
 * Description:
 * ------------
 *   This file implements the  handling about voice recognition features.
 *
 * Author:
 * -------
 *   Donglei Ji (mtk80823)
 *
 *------------------------------------------------------------------------------
 *******************************************************************************/

/*=============================================================================
 *                              Include Files
 *===========================================================================*/
#include <cutils/log.h>
#include <utils/Errors.h>
#include <cutils/properties.h>
#include "VoiceCmdRecognition.h"
#define MTK_LOG_ENABLE 1
#include "CFG_AUDIO_File.h"
#include "AudioToolkit.h"
#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "VoiceCommandRecognition"

#define SAMPLES1 160  // 10ms for 16k sample rate and 1 channel
#define SAMPLES2 240  // 15ms for 16k sample rate and 1 channel

#define kWaitingTimeOutMS 3000 // for wiat lock time out

#if VOW_PHASE25_SUPPORT
#define VOW_P25_UBM_PATH "/vendor/etc/vowphase25/training/ubmfile/"
#define VOW_P25_PATTERN_PATH "/sdcard/"
#define VOW_P25_DEBUG_PATH "/sdcard/"
#endif
#if VOW_SID_SUPPORT
#define VOW_P23_UBM_PATH "/vendor/etc/vowphase23/training/ubmfile/"
#define VOW_P23_PATTERN_PATH "/sdcard/0_vp23.dat"
#endif

using namespace android;

static __inline short ClipToShort(int x)
{
    int sign;

    /* clip to [-32768, 32767] */
    sign = x >> 31;
    if (sign != (x >> 15))
    x = sign ^ ((1 << 15) - 1);

    return (short)x;
}

String8 PrintEncodedString(String8 strKey, size_t len, void *ptr)
{
    String8 returnValue = String8("");
    size_t sz_Needed;
    size_t sz_enc;
    char *buf_enc = NULL;
    bool bPrint = false;

    //ALOGD("%s in, len = %d", __FUNCTION__, len);
    sz_Needed = Base64_OutputSize(true, len);
    buf_enc = new char[sz_Needed + 1];
    buf_enc[sz_Needed] = 0;

    sz_enc = Base64_Encode((unsigned char *)ptr, buf_enc, len);

    if (sz_enc != sz_Needed) {
        ALOGE("%s(), Encode Error!!!after encode (%s), len(%d), sz_Needed(%d), sz_enc(%d)",
            __FUNCTION__, buf_enc, (int)len, (int)sz_Needed, (int)sz_enc);
    } else {
        bPrint = true;
        //ALOGD("%s(), after encode (%s), len(%d), sz_enc(%d)", __FUNCTION__, buf_enc, len, sz_enc);
    }

    if (bPrint) {
        String8 StrVal = String8(buf_enc, sz_enc);
        returnValue += strKey;
        returnValue += StrVal;
        //returnValue += String8(";");
    }

    if (buf_enc != NULL) {
        delete[] buf_enc;
    }

    return returnValue;
}
/*
static status_t GetDecodedData(String8 strPara, size_t len, void *ptr)
{
    size_t sz_in = strPara.size();
    size_t sz_needed = Base64_OutputSize(false, sz_in);
    size_t sz_dec;
    status_t ret = NO_ERROR;

    if (sz_in <= 0) {
        return NO_ERROR;
    }

    //ALOGD("%s in, len = %d", __FUNCTION__, len);
    unsigned char *buf_dec = new unsigned char[sz_needed];
    sz_dec = Base64_Decode(strPara.string(), buf_dec, sz_in);

    if (sz_dec > sz_needed || sz_dec <= sz_needed - 3) {
        ALOGE("%s(), Decode Error!!!after decode (%s), sz_in(%d), sz_needed(%d), sz_dec(%d)",
            __FUNCTION__, buf_dec, (int)sz_in, (int)sz_needed, (int)sz_dec);
    } else {
        // sz_needed-3 < sz_dec <= sz_needed
        //ALOGD("%s(), after decode, sz_in(%d), sz_dec(%d) len(%d) sizeof(ret)=%d",
        //    __FUNCTION__, sz_in, sz_dec, len, sizeof(ret));
        //print_hex_buffer (sz_dec, buf_dec);
    }

    if ((len == 0) || (len == sz_dec-sizeof(ret))) {
       if (len) {
           ret = (status_t)*(buf_dec);
           unsigned char *buff = (buf_dec + 4);
           memcpy(ptr, buff, len);
       } else {
          const char * IntPtr = (char *)buf_dec;
          ret = atoi(IntPtr);
          //ALOGD("%s len = 0 ret(%d)", __FUNCTION__, ret);
       }
    } else {
       ALOGD("%s decoded buffer isn't right format", __FUNCTION__);
    }

    if (buf_dec != NULL) {
        delete[] buf_dec;
    }

    return ret;
}

status_t GetAudioData(int par1, size_t len, void *ptr)
{
    static String8 keyGetBuffer = String8("GetBuffer=");
    int iPara[2];
    iPara[0] = par1;
    iPara[1] = len;

    String8 strPara = PrintEncodedString(keyGetBuffer, sizeof(iPara), iPara);
    String8 returnValue = AudioSystem::getParameters(0, strPara);

    String8 newval; //remove "GetBuffer="
    newval.appendFormat("%s", returnValue.string() + keyGetBuffer.size());

    return GetDecodedData(newval, len, ptr);
}
*/
bool is_support_dual_mic()
{
    #if MTK_DUAL_MIC_SUPPORT
        return true;
    #else
        return false;
    #endif
}
/*
bool is_tablet_library()
{
    char value[PROPERTY_VALUE_MAX];
    property_get("ro.vendor.mtk_is_tablet", value, "0");
    int bflag=atoi(value);
    ALOGD("is_tablet_library:%d", bflag);

    return ((bflag == 1)?true:false);
}
*/
static void * captureVoiceLoop(void *pParam)
{
    ALOGD("capture voice thread in +");
    VoiceCmdRecognition *pVoiceRecogize = (VoiceCmdRecognition *)pParam;

    bool bFirstFrame = true;
    int size = 0;
    int confidence = 0;
    int msg = vowe_next_frame;
    int notify_back = -1;
    int readCount = 0;
    int isLRSwitched = 0;

    String8 keyValuePairs = String8("");

    short *pDLBuf = new short[SAMPLES2];
    short *pULBuf1 = new short[SAMPLES2];
    short *pULBuf2 = new short[SAMPLES2];

    memset(pDLBuf, 0, SAMPLES2*sizeof(short));
    memset(pULBuf1, 0, SAMPLES2*sizeof(short));
    memset(pULBuf2, 0, SAMPLES2*sizeof(short));
    ALOGV("pVoiceRecogize->recordLockSignal()");
    pVoiceRecogize->recordLockSignal();

    while (pVoiceRecogize->m_bStarted) {
        // reset msg
        msg = vowe_next_frame;

        if (popcount(pVoiceRecogize->m_RecognitionMode)==0) {
            usleep(5000);
        }
        // to capture valid voice data
        while ((msg == vowe_next_frame) && popcount(pVoiceRecogize->m_RecognitionMode)) {
            if (pVoiceRecogize->m_pAudioStream != 0) {
                if ((readCount % 20) == 0) {
                    readCount = 0;
                    if (is_support_dual_mic())
                    {
                        keyValuePairs = AudioSystem::getParameters(0, String8("LRChannelSwitch"));
                        sscanf(keyValuePairs.string(),"LRChannelSwitch=%d", &isLRSwitched);
                    }

                    ALOGV("captureVoiceLoop -  isLRSwitched:%d", isLRSwitched);
                }

                size = bFirstFrame ? SAMPLES2 : SAMPLES1;
                if (pVoiceRecogize->m_pAudioStream->readPCM(pULBuf1, pULBuf2, pDLBuf, size)!=OK) {
                    break;
                }

                if (bFirstFrame) {
                    bFirstFrame = false;
                }

                if (pVoiceRecogize->m_RecognitionMode & VOICE_PW_TRAINING_MODE) {
                    ALOGV("endPointDetection before");
                    if (confidence != 100) {
                        //ALOGD("size = %d", size);
                        if (VOWE_training_inputMic(0, pULBuf1, size) == vowe_bad) {
                            ALOGD("Training InputMic error");
                            break;
                        }
                        msg = VOWE_training_diagnose(&confidence);
                        ALOGV("[dato]msg=%d, confidence=%d", msg, confidence);
                    }
                }

                readCount++;
            }else{
                break;
            }
        }

        // for voice password taining mode
        if ((msg != vowe_next_frame) && (pVoiceRecogize->m_RecognitionMode & VOICE_PW_TRAINING_MODE)) {

            pVoiceRecogize->m_bNeedToWait = false;
            pVoiceRecogize->m_bNeedToRelease = true;
            switch (msg) {
                case vowe_no_speech:
                    notify_back = 6;
                    break;
                case vowe_ready_to_enroll:
                    notify_back = 0;
                    pVoiceRecogize->m_bNeedToWait = true;
                    pVoiceRecogize->m_bNeedToRelease = false;
                    break;
                case vowe_good_utterance:
                    notify_back = 1;
                    break;
                case vowe_bad_utterance:
                    notify_back = 4;//5;
                    break;
                case vowe_noisy_utterance:
                    notify_back = 2;
                    break;
                case vowe_low_snr_utterance:
                    notify_back = 3;
                    break;
                case vowe_mismatched_cmd_utterance:
                    notify_back = 4;
                    break;
                default:
                    ALOGD("[error] return value error, need check lib");
                    break;
            }

            ALOGD("PCMDiagonosis after msg:%d, confidence:%d", msg, confidence);
            pVoiceRecogize->notify(VOICE_TRAINING, notify_back, confidence);
            if ((msg == vowe_ready_to_enroll) && (pVoiceRecogize->m_bNeedToWait)) {
                pVoiceRecogize->recordLockWait(kWaitingTimeOutMS);
            }
            // msg==0 -   voice is enough, it is going to training
            // msg==1 -   voice is not enough, recording on going
            // msg==2 -   the environment is too noisy
            // msg==3 -   the sound is too little
            // msg==4 -   the password is not match with previous password
            // msg==5 -   the password is already exist
            // msg==11 -  the password is not match with the password specified by vendor
        }
    }

    if (pDLBuf!=NULL) delete [] pDLBuf;
    if (pULBuf1!=NULL) delete [] pULBuf1;
    if (pULBuf2!=NULL) delete [] pULBuf2;
    pDLBuf = NULL;
    pULBuf1 = NULL;
    pULBuf2 = NULL;

    ALOGD("capture voice thread out -");
    return 0;
}
/*
void writeCallBackWrapper(void *me, const short *pBuf, int64_t length)
{
    //VoiceCmdRecognition *pVoiceRecognize = (VoiceCmdRecognition *)me;
    //pVoiceRecognize->writeWavFile(pBuf, length);
}
*/
VoiceCmdRecognition::VoiceCmdRecognition(audio_source_t inputSource, unsigned int sampleRate, unsigned int channelCount) :
   m_RecognitionMode(VOICE_IDLE_MODE),
   m_bStarted(false),
   m_bNeedToWait(false),
   m_bNeedToRelease(true),
   //m_bSpecificRefMic(false),
   m_pListener(0),
   m_InputSource(AUDIO_SOURCE_UNPROCESSED),
   m_SampleRate(sampleRate),
   m_Channels(channelCount),
   m_PasswordFd(-1),
   m_PatternFd(-1),
   m_FeatureFd(-1),
   m_CommandId(-1),
   m_VoiceMode(VOICE_NORMAL_MODE),
   m_WakeupMode(VOICE_UNLOCK),
   m_pStrWakeupInfoPath(NULL),
   m_enrolling(false)
{
    ALOGD("VoiceCmdRecognition construct in +");
    ALOGD("input source:%d,sampe rate:%d,Channel count:%d, m_InputSource%d, inputSource%d", m_InputSource,m_SampleRate,m_Channels,m_InputSource,inputSource);
    memset(m_strPatternPath, 0, FILE_NAME_LEN_MAX);
    memset(m_strUBMPath, 0, FILE_NAME_LEN_MAX);

    if (pthread_mutex_init(&m_RecordMutex, NULL)!=0) {
        ALOGW("Failed to initialize m_RecordMutex!");
    }

    if (pthread_cond_init(&m_RecordExitCond, NULL)!=0) {
        ALOGW("Failed to initialize m_RecordExitCond!");
    }
    char *voice_version;
    voice_version = (char *)VOWE_training_version();
    ALOGD("voice unlock SWIP version is:%s", voice_version);
}

VoiceCmdRecognition::~VoiceCmdRecognition()
{
    ALOGD("VoiceCmdRecognition deconstruct in +");
    if (m_pAudioStream!=0) {
        m_pAudioStream.clear();
    }

    if (m_pListener!=0) {
        m_pListener.clear();
    }

    if (m_PasswordFd>=0) {
        ::close(m_PasswordFd);
    }

    if (m_PatternFd>=0) {
        ::close(m_PatternFd);
    }

    if (m_FeatureFd>=0) {
        ::close(m_FeatureFd);
    }
}

status_t VoiceCmdRecognition::initCheck()
{
    return m_pAudioStream != 0 ? NO_ERROR : NO_INIT;
}

status_t VoiceCmdRecognition::setVoicePasswordFile(int fd, int64_t offset, int64_t length)
{
    ALOGV("setVoicePasswordFile in +");
    if(fd<0) {
        ALOGE("Invalid file descriptor: %d", fd);
        return -EBADF;
    }

    if (m_PasswordFd>=0){
        ::close(m_PasswordFd);
    }

    m_PasswordFd = dup(fd);
    ALOGD("setVoicePasswordFile m_PasswordFd:%d, fd:%d, offset:%lld length:%lld", m_PasswordFd, fd, (long long)offset, (long long)length);
    return OK;
}

status_t VoiceCmdRecognition::setVoicePatternFile(int fd, int64_t offset, int64_t length)
{
    ALOGV("setVoicePatternFile in +");
    if(fd<0) {
        ALOGE("Invalid file descriptor: %d", fd);
        return -EBADF;
    }

    if (m_PatternFd>=0){
        ::close(m_PatternFd);
    }

    m_PatternFd = dup(fd);
    ALOGD("setVoicePatternFile m_PatternFd:%d, fd:%d, offset:%lld, length:%lld", m_PatternFd, fd, (long long)offset, (long long)length);
    return OK;
}


status_t VoiceCmdRecognition::setVoicePatternFile(const char *path)
{
    ALOGV("setVoicePatternFile in +");
    if (path==NULL) {
        ALOGE("voice patter file path is null!!");
        return BAD_VALUE;
    }

    strncpy(m_strPatternPath, path, FILE_NAME_LEN_MAX-1);
    return OK;
}

status_t VoiceCmdRecognition::setVoiceUBMFile(const char *path)
{
    ALOGV("setVoiceUBMFile in +");
    if (path==NULL) {
        ALOGE("UBM files path is null: %s", path);
        return BAD_VALUE;
    }

    strncpy(m_strUBMPath, path, FILE_NAME_LEN_MAX-1);

    return OK;
}

status_t VoiceCmdRecognition::setVoiceFeatureFile(int fd, int64_t offset, int64_t length)
{
    ALOGV("setVoiceFeatureFile in +");
    if(fd<0) {
        ALOGE("Invalid feature file descriptor: %d", fd);
        return -EBADF;
    }

    if (m_FeatureFd>=0){
        ::close(m_FeatureFd);
    }

    m_FeatureFd = dup(fd);
    ALOGD("setVoiceFeatureFile m_FeatureFd:%d, fd:%d, length%lld ,offset%lld", m_FeatureFd, fd, (long long)length, (long long)offset);
    return OK;
}

status_t VoiceCmdRecognition::setCommandId(int id)
{
    ALOGV("setCommandId in +");
    if (id<0) {
        ALOGE("command id is invalide: %d", id);
        return BAD_VALUE;
    }
    m_CommandId = id;
    return OK;
}

status_t VoiceCmdRecognition::setInputMode(int input_mode)
{
    ALOGV("setInputMode input_mode:%d", input_mode);
    
    if (input_mode>=VOICE_MODE_NUM_MAX) {
        ALOGW("input mode is invalide!!");
        return BAD_VALUE;
    }
    
    m_VoiceMode = input_mode;
    return OK;
}

/*for voice wakeup feature*/
status_t VoiceCmdRecognition::setVoiceTrainingMode(int mode)
{
    ALOGD("setVoiceTrainingMode recongnition mode: %d", mode);

    if (mode < 0 || mode > VOICE_WAKE_UP_MODE_NUM) {
        ALOGD("setVoiceTrainingMode mode error");
        return BAD_VALUE;
    }

    if (mode == VOICE_WAKEUP_NO_RECOGNIZE) {
        m_WakeupMode = vowe_mode_pdk_fullRecognizer;
    } else if (mode == VOICE_WAKEUP_RECOGNIZE) {
        m_WakeupMode = vowe_mode_udk_lowPower;
    }
    ALOGD("m_WakeupMode = %d", m_WakeupMode);
    return OK;
}

status_t VoiceCmdRecognition::setVoiceWakeupInfoPath(const char * path)
{
    int size = 0;
    if (path == NULL) {
        ALOGD("setVoiceWakeupInfoPath file path is NULL!");
        return BAD_VALUE;
    }

    ALOGV("setVoiceWakeupInfoPath file path: %s", path);
    size = strlen(path);
    if (size > FILE_NAME_LEN_MAX) {
        ALOGD("setVoiceWakeupInfoPath file path is too long length:%d!", size);
        return BAD_VALUE;
    }

    if (m_pStrWakeupInfoPath == NULL) {
        m_pStrWakeupInfoPath = new char[size + 1];
        memset(m_pStrWakeupInfoPath, 0, (size + 1) * sizeof(char));
    }

    if (m_pStrWakeupInfoPath) {
        strncpy(m_pStrWakeupInfoPath, path, size);
    } else {
        ALOGW("setVoiceWakeupInfoPath allocate memory fail!");
        return BAD_VALUE;
    }

    return OK;
}

status_t VoiceCmdRecognition::setVoiceWakeupMode(int mode)
{
    ALOGD("setVoiceWakeupMode mode: %d", mode);

    if (mode < 0 || mode > VOICE_WAKE_UP_MODE_NUM) {
        ALOGD("setVoiceWakeupMode mode error");
        return BAD_VALUE;
    }

    m_WakeupMode = mode;

    return OK;
}

status_t VoiceCmdRecognition::continueVoiceTraining()
{
    ALOGD("continueVoiceTraining in +");
    if (VOWE_training_setArgument(vowe_argid_pushToTalk, 1) == vowe_bad) {
        ALOGD("setArgument error");
    }
    return OK;
}

status_t VoiceCmdRecognition::startCaptureVoice(unsigned int mode)
{
    ALOGD("startCaptureVoice in +");

    // for create thread timeout
    struct timeval now;
    struct timespec timeout;
    gettimeofday(&now,NULL);
    timeout.tv_sec  = now.tv_sec + 3;
    timeout.tv_nsec = now.tv_usec*1000;

    // parameters mode:
    // VOICE_IDLE_MODE for idle mode, created but not work
    // VOICE_PW_TRAINING_MODE for voice password training.
    if ((popcount(mode)!=1) || ((mode&VOICE_RECOGNIZE_MODE_ALL)==0))
    return BAD_VALUE;

    if (m_RecognitionMode & VOICE_PW_TRAINING_MODE) {
        ALOGE("voice password training is running");
        return BAD_VALUE;
    }
/*
    VOICE_RECOGNITION_PARAM_STRUCT custRecogParam;
    GetAudioData(0x200, sizeof(VOICE_RECOGNITION_PARAM_STRUCT), (void *)&custRecogParam); //0x200 GET_VOICE_CUST_PARAM
    for(int i=0;i<VOICE_RECOG_FEATURE_NUM_MAX;i++) {
        for(int j=0;j<VOICE_RECOG_PARAM_NUM_MAX;j++)
            ALOGV("voice customization parameters, cust_param[%d][%d]:%d", i, j, custRecogParam.cust_param[i][j]);
    }

    short inputFIRCoef[SPC_MAX_NUM_RECORD_INPUT_FIR][WB_FIR_NUM];
    GetAudioData(0x201, sizeof(inputFIRCoef), (void *)inputFIRCoef); //0x201 GET_VOICE_FIR_COEF

    int16_t voiceGain[2];
    GetAudioData(0x202, 2*sizeof(int16_t), (void *)&voiceGain); //0x100 GET_AUDIO_VER1_DATA
    ALOGD("startCaptureVoice, phone mic gain:%d, headset mic gain:%d", voiceGain[0], voiceGain[1]);

    VOICE_UNLOCK_CUSTOM_INFO_WRAP voiceCustomInfo;
    memcpy(voiceCustomInfo.Cust_Para, custRecogParam.cust_param, sizeof(custRecogParam.cust_param[VOICE_RECOG_FEATURE_NUM_MAX][VOICE_RECOG_PARAM_NUM_MAX]));

    if (m_VoiceMode==VOICE_HEADSET_MODE) {
        voiceCustomInfo.voiceFIRCoefMic1 = inputFIRCoef[1]; // for headset mode
        voiceCustomInfo.voiceFIRCoefMic2 = NULL;

        voiceCustomInfo.voiceGainMic1 = (int)voiceGain[1]; // headset mode
        voiceCustomInfo.voiceGainMic2 = (int)voiceGain[1]; // headset mode

        voiceCustomInfo.micNumFlag = 1;
    } else if(m_VoiceMode!=VOICE_INVALID_MODE){
        if (is_support_dual_mic())
        {
            ALOGD("startCaptureVoice, dual mic FIR 0:%d, FIR 89:%d", inputFIRCoef[9][0], inputFIRCoef[9][WB_FIR_NUM-1]);
            m_bSpecificRefMic = custRecogParam.cust_param[1][0]&0x01;
            if (m_bSpecificRefMic) {
                voiceCustomInfo.voiceFIRCoefMic1 = inputFIRCoef[9]; // ref mic
                voiceCustomInfo.voiceFIRCoefMic2 = inputFIRCoef[8]; // main mic
            }else {
                voiceCustomInfo.voiceFIRCoefMic1 = inputFIRCoef[8]; // main mic
                voiceCustomInfo.voiceFIRCoefMic2 = inputFIRCoef[9]; // ref mic
            }
            voiceCustomInfo.micNumFlag = 2;
        }else {
            voiceCustomInfo.voiceFIRCoefMic1 = inputFIRCoef[0]; // for normal mode
            voiceCustomInfo.voiceFIRCoefMic2 = NULL;
            voiceCustomInfo.micNumFlag = 1;
        }
    
        voiceCustomInfo.voiceGainMic1 = (int)voiceGain[0]; // normal mode
        voiceCustomInfo.voiceGainMic2 = (int)voiceGain[0]; // normal mode
    }
*/
    // for voice recognition initialize
    status_t ret = OK;
    ret = voiceRecognitionInit(mode);
    if (ret != OK) {
        ALOGW("startCaptureVoice, voice recognition fail!");
        return ret;
    }

    ret = startAudioStream();
    if (ret != OK) {
        ALOGW("startAudioStream fail!");
        voiceRecognitionRelease(mode);
        return ret;
    }

    m_RecognitionMode = m_RecognitionMode | mode;

    ALOGD("startCaptureVoice out -");
    return OK;
}

status_t VoiceCmdRecognition::stopCaptureVoice(unsigned int mode)
{
    ALOGD("stopCaptureVoice in +");
    
    // parameters mode:
    // VOICE_IDLE_MODE for idle mode, created but not work
    // VOICE_PW_TRAINING_MODE for voice password training.
    if ((popcount(mode) != 1) || !(mode & VOICE_RECOGNIZE_MODE_ALL) || !(mode & m_RecognitionMode)) {
        ALOGW("stopCaptureVoice mode:0x%x, recognizing mode:0x%x", mode, m_RecognitionMode);
        return BAD_VALUE;
    }

    while (m_enrolling == true) {
        usleep(10000);
    }


    m_RecognitionMode = m_RecognitionMode&(~mode);
    // signal captureVoiceLoop thread to go on.
    m_bNeedToWait = false;
    ALOGV("recordLockSignal()");
    recordLockSignal();
    ALOGV("stopCaptureVoice after signal--");
    
    ALOGV("mode 0x%x m_RecognitionMode 0x%x",mode,m_RecognitionMode);
    if (popcount(m_RecognitionMode) == 0) {
        m_bStarted = false;
        ALOGD("stopCaptureVoice wait thread exit");
        pthread_join(m_Tid, NULL);
        ALOGD("stopCaptureVoice wait thread exit done");

        if (m_pAudioStream!=0) {
            m_pAudioStream->stop();
            m_pAudioStream.clear();
            m_pAudioStream = NULL;
        }
    }
	
    if (mode == VOICE_PW_TRAINING_MODE) {
        if (m_bNeedToRelease) {
            voiceRecognitionRelease(VOICE_PW_TRAINING_MODE);
        }
        
        m_bNeedToRelease = true;
    }

    ALOGD("stopCaptureVoice out -");
    return OK;
}

status_t VoiceCmdRecognition::startVoiceTraining()
{
    ALOGD("startVoiceTraining in +");
    status_t ret = OK;

    // voice data is enough, start to training voice password
    VOWE_training_enroll_return_values info;

    m_enrolling = true;
    if (VOWE_training_enroll(&info) == vowe_bad) {
        ALOGW("TrainingEnroll failed!");
        ret = UNKNOWN_ERROR;
    }
    writeWavFile(info.replaySignal, info.replaySignalSampleNumber);

    voiceRecognitionRelease(VOICE_PW_TRAINING_MODE);
    m_enrolling = false;
    return ret;
}

status_t VoiceCmdRecognition::getVoiceIntensity(int *maxAmplitude)
{
    ALOGV("getVoiceIntensity in +");

    if (maxAmplitude == NULL) {
        ALOGE("Null pointer argument");
        return BAD_VALUE;
    }

    if (m_pAudioStream!=0) {
        // get the intensity for recording PCM data
        *maxAmplitude = m_pAudioStream->getMaxAmplitude();
    } else {
        *maxAmplitude = 0;
    }

    ALOGV("getVoiceIntensity maxAmplitude %d",*maxAmplitude);
    return OK;
}

status_t VoiceCmdRecognition::setListener(const sp<VoiceCmdRecognitionListener>& listener)
{
    ALOGV("setListener in +");
    m_pListener = listener;

    return NO_ERROR;
}

void VoiceCmdRecognition::writeWavFile(const short *pBuf, int64_t length)
{
    short *buf_voice_cmd = NULL;
    ALOGV("writeWavFile in +");
    if (pBuf==NULL||length==0) {
        ALOGW("buffer pointer is null or date length is zero~");
        return;
    }

    FILE *fd = fdopen(m_PasswordFd, "wb");
    if (fd==NULL) {
        ALOGE("open file descriptor fail, errorno: %s", strerror(errno));
        return;
    }

    // write wave header, this file is for unlock password.
    m_WavHeader.riff_id = ID_RIFF;
    m_WavHeader.riff_sz = length*sizeof(short) + 8 + 16 + 8;
    m_WavHeader.riff_fmt = ID_WAVE;
    m_WavHeader.fmt_id = ID_FMT;
    m_WavHeader.fmt_sz = 16;
    m_WavHeader.audio_format = FORMAT_PCM;
    m_WavHeader.num_channels = 1;
    m_WavHeader.sample_rate = m_SampleRate;
    m_WavHeader.byte_rate = m_WavHeader.sample_rate * m_WavHeader.num_channels * 2;
    m_WavHeader.block_align = m_WavHeader.num_channels * 2;
    m_WavHeader.bits_per_sample = 16;
    m_WavHeader.data_id = ID_DATA;
    m_WavHeader.data_sz = length*sizeof(short);

    // apply gain for voice command playing file
    buf_voice_cmd = new short[length];
    memcpy(buf_voice_cmd, pBuf, length*sizeof(short));
    for(int i=0;i<length;i++) {
        buf_voice_cmd[i] = ClipToShort((int)buf_voice_cmd[i] * (int)PCM_FILE_GAIN);
    }
    fwrite(&m_WavHeader, sizeof(m_WavHeader), 1, fd);
    fwrite((const void *)buf_voice_cmd, length*sizeof(short), 1, fd);
    if (buf_voice_cmd != NULL) {
        delete[] buf_voice_cmd;
    }
    fflush(fd);
    fclose(fd);
}

void VoiceCmdRecognition::notify(int message, int ext1, int ext2, char **ext3)
{
    ALOGV("notify in + msg %d,ext1 %d,ext2 %d",message,ext1,ext2);
    m_pListener->notify(message, ext1, ext2, ext3);
}

void VoiceCmdRecognition::recordLockSignal()
{
    // for sync recording, signal to record lock wait
    pthread_mutex_lock(&m_RecordMutex);
    pthread_cond_signal(&m_RecordExitCond);
    pthread_mutex_unlock(&m_RecordMutex);
}

int VoiceCmdRecognition::recordLockWait(int delayMS)
{
    int ret = 0;
    // wait record signal, if delayMs is not 0, it will time out when reachs the delayMS time.
    pthread_mutex_lock(&m_RecordMutex);
    if (delayMS!=0) {
        struct timeval now;
        struct timespec timeout;
        gettimeofday(&now,NULL);
        timeout.tv_sec  = now.tv_sec + delayMS/1000;
        timeout.tv_nsec = now.tv_usec*1000;
        ret = pthread_cond_timedwait(&m_RecordExitCond, &m_RecordMutex, &timeout);
    }else {
        ret = pthread_cond_wait(&m_RecordExitCond, &m_RecordMutex);
    }
    pthread_mutex_unlock(&m_RecordMutex);
    ALOGV("recordLockWait ret %d",ret);
    return ret;
}

status_t VoiceCmdRecognition::voiceRecognitionInit(unsigned int mode)
{
    ALOGD("voiceRecognitionInit in +");
    status_t ret = OK;

    switch(mode) {
        case VOICE_PW_TRAINING_MODE:
            if ((m_CommandId < 0) || (m_PatternFd < 0) || (m_FeatureFd < 0)) {
                ALOGE("parameters do not initialize, command id: %d, pattern fd: %d, feature fd: %d", m_CommandId, m_PatternFd, m_FeatureFd);
                return BAD_VALUE;
            }
            /*
            {
                int len = strlen(m_strUBMPath);

                memset(ubm_param, 0, FILE_NAME_LEN_MAX);
                memcpy(ubm_param, m_strUBMPath, len-1);
                ALOGD("[dato]m_strUBMPath len = %d, ubm_param = %s", len, ubm_param);
            }
            */
            VOWE_training_init_parameters training_init_info;

            training_init_info.mode = m_WakeupMode;
            training_init_info.micNumber = 1;
            training_init_info.frameLength = 400;
            training_init_info.frameShift = 160;
            training_init_info.inputFolder = m_strUBMPath;
            training_init_info.debugFolder = NULL;
            training_init_info.outputFileDescriptor = m_PatternFd;
            if (VOWE_training_init(&training_init_info) == vowe_bad) {
                ALOGE("Traning init fail!");
                ret = BAD_VALUE;
                break;
            }
            break;
        default:
            ALOGE("voiceRecognitionInit - mode: %d is unkown", mode);
            ret = BAD_VALUE;
            break;
    }
    return ret;
}

status_t VoiceCmdRecognition::voiceRecognitionRelease(unsigned int mode)
{
    ALOGD("voiceRecognitionRelease in +");
    switch(mode) {
        case VOICE_PW_TRAINING_MODE:
            if (VOWE_training_release() == vowe_bad) {
                ALOGE("Training Release init fail!");
            }
            if (m_PasswordFd >= 0)
                ::close(m_PasswordFd);
            m_PasswordFd = -1;

            if (m_PatternFd >= 0)
                ::close(m_PatternFd);
            m_PatternFd = -1;

            if (m_FeatureFd >= 0)
                ::close(m_FeatureFd);
            m_FeatureFd = -1;
            break;
        default:
            ALOGE("voiceRecognitionRelease - mode: %d is unkown", mode);
            break;
    }

    return OK;
}

status_t VoiceCmdRecognition::startAudioStream()
{
    ALOGD("startAudioStream in +");
    // for create thread timeout
    struct timeval now;
    struct timespec timeout;
    gettimeofday(&now,NULL);
    timeout.tv_sec  = now.tv_sec + 3;
    timeout.tv_nsec = now.tv_usec*1000;
	
    if (!m_bStarted) {
        if (is_support_dual_mic())
        {
            m_Channels = m_VoiceMode==VOICE_HEADSET_MODE ? 1 : 2;
        }else {
            m_Channels = 1;
        }
        
        m_pAudioStream = new AudioStream(m_InputSource, m_SampleRate, m_Channels);
        AudioSystem::getParameters(0, String8("GET_FSYNC_FLAG=0"));
        AudioSystem::getParameters(0, String8("GET_FSYNC_FLAG=1"));
        if (m_pAudioStream==0 || m_pAudioStream->start()!=OK) {
            ALOGE("start capture voice fail");
            return UNKNOWN_ERROR;
        }

        m_bStarted = true;
        pthread_mutex_lock(&m_RecordMutex);
        pthread_create(&m_Tid, NULL, captureVoiceLoop, this);
	
        if (pthread_cond_timedwait(&m_RecordExitCond, &m_RecordMutex, &timeout)==ETIME) {
            if (m_pAudioStream!=0) {
                m_pAudioStream->stop();
            }
            pthread_mutex_unlock(&m_RecordMutex);
            m_bStarted = false;
            return UNKNOWN_ERROR;
        }
        pthread_mutex_unlock(&m_RecordMutex);
    }else {
        ALOGD("startAudioStream already started!");
    }

    return OK;
}
