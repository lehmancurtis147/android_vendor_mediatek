
/*******************************************************************************
 *
 * Filename:
 * ---------
 * AudioStream.cpp
 *
 * Project:
 * --------
 *   Android
 *
 * Description:
 * ------------
 *   The class to get uplink and downlink PCM data.
 *
 * Author:
 * -------
 *   Donglei Ji(mtk80823)
 *
 *******************************************************************************/

#define MTK_LOG_ENABLE 1
#include <utils/Errors.h>
#include <utils/String16.h>
#include <cutils/log.h>
#include <cutils/properties.h>
#include <media/AudioSystem.h>
#include <media/stagefright/MetaData.h>

#include "AudioStream.h"


static int AUDIO_DROP_FRAME_MS = 120;
static int DROP_THRESHOLD = 6400;  /*100ms for 16k sample rate stereo*/
using namespace android;

#define DL_BUF_SIZE 2048//640  // 20ms for 16k sample rate mono

#ifdef LOG_TAG
#undef LOG_TAG
#endif
#define LOG_TAG "AudioStream"

// dump pcm data
static void dumpPCMData(const char* pStr, void * buffer, int count)
{
    char value[PROPERTY_VALUE_MAX];
    property_get("vendor.voicerecognize.raw", value, "0");
	  int bflag=atoi(value);
	  if(bflag) 
	  {
		FILE * fp = NULL;

		property_get("vendor.voicerecognize_data.raw", value, "0");
		int bflag2=atoi(value);
		if(bflag2) 
		{
			if (strcmp(pStr, "uplink1")==0) {
				fp= fopen("/data/VoiceRecognize_Uplink1.pcm", "ab+");
			} else if(strcmp(pStr, "uplink2")==0) {
				fp= fopen("/data/VoiceRecognize_Uplink2.pcm", "ab+");
			} else if(strcmp(pStr, "uplink-raw")==0) {
				fp= fopen("/data/VoiceRecognize_UL_Raw.pcm", "ab+");
			} else if(strcmp(pStr, "downlink")==0) {
				fp= fopen("/data/VoiceRecognize_Downlink.pcm", "ab+");
			} else if(strcmp(pStr, "downlink-raw")==0) {
				fp= fopen("/data/VoiceRecognize_DL_Raw.pcm", "ab+");
			}else{
				return;
			}

			if(fp!=NULL) {
				fwrite(buffer,count,1,fp);
				fclose(fp);
			}else {
				ALOGE("dump voicerecognize.raw fail");
			}
		}
		else
		{
			if (strcmp(pStr, "uplink1")==0) {
				fp= fopen("/sdcard/VoiceRecognize_Uplink1.pcm", "ab+");
			} else if(strcmp(pStr, "uplink2")==0) {
				fp= fopen("/sdcard/VoiceRecognize_Uplink2.pcm", "ab+");
			}else if(strcmp(pStr, "uplink-raw")==0) {
				fp= fopen("/sdcard/VoiceRecognize_UL_Raw.pcm", "ab+");
			} else if(strcmp(pStr, "downlink")==0) {
				fp= fopen("/sdcard/VoiceRecognize_Downlink.pcm", "ab+");
			} else if(strcmp(pStr, "downlink-raw")==0) {
				fp= fopen("/sdcard/VoiceRecognize_DL_Raw.pcm", "ab+");
			}else{
				return;
			}

			if(fp!=NULL) {
				fwrite(buffer,count,1,fp);
				fclose(fp);
			}else {
				ALOGE("dump voicerecognize.raw fail");
			}
		}
	}
}
static void * threadloop(void * arg)
{
    AudioStream *pAudioStream = (AudioStream *)arg;
    pAudioStream->threadFunc();
    return 0;
}

AudioStream::AudioStream(audio_source_t inputSource, uint32_t sampleRate, uint32_t channelCount) :
    m_pULBuf(NULL),
    m_bStarted(false),
    m_bNoDLPath(false),
    m_bDLStream(false),
    m_DLStandby(true),
    m_InputSource(AUDIO_SOURCE_UNPROCESSED),
    m_SampleRate(sampleRate),
    m_ChannelCount(channelCount),
    m_DropFrameCnt(0),
    m_ULReadCnt(0),
    m_ULDataLen(0),
    m_ThreadId(-1)
{
    ALOGD("AudioStream construct in +");
    ALOGV("input source:%d,sampe rate:%d,Channel count:%d", inputSource,sampleRate,channelCount);
    m_RecorderState = VOICE_RECORDER_UNINITIALIZE;
    if (sampleRate != 16000) {
        ALOGE("Invalid sampling rate %d for voice recognition", sampleRate);
        return;
    }

    if (channelCount != 1 && channelCount != 2) {
        ALOGE("Invalid number of audio channels %d for voice recognition", channelCount);
        return;
    }

    if (inputSource!=AUDIO_SOURCE_UNPROCESSED) {
        ALOGE("Invalid audio source: %d", inputSource);
        return;
    }

    if (channelCount == 1) {
        DROP_THRESHOLD /= 2;
    }

    m_pAudioSource = new AudioSource(inputSource, String16("com.mediatek.voicecommand"), sampleRate, channelCount);

    if (m_pAudioSource == 0) {
        ALOGE("create uplink stream fail");
        return;
    }

    // not retrun the actual instance, only check if instance is opened.
    m_bDLStream = 0; //AudioSystem::getVoiceUnlockDLInstance();//mike temp remove
    //if (!m_bDLStream) { //mike temp remove
    //    ALOGE("create downlink stream fail");//mike temp remove
    //    return;//mike temp remove
    //}//mike temp remove

    // set downlink samplerate and channel counts, calculate latency
    //AudioSystem::SetVoiceUnlockSRC(sampleRate, 1);//mike temp remove
#if 0  // che-jui remove
    for(uint32_t i=0;i<BUFFERS_NUM_MAX;i++) {
        m_pDLBuf[i] = new MediaBuffer(DL_BUF_SIZE);
        if (m_pDLBuf[i]==NULL) {
            ALOGW("allocate downlink buffer fail for index:%d", i);
        }else {
            m_DLBufFree.push_back(i);
        }
    }
#endif  // che-jui remove
    char keyString[32];
    char *pParamValue = NULL;
    char *pParamName  = NULL;
    String8 keyValue = AudioSystem::getParameters(0, String8("GetCaptureDropTime"));
    if (keyValue.isEmpty() == false) {
        strncpy(keyString, keyValue.string(), 31);
        pParamValue = keyString;
        pParamName = strsep(&pParamValue, "=");

        if (pParamName != 0 && pParamValue != 0) {
            AUDIO_DROP_FRAME_MS = atoi(pParamValue);
        }
    }

    ALOGD("audio record drop frame:%d ms", AUDIO_DROP_FRAME_MS);
    m_RecorderState = VOICE_RECORDER_INITIALIZED;
}

AudioStream::~AudioStream()
{
    stop();
    if (m_pAudioSource!=0) {
        m_pAudioSource.clear();
    }
#if 0  // che-jui remove
    for(uint32_t i=0;i<BUFFERS_NUM_MAX;i++) {
        if (m_pDLBuf[i]!=NULL)  m_pDLBuf[i]->release();
        m_pDLBuf[i] = NULL;
    }
#endif  // che-jui remove
    //AudioSystem::setParameters(String8("VoiceFeatureOn=0"));//mike temp remove
}

status_t AudioStream::initCheck()
{
    return m_pAudioSource == 0 ? NO_INIT : OK;
}

int32_t AudioStream::latency()
{
    return 0;
}

status_t AudioStream::start()
{
    Mutex::Autolock autoLock(m_Lock);

    if (m_RecorderState!=VOICE_RECORDER_INITIALIZED) {
        ALOGE("the recorder state is not match, state: %d", m_RecorderState);
        return INVALID_OPERATION;
    }

    if (initCheck()!=OK)
        return NO_INIT;

    mULCurTime.tv_sec = 0;
    mULCurTime.tv_nsec = 0;
    mULTime.tv_sec = 0;
    mULTime.tv_nsec = 0;
    mDLCurTime.tv_sec = 0;
    mDLCurTime.tv_nsec = 0;
    mDLStartTime.tv_sec = 0;
    mDLStartTime.tv_nsec = 0;
    status_t err = m_pAudioSource->start();
    if (err!=OK) {
        ALOGE("start audio source fail %d", err);
        return err;
    }

    m_bStarted = true;
    m_ULReadCnt = 0;
    m_DropFrameCnt = AUDIO_DROP_FRAME_MS*m_SampleRate*m_ChannelCount*2/1000;
    startDLPath();

    m_RecorderState = VOICE_RECORDER_STARTED;
    return OK;
}

status_t AudioStream::stop()
{
    ALOGD("Stop in +");
    {
        Mutex::Autolock autoLock(m_Lock);
        if (m_RecorderState!=VOICE_RECORDER_STARTED) {
            ALOGE("the recorder state is not match, state: %d", m_RecorderState);
            return INVALID_OPERATION;
        }

        m_bStarted = false;
    }

    m_DropFrameCnt = 0;
    m_ULReadCnt = 0;
    //stopDLPath();  //mike temp remove

    {
        Mutex::Autolock autoLock(m_Lock);
        if (initCheck()!=OK)
            return NO_INIT;

        if (m_pULBuf!=NULL) {
            m_pULBuf->release();
            m_pULBuf = NULL;
        }

        status_t err = m_pAudioSource->stop();
        if (err!=OK) {
            ALOGE("stop uplink stream fail %d", err);
            return err;
        }

        releaseQueuedDLBufs();
        m_RecorderState = VOICE_RECORDER_INITIALIZED;
    }
    mULCurTime.tv_sec = 0;
    mULCurTime.tv_nsec = 0;
    mULTime.tv_sec = 0;
    mULTime.tv_nsec = 0;
    mDLCurTime.tv_sec = 0;
    mDLCurTime.tv_nsec = 0;
    mDLStartTime.tv_sec = 0;
    mDLStartTime.tv_nsec = 0;
    return OK;
}

status_t AudioStream::readPCM(short *pULBuf1, short *pULBuf2, short *pDLBuf, uint32_t size)
{
    status_t err = OK;
    size_t ulOffset = 0;
    //size_t dlOffset = 0;
    //uint32_t index = 0;

    size = size*2;
    size_t ulDataNeeded = size*m_ChannelCount;
    //size_t dlDataNeeded = size;

    (void)pDLBuf; // no-op use of pDLBuf

    Mutex::Autolock autoLock(m_Lock);
    ALOGV("readPCM obtain lock~~");

    if (m_RecorderState!=VOICE_RECORDER_STARTED) {
        ALOGE("readPCM the recorder state is not match, state: %d", m_RecorderState);
        return INVALID_OPERATION;
    }

    while (ulDataNeeded>0/*||dlDataNeeded>0*/)  // che-jui modify
    {
        if (ulDataNeeded>0) {
            Mutex::Autolock autoLock(m_ULBufLock);
            if (m_pULBuf==NULL) {
                err = m_pAudioSource->read(&m_pULBuf);
                ALOGD("readPCM read data - size:%d", err);
                if (err!=OK)
                    return err;

                if(mULCurTime.tv_sec==0 && mULCurTime.tv_nsec==0) {
                    //AudioSystem::GetVoiceUnlockULTime((void*)(&mULCurTime));//mike temp remove
                    mULCurTime.tv_nsec += AUDIO_DROP_FRAME_MS*1000*1000;
                    while(mULCurTime.tv_nsec>=1000000000) {
                        mULCurTime.tv_sec += 1;
                        mULCurTime.tv_nsec -= 1000000000;
                    }
                    ALOGD("readPCM uplink start time:%ld, ulDataNeeded:%ld", mULCurTime.tv_sec, mULCurTime.tv_nsec);
                }
                dumpPCMData("uplink-raw", (void *)m_pULBuf->data(), m_pULBuf->range_length());
                dropData();
            }
            ALOGV("readPCM read data - data length:%d, ulDataNeeded:%d", m_pULBuf->range_length(), ulDataNeeded);
            if (m_pULBuf==NULL) {
                return UNKNOWN_ERROR;
            }

            if (m_pULBuf->range_length()<=ulDataNeeded) {
                extractULData(pULBuf1+ulOffset, pULBuf2+ulOffset, (short *)((char *)(m_pULBuf->data())+m_pULBuf->range_offset()), m_pULBuf->range_length());
                ulDataNeeded = ulDataNeeded - m_pULBuf->range_length();
                ulOffset = ulOffset + m_pULBuf->range_length()/2/m_ChannelCount;
                m_pULBuf->release();
                m_pULBuf = NULL;
            }else {
                extractULData(pULBuf1+ulOffset, pULBuf2+ulOffset, (short *)((char *)(m_pULBuf->data())+m_pULBuf->range_offset()), ulDataNeeded);
                m_pULBuf->set_range(m_pULBuf->range_offset()+ulDataNeeded, m_pULBuf->range_length()-ulDataNeeded);
                ulDataNeeded = 0;
            }
        }
#if 0  //che-jui remove
        if ((!m_bNoDLPath)&&(dlDataNeeded>0)) {
            ALOGV("readPCM read DL data - dlDataNeeded:%d", dlDataNeeded);

            m_DLBufLock.lock();
            if (m_DLBufReaded.empty() && m_DLStandby) {
                m_DLBufLock.unlock();
                memset((void *)((char *)pDLBuf+dlOffset), 0, dlDataNeeded);
                dlDataNeeded = 0;
                continue;
            }

            while (m_DLBufReaded.empty())
            {
                ALOGD("readPCM wait downlink data~~");
                m_DLBufLock.unlock();
                m_WorkState.wait(m_Lock);
                m_DLBufLock.lock();
            }

            List<uint32_t>::iterator it;
            it = m_DLBufReaded.begin();
            index = *it;
            if (m_pDLBuf[index]==NULL) {
                ALOGW("readPCM - DL buffer is null, index:%d", index);
                m_DLBufLock.unlock();
                continue;
            }
            int64_t start_time = 0ll;

            if(m_pDLBuf[index]->meta_data()->findInt64(kKeyDLTSS, &start_time)) {
                mDLCurTime.tv_sec = start_time;
                m_pDLBuf[index]->meta_data()->findInt64(kKeyDLTSNS, &start_time);
                mDLCurTime.tv_nsec = start_time;
                m_pDLBuf[index]->meta_data()->remove(kKeyDLTSS);
                m_pDLBuf[index]->meta_data()->remove(kKeyDLTSNS);
                ALOGD("readPCM downlink start time:%lds:%ldus", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000);
            }

            int64_t time = 0ll;
            uint32_t len = 0;
            if (dlDataNeeded==size && (mDLCurTime.tv_sec>mULCurTime.tv_sec ||
            	(mDLCurTime.tv_sec==mULCurTime.tv_sec && (mDLCurTime.tv_nsec>=(mULCurTime.tv_nsec+62500))))) {
                time = (mDLCurTime.tv_sec-mULCurTime.tv_sec)*1000*1000*1000 +mDLCurTime.tv_nsec-mULCurTime.tv_nsec;
                len = (uint32_t)(time*m_SampleRate*2/1000/1000/1000);
                len = len & 0xFFFFFFFE;
                if (len>=dlDataNeeded) {
                    memset((void *)((char *)pDLBuf + dlOffset), 0, dlDataNeeded);
                    ALOGV("readPCM - DL buffer early size:%d,fill 0, size:%d, dlOffset:%d",len, dlDataNeeded, dlOffset);
                    dlOffset = 0;
                    dlDataNeeded = 0;
                    m_DLBufLock.unlock();
                    continue;
                }else {
                    memset((void *)((char *)pDLBuf + dlOffset), 0, len);
                    ALOGV("readPCM - DL buffer early size:%d,fill 0, dlOffset:%d",len, dlOffset);
                    dlDataNeeded -= len;
                    dlOffset += len;
                }

            }else if(dlDataNeeded==size && (mDLCurTime.tv_sec<mULCurTime.tv_sec ||
            	(mDLCurTime.tv_sec==mULCurTime.tv_sec && ((mDLCurTime.tv_nsec+62500)<=mULCurTime.tv_nsec)))){
                time = (mULCurTime.tv_sec-mDLCurTime.tv_sec)*1000*1000*1000 +mULCurTime.tv_nsec-mDLCurTime.tv_nsec;
                len = (uint32_t)(time*m_SampleRate*2/1000/1000/1000);
                len = len & 0xFFFFFFFE;
                ALOGV("readPCM DL buffer late drop:%d, DL buffer size:%d", len, m_pDLBuf[index]->range_length());
                if (len>=m_pDLBuf[index]->range_length()) {
                    updateTime(m_pDLBuf[index]->range_length(), &mDLCurTime);
                    ALOGV("readPCM DL buffer late time:%lds:%ldus", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000);
                    m_DLBufReaded.erase(it);
                    m_DLBufFree.push_back(index);
                    m_DLBufLock.unlock();
                    continue;
                }else {
                    m_pDLBuf[index]->set_range(m_pDLBuf[index]->range_offset()+len, m_pDLBuf[index]->range_length()-len);
                    updateTime(len, &mDLCurTime);
                    ALOGV("readPCM DL buffer late time:%lds:%ldus", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000);
                }
            }

            ALOGV("readPCM read DL data - index:%d, length:%d", index, m_pDLBuf[index]->range_length());
            if (m_pDLBuf[index]->range_length()<=dlDataNeeded) {
                memcpy((void *)((char *)pDLBuf+dlOffset), (void *)((char *)(m_pDLBuf[index]->data())+m_pDLBuf[index]->range_offset()), m_pDLBuf[index]->range_length());
                dlDataNeeded = dlDataNeeded - m_pDLBuf[index]->range_length();
                dlOffset = dlOffset + m_pDLBuf[index]->range_length();
                updateTime(m_pDLBuf[index]->range_length(), &mDLCurTime);
                m_DLBufReaded.erase(it);
                m_DLBufFree.push_back(index);
            }else {
                memcpy((void *)((char *)pDLBuf+dlOffset), (void *)((char *)(m_pDLBuf[index]->data())+m_pDLBuf[index]->range_offset()), dlDataNeeded);
                m_pDLBuf[index]->set_range(m_pDLBuf[index]->range_offset()+dlDataNeeded, m_pDLBuf[index]->range_length()-dlDataNeeded);
                updateTime(dlDataNeeded, &mDLCurTime);
                ALOGV("readPCM downlink current time:%lds:%ldus", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000);
                dlDataNeeded = 0;
            }

            m_DLBufLock.unlock();
        }else if(m_bNoDLPath&&dlDataNeeded>0) {
            ALOGW("no downlink path, or downlink path error");
            dlDataNeeded = 0;
            memset((void *)pDLBuf, 0, size);
        }
#endif  // che-jui remove
    }

    updateTime(size, &mULCurTime);
    ALOGV("readPCM uplink current time:%lds:%ldus", mULCurTime.tv_sec, mULCurTime.tv_nsec/1000);

    ALOGV("readPCM obtain lock - out -");
    dumpPCMData("uplink1", (void *)pULBuf1, size);
    if (m_ChannelCount==2) {
        dumpPCMData("uplink2", (void *)pULBuf2, size);
    }
#if 0  // che-jui remove
    dumpPCMData("downlink", (void *)pDLBuf, size);
#endif
    return OK;
}

status_t AudioStream::reset()
{
    ALOGD("reset in +");
    status_t err = OK;
    err = stop();

    if (m_pAudioSource!=0) {
        m_Lock.lock();
        m_pAudioSource.clear();
        m_pAudioSource = 0;
        m_Lock.unlock();
    }

    usleep(20000);

    if (err==OK) {
        m_Lock.lock();
        m_pAudioSource = new AudioSource(m_InputSource, String16("com.mediatek.voicecommand"), m_SampleRate, m_ChannelCount);
        m_Lock.unlock();
        if (m_pAudioSource == 0) {
            ALOGE("reset - create uplink stream fail");
            return UNKNOWN_ERROR;
        }
        err = start();
    }

    ALOGD("reset in -");
    return err;
}

int16_t AudioStream::getMaxAmplitude()
{
    int16_t pcmAmp = 0;

    if (m_pAudioSource!=0)
        pcmAmp = m_pAudioSource->getMaxAmplitude();

    return pcmAmp;
}

void AudioStream::threadFunc()
{
    ALOGV("read down link stream thread in+");
    //int32_t dataReaded = 0;
    //uint32_t index = 0;
    //List<uint32_t>::iterator it;
    //
    //while(m_bStarted&&(!m_bNoDLPath)) {
    //    if(m_DLBufFree.empty()) {
    //        Mutex::Autolock autoLock(m_Lock);
    //        int dataLenth = 0;
    //        m_DLBufLock.lock();
    //        ALOGV("Thread Loop downlink drop data obtain lock");
    //        if (m_DLBufReaded.empty()) {
    //            m_DLBufLock.unlock();
    //            ALOGV("Thread Loop m_DLBufReaded is empty");
    //            continue;
    //        }
    //        it = m_DLBufReaded.begin();
    //        index = *it;
    //        m_DLBufReaded.erase(it);
    //        m_DLBufFree.push_back(index);
    //        dataLenth = m_pDLBuf[index]->range_length();
    //        updateTime(m_pDLBuf[index]->range_length(), &mDLCurTime);
    //        ALOGV("Thread Loop downlink current time:%lds:%ldus", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000);
    //        m_DLBufLock.unlock();
    //
    //        dataLenth *= m_ChannelCount;
    //        while (dataLenth>0)
    //        {
    //            Mutex::Autolock autoLock(m_ULBufLock);
    //            if (m_pULBuf==NULL) {
    //                if (m_pAudioSource->read(&m_pULBuf)!=OK) {
    //                    break;
    //                }
    //
    //                if(mULCurTime.tv_sec==0 && mULCurTime.tv_nsec==0) {
    //                    //AudioSystem::GetVoiceUnlockULTime((void*)(&mULCurTime));//mike temp remove
    //                    mULCurTime.tv_nsec += AUDIO_DROP_FRAME_MS*1000*1000;
    //                    while(mULCurTime.tv_nsec>=1000000000) {
    //                        mULCurTime.tv_sec += 1;
    //                        mULCurTime.tv_nsec -= 1000000000;
    //                    }
    //                    ALOGD("Thread Loop uplink start time:%ld, ulDataNeeded:%ld", mULCurTime.tv_sec, mULCurTime.tv_nsec);
    //                }
    //                dumpPCMData("uplink-raw", (void *)m_pULBuf->data(), m_pULBuf->range_length());
    //            }
    //
    //            ALOGV("Thread Loop uplink drop data dataLenth:%d, uplink data:%d", dataLenth, m_pULBuf->range_length());
    //
    //            if (dataLenth<(int)m_pULBuf->range_length()) {
    //                m_pULBuf->set_range(m_pULBuf->range_offset()+dataLenth, m_pULBuf->range_length()-dataLenth);
    //                dataLenth = 0;
    //            }else{
    //                dataLenth -= m_pULBuf->range_length();
    //                m_pULBuf->release();
    //                m_pULBuf = NULL;
    //            }
    //        }
    //
    //        updateTime(m_pDLBuf[index]->range_length(), &mULCurTime);
    //        ALOGV("Thread Loop uplink current time:%lds:%ldus", mULCurTime.tv_sec, mULCurTime.tv_nsec/1000);
    //        ALOGW("DL buffer over flow, drop data, length:%d", m_pDLBuf[index]->range_length());
    //        continue;
    //    }
    //
    //    m_DLBufLock.lock();
    //    it = m_DLBufFree.begin();
    //    index = *it;
    //    m_DLBufFree.erase(it);
    //    m_DLBufLock.unlock();
    //    if (m_pDLBuf[index]==NULL) {
    //        ALOGW("DL buffer is null, index:%d", index);
    //        continue;
    //    }
    //    ALOGV("Thread Loop downlink bfore - index:%d", index);
    //    struct timespec time;
    //    dataReaded = AudioSystem::ReadRefFromRing((void *)m_pDLBuf[index]->data(), DL_BUF_SIZE, (void *)&time);
    //    ALOGV("Thread Loop downlink stream size:%d, req size:%d", dataReaded, DL_BUF_SIZE);
    //    if (dataReaded>0&&(mDLStartTime.tv_sec!=time.tv_sec || mDLStartTime.tv_nsec!=time.tv_nsec)) {
    //        m_pDLBuf[index]->meta_data()->setInt64(kKeyDLTSS, time.tv_sec);
    //        m_pDLBuf[index]->meta_data()->setInt64(kKeyDLTSNS, time.tv_nsec);
    //        mDLCurTime.tv_sec = time.tv_sec;
    //        mDLCurTime.tv_nsec = time.tv_nsec;
    //        mDLStartTime.tv_sec = time.tv_sec;
    //        mDLStartTime.tv_nsec = time.tv_nsec;
    //        m_ULReadCnt = 0;
    //        ALOGD("Thread Loop downlink start time:%lds:%ldms", mDLCurTime.tv_sec, mDLCurTime.tv_nsec/1000/1000);
    //    }
    //
    //    m_DLStandby = dataReaded<DL_BUF_SIZE ? true : false;
    //    m_DLStandby = true;//mike temp add
    //    m_DLBufLock.lock();
    //    if (dataReaded<0) {
    //        ALOGW("Can't capture down link data");
    //        m_pDLBuf[index]->set_range(0, 0);
    //        m_DLBufReaded.push_back(index);
    //        m_bNoDLPath = true;
    //    }else {
    //        m_pDLBuf[index]->set_range(0, dataReaded);
    //        m_DLBufReaded.push_back(index);
    //        ALOGV("read down link stream thread index:%d, datalen:%d", index, dataReaded);
    //        dumpPCMData("downlink-raw", (void *)m_pDLBuf[index]->data(), dataReaded);
    //    }
    //    m_DLBufLock.unlock();
    //    if (m_DLStandby) {
    //        Mutex::Autolock autoLock(m_Lock);
    //    }
    //    m_WorkState.signal();
    //    if(m_DLBufFree.empty()) {
    //        ALOGV("read down link stream sleep 10ms");
    //        usleep(10000);
    //    }
    //    ALOGV("read down link stream thread while end -");
    //}

    return;
}

void AudioStream::releaseQueuedDLBufs()
{
    ALOGV("release the queued buffers...");

    List<uint32_t>::iterator it;
    while (!m_DLBufReaded.empty()) {
        it = m_DLBufReaded.begin();
        m_DLBufFree.push_back((*it));
        ALOGV("release the queued buffers index:%d",*it);
        m_DLBufReaded.erase(it);
    }

    ALOGV("release the queued buffers out...");
}

void AudioStream::startDLPath()
{
    ALOGD("startDLPath in +");
    char value[PROPERTY_VALUE_MAX];
    property_get("vendor.voicerecognize.noDL", value, "0");
    int bflag=atoi(value);
    if (bflag) {
        m_bNoDLPath = true;
        return;
    }

    m_bNoDLPath = false;

    if (m_bDLStream) {
        //AudioSystem::startVoiceUnlockDL();//mike temp remove
        AudioSystem::setParameters(String8("VoiceFeatureOn=1"));
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
        pthread_create(&m_ThreadId, NULL, threadloop, (void *)this);
        pthread_attr_destroy(&attr);
    }
}

void AudioStream::stopDLPath()
{
    ALOGD("stopDLPath in +");

    //if (m_bDLStream) {//mike temp remove DL path
        //AudioSystem:: stopVoiceUnlockDL();
        //AudioSystem::setParameters(String8("VoiceFeatureOn=0"));
    //}

    char value[PROPERTY_VALUE_MAX];
    property_get("vendor.voicerecognize.noDL", value, "0");
    int bflag=atoi(value);
    if(!bflag) {
        pthread_join(m_ThreadId, NULL);
        ALOGD("stopDLPath wait thread exit done");
    }

}

void AudioStream::updateTime(int length, struct timespec *time)
{
    time->tv_nsec += (int64_t)length*1000*1000*1000/m_SampleRate/2;
    while(time->tv_nsec>=1000000000) {
        time->tv_sec += 1;
        time->tv_nsec -= 1000000000;
    }
}

void AudioStream::dropData()
{
    if(!m_DLStandby) {
        m_ULDataLen = 0;
        m_ULReadCnt = 0;
        return;
    }

    if((m_ULReadCnt != 0) && ((m_ULReadCnt%10) == 0)){
        struct timespec time_uplink;
        clock_gettime(CLOCK_MONOTONIC, &time_uplink);

        int dropDataLen = 0;
        int64_t time = (time_uplink.tv_sec - mULTime.tv_sec)*1000*1000 + (time_uplink.tv_nsec - mULTime.tv_nsec)/1000;
        if(time>0) {
            dropDataLen = (int)(time*m_SampleRate*m_ChannelCount*2/1000/1000) - m_ULDataLen;
            int framecount = 2*m_ChannelCount;
            dropDataLen = dropDataLen>0 ? (((dropDataLen + framecount-1) & ~(framecount-1))) : dropDataLen;
            ALOGD("dropData uplink drop data dropDataLen:%d", dropDataLen);
            int cnt = 0;
            if (dropDataLen > DROP_THRESHOLD) {
                while (cnt<10) {
                    if (m_pULBuf==NULL) {
                        if (m_pAudioSource->read(&m_pULBuf)!=OK) {
                            break;
                        }
                        dumpPCMData("uplink-raw", (void *)m_pULBuf->data(), m_pULBuf->range_length());
                    }

                    ALOGV("dropData uplink drop data dataLenth:%d, uplink data:%d", dropDataLen, m_pULBuf->range_length());

                    if (dropDataLen<(int)m_pULBuf->range_length()) {
                        m_pULBuf->set_range(m_pULBuf->range_offset()+dropDataLen, m_pULBuf->range_length()-dropDataLen);
                        updateTime(dropDataLen/m_ChannelCount, &mULCurTime);
                        ALOGV("dropData uplink current time:%lds:%ldus", mULCurTime.tv_sec, mULCurTime.tv_nsec/1000);
                        dropDataLen = 0;
                    }else{
                        dropDataLen -= m_pULBuf->range_length();
                        updateTime(m_pULBuf->range_length()/m_ChannelCount, &mULCurTime);
                         m_ULDataLen += m_pULBuf->range_length();
                        ALOGV("dropData uplink current time:%lds:%ldus", mULCurTime.tv_sec, mULCurTime.tv_nsec/1000);
                        m_pULBuf->release();
                        m_pULBuf = NULL;
                    }
                    cnt++;

                    if (m_pULBuf==NULL) {
                        if (m_pAudioSource->read(&m_pULBuf)!=OK) {
                            break;
                        }
                        dumpPCMData("uplink-raw", (void *)m_pULBuf->data(), m_pULBuf->range_length());
                    }
                }
            }

            if (dropDataLen <= 0 || m_ULDataLen > (int)0xDFFFFFFF) {
                m_ULReadCnt = 0;
                m_ULDataLen = 0;
            }
        } else {
            m_ULReadCnt = 0;
            m_ULDataLen = 0;
        }
    }

    if ((m_pULBuf==NULL)&&(m_pAudioSource->read(&m_pULBuf)!=OK)) {
        return;
    }

    if (m_ULReadCnt==0) {
        clock_gettime(CLOCK_MONOTONIC, &mULTime);
    }

    m_ULReadCnt++;
    m_ULDataLen += m_pULBuf->range_length();
}

void AudioStream::extractULData(short *pULBuf1, short *pULBuf2, short *data, int len)
{
    if (m_ChannelCount==1) {
        memcpy((void *)pULBuf1, (void *)data, len);
    }else if(m_ChannelCount==2) {
        len /= 4;
        int i = 0;
        while(len--) {
            pULBuf1[i] = data[2*i];
            pULBuf2[i] = data[2*i+1];
            i++;
        }
    }
}
