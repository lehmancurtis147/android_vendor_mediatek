ifeq ($(strip $(MTK_NFC_SUPPORT)), yes)

LOCAL_PATH:= mediatek/external/mtknfc

########################################
# MTK NFC Clock Type & Rate Configuration
########################################

ifeq ($(wildcard $(MTK_ROOT_CONFIG_OUT)/nfc.cfg),)
        PRODUCT_COPY_FILES += $(LOCAL_PATH)/nfc.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/nfc.cfg:mtk
else
        PRODUCT_COPY_FILES += $(MTK_ROOT_CONFIG_OUT)/nfc.cfg:$(TARGET_COPY_OUT_VENDOR)/etc/nfc.cfg:mtk
endif

#Copy Mifare lincense file
PRODUCT_COPY_FILES += $(LOCAL_PATH)/MTKNfclicense.lic:data/misc/MTKNfclicense.lic

endif

