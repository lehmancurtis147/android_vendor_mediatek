#ifndef __TYPEDEFS_H__
#define __TYPEDEFS_H__
#include <stdlib.h>
#include <errno.h>

#include <log/log.h>
typedef int s32;
typedef unsigned int u32;
typedef short s16;
typedef unsigned short u16;
typedef char s8;
typedef unsigned char u8;
typedef long time_t;
enum {MSG_DEBUG, MSG_INFO, MSG_WARNING, MSG_ERROR};

void wifi2agps_log(u32 level, s8* fmt, ...);
#endif
