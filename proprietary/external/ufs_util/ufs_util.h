/*
* Copyright (C) 2016 MediaTek Inc.
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See http://www.gnu.org/licenses/gpl-2.0.html for more details.
*/

#ifndef _UFS_UTIL_H
#define _UFS_UTIL_H

/*
 * Solution for firmware version query
 *
 * Solution 1 - UFS_FFU_QUERY_FW_VER_BY_STRING_DESCR:
 *   By INQUIRY data (For UFS 2.0 or later versions)
 *
 * Solution 2- UFS_FFU_QUERY_FW_VER_BY_INQUIRY_DATA:
 *   By String Descriptor (index of DEVICE_DESC_PARAM_PRL) (For UFS 2.1 or later versions)
 */

/* #define UFS_FFU_QUERY_FW_VER_BY_STRING_DESCR */
#define UFS_FFU_QUERY_FW_VER_BY_INQUIRY_DATA

#define UFS_FFU_BASE_RW_VER_BUF_SIZE   (UFS_IOCTL_FFU_MAX_FW_VER_BYTES + 1)

#define UFS_UTIL_ERR_MEM_FAIL          (128)
#define UFS_UTIL_ERR_IOCTL_FAIL        (129)
#define UFS_UTIL_ERR_FFU_NOT_SUPPORTED (130)
#define UFS_UTIL_ERR_FFU_DISABLED      (131)
#define UFS_UTIL_ERR_PARAM_ERR         (132)
#define UFS_UTIL_ERR_OPEN_DEV_FAIL     (133)
#define UFS_UTIL_ERR_OPEN_FW_FAIL      (134)
#define UFS_UTIL_ERR_FW_VER_FORMAT_ERR (135)
#define UFS_UTIL_ERR_FW_VER_TOO_LONG   (136)
#define UFS_UTIL_ERR_FW_VER_MISMATCH   (137)
#define UFS_UTIL_ERR_MANUID_FORMAT_ERR (138)
#define UFS_UTIL_ERR_MANUID_MISMATCH   (139)

struct ufs_ffu_data {
	__u8  ffu_supported;
	__u8  ffu_disabled;
	__u32 dev_manu_id;
	__u8  base_fw_ver[UFS_FFU_BASE_RW_VER_BUF_SIZE];
};

#endif /* _UFS_UTIL_H */

