#if !defined(__MRDUMP_SUPPORT_EXT4_H__)
#define __MRDUMP_SUPPORT_EXT4_H__

/* this define to enable DEBUG message of MRDUMP */
//#define MRDUMP_DEBUG

// C99
#include <inttypes.h>
#include <stdbool.h>

// FIGETBSZ
#include <linux/fs.h>

// ioctl (FIBMAP)
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/syscall.h>
#include <linux/version.h>

/*
 * statfs() and statvfs()
 * Note: bionic header didn't define EXT4_SUPER_MAGIC
 *       use EXT3_SUPER_MAGIC instead (the same)
 */
#include <sys/statfs.h>
#include <sys/statvfs.h>

/*
 * mmap, munmap - map or unmap files or devices into memory
 */
#include <sys/mman.h>

// FIEMAP
#if LINUX_VERSION_CODE >= KERNEL_VERSION(3,0,0)
    #include <linux/fiemap.h>
#else
    struct fiemap_extent {
        __u64 fe_logical;
        __u64 fe_physical;
        __u64 fe_length;
        __u64 fe_reserved64[2];
        __u32 fe_flags;
        __u32 fe_reserved[3];
    };

    struct fiemap {
        __u64 fm_start;
        __u64 fm_length;
        __u32 fm_flags;
        __u32 fm_mapped_extents;
        __u32 fm_extent_count;
        __u32 fm_reserved;
        struct fiemap_extent fm_extents[0];
    };
#endif
struct fiemap_info {
    __u32 lba;
    __u32 tot;
};

// fstat
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// Property */
#include <cutils/properties.h>
#include <sys/system_properties.h>

// MISC
#include <ctype.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <zlib.h>

#include "mrdump_support_f2fs.h"

// Variables defined
#define AE_DUMPSYS_DATA_PATH        "/data/vendor/dumpsys"
#define MRDUMP_EXT4_NEW_FILE        true
#define MRDUMP_EXT4_OLD_FILE        false
#define MRDUMP_EXT4_PARA_LBAOOO     "/sys/module/mrdump/parameters/lbaooo"
#define MRDUMP_EXT4_PROC_MOUNTS     "/proc/self/mounts"
#define MRDUMP_EXT4_MOUNT_POINT     "/data"
#define MRDUMP_EXT4_ALLOCATE_FILE   AE_DUMPSYS_DATA_PATH"/mrdump_preallocated"
#define MRDUMP_EXT4_MIN_ALLOCATE    256
#define MRDUMP_EXT4_1MB_SIZE        (1024*1024)
#define MRDUMP_EXT4_REST_SIZE       500
#define MRDUMP_EXT4_REST_SPACE      (MRDUMP_EXT4_REST_SIZE * MRDUMP_EXT4_1MB_SIZE)

// const.
#define MRDUMP_EXT4_BLKSIZE         4096
#define MRDUMP_EXT4_MAX_CONTINUE    64
#define MRDUMP_EXT4_EXSPACE         (MRDUMP_EXT4_BLKSIZE*MRDUMP_EXT4_MAX_CONTINUE)      // Expect continue space
#define MRDUMP_EXT4_LBA_PER_BLOCK   1022

/*
 * v1: support allocate size > 4G
 * v2: support timestamp
 */
#define MRDUMP_PAF_VERSION 0x0002

#define MRDUMP_PAF_INFO_LBA      4
#define MRDUMP_PAF_ADDR_LBA      8
#define MRDUMP_PAF_ALLOCSIZE    12
#define MRDUMP_PAF_COREDUMPSIZE 20
#define MRDUMP_PAF_TIMESTAMP    28
#define MRDUMP_PAF_CRC32        36
#define MRDUMP_LBA_DATAONLY     MRDUMP_PAF_CRC32
#define MRDUMP_PAF_TOTAL_SIZE   40

typedef enum {
    DEFAULT_DISABLE,
    DEFAULT_HALFMEM,
    DEFAULT_FULLMEM,
    USERDEFINED_MEM
} MRDUMP_DEFAULT_SIZE;

typedef enum {
    BDATA_STATE_CHECK_PASS,
    BDATA_STATE_FILE_ACCESS_ERROR,
    BDATA_STATE_BLOCK_HEADER_ERROR,
    BDATA_STATE_BLOCK_DATA_ERROR,
} MRDUMP_BDATA_STATE;

// for chattr
#define FS_IOC_GETFLAGS                 _IOR('f', 1, long)
#define FS_IOC_SETFLAGS                 _IOW('f', 2, long)
#define FS_SECRM_FL                     0x00000001 /* Secure deletion */
#define FS_IMMUTABLE_FL                 0x00000010 /* Immutable file */

struct mrdump_pafile_info {
    uint32_t info_lba;
    uint32_t addr_lba;
    uint64_t filesize;
    uint64_t coredump_size;
    uint64_t timestamp;
};

struct __attribute__((__packed__)) marked_block_data {
    uint32_t lba;
    uint64_t zero_padding[510];
    uint64_t timestamp;
    uint32_t crc;
};

#define DATA_OS_NONE 0
#define DATA_OS_EXT4 1
#define DATA_OS_F2FS 2

/* Function Prototypes */
//fiemap
int mrdump_fiemap_get_entry_lba(int fd, unsigned int blksize, unsigned int rows);
int mrdump_fiemap_get_entry_tot(int fd, unsigned int blksize, unsigned int rows);
unsigned int mrdump_fiemap_get_lba_of_block(struct fiemap_info *myinfo, unsigned int num, unsigned int block);
unsigned int mrdump_fiemap_total_entries(int fd);
bool mrdump_fiemap_get_entries(int fd, unsigned int blksize, struct fiemap_info *mapinfo, unsigned int rows);

// Public api
int mrdump_get_data_os(void);
void mrdump_file_set_maxsize(int mrdump_size);
/*
 * Setup mrdump pre-allocated file
 * reset : true to reset coredump header
 */
void mrdump_file_setup(bool reset);
bool mrdump_file_get_info(const char *allocfile, struct mrdump_pafile_info *info);
bool mrdump_file_fetch_zip_coredump(const char *outfile);

/* Function Prototypes */
extern bool mount_as_f2fs(const char *mountp);
extern bool f2fs_fallocate(const char *allocfile, uint64_t allocsize);

#endif /* __MRDUMP_SUPPORT_EXT4_H__ */
