# Copyright Statement:
#
# This software/firmware and related documentation ("MediaTek Software") are
# protected under relevant copyright laws. The information contained herein
# is confidential and proprietary to MediaTek Inc. and/or its licensors.
# Without the prior written permission of MediaTek inc. and/or its licensors,
# any reproduction, modification, use or disclosure of MediaTek Software,
# and information contained herein, in whole or in part, shall be strictly prohibited.

# MediaTek Inc. (C) 2010. All rights reserved.
#
# BY OPENING THIS FILE, RECEIVER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
# THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
# RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO RECEIVER ON
# AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
# NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
# SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
# SUPPLIED WITH THE MEDIATEK SOFTWARE, AND RECEIVER AGREES TO LOOK ONLY TO SUCH
# THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. RECEIVER EXPRESSLY ACKNOWLEDGES
# THAT IT IS RECEIVER'S SOLE RESPONSIBILITY TO OBTAIN FROM ANY THIRD PARTY ALL PROPER LICENSES
# CONTAINED IN MEDIATEK SOFTWARE. MEDIATEK SHALL ALSO NOT BE RESPONSIBLE FOR ANY MEDIATEK
# SOFTWARE RELEASES MADE TO RECEIVER'S SPECIFICATION OR TO CONFORM TO A PARTICULAR
# STANDARD OR OPEN FORUM. RECEIVER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND
# CUMULATIVE LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
# AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
# OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY RECEIVER TO
# MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
# The following software/firmware and/or related documentation ("MediaTek Software")
# have been modified by MediaTek Inc. All revisions are subject to any receiver's
# applicable license agreements with MediaTek Inc.

## ==> build this lib only when hdmi is support

ifeq ($(MTK_HDMI_SUPPORT),yes)

LOCAL_PATH:= $(call my-dir)

################ HDMI CLIENT ##################
include $(CLEAR_VARS)
# Add for MTK HDMI
ifeq ($(strip $(MTK_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_SUPPORT
endif
ifeq ($(strip $(MTK_HDMI_HDCP_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_HDCP_SUPPORT
endif
ifeq ($(strip $(MTK_INTERNAL_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_INTERNAL_HDMI_SUPPORT
endif
ifeq ($(strip $(MTK_ALPS_BOX_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_ALPS_BOX_SUPPORT
endif
# Add for MTK HDMI end
LOCAL_SRC_FILES += \
    IMtkHdmiService.cpp \
    IMtkHdmiClient.cpp

LOCAL_MODULE:= libhdmi
#LOCAL_PROPRIETARY_MODULE :=true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES+= \
    $(TOP)/vendor/mediatek/proprietary/external/hdmi/include

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    liblog \
    libutils \
    libbinder

include $(BUILD_SHARED_LIBRARY)

################## HDMI SERVICE #####################
include $(CLEAR_VARS)
# Add for MTK HDMI
ifeq ($(strip $(MTK_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_SUPPORT
endif
ifeq ($(strip $(MTK_HDMI_HDCP_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_HDMI_HDCP_SUPPORT
endif
ifeq ($(strip $(MTK_DRM_KEY_MNG_SUPPORT)), yes)
  ifeq ($(strip $(MTK_IN_HOUSE_TEE_SUPPORT)),yes)
    LOCAL_CFLAGS += -DMTK_DRM_KEY_MNG_SUPPORT
  endif
endif
ifeq ($(strip $(MTK_INTERNAL_HDMI_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_INTERNAL_HDMI_SUPPORT

endif
ifeq ($(strip $(MTK_MT8193_SUPPORT)), yes)
  LOCAL_CFLAGS += -DHDMI_MT8193_SUPPORT
endif

ifeq ($(strip $(MTK_ALPS_BOX_SUPPORT)), yes)
  LOCAL_CFLAGS += -DMTK_ALPS_BOX_SUPPORT
endif
# Add for MTK HDMI end
LOCAL_SRC_FILES:= \
    MtkHdmiService.cpp \
    event/hdmi_event.cpp

LOCAL_SHARED_LIBRARIES := \
    libcutils \
    libutils \
    libbinder \
    libhdmi \
    liblog

LOCAL_PRELINK_MODULE := false
LOCAL_MODULE:= libhdmiservice
#LOCAL_PROPRIETARY_MODULE :=true
LOCAL_MODULE_OWNER := mtk

LOCAL_C_INCLUDES:= \
    $(TOP)/vendor/mediatek/proprietary/external/hdmi/include

ifeq ($(strip $(MTK_PLATFORM)), MT8173)
LOCAL_C_INCLUDES += \
    $(TOP)/device/mediatek/mt8173/kernel-headers
else ifeq ($(strip $(MTK_PLATFORM)), MT8167)
LOCAL_C_INCLUDES += \
    $(TOP)/device/mediatek/mt8167/kernel-headers
else ifeq ($(strip $(MTK_PLATFORM)), MT6735)
  ifeq ($(strip $(MTK_MT8193_SUPPORT)), yes)
    LOCAL_C_INCLUDES += \
        $(TOP)/device/mediatek/common/kernel-headers
  endif
else ifeq ($(strip $(MTK_PLATFORM)), MT6771)
LOCAL_C_INCLUDES += \
    $(TOP)/device/mediatek/common/kernel-headers
else ifeq ($(strip $(MTK_PLATFORM)), MT8163)
LOCAL_C_INCLUDES += \
    $(TOP)/device/mediatek/mt8163/kernel-headers
endif

ifeq ($(strip $(MTK_DRM_KEY_MNG_SUPPORT)), yes)
  ifeq ($(strip $(MTK_IN_HOUSE_TEE_SUPPORT)),yes)
    LOCAL_C_INCLUDES += \
        $(TOP)/vendor/mediatek/proprietary/external/trustzone/mtee/include/tz_cross \
        $(MTK_PATH_SOURCE)/hardware/keymanage/1.0 \
        $(MTK_PATH_SOURCE)/external/km_lib/drmkey/ \
        $(MTK_PATH_SOURCE)/hardware/meta/common/inc/
  endif
    LOCAL_SHARED_LIBRARIES += libcutils libnetutils libc
  ifeq ($(strip $(MTK_IN_HOUSE_TEE_SUPPORT)),yes)
#    LOCAL_SHARED_LIBRARIES += libtz_uree liburee_meta_drmkeyinstall_v2
    LOCAL_SHARED_LIBRARIES += libhidlbase libhidltransport
    LOCAL_SHARED_LIBRARIES += vendor.mediatek.hardware.keymanage@1.0
    LOCAL_STATIC_LIBRARIES += vendor.mediatek.hardware.keymanage@1.0-util
  endif
endif
include $(BUILD_SHARED_LIBRARY)

#################### HDMI JAVA API #######################
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_MODULE := hdmimanager
LOCAL_MODULE_CLASS := JAVA_LIBRARIES
#LOCAL_PROPRIETARY_MODULE :=true
#LOCAL_MODULE_OWNER := mtk

LOCAL_SRC_FILES += \
      $(call all-java-files-under,java)

include $(BUILD_JAVA_LIBRARY)

include $(call all-makefiles-under,$(LOCAL_PATH))
endif
