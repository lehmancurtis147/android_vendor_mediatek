/****************************************************************************************/
/*  Copyright (c) 2004-2017 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/****************************************************************************************/
/*                                                                                      */
/*     $Author: nxp39687 $                                                              */
/*     $Revision: 104038 $                                                              */
/*     $Date: 2017-03-10 19:04:55 +0530 (Fri, 10 Mar 2017) $                            */
/*                                                                                      */
/****************************************************************************************/

/**
@file
Header file for the application layer interface of the LVVE module.
This files includes all definitions, types, structures and function prototypes
required by the calling layer. All other types, structures and functions are
private.
*/

#ifndef __LVVE_H__
#define __LVVE_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

/****************************************************************************************/
/*                                                                                      */
/*  Includes                                                                            */
/*                                                                                      */
/****************************************************************************************/

#include "LVVE_VID.h"

/****************************************************************************************/
/*                                                                                      */
/*  Definitions                                                                         */
/*                                                                                      */
/****************************************************************************************/
#define LVVE_MAX_PCM_FRAME_SIZE         480                    ///< LVVE maximum processing frame size

#define LVVE_MIN_LPF_TX_CORNER_HZ_NB    3500                    ///< Min corner frequency for TX-LPF NB
#define LVVE_MIN_LPF_TX_CORNER_HZ_WB    7000                    ///< Min corner frequency for TX-LPF WB
#define LVVE_MAX_LPF_TX_CORNER_HZ_NB    3900                    ///< Max corner frequency for TX-LPF NB
#define LVVE_MAX_LPF_TX_CORNER_HZ_WB    7800                    ///< Max corner frequency for TX-LPF WB
#define LVVE_MAX_LPF_TX_CORNER_24KHZ    11700                   ///< Max corner frequency for TX-LPF at 24kHz
#define LVVE_MAX_LPF_TX_CORNER_32KHZ    15600                   ///< Max corner frequency for TX-LPF at 32kHz
#define LVVE_MAX_LPF_TX_CORNER_48KHZ    23400                   ///< Max corner frequency for TX-LPF at 48kHz
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT_8K       3700        ///< default corner frequency for TX-LPF at 8kHz
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT_16K      7400        ///< default corner frequency for TX-LPF at 16kHz
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT_24K      11100       ///< default corner frequency for TX-LPF at 24kHz
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT_32K      14800       ///< default corner frequency for TX-LPF at 32kHz
#define LVVE_TX_MIC_LPF_CORNERFREQ_DEFAULT_48K      22200       ///< default corner frequency for TX-LPF at 48kHz

/* Minimum and maximum values for the Tx channel count */
#define LVVE_TX_INPUT_CHANNEL_COUNT_MIN (1) ///< Minimum number of channels for the LVVE_Tx input
#define LVVE_TX_INPUT_CHANNEL_COUNT_MAX (3) ///< Maximum number of channels for the LVVE_Tx input
#define LVVE_TX_OUTPUT_CHANNEL_COUNT_MIN (1) ///< Minimum number of channels for the LVVE_Tx output
#define LVVE_TX_OUTPUT_CHANNEL_COUNT_MAX (1) ///< Maximum number of channels for the LVVE_Tx output
#define LVVE_TX_REFERENCE_CHANNEL_COUNT_MIN (1) ///< Minimum number of channels for the LVVE_Tx echo reference
#define LVVE_TX_REFERENCE_CHANNEL_COUNT_MAX (2) ///< Maximum number of channels for the LVVE_Tx echo reference
#define LVVE_TX_REFERENCE_FIFO_LENGTH_MAX ((LVM_INT32) (LVM_MAXINT_32/sizeof(LVM_INT16))) ///< this is max allowed size of the reference FIFO "ReferenceFIFOLength" (expressed in number of frames)

/* Minimum and maximum values for the Rx channel count */
#define LVVE_RX_INPUT_CHANNEL_COUNT_MIN (1) ///< Minimum number of channels for the LVVE_Rx input
#define LVVE_RX_INPUT_CHANNEL_COUNT_MAX (1) ///< Maximum number of channels for the LVVE_Rx input
#define LVVE_RX_OUTPUT_CHANNEL_COUNT_MIN (1) ///< Minimum number of channels for the LVVE_Rx output
#define LVVE_RX_OUTPUT_CHANNEL_COUNT_MAX (1) ///< Maximum number of channels for the LVVE_Rx output

#define LVVE_NOISESAMPLES_PER_FRAME     LVVE_MAX_PCM_FRAME_SIZE     ///< Number of samples required for noise estimation per frame. Temporary version of LVAVC, corresponds to NLMS out
#ifndef LVVE_NOISESAMPLES_PER_FRAME
#define LVVE_NOISESAMPLES_PER_FRAME 0
#endif

/**
@def LVVE_RX_PRESET_LENGTH
Length of the LVVE_Rx preset buffer (number of bytes).
@see LVVE_Rx_SetPreset
@ingroup LVVE_Rx
*/
#define LVVE_RX_PRESET_LENGTH (LVVIDHEADER_CONTROLPARAMS_LVWIREFORMAT_LENGTH + LVVE_RX_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< RX preset Buffer length

/**
@def LVVE_TX_PRESET_LENGTH
Length of the LVVE_Tx preset buffer (number of bytes).
@see LVVE_Tx_SetPreset
@ingroup LVVE_Tx
*/
#define LVVE_TX_PRESET_LENGTH (LVVIDHEADER_CONTROLPARAMS_LVWIREFORMAT_LENGTH + LVVE_TX_CONTROLPARAMS_LVWIREFORMAT_LENGTH) ///< TX preset buffer length


/* Deprecated Definitions */

#define LVVE_MAX_BULK_DELAY             LVVE_TX_BULKDELAY_MAX
#define LVVE_RX_MODE_DUMMY LVVE_RX_MODE_EN_DUMMY
#define LVVE_TX_MODE_DUMMY LVVE_TX_MODE_EN_DUMMY

#define LVVE_MAX_VOL_GAIN_DB            LVVE_TX_VOL_GAIN_MAX
#define LVVE_MIN_VOL_GAIN_DB            LVVE_TX_VOL_GAIN_MIN

#define LVVE_MAX_HPF_CORNER_HZ          LVVE_TX_MIC_HPF_CORNERFREQ_MAX
#define LVVE_MIN_HPF_CORNER_HZ          LVVE_TX_MIC_HPF_CORNERFREQ_MIN

#define LVVE_MAX_HPF_RX_CORNER_HZ       LVVE_RX_HPF_CORNERFREQ_MAX

#define LVVE_MAX_NF_HZ                  LVNF_NF_FREQUENCY_MAX
#define LVVE_MIN_NF_HZ                  LVNF_NF_FREQUENCY_MIN
#define LVVE_MAX_NF_ATT_DB              LVNF_NF_ATTENUATION_MAX
#define LVVE_MIN_NF_ATT_DB              LVNF_NF_ATTENUATION_MIN
#define LVVE_MAX_QFACTOR                LVNF_NF_QFACTOR_MAX
#define LVVE_MIN_QFACTOR                LVNF_NF_QFACTOR_MIN

#define LVVE_MAX_DRC_LEVEL              LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMAX
#define LVVE_MIN_DRC_LEVEL              LVDRC_COMPRESSORCURVEINPUTLEVELS_ARRAYMIN
#define LVVE_MIN_DRC_NUMKNEES           LVDRC_NUMKNEES_MIN
#define LVVE_MAX_DRC_NUMKNEES           LVDRC_NUMKNEES_MAX
#define LVVE_MIN_DRC_ATTACKTIME         LVDRC_ATTACKTIME_MIN
#define LVVE_MAX_DRC_ATTACKTIME         LVDRC_ATTACKTIME_MAX
#define LVVE_MIN_DRC_RELEASETIME        LVDRC_RELEASETIME_MIN
#define LVVE_MAX_DRC_RELEASETIME        LVDRC_RELEASETIME_MAX

/* End deprecated defines*/
/****************************************************************************************/
/*                                                                                      */
/*  Types                                                                               */
/*                                                                                      */
/****************************************************************************************/

/**
LVVE_Tx Instance Handle
This handle is used by most of the LVVE APIs
@see LVVE_Tx_GetInstanceHandle
@ingroup LVVE_Tx
*/
typedef void *LVVE_Tx_Handle_t;   ///< LVVE Tx handle
/**
This handle is used by most of the LVVE APIs
@see LVVE_Rx_GetInstanceHandle
@ingroup LVVE_Rx
*/
typedef void *LVVE_Rx_Handle_t;  ///< LVVE Rx handle

/**
This enum type specifies the different error codes returned by the API functions
For the exact meaning see the individual function descriptions
*/
typedef enum
{
    LVVE_SUCCESS                               = 0,                ///< Successful return from a routine
    LVVE_ALIGNMENTERROR                        = 1,                ///< Memory alignment error
    LVVE_NULLADDRESS                           = 2,                ///< NULL allocation address
    LVVE_OUTOFRANGE                            = 3,                ///< Out of range parameter
    LVVE_INVALID_FRAME_COUNT                   = 4,                ///< Invalid number of frames
    LVVE_INVALID_ALGORITHM_CONFIGURATION       = 5,                ///< Mutually exclusive algorithms configured ON
    LVVE_INVALID_STATE_CONFIGURATION           = 6,                ///< Invalid state of the algorithm
    LVVE_PRESET_INVALID_BUFFER_LENGTH          = 7,                ///< Incorrect length of buffer used in SetPreset
    LVVE_PRESET_INVALID_VOLUME_INDEX           = 8,                ///< The volume index exceeds the buffer content in SetPreset
    LVVE_PRESET_INVALID_BUFFER_VERSION         = 9,                ///< The version of the preset buffer does not match this library
    LVVE_PRESET_INVALID_BASELINE_VERSION       = 10,               ///< Invalid LVVE Baseline Version in preset buffer
    LVVE_PRESET_INVALID_MASK                   = 11,               ///< Invalid algorithm mask in preset buffer
    LVVE_PRESET_INVALID_SAMPLE_RATE            = 12,               ///< Invalid sample rate @ref LVM_Fs_en in preset buffer
    LVVE_PRESET_INVALID_LVWIREFORMAT_MESSAGEID = 13,               ///< Invalid @ref LVVIDHeader_MessageID_en wire format message id in preset buffer
    LVVE_REFERENCE_FIFO_UNDERFLOW              = 14,               ///< Reference FIFO does not have sufficient frames
    LVVE_REFERENCE_FIFO_OVERFLOW               = 15,               ///< Reference FIFO has overwritten some frames
    LVVE_RETURNSTATUS_DUMMY                    = LVM_MAXENUM
} LVVE_ReturnStatus_en;

/**
Byte array containing encoded LVVE_Rx_ControlParams for one or multiple volumes.
The length of this array should be a multiple of @ref LVVE_RX_PRESET_LENGTH.
@see LVVE_Rx_SetPreset
@ingroup LVVE_Rx
*/
typedef LVM_CHAR *LVVE_Rx_Preset_t; ///< LVVE Rx preset buffer

/**
Byte array containing encoded LVVE_Tx_ControlParams for one or multiple volumes.
The length of this array should be a multiple of @ref LVVE_TX_PRESET_LENGTH.
@see LVVE_Tx_SetPreset
@ingroup LVVE_Tx
*/
typedef LVM_CHAR *LVVE_Tx_Preset_t;  ///< LVVE Tx preset buffer

/****************************************************************************************/
/*                                                                                      */
/*  Structures                                                                          */
/*                                                                                      */
/****************************************************************************************/

/**
The instance parameters define certain important operating limits required by the calling application.
These instance parameters affect how much memory is required by the LVVE and hence must be provided
when the instance is created.
@see LVVE_Rx_GetMemoryTable
@see LVVE_Rx_GetInstanceHandle
@see LVVE_Tx_GetMemoryTable
@see LVVE_Tx_GetInstanceHandle
*/
/**
EQ Module Instance Parameters Structure.
@see LVEQ_InstanceParams_st
*/
typedef struct
{
/**
Max Size of Equalizer
Sets the maximum length of the equalizer impulse response that can be used.
It must be a multiple of 8.
<table border>
<caption>EQ Max Length Table</caption>
   <tr>
       <td><b>Unit</b></td>
       <td><b>Q format</b></td>
       <td><b>Data Range</b></td>
       <td><b>Default Values</b></td>
   </tr>
   <tr>
       <td><b>Integer Length in Samples</b></td>
       <td><b>Q16.0</b></td>
       <td>[8,\ref LVEQ_EQ_LENGTH_MAX]</td>
       <td>\ref LVEQ_EQ_LENGTH_DEFAULT</td>
   </tr>
</table>
*/
    LVM_UINT16                    EQ_MaxLength;  ///< EQ Max Length
} LVEQ_InstanceParams_st;

/**
Tx Instance Parameter Structure
These parameters are set at initialization time and may not be changed during processing
@ref LVVE_Tx_GetInstanceHandle
@ingroup LVVE_Tx
*/
typedef struct
{
    LVM_Fs_en                   SampleRate;      ///< Sample rate
/**
Sets the maximum length of the bulk delay between Rx and Tx expressed in frames.
The unit of MaxBulkDelay is [number of frames] at the respective sampling rate.
<table border>
<caption>Max Bulk Delay Table</caption>
   <tr>
       <td><b>Type</b></td>
       <td><b>Unit</b></td>
       <td><b>Q format</b></td>
       <td><b>Data Range</b></td>
       <td><b>Default Values</b></td>
   </tr>
   <tr>
       <td><b>LVM_UINT16</b></td>
       <td><b>Integer Length in frames</b></td>
       <td><b>Q16.0</b></td>
       <td>[0,6400]</td>
       <td>None</td>
   </tr>
</table>
*/
    LVM_UINT16                  MaxBulkDelay;     ///< Max bulk delay
/**
Sets the LVVE_Tx reference FIFO length. This parameter affects memory requirements of the LVVE_Tx
module.
For footprint constrained integrations, we want to keep the size as small as possible to
limit the penalty and static memory requirements and use the optimal size which depends on the block
size used in the integration. For integrations where the calls to \ref LVVE_Tx_Process and
\ref LVVE_Tx_PushReference are asynchronous, the size of the Reference FIFO needs to be larger: it
needs to be sufficiently large to prevent \ref LVVE_REFERENCE_FIFO_UNDERFLOW and
\ref LVVE_REFERENCE_FIFO_OVERFLOW from happening. In most integrations, the AEC Reference is
available at the same time as the microphone buffers. In this case the best way is to call the
PushReference and Process functions synchronously and alternating (with LVVE_Tx_PushReference API
being the first function). In this case, the optimal size of the Reference FIFO is equal to the
maximal block size with which LVVE_Tx_PushReference and LVVE_Tx_Process APIs are called in the
respective integration. If the echo reference is not provided in the integration
(e.g. echo cancellation/suppression is not required), the ReferenceFIFOLength can be set to zero.
<table border>
<caption>Reference FIFO Length</caption>
   <tr>
       <td><b>Type</b></td>
       <td><b>Unit</b></td>
       <td><b>Q format</b></td>
       <td><b>Data Range</b></td>
       <td><b>Default Values</b></td>
   </tr>
   <tr>
       <td><b>LVM_INT32</b></td>
       <td><b>Number of Frames</b></td>
       <td><b>Q31.0</b></td>
       <td>[0,\ref LVVE_TX_REFERENCE_FIFO_LENGTH_MAX]</td>
       <td>None</td>
   </tr>
</table>

*/
    LVM_INT32                  ReferenceFIFOLength; ///< Length of the reference FIFO buffer (specified in number of frames)

    LVEQ_InstanceParams_st      EQ_InstParams;  ///< EQ instance
} LVVE_Tx_InstanceParams_st;

/**
Rx Instance Parameter Structure
These parameters are set at initialization time and may not be changed during processing.
@ref LVVE_Rx_GetInstanceHandle
@ingroup LVVE_Rx
*/
typedef struct
{
    LVM_Fs_en                   SampleRate;            ///< LVVE sample rate
    LVEQ_InstanceParams_st      EQ_InstParams;///< EQ instance

} LVVE_Rx_InstanceParams_st;

/**
The version information structure contains one character field to store LVVE version number.
A call to the @ref LVVE_GetVersionInfo function is needed to retrieve this information.
*/
typedef struct
{
    LVM_CHAR                    VersionNumber[64];  ///< Version number of the LifeVibes&trade; VoiceExperience library
} LVVE_VersionInfo;

/**
Used to reset LVVE_Rx module any time.
@ref LVM_ResetType_en parameter determine the type of reset required
@ingroup LVVE_Rx
*/
typedef struct
{
    LVM_ResetType_en ResetType;  ///< RX Reset Type
} LVVE_Rx_ResetParams_st;

/**
Used to reset the LVVE_Tx module any time.
LVM_ResetType_en parameter determine the type of reset required
@ingroup LVVE_Tx
*/
typedef struct
{
    LVM_ResetType_en ResetType;   ///< TX Reset Type
} LVVE_Tx_ResetParams_st;

/**
Used to communicate an information between Tx and Rx instances.
*/
typedef LVM_CHAR** LVVE_Tx2RxMessage_Handle_t;

/**
@brief Retrieve the memory requirements of the LVVE_Tx module.

This function returns a table of memory records that describe size, type and memory space of all
buffers required by the instance. The number of initialized memory records is defined by
LVVE_NR_MEMORY_REGIONS. This function is used for two purposes and is called in two different ways:

@li Memory Allocation: When the LVVE_GetMemoryTable functions are called with a NULL instance handle
    (hInstance = LVM_NULL) the function returns the memory requirements. The base address pointers
    in the memory table are set to NULL. All elements of the instance parameters structure (pointed
    to by pInstParams) must contain valid values as the memory requirements are affected by these
    settings.

@li Memory Free: When called with a non-NULL instance handle (hInstance = a valid instance handle)
      the function returns the memory table used when the instance was created.
      The base address pointers returned will be those supplied by the calling application when the
      memory was allocated and can now be used for freeing memory. The instance parameters (pointed
      to by pInstParams) are ignored and the pInstParams parameter may be set to NULL.
@li In case of memory allocation, all elements of the parameter initialization structure defined by
    pInstParams must contain valid values as the memory requirements are affected by these settings.
    In some releases of the bundle library one or more memory regions may have a zero size.

@pre The number of memory records in the array defined by pMemoryTable is equal to
     @ref LVM_NR_MEMORY_REGIONS.
@pre Exactly @ref LVM_NR_MEMORY_REGIONS memory records of pMemoryTable are initialized.

@post When this function is called with hInstance = NULL the memory base address pointers
will be NULL on return.
@post When the function is called for freeing memory, hInstance = Instance Handle the
memory table returns the allocated memory and base addresses used during
initialisation.

@return  LVVE_SUCCESS           when the function call succeeds and the memory table is filled
                                correctly
@return  LVVE_NULLADDRESS       when pMemoryTable was NULL
@return  LVVE_NULLADDRESS       when pInstParams was NULL and the call was for memory allocation
                                is NULL.
@return  LVVE_OUTOFRANGE        when pInstParams contains parameters out of the excepted range

@note  This function may be interrupted by the LVVE_Tx_Process function.

@ingroup LVVE_Tx

LVVE_Tx_GetMemoryTable: Memory Allocation Example:

The following example shows how to get the memory requirements for the LVVE_Tx instance.
\code
    InstParams_Tx.SampleRate    = LVM_FS_8000;
    // Include the other instance params here

    LVVE_Status = LVVE_Tx_GetMemoryTable ( LVM_NULL,
                                           &MemTab_Tx,
                                           &InstParams_Tx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }

    for( c1 = 0; c1 < LVVE_NR_MEMORY_REGIONS; c1++ )
    {
        if( MemTab_Tx.Region[c1].Size != 0 )
        {
            MemTab_Tx.Region[c1].pBaseAddress =
                malloc(MemTab_Tx.Region[c1].Size);
        }
    }
\endcode

LVVE_Tx_GetMemoryTable: Freeing Memory Example:

The following example shows how to free the memory from the LVVE_Tx instance.

\code
    LVVE_Status = LVVE_Tx_GetMemoryTable( hInstance,
                                              &MemTab_Tx,
                                              LVM_NULL);

        if (LVVE_Status != LVVE_SUCCESS)
        {
            // Handle errors
        }

        for( c1 = 0; c1 < LVVE_NR_MEMORY_REGIONS; c1++)
        {
            if (MemTab_Tx.Region[c1].Size != 0)
            {
                free(MemTab_Tx.Region[c1].pBaseAddress);
            }
        }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Tx_GetMemoryTable(LVVE_Tx_Handle_t               hInstance,
                                            LVM_MemoryTable_st             *pMemoryTable,
                                            const LVVE_Tx_InstanceParams_st      *pInstanceParams);

/**
@brief Retrieve the memory requirements of the LVVE_Rx module.

This function returns a table of memory records that describe size, type and memory space of all
buffers required by the instance. The number of initialized memory records is defined by
LVVE_NR_MEMORY_REGIONS. This function is used for two purposes and is called in two different ways:

@li Memory Allocation: When the LVVE_GetMemoryTable functions are called with a NULL instance
    handle (hInstance = LVM_NULL) the function returns the memory requirements. The base address
    pointers in the memory table are set to NULL. All elements of the instance parameters structure
    (pointed to by pInstParams) must contain valid values as the memory requirements are affected by
    these settings.

@li Memory Free: When called with a non-NULL instance handle (hInstance = a valid instance handle)
      the function returns the memory table used when the instance was created.
      The base address pointers returned will be those supplied by the calling application when the
      memory was allocated and can now be used for freeing memory. The instance parameters
      (pointed to by pInstParams) are ignored and the pInstParams parameter may be set to NULL.
@li In case of memory allocation, all elements of the parameter initialization structure defined by
    pInstParams must contain valid values as the memory requirements are affected by these settings.
    In some releases of the bundle library one or more memory regions may have a zero size.

@pre The number of memory records in the array defined by pMemoryTable is equal to
     @ref LVM_NR_MEMORY_REGIONS.
@pre Exactly @ref LVM_NR_MEMORY_REGIONS memory records of pMemoryTable are initialized.

@post When this function is called with hInstance = NULL the memory base address pointers
will be NULL on return.
@post When the function is called for freeing memory, hInstance = Instance Handle the
memory table returns the allocated memory and base addresses used during
initialisation.

@return  LVVE_SUCCESS           when the function call succeeds and the memory table is filled correctly
@return  LVVE_NULLADDRESS       when pMemoryTable was NULL
@return  LVVE_NULLADDRESS       when pInstParams was NULL and the call was for memory allocation
                                is NULL.
@return  LVVE_OUTOFRANGE        when pInstParams contains parameters out of the excepted range

@note  This function may be interrupted by the LVVE_Rx_Process function.

@ingroup LVVE_Rx

LVVE_Rx_GetMemoryTable: Memory Allocation Example:

The following example shows how to get the memory requirements for the LVVE_Rx instance.

\code
    InstParams_Rx.SampleRate    = LVM_FS_8000;
    // Include the other instance params here

    LVVE_Status = LVVE_Rx_GetMemoryTable ( LVM_NULL,
                                           &MemTab_Rx,
                                           &InstParams_Rx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }

    for( c1 = 0; c1 < LVVE_NR_MEMORY_REGIONS; c1++ )
    {
        if( MemTab_Rx.Region[c1].Size != 0 )
        {
            MemTab_Rx.Region[c1].pBaseAddress =
                malloc(MemTab_Rx.Region[c1].Size);
        }
    }
\endcode

LVVE_Rx_GetMemoryTable: Freeing Memory Example:

The following example shows how to free the memory from the LVVE_Rx instance.

\code
    LVVE_Status = LVVE_Rx_GetMemoryTable( hInstance,
                                              &MemTab_Rx,
                                              LVM_NULL);

        if (LVVE_Status != LVVE_SUCCESS)
        {
            // Handle errors
        }

        for( c1 = 0; c1 < LVVE_NR_MEMORY_REGIONS; c1++)
        {
            if (MemTab_Rx.Region[c1].Size != 0)
            {
                free(MemTab_Rx.Region[c1].pBaseAddress);
            }
        }
\endcode
*/

LVVE_ReturnStatus_en LVVE_Rx_GetMemoryTable(LVVE_Rx_Handle_t               hInstance,
                                            LVM_MemoryTable_st             *pMemoryTable,
                                            const LVVE_Rx_InstanceParams_st      *pInstanceParams);


/**
@brief Created handle to the instance of the LVVE_Tx module

This function is used to create the LVVE_Tx instance. All control parameters are set to invalid
values. The memory table pointed to by pMemoryTable must be created. If the memory table is not
correct then an error will be returned. The memory requirements of the Rx and Tx unit are dependent
on the instance parameters supplied and so the instance parameters provided in this function call
must be the same as those used in the @ref LVVE_Tx_GetMemoryTable function calls used for memory
allocation.

@pre The memory records tables defined by pMemoryTable contains pointers to non-overlapping buffers
with the size as requested via the prior calls to the LVVE_Tx_GetMemoryTable functions.
@pre The initialization parameter structure defined by pInstParams is identical to the structure
      passed to prior call to @ref LVVE_Tx_GetMemoryTable functions.


@param  phInstance              Pointer to the instance handle.
@param  pMemoryTable            Pointer to the memory definition table.
@param  pInstanceParams         Pointer to the instance parameters.

@return  LVVE_SUCCESS           when creation was successful
@return  LVVE_NULLADDRESS       When phInstance or pMemoryTable or pInstanceParams is NULL.
@return  LVVE_NULLADDRESS       when one or more of the memory regions with a non-zero size has been
                                given a NULL base address pointer.
@return LVVE_OUTOFRANGE         when pInstParams contains parameters out of the excepted range

@ingroup LVVE_Tx

 LVVE_Tx_GetInstanceHandle Example:

The following example shows how to initialize LVVE_Tx_GetInstanceHandle.

\code
    hInstance_Tx = LVM_NULL;
    LVVE_Status = LVVE_Tx_GetInstanceHandle( &hInstance_Tx,
                                             &MemTab_Tx,
                                             &InstParams_Tx );
    if( LVVE_Status != LVVE_SUCCESS )
    {
         // Handle Errors
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Tx_GetInstanceHandle(LVVE_Tx_Handle_t                *phInstance,
                                               LVM_MemoryTable_st              *pMemoryTable,
                                               LVVE_Tx_InstanceParams_st       *pInstanceParams);

/**
@brief Created handle to the instance of the LVVE_Rx module

This functions is used to create LVVE_Rx instance. All control parameters are set to invalid values.
The memory table pointed to by pMemoryTable must be created. If the memory table is not correct then
an error will be returned. The memory requirements of the Rx and Rx unit are dependent on the
instance parameters supplied and so the instance parameters provided in this function call must be
the same as those used in the @ref LVVE_Rx_GetMemoryTable function calls used for memory allocation.

@pre The memory records tables defined by pMemoryTable contains pointers to non-overlapping buffers
with the size as requested via the prior calls to the LVVE_Rx_GetMemoryTable functions.
@pre The initialization parameter structure defined by pInstParams is identical to the structure
     passed to prior call to @ref LVVE_Rx_GetMemoryTable functions.


@param  phInstance              Pointer to the instance handle.
@param  pMemoryTable            Pointer to the memory definition table.
@param  pInstanceParams         Pointer to the instance parameters.

@return  LVVE_SUCCESS           when creation was successful
@return  LVVE_NULLADDRESS       When phInstance or pMemoryTable or pInstanceParams is NULL.
@return  LVVE_NULLADDRESS       when one or more of the memory regions with a non-zero size has been
                                given a NULL base address pointer.
@return LVVE_OUTOFRANGE         when pInstParams contains parameters out of the excepted range

@ingroup LVVE_Rx

 LVVE_Rx_GetInstanceHandle Example:

The following example shows how to initialize LVVE_Rx_GetInstanceHandle.

\code
    hInstance_Rx = LVM_NULL;
    LVVE_Status = LVVE_Rx_GetInstanceHandle( &hInstance_Rx,
                                             &MemTab_Rx,
                                             &InstParams_Rx );
    if( LVVE_Status != LVVE_SUCCESS )
    {
         // Handle Errors
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Rx_GetInstanceHandle(LVVE_Rx_Handle_t                *phInstance,
                                               LVM_MemoryTable_st              *pMemoryTable,
                                               LVVE_Rx_InstanceParams_st       *pInstanceParams);

/**
@brief Retrieve the current LVVE_Tx control parameters.

This function copies the control parameters from the LVVE_Tx into the supplied
parameter structure, pControlParams. The values returned are the values given in the last successful
call to the LVVE_Tx_SetControlParameters function.

@param hInstance              Instance handle
@param pControlParams         Pointer to the control parameters

@pre   hInstance should be valid handle.
@pre   pControlParams should be allocated by caller.
@post  pControlParams will be filled with the values given in the last successful call to
       the LVVE_Tx_SetControlParameters function. They are not necessarily the values
       used in the last call to the LVVE_Tx_Process function, this will be the case if
       LVVE_Tx_SetControlParameters has been called since the last call to LVVE_Tx_Process.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pControlParams is NULL

@note The LVVE_Tx_GetControlParameters function can be called at any time during processing.

@ingroup LVVE_Tx

LVVE_Tx_GetControlParameters Example:

The following example shows how to get different control parameters for the LVVE_Tx instance.

\code
    LVVE_Status = LVVE_Tx_GetControlParameters( &hInstance_Tx,
                                                &ControlParams_Tx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
         // Handle Errors
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Tx_GetControlParameters(   LVVE_Tx_Handle_t           hInstance,
                                                     LVVE_Tx_ControlParams_st   *pControlParams);

/**
@brief Retrieve the current LVVE_Rx control parameters.

This function copies the control parameters from the LVVE_Tx into the supplied
parameter structure, pControlParams. The values returned are the values given in the last successful
call to the LVVE_Rx_SetControlParameters function.

@param hInstance              Instance handle
@param pControlParams         Pointer to the control parameters

@pre   hInstance should be valid handle.
@pre   pControlParams should be allocated by caller.
@post  pControlParams will be filled with the values given in the last successful call to
       the LVVE_Rx_SetControlParameters function. They are not necessarily the values
       used in the last call to the LVVE_Rx_Process function, this will be the case if
       LVVE_Tx_SetControlParameters has been called since the last call to LVVE_Tx_Process.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pControlParams is NULL

@note The LVVE_Rx_GetControlParameters function can be called at any time during processing.

@ingroup LVVE_Rx

LVVE_Rx_GetControlParameters Example:

The following example shows how to get different control parameters for the LVVE_Rx instance.

\code
    LVVE_Status = LVVE_Rx_GetControlParameters( &hInstance_Rx,
                                                &ControlParams_Rx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
         // Handle Errors
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Rx_GetControlParameters(   LVVE_Rx_Handle_t           hInstance,
                                                     LVVE_Rx_ControlParams_st   *pControlParams);

/**
@brief Sets or changes the LVVE_Tx module parameters using C-structure.

This function takes the new set of parameters and makes a local copy within
the LVVE_Tx but the parameters are only applied on the next call of the LVVE_Tx_Process
function. When a parameter is unchanged no action is taken. This function can
be called at any time during the processing, even when the LVVE_Tx_Process function
is running. LifeVibes&trade; VoiceExperience control parameters can be set using two methods.
Control Parameters can be populated in a \"C\" style structure (as explained in example) and then
sent to the library.
Another method is to populate control parameters in a byte array called Preset Buffer,
see @ref LVVE_Tx_SetPreset for example usage.

@param hInstance               Instance Handle
@param pNewParams              Pointer to a parameter structure

@pre hInstance should be valid handle.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pNewParams is NULL
@return LVVE_OUTOFRANGE       When pNewParams contains parameters of activated modules that are out
                              of the excepted range
@return LVVE_INVALID_ALGORITHM_CONFIGURATION  When two mutual exclusive algorithms are both
                                              configured ON at run-time(e.g., NoiseVoid and
                                              HandsFree)


@note This function may be interrupted by the LVVE_Tx_Process function

@ingroup LVVE_Tx

LVVE_Tx_GetControlParameters: Setting Control Parameters using C-Style structure Example:

The following example shows how to set different control parameters for the LVVE_Tx instance.

\code
    LVVE_Status = LVVE_Tx_GetControlParameters( &hInstance_Tx,
                                                &ControlParams_Tx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }

     Change Parameters as required
    ControlParams_Tx.VOL_Gain         = VOL_TABLE[i];
    ControlParams_Tx.VC_ControlParams = VC_TABLE[i];

    // Update instance with new parameters
    LVVE_Status = LVVE_Tx_SetControlParameters( &hInstance_Tx,
                                                &ControlParams_Tx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Tx_SetControlParameters(   LVVE_Tx_Handle_t           hInstance,
                                        const LVVE_Tx_ControlParams_st * const pNewParams);

/**
@brief Sets or changes the LVVE_Rx module parameters using C-structure.

This function takes the new set of parameters and makes a local copy within
LVVE_Rx but the parameters are only applied on the next call of the LVVE_Rx_Process
function. When a parameter is unchanged no action is taken. This function can
be called at any time during the processing, even when the LVVE_Rx_Process function
is running. LifeVibes&trade; VoiceExperience control parameters can be set using two methods.
Control Parameters can be populated in a \"C\" style structure (as explained in example) and then
sent to the library.
Another method is to populate control parameters in a byte array called Preset Buffer,
see @ref LVVE_Rx_SetPreset for example usage.

@param hInstance               Instance Handle
@param pNewParams              Pointer to a parameter structure

@pre hInstance should be valid handle.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pNewParams is NULL
@return LVVE_OUTOFRANGE       When pNewParams contains parameters of activated modules that are out
                              of the excepted range
@return LVVE_INVALID_ALGORITHM_CONFIGURATION  When two mutual exclusive algorithms are both
                                              configured ON at run-time(e.g., NoiseVoid and
                                              HandsFree)


@note This function may be interrupted by the LVVE_Rx_Process function

@ingroup LVVE_Rx

LVVE_Rx_GetControlParameters: Setting Control Parameters using C-Style structure Example:

The following example shows how to set different control parameters for the LVVE_Rx instance.

\code
    LVVE_Status = LVVE_Rx_GetControlParameters( &hInstance_Rx,
                                                &ControlParams_Rx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }

     Change Parameters as required
    ControlParams_Rx.VOL_Gain         = VOL_TABLE[i];
    ControlParams_Rx.VC_ControlParams = VC_TABLE[i];

    // Update instance with new parameters
    LVVE_Status = LVVE_Rx_SetControlParameters( &hInstance_Rx,
                                                &ControlParams_Rx );

    if( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_SetControlParameters(   LVVE_Rx_Handle_t           hInstance,
                                        const LVVE_Rx_ControlParams_st * const pNewParams);

/**
@brief Retrieve the current the LVVE_Tx status parameters in LVWireFormat.

This function copies internal status variables into the supplied byte buffer of size
@ref LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH. This function enables the user to dump
status parameters in byte form to a file or stream for analysis by another program. The
created byte stream is platform independent.

If you want to analyze the content of the status parameters within the same program,
it is probably easier to use @ref LVVE_Tx_GetStatusParameters.

@param hInstance            Instance handle
@param pStatusParamsBuffer  Pointer to the destination buffer for status parameters
@param BufferSize           Number of bytes available in the allocated pStatusParamsBuffer

@pre   hInstance should be valid handle.
@pre   pStatusParamsBuffer should be allocated by caller with size @ref LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH.
@post  pStatusParamsBuffer will be filled with the latest status values of the LVVE_Tx.

@return @ref LVVE_SUCCESS          Succeeded
@return @ref LVVE_NULLADDRESS      When hInstance or pStatusParamsBuffer is NULL
@return @ref LVVE_OUTOFRANGE       When the supplied buffer size is insufficient

@note The function can be called at any moment after instantiation.
This function can run in different threads than the process functions.

@ingroup LVVE_Tx

The following example shows how to use LVVE_Tx_GetStatusParametersWireFormat function call.

\code
    LVM_CHAR Tx_MonitorBuffer[LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH];
    LVVE_ReturnStatus_en Status = LVVE_Tx_GetStatusParametersWireFormat(hInstance_Tx, Tx_MonitorBuffer, LVVE_TX_STATUSPARAMS_LVWIREFORMAT_LENGTH);
    if (Status != LVVE_SUCCESS)
    {
       // Error Handling
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_GetStatusParametersWireFormat(
    const LVVE_Tx_Handle_t hInstance,
    LVM_CHAR* pStatusParamsBuffer,
    LVM_INT32 BufferSize);

/**
@brief Retrieves the current the LVVE_Tx status parameters.

This function copies internal status variables into the supplied status
parameter structures.

If you want to analyze the content of the status parameters inside another program, the
@ref LVVE_Tx_GetStatusParametersWireFormat function provides the easiest way to stream
the status parameters to a file or memory.

@param hInstance      Instance handle
@param pStatusParams  Pointer to the status parameters

@pre   hInstance should be valid handle.
@pre   pStatusParams should be allocated by caller.
@post  pStatusParams will be filled with the latest status values of the LVVE_Tx.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pStatusParams is NULL

@note The LVVE_Tx_GetStatusParameters function can be called at any time during processing.
This function can run in different threads than the process functions.


@ingroup LVVE_Tx

LVVE_Tx_GetStatusParameters: Get Internal Algorithm Status Attributes Example:
The following example shows how to use LVVE_Tx_GetStatusParameters function call.

\code
    LVVE_Tx_StatusParams_st  Tx_Monitor;
    Status=LVVE_Tx_GetStatusParameters(hInstance_Tx,&Tx_Monitor);
    if (Status != LVVE_SUCCESS )
    {
       // Error Handling
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Tx_GetStatusParameters(    LVVE_Tx_Handle_t           hInstance,
                                                     LVVE_Tx_StatusParams_st    *pStatusParams);

/**
@brief Retrieve the current the LVVE_Rx status parameters in LVWireFormat.

This function copies internal status variables into the supplied byte buffer of size
@ref LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH. This function enables the user to dump
status parameters in byte form to a file or stream for analysis by another program. The
created byte stream is platform independent.

If you want to analyze the content of the status parameters within the same program,
it is probably easier to use @ref LVVE_Rx_GetStatusParameters.

@param hInstance            Instance handle
@param pStatusParamsBuffer  Pointer to the destination buffer for status parameters
@param BufferSize           Number of bytes available in the allocated pStatusParamsBuffer

@pre   hInstance should be valid handle.
@pre   pStatusParamsBuffer should be allocated by caller with size @ref LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH.
@post  pStatusParamsBuffer will be filled with the latest status values of the LVVE_Rx.

@return @ref LVVE_SUCCESS          Succeeded
@return @ref LVVE_NULLADDRESS      When hInstance or pStatusParamsBuffer is NULL
@return @ref LVVE_OUTOFRANGE       When the supplied buffer size is insufficient

@note The function can be called at any moment after instantiation.
This function can run in different threads than the process functions.

@ingroup LVVE_Rx

The following example shows how to use LVVE_Rx_GetStatusParametersWireFormat function call.

\code
    LVM_CHAR Rx_MonitorBuffer[LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH];
    LVVE_ReturnStatus_en Status = LVVE_Rx_GetStatusParametersWireFormat(hInstance_Rx, Rx_MonitorBuffer, LVVE_RX_STATUSPARAMS_LVWIREFORMAT_LENGTH);
    if (Status != LVVE_SUCCESS)
    {
       // Error Handling
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_GetStatusParametersWireFormat(
    const LVVE_Rx_Handle_t hInstance,
    LVM_CHAR* pStatusParamsBuffer,
    LVM_INT32 BufferSize);


/**
@brief Retrieves the current LVVE_Rx status parameters.

This function copies internal status variables into the supplied status
parameter structures.

If you want to analyze the content of the status parameters inside another program, the
@ref LVVE_Rx_GetStatusParametersWireFormat function provides the easiest way to stream
the status parameters to a file or memory.

@param hInstance      Instance handle
@param pStatusParams  Pointer to the status parameters

@pre   hInstance should be valid handle.
@pre   pStatusParams should be allocated by caller.
@post  pStatusParams will be filled with the latest status values of LVVE_Rx.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pStatusParams is NULL

@note The LVVE_Rx_GetStatusParameters function can be called at any time during processing.
This function can run in different threads than the process functions.

@ingroup LVVE_Rx

LVVE_Rx_GetStatusParameters: Get Internal Algorithm Status Attributes Example:
The following example shows how to use LVVE_Rx_GetStatusParameters function call.

\code
    LVVE_Rx_StatusParams_st  Rx_Monitor;
    Status=LVVE_Rx_GetStatusParameters(hInstance_Rx,&Rx_Monitor);
    if (Status != LVVE_SUCCESS )
    {
       // Error Handling
    }
\endcode

*/

LVVE_ReturnStatus_en LVVE_Rx_GetStatusParameters(    LVVE_Rx_Handle_t           hInstance,
                                                     LVVE_Rx_StatusParams_st    *pStatusParams);

/**
@brief Resets the the LVVE_Tx module.

The LVVE_Tx module instance memory contains data which depend on the input
samples that have been processed previously.  These data are buffers
used for filter tabs and delay lines and also adaptive coefficients of the
algorithm.  The LVVE_Tx_ResetInstance function resets this input dependent data.

The LVVE_Tx_ResetInstance function should be called whenever there is a
discontinuity in the input audio stream.  A discontinuity means that the
current block of samples is not contiguous with the previous block of samples.
Examples are
@li Calling the LVVE_Tx_Process function after a period of inactivity
@li Buffer underrun or overflow in the audio driver
The LVVE_ResetInstance function of the Tx and/or Tx Unit should only be called when absolutely
necessary as re-adaptation of internal algorithms will occur.
The LVVE_ResetInstance functions can be called at any time, even when LVVE_Process is running. The
reset will be applied at the start of the next call of the LVVE_Tx_Process function.


@param hInstance               Instance Handle
@param pResetParams            Reset Type

@pre hInstance should be valid handle.

@post Depending on the ResetType value, the LVVE_Tx_ResetInstance function can
perform:
@li Soft reset: partial reset of internal buffers and adaptive behavior. To be used for very short
interruption or discontinuity, e.g., buffer under-run or overflow in the audio driver influencing
the echo path delay.
@li Hard reset: full reset of all internal buffers and adaptive behavior. To be used for long
interruption or discontinuity, e.g., before calling the LVVE_Tx_Process function after a long period
of inactivity in between calls in case instance was not freed.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pResetParams is NULL
@return LVVE_OUTOFRANGE       When content of pResetParams is invalid

@note This function may be interrupted by the LVVE_Tx_Process function

@ingroup LVVE_Tx

LVVE_Tx_ResetInstance Example:

The following example shows how to use LVVE_Tx_ResetInstance.
\code
    LVVE_Status = LVVE_Tx_ResetInstance(hInstance, pResetParams);

    if (LVVE_Status != LVVE_SUCCESS)
    {
        // Handle LVVE_Tx_ResetInstance errors
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_ResetInstance(  LVVE_Tx_Handle_t        hInstance,
                                             const LVVE_Tx_ResetParams_st *pResetParams  );

/**
@brief Resets the LVVE_Rx module.

The LVVE_Rx module instance memory contains data which depend on the input
samples that have been processed previously.  These data are buffers
used for filter tabs and delay lines and also adaptive coefficients of the
algorithm.  The LVVE_Rx_ResetInstance function resets this input dependent data.

The LVVE_Rx_ResetInstance function should be called whenever there is a
discontinuity in the input audio stream.  A discontinuity means that the
current block of samples is not contiguous with the previous block of samples.
Examples are
@li Calling the LVVE_Rx_Process function after a period of inactivity
@li Buffer underrun or overflow in the audio driver
The LVVE_ResetInstance function of the Rx and/or Tx Unit should only be called when absolutely
necessary as re-adaptation of internal algorithms will occur.
The LVVE_ResetInstance functions can be called at any time, even when LVVE_Process is running. The
reset will be applied at the start of the next call of the LVVE_Rx_Process function.


@param hInstance               Instance Handle
@param pResetParams            Reset Type

@pre hInstance should be valid handle.

@post Depending on the ResetType value, the LVVE_Rx_ResetInstance function can
perform:
@li Soft reset: partial reset of internal buffers and adaptive behavior. To be used for very short
interruption or discontinuity, e.g., buffer under-run or overflow in the audio driver influencing
the echo path delay.
@li Hard reset: full reset of all internal buffers and adaptive behavior. To be used for long
interruption or discontinuity, e.g., before calling the LVVE_Rx_Process function after a long period
of inactivity in between calls in case instance was not freed.

@return LVVE_SUCCESS          Succeeded
@return LVVE_NULLADDRESS      When hInstance or pResetParams is NULL
@return LVVE_OUTOFRANGE       When content of pResetParams is invalid

@note This function may be interrupted by the LVVE_Rx_Process function

@ingroup LVVE_Rx

LVVE_Rx_ResetInstance Example:

The following example shows how to use LVVE_Rx_ResetInstance.
\code
    LVVE_Status = LVVE_Rx_ResetInstance(hInstance, pResetParams);

    if (LVVE_Status != LVVE_SUCCESS)
    {
        // Handle LVVE_Rx_ResetInstance errors
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_ResetInstance(  LVVE_Rx_Handle_t        hInstance,
                                             const LVVE_Rx_ResetParams_st *pResetParams  );

/**
\brief get the version number of current of LVVE library.

This function returns, to host, current version of the LVVE library
@param  pVersion                version info

@return  LVVE_SUCCESS           Succeeded
@return  LVVE_NULLADDRESS       When one of hInstance, pInData or pOutData is NULL

LVVE_GetVersionInfot Example:

\code
    //Get the version information
    LVVE_VersionInfo     VersionInfo;
    LVVE_GetVersionInfo(&VersionInfo);
\endcode

*/
LVVE_ReturnStatus_en LVVE_GetVersionInfo( LVVE_VersionInfo *pVersion );

/**
@brief Sets or changes the LVVE_Tx module parameters through a Preset Buffer.

Sets or changes the LVVE_Tx module parameters through a Preset Buffer. To set parameters using
\"C\" structure see @ref LVVE_Tx_SetControlParameters.
This function takes the new set of parameters and makes a local copy within the
LVVE_Tx module but the parameters are only applied on the next call of the LVVE_Tx_Process
function. When no parameters are changed, no action is taken. This function can
be called at any time during the processing, even when the LVVE_Tx_Process function
is running.
The new parameters are supplied in the form a @ref LVVE_Tx_Preset_t (Byte array).
The length of the byte array shall be a multiple of @ref LVVE_TX_PRESET_LENGTH.

@param hInstance               Instance Handle
@param pPreset                 Pointer to a Preset buffer
@param PresetLength            Length in bytes of the Preset buffer
@param VolumeIndex             Volume index to be selected from the Preset buffer

@pre hInstance should be valid handle.

@return @ref LVVE_SUCCESS          Succeeded
@return @ref LVVE_NULLADDRESS      When hInstance or pPreset is NULL
@return @ref LVVE_OUTOFRANGE       When PresetLength or any of the new parameters is out of range
@return @ref LVVE_INVALID_ALGORITHM_CONFIGURATION When mutually exclusive algorithms are enabled in
                                                  the parameters at the same time
@return @ref LVVE_PRESET_INVALID_BUFFER_LENGTH When the length of the input buffer is smaller than
                                               LVVE_TX_PRESET_LENGTH
@return @ref LVVE_PRESET_INVALID_VOLUME_INDEX When the volume index exceeds the buffer content
@return @ref LVVE_PRESET_INVALID_BUFFER_VERSION When the version of the format of the preset buffer
                                                does not match the version of the library
@return @ref LVVE_PRESET_INVALID_BASELINE_VERSION When the baseline version of the provided preset
                                                  buffer and the baseline version of the library do
                                                  not match (excluding PatchBaselineVersion). Is the
                                                  preset generated for this library ?
@return @ref LVVE_PRESET_INVALID_MASK When the algorithm mask of the provided preset buffer and the
                                      algorithm mask of the library do not match. Is the preset
                                      generated for this library ?
@return @ref LVVE_PRESET_INVALID_SAMPLE_RATE When the sample rate @ref LVM_Fs_en of the preset does
                                             not match the sample rate of the hInstance
@return @ref LVVE_PRESET_INVALID_LVWIREFORMAT_MESSAGEID  When the LVWireformat message id
                                                         @ref LVVIDHeader_MessageID_en of the preset
                                                         is not valid

@note This function may be interrupted by the LVVE_Tx_Process function

@ingroup LVVE_Tx
LVVE_Tx_SetPreset: Setting Control Parameters using Preset Buffer Example:

The following example shows how to set different control parameters for the LVVE_Tx instance using
Preset Buffer.

\code
    LVM_CHAR LVVE_Tx_Preset_Buffer[LVVE_TX_PRESET_LENGTH];
    LVVE_Tx_Preset_t const LVVE_Tx_Preset = LVVE_Tx_Preset_Buffer;

    FILE *PRESETFILE_Tx;
    LVM_INT32 ReadBytes = 0;

    PRESETFILE_Tx=fopen(filename, "rb" );

    // Read the preset buffer
    ReadBytes = fread( LVVE_Tx_Preset_Buffer,
                       sizeof(LVM_CHAR),
                       (LVM_UINT32)LVVE_TX_PRESET_LENGTH,
                       PRESETFILE_Tx );

    fclose(PRESETFILE_Tx);

     // Update instance with new preset buffer
    LVVE_Status = LVVE_Tx_SetPreset( hInstance_Tx,
                                     LVVE_Tx_Preset,
                                     ReadBytes,
                                     0);

    if ( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }
\endcode



*/
LVVE_ReturnStatus_en LVVE_Tx_SetPreset(LVVE_Tx_Handle_t hInstance,
                                       const LVVE_Tx_Preset_t pPreset,
                                       LVM_UINT32 PresetLength,
                                       LVM_UINT16 VolumeIndex);

/**
@brief Sets or changes the LVVE_Rx module parameters through a Preset Buffer

Sets or changes the LVVE_Rx module parameters through a Preset Buffer. To set parameters using
\"C\" structure see @ref LVVE_Rx_SetControlParameters.
This function takes the new set of parameters and makes a local copy within the
LVVE_Rx module but the parameters are only applied on the next call of the LVVE_Rx_Process
function. When no parameters are changed, no action is taken. This function can
be called at any time during the processing, even when the LVVE_Rx_Process function
is running.
The new parameters are supplied in the form a @ref LVVE_Rx_Preset_t (Byte array).
The length of the byte array shall be a multiple of @ref LVVE_RX_PRESET_LENGTH.

@param hInstance               Instance Handle
@param pPreset                 Pointer to a Preset buffer
@param PresetLength            Length in bytes of the Preset buffer
@param VolumeIndex             Volume index to be selected from the Preset buffer

@pre hInstance should be valid handle.

@return @ref LVVE_SUCCESS          Succeeded
@return @ref LVVE_NULLADDRESS      When hInstance or pPreset is NULL
@return @ref LVVE_OUTOFRANGE       When PresetLength or any of the new parameters is out of range
@return @ref LVVE_INVALID_ALGORITHM_CONFIGURATION When mutually exclusive algorithms are enabled in
                                                  the parameters at the same time
@return @ref LVVE_PRESET_INVALID_BUFFER_LENGTH When the length of the input buffer is smaller than
                                               LVVE_RX_PRESET_LENGTH
@return @ref LVVE_PRESET_INVALID_VOLUME_INDEX When the volume index exceeds the buffer content
@return @ref LVVE_PRESET_INVALID_BUFFER_VERSION When the version of the format of the preset buffer
                                                does not match the version of the library
@return @ref LVVE_PRESET_INVALID_BASELINE_VERSION When the baseline version of the provided preset
                                                  buffer and the baseline version of the library do
                                                  not match (excluding PatchBaselineVersion). Is the
                                                  preset generated for this library ?
@return @ref LVVE_PRESET_INVALID_MASK When the algorithm mask of the provided preset buffer and the
                                      algorithm mask of the library do not match. Is the preset
                                      generated for this library ?
@return @ref LVVE_PRESET_INVALID_SAMPLE_RATE When the sample rate @ref LVM_Fs_en of the preset does
                                             not match the sample rate of the hInstance
@return @ref LVVE_PRESET_INVALID_LVWIREFORMAT_MESSAGEID  When the LVWireformat message id
                                                         @ref LVVIDHeader_MessageID_en of the preset
                                                         is not valid

@note This function may be interrupted by the LVVE_Rx_Process function

@ingroup LVVE_Rx

LVVE_Rx_SetPreset: Setting Control Parameters using Preset Buffer Example:

The following example shows how to set different control parameters for the LVVE_Rx instance using
Preset Buffer.

\code
    #define NUM_VOLUMES (5)
    LVM_CHAR LVVE_Rx_Preset_Buffer[NUM_VOLUMES*LVVE_RX_PRESET_LENGTH];
    LVVE_Rx_Preset_t const LVVE_Rx_Preset = LVVE_Rx_Preset_Buffer;

    FILE *PRESETFILE_Rx;
    LVM_INT32 ReadBytes = 0;
    LVM_UINT16 VolumeIndex = 3;

    PRESETFILE_Rx=fopen(filename, "rb" );

    // Read the preset buffer
    ReadBytes = fread( LVVE_Rx_Preset_Buffer,
                       sizeof(LVM_CHAR),
                       (LVM_UINT32)(NUM_VOLUMES*LVVE_RX_PRESET_LENGTH),
                       PRESETFILE_Rx );

    fclose(PRESETFILE_Rx);

     // Update instance with new preset buffer
    LVVE_Status = LVVE_Rx_SetPreset( hInstance_Rx,
                                     LVVE_Rx_Preset,
                                     ReadBytes,
                                     VolumeIndex);

    if ( LVVE_Status != LVVE_SUCCESS )
    {
        // Handle Errors
    }
\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_SetPreset(LVVE_Rx_Handle_t hInstance,
                                       const LVVE_Rx_Preset_t pPreset,
                                       LVM_UINT32 PresetLength,
                                       LVM_UINT16 VolumeIndex);


/**
@brief The Tx instance publishes an opaque message to one or several Rx instance.

This function is part of a communication mechanism between one or several Tx instance(s) and one
or several Rx instance(s). Algorithms processing Rx PCM data could require to be communicated Tx
PCM data related information, for example the noise estimate of the Tx PCM data, which corresponds
to the ambient noise of the user.
So, LVVE_Tx_PublishTx2RxMessage is called after \ref LVVE_Tx_Process for a given instance and
published a message to one or several Rx instance.

@param  hInstance               Tx Instance handle
@param  pMessage                Pointer to the message to be published
@param  pMessageSize            Pointer to the size of the message

@pre hInstance should be valid handle.
@pre pMessage should be a valid pointer pointing to an allocated memory of
at least \ref LVVE_Tx2RxMessage_GetMaxSize
@pre \ref LVVE_Tx_Process should have been called once before calling this function

@post pMessage contains the message to be transfered to the Rx instance waiting for
the corresponding information
@post pMessageSize indicates the actual size of the message being published

@return @ref  LVVE_SUCCESS                  Succeeded
@return @ref  LVVE_NULLADDRESS              When one of hInstance or pointer to Message is NULL
@return @ref  LVVE_OUTOFRANGE               When the size of the message to publish exceed the maximum authorized size.

For usage of LVVE_Tx_PublishTx2RxMessage please see \ref LVVE_Tx_Process code example

@ingroup LVVE_Tx

*/
LVVE_ReturnStatus_en LVVE_Tx_PublishTx2RxMessage(LVVE_Tx_Handle_t hInstance,
                                    LVVE_Tx2RxMessage_Handle_t pMessage,
                                    LVM_INT32* pMessageSize);

/**
@brief The Rx instance consumes an opaque message sent from a Tx instance.

This function is part of a communication mechanism between one or several Tx instance(s) and one
or several Rx instance(s). Algorithms processing Rx PCM data could require to be communicated Tx
PCM data related information, for example the noise estimate of the Tx PCM data, which corresponds
to the ambient noise of the user.
So, LVVE_Rx_ConsumeTx2RxMessage is called before \ref LVVE_Rx_Process for a given instance and
consumes a message published by one Tx instance.

@param  hInstance               Rx Instance handle
@param  pMessage                Pointer to the message to be consumed
@param  MessageSize             Size of the message

@pre hInstance should be valid handle.
@pre pMessage should be a valid message of size smaller or equal to \ref LVVE_Tx2RxMessage_GetMaxSize
@pre \ref LVVE_Rx_Process should be called only after this function

@return @ref  LVVE_SUCCESS                  Succeeded
@return @ref  LVVE_NULLADDRESS              When one of hInstance or the Message pointer is NULL
@return @ref  LVVE_OUTOFRANGE               When the message is larger then expected or not of a know type
@return @ref  LVVE_INVALID_STATE_CONFIGURATION  When there is no AEC active on the Tx audio path
i.e, neither LVHF nor LVNV are set to LVM_MODE_ON.

For usage of LVVE_Rx_ConsumeTx2RxMessage please see \ref LVVE_Rx_Process code example

@ingroup LVVE_Rx

*/
LVVE_ReturnStatus_en LVVE_Rx_ConsumeTx2RxMessage(LVVE_Rx_Handle_t hInstance,
                                    LVVE_Tx2RxMessage_Handle_t pMessage,
                                    LVM_INT32 MessageSize);


/**
@brief Returns the maximum size of a message between a Tx and a Rx instance for the integrating
framework to do proper memory allocation

This function is part of a communication mechanism between one or several Tx instance(s) and one
or several Rx instance(s). Algorithms processing Rx PCM data could require to be communicated Tx
PCM data related information, for example the noise estimate of the Tx PCM data, which corresponds
to the ambient noise of the user.
So, LVVE_Tx2RxMessage_GetMaxSize returns the maximum size for the message.

@return  value                  maximum size of the message in bytes.

For usage of LVVE_Tx2RxMessage_GetMaxSize please see \ref LVVE_Rx_Process code example

@ingroup LVVE_Rx

*/

LVM_INT32 LVVE_Tx2RxMessage_GetMaxSize( void );


/**\
LVM_PCM_Channel_t represents an array of audio samples. The audio samples are linear PCM samples and
monoaural. The samples are represented as LVM_INT16. The array does not contain multi-channel data.
@ingroup LVVE_Tx
*/
typedef LVM_INT16* LVM_PCM_Channel_t; ///< Array of linear PCM audio frames

/**
The audio buffer LVM_PCM_Buffer_st structure containing frames from multiple channels.
@ingroup LVVE_Tx
*/
typedef struct
{
/**
Designates number of frames in the audio buffer. All the channels should contain the same
number of samples and number of samples in each of the channel should be equal to FrameCount.
*/
    LVM_INT32 FrameCount; ///< Total number of frames stored in the buffer
/**
Array of \ref LVM_PCM_Channel_t mono non-interleaved audio channels used to channelize data in or
out of LVVE. In other words, the channels are structured as an array of arrays of audio samples.
*/
    LVM_PCM_Channel_t* channels; ///< Array of audio channels
} LVM_PCM_Buffer_st;


/**
@brief Pushes the echo reference signal into the Reference FIFO of the LVVE_Tx instance

Writes reference audio frames into the reference FIFO which is consumed by \ref LVVE_Tx_Process
function. Ideally reference FIFO should have audio frames at least equal to number of audio frames
for which \ref LVVE_Tx_Process function is called, otherwise \ref LVVE_Tx_Process would report
\ref LVVE_REFERENCE_FIFO_UNDERFLOW. It is recommended to call \ref LVVE_Tx_Process and
\ref LVVE_Tx_PushReference alternately with the same number of frames to avoid underflow or
overflow of the Reference FIFO.

@param  hInstance               Instance handle
@param  pPlaybackBuffer         Pointer to LVM_PCM_Buffer_st which contains reference data

@pre hInstance should be valid handle.
@pre LVVE_Tx_SetControlParameters should be called successfully once before the first call to
     LVVE_Tx_PushReference
@pre pPlaybackBuffer is filled with the input samples to process.

@return  LVVE_SUCCESS                  Succeeded
@return  LVVE_NULLADDRESS              When one of hInstance or any of the buffer in
                                       pPlaybackBuffer is NULL
@return  LVVE_REFERENCE_FIFO_OVERFLOW  When reference FIFO has overwritten older data

For usage of LVVE_Tx_PushReference please see \ref LVVE_Tx_Process code example

@ingroup LVVE_Tx

*/
LVVE_ReturnStatus_en LVVE_Tx_PushReference(LVVE_Tx_Handle_t hInstance,
                                         const LVM_PCM_Buffer_st *pPlaybackBuffer);


/**
@brief Processes a buffer of audio frames by the LVVE_Tx module.

This function processes the audio frames as configured with the current
parameter settings. If new parameters have been given since the last call to \ref LVVE_Tx_Process
then these will be applied at the beginning of this process call. The frame count of
\ref LVM_PCM_Buffer_st of the output buffer is filled in by this function. The number of frames in
the output is the same as in the input signal. When an error occurs in LVVE_Tx_Process API, output
FrameCount in the pOutputbuffer is set to zero. The FrameCount should take on values as listed
in the table.
<table border>
   <tr>
       <td><b>Unit</b></td>
       <td><b>Q format</b></td>
       <td><b>Data Range</b></td>
       <td><b>Sample Rate</b></td>
       <td><b>Default Values</b></td>
   </tr>
   <tr>
       <td rowspan=5><b>Number of Frames</b></td>
       <td rowspan=5><b>Q31.0</b></td>
       <td>Integer multiple of 80</td>
       <td>8kHz</td>
       <td>80</td>
   </tr>
   <tr>
       <td>Integer multiple of 160</td>
       <td>16kHz</td>
       <td>160</td>
   </tr>
   <tr>
       <td>Integer multiple of 240</td>
       <td>24kHz</td>
       <td>240</td>
   </tr>
   <tr>
       <td>Integer multiple of 320</td>
       <td>32kHz</td>
       <td>320</td>
   </tr>
   <tr>
       <td>Integer multiple of 480</td>
       <td>48kHz</td>
       <td>480</td>
   </tr>
</table>

@param hInstance                Instance handle
@param pInputBuffer             Pointer to input buffer of audio frames
@param pOutputBuffer            Pointer to output buffer of audio frames

@pre hInstance should be valid handle.
@pre LVVE_Tx_SetControlParameters should be called successfully once before the first call to
     LVVE_Tx_Process
@pre InputBuffer is filled with the input samples to process
@pre LVVE_Tx_PushReference function should be called before LVVE_Tx_Process function to make sure
     successful the LVVE_Tx module operation. LVVE_Tx_PushReference and LVVE_Tx_Process functions
     are candidates to be called in one thread to make sure they are always synchronized.

@post pOutData contains the processed samples and number of frames of output

@return LVVE_SUCCESS                     Succeeded
@return LVVE_NULLADDRESS                 When hInstance, pInData or pOutData is NULL
@return LVVE_INVALID_FRAME_COUNT         When the NumSamples is outside the allowed range
@return LVVE_INVALID_STATE_CONFIGURATION When invalid state of the algorithm found
@return LVVE_REFERENCE_FIFO_UNDERFLOW    When reference buffer does not have sufficient data.
                                         In this case the LVVE_Tx processes the data normally
                                         just giving warning of under flow happened
@ingroup LVVE_Tx

LVVE_Tx_Process Example:
\code

#define MAX_BLK_SIZE 80 // This number is sample rate dependent and should meet requirements as
                        // defined in @LVM_PCM_Buffer_st for FrameCount
#define TX_NUMCHANNELS_IN (4)
#define TX_NUMCHANNELS_OUT (1)
#define TX_NUMCHANNELS_REFERENCE (1)

LVM_INT16   InBuffer16_Tx_Channel_0[MAX_BLK_SIZE];     // Mono, 16-bit input buffer
LVM_INT16   InBuffer16_Tx_Channel_1[MAX_BLK_SIZE];     // Mono, 16-bit input buffer
LVM_INT16   InBuffer16_Tx_Channel_2[MAX_BLK_SIZE];     // Mono, 16-bit input buffer
LVM_INT16   InBuffer16_Tx_Channel_3[MAX_BLK_SIZE];     // Mono, 16-bit input buffer
LVM_INT16   InBuffer16_Tx_ReferenceData[MAX_BLK_SIZE]; // Mono, 16-bit input buffer
LVM_INT16   OutBuffer16_Channel[MAX_BLK_SIZE];         // Mono, 16-bit output buffer
LVM_INT16   ReadSamples;
LVM_PCM_Buffer_st MicrophoneBuffer;
LVM_PCM_Channel_t MicrophoneChannels[TX_NUMCHANNELS_IN];
LVM_PCM_Buffer_st PreProcessedBuffer;
LVM_PCM_Channel_t PreprocessedChannels[TX_NUMCHANNELS_OUT];
LVM_PCM_Buffer_st PlaybackBuffer;
LVM_PCM_Channel_t PlaybackChannels[TX_NUMCHANNELS_REFERENCE];

// File handles
FILE  *INFILE_TX_CHANNEL_0, *INFILE_TX_REFERENCE, *INFILE_TX_CHANNEL_2, *INFILE_TX_CHANNEL_3;
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
MicrophoneBuffer.channels = MicrophoneChannels;
PreProcessedBuffer.channels = PreprocessedChannels;
PlaybackBuffer.channels = PlaybackChannels;

// Select the required buffers
MicrophoneBuffer.channels[0] = InBuffer16_Tx_Channel_0;
MicrophoneBuffer.channels[1] = InBuffer16_Tx_Channel_1;
MicrophoneBuffer.channels[2] = InBuffer16_Tx_Channel_2;
MicrophoneBuffer.FrameCount = MAX_BLK_SIZE; // This number is sample rate dependent and should meet
                                            // requirements as defined in @LVM_PCM_Buffer_st for
                                            // FrameCount
PlaybackBuffer.channels[0] = InBuffer16_Tx_Reference;
PlaybackBuffer.FrameCount = MAX_BLK_SIZE;
PreProcessedBuffer.channels[0] = OutBuffer16_Tx_Channel;

// Open files and read samples
INFILE_TX_REFRENCE  = fopen("Example_REF_8kHz.pcm", "rb");
INFILE_TX_CHANNEL_0 = fopen("Example_TX_CHANNEL_0_8kHz.pcm", "rb");
INFILE_TX_CHANNEL_1 = fopen("Example_TX_CHANNEL_1_8kHz.pcm", "rb");
INFILE_TX_CHANNEL_2 = fopen("Example_TX_CHANNEL_2_8kHz.pcm", "rb");

ReadSamples = (LVM_INT16)fread( InBuffer16_Tx_Reference, sizeof(LVM_INT16), NumSamples, INFILE_TX_REFERENCE );
ReadSamples = (LVM_INT16)fread( InBuffer16_Tx_CHANNEL_0, sizeof(LVM_INT16), NumSamples, INFILE_TX_CHANNEL_0 );
ReadSamples = (LVM_INT16)fread( InBuffer16_Tx_CHANNEL_1, sizeof(LVM_INT16), NumSamples, INFILE_TX_CHANNEL_1 );
ReadSamples = (LVM_INT16)fread( InBuffer16_Tx_CHANNEL_2, sizeof(LVM_INT16), NumSamples, INFILE_TX_CHANNEL_2 );

// Here application can set number of channels allowed to process through LVVE_Tx_Process function
// using @ref LVVE_Tx_SetInputChannelCount. At the moment maximum one input channels, one output
// channels and one reference channel cab be set.

// Push data into reference buffer before calling LVVE_Tx_Process. If not called LVVE_Tx_Process
// would throw @ref LVVE_REFERENCE_FIFO_UNDERFLOW error,
// though it processes data normally. Only use it if noise suppression is turned on.
LVVE_Status = LVVE_Tx_PushReference(hInstance_Tx, // Instance handle
                                    &PlaybackBuffer); // Input buffer with echo reference signal
                                                      // (played back on speaker)

if (LVVE_Status != LVVE_SUCCESS)
{
    // Check for error and stop if needed
}

LVVE_Status = LVVE_Tx_Process(hInstance_Tx, // Instance handle
                              &MicrophoneBuffer, // Input buffer with data from all microphones
                              &PreProcessedBuffer); // Output buffer with preprocessed data

if (LVVE_Status != LVVE_SUCCESS)
{
    `// handle errors
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_Process(LVVE_Tx_Handle_t hInstance,
                                     const LVM_PCM_Buffer_st* pInputBuffer,
                                     LVM_PCM_Buffer_st* pOutputBuffer);

/**
@brief Configures the number of input channels for the LVVE_Tx module

After the LVVE_Tx instance creation number of input channels defaults to one.

@param  hInstance               Instance handle
@param  numChannels             Number of channels to set (from @ref
                                LVVE_TX_INPUT_CHANNEL_COUNT_MIN up to @ref
                                LVVE_TX_INPUT_CHANNEL_COUNT_MAX)

@pre hInstance should not be NULL

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_OUTOFRANGE          if number of channels is out of range
@return  LVVE_NULLADDRESS         When hInstance is null

@ingroup LVVE_Tx

LVVE_Tx_SetInputChannelCount Example:

\code

#define LVVE_TX_INPUT_CHANNEL 3
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle


// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
//    need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_SetInputChannelCount( hInstance_Tx, LVVE_TX_INPUT_CHANNEL);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_SetInputChannelCount(LVVE_Tx_Handle_t hInstance,
                                                  LVM_INT16 numChannels);

/**
@brief Configures the number of output channels for the LVVE_Tx module

After the LVVE_Tx instance creation the number of output channels defaults to one

@param  hInstance               Instance handle
@param  numChannels             Number of channels to set (from @ref
                                LVVE_TX_OUTPUT_CHANNEL_COUNT_MIN up to @ref
                                LVVE_TX_OUTPUT_CHANNEL_COUNT_MAX)

@pre hInstance should not be NULL

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_OUTOFRANGE          if number of channels is out of range
@return  LVVE_NULLADDRESS         When hInstance is null

@ingroup LVVE_Tx

LVVE_Tx_SetOutputChannelCount Example:

\code

#define LVVE_TX_OUTPUT_CHANNEL 1
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle


// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
// need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_SetOutputChannelCount( hInstance_Tx, LVVE_TX_OUTPUT_CHANNEL);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_SetOutputChannelCount( LVVE_Tx_Handle_t hInstance,
                                                    LVM_INT16 numChannels);

/**
@brief Configures the number of reference channels for the LVVE_Tx module

After the LVVE_Tx instance creation the number of reference channels defaults to one

@param  hInstance               Instance handle
@param  numChannels             Number of channels to set (from @ref
                                LVVE_TX_REFERENCE_CHANNEL_COUNT_MIN up to @ref
                                LVVE_TX_REFERENCE_CHANNEL_COUNT_MAX)

@pre hInstance should not be NULL

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_OUTOFRANGE          if number of channels is out of range
@return  LVVE_NULLADDRESS         When hInstance is null

@ingroup LVVE_Tx

LVVE_Tx_SetReferenceChannelCount Example:

\code

#define LVVE_TX_REFERENCE_CHANNEL 1
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle

// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
// need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_SetReferenceChannelCount( hInstance_Tx, LVVE_TX_REFERENCE_CHANNEL);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}
\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_SetReferenceChannelCount(LVVE_Tx_Handle_t hInstance,
                                                      LVM_INT16 numChannels);

/**
@brief Returns the number of input channels used by the LVVE_Tx module

@param  hInstance                 Instance handle
@param  pNumChannels              Pointer to number of output channels used by the LVVE_Tx module

@pre hInstance should not be NULL

@post  *pNumChannels              Number of input channels used by the LVVE_Tx module

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_NULLADDRESS         When hInstance is null
@return  LVVE_NULLADDRESS         When pNumChannels is null

@ingroup LVVE_Tx

LVVE_Tx_GetInputChannelCount Example:

\code

LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle
LVM_INT16 numChannels = 0;

// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
// need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_GetInputChannelCount( hInstance_Tx, &numChannels);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}
\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_GetInputChannelCount( LVVE_Tx_Handle_t hInstance,
                                                   LVM_INT16 *pNumChannels);

/**
@brief Returns the number of output channels used by the LVVE_Tx module

@param  hInstance               Instance handle
@param  pNumChannels            Pointer to number of output channels used by the LVVE_Tx module

@pre hInstance should not be NULL
@post *pNumChannles contains number of output channels used by the LVVE_Tx

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_NULLADDRESS         When hInstance is null
@return  LVVE_NULLADDRESS         When pNumChannels is null

@ingroup LVVE_Tx

LVVE_Tx_GetOutputChannelCount Example:

\code

LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle
LVM_INT16 numChannels = 0;

// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
// need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_GetOutputChannelCount( hInstance_Tx, @numChannels);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_GetOutputChannelCount( LVVE_Tx_Handle_t hInstance,
                                                    LVM_INT16 *pNumChannels);

/**
@brief Returns the number of reference channels used by the LVVE_Tx module

@param  hInstance               Instance handle
@param  pNumChannels            Pointer to number of output channels used by the LVVE_Tx module

@pre hInstance should not be NULL
@post *pNumChannles contains number of reference channels used by the LVVE_Tx

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_NULLADDRESS         When hInstance is null
@return  LVVE_NULLADDRESS         When pNumChannels is null

@ingroup LVVE_Tx

LVVE_Tx_GetReferenceChannelCount Example:

\code

LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Tx_Handle_t            hInstance_Tx; // Tx instance handle
LVM_INT16 numChannels = 0;

// Make sure handle is obtained from the LVVE_Tx before calling this function. If already called no
// need to get instance handle again
hInstance_Tx = LVM_NULL;

LVVE_Status = LVVE_Tx_GetInstanceHandle(&hInstance_Tx,
                                        &MemoryTable_Tx,
                                        &InstanceParams_Tx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Tx_GetReferenceChannelCount( hInstance_Tx, @numChannels);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Tx_GetReferenceChannelCount( LVVE_Tx_Handle_t hInstance,
                                                       LVM_INT16 *pNumChannels);

/**
@brief Processes a buffer of audio frames by the LVVE_Rx module.

This function processes the audio frames as configured with the current
parameter settings. If new parameters have been given since the last call to \ref LVVE_Rx_Process
then these will be applied at the beginning of this process call. The frame count of
\ref LVM_PCM_Buffer_st of the output buffer is filled in by this function. The number of frames in
the output is the same as in the input signal. When an error occurs in LVVE_Rx_Process API, output
FrameCount in the pOutputbuffer is set to zero. The FrameCount should take on values as listed
in the table.
<table border>
   <tr>
       <td><b>Unit</b></td>
       <td><b>Q format</b></td>
       <td><b>Data Range</b></td>
       <td><b>Sample Rate</b></td>
       <td><b>Default Values</b></td>
   </tr>
   <tr>
       <td rowspan=5><b>Number of Frames</b></td>
       <td rowspan=5><b>Q31.0</b></td>
       <td>Integer multiple of 80</td>
       <td>8kHz</td>
       <td>80</td>
   </tr>
   <tr>
       <td>Integer multiple of 160</td>
       <td>16kHz</td>
       <td>160</td>
   </tr>
   <tr>
       <td>Integer multiple of 240</td>
       <td>24kHz</td>
       <td>240</td>
   </tr>
   <tr>
       <td>Integer multiple of 320</td>
       <td>32kHz</td>
       <td>320</td>
   </tr>
   <tr>
       <td>Integer multiple of 480</td>
       <td>48kHz</td>
       <td>480</td>
   </tr>
</table>

@param hInstance                Instance handle
@param pInputBuffer             Pointer to input buffer of audio frames
@param pOutputBuffer            Pointer to output buffer of audio frames

@pre hInstance should be valid handle.
@pre LVVE_Rx_SetControlParameters should be called successfully once before the first call to
     LVVE_Rx_Process
@pre InputBuffer is filled with the input samples to process

@post pOutData contains the processed samples and number of frames of output

@return LVVE_SUCCESS                     Succeeded
@return LVVE_NULLADDRESS                 When hInstance, pInData or pOutData is NULL
@return LVVE_INVALID_FRAME_COUNT         When the NumSamples is outside the allowed range
@return LVVE_INVALID_STATE_CONFIGURATION When invalid state of the algorithm found
@return LVVE_REFERENCE_FIFO_UNDERFLOW    When reference buffer does not have sufficient data.
                                         In this case the LVVE_Rx processes the data normally
                                         just giving warning of under flow happened
@ingroup LVVE_Rx

LVVE_Rx_Process Example:
\code

#define MAX_BLK_SIZE 80 // This number is sample rate dependent and should meet requirements as
                        // defined in @LVM_PCM_Buffer_st for FrameCount
#define RX_NUMCHANNELS_IN (1)
#define RX_NUMCHANNELS_OUT (1)

LVM_INT16   InBuffer16_Rx_Channel[MAX_BLK_SIZE];       // Mono, 16-bit input buffer
LVM_INT16   OutBuffer16_Channel[MAX_BLK_SIZE];         // Mono, 16-bit output buffer
LVM_INT16   ReadSamples;

LVM_PCM_Buffer_st LineInBuffer;
LVM_PCM_Channel_t LineInChannels[RX_NUMCHANNELS_IN];
LVM_PCM_Buffer_st PostProcessedBuffer;
LVM_PCM_Channel_t PostProcessedChannels[RX_NUMCHANNELS_OUT];

FILE  *INFILE_RX_CHANNEL_0;
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LineInBuffer.channels = LineInChannels;
PostProcessedBuffer.channels = PostProcessedChannels;

// Select the required buffers
LineInBuffer.channels[0] = InBuffer16_Rx;
LineInBuffer.FrameCount = NumFrames;
PostProcessedBuffer.channels[0] = OutBuffer16_Rx;

// Open file and read samples
INFILE_RX_CHANNEL_0 = fopen("Example_RX_CHANNEL_0_8kHz.pcm", "rb");

ReadSamples = (LVM_INT16)fread( InBuffer16_Rx_CHANNEL_0, sizeof(LVM_INT16), NumSamples, INFILE_RX_CHANNEL_0 );

// Here application can set number of channels allowed to process through LVVE_Rx_Process function
// using @ref LVVE_Rx_SetInputChannelCount. At the moment maximum one input channel and one output
// channel can be set.

LVVE_Status = LVVE_Rx_Process(hInstance_Rx,          // Instance handle
                              &LineInBuffer,         // Input buffer with data from down-link
                              &PostProcessedBuffer); // Output buffer with preprocessed data

if (LVVE_Status != LVVE_SUCCESS)
{
    // handle errors
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_Process(LVVE_Rx_Handle_t hInstance,
                                     const LVM_PCM_Buffer_st* pInputBuffer,
                                     LVM_PCM_Buffer_st* pOutputBuffer);

/**
@brief Configures the number of input channels for the LVVE_Rx module

After the LVVE_Rx instance creation number of input channels defaults to one.

@param  hInstance               Instance handle
@param  numChannels             Number of channels to set (from @ref
                                LVVE_RX_INPUT_CHANNEL_COUNT_MIN up to @ref
                                LVVE_RX_INPUT_CHANNEL_COUNT_MAX)

@pre hInstance should not be NULL

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_OUTOFRANGE          if number of channels is out of range
@return  LVVE_NULLADDRESS         When hInstance is null

@ingroup LVVE_Rx

LVVE_Rx_SetInputChannelCount Example:

\code

#define LVVE_RX_INPUT_CHANNEL 1
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Rx_Handle_t            hInstance_Rx; // Rx instance handle


// Make sure handle is obtained from the LVVE_Rx before calling this function. If already called no
//    need to get instance handle again
hInstance_Rx = LVM_NULL;

LVVE_Status = LVVE_Rx_GetInstanceHandle(&hInstance_Rx,
                                        &MemoryTable_Rx,
                                        &InstanceParams_Rx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Rx_SetInputChannelCount( hInstance_Rx, LVVE_RX_INPUT_CHANNEL);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_SetInputChannelCount(LVVE_Rx_Handle_t hInstance,
                                                  LVM_INT16 numChannels);

/**
@brief Configures the number of output channels for the LVVE_Rx module

After the LVVE_Rx instance creation the number of output channels defaults to one

@param  hInstance               Instance handle
@param  numChannels             Number of channels to set (from @ref
                                LVVE_RX_OUTPUT_CHANNEL_COUNT_MIN up to @ref
                                LVVE_RX_OUTPUT_CHANNEL_COUNT_MAX)

@pre hInstance should not be NULL

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_OUTOFRANGE          if number of channels is out of range
@return  LVVE_NULLADDRESS         When hInstance is null

@ingroup LVVE_Rx

LVVE_Rx_SetOutputChannelCount Example:

\code

#define LVVE_RX_OUTPUT_CHANNEL 1
LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Rx_Handle_t            hInstance_Rx; // Rx instance handle


// Make sure handle is obtained from the LVVE_Rx before calling this function. If already called no
// need to get instance handle again
hInstance_Rx = LVM_NULL;

LVVE_Status = LVVE_Rx_GetInstanceHandle(&hInstance_Rx,
                                        &MemoryTable_Rx,
                                        &InstanceParams_Rx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Rx_SetOutputChannelCount( hInstance_Rx, LVVE_RX_OUTPUT_CHANNEL);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/

LVVE_ReturnStatus_en LVVE_Rx_SetOutputChannelCount(LVVE_Rx_Handle_t hInstance,
                                                  LVM_INT16 numChannels);

/**
@brief Returns the number of input channels used by the LVVE_Rx module

@param  hInstance                 Instance handle
@param  pNumChannels              Pointer to number of output channels used by the LVVE_Rx module

@pre hInstance should not be NULL

@post  *pNumChannels              Number of input channels used by the LVVE_Rx module

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_NULLADDRESS         When hInstance is null
@return  LVVE_NULLADDRESS         When pNumChannels is null

@ingroup LVVE_Rx

LVVE_Rx_GetInputChannelCount Example:

\code

LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Rx_Handle_t            hInstance_Rx; // Rx instance handle
LVM_INT16 numChannels = 0;

// Make sure handle is obtained from the LVVE_Rx before calling this function. If already called no
// need to get instance handle again
hInstance_Rx = LVM_NULL;

LVVE_Status = LVVE_Rx_GetInstanceHandle(&hInstance_Rx,
                                        &MemoryTable_Rx,
                                        &InstanceParams_Rx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Rx_GetInputChannelCount( hInstance_Rx, &numChannels);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}
\endcode

*/

LVVE_ReturnStatus_en LVVE_Rx_GetInputChannelCount(LVVE_Rx_Handle_t hInstance,
                                                  LVM_INT16 *pNumChannels);

/**
@brief Returns the number of output channels used by the LVVE_Rx module

@param  hInstance               Instance handle
@param  pNumChannels            Pointer to number of output channels used by the LVVE_Rx module

@pre hInstance should not be NULL
@post *pNumChannles contains number of output channels used by the LVVE_Rx

@return  LVVE_SUCCESS             Succeeded
@return  LVVE_NULLADDRESS         When hInstance is null
@return  LVVE_NULLADDRESS         When pNumChannels is null

@ingroup LVVE_Rx

LVVE_Rx_GetOutputChannelCount Example:

\code

LVVE_ReturnStatus_en        LVVE_Status; // Module return status
LVVE_Rx_Handle_t            hInstance_Rx; // Rx instance handle
LVM_INT16 numChannels = 0;

// Make sure handle is obtained from the LVVE_Rx before calling this function. If already called no
// need to get instance handle again
hInstance_Rx = LVM_NULL;

LVVE_Status = LVVE_Rx_GetInstanceHandle(&hInstance_Rx,
                                        &MemoryTable_Rx,
                                        &InstanceParams_Rx);

if (LVVE_Status == LVVE_NULLADDRESS)
{
    // handle error
}

LVVE_Status =  LVVE_Rx_GetOutputChannelCount( hInstance_Rx, @numChannels);

if(LVVE_Status != LVVE_SUCCESS)
{
    // handle error
}

\endcode

*/
LVVE_ReturnStatus_en LVVE_Rx_GetOutputChannelCount(LVVE_Rx_Handle_t hInstance,
                                                  LVM_INT16 *pNumChannels);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif      /* __LVVE_H__ */

/* End of file */
