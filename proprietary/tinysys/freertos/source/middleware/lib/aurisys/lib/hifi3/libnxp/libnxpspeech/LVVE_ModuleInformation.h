/****************************************************************************************/
/*  Copyright (c) 2004-2015 NXP Software. All rights are reserved.                      */
/*  Reproduction in whole or in part is prohibited without the prior                    */
/*  written consent of the copyright owner.                                             */
/*                                                                                      */
/*  This software and any compilation or derivative thereof is and                      */
/*  shall remain the proprietary information of NXP Software and is                     */
/*  highly confidential in nature. Any and all use hereof is restricted                 */
/*  and is subject to the terms and conditions set forth in the                         */
/*  software license agreement concluded with NXP Software.                             */
/*                                                                                      */
/*  Under no circumstances is this software or any derivative thereof                   */
/*  to be combined with any Open Source Software in any way or                          */
/*  licensed under any Open License Terms without the express prior                     */
/*  written  permission of NXP Software.                                                */
/*                                                                                      */
/*  For the purpose of this clause, the term Open Source Software means                 */
/*  any software that is licensed under Open License Terms. Open                        */
/*  License Terms means terms in any license that require as a                          */
/*  condition of use, modification and/or distribution of a work                        */
/*                                                                                      */
/*           1. the making available of source code or other materials                  */
/*              preferred for modification, or                                          */
/*                                                                                      */
/*           2. the granting of permission for creating derivative                      */
/*              works, or                                                               */
/*                                                                                      */
/*           3. the reproduction of certain notices or license terms                    */
/*              in derivative works or accompanying documentation, or                   */
/*                                                                                      */
/*           4. the granting of a royalty-free license to any party                     */
/*              under Intellectual Property Rights                                      */
/*                                                                                      */
/*  regarding the work and/or any work that contains, is combined with,                 */
/*  requires or otherwise is based on the work.                                         */
/*                                                                                      */
/*  This software is provided for ease of recompilation only.                           */
/*  Modification and reverse engineering of this software are strictly                  */
/*  prohibited.                                                                         */
/*                                                                                      */
/****************************************************************************************/

/**
@file
Header file with overview information about the LVVE module. This file only contains
information and comments. This header file does not contain any definitions or symbols.
*/

/**
@defgroup LVVE_Rx LVVE_Rx
@brief VoiceEnhancement effects for the down-link signal

LVVE_Rx Module is responsible for VoiceEnhancements in the down-link audio path (playback towards the loudspeaker of the device)
*/

/**
@defgroup LVVE_Tx LVVE_Tx
@brief VoiceEnhancement effects for the up-link signal

LVVE_Tx Module is responsible for VoiceEnhancements in the up-link audio path (recording from the microphones of the device)
*/

/**
@mainpage notitle

<table border>
<tr>
    <td><b>Security</b></td>
    <td><b>CONFIDENTIAL</b>
      The attached material and the information contained therein are proprietary to NXP Software, and are issued only under strict confidentiality arrangements.
      Without a separate written authorization, it shall not be used for any purpose whatsoever, reproduced, copied in whole or in part, adapted, modified, or disseminated.
      It must be returned to NXP Software upon its first request.
</tr>
</table>

@section description Document Description

@subsection purpose Purpose

This document provides an overview of the LifeVibes&trade; VoiceExperience (LVVE) product
and the configurations and options it provides. It also explains how the product can be used.
The intended audience includes customers who will make use of the software and in particular
software developers whose product will depend on the software resulting from the
requirements defined herein.


@subsection scope Scope

This document describes the full configuration and option set of LVVE.
Not all configurations and options described herein are supported in every release.
If in doubt, contact your NXP agent for details of your release.
This document also contains a complete API description and all information needed
to integrate LVVE.

This document does not make any platform specific assumption and therefore can be
used as a basis for discussions on all target platforms.

@section intro Introduction

LVVE is a voice processing library that enhances incoming and outgoing speech.
It provides one to three microphone setups and ensures crystal clear calls even in the noisiest environments.

The LifeVibes&trade; VoiceExperience product is available with five configurations:

@li LifeVibes&trade; HandsFree (LVHF): one-microphone setup.
@li LifeVibes&trade; NoiseVoid (LVNV): two to three microphone setup. LVHF and LVNV are mutually exclusive during run-time.
@li LifeVibes&trade; WhisperMode (LVWM)
@li LifeVibes&trade; ActiveVoiceContrast (LVAVC)
@li LifeVibes&trade; Bandwidth Extension (LVWBE)

The following options exist:
@li Loudness Maximizer
@li Full Band

@subsection Overview Overview of Functionality

@subsubsection handsfree HandsFree

LifeVibes&trade; HandsFree is a one-microphone speech enhancement algorithm to improve the quality of the transmitted nearend speech.
LVHF suppresses stationary and slowly varying non-stationary background noise. LVHF also provides acoustic echo cancellation (AEC).

@subsubsection noisevoid NoiseVoid

LifeVibes&trade; NoiseVoid is a two to three microphone speech enhancement algorithm to improve the quality of the transmitted nearend speech.
Using a microphone array of upto three microphones, LVNV spatially filters and separates the desired speech signal from the background noise.
By suppressing the stationary and non-stationary background noise components, LVNV offers a great improvement to speech quality in noisy situations.
LVNV also features acoustic echo cancellation.

@subsubsection whispermode WhisperMode

LifeVibes&trade; WhisperMode automatically adjusts the transmitted nearend speech signal to a comfortable level,
compensating for low voice levels such as in small headset or speakerphone applications where the microphone is at far distance from the user mouth.

@subsubsection activevoicecontrast ActiveVoiceContrast

LifeVibes&trade; ActiveVoiceContrast (LVAVC) improves the intelligibility of the received far-end speech under ambient noise conditions. ActiveVoiceContrast analyses the near-end background noise
and correspondingly applies the right far-end speech adaptation to make it just comfortably intelligible in concurrent noise.
ActiveVoiceContrast preserves speech characteristics while emphasizing its relevant parts. The actual version of LVAVC is only supporting Handset mode.
ActiveVoiceContrast is coupled with a FarEnd Noise Suppressor (LVFENS). LVFENS enhances the received far-end speech signal by suppressing stationary and slowly varying non-stationary noise. It enhances the listening comfort and speech intelligibility for the near-end listener.

@subsubsection bandwidthextension Bandwidth Extension

LifeVibes&trade; Bandwidth Extension attempts to recreate the wideband speech from the received narrowband signal in order to improve listening comfort, speech quality and intelligibility.
The algorithm targets high-quality processing, sounding natural and having as few artifacts as possible

@subsubsection hdvoice HD-Voice

HD-Voice is an option that provides wideband (up to 16kHz sample rate) processing. The supported sample rates are described in @ref samplerate section.

@subsubsection fullband Full Band

Full Band is an option that provides Full Band (up to 48kHz sample rate) processing. The supported sample rates are described in @ref samplerate section .

@subsection internalstruct Internal Structure

Figure:\ref INTERNALSTRUCT shows the internal structure of LVVE and an example of the surrounding application in which it may be integrated.

\anchor INTERNALSTRUCT

@image html LVVE_Structure.png "LifeVibes&trade; VoiceExperience internal software structure"

LVVE is split in two processing units, @ref LVVE_Rx unit and @ref LVVE_Tx unit:

@li The Rx unit processes the speech frames coming from the voice decoder. The processed output frames of the Rx unit are sent to the DAC.
@li The Tx unit processes the speech frames coming from the ADC. The processed output frames of the Tx unit are sent to the voice encoder.

Control commands can be given at any time to change the settings of LVVE. Both the Rx and Tx paths can be switched OFF in which case the processing
is bypassed and input data is piped directly to the output.

The Tx unit can comprise of the following voice processing algorithms:

@li  High pass filters : suppress the DC and low frequency noise (below 50Hz and configurable up to 1000Hz) from the microphone signal(s).
     The same filter coefficients are applied to both microphones.
@li  Configurable Delay Line : is introduced to the Reference data to match delay introduced by the actual delay of the system (due to audio buffering and transmission through the air etc).
@li  LifeVibes&trade; HandsFree or LifeVibes&trade; NoiseVoid. These two modules are mutually exclusive during run-time.
@li  LifeVibes&trade; Fast Noise Estimator
@li  Microphone Equalizer : compensates for microphone imperfections and enables a customized \"sound taste\"
@li  LifeVibes&trade; WhisperMode
@li  LifeVibes&trade; Dynamic Range Control : increases loudness with configurable parameters
@li  Volume control and microphone mute : consists of a tunable gain together with the possibility to mute the signal.
     Both are soft features: the volume control smoothly changes from one volume setting to another; the mute smoothly changes from the original volume setting to silence and vice versa.
@li Comfort Noise Generator: adds configurable strength of comfort noise to output

The Rx unit can comprise the following voice processing algorithms:

@li High pass filter: suppresses the DC and low frequency noise (below 50Hz and configurable up to 1500Hz) from the far-end signal
@li LifeVibes&trade; ActiveVoiceContrast
@li LifeVibes&trade; Bandwidth Extension
@li Noise Gate: suppresses idle noise components which are not suppressed by HPF or FENS
@li Notch Filter: attenuates the input around the loudspeaker resonance frequency to improve sound quality and, reduce saturations and non-linearities
@li Loudspeaker Equalizer: compensates for loudspeaker imperfections and enables a customized \"sound taste\"
@li Automatic Gain Control: normalizes speech signals, as LVWM does in Tx.
@li LifeVibes&trade; Dynamic Range Control
@li Volume control and loudspeaker mute: consist of a tunable gain together with the possibility to mute the signal.
      Both are soft features: the volume control smoothly changes from one  volume setting to another;
      the mute smoothly changes from the original volume setting to silence and vice versa
@li Comfort Noise Generator: adds configurable strength of comfort noise to output

If the release is a demo release for evaluation there will be a 1s beep sequence on the output after 5s and every 60s after that when the Voice Engine is not off.

@section interface Interfaces

@subsection samplerate Supported Sample Rates

LVVE has full support for 8kHz and 16kHz sample rates. With the Full Band option, additional sample rates 24kHz, 32kHz and
48kHz are supported for LVVE.

<B>Note:</B> Bandwidth Extension operates at all sample rates supported by LVVE but only brings an effect for input sampled at 16kHz. For other sample rates the input is simply copied to the output.


@subsection controlfeatures Control of Features

The following settings can be controlled:

@li LVHF: The AEC/NS parameters can be changed, e.g. when going from handset use case to speakerphone use case.
@li LVNV: The AEC/NS parameters can be changed.
@li LVWM: The effect level can be fully adjusted through parameters.
@li Equalizers: The equalizer impulse response can be changed.
@li LVAVC: The effect level can be fully adjusted through parameters. It enhances input signals of 8k and 16k Hz sample rates but for the rest 24, 32, 48 kHz it bypass the input signal.
      LVAVC is only supporting the Handset mode. LVAVC is not supported with Dynamic Range Control, High Pass Filter, Notch Filter on Rx path (DRC, HPF, NF are forced OFF in LVVE when LVAVC is enabled).
@li LVFENS: The amount of noise suppression can be adjusted.
@li LVWBE: The effect level can be fully adjusted through parameters.
@li Volume Control and Mute: The Rx and Tx volume can be adjusted. Rx and Tx can be muted.
@li High Pass Filters: The cut-off frequency of High-pass filters in Rx and Tx can be adjusted independently.
@li Noise Gate: The threshold and suppression amount of very-low-level noise can be fully adjusted through parameters.
      Note that it is not allowed to switch the Noise Gate module on or off during a call, except of use case changes
      (like when changing from the handset mode to the speakerphone mode, or vice versa).
@li Automatic Gain Control: The effect level can be fully adjusted through parameters.
@li Dynamic Range Control: The effect level can be fully adjusted through parameters. Note that it is not allowed to switch
      the DRC module on or off during a call with any volume change, except of use case changes
      (like when changing from the handset mode to the speakerphone mode, or vice versa).
@li Comfort Noise Generator: Output level of comfort noise can be adjusted. CNG Operation can be disabled.
@li Notch Filter: attenuation level, center frequency and width (QFactor) of the filter can be adjusted. The Notch Filter can also be disabled.

In addition, all configurations can be turned on or off separately.

@subsection IOrequirements General I/O Requirements

@li LVVE expects 16-bit PCM mono non-interleaved input streams for all input channels. The number of frames is fixed to 1/100th of the sampling rate (i.e. 10ms) or integer multiples of this.

@section api Application Programming Interfaces
@ref LVVE.h describes the API of LVVE. This file includes all function prototypes, header files, variables, data types and
enum definitions required to integrate LVVE library into the host products. @ref LVVE.h is divided into @ref LVVE_Tx and @ref LVVE_Rx  modules.

@section functions VoiceExperience Functions

@subsection callseq2 Function Call Sequence for Multi-microphone

The sequence of operations and function calls shown in Figure:\ref SEQUENCE_DIAGRAM is a typical use case for LVVE. The defined functions fall into several categories:

@li Instance creation, initialization, and termination;
@li Processing data;
@li Run-time control;
@li Run-time monitoring of internal status variables.

For simplicity, the monitoring of the internal status variables is not shown in the function call graph. The Rx and Tx Unit are also allowed to run in different threads.

\anchor SEQUENCE_DIAGRAM
@image html LVVE_callsequence.png "Function Call Diagram: Rx and Tx process functions in one thread"
@section information Additional User Information

@subsection deliverables Deliverables

The following items are part of the NXP SW-Audio delivery:

@li LVVE User Guide (this document)
@li LVVE Integration Test Specification
@li Object code for LVVE and required public header files
@li An example application in source code
@li A release note

@subsection    support Integration Support

NXP Software delivers only LVVE module and associated documentation.
NXP Software provides integration support, the extent of the support is
determined at the start of an integration project by mutual agreement.
This document provides sufficient details to aid smooth integration by customer engineers.
@subsection tuning Tuning
LVVE library is delivered with default tuning. Customized tuning may be required for specific customer
applications and target platforms to achieve optimal performance and results. When required, this tuning is
performed by NXP SW-Audio in the scope of a project.
@subsection    portability Flexibility and Portability
No specific flexibility is foreseen in LVVE module other than that described in
the API section of this document.
The module is written in a combination of C and optimized assembler and therefore is
platform specific. The delivered library can therefore not be used on other processors
or platforms.
@section example Example Application and Integration

@subsection Example Application

The example application provided in the release supports a number of different configurations of LVVE.
As such, some parts of the example application may not be relevant to all releases.
The example application demonstrates how to initialize LVVE. This requires the following steps:

@li Set the initial operating parameters
@li Set the required instance parameters
@li Call LVVE_GetMemoryTable functions
@li Allocate the memory as indicated in the returned memory tables and set the base address pointers in the memory tables
@li Call LVVE_GetInstanceHandle functions
@li Call LVVE_SetPreset functions after loading the desired preset file

The algorithm requires a number of memory regions and these are supplied by the calling application through the use of malloc
or a similar function. There is no special alignment requirement for the base addresses of these memory regions. After the call
to LVVE_GetInstanceHandle functions the returned status should be checked to ensure the initialization was successful.

The example application also illustrates how to load and set parameters from a different preset file.
LVVE_SetPreset may be used at any time whilst running the algorithm. It may even be run in a different thread than the process function.
The new parameters will be used by the process function from its next call.
The example application initializes the library with a set of default parameters.
These parameters are chosen to demonstrate the correct working for the library and are not necessarily the ideal set of
parameters for any particular application.

@subsection Integratreq Integration Requirements

Figure:\ref INTEGRATION_DIAGRAM shows the integration of LVVE in the audio path:
@li   The Rx Unit processes the speech frames coming from the voice decoder. The processed output frames of the Rx Unit are buffered and fed to the loudspeaker
@li   The Tx Unit processes the speech frames coming from the microphone(s). The processed output frames of the Tx Unit are sent to the voice encoder
@li   The Tx Unit requires the Rx signal as reference signal for Acoustic Echo Cancellation (AEC)
@li   The Rx Unit requires a noise reference signal calculated in the Tx Unit in case ActiveVoiceContrast is included in the Rx Unit (otherwise this can be a zero-filled buffer)

\anchor INTEGRATION_DIAGRAM
@image html LVVE_Integration.png "Integration of LVVE in the audio path"
@subsubsection constraints Delay Constraints
The Tx Unit internally provides a programmable delay-line (see Figure:\ref INTERNALSTRUCT) to compensate the echo path delay.
This delay includes audio buffering, DAC and ADC delay, acoustic propagation delay, etc. The echo path delay is
the delay between the signals marked by 1 and 2 in Figure:\ref INTEGRATION_DIAGRAM

@li The echo path delay should be constant during the call

The echo path delay is not allowed to drift during the call. A drifting echo path delay will require the AEC module to re-converge,
resulting in temporary echoes. If the echo path delay drifts outside the range that the AEC can cover, the AEC will not be able to re-converge and cancel the echo.

@li The echo path delay should be known and constant over different calls.

If the delay is not constant over different calls, the delay value should be measured and provided to LVVE at call set-up.

@subsubsection position Position Constraints

For optimal performance of the algorithms, the position of LVVE in the audio path compared to other processing functions is constrained.

@li LVVE should be the last audio processing module in the audio path, i.e., as close as possible to the echo path;
@li In case Rx volume control is done outside LVVE, the volume control should be at the input of the Rx Unit, outside the echo path.
        In this way, the coupling between Rx and Tx is  independent of the volume setting;
@li In case Tx mute is done outside LVVE, the mute should be at the output of the Tx Unit, outside the echo path.
        In this way, the noise suppression and the acoustic echo cancellation will be independent of the Tx mute;
@li Non-linearity should be avoided in the acoustic echo path:
@li No digital saturation in the ADC or DAC;
@li No analog saturation in the loudspeaker or microphone(s);
@li No non-linear processing (e.g., Automatic Gain Control) in the echo path

@subsubsection management Parameter Management
For optimal performance on a device, the control parameters of LVVE algorithms should be tuned depending on the acoustic properties
of the device model. A different dedicated set of parameters will have to be implemented for the each of the supported use cases of the device:

@li Normal mode or handset mode (Slide up and down for slide-phone);
@li Speakerphone mode (Slide up and down for slide-phone);
@li Wired and wireless headset;
@li Car kit

For tuning purpose the parameters should not be hard-coded in the integrating application in order to avoid re-compilation during each tuning step.
For ActiveVoiceContrast, different dedicated sets of parameters will be used for each volume step.

@section  testintegrate Integration Test

For integration tests, please refer to @cite LVVE:ITS.

@section  tunning Algorithm Tuning

For tuning the algorithm, please refer to @cite LVVE:TG.

@section issues Open Issues

There are no open issues.

@section Glossary

<table border>
<caption>Glossary</caption>
<tr>
    <td><b>ADC</b></td>
    <td>Analog-to-Digital Converter</td>
</tr>
<tr>
    <td><b>AEC</b></td>
    <td>Acoustic Echo Canceller</td>
</tr>
<tr>
    <td><b>AGC</b></td>
    <td>Automatic Gain Controller</td>
</tr>
<tr>
    <td><b>API</b></td>
    <td>Application Programmers Interface. A set of constants, types, variables and functions used to interact with a piece of software</td>
</tr>
<tr>
    <td><b>AVL</b></td>
    <td>Automatic Volume Leveler</td>
</tr>
<tr>
    <td><b>BMF</b></td>
    <td>Beamformer</td>
</tr>
<tr>
    <td><b>CAL</b></td>
    <td>Calibration</td>
</tr>
<tr>
    <td><b>CNG</b></td>
    <td>Comfort Noise Generator</td>
</tr>
<tr>
    <td><b>DAC</b></td>
    <td>Digital-to-Analog Converter</td>
</tr>
<tr>
    <td><b>DENS</b></td>
    <td>Dynamic Echo and Noise Suppressor</td>
</tr>
<tr>
    <td><b>DNNS</b></td>
    <td>Dynamic Non-stationary Noise Suppressor</td>
</tr>
<tr>
    <td><b>DRC</b></td>
    <td>Dynamic Range Control</td>
</tr>
<tr>
    <td><b>DT</b></td>
    <td>Double Talk</td>
</tr>
<tr>
    <td><b>EQ</b></td>
    <td>Equalizer</td>
</tr>
<tr>
    <td><b>FIFO</b></td>
    <td>First In, First Out</td>
</tr>
<tr>
    <td><b>FSB</b></td>
    <td>Filter-and-Sum Beamformer</td>
</tr>
<tr>
    <td><b>GSC</b></td>
    <td>Generalized Sidelobe Canceller</td>
</tr>
<tr>
    <td><b>HPF</b></td>
    <td>High-Pass Filter</td>
</tr>
<tr>
    <td><b>HPFREF</b></td>
    <td>High-Pass Filter on Echo Reference</td>
</tr>
<tr>
    <td><b>Inplace</b></td>
    <td>The name for processing data where the input and output data buffers are at the same physical location in memory. The input is overwritten by the output after processing</td>
</tr>
<tr>
    <td><b>Instance</b></td>
    <td>The specific data allocated in an application that defines a particular object</td>
</tr>
<tr>
    <td><b>LMfV</b></td>
    <td>Loudness Maximizer for Voice</td>
</tr>
<tr>
    <td><b>LPF</b></td>
    <td>Low-Pass Filter</td>
</tr>
<tr>
    <td><b>LVFENS</b></td>
    <td>LifeVibes&trade; FarEnd Noise Suppressor</td>
</tr>
<tr>
    <td><b>LVHF</b></td>
    <td>LifeVibes&trade; HandsFree</td>
</tr>
<tr>
    <td><b>LVNV</b></td>
    <td>LifeVibes&trade; NoiseVoid</td>
</tr>
<tr>
    <td><b>LVAVC</b></td>
    <td>LifeVibes&trade; ActiveVoiceContrast</td>
</tr>
<tr>
    <td><b>LVVE</b></td>
    <td>LifeVibes&trade; VoiceExperience</td>
</tr>
<tr>
    <td><b>LVWBE</b></td>
    <td>LifeVibes&trade; Bandwidth Extension</td>
</tr>
<tr>
    <td><b>LVWM</b></td>
    <td>LifeVibes&trade; WhisperMode</td>
</tr>
<tr>
    <td><b>MIPS</b></td>
    <td>Million Instructions Per Second</td>
</tr>
<tr>
    <td><b>NF</b></td>
    <td>Notch Filter</td>
</tr>
<tr>
    <td><b>NLMS</b></td>
    <td>Normalized Least Mean Squares</td>
</tr>
<tr>
    <td><b>NS</b></td>
    <td>Noise Suppression</td>
</tr>
<tr>
    <td><b>Out-of-place</b></td>
    <td>The name for processing data where the input and output data buffers are at different, non-overlapping physical locations in memory</td>
</tr>
<tr>
    <td><b>PCD</b></td>
    <td>Patch Change Detector</td>
</tr>
<tr>
    <td><b>Rx</b></td>
    <td>Signal received from the farend and to be played on the nearend loudspeaker</td>
</tr>
<tr>
    <td><b>SNR</b></td>
    <td>Signal-to-Noise-Ratio</td>
</tr>
<tr>
    <td><b>Tx</b></td>
    <td>Signal captured by the nearend microphone(s) and to be send to the farend</td>
</tr>

</table border>
*/
