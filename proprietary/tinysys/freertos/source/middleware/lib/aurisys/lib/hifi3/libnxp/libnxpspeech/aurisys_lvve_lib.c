#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <wrapped_audio.h>
#include <audio_task.h>
#include <arsi_call_type.h>

#include <arsi_api.h>
#include <arsi_library_entry_points.h> // declared library entry point

#include <aurisys_lvve_lib_private_type.h>

#include "LVVE.h"

#ifdef __cplusplus
extern "C" {
#endif


#define USE_CUST_INFO_FOR_NEW_VERSION

#ifdef USE_CUST_INFO_FOR_NEW_VERSION
static void *g_private_buf;
#endif

/* implement APIs */

#define ROUNDTO8(x) ((((x) + 7) >> 3) << 3)

status_t lvve_arsi_get_lib_version(string_buf_t *version_buf)
{
    LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;
    LVVE_VersionInfo     VersionInfo;
	
	LVVE_Status = LVVE_GetVersionInfo(&VersionInfo);
	if (LVVE_Status != LVVE_SUCCESS) {
		return FAILED_TRANSACTION;
	}
    if (version_buf == NULL) {
        return BAD_VALUE;
    }

    if (version_buf->memory_size < (strlen(VersionInfo.VersionNumber) + 1)) {
        version_buf->string_size = 0;
        return NOT_ENOUGH_DATA;
    }

    strncpy(version_buf->p_string, VersionInfo.VersionNumber, version_buf->memory_size);
    version_buf->string_size = strlen(VersionInfo.VersionNumber);

    return NO_ERROR;
}


status_t lvve_arsi_query_working_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    uint32_t                 *p_working_buf_size,
    const debug_log_fp_t      debug_log_fp)
{
    LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;
	LVVE_Tx_InstanceParams_st   InstanceParams_Tx;
	LVVE_Rx_InstanceParams_st   InstanceParams_Rx;
	LVM_MemoryTable_st          MemoryTable_Tx;
	LVM_MemoryTable_st          MemoryTable_Rx;
	uint32_t i;
	uint32_t mem_size = 0;

	switch (p_arsi_lib_config->sample_rate) {
        case 8000:
            InstanceParams_Rx.SampleRate = LVM_FS_8000;
            InstanceParams_Tx.SampleRate = LVM_FS_8000;
            break;
		case 11025:
            InstanceParams_Rx.SampleRate = LVM_FS_11025;
            InstanceParams_Tx.SampleRate = LVM_FS_11025;
            break;
		case 12000:
            InstanceParams_Rx.SampleRate = LVM_FS_12000;
            InstanceParams_Tx.SampleRate = LVM_FS_12000;
            break;
        case 16000:
            InstanceParams_Rx.SampleRate = LVM_FS_16000;
            InstanceParams_Tx.SampleRate = LVM_FS_16000;
            break;
		case 22050:
            InstanceParams_Rx.SampleRate = LVM_FS_22050;
            InstanceParams_Tx.SampleRate = LVM_FS_22050;
            break;
        case 24000:
            InstanceParams_Rx.SampleRate = LVM_FS_24000;
            InstanceParams_Tx.SampleRate = LVM_FS_24000;
            break;
        case 32000:
            InstanceParams_Rx.SampleRate = LVM_FS_32000;
            InstanceParams_Tx.SampleRate = LVM_FS_32000;
            break;
		case 44100:
            InstanceParams_Rx.SampleRate = LVM_FS_44100;
            InstanceParams_Tx.SampleRate = LVM_FS_44100;
        case 48000:
            InstanceParams_Rx.SampleRate = LVM_FS_48000;
            InstanceParams_Tx.SampleRate = LVM_FS_48000;
            break;
        default:
            debug_log_fp("LVVE Error: Sample Rate %d is not supported\n", p_arsi_lib_config->sample_rate);
            return FAILED_TRANSACTION;
    }
	InstanceParams_Tx.MaxBulkDelay = LVVE_MAX_BULK_DELAY;
	InstanceParams_Tx.ReferenceFIFOLength = 960; // data of 20 ms at 48000KHz.
	InstanceParams_Tx.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MAX;

	InstanceParams_Rx.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MAX;
	
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_working_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    /* task scene */
    debug_log_fp("%s(), scene = %d\n", __func__, p_arsi_task_config->task_scene);

    /* input device */
    debug_log_fp("input dev: 0x%x, fmt = 0x%x, fs: %u, max fs: %u, ch: %d, max ch: %d\n",
                 p_arsi_task_config->input_device_info.devices,
                 p_arsi_task_config->input_device_info.audio_format,
                 p_arsi_task_config->input_device_info.sample_rate,
                 p_arsi_task_config->max_input_device_sample_rate,
                 p_arsi_task_config->input_device_info.num_channels,
                 p_arsi_task_config->max_input_device_num_channels);

    /* output device */
    debug_log_fp("output dev: 0x%x, fmt = 0x%x, fs: %u, max fs: %u, ch: %d, max ch: %d\n",
                 p_arsi_task_config->output_device_info.devices,
                 p_arsi_task_config->output_device_info.audio_format,
                 p_arsi_task_config->output_device_info.sample_rate,
                 p_arsi_task_config->max_output_device_sample_rate,
                 p_arsi_task_config->output_device_info.num_channels,
                 p_arsi_task_config->max_output_device_num_channels);


    /* lib */
    debug_log_fp("lib, working fs: %u, fmt: 0x%x, frame = %d, b_interleave = %d\n",
                 p_arsi_lib_config->sample_rate,
                 p_arsi_lib_config->audio_format,
                 p_arsi_lib_config->frame_size_ms,
                 p_arsi_lib_config->b_interleave);

    /* buffer */
    if (p_arsi_lib_config->p_ul_buf_in) {
        debug_log_fp("ul in, ch: %d, buf fs: %u, read data fs: %u, fmt: 0x%x\n",
                     p_arsi_lib_config->p_ul_buf_in->num_channels,
                     p_arsi_lib_config->p_ul_buf_in->sample_rate_buffer,
                     p_arsi_lib_config->p_ul_buf_in->sample_rate_content,
                     p_arsi_lib_config->p_ul_buf_in->audio_format);
    }

    if (p_arsi_lib_config->p_ul_buf_out) {
        debug_log_fp("ul out, ch: %d, buf fs: %u, read data fs: %u, fmt: 0x%x\n",
                     p_arsi_lib_config->p_ul_buf_out->num_channels,
                     p_arsi_lib_config->p_ul_buf_out->sample_rate_buffer,
                     p_arsi_lib_config->p_ul_buf_out->sample_rate_content,
                     p_arsi_lib_config->p_ul_buf_out->audio_format);
    }

    if (p_arsi_lib_config->p_dl_buf_in) {
        debug_log_fp("dl in, ch: %d, buf fs: %u, read data fs: %u, fmt: 0x%x\n",
                     p_arsi_lib_config->p_dl_buf_in->num_channels,
                     p_arsi_lib_config->p_dl_buf_in->sample_rate_buffer,
                     p_arsi_lib_config->p_dl_buf_in->sample_rate_content,
                     p_arsi_lib_config->p_dl_buf_in->audio_format);
    }

    if (p_arsi_lib_config->p_dl_buf_out) {
        debug_log_fp("dl out, ch: %d, buf fs: %u, read data fs: %u, fmt: 0x%x\n",
                     p_arsi_lib_config->p_dl_buf_out->num_channels,
                     p_arsi_lib_config->p_dl_buf_out->sample_rate_buffer,
                     p_arsi_lib_config->p_dl_buf_out->sample_rate_content,
                     p_arsi_lib_config->p_dl_buf_out->audio_format);
    }


    /* calculate working buffer working_buf_size by task config & lib config*/
	LVVE_Status = LVVE_Tx_GetMemoryTable(LVM_NULL, &MemoryTable_Tx, &InstanceParams_Tx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Tx_GetMemoryTable: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}

	for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++) {
		if(MemoryTable_Tx.Region[i].Size != 0) {
			mem_size += ROUNDTO8(MemoryTable_Tx.Region[i].Size);
		}
	}

	LVVE_Status = LVVE_Rx_GetMemoryTable(LVM_NULL, &MemoryTable_Rx, &InstanceParams_Rx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Rx_GetMemoryTable: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}

	for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++) {
		if(MemoryTable_Rx.Region[i].Size != 0) {
			mem_size += ROUNDTO8(MemoryTable_Rx.Region[i].Size);
		}
	}
	mem_size += ROUNDTO8(LVVE_Tx2RxMessage_GetMaxSize());

	mem_size += ROUNDTO8(sizeof(lvve_lib_handle_t));
    *p_working_buf_size = mem_size;

    debug_log_fp("%s(), working_buf_size = %lu\n", __func__, *p_working_buf_size);

    return NO_ERROR;
}


status_t lvve_arsi_create_handler(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    data_buf_t               *p_working_buf,
    void                    **pp_handler,
    const debug_log_fp_t      debug_log_fp)
{
    lvve_lib_handle_t *lib_handler = NULL;
	LVVE_Tx_InstanceParams_st   InstanceParams_Tx;
	LVVE_Rx_InstanceParams_st   InstanceParams_Rx;
	LVM_MemoryTable_st          MemoryTable_Tx;
	LVM_MemoryTable_st          MemoryTable_Rx;
	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;
	int8_t *pBase = p_working_buf->p_buffer;
	uint32_t i;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_working_buf == NULL ||
        pp_handler == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    /* init handler by task_config */
    *pp_handler = p_working_buf->p_buffer;
    lib_handler = (lvve_lib_handle_t *)*pp_handler;

	// LVVE
	switch (p_arsi_lib_config->sample_rate) {
        case 8000:
            InstanceParams_Rx.SampleRate = LVM_FS_8000;
            InstanceParams_Tx.SampleRate = LVM_FS_8000;
            break;
		case 11025:
            InstanceParams_Rx.SampleRate = LVM_FS_11025;
            InstanceParams_Tx.SampleRate = LVM_FS_11025;
            break;
		case 12000:
            InstanceParams_Rx.SampleRate = LVM_FS_12000;
            InstanceParams_Tx.SampleRate = LVM_FS_12000;
            break;
        case 16000:
            InstanceParams_Rx.SampleRate = LVM_FS_16000;
            InstanceParams_Tx.SampleRate = LVM_FS_16000;
            break;
		case 22050:
            InstanceParams_Rx.SampleRate = LVM_FS_22050;
            InstanceParams_Tx.SampleRate = LVM_FS_22050;
            break;
        case 24000:
            InstanceParams_Rx.SampleRate = LVM_FS_24000;
            InstanceParams_Tx.SampleRate = LVM_FS_24000;
            break;
        case 32000:
            InstanceParams_Rx.SampleRate = LVM_FS_32000;
            InstanceParams_Tx.SampleRate = LVM_FS_32000;
            break;
		case 44100:
            InstanceParams_Rx.SampleRate = LVM_FS_44100;
            InstanceParams_Tx.SampleRate = LVM_FS_44100;
            break;
        case 48000:
            InstanceParams_Rx.SampleRate = LVM_FS_48000;
            InstanceParams_Tx.SampleRate = LVM_FS_48000;
            break;
        default:
            debug_log_fp("LVVE Error: Sample Rate %d is not supported\n", p_arsi_lib_config->sample_rate);
            return FAILED_TRANSACTION;
    }
	InstanceParams_Tx.MaxBulkDelay = LVVE_MAX_BULK_DELAY;
	InstanceParams_Tx.ReferenceFIFOLength = 960; // data of 20 ms at 48000KHz.
	InstanceParams_Tx.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MAX;

	InstanceParams_Rx.EQ_InstParams.EQ_MaxLength = LVEQ_EQ_LENGTH_MAX;

	LVVE_Status = LVVE_Tx_GetMemoryTable(LVM_NULL, &MemoryTable_Tx, &InstanceParams_Tx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Tx_GetMemoryTable: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}
	LVVE_Status = LVVE_Rx_GetMemoryTable(LVM_NULL, &MemoryTable_Rx, &InstanceParams_Rx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Rx_GetMemoryTable: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}
	
	pBase += ROUNDTO8(sizeof(lvve_lib_handle_t));

	for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++) {
		if(MemoryTable_Tx.Region[i].Size != 0) {
			MemoryTable_Tx.Region[i].pBaseAddress = (void *)pBase;
			pBase += ROUNDTO8(MemoryTable_Tx.Region[i].Size);
		}
	}

	for(i = 0; i < LVM_NR_MEMORY_REGIONS; i++) {
		if(MemoryTable_Rx.Region[i].Size != 0) {
			MemoryTable_Rx.Region[i].pBaseAddress = (void *)pBase;
			pBase += ROUNDTO8(MemoryTable_Rx.Region[i].Size);
		}
	}

	lib_handler->avc_message_ptr = (LVM_CHAR **)pBase;
	pBase += ROUNDTO8(LVVE_Tx2RxMessage_GetMaxSize());

	lib_handler->hInstance_Tx = LVM_NULL;
	LVVE_Status = LVVE_Tx_GetInstanceHandle(&lib_handler->hInstance_Tx,
                                            &MemoryTable_Tx,
                                            &InstanceParams_Tx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Tx_GetInstanceHandle: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}
	lib_handler->hInstance_Rx = LVM_NULL;
	LVVE_Status = LVVE_Rx_GetInstanceHandle(&lib_handler->hInstance_Rx,
                                            &MemoryTable_Rx,
                                            &InstanceParams_Rx);
	if (LVVE_Status != LVVE_SUCCESS) {
		debug_log_fp("LVVE_Rx_GetInstanceHandle: error[%d]\n", (int32_t)LVVE_Status);
		return FAILED_TRANSACTION;
	}

	LVVE_Status = LVVE_Tx_SetInputChannelCount(lib_handler->hInstance_Tx, 2);
    if (LVVE_Status != LVVE_SUCCESS){
        debug_log_fp("Error occurred during configuration: LVVE_Tx_SetInputChannelCount returned %d\n", (int32_t)LVVE_Status);
        return FAILED_TRANSACTION;
    }
	LVVE_Status = LVVE_Tx_SetOutputChannelCount(lib_handler->hInstance_Tx, 1);
    if (LVVE_Status != LVVE_SUCCESS){
        debug_log_fp("Error occurred during configuration: LVVE_Tx_SetOutputChannelCount returned %d\n", (int32_t)LVVE_Status);
        return FAILED_TRANSACTION;
    }
	LVVE_Status = LVVE_Tx_SetReferenceChannelCount(lib_handler->hInstance_Tx, 1);
    if (LVVE_Status != LVVE_SUCCESS){
        debug_log_fp("Error occurred during configuration: LVVE_Tx_SetReferenceChannelCount returned %d\n", (int32_t)LVVE_Status);
        return FAILED_TRANSACTION;
    }

	LVVE_Status = LVVE_Rx_SetInputChannelCount(lib_handler->hInstance_Rx, 1);
    if (LVVE_Status != LVVE_SUCCESS){
        debug_log_fp("Error occurred during configuration: LVVE_Rx_SetInputChannelCount returned %d\n", (int32_t)LVVE_Status);
        return FAILED_TRANSACTION;
    }
	LVVE_Status = LVVE_Rx_SetOutputChannelCount(lib_handler->hInstance_Rx, 1);
    if (LVVE_Status != LVVE_SUCCESS){
        debug_log_fp("Error occurred during configuration: LVVE_Rx_SetOutputChannelCount returned %d\n", (int32_t)LVVE_Status);
        return FAILED_TRANSACTION;
    }

    memcpy(&lib_handler->task_config,
           p_arsi_task_config,
           sizeof(arsi_task_config_t));

    memcpy(&lib_handler->lib_config,
           p_arsi_lib_config,
           sizeof(arsi_lib_config_t));

    lib_handler->print_log = debug_log_fp;

	if ((p_arsi_task_config->task_scene == TASK_SCENE_PHONE_CALL) && (p_param_buf->data_size == sizeof(lvve_sph_param_t) * 3)) {
		switch (p_arsi_lib_config->sample_rate) {
			case 8000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
				break;
			case 16000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t), sizeof(lvve_sph_param_t));
				break;
			case 32000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t) * 2, sizeof(lvve_sph_param_t));
				break;
			default:
				debug_log_fp("Unsupported TASK_SCENE_PHONE_CALL sample_rate=[%d]\n", p_arsi_lib_config->sample_rate);
				return BAD_VALUE;
		}
	}else if ((p_arsi_task_config->task_scene == TASK_SCENE_VOIP) && (p_param_buf->data_size == sizeof(lvve_sph_param_t) * 2)) {
		switch (p_arsi_lib_config->sample_rate) {
			case 16000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
				break;
			case 48000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t), sizeof(lvve_sph_param_t));
				break;
			default:
				debug_log_fp("Unsupported TASK_SCENE_VOIP sample_rate=[%d]\n", p_arsi_lib_config->sample_rate);
				return BAD_VALUE;
		}
	} else if (p_param_buf->data_size == sizeof(lvve_sph_param_t)) {
		memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
    } else {
		debug_log_fp("p_param_buf->data_size %u\n", p_param_buf->data_size);
		debug_log_fp("sizeof(lvve_sph_param_t) of %u\n", sizeof(lvve_sph_param_t));
        debug_log_fp("param size %u error!!\n", p_param_buf->data_size);
        return BAD_VALUE;
    }
    
		LVVE_Status = LVVE_Tx_SetPreset(lib_handler->hInstance_Tx, lib_handler->sph_param.LVVE_Tx_Preset_Buffer, LVVE_TX_PRESET_LENGTH, 0);

        if (LVVE_Status != LVVE_SUCCESS){
            lib_handler->print_log("Error returned by LVVE_Tx_SetPreset: Error_code = %d\n", LVVE_Status);
            return FAILED_TRANSACTION;
        }

		LVVE_Status = LVVE_Rx_SetPreset(lib_handler->hInstance_Rx, lib_handler->sph_param.LVVE_Rx_Preset_Buffer, LVVE_RX_PRESET_LENGTH, 0);

        if (LVVE_Status != LVVE_SUCCESS)
        {
            lib_handler->print_log("Error returned by LVVE_Rx_SetPreset: Error_code = %d\n", LVVE_Status);
            return FAILED_TRANSACTION;
        }
    lib_handler->avc_message_size = 0;
    
    lib_handler->tmp_buf_size = 1280;
    lib_handler->my_private_var = 0x5566;

    /* 0 dB */
    lib_handler->ul_analog_gain = 0;
    lib_handler->ul_digital_gain = 0;
    lib_handler->dl_analog_gain = 0;
    lib_handler->dl_digital_gain = 0;

    /* unmute dB */
    lib_handler->b_ul_mute_on = 0;
    lib_handler->b_dl_mute_on = 0;

    /* enhancement on */
    lib_handler->b_ul_enhance_on = 1;
    lib_handler->b_dl_enhance_on = 1;

    lib_handler->value_at_addr_0x1234 = 0;

    lib_handler->frame_index = 0;

    return NO_ERROR;
}

status_t lvve_arsi_query_max_debug_dump_buf_size(
    data_buf_t *p_debug_dump_buf,
    void       *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL || p_debug_dump_buf == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    p_debug_dump_buf->memory_size = sizeof(lvve_epl_buf_t);
    lib_handler->print_log("p_debug_dump_buf->memory_size = 0x%x\n",
                           p_debug_dump_buf->memory_size);

    return NO_ERROR;
}


status_t lvve_arsi_process_ul_buf(
    audio_buf_t *p_ul_buf_in,
    audio_buf_t *p_ul_buf_out,
    audio_buf_t *p_ul_ref_bufs,
    data_buf_t  *p_debug_dump_buf,
    void        *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
    extra_call_arg_t *p_extra_call_arg = NULL;

    uint32_t i = 0;
	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;

	LVM_PCM_Buffer_st   MicrophoneBuffer;
    LVM_PCM_Channel_t   MicrophoneChannels[2];
    LVM_PCM_Buffer_st   PreProcessedBuffer;
    LVM_PCM_Channel_t   PreprocessedChannels[1];
    LVM_PCM_Buffer_st   PlaybackBuffer;
    LVM_PCM_Channel_t   PlaybackChannels[1];

	MicrophoneBuffer.channels   = MicrophoneChannels;
    PreProcessedBuffer.channels = PreprocessedChannels;
    PlaybackBuffer.channels     = PlaybackChannels;

	MicrophoneBuffer.channels[0] = p_ul_buf_in->data_buf.p_buffer;	
	MicrophoneBuffer.channels[1] = (LVM_INT16 *)((LVM_CHAR *)p_ul_buf_in->data_buf.p_buffer + p_ul_buf_in->data_buf.data_size / p_ul_buf_in->num_channels);

	MicrophoneBuffer.FrameCount    = p_ul_buf_in->data_buf.data_size / p_ul_buf_in->num_channels / 2;

    if (p_ul_buf_in == NULL ||
        p_ul_buf_out == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("UL raw: type = %d, ch: %d, ch mask: 0x%x, fs for pcm: %u, fs for content: %u, fmt = 0x%x\n",
                           p_ul_buf_in->data_buf_type,
                           p_ul_buf_in->num_channels,
                           p_ul_buf_in->channel_mask,
                           p_ul_buf_in->sample_rate_buffer,
                           p_ul_buf_in->sample_rate_content,
                           p_ul_buf_in->audio_format);

    lib_handler->print_log("UL processed: type = %d, ch: %d, ch mask: 0x%x, fs for pcm: %u, fs for content: %u, fmt = 0x%x\n",
                           p_ul_buf_out->data_buf_type,
                           p_ul_buf_out->num_channels,
                           p_ul_buf_out->channel_mask,
                           p_ul_buf_out->sample_rate_buffer,
                           p_ul_buf_out->sample_rate_content,
                           p_ul_buf_out->audio_format);

    if (lib_handler->b_ul_mute_on == 1) {
        // mute
        memset(p_ul_buf_out->data_buf.p_buffer, 0, p_ul_buf_out->data_buf.memory_size);
        p_ul_buf_in->data_buf.data_size = 0;
        p_ul_buf_out->data_buf.data_size = p_ul_buf_out->data_buf.memory_size;
    } else if (lib_handler->b_ul_enhance_on == 0) {
        // bypass (TODO: in ch / out ch)
        memcpy(
            p_ul_buf_out->data_buf.p_buffer,
            p_ul_buf_in->data_buf.p_buffer,
            p_ul_buf_in->data_buf.data_size);
        p_ul_buf_out->data_buf.data_size = p_ul_buf_in->data_buf.data_size;
        p_ul_buf_in->data_buf.data_size = 0;
    } else {
#if 1
        // process: only bypass 1ch from raw data, ul_in -> ul_out
        //memcpy(
        //    p_ul_buf_out->data_buf.p_buffer,
        //    p_ul_buf_in->data_buf.p_buffer,
        //    p_ul_buf_out->data_buf.data_size);
#else
        // For debugging, aec_in -> ul_out
        memcpy(
            p_ul_buf_out->data_buf.p_buffer,
            p_ul_ref_bufs[0].data_buf.p_buffer,
            p_ul_buf_out->data_buf.p_buffer);
#endif        
        p_ul_buf_out->data_buf.data_size = p_ul_buf_in->data_buf.data_size / p_ul_buf_in->num_channels;
        p_ul_buf_in->data_buf.data_size = 0;
    }

#if 1
    if (p_ul_ref_bufs != NULL) {
        for (i = 0; i < lib_handler->lib_config.num_ul_ref_buf_array; i++) {
            lib_handler->print_log("data type %d\n", p_ul_ref_bufs[i].data_buf_type);

            switch (p_ul_ref_bufs[i].data_buf_type) {
            case DATA_BUF_ECHO_REF: {
                // demo clean echo ref buf
				lib_handler->print_log("p_ul_ref_bufs[%d].data_buf.data_size %d\n", i, p_ul_ref_bufs[i].data_buf.data_size);
                //memset(p_ul_ref_bufs[i].data_buf.p_buffer,
                //       0,
                //       p_ul_ref_bufs[i].data_buf.data_size);
                //p_ul_ref_bufs[i].data_buf.data_size = 0;
				// LVVE

				PlaybackBuffer.channels[0] = p_ul_ref_bufs[i].data_buf.p_buffer;
				PlaybackBuffer.FrameCount      = p_ul_ref_bufs[i].data_buf.data_size / 2;

				PreProcessedBuffer.channels[0] = p_ul_buf_out->data_buf.p_buffer;
				
				LVVE_Status = LVVE_Tx_PushReference( lib_handler->hInstance_Tx,     /* Instance handle */
                                                 &PlaybackBuffer); /* Input buffer with echo reference signal (played back on speaker) */
	            /*
	             	* Check for error and stop if needed
	             	*/
	            if (LVVE_Status != LVVE_SUCCESS){
	                lib_handler->print_log("Error while processing returned from LVVE_Tx_PushReference: %d\n", (int) LVVE_Status);
	                return FAILED_TRANSACTION;
	            }
				
				LVVE_Status = LVVE_Tx_Process(  lib_handler->hInstance_Tx,         /* Instance handle */
                                            &MicrophoneBuffer,    /* Input buffer with data from all microphones */
                                            &PreProcessedBuffer); /* Output buffer with preprocessed data */
	            /*
	             * Check for error and stop if needed
	             */
	            if (LVVE_Status != LVVE_SUCCESS){
	                lib_handler->print_log("Error while processing returned from LVVE_Tx_Process: %d\n", (int) LVVE_Status);
	                return FAILED_TRANSACTION;
	            }

	            LVVE_Status = LVVE_Tx_PublishTx2RxMessage(lib_handler->hInstance_Tx, (LVVE_Tx2RxMessage_Handle_t) lib_handler->avc_message_ptr, &lib_handler->avc_message_size);
	            /*
	             * Check for error and stop if needed
	             */
	            if (LVVE_Status != LVVE_SUCCESS){
	                lib_handler->print_log("Error while processing returned from LVVE_Tx_PublishTx2RxMessage: %d\n", (int) LVVE_Status);
	                return FAILED_TRANSACTION;
	            }
				memset(p_ul_ref_bufs[i].data_buf.p_buffer,
                       0,
                       p_ul_ref_bufs[i].data_buf.data_size);
                p_ul_ref_bufs[i].data_buf.data_size = 0;
                break;
            }
            case DATA_BUF_CALL_INFO: {
                p_extra_call_arg = (extra_call_arg_t *)p_ul_ref_bufs[i].data_buf.p_buffer;
                lib_handler->print_log("call_band_type: %d, call_net_type: %d\n",
                                       p_extra_call_arg->call_band_type,
                                       p_extra_call_arg->call_net_type);
                break;
            }
            default: {
                lib_handler->print_log("do nothing for data type %d\n",
                                       p_ul_ref_bufs[i].data_buf_type);
            }
            }
        }
    }
#endif
    lib_handler->frame_index++;
    //lib_handler->print_log("[UL] frame_index %u\n", lib_handler->frame_index);

    if (p_debug_dump_buf != NULL) { // might be NULL when debug dump turn off!!
        if (p_debug_dump_buf->p_buffer == NULL) {
            return NO_INIT;
        }
        if (p_debug_dump_buf->memory_size < sizeof(lvve_epl_buf_t)) {
            return BAD_VALUE;
        }

        lvve_epl_buf_t *lvve_epl_buf = (lvve_epl_buf_t *)p_debug_dump_buf->p_buffer;
        lvve_epl_buf->frame_index = lib_handler->frame_index;
        lvve_epl_buf->process_type = 0;
        memcpy(lvve_epl_buf->pcm_debug_buf,
               (char *)p_ul_buf_in->data_buf.p_buffer + 1280, // copy UL ch2 as debug info //LVVE: ?
               sizeof(lvve_epl_buf->pcm_debug_buf));

        lvve_epl_buf->analog_gain = lib_handler->ul_analog_gain;
        lvve_epl_buf->digital_gain = lib_handler->ul_digital_gain;
        lvve_epl_buf->mute_on = lib_handler->b_ul_mute_on;
        lvve_epl_buf->enhance_on = lib_handler->b_ul_enhance_on;

        p_debug_dump_buf->data_size = sizeof(lvve_epl_buf_t);
    }

    return NO_ERROR;
}


status_t lvve_arsi_process_dl_buf(
    audio_buf_t *p_dl_buf_in,
    audio_buf_t *p_dl_buf_out,
    audio_buf_t *p_dl_ref_bufs,
    data_buf_t  *p_debug_dump_buf,
    void        *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
    extra_call_arg_t *p_extra_call_arg = NULL;

    uint32_t i = 0;

	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;
	LVM_PCM_Buffer_st   LineInBuffer;
	LVM_PCM_Buffer_st   PostProcessedBuffer;
	LVM_PCM_Channel_t   LineInChannels[1];
	LVM_PCM_Channel_t   PostProcessedChannels[1];

    if (p_dl_buf_in == NULL ||
        p_dl_buf_out == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("DL raw: type = %d, ch: %d, ch mask: 0x%x, fs for pcm: %u, fs for content: %u, fmt = 0x%x, input size = %d\n",
                           p_dl_buf_in->data_buf_type,
                           p_dl_buf_in->num_channels,
                           p_dl_buf_in->channel_mask,
                           p_dl_buf_in->sample_rate_buffer,
                           p_dl_buf_in->sample_rate_content,
                           p_dl_buf_in->audio_format,
                           p_dl_buf_in->data_buf.data_size);

    lib_handler->print_log("DL processed: type = %d, ch: %d, ch mask: 0x%x, fs for pcm: %u, fs for content: %u, fmt = 0x%x\n",
                           p_dl_buf_out->data_buf_type,
                           p_dl_buf_out->num_channels,
                           p_dl_buf_out->channel_mask,
                           p_dl_buf_out->sample_rate_buffer,
                           p_dl_buf_out->sample_rate_content,
                           p_dl_buf_out->audio_format);


    if (lib_handler->b_dl_mute_on == 1) {
        // mute
        memset(p_dl_buf_out->data_buf.p_buffer, 0, p_dl_buf_out->data_buf.memory_size);
        p_dl_buf_in->data_buf.data_size = 0;
        p_dl_buf_out->data_buf.data_size = p_dl_buf_out->data_buf.memory_size;
    } else if (lib_handler->b_dl_enhance_on == 0) {
        // bypass (TODO: in ch / out ch)
        memcpy(
            p_dl_buf_out->data_buf.p_buffer,
            p_dl_buf_in->data_buf.p_buffer,
            p_dl_buf_in->data_buf.data_size);
        p_dl_buf_out->data_buf.data_size = p_dl_buf_in->data_buf.data_size;
        p_dl_buf_in->data_buf.data_size = 0;
    } else {
#if 1
        // process: only bypass 1ch from raw data, dl_in -> dl_out
        // memcpy(
        //     p_dl_buf_out->data_buf.p_buffer,
        //     p_dl_buf_in->data_buf.p_buffer,
        //     p_dl_buf_in->data_buf.data_size);
		// LVVE

		lib_handler->print_log("lvve_arsi_process_dl_buf lib_handler->avc_message_size: [%d]\n", (int) lib_handler->avc_message_size);
		if (lib_handler->avc_message_size != 0) {
            LVVE_Status = LVVE_Rx_ConsumeTx2RxMessage(lib_handler->hInstance_Rx, (LVVE_Tx2RxMessage_Handle_t) lib_handler->avc_message_ptr, lib_handler->avc_message_size);
            /*
             * Check for error and stop if needed
             */
            if (LVVE_Status != LVVE_SUCCESS){
                lib_handler->print_log("Error while processing returned from LVVE_Rx_ConsumeTx2RxMessage: %d\n", (int) LVVE_Status);
                return FAILED_TRANSACTION;
            }
        }
		LineInBuffer.channels        = LineInChannels;
        PostProcessedBuffer.channels = PostProcessedChannels;
		LineInBuffer.channels[0]        = p_dl_buf_in->data_buf.p_buffer;
        LineInBuffer.FrameCount         = p_dl_buf_in->data_buf.data_size/2; // 16 bits every frame
        PostProcessedBuffer.channels[0] = p_dl_buf_out->data_buf.p_buffer;
		LVVE_Status = LVVE_Rx_Process( lib_handler->hInstance_Rx,           /* Instance handle */
                                           &LineInBuffer,          /* Input buffer with data from line in */
                                           &PostProcessedBuffer); /* Output buffer with postprocessed data */
		if (LVVE_Status != LVVE_SUCCESS) {
			lib_handler->print_log("LVVE_Rx_Process: error[%d]\n", (int32_t)LVVE_Status);
			return FAILED_TRANSACTION;
		}
#else
        /* Debug purpose SmartPA, iv_in -> dl_out*/
        memcpy(
            p_dl_buf_out->data_buf.p_buffer,
            p_dl_ref_bufs[0].data_buf.p_buffer,
            p_dl_buf_out->data_buf.memory_size);
#endif
        p_dl_buf_out->data_buf.data_size = p_dl_buf_in->data_buf.data_size;
        p_dl_buf_in->data_buf.data_size = 0;
    }

    if (p_dl_ref_bufs != NULL) {
        for (i = 0; i < lib_handler->lib_config.num_dl_ref_buf_array; i++) {
            lib_handler->print_log("data type %d\n", p_dl_ref_bufs[i].data_buf_type);

            switch (p_dl_ref_bufs[i].data_buf_type) {
            case DATA_BUF_CALL_INFO: {
                p_extra_call_arg = (extra_call_arg_t *)p_dl_ref_bufs[i].data_buf.p_buffer;
                lib_handler->print_log("call_band_type: %d, call_net_type: %d\n",
                                       p_extra_call_arg->call_band_type,
                                       p_extra_call_arg->call_net_type);
                break;
            }
            case DATA_BUF_IV_BUFFER: {
                // demo clean iv buf
                memset(p_dl_ref_bufs[i].data_buf.p_buffer,
                       0,
                       p_dl_ref_bufs[i].data_buf.data_size);
                p_dl_ref_bufs[i].data_buf.data_size = 0;
                break;
            }
            default: {
                lib_handler->print_log("do nothing for data type %d\n",
                                       p_dl_ref_bufs[i].data_buf_type);
            }
            }
        }
    }

    lib_handler->frame_index++;
    //lib_handler->print_log("[DL] frame_index %u\n", lib_handler->frame_index);

    if (p_debug_dump_buf != NULL) { // might be NULL when debug dump turn off!!
        if (p_debug_dump_buf->p_buffer == NULL) {
            return NO_INIT;
        }
        if (p_debug_dump_buf->memory_size < sizeof(lvve_epl_buf_t)) {
            return BAD_VALUE;
        }

        lvve_epl_buf_t *lvve_epl_buf = (lvve_epl_buf_t *)p_debug_dump_buf->p_buffer;
        lvve_epl_buf->frame_index = lib_handler->frame_index;
        lvve_epl_buf->process_type = 1;
        memcpy(lvve_epl_buf->pcm_debug_buf,
               p_dl_buf_in->data_buf.p_buffer, // copy DL ch1 as debug info
               sizeof(lvve_epl_buf->pcm_debug_buf));

        lvve_epl_buf->analog_gain = lib_handler->dl_analog_gain;
        lvve_epl_buf->digital_gain = lib_handler->dl_digital_gain;
        lvve_epl_buf->mute_on = lib_handler->b_dl_mute_on;
        lvve_epl_buf->enhance_on = lib_handler->b_dl_enhance_on;

        p_debug_dump_buf->data_size = sizeof(lvve_epl_buf_t);
    }

    return NO_ERROR;
}


status_t lvve_arsi_reset_handler(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_handler == NULL) {
        return BAD_VALUE;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("%s(), p_handler = %p\n", __func__, p_handler);

    /* reset all private variables to init state */
    lib_handler->tmp_buf_size = 1280;
    lib_handler->my_private_var = 0x5566;

    /* 0 dB */
    lib_handler->ul_analog_gain = 0;
    lib_handler->ul_digital_gain = 0;
    lib_handler->dl_analog_gain = 0;
    lib_handler->dl_digital_gain = 0;

    /* unmute dB */
    lib_handler->b_ul_mute_on = 0;
    lib_handler->b_dl_mute_on = 0;

    /* enhancement on */
    lib_handler->b_ul_enhance_on = 1;
    lib_handler->b_dl_enhance_on = 1;

    lib_handler->value_at_addr_0x1234 = 0;

    return NO_ERROR;
}


status_t lvve_arsi_destroy_handler(void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("%s(), p_handler = %p\n", __func__, p_handler);

    return NO_ERROR;
}


status_t lvve_arsi_update_device(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    audio_devices_t input_device_old =
        lib_handler->task_config.input_device_info.devices;
    audio_devices_t input_device_new =
        p_arsi_task_config->input_device_info.devices;

    audio_devices_t output_devices_old =
        lib_handler->task_config.output_device_info.devices;
    audio_devices_t output_devices_new =
        p_arsi_task_config->output_device_info.devices;

    lib_handler->print_log("input device: 0x%x => 0x%x\n",
                           input_device_old,
                           input_device_new);
    lib_handler->print_log("output device: 0x%x => 0x%x\n",
                           output_devices_old,
                           output_devices_new);

    lib_handler->print_log("input fs for pcm: %u -> %u, fs for content: %u -> %d\n",
                           lib_handler->lib_config.p_ul_buf_in->sample_rate_buffer,
                           p_arsi_lib_config->p_ul_buf_in->sample_rate_buffer,
                           lib_handler->lib_config.p_ul_buf_in->sample_rate_content,
                           p_arsi_lib_config->p_ul_buf_in->sample_rate_content);

    lib_handler->print_log("output fs for pcm: %u -> %u, fs for content: %u -> %d\n",
                           lib_handler->lib_config.p_ul_buf_in->sample_rate_buffer,
                           p_arsi_lib_config->p_ul_buf_in->sample_rate_buffer,
                           lib_handler->lib_config.p_ul_buf_in->sample_rate_content,
                           p_arsi_lib_config->p_ul_buf_in->sample_rate_content);

	if ((p_arsi_task_config->task_scene == TASK_SCENE_PHONE_CALL) && (p_param_buf->data_size == sizeof(lvve_sph_param_t) * 3)) {
		switch (p_arsi_lib_config->sample_rate) {
			case 8000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
				break;
			case 16000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t), sizeof(lvve_sph_param_t));
				break;
			case 32000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t) * 2, sizeof(lvve_sph_param_t));
				break;
			default:
				lib_handler->print_log("Unsupported TASK_SCENE_PHONE_CALL sample_rate=[%d]\n", p_arsi_lib_config->sample_rate);
				return BAD_VALUE;
		}
	}else if ((p_arsi_task_config->task_scene == TASK_SCENE_VOIP) && (p_param_buf->data_size == sizeof(lvve_sph_param_t) * 2)) {
		switch (p_arsi_lib_config->sample_rate) {
			case 16000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
				break;
			case 48000:
				memcpy(&lib_handler->sph_param, p_param_buf->p_buffer + sizeof(lvve_sph_param_t), sizeof(lvve_sph_param_t));
				break;
			default:
				lib_handler->print_log("Unsupported TASK_SCENE_VOIP sample_rate=[%d]\n", p_arsi_lib_config->sample_rate);
				return BAD_VALUE;
		}
	} else if (p_param_buf->data_size == sizeof(lvve_sph_param_t)) {
		memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));
    } else {
		lib_handler->print_log("lvve_arsi_update_device p_param_buf->data_size %u\n", p_param_buf->data_size);
		lib_handler->print_log("lvve_arsi_update_device sizeof(lvve_sph_param_t) of %u\n", sizeof(lvve_sph_param_t));
        lib_handler->print_log("lvve_arsi_update_device param size %u error!!\n", p_param_buf->data_size);
        return BAD_VALUE;
    }

    /* update info */
    memcpy(&lib_handler->task_config,
           p_arsi_task_config,
           sizeof(arsi_task_config_t));

    memcpy(&lib_handler->lib_config,
           p_arsi_lib_config,
           sizeof(arsi_lib_config_t));

    return NO_ERROR;
}


status_t lvve_arsi_update_param(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const data_buf_t         *p_param_buf,
    void                     *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;
	int i = 0;
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        p_param_buf == NULL ||
        p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;


    /* pick up new param and update handler */
    if (p_param_buf->data_size != sizeof(lvve_sph_param_t)) {
        lib_handler->print_log("[lvve_update_param]param size %u error!!\n", p_param_buf->data_size);
        return BAD_VALUE;
    } else {
        //memcpy(&lib_handler->sph_param,
        //       p_param_buf->p_buffer,
        //       sizeof(arsi_lib_config_t));
		// LVVE
		memcpy(&lib_handler->sph_param, p_param_buf->p_buffer, sizeof(lvve_sph_param_t));		
		for(i =0;i<10;i++)
		 lib_handler->print_log("[lvve_update_param] p_param_buf->p_buffer[%d] = %d", i, *((char *)p_param_buf->p_buffer+i));
	    for(i =0;i<10;i++)
		 lib_handler->print_log("[lvve_update_param] lib_handler->sph_param[%d] = %d", i, *(lib_handler->sph_param.LVVE_Tx_Preset_Buffer+i));
		LVVE_Status = LVVE_Tx_SetPreset(lib_handler->hInstance_Tx, lib_handler->sph_param.LVVE_Tx_Preset_Buffer, LVVE_TX_PRESET_LENGTH, 0);

        if (LVVE_Status != LVVE_SUCCESS){
            lib_handler->print_log("Error returned by LVVE_Tx_SetPreset: Error_code = %d\n", LVVE_Status);
            return FAILED_TRANSACTION;
        }

		LVVE_Status = LVVE_Rx_SetPreset(lib_handler->hInstance_Rx, lib_handler->sph_param.LVVE_Rx_Preset_Buffer, LVVE_RX_PRESET_LENGTH, 0);

        if (LVVE_Status != LVVE_SUCCESS)
        {
            lib_handler->print_log("Error returned by LVVE_Rx_SetPreset: Error_code = %d\n", LVVE_Status);
            return FAILED_TRANSACTION;
        }
    }

    return NO_ERROR;
}


#ifdef USE_CUST_INFO_FOR_NEW_VERSION
/* API after V1.3.0 */
status_t lvve_arsi_query_param_buf_size_by_custom_info(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const string_buf_t       *custom_info,
    uint32_t                 *p_param_buf_size,
    const debug_log_fp_t      debug_log_fp)
{
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

	if (p_arsi_task_config->task_scene == TASK_SCENE_PHONE_CALL) {
		*p_param_buf_size = sizeof(lvve_sph_param_t) * 3;
	} else if (p_arsi_task_config->task_scene == TASK_SCENE_VOIP) {
		*p_param_buf_size = sizeof(lvve_sph_param_t) * 2;
	} else {
    	*p_param_buf_size = sizeof(lvve_sph_param_t);
  	}

    debug_log_fp("%s(), get param buf size %u for enh mode %s from file %s\n",
                 __func__, *p_param_buf_size, custom_info->p_string, param_file_path->p_string);

    return NO_ERROR;
}


status_t lvve_arsi_load_param(
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const debug_log_fp_t      debug_log_fp)
{
    if (product_info == NULL ||
        param_file_path == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    debug_log_fp("%s(), product_info %s, parsing file %s\n",
                 __func__, product_info->p_string, param_file_path->p_string);

    if (g_private_buf == NULL) {
        g_private_buf = malloc(1024);
        // NOTE: must use kal_pvPortMalloc() in FreeRTOS. #include <FreeRTOS.h>
    }

    // do fopen & fread to local private buf
    memset(g_private_buf, 0, 1024);


    debug_log_fp("%s(), alloc private buf %p\n", __func__, g_private_buf);

    return NO_ERROR;
}


status_t lvve_arsi_parsing_param_file_by_custom_info(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const string_buf_t       *custom_info,
    data_buf_t               *p_param_buf,
    const debug_log_fp_t      debug_log_fp)
{
    lvve_sph_param_t *p_sph_param = NULL;

    char key[32];
    char value[64];

	// LVVE
	FILE * fp_bin;
	uint32_t bin_size;
	size_t result;

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    debug_log_fp("%s(), parsing file %s, custom_info = %s, private buf %p\n",
                 __func__, param_file_path->p_string, custom_info->p_string,
                 g_private_buf);

    p_sph_param = (lvve_sph_param_t *)p_param_buf->p_buffer;
    debug_log_fp("p_sph_param: %p, memory_size: %u, sizeof(lvve_sph_param_t) = %lu\n",
                 p_sph_param, p_param_buf->memory_size, sizeof(lvve_sph_param_t));

    char *ptr = product_info->p_string;
    char *pkey = NULL;
    char *pvalue = NULL;
    while (*ptr != '\0' && strchr(ptr, '=') != NULL) {
        // get key
        pkey = key;
        while (*ptr != '=') {
            *pkey++ = *ptr++;
        }
        *pkey = '\0';

        // skip '='
        ptr++;

        // get value
        pvalue = value;
        while (*ptr != ',' && *ptr != '\0') {
            *pvalue++ = *ptr++;
        }
        *pvalue = '\0';
        debug_log_fp("product info: key: %s, value: %s\n", key, value);

        // skip ','
        if (*ptr == ',') {
            ptr++;
        }
    }

    // should get param from param_file_path
    // LVVE
#if 1
    fp_bin = fopen(param_file_path->p_string, "rb");
	if (fp_bin != NULL) {
		debug_log_fp("The file %s was opened\n", param_file_path->p_string);
	} else {
		debug_log_fp(" The file %s was not opened\n", param_file_path->p_string);
		return FAILED_TRANSACTION;
	}

	fseek(fp_bin, 0, SEEK_END);
	bin_size = ftell(fp_bin);
	rewind(fp_bin);

	result = fread(p_param_buf->p_buffer, 1, bin_size, fp_bin);
	if (result != bin_size) {
		debug_log_fp(" The file %s read error, result=%d, bin_size=%d\n", param_file_path->p_string, result, bin_size);
		fclose(fp_bin);
		return FAILED_TRANSACTION;
	}
	fclose(fp_bin);
#endif

    //memset((void *)p_sph_param->param1, 0x55, sizeof(p_sph_param->param1));
    //memset((void *)p_sph_param->param2, 0x66, sizeof(p_sph_param->param2));
    p_param_buf->data_size = bin_size;

    return NO_ERROR;
}
#else
/* API before V1.3.0, phase out!! */
status_t lvve_arsi_query_param_buf_size(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const int32_t             enhancement_mode,
    uint32_t                 *p_param_buf_size,
    const debug_log_fp_t      debug_log_fp)
{
    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf_size == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    *p_param_buf_size = sizeof(lvve_sph_param_t);

    debug_log_fp("%s(), get param buf size %u for enh mode %d from file %s\n",
                 __func__, *p_param_buf_size, enhancement_mode, param_file_path->p_string);

    return NO_ERROR;
}


status_t lvve_arsi_parsing_param_file(
    const arsi_task_config_t *p_arsi_task_config,
    const arsi_lib_config_t  *p_arsi_lib_config,
    const string_buf_t       *product_info,
    const string_buf_t       *param_file_path,
    const int32_t             enhancement_mode,
    data_buf_t               *p_param_buf,
    const debug_log_fp_t      debug_log_fp)
{
    lvve_sph_param_t *p_sph_param = NULL;

    char key[32];
    char value[64];

    if (p_arsi_task_config == NULL ||
        p_arsi_lib_config == NULL ||
        product_info == NULL ||
        param_file_path == NULL ||
        p_param_buf == NULL ||
        debug_log_fp == NULL) {
        return BAD_VALUE;
    }

    debug_log_fp("%s(), parsing file %s, enhancement_mode = %d\n",
                 __func__, param_file_path->p_string, enhancement_mode);

    p_sph_param = (lvve_sph_param_t *)p_param_buf->p_buffer;
    debug_log_fp("p_sph_param: %p, memory_size: %u, sizeof(lvve_sph_param_t) = %lu\n",
                 p_sph_param, p_param_buf->memory_size, sizeof(lvve_sph_param_t));

    char *ptr = product_info->p_string;
    char *pkey = NULL;
    char *pvalue = NULL;
    while (*ptr != '\0' && strchr(ptr, '=') != NULL) {
        // get key
        pkey = key;
        while (*ptr != '=') {
            *pkey++ = *ptr++;
        }
        *pkey = '\0';

        // skip '='
        ptr++;

        // get value
        pvalue = value;
        while (*ptr != ',' && *ptr != '\0') {
            *pvalue++ = *ptr++;
        }
        *pvalue = '\0';
        debug_log_fp("product info: key: %s, value: %s\n", key, value);

        // skip ','
        if (*ptr == ',') {
            ptr++;
        }
    }

    // should get param from param_file_path


    //memset((void *)p_sph_param->param1, 0x55, sizeof(p_sph_param->param1));
    //memset((void *)p_sph_param->param2, 0x66, sizeof(p_sph_param->param2));
    p_param_buf->data_size = sizeof(lvve_sph_param_t);

    return NO_ERROR;
}
#endif


status_t lvve_arsi_set_addr_value(
    const uint32_t addr,
    const uint32_t value,
    void          *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("%s(), set value 0x%x at addr 0x%x\n",
                           __func__, value, addr);

    if (addr == 0x1234) {
        lib_handler->value_at_addr_0x1234 = value;
    }

    return NO_ERROR;
}


status_t lvve_arsi_get_addr_value(
    const uint32_t addr,
    uint32_t      *p_value,
    void          *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_value == NULL || p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    if (addr == 0x1234) {
        *p_value = lib_handler->value_at_addr_0x1234;
    }

    lib_handler->print_log("%s(), value 0x%x at addr 0x%x\n",
                           __func__, *p_value, addr);
    return NO_ERROR;
}


status_t lvve_arsi_set_key_value_pair(
    const string_buf_t *key_value_pair,
    void               *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (key_value_pair == NULL || p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("%s(), key value pair = %s\n",
                           __func__, key_value_pair->p_string);

    if (strcmp(key_value_pair->p_string, "HAHA=on") == 0) {
        lib_handler->my_private_var = 0x5566;
    } else if (strcmp(key_value_pair->p_string, "HAHA=off") == 0) {
        lib_handler->my_private_var = 0;
    }

    return NO_ERROR;
}


status_t lvve_arsi_get_key_value_pair(
    string_buf_t *key_value_pair,
    void         *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (key_value_pair == NULL || p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("+%s(), key = %s\n",
                           __func__, key_value_pair->p_string);

    if (strcmp(key_value_pair->p_string, "HAHA") == 0) {
        if (lib_handler->my_private_var == 0) {
            strcat(key_value_pair->p_string, "=off");
        } else if (lib_handler->my_private_var == 0x5566) {
            strcat(key_value_pair->p_string, "=on");
        }
    }

    lib_handler->print_log("-%s(), key value pair => %s\n",
                           __func__, key_value_pair->p_string);
    return NO_ERROR;
}


status_t lvve_arsi_set_ul_digital_gain(
    const int16_t ul_analog_gain_ref_only,
    const int16_t ul_digital_gain,
    void         *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("ul_digital_gain = %d\n", ul_digital_gain);
	if ((ul_digital_gain / 4 > 24) || (ul_digital_gain / 4 < -96)) {
		return FAILED_TRANSACTION;
	}

    lib_handler->ul_analog_gain  = ul_analog_gain_ref_only;
    lib_handler->ul_digital_gain = ul_digital_gain;

	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x48] = 1;
	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x49] = 0;
	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x4A] = 0;
	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x4B] = 0;
	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x4C] = (ul_digital_gain / 4) & 0x00FF;
	lib_handler->sph_param.LVVE_Tx_Preset_Buffer[0x4D] = ((ul_digital_gain / 4) >> 8) & 0x00FF;

	LVVE_Status = LVVE_Tx_SetPreset(lib_handler->hInstance_Tx, lib_handler->sph_param.LVVE_Tx_Preset_Buffer, LVVE_TX_PRESET_LENGTH, 0);

    if (LVVE_Status != LVVE_SUCCESS)
    {
        lib_handler->print_log("lvve_arsi_set_ul_digital_gain LVVE_Tx_SetPreset: Error_code = %d\n", LVVE_Status);
        return FAILED_TRANSACTION;
    }

    return NO_ERROR;
}


status_t lvve_arsi_set_dl_digital_gain(
    const int16_t dl_analog_gain_ref_only,
    const int16_t dl_digital_gain,
    void         *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
	LVVE_ReturnStatus_en LVVE_Status = LVVE_SUCCESS;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("dl_digital_gain = %d\n", dl_digital_gain);
	if ((dl_digital_gain / 4 > 24) || (dl_digital_gain / 4 < -96)) {
		return FAILED_TRANSACTION;
	}

    lib_handler->dl_analog_gain  = dl_analog_gain_ref_only;
    lib_handler->dl_digital_gain = dl_digital_gain;

	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x26] = 1;
	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x27] = 0;
	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x28] = 0;
	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x29] = 0;
	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x2A] = (dl_digital_gain / 4) & 0x00FF;
	lib_handler->sph_param.LVVE_Rx_Preset_Buffer[0x2B] = ((dl_digital_gain / 4) >> 8) & 0x00FF;

	LVVE_Status = LVVE_Rx_SetPreset(lib_handler->hInstance_Rx, lib_handler->sph_param.LVVE_Rx_Preset_Buffer, LVVE_RX_PRESET_LENGTH, 0);

    if (LVVE_Status != LVVE_SUCCESS)
    {
        lib_handler->print_log("lvve_arsi_set_dl_digital_gain LVVE_Rx_SetPreset: Error_code = %d\n", LVVE_Status);
        return FAILED_TRANSACTION;
    }

    return NO_ERROR;
}


status_t lvve_arsi_set_ul_mute(const uint8_t b_mute_on, void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("b_mute_on = %d\n", b_mute_on);

    lib_handler->b_ul_mute_on = b_mute_on;

    return NO_ERROR;
}


status_t lvve_arsi_set_dl_mute(const uint8_t b_mute_on, void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("b_mute_on = %d\n", b_mute_on);

    lib_handler->b_dl_mute_on = b_mute_on;

    return NO_ERROR;
}


status_t lvve_arsi_set_ul_enhance(const uint8_t b_enhance_on, void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("b_enhance_on = %d\n", b_enhance_on);

    lib_handler->b_ul_enhance_on = b_enhance_on;

    return NO_ERROR;
}


status_t lvve_arsi_set_dl_enhance(const uint8_t b_enhance_on, void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log("b_enhance_on = %d\n", b_enhance_on);

    lib_handler->b_dl_enhance_on = b_enhance_on;

    return NO_ERROR;
}


status_t lvve_arsi_set_debug_log_fp(const debug_log_fp_t debug_log,
                                    void *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;

    lib_handler->print_log = debug_log;
    return NO_ERROR;
}


status_t lvve_arsi_query_process_unit_bytes(
    uint32_t *p_uplink_bytes,
    uint32_t *p_downlink_bytes,
    void     *p_handler)
{
    lvve_lib_handle_t *lib_handler = NULL;
    arsi_lib_config_t  *lib_config = NULL;

    if (p_handler == NULL) {
        return NO_INIT;
    }
    if (p_uplink_bytes == NULL || p_downlink_bytes == NULL) {
        return INVALID_OPERATION;
    }

    lib_handler = (lvve_lib_handle_t *)p_handler;
    lib_config = &lib_handler->lib_config;

    if (lib_config->frame_size_ms == 0) { /* non frame base */
        /* assume at least 4K byte for each process */
        *p_uplink_bytes = 4096;
        *p_downlink_bytes = 4096;
    } else { /* frame base */
        /* UL */
        if (lib_config->p_ul_buf_in == NULL) {
            *p_uplink_bytes = 0;
        } else {
            *p_uplink_bytes =
                (AUDIO_BYTES_PER_SAMPLE(lib_config->p_ul_buf_in->audio_format) *
                 lib_config->p_ul_buf_in->num_channels *
                 lib_config->p_ul_buf_in->sample_rate_buffer *
                 lib_config->frame_size_ms) / 1000;
        }

        /* DL */
        if (lib_config->p_dl_buf_in == NULL) {
            *p_downlink_bytes = 0;
        } else {
            *p_downlink_bytes =
                (AUDIO_BYTES_PER_SAMPLE(lib_config->p_dl_buf_in->audio_format) *
                 lib_config->p_dl_buf_in->num_channels *
                 lib_config->p_dl_buf_in->sample_rate_buffer *
                 lib_config->frame_size_ms) / 1000;
        }
    }

    return NO_ERROR;
}


/* For static link, like libXXX.a in FreeRTOS --> */
void lvve_arsi_assign_lib_fp(AurisysLibInterface *lib)
{
    lib->arsi_get_lib_version = lvve_arsi_get_lib_version;
    lib->arsi_query_working_buf_size = lvve_arsi_query_working_buf_size;
    lib->arsi_create_handler = lvve_arsi_create_handler;
    lib->arsi_query_max_debug_dump_buf_size = lvve_arsi_query_max_debug_dump_buf_size;
    lib->arsi_process_ul_buf = lvve_arsi_process_ul_buf;
    lib->arsi_process_dl_buf = lvve_arsi_process_dl_buf;
    lib->arsi_reset_handler = lvve_arsi_reset_handler;
    lib->arsi_destroy_handler = lvve_arsi_destroy_handler;
    lib->arsi_update_device = lvve_arsi_update_device;
    lib->arsi_update_param = lvve_arsi_update_param;
#ifdef USE_CUST_INFO_FOR_NEW_VERSION
    lib->arsi_query_param_buf_size = NULL;
    lib->arsi_parsing_param_file = NULL;
#else
    lib->arsi_query_param_buf_size = lvve_arsi_query_param_buf_size;
    lib->arsi_parsing_param_file = lvve_arsi_parsing_param_file;
#endif
    lib->arsi_set_addr_value = lvve_arsi_set_addr_value;
    lib->arsi_get_addr_value = lvve_arsi_get_addr_value;
    lib->arsi_set_key_value_pair = lvve_arsi_set_key_value_pair;
    lib->arsi_get_key_value_pair = lvve_arsi_get_key_value_pair;
    lib->arsi_set_ul_digital_gain = lvve_arsi_set_ul_digital_gain;
    lib->arsi_set_dl_digital_gain = lvve_arsi_set_dl_digital_gain;
    lib->arsi_set_ul_mute = lvve_arsi_set_ul_mute;
    lib->arsi_set_dl_mute = lvve_arsi_set_dl_mute;
    lib->arsi_set_ul_enhance = lvve_arsi_set_ul_enhance;
    lib->arsi_set_dl_enhance = lvve_arsi_set_dl_enhance;
    lib->arsi_set_debug_log_fp = lvve_arsi_set_debug_log_fp;
    lib->arsi_query_process_unit_bytes = lvve_arsi_query_process_unit_bytes;
#ifdef USE_CUST_INFO_FOR_NEW_VERSION
    lib->arsi_load_param = lvve_arsi_load_param;
    lib->arsi_query_param_buf_size_by_custom_info = lvve_arsi_query_param_buf_size_by_custom_info;
    lib->arsi_parsing_param_file_by_custom_info = lvve_arsi_parsing_param_file_by_custom_info;
#endif
}
/* <-- For static link, like libXXX.a in FreeRTOS */


#if 0
/* ONLY for dynamic link, like libXXX.so in HAL --> */
void dynamic_link_arsi_assign_lib_fp(AurisysLibInterface *lib)
{
    lvve_arsi_assign_lib_fp(lib); /* like */
}
/* <-- ONLY for dynamic link, like libXXX.so in HAL */
#endif



#ifdef __cplusplus
}  /* extern "C" */
#endif

